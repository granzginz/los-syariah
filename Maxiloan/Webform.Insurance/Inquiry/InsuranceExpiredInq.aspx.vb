﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
'Imports Maxiloan.Parameter.CollZipCode
Imports Maxiloan.Parameter.InsInqEntities
#End Region

Public Class InsuranceExpiredInq
    Inherits Maxiloan.Webform.WebBased

#Region "Property "
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("CmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CmdWhere") = Value
        End Set
    End Property
    Private Property CmdWhere2() As String
        Get
            Return CType(viewstate("CmdWhere2"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CmdWhere2") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(viewstate("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property
    Private Property BindMenu() As String
        Get
            Return CStr(viewstate("BindMenu"))
        End Get
        Set(ByVal Value As String)
            viewstate("BindMenu") = Value
        End Set
    End Property

#End Region
#Region "PrivateConstanta"
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private m_controller As New DataUserControlController
    Private oControllerChild As New GeneralPagingController
    Dim oEntitesChild As New Parameter.InsCoAllocationDetailList
    Dim oCustomClass As New Parameter.InsInqEntities
    Dim oInsuranceController As New InsuranceInq
    Protected WithEvents SDate As ValidDate
    Protected WithEvents EDate As ValidDate

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "InqInsExpired"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                PnlSearch.Visible = True
                pnlList.Visible = False


                Dim dtbranch As New DataTable
                Me.sesBranchId = Replace(Me.sesBranchId, "'", "")
                dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                With cboParent
                    .DataTextField = "Name"
                    .DataValueField = "ID"
                    .DataSource = dtbranch
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
                getchildcombo()
                Me.CmdWhere = ""
                Me.SortBy = ""
            End If
        End If
    End Sub
    Protected Function getchildcombo() As String
        With oEntitesChild
            .strConnection = GetConnectionString
            .BranchId = Me.sesBranchId
        End With
        Dim dtInsCo As New DataTable
        oEntitesChild = oControllerChild.GetInsuranceBranchCombo(oEntitesChild)
        dtInsCo = oEntitesChild.ListData
        Response.Write(GenerateScript(dtInsCo))
    End Function
    Protected Function BranchIDChange() As String
        Return "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= " var ListData " & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" BranchID = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("BranchID")).Trim Then
                        strType = CStr(DataRow(i)("BranchID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("ChildId")), "null", DataRow(i)("ChildId"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("ChildId")), "null", DataRow(i)("ChildId"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"
        Return strScript
    End Function


    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("InsuranceExpiredInq.aspx")
    End Sub
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", "MAXILOAN") Then
            Dim isError As Boolean
            Dim cmdwhere As String
            Dim criteria As String
            Me.CmdWhere = ""
            Me.SortBy = ""
            Me.BindMenu = ""
            cmdwhere = ""
            isError = False
            If cboCriteria.SelectedItem.Value.Trim = "1" Then
                If TxtDayExpired.Text.Trim = "" Then
                    isError = True
                ElseIf Not IsNumeric(TxtDayExpired.Text.Trim) Then
                    isError = True
                End If
            End If
            If isError Then
                ShowMessage(lblmessage, "Harap isi Jumlah Hari s/d Jatuh Tempo", True)
                getchildcombo()
                Dim strScript As String
                strScript = "<script language=""JavaScript"">" & vbCrLf
                strScript &= "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex  == -1) ? null : ListData[eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex]));"
                strScript &= "RestoreInsuranceCoIndex(" & Request("hdnBankAccount") & ");"
                strScript &= "</script>"
                divInnerHTML.InnerHtml = strScript

            End If

            If Not isError Then

                If hdnChildValue.Value.Trim = "" Or hdnChildValue.Value.Trim = "0" Then
                Else
                    cmdwhere = cmdwhere + "InsuranceAsset.InsuranceComBranchID = '" & hdnChildValue.Value.Trim & "' and "
                End If
                If cboSearch.SelectedItem.Value.Trim <> "0" Then
                    If cboSearch.SelectedItem.Value.Trim = "2" Then
                        If txtSearch.Text.Trim <> "" Then
                            If Right(txtSearch.Text.Trim, 1) = "%" Then
                                cmdwhere = cmdwhere + "customer.name like '" & txtSearch.Text.Trim & "' and "
                            Else
                                cmdwhere = cmdwhere + "customer.name = '" & txtSearch.Text.Trim & "' and "
                            End If
                        End If
                    ElseIf cboSearch.SelectedItem.Value.Trim = "1" Then
                        If txtSearch.Text.Trim <> "" Then
                            If Right(txtSearch.Text.Trim, 1) = "%" Then
                                cmdwhere = cmdwhere + "Agreement.agreementNo like '" & txtSearch.Text.Trim & "' and "
                            Else
                                cmdwhere = cmdwhere + "Agreement.agreementNo = '" & txtSearch.Text.Trim & "' and "
                            End If
                        End If
                    End If
                End If
                cmdwhere = cmdwhere + "InsuranceAsset.branchID = '" & cboParent.SelectedItem.Value.Trim & "'"
                Me.CmdWhere = cmdwhere
                Me.CmdWhere2 = cboCriteria.SelectedItem.Value.Trim
                BindGridInsuranceExpiredInq(Me.CmdWhere, Me.SortBy)
                pnlList.Visible = True
                PnlSearch.Visible = True
            End If
        End If
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lblTotRec.Text = recordCount.ToString
        PnlSearch.Visible = True
        pnlList.Visible = True

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()
        Me.BindMenu = ""
        PnlSearch.Visible = True
        BindGridInsuranceExpiredInq(Me.CmdWhere, Me.SortBy)
        PnlSearch.Visible = True
        pnlList.Visible = True
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    Me.BindMenu = ""
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridInsuranceExpiredInq(Me.CmdWhere, Me.SortBy)
                End If
            End If
        End If
        PnlSearch.Visible = True
        pnlList.Visible = True
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridInsuranceExpiredInq(Me.CmdWhere, Me.SortBy)
        pnlList.Visible = True
    End Sub
#End Region
#Region " BindGrid"

    Sub BindGridInsuranceExpiredInq(ByVal cmdWhere As String, ByVal sortBy As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oInsuranceInq As New Parameter.InsInqEntities

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = sortBy
            .BusinessDate = Me.BusinessDate
            If TxtDayExpired.Text.Trim <> "" Then
                If IsNumeric(TxtDayExpired.Text.Trim) Then
                    .DayToExpired = CInt(TxtDayExpired.Text.Trim) * -1
                End If
            Else
                .DayToExpired = 0
            End If
            .WhereCond2 = Me.CmdWhere2

        End With
        oInsuranceInq = oInsuranceController.InqInsExpired(oCustomClass)

        With oInsuranceInq
            lblTotRec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oInsuranceInq.ListData
        dtvEntity = dtsEntity.DefaultView

        dtvEntity.Sort = Me.SortBy
        dtgInsuranceExpiredInq.DataSource = dtvEntity
        Try
            dtgInsuranceExpiredInq.DataBind()
        Catch
            dtgInsuranceExpiredInq.CurrentPageIndex = 0
            dtgInsuranceExpiredInq.DataBind()
        End Try

        getchildcombo()
        Dim strScript As String
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex  == -1) ? null : ListData[eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex]));"
        strScript &= "RestoreInsuranceCoIndex(" & Request("hdnBankAccount") & ");"
        strScript &= "</script>"
        divInnerHTML.InnerHtml = strScript
        PagingFooter()
    End Sub
#End Region
    Private Sub dtgInsuranceExpiredInq_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgInsuranceExpiredInq.ItemDataBound
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim hyPolicyNo As HyperLink
        Dim m As Int32
        If e.Item.ItemIndex >= 0 Then
            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCUSTOMERNAME"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            hyTemp = CType(e.Item.FindControl("hyAGREEMENTNO"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
            '*** Policy No link
            hyPolicyNo = CType(e.Item.FindControl("hyPOLICYNO"), HyperLink)
            hyPolicyNo.NavigateUrl = "javascript:OpenWinViewPolicyNo('" & hyTemp.Text.Trim & "', " & Me.sesBranchId & ")"
        End If
    End Sub

    Private Sub dtgInsuranceExpiredInq_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dtgInsuranceExpiredInq.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridInsuranceExpiredInq(Me.CmdWhere, Me.SortBy)
    End Sub
#Region "linkTo"
    Function LinkToViewAgreementNo(ByVal strAgreementNo As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinViewAgreementNo('" & strStyle & "','" & strAgreementNo & "')"
    End Function
    Function LinkToViewCustomerName(ByVal strCustomerName As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinViewCustomerName('" & strStyle & "','" & strCustomerName & "')"
    End Function
    Function LinkToViewPolicyNo(ByVal strAgreementNo As String, ByVal strBranch As String) As String
        Return "javascript:OpenWinViewPolicyNo('" & strAgreementNo & "','" & strBranch & "')"
    End Function

#End Region

End Class