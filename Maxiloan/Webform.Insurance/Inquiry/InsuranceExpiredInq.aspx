﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsuranceExpiredInq.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsuranceExpiredInq" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InsuranceExpiredInq</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
<!--
var hdnDetail;
var hdndetailvalue;
var cboBankAccount = null;
function ParentChange(pCmbOfPayment,pBankAccount,pHdnDetail,pHdnDetailValue, itemArray)
{
		hdnDetail = eval('document.forms[0].' + pHdnDetail);
		HdnDetailValue = eval('document.forms[0].'+pHdnDetailValue);
		var i, j;
		for(i= eval('document.forms[0].' + pBankAccount).options.length;i>=0;i--)
		{   
			eval('document.forms[0].' + pBankAccount).options[i] = null
		
		}  ;
		if (itemArray==null) 
		{ 
			j = 0 ;
		}
		else
		{  
			j=1;
		};
		eval('document.forms[0].' + pBankAccount).options[0] = new Option('Select One','0');
		if (itemArray!=null)
		{
			for(i=0;i<itemArray.length;i++)
			{	
				eval('document.forms[0].' + pBankAccount).options[j++]=new Option(itemArray[i][0], itemArray[i][1]);
				
			};
			eval('document.forms[0].' + pBankAccount).selected=true;
		}
		};
		
		function cboChildonChange(selIndex,l,j)
			{
				hdnDetail.value = l; 
				HdnDetailValue.value = j;
				eval('document.forms[0].hdnBankAccount').value = selIndex;
			}
		function RestoreInsuranceCoIndex(BankAccountIndex)
		{								
			cboChild  = eval('document.forms[0].cboChild');
			if (cboChild != null)
				if(eval('document.forms[0].hdnBankAccount').value != null)
				{
					if(BankAccountIndex != null)
					cboChild.selectedIndex = BankAccountIndex;
				}
		}
}-->
    </script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        function OpenWinViewPolicyNo(pAgreementNo, pBranch) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.Insurance/ViewPolicyDetail.aspx?AgreementNo=' + pAgreementNo + '&BranchID=' + pBranch, 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnChildName" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnBankAccount" runat="server" type="hidden" name="hdSP" />
      <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    INQUIRY - ASURANSI JATUH TEMPO
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgInsuranceExpiredInq" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="AGREEMENTNO" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAGREEMENTNO" runat="server" Text='<%#Container.DataItem("AGREEMENTNO")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CUSTOMERNAME" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCUSTOMERNAME" runat="server" Text='<%#Container.DataItem("CUSTOMERNAME")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="INSURANCECO" HeaderText="PERUSAHAAN ASURANSI">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblInsuranceCo" runat="server" Text='<%#Container.DataItem("INSURANCECO")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right" Width="20%"></FooterStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="POLICYNO" HeaderText="NO POLIS">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyPOLICYNO" runat="server" Text='<%#Container.DataItem("POLICYNO")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ENDDATE" SortExpression="ENDDATE" HeaderText="TGL JATUH TEMPO"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SPPADATE" SortExpression="SPPADATE" HeaderText="TGL SPPA"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="DAYTOEXPIRED" HeaderText="AKAN JT (HARI)">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDayToExpired" runat="server" Text='<%#Container.DataItem("DAYTOEXPIRED")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerId" HeaderText="CustomerId" Visible="False">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblCustomerId" runat="server" Text='<%#Container.DataItem("CUSTOMERID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="APPLICATION ID">
                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                        <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rvGo" runat="server" ControlToValidate="txtpage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general" ></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtpage"
                            ErrorMessage="No Halaman Salah"  CssClass="validator_general" 
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
    </asp:Panel>
    <asp:Panel ID="PnlSearch" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI ASURANSI JATUH TEMPO
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class ="label_req">
                    Cabang
                </label>
                <asp:DropDownList ID="cboParent" runat="server" onchange="<%#BranchIDChange()%>">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic"
                    InitialValue="0" ErrorMessage="Harap pilih Cabang" ControlToValidate="cboParent" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label>
                    Perusahaan Asuransi
                </label>
                <asp:DropDownList ID="cboChild" runat="server" onchange="cboChildonChange(this.selectedIndex,this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Kriteria
                </label>
                <asp:DropDownList ID="cboCriteria" runat="server">
                    <asp:ListItem Value="1">Polis JT Dalam Hari</asp:ListItem>
                    <asp:ListItem Value="2">Asuransi JT Perpanjang</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form_right">
                 <label class ="label_req">
                    Hari sampai jatuh Tempo
                </label>
                <asp:TextBox ID="TxtDayExpired" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtDayExpired" runat="server" Display="Dynamic"
                    InitialValue="0" ErrorMessage="isi Hari s/d Jatuh Tempo" ControlToValidate="TxtDayExpired" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="0">Select One</asp:ListItem>
                    <asp:ListItem Value="1">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="2">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <div id="divInnerHTML" runat="server">
    </div>
    </form>
</body>
</html>
