﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsuranceExistingCoverinq.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsuranceExistingCoverinq" %>

<%@ Register TagPrefix="uc1" TagName="UcCoverageType" Src="../../webform.UserController/UcCoverageType.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucComboBranchAndInsurance" Src="../../webform.UserController/ucComboBranchAndInsurance.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchByWithNoTable" Src="../../webform.UserController/UcSearchByWithNoTable.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InsuranceExistingCoverinq</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenWinViewPolicyNo(pAgreementNo, pBranch) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.Insurance/ViewPolicyDetail.aspx?AgreementNo=' + pAgreementNo + '&BranchID=' + pBranch, 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="PnlGrid" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    INQUIRY - EXISTING COVERING
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="customername" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("customername")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CABANG ASURANSI" SortExpression="InsuranceComBranchname">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceComBranchname") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PolicyNumber" HeaderText="NO POLIS">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyPOLICYNO" runat="server" Text='<%#Container.DataItem("PolicyNumber")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="CoverPeriod" SortExpression="CoverPeriod" HeaderText="PERIODE COVER">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="golivedate" SortExpression="golivedate" HeaderText="TGL LOAN ACT"
                                DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="PERIODE ASURANSI" SortExpression="StartDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblstartdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StartDate")  %>'>
                                    </asp:Label>&nbsp;S/D&nbsp;
                                    <asp:Label ID="lblenddate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EndDate")  %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="premiumamountbycust" SortExpression="premiumamountbycust"
                                HeaderText="PREMI KE CUSTOMER" DataFormatString="{0:N2}">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemTemplate>
                                    <a href='ExisitingCoverInqDetail.aspx?AgreementNo=<%# DataBinder.Eval(Container, "DataItem.agreementNo") %>&PolicyNumber=<%# DataBinder.Eval(Container, "DataItem.PolicyNumber") %>&CustomerName=<%# DataBinder.Eval(Container, "DataItem.customername") %>&InsuranceCo=<%# DataBinder.Eval(Container, "DataItem.InsuranceComBranchname") %>'>
                                        DETAIL</asp:image> </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="" HeaderText="CUSTOMER ID">
                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("CustomerId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="APPLICATION ID">
                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                    </asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server"  CssClass="validator_general"  Type="Integer" MaximumValue="999999999" ErrorMessage="No Halaman Salah"
                        MinimumValue="1" ControlToValidate="txtpage"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server"  CssClass="validator_general"  ErrorMessage="No Halaman Salah" ControlToValidate="txtpage"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="form_title">
        <div class="form_single">
            <h4>
                CARI EXISTING COVERING
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Cabang</label>
            <uc1:uccombobranchandinsurance id="oBranchInsurance" runat="server"></uc1:uccombobranchandinsurance>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Cari Berdasarkan</label>
            <uc1:ucsearchbywithnotable id="oSearchBy" runat="server">
                </uc1:ucsearchbywithnotable>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Kriteria</label>
            <asp:DropDownList ID="DDLCriteria" runat="server">
                <asp:ListItem Value="ALL">Semua</asp:ListItem>
                <asp:ListItem Value="1">Premi Belum Dibayar Cust</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Jenis Cover</label>
            <uc1:uccoveragetype id="oCoverageType" runat="server">
                </uc1:uccoveragetype>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
        </asp:Button>
    </div>
    </form>
</body>
</html>
