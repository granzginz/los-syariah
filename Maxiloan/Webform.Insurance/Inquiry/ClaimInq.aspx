﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ClaimInq.aspx.vb" Inherits="Maxiloan.Webform.Insurance.ClaimInq" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ClaimInq</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinInqueryDet(pInsClaimSeqNo, pAsset, pIns, pApplicationID, pBranchID, pClaimstatus) {
            var x = screen.width; var y = screen.height - 100; window.open('ClaimInqDet.aspx?ClaimSeqno=' + pInsClaimSeqNo + '&AssetSeqNo=' + pAsset + '&InsSequenceNo=' + pIns + '&ApplicationID=' + pApplicationID + '&Branchid=' + pBranchID + '&ClaimStatus=' + pClaimstatus, 'AgreementNo', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinPolicyDetail(BranchID, AgreementNo, strasset, strins, strApplication) {
            var x = screen.width; var y = screen.height - 100; window.open('../ViewPolicyDetail.aspx?BranchID=' + BranchID + '&AgreementNo=' + AgreementNo + '&AssetSeqNo=' + strasset + '&InsSeqNo=' + strins + '&ApplicationID=' + strApplication + '&Back=Refund' + '&style=Insurance', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');

        }


        function ddlchange() {
            if (document.Form1.ddlcriteria.options[document.Form1.ddlcriteria.selectedIndex].value != 'All') {
                document.Form1.ucValidate2_txtDate.readOnly = true;
                document.Form1.ucValidate1_txtDate.readOnly = true;
                document.Form1.ucValidate2_txtDate.value = '';
                document.Form1.ucValidate1_txtDate.value = '';
            }
            else {
                document.Form1.ucValidate2_txtDate.readOnly = false;
                document.Form1.ucValidate1_txtDate.readOnly = false;
            }
        }
        function fbtnClick() {
            if (document.Form1.ddlcriteria) {
                if (document.Form1.ddlcriteria.options[document.Form1.ddlcriteria.selectedIndex].value != 'All') {
                    document.Form1.ucValidate2_txtDate.readOnly = true;
                    document.Form1.ucValidate1_txtDate.readOnly = true;
                    document.Form1.ucValidate2_txtDate.value = '';
                    document.Form1.ucValidate1_txtDate.value = '';

                }

            }
        }				
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
      <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlsearch" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI KLAIM ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box">      

            <div class="form_single">
                <label class ="label_req">
                    Cabang</label>
                <asp:DropDownList ID="ddlBranch" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                    InitialValue="0" ErrorMessage="Please Select Branch" ControlToValidate="ddlBranch" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Perusahaan Asuransi</label>
                <asp:DropDownList ID="ddlInsco" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Konsumen</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kriteria</label>
                <asp:DropDownList ID="ddlcriteria" runat="server" onchange="ddlchange();">
                    <asp:ListItem Value="All">All Claim</asp:ListItem>
                    <asp:ListItem Value="Insuranceclaim.ClaimStatus='N'">Klaim Baru</asp:ListItem>
                    <asp:ListItem Value="Insuranceclaim.ClaimStatus='C'">Klaim Ditolak</asp:ListItem>
                    <asp:ListItem Value="Insuranceclaim.ClaimStatus='P'">Klaim Selesai</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <asp:Panel ID="pnlClaimdate" runat="server">
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Tanggal Klaim</label>
                    <asp:TextBox runat="server" ID="txtValidate1"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtValidate1"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    &nbsp;S/D&nbsp;
                    <asp:TextBox runat="server" ID="txtValidate2"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtValidate2"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </div>
            </div>
        </asp:Panel>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    INQUIRY - KLAIM ASURANSI
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgClaimInquiryList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="AgreementNo" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn Visible="False" DataField="BranchID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="ApplicationID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="AgreementNo"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="InsClaimSeqNo"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="ClaimStatus"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="Name"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CustomerID"></asp:BoundColumn>

                         <asp:TemplateColumn HeaderText="DETAIL">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <a href="javascript:OpenWinInqueryDet('<%# Container.DataItem("InsClaimSeqNo")%>','<%# Container.DataItem("AssetSeqNo")%>','<%# Container.DataItem("InsSequenceNo")%>','<%# Container.DataItem("ApplicationID")%>','<%# Container.DataItem("BranchID")%>','<%# Container.DataItem("ClaimStatus")%>')">
                                        <img src="../../Images/Icondetail.gif" border="0"></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="AKTIVITAS">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbListAct" ImageUrl="../../Images/IconEdit.gif" CommandName="ListAct"
                                        runat="server"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                           
                           
                            <asp:TemplateColumn HeaderText="ASSETSEQNO" Visible="false">
                                <HeaderStyle HorizontalAlign="Left" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="19%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetSeqno" runat="server" Text='<%# Container.DataItem("AssetSeqNo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="InsSequenceNo" Visible="false">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="19%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblInsSeqNo" runat="server" Text='<%# Container.DataItem("InsSequenceNo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="14%"></ItemStyle>
                                <ItemTemplate>
                                    <a href="javascript:OpenAgreementNo('Insurance','<%# Container.DataItem("ApplicationID")%>')">
                                        <%# Container.DataItem("AgreementNo")%>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="19%"></ItemStyle>
                                <ItemTemplate>
                                    <a href="javascript:OpenCustomer('Insurance','<%# Container.DataItem("CustomerID")%>')">
                                        <%# Container.DataItem("Name")%>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="INSCO" HeaderText="PERUSAHAAN ASURANSI">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CLAIMDATE" HeaderText="TGL KLAIM">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CLAIMTYPE" HeaderText="JENIS KLAIM"></asp:BoundColumn>
                            <asp:BoundColumn DataField="EVENTDATE" HeaderText="TGL KEJADIAN">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="9%"></ItemStyle>
                            </asp:BoundColumn>
                           
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                        CommandName="First" CausesValidation="False" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                        CommandName="Prev" CausesValidation="False" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                        CommandName="Next" CausesValidation="False" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CommandName="Last" CausesValidation="False" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" Type="Integer" MaximumValue="999999999"
                        ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtpage"
                         CssClass="validator_general" ></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                        ControlToValidate="txtpage"  CssClass="validator_general" 
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblRecord" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    
    <asp:Panel ID="pnlDetAct" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    HISTORY KLAIM ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Kontrak</label>
                <a href="javascript:OpenAgreementNo('Insurance','<%=me.ApplicationID%>')">
                    <%=me.AgreementNo%></a>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgClaimActList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="ActSeqNo" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="NO POLIS">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                  <a href=""javascript:OpenWinPolicyDetail('<%# Container.DataItem("BranchID")%>','<%# Container.DataItem("AgreementNo")%>','<%# Container.DataItem("AssetSeqno")%>','<%# Container.DataItem("InssequenceNo")%>','<%# Container.DataItem("ApplicationId")%>')"">
												<%# Container.DataItem("PolicyNumber")%></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="INSCLAIMSEQNO" HeaderText="NO URUT KLAIM"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ACTIVITYDATE" HeaderText="TGL AKTIVITAS"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ACTIVITY" HeaderText="AKTIVITAS"></asp:BoundColumn>
                            <asp:BoundColumn DataField="RESULT" HeaderText="HASIL"></asp:BoundColumn>
                            <asp:BoundColumn DataField="EMPLOYEEID" HeaderText="KARYAWAN"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="DETAIL">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbHistActDet" CommandName="HistActDet" ImageUrl="../../Images/Icondetail.gif"
                                        runat="server"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlHistActDet" runat="server">
        <div class="form_single">
            <h4>
                DETAIL KLAIM ASURANSI
            </h4>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak</label>
                <a href="javascript:OpenAgreementNo('Insurance','<%=me.ApplicationID%>')">
                    <%=me.AgreementNo%></a>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <a href="javascript:OpenCustomer('Insurance','<%=me.CustomerID%>')">
                    <%=me.CustomerName%></a>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Perusahaan Asuransi</label>
                <asp:Literal ID="ltlInsCo" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Polis
                </label>
                <a href="javascript:OpenWinPolicyDetail('<%=me.BranchID%>','<%=me.AgreementNo%>','<%=me.AssetSeqNo%>','<%=me.InsSeqNo%>','<%=me.ApplicationID%>')">
                    <%=me.PolicyNo%></a>
            </div>
            <div class="form_right">
                <label>
                    No Klaim
                </label>
                <asp:Literal ID="ltlClaimeNo" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Klaim
                </label>
                <asp:Literal ID="ltlClaimType" runat="server"></asp:Literal>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Klaim
                </label>
                <asp:Literal ID="ltlClaimDate" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Aktivitas</label>
                <asp:Literal ID="ltlActivityDate" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Hasil</label>
                <asp:Literal ID="ltlActivityResult" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan</label>
                <asp:Literal ID="ltlActivityNotes" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama karyawan</label>
                <asp:Literal ID="ltlEmployeName" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnBack1" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
