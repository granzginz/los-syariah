﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ClaimInqDet
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Dim m_ClaimAct As New InsClaimActController
    Private oClaimAct As New Parameter.InsClaimAct
#End Region
#Region "properties"
    Public Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Public Property AssetSeqNo() As String
        Get
            Return CType(viewstate("AssetSeqNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetSeqNo") = Value
        End Set
    End Property
    Public Property InsSeqNo() As String
        Get
            Return CType(viewstate("InsSeqNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsSeqNo") = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return CType(viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property
    Public Property customerID() As String
        Get
            Return CType(viewstate("customerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("customerID") = Value
        End Set
    End Property
    Public Property PolicyNo() As String
        Get
            Return CType(viewstate("PolicyNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("PolicyNo") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.FormID = "InsInqClaim"

        If checkFeature(Me.Loginid, Me.FormID, "View", Me.AppId) Then
            If sessioninvalid() Then
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            Else
                ' btnClose.Attributes.Add("onclick", "windowClose()")
                Bindgrid()
            End If
        End If
    End Sub
    Private Sub Bindgrid()
        Dim dsClaimAct As New DataSet

        With oClaimAct
            .strConnection = GetConnectionString()
            .ClaimSeqNo = CInt(Request.QueryString("ClaimSeqno"))
            .ApplicationID = Request.QueryString("ApplicationID")
            .BranchId = Request.QueryString("BranchId")
            .ClaimStatus = Request.QueryString("ClaimStatus")
            .AssetSequenceNo = CInt(Request.QueryString("AssetSeqNo"))
            .InsSequenceNo = CInt(Request.QueryString("InsSequenceNo"))
        End With

        dsClaimAct = m_ClaimAct.GetClaimInqDet(oClaimAct)

        If Not dsClaimAct Is Nothing Then
            If dsClaimAct.Tables(0).Rows.Count > 0 Then
                With dsClaimAct.Tables(0).Rows(0)
                    ltlClaimNo.Text = CStr(IIf(IsDBNull(.Item("InsClaimSeqNo")), "", .Item("InsClaimSeqNo")))
                    ltlInsCo.Text = CStr(IIf(IsDBNull(.Item("InsCo")), "", .Item("InsCo")))
                    ltlAsset.Text = CStr(IIf(IsDBNull(.Item("Description")), "", .Item("Description")))
                    ltlCases.Text = CStr(IIf(IsDBNull(.Item("Cases")), "", .Item("Cases")))
                    ltlInsurancePeriod.Text = CStr(IIf(IsDBNull(.Item("Period")), "", .Item("Period")))
                    ltlClaimType.Text = CStr(IIf(IsDBNull(.Item("ClaimType")), "", .Item("ClaimType")))
                    ltlBengkelHead.Text = CStr(IIf(IsDBNull(.Item("BengkelPIC")), "", .Item("BengkelPIC")))
                    ltlClaimDate.Text = CStr(IIf(IsDBNull(.Item("ClaimDate")), "", .Item("ClaimDate")))
                    ltlLocationOfEvent.Text = CStr(IIf(IsDBNull(.Item("EventLocation")), "", .Item("EventLocation")))
                    ltlPartOfDamage.Text = CStr(IIf(IsDBNull(.Item("DefectPart")), "", .Item("DefectPart")))
                    ltlClaimStatus.Text = CStr(IIf(IsDBNull(.Item("ClaimStatus")), "", .Item("ClaimStatus")))
                    ltlEventDate.Text = CStr(IIf(IsDBNull(.Item("EventDate")), "", .Item("EventDate")))
                    ltlInsCoApproveDate.Text = CStr(IIf(IsDBNull(.Item("OKInscoDate")), "", .Item("OKInscoDate")))
                    ltlBengkelName.Text = CStr(IIf(IsDBNull(.Item("BengkelName")), "", .Item("BengkelName")))
                    ltlBengkelAdrress.Text = CStr(IIf(IsDBNull(.Item("BengkelAddress")), "", .Item("BengkelAddress")))
                    ltlBengkelPhone.Text = CStr(IIf(IsDBNull(.Item("BengkelPhone")), "", .Item("BengkelPhone")))
                    ltlCustApproveDate.Text = CStr(IIf(IsDBNull(.Item("OKCustDate")), "", .Item("OKCustDate")))
                    ltlReportDate.Text = CStr(IIf(IsDBNull(.Item("ReportDate")), "", .Item("ReportDate")))
                    ltlAmount.Text = CStr(IIf(IsDBNull(.Item("ClaimAmount")), "", FormatNumber(.Item("ClaimAmount"), 2)))
                    ltlClaimPaidAmount.Text = CStr(IIf(IsDBNull(.Item("PaidAmountClaim")), "", FormatNumber(.Item("PaidAmountClaim"), 2)))
                    ltlClaimAmountApprove.Text = CStr(IIf(IsDBNull(.Item("ClaimAmountApprove")), "", FormatNumber(.Item("ClaimAmountApprove"), 2)))
                    ltlReportBy.Text = CStr(IIf(IsDBNull(.Item("ReportedBy")), "", .Item("ReportedBy")))
                    ltlPaidDate.Text = CStr(IIf(IsDBNull(.Item("Paiddate")), "", .Item("Paiddate")))
                    ltlClaimCost.Text = CStr(IIf(IsDBNull(.Item("ClaimCost")), "", FormatNumber(.Item("ClaimCost"), 2)))
                    Me.BranchID = CStr(IIf(IsDBNull(.Item("BranchID")), "", .Item("BranchID")))
                    Me.AgreementNo = CStr(IIf(IsDBNull(.Item("AgreementNo")), "", .Item("AgreementNo")))
                    Me.CustomerName = CStr(IIf(IsDBNull(.Item("Name")), "", .Item("Name")))
                    Me.customerID = CStr(IIf(IsDBNull(.Item("CustomerID")), "", .Item("CustomerID")))
                    Me.ApplicationID = CStr(IIf(IsDBNull(.Item("ApplicationID")), "", .Item("ApplicationID")))
                    Me.PolicyNo = CStr(IIf(IsDBNull(.Item("PolicyNumber")), "", .Item("PolicyNumber")))
                    Me.AssetSeqNo = Request.QueryString("AssetSeqNo")
                    Me.InsSeqNo = Request.QueryString("InsSequenceNo")
                End With
            Else
                ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            End If
            If Not dsClaimAct.Tables(1) Is Nothing Then
                dtgClaimCostRequest.DataSource = dsClaimAct.Tables(1)
                dtgClaimCostRequest.DataBind()
            End If
            If Not dsClaimAct.Tables(2) Is Nothing Then
                dtgClaimDocument.DataSource = dsClaimAct.Tables(2)
                dtgClaimDocument.DataBind()
            End If
        Else
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            Exit Sub
        End If
    End Sub

End Class