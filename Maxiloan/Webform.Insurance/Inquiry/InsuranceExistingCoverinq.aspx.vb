﻿#Region "Imports"

Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class InsuranceExistingCoverinq
    Inherits Maxiloan.Webform.WebBased

#Region "Property"


#End Region
#Region "Constanta"
    Dim currentPage As Integer = CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Dim PageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    Dim currentPageNumber As Int32 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Dim totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Dim recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT
    Private Const SP_NAME_PAGING As String = "spInsuranceExistingCoveringInquiry"
    Protected WithEvents oSearchBy As UcSearchByWithNoTable
    Protected WithEvents oCoverageType As UcCoverageType
    Protected WithEvents oBranchInsurance As ucComboBranchAndInsurance
#End Region
#Region "Initial Default Panel"
    Public Sub InitialDefaultPanel()
        PnlGrid.Visible = False
    End Sub
#End Region
#Region "Page Load"


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            oSearchBy.ListData = "Agreement.AgreementNo, Agreement NO-Customer.Name , Customer Name-InsuranceAsset.PolicyNumber , PolicyNumber"
            oSearchBy.BindData()
            oCoverageType.FillRequired = False
        End If
    End Sub

#End Region
#Region "Search "


    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        If CheckFeature(Me.Loginid, CommonVariableHelper.FORM_NAME_INS_INQUIRY_EXISTING_COVER, CommonVariableHelper.FORM_FEATURE_SRC, CommonVariableHelper.APPLICATION_NAME) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        PnlGrid.Visible = True
        Me.SearchBy = ""
        If oBranchInsurance.BranchIDParent.Trim <> "" Then
            Me.SearchBy = Me.SearchBy + " AND Agreement.BranchID = '" & oBranchInsurance.BranchIDParent & "' "
        End If
        If oBranchInsurance.InsuranceComBranchID.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " And  InsuranceAsset.InsuranceComBranchID = '" & Replace(oBranchInsurance.InsuranceComBranchID.Trim, "'", "") & "' "
        End If
        If oSearchBy.Text <> "" Then
            Me.SearchBy = Me.SearchBy + " And " & oSearchBy.ValueID & " ='" & oSearchBy.Text.Trim & "' "
        End If
        If DDLCriteria.SelectedItem.Value = "1" Then
            Me.SearchBy = Me.SearchBy + " And FlagInsStatus <> 'N' "
            Me.SearchBy = Me.SearchBy & " And PaidAmountByCust < PremiumAmountByCust "
        End If

        If oCoverageType.CoverageTypeID <> "0" Then
            Me.SearchBy = Me.SearchBy & " And InsuranceAsset.MainCoverage like '%" & Replace(oCoverageType.CoverageTypeID, "'", "") & "%'"
        End If

        BindGridEntity(Me.SearchBy)

    End Sub
#End Region
#Region "BindGrid"
    Public Sub BindGridEntity(ByVal cmdWhere As String)
        Dim m_controller As New GeneralPagingController
        Dim dtEntity As New DataTable
        Dim oGeneralPaging As New Parameter.GeneralPaging
        If Me.SearchBy = "" Then Me.SearchBy = cmdWhere
        If Me.SortBy = "" Then Me.SortBy = " dbo.Agreement.AgreementNo "
        With oGeneralPaging
            .SpName = SP_NAME_PAGING
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = PageSize
            .SortBy = Me.SortBy
            .strConnection = GetConnectionString
        End With

        Try
            oGeneralPaging = m_controller.GetGeneralPaging(oGeneralPaging)
        Catch ex As MaxiloanExceptions

        End Try

        If Not oGeneralPaging Is Nothing Then
            dtEntity = oGeneralPaging.ListData
            recordCount = oGeneralPaging.TotalRecords
        Else
            recordCount = 0
        End If
        DtgPaging.DataSource = dtEntity.DefaultView
        DtgPaging.CurrentPageIndex = 0
        DtgPaging.DataBind()
        PagingFooter()
    End Sub
    Private Sub DtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPaging.ItemDataBound
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim hyPolicyNo As HyperLink
        Dim m As Int32
        If e.Item.ItemIndex >= 0 Then
            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
            '*** Policy No link
            hyPolicyNo = CType(e.Item.FindControl("hypolicyNo"), HyperLink)
            hyPolicyNo.NavigateUrl = "javascript:OpenWinViewPolicyNo('" & Server.UrlEncode(hyTemp.Text.Trim) & "', '" & Me.sesBranchId & "')"
        End If
    End Sub

#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(PageSize, Integer)), Double))
        If totalPages = 0 Then
            'ShowMessage(lblMessage, "Data tidak ditemukan!", true)
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            ' rgvGo.MaximumValue = CType(totalPages, String)

        End If

        lblTotRec.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        BindGridEntity(Me.SearchBy)
    End Sub
    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int16)
                BindGridEntity(Me.SearchBy)
            End If
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles DtgPaging.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy)
    End Sub
#End Region
#Region "Reset"
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        oSearchBy.Text = ""
        oSearchBy.DataBind()
        Me.SearchBy = ""
        BindGridEntity(Me.SearchBy)
    End Sub
#End Region

End Class