﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
'Imports Maxiloan.Parameter.CollZipCode
Imports Maxiloan.Parameter.InsInqEntities
#End Region

Public Class InsuranceInvoiceInq
    Inherits Maxiloan.Webform.WebBased

#Region "Property "
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(viewstate("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property
    Private Property BindMenu() As String
        Get
            Return CStr(viewstate("BindMenu"))
        End Get
        Set(ByVal Value As String)
            viewstate("BindMenu") = Value
        End Set
    End Property

#End Region
#Region "PrivateConstanta"
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private m_controller As New DataUserControlController
    Private oControllerChild As New GeneralPagingController
    Dim oEntitesChild As New Parameter.InsCoAllocationDetailList
    Dim oCustomClass As New Parameter.InsInqEntities
    Dim oInsuranceController As New InsuranceInq

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "InqInsInvoice"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                PnlSearch.Visible = True
                pnlList.Visible = False

                Dim dtbranch As New DataTable
                Dim dtInsCo As New DataTable

                Me.sesBranchId = Replace(Me.sesBranchId, "'", "")

                If Me.IsHoBranch Then
                    dtbranch = m_controller.GetBranchName(GetConnectionString, "All")
                Else
                    dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                End If

                With cboParent
                    .DataTextField = "Name"
                    .DataValueField = "ID"
                    .DataSource = dtbranch
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With

                getchildcombo()
                Me.CmdWhere = ""
            End If
        End If
    End Sub
    Protected Function getchildcombo() As String
        
        Dim dtInsCo As New DataTable
 
        dtInsCo = getchildbranchCombotDT()
        Response.Write(GenerateScript(dtInsCo))
    End Function

    Private Function getchildbranchCombotDT() As DataTable
        With oEntitesChild
            .strConnection = GetConnectionString()
            If Me.IsHoBranch Then
                .BranchId = "All"
            Else
                .BranchId = Me.sesBranchId
            End If
        End With

        Dim dtInsCo As New DataTable

        oEntitesChild = oControllerChild.GetInsuranceBranchCombo(oEntitesChild)
        dtInsCo = oEntitesChild.ListData
        Return dtInsCo
    End Function
    Protected Function BranchIDChange() As String
        Return "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" BranchID = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("BranchID")).Trim Then
                        strType = CStr(DataRow(i)("BranchID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("ChildId")), "null", DataRow(i)("ChildId"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("ChildId")), "null", DataRow(i)("ChildId"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"
        Return strScript
    End Function


    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("InsuranceInvoiceInq.aspx")
    End Sub
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", "MAXILOAN") Then
            Dim cmdwhere As String
            Me.CmdWhere = ""
            Me.BindMenu = ""
            Context.Trace.Write("Search Conditional is begin")
            Context.Trace.Write("hdnChildValue.Value.Trim = " & hdnChildValue.Value.Trim)
            Context.Trace.Write("cboSearch.SelectedItem.Value.Trim = " & cboSearch.SelectedItem.Value.Trim)
            Context.Trace.Write("txtSearch.Text.Trim = " & txtSearch.Text.Trim)
            Context.Trace.Write("cboCriteria.SelectedItem.Value.Trim = " & cboCriteria.SelectedItem.Value.Trim)
            'Context.Trace.Write("cboDate.SelectedItem.Value.Trim = " & cboDate.SelectedItem.Value.Trim)

            If hdnChildValue.Value.Trim = "" Or hdnChildValue.Value.Trim = "0" Then
                Context.Trace.Write("Syarat 1")
            Else
                Context.Trace.Write("Syarat 2")
                cmdwhere = cmdwhere + "InsuranceAsset.MaskAssBranchID = '" & hdnChildValue.Value.Trim & "' and "
            End If

            If cboSearch.SelectedItem.Value.Trim <> "0" Then
                Context.Trace.Write("Syarat 3")
                If cboSearch.SelectedItem.Value.Trim = "2" Then
                    Context.Trace.Write("Syarat 4")
                    If txtSearch.Text.Trim <> "" Then
                        Context.Trace.Write("Syarat 5")
                        cmdwhere = cmdwhere + "customer.name like '%" & txtSearch.Text.Trim & "%' and "
                    End If
                ElseIf cboSearch.SelectedItem.Value.Trim = "1" Then
                    Context.Trace.Write("Syarat 8")
                    If txtSearch.Text.Trim <> "" Then
                        Context.Trace.Write("Syarat 9")
                        cmdwhere = cmdwhere + "Agreement.agreementNo like '%" & txtSearch.Text.Trim & "%' and "
                    End If
                ElseIf cboSearch.SelectedItem.Value.Trim = "3" Then
                    Context.Trace.Write("Syarat 12")
                    If txtSearch.Text.Trim <> "" Then
                        Context.Trace.Write("Syarat 13")
                        cmdwhere = cmdwhere + "InsuranceAsset.PolicyNumber like '%" & txtSearch.Text.Trim & "%' and "
                    End If
                End If
            End If

            If cboCriteria.SelectedItem.Value.Trim = "1" Then
                Context.Trace.Write("Syarat 16")
                cmdwhere = cmdwhere + "InsuranceAsset.PolicyNumber <> '-' and InsuranceAsset.FlagDocStatus = 'O' and InsuranceAsset.InvoiceNo = '-' and "
            ElseIf cboCriteria.SelectedItem.Value.Trim = "2" Then
                Context.Trace.Write("Syarat 17")
                cmdwhere = cmdwhere + "InsuranceAsset.InvoiceNo <> '-' and AccountPayable.status <> 'P' and "
            End If

            'If cboDate.SelectedItem.Value.Trim <> "0" Then
            '    Context.Trace.Write("Syarat 18")
            '    If cboDate.SelectedItem.Value.Trim = "1" Then
            '        Context.Trace.Write("Syarat 19")
            '        If SDate.dateValue.Trim <> "" And EDate.dateValue.Trim <> "" Then
            '            Context.Trace.Write("Syarat 20")
            '            cmdwhere = cmdwhere + "agreement.GoLiveDate >= '" & ConvertDate2(SDate.dateValue.Trim).ToString("yyyyMMdd") & "' And "
            '            cmdwhere = cmdwhere + "agreement.GoLiveDate <= '" & ConvertDate2(EDate.dateValue.Trim).ToString("yyyyMMdd") & "'"
            '        End If
            '    ElseIf cboDate.SelectedItem.Value.Trim = "2" Then
            '        Context.Trace.Write("Syarat 21")
            '        If SDate.dateValue.Trim <> "" And EDate.dateValue.Trim <> "" Then
            '            Context.Trace.Write("Syarat 22")
            '            cmdwhere = cmdwhere + "InsuranceAsset.PolicyReceiveDate >= '" & ConvertDate2(SDate.dateValue.Trim).ToString("yyyyMMdd") & "' And "
            '            cmdwhere = cmdwhere + "InsuranceAsset.PolicyReceiveDate <= '" & ConvertDate2(EDate.dateValue.Trim).ToString("yyyyMMdd") & "'"
            '        End If
            '    ElseIf cboDate.SelectedItem.Value.Trim = "3" Then
            '        Context.Trace.Write("Syarat 23")
            '        If SDate.dateValue.Trim <> "" And EDate.dateValue.Trim <> "" Then
            '            Context.Trace.Write("Syarat 24")
            '            cmdwhere = cmdwhere + "InsuranceInvoiceH.InvoiceDate >= '" & ConvertDate2(SDate.dateValue.Trim).ToString("yyyyMMdd") & "' And "
            '            cmdwhere = cmdwhere + "InsuranceInvoiceH.InvoiceDate <= '" & ConvertDate2(EDate.dateValue.Trim).ToString("yyyyMMdd") & "'"
            '        End If
            '    End If
            'End If
            cmdwhere = cmdwhere + "InsuranceAsset.branchID = '" & cboParent.SelectedItem.Value.Trim & "'"
            Context.Trace.Write("Syarat 25")

            Me.CmdWhere = cmdwhere
            If Me.SortBy = "" Then Me.SortBy = " AgreementNo "

            Context.Trace.Write("Me.CmdWhere  = " & Me.CmdWhere)
            Context.Trace.Write("Me.SortBy  = " & Me.SortBy)
            BindGridInsuranceInvoiceInq(Me.CmdWhere, Me.SortBy)

            pnlList.Visible = True
            PnlSearch.Visible = True
        End If

    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lblTotRec.Text = recordCount.ToString
        PnlSearch.Visible = True
        pnlList.Visible = True

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()
        Me.BindMenu = ""
        PnlSearch.Visible = True
        BindGridInsuranceInvoiceInq(Me.CmdWhere, Me.SortBy)
        PnlSearch.Visible = True
        pnlList.Visible = True
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    Me.BindMenu = ""
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridInsuranceInvoiceInq(Me.CmdWhere, Me.SortBy)
                End If
            End If
        End If
        PnlSearch.Visible = True
        pnlList.Visible = True
    End Sub

    'Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dtgInsuranceInvoiceInq.SortCommand
        'Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles DtgPaging.SortCommand
        'Handles dtgInsuranceInvoiceInq.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Context.Trace.Write("Ketika DiSortir")
        Context.Trace.Write("Me.CmdWhere = " & Me.CmdWhere)
        Context.Trace.Write("Me.SortBy = " & Me.SortBy)
        If Me.SortBy = "" Then
            Me.SortBy = " AGREEMENTNO "
        End If
        BindGridInsuranceInvoiceInq(Me.CmdWhere, Me.SortBy)
        pnlList.Visible = True
    End Sub
#End Region
#Region " BindGrid"

    Sub BindGridInsuranceInvoiceInq(ByVal cmdWhere As String, ByVal sortBy As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oInsuranceInq As New Parameter.InsInqEntities

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = sortBy
        End With

        oInsuranceInq = oInsuranceController.InqInsInvoice(oCustomClass)

        With oInsuranceInq
            lblTotRec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oInsuranceInq.ListData
        dtvEntity = dtsEntity.DefaultView

        dtvEntity.Sort = Me.SortBy
        dtgInsuranceInvoiceInq.DataSource = dtvEntity
        Try
            dtgInsuranceInvoiceInq.DataBind()
        Catch
            dtgInsuranceInvoiceInq.CurrentPageIndex = 0
            dtgInsuranceInvoiceInq.DataBind()
        End Try
        getchildcombo()
        Dim strScript As String
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex  == -1) ? null : ListData[eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex]));"
        strScript &= "RestoreInsuranceCoIndex(" & Request("hdnBankAccount") & ");"
        strScript &= "</script>"
        divInnerHTML.InnerHtml = strScript
        PagingFooter()
    End Sub
#End Region
    Private Sub dtgInsuranceInvoiceInq_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgInsuranceInvoiceInq.ItemDataBound
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim hyPolicyNo As HyperLink
        Dim m As Int32
        If e.Item.ItemIndex >= 0 Then
            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCUSTOMERNAME"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            hyTemp = CType(e.Item.FindControl("hyAGREEMENTNO"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"

        End If
    End Sub

    Private Sub dtgInsuranceInvoiceInq_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dtgInsuranceInvoiceInq.SortCommand
        'If InStr(Me.SortBy, "DESC") > 0 Then
        '    Me.SortBy = e.SortExpression + " ASC"
        'Else
        '    Me.SortBy = e.SortExpression + " DESC"
        'End If
        'BindGridInsuranceInvoiceInq(Me.CmdWhere, Me.SortBy)
    End Sub
#Region "linkTo"
    Function LinkToViewAgreementNo(ByVal strAgreementNo As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinViewAgreementNo('" & strStyle & "','" & strAgreementNo & "')"
    End Function
    Function LinkToViewCustomerName(ByVal strCustomerName As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinViewCustomerName('" & strStyle & "','" & strCustomerName & "')"
    End Function
    Function LinkToViewPolicyNo(ByVal strAgreementNo As String, ByVal strBranch As String) As String
        Return "javascript:OpenWinViewPolicyNo('" & strAgreementNo & "','" & strBranch & "')"
    End Function

#End Region

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim DtChild As DataTable = getchildbranchCombotDT()
        Dim i As Integer

        For i = 0 To DtChild.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(cboChild.UniqueID, CStr(DtChild.Rows(i).Item("ChildId")).Trim)
        Next

        Page.ClientScript.RegisterForEventValidation(cboChild.UniqueID, "0")


        MyBase.Render(writer)
    End Sub

End Class