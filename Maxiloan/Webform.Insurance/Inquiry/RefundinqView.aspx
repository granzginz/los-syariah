﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RefundinqView.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.RefundinqView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RefundinqView</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
<!--
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function windowClose() {
            window.close();
        }
        function fback() {
            history.go(-1);
            return false;
        }

        function OpenWinHistory(pCGID, pExID, pExName) {
            var x = screen.width; var y = screen.height - 100; window.open('../ExecutorHistory.aspx?CGID=' + pCGID + '&ExecutorID=' + pExID + '&ExecutorName=' + pExName + '', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCollector(pCollID, pCGID) {
            var x = screen.width; var y = screen.height - 100; window.open('../../Setting/CollectorView.aspx?CGID=' + pCGID + '&CollectorID=' + pCollID + '', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID) {
            var x = screen.width; var y = screen.height - 100; window.open('../../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinPolicyDetail(BranchID, AgreementNo, strasset, strins, strApplication) {
            var x = screen.width; var y = screen.height - 100; window.open('../ViewPolicyDetail.aspx?BranchID=' + BranchID + '&AgreementNo=' + AgreementNo + '&AssetSeqNo=' + strasset + '&InsSeqNo=' + strins + '&ApplicationID=' + strApplication + '&Back=Refund' + '&style=Insurance', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function currencyFormatku(fld, e) {
            var key = '';
            var i = j = 0;
            var strCheck = '0123456789';
            var whichCode = (window.Event) ? e.which : e.keyCode;
            document.onkeypress = window.event;
            if (whichCode == 13) return true;  // Enter
            key = String.fromCharCode(whichCode);  // Get key value from key code
            if (strCheck.indexOf(key) == -1) return false;  // Not a valid key
        }	
	
//-->
    </script>
</head>
<body>
      <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                VIEW - REFUND ASURANSI
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nama Asset</label>
            <asp:Label ID="lblAsset" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Polis
            </label>
            <asp:HyperLink ID="lblPolicyNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Status Kontrak
            </label>
            <asp:Label ID="lblContractStatus" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tgl Request Refund
            </label>
            <asp:Label ID="lblRefundRequestDate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Status Refund
            </label>
            <asp:Label ID="lblRefundStatus" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Refund Dari Asuransi
            </label>
            <asp:Label ID="lblRefundFromInsCo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Refund Terima Asuransi
            </label>
            <asp:Label ID="lblRefundReceiveFromInsco" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tanggal Terima
            </label>
            <asp:Label ID="lblReceiveDate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Voucher
            </label>
            <asp:Label ID="lblVoucherNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Refund Ke Customer
            </label>
            <asp:Label ID="lblRefundToCustomer" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jumlah Prepaid
            </label>
            <asp:Label ID="lblPrepaidAmount" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Refund Ke Supplier
            </label>
            <asp:Label ID="lblRefundToSupplier" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Alasan Penutupan</label>
            <asp:Label ID="lblReason" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Catatan</label>
            <asp:Label ID="lblNotes" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnClose" runat="server" OnClientClick="windowClose();" CausesValidation="False"
            Text="Close" CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
