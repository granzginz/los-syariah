﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsuranceDue.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsuranceDue" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../webform.UserController/ValidDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchByWithNoTable" Src="../../webform.UserController/UcSearchByWithNoTable.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../webform.UserController/UcBranchAll.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InsuranceDue</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
     <asp:Panel ID="PnlTop" runat="server">
          <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI ASURANSI JATUH TEMPO
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <uc1:ucbranchall id="oBranch" runat="server"></uc1:ucbranchall>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <uc1:ucsearchbywithnotable id="oSearchBy" runat="server"></uc1:ucsearchbywithnotable>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Mulai</label>
                <asp:TextBox runat="server" ID="txtSDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtSDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                &nbsp;S/D&nbsp;
                <asp:TextBox runat="server" ID="txtEDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtEDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" CausesValidation="False" runat="server" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlGrid" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    INQUIRY - ASURANSI YANG JATUH TEMPO
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                        <asp:TemplateColumn HeaderText="DETAIL">
                                <ItemStyle HorizontalAlign="center"></ItemStyle>
                                <ItemTemplate>
                                    <a href='../ViewInsuranceDetail.aspx?ApplicationID=<%# DataBinder.eval(Container,"DataItem.ApplicationID") %>&BranchId=<%# DataBinder.eval(Container,"DataItem.BranchID") %> &pagesource=InsuranceDue'>
                                        <asp:Image ID="imgRate" ImageUrl="../../images/iconinsurance.gif" runat="server">
                                        </asp:Image>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblApplicationId" runat="server" Visible="false" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="customername" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("customername")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblCustomerId" runat="server" Visible="false" Text='<%#Container.DataItem("CustomerId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Insurancedue" HeaderText="JUMLAH JATUH TEMPO">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="right" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Literal ID="ltlInsuranceDue" runat="server" Text='<%#formatnumber(Container.DataItem("InsuranceDue"),2)%>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Prepaid" HeaderText="JUMLAH PREPAID">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="right" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Literal ID="ltlPrepaid" runat="server" Text='<%# formatnumber(Container.DataItem("PrepaidAmount"),2)%>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InsAsset.YearNum" HeaderText="TAHUN">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Literal ID="ltlYearNum" runat="server" Text='<%#Container.DataItem("YearNum")%>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InsAsset.StartDate" HeaderText="TGL MULAI">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Literal ID="ltlStartDate" runat="server" Text='<%# Container.DataItem("StartDate")%>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PaidByCust" HeaderText="KONS BAYAR">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="right" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Literal ID="ltlPaidBy" runat="server" Text='<%#Container.DataItem("PaidBy")%>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                    </asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server"  CssClass="validator_general"  Type="Integer" MaximumValue="999999999" ErrorMessage="No Halaman Salah"
                        MinimumValue="1" ControlToValidate="txtpage"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server"  CssClass="validator_general"  ErrorMessage="No Halaman Salah" ControlToValidate="txtpage"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblRecord" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        
    </asp:Panel>
   
    </form>
</body>
</html>
