﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ClaimInqDet.aspx.vb" Inherits="Maxiloan.Webform.Insurance.ClaimInqDet" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ClaimInqDet</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript">
    </script>
     <script language="JavaScript" type="text/javascript">
         function fClose() {
             window.close();
             return false;
         }				
    </script>
</head>
<body>
    <form id="form1" runat="server">
      <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                VIEW - KLAIM
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <a href="../../Webform.LoanOrg/Credit/ViewStatementOfAccount.aspx?Style=Insurance&amp;ApplicationId=<%=me.ApplicationID%>">
                <%=me.AgreementNo%></a>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <a href="../../webform/Credit/ViewPersonalCustomer.aspx?Style=Insurance&amp;CustomerId=<%=me.CustomerID%>">
                <%=me.CustomerName%></a>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Asset
            </label>
            <asp:Literal ID="ltlAsset" runat="server"></asp:Literal>
        </div>
        <div class="form_right">
            <label>
                Biaya Klaim
            </label>
            <asp:Literal ID="ltlClaimCost" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Perusahaan Asuransi
            </label>
            <asp:Literal ID="ltlInsCo" runat="server"></asp:Literal>
        </div>
        <div class="form_right">
            <label>
                Periode Asuransi
            </label>
            <asp:Literal ID="ltlInsurancePeriod" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Polis
            </label>
            <a href="../ViewPolicyDetail.aspx?AgreementNo=<%= me.AgreementNo%>&amp;BranchID=<%= me.BranchID%>&amp;ApplicationID=<%= me.ApplicationID%>&amp;AssetSeqNo=<%= me.assetseqno%>&amp;InsSeqNo=<%= me.insseqno%>&amp;Back=Claim">
                <%= me.PolicyNo%>
            </a>
        </div>
        <div class="form_right">
            <label>
                Jenis Klaim
            </label>
            <asp:Literal ID="ltlClaimType" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Klaim
            </label>
            <asp:Literal ID="ltlClaimNo" runat="server"></asp:Literal>
        </div>
        <div class="form_right">
            <label>
                Kepala Bengkel / PIC
            </label>
            <asp:Literal ID="ltlBengkelHead" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Kasus
            </label>
            <asp:Literal ID="ltlCases" runat="server"></asp:Literal>
        </div>
        <div class="form_right">
            <label>
                Tanggal Klaim
            </label>
            <asp:Literal ID="ltlClaimDate" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Lokasi Kejadian
            </label>
            <asp:Literal ID="ltlLocationOfEvent" runat="server"></asp:Literal>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Bagian yang Rusak / Hilang
            </label>
            <asp:Literal ID="ltlPartOfDamage" runat="server"></asp:Literal>
        </div>
        <div class="form_right">
            <label>
                Status Klaim
            </label>
            <asp:Literal ID="ltlClaimStatus" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tanggal Kejadian
            </label>
            <asp:Literal ID="ltlEventDate" runat="server"></asp:Literal>
        </div>
        <div class="form_right">
            <label>
                Tanggal Approve/Reject
            </label>
            <asp:Literal ID="ltlInsCoApproveDate" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Bengkel
            </label>
            <asp:Literal ID="ltlBengkelName" runat="server"></asp:Literal>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Alamat Bengkel
            </label>
            <asp:Literal ID="ltlBengkelAdrress" runat="server"></asp:Literal>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Telepon
            </label>
            <asp:Literal ID="ltlBengkelPhone" runat="server"></asp:Literal>
        </div>
        <div class="form_right">
            <label>
                Tanggal Approved Customer
            </label>
            <asp:Literal ID="ltlCustApproveDate" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tanggal Lapor
            </label>
            <asp:Literal ID="ltlReportDate" runat="server"></asp:Literal>
        </div>
        <div class="form_right">
            <label>
                Jumlah Klaim
            </label>
            <asp:Literal ID="ltlAmount" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jumlah Klaim DiBayar
            </label>
            <asp:Literal ID="ltlClaimPaidAmount" runat="server"></asp:Literal>
        </div>
        <div class="form_right">
            <label>
                Jumlah Klaim diApprove
            </label>
            <asp:Literal ID="ltlClaimAmountApprove" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Dilaporkan Oleh
            </label>
            <asp:Literal ID="ltlReportBy" runat="server"></asp:Literal>
        </div>
        <div class="form_right">
            <label>
                Tanggal Bayar
            </label>
            <asp:Literal ID="ltlPaidDate" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                BIAYA KLAIM
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgClaimCostRequest" runat="server" AutoGenerateColumns="False"
                    BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <HeaderStyle HorizontalAlign="left" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="19%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.No") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TGL MINTA">
                            <HeaderStyle HorizontalAlign="left" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="19%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RequestDate") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="BIAYA KLAIM">
                            <HeaderStyle HorizontalAlign="left" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="right" Width="19%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.ClaimCost"),2) %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CATATAN">
                            <HeaderStyle HorizontalAlign="left" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="19%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Notes") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="STATUS">
                            <HeaderStyle HorizontalAlign="left" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="19%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                        Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DOKUMEN KLAIM
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgClaimDocument" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                    BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="CABANG">
                            <HeaderStyle HorizontalAlign="Left" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="19%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BranchID") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO APLIKASI">
                            <HeaderStyle HorizontalAlign="Left" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="19%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationId") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA DOKUMEN">
                            <HeaderStyle HorizontalAlign="Left" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="19%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InsDocDescription") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DOKUMEN ASLI">
                            <HeaderStyle HorizontalAlign="Left" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="19%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.OriginalDoc") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DOKUMEN COPY">
                            <HeaderStyle HorizontalAlign="Left" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="19%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label10" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CopyDoc") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TGL TERIMA">
                            <HeaderStyle HorizontalAlign="Left" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="19%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label11" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ReceiveDocDate") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TERIMA DARI">
                            <HeaderStyle HorizontalAlign="Left" Height="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="19%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label12" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ReceivedFrom") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                        Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="ButtonClose" OnClientClick="fClose();" runat="server"  Text="Close" CssClass ="small button gray"
        CausesValidation="false"></asp:Button>
    </div>  
    </form>
</body>
</html>
