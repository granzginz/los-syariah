﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class RefundinqView
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property AgreementNo() As String
        Get
            Return (CType(viewstate("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return (CType(viewstate("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return (CType(viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Public Property InsSequenceNo() As Integer
        Get
            Return CType(viewstate("InsSequenceNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("InsSequenceNo") = Value
        End Set
    End Property
    Public Property AssetSequenceNo() As Integer
        Get
            Return CType(viewstate("AssetSequenceNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("AssetSequenceNo") = Value
        End Set
    End Property

#End Region
#Region "Constanta"
    Dim m_InsInqRefund As New InsInqRefundController
    Private oInsInqRefund As New Parameter.InsInqRefund

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.FormID = "InsInqRefund"
            If CheckFeature(Me.Loginid, Me.FormID, "View", "MAXILOAN") Then
                btnClose.Attributes.Add("onclick", "windowClose()")
            End If
        End If
        Me.ApplicationID = Request("ApplicationID")
        Me.BranchID = Request("BranchID")
        Me.AssetSequenceNo = CInt(Request("Ass"))
        Me.InsSequenceNo = CInt(Request("Ins"))
        Me.CustomerID = Request("CustomerID")
        Context.Trace.Write("ApplicationID = " + Me.ApplicationID)
        Context.Trace.Write("BranchId = " + Me.BranchID)
        Context.Trace.Write("AssetSeqNo = " + CStr(Me.AssetSequenceNo))
        Context.Trace.Write("InsSeqNo = " + CStr(Me.InsSequenceNo))

        Cache.Insert(CACHE_VIEW_INSURANCE_DETAIL_PARAMETER_APPLICATION_ID, Me.ApplicationID)
        Cache.Insert(CACHE_VIEW_INSURANCE_DETAIL_PARAMETER_BRANCH_ID, Me.BranchID)
        Cache.Insert("CacheViewInsuranceDetailAssSeqNo", Me.AssetSequenceNo)
        Cache.Insert("CacheViewInsuranceDetailInsSeqNo", Me.InsSequenceNo)

        Dim dt As New DataTable
        With oInsInqRefund
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .AssetSequenceNo = Me.AssetSequenceNo
            .InsSequenceNo = Me.InsSequenceNo
        End With

        oInsInqRefund = m_InsInqRefund.InsInqRefundDetail(oInsInqRefund)
        dt = oInsInqRefund.ListData

        If dt.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)
            Exit Sub
        End If
        With dt.Rows(0)
            lblAgreementNo.Text = CStr(IIf(IsDBNull(.Item("AgreementNo")), "", .Item("AgreementNo")))
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblCustomerName.Text = CStr(IIf(IsDBNull(.Item("Name")), "", .Item("Name")))
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
            lblAsset.Text = CStr(IIf(IsDBNull(.Item("Description")), "", .Item("Description")))
            Me.AgreementNo = CStr(IIf(IsDBNull(.Item("AgreementNo")), "", .Item("AgreementNo")))
            lblContractStatus.Text = CStr(IIf(IsDBNull(.Item("ContractStatus")), "", .Item("ContractStatus")))
            lblPolicyNo.Text = CStr(IIf(IsDBNull(.Item("PolicyNumber")), "", .Item("PolicyNumber")))
            lblPolicyNo.NavigateUrl = "javascript:OpenWinPolicyDetail('" & Server.UrlEncode(Me.BranchID.Trim) & "','" & Me.AgreementNo & "','" & Me.AssetSequenceNo & "','" & Me.InsSequenceNo & "','" & Me.ApplicationID & "')"
            lblRefundRequestDate.Text = CStr(IIf(IsDBNull(.Item("RefundRequestDate")), "", (.Item("RefundRequestDate"))))

            If Not lblRefundRequestDate.Text <> "" Then
                ' lblRefundRequestDate.Text = lblRefundRequestDate.Text.ToString("dd/MM/yyyy")
            End If

            lblRefundStatus.Text = CStr(IIf(IsDBNull(.Item("RefundStatus")), "", .Item("RefundStatus")))
            lblRefundFromInsCo.Text = CStr(IIf(IsDBNull(.Item("RefundFromInsco")), "", FormatNumber(.Item("RefundFromInsco"), 0)))
            lblRefundReceiveFromInsco.Text = CStr(IIf(IsDBNull(.Item("RefundReceiveFromInsCo")), "", FormatNumber(.Item("RefundReceiveFromInsCo"), 0)))
            lblReceiveDate.Text = CStr(IIf(IsDBNull(.Item("ReceiveDate")), "", (.Item("ReceiveDate"))))
            lblVoucherNo.Text = CStr(IIf(IsDBNull(.Item("VoucherNo")), "", .Item("VoucherNo")))
            lblRefundToCustomer.Text = CStr(IIf(IsDBNull(.Item("RefundToCustomer")), "", FormatNumber(.Item("RefundToCustomer"), 0)))
            lblPrepaidAmount.Text = CStr(IIf(IsDBNull(.Item("PrepaidAmount")), "", FormatNumber(.Item("PrepaidAmount"), 0)))
            lblRefundToSupplier.Text = CStr(IIf(IsDBNull(.Item("RefundToSupplier")), "", FormatNumber(.Item("RefundToSupplier"), 0)))
            lblReason.Text = CStr(IIf(IsDBNull(.Item("explanation")), "", .Item("explanation")))
            lblNotes.Text = CStr(IIf(IsDBNull(.Item("Notes")), "", .Item("Notes")))
        End With


    End Sub

End Class