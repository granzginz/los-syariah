﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Refundinq.aspx.vb" Inherits="Maxiloan.Webform.Insurance.Refundinq" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Refundinq</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
		<!--
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function OpenWinHistory(pCGID, pExID, pExName) {
            var x = screen.width; var y = screen.height - 100; window.open('../ExecutorHistory.aspx?CGID=' + pCGID + '&ExecutorID=' + pExID + '&ExecutorName=' + pExName + '', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCollector(pCollID, pCGID) {
            var x = screen.width; var y = screen.height - 100; window.open('../../Webform.Setup/CollectorView.aspx?CGID=' + pCGID + '&CollectorID=' + pCollID + '', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID) {
            var x = screen.width; var y = screen.height - 100; window.open('../../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinRefundinqView(ApplicationID, BranchID, Ass, Ins, CustomerID) {
            var x = screen.width; var y = screen.height - 100; window.open('RefundinqView.aspx?ApplicationID=' + ApplicationID + '&BranchID=' + BranchID + '&Ass=' + Ass + '&CustomerID=' + CustomerID + '&Ins=' + Ins + '&style=Insurance', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinInsCoDetail(InsCoID, InsCoBranchID) {
            var x = screen.width; var y = screen.height - 100; window.open('../../Webform.Insurance/Setting/InsCoView.aspx?InsCoID=' + InsCoID + '&InsCoBranchID=' + InsCoBranchID + '&Style=Insurance', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function currencyFormatku(fld, e) {
            var key = '';
            var i = j = 0;
            var strCheck = '0123456789';
            var whichCode = (window.Event) ? e.which : e.keyCode;
            document.onkeypress = window.event;
            if (whichCode == 13) return true;  // Enter
            key = String.fromCharCode(whichCode);  // Get key value from key code
            if (strCheck.indexOf(key) == -1) return false;  // Not a valid key
        }
     //-->
    </script>
</head>
<body>
      <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlsearch" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI REFUND ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
               <label class ="label_req">
                    Cabang</label>
                <asp:DropDownList ID="ddlBranch" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                    ControlToValidate="ddlBranch" ErrorMessage="Harap Pilih Cabang" InitialValue="0" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Konsumen</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kriteria</label>
                <asp:DropDownList ID="cboCriteria" runat="server">
                    <asp:ListItem Value="all">Semua Refund</asp:ListItem>
                    <asp:ListItem Value="N">Outstanding Refund</asp:ListItem>
                    <asp:ListItem Value="P">Refund DiBayar</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    INQUIRY - REFUND ASURANSI
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgInsRefundList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="AgreementNo" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn Visible="False" DataField="BranchID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="ApplicationID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CustomerID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="AgreementNo"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CustomerName"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="DETAIL">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkDetail" runat="server" ImageUrl="../../Images/icondetail.gif"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="14%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="19%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InsuranceAsset.MaskAssBranchId" HeaderText="PERUSAHAAN ASURANSI">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="14%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkInsCo" runat="server" Text='<%#Container.DataItem("InsuranceCo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ClaimDate" HeaderText="TGL REQUEST">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblRequestDate" runat="server" Text='<%#format(Container.DataItem("RequestDate"),"dd/MM/yyyy")%>'>></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PaidAmountClaim" HeaderText="REFUND DARI ASURANSI">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblrefundfrominsco" runat="server" Text='<%#formatnumber(Container.DataItem("RefundFromInsco"),0)%>'>></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ClaimAmountByCust" HeaderText="REFUND KE CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblRefundToCust" runat="server" Text='<%#formatnumber(Container.DataItem("RefundToCust"),0)%>'>></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InsRefundToSupplier" HeaderText="REFUND KE SUPPLIER">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblaRefundToSupplier" runat="server" Text='<%#formatnumber(Container.DataItem("RefundToSupplier"),0)%>'>></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            
                            <asp:BoundColumn Visible="False" DataField="AssetSeqNo" HeaderText="AssetSeqNo">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="InsSequenceNo" HeaderText="InsSeqNo">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="MaskAssID" HeaderText="InsCoyId">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="MaskAssBranchID" HeaderText="InsCoyBranchID">
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CommandName="First" OnCommand="NavigationLink_Click"
                        CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CommandName="Prev" OnCommand="NavigationLink_Click"
                        CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CommandName="Next" OnCommand="NavigationLink_Click"
                        CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CommandName="Last" OnCommand="NavigationLink_Click"
                        CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtPage" runat="server" Width="34px" >1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo"  CssClass="validator_general" 
                        runat="server" ControlToValidate="txtpage" MinimumValue="1" ErrorMessage="No Halaman Salah"
                        MaximumValue="999999999" Type="Integer"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo"  CssClass="validator_general" 
                        runat="server" ControlToValidate="txtpage" ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator></tr>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblRecord" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    
    </form>
</body>
</html>
