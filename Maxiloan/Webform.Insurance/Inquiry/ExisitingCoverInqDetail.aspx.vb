﻿#Region "Imports"

Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class ExisitingCoverInqDetail
    Inherits Maxiloan.Webform.WebBased

#Region "PageLoad"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If CheckFeature(Me.Loginid, CommonVariableHelper.FORM_NAME_INS_INQUIRY_EXISTING_COVER, CommonVariableHelper.FORM_FEATURE_VIEW, CommonVariableHelper.APPLICATION_NAME) Then
                Dim ParamAgreementNo As String = Replace(Request.QueryString("AgreementNo"), "'", "")
                Dim ParamPolicyNumber As String = Replace(Request.QueryString("PolicyNumber"), "'", "")
                Dim ParamInsuranceCo As String = Replace(Request.QueryString("InsuranceCo"), "'", "")
                Dim ParamCustomerName As String = Replace(Request.QueryString("CustomerName"), "'", "")
                Me.SearchBy = " Where dbo.Agreement.AgreementNo = '" & ParamAgreementNo & "' And dbo.InsuranceAsset.PolicyNumber = '" & ParamPolicyNumber & "' "
                Me.SortBy = " dbo.Agreement.AgreementNo "
                LblAgreementNo.Text = ParamAgreementNo
                LblPolicyNumber.Text = ParamPolicyNumber
                LblCustomerName.Text = ParamCustomerName
                LblInsuranceCompany.Text = ParamInsuranceCo
                BindGridEntity(Me.SearchBy)
            End If
        End If
    End Sub
#End Region
#Region "BindGridEntity"
    Public Sub BindGridEntity(ByVal cmdWhere As String)
        Dim oGeneralPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController
        Dim dtEntity As New DataSet

        With oGeneralPaging
            .SpName = "spInsExistingCoveringInqDetail"
            .WhereCond = Me.SearchBy
            .SortBy = Me.SortBy
            .strConnection = GetConnectionString

        End With
        oGeneralPaging = m_controller.GetReportWithParameterWhereAndSort(oGeneralPaging)
        If Not oGeneralPaging Is Nothing Then
            dtEntity = oGeneralPaging.ListDataReport
        End If

        DtgPaging.DataSource = dtEntity
        DtgPaging.CurrentPageIndex = 0
        DtgPaging.DataBind()

    End Sub
#End Region
#Region "Back"

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("InsuranceExistingCoverInq.aspx")
    End Sub
#End Region

#Region "genImage"

    Public Function genImage(ByVal startdate As Date, ByVal enddate As Date) As Boolean
        Dim VarDate As DateTime = Me.BusinessDate
        Context.Trace.Write("VarDate = " + CType(VarDate, String))
        Context.Trace.Write("StartDate = " + CType(startdate, String))
        Context.Trace.Write("enddate = " + CType(enddate, String))

        If VarDate > startdate And VarDate < enddate Then
            Context.Trace.Write("True --> GenImage")
            Return True
        Else
            Context.Trace.Write("False --> GenImage")
            Return False
        End If

    End Function
#End Region

    'GenTextonLabel
    Public Function GenTextonLabel(ByVal Param As Boolean) As String
        If Param = True Then
            Return "Y"
        Else
            Return "N"
        End If

    End Function


    
End Class