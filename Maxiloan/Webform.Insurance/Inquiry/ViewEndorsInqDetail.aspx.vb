﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class ViewEndorsInqDetail
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property BDEndors() As Integer
        Get
            Return (CType(viewstate("BDEndors"), Integer))
        End Get
        Set(ByVal Value As Integer)
            viewstate("BDEndors") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return (CType(viewstate("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return (CType(viewstate("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return (CType(viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Public Property InsSequenceNo() As Integer
        Get
            Return CType(viewstate("InsSequenceNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("InsSequenceNo") = Value
        End Set
    End Property
    Public Property AssetSequenceNo() As Integer
        Get
            Return CType(viewstate("AssetSequenceNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("AssetSequenceNo") = Value
        End Set
    End Property

#End Region
#Region "Constanta"
    Dim m_InsInqEndors As New InsEndorsInqController
    Private oInsInqEndors As New Parameter.InsEndorsInq

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.FormID = "InsEndorsInq"
            If CheckFeature(Me.Loginid, Me.FormID, "View", "MAXILOAN") Then
                btnClose.Attributes.Add("onclick", "windowClose()")
            End If
        End If
        Me.ApplicationID = Request("ApplicationID")
        Me.BranchID = Request("BranchID")
        Me.AssetSequenceNo = CInt(Request("Ass"))
        Me.InsSequenceNo = CInt(Request("Ins"))
        Me.BDEndors = CInt(Request("BDEndors"))




        Dim dtList As New DataTable
        Dim dtDetail As New DataTable
        With oInsInqEndors
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .AssetSequenceNo = Me.AssetSequenceNo
            .InsSequenceNo = Me.InsSequenceNo
            .bdendors = Me.BDEndors
        End With

        oInsInqEndors = m_InsInqEndors.InsInqEndorsDetail(oInsInqEndors)
        dtList = oInsInqEndors.ListData
        dtDetail = oInsInqEndors.Detail

        If dtList.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True)

            Exit Sub
        End If
        With dtList.Rows(0)
            Me.CustomerID = CStr(IIf(IsDBNull(.Item("CustomerID")), "", .Item("CustomerID")))
            lblAgreementNo.Text = CStr(IIf(IsDBNull(.Item("AgreementNo")), "", .Item("AgreementNo")))
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lnkCustomer.Text = CStr(IIf(IsDBNull(.Item("Name")), "", .Item("Name")))
            lnkCustomer.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
            lblEndorsPolicyNo.Text = CStr(IIf(IsDBNull(.Item("PolicyNumber")), "", .Item("PolicyNumber")))
            lblEndorsDate.Text = CDate(CStr(IIf(IsDBNull(.Item("BDEndorsDate")), "", .Item("BDEndorsDate")))).ToString("dd/MM/yyyy")
            lblEndorsDocNo.Text = CStr(IIf(IsDBNull(.Item("BDEndorsDocNo")), "", .Item("BDEndorsDocNo")))
            lblSumInsured.Text = FormatNumber(IIf(IsDBNull(.Item("SumInsured")), "", .Item("SumInsured")), 2)
            lblAcc.Text = CStr(IIf(IsDBNull(.Item("AccNotes")), "", .Item("AccNotes")))
            lblIns.Text = CStr(IIf(IsDBNull(.Item("InsNotes")), "", .Item("InsNotes")))
        End With
        DGridEndorsmentDetail.DataSource = dtDetail.DefaultView
        DGridEndorsmentDetail.DataBind()

    End Sub

End Class