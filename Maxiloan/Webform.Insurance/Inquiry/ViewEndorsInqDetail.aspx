﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewEndorsInqDetail.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.ViewEndorsInqDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewEndorsInqDetail</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
<!--
        function windowClose() {
            window.close();
        }
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function OpenWinHistory(pCGID, pExID, pExName) {
            var x = screen.width; var y = screen.height - 100; window.open('../ExecutorHistory.aspx?CGID=' + pCGID + '&ExecutorID=' + pExID + '&ExecutorName=' + pExName + '', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCollector(pCollID, pCGID) {
            var x = screen.width; var y = screen.height - 100; window.open('../../Setting/CollectorView.aspx?CGID=' + pCGID + '&CollectorID=' + pCollID + '', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID) {
            var x = screen.width; var y = screen.height - 100; window.open('../../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinRefundinqView(ApplicationID, BranchID, Ass, Ins, CustomerID) {
            var x = screen.width; var y = screen.height - 100; window.open('RefundinqView.aspx?ApplicationID=' + ApplicationID + '&BranchID=' + BranchID + '&Ass=' + Ass + '&CustomerID=' + CustomerID + '&Ins=' + Ins + '&style=Insurance', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinInsCoDetail(InsCoID, InsCoBranchID) {
            var x = screen.width; var y = screen.height - 100; window.open('../Setting/InsCoView.aspx?InsCoID=' + InsCoID + '&InsCoBranchID=' + InsCoBranchID + '&Style=Insurance', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function currencyFormatku(fld, e) {
            var key = '';
            var i = j = 0;
            var strCheck = '0123456789';
            var whichCode = (window.Event) ? e.which : e.keyCode;
            document.onkeypress = window.event;
            if (whichCode == 13) return true;  // Enter
            key = String.fromCharCode(whichCode);  // Get key value from key code
            if (strCheck.indexOf(key) == -1) return false;  // Not a valid key
        }
//-->
    </script>
</head>
<body>
      <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                HISTORY ENDORSEMENT
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                No Kontrak</label>
            <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nama Customer</label>
            <asp:HyperLink ID="lnkCustomer" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Endors No Polis</label>
            <asp:Label ID="lblEndorsPolicyNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Endors No Dokumen</label>
            <asp:Label ID="lblEndorsDocNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tanggal Endors</label>
            <asp:Label ID="lblEndorsDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nilai Pertanggungan</label>
            <asp:Label ID="lblSumInsured" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Catatan Accessories</label>
            <asp:Label ID="lblAcc" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Catatan Asuransi</label>
            <asp:Label ID="lblIns" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DGridEndorsmentDetail" runat="server" AutoGenerateColumns="False"
                    BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:BoundColumn DataField="YearNum" HeaderText="TAHUN"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CoverageType" HeaderText="JENIS COVER"></asp:BoundColumn>
                        <asp:BoundColumn DataField="SRCC" HeaderText="SRCC"></asp:BoundColumn>
                        <asp:BoundColumn DataField="TPL" HeaderText="TPL"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Flood" HeaderText="FLOOD"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnClose" runat="server" OnClientClick="windowClose();" CausesValidation="False"
            Text="Close" CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
