﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsuranceInvoiceInq.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsuranceInvoiceInq" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InsuranceInvoiceInq</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';						
    </script>
    <script language="JavaScript" type="text/javascript">

var hdnDetail;
var hdndetailvalue;
var cboBankAccount = null;
function ParentChange(pCmbOfPayment,pBankAccount,pHdnDetail,pHdnDetailValue, itemArray)
{
		hdnDetail = eval('document.forms[0].' + pHdnDetail);
		HdnDetailValue = eval('document.forms[0].'+pHdnDetailValue);
		var i, j;
		for(i= eval('document.forms[0].' + pBankAccount).options.length;i>=0;i--)
		{   
			eval('document.forms[0].' + pBankAccount).options[i] = null
		
		}  ;
		if (itemArray==null) 
		{ 
			j = 0 ;
		}
		else
		{  
			j=1;
		};
		eval('document.forms[0].' + pBankAccount).options[0] = new Option('Select One','0');
		if (itemArray!=null)
		{
			for(i=0;i<itemArray.length;i++)
			{	
				eval('document.forms[0].' + pBankAccount).options[j++]=new Option(itemArray[i][0], itemArray[i][1]);
				
			};
			eval('document.forms[0].' + pBankAccount).selected=true;
		}
		};
		
		function cboChildonChange(selIndex,l,j)
			{
				hdnDetail.value = l; 
				HdnDetailValue.value = j;
				eval('document.forms[0].hdnBankAccount').value = selIndex;
			}
		function RestoreInsuranceCoIndex(BankAccountIndex)
		{								
			cboChild  = eval('document.forms[0].cboChild');
			if (cboChild != null)
				if(eval('document.forms[0].hdnBankAccount').value != null)
				{
					if(BankAccountIndex != null)
					cboChild.selectedIndex = BankAccountIndex;
				}
		}

    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnChildName" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnBankAccount" runat="server" type="hidden" name="hdSP" />
    <asp:Panel ID="PnlSearch" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI INOVICE ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                 <label class ="label_req">
                    Cabang
                </label>
                <asp:DropDownList ID="cboParent" runat="server" onchange="<%#BranchIDChange()%>">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic"
                    InitialValue="0" ErrorMessage="Harap Pilih Cabang" ControlToValidate="cboParent" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label>
                    Perusahaan Asuransi
                </label>
                <asp:DropDownList ID="cboChild" runat="server" onchange="cboChildonChange(this.selectedIndex,this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0"
                    ErrorMessage="Harap Pilih Perusahaan Asuransi" ControlToValidate="cboChild" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="0">Select One</asp:ListItem>
                    <asp:ListItem Value="1">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="2">Nama Konsumen</asp:ListItem>
                    <asp:ListItem Value="3">No Polis</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
            <div class="form_right">
                <label>
                    Kriteria
                </label>
                <asp:DropDownList ID="cboCriteria" runat="server">
                    <asp:ListItem Value="1">Invoice Belum diterima</asp:ListItem>
                    <asp:ListItem Value="2">Invoice Belum dibayar</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    INQUIRY - TAGIHAN ASURANSI
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgInsuranceInvoiceInq" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="AGREEMENTNO" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="11%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAGREEMENTNO" runat="server" Text='<%#Container.DataItem("AGREEMENTNO")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CUSTOMERNAME" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCUSTOMERNAME" runat="server" Text='<%#Container.DataItem("CUSTOMERNAME")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="INSURANCECO" SortExpression="INSURANCECO" HeaderText="PERUSAHAAN ASURANSI">
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="DARI TGL">
                                <ItemTemplate>
                                    <asp:Label ID="lblStartPeriod" runat="server" Text='<%#container.dataitem("InsuranceStartPeriod")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="S/D TGL">
                                <ItemTemplate>
                                    <asp:Label ID="lblEndPeriod" runat="server" Text='<%#container.dataitem("InsuranceEndPeriod")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="POLICYRECEIVEDATE" SortExpression="POLICYRECEIVEDATE"
                                HeaderText="TERIMA POLIS">
                                <HeaderStyle HorizontalAlign="CENTER"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="PremiumToCust" HeaderText="PREMI KONS">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPremiumToCust" runat="server" Text='<%#formatnumber(Container.DataItem("PREMIUMTOCUST"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="StdPremium" HeaderText="PREMI STD">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblStdPremium" runat="server" Text='<%#formatnumber(Container.DataItem("STDPREMIUM"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PremiumToInsco" HeaderText="PREMI MASK">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPremiumToInsco" runat="server" Text='<%#formatnumber(Container.DataItem("PREMIUMTOINSCO"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="INVOICEDATE" SortExpression="INVOICEDATE" HeaderText="TGL INVOICE">
                                <HeaderStyle HorizontalAlign="CENTER"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="INVOICENO" SortExpression="INVOICENO" HeaderText="NO INVOICE">
                                <HeaderStyle HorizontalAlign="CENTER"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="CustomerId" HeaderText="CustomerId" Visible="False">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblCustomerId" runat="server" Text='<%#Container.DataItem("CustomerId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="APPLICATION ID">
                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                    </asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rvGo" runat="server"  CssClass="validator_general"  Type="Integer" MaximumValue="999999999" ErrorMessage="No Halaman Salah"
                        MinimumValue="1" ControlToValidate="txtGopage"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server"  CssClass="validator_general"  ErrorMessage="No Halaman Salah" ControlToValidate="txtGopage"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    
    <div id="divInnerHTML" runat="server">
    </div>
    </form>
</body>
</html>
