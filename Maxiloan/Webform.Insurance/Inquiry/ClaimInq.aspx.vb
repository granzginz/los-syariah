﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ClaimInq
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Dim m_ClaimAct As New InsClaimActController
    Dim m_ClaimActPaging As New GeneralPagingController
    Private oClaimAct As New Parameter.InsClaimAct
    Private oClaimActPaging As New Parameter.GeneralPaging
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim ds As DataSet
#End Region
#Region "properties"
    Public Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Public Property AssetSeqNo() As Integer
        Get
            Return CType(viewstate("AssetSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("AssetSeqNo") = Value
        End Set
    End Property
    Public Property InsSeqNo() As Integer
        Get
            Return CType(viewstate("InsSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("InsSeqNo") = Value
        End Set
    End Property
    Public Property InsClaimSeqNo() As String
        Get
            Return CType(viewstate("InsClaimSeqNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsClaimSeqNo") = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Public Property ClaimType() As String
        Get
            Return CType(viewstate("ClaimType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ClaimType") = Value
        End Set
    End Property
    Public Property ClaimStatus() As String
        Get
            Return CType(viewstate("ClaimStatus"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ClaimStatus") = Value
        End Set
    End Property
    Public Property PolicyNo() As String
        Get
            Return CType(viewstate("PolicyNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("PolicyNo") = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return CType(viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property
    Public Property customerID() As String
        Get
            Return CType(viewstate("customerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("customerID") = Value
        End Set
    End Property

#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblRecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.FormID = "InsInqClaim"
        If Not Page.IsPostBack Then
            fillcbo()
            defaultpanel()

        End If

        If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            InitialDefaultPanel()
        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If
    End Sub
    Private Sub defaultpanel()
        pnlsearch.Visible = True
        pnlDtGrid.Visible = False
        pnlDetAct.Visible = False
        pnlHistActDet.Visible = False
    End Sub
    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Me.SearchBy = ""
        Me.SortBy = ""

        If ddlBranch.SelectedValue <> "" And ddlBranch.SelectedValue <> "ALL" Then
            Me.SearchBy = "BranchID='" & ddlBranch.SelectedValue & "'"
        End If
        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
            Else
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
            End If
        End If
        If ddlInsco.SelectedValue <> "" And ddlInsco.SelectedValue <> "ALL" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = "InsCo='" & ddlInsco.SelectedValue & "'"
            Else
                Me.SearchBy += " and InsCo='" & ddlInsco.SelectedValue & "'"
            End If
        End If
        If ddlcriteria.SelectedValue <> "" Then
            If ddlcriteria.SelectedValue.Trim <> "All" Then
                If Me.SearchBy = "" Then
                    Me.SearchBy = ddlcriteria.SelectedValue
                Else
                    Me.SearchBy += " and " & ddlcriteria.SelectedValue
                End If
            Else
                If txtValidate1.Text.Trim <> "" And txtValidate2.Text.Trim <> "" Then
                    Dim strdate1() As String = Split(txtValidate1.Text, "/")
                    Dim strdate2() As String = Split(txtValidate2.Text, "/")
                    If Me.SearchBy = "" Then
                        Me.SearchBy = " InsuranceClaim.Claimdate between '" & strdate1(1) & "/" & strdate1(0) & "/" & strdate1(2) & "' and '" & strdate2(1) & "/" & strdate2(0) & "/" & strdate2(2) & "'"
                    Else
                        Me.SearchBy += " and InsuranceClaim.Claimdate between '" & strdate1(1) & "/" & strdate1(0) & "/" & strdate1(2) & "' and '" & strdate2(1) & "/" & strdate2(0) & "/" & strdate2(2) & "'"
                    End If
                End If
            End If
        End If

        Bindgrid(Me.SearchBy, Me.SortBy)

    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True

    End Sub
    Private Sub fillcbo()
        Dim m_controller As New DataUserControlController

        With ddlBranch
            If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "ALL")
                .Items(0).Value = "ALL"
            Else
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                Dim strBranch() As String
                strBranch = Split(Me.sesBranchId, ",")
                If UBound(strBranch) > 0 Then
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                End If
            End If
            If .Items.Count > 1 Then
                .Items(1).Selected = True
            End If

        End With

        oClaimAct.strConnection = GetConnectionString

        With ddlInsco
            .DataSource = m_ClaimAct.GetInsCo(oClaimAct)
            .DataValueField = "InsuranceComBranchID"
            .DataTextField = "MaskAssBranchName"
            .DataBind()
            .Items.Insert(0, "ALL")
            .Items(0).Value = "ALL"
        End With
    End Sub

    Private Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable

        With oClaimActPaging
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spClaimInquiryList"
        End With

        oClaimActPaging = m_ClaimActPaging.GetGeneralPaging(oClaimActPaging)
        If Not oClaimActPaging Is Nothing Then
            With oClaimActPaging
                lblRecord.Text = CType(.TotalRecord, String)
                lblTotPage.Text = CType(.PageSize, String)
                lblPage.Text = CType(.CurrentPage, String)
                recordCount = .TotalRecords
            End With

            dtsEntity = oClaimActPaging.ListData
            dtsEntity.TableName = "Table"

            dtvEntity = dtsEntity.DefaultView

            dtgClaimInquiryList.DataSource = dtvEntity

            If Not dtvEntity Is Nothing Then
                dtgClaimInquiryList.DataBind()
            End If

            PagingFooter()
            pnlDtGrid.Visible = True
        Else
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            Exit Sub
        End If
    End Sub

    Private Sub dtgClaimInquiryList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgClaimInquiryList.ItemCommand
        Me.BranchID = e.Item.Cells(0).Text.Trim
        Me.ApplicationID = e.Item.Cells(1).Text.Trim
        Me.AgreementNo = e.Item.Cells(2).Text.Trim
        Me.InsClaimSeqNo = e.Item.Cells(3).Text.Trim
        Me.ClaimStatus = e.Item.Cells(4).Text.Trim
        Me.CustomerName = e.Item.Cells(5).Text.Trim
        Me.customerID = e.Item.Cells(6).Text.Trim
        Me.ClaimType = e.Item.Cells(15).Text.Trim
        Dim lblAssetSeqno As Label
        Dim lblInsSeqNo As Label
        lblAssetSeqno = CType(e.Item.FindControl("lblAssetSeqno"), Label)
        lblInsSeqNo = CType(e.Item.FindControl("lblInsSeqNo"), Label)
        Me.AssetSeqNo = CInt(lblAssetSeqno.Text.Trim)
        Me.InsSeqNo = CInt(lblInsSeqNo.Text.Trim)
        Select Case e.CommandName
            Case "ListAct"
                If checkFeature(Me.Loginid, Me.FormID, "Act", Me.AppId) Then
                    If sessioninvalid() Then
                        Dim strHTTPServer As String
                        Dim strHTTPApp As String
                        Dim strNameServer As String
                        strHTTPServer = Request.ServerVariables("PATH_INFO")
                        strNameServer = Request.ServerVariables("SERVER_NAME")
                        strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                        Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
                    Else
                        viewActList()
                    End If
                End If
                'Case "Edit"
                '    If checkFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                '        If sessioninvalid() Then
                '            Dim strHTTPServer As String
                '            Dim strHTTPApp As String
                '            Dim strNameServer As String
                '            strHTTPServer = Request.ServerVariables("PATH_INFO")
                '            strNameServer = Request.ServerVariables("SERVER_NAME")
                '            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                '            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
                '        Else
                '            If Me.ClaimStatus = "N" And Me.ClaimType <> "TMN" Then Response.Redirect("../Claim/ClaimRequest.aspx?flag=Edit&Applicationid=" & Me.ApplicationID & "&branchID=" & Me.BranchID.Trim & "&assetsequenceno=" & CStr(Me.AssetSeqNo) & "&inssequenceno=" & CStr(Me.InsSeqNo) & "&customerid=" & Me.customerID.Trim & "&insclaimseqno=" & Me.InsClaimSeqNo)
                '        End If
                '    End If
        End Select
    End Sub
    Private Sub viewActList()
        pnlsearch.Visible = False
        pnlDtGrid.Visible = False
        pnlHistActDet.Visible = False
        pnlDetAct.Visible = True
        With oClaimAct
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .InsClaimSeqNo = Me.InsClaimSeqNo
            .ClaimStatus = Me.ClaimStatus
            .InsSequenceNo = Me.InsSeqNo
            .AssetSequenceNo = Me.AssetSeqNo
        End With
        ds = m_ClaimAct.GetClaimActList(oClaimAct)
        If Not ds Is Nothing Then
            If Not ds.Tables(0) Is Nothing Then
                dtgClaimActList.DataSource = ds.Tables(0)
                dtgClaimActList.DataBind()
            Else
                ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
                Exit Sub
            End If
        Else
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            Exit Sub
        End If
    End Sub

    Private Sub dtgClaimActList_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Select Case e.CommandName
            Case "HistActDet"
                If checkFeature(Me.Loginid, Me.FormID, "Act", Me.AppId) Then
                    If sessioninvalid() Then
                        Dim strHTTPServer As String
                        Dim strHTTPApp As String
                        Dim strNameServer As String
                        strHTTPServer = Request.ServerVariables("PATH_INFO")
                        strNameServer = Request.ServerVariables("SERVER_NAME")
                        strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                        Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
                    Else
                        viewActListDet(e.Item.Cells(0).Text.Trim)
                    End If
                End If
        End Select
    End Sub
    Private Sub viewActListDet(ByVal ActSeqNo As String)
        pnlsearch.Visible = False
        pnlDtGrid.Visible = False
        pnlDetAct.Visible = False
        pnlHistActDet.Visible = True
        With oClaimAct
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .InsClaimSeqNo = Me.InsClaimSeqNo
            .ClaimStatus = Me.ClaimStatus
            .ActSeqNo = ActSeqNo
            .InsSequenceNo = Me.InsSeqNo
            .AssetSequenceNo = Me.AssetSeqNo
        End With
        ds = m_ClaimAct.GetClaimActHistList(oClaimAct)
        If Not ds Is Nothing Then
            If ds.Tables(0).Rows.Count > 0 Then
                With ds.Tables(0).Rows(0)
                    ltlActivityDate.Text = CStr(IIf(IsDBNull(.Item("ActivityDate")), "", .Item("ActivityDate")))
                    ltlActivityResult.Text = CStr(IIf(IsDBNull(.Item("Result")), "", .Item("Result")))
                    ltlActivityNotes.Text = CStr(IIf(IsDBNull(.Item("Notes")), "", .Item("Notes")))
                    ltlEmployeName.Text = CStr(IIf(IsDBNull(.Item("EmployeeId")), "", .Item("EmployeeId")))
                    ltlClaimType.Text = CStr(IIf(IsDBNull(.Item("ClaimType")), "", .Item("ClaimType")))
                    ltlClaimeNo.Text = CStr(IIf(IsDBNull(.Item("InsClaimSeqNo")), "", .Item("InsClaimSeqNo")))
                    ltlClaimDate.Text = CStr(IIf(IsDBNull(.Item("ClaimDate")), "", .Item("ClaimDate")))
                    ltlInsCo.Text = CStr(IIf(IsDBNull(.Item("InsCo")), "", .Item("InsCo")))
                    Me.PolicyNo = CStr(IIf(IsDBNull(.Item("PolicyNumber")), "", .Item("PolicyNumber")))
                    Me.AssetSeqNo = CInt(.Item("AssetSeqNo"))
                    Me.InsSeqNo = CInt(.Item("Inssequenceno"))
                    Me.ApplicationID = CStr(.Item("ApplicationId"))
                End With
            Else
                ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            End If
        Else
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            Exit Sub
        End If
    End Sub

    Private Sub btnback_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        pnlsearch.Visible = True
        pnlDtGrid.Visible = True
        pnlDetAct.Visible = False
        pnlHistActDet.Visible = False
    End Sub

    Private Sub btnback1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack1.Click
        pnlsearch.Visible = True
        pnlDtGrid.Visible = True
        pnlDetAct.Visible = False
        pnlHistActDet.Visible = False
    End Sub

    Private Sub dtgClaimInquiryList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgClaimInquiryList.ItemDataBound
        Dim lblAssetSeqno As Label
        Dim lblInsSeqNo As Label
        ' Dim HYEdit As HyperLink
        If e.Item.ItemIndex >= 0 Then
            Me.BranchID = e.Item.Cells(0).Text.Trim
            Me.ApplicationID = e.Item.Cells(1).Text.Trim
            Me.AgreementNo = e.Item.Cells(2).Text.Trim
            Me.InsClaimSeqNo = e.Item.Cells(3).Text.Trim
            Me.ClaimStatus = e.Item.Cells(4).Text.Trim
            Me.CustomerName = e.Item.Cells(5).Text.Trim
            Me.customerID = e.Item.Cells(6).Text.Trim
            Me.ClaimType = e.Item.Cells(15).Text.Trim
            lblAssetSeqno = CType(e.Item.FindControl("lblAssetSeqno"), Label)
            lblInsSeqNo = CType(e.Item.FindControl("lblInsSeqNo"), Label)
            Me.AssetSeqNo = CInt(lblAssetSeqno.Text.Trim)
            Me.InsSeqNo = CInt(lblInsSeqNo.Text.Trim)
            'HYEdit = CType(e.Item.FindControl("HYEdit"), HyperLink)
            'If Me.ClaimStatus = "N" And Me.ClaimType <> "TMN" Then HYEdit.NavigateUrl = "../Claim/ClaimRequest.aspx?flag=Edit&Applicationid=" & Me.ApplicationID & "&branchID=" & Me.BranchID.Trim & "&assetsequenceno=" & CStr(Me.AssetSeqNo) & "&inssequenceno=" & CStr(Me.InsSeqNo) & "&customerid=" & Me.customerID.Trim & "&insclaimseqno=" & Me.InsClaimSeqNo 'Response.Redirect("../Claim/ClaimRequest.aspx?flag=Edit&Applicationid=" & Me.ApplicationID & "&branchID=" & Me.BranchID.Trim & "&assetsequenceno=" & CStr(Me.AssetSeqNo) & "&inssequenceno=" & CStr(Me.InsSeqNo) & "&customerid=" & Me.customerID.Trim & "&insclaimseqno=" & Me.InsClaimSeqNo)
        End If
    End Sub

End Class