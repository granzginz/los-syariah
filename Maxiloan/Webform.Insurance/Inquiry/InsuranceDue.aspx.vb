﻿#Region "Imports"
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class InsuranceDue
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents oSearchBy As UcSearchByWithNoTable
#Region "Constanta"
    'Private m_controller As New PremiumToCustomerController
    Private oInsuranceDue As New Parameter.InsuranceDue
    Private m_controller As New insuranceduecontroller
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private Enum ActionType
        Add
        Edit
        Personal
        Company
    End Enum
#End Region
#Region "Properties"
    Private Property FilterBy() As String
        Get
            Return DirectCast(viewstate("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "InsInqInsuranceDue"
            oSearchBy.ListData = "AgreementNo,No Kontrak-Name,Nama Konsumen"
            oSearchBy.BindData()

            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
            PnlTop.Visible = True
            PnlGrid.Visible = False
        End If
    End Sub
#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        PnlTop.Visible = True
        PnlGrid.Visible = False



    End Sub
#End Region
#Region "Navigation "
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity()
    End Sub
#End Region
#Region "Paging Footer"

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblRecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
#End Region
#Region "Go Page"
    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                BindGridEntity()
            End If
        End If
    End Sub

#End Region
#Region "Sorting"

    Public Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity()
    End Sub

#End Region
#Region "BindGridEntity"
    Sub BindGridEntity()
        With oInsuranceDue
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
            .strConnection = GetConnectionString
        End With
        oInsuranceDue = m_controller.InsuranceDuepaging(oInsuranceDue)
        recordCount = oInsuranceDue.TotalRecord
        DtgPaging.DataSource = oInsuranceDue.ListData
        DtgPaging.CurrentPageIndex = 0
        DtgPaging.DataBind()

        PnlTop.Visible = True
        PnlGrid.Visible = True
        PagingFooter()
    End Sub

#End Region


    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim strSearch As New StringBuilder
        Dim strFilter As New StringBuilder

        Dim datefrom As String
        Dim dateto As String

        Me.SearchBy = ""
        Me.SortBy = ""

        If oBranch.BranchName <> "Select One" Then
            strSearch.Append(" and Agr.Branchid = '" & oBranch.BranchID & "'")
            strFilter.Append(" BranchName = '" & oBranch.BranchName & "'")
        End If

        If oSearchBy.Text.Trim <> "" Then
            strSearch.Append(" And ") 
            strSearch.Append(oSearchBy.ValueID)
            strSearch.Append(" Like '%")
            strSearch.Append(oSearchBy.Text)
            strSearch.Append("%'")

            strFilter.Append(", " & oSearchBy.ValueID)
            strFilter.Append(" Like '%")
            strFilter.Append(oSearchBy.Text)
            strFilter.Append("%'")

        End If
       

        If txtSDate.Text <> "" Then
            datefrom = CStr(ConvertDate2(txtSDate.Text.Trim))
            strSearch.Append(" and")
            strSearch.Append(" InsAsset.EndDate >= '" & datefrom & "'")

            strFilter.Append(" and")
            strFilter.Append(", EndDate >= '" & datefrom & "'")

        End If

        If txtEDate.Text <> "" Then
            dateto = CStr(ConvertDate2(txtEDate.Text.Trim))
            strSearch.Append(" and")
            strSearch.Append(" InsAsset.Enddate <= '" & dateto & "'")

            strFilter.Append(" and")
            strFilter.Append(", EndDate <= '" & dateto & "'")
        End If

        Me.SearchBy = strSearch.ToString
        Me.FilterBy = strFilter.ToString


        PnlGrid.Visible = True
        PnlTop.Visible = True
        BindGridEntity()
    End Sub


    Private Sub DtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblTemp As Label
            Dim hyTemp As HyperLink
            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        oSearchBy.Text = ""
        txtSDate.Text = ""
        txtEDate.Text = ""
        PnlTop.Visible = True
        PnlGrid.Visible = False
        oBranch.BranchID = ""
        oBranch.BranchName = ""

    End Sub

    
End Class