﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class EndorsmentInq
    Inherits Maxiloan.Webform.WebBased


#Region "Constanta"
    Dim m_InsInqEndors As New InsEndorsInqController
    Private oInsInqEndors As New Parameter.InsEndorsInq
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Private currentPage As Integer = 1
#End Region
#Region "properties"

    Public Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return CType(viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Public Property Criteria() As String
        Get
            Return CType(viewstate("Criteria"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Criteria") = Value
        End Set
    End Property
    Public Property customerID() As String
        Get
            Return CType(viewstate("customerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("customerID") = Value
        End Set
    End Property
    Public Property Eventdate() As String
        Get
            Return CType(viewstate("Eventdate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Eventdate") = Value
        End Set
    End Property
    Public Property InsSequenceNo() As Integer
        Get
            Return CType(viewstate("InsSequenceNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("InsSequenceNo") = Value
        End Set
    End Property
    Public Property AssetSequenceNo() As Integer
        Get
            Return CType(viewstate("AssetSequenceNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("AssetSequenceNo") = Value
        End Set
    End Property


#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblRecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.Criteria, Me.Eventdate, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.Criteria, Me.Eventdate, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.Criteria, Me.Eventdate, Me.SortBy)
    End Sub
#End Region
#Region "Sub and Functions"
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True



    End Sub
    'description : sub ini untuk mengisi data ke combo box branch
    Sub fillcbo()
        Dim m_controller As New DataUserControlController
        With cboBranch
            If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "ALL")
                .Items(0).Value = "ALL"
            Else
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                Dim strBranch() As String
                strBranch = Split(Me.sesBranchId, ",")
                If UBound(strBranch) > 0 Then
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                End If
            End If
        End With

    End Sub
    'sub ini untuk melakukan perbandingan pada tanggal awal dan tanggal akhir yang dimasukkan oleh user
    Private Sub CompareDate(ByVal txtStartDate As String, ByVal txtEndDate As String)



        If txtStartDate.Length = 8 Then
            txtStartDate = Mid(txtStartDate, 5, 4) & "0" & Mid(txtStartDate, 3, 1) & "0" & Mid(txtStartDate, 1, 1)
        ElseIf txtStartDate.Length = 10 Then
            txtStartDate = Mid(txtStartDate, 7, 4) & Mid(txtStartDate, 4, 2) & Mid(txtStartDate, 1, 2)
        ElseIf txtStartDate.Length = 9 And Mid(txtStartDate, 3, 1) = "/" Then
            txtStartDate = Mid(txtStartDate, 6, 4) & Mid(txtStartDate, 4, 1) & Mid(txtStartDate, 1, 2)
        ElseIf txtStartDate.Length = 9 And Mid(txtStartDate, 2, 1) = "/" Then
            txtStartDate = Mid(txtStartDate, 6, 4) & Mid(txtStartDate, 3, 2) & Mid(txtStartDate, 1, 1)
        End If

        If txtEndDate.Length = 8 Then
            txtEndDate = Mid(txtEndDate, 5, 4) & "0" & Mid(txtEndDate, 3, 1) & "0" & Mid(txtEndDate, 1, 1)
        ElseIf txtEndDate.Length = 10 Then
            txtEndDate = Mid(txtEndDate, 7, 4) & Mid(txtEndDate, 4, 2) & Mid(txtEndDate, 1, 2)
        ElseIf txtEndDate.Length = 9 And Mid(txtEndDate, 3, 1) = "/" Then
            txtEndDate = Mid(txtEndDate, 6, 4) & Mid(txtEndDate, 4, 1) & Mid(txtEndDate, 1, 2)
        ElseIf txtEndDate.Length = 9 And Mid(txtEndDate, 2, 1) = "/" Then
            txtEndDate = Mid(txtEndDate, 6, 4) & Mid(txtEndDate, 3, 2) & Mid(txtEndDate, 1, 1)
        End If

        If txtStartDate > txtEndDate Then
            ShowMessage(lblMessage, "Tanggal Awal harus < dari tanggal Akhir", True)
        End If
    End Sub

    'description : sub ini untuk bind data endorsment customer ke datagrid

    Sub Bindgrid(ByVal cmdWhere As String, ByVal criteria As String, ByVal eventdate As String, ByVal cmSort As String)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable

        With oInsInqEndors
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = cmSort
            .Criteria = criteria
            .EventDate = eventdate
        End With

        oInsInqEndors = m_InsInqEndors.InsInqEndorsList(oInsInqEndors)

        With oInsInqEndors
            lblRecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oInsInqEndors.ListData
        dtsEntity.TableName = "Table"

        dtvEntity = dtsEntity.DefaultView

        dtgEndorsInq.DataSource = dtvEntity

        If Not dtvEntity Is Nothing Then
            dtgEndorsInq.DataBind()
        End If

        PagingFooter()
        pnlDtGrid.Visible = True

    End Sub





#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.FormID = "InsEndorsInq"

        If Not Page.IsPostBack Then
            fillcbo()
        End If

        If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            InitialDefaultPanel()

        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If
    End Sub


    Private Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Try
            lblMessage.Visible = False
            CompareDate(txtStartDate.Text, txtEndDate.Text)
            If lblMessage.Text <> "" Then
                lblMessage.Visible = True
                Exit Sub
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            lblMessage.Visible = True
        End Try

        Me.SearchBy = ""
        Me.SortBy = ""

        If cboBranch.SelectedValue <> "" And cboBranch.SelectedValue <> "ALL" Then
            Me.SearchBy = "BranchID='" & cboBranch.SelectedValue & "'"
        End If
        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
            Else
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
            End If
        End If
        If cboCriteria.SelectedItem.Value.Trim = "Endors" Then
            Me.Criteria = "Endors"
        Else
            Me.Criteria = "Terminate"
        End If
        If txtStartDate.Text <> "" And txtEndDate.Text <> "" Then
            If Me.Criteria = "Endors" Then
                If txtStartDate.Text <> "" Then
                    Me.Eventdate = "  InsuranceAsset.BDEndorsDate >= '" & ConvertDate2(txtStartDate.Text.Trim).ToString("yyyyMMdd") & "'"

                End If
                If txtEndDate.Text <> "" Then
                    Me.Eventdate = " InsuranceAsset.BDEndorsDate <= '" & ConvertDate2(txtEndDate.Text.Trim).ToString("yyyyMMdd") & "'"

                End If
            Else
                If txtStartDate.Text <> "" Then
                    Me.Eventdate = "  InsuranceAsset.TerminationDate >= '" & ConvertDate2(txtStartDate.Text.Trim).ToString("yyyyMMdd") & "'"

                End If
                If txtEndDate.Text <> "" Then
                    Me.Eventdate = " InsuranceAsset.TerminationDate <= '" & ConvertDate2(txtEndDate.Text.Trim).ToString("yyyyMMdd") & "'"

                End If
            End If
        Else
            Me.Eventdate = ""

        End If
        Bindgrid(Me.SearchBy, Me.Criteria, Me.Eventdate, Me.SortBy)
    End Sub
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboBranch.ClearSelection()
        cboSearchBy.ClearSelection()
        cboCriteria.ClearSelection()
        txtSearchBy.Text = ""
    End Sub

    Private Sub dtgEndorsInq_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEndorsInq.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lnkAgreementNo As HyperLink
            Dim lnkCustomerName As HyperLink
            Dim strCustomerID As String
            Dim strApplicationID As String
            Dim lnkPolicyno As HyperLink
            Dim strbranchid As String = e.Item.Cells(0).Text.Trim
            Dim strasset As String = e.Item.Cells(12).Text.Trim
            Dim strins As String = e.Item.Cells(13).Text.Trim
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            strApplicationID = e.Item.Cells(1).Text.Trim
            If lnkAgreementNo.Text.Trim.Length > 0 Then
                lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(strApplicationID.Trim) & "')"
            End If
            strCustomerID = e.Item.Cells(2).Text.Trim
            lnkCustomerName = CType(e.Item.FindControl("lnkCustomerName"), HyperLink)

            If strCustomerID.Trim.Length > 0 Then
                lnkCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(strCustomerID.Trim) & "')"
            End If
            lnkPolicyno = CType(e.Item.FindControl("lnkPolicyNo"), HyperLink)
            lnkPolicyno.NavigateUrl = "../ViewPolicyDetail.aspx?ApplicationID=" & strApplicationID & "&BranchID=" & strbranchid & "&AssetSeqNo=" & strasset & "&InsSeqNo=" & strins & "&AgreementNo=" & lnkAgreementNo.Text.Trim

        End If
    End Sub

End Class