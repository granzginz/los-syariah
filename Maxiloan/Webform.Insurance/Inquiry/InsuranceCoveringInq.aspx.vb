﻿#Region "Imports"

'Option Strict On
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
#End Region

Public Class InsuranceCoveringInq
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Protected WithEvents oSearchBy As UcSearchByWithNoTable
    Protected WithEvents oBranchInsurance As ucComboBranchAndInsurance
    Dim currentPage As Integer = CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Dim PageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    Dim currentPageNumber As Int32 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Dim totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Dim recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT
    Private Const SP_NAME_PAGING As String = "spInsuranceCoveringInquiry"
    Private m_controller As New GeneralPagingController
#End Region
#Region "Property"

    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property

#End Region
#Region "Initial defaultpanel"
    Public Sub InitialDefaultPanel()
        PnlGrid.Visible = False

    End Sub
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            InitialDefaultPanel()
            oSearchBy.ListData = "Agreement.AgreementNo, No Kontrak-Customer.Name , Nama Konsumen-InsuranceAsset.PolicyNumber , No Polis"
            oSearchBy.BindData()
            If Me.SortBy = "" Then Me.SortBy = " Agreement.AgreementNo "
        End If
    End Sub
#End Region

#Region "Search"
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        PnlGrid.Visible = True
        Me.SearchBy = ""

        If oBranchInsurance.BranchIDParent <> "" Then
            Me.SearchBy = " AND Agreement.BranchID = '" & oBranchInsurance.BranchIDParent & "' "
        End If

        If oBranchInsurance.InsuranceComBranchID.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " And  InsuranceAsset.InsuranceComBranchID = '" & Replace(oBranchInsurance.InsuranceComBranchID.Trim, "'", "") & "' "
        End If

        'Response.Write("BranchInsurance = " & oBranchInsurance.InsuranceComBranchID & " ")

        Dim criteria As String = DDLCriteria.SelectedItem.Value.Trim


        If criteria.Trim = "1" Then
            Me.SearchBy = Me.SearchBy & " And InsuranceAsset.InsCoSelectionDate is null and FlagInsStatus <> 'X' "

        ElseIf criteria.Trim = "2" Then
            Me.SearchBy = Me.SearchBy & " And InsuranceAsset.InsCoSelectionDate is not null And SPPANo='-' And FlagInsStatus <> 'X' "

        ElseIf criteria.Trim = "3" Then
            Me.SearchBy = Me.SearchBy & " And InsuranceAsset.SPPANo <> '-' And PolicyNumber = '-' And FlagInsStatus <> 'X' "
        End If

        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%' "
        End If

        If txtStartSPPADate.Text <> "" And txtEndSPPADate.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " And InsuranceAsset.SPPADate Between '" & ConvertDate2(txtStartSPPADate.Text) & "' And '" & ConvertDate2(txtEndSPPADate.Text) & "' "
        End If

        BindGridEntity(Me.SearchBy)

    End Sub
#End Region
#Region "Reset"

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Me.SearchBy = " And InsuranceAsset.InsCoSelectionDate is null and FlagInsStatus <> 'X' "
        If Me.SortBy = "" Then Me.SortBy = " Agreement.AgreementNo "
        oSearchBy.Text = ""
        oSearchBy.DataBind()
        txtStartSPPADate.Text = ""
        txtEndSPPADate.Text = ""
        BindGridEntity(Me.SearchBy)
    End Sub


#End Region
#Region "BindGrid"
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As New DataTable
        Dim oGeneralPaging As New Parameter.GeneralPaging
        If Me.SearchBy = "" Then Me.SearchBy = cmdWhere
        With oGeneralPaging
            .SpName = SP_NAME_PAGING
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = PageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString
        End With

        Try
            oGeneralPaging = m_controller.GetGeneralPaging(oGeneralPaging)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try

        If Not oGeneralPaging Is Nothing Then
            dtEntity = oGeneralPaging.ListData
            recordCount = oGeneralPaging.TotalRecords
        Else
            recordCount = 0
        End If

        DtgPaging.DataSource = dtEntity.DefaultView
        DtgPaging.CurrentPageIndex = 0
        DtgPaging.DataBind()
        PagingFooter()
    End Sub
    Private Sub DtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPaging.ItemDataBound
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim m As Int32
        If e.Item.ItemIndex >= 0 Then
            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(PageSize, Integer)), Double))
        If totalPages = 0 Then
            'ShowMessage(lblMessage, "Data tidak ditemukan!", true)
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            ' rgvGo.MaximumValue = CType(totalPages, String)

        End If

        lblTotRec.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        BindGridEntity(Me.SearchBy)
    End Sub
    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int16)
                BindGridEntity(Me.SearchBy)
            End If
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles DtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy)
    End Sub
#End Region

 
End Class