﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ExisitingCoverInqDetail.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.ExisitingCoverInqDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ExisitingCoverInqDetail</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                INQUIRY - EXISTING COVER
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:Label ID="LblAgreementNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:Label ID="LblCustomerName" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Perusahaan Asuransi
            </label>
            <asp:Label ID="LblInsuranceCompany" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Polis
            </label>
            <asp:Label ID="LblPolicyNumber" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DETAIL COVER ASURANSI
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgPaging" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                    BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="TAHUN">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearNum") %>'>
                                </asp:Label>
                                <asp:ImageButton ID="ImageButton2" runat="server" Visible='<%# genImage(DataBinder.Eval(Container, "DataItem.ImgStartDate"), DataBinder.Eval(Container, "DataItem.ImgEndDate") ) %>'
                                    ImageUrl="../../Images/BulatMerah.gif"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="PERIODE ASURANSI">
                            <ItemTemplate>
                                <asp:Label ID="lblstartdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StartDate")  %>'>
                                </asp:Label>&nbsp;S/D&nbsp;
                                <asp:Label ID="lblenddate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EndDate")  %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="CoverageType" HeaderText="JENIS COVER"></asp:BoundColumn>
                        <asp:BoundColumn DataField="SrccPRemiumToCust" HeaderText="PREMI SRCC" DataFormatString="{0:N2}">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TplAmountToCust" HeaderText="TPL" DataFormatString="{0:N2}">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TPLPremiumToCust" HeaderText="PREMI TPL" DataFormatString="{0:N2}">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="FloodPremiumToCust" HeaderText="PREMI FLOOD" DataFormatString="{0:N2}">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="PremiumToCustAmount" HeaderText="PREMI KE CUSTOMER" DataFormatString="{0:N2}">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="PaidByCust" HeaderText="PAID BY"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="STATUS BAYAR">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# GenTextonLabel(DataBinder.Eval(Container, "DataItem.isPaidByCust")) %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnBack" CausesValidation="False" runat="server" Text="Back" CssClass="small button gray">
        </asp:Button>
        <asp:ImageButton ID="Imagebutton1" CausesValidation="False" ImageUrl="../../Images/BulatMerah.gif"
            runat="server"></asp:ImageButton>Active Year
    </div>
    </form>
</body>
</html>
