﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Webform.UserController
#End Region
Public Class PrintRefundAsuransiForm
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Private m_controller As New InsTerminateController
    Private oCustomClass As New Parameter.InsTerminate
#End Region
#Region "Property"

    Private Property ApplicationId() As String
        Get
            Return CType(ViewState("ApplicationId"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationId") = Value
        End Set
    End Property

    Private Property AssetSequenceNo() As Integer
        Get
            Return CType(ViewState("AssetSequenceNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("AssetSequenceNo") = Value
        End Set
    End Property
    Private Property InsSequenceNo() As Integer
        Get
            Return CType(ViewState("InsSequenceNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("InsSequenceNo") = Value
        End Set
    End Property



#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        BindReport()
        'BindReportRefund()
    End Sub
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("CollPrintBPK")
        Me.ApplicationId = Request.QueryString("ApplicationID")
        Me.BranchID = Request.QueryString("BranchID")
        'Me.AssetSequenceNo = CInt(Request.QueryString("AssSeqNo"))
        'Me.InsSequenceNo = CInt(Request.QueryString("InsSeqNo"))
    End Sub

    Sub BindReport()
        GetCookies()
        Dim dstPrintRefundForm As New DataSet
        Dim PrintingRefundForm As New RptRefundAsuransiPrint
        With oCustomClass
            .strConnection = GetConnectionString
            .ApplicationID = Me.ApplicationId
            .BranchId = Me.BranchID
            '.AssetSequenceNo = Me.AssetSequenceNo
            '.InsSequenceNo = Me.InsSequenceNo
            .LoginId = Me.Loginid
            .BusinessDate = Me.BusinessDate
        End With

        dstPrintRefundForm = m_controller.InsrequestPrintRefund(oCustomClass)
        Dim numCopies As Int16 = 1
        Dim Collate As Boolean = False
        Dim startpage As Int16 = 1
        PrintingRefundForm.SetDataSource(dstPrintRefundForm)


        crvPrintRefundForm.ReportSource = PrintingRefundForm
        crvPrintRefundForm.Visible = True
        crvPrintRefundForm.DataBind()

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        PrintingRefundForm.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        PrintingRefundForm.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        Dim PDFFile As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        PDFFile = Me.Session.SessionID + Me.Loginid + "InsTerminateForm.pdf"

        strFileLocation += PDFFile
        DiskOpts.DiskFileName = strFileLocation

        PrintingRefundForm.ExportOptions.DestinationOptions = DiskOpts
        PrintingRefundForm.Export()
        PrintingRefundForm.Close()

        Response.Redirect("InsTerminate.aspx?strFileLocation=" & PDFFile)
    End Sub

    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBackReport.Click
        Response.Redirect("InsTerminate.aspx")
    End Sub

End Class