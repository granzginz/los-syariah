﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsuranceEndorsValid.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsuranceEndorsValid" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucdatece" Src="../webform.UserController/ucdatece.ascx" %>
<%@ Register TagName="UcNotaAsuransi" TagPrefix="uc1" Src="../webform.UserController/UcNotaAsuransi.ascx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InsuranceEndorsValid</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenWinViewPolicyNo(pAgreementNo, pBranch) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.Insurance/ViewPolicyDetail.aspx?AgreementNo=' + pAgreementNo + '&BranchID=' + pBranch, 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="Panel1" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h3>
                    ENDORSMENT TERIMA POLIS
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <asp:DataGrid ID="dtgEndors" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                    CellPadding="3" CellSpacing="1" CssClass="tablegrid" HorizontalAlign="Center"
                    Width="95%">
                    <AlternatingItemStyle CssClass="tdgenap" />
                    <ItemStyle CssClass="tdganjil" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="headerGrid" />
                    <Columns>
                        <asp:BoundColumn DataField="EndorsItem">
                            <HeaderStyle Width="25%" />
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="DataBenarDesc">
                            <HeaderStyle Width="50%" />
                        </asp:BoundColumn>
                        <asp:TemplateColumn>
                            <HeaderStyle Width="25%" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkEndors" Checked="true" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" />
                </asp:DataGrid>
            </div>
        </div>
        <div class="form_box">            
            <div class="form_left">
                <label>
                    No Polis</label>
                <asp:HyperLink ID="lnkNoPolis" runat="server"></asp:HyperLink>
                <asp:Label ID="lblNoPolis" runat="server" Visible="False"></asp:Label>
		    </div>
		    <div class="form_right">	
                <asp:CheckBox ID="chkNoPolis" Checked="true" runat="server" />			
		    </div>
        </div>
        <div class="form_box">            
            <div class="form_left">
                <label>
                    Nilai Pertanggungan</label>
                <uc1:ucnumberformat id="txtSumInsured" runat="server"></uc1:ucnumberformat>
                <asp:Label ID="lblS" runat="server" ForeColor="Red" Text="Data sudah benar"></asp:Label>
                <asp:Label ID="lblSumInsured" runat="server" Visible="False"></asp:Label>
		    </div>
		    <div class="form_right">		
                <asp:CheckBox ID="chkSumInsured" Checked="true" runat="server" />		
		    </div>
        </div>
        <div class="form_box">            
            <div class="form_left">
                <label>
                    No Rangka</label>
                <asp:TextBox ID="txtChasis" runat="server" Enabled="False"></asp:TextBox>
                <asp:Label ID="lblC" runat="server" ForeColor="Red" Text="Data sudah benar"></asp:Label>
                <asp:Label ID="lblChasis" runat="server" Visible="False"></asp:Label>
		    </div>
		    <div class="form_right">	
                <asp:CheckBox ID="chkChasis" Checked="true" runat="server" />		
		    </div>
        </div>
        <div class="form_box">            
            <div class="form_left">
                <label>
                    No Mesin</label>
                <asp:TextBox ID="txtMachine" runat="server" Enabled="False"></asp:TextBox>
                <asp:Label ID="lblM" runat="server" ForeColor="Red" Text="Data sudah benar"></asp:Label>
                <asp:Label ID="lblMachine" runat="server" Visible="False"></asp:Label>
		    </div>
		    <div class="form_right">			
                <asp:CheckBox ID="chkMachine" Checked="true" runat="server" />	
		    </div>
        </div>
        <div class="form_box_hide">            
            <div class="form_left">
                <label>
                    Premi Ke Perusahaan Asuransi</label>
                <uc1:ucnumberformat id="txtTagihan" runat="server"></uc1:ucnumberformat>
                <asp:Label ID="lblT" runat="server" ForeColor="Red" Text="Data sudah benar"></asp:Label>
                <asp:Label ID="lblTagihan" runat="server" Visible="False"></asp:Label>
		    </div>
		    <div class="form_right">	
                <asp:CheckBox ID="chkTagihan" Checked="true" runat="server" />			
		    </div>
        </div>
        <div class="form_box">            
            <div class="form_left">
                <label>
                    Tanggal Terima</label>
                <uc1:ucdatece id="ucTanggalTerima" runat="server"></uc1:ucdatece>
		    </div>
		    <div class="form_right">		
                <asp:CheckBox ID="chkTanggalTerima" Checked="true" runat="server" />		
		    </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnValidate" runat="server" CausesValidation="False" Text="Next"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="btnExit" runat="server" CausesValidation="False" Text="Exit" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlNotaAsuransi" Visible="false">
        <uc1:ucnotaasuransi id="UcNotaAsuransi1" runat="server" />
        <div class="form_button">
            <asp:Button ID="ButtonSaveNota" runat="server" Text="Save" CssClass="small button blue" />
            <asp:Button ID="ButtonCancelNota" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray" />
        </div>
    </asp:Panel>
    <!-- End Panel -->
    <!-- Panel untuk tampilin layar validasi2 -->
    <asp:Panel ID="PnlValid" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    ENDORSEMENT
                </h4>
            </div>
        </div>
        <%--    <table id="tblValidation" cellspacing="0" cellpadding="3" width="95%" align="center"
            border="1" runat="server">
        </table>--%>
        <asp:Panel ID="pnlValidationDiv" runat="server">
        </asp:Panel>
    </asp:Panel>
    <!-- End Panel -->
    <!-- Panel untuk tampilin insurance asset detail -->
    <asp:Panel ID="PnlValid2" runat="server" Width="100%" Visible="False">
        <%--<table id="tblValidation2" cellspacing="0" cellpadding="3" width="95%" align="center"
            border="1" runat="server">
            <tr align="center">
                <td width="15%">
                    Tahun Ke
                </td>
                <td width="20%">
                    Jenis Cover
                </td>
                <td width="20%">
                    TPL
                </td>
                <td width="20%">
                    RSCC
                </td>
                <td width="25%">
                    &nbsp;
                </td>
            </tr>
        </table>--%>
        <asp:Panel ID="pnlValidationDiv2" runat="server">
            <div class="form_box">
                <div class="form_onefifth">
                    Tahun Ke
                </div>
                <div class="form_onefifth">
                    Jenis Cover
                </div>
                <div class="form_onefifth">
                    TPL
                </div>
                <div class="form_onefifth">
                    RSCC
                </div>
                <div class="form_onefifth">
                </div>
            </div>
        </asp:Panel>
    </asp:Panel>
    <!-- End Panel -->
    <!-- Panel untuk tampilin data billing -->
    <asp:Panel ID="pnlBilling" runat="server" Width="100%" Visible="False">
        <%-- <table id="tblValidation3" cellspacing="0" cellpadding="3" width="95%" align="center"
            border="1" runat="server">
            <tr>
                <td width="25%">
                    &nbsp;
                </td>
                <td width="35%">
                    Data Dari Asuransi
                </td>
                <td width="40%">
                    Data Seharusnya
                </td>
            </tr>
        </table>--%>
        <asp:Panel ID="pnlValidationDiv3" runat="server">
            <div class="form_box">
                <div class="form_quarter">
                </div>
                <div class="form_quarter">
                    Data Dari Asuransi
                </div>
                <div class="form_quarter">
                    Data Seharusnya
                </div>
            </div>
        </asp:Panel>
        <br />
    </asp:Panel>
    <!-- End Panel -->
    <!-- Panel untuk tampilin button -->
    <asp:Panel ID="PnlBtn" runat="server" Width="100%" Visible="False">
        <div class="form_button">
            <asp:Button ID="btnEndors" runat="server" CausesValidation="False" Text="Next"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" Text="Edit" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnExitBawah" runat="server" CausesValidation="False" Text="Exit"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <!-- End Panel -->
    </form>
</body>
</html>
