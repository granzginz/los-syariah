﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SPPA.aspx.vb" Inherits="Maxiloan.Webform.Insurance.SPPA" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../webform.UserController/UcBranchAll.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcInsuranceBranchName" Src="../Webform.UserController/UcInsuranceBranchName.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchByWithNoTable" Src="../Webform.UserController/UcSearchByWithNoTable.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SPPA</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="LblErrorMessages" runat="server"></asp:Label>
    <asp:Panel ID="PnlSPPAListing" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    CETAK SPPA (SURAT PENUTUPAN PENCOVERAN ASURANSI)
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DGridSPPA" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn>
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkAll" runat="server" OnCheckedChanged="ChkStatus_CheckedChanged"
                                    AutoPostBack="True" CssClass="checkbox_general"></asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="ChkStatusAgreementNo" runat="server"></asp:CheckBox>
                                    <asp:Label ID="LblAgreementNo" runat="server" Font-Size="Smaller" Visible="False"
                                        Text='<%#  container.dataitem("AgreementNo") %>'>
                                    </asp:Label>
                                    <asp:Label ID="LblApplicationID" runat="server" Visible="False" Text='<%#  container.dataitem("ApplicationID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="MaskAssbranchName" HeaderText="PERUSAHAAN ASURANSI">
                                <HeaderStyle Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblInsCo" runat="server" Text='<%#Container.DataItem("MaskAssbranchName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="InscoSelectionDate" HeaderText="TGL PILIH ASURANSI" DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle Width="10%" HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="AGREEMENTNO" HeaderText="NO KONTRAK">
                                <HeaderStyle Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAgreementNo" runat="server" Text='<%#Container.DataItem("AGREEMENTNO")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CUSTOMERNAME" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkCust" runat="server" Text='<%# Container.DataItem("CUSTOMERNAME")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetMasterDescr" HeaderText="NAMA ASSET">
                                <HeaderStyle Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAsset" runat="server" Text='<%# Container.DataItem("AssetMasterDescr")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerID" HeaderText="CustomerID" Visible="False">
                                <HeaderStyle></HeaderStyle>
                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerID" runat="server" Text='<%# Container.DataItem("CustomerID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="small button blue"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <div class="form_title">
        <div class="form_single">
            <h4>
                CARI SPPA
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Cabang
            </label>
            <uc1:ucbranchall id="oBranch" runat="server">
                </uc1:ucbranchall>
        </div>
        <div class="form_right">
            <label>
                Perusahaan Asuransi
            </label>
            <uc1:ucinsurancebranchname id="oInsuranceBranchName" runat="server">
                </uc1:ucinsurancebranchname>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jenis
            </label>
            <asp:DropDownList ID="cboType" runat="server">
                <asp:ListItem Value="N">Kontrak Baru</asp:ListItem>
                <asp:ListItem Value="R">Perpanjangan</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form_right">
            <label>
                Tanggal Pilih Asuransi
            </label>
            <asp:TextBox runat="server" ID="txtStartSelectionDate" CssClass ="small_text"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtStartSelectionDate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
            <label class="label_auto">
                s/d
            </label>
            <asp:TextBox runat="server" ID="txtEndSelectionDate" CssClass ="small_text" ></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtEndSelectionDate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Product Asuransi
            </label>
            <asp:DropDownList ID="cboProductAsuransi" runat="server">
                <asp:ListItem Value="CP">Pembiayaan Protection</asp:ListItem>
                <asp:ListItem Value="JK">Jaminan Pembiayaan</asp:ListItem>
                <asp:ListItem Value="KB">Kendaraan Bermotor</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray">
        </asp:Button>
    </div>
    </form>
</body>
</html>
