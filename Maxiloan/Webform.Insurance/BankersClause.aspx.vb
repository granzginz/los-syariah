﻿
#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports System.Linq
#End Region

Public Class BankersClause
    Inherits Maxiloan.Webform.WebBased    

    Private m_controller As New InsPolicyNumberControler
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property
    Private Property No() As Integer
        Get
            Return CType(ViewState("No"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("No") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property FundingCoyName() As String
        Get
            Return CType(ViewState("FundingCoyName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyName") = Value
        End Set
    End Property
#End Region
#Region "LinkTo"
    Function LinkTo(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinApplication('" & strApplicationID & "','" & strStyle & "')"
    End Function

    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            Me.FormID = "BANKERSCL"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                txtPage.Text = "1"
                Me.Sort = "ApplicationID ASC"
                Me.CmdWhere = " BranchID = '" + Me.sesBranchId.Replace("'", "").Trim() + "'".Trim()
                BindComboInsuranceBranch()
                BindCleardtgPaging()
            End If
        End If

    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oBankersClause As New Parameter.BankersClause

        Dim DataList As New List(Of Parameter.BankersClause)
        Dim bankersClause As New Parameter.BankersClause
        If Not TempDataTable Is Nothing Then
            For index = 0 To TempDataTable.Rows.Count - 1
                bankersClause = New Parameter.BankersClause

                bankersClause.ApplicationID = TempDataTable.Rows(index).Item("ApplicationID").ToString.Trim
                bankersClause.InsuranceComBranchID = TempDataTable.Rows(index).Item("FundingCoyName").ToString.Trim
                DataList.Add(bankersClause)
            Next
        End If

        InitialDefaultPanel()
        oBankersClause.strConnection = GetConnectionString()
        oBankersClause.WhereCond = cmdWhere
        oBankersClause.CurrentPage = currentPage
        oBankersClause.PageSize = pageSize
        oBankersClause.SortBy = Me.Sort
        oBankersClause = m_controller.GetBankersClause(oBankersClause)

        If Not oBankersClause Is Nothing Then
            dtEntity = oBankersClause.Listdata
            recordCount = oBankersClause.Totalrecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()

        For intLoopGrid = 0 To dtgPaging.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(dtgPaging.Items(intLoopGrid).FindControl("check"), CheckBox)
            Dim ApplicationID As String = CType(dtgPaging.Items(intLoopGrid).FindControl("lblApplicationID"), HyperLink).Text.Trim
            Dim FundingCoyName As String = dtgPaging.Items(intLoopGrid).Cells(3).Text.Trim

            Me.ApplicationID = ApplicationID
            Me.FundingCoyName = FundingCoyName

            Dim query As New Parameter.BankersClause

            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If


            If Not query Is Nothing Then
                chek.Checked = True
            End If
        Next
    End Sub
    Sub BindDataGridEndorsment()
        Dim DataList As New List(Of Parameter.BankersClause)
        Dim bankersClause As New Parameter.BankersClause

        If TempDataTable Is Nothing Then
            TempDataTable = New DataTable
            With TempDataTable
                .Columns.Add(New DataColumn("No", GetType(String)))
                .Columns.Add(New DataColumn("Name", GetType(String)))
                .Columns.Add(New DataColumn("CustomerID", GetType(String)))
                .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
                .Columns.Add(New DataColumn("FundingCoyName", GetType(String)))
                .Columns.Add(New DataColumn("AttributeContent", GetType(String)))
                .Columns.Add(New DataColumn("PolicyNumber", GetType(String)))
            End With
        Else
            For index = 0 To TempDataTable.Rows.Count - 1
                bankersClause = New Parameter.BankersClause

                bankersClause.ApplicationID = TempDataTable.Rows(index).Item("ApplicationID").ToString.Trim
                bankersClause.InsuranceComBranchID = TempDataTable.Rows(index).Item("FundingCoyName").ToString.Trim
                DataList.Add(bankersClause)
            Next
        End If


        Dim oRow As DataRow
        If No = 0 Then
            No = 1
        End If


        For intLoopGrid = 0 To dtgPaging.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(dtgPaging.Items(intLoopGrid).FindControl("check"), CheckBox)
            Dim Name As String = CType(dtgPaging.Items(intLoopGrid).FindControl("lblName"), HyperLink).Text.Trim
            Dim CustomerID As String = CType(dtgPaging.Items(intLoopGrid).FindControl("hdnCustomerID"), HiddenField).Value.Trim
            Dim ApplicationID As String = CType(dtgPaging.Items(intLoopGrid).FindControl("lblApplicationID"), HyperLink).Text.Trim
            Dim FundingCoyName As String = dtgPaging.Items(intLoopGrid).Cells(3).Text.Trim
            Dim AttributeContent As String = dtgPaging.Items(intLoopGrid).Cells(4).Text.Trim
            Dim PolicyNumber As String = dtgPaging.Items(intLoopGrid).Cells(5).Text.Trim

            Me.ApplicationID = ApplicationID
            Me.FundingCoyName = FundingCoyName

            Dim query As New Parameter.BankersClause

            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If


            If chek.Checked And query Is Nothing Then
                oRow = TempDataTable.NewRow()
                oRow("No") = No.ToString
                oRow("Name") = Name
                oRow("CustomerID") = CustomerID
                oRow("ApplicationID") = ApplicationID
                oRow("FundingCoyName") = FundingCoyName
                oRow("AttributeContent") = AttributeContent
                oRow("PolicyNumber") = PolicyNumber
                TempDataTable.Rows.Add(oRow)
                No += 1
            End If
        Next
    End Sub
    Public Function PredicateFunction(ByVal bankersClause As Parameter.BankersClause) As Boolean
        Return bankersClause.ApplicationID = Me.ApplicationID And bankersClause.InsuranceComBranchID = Me.FundingCoyName
    End Function
    Sub BindCleardtgPaging()
        Dim data As New DataTable
        With data
            .Columns.Add(New DataColumn("No", GetType(String)))
            .Columns.Add(New DataColumn("Name", GetType(String)))
            .Columns.Add(New DataColumn("CustomerID", GetType(String)))
            .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            .Columns.Add(New DataColumn("FundingCoyName", GetType(String)))
            .Columns.Add(New DataColumn("AttributeContent", GetType(String)))
            .Columns.Add(New DataColumn("PolicyNumber", GetType(String)))
        End With
        dtgPaging.DataSource = data.DefaultView
        dtgPaging.DataBind()
    End Sub
    Sub BindClearDataGridEndorsment()
        TempDataTable = New DataTable
        With TempDataTable
            .Columns.Add(New DataColumn("No", GetType(String)))
            .Columns.Add(New DataColumn("Name", GetType(String)))
            .Columns.Add(New DataColumn("CustomerID", GetType(String)))
            .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            .Columns.Add(New DataColumn("FundingCoyName", GetType(String)))
            .Columns.Add(New DataColumn("AttributeContent", GetType(String)))
            .Columns.Add(New DataColumn("PolicyNumber", GetType(String)))
        End With

        DataGridEndorsment.DataSource = TempDataTable.DefaultView
        DataGridEndorsment.DataBind()

    End Sub
    Sub ReadDataGridEndorsment(ByVal ListApplicationID As List(Of String))
        For intLoopGrid = 0 To DataGridEndorsment.Items.Count - 1
            Dim ApplicationID As String = CType(DataGridEndorsment.Items(intLoopGrid).FindControl("lblApplicationID"), HyperLink).Text.Trim
            ListApplicationID.Add(ApplicationID)
        Next
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lblName As New HyperLink
        Dim lblApplicationID As New HyperLink
        If e.Item.ItemIndex >= 0 Then
            lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), HyperLink)

            Dim CustomerID As String = CType(e.Item.FindControl("hdnCustomerID"), HiddenField).Value.Trim
            lblName.NavigateUrl = LinkToCustomer(CustomerID, "AccAcq")
            lblApplicationID.NavigateUrl = LinkTo(lblApplicationID.Text.Trim, "AccAcq")
        End If
    End Sub
    Private Sub DataGridEndorsment_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DataGridEndorsment.ItemDataBound
        Dim lblName As New HyperLink
        Dim lblApplicationID As New HyperLink
        If e.Item.ItemIndex >= 0 Then
            lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), HyperLink)


            Dim CustomerID As String = CType(e.Item.FindControl("hdnCustomerID"), HiddenField).Value.Trim
            lblName.NavigateUrl = LinkToCustomer(CustomerID, "AccAcq")
            lblApplicationID.NavigateUrl = LinkTo(lblApplicationID.Text.Trim, "AccAcq")
        End If
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindDataGridEndorsment()
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindDataGridEndorsment()
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim customClass As New Parameter.BankersClause
        Dim ListApplicationID As New List(Of String)
        Dim message As String = ""

        With customClass
            .NoClause = txtNoClause.Text.Trim
            .TglClause = Date.ParseExact(TglClause.Text, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            .InsuranceComBranchID = cboInsuranceBranch.SelectedValue.Trim
            .BranchId = Me.sesBranchId.Replace("'", "").Trim
            .ContactPersonName = txtContactPerson.Text.Trim
            .strConnection = GetConnectionString()
        End With

        Try
            message = m_controller.BankersClauseHSaveAdd(customClass)
            ReadDataGridEndorsment(ListApplicationID)

            If ListApplicationID.Count > 0 And message.Trim = "" Then
                For index = 0 To ListApplicationID.Count - 1
                    Dim ApplicationID As String = ListApplicationID(index)
                    With customClass
                        .NoClause = txtNoClause.Text.Trim
                        .ApplicationID = ApplicationID.Trim
                        .InsuranceComBranchID = cboInsuranceBranch.SelectedValue.Trim
                        .BranchId = Me.sesBranchId.Replace("'", "").Trim
                        .strConnection = GetConnectionString()
                    End With
                    message = m_controller.BankersClauseDSaveAdd(customClass)

                    If message.Trim <> "" Then
                        Stop
                    End If

                Next
            End If
            pnlList.Visible = True
            pnlAddEdit.Visible = False


            If message.Trim <> "" Then
                ShowMessage(lblMessage, message, True)
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
            End If

            ClearALL()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("BankersClause")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("BankersClause")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
        BindDataGridEndorsment()
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = " BranchID = '" + Me.sesBranchId.Replace("'", "").Trim() + "'".Trim()
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = " BranchID = '" + Me.sesBranchId.Replace("'", "").Trim() + "'".Trim() + " and " & cboSearch.SelectedValue & " like '%" & txtSearch.Text.ToUpper().Trim & "%'"

            If cboInsuranceBranch.SelectedValue.Trim <> "" And cboInsuranceBranch.SelectedValue.Trim <> "0" Then
                Me.CmdWhere = " BranchID = '" + Me.sesBranchId.Replace("'", "").Trim() + "'".Trim() _
                    + " and MaskAssBranchID = '" + cboInsuranceBranch.SelectedValue + "'" _
                    + " and " & cboSearch.SelectedValue & " like '%" & txtSearch.Text.ToUpper().Trim & "%'"
            End If
        Else
            Me.CmdWhere = " BranchID = '" + Me.sesBranchId.Replace("'", "").Trim() + "'".Trim()

            If cboInsuranceBranch.SelectedValue.Trim <> "" And cboInsuranceBranch.SelectedValue.Trim <> "0" Then
                Me.CmdWhere = " BranchID = '" + Me.sesBranchId.Replace("'", "").Trim() + "'".Trim() _
                    + " and MaskAssBranchID = '" + cboInsuranceBranch.SelectedValue + "'"
            End If

        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        ClearALL()
    End Sub
    Sub ClearALL()
        txtContactPerson.Text = ""
        txtNoClause.Text = ""
        txtSearch.Text = ""
        TglClause.Text = ""

        cboInsuranceBranch.SelectedIndex = 0
        cboSearch.SelectedIndex = 0

        pnlList.Visible = True
        pnlAddEdit.Visible = False
        BindClearDataGridEndorsment()
        BindGridEntity(Me.CmdWhere)
    End Sub
    Protected Sub ButtonSelect_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSelect.Click
        BindDataGridEndorsment()
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        If TempDataTable.Rows.Count > 0 Then
            DataGridEndorsment.DataSource = TempDataTable.DefaultView
            DataGridEndorsment.DataBind()
        End If
    End Sub
    Sub BindComboInsuranceBranch()
        Dim DtInsuranceBranchName As New DataTable
        Dim m_controller As New DataUserControlController

        DtInsuranceBranchName = m_controller.GetInsuranceBranchName(GetConnectionString, Me.sesBranchId, Me.Loginid)

        cboInsuranceBranch.DataValueField = "ID"
        cboInsuranceBranch.DataTextField = "Name"
        cboInsuranceBranch.DataSource = DtInsuranceBranchName
        cboInsuranceBranch.DataBind()
        cboInsuranceBranch.Items.Insert(0, "Select One")
        If ReqValField.Enabled = True Then
            cboInsuranceBranch.Items(0).Value = ""
        Else
            cboInsuranceBranch.Items(0).Value = "0"
        End If
    End Sub
    Protected Sub OnTextChanged_InsuranceBranch(ByVal sender As Object, ByVal e As EventArgs)
        Dim customClass As New Parameter.BankersClause


        If cboInsuranceBranch.SelectedValue <> "" And cboInsuranceBranch.SelectedValue <> "0" Then
            customClass.BranchId = Me.sesBranchId.Replace("'", "").Trim
            customClass.InsuranceComBranchID = cboInsuranceBranch.SelectedValue
            customClass.strConnection = GetConnectionString()
            customClass = m_controller.GetInsCompanyBranchByID(customClass)

            If customClass.Listdata.Rows.Count > 0 Then
                txtContactPerson.Text = customClass.Listdata.Rows(0).Item("ContactPersonName").ToString.Trim
            End If
        End If
    End Sub

End Class