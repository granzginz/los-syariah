﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class InsuranceBilling
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oInsuranceBranchName As UcInsuranceBranchName

#Region "Property "

    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property JmlRecord() As Int64
        Get
            Return CType(viewstate("JmlRecord"), Int64)
        End Get
        Set(ByVal Value As Int64)
            viewstate("JmlRecord") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
    Private Property InsCoySelect() As String
        Get
            Return CType(viewstate("InsCoySelect"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsCoySelect") = Value
        End Set
    End Property
    Private Property InsCoySelectName() As String
        Get
            Return CType(viewstate("InsCoySelectName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsCoySelectName") = Value
        End Set
    End Property
    Private Property TermOfPayment() As Int16
        Get
            Return CType(viewstate("TermOfPayment"), Int16)
        End Get
        Set(ByVal Value As Int16)
            viewstate("TermOfPayment") = Value
        End Set
    End Property
    Private Property ApplicationId() As String
        Get
            Return CType(viewstate("ApplicationId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationId") = Value
        End Set
    End Property
    Private Property StatusPage() As String
        Get
            Return CType(ViewState("StatusPage"), String)
        End Get
        Set(ByVal StatusPage As String)
            ViewState("StatusPage") = StatusPage
        End Set
    End Property
    Private Property MaskAssBranchID() As String
        Get
            Return CType(ViewState("MaskAssBranchID"), String)
        End Get
        Set(ByVal MaskAssBranchID As String)
            ViewState("MaskAssBranchID") = MaskAssBranchID
        End Set
    End Property

    Private Property BranchIDIns() As String
        Get
            Return CType(ViewState("BranchIDIns"), String)
        End Get
        Set(ByVal BranchIDIns As String)
            ViewState("BranchIDIns") = BranchIDIns
        End Set
    End Property

#End Region
#Region "Constanta"
    Protected WithEvents oSearchBy As UcSearchByWithNoTable
    Dim currentPage As Integer = CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Dim PageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    ' Dim PageSize As Int16 = 2
    Dim currentPageNumber As Int32 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Dim totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Dim recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT
    Private Const SP_NAME_PAGING As String = "spPagingInsBilling"
    Private Const SP_NAME_PAGINGCP As String = "spPagingInsBillingCP"
    Private Const SP_NAME_PAGINGJK As String = "spPagingInsBillingJK"
    Private oController As New SPPAController
    Private m_controller As New AssetDataController
#End Region
#Region "InitialDefaultPanel"

    Public Sub InitialDefaultPanel()
        PnlGrid.Visible = False
        oInsuranceBranchName.loadInsBranch(False)

        FillCbo(cboProductAsuransi, "dbo.tblinsuranceproduct")

    End Sub
#End Region
#Region "PageLoad"


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If

        LblBranchName.Text = Me.BranchName

        If Not IsPostBack Then
            Me.MaskAssBranchID = Request.QueryString("MaskAssBranchID")
            Me.StatusPage = IIf(Request.QueryString("StatusPage") <> "", Request.QueryString("StatusPage"), "TagihanAsuransi")

            If Me.SortBy = "" Then Me.SortBy = " AgreementNo "

            If Me.MaskAssBranchID <> "" Then
                oInsuranceBranchName.loadInsBranch(False)
                Search(True)
            Else
                InitialDefaultPanel()
            End If
            ClearViewState()
            LblErrorMessages.Text = ""
            If IsSingleBranch() Then 'And CInt(IIf(Replace(Me.sesBranchId, "'", "") = "", "0", Replace(Me.sesBranchId, "'", ""))) >= 110 Then
                'oSearchBy.ListData = "AgreementNo, No Kontrak-Name , Nama Konsumen-PolicyNumber, No Polis-NotaAsuransi.NoNotaAsuransi, No Nota"
                oSearchBy.ListData = "AgreementNo, Agreement NO-Name , Customer Name-PolicyNumber , PolicyNo"
                oSearchBy.BindData()
            Else
                KickOutForMultipleBranch()
            End If
        End If

    End Sub

#End Region
#Region "Ketika Tombol Search"
    Sub Search(ByVal BindSearch As Boolean)
        ClearViewState()

        PnlGrid.Visible = True
        Me.SearchBy = ""
        'Me.SearchBy = " And InsuranceAsset.BranchId = '" & Replace(Me.sesBranchId, "'", "") & "'"

        If BindSearch Then
            oInsuranceBranchName.SelectedValue = Me.MaskAssBranchID
        Else
            If oSearchBy.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " like  '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%' "
            End If
        End If

        If oInsuranceBranchName.SelectedValue <> "0" Then
            'If cboProductAsuransi.SelectedItem.Value = "KB" Then
            '    Me.SearchBy = Me.SearchBy & " And InsuranceAsset.MaskAssID = '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "'  "
            'ElseIf cboProductAsuransi.SelectedItem.Value = "CP" Then
            '    Me.SearchBy = Me.SearchBy & " And InsuranceAsset.MaskAssID = '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "'  "
            'ElseIf cboProductAsuransi.SelectedItem.Value = "JK" Then
            '    Me.SearchBy = Me.SearchBy & " And InsuranceAsset.MaskAssID = '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "'  "
            'End If

            'modify nofi 0622018 
            If cboProductAsuransi.SelectedItem.Value = "KB" Then
                Me.SearchBy = Me.SearchBy & " And InsuranceAsset.MaskAssBranchID = '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "' and InsuranceAsset.KendaraanMotor='1' "
            ElseIf cboProductAsuransi.SelectedItem.Value = "CP" Then
                Me.SearchBy = Me.SearchBy & " And InsuranceAsset.MaskAssBranchIDCreditProtection = '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "' and InsuranceAsset.CreditProtection = '1' "
            ElseIf cboProductAsuransi.SelectedItem.Value = "JK" Then
                Me.SearchBy = Me.SearchBy & " And InsuranceAsset.MaskAssBranchIDJaminanCredit = '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "' And InsuranceAsset.JaminanCredit = '1'  "
            End If
            Me.InsCoySelect = Replace(oInsuranceBranchName.SelectedValue, "'", "")
            Me.InsCoySelectName = oInsuranceBranchName.SelectedText
        End If

        If Me.StatusPage.Trim = "TerimaTagihan" Then
            Me.SearchBy = Me.SearchBy & " And isnull(InsuranceAsset.NoNotaAsuransi,'') <> '' "
        End If
        If cboProductAsuransi.SelectedItem.Value = "KB" Then
            BindGridEntity(Me.SearchBy)
        ElseIf cboProductAsuransi.SelectedItem.Value = "CP" Then
            BindGridEntityCP(Me.SearchBy)
        ElseIf cboProductAsuransi.SelectedItem.Value = "JK" Then
            BindGridEntityJK(Me.SearchBy)
        End If

    End Sub
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Search(False)
    End Sub
#End Region
#Region "DataBind"

    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As New DataTable
        Dim oGeneralPaging As New Parameter.GeneralPaging
        With oGeneralPaging
            .SpName = SP_NAME_PAGING
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = PageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString()
        End With
        Dim m_controller As New GeneralPagingController
        Try
            oGeneralPaging = m_controller.GetGeneralPaging(oGeneralPaging)

        Catch ex As Exception
            '  ex.WriteLog(ex.Message, "MAXILOAN", "ModuleInsurance")

        End Try


        If Not oGeneralPaging Is Nothing Then
            dtEntity = oGeneralPaging.ListData
            recordCount = oGeneralPaging.TotalRecords
        Else
            recordCount = 0
        End If

        Me.JmlRecord = recordCount

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()


        PagingFooter()
    End Sub

    Sub BindGridEntityCP(ByVal cmdWhere As String)
        Dim dtEntity As New DataTable
        Dim oGeneralPaging As New Parameter.GeneralPaging
        With oGeneralPaging
            .SpName = SP_NAME_PAGINGCP
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            '.PageSize = PageSize
            .PageSize = 20
            .SortBy = Me.Sort
            .strConnection = GetConnectionString()
        End With
        Dim m_controller As New GeneralPagingController
        Try
            oGeneralPaging = m_controller.GetGeneralPaging(oGeneralPaging)

        Catch ex As Exception
            '  ex.WriteLog(ex.Message, "MAXILOAN", "ModuleInsurance")

        End Try


        If Not oGeneralPaging Is Nothing Then
            dtEntity = oGeneralPaging.ListData
            recordCount = oGeneralPaging.TotalRecords
        Else
            recordCount = 0
        End If

        Me.JmlRecord = recordCount

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()


        PagingFooter()
    End Sub


    Sub BindGridEntityJK(ByVal cmdWhere As String)
        Dim dtEntity As New DataTable
        Dim oGeneralPaging As New Parameter.GeneralPaging
        With oGeneralPaging
            .SpName = SP_NAME_PAGINGJK
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = PageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString()
        End With
        Dim m_controller As New GeneralPagingController
        Try
            oGeneralPaging = m_controller.GetGeneralPaging(oGeneralPaging)

        Catch ex As Exception
            '  ex.WriteLog(ex.Message, "MAXILOAN", "ModuleInsurance")

        End Try


        If Not oGeneralPaging Is Nothing Then
            dtEntity = oGeneralPaging.ListData
            recordCount = oGeneralPaging.TotalRecords
        Else
            recordCount = 0
        End If

        Me.JmlRecord = recordCount

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()


        PagingFooter()
    End Sub


#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(PageSize, Integer)), Double))
        If totalPages = 0 Then
            'ShowMessage(lblMessage, "Data tidak ditemukan!", true)
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            ' rgvGo.MaximumValue = CType(totalPages, String)

        End If

        lblTotRec.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select


        FillViewStateHalaman()
        BindGridEntity(Me.SearchBy)
        CheckIfEverChecked()
    End Sub
    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int16)
                FillViewStateHalaman()
                BindGridEntity(Me.SearchBy)
            End If
        End If
    End Sub
#End Region
#Region "Sorting"


    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy)
    End Sub
#End Region
#Region "Reset"
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Me.SearchBy = " And BranchId  ='" & Replace(Me.sesBranchId, "'", "") & "'"
        ClearViewState()
        oSearchBy.Text = ""
        InitialDefaultPanel()
    End Sub
#End Region
#Region "CheckStatus"

    Public Sub ChkStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim loopitem As Int16
        Dim ChkStatusAccountPayableNo As CheckBox

        For loopitem = 0 To CType(dtgPaging.Items.Count - 1, Int16)
            Context.Trace.Write("loopitem = " & loopitem)
            ChkStatusAccountPayableNo = New CheckBox
            ChkStatusAccountPayableNo = CType(dtgPaging.Items(loopitem).FindControl("ChkStatusAccountPayableNo"), CheckBox)

            If ChkStatusAccountPayableNo.Enabled Then
                If CType(sender, CheckBox).Checked = True Then
                    Context.Trace.Write("CType(sender, CheckBox).Checked  = True ")
                    ChkStatusAccountPayableNo.Checked = True
                Else
                    Context.Trace.Write("CType(sender, CheckBox).Checked  = False ")
                    ChkStatusAccountPayableNo.Checked = False
                End If
            Else
                Context.Trace.Write("ChkStatusAccountPayableNo.Checked = False")
                ChkStatusAccountPayableNo.Checked = False
            End If
        Next

    End Sub
#End Region
#Region "FillViewStateHalaman"

    Private Sub FillViewStateHalaman()
        'Isi ViewState per halaman grid
        Context.Trace.Write("FillViewStateHalaman")
        Dim irecord As Int16
        Dim cb As CheckBox
        Dim hyAgreementGrid As HyperLink
        Dim hyAccountPayableNo As HyperLink
        Dim strAccountPayableNo As String
        Dim koma As String
        Dim recordDataGrid As Int16
        Dim labelTermPayment As Label
        Dim lblBranchIDIns As Label

        recordDataGrid = CType(dtgPaging.Items.Count, Int16)
        ' recordDataGrid = 10
        strAccountPayableNo = ""
        viewstate("hal" & CStr(CInt(lblPage.Text)).Trim) = ""
        Context.Trace.Write("recordDataGrid :" + CType(recordDataGrid, String))
        Try
            For irecord = 0 To CType(recordDataGrid - 1, Int16)
                Context.Trace.Write("Var Irecord :" + CType(irecord, String))
                Context.Trace.Write("Var PageSize :" + CType(PageSize, String))
                cb = CType(dtgPaging.Items(irecord).FindControl("ChkStatusAccountPayableNo"), CheckBox)
                hyAgreementGrid = CType(dtgPaging.Items(irecord).Cells(0).FindControl("hyAgreementGrid"), HyperLink)
                hyAccountPayableNo = CType(dtgPaging.Items(irecord).Cells(0).FindControl("hyAccountPayableNo"), HyperLink)
                labelTermPayment = CType(dtgPaging.Items(irecord).FindControl("lblTermOfPayment"), Label)
                Me.TermOfPayment = CType(labelTermPayment.Text, Int16)
                'tambahin by wira 2016-02-23
                lblBranchIDIns = CType(dtgPaging.Items(irecord).Cells(0).FindControl("lblBranchIdIns"), Label)
                Me.BranchIDIns = lblBranchIDIns.Text

                If cb.Checked = True Then
                    If strAccountPayableNo = "" Then koma = "" Else koma = ","
                    strAccountPayableNo = strAccountPayableNo & koma & hyAccountPayableNo.Text.Trim
                    viewstate("hal" & CStr(CInt(lblPage.Text)).Trim) = strAccountPayableNo.Trim
                End If

            Next

            Context.Trace.Write("strAccountPayableNo = " & strAccountPayableNo)

        Catch ex As Exception
            Response.Write(ex.Message)
            Dim oErr As New MaxiloanExceptions
            oErr.WriteLog("InsuranceBilling.aspx", "Sub FillViewStateHalaman", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        End Try

    End Sub


#End Region
#Region "JoinAllViewstateHalaman"

    Private Function JoinAllViewstateHalaman() As String
        Dim totalpages As Integer = CType(lblTotPage.Text, Integer)
        Dim strAccountPayableNo As String
        Dim koma As String
        Dim ihal As Integer
        Dim hal As String
        Context.Trace.Write("JoinAllViewstateHalaman")

        strAccountPayableNo = ""
        For ihal = 1 To totalpages
            hal = CStr("hal" & CStr(ihal))
            If strAccountPayableNo.Trim = "" Then koma = "" Else koma = ","
            strAccountPayableNo = strAccountPayableNo & koma & CType(viewstate(hal), String)
            Viewstate("AllPage") = strAccountPayableNo

        Next
        Context.Trace.Write("strAccountPayableNo.Trim = " & strAccountPayableNo.Trim)
        Return strAccountPayableNo.Trim

    End Function

#End Region
#Region "Check apakah agrement ini pernah di centang atau tidak"

    Public Sub CheckIfEverChecked()
        Context.Trace.Write("CheckIfEverChecked()")
        Dim AccountPayablechecked(PageSize) As String
        Dim chkAccountPayableNo As CheckBox
        Dim loopingchecked As Int16
        Dim hyAgreementGrid As HyperLink
        Dim hyAccountPayableNo As HyperLink
        Dim recordDataGrid As Int16
        recordDataGrid = CType(dtgPaging.Items.Count, Int16)


        If CType(viewstate("hal" & CStr(CInt(lblPage.Text)).Trim), String) <> "" Then
            AccountPayablechecked = Split(CType(viewstate("hal" & CStr(CInt(lblPage.Text)).Trim), String), ",")
            For loopingchecked = 0 To CType(recordDataGrid - 1, Int16)
                Context.Trace.Write("Var loopingchecked :" + CType(loopingchecked, String))
                chkAccountPayableNo = CType(dtgPaging.Items(loopingchecked).FindControl("ChkStatusAccountPayableNo"), CheckBox)
                hyAgreementGrid = CType(dtgPaging.Items(loopingchecked).FindControl("hyAgreementGrid"), HyperLink)
                hyAccountPayableNo = CType(dtgPaging.Items(loopingchecked).FindControl("hyAccountPayableNo"), HyperLink)
                'Context.Trace.Write("UBound(AccountPayablechecked) =" & UBound(AccountPayablechecked))
                For periksa As Int16 = 0 To CType(UBound(AccountPayablechecked), Int16)
                    If chkAccountPayableNo.Checked = False Then
                        If hyAccountPayableNo.Text.Trim = AccountPayablechecked(periksa).Trim Then
                            chkAccountPayableNo.Checked = True
                        End If
                    End If

                Next
            Next


        End If

    End Sub

#End Region
#Region "Clear ViewState"

    Private Sub ClearViewState()
        Dim totalpages As Integer = dtgPaging.PageCount
        Dim strAccountPayableNo As String
        Dim koma As String
        Dim i As Integer
        Dim hal As String

        strAccountPayableNo = ""
        For i = 0 To totalpages
            hal = CStr("hal" & CStr(i))
            viewstate(hal) = ""
        Next
        viewstate("AllPage") = ""
    End Sub

#End Region

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        FillViewStateHalaman()
        Dim AccountPayableChecked As String = JoinAllViewstateHalaman().Trim
        If AccountPayableChecked = "" Then
            Exit Sub
        End If
        Context.Trace.Write("AccountPayableChecked sebelum replace= " & AccountPayableChecked)
        Dim JmlRecorddicheck As String = Replace(AccountPayableChecked, ",,", "")
        If Right(JmlRecorddicheck, 1) = "," Then
            JmlRecorddicheck = Left(JmlRecorddicheck, JmlRecorddicheck.Length - 1)
        End If
        Dim oDataTable As DataTable = New DataTable
        Dim objRow As DataRow
        Dim objrowResult As DataRow
        oDataTable.Columns.Add("AccountPayableNo", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("APDescription", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("AgreementNo", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("CustomerName", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("PolicyNumber", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("FlagInsActivation", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("PremiumEndorsmentAmountToInsco", System.Type.GetType("System.Double"))
        oDataTable.Columns.Add("PremiumAmountToInsco", System.Type.GetType("System.Double"))
        oDataTable.Columns.Add("EndorsToInsCoPremium", System.Type.GetType("System.Double"))
        oDataTable.Columns.Add("PremiumToBePaid", System.Type.GetType("System.Double"))
        oDataTable.Columns.Add("SelisihToleransi", System.Type.GetType("System.Double"))
        oDataTable.Columns.Add("CustomerId", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("ApplicationId", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("PremiumPaidByCUst", System.Type.GetType("System.Double"))
        oDataTable.Columns.Add("PremiumAmountToAgent", System.Type.GetType("System.Double"))
        oDataTable.Columns.Add("AdminFee", System.Type.GetType("System.Double"))
        oDataTable.Columns.Add("MateraiFee", System.Type.GetType("System.Double"))
        oDataTable.Columns.Add("PPh", System.Type.GetType("System.Double"))
        oDataTable.Columns.Add("PPN", System.Type.GetType("System.Double"))
        'oDataTable.Columns.Add("NoNotaAsuransi", System.Type.GetType("System.String"))
        Dim pisahkanAccountPayableNo() As String
        Dim pisahkanperbaris() As String
        Dim iloop As Integer = 0
        pisahkanAccountPayableNo = Split(CStr(JmlRecorddicheck), ",")
        Context.Trace.Write("UBound(pisahkanAccountPayableNo) = " & UBound(pisahkanAccountPayableNo))
        Dim oEntities As New Parameter.SPPA
        For iloop = 0 To UBound(pisahkanAccountPayableNo)
            pisahkanperbaris = Split(pisahkanAccountPayableNo(iloop), ",")
            Context.Trace.Write("iloop = " & iloop)
            Context.Trace.Write("Masukkan ke data table = " & pisahkanperbaris(0))

            With oEntities
                '.WhereCond = "Where AccountPayable.AccountPayableNo = '" & Replace(pisahkanperbaris(0), "'", "") & "' and Agreement.BranchId = '" & Replace(Me.sesBranchId, "'", "") & "' "
                '.WhereCond = "Where AccountPayable.AccountPayableNo = '" & Replace(pisahkanperbaris(0), "'", "") & "' and Agreement.BranchId = '" & Replace(Me.BranchIDIns, "'", "") & "' "
                .WhereCond = "Where AccountPayable.AccountPayableNo = '" & Replace(pisahkanperbaris(0), "'", "") & "' and Agreement.BranchId = '" & Left(Replace(pisahkanperbaris(0), "'", ""), 3) & "' "
                If cboProductAsuransi.SelectedItem.Value = "KB" Then
                    .PageSource = CommonVariableHelper.PAGE_SOURCE_GET_INSURANCE_BILLING_SELECT
                ElseIf cboProductAsuransi.SelectedItem.Value = "CP" Then
                    .PageSource = "PAGE_SOURCE_GET_INSURANCE_BILLING_SELECTCP"
                ElseIf cboProductAsuransi.SelectedItem.Value = "JK" Then
                    .PageSource = "PAGE_SOURCE_GET_INSURANCE_BILLING_SELECTJK"
                End If

                .strConnection = GetConnectionString()
            End With
            Try
                oEntities = oController.GetListCreateSPPA(oEntities)
                Dim dtEntity As New DataTable
                If Not oEntities Is Nothing Then
                    dtEntity = oEntities.ListData
                    objRow = oDataTable.NewRow
                    objrowResult = dtEntity.Rows.Item(0)
                    objRow("AccountPayableNo") = objrowResult("AccountPayableNo")
                    objRow("APDescription") = objrowResult("APDescription")
                    objRow("AgreementNo") = objrowResult("AgreementNo")
                    objRow("CustomerName") = objrowResult("CustomerName")
                    objRow("PolicyNumber") = objrowResult("PolicyNumber")
                    objRow("FlagInsActivation") = objrowResult("FlagInsActivation")
                    objRow("PremiumEndorsmentAmountToInsco") = objrowResult("PremiumEndorsmentAmountToInsco")
                    objRow("PremiumAmountToInsco") = objrowResult("PremiumAmountToInsco")
                    objRow("EndorsToInsCoPremium") = objrowResult("EndorsToInsCoPremium")
                    objRow("PremiumToBePaid") = objrowResult("PremiumToBePaid")
                    objRow("SelisihToleransi") = objrowResult("SelisihToleransi")
                    objRow("CustomerId") = objrowResult("CustomerId")
                    objRow("ApplicationId") = objrowResult("ApplicationId")
                    objRow("PremiumPaidByCUst") = objrowResult("PremiumPaidByCUst")
                    objRow("PremiumAmountToAgent") = objrowResult("PremiumAmountToAgent")
                    objRow("AdminFee") = objrowResult("AdminFee")
                    objRow("MateraiFee") = objrowResult("MateraiFee")
                    objRow("PPh") = objrowResult("PPh")
                    objRow("PPN") = objrowResult("PPN")
                    'objRow("NoNotaAsuransi") = objrowResult("NoNotaAsuransi")
                    If CType(objrowResult("AccountPayableNo"), String) <> "" Then
                        oDataTable.Rows.Add(objRow)
                    End If
                End If
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        Next
        AccountPayableChecked = Replace(AccountPayableChecked, ",", "','").Trim
        AccountPayableChecked = " Where AccountPayable.AccountPayableNo in ('" & AccountPayableChecked & "')".Trim
        Context.Trace.Write("AccountPayableChecked sesudah replace = " & AccountPayableChecked)
        Dim oCustomClass As New Parameter.SPPA
        With oCustomClass
            .ListData = oDataTable
            '.MaskAssBranchID = Me.InsCoySelect
            .MaskAssBranchID = Me.InsCoySelect + "-" + Left(JmlRecorddicheck, 3)
            .MaskAssBranchName = Me.InsCoySelectName
            .TermOfPayment = CType(Me.TermOfPayment, String)
        End With
        HttpContext.Current.Items("InsuranceBilling") = oCustomClass

        If cboProductAsuransi.SelectedItem.Value = "KB" Then
            Server.Transfer("InsuranceBillingSelect.aspx?StatusPage=" + Me.StatusPage + "")
        ElseIf cboProductAsuransi.SelectedItem.Value = "CP" Then
            Server.Transfer("InsuranceBillingSelectCP.aspx?StatusPage=" + Me.StatusPage + "")
        ElseIf cboProductAsuransi.SelectedItem.Value = "JK" Then
            Server.Transfer("InsuranceBillingSelectJK.aspx?StatusPage=" + Me.StatusPage + "")
        End If


    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim hyPolicyNo As HyperLink

        Dim lblAsset As Label
        Dim lblIns As Label
        'Dim lblBranchId As Label

        lblAsset = CType(e.Item.FindControl("lblAsset"), Label)
        lblIns = CType(e.Item.FindControl("lblIns"), Label)
        'lblBranchId = CType(dtgPaging.Items(irecord).Cells(0).FindControl("lblBranchIdIns"), Label)
        Dim m As Int32
        If e.Item.ItemIndex >= 0 Then
            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            Me.ApplicationId = lblApplicationId.Text.Trim
            hyTemp = CType(e.Item.FindControl("hyAgreementGrid"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
            '*** Policy No link
            ' "javascript:OpenWinPolicyDetail('" & Server.UrlEncode(Me.BranchID.Trim) & "','" & Me.AgreementNo & "','" & Me.AssetSequenceNo & "','" & Me.InsSequenceNo & "','" & Me.ApplicationID & "')"
            hyPolicyNo = CType(e.Item.FindControl("hypolicyNo"), HyperLink)
            'hyPolicyNo.NavigateUrl = "javascript:OpenWinViewPolicyNo('" & hyTemp.Text.Trim & "', " & Me.sesBranchId & ",'" & Me.ApplicationId.Trim & "','" & lblAsset.Text.Trim & "','" & lblIns.Text.Trim & "')"
            hyPolicyNo.NavigateUrl = "javascript:OpenWinViewPolicyNo(" & Server.UrlEncode(Me.sesBranchId.Trim) & ",'" & hyTemp.Text.Trim & " ','" & lblAsset.Text.Trim & "','" & lblIns.Text.Trim & "','" & Me.ApplicationId.Trim & "')"
            'hyPolicyNo.NavigateUrl = "javascript:OpenWinViewPolicyNo(" & Server.UrlEncode(Me.BranchIDIns.Trim) & ",'" & hyTemp.Text.Trim & " ','" & lblAsset.Text.Trim & "','" & lblIns.Text.Trim & "','" & Me.ApplicationId.Trim & "')"
        End If
    End Sub
    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = m_controller.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Description"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub
End Class

