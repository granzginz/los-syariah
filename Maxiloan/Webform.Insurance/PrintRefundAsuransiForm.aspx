﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrintRefundAsuransiForm.aspx.vb" Inherits="Maxiloan.Webform.Insurance.PrintRefundAsuransiForm" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692FBEA5521E1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ImageButton ID="imbBackReport" runat="server" ImageUrl="../../images/buttonback.gif">
        </asp:ImageButton>
    </div>
    <div>
        <CR:CrystalReportViewer ID="crvPrintRefundForm" runat="server" AutoDataBind="true"
            HasExportButton="False" />
    </div>
    </form>
</body>
</html>