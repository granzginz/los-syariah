﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class EditRenewaldetail
    Inherits Maxiloan.Webform.WebBased

#Region "Property"

    Private Property CoverageTypeMs() As DataTable
        Get
            Return CType(ViewState("CoverageTypeMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("CoverageTypeMs") = Value
        End Set
    End Property
    Private Property Coverageselection() As String
        Get
            Return CType(ViewState("Coverageselection"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Coverageselection") = Value
        End Set
    End Property

    Private Property SRCCAmount() As Double
        Get
            Return CType(ViewState("SRCCAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("SRCCAmount") = Value
        End Set
    End Property
    Private Property SRCCMs() As DataTable
        Get
            Return CType(ViewState("SRCCMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("SRCCMs") = Value
        End Set
    End Property

    Private Property TPLMs() As DataTable
        Get
            Return CType(ViewState("TPLMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TPLMs") = Value
        End Set
    End Property
    Private Property TPLAmount() As Double
        Get
            Return CType(ViewState("TPLAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("TPLAmount") = Value
        End Set
    End Property

    Private Property TPLPremi() As Double
        Get
            Return CType(ViewState("TPLPremi"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("TPLPremi") = Value
        End Set
    End Property
    Private Property LoadingFeePremi() As Double
        Get
            Return CType(ViewState("LoadingFeePremi"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("LoadingFeePremi") = Value
        End Set
    End Property

    Private Property FloodAmount() As Double
        Get
            Return CType(ViewState("FloodAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("FloodAmount") = Value
        End Set
    End Property
    Private Property FloodMs() As DataTable
        Get
            Return CType(ViewState("FloodMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("FloodMs") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return CType(ViewState("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property

    Private Property InsStartDate() As Date
        Get
            Return CType(ViewState("InsStartDate"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("InsStartDate") = Value
        End Set
    End Property

    Private Property InsEndDate() As Date
        Get
            Return CType(ViewState("InsEndDate"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("InsEndDate") = Value
        End Set
    End Property

    Private Property NewMainPremium() As Double
        Get
            Return CType(ViewState("NewMainPremium"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("NewMainPremium") = Value
        End Set
    End Property
    Private Property NewTotalPremium() As Double
        Get
            Return CType(ViewState("NewTotalPremium"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("NewTotalPremium") = Value
        End Set
    End Property
    Private Property LastTotalPremium() As Double
        Get
            Return CType(ViewState("Lasttotalpremium"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("Lasttotalpremium") = Value

        End Set
    End Property

    Private Property PremiumAmountByCust() As Double
        Get
            Return CType(ViewState("PremiumAmountByCust"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("PremiumAmountByCust") = Value
        End Set
    End Property

#End Region
#Region "Constanta"

    Private oController As New RenewalController
    Private oGeneralController As New GeneralPagingController
    Private Const FILE_REDIRECT As String = "EditRenewal.aspx"
    Dim strBranchID As String
    Dim strApplicationID As String
    Dim strAgreementNo As String

#End Region
#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            If CheckFeature(Me.Loginid, CommonVariableHelper.FORM_NAME_INS_EDIT_RENEWAL, CommonVariableHelper.FORM_FEATURE_EDIT, CommonVariableHelper.APPLICATION_NAME) Then
                strBranchID = Request.QueryString("BranchID")
                strApplicationID = Request.QueryString("ApplicationID")
                strAgreementNo = Request.QueryString("AgreementNo")
                LblAgreementNo.Text = strAgreementNo
                Me.BranchID = strBranchID
                Me.ApplicationID = strApplicationID
                Me.AgreementNo = strAgreementNo
                '===============================VIEW TOP RENEWAL AND PREVIOUS COVERAGE ========================================
                ViewTopRenewal()
                '==================================== NEW COVERAGE ================================================
                NewCoverage()
                '============================== GRID INSURANCE ===================================================
                GridInsurance()

                'Dim ThreadtopRenewal As New Thread(AddressOf ViewTopRenewal)
                'Dim ThreadNewCoverage As New Thread(AddressOf NewCoverage)
                'Dim ThreadGridInsurance As New Thread(AddressOf GridInsurance)

                'ThreadtopRenewal.Priority = ThreadPriority.AboveNormal
                'ThreadNewCoverage.Priority = ThreadPriority.AboveNormal
                'ThreadGridInsurance.Priority = ThreadPriority.AboveNormal

                'ThreadtopRenewal.Start()
                'ThreadNewCoverage.Start()
                'ThreadGridInsurance.Start()
            End If
        End If
    End Sub
#End Region
#Region "ViewTopRenewal"

    Sub ViewTopRenewal()
        '===============================VIEW TOP RENEWAL AND PREVIOUS COVERAGE ========================================

        Try


            Dim SqlCondition As New StringBuilder
            SqlCondition.Append(" And dbo.Agreement.BranchID = '" & strBranchID & "' ")
            SqlCondition.Append(" And dbo.Agreement.ApplicationID = '" & strApplicationID & "' ")
            SqlCondition.Append(" And dbo.Agreement.AgreementNo = '" & strAgreementNo & "' ")

            Me.SearchBy = SqlCondition.ToString.Trim


            Dim oEntities As New Parameter.Renewal

            With oEntities
                .strConnection = GetConnectionString()
                .WhereCond = Me.SearchBy
            End With


            oEntities = oController.GetRenewalDetailTop(oEntities)

            If Not oEntities Is Nothing Then

                With oEntities
                    LblCustomerName.Text = .CustomerName
                    LblAssetDescr.Text = .AssetMasterDescr
                    LblPolicyNo.Text = .PolicyNumber
                    LblAssetNewUsed.Text = .UsedNew
                    LblAssetYear.Text = .ManufacturingYear
                    LblInsuranceCo.Text = .InsuranceComBranchName
                    LblStartDateInsPeriod.Text = CType(.StartDate, String)
                    Me.InsStartDate = .StartDate
                    Me.InsEndDate = .EndDate
                    LblStartDateInsPeriod.Text = Me.InsStartDate.ToString("dd/MM/yyyy")
                    LblEndDateInsPeriod.Text = Me.InsEndDate.ToString("dd/MM/yyyy")
                    LblInsuranceTypeDescr.Text = .InsuranceTypeDescr
                    LblUsage.Text = .AssetUsageDescr
                    LblApplicationType.Text = .ApplicationTypeDescr
                    LblChasisNo.Text = .SerialNo1
                    LblEngineNo.Text = .SerialNo2
                    LblSumInsured.Text = FormatNumber(.SumInsured, 2, , , )
                    LblMainPremium.Text = FormatNumber(.MainPremiumToCust, 2, , , )
                    LblSRCCPremium.Text = FormatNumber(.SRCCToCust, 2, , , )
                    LblFloodPremium.Text = FormatNumber(.FloodToCust, 2, , , )
                    LblAdminFee.Text = FormatNumber(.AdminFeeToCust, 2, , , )
                    LblDiscAmountToCust.Text = FormatNumber(.DiscToCustAmount, 2, , , )
                    LblTPLPremium.Text = FormatNumber(.TPLToCust, 2, , , )
                    LblLoadingFee.Text = FormatNumber(.LoadingFeeToCust, 2, , , )
                    LblMeteraiFee.Text = FormatNumber(.MeteraiFeeToCust, 2, , , )
                    LblTotPremiumByCust.Text = FormatNumber(.PremiumAmountByCust, 2, , , )
                    Me.PremiumAmountByCust = .PremiumAmountByCust
                    LblInsNotes.Text = .InsNotes
                    LblACCNotes.Text = .AccNotes
                    btnCalculate.Visible = True
                    btnSave.Visible = True
                End With
            Else
                btnCalculate.Visible = False
                btnSave.Visible = False
            End If

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("EditRenewalDetail.aspx.vb", "ViewTopRenewal", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        End Try


    End Sub

#End Region
#Region "NewCoverage"
    Sub NewCoverage()
        '==================================== NEW COVERAGE ================================================
        Try

            Dim oCoverageRenewalEntities As New Parameter.Renewal
            With oCoverageRenewalEntities
                .WhereCond = Me.SearchBy
                .strConnection = GetConnectionString()
            End With

            Dim oRenewalController As New RenewalController
            oCoverageRenewalEntities = oRenewalController.GetRenewalNewCoverage(oCoverageRenewalEntities)

            If Not oCoverageRenewalEntities Is Nothing Then

                With oCoverageRenewalEntities
                    ' TxtNewSumInsured.Text = FormatNumber(.SumInsured, 2, , , )
                    TxtNewSumInsured.Text = CType(.SumInsured, String)
                    LblMainPremiumNewCoverage.Text = FormatNumber(.MainPremiumToCust, 2, , , )
                    LblSRCCPremiumNewCoverage.Text = FormatNumber(.SRCCToCust, 2, , , )
                    LblTPLPremiumNewCoverage.Text = FormatNumber(.TPLToCust, 2, , , )
                    LblFloodPremiumNewCoverage.Text = FormatNumber(.FloodToCust, 2, , , )
                    LblLoadingFeeNewCoverage.Text = FormatNumber(.LoadingFeeToCust, 2, , , )
                    LblNewAdminFee.Text = FormatNumber(.AdminFeeToCust, 2, , , )
                    LblNewMeteraiFee.Text = FormatNumber(.MeteraiFeeToCust, 2, , , )
                    LblNewPremiumToCust.Text = FormatNumber(.PremiumAmountByCust, 2, , , )
                    'LblNewPremiumToCust.Text = FormatNumber(Me.PremiumAmountByCust, 2, , )
                    Me.InsEndDate = .EndDate
                    LblExpiredDate.Text = Me.InsEndDate.ToString("dd/MM/yyyy")
                    TxtNewAccNotes.Text = .AccNotes
                    TxtNewInsNotes.Text = .InsNotes
                End With
                btnCalculate.Visible = True
                btnSave.Visible = True
            Else
                btnCalculate.Visible = False
                btnSave.Visible = False
            End If
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("EditRenewalDetail.aspx.vb", "NewCoverage", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        End Try

    End Sub

#End Region
#Region "GridInsurance"
    Sub GridInsurance()
        '============================== GRID INSURANCE ===================================================

        Dim oGeneralEntities As New Parameter.GeneralPaging

        With oGeneralEntities
            .SpName = "spCoverageRenewalGridIns"
            .WhereCond = Me.SearchBy
            .strConnection = GetConnectionString()
        End With

        oGeneralEntities = oGeneralController.GetReportWithParameterWhereCond(oGeneralEntities)
        Dim ds As DataSet
        If Not oGeneralEntities Is Nothing Then
            ds = oGeneralEntities.ListDataReport
        End If

        DgridInsurance.DataSource = ds
        DgridInsurance.DataBind()
    End Sub
#End Region
#Region "Data Bound untuk Grid Insurance "


    Private Sub DgridInsurance_DataBinding(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DgridInsurance.ItemDataBound
        Try

            Dim CboCoverageTypeDT As New DataTable("CoverageTypeMs")
            Dim DDLCoverageTypePilihan As DropDownList
            Dim CboBooleanSRCCDT As New DataTable("BolSRCC")
            Dim DDLBooleanSRCCPilihan As DropDownList
            Dim CboTPLPDT As New DataTable("TPLMs")
            Dim DDLTPLPilihan As New DropDownList
            Dim CboBooleanFloodDT As New DataTable("BolFlood")
            Dim DDLBooleanFloodPilihan As DropDownList
            Dim CboPaidByCustDT As New DataTable("PaidByCustMs")
            Dim DDLPaidByCustPilihan As DropDownList

            Dim LblHiddenCoverageTypeDGrid As Label
            Dim LblHiddenSRCC As Label
            Dim LblHiddenSRCCAmount As Label
            Dim LblHiddenTPL As Label
            Dim LBLHiddenTPLPremi As Label
            Dim LblHiddenFlood As Label
            Dim LblHiddenFloodAmount As Label
            Dim LblHiddenPaidByCust As Label


            If e.Item.ItemIndex > -1 Then
                '============= Handle CoverageType =============
                DDLCoverageTypePilihan = CType(e.Item.Cells(2).FindControl("DDLCoverageTypeDgrid"), DropDownList)
                LblHiddenCoverageTypeDGrid = CType(e.Item.Cells(3).FindControl("LblHiddenCoverageTypeDGrid"), Label)
                CboCoverageTypeDT = Me.CoverageTypeMs
                Dim cbodvCoverageType As DataView
                cbodvCoverageType = CboCoverageTypeDT.DefaultView
                DDLCoverageTypePilihan.DataSource = cbodvCoverageType
                DDLCoverageTypePilihan.DataTextField = "Description"
                DDLCoverageTypePilihan.DataValueField = "ID"
                DDLCoverageTypePilihan.DataBind()
                DDLCoverageTypePilihan.ClearSelection()
                DDLCoverageTypePilihan.Items.FindByValue(Trim(LblHiddenCoverageTypeDGrid.Text)).Selected = True
                Context.Trace.Write("LblHiddenCoverageTypeDGrid = " & LblHiddenCoverageTypeDGrid.Text)


                '============= Handle SRCC =============

                DDLBooleanSRCCPilihan = CType(e.Item.Cells(5).FindControl("DDLBolSRCCPilihan"), DropDownList)
                LblHiddenSRCC = CType(e.Item.Cells(6).FindControl("LblHiddenSRCCDGrid"), Label)
                CboBooleanSRCCDT = Me.SRCCMs
                LblHiddenSRCCAmount = CType(e.Item.FindControl("LblSRCCAmountDGrid"), Label)
                Me.SRCCAmount = CType(LblHiddenSRCCAmount.Text, Double)

                Dim CbodvSRCC As DataView
                CbodvSRCC = CboBooleanSRCCDT.DefaultView
                DDLBooleanSRCCPilihan.DataSource = CbodvSRCC
                DDLBooleanSRCCPilihan.DataTextField = "Description"
                DDLBooleanSRCCPilihan.DataValueField = "ID"
                DDLBooleanSRCCPilihan.DataBind()
                DDLBooleanSRCCPilihan.ClearSelection()
                Dim strBol As Int16
                If LblHiddenSRCC.Text.ToUpper.Trim = "TRUE" Then
                    strBol = 1
                Else
                    strBol = 0
                End If
                DDLBooleanSRCCPilihan.Items.FindByValue(Trim(strBol.ToString)).Selected = True
                Context.Trace.Write("LblHiddenSRCC = " & LblHiddenSRCC.Text)
                Context.Trace.Write("strBolSRCC = " & strBol)

                '================= Handle TPL =======================

                DDLTPLPilihan = CType(e.Item.Cells(3).FindControl("DDLTPLPilihan"), DropDownList)
                LblHiddenTPL = CType(e.Item.FindControl("LblHiddenTPLDgrid"), Label) ' <-- ini COST Amount TPL
                LBLHiddenTPLPremi = CType(e.Item.FindControl("LblTPLAmount"), Label) ' <-- ini Amount Premi TPL
                Me.TPLPremi = CType(LBLHiddenTPLPremi.Text, Double)
                Me.TPLAmount = CType(LblHiddenTPL.Text, Double)
                CboCoverageTypeDT = Me.TPLMs
                Dim CbodvTPL As DataView
                CbodvTPL = CboCoverageTypeDT.DefaultView
                DDLTPLPilihan.DataSource = CbodvTPL
                DDLTPLPilihan.DataTextField = "TPLAmount"
                DDLTPLPilihan.DataValueField = "TPLAmount"
                DDLTPLPilihan.DataBind()
                DDLTPLPilihan.ClearSelection()

                Context.Trace.Write("LblHiddenTPL.text.trim = " & LblHiddenTPL.Text.Trim)

                Dim strTPLPremiFormat As String

                If LblHiddenTPL.Text.Trim <> "0" Then
                    strTPLPremiFormat = Replace(LblHiddenTPL.Text.Trim, ".00", "") & ".00"
                    Context.Trace.Write("LblTPLPremiFormat dikasih .00 ")
                Else
                    strTPLPremiFormat = LblHiddenTPL.Text.Trim
                    Context.Trace.Write("LblTPLPremiFormat tidak dikasih .00 ")
                End If
                Context.Trace.Write("strTPLPremiFormat = " & strTPLPremiFormat)
                DDLTPLPilihan.Items.FindByValue(Trim(strTPLPremiFormat)).Selected = True



                '============= Handle FLOOD =============
                DDLBooleanFloodPilihan = CType(e.Item.Cells(7).FindControl("DDLFloodPilihan"), DropDownList)
                LblHiddenFlood = CType(e.Item.Cells(8).FindControl("LblHiddenFloodDGrid"), Label)
                LblHiddenFloodAmount = CType(e.Item.FindControl("LblFloodAmountDGrid"), Label)
                Me.FloodAmount = CType(LblHiddenFloodAmount.Text, Double)
                CboBooleanFloodDT = Me.FloodMs
                Dim CboDvFlood As DataView
                CboDvFlood = CboBooleanFloodDT.DefaultView
                DDLBooleanFloodPilihan.DataSource = CboDvFlood
                DDLBooleanFloodPilihan.DataTextField = "Description"
                DDLBooleanFloodPilihan.DataValueField = "ID"
                DDLBooleanFloodPilihan.DataBind()
                DDLBooleanFloodPilihan.ClearSelection()
                Dim strBol2 As Int16
                If LblHiddenFlood.Text.ToUpper.Trim = "TRUE" Then
                    strBol2 = 1
                Else
                    strBol2 = 0
                End If
                DDLBooleanFloodPilihan.Items.FindByValue(Trim(strBol2.ToString)).Selected = True
                Context.Trace.Write("strBol2Flood = " & strBol2)

                '============================ Loading Premi ============================
                Dim LblHiddenLoadingPremi As Label
                LblHiddenLoadingPremi = CType(e.Item.FindControl("LblPremiLoadingFee"), Label)
                Me.LoadingFeePremi = CType(LblHiddenLoadingPremi.Text, Double)

                '================================== Main Premium ==============================
                Dim LblHiddenMainPremium As Label
                LblHiddenMainPremium = CType(e.Item.FindControl("LblPremiumDGrid"), Label)
                Me.NewMainPremium = CType(LblHiddenMainPremium.Text, Double)

                '======================== Total Premium ===============
                Dim LblHiddenTotalPremium As Label
                LblHiddenTotalPremium = CType(e.Item.FindControl("LblNewTotalPremium"), Label)
                Me.NewTotalPremium = CType(LblHiddenTotalPremium.Text, Double)


            Else

                '============= Handle CoverageType =============

                Dim strsqlQuery As String
                strsqlQuery = CommonVariableHelper.SQL_QUERY_COVERAGE_TYPEMS
                Dim objAdapter As New SqlDataAdapter(strsqlQuery, GetConnectionString)
                objAdapter.Fill(CboCoverageTypeDT)
                Me.CoverageTypeMs = CboCoverageTypeDT

                '============= Handle SRCC =============
                Dim strsqlQuerySRCC As String
                Dim SBuilder As New StringBuilder
                SBuilder.Append("create table #TempTable (")
                SBuilder.Append("ID  int ,")
                SBuilder.Append("Description char(8) ) ")
                SBuilder.Append("EXEC ('")
                SBuilder.Append("INSERT INTO #TEMPTABLE Select ''1'' as ID ,''Yes'' as Description ")
                SBuilder.Append("INSERT INTO #TEMPTABLE Select ''0'' as ID ,''No'' as Description ")
                SBuilder.Append("Select ID,Description From #TempTable ')")
                strsqlQuerySRCC = SBuilder.ToString
                Dim objAdapterSRCC As New SqlDataAdapter(strsqlQuerySRCC, GetConnectionString)
                objAdapterSRCC.Fill(CboBooleanSRCCDT)
                Me.SRCCMs = CboBooleanSRCCDT


                '============= Handle TPL =============
                Dim strQueryTPL As String
                strQueryTPL = CommonVariableHelper.SQL_QUERY_TPL & " Where BranchID= '" & Me.BranchID & "'"
                Dim ObjAdapterTPL As New SqlDataAdapter(strQueryTPL, GetConnectionString)
                ObjAdapterTPL.Fill(CboTPLPDT)
                Me.TPLMs = CboTPLPDT

                '============= Handle Flood =============
                Dim strsqlQueryFlood As String
                Dim SBuilder2 As New StringBuilder
                SBuilder2.Append("create table #TempTable (")
                SBuilder2.Append("ID  int ,")
                SBuilder2.Append("Description char(8) ) ")
                SBuilder2.Append("EXEC ('")
                SBuilder2.Append("INSERT INTO #TEMPTABLE Select ''1'' as ID ,''Yes'' as Description ")
                SBuilder2.Append("INSERT INTO #TEMPTABLE Select ''0'' as ID ,''No'' as Description ")
                SBuilder2.Append("Select ID,Description From #TempTable ')")
                strsqlQueryFlood = SBuilder2.ToString
                Dim objAdapterFlood As New SqlDataAdapter(strsqlQueryFlood, GetConnectionString)
                objAdapterFlood.Fill(CboBooleanFloodDT)
                Me.FloodMs = CboBooleanFloodDT

            End If

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("", "", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Response.Write(ex.StackTrace)
            Response.Write(ex.Message)
        End Try


    End Sub


#End Region
#Region "Ketika Tombol Cancel Diclik"


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(FILE_REDIRECT)
    End Sub

#End Region
#Region "Ketika Tombol Save Ditekan !"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            LblErrorMessages.Text = ""
            GettingCalculate()
            Context.Trace.Write("New Total Main Premium = " & Me.NewTotalPremium)
            Context.Trace.Write("Last Total Main Premium = " & Me.LastTotalPremium)
            If Me.LastTotalPremium = Me.NewTotalPremium Then
                Response.Write("Boleh Disave !")
                Dim osaveController As New RenewalController
                Dim oEntitiesSave As New Parameter.Renewal

                With oEntitiesSave
                    .BranchId = Me.BranchID
                    .AgreementNo = Me.AgreementNo
                    .SumInsured = CType(TxtNewSumInsured.Text, Double)
                    .InsNotes = TxtNewInsNotes.Text
                    .AccNotes = TxtNewAccNotes.Text
                    .CoverageType = Me.Coverageselection
                    .MainPremiumToCust = Me.NewMainPremium
                    .SRCCToCust = Me.SRCCAmount
                    .FloodToCust = Me.FloodAmount
                    .TPLPremium = Me.TPLPremi
                    .LoadingFeeToCust = Me.LoadingFeePremi
                    .PremiumAmountByCust = Me.NewTotalPremium
                    .BusinessDate = Me.BusinessDate
                    .strConnection = GetConnectionString()
                    .TPLToCust = Me.TPLAmount
                End With
                Context.Trace.Write("Sum Insured = " & CType(TxtNewSumInsured.Text, Double))
                Context.Trace.Write("CoverageType = " & Me.Coverageselection)
                osaveController.CalculateAndSaveRenewal(oEntitiesSave)
                Response.Redirect("EditRenewal.aspx")
            Else
                Dim errmes As New StringBuilder
                errmes.Append("Total Premi Customer Berbeda dengan Kalkulasi Terakhir")
                errmes.Append(", Kalkulasi Terakhir adalah " & FormatNumber(Me.LastTotalPremium, 2, , , ) & " Sedangkan Premi Baru adalah  " & FormatNumber(Me.NewTotalPremium, 2, , , ))
                errmes.Append(" Harap Cek Nilai Pertanggungan dan Jenis Cover, Kemudian Klik Tombol calculate ")
                LblErrorMessages.Text = errmes.ToString
            End If

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("", "", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Response.Write(ex.Message)
            Response.Write(ex.StackTrace)
        End Try

    End Sub

#End Region
#Region "Ketika Tombol Calculate ditekan !"

    Private Sub btnCalculate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalculate.Click
        LblErrorMessages.Text = ""
        GettingCalculate()
        Me.LastTotalPremium = Me.NewTotalPremium
    End Sub

#End Region
#Region "GettingCalculate"
    Public Sub GettingCalculate()

        Try

            '====================================================================================

            Dim intGrid As Int16 = CType(DgridInsurance.Items.Count - 1, Int16)
            Dim oLoop As Int16
            Dim CoverageTpye As DropDownList
            Dim SRCC As DropDownList
            Dim bolSRCC As Boolean
            Dim TPLAmount As DropDownList
            Dim Flood As DropDownList
            Dim bolFlood As Boolean

            For oLoop = 0 To CType(intGrid, Int16)
                CoverageTpye = CType(DgridInsurance.Items(oLoop).FindControl("DDLCoverageTypeDgrid"), DropDownList)
                SRCC = CType(DgridInsurance.Items(oLoop).FindControl("DDLBolSRCCPilihan"), DropDownList)
                TPLAmount = CType(DgridInsurance.Items(oLoop).FindControl("DDLTPLPilihan"), DropDownList)
                Flood = CType(DgridInsurance.Items(oLoop).FindControl("DDLFloodPilihan"), DropDownList)
            Next

            Context.Trace.Write("CoverageType = " + CoverageTpye.SelectedItem.Value.Trim)
            Context.Trace.Write("SRCC = " + SRCC.SelectedItem.Value.Trim)
            Context.Trace.Write("Flood = " + Flood.SelectedItem.Value.Trim)
            Context.Trace.Write("TPLAmount = " + TPLAmount.SelectedItem.Value.Trim)


            If SRCC.SelectedItem.Value.Trim = "1" Then
                bolSRCC = True
            Else
                bolSRCC = False
            End If

            If Flood.SelectedItem.Value.Trim = "1" Then
                bolFlood = True
            Else
                bolFlood = False
            End If

            '============================== GRID INSURANCE KETIKA DI CALCULATE ===================================================

            Dim oGeneralEntities As New Parameter.Renewal

            With oGeneralEntities
                .BranchId = Me.BranchID
                .AgreementNo = Me.AgreementNo
                .SumInsured = CType(TxtNewSumInsured.Text, Double)
                .CoverageType = CoverageTpye.SelectedItem.Value.Trim
                .BolSRCC = bolSRCC
                .TPLToCust = CType(TPLAmount.SelectedItem.Value.Trim, Double)
                .BolFlood = bolFlood
                .BusinessDate = Me.BusinessDate
                .strConnection = GetConnectionString()
                Me.Coverageselection = CoverageTpye.SelectedItem.Value.Trim

            End With

            Context.Trace.Write("Mode_Calculate_DGRID -> BranchID = " + Me.BranchID)
            Context.Trace.Write("Mode_Calculate_DGRID -> AgreementNo = " + Me.AgreementNo)
            Context.Trace.Write("Mode_Calculate_DGRID -> SumInsured = " + TxtNewSumInsured.Text)
            Context.Trace.Write("Mode_Calculate_DGRID -> CoverageType = " + CoverageTpye.SelectedItem.Value.Trim)
            Context.Trace.Write("Mode_Calculate_DGRID -> BolSRCC = " + CType(bolSRCC, String))
            Context.Trace.Write("Mode_Calculate_DGRID -> TPLToCust = " + CType(TPLAmount.SelectedItem.Value.Trim, String))
            Context.Trace.Write("Mode_Calculate_DGRID -> BolFlood = " + CType(bolFlood, String))
            Context.Trace.Write("Mode_Calculate_DGRID -> BusinessDate =  " + Me.BusinessDate.ToString("dd/MM/yyyy"))
            Context.Trace.Write("Mode_Calculate_DGRID -> strConnection =  " + GetConnectionString())


            Dim oCalcControlle As New RenewalController
            oGeneralEntities = oCalcControlle.GetCoverageRenewalGridInsCALCULATE(oGeneralEntities)
            Dim ds As DataSet
            If Not oGeneralEntities Is Nothing Then
                ds = oGeneralEntities.ListDataSet
            End If

            DgridInsurance.DataSource = ds
            DgridInsurance.DataBind()

            '============================================================================================

            LblNewPremiumToCust.Text = FormatNumber(Me.NewTotalPremium, 2, , , )
            LblMainPremiumNewCoverage.Text = FormatNumber(Me.NewMainPremium, 2, , , )
            LblSRCCPremiumNewCoverage.Text = FormatNumber(Me.SRCCAmount, 2, , , )
            LblTPLPremiumNewCoverage.Text = FormatNumber(Me.TPLPremi, 2, , , )
            LblFloodPremiumNewCoverage.Text = FormatNumber(Me.FloodAmount, 2, , , )
            LblLoadingFeeNewCoverage.Text = FormatNumber(Me.LoadingFeePremi, 2, , , )


        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            Response.Write(ex.Message)
            Response.Write(ex.StackTrace)
            err.WriteLog("EditRenewalDetail.aspx", "GettingCalculate", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        End Try


    End Sub
#End Region

End Class