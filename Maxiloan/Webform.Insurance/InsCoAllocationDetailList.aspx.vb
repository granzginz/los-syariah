﻿#Region "Imports"
Imports System.IO
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonSimpleRuleHelper
#End Region

Public Class InsCoAllocationDetailList
    Inherits Maxiloan.Webform.WebBased

#Region "Property"

    Private Property FlagInsActivation() As String
        Get
            Return (CType(viewstate("FlagInsActivation"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("FlagInsActivation") = Value
        End Set
    End Property

    Private Property MaxYearNum() As Int16
        Get
            Return (CType(viewstate("MaxYearNum"), Int16))
        End Get
        Set(ByVal Value As Int16)
            viewstate("MaxYearNum") = Value
        End Set
    End Property


    Private Property MaturityDate() As String
        Get
            Return (CType(viewstate("MaturityDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("MaturityDate") = Value
        End Set
    End Property

    Private Property StartDate() As String
        Get
            Return (CType(viewstate("StartDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("StartDate") = Value
        End Set
    End Property
    Private Property EndDateEntry() As String
        Get
            Return (CType(viewstate("EndDateEntry"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("EndDateEntry") = Value
        End Set
    End Property

    Private Property StartDateRenewal() As String
        Get
            Return (CType(viewstate("StartDateRenewal"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("StartDateRenewal") = Value
        End Set
    End Property

    Private Property MaskAssID() As String
        Get
            Return (CType(viewstate("MaskAssID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("MaskAssID") = Value
        End Set
    End Property

    Private Property InsuranceComBranchID() As String
        Get
            Return (CType(viewstate("InsuranceComBranchID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("InsuranceComBranchID") = Value
        End Set
    End Property


    Private Property AgreementNo() As String
        Get
            Return (CType(viewstate("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return (CType(viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Private Property CoverPeriod() As String
        Get
            Return (CType(viewstate("CoverPeriod"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("CoverPeriod") = Value
        End Set
    End Property

#End Region
#Region "Constanta"
    Private oController As New InsCoAllocationDetailListController
    Protected WithEvents oInsuranceBranchName As UcInsuranceBranchName
#End Region
#Region "InitialDEfaultPanel"

    Public Sub InitialDefaulPanel()
        PnlGrid.Visible = False

    End Sub

#End Region

    Function LinkToAgreement(ByVal strStyle As String, ByVal strApplicationId As String) As String
        Return "javascript:OpenAgreementNo('" & strStyle & "', '" & strApplicationId & "')"
    End Function
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function

#Region "Page Load"


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then

            Dim DataTdkLengkap As Boolean = False
            InitialDefaulPanel()
            Me.AgreementNo = Request.QueryString("AgreementNo")
            Me.ApplicationID = Request.QueryString("ApplicationId")
            Me.BranchID = Request.QueryString("BranchId")
            LblAgreementNo.Text = Me.AgreementNo

            Dim oInsCoAllocationDetailList As New Parameter.InsCoAllocationDetailList

            With oInsCoAllocationDetailList
                .AgreementNo = Me.AgreementNo
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.BranchID
                HplinkAgreementNo.Text = Me.AgreementNo
                HplinkAgreementNo.NavigateUrl = LinkToAgreement("Insurance", Me.ApplicationID)
                .strConnection = GetConnectionString

            End With

            Try
                oInsCoAllocationDetailList = oController.GetListByApplicationId(oInsCoAllocationDetailList)


                With oInsCoAllocationDetailList
                    lblTenor.Text = CType(.Tenor, String)
                    lblAssetYear.Text = .AssetYear
                    LblCustomerName.Text = .CustomerName
                    HpLinkCustName.Text = .CustomerName
                    HpLinkCustName.NavigateUrl = LinkToCustomer(.CustomerID, "Insurance")
                    LblInsuranceAssetType.Text = .AssetTypeDescr
                    LblAssetNewUSed.Text = .UsedNew
                    LblAssetUsage.Text = .AssetUsageDescr
                    LblTotalStandardPremium.Text = FormatNumber(CType(.TotalStdPremium, String), 2, , , )
                    LblGoLiveDate.Text = .GoLiveDate.ToString("dd/MM/yyyy")
                    Me.StartDateRenewal = .StartDateRenewal.ToString("dd/MM/yyyy")
                    LblAdminFee.Text = FormatNumber(CType(.AdminFeeToCust, String), 2, , , )
                    LblStampDutyFee.Text = FormatNumber(CType(.MeteraiFeeToCust, String), 2, , , )
                    LblAmountCoverage.Text = FormatNumber(CType(.AmountCoverage, String), 2, , , )

                    LblTotalPremiumByCust.Text = FormatNumber(.PremiumAmountByCust, 2, , , )

                    LblLoadingFee.Text = FormatNumber(CType(.LoadingFeeToCust, String), 2, , , )
                    LblApplicationIDHidden.Text = .ApplicationID
                    LblBranchIDHidden.Text = .BranchId
                    LBlDataExist.Text = CType(.DataExist, String)
                    LblInsuredByDescr.Text = .InsuredByDescr
                    LblPaidBy.Text = .PaidByDescr
                    LblFlagInsActivationHidden.Text = .FlagInsActivation

                    Me.MaturityDate = .MaturityDate.ToString("dd/MM/yyyy")
                    Me.MaxYearNum = .MaxYearNum
                    LblMaturityDate.Text = Me.MaturityDate

                    Me.FlagInsActivation = .FlagInsActivation
                    Me.ApplicationID = LblApplicationIDHidden.Text
                    Me.BranchID = LblBranchIDHidden.Text

                    If .OutPutCoverPeriodSelection = True Then
                        DDLCoverPeriod.Enabled = False
                    Else
                        DDLCoverPeriod.Enabled = True
                    End If

                    Context.Trace.Write("Me.MaturityDate = " & Me.MaturityDate)
                    Context.Trace.Write("Me.MaxYearNum = " & Me.MaxYearNum)
                    Context.Trace.Write("Me.StartDateRenewal = " & Me.StartDateRenewal)

                    txtodate.Text = .GoLiveDate.ToString("dd/MM/yyyy")
                End With

                btnCancelAtas.Visible = True

                ''================== tentukan apakah odate
                Context.Trace.Write("Me.FlagInsActivation = " & Me.FlagInsActivation)
                txtodate.ReadOnly = True
                If Me.FlagInsActivation = "R" Then
                    txtodate.Text = Me.StartDateRenewal
                    Context.Trace.Write("Bila Me.FlagInsActivation = 'R'")
                    Context.Trace.Write(" txtoDate.readonly=true")
                    Context.Trace.Write("txtoDate.text = " & txtodate.Text)
                    DDLCoverPeriod.SelectedIndex = 1

                Else

                    Context.Trace.Write("Bila Me.FlagInsActivation Bukan 'R'")
                    Context.Trace.Write(" txtoDate.Enable()")
                End If

            Catch ex As Exception
                ' ex.WriteLog(ex.Message, "MAXILOAN", "ModuleInsurance")
                Dim err As New MaxiloanExceptions
                err.WriteLog("InsCoAllocationDetailList", "Page_Load", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
                'LblErrorMessages.Text = MessageHelper.MESSAGE_DATA_DETAIL_NOT_FOUND
                LblErrorMessages.Text = ex.Source + ex.TargetSite.Name + ex.Message + ex.StackTrace
                btnOkAtas.Visible = False
            End Try


        End If


    End Sub
#End Region
#Region "Cancel dibagian paling bawah"

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("InsCoAllocation.aspx")

    End Sub

#End Region
#Region "Ketika Tombol Ok Dipijit"

    Private Sub btnOkAtas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOkAtas.Click
        PnlGrid.Visible = True
        Context.Trace.Write("Ketika Tombol Ok Dipijit !")
        Context.Trace.Write("Me.StartDate = " & Me.StartDate)
        Me.StartDate = txtodate.Text
        Context.Trace.Write("Check Apakah Me.StartDate Kosong ?")
        If Me.StartDate = "" Then
            Context.Trace.Write("Kalau Kosong, tampil messages")
            LblMessageForDate.Text = "Harap isi Tanggal"
            Exit Sub
        End If


        Context.Trace.Write("Me.StartDate = " & Me.StartDate)
        Context.Trace.Write("Ambil Tenor")
        '============== Ambil Tenor ========================
        Dim oInsCoAllocationDetailList As New Parameter.InsCoAllocationDetailList
        With oInsCoAllocationDetailList
            .ApplicationID = Me.ApplicationID
            .strConnection = GetConnectionString()
        End With

        Try
            oInsCoAllocationDetailList = oController.GetGridTenor(oInsCoAllocationDetailList)
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("InscoAllocatationDetailList.aspx", "ImbOk_Click-getgridtenor", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Response.Write(ex.StackTrace)
        End Try

        Dim dtEntity As New DataTable

        If Not oInsCoAllocationDetailList Is Nothing Then
            dtEntity = oInsCoAllocationDetailList.ListData
        End If

        DGridTenor.DataSource = dtEntity
        DGridTenor.DataBind()

        Context.Trace.Write("Ambil GRID INSCO PREMIUM ")
        '===== Ambil GRID INSCO PREMIUM =====
        Me.CoverPeriod = DDLCoverPeriod.SelectedItem.Value

        Dim oInsCoAllocationInsCO As New Parameter.InsCoAllocationDetailList

        With oInsCoAllocationInsCO
            .ApplicationID = Me.ApplicationID
            .CoverPeriodSelection = Me.CoverPeriod
            .BranchId = Me.BranchID
            .strConnection = GetConnectionString()
        End With

        Try
            oInsCoAllocationInsCO = oController.GetGridInsCoPremium(oInsCoAllocationInsCO)
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("InscoAllocatationDetailList.aspx", "ImbOk_Click-getgridinscopermium", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Response.Write(ex.StackTrace)

        End Try

        Dim dtEntityInsCO As New DataTable

        If Not oInsCoAllocationInsCO Is Nothing Then
            dtEntityInsCO = oInsCoAllocationInsCO.ListData
        End If

        DGridInsurance.DataSource = dtEntityInsCO
        DGridInsurance.DataBind()

        '=============== AMBIL INSURANCE STATISTIC QUOTA =============
        Context.Trace.Write("Ambil INSURANCE STATISTIC QUOTA ")
        Dim oInsquota As New Parameter.InsCoAllocationDetailList

        Dim bulan As Integer = CType(Month(Me.BusinessDate), Integer)
        Dim tahun As Integer = CType(Year(Me.BusinessDate), Integer)
        With oInsquota
            .StatMonth = CType(bulan, Int16)
            .StatYear = CType(tahun, Int16)
            .strConnection = GetConnectionString()
            .BranchId = Me.sesBranchId.Replace("'", "")
        End With
        Try
            oInsquota = oController.GetGridInsuranceStatistic(oInsquota)
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("InscoAllocatationDetailList.aspx", "ImbOk_Click-GetGridInsuranceStatistic", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Response.Write(ex.StackTrace)
        End Try

        Dim dtEntityInsuranceStatistic As New DataTable

        If Not oInsquota Is Nothing Then
            dtEntityInsuranceStatistic = oInsquota.ListData
        End If

        DgridQuota.DataSource = dtEntityInsuranceStatistic
        DgridQuota.DataBind()

        Dim ProcessDefaultEndDate As Date
        Dim StartDateMMDDYYY As String
        Dim Proc As New CommonSimpleRuleHelper
        StartDateMMDDYYY = Proc.ConvertDateReturnAsString(Me.StartDate)

        Context.Trace.Write("Me.MaxYearNum = " & Me.MaxYearNum)
        Context.Trace.Write("StartDateMMDDYYY = " & StartDateMMDDYYY)

        'ProcessDefaultEndDate = DateAdd(DateInterval.Year, Me.MaxYearNum, CType(StartDateMMDDYYY, Date))
        ProcessDefaultEndDate = DateAdd(DateInterval.Month, CDbl(lblTenor.Text.Trim), CType(StartDateMMDDYYY, Date))
        Context.Trace.Write(" ProcessDefaultEndDate = " & ProcessDefaultEndDate)

        txtEndDate.Text = CType(ProcessDefaultEndDate, String)
        Context.Trace.Write("txtEndDate.text = " & txtEndDate.Text)

        Dim MaturityDateMMDDYYYY As String
        MaturityDateMMDDYYYY = Proc.ConvertDateReturnAsString(Me.MaturityDate)

        Context.Trace.Write("MaturityDateMMDDYYYY = " & MaturityDateMMDDYYYY)
        Context.Trace.Write("ProcessDefaultEndDate= " & ProcessDefaultEndDate)
        Context.Trace.Write("Apakah Maturity Date > dari txtendDate.text ?")
        Context.Trace.Write("Apakah " & MaturityDateMMDDYYYY & " > dari " & txtEndDate.Text & " ?")

        If CType(MaturityDateMMDDYYYY, Date) > CType(ProcessDefaultEndDate, Date) Then
            txtEndDate.Text = CType(Me.MaturityDate, String)
            Context.Trace.Write("Iya ....Maturity Date > dari txtendDate.text")
        Else

            Context.Trace.Write("Tidak !! ....Maturity Date < dari txtendDate.text")
            Context.Trace.Write("CType(ProcessDefaultEndDate, String) = " & CType(ProcessDefaultEndDate, String))
            txtEndDate.Text = Proc.ConvertDateReturnAsString(CType(ProcessDefaultEndDate, String))

        End If
        Context.Trace.Write("CType(Me.MaturityDate, String) = " & CType(Me.MaturityDate, String))
        Context.Trace.Write("Proc.ConvertDateMMDDYYY(CType(ProcessDefaultEndDate, String)) =" & Proc.ConvertDateReturnAsString(CType(ProcessDefaultEndDate, String)))
        Context.Trace.Write("txtEndDate.text =" & txtEndDate.Text)

        oInsuranceBranchName.loadInsBranch(False)
        oInsuranceBranchName.FillRequired = True

        Me.EndDateEntry = txtEndDate.Text
        Context.Trace.Write("Me.EndDateEntry = " & Me.EndDateEntry)

    End Sub
#End Region
#Region "Ketika Tombol Save Dipijit "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If SessionInvalid() Then
            Exit Sub
        End If

        Context.Trace.Write("Ketika Tombol Save Dipijit !")
        Me.InsuranceComBranchID = oInsuranceBranchName.SelectedValue

        If Me.EndDateEntry = "" Then
            LblMessageEndDate.Text = "Harap isi Tanggal Sampai"
            Exit Sub
        End If


        '==================== Grid Tenor ==================================

        '========================================= Grid Insco Premium ===========================
        Context.Trace.Write("Grid Insco Premium  !")
        Dim LoopGridInsco As Int16

        For LoopGridInsco = 0 To CType(DGridInsurance.Items.Count - 1, Int16)
            Context.Trace.Write("Awal LoopGridInsco =" & LoopGridInsco)

            Dim LblInsuranceComBranchID As Label
            LblInsuranceComBranchID = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridInsuranceComBranchID"), Label)
            Dim LblGrandTotal As Label
            LblGrandTotal = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridGrandTotal"), Label)

            If Me.InsuranceComBranchID.Trim.ToLower = CType(LblInsuranceComBranchID.Text, String).Trim.ToLower Then
                Dim LblMainPremiumSave As Label
                LblMainPremiumSave = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridMainPremium"), Label)
                Dim LblSRCCPremiumSave As Label
                LblSRCCPremiumSave = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridSRCCPremium"), Label)
                Dim LblTPLPremiumSave As Label
                LblTPLPremiumSave = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridTPLPremium"), Label)

                Dim LblFloodPremiumSave As Label
                LblFloodPremiumSave = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridFloodPremium"), Label)

                Dim LblLoadingFeeSave As Label
                LblLoadingFeeSave = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridLoadingFee"), Label)

                Dim LblAdminFeeSave As Label
                LblAdminFeeSave = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridAdminFee"), Label)

                Dim LblMeteraiFeeSave As Label
                LblMeteraiFeeSave = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridMeteraiFee"), Label)

                Dim LblGrandTotalSave As Label
                LblGrandTotalSave = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridGrandTotal"), Label)

                LblPremiumInsCo.Text = CType(LblGrandTotal.Text, String)


                '====Proses Save dilakukan !!!! ==================
                Dim oSaveInsuranceCompanySelection As New Parameter.InsCoAllocationDetailList
                Dim oController As New InsCoAllocationDetailListController

                'Me.EndDateEntry di-update karena ada kemungkinan user merubah default EndDate
                Me.EndDateEntry = txtEndDate.Text

                With oSaveInsuranceCompanySelection
                    .BranchId = Me.BranchID
                    .ApplicationID = Me.ApplicationID
                    .StartDate = ConvertDate2(Me.StartDate)
                    .EndDate = ConvertDate2(Me.EndDateEntry)
                    .InsuranceComBranchID = Me.InsuranceComBranchID
                    .MainPremiumToInsco = CType(LblMainPremiumSave.Text, Double)
                    .RSCCToInsco = CType(LblSRCCPremiumSave.Text, Double)
                    .TPLToInsco = CType(LblTPLPremiumSave.Text, Double)
                    .FloodToInsco = CType(LblFloodPremiumSave.Text, Double)
                    .LoadingFeeToInsco = CType(LblLoadingFeeSave.Text, Double)
                    .TotalPremiumToInsco = CType(LblGrandTotalSave.Text, Double)
                    .AdminFee = CType(LblAdminFeeSave.Text, Double)
                    .MeteraiFee = CType(LblMeteraiFeeSave.Text, Double)
                    .StatMonth = CType(Me.BusinessDate.Month, Int16)
                    .StatYear = CType(Me.BusinessDate.Year, Int16)
                    .BusinessDate = Me.BusinessDate
                    .CoyID = Me.SesCompanyID
                    .CoverPeriodSelection = Me.CoverPeriod
                    .strConnection = GetConnectionString()

                    Context.Trace.Write("BranchId = " & .BranchId)
                    Context.Trace.Write("ApplicationID = " & .ApplicationID)
                    Context.Trace.Write("StartDate = " & .StartDate)
                    Context.Trace.Write("EndDate = " & .EndDate)
                    Context.Trace.Write("InsuranceComBranchID = " & .InsuranceComBranchID)
                    Context.Trace.Write("MainPremiumToInsco = " & .MainPremiumToInsco)
                    Context.Trace.Write("RSCCToInsco = " & .RSCCToInsco)
                    Context.Trace.Write("TPLToInsco = " & .TPLToInsco)
                    Context.Trace.Write("FloodToInsco = " & .FloodToInsco)
                    Context.Trace.Write("LoadingFeeToInsco = " & .LoadingFeeToInsco)
                    Context.Trace.Write("TotalPremiumToInsco = " & .TotalPremiumToInsco)
                    Context.Trace.Write("AdminFee = " & .AdminFee)
                    Context.Trace.Write("MeteraiFee = " & .MeteraiFee)
                    Context.Trace.Write("StatMonth = " & .StatMonth)
                    Context.Trace.Write("StatYear = " & .StatYear)
                    Context.Trace.Write("BusinessDate = " & .BusinessDate)
                    Context.Trace.Write("CoyID = " & .CoyID)
                    Context.Trace.Write("CoverPeriodSelection = " & .CoverPeriodSelection)

                End With
                Context.Trace.Write("Akhir LoopGridInsco =" & LoopGridInsco)
                Dim errMessages As String = ""
                'sementara ini diremark dulu ! Kamis 20 Nov 2003
                errMessages = oController.SaveInsuranceCompanySelectionLastProcess(oSaveInsuranceCompanySelection)
                If errMessages = "2" Then
                    errMessages = "Simpan Data Berhasil"
                End If
                Response.Redirect("InsCoAllocation.aspx?errMessages=" & errMessages & "")

            End If
        Next

    End Sub
#End Region
#Region "Tombol Cancel diatas"

    Private Sub btnCancelAtas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelAtas.Click
        Response.Redirect("InsCoAllocation.aspx")
    End Sub

#End Region

End Class