﻿#Region "Imports"
Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class EditRenewal
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Protected WithEvents oSearchBy As UcSearchByWithNoTable
    Dim currentPage As Integer = CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Dim PageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    Dim currentPageNumber As Int32 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Dim totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Dim recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT
    Private Const SP_NAME_PAGING As String = "spPagingInsEditCoverageForRenewal"
    Private m_controller As New GeneralPagingController

#End Region
#Region "Property"
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
#End Region
#Region "Page Load"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            InitialDefaultPanel()
            LblBranchName.Text = Me.BranchName
            oSearchBy.ListData = "dbo.Agreement.AgreementNo, Agreement NO-dbo.Customer.Name , Customer Name"
            oSearchBy.BindData()
        End If

    End Sub

#End Region
#Region "InitialDefaultPanel"

    Public Sub InitialDefaultPanel()
        PnlGrid.Visible = False
    End Sub

#End Region
#Region "Search"
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        PnlGrid.Visible = True
        Me.SearchBy = " AND InsuranceAsset.BranchID = '" & Replace(Me.sesBranchId, "'", "") & "' "
        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%' "
        End If
        Context.Trace.Write("Var Search By = " + Me.SearchBy)
        BindGridEntity(Me.SearchBy)

    End Sub

#End Region
#Region "Bind Grid"


    Sub BindGridEntity(ByVal cmdWhere As String)
        Try

            Dim dtEntity As New DataTable
            Dim oGeneralPaging As New Parameter.GeneralPaging
            If Me.Sort = "" Then Me.Sort = " InsuranceAsset.ApplicationID "
            With oGeneralPaging
                .SpName = SP_NAME_PAGING
                .WhereCond = cmdWhere
                .CurrentPage = currentPage
                .PageSize = PageSize
                .SortBy = Me.Sort
                .strConnection = GetConnectionString
            End With


            oGeneralPaging = m_controller.GetGeneralPaging(oGeneralPaging)

            If Not oGeneralPaging Is Nothing Then
                dtEntity = oGeneralPaging.ListData
                recordCount = oGeneralPaging.TotalRecords
            Else
                recordCount = 0
            End If

            DGrid.DataSource = dtEntity.DefaultView
            DGrid.CurrentPageIndex = 0
            DGrid.DataBind()
            PagingFooter()

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            Response.Write(ex.Message)
            err.WriteLog("EditRenewal.aspx.vb", "BindGridEntitiy", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        End Try



    End Sub
    Private Sub DGrid_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DGrid.ItemDataBound
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim hyPolicyNo As HyperLink
        Dim m As Int32
        If e.Item.ItemIndex >= 0 Then
            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
            '*** Policy No link
            hyPolicyNo = CType(e.Item.FindControl("hypolicyNo"), HyperLink)
            hyPolicyNo.NavigateUrl = "javascript:OpenWinViewPolicyNo('" & hyTemp.Text.Trim & "', " & Me.sesBranchId & ")"
        End If
    End Sub


#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(PageSize, Integer)), Double))
        If totalPages = 0 Then
            'ShowMessage(lblMessage, "Data tidak ditemukan!", true)
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            ' rgvGo.MaximumValue = CType(totalPages, String)

        End If

        lblTotRec.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select


        BindGridEntity(Me.SearchBy)
    End Sub
    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int16)
                BindGridEntity(Me.SearchBy)
            End If
        End If
    End Sub
#End Region
#Region "Sorting"


    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles DGrid.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy)
    End Sub
#End Region
#Region "Reset"

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Me.SearchBy = ""
        BindGridEntity(Me.SearchBy)
    End Sub
#End Region


End Class