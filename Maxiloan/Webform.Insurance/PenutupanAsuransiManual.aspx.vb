﻿
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class PenutupanAsuransiManual
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents GridNavigator As ucGridNav
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
    Private oInsCoAllocationDetailListController As New InsCoAllocationDetailListController




    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return (CType(ViewState("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property

    Private Property MaskAssBranchID() As String
        Get
            Return (CType(ViewState("MaskAssBranchID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("MaskAssBranchID") = Value
        End Set
    End Property

    Protected WithEvents txtNilaiAksesoris As ucNumberFormat
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If 
            Me.FormID = "TTPASURANSIMNL"
            oSearchBy.ListData = "Name, Nama Konsumen-Agreementno, No Kontrak "
            oSearchBy.BindData()

            pnlPenutupan.Visible = False 
            pnlList.Visible = True

            Dim dtbranch As New DataTable

            dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId.Replace("'", "").Trim)
            With cbobranch
                .DataTextField = "Name"
                .DataValueField = "ID"
                .DataSource = dtbranch
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            End With
            cbobranch.Items.FindByValue(Me.sesBranchId.Replace("'", "").Trim).Selected = True

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If


        End If 
    End Sub
    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView


        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spPenutupanAsuransi"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)

        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If

        DtgAsuransi.DataSource = DtUserList.DefaultView
        DtgAsuransi.CurrentPageIndex = 0
        DtgAsuransi.DataBind()

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        pnlList.Visible = True
        pnlDatagrid.Visible = True

    End Sub

    Private Sub DtgAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAsuransi.ItemDataBound
        If SessionInvalid() Then
            Exit Sub
        End If

        If e.Item.ItemIndex >= 0 Then
            'Dim branch = CType(e.Item.FindControl("hdnBranch"), Label).Text
            Dim appID = CType(e.Item.FindControl("hdnAppId"), Label).Text
            Dim custID = CType(e.Item.FindControl("hdnCustomerId"), Label).Text  
            CType(e.Item.FindControl("hyAgreementNo"), HyperLink).NavigateUrl =
                "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(appID.Trim) & "')" 

            CType(e.Item.FindControl("hyCustomerName"), HyperLink).NavigateUrl =
                "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(custID.Trim) & "')"
 
        End If
    End Sub

    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        Me.SearchBy = String.Format(" a.branchid = '{0}' ", cbobranch.SelectedItem.Value.Trim)

        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''").Replace("%", "") & "%'"
        End If
        pnlDatagrid.Visible = True
        DtgAsuransi.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)

    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub


    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("PenutupanAsuransiManual.aspx")
    End Sub

    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAsuransi.ItemCommand 
        If e.CommandName = "Select" Then
            ApplicationID = CType(e.Item.FindControl("hdnAppId"), Label).Text
            MaskAssBranchID = CType(e.Item.FindControl("hdnMaskAssBranchID"), Label).Text

            pnlPenutupan.Visible = True
            pnlDatagrid.Visible = False
            pnlList.Visible = False
            ShowEntryData()
            DisplayGridInsco()
        End If
    End Sub


    Sub ShowEntryData()
        Dim oController As New NewAppInsuranceByCompanyController
        Dim oNewAppInsuranceByCompany As New Parameter.NewAppInsuranceByCompany

        With oNewAppInsuranceByCompany
            .ApplicationID = Me.ApplicationID.Trim
            .strConnection = GetConnectionString()
        End With

        Try
            oNewAppInsuranceByCompany = oController.GetInsuranceEntryStep1List(oNewAppInsuranceByCompany)
            With oNewAppInsuranceByCompany
                lblInsuranceAssetType.Text = .InsuranceAssetDescr
                LblAssetUsageDescr.Text = .AssetUsageDescr
                LblAssetUsageID.Text = .AssetUsageID.Trim

                lblAssetNewused.Text = .AssetUsageNewUsed
                LblInsuredBy.Text = .InsAssetInsuredByName
                 
                TxtTenorCredit.Text = CType(.Tenor, String)
                TxtTenorCredit.Enabled = False
                LblPaidBy.Text = .InsAssetPaidByName

                lblOTR.Text = FormatNumber(.TotalOTR, 0)
                txtJenisAksesoris.Text = .JenisAksesoris
                txtJenisAksesoris.Enabled = False

                txtNilaiAksesoris.Text = FormatNumber(.NilaiAksesoris)
                txtNilaiAksesoris.Enabled = False
                lblWilayahAsuransi.Text = CType(.DescWil, String)

                Dim amountC = CDbl(lblOTR.Text) + CDbl(txtNilaiAksesoris.Text)
                lblAmountCoverage.Text = FormatNumber(amountC, 0)
                lblTahunKendaraan.Text = CType(.ManufacturingYear, String)

                If .PerluasanPA = True Then
                    rdoPerluasanPersolanAccident.SelectedValue = "True"
                Else
                    rdoPerluasanPersolanAccident.SelectedValue = "False"
                End If
                rdoPerluasanPersolanAccident.Enabled = False 
              
            End With
             
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)

        End Try

    End Sub

    Private Sub DisplayGridInsco()

        Dim oInsCoAllocationInsCO As New Parameter.InsCoAllocationDetailList
        Dim dtEntityInsCO As New DataTable
        Dim dtEntityInsCOD As New DataTable
        With oInsCoAllocationInsCO
            .ApplicationID = Me.ApplicationID
            ' .CoverPeriodSelection = "FT"
            .BranchId = Me.sesBranchId.Replace("'", "").Trim
            .strConnection = GetConnectionString()
        End With


        Try
            oInsCoAllocationInsCO = oInsCoAllocationDetailListController.GetGridInsCoPremiumView(oInsCoAllocationInsCO)
            If oInsCoAllocationInsCO.ErrStr <> "" Then
                ShowMessage(lblMessage, oInsCoAllocationInsCO.ErrStr, True)
                Exit Sub
            Else
                lblMessage.Visible = False
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

        If Not oInsCoAllocationInsCO Is Nothing Then
            dtEntityInsCOD = oInsCoAllocationInsCO.ListData2
            dtEntityInsCO = oInsCoAllocationInsCO.ListData
        End If

        gridInsco.DataSource = dtEntityInsCO
        gridInsco.DataBind()

        gridDetailInsco.DataSource = dtEntityInsCOD
        gridDetailInsco.DataBind()



        txtstartDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        lblEndDate.Text = Me.BusinessDate.AddYears(1).ToString("dd/MM/yyyy")



    End Sub


    Private Sub gridInsco_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gridInsco.ItemDataBound
        If e.Item.ItemIndex > -1 Then
            Dim i As Integer = e.Item.ItemIndex
            Dim lblGridAdminFee As Label
            Dim lblGridGrandTotal As Label
            Dim lblGridMeteraiFee As Label
            If MaskAssBranchID.Trim = CType(e.Item.FindControl("lblGridInsuranceComBranchID"), Label).Text.Trim Then
                
                e.Item.CssClass = "item_grid item_gridselected"


                lblGridGrandTotal = CType(e.Item.FindControl("lblGridGrandTotal"), Label)
                lblGridAdminFee = CType(e.Item.FindControl("lblGridAdminFee"), Label)
                lblGridMeteraiFee = CType(e.Item.FindControl("lblGridMeteraiFee"), Label)

                'txtAmountRate.Text = FormatNumber(CDbl(lblGridGrandTotal.Text) - CDbl(lblGridAdminFee.Text) - CDbl(lblGridMeteraiFee.Text), 0)
                'calculatePremiGross()

                lblMaskapaiAsuransi.Text = CType(e.Item.FindControl("lblMaskapaiAsuransiDesc"), Label).Text
            End If
        End If
    End Sub



    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim oInsCoAllocationInsCO = New Parameter.InsCoAllocationDetailList 
        Dim startDate = ConvertDate2(txtstartDate.Text)

        If (startDate.Ticks - Me.BusinessDate.Ticks < 0) Then
            ShowMessage(lblMessage, "Tanggal Penutupan Tidak Boleh Kecil dari Busines Date", True)
            Exit Sub
        End If


        With oInsCoAllocationInsCO
            .ApplicationID = Me.ApplicationID
            .CoverPeriodSelection = cboCoverPeriod.SelectedValue()
            .BranchId = Me.sesBranchId.Replace("'", "").Trim
            .StartDate = ConvertDate2(txtstartDate.Text)
            .strConnection = GetConnectionString()
        End With
         
        Try 
            Dim result = oInsCoAllocationDetailListController.PenutupanAsuransiManualUpdate(oInsCoAllocationInsCO)
            If result.Trim <> "OK" Then
                ShowMessage(lblMessage, result, True)
                Exit Sub
            Else
                ShowMessage(lblMessage, "Sukses", False)
                pnlPenutupan.Visible = False
                btnsearch_Click(Nothing, Nothing)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlPenutupan.Visible = False
        btnsearch_Click(Nothing, Nothing)
    End Sub
End Class