﻿#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports System.Math
Imports System.Text
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class NewCoverInsuredByCust
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oCoverageType As UcCoverageType
    Protected WithEvents oApplicationType As UcApplicationType
    Protected WithEvents TxtAmountCoverage As ucNumberFormat

#Region "Property"
    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Private Property PBMaximumTenor() As String
        Get
            Return (CType(ViewState("PBMaximumTenor"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PBMaximumTenor") = Value
        End Set
    End Property
    Private Property PBMinimumTenor() As String
        Get
            Return (CType(ViewState("PBMinimumTenor"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PBMinimumTenor") = Value
        End Set
    End Property
    Private Property PMaximumTenor() As String
        Get
            Return (CType(ViewState("PMaximumTenor"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PMaximumTenor") = Value
        End Set
    End Property
    Private Property PMinimumTenor() As String
        Get
            Return (CType(ViewState("PMinimumTenor"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PMinimumTenor") = Value
        End Set
    End Property
    Private Property CustomerName() As String
        Get
            Return (CType(ViewState("CustomerName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property

    Private Property CoverageTypeID() As String
        Get
            Return (CType(ViewState("CoverageTypeID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CoverageTypeID") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property ExpiredDate() As String
        Get
            Return (CType(ViewState("ExpiredDate"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ExpiredDate") = Value
        End Set
    End Property
    Public Property InterestType() As String
        Get
            Return (CType(ViewState("InterestType"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InterestType") = Value
        End Set
    End Property
    Public Property InstallmentScheme() As String
        Get
            Return (CType(ViewState("InstallmentScheme"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InstallmentScheme") = Value
        End Set
    End Property
    Public Property MaturityDate() As Date
        Get
            Return (CType(ViewState("MaturityDate"), Date))
        End Get
        Set(ByVal Value As Date)
            ViewState("MaturityDate") = Value
        End Set
    End Property

#End Region
#Region "Constanta"
    Private oController As New InsNewCoverController

#End Region
#Region "Page Load"


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If


        'If Not CheckForm(Me.Loginid, CommonVariableHelper.FORM_NAME_NEW_APPLICATION_BY_COMPANY, CommonVariableHelper.APPLICATION_NAME) Then
        '    Exit Sub
        'End If

        If Not IsPostBack Then

            If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then

                LblErrorMessages.Text = ""
                Me.ApplicationID = Request.QueryString("ApplicationID")
                txtExpiredDate.Text = Format(Me.BusinessDate, "dd/MM/yyyy")

                Me.ExpiredDate = txtExpiredDate.Text

                'oCoverageType.SetValidatorFalse()
                oCoverageType.FillRequired = True
                Dim oNewCoverInsByCust As New Parameter.InsNewCover

                With oNewCoverInsByCust
                    .ApplicationID = Me.ApplicationID
                    .strConnection = GetConnectionString()
                End With

                Try
                    oNewCoverInsByCust = oController.GetInsuredByCustomer(oNewCoverInsByCust)
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try

                With oNewCoverInsByCust
                    LblBranchID.Text = .BranchId
                    LblApplicationID.Text = .ApplicationID
                    LblCustomerName.Text = .CustomerName
                    Me.CustomerName = LblCustomerName.Text
                    LblInsuredByName.Text = "Customer"
                    LblPaidByName.Text = .InsAssetPaidByName
                    Me.BranchID = .BranchId
                    Me.PBMaximumTenor = CType(.PBMaximumTenor, String)
                    Me.PBMinimumTenor = CType(.PBMinimumTenor, String)
                    Me.PMaximumTenor = CType(.PMaximumTenor, String)
                    Me.PMinimumTenor = CType(.PMinimumTenor, String)
                    'Me.InterestType = .InterestType
                    'Me.InstallmentScheme = .InstallmentScheme
                    Me.CustomerID = .CustomerId
                    Me.MaturityDate = CType(.MaturityDate, Date)
                    lblMaturityDate.Text = Format(Me.MaturityDate, "dd/MM/yyyy")
                End With

            End If
        End If

    End Sub

#End Region
#Region "Save "


    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click


        Me.CoverageTypeID = oCoverageType.CoverageTypeID.Trim
        If Me.CoverageTypeID = "0" Then
            Me.CoverageTypeID = "-"
        End If


        LblErrorMessages.Text = ""

        Me.ExpiredDate = txtExpiredDate.Text
        If Me.ExpiredDate <> "" Then
            If ConvertDate2(Me.ExpiredDate) > Me.MaturityDate Then
                LblErrorMessages.Text = "Tanggal Selesai harus <= Tanggal Jatuh Tempo"
                Exit Sub
            End If


        End If


        'save
        Dim oNewCoverInsByCust As New Parameter.InsNewCover

        If TxtAmountCoverage.Text.Trim = "" Then TxtAmountCoverage.Text = "0"

        With oNewCoverInsByCust
            .BranchId = LblBranchID.Text
            .ApplicationID = Me.ApplicationID
            .InsuranceCompanyName = TxtInsuranceCompany.Text
            .SumInsured = CType(TxtAmountCoverage.Text, Decimal)
            .CoverageType = Me.CoverageTypeID
            .PolicyNo = TxtPolicyNumber.Text
            .ExpiredDate = ConvertDate2(Me.ExpiredDate)
            .InsNotes = txtinsurancenotes.Text
            .BusinessDate = Me.BusinessDate
            .ApplicationTypeDescr = oApplicationType.AppTypeID.Trim

            .strConnection = GetConnectionString()
        End With

        Try
            oController.ProcessSaveInsuranceByCustomer(oNewCoverInsByCust)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Response.Redirect("NewCoverForExistingApp.aspx")

    End Sub
#End Region

#Region "Cancel "


#End Region



    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("NewCoverForExistingApp.aspx")
    End Sub

End Class