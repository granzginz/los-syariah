﻿#Region "Imports"

Option Strict On
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
#End Region

Public Class SPPA
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents oInsuranceBranchName As UcInsuranceBranchName


#Region "Constanta"
    Private oController As New SPPAController
    Private oControllerReport As New GeneralPagingController
    Private Const FILE_NAME_VIEWER As String = "SPPAViewer.aspx"
    Private Const COOKIES_GENERAL_SPPA_REPORT As String = CommonCookiesHelper.COOKIES_SPPA_REPORT


#End Region
#Region "Property"

    Private Property StrAggrementChecked() As String
        Get
            Return (CType(viewstate("StrAggrementChecked"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("StrAggrementChecked") = Value
        End Set
    End Property
    Private Property strApplicationChecked() As String
        Get
            Return (CType(viewstate("strApplicationChecked"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("strApplicationChecked") = Value
        End Set
    End Property

    Private Property StartSelectionDate() As String
        Get
            Return (CType(viewstate("StartSelectionDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("StartSelectionDate") = Value
        End Set
    End Property


    Private Property EndSelectionDate() As String
        Get
            Return (CType(viewstate("EndSelectionDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("EndSelectionDate") = Value
        End Set
    End Property
    Public Property SPPANo() As String
        Get
            Return (CType(viewstate("SPPANo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("SPPANo") = Value
        End Set
    End Property
    Public Property SPPACPNo() As String
        Get
            Return (CType(ViewState("SPPACPNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SPPACPNo") = Value
        End Set
    End Property
    Public Property SPPAJKNo() As String
        Get
            Return (CType(ViewState("SPPAJKNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SPPAJKNo") = Value
        End Set
    End Property
    Public Property InsCoID() As String
        Get
            Return (CType(viewstate("InsCoID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("InsCoID") = Value
        End Set
    End Property


#End Region
#Region "InitialDefaultPanel"

    Public Sub InitialDefaultPanel()
        PnlSPPAListing.Visible = False
    End Sub


#End Region

#Region "PageLoad"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            oBranch.IsAll = True

            oInsuranceBranchName.loadInsBranch(False)
            oInsuranceBranchName.FillRequired = True

            LblErrorMessages.Text = ""
            If Me.IsHoBranch = False Then
                If Request.QueryString("file") <> "" Then

                    Dim strFileLocation As String = "../XML/" & Request.QueryString("file")

                    Response.Write("<script language = javascript>" & vbCrLf _
                    & "var x = screen.width; " & vbCrLf _
                    & "var y = screen.height; " & vbCrLf _
                    & "window.open('" & strFileLocation & "','SPPA', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                    & "</script>")
                End If

                InitialDefaultPanel()
                txtStartSelectionDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                txtEndSelectionDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

                'oSearchBy.ListData = "dbo.Agreement.AgreementNo, Agreement NO-dbo.Customer.Name , Customer Name-dbo.AssetMaster.Description , Asset"
                'oSearchBy.BindData()

                If Me.SortBy = "" Then Me.SortBy = " InsCoSelectionDate "
            Else
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            End If
        End If

        Me.StartSelectionDate = txtStartSelectionDate.Text
        Me.EndSelectionDate = txtEndSelectionDate.Text
    End Sub
#End Region

#Region "Search"
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        PnlSPPAListing.Visible = True
        If oBranch.BranchID.Trim = "ALL" Then
            Me.SearchBy = " And dbo.Agreement.AgreementNo <> '-' "
        Else
            Me.SearchBy = " And dbo.InsuranceAsset.BranchId = '" & oBranch.BranchID.Trim & "'  And dbo.Agreement.AgreementNo <> '-' "
        End If

        If oInsuranceBranchName.SelectedValue <> "" Then
            If cboType.SelectedItem.Value = "N" Then
                Me.SearchBy = Me.SearchBy & " And FlagInsActivation <> 'R' "
                If Me.sesBranchId.Replace("'", "") = "000" Then
                    Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssID =  '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "'"
                Else
                    Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssBranchID =  '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "'"
                End If

                Me.InsCoID = " And MaskAssBranchID = '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "' "
            Else
                Me.SearchBy = Me.SearchBy & " And FlagInsActivation = 'R' "
                If Me.sesBranchId.Replace("'", "") = "000" Then
                    Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssID =  '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "'"
                Else
                    Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssBranchID =  '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "'"
                End If

                Me.InsCoID = " And  MaskAssBranchID = '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "' "
            End If
        End If

        If txtStartSelectionDate.Text <> "" And txtEndSelectionDate.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " AND dbo.InsuranceAsset.InscoSelectionDate Between Convert(DateTime,'" & Me.StartSelectionDate & "',103)  And Convert(DateTime,'" & Me.EndSelectionDate & "',103) "
        End If

        'If cboProductAsuransi.SelectedValue = "CP" Then
        '    Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.creditprotection =  '1' And (SPPACPNo = '-' OR SPPACPNo IS NULL) and (SPPACPDate IS NULL OR SPPACPDate = '1900-01-01 00:00:00.000') "
        'ElseIf cboProductAsuransi.SelectedValue = "JK" Then
        '    Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.jaminancredit =  '1' And (SPPAJKNo = '-' OR SPPAJKNo IS NULL) and (SPPAJKDate IS NULL OR SPPAJKDate = '1900-01-01 00:00:00.000') "
        'Else
        '    Me.SearchBy = Me.SearchBy & " And (SPPANo = '-' OR SPPANo IS NULL) and (SPPADate IS NULL OR SPPADate = '1900-01-01 00:00:00.000') "
        'End If

        btnPrint.Visible = True
        DoBind(Me.SearchBy)
    End Sub


#End Region
#Region "BindGrid SPPA"

    Sub DoBind(ByVal cmdWhere As String)
        Dim oEntities As New Parameter.SPPA

        'Modify by Wira 20180102
        If cboProductAsuransi.SelectedValue = "CP" Then
            cmdWhere = cmdWhere & " And dbo.InsuranceAsset.creditprotection =  '1' And (SPPACPNo = '-' OR SPPACPNo IS NULL) and (SPPACPDate IS NULL OR SPPACPDate = '1900-01-01 00:00:00.000') "
        ElseIf cboProductAsuransi.SelectedValue = "JK" Then
            cmdWhere = cmdWhere & " And dbo.InsuranceAsset.jaminancredit =  '1' And (SPPAJKNo = '-' OR SPPAJKNo IS NULL) and (SPPAJKDate IS NULL OR SPPAJKDate = '1900-01-01 00:00:00.000') "
        Else
            cmdWhere = cmdWhere & " And (SPPANo = '-' OR SPPANo IS NULL) and (SPPADate IS NULL OR SPPADate = '1900-01-01 00:00:00.000') "
        End If

        With oEntities
            .WhereCond = cmdWhere
            .PageSource = CommonVariableHelper.PAGE_SOURCE_CREATE_SPPA
            .strConnection = GetConnectionString
        End With

        Try
            oEntities = oController.GetListCreateSPPA(oEntities)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Dim dtEntity As New DataTable

        If Not oEntities Is Nothing Then
            dtEntity = oEntities.ListData
        End If


        DGridSPPA.DataSource = dtEntity
        DGridSPPA.DataBind()




    End Sub

#End Region
#Region "Function Click"
    Sub ResetMenthod()
        Me.SearchBy = " And dbo.InsuranceAsset.BranchId = '" & oBranch.BranchID.Trim & "' "
        txtStartSelectionDate.Text = ""
        txtEndSelectionDate.Text = ""
        oInsuranceBranchName.SelectedValue = ""
        oInsuranceBranchName.DataBind()
        PnlSPPAListing.Visible = False
        DoBind(Me.SearchBy)
    End Sub

#End Region
#Region "Reset Click"

    Private Sub ImbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ResetMenthod()
    End Sub

#End Region
#Region "CheckStatus"

    Public Sub ChkStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim loopitem As Int16
        Dim ChkStatusAgreementNo As CheckBox

        For loopitem = 0 To CType(DGridSPPA.Items.Count - 1, Int16)
            ChkStatusAgreementNo = New CheckBox
            ChkStatusAgreementNo = CType(DGridSPPA.Items(loopitem).FindControl("ChkStatusAgreementNo"), CheckBox)

            'If ChkStatusAgreementNo.Enabled Then
            If CType(sender, CheckBox).Checked = True Then
                ChkStatusAgreementNo.Checked = True
            Else
                ChkStatusAgreementNo.Checked = False
            End If
            'Else
            '    ChkStatusAgreementNo.Checked = False
            'End If

            'ChkStatusAgreementNo.Checked = True

        Next

    End Sub
#End Region
#Region "Print "

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        If CheckFeature(Me.Loginid, CommonVariableHelper.FORM_NAME_SPPA, CommonVariableHelper.FORM_FEATURE_PRINT, CommonVariableHelper.APPLICATION_NAME) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        LblErrorMessages.Text = ""
        Dim LoopPrint As Int32
        Dim TempAgreementStatusPrint As String
        Dim TempApplicationStatusPrint As String

        For LoopPrint = 0 To DGridSPPA.Items.Count - 1
            Dim CurrentCheckBox As New CheckBox
            Dim strAgreementNo As New Label
            Dim strApplicationID As New Label
            Dim koma As String

            CurrentCheckBox = CType(DGridSPPA.Items(LoopPrint).FindControl("ChkStatusAgreementNo"), CheckBox)
            strApplicationID = CType(DGridSPPA.Items(LoopPrint).FindControl("LblApplicationID"), Label)
            strAgreementNo = CType(DGridSPPA.Items(LoopPrint).FindControl("LblAgreementNo"), Label)

            If CurrentCheckBox.Checked Then
                If TempApplicationStatusPrint = "" Then koma = "" Else koma = ","
                TempApplicationStatusPrint = TempApplicationStatusPrint & koma & strApplicationID.Text.Trim
                Context.Trace.Write("TempApplicationStatusPrint = " + TempApplicationStatusPrint)
                If TempAgreementStatusPrint = "" Then koma = "" Else koma = ","
                TempAgreementStatusPrint = TempAgreementStatusPrint & koma & strAgreementNo.Text.Trim
            End If
        Next


        If TempApplicationStatusPrint <> "" Then

            ApplicationToBePrinted(TempApplicationStatusPrint)
            AgreementToBePrinted(TempAgreementStatusPrint)

            Dim oEntities As New Parameter.SPPA
            With oEntities
                oEntities.BranchId = oBranch.BranchID.Trim
                oEntities.BusinessDate = Me.BusinessDate
                oEntities.BulkOfApplicationID = Me.strApplicationChecked
                oEntities.strConnection = GetConnectionString()
                oEntities.MaskAssBranchID = Me.InsCoID
                oEntities.ProductAsuransi = cboProductAsuransi.SelectedItem.Value

            End With


            'If cboProductAsuransi.SelectedItem.Value = "CP" Then
            '    oEntities = oController.GenerateSPPACP(oEntities)
            'End If
            oEntities = oController.GenerateSPPA(oEntities)
            Me.SPPANo = oEntities.SPPANo
            SendCookies()
            Response.Redirect(FILE_NAME_VIEWER)

        Else
            Exit Sub
        End If
    End Sub
#End Region
#Region "ApplicationToBePrinted"
    Public Sub ApplicationToBePrinted(ByVal TempApplicationStatusPrint As String)

        Dim ApplicationChecked As String
        ApplicationChecked = Replace(TempApplicationStatusPrint, ",", "','").Trim
        ApplicationChecked = " And ApplicationID in ('" & ApplicationChecked & "')".Trim
        Context.Trace.Write("ApplicationChecked = " + ApplicationChecked)
        Me.strApplicationChecked = ApplicationChecked

    End Sub

#End Region

#Region "AgreementToBePrinted"
    Public Sub AgreementToBePrinted(ByVal TempAgreementToBePrinted As String)
        Dim AgreementChecked As String

        AgreementChecked = Replace(TempAgreementToBePrinted, ",", "','").Trim
        AgreementChecked = " And AgreementNo in ('" & AgreementChecked & "')".Trim
        Context.Trace.Write("AgreementChecked = " + AgreementChecked)
        Me.StrAggrementChecked = AgreementChecked
    End Sub
#End Region
#Region "SendCookies"
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies(COOKIES_GENERAL_SPPA_REPORT)
        Dim cmdwhere As String

        cmdwhere = ""
        cmdwhere = Me.SearchBy

        'Modify by Wira 20180102
        If cboProductAsuransi.SelectedValue = "CP" Then
            Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.creditprotection =  '1' And (SPPACPNo <> '-' OR SPPACPNo IS NOT NULL) and (SPPACPDate IS NOT NULL OR SPPACPDate <> '1900-01-01 00:00:00.000') "
        ElseIf cboProductAsuransi.SelectedValue = "JK" Then
            Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.jaminancredit =  '1' And (SPPAJKNo <> '-' OR SPPAJKNo IS NOT NULL) and (SPPAJKDate IS NOT NULL OR SPPAJKDate <> '1900-01-01 00:00:00.000') "
        Else
            Me.SearchBy = Me.SearchBy & " And (SPPANo <> '-' OR SPPANo IS NOT NULL) and (SPPADate IS NOT NULL OR SPPADate <> '1900-01-01 00:00:00.000') "
        End If

        If cboProductAsuransi.SelectedItem.Value = "CP" Then
            Me.SearchBy = Me.SearchBy + " And dbo.insuranceAsset.SPPACPNo = '" & Me.SPPANo & "' "
            If Not cookie Is Nothing Then
                cookie.Values("SearchBy") = Me.SearchBy
                cookie.Values("SPPACPNO") = Me.SPPANo
                cookie.Values("ProductAsuransi") = cboProductAsuransi.SelectedItem.Value
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(COOKIES_GENERAL_SPPA_REPORT)
                cookieNew.Values("SearchBy") = Me.SearchBy
                cookieNew.Values("SPPACPNO") = Me.SPPANo
                cookieNew.Values("ProductAsuransi") = cboProductAsuransi.SelectedItem.Value
                Response.AppendCookie(cookieNew)
            End If


        ElseIf cboProductAsuransi.SelectedItem.Value = "JK" Then
            Me.SearchBy = Me.SearchBy + " And dbo.insuranceAsset.SPPAJKNo = '" & Me.SPPANo & "' "
            If Not cookie Is Nothing Then
                cookie.Values("SearchBy") = Me.SearchBy
                cookie.Values("SPPAJKNO") = Me.SPPANo
                cookie.Values("ProductAsuransi") = cboProductAsuransi.SelectedItem.Value
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(COOKIES_GENERAL_SPPA_REPORT)
                cookieNew.Values("SearchBy") = Me.SearchBy
                cookieNew.Values("SPPAJKNO") = Me.SPPANo
                cookieNew.Values("ProductAsuransi") = cboProductAsuransi.SelectedItem.Value
                Response.AppendCookie(cookieNew)
            End If
        Else
            Me.SearchBy = Me.SearchBy + " And dbo.insuranceAsset.SPPANo = '" & Me.SPPANo & "' "
            If Not cookie Is Nothing Then
                cookie.Values("SearchBy") = Me.SearchBy
                cookie.Values("SPPANO") = Me.SPPANo
                cookie.Values("ProductAsuransi") = cboProductAsuransi.SelectedItem.Value
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(COOKIES_GENERAL_SPPA_REPORT)
                cookieNew.Values("SearchBy") = Me.SearchBy
                cookieNew.Values("SPPANO") = Me.SPPANo
                cookieNew.Values("ProductAsuransi") = cboProductAsuransi.SelectedItem.Value
                Response.AppendCookie(cookieNew)
            End If
        End If
        Me.SearchBy = cmdwhere
    End Sub

#End Region

    Private Sub DGridSPPA_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Dim lnkAgreementNo As HyperLink
        Dim lnkCust As HyperLink
        Dim lblCustomerID As Label
        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            lnkCust = CType(e.Item.FindControl("lnkCust"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("LblApplicationID"), Label)
            lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
            lnkCust.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & Server.UrlEncode(lblCustomerID.Text) & "')"
        End If
    End Sub

End Class