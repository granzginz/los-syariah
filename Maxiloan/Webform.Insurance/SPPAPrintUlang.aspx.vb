﻿#Region " Imports"
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
#End Region

Public Class SPPAPrintUlang
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents oInsuranceBranchName As UcInsuranceBranchName
    Protected WithEvents oBranch As UcBranchAll
    Private oController As New SPPAController
    Private m_controller2 As New DataUserControlController
    Private oControllerChild As New GeneralPagingController
    Dim oEntitesChild As New Parameter.InsCoAllocationDetailList

#Region " Properties"
    Public Property SPPANo() As String
        Get
            Return (CType(viewstate("SPPANo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("SPPANo") = Value
        End Set
    End Property
    Private Property StartSelectionDate() As String
        Get
            Return (CType(viewstate("StartSelectionDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("StartSelectionDate") = Value
        End Set
    End Property
    Private Property EndSelectionDate() As String
        Get
            Return (CType(viewstate("EndSelectionDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("EndSelectionDate") = Value
        End Set
    End Property
    Public Property InsCoID() As String
        Get
            Return (CType(viewstate("InsCoID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("InsCoID") = Value
        End Set
    End Property
#End Region
#Region " Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        
        If Not IsPostBack Then
            If Request.QueryString("file") <> "" Then

                Dim strFileLocation As String = "../XML/" & Request.QueryString("file")

                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; " & vbCrLf _
                & "var y = screen.height; " & vbCrLf _
                & "window.open('" & strFileLocation & "','SPPA', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")
            End If
            'oBranch.IsAll = False

            'oInsuranceBranchName.loadInsBranch(False)
            'oInsuranceBranchName.FillRequired = True
            ' remark by WIra 2016-02-24
            LblErrorMessages.Text = ""
            Dim dtbranch As New DataTable
            Dim dtInsCo As New DataTable

            Me.sesBranchId = Replace(Me.sesBranchId, "'", "")

            If Me.IsHoBranch Then
                dtbranch = m_controller2.GetBranchName(GetConnectionString, "All")
            Else
                dtbranch = m_controller2.GetBranchName(GetConnectionString, Me.sesBranchId)
            End If

            With cboParent
                .DataTextField = "Name"
                .DataValueField = "ID"
                .DataSource = dtbranch
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            End With

            getchildcombo()
            'If IsSingleBranch() And Me.IsHoBranch = False Then
            'by wira 2016-02-23, supaya ho bisa cetak ulang sppa
            If IsSingleBranch() Then
                PnlSPPAListing.Visible = False
                txtStartSelectionDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                txtEndSelectionDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                If Me.SortBy = "" Then Me.SortBy = " InsCoSelectionDate "
            Else
                KickOutForMultipleBranch()
            End If
        End If

        Me.StartSelectionDate = txtStartSelectionDate.Text
        Me.EndSelectionDate = txtEndSelectionDate.Text
    End Sub
#End Region
#Region " Search"
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        PnlSPPAListing.Visible = True
        'Me.SearchBy = " And dbo.InsuranceAsset.BranchId = '" & oBranch.BranchID.Trim & "' "
        Me.SearchBy = " And dbo.InsuranceAsset.BranchId = '" & cboParent.SelectedItem.Value.Trim & "' "

        'If oInsuranceBranchName.SelectedValue <> "" Then
        If cboParent.SelectedValue <> "" Then
            If cboType.SelectedItem.Value = "N" Then
                Me.SearchBy = Me.SearchBy & " And FlagInsActivation <> 'R' "
                If Me.sesBranchId.Replace("'", "") = "000" Then
                    'Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssID =  '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "'"
                    Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssBranchID = '" & hdnChildValue.Value.Trim & "' "
                Else
                    'Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssBranchID =  '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "'"
                    Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssBranchID = '" & hdnChildValue.Value.Trim & "' "
                End If

                'Me.InsCoID = " And MaskAssID = '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "' "
                Me.InsCoID = " And MaskAssID = '" & hdnChildValue.Value.Trim & "' "
            Else
                Me.SearchBy = Me.SearchBy & " And FlagInsActivation = 'R' "
                If Me.sesBranchId.Replace("'", "") = "000" Then
                    'Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssID =  '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "'"
                    Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssBranchID =  '" & hdnChildValue.Value.Trim & "'"
                Else
                    'Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssBranchID =  '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "'"
                    Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssBranchID =  '" & hdnChildValue.Value.Trim & "'"
                End If

                'Me.InsCoID = " And  MaskAssID = '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "' "
                Me.InsCoID = " And  MaskAssID = '" & hdnChildValue.Value.Trim & "' "
            End If
        End If

        If txtStartSelectionDate.Text <> "" And txtEndSelectionDate.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " AND dbo.InsuranceAsset.InscoSelectionDate Between Convert(DateTime,'" & Me.StartSelectionDate & "',103)  And Convert(DateTime,'" & Me.EndSelectionDate & "',103) "
        End If

        DoBind(Me.SearchBy)
    End Sub

#End Region
#Region " Binding"
    Sub DoBind(ByVal cmdWhere As String)
        Dim oEntities As New Parameter.SPPA

        With oEntities
            .WhereCond = cmdWhere
            .strConnection = GetConnectionString
        End With

        Try
            oEntities = oController.GetSPPAList(oEntities)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Dim dtEntity As New DataTable

        If Not oEntities Is Nothing Then
            dtEntity = oEntities.ListData
        End If

        DGridSPPA.DataSource = dtEntity
        DGridSPPA.DataBind()
    End Sub
#End Region
#Region " Print"
    Private Sub DGridSPPA_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGridSPPA.ItemCommand
        If e.CommandName = "PRINT" Then
            Me.SPPANo = e.Item.Cells(2).Text.Trim
            SendCookies()
            Response.Redirect("SPPAViewer.aspx?caller=SPPAPrintUlang.aspx")
        End If
    End Sub
#End Region
#Region " Send Cookies"
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies(CommonCookiesHelper.COOKIES_SPPA_REPORT)
        Dim cmdwhere As String

        cmdwhere = ""
        cmdwhere = Me.SearchBy

        Me.SearchBy = Me.SearchBy + " And dbo.insuranceAsset.SPPANo = '" & Me.SPPANo & "' "

        If Not cookie Is Nothing Then
            cookie.Values("SearchBy") = Me.SearchBy
            cookie.Values("SPPANO") = Me.SPPANo
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(CommonCookiesHelper.COOKIES_SPPA_REPORT)
            cookieNew.Values("SearchBy") = Me.SearchBy
            cookie.Values("SPPANO") = Me.SPPANo
            Response.AppendCookie(cookieNew)
        End If

        Me.SearchBy = cmdwhere
    End Sub

#End Region

    'by Wira 20160224
    Protected Function BranchIDChange() As String
        Return "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Protected Function getchildcombo() As String

        Dim dtInsCo As New DataTable

        dtInsCo = getchildbranchCombotDT()
        Response.Write(GenerateScript(dtInsCo))
    End Function

    Private Function getchildbranchCombotDT() As DataTable
        With oEntitesChild
            .strConnection = GetConnectionString()
            If Me.IsHoBranch Then
                .BranchId = "All"
            Else
                .BranchId = Me.sesBranchId
            End If
        End With

        Dim dtInsCo As New DataTable

        oEntitesChild = oControllerChild.GetInsuranceBranchCombo(oEntitesChild)
        dtInsCo = oEntitesChild.ListData
        Return dtInsCo
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" BranchID = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("BranchID")).Trim Then
                        strType = CStr(DataRow(i)("BranchID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("ChildId")), "null", DataRow(i)("ChildId"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("ChildId")), "null", DataRow(i)("ChildId"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"
        Return strScript
    End Function

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim DtChild As DataTable = getchildbranchCombotDT()
        Dim i As Integer

        For i = 0 To DtChild.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(cboChild.UniqueID, CStr(DtChild.Rows(i).Item("ChildId")).Trim)
        Next

        Page.ClientScript.RegisterForEventValidation(cboChild.UniqueID, "0")


        MyBase.Render(writer)
    End Sub
#Region "Reset "

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click        
        'cboParent.SelectedValue = ""
        'Me.SearchBy = " And dbo.InsuranceAsset.BranchId = '" & Replace(Me.sesBranchId, "'", "") & "' "
        'getchildcombo()
        'SearchBy.Text = ""
        'SearchBy.BindData()
        'BindGridEntity(Me.SearchBy)
        SearchBy = ""
        'DoBind(Me.SearchBy)

    End Sub

#End Region

End Class