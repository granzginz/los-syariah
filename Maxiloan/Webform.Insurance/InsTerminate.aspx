﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsTerminate.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsTerminate" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UCReason" Src="../webform.UserController/UCReason.ascx" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Insurance Termination</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
       <script src="../js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script language="javascript" type="text/javascript">

        function toCommas(yourNumber) {
            var n = yourNumber.toString().split(".");
            n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return n.join(".");
        }

        function SisaPenghasilan() {

            var amountToCust = parseFloat($('#txtRefundAmountToCust_txtNumber').val().replace(/,/g, ""));
            var deductAmount = parseFloat($('#txtDeductAmount_txtNumber').val().replace(/,/g, ""));
            var c = amountToCust - deductAmount;
            $('#lblTtl').text(toCommas(c));


            var refundAmountFromInsCo = parseFloat($('#lblRefundAmountFromInsCo').text().replace(/,/g, ""));
            var gain = refundAmountFromInsCo - c;
            $('#lblGain').text(toCommas(gain));

        }


        $(document).ready(function () {
            $('#txtRefundAmountToCust_txtNumber').change(function (e) { SisaPenghasilan(); });
            $('#txtDeductAmount_txtNumber').change(function (e) { SisaPenghasilan(); });
         });


        function OpenWinHistory(pCGID, pExID, pExName) {
            var x = screen.width; var y = screen.height - 100; window.open('../ExecutorHistory.aspx?CGID=' + pCGID + '&ExecutorID=' + pExID + '&ExecutorName=' + pExName + '', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCollector(pCollID, pCGID) {
            var x = screen.width; var y = screen.height - 100; window.open('../../Webform.Setup/CollectorView.aspx?CGID=' + pCGID + '&CollectorID=' + pCollID + '', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID) {
            var x = screen.width; var y = screen.height - 100; window.open('../../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinPrintClaimForm(BranchID, ApplicationID) {
            var x = screen.width; var y = screen.height - 100; window.open('PrintClaimForm.aspx?Branch=' + BranchID + '&style=Insurance&ApplicationID=' + ApplicationID, 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function currencyFormatku(fld, e) {
            var key = '';
            var i = j = 0;
            var strCheck = '0123456789';
            var whichCode = (window.Event) ? e.which : e.keyCode;
            document.onkeypress = window.event;
            if (whichCode == 13) return true;  // Enter
            key = String.fromCharCode(whichCode);  // Get key value from key code
            if (strCheck.indexOf(key) == -1) return false;  // Not a valid key
        }

        function OpenWinInsurance(pSource, pApplicationID, pBranchID) {
            var x = screen.width; var y = screen.height - 100;
            window.open('../Webform.Insurance/ViewInsuranceDetail.aspx?pagesource=' + pSource + '&ApplicationID=' + pApplicationID + '&BranchID=' + pBranchID, 'OPENINSURANCE', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1');
        }

    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="LblMessage" runat="server" ForeColor="Red"></asp:Label>
    <asp:Panel ID="pnlsearch" runat="server">
        <div class="form_title">
          <div class="title_strip"> </div>
            <div class="form_single">
                <h4>
                    PENUTUPAN ASURANSI
                </h4>
            </div>
        </div>

        <div class="form_box">
            <div class="form_single">
                 <label class ="label_req">
                    Cabang</label>
                <asp:DropDownList ID="ddlBranch" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                    ControlToValidate="ddlBranch" ErrorMessage="Harap pilih Cabang" InitialValue="0" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Konsumen</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_title">
            <div class="title_strip"> </div>
            <div class="form_single">
                <h4>PENUTUPAN ASURANSI</h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgClaimRequestList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="AgreementNo" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TUTUP">
                                <ItemStyle HorizontalAlign="Center" width='3%' VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgTerminate" ImageUrl="../Images/IconEdit.gif" CommandName="Terminate" runat="server" Visible='<%#iif(Container.dataitem("ClaimType")="TMN" and Container.dataitem("ClaimStatus")<>"J" , "False", "True")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SURAT">
                                <ItemStyle HorizontalAlign="Center" width='3%' VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbPrint" ImageUrl="../Images/IconPrinter.gif" CommandName="Print" runat="server"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="SURATREFUND">
                                <ItemStyle HorizontalAlign="Center" width='3%' VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbPrintRefund" ImageUrl="../Images/IconPrinter.gif" CommandName="PrintRefund" runat="server"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ASR">
                                <ItemStyle HorizontalAlign="Center" width='3%' VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyInsurance" runat="server">
                                        <asp:Image ID="imgRate" ImageUrl="../images/iconinsurance.gif" runat="server"></asp:Image>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="BranchID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="ApplicationID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CustomerID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="AgreementNo"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CustomerName"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="24%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn SortExpression="description" DataField="description" HeaderText="NAMA ASSET">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn SortExpression="PolicyNumber" DataField="PolicyNumber" HeaderText="NO POLIS">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn SortExpression="Contractstatus" DataField="Contractstatus" HeaderText="STS">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="3%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="AssetSeqNo" HeaderText="AssetSeqNo">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="InsSequenceNo" HeaderText="InsSeqNo">
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                       <%--<uc2:ucGridNav id="GridNavigator" runat="server"/>  --%>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server" Width="34px" >1</asp:TextBox>
                        <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server"  CssClass="validator_general"  ControlToValidate="txtPage" MinimumValue="1" ErrorMessage="No Halaman Salah"
                            MaximumValue="999999999" Type="Integer"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server"  CssClass="validator_general"  ControlToValidate="txtPage" ErrorMessage="No Halaman Salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblRecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    
    <asp:Panel ID="pnlView" runat="server">
     <div class="form_title">
          <div class="title_strip"> </div>
        <div class="form_single">
            <h4>
                PENUTUPAN ASURANSI
            </h4>
        </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Asset
                </label>
                <asp:Label ID="lblAsset" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jenis Cover
                </label>
                <asp:Label ID="lblCoverage" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Perusahaan Asuransi
                </label>
                <asp:Label ID="lblInsCo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Polis
                </label>
                <asp:Label ID="lblPolicyNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Status Kontrak
                </label>
                <asp:Label ID="lblContractStatus" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jangka Waktu
                </label>
                <asp:Label ID="lblTenor" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Rangka
                </label>
                <asp:Label ID="lblChassisNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Mesin
                </label>
                <asp:Label ID="lblEngineNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jangka Waktu
                </label>
                <asp:Label ID="lblInsuranceLength" runat="server"></asp:Label>&nbsp;month
            </div>
            <div class="form_right">
                <label>
                    Periode Asuransi
                </label>
                <asp:Label ID="lblStartDate" runat="server"></asp:Label>&nbsp;S/D&nbsp;
                <asp:Label ID="lblEndDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Premi Utama Customer
                </label>
                <asp:Label ID="lblMainPremiumToCust" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Premi Ke Customer
                </label>
                <asp:Label ID="lblPremiumToCust" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Premi Utama ke Asuransi
                </label>
                <asp:Label ID="lblMainPremiumToInsCo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Premi Utama Ke Asuransi
                </label>
                <asp:Label ID="lblPremiumToInsCo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Apakah bisa Refund
                </label>
                <asp:Label ID="lblisrefund" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Tanggal Tutup</label>
                <asp:TextBox runat="server" ID="txtTerminateDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtTerminateDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alasan Tutup</label>
                <uc1:ucreason id="oReason" runat="server"></uc1:ucreason>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan</label>
                <asp:TextBox ID="txtNotes" runat="server" Width="70%" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnCalculate" runat="server" Text="Calculate" CssClass="small button green">
            </asp:Button>
            <asp:Button ID="btnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgMainPremium" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                        OnSortCommand="Sorting" DataKeyField="Year" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TAHUN">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="14%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblYear" runat="server" Text='<%#Container.DataItem("Year")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMI UTAMA KE CUST">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="right" Width="14%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblMainPremToCust" runat="server" Text='<%#formatnumber(Container.DataItem("MainPremiumToCust"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DIBAYAR OLEH CUST">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="right" Width="14%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblPaidAmtByCust" runat="server" Text='<%#formatnumber(Container.DataItem("PaidAmtByCust"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TGL MULAI">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="14%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblStartDateCust" runat="server" Text='<%#Format(Container.DataItem("StartDateCust"), "dd/MM/yyyy")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TGL SELESAI">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="14%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDateCust" runat="server" Text='<%#Format(Container.DataItem("EndDateCust"), "dd/MM/yyyy")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA PERIODE OLEH CUST">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="14%" />
                                <ItemTemplate>
                                    <asp:Label ID="RestPeriodCust" runat="server" Text='<%#Container.DataItem("RestOfPeriodByCust")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" Mode="NumericPages"
                            Visible="False" />
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlCalculate" runat="server">
       <div class="form_box_header">
        <div class="form_single">
            <h4>
                PERHITUNGAN REFUND
            </h4>
        </div>
        </div>
        <div class="form_box"> 
            <div class="form_single"> 
                <label> Sisa Periode  </label>
                <asp:Label ID="lblRestInsToInsCo" runat="server"></asp:Label>&nbsp;days
            </div>
        </div>


        <div class="form_box"> 
            <div class="form_single"> 
                <label>Total Refund</label>
               <asp:Label ID="lblRefundAmountFromInsCo" runat="server"> </asp:Label>
            </div>
        </div>


          <div class="form_box"> 
            <div class="form_single"> 
                <label class ="label_req">Refund Ke Customer</label> 
                <uc1:ucnumberformat id="txtRefundAmountToCust" runat="server" /> 
            </div>
        </div>

        <div class="form_box"> 
            <div class="form_single"> 
                 <label class ="label_req"> Jumlah Pengurangan </label> 
                <uc1:ucnumberformat id="txtDeductAmount" runat="server" /> 
            </div>
        </div>

        <div class="form_box"> 
            <div class="form_single"> 
                <label> Total Refund Ke Customer </label>
                <asp:Label ID="lblTtl" runat="server"></asp:Label>
            </div>
        </div>
      <div class="form_box"> 
            <div class="form_single"> 
                <label> Pendapatan Asuransi</label>
                <asp:Label ID="lblGain" runat="server"></asp:Label>
            </div>
        </div>

     
  <%--      <div class="form_box">
            <div class="form_left">
                <label>
                    Refund Ke Supplier
                </label>
                <asp:Label ID="lblRefundToSupplier" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Refund Ke MultiFinance
                </label>
                <asp:Label ID="lblRefundAmountFromInsCo" runat="server"></asp:Label>
            </div>
           
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Prosentase Pengurangan
                </label>
                <asp:Label ID="lblDeductPercent" runat="server"></asp:Label>&nbsp;%
            </div> 
        </div>
--%>
           
        <div class="form_button">
            <asp:Button ID="btnSave2" runat="server" Text="Save" CssClass="small button blue" />
            <asp:Button ID="btnCancel2" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray" />
        </div>
    </asp:Panel>
    </form>
</body>
</html>
