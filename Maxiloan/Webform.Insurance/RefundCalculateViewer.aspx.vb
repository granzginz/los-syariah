﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Webform.UserController
#End Region


Public Class RefundCalculateViewer
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Private m_controller As New InsTerminateController
    Private oCustomClass As New Parameter.InsTerminate
#End Region
#Region "Property"

    Private Property ApplicationId() As String
        Get
            Return CType(viewstate("ApplicationId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationId") = Value
        End Set
    End Property

    Private Property AssetSequenceNo() As Integer
        Get
            Return CType(viewstate("AssetSequenceNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("AssetSequenceNo") = Value
        End Set
    End Property
    Private Property InsSequenceNo() As Integer
        Get
            Return CType(viewstate("InsSequenceNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("InsSequenceNo") = Value
        End Set
    End Property



#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If        
        BindReport()

    End Sub
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("REFUND_COOKIES")
        Me.ApplicationId = cookie.Values("ApplicationId")
        Me.BranchID = cookie.Values("BranchID")
        Me.AssetSequenceNo = CInt(cookie.Values("AssSequenceNo"))
        Me.InsSequenceNo = CInt(cookie.Values("InsSequenceNo"))
    End Sub
    Sub BindReport()
        GetCookies()
        Dim dstPrintRefundForm As New DataSet
        Dim PrintingRefundForm As New rptRefundCalculate
        With oCustomClass
            .strConnection = GetConnectionString
            .ApplicationID = Me.ApplicationId
            .BranchId = Me.BranchID
            .AssetSequenceNo = Me.AssetSequenceNo
            .InsSequenceNo = Me.InsSequenceNo
            .LoginId = Me.Loginid
        End With

        dstPrintRefundForm = m_controller.InsRefundCalculate(oCustomClass)

        PrintingRefundForm.SetDataSource(dstPrintRefundForm)
        PrintingRefundForm.SetParameterValue(0, Me.UserID)
        PrintingRefundForm.SetParameterValue(1, Me.BusinessDate)
        crvInsRefundCalculate.ReportSource = PrintingRefundForm
        crvInsRefundCalculate.DataBind()

        With PrintingRefundForm.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer
        'doctoprint.PrinterSettings.PrinterName = "Maxiloan Printer Server"

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "RefundCalculateViewer" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                PrintingRefundForm.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim strFileLocation As String
        Dim strFile As String = Me.Session.SessionID + Me.Loginid + "RefundCalculateViewer.pdf"

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += strFile

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With PrintingRefundForm
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        Response.Redirect("InsTerminate.aspx?strFileLocation=" & strFile)
    End Sub

    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBackReport.Click
        Response.Redirect("InsTerminate.aspx")
    End Sub
End Class