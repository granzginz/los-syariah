﻿
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports System.Drawing

Public Class PolicyReceiveDownload
    Inherits Maxiloan.Webform.WebBased
    Private m_controller As New InsurancePolicyReceiveController

    Dim currentPage As Integer = CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Dim PageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    Dim currentPageNumber As Int32 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Dim totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Protected WithEvents imbSave As System.Web.UI.WebControls.ImageButton
    Dim recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT


    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property

    Private Property ExcelFilePath() As String
        Get
            Return CType(viewstate("ExcelFilePath"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ExcelFilePath") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
           
            txtsdate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            Me.SearchBy = ""
            Me.SortBy = "kontrak"
        End If
    End Sub

    'Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
    '    If fileUploadCheck() Then

    '        Dim strFileLocation As String
    '        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "Polis\" & _
    '        Me.Session.SessionID + Me.Loginid + "policyReceive.xls"
    '        fileUpload.PostedFile.SaveAs(strFileLocation)
    '        Me.ExcelFilePath = strFileLocation
    '        BindGridEntity(Me.SearchBy)
    '        pnlGrid.Visible = True

    '    End If
    'End Sub


    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If fileUploadCheck() Then

            Dim strFileLocation As String
            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "Polis\" & _
            Me.Session.SessionID + Me.Loginid + "policyReceive.xls"

            fileUpload.PostedFile.SaveAs(strFileLocation)

            Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & strFileLocation & ";Extended Properties=""Excel 8.0;HDR=YES;"""
            Dim objComm As New OleDb.OleDbCommand
            Dim objConn As New OleDb.OleDbConnection(strConn)

            If objConn.State = ConnectionState.Closed Then objConn.Open()

            objComm.Connection = objConn
            objComm.CommandType = CommandType.Text
            objComm.CommandText = "select * from [Sheet1$]"

            Dim oReader As OleDb.OleDbDataReader
            Dim dt1 As New DataTable
            Dim oCols(13) As String

            oReader = objComm.ExecuteReader(CommandBehavior.CloseConnection)

            dt1.Columns.Add("KONTRAK")
            dt1.Columns.Add("SERTIFIKAT")
            dt1.Columns.Add("NAMA_TERTANGGUNG")
            dt1.Columns.Add("MODEL")
            dt1.Columns.Add("MERK")
            dt1.Columns.Add("TAHUN")
            dt1.Columns.Add("WARNA")
            dt1.Columns.Add("NO_MESIN")
            dt1.Columns.Add("NO_RANGKA")
            dt1.Columns.Add("TSI")
            dt1.Columns.Add("PERIODE_START")
            dt1.Columns.Add("PERIODE_END")
            dt1.Columns.Add("TERMS")
            dt1.Columns.Add("PREMI")

            While oReader.Read
                oCols(0) = oReader.Item("KONTRAK").ToString
                oCols(1) = oReader.Item("SERTIFIKAT").ToString
                oCols(2) = oReader.Item("NAMA_TERTANGGUNG").ToString
                oCols(3) = oReader.Item("MODEL").ToString
                oCols(4) = oReader.Item("MERK").ToString
                oCols(5) = oReader.Item("TAHUN").ToString
                oCols(6) = oReader.Item("WARNA").ToString
                oCols(7) = oReader.Item("NO_MESIN").ToString
                oCols(8) = oReader.Item("NO_RANGKA").ToString
                oCols(9) = oReader.Item("TSI").ToString
                oCols(10) = oReader.Item("PERIODE_START").ToString
                oCols(11) = oReader.Item("PERIODE_END").ToString
                oCols(12) = oReader.Item("TERMS").ToString
                oCols(13) = oReader.Item("PREMI").ToString

                dt1.Rows.Add(oCols)
            End While

            objConn.Close()

            Dim oInsStdRate As New Parameter.InsuranceStandardPremium

            With oInsStdRate
                .dtPolis = dt1
                .strConnection = GetConnectionString
            End With

            Try
                m_controller.savePolicyUploadXLS(oInsStdRate)
                BindGridEntity(Me.SearchBy)
                pnlGrid.Visible = True
            Catch ex As Exception
                LblErrorMessages.Text = ex.Message
                pnlGrid.Visible = False
            End Try
        End If
    End Sub

    Public Function fileUploadCheck() As Boolean
        If fileUpload.PostedFile Is Nothing Then Return False
        If fileUpload.PostedFile.ContentLength = 0 Then Return False
        Return True
    End Function

    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy)
    End Sub

    Public Sub ChkStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim loopitem As Int16
        Dim ChkStatusAccountPayableNo As CheckBox
        For loopitem = 0 To CType(dtgPaging.Items.Count - 1, Int16)
            Context.Trace.Write("loopitem = " & loopitem)
            ChkStatusAccountPayableNo = New CheckBox
            ChkStatusAccountPayableNo = CType(dtgPaging.Items(loopitem).FindControl("ChkStatusAccountPayableNo"), CheckBox)
            If ChkStatusAccountPayableNo.Enabled Then
                If CType(sender, CheckBox).Checked = True Then
                    Context.Trace.Write("CType(sender, CheckBox).Checked  = True ")
                    ChkStatusAccountPayableNo.Checked = True
                Else
                    Context.Trace.Write("CType(sender, CheckBox).Checked  = False ")
                    ChkStatusAccountPayableNo.Checked = False
                End If
            Else
                Context.Trace.Write("ChkStatusAccountPayableNo.Checked = False")
                ChkStatusAccountPayableNo.Checked = False
            End If
        Next
    End Sub

    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As New DataTable
        Dim oInsStdRate As New Parameter.InsuranceStandardPremium
        Me.SearchBy = cmdWhere
        With oInsStdRate
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = PageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString
            .ExcelFilePath = Me.ExcelFilePath
        End With
        Try
            oInsStdRate = m_controller.getPolicyUpload(oInsStdRate)
            If Not oInsStdRate Is Nothing Then
                dtEntity = oInsStdRate.ListData
                recordCount = oInsStdRate.TotalRecords
            Else
                recordCount = 0
            End If
        Catch ex As Exception
            LblErrorMessages.Text = ex.Message
            LblErrorMessages.Visible = True
        End Try
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(PageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString
        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy)
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim chk1 As CheckBox
            Dim lblAgreementNo As Label
            Dim lblPolicyNumber As Label
            Dim i As Integer

            For i = 0 To dtgPaging.Items.Count - 1
                chk1 = CType(dtgPaging.Items(i).Cells(0).FindControl("ChkStatusAccountPayableNo"), CheckBox)
                If chk1.Checked Then
                    lblAgreementNo = CType(dtgPaging.Items(i).Cells(1).FindControl("lblAgreementNo"), Label)
                    lblPolicyNumber = CType(dtgPaging.Items(i).Cells(2).FindControl("lblPolicyNumber"), Label)
                    m_controller.savePolicyUpload(GetConnectionString, lblAgreementNo.Text.Trim, _
                        lblPolicyNumber.Text.Trim, _
                        ConvertDate2(txtsdate.Text), _
                        Me.Loginid)
                End If
            Next
        Catch ex As Exception
            LblErrorMessages.Text = ex.Message
            LblErrorMessages.Visible = True
        Finally
            BindGridEntity(Me.SearchBy)
        End Try
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                Dim lblStartPeriod As Label
                Dim lblEndPeriod As Label
                Dim lblStartPeriod1 As Label
                Dim lblEndPeriod1 As Label
                Dim lblTenor As Label
                Dim lblTenor1 As Label
                Dim lblSumCover As Label
                Dim lblSumCover1 As Label
                Dim lblPremi As Label
                Dim lblPremi1 As Label

                lblStartPeriod = CType(e.Item.FindControl("lblStartPeriod"), Label)
                lblEndPeriod = CType(e.Item.FindControl("lblEndPeriod"), Label)
                lblStartPeriod1 = CType(e.Item.FindControl("lblStartPeriod1"), Label)
                lblEndPeriod1 = CType(e.Item.FindControl("lblEndPeriod1"), Label)
                lblTenor = CType(e.Item.FindControl("lblTenor"), Label)
                lblTenor1 = CType(e.Item.FindControl("lblTenor1"), Label)
                lblSumCover = CType(e.Item.FindControl("lblSumCover"), Label)
                lblSumCover1 = CType(e.Item.FindControl("lblSumCover1"), Label)
                lblPremi = CType(e.Item.FindControl("lblPremi"), Label)
                lblPremi1 = CType(e.Item.FindControl("lblPremi1"), Label)


                If (lblStartPeriod.Text = lblStartPeriod1.Text) And _
                    (lblEndPeriod.Text = lblEndPeriod1.Text) And _
                    (lblTenor.Text = lblTenor1.Text) And _
                    (lblSumCover.Text = lblSumCover1.Text) And _
                    (lblPremi.Text = lblPremi1.Text) Then
                    'e.Item.BackColor = Color.White
                    'e.Item.Style.Add("background-color", "#f9f9f9 !important")
                    e.Item.Attributes.Add("class", "item_grid")
                Else
                    'e.Item.BackColor = Color.FromName("#ffcccc")
                    e.Item.Style.Add("background-color", "#ffcccc !important")
                End If
        End Select
    End Sub

End Class