﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PolicyReceiveDownload.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.PolicyReceiveDownload" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../Webform.UserController/ValidDate.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Policy Receive Download</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="LblErrorMessages" runat="server"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div> 
        <div class="form_single">
            <h4>REGISTER POLIS ASURANSI</h4>
        </div>
    </div>
    <asp:Panel ID="pnlUpload" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>Upload polis
                </label>
                 <input id="fileUpload" type="file" name="fileUpload" runat="server" />
            </div>
        </div>
        <div class ="form_button">
            <asp:Button ID="btnUpload" runat="server" CssClass="small button blue" Text="Upload"></asp:Button>
        </div>
        
    </asp:Panel>
    <asp:Panel ID="pnlGrid" runat="server" Visible="False">
        <div class="form_title">          
            <div class="form_single">
                <h4>
                    DAFTAR POLIS ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Terima</label>
                <asp:TextBox runat="server" ID="txtsdate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtsdate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="kontrak" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <%--<ItemStyle CssClass="item_grid" />--%>
                        <Columns>
                            <asp:TemplateColumn>
                                <HeaderStyle Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkAll" runat="server" OnCheckedChanged="ChkStatus_CheckedChanged"
                                        AutoPostBack="True"></asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="ChkStatusAccountPayableNo" runat="server"></asp:CheckBox>
                                    <br />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="KONTRAK" HeaderText="NO. KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAgreementNo" runat="server" Text='<%#  container.dataitem("kontrak") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="NO POLIS" HeaderText="SERTIFIKAT">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPolicyNumber" runat="server" Text='<%#  container.dataitem("sertifikat") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="branchFullName" HeaderText="CABANG">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#  container.dataitem("branchFullName") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PERIODE_START" HeaderText="PERIODE START">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblStartPeriod" runat="server" Text='<%#  format(container.dataitem("PERIODE_START"),"dd/MM/yyyy") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PERIODE_END" HeaderText="PERIODE END">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblEndPeriod" runat="server" Text='<%#  format(container.dataitem("PERIODE_END"),"dd/MM/yyyy") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="startDate" HeaderText="START DATE (COMPANY)">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblStartPeriod1" runat="server" Text='<%#  format(container.dataitem("startDate"),"dd/MM/yyyy") %>'>
                                    </asp:Label>--%>
                                    <asp:Label ID="lblStartPeriod1" runat="server" Text='<%#  format(container.dataitem("startDate"), "dd/MM/yyyy") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="EndDate" HeaderText="END DATE (COMPANY)">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblEndPeriod1" runat="server" Text='<%#  format(container.dataitem("EndDate"),"dd/MM/yyyy") %>'>
                                    </asp:Label>--%>
                                    <asp:Label ID="lblEndPeriod1" runat="server" Text='<%#  format(container.dataitem("EndDate"), "dd/MM/yyyy") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="TERMS" HeaderText="TENOR">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTenor" runat="server" Text='<%#  container.dataitem("terms") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InsLength" HeaderText="TENOR (COMPANY)">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTenor1" runat="server" Text='<%#  container.dataitem("InsLength") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="tsi" HeaderText="SUM COVERAGE">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblSumCover" runat="server" Text='<%#  formatnumber(container.dataitem("tsi"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="SumInsured" HeaderText="SUM COVERAGE (COMPANY)">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblSumCover1" runat="server" Text='<%#  formatnumber(container.dataitem("SumInsured"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PREMI" HeaderText="PREMI">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPremi" runat="server" Text='<%#  formatnumber(container.dataitem("PREMI"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="MainPremiumToInsco" HeaderText="PREMI (COMPANY)">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPremi1" runat="server" Text='<%#  formatnumber(container.dataitem("MainPremiumToInsco"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="PremiumAmountToAgent" HeaderText="PREMI AGENT">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label12" runat="server" Text='<%#  formatnumber(container.dataitem("PremiumAmountToAgent"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server"  Width="34px">1</asp:TextBox>
                        <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server"  CssClass="validator_general"  Type="Integer" MaximumValue="999999999" ErrorMessage="No Halaman Salah"
                            MinimumValue="1" ControlToValidate="txtGopage"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server"  CssClass="validator_general"  ErrorMessage="No Halaman Salah" ControlToValidate="txtGopage"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
        </div>        
    </asp:Panel>
    </form>
</body>
</html>
