﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ClaimActivity
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oReason As UCReason
#Region "Constanta"
    Dim m_ClaimAct As New InsClaimActController
    Private oClaimAct As New Parameter.InsClaimAct
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    'Protected WithEvents txtClaimamountApprove As ucNumberFormat
#End Region
#Region "properties"
    Public Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return CType(viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Public Property InsClaimSeqNo() As String
        Get
            Return CType(viewstate("InsClaimSeqNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsClaimSeqNo") = Value
        End Set
    End Property
    Public Property customerID() As String
        Get
            Return CType(viewstate("customerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("customerID") = Value
        End Set
    End Property
    Public Property InsSequenceNo() As String
        Get
            Return CType(viewstate("InsSequenceNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsSequenceNo") = Value
        End Set
    End Property
    Public Property AssetSequenceNo() As String
        Get
            Return CType(viewstate("AssetSequenceNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetSequenceNo") = Value
        End Set
    End Property

    Public Property ClaimType() As String
        Get
            Return CType(viewstate("ClaimType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ClaimType") = Value
        End Set
    End Property
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblRecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.FormID = "InsClaimAct"
        If Not Page.IsPostBack Then
            fillcbo()
            oReason.ReasonTypeID = "CLAIM"
            oReason.BindReason()
        End If

        If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            InitialDefaultPanel()
        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If

        txtClaimamountApprove.Attributes.Add("onKeyPress", "return(currencyFormatku(this,event))")
        'oReason.ReasonTypeID = "CLAIM"
        'oReason.BindReason()
    End Sub
#End Region
#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlSelect.Visible = False
        pnlReject.Visible = False
        pnlAccept.Visible = False
    End Sub
#End Region

    Sub fillcbo()
        Dim m_controller As New DataUserControlController
        With ddlBranch
            If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "ALL")
                .Items(0).Value = "ALL"
            Else
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                Dim strBranch() As String
                strBranch = Split(Me.sesBranchId, ",")
                If UBound(strBranch) > 0 Then
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                End If
            End If
        End With
    End Sub

    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Me.SearchBy = ""
        Me.SortBy = ""

        If ddlBranch.SelectedValue <> "" And ddlBranch.SelectedValue <> "ALL" Then
            Me.SearchBy = "BranchID='" & ddlBranch.SelectedValue & "'"
        End If
        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
            Else
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
            End If
        End If
        Context.Trace.Write("Me.SearchBy = " & Me.SearchBy)
        Bindgrid(Me.SearchBy, Me.SortBy)

    End Sub

    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable

        With oClaimAct
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oClaimAct = m_ClaimAct.ClaimActList(oClaimAct)

        With oClaimAct
            lblRecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oClaimAct.ListData
        dtsEntity.TableName = "Table"

        dtvEntity = dtsEntity.DefaultView

        dtgClaimActivityList.DataSource = dtvEntity

        If Not dtvEntity Is Nothing Then
            dtgClaimActivityList.DataBind()
        End If

        PagingFooter()
        pnlDtGrid.Visible = True
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        ddlBranch.ClearSelection()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Context.Trace.Write("imbSave")
        Dim intloop As Integer
        Dim strerrmsg As String
        Dim strReceivedFrom, strReceivedocDate, strOriginalDoc, strCopyDoc, strInsDocID As String
        Context.Trace.Write("txtInscoApprove.text.Trim = " & txtInscoApprove.Text.Trim)
        Context.Trace.Write("txtCustomerApprove.text.Trim = " & txtCustomerApprove.Text.Trim)
        Context.Trace.Write("txtLetterFromKadit.dateValue.Trim = " & txtLetterFromKadit.Text.Trim)

        If txtInscoApprove.Text.Trim <> "" Or txtCustomerApprove.Text.Trim <> "" Or txtLetterFromKadit.Text <> "" Then
            If txtInscoApprove.Text.Trim <> "" Then
                If ConvertDate2(txtInscoApprove.Text.Trim) > Me.BusinessDate Then
                    pnlDtGrid.Visible = False
                    pnlsearch.Visible = False
                    pnlSelect.Visible = True

                    ShowMessage(lblMessage, "Tanggal Approve Pers Asuransi harus < Hari ini", True)
                    txtInscoApprove.Enabled = True
                    Exit Sub
                End If
            End If
            If txtCustomerApprove.Text.Trim <> "" Then
                If ConvertDate2(txtCustomerApprove.Text.Trim) > Me.BusinessDate Then
                    pnlDtGrid.Visible = False
                    pnlsearch.Visible = False
                    pnlSelect.Visible = True

                    ShowMessage(lblMessage, "Tanggal Approve Customer harus < hari ini", True)
                    txtCustomerApprove.Enabled = True
                    Exit Sub
                End If
            End If
            If txtLetterFromKadit.Text <> "" Then
                If ConvertDate2(txtLetterFromKadit.Text.Trim) > Me.BusinessDate Then
                    pnlDtGrid.Visible = False
                    pnlsearch.Visible = False
                    pnlSelect.Visible = True

                    ShowMessage(lblMessage, "Tanggal surat dari Kaditserse harus < hari ini", True)
                    txtLetterFromKadit.Enabled = True
                    Exit Sub
                End If
            End If

        End If



        Dim dttable As New DataTable
        Dim objrow As DataRow
        dttable.Columns.Add("Original", GetType(String))
        dttable.Columns.Add("Copy", GetType(String))
        dttable.Columns.Add("ReceivedDate", GetType(Date))
        dttable.Columns.Add("ReceivedFrom", GetType(String))
        dttable.Columns.Add("InsDocID", GetType(Integer))

        With dtgClaimDoc.Items
            For intloop = 0 To .Count - 1
                objrow = dttable.NewRow
                If CType(dtgClaimDoc.Items(intloop).FindControl("chkbOriginal"), CheckBox).Checked Then
                    objrow("Original") = "Y"
                Else
                    objrow("Original") = "N"
                End If

                If CType(dtgClaimDoc.Items(intloop).FindControl("chkbCopy"), CheckBox).Checked Then
                    objrow("Copy") = "Y"
                Else
                    objrow("Copy") = "N"
                End If

                If CType(dtgClaimDoc.Items(intloop).FindControl("txtReceiveDoc"), TextBox).Text = "" Then
                    objrow("ReceivedDate") = CDate("01/01/1900")
                Else
                    objrow("ReceivedDate") = ConvertDate2(CType(dtgClaimDoc.Items(intloop).FindControl("txtReceiveDoc"), TextBox).Text)
                End If


                If CType(dtgClaimDoc.Items(intloop).FindControl("txtReceiveFrom"), TextBox).Text.Trim = "" Then
                    objrow("ReceivedFrom") = "-"
                Else
                    objrow("ReceivedFrom") = CType(dtgClaimDoc.Items(intloop).FindControl("txtReceiveFrom"), TextBox).Text.Trim
                End If
                objrow("InsDocID") = dtgClaimDoc.Items(intloop).Cells(0).Text.Trim
                dttable.Rows.Add(objrow)





            Next
        End With

        With oClaimAct
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .InsSequenceNo = CInt(Me.InsSequenceNo)
            .InsClaimSeqNo = Me.InsClaimSeqNo
            .BusinessDate = Me.BusinessDate
            .Activity = txtClaimAct.Text
            .Result = txtResult.Text
            .LoginId = Me.Loginid
            .Notes = txtNotes.Text
            .AssetSequenceNo = CInt(Me.AssetSequenceNo)
            If txtCustomerApprove.Text = "" Then
                .OKCustDate = "01/01/1900"
            Else
                .OKCustDate = txtCustomerApprove.Text
            End If
            If txtInscoApprove.Text = "" Then
                .OKInscoDate = "01/01/1900"
            Else
                .OKInscoDate = txtInscoApprove.Text
            End If
            If txtLetterFromKadit.Text = "" Then
                .LetterFromKaditserseDate = "01/01/1900"
            Else
                .LetterFromKaditserseDate = txtLetterFromKadit.Text

            End If
            .ClaimAmount = CInt(txtClaimamountApprove.Text.Trim)
            .ListData = dttable
        End With



        If m_ClaimAct.ClaimActsave(oClaimAct) Then
            ShowMessage(lblMessage, "Simpan Data Berhasil", False)
        Else
            ShowMessage(lblMessage, "Simpan Data Gagal", True)
        End If

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        InitialDefaultPanel()
        pnlDtGrid.Visible = True
    End Sub

    Private Sub dtgClaimActivityList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgClaimActivityList.ItemCommand
        'Me.BranchID = e.Item.Cells(2).Text.Trim
        'Me.ApplicationID = e.Item.Cells(3).Text.Trim
        'Me.customerID = e.Item.Cells(4).Text.Trim
        'Me.AgreementNo = e.Item.Cells(5).Text.Trim
        'Me.CustomerName = e.Item.Cells(6).Text.Trim
        'Me.InsClaimSeqNo = e.Item.Cells(7).Text.Trim
        'Me.InsSequenceNo = e.Item.Cells(8).Text.Trim
        'Me.AssetSequenceNo = e.Item.Cells(9).Text.Trim
        'Me.ClaimType = e.Item.Cells(16).Text.Trim

        Me.BranchID = e.Item.Cells(3).Text.Trim
        Me.ApplicationID = e.Item.Cells(4).Text.Trim
        Me.customerID = e.Item.Cells(5).Text.Trim
        Me.AgreementNo = e.Item.Cells(6).Text.Trim
        Me.CustomerName = e.Item.Cells(7).Text.Trim
        Me.InsClaimSeqNo = e.Item.Cells(8).Text.Trim
        Me.InsSequenceNo = e.Item.Cells(9).Text.Trim
        Me.AssetSequenceNo = e.Item.Cells(10).Text.Trim
        Me.ClaimType = e.Item.Cells(17).Text.Trim

        'Dim LblClaimType As Label = CType(dtgClaimActivityList.Items(e.Item.ItemIndex).FindControl("LblClaimType"), Label)
        'Me.ClaimType = LblClaimType.Text

        Select Case e.CommandName
            Case "Act"
                If checkFeature(Me.Loginid, Me.FormID, "Act", Me.AppId) Then
                    If sessioninvalid() Then
                        Dim strHTTPServer As String
                        Dim strHTTPApp As String
                        Dim strNameServer As String
                        strHTTPServer = Request.ServerVariables("PATH_INFO")
                        strNameServer = Request.ServerVariables("SERVER_NAME")
                        strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                        Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
                    Else
                        viewRepoDetail()
                    End If
                End If
            Case "Rej"
                If checkFeature(Me.Loginid, Me.FormID, "Rej", Me.AppId) Then
                    If sessioninvalid() Then
                        Dim strHTTPServer As String
                        Dim strHTTPApp As String
                        Dim strNameServer As String
                        strHTTPServer = Request.ServerVariables("PATH_INFO")
                        strNameServer = Request.ServerVariables("SERVER_NAME")
                        strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                        Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
                    Else
                        viewRejectDetail()
                    End If
                End If
            Case "Acp"
                If CheckFeature(Me.Loginid, Me.FormID, "Acp", Me.AppId) Then
                    If SessionInvalid() Then
                        Dim strHTTPServer As String
                        Dim strHTTPApp As String
                        Dim strNameServer As String
                        strHTTPServer = Request.ServerVariables("PATH_INFO")
                        strNameServer = Request.ServerVariables("SERVER_NAME")
                        strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                        Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
                    Else
                        viewAcceptDetail()
                    End If
                End If
        End Select
    End Sub
#Region "ViewRejectDetail"
    Private Sub viewRejectDetail()
        context.Trace.Write("viewRejectDetail()")
        clear()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = False
        pnlReject.Visible = True
        pnlAccept.Visible = False

        Dim dtsEntity As New DataTable
        Dim dtvEntity As New DataView

        Dim dt As New DataTable


        With oClaimAct
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .BusinessDate = Me.BusinessDate
            .AssetSequenceNo = CInt(Me.AssetSequenceNo)
            .InsSequenceNo = CInt(Me.InsSequenceNo)
            .ClaimType = Me.ClaimType
            .InsClaimSeqNo = Me.InsClaimSeqNo
        End With

        oClaimAct = m_ClaimAct.ClaimActDetail(oClaimAct)
        dt = oClaimAct.ListData
        btnSave.Visible = True
        If dt.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            btnSave.Visible = False
            Exit Sub
        End If
        With dt.Rows(0)
            lblRejectClaimType.Text = CStr(IIf(IsDBNull(.Item("ClaimType")), "", .Item("ClaimType")))
            lblRejectCoverage.Text = CStr(IIf(IsDBNull(.Item("MainCoverage")), "", .Item("MainCoverage")))
            lblRejectInsPeriod.Text = CStr(IIf(IsDBNull(.Item("Period")), "", .Item("Period")))
            lblRejectInsCo.Text = CStr(IIf(IsDBNull(.Item("InsCo")), "", .Item("InsCo")))
            lblRejectPolicy.Text = CStr(IIf(IsDBNull(.Item("PolicyNumber")), "", .Item("PolicyNumber")))
            lblRejectAsset.Text = CStr(IIf(IsDBNull(.Item("Description")), "", .Item("Description")))
            lblRejectAmount.Text = FormatNumber(CStr(IIf(IsDBNull(.Item("ClaimAmount")), "", .Item("ClaimAmount"))), 0)
            lblRejectClaimDate.Text = CStr(IIf(IsDBNull(.Item("ClaimDate")), "", .Item("ClaimDate")))
            lblRejectDefect.Text = CStr(IIf(IsDBNull(.Item("DefectPart")), "", .Item("DefectPart")))
        End With
        txtRejectDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")


    End Sub
#End Region

#Region "ViewRepoDetail"
    Private Sub viewRepoDetail()
        context.Trace.Write("viewRepoDetail()")
        clear()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = False
        pnlSelect.Visible = True
        pnlAccept.Visible = False

        Dim dtsEntity As New DataTable
        Dim dtvEntity As New DataView

        Dim dt As New DataTable



        With oClaimAct
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .BusinessDate = Me.BusinessDate
            .AssetSequenceNo = CInt(Me.AssetSequenceNo)
            .InsSequenceNo = CInt(Me.InsSequenceNo)
            .ClaimType = Me.ClaimType
            .InsClaimSeqNo = Me.InsClaimSeqNo
        End With

        oClaimAct = m_ClaimAct.ClaimActDetail(oClaimAct)
        dt = oClaimAct.ListData
        btnSave.Visible = True
        If dt.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            btnSave.Visible = False
            Exit Sub
        End If
        With dt.Rows(0)
            lblClaimType.Text = CStr(IIf(IsDBNull(.Item("ClaimType")), "", .Item("ClaimType")))
            lblCoverageName.Text = CStr(IIf(IsDBNull(.Item("MainCoverage")), "", .Item("MainCoverage")))
            lblInsurancePeriode.Text = CStr(IIf(IsDBNull(.Item("Period")), "", .Item("Period")))
            lblInsCo.Text = CStr(IIf(IsDBNull(.Item("InsCo")), "", .Item("InsCo")))
            lblPolicyNo.Text = CStr(IIf(IsDBNull(.Item("PolicyNumber")), "", .Item("PolicyNumber")))
            lblAsset.Text = CStr(IIf(IsDBNull(.Item("Description")), "", .Item("Description")))
            lblProsesToKadit.Text = CStr(IIf(IsDBNull(.Item("ProcessToKaditserseDate")), "", .Item("ProcessToKaditserseDate")))
            txtLetterFromKadit.Text = CStr(IIf(IsDBNull(.Item("LetterFromKaditserseDate")), "", .Item("LetterFromKaditserseDate")))
            Context.Trace.Write("txtLetterFromKadit.text.Trim = " & txtLetterFromKadit.Text.Trim)
            If txtLetterFromKadit.Text.Trim <> "" Then
                txtLetterFromKadit.ReadOnly = True
                Context.Trace.Write("txtLetterFromKadit.readonly=truee")
            Else
                txtLetterFromKadit.Enabled = True
                Context.Trace.Write("txtLetterFromKadit.Enable=true")
            End If

            txtInscoApprove.Text = CStr(IIf(IsDBNull(.Item("OKInscoDate")), "", .Item("OKInscoDate")))

            Context.Trace.Write("txtInscoApprove.t.Trim ext = " & txtInscoApprove.Text.Trim)

            If txtInscoApprove.Text.Trim <> "" Then
                txtInscoApprove.ReadOnly = True
                RvClaimAmountApprove.Enabled = True
                txtClaimamountApprove.Enabled = True
                'txtClaimamountApprove.Enabled = True

                Context.Trace.Write("txtClaimamountApprove.ReadOnly = False ")
                Context.Trace.Write("txtClaimamountApprove.Enabled = True")
                Context.Trace.Write("txtInscoApprove.readonly=true")
            Else
                txtInscoApprove.Enabled = True
                RvClaimAmountApprove.Enabled = False
                txtClaimamountApprove.Enabled = False
                'txtClaimamountApprove.Enabled = False

                Context.Trace.Write("txtClaimamountApprove.ReadOnly = True ")
                Context.Trace.Write("txtClaimamountApprove.Enabled = False")
                Context.Trace.Write("txtInscoApprove.Enable=true")

            End If

            txtCustomerApprove.Text = CStr(IIf(IsDBNull(.Item("OKCustDate")), "", .Item("OKCustDate")))
            Context.Trace.Write("txtCustomerApprove.text.Trim  = " & txtCustomerApprove.Text.Trim)

            If txtCustomerApprove.Text.Trim <> "" Then
                txtCustomerApprove.ReadOnly = True
                Context.Trace.Write("txtCustomerApprove.readonly=true")
            Else
                txtCustomerApprove.Enabled = True
                Context.Trace.Write("txtCustomerApprove.Enable=true")
            End If

            TxtClaimAmounRequestByCust.Text = CStr(IIf(IsDBNull(.Item("ClaimAmountByCust")), "", FormatNumber(.Item("ClaimAmountByCust"), 2)))
            txtClaimamountApprove.Text = CStr(IIf(IsDBNull(.Item("ClaimAmount")), "", FormatNumber(.Item("ClaimAmount"), 2)))

            context.Trace.Write("txtClaimamountApprove.Text = " & txtClaimamountApprove.Text)


        End With

        '==============================================================

        With oClaimAct
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .InsClaimSeqNo = Me.InsClaimSeqNo
            .AssetSequenceNo = CInt(Me.AssetSequenceNo)
            .InsSequenceNo = CInt(Me.InsSequenceNo)
            .ClaimType = Me.ClaimType
        End With


        oClaimAct = m_ClaimAct.ClaimActGetDoc(oClaimAct)
        dtsEntity = oClaimAct.ListData
        dtsEntity.TableName = "Table"

        dtvEntity = dtsEntity.DefaultView

        dtgClaimDoc.DataSource = dtvEntity

        If Not dtvEntity Is Nothing Then
            dtgClaimDoc.DataBind()
        End If

    End Sub
#End Region

#Region "Clear"


    Sub clear()
        lblClaimType.Text = ""
        lblCoverageName.Text = ""
        lblInsurancePeriode.Text = ""
        lblInsCo.Text = ""
        lblPolicyNo.Text = ""
        lblAsset.Text = ""
        lblProsesToKadit.Text = ""
        txtLetterFromKadit.Text = ""
        txtInscoApprove.Text = ""
        txtCustomerApprove.Text = ""
        txtClaimAct.Text = ""
        txtResult.Text = ""
        txtNotes.Text = ""
        txtClaimamountApprove.Text = "0"

        lblRejectClaimType.Text = ""
        lblRejectCoverage.Text = ""
        lblRejectInsPeriod.Text = ""
        lblRejectInsCo.Text = ""
        lblRejectPolicy.Text = ""
        lblRejectAsset.Text = ""
        lblRejectAmount.Text = ""
        lblRejectClaimDate.Text = ""
        lblRejectDefect.Text = ""
    End Sub
#End Region

    Private Sub dtgClaimDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgClaimDoc.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim txtReceiveDocDate As TextBox = CType(e.Item.FindControl("txtReceiveDoc"), TextBox)

            If ((e.Item.Cells(4).Text.Trim <> "" And e.Item.Cells(4).Text.Trim <> "&nbsp;") And e.Item.Cells(5).Text.Trim <> "-") Or ((e.Item.Cells(4).Text.Trim = "" Or e.Item.Cells(4).Text.Trim = "&nbsp;") And e.Item.Cells(5).Text.Trim <> "-") Or ((e.Item.Cells(4).Text.Trim <> Nothing And e.Item.Cells(4).Text.Trim <> "&nbsp;") And e.Item.Cells(5).Text.Trim = "-") Or e.Item.Cells(2).Text = "Y" Or e.Item.Cells(3).Text = "Y" Then
                Dim chkboriginal As CheckBox = CType(e.Item.FindControl("chkbOriginal"), CheckBox)
                Dim chkbcopy As CheckBox = CType(e.Item.FindControl("chkbCopy"), CheckBox)
                Dim txtReceiveFrom As TextBox = CType(e.Item.FindControl("txtReceiveFrom"), TextBox)
                With e.Item
                    If (e.Item.Cells(4).Text = "" Or e.Item.Cells(4).Text = "&nbsp;") Then
                        txtReceiveDocDate.Text = ""
                        Context.Trace.Write("txtReceiveDocDate.text = " & txtReceiveDocDate.Text)
                    Else
                        txtReceiveDocDate.Text = .Cells(4).Text
                        Context.Trace.Write("txtReceiveDocDate.text = " & txtReceiveDocDate.Text)
                    End If

                    txtReceiveDocDate.ReadOnly = True

                    txtReceiveFrom.Text = .Cells(5).Text
                    txtReceiveFrom.Enabled = False
                    If .Cells(2).Text.Trim = "Y" Then
                        chkboriginal.Checked = True
                    End If
                    If .Cells(3).Text.Trim = "Y" Then
                        chkbcopy.Checked = True
                    End If
                End With
                chkboriginal.Enabled = False
                chkbcopy.Enabled = False
            Else

                Dim chkboriginal As CheckBox = CType(e.Item.FindControl("chkbOriginal"), CheckBox)
                Dim chkbcopy As CheckBox = CType(e.Item.FindControl("chkbCopy"), CheckBox)
                Dim txtReceiveFrom As TextBox = CType(e.Item.FindControl("txtReceiveFrom"), TextBox)


                With e.Item
                    txtReceiveDocDate.Text = ""
                    txtReceiveDocDate.Enabled = True
                    txtReceiveFrom.Text = .Cells(5).Text
                    txtReceiveFrom.Enabled = True
                End With
                chkboriginal.Enabled = True
                chkbcopy.Enabled = True

            End If
        End If
    End Sub

    Private Sub btnRejectSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRejectSave.Click
        If oReason.ReasonID.Trim = "" Then
            Response.Write("<script language='javascript'>")
            Response.Write("alert('Please Fill The Reason ')")
            Response.Write("</script>")
            pnlDtGrid.Visible = False
            pnlsearch.Visible = False
            pnlReject.Visible = True
            pnlAccept.Visible = False
            Exit Sub
        End If
        If txtRejectDate.Text.Trim = "" Then
            Response.Write("<script language='javascript'>")
            Response.Write("alert('Please Fill Reject Date')")
            Response.Write("</script>")
            pnlDtGrid.Visible = False
            pnlsearch.Visible = False
            pnlReject.Visible = True
            pnlAccept.Visible = False
            Exit Sub
        End If
        If ConvertDate2(txtRejectDate.Text.Trim) < Me.BusinessDate Then
            lblMessage.Visible = True
            ShowMessage(lblMessage, "Tanggal Reject harus < hari ini ", True)
            pnlDtGrid.Visible = False
            pnlsearch.Visible = False
            pnlReject.Visible = True
            pnlAccept.Visible = False
            Exit Sub
        End If
        With oClaimAct
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .InsSequenceNo = CInt(Me.InsSequenceNo)
            .InsClaimSeqNo = Me.InsClaimSeqNo
            .AssetSequenceNo = CInt(Me.AssetSequenceNo)
            .Notes = txtRejectNotes.Text.Trim
            .ReasonID = oReason.ReasonID
            .ActivityDate = txtRejectDate.Text
            Context.Trace.Write("oReason.ReasonID = " & oReason.ReasonID)
            Context.Trace.Write("oReason.Description = " & oReason.Description)
        End With
        If m_ClaimAct.ClaimRejectsave(oClaimAct) Then
            ShowMessage(lblMessage, "Simpan Data Berhasil", False)
        Else
            ShowMessage(lblMessage, "Simpan Data Gagal", True)
        End If

    End Sub

    Private Sub btnRejectCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRejectCancel.Click
        InitialDefaultPanel()
        pnlDtGrid.Visible = True
    End Sub

#Region "ViewAcceptDetail"
    Private Sub viewAcceptDetail()
        Context.Trace.Write("viewAcceptDetail()")
        clear()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = False
        pnlReject.Visible = False
        pnlAccept.Visible = True

        Dim dtsEntity As New DataTable
        Dim dtvEntity As New DataView

        Dim dt As New DataTable


        With oClaimAct
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .BusinessDate = Me.BusinessDate
            .AssetSequenceNo = CInt(Me.AssetSequenceNo)
            .InsSequenceNo = CInt(Me.InsSequenceNo)
            .ClaimType = Me.ClaimType
            .InsClaimSeqNo = Me.InsClaimSeqNo
        End With

        oClaimAct = m_ClaimAct.ClaimActDetail(oClaimAct)
        dt = oClaimAct.ListData
        btnSave.Visible = True
        If dt.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            btnSave.Visible = False
            Exit Sub
        End If
        With dt.Rows(0)
            lblAcceptClaimType.Text = CStr(IIf(IsDBNull(.Item("ClaimType")), "", .Item("ClaimType")))
            lblAcceptCoverage.Text = CStr(IIf(IsDBNull(.Item("MainCoverage")), "", .Item("MainCoverage")))
            lblAcceptInsPeriod.Text = CStr(IIf(IsDBNull(.Item("Period")), "", .Item("Period")))
            lblAcceptInsCo.Text = CStr(IIf(IsDBNull(.Item("InsCo")), "", .Item("InsCo")))
            lblAcceptPolicy.Text = CStr(IIf(IsDBNull(.Item("PolicyNumber")), "", .Item("PolicyNumber")))
            lblAcceptAsset.Text = CStr(IIf(IsDBNull(.Item("Description")), "", .Item("Description")))
            lblAcceptAmount.Text = FormatNumber(CStr(IIf(IsDBNull(.Item("ClaimAmount")), "", .Item("ClaimAmount"))), 0)
            lblAcceptClaimDate.Text = CStr(IIf(IsDBNull(.Item("ClaimDate")), "", .Item("ClaimDate")))
        End With
    End Sub
#End Region

    Private Sub btnAcceptSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAcceptSave.Click
        With oClaimAct
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .InsSequenceNo = CInt(Me.InsSequenceNo)
            .InsClaimSeqNo = Me.InsClaimSeqNo
            .AssetSequenceNo = CInt(Me.AssetSequenceNo)
            .Notes = txtAcceptNotes.Text.Trim
            .ActivityDate = txtAcceptDate.Text

        End With
        If m_ClaimAct.ClaimAcceptsave(oClaimAct) Then
            ShowMessage(lblMessage, "Simpan Data Berhasil", False)
        Else
            ShowMessage(lblMessage, "Simpan Data Gagal", True)
        End If

    End Sub

    Private Sub btnAcceptCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAcceptCancel.Click
        InitialDefaultPanel()
        pnlDtGrid.Visible = True
    End Sub
End Class