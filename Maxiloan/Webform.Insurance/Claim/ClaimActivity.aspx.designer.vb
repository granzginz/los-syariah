﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ClaimActivity

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlsearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlsearch As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ddlBranch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlBranch As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Requiredfieldvalidator1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Requiredfieldvalidator1 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''cboSearchBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSearchBy As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtSearchBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearchBy As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnsearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnsearch As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnReset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnReset As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlDtGrid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlDtGrid As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''dtgClaimActivityList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtgClaimActivityList As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''imbFirstPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imbFirstPage As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''imbPrevPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imbPrevPage As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''imbNextPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imbNextPage As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''imbLastPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imbLastPage As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''txtPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPage As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''BtnGoPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnGoPage As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''rgvGo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rgvGo As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''rfvGo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfvGo As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''lblPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTotPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotPage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRecord control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRecord As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlSelect control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSelect As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblInsCo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInsCo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPolicyNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPolicyNo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAsset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAsset As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCoverageName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCoverageName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblInsurancePeriode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInsurancePeriode As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblClaimType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClaimType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblProsesToKadit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProsesToKadit As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtClaimAct control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtClaimAct As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''rfvClaimAct control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfvClaimAct As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''txtResult control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtResult As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''rfvResult control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfvResult As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''txtNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNotes As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtLetterFromKadit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtLetterFromKadit As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender1 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''txtInscoApprove control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtInscoApprove As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender2 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''TxtClaimAmounRequestByCust control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TxtClaimAmounRequestByCust As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtClaimamountApprove control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtClaimamountApprove As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''RvClaimAmountApprove control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RvClaimAmountApprove As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''txtCustomerApprove control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCustomerApprove As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender3 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''dtgClaimDoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtgClaimDoc As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCancel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlReject control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlReject As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblRejectInsCo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRejectInsCo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRejectPolicy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRejectPolicy As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRejectAsset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRejectAsset As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRejectInsPeriod control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRejectInsPeriod As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRejectCoverage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRejectCoverage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRejectClaimType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRejectClaimType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRejectClaimDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRejectClaimDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRejectAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRejectAmount As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRejectDefect control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRejectDefect As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtRejectDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRejectDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender4 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''txtRejectNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRejectNotes As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnRejectSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRejectSave As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnRejectCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRejectCancel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlAccept control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAccept As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblAcceptInsCo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAcceptInsCo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAcceptPolicy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAcceptPolicy As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAcceptAsset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAcceptAsset As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAcceptInsPeriod control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAcceptInsPeriod As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAcceptCoverage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAcceptCoverage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAcceptClaimType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAcceptClaimType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAcceptClaimDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAcceptClaimDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAcceptAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAcceptAmount As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtAcceptDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAcceptDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender5 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''txtAcceptNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAcceptNotes As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnAcceptSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAcceptSave As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnAcceptCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAcceptCancel As Global.System.Web.UI.WebControls.Button
End Class
