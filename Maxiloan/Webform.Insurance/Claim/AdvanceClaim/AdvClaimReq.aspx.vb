﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class AdvClaimReq
    Inherits Maxiloan.webform.WebBased
    Protected WithEvents oSearchBY As UcSearchBy

#Region "Constanta"    
    Private oInsAdvClaimReq As New Parameter.AdvClaimReq
    Private m_controller As New AdvClaimReqControler
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load        
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then

            txtPage.Text = "1"
            Me.FormID = "InsAdvClaimReq"
            oSearchBY.ListData = "AGREEMENTNO,Agreement No-Name, Customer Name"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                InitialDefaultPanel()
            End If
        End If
    End Sub

#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        PnlGrid.Visible = False
        pnlSearch.Visible = True

    End Sub

#End Region
#Region "Navigation "
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity()
    End Sub
#End Region
#Region "Paging Footer"

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)
            lblTotPage.Text = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblRecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
#End Region
#Region "Go Page"
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                BindGridEntity()
            End If
        End If
    End Sub

#End Region
#Region "Sorting"

    Public Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity()
    End Sub

#End Region
#Region "Reseh..eh Reset !"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImbReset.Click
        Me.SearchBy = " Agreement.BranchID = '" & Me.sesBranchId.Trim.Replace("'", "") & "' "
        Me.SortBy = ""
        BindGridEntity()
    End Sub
#End Region
    Sub BindGridEntity()
        With oInsAdvClaimReq
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
            .strConnection = GetConnectionString
        End With

        oInsAdvClaimReq = m_controller.AdvClaimReqPaging(oInsAdvClaimReq)

        recordCount = oInsAdvClaimReq.TotalRecord
        dtgPaging.DataSource = oInsAdvClaimReq.listData
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        pnlSearch.Visible = True
        PnlGrid.Visible = True
        PagingFooter()
    End Sub

    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearch.Click
        pnlSearch.Visible = True
        PnlGrid.Visible = True
        Dim strSearch As New StringBuilder
        strSearch.Append(" Agreement.BranchID = '" & Me.sesBranchId.Trim.Replace("'", "") & "' ")

        If oSearchBY.Text <> "" Then
            strSearch.Append(" and ")
            If Right(oSearchBY.Text, 1) = "%" Then
                strSearch.Append(oSearchBY.ValueID)
                strSearch.Append(" like '")
                strSearch.Append(oSearchBY.Text)
                strSearch.Append("'")
            Else
                strSearch.Append(oSearchBY.ValueID)
                strSearch.Append(" = '")
                strSearch.Append(oSearchBY.Text)
                strSearch.Append("'")
            End If
        End If
        Me.SearchBy = strSearch.ToString

        BindGridEntity()
    End Sub

    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim lblApplicationId As Label
        If e.CommandName = "Request" Then
            If sessioninvalid() Then
                Exit Sub
            End If
            If CheckFeature(Me.Loginid, Me.FormID, "Req", Me.AppId) Then
                lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
                Response.Redirect("AdvClaimReqDetail.aspx?ApplicationID=" & lblApplicationId.Text & "&BranchID=" & Me.sesBranchId.Replace("'", ""))
            End If
        End If
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim HypAgreement As HyperLink
        Dim HypCustomerName As HyperLink        
        Dim lblCustId As Label
        Dim lblApplicationId As Label

        If e.Item.ItemIndex >= 0 Then
            HypAgreement = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            If HypAgreement.Text.Trim.Length > 0 Then
                HypAgreement.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
            End If

            HypCustomerName = CType(e.Item.FindControl("hycustomerName"), HyperLink)
            lblCustId = CType(e.Item.FindControl("lblcustomerId"), Label)
            If HypCustomerName.Text.Trim.Length > 0 Then
                HypCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(lblCustId.Text.Trim) & "')"
            End If
        End If
    End Sub

    Private Sub dtgPaging_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity()
    End Sub
End Class