﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdvClaimReqDetail.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.AdvClaimReqDetail" %>

<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../../Webform.UserController/UcBankAccountID.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';


        function OpenWinViewPolicyNo(BranchID, AgreementNo, strasset, strins, strApplication) {
            var x = screen.width; var y = screen.height - 100; window.open('../../ViewPolicyDetail.aspx?BranchID=' + BranchID + '&AgreementNo=' + AgreementNo + '&AssetSeqNo=' + strasset + '&InsSeqNo=' + strins + '&ApplicationID=' + strApplication + '&Back=Refund' + '&style=Insurance', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }				
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" ForeColor="#993300" font-name="Verdana"
        Font-Size="11px"></asp:Label>
    <asp:Panel ID="pnlAdvanceClaimCostRequestDetail" runat="server">
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td class="tdtopikiri" width="10" height="20">&nbsp;
                    
                </td>
                <td class="tdtopi" align="center">
                    Advance Claim Cost Request Detail
                </td>
                <td class="tdtopikanan" width="10">&nbsp;
                    
                </td>
            </tr>
        </table>
        <table class="tablegrid" cellspacing="1" cellpadding="3" width="95%" align="center"
            border="0">
            <tr>
                <td class="tdgenap" style="width: 20%">
                    Agreement No
                </td>
                <td class="tdganjil">
                    <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
                </td>
                <td class="tdgenap" style="width: 20%">
                    Customer Name
                </td>
                <td class="tdganjil" width="30%">
                    <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" style="width: 20%; height: 18px">
                    Asset Description
                </td>
                <td class="tdganjil" style="height: 18px" align="left" width="30%" colspan="3">
                    <asp:Label ID="lblAssetDescription" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" style="width: 20%">
                    Insurance Co
                </td>
                <td class="tdganjil" align="left" width="30%">
                    <asp:Label ID="lblInsuranceCo" runat="server"></asp:Label>
                </td>
                <td class="tdgenap" style="width: 20%">
                    Policy No
                </td>
                <td class="tdganjil" align="left" width="30%">
                    <asp:HyperLink ID="hyPolicyNo" runat="server"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" style="width: 20%">
                    Coverage
                </td>
                <td class="tdganjil" align="left" width="30%">
                    <asp:Label ID="lblCoverage" runat="server"></asp:Label>
                </td>
                <td class="tdgenap" style="width: 20%">
                </td>
                <td class="tdganjil" align="left" width="30%">
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td>
                    <asp:DataGrid ID="dtgPaging" runat="server" PageSize="5" HorizontalAlign="Center"
                        CssClass="tablegrid" CellSpacing="1" CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False"
                        Width="100%">
                        <AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
                        <ItemStyle HorizontalAlign="Center" CssClass="tdganjil"></ItemStyle>
                        <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn SortExpression="No" HeaderText="NO">
                                <HeaderStyle HorizontalAlign="Center" Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="3%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="RequestDate" HeaderText="REQUEST DATE">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblRequestDate" runat="server" Text='<%#Container.DataItem("RequestDate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ClaimCost" HeaderText="CLAIM COST">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblClaimCost" runat="server" Text='<%#Formatnumber(Container.DataItem("ClaimCost"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("Status")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Notes" HeaderText="NOTES">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("Notes")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        <table class="tablegrid" cellspacing="1" cellpadding="3" width="95%" align="center"
            border="0">
            <tr>
                <td class="tdgenap" width="20%">
				 <label class ="label_req">
                    Advance Claim Cost Amount
				 </label>
                </td>
                <td class="tdganjil" align="left" width="80%">
                    <asp:TextBox ID="txtClaimCost" runat="server" ></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regClaimCostAmount" runat="server" Display="Dynamic"
                        Visible="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" Enabled="True"
                        ErrorMessage="You have to fill numeric value" ControlToValidate="txtClaimCost" CssClass="validator_general" ></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="ReqtxtClaimCost" runat="server" Width="128px" Display="Dynamic"
                        ControlToValidate="txtClaimCost" ErrorMessage="Claim Cost Must Be Fill" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </td>
                <tr>
                    <td class="tdgenap" width="20%">
                        Notes
                    </td>
                    <td class="tdganjil" align="left" width="80%">
                        <asp:TextBox ID="txtNotes" runat="server"  Width="232px" TextMode="MultiLine"
                            Height="26px"></asp:TextBox>
                    </td>
                    <tr>
                        <td class="tdgenap" width="20%">
						 <label class ="label_req">
                            Approve By
							</label>
                        </td>
                        <td class="tdganjil" align="left" width="80%">
                            <asp:DropDownList ID="cboApprovedBy" runat="server"  Width="104px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="ReqCboApprovedBy" runat="server" ErrorMessage="Please Select Approved By"
                                ControlToValidate="cboApprovedBy" Display="Dynamic" InitialValue="0" CssClass="validator_general" ></asp:RequiredFieldValidator>
                        </td>
                        <tr>
                            <td class="tdgenap" style="width: 20%">
                                Bank Account
                            </td>
                            <td class="tdganjil" align="left" width="80%">
                                <uc1:UcBankAccountID id="oBankAccount" runat="server"></uc1:UcBankAccountID>
                            </td>
                        </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlSave" runat="server">
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td align="left">
                    <asp:ImageButton ID="imbSave" runat="server" CausesValidation="True" ImageUrl="../../../Images/ButtonSave.gif">
                    </asp:ImageButton>&nbsp;
                    <asp:ImageButton ID="imbCancel" runat="server" CausesValidation="False" ImageUrl="../../../Images/ButtonCancel.gif">
                    </asp:ImageButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
