﻿
#Region "Import"
Imports System.Threading
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class InsAdvClaimReqView
    Inherits Maxiloan.webform.WebBased

#Region "Properties"
    Public Property ApplicationId() As String
        Get
            Return CType(viewstate("ApplicationId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationId") = Value
        End Set
    End Property
    Public Property ApprovalNo() As String
        Get
            Return CType(viewstate("ApprovalNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApprovalNo") = Value
        End Set
    End Property
    Public Property CustomerId() As String
        Get
            Return CType(viewstate("CustomerId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerId") = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value

        End Set
    End Property
    Public Property AssetSeqNo() As Integer
        Get
            Return CType(viewstate("AssetSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("AssetSeqNo") = Value
        End Set
    End Property

    Public Property InsSequenceNo() As Integer
        Get
            Return CType(viewstate("InsSequenceNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("InsSequenceNo") = Value
        End Set
    End Property


#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load        
        If sessioninvalid Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ApplicationId = Request.QueryString("ApplicationID")
            Me.ApprovalNo = Request.QueryString("ApprovalNo")            
            Me.FormID = "InsAdvClaimReq"
            imbExit.Attributes.Add("onClick", "javascript:fclose()")
            If checkform(Me.Loginid, Me.FormID, Me.AppId) Then
                DoBind()
            End If
        End If
    End Sub

    Private Sub DoBind()
        Dim oController As New AdvClaimReqControler
        Dim oCustomClass As New Parameter.AdvClaimReq
        Dim oCustomClass1 As New Parameter.AdvClaimReq
        With oCustomClass
            .strConnection = GetConnectionString
            .ApprovalNo = Me.ApprovalNo
            .ApplicationId = Me.ApplicationId
        End With
        oCustomClass = oController.AdvanceClaimCostRequestViewApproval(oCustomClass)
        With oCustomClass
            Me.AgreementNo = .AgreementNo
            Me.CustomerId = .CustomerId
            Me.ApplicationId = .ApplicationId
            With oCustomClass1
                .strConnection = GetConnectionString
                .ApplicationId = Me.ApplicationId
                .BranchId = Me.sesBranchId.Replace("'", "").Trim
            End With
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(Me.ApplicationId.Trim) & "')"
            lblAgreementNo.Text = .AgreementNo
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(Me.CustomerId.Trim) & "')"
            lblAssetDescription.Text = .AssetDescription
            lblInsuranceCo.Text = .InsuranceComBranchId
            lblCoverage.Text = .MainCoverage
            ltlClaimExpenseAmount.Text = FormatNumber(.ClaimCost, 2)
            ltlRequestDate.Text = .RequestDate.ToString("dd/MM/yyyy")
            ltlNotes.Text = .Notes
        End With
        oCustomClass1 = oController.AdvanceClaimCostRequestView(oCustomClass1)
        With oCustomClass1
            Me.AgreementNo = .AgreementNo
            Me.InsSequenceNo = .InsSequnceNo
            Me.AssetSeqNo = .AssetSeqNo
            Me.BranchID = .BranchId
            lblPolicyNumber.Text = .PolicyNumber
            lblPolicyNumber.NavigateUrl = "javascript:OpenWinViewPolicyNo('" & Server.UrlEncode(Me.BranchID.Trim) & "','" & Me.AgreementNo & "','" & Me.AssetSeqNo & "','" & Me.InsSequenceNo & "','" & Me.ApplicationId & "')"
        End With
    End Sub
End Class