﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdvClaimReq.aspx.vb" Inherits="Maxiloan.Webform.Insurance.AdvClaimReq" %>

<%@ Register TagPrefix="uc1" TagName="UCSearchBY" Src="../../../Webform.UserController/UCSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label><br />
    <asp:Panel ID="PnlGrid" runat="server">
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr class="trtopi">
                <td class="tdtopikiri" width="10" height="20">
                    &nbsp;
                </td>
                <td class="tdtopi" align="center">
                    List Of Advance Claim Request
                </td>
                <td class="tdtopikanan" width="10">
                    &nbsp;
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td>
                    <asp:DataGrid ID="dtgPaging" runat="server" PageSize="5" HorizontalAlign="Center"
                        CssClass="tablegrid" CellSpacing="1" CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False"
                        AllowSorting="True" Width="100%">
                        <AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
                        <ItemStyle HorizontalAlign="Center" CssClass="tdganjil"></ItemStyle>
                        <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="AGREEMENT NO">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'
                                        Visible="False">
                                    </asp:Label>
                                    <asp:Label ID="lblBranchId" runat="server" Text='<%#Container.DataItem("BranchID")%>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="CUSTOMER NAME">
                                <HeaderStyle HorizontalAlign="Center" Width="30%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblCustomerID" runat="server" Visible="False" Text='<%#Container.DataItem("CustomerID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PolicyNumber" HeaderText="POLICY NO">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="hyPOLICYNO" runat="server" Text='<%#Container.DataItem("PolicyNumber")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="EndDate" HeaderText="POLICY END DATE">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAndDate" runat="server" Text='<%#Container.DataItem("EndDate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="MainCoverage" HeaderText="COVERAGE">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Container.DataItem("MainCoverage")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="REQUEST">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbRequest" runat="server" CommandName="Request" ImageUrl="../../../Images/IconRequest.gif">
                                    </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        <table class="nav_tbl" cellspacing="0" cellpadding="0" align="center">
            <tr>
                <td class="nav_tbl_td1">
                    <table class="nav_command_tbl" cellspacing="0" cellpadding="0">
                        <tr>
                        </tr>
                    </table>
                </td>
                <td class="nav_tbl_td2">
                    <table class="nav_page_tbl" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                    CommandName="First" ImageUrl="../../../Images/butkiri1.gif"></asp:ImageButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                    CommandName="Prev" ImageUrl="../../../Images/butkiri.gif"></asp:ImageButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                    CommandName="Next" ImageUrl="../../../Images/butkanan.gif"></asp:ImageButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                    CommandName="Last" ImageUrl="../../../Images/butkanan1.gif"></asp:ImageButton>
                            </td>
                            <td>
                                Page&nbsp;
                                <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbGoPage" runat="server" ImageUrl="../../../Images/butgo.gif"
                                    EnableViewState="False"></asp:ImageButton>
                            </td>
                            <asp:RangeValidator ID="rgvGo" runat="server"  CssClass="validator_general"  Type="Integer" MaximumValue="999999999" ErrorMessage="Page No. is not valid"
                                MinimumValue="1" ControlToValidate="txtpage"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server"  CssClass="validator_general"  ErrorMessage="Page No. is not valid" ControlToValidate="txtpage"
                                Display="Dynamic"></asp:RequiredFieldValidator></tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="nav_totpage" colspan="2">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblRecord" runat="server"></asp:Label>&nbsp;record(s)
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlSearch" runat="Server">
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td class="tdtopikiri" width="10" height="20">
                    &nbsp;
                </td>
                <td class="tdtopi" align="center">
                    Find Advance Claim Request
                </td>
                <td class="tdtopikanan" width="10">
                    &nbsp;
                </td>
            </tr>
        </table>
        <uc1:ucsearchby id="oSearchBy" runat="server">
        </uc1:ucsearchby>
        </TD>
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td align="left">
                    <asp:ImageButton ID="imbSearch" CausesValidation="False" ImageUrl="../../../Images/ButtonSearch.gif"
                        runat="server"></asp:ImageButton>&nbsp;
                    <asp:ImageButton ID="ImbReset" CausesValidation="False" ImageUrl="../../../Images/ButtonReset.gif"
                        runat="server"></asp:ImageButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
