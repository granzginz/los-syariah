﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsAdvClaimReqView.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsAdvClaimReqView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function OpenWinViewPolicyNo(BranchID, AgreementNo, strasset, strins, strApplication) {
            var x = screen.width; var y = screen.height - 100; window.open('../../ViewPolicyDetail.aspx?BranchID=' + BranchID + '&AgreementNo=' + AgreementNo + '&AssetSeqNo=' + strasset + '&InsSeqNo=' + strins + '&ApplicationID=' + strApplication + '&Back=Refund' + '&style=Insurance', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
			
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlAdvanceClaimCostRequestDetail" runat="server">
        <br/>
        <asp:Label ID="lblMessage" runat="server" ForeColor="#993300" font-name="Verdana"
            Font-Size="11px"></asp:Label>
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td class="tdtopikiri" width="10" height="20">
                    &nbsp;
                </td>
                <td class="tdtopi" align="center">
                    Advance Claim Cost Request Detail
                </td>
                <td class="tdtopikanan" width="10">
                    &nbsp;
                </td>
            </tr>
        </table>
        <table class="tablegrid" cellspacing="1" cellpadding="3" width="95%" align="center"
            border="0">
            <tr>
                <td class="tdgenap" style="width: 20%">
                    Agreement No
                </td>
                <td class="tdganjil">
                    <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
                </td>
                <td class="tdgenap" style="width: 20%">
                    Customer Name
                </td>
                <td class="tdganjil" width="30%">
                    <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" style="width: 20%; height: 18px">
                    Asset Description
                </td>
                <td class="tdganjil" style="height: 18px" align="left" width="30%" colspan="3">
                    <asp:Label ID="lblAssetDescription" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" style="width: 20%">
                    Insurance Co
                </td>
                <td class="tdganjil" align="left" width="30%">
                    <asp:Label ID="lblInsuranceCo" runat="server"></asp:Label>
                </td>
                <td class="tdgenap" style="width: 20%">
                    Policy No
                </td>
                <td class="tdganjil" align="left" width="30%">
                    <asp:HyperLink ID="lblPolicyNumber" runat="server"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" style="width: 20%">
                    Coverage
                </td>
                <td class="tdganjil" align="left" width="30%">
                    <asp:Label ID="lblCoverage" runat="server"></asp:Label>
                </td>
                <td class="tdgenap" style="width: 20%">
                </td>
                <td class="tdganjil" align="left" width="30%">
                </td>
            </tr>
        </table>
        <table class="tablegrid" cellspacing="1" cellpadding="3" width="95%" align="center"
            border="0">
            <tr>
                <td class="tdgenap" width="20%">
                    Claim Expense Amount
                </td>
                <td class="tdganjil" align="left" width="80%">
                    <asp:Literal ID="ltlClaimExpenseAmount" runat="server"></asp:Literal>
                    <tr>
                        <td class="tdgenap" width="20%">
                            Request Date
                        </td>
                        <td class="tdganjil" align="left" width="80%">
                            <asp:Literal ID="ltlRequestDate" runat="server"></asp:Literal>
                            <tr>
                                <td class="tdgenap" width="20%">
                                    Notes
                                </td>
                                <td class="tdganjil" align="left" width="80%">
                                    <asp:Literal ID="ltlNotes" runat="server"></asp:Literal>
                                </td>
                            </tr>
        </table>
        <br/>
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td align="left">
                    <asp:ImageButton ID="imbExit" runat="server" ImageUrl="../../../Images/ButtonExit.gif"
                        CausesValidation="True"></asp:ImageButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
