﻿

#Region "Import"
Imports System.Threading
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class AdvClaimReqDetail
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oBankAccount As UcBankAccountID

#Region "Properties"
    Public Property ApplicationId() As String
        Get
            Return CType(viewstate("ApplicationId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationId") = Value
        End Set
    End Property
    Public Property CustomerId() As String
        Get
            Return CType(viewstate("CustomerId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerId") = Value
        End Set
    End Property
    Public Property AssetSeqNo() As Integer
        Get
            Return CType(viewstate("AssetSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("AssetSeqNo") = Value
        End Set
    End Property

    Public Property InsSequenceNo() As Integer
        Get
            Return CType(viewstate("InsSequenceNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("InsSequenceNo") = Value
        End Set
    End Property

    Public Property InsClaimSeqNo() As Integer
        Get
            Return CType(viewstate("InsClaimSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("InsClaimSeqNo") = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value

        End Set
    End Property



#End Region
    Private iNo As Integer = 1
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ApplicationId = Request.QueryString("ApplicationID")
            Me.BranchID = Request.QueryString("BranchID")
            Me.FormID = "InsAdvClaimReq"



            If checkform(Me.Loginid, Me.FormID, Me.AppId) Then
                cboApprovedBy.DataSource = Me.Get_UserApproval("CADV", Me.BranchID)
                cboApprovedBy.DataTextField = "Name"
                cboApprovedBy.DataValueField = "ID"
                cboApprovedBy.DataBind()
                cboApprovedBy.Items.Insert(0, "Select One")
                cboApprovedBy.Items(0).Value = "0"
                '====ucBankAccount========
                With oBankAccount
                    .IsAll = False
                    .BankType = "B"
                    .IsAutoPostback = False
                    .ValidatorFalse()
                    .BindBankAccount()
                End With
                DoBind()
                DoBindGrid()
            End If
        End If

    End Sub
    Private Sub DoBindGrid()
        Dim oController As New AdvClaimReqControler
        Dim oCustomClass As New Parameter.AdvClaimReq


        With oCustomClass
            .strConnection = GetConnectionString
            .ApplicationId = Me.ApplicationId
            .BranchId = Me.BranchID
            .InsSequnceNo = Me.InsSequenceNo
            .AssetSeqNo = Me.AssetSeqNo
            .InsClaimSeqNo = Me.InsClaimSeqNo
        End With
        oCustomClass = oController.AdvanceClaimCostRequestViewGrid(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dtgPaging.DataSource = oCustomClass.ViewGrid
            dtgPaging.CurrentPageIndex = 0
            dtgPaging.DataBind()
        Else
            Exit Sub
        End If




    End Sub


    Private Sub DoBind()
        Dim oController As New AdvClaimReqControler
        Dim oCustomClass As New Parameter.AdvClaimReq

        With oCustomClass
            .strConnection = GetConnectionString
            .ApplicationId = Me.ApplicationId
            .BranchId = Me.BranchID
        End With
        oCustomClass = oController.AdvanceClaimCostRequestView(oCustomClass)

        If oCustomClass Is Nothing Then
            Exit Sub
        End If

        With oCustomClass
            Me.AgreementNo = .AgreementNo
            Me.CustomerId = .CustomerId
            Me.InsSequenceNo = .InsSequnceNo
            Me.AssetSeqNo = .AssetSeqNo
            Me.InsClaimSeqNo = .InsClaimSeqNo

            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(Me.ApplicationId.Trim) & "')"
            lblAgreementNo.Text = .AgreementNo
            lblCustomerName.Text = .CustomerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(Me.CustomerId.Trim) & "')"

            hyPolicyNo.Text = .PolicyNumber
            hyPolicyNo.NavigateUrl = "javascript:OpenWinViewPolicyNo('" & Server.UrlEncode(Me.BranchID.Trim) & "','" & Me.AgreementNo & "','" & Me.AssetSeqNo & "','" & Me.InsSequenceNo & "','" & Me.ApplicationId & "')"


            '          function OpenWinPolicyDetail(BranchID,AgreementNo,strasset,strins,strApplication){
            'window.open('../ViewPolicyDetail.aspx?BranchID=' + BranchID  + '&AgreementNo=' +AgreementNo +'&AssetSeqNo=' + strasset + '&InsSeqNo=' + strins + '&ApplicationID='+ strApplication + '&Back=Refund' +'&style=Insurance', 'UserLookup', 'left=50, top=10, width=600, height=480, menubar=0, scrollbars=yes');
            '}


            '"javascript:OpenWinViewPolicyNo('" & Server.UrlEncode(Me.BranchID.Trim) & "','" & Me.AgreementNo & "','" & Me.AssetSequenceNo & "','" & Me.InsSequenceNo & "','" & Me.ApplicationID & "')"

            lblAssetDescription.Text = .AssetDescription
            lblInsuranceCo.Text = .InsuranceComBranchId

            lblCoverage.Text = .MainCoverage

            If .CanRequest Then
                imbSave.Visible = True
            Else
                imbSave.Visible = False
            End If


        End With
    End Sub

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCancel.Click
        Response.Redirect("AdvClaimReq.aspx")

    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click

        Dim oCustomClass As New Parameter.AdvClaimReq
        Dim oController As New AdvClaimReqControler
        With oCustomClass
            .strConnection = GetConnectionString
            .BranchId = Me.sesBranchId.Replace("'", "")
            .ApplicationId = Me.ApplicationId
            .BusinessDate = Me.BusinessDate
            .LoginId = Me.Loginid
            .AssetSeqNo = Me.AssetSeqNo
            .InsSequnceNo = Me.InsSequenceNo
            .InsClaimSeqNo = Me.InsClaimSeqNo
            .RequestDate = Me.BusinessDate
            .ClaimCost = CDbl(IIf(txtClaimCost.Text = "", 0, txtClaimCost.Text))
            .Notes = txtNotes.Text
            .Status = "REQ"
            .ApproveBy = cboApprovedBy.SelectedItem.Text
            .BankAccountId = oBankAccount.BankAccountID
        End With
        Try
            oController.AdvanceClaimCostRequestSave(oCustomClass)
            'ShowMessage(lblMessage, MessageHelper.
            Response.Redirect("AdvClaimReq.aspx")
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub


    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        With e.Item
            If .ItemIndex >= 0 Then
                Dim lblNo As Label
                lblNo = CType(e.Item.FindControl("lblNo"), Label)
                lblNo.Text = CStr(iNo)
                iNo += 1
            End If
        End With
    End Sub

End Class