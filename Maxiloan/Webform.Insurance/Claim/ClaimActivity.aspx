﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ClaimActivity.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.ClaimActivity" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="Reason" Src="../../Webform.UserController/ucReason.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ClaimActivity</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    
    <script language="javascript" type="text/javascript">
        function OpenWinHistory(pCGID, pExID, pExName) {
            var x = screen.width; var y = screen.height - 100; window.open('../ExecutorHistory.aspx?CGID=' + pCGID + '&ExecutorID=' + pExID + '&ExecutorName=' + pExName + '', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCollector(pCollID, pCGID) {
            var x = screen.width; var y = screen.height - 100; window.open('../../Webform.ARMgt/Setting/CollectorView.aspx?CGID=' + pCGID + '&CollectorID=' + pCollID + '', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID) {
            var x = screen.width; var y = screen.height - 100; window.open('../../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Insurance', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinPrintClaimForm(BranchID, ApplicationID) {
            var x = screen.width; var y = screen.height - 100; window.open('PrintClaimForm.aspx?Branch=' + BranchID + '&style=Insurance&ApplicationID=' + ApplicationID, 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function currencyFormatku(fld, e) {
            var key = '';
            var i = j = 0;
            var strCheck = '0123456789';
            var whichCode = (window.Event) ? e.which : e.keyCode;
            document.onkeypress = window.event;
            if (whichCode == 13) return true;  // Enter
            key = String.fromCharCode(whichCode);  // Get key value from key code
            if (strCheck.indexOf(key) == -1) return false;  // Not a valid key
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlsearch" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
                <h4>
                    AKTIVITAS KLAIM
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
                    Cabang</label>
                <asp:DropDownList ID="ddlBranch" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                    InitialValue="0" ErrorMessage="Harap Pilih Cabang" ControlToValidate="ddlBranch" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Konsumen</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_title">
             <div class="form_single">
                <h4>
                    DAFTAR AKTIVITAS KLAIM
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgClaimActivityList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="AgreementNo" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="AKTIVITAS">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbAct" ImageUrl="../../Images/IconEdit.gif" CommandName="Act"
                                        runat="server"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="APR">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbAcp" ImageUrl="../../Images/IconReceived.gif" CommandName="Acp"
                                        runat="server"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TOLAK">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbReject" ImageUrl="../../Images/IconNegCov.gif" CommandName="Rej"
                                        runat="server"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="BranchID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="ApplicationID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CustomerID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="AgreementNo"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="Name"></asp:BoundColumn>
                            <asp:BoundColumn Visible="false" DataField="InsClaimSeqNo"></asp:BoundColumn>
                            <asp:BoundColumn Visible="false" DataField="InsSequenceNo"></asp:BoundColumn>
                            <asp:BoundColumn Visible="false" DataField="AssetSeqNo"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <a href="javascript:OpenAgreementNo('Insurance','<%# Container.DataItem("ApplicationID")%>')">
                                        <%# Container.DataItem("AgreementNo")%>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA KONSUMEN">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="24%"></ItemStyle>
                                <ItemTemplate>
                                    <a href="javascript:OpenCustomer('Insurance','<%# Container.DataItem("CustomerID")%>')">
                                        <%# Container.DataItem("Name")%>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PolicyNumber" HeaderText="NO POLIS">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="STARTDATE" HeaderText="TGL MULAI">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ENDDATE" HeaderText="TGL SELESAI">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="9%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="COVERAGE" HeaderText="COVER"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="JENIS KLAIM">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Container.DataItem("ClaimType") %>' ID="Label1">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="InsClaimSeqNo" Visible="False">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Container.DataItem("InsClaimSeqNo") %>' ID="LblInsCLaimSeqNo">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo"  CssClass="validator_general" 
                        runat="server" ControlToValidate="txtpage" MinimumValue="1" ErrorMessage="No Halaman Salah"
                        MaximumValue="999999999" Type="Integer"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo"  CssClass="validator_general" 
                        runat="server" ControlToValidate="txtpage" ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblRecord" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    
    <asp:Panel ID="pnlSelect" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    FORM PROSES KLAIM
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Perusahaan Asuransi
                </label>
                <asp:Label ID="lblInsCo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Polis
                </label>
                <asp:Label ID="lblPolicyNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <a href="javascript:OpenAgreementNo('Insurance','<%=me.ApplicationID%>')">
                    <%=me.AgreementNo%></a>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <a href="javascript:OpenCustomer('Insurance','<%=me.CustomerID%>')">
                    <%=me.CustomerName%></a>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Asset</label>
                <asp:Label ID="lblAsset" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Cover
                </label>
                <asp:Label ID="lblCoverageName" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Periode Asuransi
                </label>
                <asp:Label ID="lblInsurancePeriode" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Klaim
                </label>
                <asp:Label ID="lblClaimType" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Proses Ke Kaditserse</label>
                <asp:Label ID="lblProsesToKadit" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
               <label class ="label_req">
                    Aktivitas Klaim</label>
                <asp:TextBox ID="txtClaimAct" runat="server"  Width="400px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvClaimAct" runat="server" Display="Dynamic" ErrorMessage="Harap Isi Aktivitas Klaim"
                    ControlToValidate="txtClaimAct" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
               <label class ="label_req">
                    Hasil</label>
                <asp:TextBox ID="txtResult" runat="server"  Width="400px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvResult" runat="server" Display="Dynamic" ErrorMessage="Harap Isi Hasil Aktivitas"
                    ControlToValidate="txtResult" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan</label>
                <asp:TextBox ID="txtNotes" runat="server"  Width="400px" TextMode="MultiLine"
                    Height="62px"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tgl Surat Dari Kaditserse</label>
                <asp:TextBox runat="server" ID="txtLetterFromKadit"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtLetterFromKadit"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Approval Asuransi</label>
                <asp:TextBox runat="server" ID="txtInscoApprove"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtInscoApprove"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Klaim Diajukan Customer</label>
                <asp:TextBox ID="TxtClaimAmounRequestByCust" runat="server"  Width="160px"
                    Enabled="False"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Klaim Disetujui</label>
                    <%--<uc4:ucNumberFormat ID="txtClaimamountApprove" runat="server" CssClass="small_text" />--%>
                <asp:TextBox ID="txtClaimamountApprove" runat="server"  Width="160px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RvClaimAmountApprove" runat="server" Display="Dynamic"
                    ErrorMessage="Harap diisi dengan Angka" ControlToValidate="txtClaimamountApprove"
                    Enabled="False" ValidationExpression="\d*"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Persetujuan Customer</label>
                <asp:TextBox runat="server" ID="txtCustomerApprove"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender3" TargetControlID="txtCustomerApprove"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DOKUMEN KLAIM
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <asp:DataGrid ID="dtgClaimDoc" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                    CellPadding="2" OnSortCommand="Sorting" Width="100%">
                    <SelectedItemStyle CssClass="tdgenap" />
                    <AlternatingItemStyle CssClass="tdgenap" />
                    <ItemStyle CssClass="tdganjil" />
                    <HeaderStyle CssClass="tdjudul" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <Columns>
                        <asp:BoundColumn DataField="InsDocID" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="INSDOCDESCRIPTION" HeaderText="DOKUMEN KLAIM"></asp:BoundColumn>
                        <asp:BoundColumn DataField="OriginalDoc" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CopyDoc" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ReceiveDocDate" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ReceivedFrom" Visible="False"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="ASLI">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkbOriginal" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="COPY">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkbCopy" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TGL TERIMA">
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txtReceiveDoc"></asp:TextBox>
                                <asp:CalendarExtender runat="server" ID="CalendarExtender4" TargetControlID="txtReceiveDoc"
                                    Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TERIMA DARI">
                            <ItemTemplate>
                                <asp:TextBox ID="txtReceiveFrom" runat="server"  Width="160px"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" Mode="NumericPages"
                        Visible="False" />
                </asp:DataGrid>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlReject" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    KLAIM DITOLAK
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Perusahaan Asuransi
                </label>
                <asp:Label ID="lblRejectInsCo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Polis
                </label>
                <asp:Label ID="lblRejectPolicy" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <a href="javascript:OpenAgreementNo('Insurance','<%=me.ApplicationID%>')">
                    <%=me.AgreementNo%></a>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <a href="javascript:OpenCustomer('Insurance','<%=me.CustomerID%>')">
                    <%=me.CustomerName%></a>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Asset
                </label>
                <asp:Label ID="lblRejectAsset" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Periode Asuransi
                </label>
                <asp:Label ID="lblRejectInsPeriod" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Cover
                </label>
                <asp:Label ID="lblRejectCoverage" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jenis Klaim
                </label>
                <asp:Label ID="lblRejectClaimType" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal Klaim
                </label>
                <asp:Label ID="lblRejectClaimDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Klaim
                </label>
                <asp:Label ID="lblRejectAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Bagian yg Rusak/Hilang
                </label>
                <asp:Label ID="lblRejectDefect" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alasan DiTolak</label>
                <uc:reason id="oReason" runat="server"></uc:reason>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Tolak</label>
                <asp:TextBox runat="server" ID="txtRejectDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender4" TargetControlID="txtRejectDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan</label>
                <asp:TextBox ID="txtRejectNotes" runat="server"  Width="400px"
                    TextMode="MultiLine" Height="62px"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnRejectSave" runat="server" CausesValidation="True" Text="Save"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="btnRejectCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAccept" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    KLAIM DISETUJUI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Perusahaan Asuransi
                </label>
                <asp:Label ID="lblAcceptInsCo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Polis
                </label>
                <asp:Label ID="lblAcceptPolicy" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <a href="javascript:OpenAgreementNo('Insurance','<%=me.ApplicationID%>')">
                    <%=me.AgreementNo%></a>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <a href="javascript:OpenCustomer('Insurance','<%=me.CustomerID%>')">
                    <%=me.CustomerName%></a>
        </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Asset
                </label>
                <asp:Label ID="lblAcceptAsset" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Periode Asuransi
                </label>
                <asp:Label ID="lblAcceptInsPeriod" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Cover
                </label>
                <asp:Label ID="lblAcceptCoverage" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jenis Klaim
                </label>
                <asp:Label ID="lblAcceptClaimType" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal Klaim
                </label>
                <asp:Label ID="lblAcceptClaimDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Klaim
                </label>
                <asp:Label ID="lblAcceptAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Disetujui</label>
                <asp:TextBox runat="server" ID="txtAcceptDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender5" TargetControlID="txtAcceptDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan</label>
                <asp:TextBox ID="txtAcceptNotes" runat="server"  Width="400px"
                    TextMode="MultiLine" Height="62px"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAcceptSave" runat="server" CausesValidation="True" Text="Save"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="btnAcceptCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
