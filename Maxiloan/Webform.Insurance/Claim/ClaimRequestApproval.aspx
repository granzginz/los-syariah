﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ClaimRequestApproval.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.ClaimRequestApproval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ClaimRequestApproval</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinHistory(pCGID, pExID, pExName) {
            var x = screen.width; var y = screen.height - 100; window.open('../ExecutorHistory.aspx?CGID=' + pCGID + '&ExecutorID=' + pExID + '&ExecutorName=' + pExName + '', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCollector(pCollID, pCGID) {
            var x = screen.width; var y = screen.height - 100; window.open('../../Webform.Setup/CollectorView.aspx?CGID=' + pCGID + '&CollectorID=' + pCollID + '', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID) {
            var x = screen.width; var y = screen.height - 100; window.open('../../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinPrintClaimForm(BranchID, ApplicationID) {
            var x = screen.width; var y = screen.height - 100; window.open('PrintClaimForm.aspx?Branch=' + BranchID + '&style=Insurance&ApplicationID=' + ApplicationID, 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function currencyFormatku(fld, e) {
            var key = '';
            var i = j = 0;
            var strCheck = '0123456789';
            var whichCode = (window.Event) ? e.which : e.keyCode;
            document.onkeypress = window.event;
            if (whichCode == 13) return true;  // Enter
            key = String.fromCharCode(whichCode);  // Get key value from key code
            if (strCheck.indexOf(key) == -1) return false;  // Not a valid key
        }

    </script>
    
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Label ID="lblMessageEvent" runat="server"></asp:Label>
    <asp:Label ID="lblMessageProcess" runat="server"></asp:Label>
    <asp:Panel ID="pnlsearch" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    APPROVAL PENGAJUAN KLAIM
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
                    Cabang</label>
                <asp:DropDownList ID="ddlBranch" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                    InitialValue="0" ControlToValidate="ddlBranch" ErrorMessage="Harap Pilih Cabang" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Konsumen</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR PENGAJUAN KLAIM
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgClaimRequestList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="AgreementNo" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="APPROVE">
                                <ItemStyle HorizontalAlign="Center" Width="3%" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton   ID="imbApprove" runat="server" CommandArgument='<%#container.dataitem("ApplicationID")%>'  CommandName="APPROVE" Text='APPROVE' CausesValidation='false'></asp:LinkButton >
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="BranchID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="ApplicationID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="YearNum"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CustomerID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="AgreementNo"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CustomerName"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="FlagInsStatus"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA KONSUMEN">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="description" HeaderText="NAMA ASSET">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PolicyNumber" HeaderText="NO POLIS">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Periode" HeaderText="PERIODE ASURANSI">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MainCoverage" HeaderText="COVER">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="4%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="AssetSeqNo" HeaderText="AssetSeqNo">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="InsSequenceNo" HeaderText="InsSeqNo">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="ClaimApproveStatus"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="InsClaimSeqNo"></asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CommandName="First" OnCommand="NavigationLink_Click"
                        ImageUrl="../../Images/grid_navbutton01.png" CausesValidation="False"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CommandName="Prev" OnCommand="NavigationLink_Click"
                        ImageUrl="../../Images/grid_navbutton02.png" CausesValidation="False"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CommandName="Next" OnCommand="NavigationLink_Click"
                        ImageUrl="../../Images/grid_navbutton03.png" CausesValidation="False"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CommandName="Last" OnCommand="NavigationLink_Click"
                        ImageUrl="../../Images/grid_navbutton04.png" CausesValidation="False"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtPage" runat="server" Width="34px" >1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtpage" MinimumValue="1"
                        ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"  CssClass="validator_general" ></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtpage"
                        ErrorMessage="No Halaman Salah"   CssClass="validator_general" 
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblRecord" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    
    <asp:Panel ID="pnlSelect" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    <asp:Label ID="lbljudul" runat="server" Text=""></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Asset</label>
                <asp:Label ID="lblAsset" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Rangka
                </label>
                <asp:Label ID="lblChasisNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Mesin
                </label>
                <asp:Label ID="lblEngineNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Polisi
                </label>
                <asp:Label ID="lblLicensePlate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Warna
                </label>
                <asp:Label ID="lblColor" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tahun Produksi
                </label>
                <asp:Label ID="lblAssetYear" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Polis
                </label>
                <asp:Label ID="lblPolicyNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Perusahaan Asuransi</label>
                <asp:Label ID="lblInsCo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Telepon Asuransi
                </label>
                <asp:Label ID="lblInsCoTelp" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Fax Asuransi
                </label>
                <asp:Label ID="lblInsCoFax" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Premi Dibayar ke Pers Asuransi
                </label>
                <asp:Label ID="lblPremiumPaidtoInsCo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Premi dibayar Customer
                </label>
                <asp:Label ID="lblPremiumPaidByCust" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgSelect" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TAHUN">
                                <HeaderStyle HorizontalAlign="LEFT"></HeaderStyle>
                                <ItemStyle HorizontalAlign="LEFT"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YEARNUM") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TGL SELESAI">
                                <HeaderStyle HorizontalAlign="LEFT"></HeaderStyle>
                                <ItemStyle HorizontalAlign="LEFT"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ENDDATE") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NILAI PERTANGGUNGAN">
                                <HeaderStyle HorizontalAlign="LEFT"></HeaderStyle>
                                <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.SUMINSURED"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JENIS COVER">
                                <HeaderStyle HorizontalAlign="LEFT"></HeaderStyle>
                                <ItemStyle HorizontalAlign="LEFT"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.COVERAGE") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TPL">
                                <HeaderStyle HorizontalAlign="LEFT"></HeaderStyle>
                                <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.TPLAMOUNT"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SRCC">
                                <HeaderStyle HorizontalAlign="LEFT"></HeaderStyle>
                                <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.SRCC"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FLOOD">
                                <HeaderStyle HorizontalAlign="LEFT"></HeaderStyle>
                                <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.FLOOD"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Klaim</label>
                <asp:DropDownList ID="ddlClaimType" runat="server" Enabled="false">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kasus</label>
                <asp:DropDownList ID="ddlCases" runat="server" Enabled="false">
                    <asp:ListItem Value="Stolen">Stolen</asp:ListItem>
                    <asp:ListItem Value="AccidentTotalLoss">Accident Total Loss</asp:ListItem>
                    <asp:ListItem Value="Accident">Accident</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="">
                    Lokasi Kejadian</label>
                <asp:TextBox ID="txtLocationEvent" runat="server" Enabled="false" ></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Bagian yang Rusak/Hilang</label>
                <asp:TextBox ID="txtStolen" runat="server" Enabled="false"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="">
                    Tanggal Kejadian</label>
                <asp:TextBox runat="server" ID="txtEventDate" Enabled="false"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Derek</label>
                <asp:TextBox runat="server" ID="txtDerekDate" Enabled="false"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Bengkel</label>
                <asp:TextBox ID="txtBengkelName" runat="server" Enabled="false"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alamat Bengkel</label>
                <asp:TextBox ID="txtBengkelAddres" runat="server"  Width="392px" Enabled="false"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Telepon Bengkel</label>
                <asp:TextBox ID="txtAreaCode" runat="server"  MaxLength="4" Width="48px" Enabled="false"></asp:TextBox>
                &nbsp;-
                <asp:TextBox ID="txtPhone" runat="server" Enabled="false"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kepala Bengkel/PIC</label>
                <asp:TextBox ID="txtBengkelHead" runat="server" Enabled="false"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Lapor</label>
                <asp:TextBox runat="server" ID="txtReportdate" Enabled="false"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Survey</label>
                <asp:TextBox runat="server" ID="txtSurveyDate" Enabled="false"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="">
                    Tanggal Proses Ke KaditSerse</label>
                <asp:TextBox runat="server" ID="txtProses" Enabled="false"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="">
                    Tanggal Klaim</label>
                <asp:TextBox runat="server" ID="txtClaimDate" Enabled="false"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Klaim</label>
                <%--<asp:TextBox ID="txtAmount" runat="server" Enabled="false"></asp:TextBox>--%>
                <asp:TextBox ID="txtAmount" runat="server" ></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Klaim</label>
                <asp:TextBox ID="txtClaimNo" runat="server" Enabled="false"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    DiLaporkan Oleh</label>
                <asp:TextBox ID="txtReportBy" runat="server" Enabled="false"></asp:TextBox>
                &nbsp;sebagai&nbsp;
                <asp:TextBox ID="txtReportAs" runat="server" Enabled="false"></asp:TextBox>
                &nbsp;
            </div>
        </div>
        <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">Approval</label>
                        <asp:DropDownList ID="ddlApprove" runat="server">
                            <asp:ListItem Value="0">Select One</asp:ListItem>
                            <asp:ListItem Value="APR">Approve</asp:ListItem>
                            <asp:ListItem Value="REJ">Reject</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="reqFloatingPeriod" runat="server"   Display="Dynamic" ErrorMessage="Harap pilih Approval" ControlToValidate="ddlApprove" CssClass="validator_general" InitialValue="0" />
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_single">
                        <label>Catatan</label>
                        <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine" />
                    </div>
                </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
