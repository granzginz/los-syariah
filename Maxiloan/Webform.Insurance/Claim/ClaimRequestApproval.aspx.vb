﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ClaimRequestApproval
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_Claimreq As New ClaimReqController
    Private oClaimReq As New Parameter.ClaimRequest
    Private oClaimReqApr As New Parameter.InsClaimAct

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Protected WithEvents lblChasisNo As System.Web.UI.WebControls.Label
    Protected WithEvents lblEngineNo As System.Web.UI.WebControls.Label
    Protected WithEvents lblLicensePlate As System.Web.UI.WebControls.Label
    Protected WithEvents lblColor As System.Web.UI.WebControls.Label
    Protected WithEvents lblAssetYear As System.Web.UI.WebControls.Label
    Protected WithEvents lblPolicyNo As System.Web.UI.WebControls.Label
    Protected WithEvents lblInsCo As System.Web.UI.WebControls.Label
    Protected WithEvents lblInsCoTelp As System.Web.UI.WebControls.Label
    Protected WithEvents lblPremiumPaidtoInsCo As System.Web.UI.WebControls.Label
    Protected WithEvents lblPremiumPaidByCust As System.Web.UI.WebControls.Label
    Protected WithEvents lblInsCoFax As System.Web.UI.WebControls.Label
    Protected WithEvents ddlClaimType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlCases As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtLocationEvent As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtBengkelName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtBengkelAddres As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAreaCode As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPhone As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtBengkelHead As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAmount As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtReportBy As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtReportAs As System.Web.UI.WebControls.TextBox
    Protected WithEvents imbCancel As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtClaimNo As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblAgreementNo As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lblCustomerName As System.Web.UI.WebControls.HyperLink
    Protected WithEvents ddlBranch As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cboSearchBy As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtSearchBy As System.Web.UI.WebControls.TextBox
    Protected WithEvents imgsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbReset As System.Web.UI.WebControls.ImageButton
    Protected WithEvents pnlsearch As System.Web.UI.WebControls.Panel
    Protected WithEvents dtgClaimRequestList As System.Web.UI.WebControls.DataGrid
    Protected WithEvents imbFirstPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbPrevPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbNextPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbLastPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbGoPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents rfvGo As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents lblPage As System.Web.UI.WebControls.Label
    Protected WithEvents lblTotPage As System.Web.UI.WebControls.Label
    Protected WithEvents lbljudul As System.Web.UI.WebControls.Label
    Protected WithEvents lblRecord As System.Web.UI.WebControls.Label
    Protected WithEvents pnlDtGrid As System.Web.UI.WebControls.Panel
    Protected WithEvents lblAsset As System.Web.UI.WebControls.Label
    Protected WithEvents dtgSelect As System.Web.UI.WebControls.DataGrid
    Protected WithEvents pnlSelect As System.Web.UI.WebControls.Panel
    Protected WithEvents txtStolen As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents lblMessageEvent As System.Web.UI.WebControls.Label
    Protected WithEvents Requiredfieldvalidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents rfvLocation As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents lblMessageProcess As System.Web.UI.WebControls.Label
    Protected WithEvents ImbAdd As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Imagebutton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbBack As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtPage As System.Web.UI.WebControls.TextBox
    Protected WithEvents rgvGo As System.Web.UI.WebControls.RangeValidator
    Private recordCount As Int64 = 1
#End Region

#Region "properties"
    Public Property ClaimDate() As Date
        Get
            Return CType(ViewState("ClaimDate"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("ClaimDate") = Value
        End Set
    End Property
    Public Property EventDate() As Date
        Get
            Return CType(ViewState("EventDate"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("EventDate") = Value
        End Set
    End Property
    Public Property ReportDate() As Date
        Get
            Return CType(ViewState("ReportDate"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("ReportDate") = Value
        End Set
    End Property
    Public Property DerekDate() As Date
        Get
            Return CType(ViewState("DerekDate"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("DerekDate") = Value
        End Set
    End Property
    Public Property SurveyDate() As Date
        Get
            Return CType(ViewState("SurveyDate"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("SurveyDate") = Value
        End Set
    End Property
    Public Property ProcessToKaditserseDate() As Date
        Get
            Return CType(ViewState("ProcessToKaditserseDate"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("ProcessToKaditserseDate") = Value
        End Set
    End Property
    Public Property InsClaimSeqNo() As String
        Get
            Return CType(ViewState("InsClaimSeqNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InsClaimSeqNo") = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return CType(ViewState("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return CType(ViewState("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property
    Public Property YearNum() As String
        Get
            Return CType(ViewState("YearNum"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("YearNum") = Value
        End Set
    End Property
    Public Property customerID() As String
        Get
            Return CType(ViewState("customerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("customerID") = Value
        End Set
    End Property
    Public Property InsSequenceNo() As String
        Get
            Return CType(ViewState("InsSequenceNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InsSequenceNo") = Value
        End Set
    End Property
    Public Property AssetSequenceNo() As String
        Get
            Return CType(ViewState("AssetSequenceNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetSequenceNo") = Value
        End Set
    End Property
    Public Property Status() As Boolean
        Get
            Return CType(ViewState("Status"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("Status") = Value
        End Set
    End Property
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblRecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.FormID = "InsClaimApr"
        lblMessage.Visible = False
        If Not Page.IsPostBack Then
            lbljudul.Text = "APPROVE CLAIM PROCESS"
            fillcbo()
            Me.Status = True
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                InitialDefaultPanel()
            Else
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            End If
        End If
    End Sub

    Public Sub bindData()
        Me.YearNum = "0"
        Me.Status = False
        lbljudul.Text = "APPROVE CLAIM PROCESS"
        pnlDtGrid.Visible = False
        pnlsearch.Visible = False
        pnlSelect.Visible = True
        Dim dtsEntity As New DataTable
        Dim dtvEntity As New DataView
        Dim dt As New DataTable
        Dim dtView As New DataTable

        With oClaimReq
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .AssetSequenceNo = CInt(Me.AssetSequenceNo)
            .InsSequenceNo = CInt(Me.InsSequenceNo)
            .YearNum = CType(Me.YearNum, Integer)
        End With
        oClaimReq = m_Claimreq.ClaimRequestDetail(oClaimReq)
        dt = oClaimReq.ListData

        If dt.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            Exit Sub
        End If
        With dt.Rows(0)
            lblAgreementNo.Text = CStr(IIf(IsDBNull(.Item("AgreementNo")), "", .Item("AgreementNo")))
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(Me.customerID.Trim) & "')"
            lblCustomerName.Text = CStr(IIf(IsDBNull(.Item("Name")), "", .Item("Name")))
            lblAssetYear.Text = CStr(IIf(IsDBNull(.Item("AssetYear")), "", .Item("AssetYear")))
            lblChasisNo.Text = CStr(IIf(IsDBNull(.Item("ChassisNo")), "", .Item("ChassisNo")))
            lblColor.Text = CStr(IIf(IsDBNull(.Item("Color")), "", .Item("Color")))
            lblEngineNo.Text = CStr(IIf(IsDBNull(.Item("EngineNo")), "", .Item("EngineNo")))
            lblInsCo.Text = CStr(IIf(IsDBNull(.Item("InsCo")), "", .Item("InsCo")))
            lblInsCoFax.Text = CStr(IIf(IsDBNull(.Item("InsCoFax")), "", .Item("InsCoFax")))
            lblInsCoTelp.Text = CStr(IIf(IsDBNull(.Item("InsCoPhone")), "", .Item("InsCoPhone")))
            lblLicensePlate.Text = CStr(IIf(IsDBNull(.Item("LicensePlate")), "", .Item("LicensePlate")))
            lblPolicyNo.Text = CStr(IIf(IsDBNull(.Item("PolicyNumber")), "", .Item("PolicyNumber")))
            lblPremiumPaidByCust.Text = CStr(IIf(IsDBNull(.Item("PremiumPaidByCust")), "", FormatNumber(.Item("PremiumPaidByCust"), 2)))
            lblPremiumPaidtoInsCo.Text = CStr(IIf(IsDBNull(.Item("PremiumPaidToInsCo")), "", FormatNumber(.Item("PremiumPaidToInsCo"), 2)))
            lblAsset.Text = CStr(IIf(IsDBNull(.Item("Description")), "", .Item("Description")))
        End With
        oClaimReq = m_Claimreq.ClaimrequestCoverage(oClaimReq)
        dtsEntity = oClaimReq.ListData
        dtsEntity.TableName = "Table"
        dtvEntity = dtsEntity.DefaultView
        dtgSelect.DataSource = dtvEntity
        If Not dtvEntity Is Nothing Then
            dtgSelect.DataBind()
        End If
        oClaimReq = m_Claimreq.ClaimrequestGetClaimType(oClaimReq)
        dtsEntity = oClaimReq.ListData
        ddlClaimType.DataSource = dtsEntity
        ddlClaimType.DataTextField = "Description"
        ddlClaimType.DataValueField = "ClaimType"
        ddlClaimType.DataBind()
        With oClaimReq
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .AssetSequenceNo = CInt(Me.AssetSequenceNo)
            .InsSequenceNo = CInt(Me.InsSequenceNo)
            .InsClaimSeq = CInt(Me.InsClaimSeqNo)
        End With
        oClaimReq = m_Claimreq.ViewInsuraceClaimInq(oClaimReq)
        dtView = oClaimReq.ListData
        If dtView.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            Exit Sub
        End If
        Dim Null As Date
        Null = CType("1/1/1900", Date)
        With dtView.Rows(0)
            ddlClaimType.SelectedIndex = ddlClaimType.Items.IndexOf(ddlClaimType.Items.FindByValue(.Item("ClaimType").ToString))
            txtStolen.Text = CStr(IIf(IsDBNull(.Item("DefectPart")), "", .Item("DefectPart")))
            txtBengkelName.Text = CStr(IIf(IsDBNull(.Item("BengkelName")), "", .Item("BengkelName")))
            txtBengkelAddres.Text = CStr(IIf(IsDBNull(.Item("BengkelAddress")), "", .Item("BengkelAddress")))
            txtPhone.Text = CStr(IIf(IsDBNull(.Item("BengkelPhone")), "", .Item("BengkelPhone")))
            'txtAreaCode.Text.Trim & 
            txtBengkelHead.Text = CStr(IIf(IsDBNull(.Item("BengkelPIC")), "", .Item("BengkelPIC")))
            txtReportBy.Text = CStr(IIf(IsDBNull(.Item("ReportedBy")), "", .Item("ReportedBy")))
            txtReportAs.Text = CStr(IIf(IsDBNull(.Item("ReportedAs")), "", .Item("ReportedAs")))
            txtLocationEvent.Text = CStr(IIf(IsDBNull(.Item("EventLocation")), "", .Item("EventLocation")))
            txtAmount.Text = CStr(IIf(IsDBNull(.Item("ClaimAmountByCust")), "", .Item("ClaimAmountByCust")))
            If CDate(.Item("ClaimDate")) <> Null Then
                Me.ClaimDate = CDate(.Item("ClaimDate"))
                txtClaimDate.Text = CStr(IIf(IsDBNull(.Item("ClaimDate")), "", Me.ClaimDate.ToString("dd/MM/yyyy")))
            Else
                txtClaimDate.Text = ""
            End If

            If CDate(.Item("EventDate")) <> Null Then
                Me.EventDate = CDate(.Item("EventDate"))
                txtEventDate.Text = CStr(IIf(IsDBNull(.Item("EventDate")), "", Me.EventDate.ToString("dd/MM/yyyy")))
            Else
                txtEventDate.Text = ""
            End If

            If CDate(.Item("ReportDate")) <> Null Then
                Me.ReportDate = CDate(.Item("ReportDate"))
                txtReportdate.Text = CStr(IIf(IsDBNull(.Item("ReportDate")), "", Me.ReportDate.ToString("dd/MM/yyyy")))
            Else
                txtReportdate.Text = ""
            End If

            If CDate(.Item("DerekDate")) <> Null Then
                Me.DerekDate = CDate(.Item("DerekDate"))
                txtDerekDate.Text = CStr(IIf(IsDBNull(.Item("DerekDate")), "", Me.DerekDate.ToString("dd/MM/yyyy"))) ' ConvertDate2(Me.DerekDate)
            Else
                txtDerekDate.Text = ""
            End If

            If CDate(.Item("SurveyDate")) <> Null Then
                Me.SurveyDate = CDate(.Item("SurveyDate"))
                txtSurveyDate.Text = CStr(IIf(IsDBNull(.Item("SurveyDate")), "", Me.SurveyDate.ToString("dd/MM/yyyy"))) 'ConvertDate(Me.SurveyDate)
            Else
                txtSurveyDate.Text = ""
            End If

            If CDate(.Item("ProcessToKaditserseDate")) <> Null Then
                Me.ProcessToKaditserseDate = CDate(.Item("ProcessToKaditserseDate"))
                txtProses.Text = CStr(IIf(IsDBNull(.Item("ProcessToKaditserseDate")), "", Me.ProcessToKaditserseDate.ToString("dd/MM/yyyy"))) 'ConvertDate(Me.ProcessToKaditserseDate)
            Else
                txtProses.Text = ""
            End If
            txtClaimNo.Text = CStr(IIf(IsDBNull(.Item("InsCoClaimNo")), "", .Item("InsCoClaimNo")))
            ddlCases.SelectedIndex = ddlCases.Items.IndexOf(ddlCases.Items.FindByValue(.Item("Cases").ToString))
        End With
    End Sub

    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlSelect.Visible = False
    End Sub

    Sub fillcbo()
        Dim m_controller As New DataUserControlController
        With ddlBranch
            If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "ALL")
                .Items(0).Value = "ALL"
            Else
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                Dim strBranch() As String
                strBranch = Split(Me.sesBranchId, ",")
                If UBound(strBranch) > 0 Then
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                End If
            End If
        End With
    End Sub

    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Me.SearchBy = ""
        Me.SortBy = ""

        If ddlBranch.SelectedValue <> "" And ddlBranch.SelectedValue <> "ALL" Then
            Me.SearchBy = "BranchID='" & ddlBranch.SelectedValue & "'"
        End If
        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
            Else
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
            End If
        End If

        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable

        With oClaimReqApr
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oClaimReqApr = m_Claimreq.ClaimRequestListApproval(oClaimReqApr)

        With oClaimReqApr
            lblRecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oClaimReqApr.ListData
        dtsEntity.TableName = "Table"

        dtvEntity = dtsEntity.DefaultView

        dtgClaimRequestList.DataSource = dtvEntity

        If Not dtvEntity Is Nothing Then
            dtgClaimRequestList.DataBind()
        End If

        PagingFooter()
        pnlDtGrid.Visible = True
    End Sub

    Private Sub dtgClaimRequestList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgClaimRequestList.ItemCommand
        Me.BranchID = e.Item.Cells(1).Text.Trim
        Me.ApplicationID = e.Item.Cells(2).Text.Trim
        Me.YearNum = e.Item.Cells(3).Text.Trim
        Me.customerID = e.Item.Cells(4).Text.Trim
        Me.AgreementNo = e.Item.Cells(5).Text.Trim
        Me.CustomerName = e.Item.Cells(6).Text.Trim
        Me.AssetSequenceNo = e.Item.Cells(14).Text.Trim
        Me.InsSequenceNo = e.Item.Cells(15).Text.Trim
        Me.InsClaimSeqNo = e.Item.Cells(17).Text.Trim

        Select Case e.CommandName
            Case "APPROVE"
                If CheckFeature(Me.Loginid, Me.FormID, "Apr", Me.AppId) Then
                    If SessionInvalid() Then
                        Dim strHTTPServer As String
                        Dim strHTTPApp As String
                        Dim strNameServer As String
                        strHTTPServer = Request.ServerVariables("PATH_INFO")
                        strNameServer = Request.ServerVariables("SERVER_NAME")
                        strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                        Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
                    Else
                        pnlDtGrid.Visible = False
                        pnlsearch.Visible = False
                        pnlSelect.Visible = True
                        bindData()
                    End If
                End If
        End Select
    End Sub
   
    Sub clear()
        lblAssetYear.Text = ""
        lblChasisNo.Text = ""
        lblColor.Text = ""
        lblEngineNo.Text = ""
        lblInsCo.Text = ""
        lblInsCoFax.Text = ""
        lblInsCoTelp.Text = ""
        lblLicensePlate.Text = ""
        lblPolicyNo.Text = ""
        lblPremiumPaidByCust.Text = ""
        lblPremiumPaidtoInsCo.Text = ""
        lblAsset.Text = ""

        txtStolen.Text = ""
        txtBengkelName.Text = ""
        txtBengkelAddres.Text = ""
        txtAreaCode.Text = ""
        txtPhone.Text = ""
        txtBengkelHead.Text = ""
        txtClaimDate.Text = ""
        txtEventDate.Text = ""
        txtReportdate.Text = ""
        txtReportBy.Text = ""
        txtReportAs.Text = ""
        txtLocationEvent.Text = ""
        txtAmount.Text = "0"
        txtDerekDate.Text = ""
        txtSurveyDate.Text = ""
        txtProses.Text = ""
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        lblMessageEvent.Text = ""
        lblMessageProcess.Text = ""

        With oClaimReqApr
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .InsClaimSeq = CInt(Me.InsClaimSeqNo)
            .InsSequenceNo = CInt(Me.InsSequenceNo.Trim)
            .AssetSequenceNo = CInt(Me.AssetSequenceNo.Trim)
            .InsClaimSeqNo = CInt(Me.InsClaimSeqNo)
            .ApprovalNotes = txtNotes.Text
            .ApprovalStatus = ddlApprove.SelectedValue
            .LoginId = Me.Loginid
            .ClaimType = Me.ddlClaimType.SelectedValue
            'Modify by WIra 20180327
            'claim amount bisa diubah ketika approval
            .ClaimAmount = txtAmount.Text
        End With
        Dim result = m_Claimreq.ClaimrequestSaveApprove(oClaimReqApr)
        ShowMessage(lblMessage, "Data berhasil " & ddlApprove.SelectedItem.Text, False)
        InitialDefaultPanel()
        fillcbo()
    End Sub

    Private Sub dtgClaimRequestList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgClaimRequestList.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lnkAgreementNo As HyperLink
            Dim lnkCustomerName As HyperLink

            Dim strCustomerID As String
            Dim strApplicationID As String
            
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            strApplicationID = e.Item.Cells(2).Text.Trim
            If lnkAgreementNo.Text.Trim.Length > 0 Then
                lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(strApplicationID.Trim) & "')"
            End If
            strCustomerID = e.Item.Cells(4).Text.Trim
            lnkCustomerName = CType(e.Item.FindControl("lnkCustomerName"), HyperLink)

            If strCustomerID.Trim.Length > 0 Then
                lnkCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(strCustomerID.Trim) & "')"
            End If
        End If
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("ClaimRequestApproval.aspx")
        'If Me.Status = True Then

        '    InitialDefaultPanel()
        '    pnlDtGrid.Visible = True
        'Else
        '    Response.Redirect("../Inquiry/ClaimInq.aspx")
        'End If

    End Sub
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        ddlBranch.ClearSelection()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
    End Sub

End Class