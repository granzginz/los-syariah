﻿#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports System.Math
Imports System.Text
Imports Maxiloan.General
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Controller
Imports Maxiloan.Parameter

#End Region

Public Class InsuranceEndorsValid
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtSumInsured As ucNumberFormat
    Protected WithEvents txtTagihan As ucNumberFormat
    Protected WithEvents UcNotaAsuransi1 As UcNotaAsuransi
    Private m_Doc As New DocReceiveController
    Protected WithEvents ucTanggalTerima As ucDateCE


#Region "Deklarasi Variabel"


    Dim dsSearch As DataSet
    Dim conSearch As SqlConnection
    Dim cmdSearch As SqlCommand
    Dim gStrSQL As String
    Dim gBytPageSize As Byte
    Dim agreementno As String
    Dim customerID As String
    Dim custid As String
    Dim desc As String
    Dim customertype As String
    Dim checkbutton As String
    Dim c As Integer
    Dim currencyid As String
    Dim branch As String
    '  Dim lObjString As New el_string()
#End Region
#Region "Property"
    Private Property BDEndorsPolicyNo() As String
        Get
            Return (CType(ViewState("BDEndorsPolicyNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BDEndorsPolicyNo") = Value
        End Set
    End Property
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal ApplicationID As String)
            ViewState("ApplicationID") = ApplicationID
        End Set
    End Property
    Property assetseqno() As String
        Get
            Return ViewState("assetseqno").ToString
        End Get
        Set(ByVal assetseqno As String)
            ViewState("assetseqno") = assetseqno
        End Set
    End Property
    Property inssequenceno() As String
        Get
            Return ViewState("inssequenceno").ToString
        End Get
        Set(ByVal inssequenceno As String)
            ViewState("inssequenceno") = inssequenceno
        End Set
    End Property
    Property total() As Decimal
        Get
            Return CDec(ViewState("total").ToString)
        End Get
        Set(ByVal total As Decimal)
            ViewState("total") = total
        End Set
    End Property
    Property InsuranceComBranchID() As String
        Get
            Return ViewState("InsuranceComBranchID").ToString
        End Get
        Set(ByVal InsuranceComBranchID As String)
            ViewState("InsuranceComBranchID") = InsuranceComBranchID
        End Set
    End Property
    Property Next1() As String
        Get
            Return ViewState("Next1").ToString
        End Get
        Set(ByVal Next1 As String)
            ViewState("Next1") = Next1
        End Set
    End Property
    Property Status() As String
        Get
            Return ViewState("Status").ToString
        End Get
        Set(ByVal Status As String)
            ViewState("Status") = Status
        End Set
    End Property
    Property StatusPrint() As String
        Get
            Return ViewState("StatusPrint").ToString
        End Get
        Set(ByVal Status As String)
            ViewState("StatusPrint") = Status
        End Set
    End Property
#End Region
#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            'If Request.QueryString("strFileLocation") <> "" Then
            '    Dim strHTTPServer As String
            '    Dim StrHTTPApp As String
            '    Dim strNameServer As String
            '    Dim strFileLocation As String

            '    strHTTPServer = Request.ServerVariables("PATH_INFO")
            '    strNameServer = Request.ServerVariables("SERVER_NAME")
            '    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            '    strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
            '    '& "history.back(-1) " & vbCrLf _
            '    'Call file PDF 
            '    Response.Write("<script language = javascript>" & vbCrLf _
            '    & "window.open('" & strFileLocation & "','Insurance', 'left=0, top=0, width=600, height=480, menubar=0, scrollbars=yes') " & vbCrLf _
            '    & "</script>")

            'End If
            PnlValid.Visible = False

            ucTanggalTerima.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            Bindgrid()
        End If
    End Sub
#End Region
#Region "Connection"

    Sub Connection()
        Dim strConnect As String
        strConnect = GetConnectionString()
        Try
            conSearch = New SqlConnection(strConnect)
        Catch objError As Exception
            Exit Sub
        End Try
    End Sub
#End Region
#Region "BindGrid"

    Sub Bindgrid()
        agreementno = Request.QueryString("agreementno")
        BranchID = Replace(Request.QueryString("BranchId"), "'", "").Trim
        customerID = Request.QueryString("customerID").Trim
        assetseqno = Replace(Request.QueryString("assetseqno"), "'", "").Trim
        inssequenceno = Request.QueryString("inssequenceno").Trim
        ApplicationID = Replace(Request.QueryString("applicationid"), "'", "").Trim
        'custid = Replace(Request.QueryString("custid"), "'", "").Trim
        custid = customerID
        desc = Request.QueryString("desc")
        customertype = Request.QueryString("customertype")
        checkbutton = Request.QueryString("checkbutton")
        Me.branch = BranchID
        Me.agreementno = agreementno
        Me.ApplicationID = ApplicationID
        Me.assetseqno = assetseqno
        Me.inssequenceno = inssequenceno

        BindDataNotaAsuransi()

        If IsNothing(conSearch) Then Connection()

        Dim strSelect As String


        '========== View YearNum =================================2===============
        Dim myCommand As SqlCommand
        Dim myReader As SqlDataReader

        Dim objDataRow1 As DataRow

        'Dim strSQL As String = "SELECT Max(IsNull(Endorske,'0')) As endorske FROM EndorsmentHistoryDetail " & _
        '            "WHERE ApplicationID ='" & applicationid & "' " & _
        '            "   AND AssetSeqNo ='" & assetseqno & "' " & _
        '            "   AND InsSequenceNo ='" & inssequenceno & "'"

        Dim strSqlEndors As New StringBuilder
        strSqlEndors.Append(" SELECT isnull(MAx(Endorske),'0') As endorske FROM EndorsmentHistoryDetail  ")
        strSqlEndors.Append(" WHERE ApplicationID ='" & ApplicationID & "' ")
        strSqlEndors.Append("    AND AssetSeqNo ='" & assetseqno & "'  ")
        strSqlEndors.Append("    AND InsSequenceNo ='" & inssequenceno & "' ")



        Dim EobjAdapter As New SqlDataAdapter(strSqlEndors.ToString, conSearch)

        dsSearch = New DataSet
        EobjAdapter.Fill(dsSearch, "EndorsmentHistoryDetail")

        Dim objRow1 As DataRowView = dsSearch.Tables("EndorsmentHistoryDetail").DefaultView(0)

        Dim endorske As Integer
        endorske = CType(objRow1("endorske"), Integer)
        Dim oSQL As New StringBuilder

        oSQL.Append(" SELECT InsuranceAsset.PolicyNumber, isnull(InsuranceAsset.PolicyReceiveDate,'') as PolicyReceiveDate , ")
        oSQL.Append(" InsuranceAsset.SumInsured, InsuranceAsset.PaidAmountToInsco,  ")
        oSQL.Append(" InsuranceAsset.SRCCToInsco, InsuranceAsset.TPLToInsco,  ")
        oSQL.Append(" InsuranceAsset.AdminFee , InsuranceAsset.MeteraiFee,  ")
        oSQL.Append("   Isnull(AgreementAsset.SerialNo1,'') as SerialNo1 , IsNull(AgreementAsset.SerialNo2,'') As SerialNo2 ,  ")
        oSQL.Append(" InsuranceAsset.BDEndorsPolicyNo, InsuranceAsset.PremiumAmountToInsCo ")
        oSQL.Append(" FROM InsuranceAsset INNER JOIN ")
        oSQL.Append("  AgreementAsset ON AgreementAsset.ApplicationID = InsuranceAsset.ApplicationID AND  ")
        oSQL.Append("  AgreementAsset.AssetSeqNo = InsuranceAsset.AssetSeqNo INNER JOIN  ")
        oSQL.Append("  Agreement ON Agreement.ApplicationID = InsuranceAsset.ApplicationID  ")
        oSQL.Append(" WHERE InsuranceAsset.ApplicationID ='" & ApplicationID & "'  ")
        oSQL.Append("  AND InsuranceAsset.AssetSeqNo ='" & assetseqno & "' ")
        oSQL.Append(" AND InsuranceAsset.InsSequenceNo ='" & inssequenceno & "' ")

        Dim objAdapter As New SqlDataAdapter(oSQL.ToString, conSearch)
        Context.Trace.Write("Sql = " & oSQL.ToString)

        dsSearch = New DataSet
        objAdapter.Fill(dsSearch, "InsuranceAsset")

        'Dim paidamount As Integer
        'Dim rscctoinsco As Integer
        'Dim tpltoinsco As Integer
        'Dim adminfee As Integer
        'Dim meteraifee As Integer
        'Dim total As Integer
        Dim objRow As DataRowView

        If dsSearch.Tables("InsuranceAsset").Rows.Count > 0 Then
            objRow = dsSearch.Tables("InsuranceAsset").DefaultView(0)
            Dim strBDEndorsPolicyNo As String
            strBDEndorsPolicyNo = CType(objRow("BDEndorsPolicyNo"), String)
            Me.BDEndorsPolicyNo = strBDEndorsPolicyNo

            txtSumInsured.Text = FormatNumber(objRow("SumInsured"), 0)
            txtChasis.Text = CType(objRow("SerialNo1"), String)
            txtMachine.Text = CType(objRow("SerialNo2"), String)
            txtTagihan.Text = FormatNumber(objRow("PremiumAmountToInsco"), 0)
            lblNoPolis.Text = CType(objRow("PolicyNumber"), String)
            lnkNoPolis.Text = CType(objRow("PolicyNumber"), String)
            '     lnkNoPolis.NavigateUrl = LinkToPN(applicationid, assetseqno, inssequenceno)
            lnkNoPolis.NavigateUrl = "javascript:OpenWinViewPolicyNo('" & Me.agreementno & "', " & Me.sesBranchId & ")"

            lblSumInsured.Text = CType(objRow("SumInsured"), String)
            lblChasis.Text = CType(objRow("SerialNo1"), String)
            lblMachine.Text = CType(objRow("SerialNo2"), String)
            'lblTagihan.Text = total
            lblTagihan.Text = CType(objRow("PaidAmountToInsco"), String)
            'currencyid = CType(objRow("CurrencyID"), String)
            ViewState("policydate") = CType(objRow("PolicyReceiveDate"), String)
            'BFI - data di SerialNo1 & SerialNo2 ambil dr tabel EndorsmentHistoryDetail

            Dim mySQLChasisCommand As SqlCommand
            Dim myChasisReader As SqlDataReader

            Dim strsqlChasis As New StringBuilder
            'strsqlChasis = "SELECT DataSalahDesc, DataBenarDesc " _
            '        & "FROM EndorsmentHistoryDetail " _
            '        & "WHERE ApplicationID = '" & applicationid & "' AND " _
            '        & "AssetSeqNo = '" & assetseqno & "' AND " _
            '        & "InsSequenceNo = '" & inssequenceno & "' AND " _
            '        & "YgEndors = '" & CType(objRow("SerialNo1"), String) & "' AND " _
            '        & "EndorsKe = " & endorske

            strsqlChasis.Append(" SELECT DataSalahDesc, DataBenarDesc ")
            strsqlChasis.Append(" FROM EndorsmentHistoryDetail ")
            strsqlChasis.Append(" WHERE ApplicationID = '" & ApplicationID & "' AND ")
            strsqlChasis.Append(" AssetSeqNo = '" & assetseqno & "' AND ")
            strsqlChasis.Append(" InsSequenceNo = '" & inssequenceno & "' AND ")
            strsqlChasis.Append(" EndorsItem = 'Chassis Number' AND ")
            strsqlChasis.Append(" EndorsKe = " & endorske)

            Context.Trace.Write("Sql = " & strsqlChasis.ToString)

            mySQLChasisCommand = New SqlCommand(strsqlChasis.ToString, conSearch)
            conSearch.Open()
            myChasisReader = mySQLChasisCommand.ExecuteReader
            txtChasis.Text = ""
            If myChasisReader.Read Then
                txtChasis.Text = CType(myChasisReader.Item("DataBenarDesc"), String)
                If CType(myChasisReader.Item("DataSalahDesc"), String) <> "" Then
                    txtChasis.Enabled = True
                    lblC.Visible = False
                Else
                    txtChasis.Enabled = False
                    lblC.Visible = True
                End If
            End If
            myChasisReader.Close()
            conSearch.Close()


            Dim mySqlCommandEngine As SqlCommand
            Dim ReaderEngine As SqlDataReader
            Dim strSqlEngine As New StringBuilder
            'strSqlEngine = "SELECT DataSalahDesc, DataBenarDesc " _
            '        & "FROM EndorsmentHistoryDetail " _
            '        & "WHERE ApplicationID = '" & applicationid & "' AND " _
            '        & "AssetSeqNo = '" & assetseqno & "' AND " _
            '        & "InsSequenceNo = '" & inssequenceno & "' AND " _
            '        & "YgEndors = '" & CType(objRow("SerialNo2"), String) & "' AND " _
            '        & "EndorsKe = " & endorske
            strSqlEngine.Append(" SELECT DataSalahDesc, DataBenarDesc ")
            strSqlEngine.Append(" FROM EndorsmentHistoryDetail ")
            strSqlEngine.Append(" WHERE ApplicationID = '" & ApplicationID & "' AND ")
            strSqlEngine.Append(" AssetSeqNo = '" & assetseqno & "' AND ")
            strSqlEngine.Append(" InsSequenceNo = '" & inssequenceno & "' AND  ")
            strSqlEngine.Append(" EndorsItem = 'Engine Number' AND ")
            strSqlEngine.Append(" EndorsKe = " & endorske)

            Context.Trace.Write("Sql = " & strSqlEngine.ToString)

            mySqlCommandEngine = New SqlCommand(strSqlEngine.ToString, conSearch)
            conSearch.Open()
            ReaderEngine = mySqlCommandEngine.ExecuteReader
            txtMachine.Text = ""
            If ReaderEngine.Read Then
                txtMachine.Text = CType(ReaderEngine.Item("DataBenarDesc"), String)
                If CType(ReaderEngine.Item("DataSalahDesc"), String) <> "" Then
                    txtMachine.Enabled = True
                    lblM.Visible = False
                Else
                    txtMachine.Enabled = False
                    lblM.Visible = True
                End If
            End If
            ReaderEngine.Close()
            conSearch.Close()

            If txtChasis.Text = "" Then
                txtChasis.Text = CType(objRow("SerialNo1"), String)
            End If
            If txtMachine.Text = "" Then
                txtMachine.Text = CType(objRow("SerialNo2"), String)
            End If
        Else
            Response.Write("<p><font color='red' face='Tahoma' size='2'>Data tidak Ditemukan</font></p>")
            'ImbValidate.Visible = False
        End If


        '=====================================================
        '========================View dtgEndors============================
        Dim LStrSQL2 As String
        Dim LObjAdpt2 As New SqlDataAdapter
        Dim LObjCom2 As SqlCommand
        dsSearch = New DataSet

        'LStrSQL2 = "SELECT * FROM EndorsmentHistoryDetail " & _
        '            " WHERE ApplicationID ='" & applicationid & "' " & _
        '            "   AND AssetSeqNo ='" & assetseqno & "' " & _
        '            "   AND InsSequenceNo ='" & inssequenceno & "' " & _
        '            "   AND DataSalahDesc ='' " & _
        '            "   AND Endorske = " & endorske

        Dim strSQLEndorsDetail As New StringBuilder
        strSQLEndorsDetail.Append(" SELECT EndorsItem,DataBenarDesc FROM EndorsmentHistoryDetail ")
        strSQLEndorsDetail.Append(" WHERE ApplicationID ='" & ApplicationID & "' ")
        strSQLEndorsDetail.Append(" AND AssetSeqNo ='" & assetseqno & "' ")
        strSQLEndorsDetail.Append(" AND InsSequenceNo ='" & inssequenceno & "' ")
        'strSQLEndorsDetail.Append(" AND DataSalahDesc ='-' ")
        strSQLEndorsDetail.Append(" AND Endorske = " & endorske)


        LObjAdpt2 = New SqlDataAdapter(strSQLEndorsDetail.ToString, conSearch)
        LObjAdpt2.Fill(dsSearch, "EndorsmentHistoryDetail")

        Dim dvEndors As DataView
        dvEndors = New DataView(dsSearch.Tables("EndorsmentHistoryDetail"))
        dtgEndors.DataSource = dvEndors
        dtgEndors.DataBind()

    End Sub
#End Region
#Region "ValidateData"

    Private Function ValidateData(ByVal txtText As String, ByVal txtJudul As String) As Boolean
        Try
            If txtText <> "" Then
                lblmessage.Visible = False
            Else
                lblmessage.Visible = True
                lblmessage.Text = " " & txtJudul & " Harap diisi "
                Return False
            End If
        Catch
            lblmessage.Visible = True
            lblmessage.Text = " " & txtJudul & " Harap diisi "
            Return False
        End Try
        Return True
    End Function
#End Region
#Region "ValidateDate"

    Private Function ValidateDate(ByVal txtText As String, ByVal txtJudul As String) As Boolean
        Dim arrdate() As String
        Dim tempdate As String
        If InStr(1, txtText, "/") >= 2 Then
            arrdate = Split(txtText, "/")
            tempdate = arrdate(1) & "/" & arrdate(0) & "/" & arrdate(2)
            Try
                If CDate(tempdate) <= CType(Me.BusinessDate, Date) Then
                    lblmessage.Visible = False
                Else
                    lblmessage.Visible = True
                    lblmessage.Text = " " & txtJudul & " Tanggal harus <= Hari ini"
                    Return False
                End If
            Catch
                lblmessage.Visible = True
                lblmessage.Text = " " & txtJudul & " Tanggal harus <= Hari ini"
                Return False
            End Try
        Else
            lblmessage.Visible = True
            lblmessage.Text = " " & txtJudul & " Tanggal Salah"
            Return False
        End If
        Return True
    End Function
#End Region
#Region "ValidateDate1"

    Private Function ValidateDate1(ByVal txtText As String, ByVal txtJudul As String) As Boolean
        Dim arrdate() As String
        Dim tempdate As String
        If InStr(1, txtText, "/") >= 2 Then
            arrdate = Split(txtText, "/")
            tempdate = arrdate(1) & "/" & arrdate(0) & "/" & arrdate(2)
            Try
                If CDate(tempdate) >= CType(ViewState("policydate"), Date) Then
                    lblmessage.Visible = False
                Else
                    lblmessage.Visible = True
                    lblmessage.Text = " " & txtJudul & " Tanggal Harus >= Tanggal Polis"
                    Return False
                End If
            Catch
                lblmessage.Visible = True
                lblmessage.Text = " " & txtJudul & " Tanggal Harus >= Tanggal Polis"
                Return False
            End Try
        Else
            lblmessage.Visible = True
            lblmessage.Text = " " & txtJudul & " Tanggal Salah"
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Div Table in code"

    Private Sub insertDiv(ByVal a As HtmlGenericControl, ByVal b As HtmlGenericControl)
        a.Controls.Add(b)
    End Sub

    Private Sub insertDiv(ByVal a As HtmlGenericControl, ByVal b As HtmlGenericControl, ByVal c As HtmlGenericControl)
        a.Controls.Add(b)
        a.Controls.Add(c)
    End Sub
    Private Function genDiv(ByVal classname As String) As HtmlGenericControl
        Dim a As New HtmlGenericControl("div")
        a.Attributes.Add("class", classname)
        Return a
    End Function

    Private Function genLabel(ByVal value As String) As HtmlGenericControl
        Dim a As New HtmlGenericControl("label")
        a.InnerHtml = value
        Return a
    End Function

    Private Function genCustom(ByVal value As Control) As HtmlGenericControl
        Dim a As New HtmlGenericControl()
        a.Controls.Add(value)
        Return a
    End Function

#End Region
#Region "Generate Table"
    Private Sub genTemplateDiv(ByVal labelLeft As String, ByVal valueLeft As String, ByVal ctrlRight As Control, ByVal valueRight As String, ByVal pnlTarget As Panel)
        genTemplateDiv(labelLeft, valueLeft, New Label, ctrlRight, valueRight, pnlTarget)
    End Sub

    Private Sub genTemplateDiv(ByVal labelLeft As String, ByVal valueLeft As String, ByVal ctrlLeft As Control, ByVal ctrlRight As Control, ByVal valueRight As String, ByVal pnlTarget As Panel)
        Dim row As HtmlGenericControl
        Dim innerRow As HtmlGenericControl
        Dim leftside As HtmlGenericControl
        Dim rightside As HtmlGenericControl
        Dim item1 As HtmlGenericControl
        Dim item2 As HtmlGenericControl

        row = genDiv("form_box")
        innerRow = genDiv("form_left")
        leftside = genLabel(labelLeft)

        rightside = genLabel(valueLeft)
        item1 = genCustom(ctrlLeft)
        insertDiv(rightside, item1)

        insertDiv(innerRow, leftside, rightside)
        insertDiv(row, innerRow)

        innerRow = genDiv("form_right")
        leftside = genLabel(valueRight)
        item1 = genCustom(ctrlRight)
        insertDiv(leftside, item1)

        insertDiv(innerRow, leftside)
        insertDiv(row, innerRow)

        pnlValidationDiv.Controls.Add(row)
    End Sub

    Private Sub Gen_Table(ByVal pblnIsFill As Boolean, ByRef counter As Integer)
        'Private Sub Gen_Table(ByVal pblnIsFill As Boolean)
        'Dim lObjTblRow As HtmlTableRow
        'Dim lObjTblCell As HtmlTableCell
        Dim lObjChkBox As HtmlInputCheckBox
        Dim lObjTxtBox As HtmlInputText
        Dim i As Integer
        Dim chkdtg As CheckBox
        Dim valValid1 As Boolean
        Dim valValid As Boolean
        Dim x As Integer
        x = dtgEndors.Items.Count
        Dim EndorsItem(x) As String
        Dim databenar(x) As String

        Dim row As HtmlGenericControl
        Dim innerRow As HtmlGenericControl


        agreementno = Request.QueryString("agreementno")
        assetseqno = Request.QueryString("assetseqno")
        inssequenceno = Request.QueryString("inssequenceno")
        ApplicationID = Request.QueryString("applicationid")
        valValid = True
        valValid1 = True
        counter = 0

        '   Dim CoInitial As String = lObjString.getShortNameCompany

        If (chkTanggalTerima.Checked = False) Or (chkTagihan.Checked = False) Or (chkMachine.Checked = False) Or (chkChasis.Checked = False) _
           Or (chkSumInsured.Checked = False) Or (chkNoPolis.Checked = False) Then

            If chkTanggalTerima.Checked = False Then
                counter = counter + 1
                lObjChkBox = New HtmlInputCheckBox
                lObjChkBox.ID = "chkNewTanggalTerima"
                If pblnIsFill Then
                    lObjChkBox.Disabled = False
                Else
                    lObjChkBox.Disabled = True
                End If

                genTemplateDiv("Tanggal Terima", ucTanggalTerima.Text, lObjChkBox, String.Empty, pnlValidationDiv)

            End If
            If chkTagihan.Checked = False Then
                counter = counter + 1
                lObjChkBox = New HtmlInputCheckBox
                lObjChkBox.ID = "chkNewTagihan"
                If pblnIsFill Then
                    lObjChkBox.Disabled = False
                Else
                    lObjChkBox.Disabled = True
                End If

                genTemplateDiv("Premi Ke Perusahaan Asuransi", txtTagihan.Text, lObjChkBox, String.Empty, pnlValidationDiv)

            End If
            If chkMachine.Checked = False Then
                counter = counter + 1
                lObjChkBox = New HtmlInputCheckBox
                lObjChkBox.ID = "chkNewMachine"
                If pblnIsFill Then
                    lObjChkBox.Disabled = False
                Else
                    lObjChkBox.Disabled = True
                End If

                genTemplateDiv("No Mesin", txtMachine.Text, lObjChkBox, String.Empty, pnlValidationDiv)

            End If
            If chkChasis.Checked = False Then
                counter = counter + 1
                lObjChkBox = New HtmlInputCheckBox
                lObjChkBox.ID = "chkNewChasis"
                If pblnIsFill Then
                    lObjChkBox.Disabled = False
                Else
                    lObjChkBox.Disabled = True
                End If

                genTemplateDiv("No Rangka", txtChasis.Text, lObjChkBox, String.Empty, pnlValidationDiv)

            End If
            If chkSumInsured.Checked = False Then
                counter = counter + 1
                lObjChkBox = New HtmlInputCheckBox
                lObjChkBox.ID = "chkNewSumInsured"
                If pblnIsFill Then
                    lObjChkBox.Disabled = False
                Else
                    lObjChkBox.Disabled = True
                End If

                genTemplateDiv("Nilai Pertanggungan", txtSumInsured.Text, lObjChkBox, String.Empty, pnlValidationDiv)

            End If
            If chkNoPolis.Checked = False Then
                counter = counter + 1
                lObjChkBox = New HtmlInputCheckBox
                lObjChkBox.ID = "chkNewNoPolis"
                If pblnIsFill Then
                    lObjChkBox.Disabled = False
                Else
                    lObjChkBox.Disabled = True
                End If

                genTemplateDiv("No Polis", lnkNoPolis.Text, lObjChkBox, String.Empty, pnlValidationDiv)

            End If
        End If


        c = 0
        Dim tampil As Boolean
        tampil = False

        For i = 0 To dtgEndors.Items.Count - 1
            chkdtg = New CheckBox
            chkdtg = CType(dtgEndors.Items(i).FindControl("chkEndors"), CheckBox)
            If chkdtg.Checked = False Then
                EndorsItem(counter) = dtgEndors.Items(i).Cells(0).Text
                databenar(counter) = dtgEndors.Items(i).Cells(1).Text
                If databenar(counter) = "&nbsp;" Then databenar(counter) = ""

                'lObjTblRow = New HtmlTableRow
                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .InnerText = EndorsItem(counter)
                '    .Attributes.Add("width", "25%")
                '    .Attributes.Add("bgcolor", "#ecf5ff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                row = genDiv("form_box")
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genLabel(EndorsItem(counter)))
                insertDiv(row, innerRow)

                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .InnerText = databenar(counter)
                '    .Attributes.Add("width", "50%")
                '    .Attributes.Add("bgcolor", "#ffffff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genLabel(databenar(counter)))
                insertDiv(row, innerRow)

                lObjChkBox = New HtmlInputCheckBox
                If pblnIsFill Then
                    lObjChkBox.Disabled = False
                Else
                    lObjChkBox.Disabled = True
                End If
                lObjChkBox.ID = "chkNewEndors" & counter

                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .Controls.Add(lObjChkBox)
                '    .Attributes.Add("width", "25%")
                '    .Attributes.Add("bgcolor", "#ffffff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                'tblValidation.Rows.Add(lObjTblRow)
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genCustom(lObjChkBox))
                insertDiv(row, innerRow)

                pnlValidationDiv.Controls.Add(row)

                counter = counter + 1
                c = c + 1
                tampil = True
            End If
        Next

        If tampil = True Then
            PnlValid.Visible = True
            PnlBtn.Visible = True
            Panel1.Visible = False
            valValid = False
        End If

        'Check If data billing sama dgn database atau tidak
        Dim lSumInsured As String
        Dim tSumInsured As String
        Dim lChasis As String
        Dim tChasis As String
        Dim lMachine As String
        Dim tMachine As String
        Dim lTagihan As String
        Dim tTagihan As String

        lSumInsured = lblSumInsured.Text
        tSumInsured = Replace(txtSumInsured.Text, ",", "")
        lChasis = lblChasis.Text
        tChasis = txtChasis.Text
        lMachine = lblMachine.Text
        tMachine = txtMachine.Text
        lTagihan = lblTagihan.Text
        tTagihan = txtTagihan.Text
        valValid1 = True
        If (lSumInsured <> tSumInsured) Or (lChasis <> tChasis) Or (lMachine <> tMachine) Then

            If lSumInsured <> tSumInsured Then
                counter = counter + 1
                'lObjTblRow = New HtmlTableRow
                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .InnerText = "Sum Insured"
                '    .Attributes.Add("width", "25%")
                '    .Attributes.Add("bgcolor", "#ecf5ff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                row = genDiv("form_box")
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genLabel("Sum Insured"))
                insertDiv(row, innerRow)

                lObjTxtBox = New HtmlInputText

                With lObjTxtBox
                    .ID = "txtNewSumInsured"
                    If pblnIsFill Then
                        .Disabled = False
                    Else
                        .Disabled = True
                    End If
                    .Value = tSumInsured
                End With

                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .Controls.Add(lObjTxtBox)
                '    .Attributes.Add("width", "35%")
                '    .Attributes.Add("bgcolor", "#ffffff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genCustom(lObjTxtBox))
                insertDiv(row, innerRow)

                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .InnerText = lSumInsured
                '    .Attributes.Add("width", "40%")
                '    .Attributes.Add("bgcolor", "#ffffff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                'tblValidation3.Rows.Add(lObjTblRow)

                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genLabel(lSumInsured))
                insertDiv(row, innerRow)

                pnlValidationDiv3.Controls.Add(row)

                valValid1 = False
            End If

            If lChasis <> tChasis Then
                counter = counter + 1
                'lObjTblRow = New HtmlTableRow
                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .InnerText = "Chasis Number"
                '    .Attributes.Add("width", "25%")
                '    .Attributes.Add("bgcolor", "#ecf5ff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                row = genDiv("form_box")
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genLabel("Chasis Number"))
                insertDiv(row, innerRow)

                lObjTxtBox = New HtmlInputText

                With lObjTxtBox
                    .ID = "txtNewChasis"
                    If pblnIsFill Then
                        .Disabled = False
                    Else
                        .Disabled = True
                    End If
                    .Value = tChasis
                End With
                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .Controls.Add(lObjTxtBox)
                '    .Attributes.Add("width", "35%")
                '    .Attributes.Add("bgcolor", "#ffffff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genCustom(lObjTxtBox))
                insertDiv(row, innerRow)

                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .InnerText = lChasis
                '    .Attributes.Add("width", "40%")
                '    .Attributes.Add("bgcolor", "#ffffff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                'tblValidation3.Rows.Add(lObjTblRow)

                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genLabel("lChasis"))
                insertDiv(row, innerRow)

                pnlValidationDiv3.Controls.Add(row)

                valValid1 = False
            End If

            If lMachine <> tMachine Then
                counter = counter + 1
                'lObjTblRow = New HtmlTableRow
                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .InnerText = "Machine Number"
                '    .Attributes.Add("width", "25%")
                '    .Attributes.Add("bgcolor", "#ecf5ff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                row = genDiv("form_box")
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genLabel("Machine Number"))
                insertDiv(row, innerRow)

                lObjTxtBox = New HtmlInputText
                With lObjTxtBox
                    .ID = "txtNewMachine"
                    If pblnIsFill Then
                        .Disabled = False
                    Else
                        .Disabled = True
                    End If
                    .Value = tMachine
                End With
                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .Controls.Add(lObjTxtBox)
                '    .Attributes.Add("width", "35%")
                '    .Attributes.Add("bgcolor", "#ffffff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genCustom(lObjTxtBox))
                insertDiv(row, innerRow)

                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .InnerText = lMachine
                '    .Attributes.Add("width", "40%")
                '    .Attributes.Add("bgcolor", "#ffffff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                'tblValidation3.Rows.Add(lObjTblRow)

                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genLabel(lMachine))
                insertDiv(row, innerRow)

                pnlValidationDiv3.Controls.Add(row)

                valValid1 = False
            End If

            pnlBilling.Visible = True
            PnlBtn.Visible = True
            Panel1.Visible = False

        End If

        If valValid And valValid1 Then

            Connection()


            Dim oUpdateInsAsset As New StringBuilder
            oUpdateInsAsset.Append(" UPDATE InsuranceAsset ")
            If Me.BDEndorsPolicyNo = "-" Then
                oUpdateInsAsset.Append(" SET BDEndorsPolicyReceiveDate = @BDEndorsPolicyReceiveDate, ")
            Else
                oUpdateInsAsset.Append(" SET PolicyReceiveDate = @PolicyReceiveDate, ")
            End If

            oUpdateInsAsset.Append(" FlagDocStatus = @FlagDocStatus, ")
            oUpdateInsAsset.Append(" FlagInsStatus = @FlagInsStatus, ")
            oUpdateInsAsset.Append(" PolicyNumber = @PolicyNumber,  ")
            oUpdateInsAsset.Append(" PolicyReceiveBy = @PolicyReceiveBy ")
            oUpdateInsAsset.Append(" WHERE ApplicationID = @ApplicationID ")
            oUpdateInsAsset.Append(" AND AssetSeqNo = @AssetSeqNo  ")
            oUpdateInsAsset.Append(" AND InsSequenceNo = @InsSequenceNo ")

            cmdSearch = New SqlCommand(oUpdateInsAsset.ToString, conSearch)

            Dim arrDate() As String
            Dim tempDate As String

            With cmdSearch

                If Me.BDEndorsPolicyNo = "-" Then
                    .Parameters.Add(New SqlParameter("@BDEndorsPolicyReceiveDate", SqlDbType.DateTime, 8))
                Else
                    .Parameters.Add(New SqlParameter("@PolicyReceiveDate", SqlDbType.DateTime, 8))
                End If

                .Parameters.Add(New SqlParameter("@FlagDocStatus", SqlDbType.Char, 1))
                .Parameters.Add(New SqlParameter("@FlagInsStatus", SqlDbType.Char, 1))
                .Parameters.Add(New SqlParameter("@PolicyNumber", SqlDbType.VarChar, 25))
                .Parameters.Add(New SqlParameter("@PolicyReceiveBy", SqlDbType.VarChar, 25))
                .Parameters.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20))
                .Parameters.Add(New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt, 2))
                .Parameters.Add(New SqlParameter("@InsSequenceNo", SqlDbType.SmallInt, 2))

                arrDate = Split(ucTanggalTerima.Text, "/")
                tempDate = arrDate(1) & "/" & arrDate(0) & "/" & arrDate(2)

                If Me.BDEndorsPolicyNo = "-" Then
                    .Parameters("@BDEndorsPolicyReceiveDate").Value = tempDate

                Else
                    .Parameters("@PolicyReceiveDate").Value = tempDate
                End If


                .Parameters("@FlagDocStatus").Value = "O"
                .Parameters("@FlagInsStatus").Value = "A"
                .Parameters("@PolicyNumber").Value = lblNoPolis.Text
                .Parameters("@PolicyReceiveBy").Value = Me.Loginid
                .Parameters("@ApplicationID").Value = ApplicationID
                .Parameters("@AssetSeqNo").Value = assetseqno
                .Parameters("@InsSequenceNo").Value = inssequenceno

                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
            End With
            'StepNotaAsuransi
            StepNotaAsuransi()
        End If


    End Sub
#End Region
#Region "LinkTo"

    Function LinkTo(ByVal pStrType As String, ByVal strCustType As String, ByVal strCustID As String, ByVal strAgreeNo As String, ByVal strAppID As String, ByVal strAsq As String, Optional ByVal strIsq As String = "", Optional ByVal strInsc As String = "", Optional ByVal strInsBr As String = "", Optional ByVal pCust As String = "", Optional ByVal assetcode As String = "", Optional ByVal checkbutton As String = "") As String
        Return "javascript:OpenWinMain('" & strCustType & pStrType & "','" & strCustID.Trim & "','" & strAgreeNo.Trim & "', '" & strAppID.Trim & "','" & strAsq.Trim & "', '" & strIsq.Trim & "','" & strInsc.Trim & "','" & strInsBr.Trim & "','" & pCust.Trim & "','" & assetcode.Trim & "','" & checkbutton.Trim & "')"
    End Function
#End Region
#Region "LinkToPN"

    Function LinkToPN(ByVal strAppID As String, ByVal strAsq As String, ByVal strIsq As String) As String
        Return "javascript:OpenWinPN('" & strAppID.Trim & "','" & strAsq.Trim & "','" & strIsq.Trim & "')"
    End Function
#End Region
#Region "Ketika tombol Exit Paling Atas di klik"
    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Response.Redirect("PolicyReceive.aspx?back=yes&InsuranceComBranchID=" + Me.InsuranceComBranchID + "")
    End Sub
#End Region
#Region "Tombol Keluar Paling Bawah!"
    Private Sub btnExitBawah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExitBawah.Click
        Response.Redirect("PolicyReceive.aspx?back=yes&InsuranceComBranchID=" + Me.InsuranceComBranchID + "")
    End Sub
#End Region
#Region "Ketika Tombol Validate dipijit !"
    Private Sub btnValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnValidate.Click
        Dim counter As Integer

        If ValidateData(ucTanggalTerima.Text, "Received Date") = False Then
            Exit Sub
        End If
        If ValidateDate(ucTanggalTerima.Text, "Received Date") = False Then
            Exit Sub
        End If
        If ValidateDate1(ucTanggalTerima.Text, "Received Date") = False Then
            Exit Sub
        End If

        StepNext2(False, counter)

        If CBool(Status) Then
            StepNotaAsuransi()
            Next1 = "False"
        Else
            Next1 = "True"
            pnlNotaAsuransi.Visible = False
        End If
    End Sub
#End Region
#Region "Ketika tombol edit diklik !"

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Panel1.Visible = True
        PnlValid.Visible = False
        PnlValid2.Visible = False
        pnlBilling.Visible = False
        PnlBtn.Visible = False
    End Sub
#End Region
#Region "Button"
    Private Sub btnEndors_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEndors.Click
        Next1 = "True"
        StepNotaAsuransi()
    End Sub
    Private Sub ButtonSaveNota_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSaveNota.Click
        Dim m_controller As New InsurancePolicyReceiveController
        Dim oInsurance As New Parameter.InsuranceStandardPremium
        Dim selis As Boolean = True
        Dim totalInput As Decimal = 0
        Dim result As Decimal = 0

        totalInput = (CDec(UcNotaAsuransi1.PremiNettxt) - (CDec(UcNotaAsuransi1.PPNtxt) - CDec(UcNotaAsuransi1.PPHtxt))) + CDec(UcNotaAsuransi1.BiayaPolistxt) + CDec(UcNotaAsuransi1.BiayaMateraitxt)
        result = Me.total - totalInput

        'If totalInput = 0 Then
        '    selis = False
        'Else
        '    If Math.Abs(result) > 0 Then
        '        selis = True
        '    End If
        'End If

        With oInsurance
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .AssetSeqNo = Me.assetseqno
            .InsSequenceNo = Me.inssequenceno
            .NoNotaAsuransi = UcNotaAsuransi1.NoNotatxt
            .TglNotaAsuransi = ConvertDate2(UcNotaAsuransi1.TglNota)
            .Selisih = selis
            .PremiGross = CDec(UcNotaAsuransi1.PremiGrosstxt)
            .Komisi = CDec(UcNotaAsuransi1.Komisitxt)
            .PremiNet = CDec(UcNotaAsuransi1.PremiNettxt)
            .PPN = CDec(UcNotaAsuransi1.PPNtxt)
            .PPH = CDec(UcNotaAsuransi1.PPHtxt)
            .AdminFee = CDec(UcNotaAsuransi1.BiayaPolistxt)
            .MeteraiFee = CDec(UcNotaAsuransi1.BiayaMateraitxt)
            .PayAmount = CDec(UcNotaAsuransi1.TotalTagihanlbl)
            .BillAmount = CDec(UcNotaAsuransi1.TotalTagihantxt)
        End With

        Try
            If Not CBool(Next1) Then
                StepNext1()
                StatusPrint = "no"
            Else
                StepNext3()
                StatusPrint = "yes"
            End If

            m_controller.NotaAsuransiSave(oInsurance)
            SendCookies()
            Response.Redirect("PolicyReceive.aspx?back=no&InsuranceComBranchID=" + Me.InsuranceComBranchID + "&print=" + StatusPrint + "")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
    End Sub
    Private Sub ButtonCancelNota_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancelNota.Click
        Response.Redirect("PolicyReceive.aspx?back=yes&InsuranceComBranchID=" + Me.InsuranceComBranchID + "")
    End Sub

#End Region
    Sub BindDataNotaAsuransi()
        Dim oData As New DataTable
        Dim oDoc As New DocRec

        oDoc.strConnection = GetConnectionString()
        oDoc.WhereCond = " InsuranceAsset.ApplicationId = '" + Me.ApplicationID.Trim + "'"
        oDoc.SpName = "spGetNotaAsuransi"
        oDoc = m_Doc.GetSPReport(oDoc)
        oData = oDoc.ListDataReport.Tables(0)

        If oData.Rows.Count > 0 Then
            Me.InsuranceComBranchID = oData.Rows(0).Item("InsuranceComBranchID").ToString.Trim

            UcNotaAsuransi1.Asuransilbl = oData.Rows(0).Item("InsuranceComBranchName").ToString
            UcNotaAsuransi1.NoKontraklbl = oData.Rows(0).Item("AgreementNo").ToString
            UcNotaAsuransi1.NamaKonsumenlbl = oData.Rows(0).Item("Name").ToString
            UcNotaAsuransi1.Tenorlbl = oData.Rows(0).Item("Tenor").ToString
            UcNotaAsuransi1.Periodelbl = IIf(CDate(oData.Rows(0).Item("StartDate").ToString).ToString("dd/MM/yyyy").Contains("1900"), "", CDate(oData.Rows(0).Item("StartDate").ToString).ToString("dd/MM/yyyy")).ToString + " s/d" _
                                        + " " + IIf(CDate(oData.Rows(0).Item("EndDate").ToString).ToString("dd/MM/yyyy").Contains("1900"), "", CDate(oData.Rows(0).Item("EndDate").ToString).ToString("dd/MM/yyyy")).ToString

            UcNotaAsuransi1.PremiGrosslbl = FormatNumber(oData.Rows(0).Item("PremiGross").ToString, 0)
            UcNotaAsuransi1.Komisilbl = FormatNumber(oData.Rows(0).Item("Komisi").ToString, 0)
            UcNotaAsuransi1.PremiNetlbl = FormatNumber(oData.Rows(0).Item("PremiNett").ToString, 0)
            UcNotaAsuransi1.PPNlbl = FormatNumber(oData.Rows(0).Item("PPn").ToString, 0)
            UcNotaAsuransi1.PPHlbl = FormatNumber(oData.Rows(0).Item("PPH").ToString, 0)
            UcNotaAsuransi1.BiayaPolislbl = FormatNumber(oData.Rows(0).Item("BiayaPolis").ToString, 0)
            UcNotaAsuransi1.BiayaMaterailbl = FormatNumber(oData.Rows(0).Item("BiayaMaterai").ToString, 0)

            UcNotaAsuransi1.PremiGrosstxt = FormatNumber(oData.Rows(0).Item("PremiGross").ToString, 0)
            UcNotaAsuransi1.Komisitxt = FormatNumber(oData.Rows(0).Item("Komisi").ToString, 0)
            UcNotaAsuransi1.PremiNettxt = FormatNumber(oData.Rows(0).Item("PremiNett").ToString, 0)
            UcNotaAsuransi1.PPNtxt = FormatNumber(oData.Rows(0).Item("PPn").ToString, 0)
            UcNotaAsuransi1.PPHtxt = FormatNumber(oData.Rows(0).Item("PPH").ToString, 0)
            UcNotaAsuransi1.BiayaPolistxt = FormatNumber(oData.Rows(0).Item("BiayaPolis").ToString, 0)
            UcNotaAsuransi1.BiayaMateraitxt = FormatNumber(oData.Rows(0).Item("BiayaMaterai").ToString, 0)

            Me.total = (CDec(UcNotaAsuransi1.PremiNetlbl) - (CDec(UcNotaAsuransi1.PPNlbl) - CDec(UcNotaAsuransi1.PPHlbl))) + CDec(UcNotaAsuransi1.BiayaPolislbl) + CDec(UcNotaAsuransi1.BiayaMaterailbl)

            UcNotaAsuransi1.TotalTagihanlbl = FormatNumber(total, 0)
            UcNotaAsuransi1.TotalTagihantxt = FormatNumber(total, 0)
            AutoChangesText()
        End If

    End Sub
    Sub txtPremiGross_load() Handles UcNotaAsuransi1.TextChanged_txtPremiGross
        AutoChangesText()
    End Sub
    Sub TextChanged_txtKomisi_load() Handles UcNotaAsuransi1.TextChanged_txtKomisi
        AutoChangesText()
    End Sub
    Sub TextChanged_txtPremiNet_load() Handles UcNotaAsuransi1.TextChanged_txtPremiNet
        AutoChangesText()
    End Sub
    Sub TextChanged_txtPPN_load() Handles UcNotaAsuransi1.TextChanged_txtPPN
        AutoChangesText()
    End Sub
    Sub TextChanged_txtPPH_load() Handles UcNotaAsuransi1.TextChanged_txtPPH
        AutoChangesText()
    End Sub
    Sub TextChanged_txtBiayaPolis_load() Handles UcNotaAsuransi1.TextChanged_txtBiayaPolis
        AutoChangesText()
    End Sub
    Sub TextChanged_txtBiayaMaterai_load() Handles UcNotaAsuransi1.TextChanged_txtBiayaMaterai
        AutoChangesText()
    End Sub
    Sub TextChanged_txtTotalTagihan_load() Handles UcNotaAsuransi1.TextChanged_txtTotalTagihan
        AutoChangesText()
    End Sub
    Sub AutoChangesText()
        UcNotaAsuransi1.PremisGrossSelisihlbl = FormatNumber(Abs(CDec(UcNotaAsuransi1.PremiGrosslbl) - CDec(UcNotaAsuransi1.PremiGrosstxt)).ToString, 0)
        UcNotaAsuransi1.KomisiSelisihlbl = FormatNumber(Abs(CDec(UcNotaAsuransi1.Komisilbl) - CDec(UcNotaAsuransi1.Komisitxt)).ToString, 0)
        UcNotaAsuransi1.PremiNetSelisihlbl = FormatNumber(Abs(CDec(UcNotaAsuransi1.PremiNetlbl) - CDec(UcNotaAsuransi1.PremiNettxt)).ToString, 0)
        UcNotaAsuransi1.PPNSelisihlbl = FormatNumber(Abs(CDec(UcNotaAsuransi1.PPNlbl) - CDec(UcNotaAsuransi1.PPNtxt)).ToString, 0)
        UcNotaAsuransi1.PPHSelisihlbl = FormatNumber(Abs(CDec(UcNotaAsuransi1.PPHlbl) - CDec(UcNotaAsuransi1.PPHtxt)).ToString, 0)
        UcNotaAsuransi1.BiayaPolisSelisihlbl = FormatNumber(Abs(CDec(UcNotaAsuransi1.BiayaPolislbl) - CDec(UcNotaAsuransi1.BiayaPolistxt)).ToString, 0)
        UcNotaAsuransi1.BiayaMateraiSelisihlbl = FormatNumber(Abs(CDec(UcNotaAsuransi1.BiayaMaterailbl) - CDec(UcNotaAsuransi1.BiayaMateraitxt)).ToString, 0)        

        UcNotaAsuransi1.PremiNettxt = FormatNumber(Abs(CDec(UcNotaAsuransi1.PremiGrosstxt) - CDec(UcNotaAsuransi1.Komisitxt)).ToString, 0)
        UcNotaAsuransi1.TotalTagihantxt = FormatNumber(((CDec(UcNotaAsuransi1.PremiGrosstxt) - CDec(UcNotaAsuransi1.Komisitxt)) - (CDec(UcNotaAsuransi1.PPNtxt) - CDec(UcNotaAsuransi1.PPHtxt))) + CDec(UcNotaAsuransi1.BiayaPolistxt) + CDec(UcNotaAsuransi1.BiayaMateraitxt).ToString, 0)
        UcNotaAsuransi1.TotalTagihanSelisihlbl = FormatNumber(Abs(CDec(UcNotaAsuransi1.TotalTagihanlbl) - CDec(UcNotaAsuransi1.TotalTagihantxt)).ToString, 0)
    End Sub
#Region "SendCookies"
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies(CommonCookiesHelper.COOKIES_ENDORS_LETTER_PRINT)
        Dim strWhere As New StringBuilder
        strWhere.Append(" And InsuranceAsset.BranchID='" & Replace(Me.BranchID, "'", "") & "' ")
        strWhere.Append(" And InsuranceAsset.ApplicationID='" & Replace(Me.ApplicationID, "'", "") & "' ")
        strWhere.Append(" And InsuranceAsset.AssetSeqNo='" & Replace(Me.assetseqno, "'", "") & "' ")
        strWhere.Append(" And InsuranceAsset.InsSequenceNo='" & Replace(Me.inssequenceno, "'", "") & "' ")
        If Not cookie Is Nothing Then
            cookie.Values("SearchBy") = strWhere.ToString
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(CommonCookiesHelper.COOKIES_ENDORS_LETTER_PRINT)
            cookieNew.Values("SearchBy") = strWhere.ToString
            Response.AppendCookie(cookieNew)
        End If
    End Sub

#End Region
    Sub StepNotaAsuransi()
        Panel1.Visible = False
        pnlNotaAsuransi.Visible = True
        pnlBilling.Visible = False
        PnlBtn.Visible = False
        PnlValid.Visible = False
        PnlValid2.Visible = False
        pnlValidationDiv.Visible = False
        pnlValidationDiv2.Visible = False
        pnlValidationDiv3.Visible = False
        BindDataNotaAsuransi()
    End Sub
    Sub StepNext1()
        Dim counter As Integer
        Gen_Table(False, counter)
    End Sub
    Sub StepNext2(ByVal pblnIsFill As Boolean, ByRef counter As Integer)
        'Private Sub Gen_Table(ByVal pblnIsFill As Boolean)
        'Dim lObjTblRow As HtmlTableRow
        'Dim lObjTblCell As HtmlTableCell
        Dim lObjChkBox As HtmlInputCheckBox
        Dim lObjTxtBox As HtmlInputText
        Dim i As Integer
        Dim chkdtg As CheckBox
        Dim valValid1 As Boolean
        Dim valValid As Boolean
        Dim x As Integer
        x = dtgEndors.Items.Count
        Dim EndorsItem(x) As String
        Dim databenar(x) As String

        Dim row As HtmlGenericControl
        Dim innerRow As HtmlGenericControl


        agreementno = Request.QueryString("agreementno")
        assetseqno = Request.QueryString("assetseqno")
        inssequenceno = Request.QueryString("inssequenceno")
        ApplicationID = Request.QueryString("applicationid")
        valValid = True
        valValid1 = True
        counter = 0

        '   Dim CoInitial As String = lObjString.getShortNameCompany

        c = 0
        Dim tampil As Boolean
        tampil = False

        For i = 0 To dtgEndors.Items.Count - 1
            chkdtg = New CheckBox
            chkdtg = CType(dtgEndors.Items(i).FindControl("chkEndors"), CheckBox)
            If chkdtg.Checked = False Then
                EndorsItem(counter) = dtgEndors.Items(i).Cells(0).Text
                databenar(counter) = dtgEndors.Items(i).Cells(1).Text
                If databenar(counter) = "&nbsp;" Then databenar(counter) = ""

                'lObjTblRow = New HtmlTableRow
                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .InnerText = EndorsItem(counter)
                '    .Attributes.Add("width", "25%")
                '    .Attributes.Add("bgcolor", "#ecf5ff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                row = genDiv("form_box")
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genLabel(EndorsItem(counter)))
                insertDiv(row, innerRow)

                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .InnerText = databenar(counter)
                '    .Attributes.Add("width", "50%")
                '    .Attributes.Add("bgcolor", "#ffffff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genLabel(databenar(counter)))
                insertDiv(row, innerRow)

                lObjChkBox = New HtmlInputCheckBox
                If pblnIsFill Then
                    lObjChkBox.Disabled = False
                Else
                    lObjChkBox.Disabled = True
                End If
                lObjChkBox.ID = "chkNewEndors" & counter

                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .Controls.Add(lObjChkBox)
                '    .Attributes.Add("width", "25%")
                '    .Attributes.Add("bgcolor", "#ffffff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                'tblValidation.Rows.Add(lObjTblRow)
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genCustom(lObjChkBox))
                insertDiv(row, innerRow)

                pnlValidationDiv.Controls.Add(row)

                counter = counter + 1
                c = c + 1
                tampil = True
            End If
        Next

        If (chkTanggalTerima.Checked = False) Or (chkTagihan.Checked = False) Or (chkMachine.Checked = False) Or (chkChasis.Checked = False) _
           Or (chkSumInsured.Checked = False) Or (chkNoPolis.Checked = False) Then

            If chkTanggalTerima.Checked = False Then
                counter = counter + 1
                lObjChkBox = New HtmlInputCheckBox
                lObjChkBox.ID = "chkNewTanggalTerima"
                If pblnIsFill Then
                    lObjChkBox.Disabled = False
                Else
                    lObjChkBox.Disabled = True
                End If

                genTemplateDiv("Tanggal Terima", ucTanggalTerima.Text, lObjChkBox, String.Empty, pnlValidationDiv)

            End If
            If chkTagihan.Checked = False Then
                counter = counter + 1
                lObjChkBox = New HtmlInputCheckBox
                lObjChkBox.ID = "chkNewTagihan"
                If pblnIsFill Then
                    lObjChkBox.Disabled = False
                Else
                    lObjChkBox.Disabled = True
                End If

                genTemplateDiv("Premi Ke Perusahaan Asuransi", txtTagihan.Text, lObjChkBox, String.Empty, pnlValidationDiv)

            End If
            If chkMachine.Checked = False Then
                counter = counter + 1
                lObjChkBox = New HtmlInputCheckBox
                lObjChkBox.ID = "chkNewMachine"
                If pblnIsFill Then
                    lObjChkBox.Disabled = False
                Else
                    lObjChkBox.Disabled = True
                End If

                genTemplateDiv("No Mesin", txtMachine.Text, lObjChkBox, String.Empty, pnlValidationDiv)

            End If
            If chkChasis.Checked = False Then
                counter = counter + 1
                lObjChkBox = New HtmlInputCheckBox
                lObjChkBox.ID = "chkNewChasis"
                If pblnIsFill Then
                    lObjChkBox.Disabled = False
                Else
                    lObjChkBox.Disabled = True
                End If

                genTemplateDiv("No Rangka", txtChasis.Text, lObjChkBox, String.Empty, pnlValidationDiv)

            End If
            If chkSumInsured.Checked = False Then
                counter = counter + 1
                lObjChkBox = New HtmlInputCheckBox
                lObjChkBox.ID = "chkNewSumInsured"
                If pblnIsFill Then
                    lObjChkBox.Disabled = False
                Else
                    lObjChkBox.Disabled = True
                End If

                genTemplateDiv("Nilai Pertanggungan", txtSumInsured.Text, lObjChkBox, String.Empty, pnlValidationDiv)

            End If
            If chkNoPolis.Checked = False Then
                counter = counter + 1
                lObjChkBox = New HtmlInputCheckBox
                lObjChkBox.ID = "chkNewNoPolis"
                If pblnIsFill Then
                    lObjChkBox.Disabled = False
                Else
                    lObjChkBox.Disabled = True
                End If

                genTemplateDiv("No Polis", lnkNoPolis.Text, lObjChkBox, String.Empty, pnlValidationDiv)

            End If

            tampil = True
        End If

        If tampil = True Then
            PnlValid.Visible = True
            PnlBtn.Visible = True
            Panel1.Visible = False
            valValid = False
        End If

        'Check If data billing sama dgn database atau tidak
        Dim lSumInsured As String
        Dim tSumInsured As String
        Dim lChasis As String
        Dim tChasis As String
        Dim lMachine As String
        Dim tMachine As String
        Dim lTagihan As String
        Dim tTagihan As String

        lSumInsured = lblSumInsured.Text
        tSumInsured = Replace(txtSumInsured.Text, ",", "")
        lChasis = lblChasis.Text
        tChasis = txtChasis.Text
        lMachine = lblMachine.Text
        tMachine = txtMachine.Text
        lTagihan = lblTagihan.Text
        tTagihan = txtTagihan.Text
        valValid1 = True
        If (lSumInsured <> tSumInsured) Or (lChasis <> tChasis) Or (lMachine <> tMachine) Then

            If lSumInsured <> tSumInsured Then
                counter = counter + 1
                'lObjTblRow = New HtmlTableRow
                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .InnerText = "Sum Insured"
                '    .Attributes.Add("width", "25%")
                '    .Attributes.Add("bgcolor", "#ecf5ff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                row = genDiv("form_box")
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genLabel("Sum Insured"))
                insertDiv(row, innerRow)

                lObjTxtBox = New HtmlInputText

                With lObjTxtBox
                    .ID = "txtNewSumInsured"
                    If pblnIsFill Then
                        .Disabled = False
                    Else
                        .Disabled = True
                    End If
                    .Value = tSumInsured
                End With

                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .Controls.Add(lObjTxtBox)
                '    .Attributes.Add("width", "35%")
                '    .Attributes.Add("bgcolor", "#ffffff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genCustom(lObjTxtBox))
                insertDiv(row, innerRow)

                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .InnerText = lSumInsured
                '    .Attributes.Add("width", "40%")
                '    .Attributes.Add("bgcolor", "#ffffff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                'tblValidation3.Rows.Add(lObjTblRow)

                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genLabel(lSumInsured))
                insertDiv(row, innerRow)

                pnlValidationDiv3.Controls.Add(row)

                valValid1 = False
            End If

            If lChasis <> tChasis Then
                counter = counter + 1
                'lObjTblRow = New HtmlTableRow
                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .InnerText = "Chasis Number"
                '    .Attributes.Add("width", "25%")
                '    .Attributes.Add("bgcolor", "#ecf5ff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                row = genDiv("form_box")
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genLabel("Chasis Number"))
                insertDiv(row, innerRow)

                lObjTxtBox = New HtmlInputText

                With lObjTxtBox
                    .ID = "txtNewChasis"
                    If pblnIsFill Then
                        .Disabled = False
                    Else
                        .Disabled = True
                    End If
                    .Value = tChasis
                End With
                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .Controls.Add(lObjTxtBox)
                '    .Attributes.Add("width", "35%")
                '    .Attributes.Add("bgcolor", "#ffffff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genCustom(lObjTxtBox))
                insertDiv(row, innerRow)

                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .InnerText = lChasis
                '    .Attributes.Add("width", "40%")
                '    .Attributes.Add("bgcolor", "#ffffff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                'tblValidation3.Rows.Add(lObjTblRow)

                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genLabel("lChasis"))
                insertDiv(row, innerRow)

                pnlValidationDiv3.Controls.Add(row)

                valValid1 = False
            End If

            If lMachine <> tMachine Then
                counter = counter + 1
                'lObjTblRow = New HtmlTableRow
                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .InnerText = "Machine Number"
                '    .Attributes.Add("width", "25%")
                '    .Attributes.Add("bgcolor", "#ecf5ff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                row = genDiv("form_box")
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genLabel("Machine Number"))
                insertDiv(row, innerRow)

                lObjTxtBox = New HtmlInputText
                With lObjTxtBox
                    .ID = "txtNewMachine"
                    If pblnIsFill Then
                        .Disabled = False
                    Else
                        .Disabled = True
                    End If
                    .Value = tMachine
                End With
                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .Controls.Add(lObjTxtBox)
                '    .Attributes.Add("width", "35%")
                '    .Attributes.Add("bgcolor", "#ffffff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genCustom(lObjTxtBox))
                insertDiv(row, innerRow)

                'lObjTblCell = New HtmlTableCell
                'With lObjTblCell
                '    .InnerText = lMachine
                '    .Attributes.Add("width", "40%")
                '    .Attributes.Add("bgcolor", "#ffffff")
                'End With
                'lObjTblRow.Cells.Add(lObjTblCell)
                'tblValidation3.Rows.Add(lObjTblRow)

                innerRow = genDiv("form_quarter")
                insertDiv(innerRow, genLabel(lMachine))
                insertDiv(row, innerRow)

                pnlValidationDiv3.Controls.Add(row)

                valValid1 = False
            End If

            pnlBilling.Visible = True
            PnlBtn.Visible = True
            Panel1.Visible = False
        End If
        Status = valValid1.ToString
    End Sub
    Sub StepNext3()
        Try


            assetseqno = Request.QueryString("assetseqno").Trim
            inssequenceno = Request.QueryString("inssequenceno").Trim
            ApplicationID = Request.QueryString("applicationid").Trim
            'ImbEdit.Visible = False
            Connection()


            Dim oUpdateAsset As New StringBuilder
            oUpdateAsset.Append("  UPDATE InsuranceAsset  ")
            oUpdateAsset.Append(" SET PolicyReceiveDate = @PolicyReceiveDate, ")
            oUpdateAsset.Append(" FlagDocStatus = @FlagDocStatus, ")
            oUpdateAsset.Append(" PolicyNumber = @PolicyNumber  ")
            oUpdateAsset.Append(" WHERE ApplicationID = @ApplicationID ")
            oUpdateAsset.Append(" AND AssetSeqNo = @AssetSeqNo ")
            oUpdateAsset.Append(" AND InsSequenceNo = @InsSequenceNo ")

            Context.Trace.Write(" SQLUpdate = " & oUpdateAsset.ToString)
            cmdSearch = New SqlCommand(oUpdateAsset.ToString, conSearch)
            With cmdSearch
                .Parameters.Add(New SqlParameter("@PolicyReceiveDate", SqlDbType.DateTime, 8))
                .Parameters.Add(New SqlParameter("@FlagDocStatus", SqlDbType.Char, 1))
                .Parameters.Add(New SqlParameter("@PolicyNumber", SqlDbType.VarChar, 25))
                .Parameters.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20))
                .Parameters.Add(New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt, 2))
                .Parameters.Add(New SqlParameter("@InsSequenceNo", SqlDbType.SmallInt, 2))
                .Parameters("@PolicyReceiveDate").Value = Me.BusinessDate
                .Parameters("@FlagDocStatus").Value = "E"
                .Parameters("@PolicyNumber").Value = lblNoPolis.Text
                .Parameters("@ApplicationID").Value = ApplicationID
                .Parameters("@AssetSeqNo").Value = assetseqno
                .Parameters("@InsSequenceNo").Value = inssequenceno

                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
            End With

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsuranceEndorsValid.aspx.vb", "ENDORS Proses 1", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try
        '====================Create data to EndorsmentHistory=================
        'Get Endorske from EndorsmentHistory

        Dim StrSQLEndorsHistory As New StringBuilder
        StrSQLEndorsHistory.Append(" SELECT Max(Endorske) as endorske FROM EndorsmentHistory  ")
        StrSQLEndorsHistory.Append(" WHERE ApplicationID ='" & ApplicationID & "' ")
        StrSQLEndorsHistory.Append(" AND AssetSeqNo ='" & assetseqno & "' ")
        StrSQLEndorsHistory.Append(" AND InsSequenceNo ='" & inssequenceno & "' ")

        Context.Trace.Write(" SQL Max Endors = " & StrSQLEndorsHistory.ToString)
        Dim objAdapter As New SqlDataAdapter(StrSQLEndorsHistory.ToString, conSearch)

        dsSearch = New DataSet
        objAdapter.Fill(dsSearch, "EndorsmentHistory")

        Dim endorske As Integer
        Dim objRow1 As DataRowView
        If dsSearch.Tables("EndorsmentHistory").Rows.Count > 0 Then
            objRow1 = dsSearch.Tables("EndorsmentHistory").DefaultView(0)
            endorske = CType(objRow1("endorske"), Integer)
        Else
            endorske = 0
        End If
        ViewState("nomorendors") = endorske + 1


        Dim notransaksi As String
        ' ======================== Generate Nomor Transaksi ==========================
        Try

            Dim objconnect As SqlConnection
            Dim cmd As SqlCommand
            Dim drd As SqlDataReader
            Dim objparam As SqlParameter
            'Declare @no as varchar(100)
            'exec (spGetNoTransaction) '000', '7/1/2003','ENDORS', @no output
            'print @no

            'Dim sqlgenerate As String = "select dbo.spGenerateNoTransaction ('" & Me.BranchID & "', '" & Me.BusinessDate & "','ENDORS') as NoTransaksi"
            'Dim sqlgenerate As String = "exec spGetNoTransaction  '" & Me.BranchID & "' , '" & Me.BusinessDate & " ','ENDORS, @No OutPut "
            cmd = New SqlCommand("spGetNoTransaction", conSearch)
            cmd.CommandType = CommandType.StoredProcedure

            With cmd
                .Connection.Open()
                .Parameters.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3))
                .Parameters("@BranchID").Value = Me.BranchID
                .Parameters.Add(New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime, 4))
                .Parameters("@BusinessDate").Value = Me.BusinessDate
                .Parameters.Add(New SqlParameter("@ID", SqlDbType.Char, 10))
                .Parameters("@ID").Value = "ENDORS"
                objparam = .Parameters.Add(New SqlParameter("@sequenceNo", SqlDbType.VarChar, 20))
                objparam.Direction = ParameterDirection.Output
                .ExecuteReader()
                .Connection.Close()
                notransaksi = CType(objparam.Value, String)
            End With

            Context.Trace.Write("No Transaksi = " & notransaksi)

        Catch ex As Exception
            Dim errtrans As New MaxiloanExceptions
            errtrans.WriteLog("InsuranceEndorsValid.aspx.vb", "Generate Nomor Transaksi EndorsDocNo ", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Context.Trace.Write("Error Di Generate No Transaction ")
        End Try

        'Insert data to EndorsmentHistory

        Dim oInsertEndors As New StringBuilder

        oInsertEndors.Append(" INSERT INTO EndorsmentHistory ")
        oInsertEndors.Append(" (BranchID, ApplicationID, AssetSeqNo,  ")
        oInsertEndors.Append(" InsSequenceNo, Endorske, EndorsDate, ")
        oInsertEndors.Append(" EndorsDocNo ) ")
        oInsertEndors.Append("  VALUES  ")
        oInsertEndors.Append(" (@BranchId, @ApplicationID, @AssetSeqNo, ")
        oInsertEndors.Append(" @InsSequenceNo, @Endorske, @EndorsDate, ")
        oInsertEndors.Append(" @EndorsDocNo ) ")

        Context.Trace.Write(" SqlInsert = " & oInsertEndors.ToString)
        cmdSearch = New SqlCommand(oInsertEndors.ToString, conSearch)

        With cmdSearch

            .Parameters.Add(New SqlParameter("@BranchId", SqlDbType.Char, 3))
            .Parameters.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20))
            .Parameters.Add(New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt, 2))
            .Parameters.Add(New SqlParameter("@InsSequenceNo", SqlDbType.SmallInt, 2))
            .Parameters.Add(New SqlParameter("@Endorske", SqlDbType.SmallInt, 2))
            .Parameters.Add(New SqlParameter("@EndorsDate", SqlDbType.SmallDateTime, 4))
            .Parameters.Add(New SqlParameter("@EndorsDocNo", SqlDbType.Char, 30))

            .Parameters("@BranchId").Value = BranchID
            .Parameters("@ApplicationID").Value = ApplicationID
            .Parameters("@AssetSeqNo").Value = assetseqno
            .Parameters("@InsSequenceNo").Value = inssequenceno
            .Parameters("@Endorske").Value = ViewState("nomorendors") '+ 1
            .Parameters("@EndorsDate").Value = Me.BusinessDate
            .Parameters("@EndorsDocNo").Value = notransaksi

            .Connection.Open()
            .ExecuteNonQuery()
            .Connection.Close()
        End With

        '==================== Update EndorsDocNo di tbl InsuranceAsset


        Dim oUpdateEndorsDocNo As New StringBuilder
        oUpdateEndorsDocNo.Append("  UPDATE InsuranceAsset  ")
        oUpdateEndorsDocNo.Append(" SET EndorsDocNo = @EndorsDocNo ")
        oUpdateEndorsDocNo.Append(" WHERE ApplicationID = @ApplicationID ")
        oUpdateEndorsDocNo.Append(" AND AssetSeqNo = @AssetSeqNo ")
        oUpdateEndorsDocNo.Append(" AND InsSequenceNo = @InsSequenceNo ")

        Context.Trace.Write("SqlUpdateEndors = " & oUpdateEndorsDocNo.ToString)
        cmdSearch = New SqlCommand(oUpdateEndorsDocNo.ToString, conSearch)
        With cmdSearch
            .Parameters.Add(New SqlParameter("@EndorsDocNo", SqlDbType.Char, 30))
            .Parameters.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20))
            .Parameters.Add(New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt, 2))
            .Parameters.Add(New SqlParameter("@InsSequenceNo", SqlDbType.SmallInt, 2))
            .Parameters("@EndorsDocNo").Value = notransaksi
            .Parameters("@ApplicationID").Value = ApplicationID
            .Parameters("@AssetSeqNo").Value = assetseqno
            .Parameters("@InsSequenceNo").Value = inssequenceno

            .Connection.Open()
            .ExecuteNonQuery()
            .Connection.Close()
        End With


        '===== End Create ==================================


        '==========Create data to EndorsmentHistoryDetail==========
        Dim counter As Integer
        Gen_Table(False, counter)
        Dim arrdata(counter) As String
        Dim arrisidata(counter) As String
        Dim arrdatabenar(counter) As String
        Dim i As Integer
        i = 0

        'Get value from Gen_Table
        Dim chk As HtmlInputCheckBox
        Dim txt As HtmlInputText

        'check chkNewEndors
        Dim r As Integer
        For r = 0 To counter '- 1
            'chk = CType(tblValidation.FindControl("chkNewEndors" & r), HtmlInputCheckBox)
            chk = CType(pnlValidationDiv.FindControl("chkNewEndors" & r), HtmlInputCheckBox)
            If Not IsNothing(chk) Then
                'arrdata(i) = tblValidation.Rows(r).Cells(0).InnerText  'EndorsItem(r) 'dtgEndors.Items(r).Cells(0).Text
                'arrisidata(i) = ""
                'arrdatabenar(i) = tblValidation.Rows(r).Cells(1).InnerText 'dtgEndors.Items(r).Cells(1).Text 'databenar(r)
                'If arrdatabenar(i) = "&nbsp;" Then arrdatabenar(i) = ""
                'i = i + 1
            End If
        Next
        'end check


        'txt = CType(tblValidation.FindControl("txtNewSumInsured"), HtmlInputText)
        txt = CType(pnlValidationDiv.FindControl("txtNewSumInsured"), HtmlInputText)
        If Not IsNothing(txt) Then
            arrdata(i) = "Sum Insured"
            If txtSumInsured.Text = "" Then arrisidata(i) = "-" Else arrisidata(i) = txtSumInsured.Text
            'arrisidata(i) = txtSumInsured.Text
            arrdatabenar(i) = lblSumInsured.Text
            i = i + 1
        End If

        'txt = CType(tblValidation.FindControl("txtNewChasis"), HtmlInputText)
        txt = CType(pnlValidationDiv.FindControl("txtNewChasis"), HtmlInputText)
        If Not IsNothing(txt) Then
            arrdata(i) = "Chassis Number"
            If txtChasis.Text = "" Then arrisidata(i) = "-" Else arrisidata(i) = txtChasis.Text
            'arrisidata(i) = txtChasis.Text
            arrdatabenar(i) = lblChasis.Text
            i = i + 1
        End If

        'txt = CType(tblValidation.FindControl("txtNewMachine"), HtmlInputText)
        txt = CType(pnlValidationDiv.FindControl("txtNewMachine"), HtmlInputText)
        If Not IsNothing(txt) Then
            arrdata(i) = "Engine Number"
            If txtMachine.Text = "" Then arrisidata(i) = "-" Else arrisidata(i) = txtMachine.Text
            'arrisidata(i) = txtMachine.Text
            arrdatabenar(i) = lblMachine.Text
            i = i + 1
        End If


        'Insert Data to EndorsmentHistoryDetail
        Dim x As Integer

        For x = 0 To i - 1
            'Dim UpdateCmd2 As String = "INSERT INTO EndorsmentHistoryDetail (ApplicationID, AssetSeqNo, InsSequenceNo, Endorske, EndorsItem, DataSalahDesc, DataBenarDesc) VALUES " & _
            '    "(@ApplicationID, @AssetSeqNo, @InsSequenceNo, @Endorske, @EndorsItem, @DataSalahDesc, @DataBenarDesc)"

            Dim oInsertEndorsDetail As New StringBuilder
            oInsertEndorsDetail.Append(" INSERT INTO  EndorsmentHistoryDetail  ")
            oInsertEndorsDetail.Append(" (BranchID, ApplicationID, AssetSeqNo, ")
            oInsertEndorsDetail.Append(" InsSequenceNo, Endorske, EndorsItem, ")
            oInsertEndorsDetail.Append(" DataSalahDesc, DataBenarDesc) ")
            oInsertEndorsDetail.Append(" VALUES  ")
            oInsertEndorsDetail.Append(" (@BranchID, @ApplicationID, @AssetSeqNo, ")
            oInsertEndorsDetail.Append("  @InsSequenceNo, @Endorske, @EndorsItem, ")
            oInsertEndorsDetail.Append(" @DataSalahDesc, @DataBenarDesc) ")

            Context.Trace.Write("SqlInsertEndorsDetail = " & oInsertEndorsDetail.ToString)
            cmdSearch = New SqlCommand(oInsertEndorsDetail.ToString, conSearch)

            With cmdSearch

                .Parameters.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3))
                .Parameters.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20))
                .Parameters.Add(New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt, 2))
                .Parameters.Add(New SqlParameter("@InsSequenceNo", SqlDbType.SmallInt, 2))
                .Parameters.Add(New SqlParameter("@Endorske", SqlDbType.SmallInt, 2))
                .Parameters.Add(New SqlParameter("@EndorsItem", SqlDbType.VarChar, 30))
                .Parameters.Add(New SqlParameter("@DataSalahDesc", SqlDbType.VarChar, 50))
                .Parameters.Add(New SqlParameter("@DataBenarDesc", SqlDbType.VarChar, 50))

                .Parameters("@BranchID").Value = BranchID
                .Parameters("@ApplicationID").Value = ApplicationID
                .Parameters("@AssetSeqNo").Value = assetseqno
                .Parameters("@InsSequenceNo").Value = inssequenceno
                .Parameters("@Endorske").Value = ViewState("nomorendors")
                .Parameters("@EndorsItem").Value = arrdata(x)
                .Parameters("@DataSalahDesc").Value = arrisidata(x)
                .Parameters("@DataBenarDesc").Value = arrdatabenar(x)

                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
            End With
        Next
        '==========End Insert======================================
        Try
            'cetak surat...ambil counter cetak surat ke..berapa ?

            Dim cmdctksurat As SqlCommand
            Dim drdctksurat As SqlDataReader
            Dim strSqlcetaksurat As New StringBuilder
            Dim countercetaksurat As Integer
            strSqlcetaksurat.Append(" SELECT isnull(mailprintednum,'0') as mailprintednum  FROM mailtransaction ")
            strSqlcetaksurat.Append(" WHERE ApplicationID ='" & Replace(ApplicationID.Trim, "'", "") & "' ")
            strSqlcetaksurat.Append(" AND branchid ='" & Replace(BranchID.Trim, "'", "") & "' ")
            strSqlcetaksurat.Append(" AND MailTypeID = 'ENDORS' ")
            conSearch.Open()
            Context.Trace.Write("string sql = " + strSqlcetaksurat.ToString)
            cmdctksurat = New SqlCommand(strSqlcetaksurat.ToString, conSearch)
            drdctksurat = cmdctksurat.ExecuteReader
            If drdctksurat.Read Then
                countercetaksurat = CType(drdctksurat("mailprintednum"), Integer)
            End If
            conSearch.Close()

        Catch ex As Exception
            Response.Write(ex.Message)
            Dim err2 As New MaxiloanExceptions
            err2.WriteLog("InsuranceEndorsValid", "Cetak Surat - Ambil Counter", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        End Try


        'Insert data ke mail transaction
        Try
            Dim countercetaksurat As Integer
            Dim oInsertMail As New StringBuilder
            oInsertMail.Append(" INSERT INTO MAILTRANSACTION ")
            oInsertMail.Append(" (BranchID, MailTypeID , MailTransNo, ")
            oInsertMail.Append(" applicationid, MailDateCreate, MailUserCreate, ")
            oInsertMail.Append(" MailDatePrint, MailPrintedNum ) ")
            oInsertMail.Append(" VALUES ")
            oInsertMail.Append(" (@BranchID, @MailTypeID, @MailTransNo ,  ")
            oInsertMail.Append(" @ApplicationID, @MailDateCreate, @MailUserCreate , ")
            oInsertMail.Append(" @MailDatePrint, @MailPrintedNum )")

            Context.Trace.Write("oInsertMail = " & oInsertMail.ToString)
            cmdSearch = New SqlCommand(oInsertMail.ToString, conSearch)
            With cmdSearch
                .Parameters.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3))
                .Parameters.Add(New SqlParameter("@MailTypeID", SqlDbType.Char, 20))
                .Parameters.Add(New SqlParameter("@MailTransNo", SqlDbType.Char, 30))
                .Parameters.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20))
                .Parameters.Add(New SqlParameter("@MailDateCreate", SqlDbType.DateTime))
                .Parameters.Add(New SqlParameter("@MailUserCreate", SqlDbType.Char, 50))
                .Parameters.Add(New SqlParameter("@MailDatePrint", SqlDbType.DateTime))
                .Parameters.Add(New SqlParameter("@MailPrintedNum", SqlDbType.Int))

                .Parameters("@BranchID").Value = BranchID
                .Parameters("@MailTypeID").Value = "ENDORS"
                .Parameters("@MailTransNo").Value = notransaksi
                .Parameters("@ApplicationID").Value = ApplicationID
                .Parameters("@MailDateCreate").Value = Me.BusinessDate
                .Parameters("@MailUserCreate").Value = Me.Loginid
                .Parameters("@MailDatePrint").Value = Me.BusinessDate
                .Parameters("@MailPrintedNum").Value = countercetaksurat + 1
                Context.Trace.Write("BranchID = " & BranchID)
                Context.Trace.Write("notransaksi = " & notransaksi)
                Context.Trace.Write("applicationid = " & ApplicationID)
                Context.Trace.Write("MailUserCreate = " & Me.Loginid)
                Context.Trace.Write("BusinessDate = " & Me.BusinessDate)
                Context.Trace.Write("countercetaksurat = " & countercetaksurat + 1)
                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
            End With
        Catch ex As Exception
            Response.Write(ex.Message)
            Dim err3 As New MaxiloanExceptions
            err3.WriteLog("InsuranceEndorsValid.aspx.vb", "Endors - Proses Mail Transaction ", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        End Try
    End Sub
End Class