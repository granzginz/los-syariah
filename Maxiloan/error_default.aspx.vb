﻿Public Class error_default
    Inherits WebBased

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ShowMessage(lblMessage, "Page error, please contact application support!", True)
    End Sub

End Class