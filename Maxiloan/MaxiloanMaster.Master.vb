﻿Imports Maxiloan.Controller
Imports System.IO

Public Class MaxiloanMaster
    Inherits Maxiloan.Webform.MasterBased

    'Inherits System.Web.UI.MasterPage

    Private oCustomClass As New Maxiloan.Parameter.Login
    Private oController As New LoginController


    Private Sub createMenu()
        Dim XMLFileName As String
        Dim WriteXML As New MainMenu
        Dim objCom As New am_com_001
        Dim LNumValid As Boolean = False

        XMLFileName = Request.ServerVariables("APPL_PHYSICAL_PATH") & _
             "webform.AppMgt\menu\" & "Menu_" & _
              Me.AppId & "_" & Me.Loginid & "_" & Me.GroubDbID & ".xml"

        oCustomClass.LoginId = Me.Loginid
        oCustomClass.Applicationid = Me.AppId

        LNumValid = objCom.isValidSuperUser(Me.Loginid, Me.Password)

        If LNumValid Then
            If Not File.Exists(XMLFileName) Then
                WriteXML.WriteXMLToFile(XMLFileName, Me.AppId)
            End If
            BacaFile(XMLFileName)
        Else
            Try
                If oController.ListTreeMenu(oCustomClass) Then
                    If File.Exists(XMLFileName) Then
                        File.Delete(XMLFileName)
                    End If
                    WriteXML.WriteXmlforUser(Me.AppId, Me.Loginid, XMLFileName)
                Else
                    If Not File.Exists(XMLFileName) Then
                        WriteXML.WriteXmlforUser(Me.AppId, Me.Loginid, XMLFileName)
                    End If
                End If
                BacaFile(XMLFileName)
            Catch exp As Exception
                Response.Write("No Menu")
            End Try
        End If
    End Sub

    Private Sub BacaFile(ByVal XMLFileName As String)
        Dim readXmlFileSample As New ReadXMLFile

        XMLFileName = Replace(Replace(XMLFileName, Request.ServerVariables("APPL_PHYSICAL_PATH"), ""), "\", "/")

        Dim strHeader As String
        Dim strNode As String
        Dim strFooter As String

        strHeader = "<script>" & vbCrLf
        strNode = "var menu;" & _
                    "function initMenu() {" & _
                    "menu = new dhtmlXMenuObject('menuObj');" & _
                    "menu.setIconsPath('Images/Menu/');" & _
                    "menu.loadXML('" & XMLFileName & "');}"
        strFooter = vbCrLf & "</script>"

        outtext.InnerHtml = strHeader & strNode & strFooter
    End Sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Not Page.IsPostBack Then
        createMenu()
        'End If
    End Sub
End Class