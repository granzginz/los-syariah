﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="am_login.aspx.vb" Inherits="Maxiloan.Webform.am_login" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>
    <style>
        .luvFb {
            align-items: center;
            background-color: #fff;
            border: none;
            border-radius: 8px;
            box-shadow: 0 2px 4px rgb(0 0 0 / 10%), 0 8px 16px rgb(0 0 0 / 10%);
            box-sizing: border-box;
            margin: auto;
            padding: 20px 0 28px;
            width: 322px;
            opacity: 90%;
            position: relative;
            top: 100px;
        }

        .carouselImg {
            overflow: hidden;
            height: 720px;
            width: 100%;
        }

        .footerBig {
            height: 237px;
        }

        .addTop9 {
            top: 9px;
        }

        .about {
            align-items: center;
            background-color: #fff;
            border: none;
            border-radius: 8px;
            box-shadow: 0 2px 4px rgb(0 0 0 / 10%), 0 8px 16px rgb(0 0 0 / 10%);
            box-sizing: border-box;
            margin: auto;
            padding: 20px 0 28px;
            width: auto;
            opacity: 50%;
            position: relative;
        }

        .pnlHead {
            height: 600px;
            background-image: url(Images/islamic-new-year.jpg);
            background-size: cover;
            background-repeat: no-repeat;
            margin: -1px;
            top: -1px;
            border: none;
        }
    </style>

    <%--Old Stylesheet--%>
    <%--<link href="Include/General.css" rel="stylesheet" type="text/css" />--%>

    <!-- Global stylesheets -->
    <link href="lib/limitless/assets/css/fonts.css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/colors.css" rel="stylesheet" type="text/css" />
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <%--<script type="text/javascript" src="lib/limitless/assets/js/plugins/loaders/pace.min.js"></script>--%>
    <script type="text/javascript" src="lib/limitless/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="lib/limitless/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="lib/limitless/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="lib/limitless/assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script type="text/javascript" src="lib/limitless/assets/js/core/app.js"></script>
    <script type="text/javascript" src="lib/limitless/assets/js/pages/login.js"></script>
    <script type="text/javascript" src="lib/limitless/assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="lib/limitless/assets/js/pages/components_notifications_pnotify.js"></script>

    <!-- /theme JS files -->

    <script type="text/javascript">

        $('.carousel').carousel({
            interval: 20
        })

        $(document).ready(function () {
            $('.carousel').carousel();

            document.getElementById("txtUsrNm").focus();
        });

    </script>
</head>

<body style="background: #ffffff;" class="navbar-top">

    <%--<div class="row">--%>
    <header>
    <div class="navbar navbar-fixed-top" style="background-color: #000063;">
    </div>
        
    <%--</div>--%>
    <%--<div class="row">--%>
    
    <div class="navbar navbar-inverse navbar-fixed-top header-highlight" style="box-shadow: 0px 4px 0px 0px black;background-color: #311a91;top: 10px;height: 120px;">
        <div class="navbar-header">
            <a href="Webform.AppMgt/forms/am_inbox_004.aspx" target="content">
                <%--<img src="Images/Login/islamic-logo-no-background-clipart.png" alt="company_logo" style="height: 120px; width: 120px;" />--%>
            </a>

        </div>

        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li>
                    <asp:Label ID="Label1" runat="server" CssClass="label navbar-text"></asp:Label></li>

                <li>
                    <%= Session("BranchName")%>
                </li>
            </ul>
        </div>


    </div>
    </header>

    <Main>
    <asp:Panel ID="pnllogininfo" runat="server">

        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main content -->
                <div class="content-wrapper">


                    <div class="panel pnlHead">
                        <!-- Advanced login -->
                        <form id="form1" runat="server" class="luvFb">
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                            <asp:ValidationSummary ID="VlsLogin" runat="server" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
                            <%--<asp:RequiredFieldValidator ID="RfvUserNm" runat="server" ErrorMessage="User Name cannot be blank"
                                        ControlToValidate="txtUsrNm" Display="None"></asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ID="RfvPwd" runat="server" ErrorMessage="Password cannot be blank"
                                        ControlToValidate="txtPwd" Display="None"></asp:RequiredFieldValidator>

                                    <asp:RequiredFieldValidator ID="rfvGroupDB" runat="server" ErrorMessage="Group DB must be fill"
                                        ControlToValidate="SlcGroupDBID" Display="None"></asp:RequiredFieldValidator>--%>




                            <asp:Panel ID="pnlgroupdb" runat="server">
                                <label>
                                    Cabang
                                </label>
                                <asp:DropDownList ID="SlcGroupDBID" runat="server">
                                </asp:DropDownList>
                                <asp:Button ID="btnLogin" runat="server" Text="Login" />
                            </asp:Panel>


                            <div class="panel-body login-form">
                                <div class="text-center">
                                    <h5 class="content-group">Login to your account <small class="display-block">Your credentials</small></h5>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <asp:DropDownList ID="cboApplication" runat="server" class="form-control"></asp:DropDownList>
                                    <div class="form-control-feedback">
                                        <i class="icon-user text-muted" style="top: 9px;"></i>
                                    </div>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <asp:TextBox ID="txtUsrNm" runat="server" class="form-control" name="user" placeholder="Username" data-animation="shake"></asp:TextBox>
                                    <div class="form-control-feedback">
                                        <i class="icon-user text-muted" style="top: 9px;"></i>
                                    </div>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <asp:TextBox ID="txtPwd" runat="server" class="form-control" type="password" name="pass" placeholder="Password"></asp:TextBox>
                                    <div class="form-control-feedback">
                                        <i class="icon-lock2 text-muted" style="top: 9px;"></i>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Button ID="btnLogin1" runat="server" Text="Login" class="btn bg-blue btn-block" />
                                </div>

                                <div class="content-divider text-muted form-group">
                                    <span>
                                        <asp:Label ID="lblDate" runat="server" CssClass="smallText black"></asp:Label></span>
                                </div>

                            </div>
                        </form>
                        <!-- /advanced login -->
                    </div>

                    <!-- Content area -->
                    <div class="content">


                        <div class="row">


                            <div class="col-md-6">
                                <div class="panel panel-flat border-left-xlg border-left-info">
								<div class="panel-heading">
									<h6 class="panel-title">Description</h6>
								</div>
								
								<div class="panel-body">
									A bank is a financial institution that accepts deposits from the public and creates a demand deposit while simultaneously making loans. Lending activities can be directly performed by the bank or indirectly through capital markets
								</div>
							</div>
                            </div>



                            <%--<div class="col-md-4">
                            </div>--%>
                        </div>



                    </div>
                    <!-- /content area -->


                    <%--  </div>--%>
                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->
    </asp:Panel>
    <%-- <div class="row">
                        <div class="col">--%>
    </Main>
    <footer>
    <div class="navbar navbar-default navbar-fixed-bottom">
        <ul class="nav navbar-nav no-border visible-xs-block">
            <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second"><i class="icon-circle-up2"></i></a></li>
        </ul>

        <div class="navbar-collapse collapse" id="navbar-second">
            <div class="navbar-left">
                <ul class="nav navbar-nav">
                    <li>

                        <img src="Images/Treemas.jfif" alt="company_logo" style="height: 27px; width: 62px;" />

                    </li>
                    <li>
                        <div class="navbar-text">
                            © 2021. IDeaLoan
                        </div>
                    </li>
                </ul>
            </div>


            <div class="navbar-right">
            </div>
        </div>


    </div>
    </footer>
    <%--       </div>
                    </div>--%>
</body>
</html>
