﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ChangeAddressARM.aspx.vb"
    Inherits="Maxiloan.Webform.ARMonitor.ChangeAddressARM" %>

<%@ Register TagPrefix="uc1" TagName="ucCompanyAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="System.Web.UI.WebControls" Assembly="System.Web" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ChangeAddressARM</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function SelectFromArray(itemArray) {
            var objAdd = 'document.forms[0].oAddress_';
            var pAddress = eval(objAdd + 'txtAddress');
            var pRT = eval(objAdd + 'txtRT');
            var pRW = eval(objAdd + 'txtRW');
            var pKelurahan = eval(objAdd + 'oLookUpZipCode_txtKelurahan');
            var pKecamatan = eval(objAdd + 'oLookUpZipCode_txtKecamatan');
            var pCity = eval(objAdd + 'oLookUpZipCode_txtCity');
            var pZipCode = eval(objAdd + 'oLookUpZipCode_txtZipCode');
            var pAreaPhone1 = eval(objAdd + 'txtAreaPhone1');
            var pPhone1 = eval(objAdd + 'txtPhone1');
            var pAreaPhone2 = eval(objAdd + 'txtAreaPhone2');
            var pPhone2 = eval(objAdd + 'txtPhone2');
            var pAreaFax = eval(objAdd + 'txtAreaFax');
            var pFax = eval(objAdd + 'txtFax');

            pAddress.value = itemArray[0][0];
            pRT.value = itemArray[0][1];
            pRW.value = itemArray[0][2];
            pKelurahan.value = itemArray[0][3];
            pKecamatan.value = itemArray[0][4];
            pCity.value = itemArray[0][5];
            pZipCode.value = itemArray[0][6];
            pAreaPhone1.value = itemArray[0][7];
            pPhone1.value = itemArray[0][8];
            pAreaPhone2.value = itemArray[0][9];
            pPhone2.value = itemArray[0][10];
            pAreaFax.value = itemArray[0][11];
            pFax.value = itemArray[0][12];
            return false;
        }
        function OpenWinCustomer(pID) {
            window.open('../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>    
    <form id="form1" runat="server">
    <input id="hdnAgreement" type="hidden" name="hdnAgreement" runat="server" />
    <input id="hdnCustName" type="hidden" name="hdnCustName" runat="server" />
    <input id="hdnCustID" type="hidden" name="hdnCustID" runat="server" />
    <div id="Div1" runat="server">
    </div>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    GANTI ALAMAT
                </h3>
            </div>
        </div>  
        <asp:Panel ID="pnlsearch" runat="server">        
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">
				Cabang</label>
                <asp:DropDownList ID="cboCG" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvCG" runat="server" ErrorMessage="*" ControlToValidate="cboCG" CssClass="validator_general"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem>Select One</asp:ListItem>
                    <asp:ListItem Value="Agreement.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" CssClass="inptype"></asp:TextBox>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"   Text="Search" CssClass ="small button blue"></asp:Button>
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>        
        </asp:Panel>    
        <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR GANTI ALAMAT
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">            
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgAddress" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                        AllowSorting="True"
                        AutoGenerateColumns="False" DataKeyField="ApplicationID" OnSortCommand="SortGrid">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="APPLICATIONID" HeaderText="NO APLIKASI">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypApplicationIDdtg" runat="server" Text='<%#Container.dataitem("ApplicationID")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AGREEMENTNO" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypAgreementNodtg" runat="server" Text='<%#Container.dataitem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <a href="javascript:OpenWinCustomer('<%# Container.DataItem("CustomerID")%>');">
                                        <asp:Label ID="hypCustomerdtg" runat="server" Text='<%#Container.dataitem("CustomerName")%>'>
                                        </asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="MAILINGADDRESS" SortExpression="MAILINGADDRESS" HeaderText="ALAMAT">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MAILINGCITY" SortExpression="MAILINGCITY" HeaderText="KOTA">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MAILINGPHONE1" SortExpression="MAILINGPHONE1" HeaderText="NOTELP-1">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MAILINGPHONE2" SortExpression="MAILINGPHONE2" HeaderText="NOTELP-2">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <a href='ChangeAddressARM.aspx?cmd=change&id=<%# DataBinder.eval(Container.dataitem,"ApplicationID")%>&no=<%# DataBinder.eval(Container.dataitem,"AgreementNo")%>'>
                                        <asp:Label ID="hypChangedtg" runat="server" Text="GANTI"></asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="ApplicationId" Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationId" runat="server" Text='<%# Container.Dataitem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                </asp:DataGrid>        
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>     
        </div>       
        </div>
        </asp:Panel>
        <asp:Panel ID="pnlChange" runat="server">
        <div class="form_box_title">
	        <div class="form_single">
                <label>No Kontrak</label>
                <asp:HyperLink ID="hypAgreementNo" runat="server"></asp:HyperLink>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Customer</label>
                <asp:HyperLink ID="hypCustomerName" runat="server"></asp:HyperLink>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Alamat</label>
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>RT/RW</label>
                <asp:Label ID="lblRT" runat="server"></asp:Label>&nbsp;/
                <asp:Label ID="lblRW" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kelurahan</label>
                <asp:Label ID="lblKelurahan" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kecamatan</label>
                <asp:Label ID="lblKecamatan" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kota</label>
                <asp:Label ID="lblCity" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kode Pos</label>
                <asp:Label ID="lblZipCode" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Telepon-1</label>
                <asp:Label ID="lblAreaPhone1" runat="server"></asp:Label>&nbsp;-
                <asp:Label ID="lblPhone1" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Telepon-2</label>
                <asp:Label ID="lblAreaPhone2" runat="server"></asp:Label>&nbsp;-
                <asp:Label ID="lblPhone2" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Fax</label>
                <asp:Label ID="lblAreaFax" runat="server"></asp:Label>&nbsp;-
                <asp:Label ID="lblFaxNo" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    ALAMAT TAGIH BARU
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Copy Alamat Dari</label>
                <asp:DropDownList ID="cboCopy" runat="server" onChange="SelectFromArray((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]);">
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_box_uc">
                <uc1:ucCompanyAddress id="oAddress" runat="server"></uc1:ucCompanyAddress>	        
        </div>
        <div class="form_box">
	        <div class="form_single">                
                <label class="label_general">Catatan</label>
                <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server"  Text="Save" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False"  Text="Cancel" CssClass ="small button gray">
            </asp:Button>
	    </div>        
    </asp:Panel>
    </form>
</body>
</html>
