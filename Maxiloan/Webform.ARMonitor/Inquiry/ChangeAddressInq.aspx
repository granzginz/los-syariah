﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ChangeAddressInq.aspx.vb"
    Inherits="Maxiloan.Webform.ARMonitor.ChangeAddressInq" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ChangeAddressInq</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    INQUIRY GANTI ALAMAT
                </h3>
            </div>
        </div>  
        <asp:Panel ID="pnlsearch" runat="server">   
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">
				Cabang</label>
                <asp:DropDownList ID="cboBranch" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvBranch" runat="server" ErrorMessage="*" Display="Dynamic" CssClass="validator_general"
                ControlToValidate="cboBranch"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem>Select One</asp:ListItem>
                    <asp:ListItem Value="AGreement.ApplicationID">No Aplikasi</asp:ListItem>
                    <asp:ListItem Value="Agreement.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" CssClass="inptype"></asp:TextBox>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue"></asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>             
        </asp:Panel>    
        <asp:Panel ID="pnlDtGrid" runat="server">  
        <div class="form_box_title">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgAddress" runat="server" Width="100%" AllowSorting="True"
                        AutoGenerateColumns="False" DataKeyField="ApplicationID"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="APPLICATIONID" HeaderText="NO APLIAKSI">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationIDdtg" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AGREEMENTNO" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAgreementNodtg" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CUSTOMERNAME" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerNamedtg" runat="server" Text='<%#Container.DataItem("CustomerName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="MAILINGADDRESS" SortExpression="MAILINGADDRESS" HeaderText="ALAMAT">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="DETAIL">                                
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Detail" ImageUrl="../../Images/icondetail.gif">
                                    </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                </asp:DataGrid>        
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>     
        </div>     
        </div>     
        </asp:Panel>
        <asp:Panel ID="pnlDetail" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    GANTI ALAMAT
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Kontrak</label>
                <asp:Label ID="lblAgreementNo" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Customer</label>
                <asp:Label ID="lblCustomerName" runat="server"></asp:Label>
	        </div>
        </div>    
        <div class="form_box_title">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgDetail" runat="server" Width="100%" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="SEQNO" HeaderText="NO">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CHANGEDATE" HeaderText="TGL GANTI" DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="ALAMAT SEBELUMNYA">                                
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="PrevAddress" Text='<%#Container.dataItem("PrevAddress")%>'>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PrevPhoneNo1" HeaderText="NO TELEPON SEBELUMNYA">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="ALAMAT BARU">                                
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="NewAddress" Text='<%#Container.dataItem("NewAddress")%>'>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="NewPhoneNo1" HeaderText="NO TELEPON BARU">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="NOTES" HeaderText="CATATAN">                                
                            </asp:BoundColumn>
                        </Columns>                        
                </asp:DataGrid>
            </div>  
        </div>        
        </div>   
        <asp:Panel ID="pnlAddress" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>&nbsp;ALAMAT
                </h4>
            </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>No</label>
                    <asp:Label ID="lblSeqNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Tanggal Ganti</label>	
                    <asp:Label ID="lblChangeDate" runat="server"></asp:Label>	
		        </div>	        
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Alamat</label>
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Kelurahan</label>
                    <asp:Label ID="lblKelurahan" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">
                    <label>Kecamatan</label>
                    <asp:Label ID="lblKecamatan" runat="server"></asp:Label>			
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Kota</label>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Kode Pos</label>		
                    <asp:Label ID="lblZipCode" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Telepon-1</label>
                    <asp:Label ID="lblPhone1" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>No Telepon-2</label>		
                    <asp:Label ID="lblPhone2" runat="server"></asp:Label>
		        </div>	        
        </div>  
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Fax</label>
                    <asp:Label ID="lblFaxNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
		        </div>	        
        </div>                     
        </asp:Panel>
        <div class="form_button">
            <asp:Button ID="ButtonBackDtl" runat="server"  Text="Back" CssClass ="small button gray"></asp:Button>&nbsp;
            <asp:Button ID="ButtonBackList" runat="server" Text="Back" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>
    </asp:Panel>
    </form>
</body>
</html>
