﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class SIPP1300 : Inherits ComponentBase
    Implements ISIPP1300

    Public Function GetSelectSIPP1300(oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300 Implements ISIPP1300.GetSelectSIPP1300
        Try
            Dim DSIPP1300 As New SQLEngine.SIPP.SIPP1300
            Return DSIPP1300.GetSelectSIPP1300(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSIPP1300(oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300 Implements ISIPP1300.GetSIPP1300
        Try
            Dim DSIPP1300 As New SQLEngine.SIPP.SIPP1300
            Return DSIPP1300.GetSIPP1300(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP1300Edit(oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300 Implements ISIPP1300.SIPP1300Edit
        Try
            Dim DSIPP1300 As New SQLEngine.SIPP.SIPP1300
            Return DSIPP1300.SIPP1300Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP1300Save(oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300 Implements ISIPP1300.SIPP1300Save
        Try
            Dim DSIPP1300 As New SQLEngine.SIPP.SIPP1300
            Return DSIPP1300.SIPP1300Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP1300Add(oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300 Implements ISIPP1300.SIPP1300Add
        Try
            Dim DSIPP1300 As New SQLEngine.SIPP.SIPP1300
            Return DSIPP1300.SIPP1300Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP1300Delete(oCustomClass As Parameter.SIPP1300) As String Implements ISIPP1300.SIPP1300Delete
        Try
            Dim DSIPP1300 As New SQLEngine.SIPP.SIPP1300
            Return DSIPP1300.SIPP1300Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo(oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300 Implements ISIPP1300.GetCbo
        Try
            Dim DSIPP1300 As New SQLEngine.SIPP.SIPP1300
            Return DSIPP1300.GetCbo(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300 Implements ISIPP1300.GetCboBulandataSIPP
        Try
            Dim DSIPP1300 As New SQLEngine.SIPP.SIPP1300
            Return DSIPP1300.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class


