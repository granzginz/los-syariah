﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ComponentBase
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
Imports Maxiloan.SQLEngine
#End Region


Public Class SIPP3010 : Inherits ComponentBase
    Implements ISIPP3010
    Public Function GetSIPP3010(oCustomClass As Parameter.SIPP3010) As Parameter.SIPP3010 Implements [Interface].ISIPP3010.GetSIPP3010
        Try
            Dim SIPP3010DA As New SQLEngine.SIPP.SIPP3010
            Return SIPP3010DA.GetSIPP3010(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSIPP3010Edit(ocustomClass As Parameter.SIPP3010) As Parameter.SIPP3010 Implements [Interface].ISIPP3010.GetSIPP3010Edit
        Try
            Dim SIPP3010DA As New SQLEngine.SIPP.SIPP3010
            Return SIPP3010DA.GetSIPP3010Edit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP3010) As Parameter.SIPP3010 Implements ISIPP3010.GetCboBulandataSIPP
        Try
            Dim DSIPP3010 As New SQLEngine.SIPP.SIPP3010
            Return DSIPP3010.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function SIPP3010SaveEdit(ocustomClass As Parameter.SIPP3010) As Parameter.SIPP3010 Implements [Interface].ISIPP3010.SIPP3010SaveEdit
        Try
            Dim SIPP3010DA As New SQLEngine.SIPP.SIPP3010
            Return SIPP3010DA.SIPP3010SaveEdit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP3010SaveAdd(ocustomClass As Parameter.SIPP3010) As Parameter.SIPP3010 Implements [Interface].ISIPP3010.SIPP3010SaveAdd
        Try
            Dim SIPP3010DA As New SQLEngine.SIPP.SIPP3010
            Return SIPP3010DA.SIPP3010SaveAdd(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Function GetCbo(ByVal oCustomClass As Parameter.SIPP3010) As Parameter.SIPP3010 Implements [Interface].ISIPP3010.GetCbo
        Try
            Dim SIPP3010DA As New SQLEngine.SIPP.SIPP3010
            Return SIPP3010DA.GetCbo(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SIPP3010Delete(ByVal oCustomClass As Parameter.SIPP3010) As String Implements [Interface].ISIPP3010.SIPP3010Delete
        Try
            Dim SIPP3010DA As New SQLEngine.SIPP.SIPP3010
            Return SIPP3010DA.SIPP3010Delete(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
End Class
