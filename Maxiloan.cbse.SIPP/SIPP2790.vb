﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region

Public Class SIPP2790 : Inherits ComponentBase
    Implements ISIPP2790

    Public Function GetSelectSIPP2790(oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790 Implements ISIPP2790.GetSelectSIPP2790
        Try
            Dim DSIPP2790 As New SQLEngine.SIPP.SIPP2790
            Return DSIPP2790.GetSelectSIPP2790(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSIPP2790(oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790 Implements ISIPP2790.GetSIPP2790
        Try
            Dim DSIPP2790 As New SQLEngine.SIPP.SIPP2790
            Return DSIPP2790.GetSIPP2790(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2790Edit(oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790 Implements ISIPP2790.SIPP2790Edit
        Try
            Dim DSIPP2790 As New SQLEngine.SIPP.SIPP2790
            Return DSIPP2790.SIPP2790Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2790Save(oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790 Implements ISIPP2790.SIPP2790Save
        Try
            Dim DSIPP2790 As New SQLEngine.SIPP.SIPP2790
            Return DSIPP2790.SIPP2790Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2790Add(oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790 Implements ISIPP2790.SIPP2790Add
        Try
            Dim DSIPP2790 As New SQLEngine.SIPP.SIPP2790
            Return DSIPP2790.SIPP2790Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2790Delete(oCustomClass As Parameter.SIPP2790) As String Implements ISIPP2790.SIPP2790Delete
        Try
            Dim DSIPP2790 As New SQLEngine.SIPP.SIPP2790
            Return DSIPP2790.SIPP2790Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo(oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790 Implements ISIPP2790.GetCbo
        Try
            Dim DSIPP2790 As New SQLEngine.SIPP.SIPP2790
            Return DSIPP2790.GetCbo(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790 Implements ISIPP2790.GetCboBulandataSIPP
        Try
            Dim DSIPP2790 As New SQLEngine.SIPP.SIPP2790
            Return DSIPP2790.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class
