﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ComponentBase
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
Imports Maxiloan.SQLEngine
#End Region


Public Class SIPP5310 : Inherits ComponentBase
    Implements ISIPP5310
    Public Function GetSIPP5310List(oCustomClass As Parameter.SIPP5310) As Parameter.SIPP5310 Implements [Interface].ISIPP5310.GetSIPP5310List
        Try
            Dim SIPP5310DA As New SQLEngine.SIPP.SIPP5310
            Return SIPP5310DA.GetSIPP5310(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSIPP5310Edit(ocustomClass As Parameter.SIPP5310) As Parameter.SIPP5310 Implements [Interface].ISIPP5310.GetSIPP5310Edit
        Try
            Dim SIPP5310DA As New SQLEngine.SIPP.SIPP5310
            Return SIPP5310DA.GetSIPP5310EditList(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function SIPP5310SaveEdit(ocustomClass As Parameter.SIPP5310) As Parameter.SIPP5310 Implements [Interface].ISIPP5310.SIPP5310SaveEdit
        Try
            Dim SIPP5310DA As New SQLEngine.SIPP.SIPP5310
            Return SIPP5310DA.SIPP5310SaveEdit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP5310) As Parameter.SIPP5310 Implements ISIPP5310.GetCboBulandataSIPP
        Try
            Dim DSIPP5310 As New SQLEngine.SIPP.SIPP5310
            Return DSIPP5310.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function SIPP5310SaveAdd(ocustomClass As Parameter.SIPP5310) As Parameter.SIPP5310 Implements [Interface].ISIPP5310.SIPP5310SaveAdd
        Try
            Dim SIPP5310DA As New SQLEngine.SIPP.SIPP5310
            Return SIPP5310DA.SIPP5310SaveAdd(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Function GetCbo(ByVal oCustomClass As Parameter.SIPP5310) As Parameter.SIPP5310 Implements [Interface].ISIPP5310.GetCbo
        Try
            Dim SIPP5310DA As New SQLEngine.SIPP.SIPP5310
            Return SIPP5310DA.GetCbo(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SIPP5310Delete(ByVal oCustomClass As Parameter.SIPP5310) As String Implements [Interface].ISIPP5310.SIPP5310Delete
        Try
            Dim SIPP5310DA As New SQLEngine.SIPP.SIPP5310
            Return SIPP5310DA.SIPP5310Delete(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
End Class
