﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ComponentBase
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
Imports Maxiloan.SQLEngine
#End Region


Public Class SIPP2100 : Inherits ComponentBase
    Implements ISIPP2100
    Public Function GetSIPP2100(oCustomClass As Parameter.SIPP2100) As Parameter.SIPP2100 Implements [Interface].ISIPP2100.GetSIPP2100
        Try
            Dim SIPP2100DA As New SQLEngine.SIPP.SIPP2100
            Return SIPP2100DA.GetSIPP2100(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSelectSIPP2100(oCustomClass As Parameter.SIPP2100) As Parameter.SIPP2100 Implements ISIPP2100.GetSelectSIPP2100
        Try
            Dim DSIPP2100 As New SQLEngine.SIPP.SIPP2100
            Return DSIPP2100.GetSelectSIPP2100(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSIPP2100Edit(ocustomClass As Parameter.SIPP2100) As Parameter.SIPP2100 Implements [Interface].ISIPP2100.GetSIPP2100Edit
        Try
            Dim SIPP2100DA As New SQLEngine.SIPP.SIPP2100
            Return SIPP2100DA.GetSIPP2100Edit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP2100) As Parameter.SIPP2100 Implements ISIPP2100.GetCboBulandataSIPP
        Try
            Dim DSIPP2100 As New SQLEngine.SIPP.SIPP2100
            Return DSIPP2100.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function SIPP2100SaveEdit(ocustomClass As Parameter.SIPP2100) As Parameter.SIPP2100 Implements [Interface].ISIPP2100.SIPP2100SaveEdit
        Try
            Dim SIPP2100DA As New SQLEngine.SIPP.SIPP2100
            Return SIPP2100DA.SIPP2100SaveEdit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2100SaveAdd(ocustomClass As Parameter.SIPP2100) As Parameter.SIPP2100 Implements [Interface].ISIPP2100.SIPP2100SaveAdd
        Try
            Dim SIPP2100DA As New SQLEngine.SIPP.SIPP2100
            Return SIPP2100DA.SIPP2100SaveAdd(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Function GetCbo(ByVal oCustomClass As Parameter.SIPP2100) As Parameter.SIPP2100 Implements [Interface].ISIPP2100.GetCbo
        Try
            Dim SIPP2100DA As New SQLEngine.SIPP.SIPP2100
            Return SIPP2100DA.GetCbo(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SIPP2100Delete(ByVal oCustomClass As Parameter.SIPP2100) As String Implements [Interface].ISIPP2100.SIPP2100Delete
        Try
            Dim SIPP2100DA As New SQLEngine.SIPP.SIPP2100
            Return SIPP2100DA.SIPP2100Delete(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
End Class
