﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class SIPP3020 : Inherits ComponentBase
    Implements ISIPP3020

    Public Function GetSelectSIPP3020(oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020 Implements ISIPP3020.GetSelectSIPP3020
        Try
            Dim DSIPP3020 As New SQLEngine.SIPP.SIPP3020
            Return DSIPP3020.GetSelectSIPP3020(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSIPP3020(oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020 Implements ISIPP3020.GetSIPP3020
        Try
            Dim DSIPP3020 As New SQLEngine.SIPP.SIPP3020
            Return DSIPP3020.GetSIPP3020(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP3020Edit(oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020 Implements ISIPP3020.SIPP3020Edit
        Try
            Dim DSIPP3020 As New SQLEngine.SIPP.SIPP3020
            Return DSIPP3020.SIPP3020Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP3020Save(oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020 Implements ISIPP3020.SIPP3020Save
        Try
            Dim DSIPP3020 As New SQLEngine.SIPP.SIPP3020
            Return DSIPP3020.SIPP3020Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP3020Add(oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020 Implements ISIPP3020.SIPP3020Add
        Try
            Dim DSIPP3020 As New SQLEngine.SIPP.SIPP3020
            Return DSIPP3020.SIPP3020Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP3020Delete(oCustomClass As Parameter.SIPP3020) As String Implements ISIPP3020.SIPP3020Delete
        Try
            Dim DSIPP3020 As New SQLEngine.SIPP.SIPP3020
            Return DSIPP3020.SIPP3020Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo(oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020 Implements ISIPP3020.GetCbo
        Try
            Dim DSIPP3020 As New SQLEngine.SIPP.SIPP3020
            Return DSIPP3020.GetCbo(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020 Implements ISIPP3020.GetCboBulandataSIPP
        Try
            Dim DSIPP3020 As New SQLEngine.SIPP.SIPP3020
            Return DSIPP3020.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class
