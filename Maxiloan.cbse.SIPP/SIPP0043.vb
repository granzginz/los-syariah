﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class SIPP0043 : Inherits ComponentBase
    Implements ISIPP0043

    Public Function GetSelectSIPP0043(oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043 Implements ISIPP0043.GetSelectSIPP0043
        Try
            Dim DSIPP0043 As New SQLEngine.SIPP.SIPP0043
            Return DSIPP0043.GetSelectSIPP0043(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSIPP0043(oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043 Implements ISIPP0043.GetSIPP0043
        Try
            Dim DSIPP0043 As New SQLEngine.SIPP.SIPP0043
            Return DSIPP0043.GetSIPP0043(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0043Edit(oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043 Implements ISIPP0043.SIPP0043Edit
        Try
            Dim DSIPP0043 As New SQLEngine.SIPP.SIPP0043
            Return DSIPP0043.SIPP0043Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0043Save(oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043 Implements ISIPP0043.SIPP0043Save
        Try
            Dim DSIPP0043 As New SQLEngine.SIPP.SIPP0043
            Return DSIPP0043.SIPP0043Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0043Add(oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043 Implements ISIPP0043.SIPP0043Add
        Try
            Dim DSIPP0043 As New SQLEngine.SIPP.SIPP0043
            Return DSIPP0043.SIPP0043Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0043Delete(oCustomClass As Parameter.SIPP0043) As String Implements ISIPP0043.SIPP0043Delete
        Try
            Dim DSIPP0043 As New SQLEngine.SIPP.SIPP0043
            Return DSIPP0043.SIPP0043Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo(oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043 Implements ISIPP0043.GetCbo
        Try
            Dim DSIPP0043 As New SQLEngine.SIPP.SIPP0043
            Return DSIPP0043.GetCbo(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043 Implements ISIPP0043.GetCboBulandataSIPP
        Try
            Dim DSIPP0043 As New SQLEngine.SIPP.SIPP0043
            Return DSIPP0043.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class
