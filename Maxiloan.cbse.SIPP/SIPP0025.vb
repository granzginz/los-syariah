﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ComponentBase
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
Imports Maxiloan.SQLEngine
#End Region


Public Class SIPP0025 : Inherits ComponentBase
    Implements ISIPP0025
    Public Function GetSIPP0025(oCustomClass As Parameter.SIPP0025) As Parameter.SIPP0025 Implements [Interface].ISIPP0025.GetSIPP0025
        Try
            Dim SIPP0025DA As New SQLEngine.SIPP.SIPP0025
            Return SIPP0025DA.GetSIPP0025(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSIPP0025Edit(ocustomClass As Parameter.SIPP0025) As Parameter.SIPP0025 Implements [Interface].ISIPP0025.GetSIPP0025Edit
        Try
            Dim SIPP0025DA As New SQLEngine.SIPP.SIPP0025
            Return SIPP0025DA.GetSIPP0025Edit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0025SaveEdit(ocustomClass As Parameter.SIPP0025) As Parameter.SIPP0025 Implements [Interface].ISIPP0025.SIPP0025SaveEdit
        Try
            Dim SIPP0025DA As New SQLEngine.SIPP.SIPP0025
            Return SIPP0025DA.SIPP0025SaveEdit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0025SaveAdd(ocustomClass As Parameter.SIPP0025) As Parameter.SIPP0025 Implements [Interface].ISIPP0025.SIPP0025SaveAdd
        Try
            Dim SIPP0025DA As New SQLEngine.SIPP.SIPP0025
            Return SIPP0025DA.SIPP0025SaveAdd(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Function GetCbo(ByVal oCustomClass As Parameter.SIPP0025) As Parameter.SIPP0025 Implements [Interface].ISIPP0025.GetCbo
        Try
            Dim SIPP0025DA As New SQLEngine.SIPP.SIPP0025
            Return SIPP0025DA.GetCbo(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SIPP0025Delete(ByVal oCustomClass As Parameter.SIPP0025) As String Implements [Interface].ISIPP0025.SIPP0025Delete
        Try
            Dim SIPP0025DA As New SQLEngine.SIPP.SIPP0025
            Return SIPP0025DA.SIPP0025Delete(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function


    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP0025) As Parameter.SIPP0025 Implements ISIPP0025.GetCboBulandataSIPP
        Try
            Dim DSIPP0025 As New SQLEngine.SIPP.SIPP0025
            Return DSIPP0025.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class
