﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class SIPP0035 : Inherits ComponentBase
    Implements ISIPP0035

    Public Function GetSelectSIPP0035(oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035 Implements ISIPP0035.GetSelectSIPP0035
        Try
            Dim DSIPP0035 As New SQLEngine.SIPP.SIPP0035
            Return DSIPP0035.GetSelectSIPP0035(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSIPP0035(oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035 Implements ISIPP0035.GetSIPP0035
        Try
            Dim DSIPP0035 As New SQLEngine.SIPP.SIPP0035
            Return DSIPP0035.GetSIPP0035(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0035Edit(oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035 Implements ISIPP0035.SIPP0035Edit
        Try
            Dim DSIPP0035 As New SQLEngine.SIPP.SIPP0035
            Return DSIPP0035.SIPP0035Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0035Save(oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035 Implements ISIPP0035.SIPP0035Save
        Try
            Dim DSIPP0035 As New SQLEngine.SIPP.SIPP0035
            Return DSIPP0035.SIPP0035Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0035Add(oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035 Implements ISIPP0035.SIPP0035Add
        Try
            Dim DSIPP0035 As New SQLEngine.SIPP.SIPP0035
            Return DSIPP0035.SIPP0035Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0035Delete(oCustomClass As Parameter.SIPP0035) As String Implements ISIPP0035.SIPP0035Delete
        Try
            Dim DSIPP0035 As New SQLEngine.SIPP.SIPP0035
            Return DSIPP0035.SIPP0035Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo(oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035 Implements ISIPP0035.GetCbo
        Try
            Dim DSIPP0035 As New SQLEngine.SIPP.SIPP0035
            Return DSIPP0035.GetCbo(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035 Implements ISIPP0035.GetCboBulandataSIPP
        Try
            Dim DSIPP0035 As New SQLEngine.SIPP.SIPP0035
            Return DSIPP0035.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class

