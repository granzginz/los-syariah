﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ComponentBase
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
Imports Maxiloan.SQLEngine
#End Region


Public Class SIPP1200 : Inherits ComponentBase
    Implements ISIPP1200
    Public Function GetSIPP1200List(oCustomClass As Parameter.SIPP1200) As Parameter.SIPP1200 Implements [Interface].ISIPP1200.GetSIPP1200List
        Try
            Dim SIPP1200DA As New SQLEngine.SIPP.SIPP1200
            Return SIPP1200DA.GetSIPP1200List(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSIPP1200Edit(ocustomClass As Parameter.SIPP1200) As Parameter.SIPP1200 Implements [Interface].ISIPP1200.GetSIPP1200Edit
        Try
            Dim SIPP1200DA As New SQLEngine.SIPP.SIPP1200
            Return SIPP1200DA.GetSIPP1200Edit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function SIPP1200SaveEdit(ocustomClass As Parameter.SIPP1200) As Parameter.SIPP1200 Implements [Interface].ISIPP1200.SIPP1200SaveEdit
        Try
            Dim SIPP1200DA As New SQLEngine.SIPP.SIPP1200
            Return SIPP1200DA.SIPP1200SaveEdit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP1200SaveAdd(ocustomClass As Parameter.SIPP1200) As Parameter.SIPP1200 Implements [Interface].ISIPP1200.SIPP1200SaveAdd
        Try
            Dim SIPP1200DA As New SQLEngine.SIPP.SIPP1200
            Return SIPP1200DA.SIPP1200SaveAdd(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Function GetCbo(ByVal oCustomClass As Parameter.SIPP1200) As Parameter.SIPP1200 Implements [Interface].ISIPP1200.GetCbo
        Try
            Dim SIPP1200DA As New SQLEngine.SIPP.SIPP1200
            Return SIPP1200DA.GetCbo(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SIPP1200Delete(ByVal oCustomClass As Parameter.SIPP1200) As String Implements [Interface].ISIPP1200.SIPP1200Delete
        Try
            Dim SIPP1200DA As New SQLEngine.SIPP.SIPP1200
            Return SIPP1200DA.SIPP1200Delete(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP1200) As Parameter.SIPP1200 Implements ISIPP1200.GetCboBulandataSIPP
        Try
            Dim DSIPP1200 As New SQLEngine.SIPP.SIPP1200
            Return DSIPP1200.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class
