﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ComponentBase
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
Imports Maxiloan.SQLEngine
#End Region


Public Class SIPP0041 : Inherits ComponentBase
    Implements ISIPP0041
    Public Function GetSIPP0041Edit(oCustomClass As Parameter.SIPP0041) As Parameter.SIPP0041 Implements [Interface].ISIPP0041.GetSIPP0041Edit
        Try
            Dim SIPP0041DA As New SQLEngine.SIPP.SIPP0041
            Return SIPP0041DA.GetSIPP0041EditList(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSIPP0041List(ocustomClass As Parameter.SIPP0041) As Parameter.SIPP0041 Implements [Interface].ISIPP0041.GetSIPP0041List
        Try
            Dim SIPP0041DA As New SQLEngine.SIPP.SIPP0041
            Return SIPP0041DA.GetSIPP0041(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0041AddEdit(ocustomClass As Parameter.SIPP0041) As Parameter.SIPP0041 Implements [Interface].ISIPP0041.SIPP0041AddEdit
        Try
            Dim SIPP0041DA As New SQLEngine.SIPP.SIPP0041
            Return SIPP0041DA.SIPP0041SaveEdit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0041SaveAdd(ocustomClass As Parameter.SIPP0041) As Parameter.SIPP0041 Implements [Interface].ISIPP0041.SIPP0041SaveAdd
        Try
            Dim SIPP0041DA As New SQLEngine.SIPP.SIPP0041
            Return SIPP0041DA.SIPP0041SaveAdd(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP0041) As Parameter.SIPP0041 Implements ISIPP0041.GetCboBulandataSIPP
        Try
            Dim DSIPP0041 As New SQLEngine.SIPP.SIPP0041
            Return DSIPP0041.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Function GetCbo(ByVal oCustomClass As Parameter.SIPP0041) As Parameter.SIPP0041 Implements [Interface].ISIPP0041.GetCbo
        '    Try
        '        Dim SIPP0041DA As New SQLEngine.SIPP.SIPP0041
        '        Return SIPP0041DA.GetCbo(oCustomClass)
        '    Catch ex As Exception
        '        'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        '    End Try
    End Function
    Function SIPP0041Delete(ByVal oCustomClass As Parameter.SIPP0041) As String Implements [Interface].ISIPP0041.SIPP0041Delete
        Try
            Dim SIPP0041DA As New SQLEngine.SIPP.SIPP0041
            Return SIPP0041DA.SIPP0041Delete(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
End Class
