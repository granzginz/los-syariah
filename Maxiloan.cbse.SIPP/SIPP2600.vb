﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class SIPP2600 : Inherits ComponentBase
    Implements ISIPP2600

    Public Function GetSelectSIPP2600(oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600 Implements ISIPP2600.GetSelectSIPP2600
        Try
            Dim DSIPP2600 As New SQLEngine.SIPP.SIPP2600
            Return DSIPP2600.GetSelectSIPP2600(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSIPP2600(oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600 Implements ISIPP2600.GetSIPP2600
        Try
            Dim DSIPP2600 As New SQLEngine.SIPP.SIPP2600
            Return DSIPP2600.GetSIPP2600(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2600Edit(oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600 Implements ISIPP2600.SIPP2600Edit
        Try
            Dim DSIPP2600 As New SQLEngine.SIPP.SIPP2600
            Return DSIPP2600.SIPP2600Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2600Save(oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600 Implements ISIPP2600.SIPP2600Save
        Try
            Dim DSIPP2600 As New SQLEngine.SIPP.SIPP2600
            Return DSIPP2600.SIPP2600Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2600Add(oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600 Implements ISIPP2600.SIPP2600Add
        Try
            Dim DSIPP2600 As New SQLEngine.SIPP.SIPP2600
            Return DSIPP2600.SIPP2600Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2600Delete(oCustomClass As Parameter.SIPP2600) As String Implements ISIPP2600.SIPP2600Delete
        Try
            Dim DSIPP2600 As New SQLEngine.SIPP.SIPP2600
            Return DSIPP2600.SIPP2600Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo(oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600 Implements ISIPP2600.GetCbo
        Try
            Dim DSIPP2600 As New SQLEngine.SIPP.SIPP2600
            Return DSIPP2600.GetCbo(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600 Implements ISIPP2600.GetCboBulandataSIPP
        Try
            Dim DSIPP2600 As New SQLEngine.SIPP.SIPP2600
            Return DSIPP2600.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class
