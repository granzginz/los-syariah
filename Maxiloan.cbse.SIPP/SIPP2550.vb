﻿#Region "Imports"
Imports Maxiloan.BusinessProcess
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ComponentBase
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
Imports Maxiloan.SQLEngine
#End Region


Public Class SIPP2550 : Inherits ComponentBase
    Implements ISIPP2550
    Public Function GetSIPP2550(oCustomClass As Parameter.SIPP2550) As Parameter.SIPP2550 Implements [Interface].ISIPP2550.GetSIPP2550
        Try
            Dim SIPP2550DA As New SQLEngine.SIPP.SIPP2550
            Return SIPP2550DA.GetSIPP2550(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSIPP2550Edit(ocustomClass As Parameter.SIPP2550) As Parameter.SIPP2550 Implements [Interface].ISIPP2550.GetSIPP2550Edit
        Try
            Dim SIPP2550DA As New SQLEngine.SIPP.SIPP2550
            Return SIPP2550DA.GetSIPP2550Edit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2550SaveEdit(ocustomClass As Parameter.SIPP2550) As Parameter.SIPP2550 Implements [Interface].ISIPP2550.SIPP2550SaveEdit
        Try
            Dim SIPP2550DA As New SQLEngine.SIPP.SIPP2550
            Return SIPP2550DA.SIPP2550SaveEdit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2550SaveAdd(ocustomClass As Parameter.SIPP2550) As Parameter.SIPP2550 Implements [Interface].ISIPP2550.SIPP2550SaveAdd
        Try
            Dim SIPP2550DA As New SQLEngine.SIPP.SIPP2550
            Return SIPP2550DA.SIPP2550SaveAdd(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Function GetCbo(ByVal oCustomClass As Parameter.SIPP2550) As Parameter.SIPP2550 Implements [Interface].ISIPP2550.GetCbo
        Try
            Dim SIPP2550DA As New SQLEngine.SIPP.SIPP2550
            Return SIPP2550DA.GetCbo(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SIPP2550Delete(ByVal oCustomClass As Parameter.SIPP2550) As String Implements [Interface].ISIPP2550.SIPP2550Delete
        Try
            Dim SIPP2550DA As New SQLEngine.SIPP.SIPP2550
            Return SIPP2550DA.SIPP2550Delete(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP2550) As Parameter.SIPP2550 Implements ISIPP2550.GetCboBulandataSIPP
        Try
            Dim DSIPP2550 As New SQLEngine.SIPP.SIPP2550
            Return DSIPP2550.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class
