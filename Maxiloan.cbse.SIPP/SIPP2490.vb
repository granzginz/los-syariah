﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region

Public Class SIPP2490 : Inherits ComponentBase
    Implements ISIPP2490

    Public Function GetSelectSIPP2490(oCustomClass As Parameter.SIPP2490) As Parameter.SIPP2490 Implements ISIPP2490.GetSelectSIPP2490
        Try
            Dim DSIPP2490 As New SQLEngine.SIPP.SIPP2490
            Return DSIPP2490.GetSelectSIPP2490(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSIPP2490(oCustomClass As Parameter.SIPP2490) As Parameter.SIPP2490 Implements ISIPP2490.GetSIPP2490
        Try
            Dim DSIPP2490 As New SQLEngine.SIPP.SIPP2490
            Return DSIPP2490.GetSIPP2490(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2490Edit(oCustomClass As Parameter.SIPP2490) As Parameter.SIPP2490 Implements ISIPP2490.SIPP2490Edit
        Try
            Dim DSIPP2490 As New SQLEngine.SIPP.SIPP2490
            Return DSIPP2490.SIPP2490Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2490Save(oCustomClass As Parameter.SIPP2490) As Parameter.SIPP2490 Implements ISIPP2490.SIPP2490Save
        Try
            Dim DSIPP2490 As New SQLEngine.SIPP.SIPP2490
            Return DSIPP2490.SIPP2490Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2490Add(oCustomClass As Parameter.SIPP2490) As Parameter.SIPP2490 Implements ISIPP2490.SIPP2490Add
        Try
            Dim DSIPP2490 As New SQLEngine.SIPP.SIPP2490
            Return DSIPP2490.SIPP2490Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2490Delete(oCustomClass As Parameter.SIPP2490) As String Implements ISIPP2490.SIPP2490Delete
        Try
            Dim DSIPP2490 As New SQLEngine.SIPP.SIPP2490
            Return DSIPP2490.SIPP2490Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo(oCustomClass As Parameter.SIPP2490) As Parameter.SIPP2490 Implements ISIPP2490.GetCbo
        Try
            Dim DSIPP2490 As New SQLEngine.SIPP.SIPP2490
            Return DSIPP2490.GetCbo(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP2490) As Parameter.SIPP2490 Implements ISIPP2490.GetCboBulandataSIPP
        Try
            Dim DSIPP2490 As New SQLEngine.SIPP.SIPP2490
            Return DSIPP2490.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class

