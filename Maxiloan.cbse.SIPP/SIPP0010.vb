﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ComponentBase
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
Imports Maxiloan.SQLEngine
#End Region


Public Class SIPP0010 : Inherits ComponentBase
    Implements ISIPP0010
    Public Function GetSIPP0010(oCustomClass As Parameter.SIPP0010) As Parameter.SIPP0010 Implements [Interface].ISIPP0010.GetSIPP0010
        Try
            Dim SIPP0010DA As New SQLEngine.SIPP.SIPP0010
            Return SIPP0010DA.GetSIPP0010(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSIPP0010Edit(ocustomClass As Parameter.SIPP0010) As Parameter.SIPP0010 Implements [Interface].ISIPP0010.GetSIPP0010Edit
        Try
            Dim SIPP0010DA As New SQLEngine.SIPP.SIPP0010
            Return SIPP0010DA.GetSIPP0010Edit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0010SaveEdit(ocustomClass As Parameter.SIPP0010) As Parameter.SIPP0010 Implements [Interface].ISIPP0010.SIPP0010SaveEdit
        Try
            Dim SIPP0010DA As New SQLEngine.SIPP.SIPP0010
            Return SIPP0010DA.SIPP0010SaveEdit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0010SaveAdd(ocustomClass As Parameter.SIPP0010) As Parameter.SIPP0010 Implements [Interface].ISIPP0010.SIPP0010SaveAdd
        Try
            Dim SIPP0010DA As New SQLEngine.SIPP.SIPP0010
            Return SIPP0010DA.SIPP0010SaveAdd(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Function GetCbo(ByVal oCustomClass As Parameter.SIPP0010) As Parameter.SIPP0010 Implements [Interface].ISIPP0010.GetCbo
        Try
            Dim SIPP0010DA As New SQLEngine.SIPP.SIPP0010
            Return SIPP0010DA.GetCbo(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SIPP0010Delete(ByVal oCustomClass As Parameter.SIPP0010) As String Implements [Interface].ISIPP0010.SIPP0010Delete
        Try
            Dim SIPP0010DA As New SQLEngine.SIPP.SIPP0010
            Return SIPP0010DA.SIPP0010Delete(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP0010) As Parameter.SIPP0010 Implements ISIPP0010.GetCboBulandataSIPP
        Try
            Dim DSIPP0010 As New SQLEngine.SIPP.SIPP0010
            Return DSIPP0010.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class
