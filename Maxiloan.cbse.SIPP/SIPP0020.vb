﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class SIPP0020 : Inherits ComponentBase
    Implements ISIPP0020

    Public Function GetSelectSIPP0020(oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020 Implements ISIPP0020.GetSelectSIPP0020
        Try
            Dim DSIPP0020 As New SQLEngine.SIPP.SIPP0020
            Return DSIPP0020.GetSelectSIPP0020(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSIPP0020(oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020 Implements ISIPP0020.GetSIPP0020
        Try
            Dim DSIPP0020 As New SQLEngine.SIPP.SIPP0020
            Return DSIPP0020.GetSIPP0020(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0020Edit(oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020 Implements ISIPP0020.SIPP0020Edit
        Try
            Dim DSIPP0020 As New SQLEngine.SIPP.SIPP0020
            Return DSIPP0020.SIPP0020Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0020Save(oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020 Implements ISIPP0020.SIPP0020Save
        Try
            Dim DSIPP0020 As New SQLEngine.SIPP.SIPP0020
            Return DSIPP0020.SIPP0020Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0020Add(oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020 Implements ISIPP0020.SIPP0020Add
        Try
            Dim DSIPP0020 As New SQLEngine.SIPP.SIPP0020
            Return DSIPP0020.SIPP0020Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0020Delete(oCustomClass As Parameter.SIPP0020) As String Implements ISIPP0020.SIPP0020Delete
        Try
            Dim DSIPP0020 As New SQLEngine.SIPP.SIPP0020
            Return DSIPP0020.SIPP0020Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo(oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020 Implements ISIPP0020.GetCbo
        Try
            Dim DSIPP0020 As New SQLEngine.SIPP.SIPP0020
            Return DSIPP0020.GetCbo(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCboBulanDataSIPP(oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020 Implements ISIPP0020.GetCboBUlanDataSIPP
        Try
            Dim DSIPP0020 As New SQLEngine.SIPP.SIPP0020
            Return DSIPP0020.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class
