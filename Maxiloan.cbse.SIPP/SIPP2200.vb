﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ComponentBase
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
Imports Maxiloan.SQLEngine
#End Region


Public Class SIPP2200 : Inherits ComponentBase
    Implements ISIPP2200
    Public Function GetSIPP2200(oCustomClass As Parameter.SIPP2200) As Parameter.SIPP2200 Implements [Interface].ISIPP2200.GetSIPP2200
        Try
            Dim SIPP2200DA As New SQLEngine.SIPP.SIPP2200
            Return SIPP2200DA.GetSIPP2200(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSIPP2200Edit(ocustomClass As Parameter.SIPP2200) As Parameter.SIPP2200 Implements [Interface].ISIPP2200.GetSIPP2200Edit
        Try
            Dim SIPP2200DA As New SQLEngine.SIPP.SIPP2200
            Return SIPP2200DA.GetSIPP2200Edit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP2200) As Parameter.SIPP2200 Implements ISIPP2200.GetCboBulandataSIPP
        Try
            Dim DSIPP2200 As New SQLEngine.SIPP.SIPP2200
            Return DSIPP2200.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function SIPP2200SaveEdit(ocustomClass As Parameter.SIPP2200) As Parameter.SIPP2200 Implements [Interface].ISIPP2200.SIPP2200SaveEdit
        Try
            Dim SIPP2200DA As New SQLEngine.SIPP.SIPP2200
            Return SIPP2200DA.SIPP2200SaveEdit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2200SaveAdd(ocustomClass As Parameter.SIPP2200) As Parameter.SIPP2200 Implements [Interface].ISIPP2200.SIPP2200SaveAdd
        Try
            Dim SIPP2200DA As New SQLEngine.SIPP.SIPP2200
            Return SIPP2200DA.SIPP2200SaveAdd(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Function GetCbo(ByVal oCustomClass As Parameter.SIPP2200) As Parameter.SIPP2200 Implements [Interface].ISIPP2200.GetCbo
        Try
            Dim SIPP2200DA As New SQLEngine.SIPP.SIPP2200
            Return SIPP2200DA.GetCbo(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SIPP2200Delete(ByVal oCustomClass As Parameter.SIPP2200) As String Implements [Interface].ISIPP2200.SIPP2200Delete
        Try
            Dim SIPP2200DA As New SQLEngine.SIPP.SIPP2200
            Return SIPP2200DA.SIPP2200Delete(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
End Class
