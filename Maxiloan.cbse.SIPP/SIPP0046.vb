﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class SIPP0046 : Inherits ComponentBase
    Implements ISIPP0046

    Public Function GetSelectSIPP0046(oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046 Implements ISIPP0046.GetSelectSIPP0046
        Try
            Dim DSIPP0046 As New SQLEngine.SIPP.SIPP0046
            Return DSIPP0046.GetSelectSIPP0046(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSIPP0046(oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046 Implements ISIPP0046.GetSIPP0046
        Try
            Dim DSIPP0046 As New SQLEngine.SIPP.SIPP0046
            Return DSIPP0046.GetSIPP0046(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0046Edit(oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046 Implements ISIPP0046.SIPP0046Edit
        Try
            Dim DSIPP0046 As New SQLEngine.SIPP.SIPP0046
            Return DSIPP0046.SIPP0046Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0046Save(oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046 Implements ISIPP0046.SIPP0046Save
        Try
            Dim DSIPP0046 As New SQLEngine.SIPP.SIPP0046
            Return DSIPP0046.SIPP0046Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0046Add(oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046 Implements ISIPP0046.SIPP0046Add
        Try
            Dim DSIPP0046 As New SQLEngine.SIPP.SIPP0046
            Return DSIPP0046.SIPP0046Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0046Delete(oCustomClass As Parameter.SIPP0046) As String Implements ISIPP0046.SIPP0046Delete
        Try
            Dim DSIPP0046 As New SQLEngine.SIPP.SIPP0046
            Return DSIPP0046.SIPP0046Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo(oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046 Implements ISIPP0046.GetCbo
        Try
            Dim DSIPP0046 As New SQLEngine.SIPP.SIPP0046
            Return DSIPP0046.GetCbo(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046 Implements ISIPP0046.GetCboBulandataSIPP
        Try
            Dim DSIPP0046 As New SQLEngine.SIPP.SIPP0046
            Return DSIPP0046.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class
