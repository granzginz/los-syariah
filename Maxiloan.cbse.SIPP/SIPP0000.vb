﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class SIPP0000 : Inherits ComponentBase
    Implements ISIPP0000

    Public Function GetSelectSIPP0000(oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000 Implements ISIPP0000.GetSelectSIPP0000
        Try
            Dim DSIPP0000 As New SQLEngine.SIPP.SIPP0000
            Return DSIPP0000.GetSelectSIPP0000(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSIPP0000(oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000 Implements ISIPP0000.GetSIPP0000
        Try
            Dim DSIPP0000 As New SQLEngine.SIPP.SIPP0000
            Return DSIPP0000.GetSIPP0000(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0000Edit(oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000 Implements ISIPP0000.SIPP0000Edit
        Try
            Dim DSIPP0000 As New SQLEngine.SIPP.SIPP0000
            Return DSIPP0000.SIPP0000Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0000Save(oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000 Implements ISIPP0000.SIPP0000Save
        Try
            Dim DSIPP0000 As New SQLEngine.SIPP.SIPP0000
            Return DSIPP0000.SIPP0000Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0000Add(oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000 Implements ISIPP0000.SIPP0000Add
        Try
            Dim DSIPP0000 As New SQLEngine.SIPP.SIPP0000
            Return DSIPP0000.SIPP0000Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0000Delete(oCustomClass As Parameter.SIPP0000) As String Implements ISIPP0000.SIPP0000Delete
        Try
            Dim DSIPP0000 As New SQLEngine.SIPP.SIPP0000
            Return DSIPP0000.SIPP0000Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo(oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000 Implements ISIPP0000.GetCbo
        Try
            Dim DSIPP0000 As New SQLEngine.SIPP.SIPP0000
            Return DSIPP0000.GetCbo(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000 Implements ISIPP0000.GetCboBulandataSIPP
        Try
            Dim DSIPP0000 As New SQLEngine.SIPP.SIPP0000
            Return DSIPP0000.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class
