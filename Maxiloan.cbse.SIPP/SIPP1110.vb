﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class SIPP1110 : Inherits ComponentBase
    Implements ISIPP1110

    Public Function GetSelectSIPP1110(oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110 Implements ISIPP1110.GetSelectSIPP1110
        Try
            Dim DSIPP1110 As New SQLEngine.SIPP.SIPP1110
            Return DSIPP1110.GetSelectSIPP1110(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSIPP1110(oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110 Implements ISIPP1110.GetSIPP1110
        Try
            Dim DSIPP1110 As New SQLEngine.SIPP.SIPP1110
            Return DSIPP1110.GetSIPP1110(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP1110Edit(oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110 Implements ISIPP1110.SIPP1110Edit
        Try
            Dim DSIPP1110 As New SQLEngine.SIPP.SIPP1110
            Return DSIPP1110.SIPP1110Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP1110Save(oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110 Implements ISIPP1110.SIPP1110Save
        Try
            Dim DSIPP1110 As New SQLEngine.SIPP.SIPP1110
            Return DSIPP1110.SIPP1110Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP1110Add(oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110 Implements ISIPP1110.SIPP1110Add
        Try
            Dim DSIPP1110 As New SQLEngine.SIPP.SIPP1110
            Return DSIPP1110.SIPP1110Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP1110Delete(oCustomClass As Parameter.SIPP1110) As String Implements ISIPP1110.SIPP1110Delete
        Try
            Dim DSIPP1110 As New SQLEngine.SIPP.SIPP1110
            Return DSIPP1110.SIPP1110Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo(oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110 Implements ISIPP1110.GetCbo
        Try
            Dim DSIPP1110 As New SQLEngine.SIPP.SIPP1110
            Return DSIPP1110.GetCbo(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110 Implements ISIPP1110.GetCboBulandataSIPP
        Try
            Dim DSIPP1110 As New SQLEngine.SIPP.SIPP1110
            Return DSIPP1110.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class

