﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ComponentBase
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
Imports Maxiloan.SQLEngine
#End Region


Public Class SIPP1100 : Inherits ComponentBase
    Implements ISIPP1100
    Public Function GetSIPP1100List(oCustomClass As Parameter.SIPP1100) As Parameter.SIPP1100 Implements [Interface].ISIPP1100.GetSIPP1100List
        Try
            Dim SIPP1100DA As New SQLEngine.SIPP.SIPP1100
            Return SIPP1100DA.GetSIPP1100List(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP1100) As Parameter.SIPP1100 Implements ISIPP1100.GetCboBulandataSIPP
        Try
            Dim DSIPP1100 As New SQLEngine.SIPP.SIPP1100
            Return DSIPP1100.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetSIPP1100Edit(ocustomClass As Parameter.SIPP1100) As Parameter.SIPP1100 Implements [Interface].ISIPP1100.GetSIPP1100Edit
        Try
            Dim SIPP1100DA As New SQLEngine.SIPP.SIPP1100
            Return SIPP1100DA.SIPP1100Edit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP1100SaveEdit(ocustomClass As Parameter.SIPP1100) As Parameter.SIPP1100 Implements [Interface].ISIPP1100.SIPP1100SaveEdit
        Try
            Dim SIPP1100DA As New SQLEngine.SIPP.SIPP1100
            Return SIPP1100DA.SIPP1100SaveEdit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP1100SaveAdd(ocustomClass As Parameter.SIPP1100) As Parameter.SIPP1100 Implements [Interface].ISIPP1100.SIPP1100SaveAdd
        Try
            Dim SIPP1100DA As New SQLEngine.SIPP.SIPP1100
            Return SIPP1100DA.SIPP1100SaveAdd(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Function GetCbo(ByVal oCustomClass As Parameter.SIPP1100) As Parameter.SIPP1100 Implements [Interface].ISIPP1100.GetCbo
        Try
            Dim SIPP1100DA As New SQLEngine.SIPP.SIPP1100
            Return SIPP1100DA.GetCbo(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SIPP1100Delete(ByVal oCustomClass As Parameter.SIPP1100) As String Implements [Interface].ISIPP1100.SIPP1100Delete
        Try
            Dim SIPP1100DA As New SQLEngine.SIPP.SIPP1100
            Return SIPP1100DA.SIPP1100Delete(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
End Class
