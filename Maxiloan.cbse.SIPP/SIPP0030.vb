﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ComponentBase
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
Imports Maxiloan.SQLEngine
#End Region


Public Class SIPP0030 : Inherits ComponentBase
    Implements ISIPP0030
    Public Function GetSIPP0030(oCustomClass As Parameter.SIPP0030) As Parameter.SIPP0030 Implements [Interface].ISIPP0030.GetSIPP0030
        Try
            Dim SIPP0030DA As New SQLEngine.SIPP.SIPP0030
            Return SIPP0030DA.GetSIPP0030(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSIPP0030Edit(ocustomClass As Parameter.SIPP0030) As Parameter.SIPP0030 Implements [Interface].ISIPP0030.GetSIPP0030Edit
        Try
            Dim SIPP0030DA As New SQLEngine.SIPP.SIPP0030
            Return SIPP0030DA.GetSIPP0030Edit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0030SaveEdit(ocustomClass As Parameter.SIPP0030) As Parameter.SIPP0030 Implements [Interface].ISIPP0030.SIPP0030SaveEdit
        Try
            Dim SIPP0030DA As New SQLEngine.SIPP.SIPP0030
            Return SIPP0030DA.SIPP0030SaveEdit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0030SaveAdd(ocustomClass As Parameter.SIPP0030) As Parameter.SIPP0030 Implements [Interface].ISIPP0030.SIPP0030SaveAdd
        Try
            Dim SIPP0030DA As New SQLEngine.SIPP.SIPP0030
            Return SIPP0030DA.SIPP0030SaveAdd(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Function GetCbo(ByVal oCustomClass As Parameter.SIPP0030) As Parameter.SIPP0030 Implements [Interface].ISIPP0030.GetCbo
        Try
            Dim SIPP0030DA As New SQLEngine.SIPP.SIPP0030
            Return SIPP0030DA.GetCbo(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SIPP0030Delete(ByVal oCustomClass As Parameter.SIPP0030) As String Implements [Interface].ISIPP0030.SIPP0030Delete
        Try
            Dim SIPP0030DA As New SQLEngine.SIPP.SIPP0030
            Return SIPP0030DA.SIPP0030Delete(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP0030) As Parameter.SIPP0030 Implements ISIPP0030.GetCboBulandataSIPP
        Try
            Dim DSIPP0030 As New SQLEngine.SIPP.SIPP0030
            Return DSIPP0030.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class
