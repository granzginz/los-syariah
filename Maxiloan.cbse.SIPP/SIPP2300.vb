﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ComponentBase
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
Imports Maxiloan.SQLEngine
#End Region


Public Class SIPP2300 : Inherits ComponentBase
    Implements ISIPP2300
    Public Function GetSIPP2300(oCustomClass As Parameter.SIPP2300) As Parameter.SIPP2300 Implements [Interface].ISIPP2300.GetSIPP2300
        Try
            Dim SIPP2300DA As New SQLEngine.SIPP.SIPP2300
            Return SIPP2300DA.GetSIPP2300(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSIPP2300Edit(ocustomClass As Parameter.SIPP2300) As Parameter.SIPP2300 Implements [Interface].ISIPP2300.GetSIPP2300Edit
        Try
            Dim SIPP2300DA As New SQLEngine.SIPP.SIPP2300
            Return SIPP2300DA.GetSIPP2300Edit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2300SaveEdit(ocustomClass As Parameter.SIPP2300) As Parameter.SIPP2300 Implements [Interface].ISIPP2300.SIPP2300SaveEdit
        Try
            Dim SIPP2300DA As New SQLEngine.SIPP.SIPP2300
            Return SIPP2300DA.SIPP2300SaveEdit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2300SaveAdd(ocustomClass As Parameter.SIPP2300) As Parameter.SIPP2300 Implements [Interface].ISIPP2300.SIPP2300SaveAdd
        Try
            Dim SIPP2300DA As New SQLEngine.SIPP.SIPP2300
            Return SIPP2300DA.SIPP2300SaveAdd(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Function GetCbo(ByVal oCustomClass As Parameter.SIPP2300) As Parameter.SIPP2300 Implements [Interface].ISIPP2300.GetCbo
        Try
            Dim SIPP2300DA As New SQLEngine.SIPP.SIPP2300
            Return SIPP2300DA.GetCbo(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SIPP2300Delete(ByVal oCustomClass As Parameter.SIPP2300) As String Implements [Interface].ISIPP2300.SIPP2300Delete
        Try
            Dim SIPP2300DA As New SQLEngine.SIPP.SIPP2300
            Return SIPP2300DA.SIPP2300Delete(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.SIPP2300) As Parameter.SIPP2300 Implements ISIPP2300.GetCboBulandataSIPP
        Try
            Dim DSIPP2300 As New SQLEngine.SIPP.SIPP2300
            Return DSIPP2300.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class
