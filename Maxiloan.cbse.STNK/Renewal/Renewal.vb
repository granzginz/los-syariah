

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class Renewal
    Implements ISTNKRenewal

    Public Function GetSTNKRequest(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal Implements [Interface].ISTNKRenewal.GetSTNKRequest
        Dim DA As New SQLEngine.STNK.Renewal
        Return DA.GetSTNKRequest(oCustomClass)
    End Function

    Public Function SaveSTNKRequest(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal Implements [Interface].ISTNKRenewal.SaveSTNKRequest
        Dim DA As New SQLEngine.STNK.Renewal
        Return DA.SaveSTNKRequest(oCustomClass)
    End Function

    Public Function ReportSTNKRenewal(ByVal oCustomClass As Parameter.STNKRenewal) As System.Data.DataSet Implements [Interface].ISTNKRenewal.ReportSTNKRenewal
        Dim DA As New SQLEngine.STNK.Renewal
        Return DA.ReportSTNKRenewal(oCustomClass)
    End Function
    Public Function SaveSTNKRegister(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal Implements [Interface].ISTNKRenewal.SaveSTNKRegister
        Dim DA As New SQLEngine.STNK.Renewal
        Return DA.SaveSTNKRegister(oCustomClass)
    End Function
    Public Function CancelSTNKRegister(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal Implements [Interface].ISTNKRenewal.CancelSTNKRegister
        Dim DA As New SQLEngine.STNK.Renewal
        Return DA.CancelSTNKRegister(oCustomClass)
    End Function
    Public Function STNKView(ByVal strConnection As String, ByVal BranchId As String) As DataTable Implements [Interface].ISTNKRenewal.STNKView
        Dim DAAgency As New SQLEngine.STNK.Renewal
        Return DAAgency.STNKView(strConnection, BranchId)
    End Function
    Public Function SaveSTNKInvoicing(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal Implements [Interface].ISTNKRenewal.SaveSTNKInvoicing
        Dim DA As New SQLEngine.STNK.Renewal
        Return DA.SaveSTNKInvoicing(oCustomClass)
    End Function
    Public Function STNKInquiryReport(ByVal customclass As Parameter.STNKRenewal) As Parameter.STNKRenewal Implements [Interface].ISTNKRenewal.STNKInquiryReport
        Dim DA As New SQLEngine.STNK.Renewal
        Return DA.STNKInquiryReport(customclass)
    End Function
    'Public Function STNKRenewalUpdatePrint1(ByVal customclass As Parameter.STNKRenewal) As Boolean Implements [Interface].ISTNKRenewal.STNKRenewalUpdatePrint
    Public Function STNKRenewalUpdatePrint(ByVal customclass As Parameter.STNKRenewal) As Boolean Implements [Interface].ISTNKRenewal.STNKRenewalUpdatePrint
        Dim DA As New SQLEngine.STNK.Renewal
        Return DA.STNKRenewalUpdatePrint(customclass)
    End Function

    Public Function GetLastDate(ByVal strConnection As String) As String Implements [Interface].ISTNKRenewal.GetLastDate
        Dim DA As New SQLEngine.STNK.Renewal
        Return DA.GetLastDate(strConnection)
    End Function

    Public Function GetReportSTNKRenewalIncome(ByVal strConnection As String, ByVal BranchID As String, ByVal BusinessDate As String) As System.Data.DataSet Implements [Interface].ISTNKRenewal.GetReportSTNKRenewalIncome
        Dim DA As New SQLEngine.STNK.Renewal
        Return DA.GetReportSTNKRenewalIncome(strConnection, BranchID, BusinessDate)
    End Function
    Public Function STNKRequestInquiryPaging(ByVal customclass As Parameter.STNKRenewal) As Parameter.STNKRenewal Implements [Interface].ISTNKRenewal.STNKRequestInquiryPaging
        Dim DA As New SQLEngine.STNK.Renewal
        Return DA.STNKRequestInquiryPaging(customclass)
    End Function


    Public Function SaveEstimasiBiayaSTNK(oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal Implements [Interface].ISTNKRenewal.SaveEstimasiBiayaSTNK
        Dim DA As New SQLEngine.STNK.Renewal
        Return DA.SaveEstimasiBiayaSTNK(oCustomClass)
    End Function
End Class
