

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class Branch : Inherits ComponentBase
    Implements IBranch



    Public Sub BranchAdd(ByVal obranch As Parameter.Branch, ByVal oAddress As Parameter.Address, ByVal operson As Parameter.Personal) Implements [Interface].IBranch.BranchAdd
        Try
            Dim DA As New SQLEngine.Setup.Branch
            DA.BranchAdd(obranch, oAddress, operson)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub BranchDelete(ByVal obranch As Parameter.Branch) Implements [Interface].IBranch.BranchDelete
        Try
            Dim DA As New SQLEngine.Setup.Branch
            DA.BranchDelete(obranch)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub BranchEdit(ByVal obranch As Parameter.Branch, ByVal oAddress As Parameter.Address, ByVal operson As Parameter.Personal) Implements [Interface].IBranch.BranchEdit
        Try
            Dim DA As New SQLEngine.Setup.Branch
            DA.BranchEdit(obranch, oAddress, operson)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function BranchList(ByVal oBranch As Parameter.Branch) As Parameter.Branch Implements [Interface].IBranch.BranchList
        Dim DA As New SQLEngine.Setup.Branch
        Return DA.ListBranch(oBranch)
    End Function

    Public Function BranchReport(ByVal oBranch As Parameter.Branch) As Parameter.Branch Implements [Interface].IBranch.BranchReport
        Dim DA As New SQLEngine.Setup.Branch
        Return DA.Report(oBranch)
    End Function

    Public Function GetCombo(ByVal strcon As String, ByVal strCombo As String) As System.Data.DataTable Implements [Interface].IBranch.GetCombo
        Dim DA As New SQLEngine.Setup.Branch
        Return DA.GetCombo(strcon, strCombo)
    End Function

    Public Function BranchByID(ByVal oBranch As Parameter.Branch) As Parameter.Branch Implements [Interface].IBranch.BranchByID
        Dim DA As New SQLEngine.Setup.Branch
        Return DA.ListBranchByID(oBranch)
    End Function

    Public Function BranchBudgetListing(ByVal customclass As Parameter.Branch) As Parameter.Branch Implements [Interface].IBranch.BranchBudgetListing
        Dim DA As New SQLEngine.Setup.Branch
        Return DA.BranchBudgetListing(customclass)
    End Function

    Public Function BranchBudgetPrint(ByVal customclass As Parameter.Branch) As Parameter.Branch Implements [Interface].IBranch.BranchBudgetPrint
        Dim DA As New SQLEngine.Setup.Branch
        Return DA.BranchBudgetPrint(customclass)
    End Function

    Public Function BranchForecastListing(ByVal customclass As Parameter.Branch) As Parameter.Branch Implements [Interface].IBranch.BranchForecastListing
        Dim DA As New SQLEngine.Setup.Branch
        Return DA.BranchForecastListing(customclass)
    End Function

    Public Function BranchForecastPrint(ByVal customclass As Parameter.Branch) As Parameter.Branch Implements [Interface].IBranch.BranchForecastPrint
        Dim DA As New SQLEngine.Setup.Branch
        Return DA.BranchForecastPrint(customclass)
    End Function
End Class
