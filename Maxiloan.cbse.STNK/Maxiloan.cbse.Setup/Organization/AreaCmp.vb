

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class AreaCmp : Inherits ComponentBase
    Implements IArea

    Public Function ListArea(ByVal customclass As Parameter.Area) As Parameter.Area Implements [Interface].IArea.ListArea
        Dim DAListArea As New SQLEngine.Setup.AreaDtAcc
        Return DAListArea.ListArea(customclass)
    End Function

    Public Function SaveArea(ByVal customclass As Parameter.Area) As String Implements [Interface].IArea.SaveArea
        Dim DASaveArea As New SQLEngine.Setup.AreaDtAcc
        Return DASaveArea.SaveArea(customclass)
    End Function

    Public Function DeleteArea(ByVal customclass As Parameter.Area) As String Implements [Interface].IArea.DeleteArea
        Dim DADeleteArea As New SQLEngine.Setup.AreaDtAcc
        Return DADeleteArea.DeleteArea(customclass)
    End Function

    Public Function UpdateArea(ByVal customclass As Parameter.Area) As String Implements [Interface].IArea.UpdateArea
        Dim DAUpdateArea As New SQLEngine.Setup.AreaDtAcc
        Return DAUpdateArea.UpdateArea(customclass)
    End Function

    Public Function ReportArea(ByVal customclass As Parameter.Area) As Parameter.Area Implements [Interface].IArea.ReportArea
        Dim DAReportArea As New SQLEngine.Setup.AreaDtAcc
        Return DAReportArea.ReportArea(customclass)
    End Function

    Public Function AreaInfo(ByVal customclass As Parameter.Area) As Parameter.Area Implements [Interface].IArea.AreaInfo
        Dim DAAreaInfo As New SQLEngine.Setup.AreaDtAcc
        Return DAAreaInfo.AreaInfo(customclass)
    End Function
End Class
