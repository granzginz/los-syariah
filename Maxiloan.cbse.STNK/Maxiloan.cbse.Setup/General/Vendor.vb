
#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class Vendor : Inherits ComponentBase
    Implements IVendor
    Dim oSQLE As New SQLEngine.Setup.Vendor

    Public Function VendorDelete(ByVal oCustomClass As Parameter.Vendor) As String Implements [Interface].IVendor.VendorDelete
        Return oSQLE.VendorDelete(oCustomClass)
    End Function

    Public Function VendorSaveAdd(ByVal oCustomClass As Parameter.Vendor) As String Implements [Interface].IVendor.VendorSaveAdd
        Return oSQLE.VendorSaveAdd(oCustomClass)
    End Function

    Public Sub VendorSaveEdit(ByVal oCustomClass As Parameter.Vendor) Implements [Interface].IVendor.VendorSaveEdit
        oSQLE.VendorSaveEdit(oCustomClass)
    End Sub

    Public Function GetVendor(ByVal oCustomClass As Parameter.Vendor) As Parameter.Vendor Implements [Interface].IVendor.GetVendor
        Return oSQLE.GetVendor(oCustomClass)
    End Function

    Public Function GetVendorList(ByVal oCustomClass As Parameter.Vendor) As Parameter.Vendor Implements [Interface].IVendor.GetVendorList
        Return oSQLE.GetVendorList(oCustomClass)
    End Function
End Class