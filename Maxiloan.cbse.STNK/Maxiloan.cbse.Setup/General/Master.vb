
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class Master : Inherits ComponentBase
    Implements IMaster

    Public Function GetMaster(ByVal customClass As Parameter.Master) As Parameter.Master Implements [Interface].IMaster.GetMaster
        Try
            Dim MasterDA As New SQLEngine.Setup.Master
            Return MasterDA.GetMaster(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetMasterReport(ByVal customClass As Parameter.Master) As Parameter.Master Implements [Interface].IMaster.GetMasterReport
        Try
            Dim MasterDA As New SQLEngine.Setup.Master
            Return MasterDA.GetMasterReport(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetKeyWord(ByVal customClass As Parameter.Master) As Parameter.Master Implements [Interface].IMaster.GetKeyWord
        Try
            Dim MasterDA As New SQLEngine.Setup.Master
            Return MasterDA.GetKeyWord(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function MasterSaveAdd(ByVal customClass As Parameter.Master) As String Implements IMaster.MasterSaveAdd
        Try
            Dim MasterDA As New SQLEngine.Setup.Master
            Return MasterDA.MasterSaveAdd(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub MasterSaveEdit(ByVal customClass As Parameter.Master) Implements IMaster.MasterSaveEdit
        Try
            Dim MasterDA As New SQLEngine.Setup.Master
            MasterDA.MasterSaveEdit(customClass)
        Catch ex As Exception
            Throw New Exception("Error when saving Update Master", ex)
        End Try
    End Sub

    Public Function MasterDelete(ByVal customClass As Parameter.Master) As String Implements IMaster.MasterDelete
        'Try
        Dim MasterDA As New SQLEngine.Setup.Master
        Return MasterDA.MasterDelete(customClass)
        'Catch ex As Exception
        '    Throw New Exception("Error when Delete Master", ex)
        'End Try
    End Function
End Class