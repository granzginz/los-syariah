﻿
#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class Negara : Inherits ComponentBase
    Implements INegara

    Dim oSQLE As New SQLEngine.Setup.Negara

    Public Function GetNegara(ByVal oCustomClass As Parameter.Negara) As Parameter.Negara Implements [Interface].INegara.GetNegara
        Return oSQLE.GetNegara(oCustomClass)
    End Function

    Public Function GetNegaraList(ByVal oCustomClass As Parameter.Negara) As Parameter.Negara Implements [Interface].INegara.GetNegaraList
        Return oSQLE.GetNegaraList(oCustomClass)
    End Function

    Public Function GetNegaraCombo(ByVal customclass As Parameter.Negara) As System.Data.DataTable Implements [Interface].INegara.GetNegaraCombo
        Return oSQLE.GetNegaraCombo(customclass)
    End Function
End Class
