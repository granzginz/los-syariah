
#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class DatiII : Inherits ComponentBase
    Implements IDatiII
    Dim oSQLE As New SQLEngine.Setup.DatiII

    Public Function DatiDelete(ByVal oCustomClass As Parameter.Dati) As String Implements [Interface].IDatiII.DatiDelete
        Return oSQLE.DatiDelete(oCustomClass)
    End Function

    Public Function DatiSaveAdd(ByVal oCustomClass As Parameter.Dati) As String Implements [Interface].IDatiII.DatiSaveAdd
        Return oSQLE.DatiSaveAdd(oCustomClass)
    End Function

    Public Sub DatiSaveEdit(ByVal oCustomClass As Parameter.Dati) Implements [Interface].IDatiII.DatiSaveEdit
        oSQLE.DatiSaveEdit(oCustomClass)
    End Sub

    Public Function GetDati(ByVal oCustomClass As Parameter.Dati) As Parameter.Dati Implements [Interface].IDatiII.GetDati
        Return oSQLE.GetDati(oCustomClass)
    End Function

    Public Function GetDatiList(ByVal oCustomClass As Parameter.Dati) As Parameter.Dati Implements [Interface].IDatiII.GetDatiList
        Return oSQLE.GetDatiList(oCustomClass)
    End Function

    Public Function GetDatiReport(ByVal oCustomClass As Parameter.Dati) As Parameter.Dati Implements [Interface].IDatiII.GetDatiReport
        Return oSQLE.GetDatiReport(oCustomClass)
    End Function
End Class