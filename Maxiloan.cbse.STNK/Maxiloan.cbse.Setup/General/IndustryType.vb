
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class IndustryType : Inherits ComponentBase
    Implements IIndustryType

    Public Function GetIndustryType(ByVal oCustomClass As Parameter.IndustryType) As Parameter.IndustryType Implements [Interface].IIndustryType.GetIndustryType
        Try
            Dim IndustryTypeDA As New SQLEngine.Setup.IndustryType
            Return IndustryTypeDA.GetIndustryType(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetIndustryTypeReport(ByVal oCustomClass As Parameter.IndustryType) As Parameter.IndustryType Implements [Interface].IIndustryType.GetIndustryTypeReport
        Try
            Dim IndustryTypeDA As New SQLEngine.Setup.IndustryType
            Return IndustryTypeDA.GetIndustryTypeReport(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetSector(ByVal oCustomClass As Parameter.IndustryType) As Parameter.IndustryType Implements [Interface].IIndustryType.GetSector
        Try
            Dim IndustryTypeDA As New SQLEngine.Setup.IndustryType
            Return IndustryTypeDA.GetSector(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetIndustryTypeEdit(ByVal oCustomClass As Parameter.IndustryType) As Parameter.IndustryType Implements [Interface].IIndustryType.GetIndustryTypeEdit
        Try
            Dim IndustryTypeDA As New SQLEngine.Setup.IndustryType
            Return IndustryTypeDA.GetIndustryTypeEdit(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function IndustryTypeSaveAdd(ByVal oCustomClass As Parameter.IndustryType) As String Implements IIndustryType.IndustryTypeSaveAdd
        'Trace.WriteIf(MethodTrace.Enabled, "Enter")
        Try
            Dim IndustryTypeDA As New SQLEngine.Setup.IndustryType
            Return IndustryTypeDA.IndustryTypeSaveAdd(oCustomClass)
            'Throw busEx
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

    Public Sub IndustryTypeSaveEdit(ByVal oCustomClass As Parameter.IndustryType) Implements IIndustryType.IndustryTypeSaveEdit
        Try
            Dim IndustryTypeDA As New SQLEngine.Setup.IndustryType
            IndustryTypeDA.IndustryTypeSaveEdit(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when saving Update IndustryType", ex)
        End Try
    End Sub

    Public Function IndustryTypeDelete(ByVal oCustomClass As Parameter.IndustryType) As String Implements IIndustryType.IndustryTypeDelete
        Try
            Dim IndustryTypeDA As New SQLEngine.Setup.IndustryType
            Return IndustryTypeDA.IndustryTypeDelete(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when Delete IndustryType", ex)
        End Try
    End Function
End Class