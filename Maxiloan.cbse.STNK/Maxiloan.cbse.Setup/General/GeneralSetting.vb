

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region


Public Class GeneralSetting : Inherits ComponentBase
    Implements IGeneralSetting


    Public Function GetGeneralSetting(ByVal customClass As Parameter.GeneralSetting) As Parameter.GeneralSetting Implements IGeneralSetting.GetGeneralSetting
        Dim GetGeneralSettingDA As New SQLEngine.Setup.GeneralSetting
        Return GetGeneralSettingDA.GetGeneralSetting(customClass)
    End Function

    Public Function GeneralSettingSaveEdit(ByVal customClass As Parameter.GeneralSetting) As Boolean Implements IGeneralSetting.GeneralSettingSaveEdit
        Try
            Dim GeneralSettingSaveEditDA As New SQLEngine.Setup.GeneralSetting
            Return GeneralSettingSaveEditDA.GeneralSettingSaveEdit(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetGeneralSettingReport(ByVal customClass As Parameter.GeneralSetting) As Parameter.GeneralSetting Implements IGeneralSetting.GetGeneralSettingReport
        Try
            Dim DA As New SQLEngine.Setup.GeneralSetting
            Return DA.GetGeneralSettingReport(customClass)
        Catch ex As Exception
            '
        End Try
    End Function


    Public Function GetGeneralSettingByID(ByVal oCustomClass As Parameter.GeneralSetting) As Parameter.GeneralSetting Implements [Interface].IGeneralSetting.GetGeneralSettingByID
        Dim GetGeneralSettingDA As New SQLEngine.Setup.GeneralSetting
        Try
            Return GetGeneralSettingDA.GetGeneralSettingByID(oCustomClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetGeneralSettingValue(oClass As Parameter.GeneralSetting) As String Implements [Interface].IGeneralSetting.GetGeneralSettingValue
        Dim oSQLE As New SQLEngine.Setup.GeneralSetting

        Try
            Return oSQLE.getGeneralSettingValue(oClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
