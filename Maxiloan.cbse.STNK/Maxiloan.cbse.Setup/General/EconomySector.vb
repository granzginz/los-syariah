
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class EconomySector : Inherits ComponentBase
    Implements IEconomySector

    Public Function GetEconomySector(ByVal customClass As Parameter.EconomySector) As Parameter.EconomySector Implements [Interface].IEconomySector.GetEconomySector
        Try
            Dim EconomySectorDA As New SQLEngine.Setup.EconomySector
            Return EconomySectorDA.GetEconomySector(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetEconomySectorReport(ByVal customClass As Parameter.EconomySector) As Parameter.EconomySector Implements [Interface].IEconomySector.GetEconomySectorReport
        Try
            Dim EconomySectorDA As New SQLEngine.Setup.EconomySector
            Return EconomySectorDA.GetEconomySectorReport(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function EconomySectorSaveAdd(ByVal customClass As Parameter.EconomySector) As String Implements IEconomySector.EconomySectorSaveAdd
        'Trace.WriteIf(MethodTrace.Enabled, "Enter")
        Try
            Dim EconomySectorDA As New SQLEngine.Setup.EconomySector
            Return EconomySectorDA.EconomySectorSaveAdd(customClass)
            'Throw busEx
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

    Public Sub EconomySectorSaveEdit(ByVal customClass As Parameter.EconomySector) Implements IEconomySector.EconomySectorSaveEdit
        Try
            Dim EconomySectorDA As New SQLEngine.Setup.EconomySector
            EconomySectorDA.EconomySectorSaveEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when saving Update EconomySector", ex)
        End Try
    End Sub

    Public Function EconomySectorDelete(ByVal customClass As Parameter.EconomySector) As String Implements IEconomySector.EconomySectorDelete
        Try
            Dim EconomySectorDA As New SQLEngine.Setup.EconomySector
            Return EconomySectorDA.EconomySectorDelete(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when Delete EconomySector", ex)
        End Try
    End Function
End Class