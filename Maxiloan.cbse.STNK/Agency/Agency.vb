

#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class Agency
    Implements IAgency


    Public Sub STNKAgencyDelete(ByVal oAgency As Parameter.Agency) Implements [Interface].IAgency.STNKAgencyDelete
        Dim DAAgency As New SQLEngine.STNK.Agency
        DAAgency.STNKAgencyDelete(oAgency)
    End Sub

    Public Function STNKAgencyPaging(ByVal customclass As Parameter.Agency) As Parameter.Agency Implements [Interface].IAgency.STNKAgencyPaging
        Dim DAAgency As New SQLEngine.STNK.Agency
        Return DAAgency.STNKAgencyPaging(customclass)

    End Function

    Public Sub STNKAgencySave(ByVal oAgency As Parameter.Agency, ByVal oBankAccount As Parameter.BankAccount, ByVal oAddress As Parameter.Address, ByVal oPersonal As Parameter.Personal) Implements [Interface].IAgency.STNKAgencySave
        Dim DAAgency As New SQLEngine.STNK.Agency
        DAAgency.STNKAgencySave(oAgency, oBankAccount, oAddress, oPersonal)
    End Sub

    Public Sub STNKAgencyUpdate(ByVal oAgency As Parameter.Agency, ByVal oBankAccount As Parameter.BankAccount, ByVal oAddress As Parameter.Address, ByVal oPersonal As Parameter.Personal) Implements [Interface].IAgency.STNKAgencyUpdate
        Dim DAAgency As New SQLEngine.STNK.Agency
        DAAgency.STNKAgencyUpdate(oAgency, oBankAccount, oAddress, oPersonal)

    End Sub

    Public Function STNKAgencyView(ByVal strConnection As String, ByVal BranchId As String, ByVal AgentId As String) As System.Data.DataTable Implements [Interface].IAgency.STNKAgencyView
        Dim DAAgency As New SQLEngine.STNK.Agency
        Return DAAgency.STNKAgencyView(strConnection, BranchId, AgentId)
    End Function

    Public Function STNKAgencyReport(ByVal oCustomClass As Parameter.Agency) As Parameter.Agency Implements [Interface].IAgency.STNKAgencyReport
        Dim DAAgency As New SQLEngine.STNK.Agency
        Return DAAgency.STNKAgencyReport(oCustomClass)

    End Function
End Class
