
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class Floating : Inherits AccMntBase
    Private Const SPFloatingData As String = "spFloatingData"
    Private Const SPLASTADJUSTMENT As String = "spLastAdjustment"

#Region "Floating Data "
    Public Function ReschedulingProduct(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim objread As SqlDataReader
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID.Trim

            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, SPFloatingData, params)
            With oCustomClass
                If objread.Read Then
                    .BranchId = objread("BranchID").ToString()
                    .ApplicationID = objread("ApplicationID").ToString()
                    .FloatingPeriod = objread("FloatingPeriod").ToString
                    .txtSearch = objread("FloatingPeriodDesc").ToString
                    .FloatingNextReviewDate = CDate(objread("FloatingNextReviewDate"))
                    .EffectiveRate = CDbl(objread("EffectiveRate"))
                    .FlatRate = CDbl(objread("FlatRate"))
                    .PaymentFrequency = objread("PaymentFrequency").ToString
                    .NumOfInstallment = CInt(objread("NumOfInstallment"))
                    .Tenor = CInt(objread("Tenor"))
                    .SeqNo = CInt(objread("MinFloatingSeqNo"))
                    .EffRate = CInt(objread("EffRate"))
                    .EffectiveRateBehaviour = objread("EffectiveRateBehaviour").ToString
                    .InstallmentScheme = objread("InstallmentScheme").ToString
                End If
            End With

            Return oCustomClass
        Catch exp As Exception
            WriteException("Floating", "ReschedulingProduct", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccMnt.Floating.ReschedulingProduct")
        End Try
    End Function

    Public Function LastAdjustment(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oCustomClass.BranchId

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID

            oCustomClass.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, SPLASTADJUSTMENT, params).Tables(0)
            Return oCustomClass
        Catch exp As Exception
            WriteException("Floating", "LastAdjustment", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetFloatingData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim objread As SqlDataReader
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID.Trim
            params(2) = New SqlParameter("@InsSeqNo", SqlDbType.BigInt)
            params(2).Value = oCustomClass.SeqNo
            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetFloatingPrincipal", params)
            With oCustomClass
                If objread.Read Then
                    .BranchId = objread("BranchID").ToString()
                    .ApplicationID = objread("ApplicationID").ToString()
                    .OutstandingPrincipalOld = CDbl(objread("OutstandingPrincipal"))
                    .NewNumInst = CInt(objread("MinSeqNo"))
                    .EffectiveDate = CDate(objread("DueDate"))
                End If
            End With

            Return oCustomClass
        Catch ex As Exception

        End Try

    End Function
#End Region

#Region "Floating Review "
    Public Function SaveFloatingData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim transaction As SqlTransaction
        Dim intLoop As Integer
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(8) {}
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID
            params(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(2).Value = oCustomClass.BusinessDate
            params(3) = New SqlParameter("@EffectiveRate", SqlDbType.BigInt)
            params(3).Value = oCustomClass.EffectiveRate
            params(4) = New SqlParameter("@FlatRate", SqlDbType.Decimal)
            params(4).Value = oCustomClass.FlatRate
            params(5) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
            params(5).Value = oCustomClass.InstallmentAmount
            params(6) = New SqlParameter("@FloatingPeriod", SqlDbType.VarChar, 2)
            params(6).Value = oCustomClass.FloatingPeriod
            params(7) = New SqlParameter("@InsSeqNo", SqlDbType.BigInt)
            params(7).Value = oCustomClass.SeqNo
            params(8) = New SqlParameter("@FloatingSeqNo ", SqlDbType.Int)
            params(8).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFloatingSaveReview", params)

            Dim params2() As SqlParameter = New SqlParameter(4) {}
            params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params2(0).Value = oCustomClass.BranchId
            params2(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params2(1).Value = oCustomClass.ApplicationID
            params2(2) = New SqlParameter("@FloatingSeqNo", SqlDbType.Int)
            params2(2).Value = params(8).Value.ToString '.Value = oCustomClass.SeqNo
            params2(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params2(3).Value = oCustomClass.BusinessDate
            params2(4) = New SqlParameter("@InsSeqNo", SqlDbType.BigInt)
            params2(4).Value = oCustomClass.SeqNo

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFloatingSaveReview3", params2)

            If oCustomClass.MydataSet.Tables("AmortTable").Rows.Count > 0 Then
                Dim params1() As SqlParameter = New SqlParameter(10) {}
                Dim output As String
                params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params1(2) = New SqlParameter("@InsSeqNo", SqlDbType.SmallInt)
                params1(3) = New SqlParameter("@DueDate", SqlDbType.VarChar, 8)
                params1(4) = New SqlParameter("@FloatingSeqNo", SqlDbType.SmallInt)
                params1(5) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
                params1(6) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
                params1(7) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
                params1(8) = New SqlParameter("@OutstandingPrincipal", SqlDbType.Decimal)
                params1(9) = New SqlParameter("@OutstandingInterest", SqlDbType.Decimal)
                params1(10) = New SqlParameter("@Output", SqlDbType.VarChar, 50)
                For intLoop = 0 To oCustomClass.MydataSet.Tables("AmortTable").Rows.Count - 1
                    If intLoop > 0 Then
                        params1(0).Value = oCustomClass.BranchId
                        params1(1).Value = oCustomClass.ApplicationID
                        params1(2).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("No")
                        params1(3).Value = CType(oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("DueDate"), String)
                        params1(4).Value = params(8).Value.ToString
                        params1(5).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("Installment")
                        params1(6).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("Principal")
                        params1(7).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("Interest")
                        params1(8).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("PrincBalance")
                        params1(9).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("PrincInterest")
                        params1(10).Direction = ParameterDirection.Output
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFloatingSaveReview2", params1)
                        output = CStr(params1(10).Value)
                        If output <> "" Then
                            transaction.Rollback()
                            oReturnValue.Output = output
                            Return oReturnValue
                        End If
                    End If
                Next
            End If

            Dim params3() As SqlParameter = New SqlParameter(4) {}
            params3(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params3(0).Value = oCustomClass.BranchId
            params3(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params3(1).Value = oCustomClass.ApplicationID
            params3(2) = New SqlParameter("@InsSeqNo", SqlDbType.BigInt)
            params3(2).Value = oCustomClass.SeqNo
            params3(3) = New SqlParameter("@NumOfInstallment", SqlDbType.BigInt)
            params3(3).Value = oCustomClass.NumOfInstallment
            params3(4) = New SqlParameter("@FloatingSeqNo", SqlDbType.Int)
            params3(4).Value = params(8).Value.ToString '.Value = oCustomClass.SeqNo

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFloatingSaveReview4", params3)


            transaction.Commit()
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccMnt.Floating")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "Print Trial "
    Public Function FloatingPrintTrial(ByVal oCustomClass As Parameter.FinancialData) As System.Data.DataSet
        Dim params() As SqlParameter = New SqlParameter(7) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.ApplicationID
        params(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(2).Value = oCustomClass.BusinessDate
        params(3) = New SqlParameter("@NumberOfInstallment", SqlDbType.SmallInt)
        params(3).Value = oCustomClass.NumOfInstallment
        params(4) = New SqlParameter("@FloatingPeriod", SqlDbType.VarChar, 50)
        params(4).Value = oCustomClass.FloatingPeriod
        params(5) = New SqlParameter("@EffectiveRate", SqlDbType.Decimal)
        params(5).Value = oCustomClass.EffectiveRate
        params(6) = New SqlParameter("@FlatRate", SqlDbType.Decimal)
        params(6).Value = oCustomClass.FlatRate
        params(7) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
        params(7).Value = oCustomClass.InstallmentAmount
        Try
            Return SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spFloatingPrintTrial", params)
        Catch exp As Exception
            WriteException("Floating", "PrintTrial", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region

End Class
