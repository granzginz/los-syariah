
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class LCWaived : Inherits Maxiloan.SQLEngine.LoanMnt.AccMntBase
    Private Const PARAM_REQUESTBY As String = "@requestby"
    Private Const PARAM_APPROVALNO As String = "@ApprovalNo"
    Private Const PARAM_WAIVENO As String = "@WaiveNo"
    Private Const REQUESTWAIVE As String = "spWaivedTransactionRequest"
    Private Const WAIVEVIEW As String = "spWaiveTransactionView"
    Private Const VIEWWAIVE As String = "spViewWaiveTransaction"

    Public Sub SaveLCWaived(ByVal customclass As Parameter.FullPrepay)
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim objTransaction As SqlTransaction
        Dim strApprovalNo As String
        Dim TotalDiscount As Double
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            objTransaction = oConnection.BeginTransaction
            With customclass
                TotalDiscount = .LcInstallmentWaived + _
                                .LcInsuranceWaived + _
                                .InstallmentCollFeeWaived + _
                                .InsuranceCollFeeWaived + _
                                .PDCBounceFeeWaived
            End With
            With oEntitiesApproval
                .ApprovalTransaction = objTransaction
                .BranchId = customclass.BranchId
                .SchemeID = "WA__"
                .RequestDate = customclass.BusinessDate
                .TransactionNo = customclass.ApplicationID
                .ApprovalValue = TotalDiscount
                .UserRequest = customclass.LoginId
                .UserApproval = customclass.RequestTo
                .ApprovalNote = customclass.ReasonDescription
                .AprovalType = Parameter.Approval.ETransactionType.Waive_Transaction
            End With
            strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

            Dim params() As SqlParameter = New SqlParameter(12) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID.Trim

            params(2) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(2).Value = customclass.ValueDate

            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = customclass.BusinessDate

            params(4) = New SqlParameter(PARAM_LCINSTALLMENTWAIVED, SqlDbType.Decimal)
            params(4).Value = customclass.LcInstallmentWaived

            params(5) = New SqlParameter(PARAM_LCINSURANCEWAIVED, SqlDbType.Decimal)
            params(5).Value = customclass.LcInsuranceWaived

            params(6) = New SqlParameter(PARAM_INSTALLMENTCOLLWAIVED, SqlDbType.Decimal)
            params(6).Value = customclass.InstallmentCollFeeWaived

            params(7) = New SqlParameter(PARAM_INSURANCECOLLWAIVED, SqlDbType.Decimal)
            params(7).Value = customclass.InsuranceCollFeeWaived

            params(8) = New SqlParameter(PARAM_PDCBOUNCEFEEWAIVED, SqlDbType.Decimal)
            params(8).Value = customclass.PDCBounceFeeWaived

            params(9) = New SqlParameter(PARAM_REASONID, SqlDbType.VarChar, 10)
            params(9).Value = customclass.ReasonID

            params(10) = New SqlParameter(PARAM_REQUESTBY, SqlDbType.VarChar, 12)
            params(10).Value = customclass.LoginId

            params(11) = New SqlParameter(PARAM_APPROVALNO, SqlDbType.VarChar, 50)
            params(11).Value = strApprovalNo

            params(12) = New SqlParameter(PARAM_NOTES, SqlDbType.Text)
            params(12).Value = customclass.ReasonDescription

            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, REQUESTWAIVE, params)
            objTransaction.Commit()
        Catch exp As Exception
            WriteException("LCWaived", "SaveLCWaived", exp.Message + exp.StackTrace)
            objTransaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Sub
    Public Function ListLCWaived(ByVal customclass As Parameter.FullPrepay) As Parameter.FullPrepay
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            If Not customclass.BranchId Is Nothing Then
                params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
                params(0).Value = customclass.BranchId.Replace("'", "")
            Else
                params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
                params(0).Value = ""
            End If

            If Not customclass.WaiveNo Is Nothing Then
                params(1) = New SqlParameter(PARAM_WAIVENO, SqlDbType.VarChar, 20)
                params(1).Value = customclass.WaiveNo.Trim
            Else
                params(1) = New SqlParameter(PARAM_WAIVENO, SqlDbType.VarChar, 20)
                params(1).Value = ""
            End If

            If Not customclass.ApplicationID Is Nothing Then
                params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
                params(2).Value = customclass.ApplicationID
            Else
                params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
                params(2).Value = ""

            End If


            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, WAIVEVIEW, params)
            If objread.Read Then
                With customclass
                    .BranchId = CStr(objread("BranchID"))
                    .WaiveNo = CStr(objread("WaiveNo"))
                    .Agreementno = CStr(objread("AgreementNo"))
                    .ApplicationID = CStr(objread("ApplicationID"))
                    .CustomerID = CStr(objread("CustomerID"))
                    .CustomerName = CStr(objread("CustomerName"))
                    .ValueDate = CDate(objread("EffectiveDate"))
                    .LcInstallmentWaived = CDbl(objread("LCInstallmentWaive"))
                    .LcInsuranceWaived = CDbl(objread("LCInsuranceWaive"))
                    .InstallmentCollFeeWaived = CDbl(objread("InstallCollectionFeeWaive"))
                    .InsuranceCollFeeWaived = CDbl(objread("InsuranceCollectionFeeWaive"))
                    .PDCBounceFeeWaived = CDbl(objread("PDCBounceFeeWaive"))
                    .UserApproval = CStr(objread("RequestBy"))
                    .Notes = CStr(objread("Notes"))
                    .ReasonID = CStr(objread("ReasonDescription"))
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("LCWaived", "ListLCWaived", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ViewLCWaived(ByVal customclass As Parameter.FullPrepay) As Parameter.FullPrepay
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            If Not customclass.BranchId Is Nothing Then
                params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
                params(0).Value = customclass.BranchId.Replace("'", "")
            Else
                params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
                params(0).Value = ""
            End If

            If Not customclass.WaiveNo Is Nothing Then
                params(1) = New SqlParameter(PARAM_WAIVENO, SqlDbType.VarChar, 20)
                params(1).Value = customclass.WaiveNo.Trim
            Else
                params(1) = New SqlParameter(PARAM_WAIVENO, SqlDbType.VarChar, 20)
                params(1).Value = ""
            End If

            If Not customclass.ApplicationID Is Nothing Then
                params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
                params(2).Value = customclass.ApplicationID
            Else
                params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
                params(2).Value = ""

            End If


            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, VIEWWAIVE, params)
            If objread.Read Then
                With customclass
                    .BranchId = CStr(objread("BranchID"))
                    .WaiveNo = CStr(objread("WaiveNo"))
                    .Agreementno = CStr(objread("AgreementNo"))
                    .ApplicationID = CStr(objread("ApplicationID"))
                    .CustomerID = CStr(objread("CustomerID"))
                    .CustomerName = CStr(objread("CustomerName"))
                    .ValueDate = CDate(objread("EffectiveDate"))
                    .LcInstallmentWaived = CDbl(objread("LCInstallmentWaive"))
                    .LcInsuranceWaived = CDbl(objread("LCInsuranceWaive"))
                    .InstallmentCollFeeWaived = CDbl(objread("InstallCollectionFeeWaive"))
                    .InsuranceCollFeeWaived = CDbl(objread("InsuranceCollectionFeeWaive"))
                    .PDCBounceFeeWaived = CDbl(objread("PDCBounceFeeWaive"))
                    .TotalNilai = CDbl(objread("TotalwaiveAmount"))
                    .UserApproval = CStr(objread("RequestBy"))
                    .Notes = CStr(objread("Notes"))
                    .ReasonID = CStr(objread("Reason"))
                    .UserApproval = CStr(objread("EmployeeName"))

                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("ViewLCWaived", "ViewLCWaived", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
