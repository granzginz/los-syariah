

#Region "Imports"
Imports System.IO
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class CashierTransaction : Inherits AccMntBase
    Private Const LISTCASHIERHISTORY As String = "spCashierTransactionList"

    Private Const OPENCASHIER As String = "spCashierTransactionOpen"
    Private Const CLOSEHEADCASHIER As String = "spCashierHeadClose"
    Private Const CLOSECASHIER As String = "spCashierTransactionClose"
    Private Const SPCASHIERLISTCLOSE As String = "spCashierListClose"
    Private Const SPCASHIERHEADLISTCLOSE As String = "spCashierHeadListClose"
    Private Const SPCHECKHEADCASHIER As String = "spCheckHeadCashier"
    Private Const SPCHECKALLCASHIERCLOSE As String = "spCheckAllCashierClose"
    Private Const SPVIEWCASHIER As String = "spGetCashier"
    Private Const SPPROCESSPDCCLEARING As String = "spProcessPDCClearing"
    Private Const SPDISBURSEMENTPROCESS As String = "spProcesDisbursementAP"
    Private Const SPGLINTERFACEFILEALLOCATION As String = "spGLJournalInterfaceFileAllocation"

    Private Const SPGLJOURNALHINTERFACE As String = "spGLJournalHInterface"
    Private Const SPGLJOURNALDINTERFACE As String = "spGLJournalDInterface"

    Private Const SPOUTSTANDING As String = "spOutstandingProcess"



    Private Const PARAM_ISHEADCASHIERCLOSE As String = "@IsHeadCashierClose"
    Private Const PARAM_HEADCASHIERBALANCE As String = "@HeadCashierBalance"
    Private Const PARAM_OPENINGSEQUENCE As String = "@openingsequence"
    Private Const PARAM_STATUS As String = "@status"
    Private Const PARAM_OPENINGAMOUNT As String = "@OpeningAmount"
    Private Const PARAM_AMOUNTPDC As String = "@PDCAmount"
    Private Const PARAM_NUMBERPDC As String = "@NumberPDC"
    Private Const PARAM_AMOUNTAPDISBURSE As String = "@AmountAPDisburse"

    Private Const PARAM_OPENINGDATE As String = "@openingdate"
    Private Const PARAM_ISCLOSED As String = "@isclosed"
    Private Const PARAM_CLOSINGAMOUNT As String = "@ClosingAmount"
    Private Const SPVIEWTRANS As String = "spGetTransaction"

#Region "Cashier"

    Public Function ListCashierTransactionHistory(ByVal customclass As Parameter.CashierTransaction) As Parameter.CashierTransaction

        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")

            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter(PARAM_HEADCASHIERBALANCE, SqlDbType.Decimal)
            params(2).Direction = ParameterDirection.Output

            params(3) = New SqlParameter(PARAM_ISHEADCASHIERCLOSE, SqlDbType.Bit)
            params(3).Direction = ParameterDirection.Output

            customclass.ListCashierHistory = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, LISTCASHIERHISTORY, params).Tables(0)
            customclass.IsHeadCashierClose = CBool(params(3).Value)
            customclass.HeadCashierBalance = CDbl(params(2).Value)
            Return customclass
        Catch exp As Exception
            WriteException("CashierTransaction", "ListCashierTransactionHistory", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub CashierClose(ByVal customclass As Parameter.CashierTransaction)
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(5) {}

            params(0) = New SqlParameter(PARAM_CASHIERID, SqlDbType.VarChar, 20)
            params(0).Value = customclass.LoginId

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 5)
            params(1).Value = customclass.BranchId

            params(2) = New SqlParameter(PARAM_OPENINGSEQUENCE, SqlDbType.Int)
            params(2).Value = customclass.OpeningSequence

            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = customclass.BusinessDate

            params(4) = New SqlParameter(PARAM_CLOSINGAMOUNT, SqlDbType.Decimal)
            params(4).Precision = 17
            params(4).Scale = 2
            params(4).Value = customclass.ClosingAmount



            params(5) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
            params(5).Value = getReferenceDBName()





            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, CLOSECASHIER, params)
            objtrans.Commit()

        Catch exp As Exception
            objtrans.Rollback()
            WriteException("CashierTransaction", "CashierClose", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Public Sub CashierOpen(ByVal customclass As Parameter.CashierTransaction)
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(4) {}

            params(0) = New SqlParameter(PARAM_CASHIERID, SqlDbType.VarChar, 20)
            params(0).Value = customclass.LoginId

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId.Replace("'", "")

            params(2) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(2).Value = customclass.BusinessDate

            params(3) = New SqlParameter(PARAM_OPENINGAMOUNT, SqlDbType.Decimal)
            params(3).Value = customclass.OpeningAmount

            params(4) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
            params(4).Value = getReferenceDBName()



            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, OPENCASHIER, params)
            objtrans.Commit()

        Catch exp As Exception
            objtrans.Rollback()
            WriteException("CashierTransaction", "CashierOpen", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Public Sub HeadCashierClose(ByVal customclass As Parameter.CashierTransaction)
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(0) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, CLOSEHEADCASHIER, params)
            objtrans.Commit()

        Catch exp As Exception
            objtrans.Rollback()
            WriteException("CashierTransaction", "HeadCashierClose", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Public Function CashierCloseList(ByVal customclass As Parameter.CashierTransaction) As Parameter.CashierTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(9) {}
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")

            params(1) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.LoginId

            params(2) = New SqlParameter(PARAM_OPENINGSEQUENCE, SqlDbType.Int)
            params(2).Value = customclass.OpeningSequence

            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = customclass.BusinessDate

            params(4) = New SqlParameter(PARAM_NUMBERPDC, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output

            params(5) = New SqlParameter(PARAM_AMOUNTPDC, SqlDbType.Decimal)
            params(5).Precision = 17
            params(5).Scale = 2
            params(5).Direction = ParameterDirection.Output

            params(6) = New SqlParameter(PARAM_OPENINGDATE, SqlDbType.DateTime)
            params(6).Direction = ParameterDirection.Output

            params(7) = New SqlParameter(PARAM_OPENINGAMOUNT, SqlDbType.Decimal)
            params(7).Precision = 17
            params(7).Scale = 2
            params(7).Direction = ParameterDirection.Output

            params(8) = New SqlParameter(PARAM_CLOSINGAMOUNT, SqlDbType.Decimal)
            params(8).Precision = 17
            params(8).Scale = 2
            params(8).Direction = ParameterDirection.Output

            params(9) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
            params(9).Value = getReferenceDBName()




            With customclass
                .ListCashierClose = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPCASHIERLISTCLOSE, params).Tables(0)
                .NumberOfPDC = CType(params(4).Value, Integer)
                .PDCAmount = CType(params(5).Value, Double)
                .BusinessDate = CType(params(6).Value, DateTime)
                .OpeningAmount = CType(params(7).Value, Double)
                .ClosingAmount = CType(params(8).Value, Double)
            End With
            Return customclass
        Catch exp As Exception
            WriteException("CashierTransaction", "CashierCloseList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function CHCloseList(ByVal customclass As Parameter.CashierTransaction) As Parameter.CashierTransaction
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")
            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate
            params(2) = New SqlParameter(PARAM_NUMBERPDC, SqlDbType.Int)
            params(2).Direction = ParameterDirection.Output
            params(3) = New SqlParameter(PARAM_AMOUNTAPDISBURSE, SqlDbType.Int)
            params(3).Direction = ParameterDirection.Output
            With customclass
                .ListHeadCashierHistory = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPCASHIERHEADLISTCLOSE, params).Tables(0)
                .TotalPDCToClear = CInt(params(2).Value)
                .TotalAPToDisburse = CInt(params(3).Value)
            End With
            Return customclass
        Catch exp As Exception
            WriteException("CashierTransaction", "CHCloseList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function CheckHeadCashier(ByVal customclass As Parameter.CashierTransaction) As Boolean
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")

            params(1) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.LoginId
            Return CBool(SqlHelper.ExecuteScalar(customclass.strConnection, CommandType.StoredProcedure, SPCHECKHEADCASHIER, params))
        Catch exp As Exception
            WriteException("CashierTransaction", "CheckHeadCashier", exp.Message)
        End Try
    End Function

    Public Function CheckAllCashierClose(ByVal customclass As Parameter.CashierTransaction) As Boolean
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")
            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate
            params(2) = New SqlParameter(PARAM_ISCLOSED, SqlDbType.Bit)
            params(2).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, SPCHECKALLCASHIERCLOSE, params)
            Return CBool(params(2).Value)
        Catch exp As Exception
            WriteException("CashierTransaction", "CheckHeadCashier", exp.Message)
        End Try
    End Function


    Public Function GetCashier(ByVal customclass As Parameter.CashierTransaction) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
            params(0).Value = customclass.WhereCond

            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPVIEWCASHIER, params).Tables(0)
        Catch exp As Exception
            WriteException("CashierTransaction", "GetCashier", exp.Message)
        End Try
    End Function

    Public Sub HeadCashierBatchProcess(ByVal customclass As Parameter.CashierTransaction)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim objcommand As New SqlCommand
        Dim objCommand2 As New SqlCommand
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Dim Params2() As SqlParameter = New SqlParameter(3) {}

        With objcommand
            .Parameters.Add(PARAM_BRANCHID, SqlDbType.VarChar, 3).Value = customclass.BranchId.Replace("'", "")
            .Parameters.Add(PARAM_COMPANYID, SqlDbType.VarChar, 3).Value = customclass.CoyID
            .Parameters.Add(PARAM_BUSINESSDATE, SqlDbType.DateTime).Value = customclass.BusinessDate
            .Parameters.Add(PARAM_LOGINID, SqlDbType.VarChar, 20).Value = customclass.LoginId
        End With
        With objCommand2
            .Parameters.Add(PARAM_BRANCHID, SqlDbType.VarChar, 3).Value = customclass.BranchId.Replace("'", "")
            .Parameters.Add(PARAM_COMPANYID, SqlDbType.VarChar, 3).Value = customclass.CoyID
            .Parameters.Add(PARAM_BUSINESSDATE, SqlDbType.DateTime).Value = customclass.BusinessDate
            .Parameters.Add(PARAM_LOGINID, SqlDbType.VarChar, 20).Value = customclass.LoginId
        End With
        Try
            Try
                If objcon.State = ConnectionState.Closed Then objcon.Open()
                objtrans = objcon.BeginTransaction
                With objcommand
                    .Transaction = objtrans
                    .CommandTimeout = 999999999
                    .CommandText = SPPROCESSPDCCLEARING
                    .CommandType = CommandType.StoredProcedure
                    .Connection = objcon
                End With
                objcommand.ExecuteNonQuery()
                '        SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPPROCESSPDCCLEARING, params)
                objtrans.Commit()
            Catch exp As Exception
                WriteException("CashierTransaction", "HeadCashierBatchProcess(PDC)", exp.Message)
                objtrans.Rollback()
                Throw New Exception("Batch PDC UnSuccess :" & exp.Message)
            Finally
                objcommand.Parameters.Clear()
                objcommand.Dispose()
                If objcon.State = ConnectionState.Open Then objcon.Close()
            End Try

            Try
                If objcon.State = ConnectionState.Closed Then objcon.Open()
                objtrans = objcon.BeginTransaction
                objCommand2.Transaction = objtrans
                With objCommand2
                    .Transaction = objtrans
                    .CommandTimeout = 999999999
                    .CommandText = SPPROCESSPDCCLEARING
                    .CommandType = CommandType.StoredProcedure
                    .Connection = objcon
                End With
                'SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPDISBURSEMENTPROCESS, params)
                objCommand2.ExecuteNonQuery()
                objtrans.Commit()
            Catch exp As Exception
                objtrans.Rollback()
                WriteException("CashierTransaction", "HeadCashierBatchProcess(APDisburse)", exp.Message)
                Throw New Exception("Batch PDC AP Disburse :" & exp.Message)
            Finally
                objCommand2.Parameters.Clear()
                objCommand2.Dispose()
                If objcon.State = ConnectionState.Open Then objcon.Close()
            End Try
        Catch exp As Exception
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub

    Public Function GLInterfaceDetail(ByVal ocustomclass As Parameter.CashierTransaction) As System.Data.DataTable
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = ocustomclass.BranchId.Replace("'", "")
        params(1) = New SqlParameter(PARAM_JOURNALCODE, SqlDbType.VarChar, 20)
        params(1).Value = ocustomclass.JournalCode

        Try
            Return SqlHelper.ExecuteDataset(ocustomclass.strConnection, CommandType.StoredProcedure, SPGLJOURNALDINTERFACE, params).Tables(0)
        Catch exp As Exception
            WriteException("CashierTransaction", "GLInterfaceDetail", exp.Message)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function GLInterfaceHeader(ByVal ocustomclass As Parameter.CashierTransaction) As System.Data.DataTable

        'Dim params() As SqlParameter = New SqlParameter(1) {}
        'params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        'params(0).Value = ocustomclass.BranchId.Replace("'", "")
        'params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
        'params(1).Value = ocustomclass.BusinessDate
        'Try
        '    Return SqlHelper.ExecuteDataset(ocustomclass.strConnection, CommandType.StoredProcedure, SPGLJOURNALHINTERFACE, params).Tables(0)
        'Catch exp As Exception
        '    WriteException("CashierTransaction", "GLInterfaceHeader", exp.Message)
        '    Throw New Exception(exp.Message)
        'End Try
        Dim DTJournalH As New DataTable
        Dim DTJournalD As New DataTable
        Dim PathFile As String
        Dim counterHeader As Integer
        Dim counterDetail As Integer
        Dim objStreamWriter As StreamWriter
        Dim strFileName As String
        Dim currentDate As Date
        Dim currentTransactionID As String
        Dim DA As New SqlDataAdapter
        Dim objread As SqlDataReader
        Dim strGLInterface As New StringBuilder

        Dim objcommand As New SqlCommand
        Dim objconnection As New SqlConnection(ocustomclass.strConnection)
        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objcommand.CommandType = CommandType.StoredProcedure
        objcommand.CommandText = "spGLJournalInterfaceFileAllocation"
        objcommand.Connection = objconnection

        PathFile = CType(objcommand.ExecuteScalar, String)

        If Not IO.Directory.Exists(PathFile) Then
            IO.Directory.CreateDirectory(PathFile)
        End If
        objcommand.Parameters.Add("@BranchID", SqlDbType.Char, 3).Value = ocustomclass.BranchId.Replace("'", "")
        objcommand.Parameters.Add("@BusinessDate", SqlDbType.DateTime).Value = ocustomclass.BusinessDate
        objcommand.CommandText = "SPGLJOURNALHINTERFACE"
        DA.SelectCommand = objcommand

        DA.Fill(DTJournalH)
        If DTJournalH.Rows.Count > 0 Then
            objcommand.Parameters.Clear()
            objcommand.Parameters.Add("@BranchID", SqlDbType.Char, 3)
            objcommand.Parameters.Add("@TransactionID", SqlDbType.VarChar, 20)
            objcommand.Parameters.Add("@BusinessDate", SqlDbType.DateTime)
            Dim strWriter As String = ""
            strFileName = CStr(ocustomclass.BranchId) + CDate(DTJournalH.Rows(counterHeader).Item("Tr_date")).ToString("ddMMyyyy")
            If Not File.Exists(PathFile & strFileName) Then
                objStreamWriter = New StreamWriter(PathFile & strFileName & ".txt", True, _
                    Encoding.ASCII)
                'strWriter = """RECTYPE""," & """BATCHID""," & """BTCHENTRY""," & """SRCELEDGER""," & """SRCETYPE""," & """JRNLDESC""" & vbCrLf
                'strWriter &= """RECTYPE""," & """BATCHNBR""," & """JOURNALID""," & """TRANSNBR""," & """ACCTID""," & """TRANSAMT""," & """TRANSDESC""" & vbCrLf
                strGLInterface.Append("""RECTYPE""," & """BATCHID""," & """BTCHENTRY""," & """SRCELEDGER""," & """SRCETYPE""," & """JRNLDESC""" & vbCrLf)
                strGLInterface.Append("""RECTYPE""," & """BATCHNBR""," & """JOURNALID""," & """TRANSNBR""," & """ACCTID""," & """TRANSAMT""," & """TRANSDESC""" & vbCrLf)

                For counterHeader = 0 To DTJournalH.Rows.Count - 1
                    counterDetail = 0
                    'strWriter &= """1"","
                    'strWriter &= """000001"","
                    'strWriter &= """" & CStr(counterHeader + 1).PadLeft(6, CType("0", Char)) & ""","
                    'strWriter &= """GL"","
                    'strWriter &= """HR"","
                    'strWriter &= """" & CStr(DTJournalH.Rows(counterHeader).Item("Tr_description")) & """" & vbCrLf
                    strGLInterface.Append("""1"",")
                    strGLInterface.Append("""000001"",")
                    strGLInterface.Append("""" & CStr(counterHeader + 1).PadLeft(6, CType("0", Char)) & """,")
                    strGLInterface.Append("""GL"",")
                    strGLInterface.Append("""HR"",")
                    strGLInterface.Append("""" & CStr(DTJournalH.Rows(counterHeader).Item("Tr_description")) & """" & vbCrLf)

                    objcommand.Parameters("@TransactionID").Value = CStr(DTJournalH.Rows(counterHeader).Item("TransactionID"))
                    objcommand.Parameters("@BusinessDate").Value = ocustomclass.BusinessDate
                    objcommand.Parameters("@BranchID").Value = ocustomclass.BranchId.Replace("'", "")
                    objcommand.CommandText = "SPGLJOURNALDINTERFACE"
                    objread = objcommand.ExecuteReader
                    While objread.Read
                        counterDetail += 1
                        'strWriter &= """2"","
                        'strWriter &= """000001"","
                        'strWriter &= """" & CStr(counterHeader + 1).PadLeft(6, CType("0", Char)) & ""","
                        'strWriter &= """" & CStr((counterDetail) * 10) & ""","
                        'strWriter &= """" & CStr(objread.Item("CoaID")) & ""","
                        'strWriter &= """" & CStr(objread.Item("Amount")) & ""","
                        'strWriter &= """" & CStr(objread.Item("Tr_description")) & """" & vbCrLf
                        strGLInterface.Append("""2"",")
                        strGLInterface.Append("""000001"",")
                        strGLInterface.Append("""" & CStr(counterHeader + 1).PadLeft(6, CType("0", Char)) & """,")
                        strGLInterface.Append("""" & CStr((counterDetail) * 10) & """,")
                        strGLInterface.Append("""" & CStr(objread.Item("CoaID")) & """,")
                        strGLInterface.Append("""" & CStr(objread.Item("Amount")) & """,")
                        strGLInterface.Append("""" & CStr(objread.Item("Tr_description")) & """" & vbCrLf)
                    End While
                    objread.Close()
                Next
                objStreamWriter.Write(strGLInterface.ToString)
                objStreamWriter.Close()
            End If
        End If
    End Function
    Public Function FileAllocationGLInterface(ByVal strConnection As String) As String
        Try
            Return CStr(SqlHelper.ExecuteScalar(strConnection, CommandType.StoredProcedure, SPGLINTERFACEFILEALLOCATION))
        Catch exp As Exception
            WriteException("CashierTransaction", "FileAllocationGLInterface", exp.Message)
            Throw New Exception(exp.Message)
        End Try
    End Function

#End Region

#Region "GetTransaction"

    Public Function GetTransaction(ByVal customclass As Parameter.CashierTransaction) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
            params(0).Value = customclass.WhereCond

            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPVIEWTRANS, params).Tables(0)
        Catch exp As Exception
            WriteException("CashierTransaction", "GetTransaction", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "GetOutProc"
    Public Function GetOutProc(ByVal customclass As Parameter.CashierTransaction) As Parameter.CashierTransaction
        Dim objread As SqlDataReader
        Try
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.Text, customclass.WhereCond)
            With customclass
                If objread.Read Then
                    .TotalRecord = CType(objread("Total"), Integer)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("CashierTransaction", "GetOutProc", exp.Message + exp.StackTrace)
        End Try

    End Function


    Public Function GetListOutProc(ByVal customclass As Parameter.CashierTransaction) As Parameter.CashierTransaction
        Try
            customclass.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPOUTSTANDING).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("CashierTransaction", "GetListOutProc", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

    Public Function getCashBankBalance(ByVal customClass As Parameter.CashierTransaction) As Parameter.CashierTransaction
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 4000)
            params(0).Value = customclass.WhereCond
            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 255)
            params(1).Value = customclass.SortBy
            customClass.DataSet = SqlHelper.ExecuteDataset(customClass.strConnection, _
            CommandType.StoredProcedure, "spCashBankEndingBalance", params)
            Return customClass
        Catch exp As Exception
            WriteException("CashierTransaction", "spGetCashBankEndingBalance", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function getCashBankBalanceHistory(ByVal customClass As Parameter.CashierTransaction) As Parameter.CashierTransaction
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@whereby", SqlDbType.VarChar, 4000)
            params(0).Value = customClass.WhereCond
            params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(1).Value = customClass.BusinessDate
            params(2) = New SqlParameter("@VocuherNo", SqlDbType.VarChar, 25)
            params(2).Value = customClass.JournalCode
            customClass.DataSet = SqlHelper.ExecuteDataset(customClass.strConnection, _
            CommandType.StoredProcedure, "spDailyTransReport", params)
            Return customClass
        Catch exp As Exception
            WriteException("CashierTransaction", "spDailyTransReport", exp.Message + exp.StackTrace)
        End Try
    End Function


End Class

