
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter

#End Region

Public Class DisburseFactoring : Inherits Maxiloan.SQLEngine.DataAccessBase


    Function getApprovalSchemeID(cnn As String, appId As String) As String
        Dim par As IList(Of SqlParameter) = New List(Of SqlParameter)
        par.Add(New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20) With {.Value = appId})
        Dim reader = SqlHelper.ExecuteReader(cnn, CommandType.StoredProcedure, "GetAgreementSchemeID", par.ToArray)
        If reader.Read Then
            Return CType(reader("SchemeID"), String)
        End If

        Return ""
    End Function

    Public Function DisburseApprovalList(ByVal customclass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim params(5) As SqlParameter
        With customclass
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = .SortBy
            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output
            params(5) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 20)
            params(5).Value = .AppID

        End With
        Try
            customclass.Listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spDisburseApprovalList", params).Tables(0)
            customclass.TotalRecord = CType(params(4).Value, Int16)
            Return customclass
        Catch exp As Exception
            WriteException("Disburse Factoring", "DisburseApprovalList", exp.Message + exp.StackTrace)
            Return Nothing
        End Try
    End Function

    Public Function GetCboUserAprPCAll(ByVal customclass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim oReturnValue As New Parameter.FactoringInvoice

        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@ApprType", SqlDbType.VarChar, 20)
        params(0).Value = customclass.ApprovalSchemeID
        params(1) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
        params(1).Value = customclass.AppID
        Try
            oReturnValue.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spApprovalListUser", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Disburse Factoring", "GetCboUserAprPCAll", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCboUserApproval(ByVal customclass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim oReturnValue As New Parameter.FactoringInvoice

        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
        params(0).Value = customclass.AppID
        params(1) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
        params(1).Value = customclass.LoginId

        Try
            oReturnValue.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGetCboUserApproval", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Disburse Factoring", "GetCboUserApproval", exp.Message + exp.StackTrace)
        End Try
    End Function

    Function ApproveSaveToNextPerson(ByVal customclass As Parameter.FactoringInvoice) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction

        If objConnection.State = ConnectionState.Closed Then objConnection.Open()
        objtrans = objConnection.BeginTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(14) {}
            params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.VarChar, 10)
            params(0).Value = customclass.ApprovalSchemeID
            params(1) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(1).Value = customclass.InvoiceSeqNo
            params(2) = New SqlParameter("@ApprovalResult", SqlDbType.VarChar, 1)
            params(2).Value = customclass.ApprovalResult
            params(3) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
            params(3).Value = customclass.BranchId
            params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(4).Value = customclass.BusinessDate
            params(5) = New SqlParameter("@ApprovedDate", SqlDbType.DateTime)
            params(5).Value = customclass.ApprovedDate
            params(6) = New SqlParameter("@notes", SqlDbType.VarChar, 1000)
            params(6).Value = customclass.notes
            params(7) = New SqlParameter("@SecurityCode", SqlDbType.VarChar, 50)
            params(7).Value = customclass.SecurityCode
            params(8) = New SqlParameter("@UserApproval", SqlDbType.VarChar, 255)
            params(8).Value = customclass.UserApproval
            params(9) = New SqlParameter("@UserSecurityCode", SqlDbType.VarChar, 255)
            params(9).Value = customclass.UserSecurityCode
            params(10) = New SqlParameter("@IsEverRejected", SqlDbType.Bit)
            params(10).Value = customclass.IsEverRejected
            params(11) = New SqlParameter("@LoginID", SqlDbType.VarChar, 255)
            params(11).Value = customclass.LoginId
            params(12) = New SqlParameter("@NextPersonApproval", SqlDbType.VarChar, 255)
            params(12).Value = customclass.NextPersonApproval
            params(13) = New SqlParameter("@ipaddress", SqlDbType.VarChar, 255)
            params(13).Value = ""
            params(14) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(14).Value = customclass.ApplicationID

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spApprovalRequestToNextPersonFactoring", params.ToArray)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Disburse Factoring", "ApproveSaveToNextPerson", exp.Message + exp.StackTrace)
            Throw New Exception("ERROR: Approval process error. " + exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Public Function ApproveSave(ByVal customclass As Parameter.FactoringInvoice) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        If objConnection.State = ConnectionState.Closed Then objConnection.Open()
        objtrans = objConnection.BeginTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(12) {}
            params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.VarChar, 10)
            params(0).Value = customclass.ApprovalSchemeID
            params(1) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(1).Value = customclass.InvoiceSeqNo
            params(2) = New SqlParameter("@ApprovalResult", SqlDbType.VarChar, 1)
            params(2).Value = customclass.ApprovalResult
            params(3) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
            params(3).Value = customclass.BranchId
            params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(4).Value = customclass.BusinessDate
            params(5) = New SqlParameter("@ApprovedDate", SqlDbType.DateTime)
            params(5).Value = customclass.ApprovedDate
            params(6) = New SqlParameter("@notes", SqlDbType.VarChar, 1000)
            params(6).Value = customclass.notes
            params(7) = New SqlParameter("@SecurityCode", SqlDbType.VarChar, 50)
            params(7).Value = customclass.SecurityCode
            params(8) = New SqlParameter("@UserApproval", SqlDbType.VarChar, 255)
            params(8).Value = customclass.UserApproval
            params(9) = New SqlParameter("@UserSecurityCode", SqlDbType.VarChar, 255)
            params(9).Value = customclass.UserSecurityCode
            params(10) = New SqlParameter("@IsEverRejected", SqlDbType.Bit)
            params(10).Value = customclass.IsEverRejected
            params(11) = New SqlParameter("@LoginID", SqlDbType.VarChar, 255)
            params(11).Value = customclass.LoginId
            params(12) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(12).Value = customclass.ApplicationID

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spApprovalSaveFinalFactoring", params.ToArray)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Disburse Factoring ", "ApproveSave", exp.Message + exp.StackTrace)
            Throw New Exception("ERROR: Approval process error. " + exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Function

    Public Function ApproveisFinal(ByVal customclass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim objread As SqlDataReader
        Dim params(4) As SqlParameter
        With customclass
            params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.VarChar, 50)
            params(0).Value = .ApprovalSchemeID
            params(1) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(1).Value = .InvoiceSeqNo
            params(2) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
            params(2).Value = .LoginId
            params(3) = New SqlParameter("@IsReRequest", SqlDbType.Bit)
            params(3).Value = .IsReRequest
            params(4) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(4).Value = .ApplicationID

        End With
        Try
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spFactoringApprovalScreenSetLimit", params)

            If Not IsNothing(objread) Then
                With customclass
                    If objread.Read Then
                        .IsFinal = CType(objread("IsFinal"), String)
                        .NextPersonApproval = CType(objread("NextPersonApproval"), String)
                    End If
                    objread.Close()
                End With
            End If

            Return customclass
        Catch exp As Exception
            WriteException("Disburse Factoring", "ApproveisFinal", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ApproveIsValidApproval(ByVal customclass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim objread As SqlDataReader
        Dim params(2) As SqlParameter
        With customclass
            params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.VarChar, 50)
            params(0).Value = .ApprovalSchemeID
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = .ApplicationID
            params(2) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(2).Value = .InvoiceSeqNo
        End With
        Try
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spFactoringApprovalScreenIsValidApproval", params)

            If Not IsNothing(objread) Then
                With customclass
                    If objread.Read Then
                        .UserApproval = CType(objread("UserApproval"), String)
                        .ApprovalNo = CType(objread("ApprovalNo"), String)
                        .ApplicationID = CType(objread("ApplicationID"), String)
                        .InvoiceNo = CType(objread("InvoiceNo"), String)
                        .InvoiceSeqNo = CType(objread("InvoiceSeqNo"), Integer)
                        .AgreementNo = CType(objread("AgreementNo"), String)
                    End If
                    objread.Close()
                End With
            End If

            Return customclass

        Catch exp As Exception
            WriteException("Disburse Factoring", "ApproveIsValidApproval", exp.Message + exp.StackTrace)
        End Try
    End Function


End Class
