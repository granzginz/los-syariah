

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region
Public Class TransferFundRequest : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name  
    Private Const LIST_REQUESTNO As String = "spGetNoTransaction"
    Private Const SAVE1 As String = "spPayReqSave1"
    Private Const SAVE2 As String = "spPayReqSave2"
    Private Const STR_SP_PR_PAGING As String = "spTransferFundRequestPaging"
    Private Const LIST_SELECT As String = "spPagingBankAccountOther"
    Private Const Update1 As String = "spPayReqUpdate1"
    Private Const Update2 As String = "spPayReqUpdate2"
    Private Const CekPR As String = "spCekPR"

#End Region
#Region "Get_RequestNo"
    Public Function Get_RequestNo(ByVal customClass As Parameter.TransferFundRequest) As String
        Dim Result As String
        Dim params() As SqlParameter = New SqlParameter(3) {}

        params(0) = New SqlParameter("@branchId", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@businessdate", SqlDbType.DateTime)
        params(1).Value = customClass.BusinessDate
        params(2) = New SqlParameter("@ID", SqlDbType.VarChar, 10)
        params(2).Value = customClass.ID
        params(3) = New SqlParameter("@sequenceNo", SqlDbType.VarChar, 20)
        params(3).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_REQUESTNO, params)
            Result = CType(params(3).Value, String)
            If Result <> "" Then
                Return Result
            End If
            Return ""
        Catch exp As Exception
            WriteException("TransferFundRequest", "Get_RequestNo", exp.Message)
        End Try

    End Function
#End Region

    Public Function SavePR(ByVal customClass As Parameter.TransferFundRequest) As String
        Dim oReturnValue As New Parameter.TransferFundRequest
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        'Dim intLoop As Integer
        Dim SeqNo As Integer = 0
        Dim Data1 As DataTable = customClass.PRDataTable
        Dim params() As SqlParameter = New SqlParameter(15) {}
        'Dim params1() As SqlParameter = New SqlParameter(7) {}
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@LoginID", SqlDbType.VarChar, 100)
            params(0).Value = customClass.LoginId
            params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(1).Value = customClass.BranchId
            params(2) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 100)
            params(2).Value = customClass.RequestNo
            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = customClass.BusinessDate
            params(4) = New SqlParameter("@BankAccount", SqlDbType.Char, 10)
            params(4).Value = customClass.BankAccount
            params(5) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
            params(5).Value = customClass.Description
            params(6) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(6).Value = customClass.Amount
            params(7) = New SqlParameter("@DepartementID", SqlDbType.VarChar, 3)
            params(7).Value = customClass.Departement
            params(8) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 100)
            params(8).Value = customClass.NoMemo
            params(9) = New SqlParameter("@AccountName", SqlDbType.VarChar, 100)
            params(9).Value = customClass.NamaRekening
            params(10) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 100)
            params(10).Value = customClass.NoRekening
            params(11) = New SqlParameter("@BankName", SqlDbType.VarChar, 100)
            params(11).Value = customClass.NamaBank
            params(12) = New SqlParameter("@StatusBank", SqlDbType.VarChar, 10)
            params(12).Value = customClass.StatusBank
            params(13) = New SqlParameter("@BankId", SqlDbType.VarChar, 5)
            params(13).Value = customClass.BankId

            params(14) = New SqlParameter("@detail", SqlDbType.Structured)
            params(14).Value = customClass.PRDataTable

            params(15) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(15).Direction = ParameterDirection.Output

            'dipindah ke Verifikasi Permintaan Pembayaran ACC (HO)
            'get approval scheme no 
            'oApproval.ApprovalTransaction = transaction
            'oApproval.TransactionNo = CStr(params(2).Value)
            'strApprovalNo = oApprovalID.RequestForApproval(oApproval)


            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SAVE1, params)
            ErrMessage = CType(params(15).Value, String)
            If ErrMessage <> "" Then
                transaction.Rollback()
                Return ErrMessage
            End If

            'If Data1.Rows.Count > 0 Then
            '    params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            '    params1(1) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            '    params1(2) = New SqlParameter("@SequenceNo", SqlDbType.Int)
            '    params1(3) = New SqlParameter("@PaymentAllocationID", SqlDbType.Char, 10)
            '    params1(4) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
            '    params1(5) = New SqlParameter("@Amount", SqlDbType.Decimal)
            '    params1(6) = New SqlParameter("@DepartementID", SqlDbType.VarChar, 3)
            '    params1(7) = New SqlParameter("@isPotong", SqlDbType.Bit)



            '    For intLoop = 0 To Data1.Rows.Count - 1
            '        SeqNo = intLoop + 1

            '        params1(0).Value = customClass.BranchId
            '        params1(1).Value = customClass.RequestNo
            '        params1(2).Value = SeqNo
            '        params1(3).Value = Data1.Rows(intLoop).Item("PaymentAllocationID")
            '        params1(4).Value = Data1.Rows(intLoop).Item("txtKeterangan")
            '        params1(5).Value = Data1.Rows(intLoop).Item("txtAmountTrans")
            '        params1(6).Value = Data1.Rows(intLoop).Item("DepartmentID")
            '        params1(7).Value = Data1.Rows(intLoop).Item("chkIsPotong")


            '        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SAVE2, params1)
            '    Next
            'End If

            transaction.Commit()
            Return ""
        Catch exp As Exception
            WriteException("TransferFundRequest", "SavePR", exp.Message)
            transaction.Rollback()
            Return Nothing
        Finally
            If conn.State = ConnectionState.Open Then conn.Dispose()
            conn.Dispose()
        End Try

    End Function
    Public Function CekPayment(ByVal customClass As Parameter.TransferFundRequest) As String
        Dim oReturnValue As New Parameter.TransferFundRequest
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 100)
            params(0).Value = customClass.NoMemo

            params(1) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(1).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, CekPR, params)
            ErrMessage = CType(params(1).Value, String)
            If ErrMessage <> "" Then
                transaction.Rollback()
                Return ErrMessage
            End If
            transaction.Commit()
            Return ""
        Catch exp As Exception
            WriteException("TransferFundRequest", "CekPR", exp.Message)
            transaction.Rollback()
            Return Nothing
        Finally
            If conn.State = ConnectionState.Open Then conn.Dispose()
            conn.Dispose()
        End Try

    End Function
    '
#Region "GetTransferFundRequestPaging"
    Public Function GetTransferFundRequestPaging(ByVal customClass As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
        Dim params(4) As SqlParameter

        Try
            With customClass
                params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
                params(0).Value = .CurrentPage

                params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
                params(1).Value = .PageSize

                params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond

                params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
                params(3).Value = .SortBy

                params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
                params(4).Direction = ParameterDirection.Output

                .ListData = SqlHelper.ExecuteDataset(.strConnection, CommandType.StoredProcedure, STR_SP_PR_PAGING, params).Tables(0)
                .TotalRecord = CInt(params(4).Value)
            End With
            Return customClass
        Catch exp As Exception
            WriteException("TransferFundRequest", "GetTransferFundRequestPaging", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region
#Region "GetTransferFundRequestHeader"
    Public Function GetTransferFundRequestHeader(ByVal oET As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
        Dim params(0) As SqlParameter
        Dim objReader As SqlDataReader

        Try
            With oET

                params(0) = New SqlParameter("@RequestNo", SqlDbType.Char, 100)
                params(0).Value = .RequestNo

                objReader = SqlHelper.ExecuteReader(.strConnection, CommandType.StoredProcedure, "spGetTransferFundRequestHeader", params)
            End With

            If objReader.Read Then
                With oET
                    .BranchName = CType(objReader("BranchFullName"), String)
                    .BankAccount = CType(objReader("BankAccountName"), String)
                    .Description = CType(objReader("Description"), String)
                    .Departement = CType(objReader("DepartementName"), String)
                    .RequestDate = CType(objReader("RequestDate"), Date)
                    .TotalAmount = CType(objReader("TotalAmount"), Double)
                    .TransferRefVoucherNo = CType(objReader("TransferVoucherNo"), String)
                    .TransferReferenceNo = CType(objReader("ReferenceNo"), String)
                    .TransferDate = CType(objReader("TransferDate"), Date)
                    .TransferAmount = CType(objReader("TransferAmount"), Double)
                    .Status = CType(objReader("Status"), String)
                    .StatusDate = CType(IIf(objReader("StatusDate") Is DBNull.Value, Date.MinValue, objReader("StatusDate")), Date)
                    .RequestBy = CType(objReader("RequestBy"), String)
                    '.PaymentAllocationID = CType(objReader("PaymentAllocationID"), String)
                    '.TransactionName = CType(objReader("TransactionName"), String)
                    .notes = CType(objReader("Note"), String)
                    '.COA = CType(objReader("COA"), String)
                    .DepartementId = CType(objReader("DepartementID"), String)
                    .NoMemo = CType(objReader("NoMemo"), String)
                    .NamaRekening = CType(objReader("AccountName"), String)
                    .NoRekening = CType(objReader("AccountNo"), String)
                    .StatusBank = CType(objReader("StatusBank"), String)
                    .BankAccountId = CType(objReader("BankAccountID"), String)
                    .SandiBank = CType(objReader("SandiBank"), String)
                    .Num = CType(objReader("Num"), Int64)
                    .NumApprove = CType(objReader("NumApprove"), Int64)
                End With
            End If
            objReader.Close()
            Return oET
        Catch exp As Exception
            WriteException("TransferFundRequest", "GetTransferFundRequestHeader", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region
#Region "GetTransferFundRequestDetail"
    Public Function GetTransferFundRequestDetail(ByVal oET As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
        Dim params(0) As SqlParameter
        Dim objReader As SqlDataReader

        Try
            With oET

                params(0) = New SqlParameter("@RequestNo", SqlDbType.Char, 100)
                params(0).Value = .RequestNo

                .ListData = SqlHelper.ExecuteDataset(.strConnection, CommandType.StoredProcedure, "spTransferFundRequestDetail", params).Tables(0)
            End With
            Return oET
        Catch exp As Exception
            WriteException("TransferFundRequest", "GetTransferFundRequestDetail", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "GetTransferFundRequestHistoryReject"
    Public Function GetTransferFundRequestHistoryReject(ByVal oET As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
        Dim params(0) As SqlParameter
        Dim objReader As SqlDataReader

        Try
            With oET

                params(0) = New SqlParameter("@RequestNo", SqlDbType.Char, 100)
                params(0).Value = .RequestNo

                .ListDataHistory = SqlHelper.ExecuteDataset(.strConnection, CommandType.StoredProcedure, "spTransferFundRequestHistoryReject", params).Tables(0)
            End With
            Return oET
        Catch exp As Exception
            WriteException("TransferFundRequest", "GetTransferFundRequestHistoryReject", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "GetTransferFundRequestHistoryApproval"
    Public Function GetTransferFundRequestHistoryApproval(ByVal oET As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
        Dim params(0) As SqlParameter
        Dim objReader As SqlDataReader

        Try
            With oET

                params(0) = New SqlParameter("@RequestNo", SqlDbType.Char, 100)
                params(0).Value = .RequestNo

                .ListDataHistoryApproval = SqlHelper.ExecuteDataset(.strConnection, CommandType.StoredProcedure, "spTransferFundRequestHistoryApproval", params).Tables(0)
            End With
            Return oET
        Catch exp As Exception
            WriteException("TransferFundRequest", "GetTransferFundRequestHistoryApproval", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

    Public Function GetPaymentVoucherByRequestNoPrint(ByVal ocustom As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
        Dim params(0) As SqlParameter
        Try
            With ocustom
                params(0) = New SqlParameter("@RequestNo", SqlDbType.Char, 100)
                params(0).Value = .RequestNo

                .PRDataSet = SqlHelper.ExecuteDataset(.strConnection, CommandType.StoredProcedure, "spTransferFundRequestVoucherPrint", params)

            End With
            Return ocustom
        Catch exp As Exception
            WriteException("TransferFundRequest", "spTransferFundRequestVoucherPrint", exp.Message + exp.StackTrace)
        End Try
    End Function
#Region "GetTransferFundRequestHeaderAndDetail"
    Public Function GetTransferFundRequestHeaderAndDetail(ByVal customClass As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
        Dim oDetail As Parameter.TransferFundRequest
        Try
            oDetail = GetTransferFundRequestDetail(customClass)
            oDetail = GetTransferFundRequestHistoryReject(customClass)
            oDetail = GetTransferFundRequestHistoryApproval(customClass)
            customClass = GetTransferFundRequestHeader(customClass)
            customClass.ListData = oDetail.ListData
            Return customClass
        Catch exp As Exception
            WriteException("TransferFundRequest", "GetTransferFundRequestHeaderAndDetail", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "GetTransferFundRequestDataset"
    Public Function GetTransferFundRequestDataset(ByVal customClass As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
        Dim params(4) As SqlParameter

        Try
            With customClass
                params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
                params(0).Value = -1

                params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
                params(1).Value = .PageSize

                params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond

                params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
                params(3).Value = .SortBy

                params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
                params(4).Direction = ParameterDirection.Output

                .PRDataSet = SqlHelper.ExecuteDataset(.strConnection, CommandType.StoredProcedure, STR_SP_PR_PAGING, params)
                .TotalRecord = CInt(params(4).Value)
            End With
            Return customClass
        Catch exp As Exception
            WriteException("TransferFundRequest", "GetTransferFundRequestDataset", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

    Sub saveApprovalTransferFundRequest(ByVal oClass As Parameter.TransferFundRequest, ByVal strApproval As String)
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim conn As New SqlConnection(oClass.strConnection)
        Dim transaction As SqlTransaction = Nothing

        Dim par() As SqlParameter = New SqlParameter(1) {}

        par(0) = New SqlParameter("@RequestNo", SqlDbType.Char, 100)
        par(0).Value = oClass.RequestNo
        par(1) = New SqlParameter("@Approval", SqlDbType.Char, 3)
        par(1).Value = strApproval

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            If (strApproval.Trim = "ACC") Then
                oClass.Approval.ApprovalTransaction = transaction
                'oClass.Approval.TransactionNo = CStr(params(2).Value)
                Dim strApprovalNo = oApprovalID.RequestForApproval(oClass.Approval)
            End If

            SqlHelper.ExecuteNonQuery(oClass.strConnection, CommandType.StoredProcedure, "spTransferFundRequestApproval ", par)
            transaction.Commit()

        Catch ex As Exception
            transaction.Rollback()
            Throw New Exception(ex.Message)

        Finally
            If conn.State = ConnectionState.Open Then conn.Dispose()
            conn.Dispose()

        End Try

    End Sub
    Sub PayReqStatusReject(ByVal oClass As Parameter.TransferFundRequest, ByVal strApproval As String)
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim conn As New SqlConnection(oClass.strConnection)
        Dim transaction As SqlTransaction = Nothing

        Dim par() As SqlParameter = New SqlParameter(6) {}

        par(0) = New SqlParameter("@RequestNo", SqlDbType.Char, 100)
        par(0).Value = oClass.RequestNo
        par(1) = New SqlParameter("@StatusPayment", SqlDbType.VarChar, 3)
        par(1).Value = oClass.PaymentStatus
        par(2) = New SqlParameter("@Note", SqlDbType.VarChar, 255)
        par(2).Value = oClass.notes
        par(3) = New SqlParameter("@RequestBy", SqlDbType.VarChar, 255)
        par(3).Value = oClass.RequestBy
        par(4) = New SqlParameter("@RequestDate", SqlDbType.DateTime)
        par(4).Value = oClass.RequestDate
        par(5) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
        par(5).Value = oClass.BranchId
        par(6) = New SqlParameter("@errMsg", SqlDbType.VarChar, 100)
        par(6).Direction = ParameterDirection.Output

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            SqlHelper.ExecuteNonQuery(oClass.strConnection, CommandType.StoredProcedure, "spTransferFundRequestReject", par)
            transaction.Commit()

        Catch ex As Exception
            transaction.Rollback()
            Throw New Exception(ex.Message)

        Finally
            If conn.State = ConnectionState.Open Then conn.Dispose()
            conn.Dispose()

        End Try

    End Sub
    Public Function PRCOAUpdate(ByVal customclass As Parameter.TransferFundRequest) As String
        Dim objconnection As New SqlConnection(customclass.strConnection)
        Dim objtransaction As SqlTransaction = Nothing
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objtransaction = objconnection.BeginTransaction

            params.Add(New SqlParameter("@RequestNo", SqlDbType.VarChar, 100) With {.Value = customclass.RequestNo})
            params.Add(New SqlParameter("@dtTransferFundRequestTrans", SqlDbType.Structured) With {.Value = customclass.ListData})


            Dim err = New SqlParameter("@errMsg", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            params.Add(err)

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "TransferFundRequestUpdateCOA", params.ToArray)

            objtransaction.Commit()
            Return err.Value

        Catch exp As Exception
            objtransaction.Rollback()
            WriteException("PCReimburse", "TransferFundRequestUpdateCOA", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update AP Disburse")

        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try

    End Function

    Public Function GetTransferFundRequestFormKuning(ByVal customClass As IList(Of String), cnn As String) As DataSet
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Dim dt = New DataTable()

        dt.Columns.Add("id", GetType(String))
        dt.Columns.Add("value1", GetType(String))
        dt.Columns.Add("value2", GetType(String))
        For Each v In customClass
            Dim row = dt.NewRow()
            row("id") = v
            row("value1") = ""
            row("value1") = ""
            dt.Rows.Add(row)
        Next
        params(0) = New SqlParameter("@RequestNo", SqlDbType.Structured)
        params(0).Value = dt

        Try '
            Return SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, "spTransferFundRequestFormKuning", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetBankAccountOther(ByVal oCustomClass As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
        Dim oReturnValue As New Parameter.TransferFundRequest
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(4).Value = oCustomClass.BranchId
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(5).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "GetCustomer", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.GetCustomer")
        End Try
    End Function
    Public Function UpdatePR(ByVal customClass As Parameter.TransferFundRequest) As String
        Dim oReturnValue As New Parameter.TransferFundRequest
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        'Dim intLoop As Integer
        Dim SeqNo As Integer = 0
        Dim Data1 As DataTable = customClass.PRDataTable
        Dim params() As SqlParameter = New SqlParameter(15) {}
        'Dim params1() As SqlParameter = New SqlParameter(6) {}
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@LoginID", SqlDbType.VarChar, 100)
            params(0).Value = customClass.LoginId
            params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(1).Value = customClass.BranchId
            params(2) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 100)
            params(2).Value = customClass.RequestNo
            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = customClass.BusinessDate
            params(4) = New SqlParameter("@BankAccount", SqlDbType.Char, 10)
            params(4).Value = customClass.BankAccount
            params(5) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
            params(5).Value = customClass.Description
            params(6) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(6).Value = customClass.Amount
            params(7) = New SqlParameter("@DepartementID", SqlDbType.VarChar, 3)
            params(7).Value = customClass.Departement
            params(8) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
            params(8).Value = customClass.NoMemo
            params(9) = New SqlParameter("@AccountName", SqlDbType.VarChar, 100)
            params(9).Value = customClass.NamaRekening
            params(10) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 100)
            params(10).Value = customClass.NoRekening
            params(11) = New SqlParameter("@BankName", SqlDbType.VarChar, 100)
            params(11).Value = customClass.NamaBank
            params(12) = New SqlParameter("@StatusBank", SqlDbType.VarChar, 10)
            params(12).Value = customClass.StatusBank
            params(13) = New SqlParameter("@BankId", SqlDbType.VarChar, 5)
            params(13).Value = customClass.BankId

            params(14) = New SqlParameter("@detail", SqlDbType.Structured)
            params(14).Value = customClass.PRDataTable

            params(15) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(15).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, Update1, params)
            ErrMessage = CType(params(15).Value, String)
            If ErrMessage <> "" Then
                transaction.Rollback()
                Return ErrMessage
            End If


            'If Data1.Rows.Count > 0 Then
            '    params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            '    params1(1) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            '    params1(2) = New SqlParameter("@SequenceNo", SqlDbType.Int)
            '    params1(3) = New SqlParameter("@PaymentAllocationID", SqlDbType.Char, 10)
            '    params1(4) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
            '    params1(5) = New SqlParameter("@Amount", SqlDbType.Decimal)
            '    params1(6) = New SqlParameter("@DepartementID", SqlDbType.VarChar, 3)

            '    For intLoop = 0 To Data1.Rows.Count - 1
            '        SeqNo = intLoop + 1

            '        params1(0).Value = customClass.BranchId
            '        params1(1).Value = customClass.RequestNo
            '        params1(2).Value = SeqNo
            '        params1(3).Value = Data1.Rows(intLoop).Item("PaymentAllocationID")
            '        params1(4).Value = Data1.Rows(intLoop).Item("txtKeterangan")
            '        params1(5).Value = Data1.Rows(intLoop).Item("txtAmountTrans")
            '        params1(6).Value = Data1.Rows(intLoop).Item("DepartmentID")

            '        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, Update2, params1)
            '    Next
            'End If

            transaction.Commit()
            Return ""
        Catch exp As Exception
            WriteException("TransferFundRequest", "UpdatePR", exp.Message)
            transaction.Rollback()
            Return Nothing
        Finally
            If conn.State = ConnectionState.Open Then conn.Dispose()
            conn.Dispose()
        End Try

    End Function
End Class
