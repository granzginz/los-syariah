
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region


Public Class Fiducia
    Inherits AccMntBase
#Region "Constanta"
    Private Const SP_NOTARY_CHARGE_VIEW As String = "spNotaryChargeView"
#End Region

#Region "GetNotary"
    Public Function GetNotary(ByVal customclass As Parameter.Fiducia) As Parameter.Fiducia
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@NotaryID", SqlDbType.VarChar, 10)
            params(1).Value = customclass.NotaryID

            customclass.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "SpGetNotary", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("Fiducia", "GetNotary", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("Fiducia-DA", "GetNotary", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
        End Try

    End Function
#End Region

#Region "GetNotaryCharge"
    Public Function GetNotaryCharge(ByVal customclass As Parameter.Fiducia) As Parameter.Fiducia
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@NotaryID", SqlDbType.VarChar, 10)
            params(1).Value = customclass.NotaryID

            params(2) = New SqlParameter("@SeqNo", SqlDbType.VarChar, 2)
            params(2).Value = customclass.SeqNo

            customclass.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SP_NOTARY_CHARGE_VIEW, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("Fiducia", "GetNotaryCharge", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("Fiducia-DA", "GetNotaryCharge", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
        End Try
    End Function
#End Region

#Region "FiduciaAktaReceive"
    Public Sub FiduciaAktaReceive(ByVal customclass As Parameter.Fiducia)
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim objTransaction As SqlTransaction
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        objTransaction = oConnection.BeginTransaction
        Dim params() As SqlParameter = New SqlParameter(12) {}

        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID

            params(2) = New SqlParameter("@SeqNo", SqlDbType.VarChar, 2)
            params(2).Value = customclass.SeqNo ' Ini adalah AssetSeqNo

            params(3) = New SqlParameter("@AktaNo", SqlDbType.VarChar, 30)
            params(3).Value = customclass.AktaNo

            params(4) = New SqlParameter("@AktaDate", SqlDbType.DateTime)
            params(4).Value = customclass.AktaDate

            params(5) = New SqlParameter("@CertificateNo", SqlDbType.VarChar, 30)
            params(5).Value = customclass.CertificateNo

            params(6) = New SqlParameter("@CertificateDate", SqlDbType.DateTime)
            params(6).Value = customclass.CertificateDate

            params(7) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 30)
            params(7).Value = customclass.InvoiceNo

            params(8) = New SqlParameter("@InvoiceDate", SqlDbType.DateTime)
            params(8).Value = customclass.InvoiceDate

            params(9) = New SqlParameter("@InvoiceNotes", SqlDbType.VarChar, 30)
            params(9).Value = customclass.InvoiceNotes

            params(10) = New SqlParameter("@FiduciaFee", SqlDbType.Float)
            params(10).Value = customclass.FiduciaFee

            params(11) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(11).Value = customclass.BusinessDate

            params(12) = New SqlParameter("@DueDate", SqlDbType.DateTime)
            params(12).Value = customclass.DueDate

            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "spFiduciaAktaReceive", params)
            objTransaction.Commit()

        Catch exp As Exception
            WriteException("Fiducia", "FiduciaAktaReceive", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("Fiducia-DA", "FiduciaAktaReceive", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            objTransaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try

    End Sub
#End Region

#Region "spFiduciaFarsial"
    Public Sub FiduciaFarsial(ByVal customclass As Parameter.Fiducia)
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim objTransaction As SqlTransaction
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        objTransaction = oConnection.BeginTransaction

        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            params.Add(New SqlParameter("@BranchID", SqlDbType.VarChar, 3) With {.Value = customclass.BranchId})
            params.Add(New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20) With {.Value = customclass.ApplicationID})
            params.Add(New SqlParameter("@SeqNo", SqlDbType.VarChar, 2) With {.Value = customclass.SeqNo})
            params.Add(New SqlParameter("@AktaNo", SqlDbType.VarChar, 30) With {.Value = customclass.AktaNo})
            params.Add(New SqlParameter("@AktaDate", SqlDbType.DateTime) With {.Value = customclass.AktaDate})
            params.Add(New SqlParameter("@CertificateNo", SqlDbType.VarChar, 30) With {.Value = customclass.CertificateNo})
            params.Add(New SqlParameter("@CertificateDate", SqlDbType.DateTime) With {.Value = customclass.CertificateDate})
            params.Add(New SqlParameter("@ReceiveDate", SqlDbType.DateTime) With {.Value = customclass.ReceiveDate})
            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "spFiduciaFarsial", params.ToArray())
            objTransaction.Commit()
        Catch exp As Exception
            Throw New Exception(exp.Message)
            objTransaction.Rollback()
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try

    End Sub
#End Region

#Region "GenerateFiduciaRoyaRegisterNo"
    Public Sub GenerateFiduciaRoyaRegisterNo(ByVal customclass As Parameter.Fiducia)
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim objTransaction As SqlTransaction
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        objTransaction = oConnection.BeginTransaction
        Dim params() As SqlParameter = New SqlParameter(2) {}

        Try

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID

            params(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(2).Value = customclass.BusinessDate

            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "spGenerateFiduciaRoyaRegisterNo", params)
            objTransaction.Commit()

        Catch exp As Exception
            WriteException("Fiducia", "GenerateFiduciaRoyaRegisterNo", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("Fiducia-DA", "GenerateFiduciaRoyaRegisterNo", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            objTransaction.Rollback()
            Throw New Exception(exp.Message)


        End Try

    End Sub
#End Region

#Region "FiduciaAdd"
    Public Sub FiduciaAdd(ByVal customclass As Parameter.Fiducia)
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim objTransaction As SqlTransaction
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        objTransaction = oConnection.BeginTransaction
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID

            params(2) = New SqlParameter("@SeqNo", SqlDbType.VarChar, 2)
            params(2).Value = customclass.SeqNo ' Ini adalah AssetSeqNo

            params(3) = New SqlParameter("@NotaryID", SqlDbType.VarChar, 10)
            params(3).Value = customclass.NotaryID

            params(4) = New SqlParameter("@RegisterDate", SqlDbType.DateTime)
            params(4).Value = customclass.RegisterDate

            params(5) = New SqlParameter("@OfferingDate", SqlDbType.DateTime)
            params(5).Value = customclass.OfferingDate

            params(6) = New SqlParameter("@RegisterNotes", SqlDbType.VarChar, 100)
            params(6).Value = customclass.RegisterNotes

            params(7) = New SqlParameter("@RegisterBy", SqlDbType.VarChar, 100)
            params(7).Value = customclass.RegisterBy

            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "SpFiduciaAdd", params)
            objTransaction.Commit()
        Catch exp As Exception
            WriteException("Fiducia", "FiduciaAdd", exp.Message + exp.StackTrace)
            objTransaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Sub
#End Region

#Region "NotaryChargeAdd"
    Public Sub NotaryChargeAdd(ByVal customclass As Parameter.Fiducia)
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim objTransaction As SqlTransaction
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        objTransaction = oConnection.BeginTransaction
        Dim params() As SqlParameter = New SqlParameter(6) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@NotaryID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.NotaryID

            params(2) = New SqlParameter("@OTRFrom", SqlDbType.Float)
            params(2).Value = customclass.OTRFrom

            params(3) = New SqlParameter("@OTRUntil", SqlDbType.Float)
            params(3).Value = customclass.OTRUntil

            params(4) = New SqlParameter("@FiduciaFee", SqlDbType.Float)
            params(4).Value = customclass.FiduciaFee

            params(5) = New SqlParameter("@SeqNoEdit", SqlDbType.VarChar, 3)
            params(5).Value = customclass.SeqNo

            params(6) = New SqlParameter("@ModeStatus", SqlDbType.VarChar, 10)
            params(6).Value = customclass.FullName  ' ModeStatus Add Or Edit

            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "spNotaryChargeAdd", params)
            objTransaction.Commit()
        Catch exp As Exception
            WriteException("Fiducia", "NotaryChargeAdd", exp.Message + exp.StackTrace)
            objTransaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try

    End Sub
#End Region
#Region "AddNotary"
    Public Sub AddNotary(ByVal customclass As Parameter.Fiducia)
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim objTransaction As SqlTransaction
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        objTransaction = oConnection.BeginTransaction
        Dim params() As SqlParameter = New SqlParameter(31) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@NotaryID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.NotaryID

            params(2) = New SqlParameter("@NotaryName", SqlDbType.VarChar, 50)
            params(2).Value = customclass.NotaryName

            params(3) = New SqlParameter("@NoSKMenKeh", SqlDbType.VarChar, 30)
            params(3).Value = customclass.NoSKMenkeh

            params(4) = New SqlParameter("@TglSkMenKeh", SqlDbType.DateTime)
            params(4).Value = customclass.TglSKMenkeh


            params(5) = New SqlParameter("@NotaryAddress", SqlDbType.VarChar, 200)
            params(5).Value = customclass.NotaryAddress

            params(6) = New SqlParameter("@NotaryRT", SqlDbType.VarChar, 10)
            params(6).Value = customclass.NotaryRT

            params(7) = New SqlParameter("@NotaryRW", SqlDbType.VarChar, 10)
            params(7).Value = customclass.NotaryRW

            params(8) = New SqlParameter("@NotaryKelurahan ", SqlDbType.VarChar, 50)
            params(8).Value = customclass.NotaryKelurahan

            params(9) = New SqlParameter("@NotaryKecamatan ", SqlDbType.VarChar, 50)
            params(9).Value = customclass.NotaryKecamatan

            params(10) = New SqlParameter("@NotaryCity ", SqlDbType.VarChar, 20)
            params(10).Value = customclass.NotaryCity

            params(11) = New SqlParameter("@NotaryZipCode ", SqlDbType.VarChar, 20)
            params(11).Value = customclass.NotaryZipCode

            params(12) = New SqlParameter("@NotaryAreaPhone1 ", SqlDbType.Char, 6)
            params(12).Value = customclass.NotaryAreaPhone1

            params(13) = New SqlParameter("@NotaryPhone1 ", SqlDbType.Char, 18)
            params(13).Value = customclass.NotaryPhone1

            params(14) = New SqlParameter("@NotaryAreaPhone2 ", SqlDbType.Char, 6)
            params(14).Value = customclass.NotaryAreaPhone2

            params(15) = New SqlParameter("@NotaryPhone2 ", SqlDbType.Char, 18)
            params(15).Value = customclass.NotaryPhone2

            params(16) = New SqlParameter("@NotaryAreaFax ", SqlDbType.Char, 6)
            params(16).Value = customclass.NotaryAreaFax

            params(17) = New SqlParameter("@NotaryFax ", SqlDbType.Char, 18)
            params(17).Value = customclass.NotaryFax

            params(18) = New SqlParameter("@EMail ", SqlDbType.Char, 20)
            params(18).Value = customclass.EMail

            params(19) = New SqlParameter("@MobilePhone ", SqlDbType.Char, 20)
            params(19).Value = customclass.MobilePhone

            params(20) = New SqlParameter("@ContactPersonName ", SqlDbType.Char, 50)
            params(20).Value = customclass.ContactPersonName

            params(21) = New SqlParameter("@ContactPersonTitle ", SqlDbType.Char, 10)
            params(21).Value = customclass.ContactPersonTitle

            params(22) = New SqlParameter("@NotaryBankID", SqlDbType.Char, 16)
            params(22).Value = customclass.NotaryBankID

            params(23) = New SqlParameter("@NotaryBankBranch", SqlDbType.Char, 40)
            params(23).Value = customclass.NotaryBankBranch

            params(24) = New SqlParameter("@NotaryAccountName ", SqlDbType.Char, 50)
            params(24).Value = customclass.NotaryAccountName

            params(25) = New SqlParameter("@NotaryAccountNo ", SqlDbType.Char, 30)
            params(25).Value = customclass.NotaryAccountNo

            params(26) = New SqlParameter("@IsActive", SqlDbType.Bit)
            params(26).Value = customclass.IsActive

            params(27) = New SqlParameter("@ModeStatus", SqlDbType.Char, 10)
            params(27).Value = customclass.ModeStatus

            params(28) = New SqlParameter("@NotaryBankName", SqlDbType.VarChar, 50)
            params(28).Value = customclass.NotaryBankName

            params(29) = New SqlParameter("@NotaryBankBranchID", SqlDbType.Int)
            params(29).Value = CInt(customclass.NotaryBankBranchID)

            params(30) = New SqlParameter("@NotaryBankBranchCity", SqlDbType.VarChar, 100)
            params(30).Value = customclass.NotaryBankBranchCity

            params(31) = New SqlParameter("@NotaryNPWP", SqlDbType.VarChar, 35)
            params(31).Value = customclass.NotaryNPWP

            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "SpAddNotary", params)
            objTransaction.Commit()

        Catch exp As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("Fiducia-DA.asp", "AddNotary", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            WriteException("Fiducia", "AddNotary", exp.Message + exp.StackTrace)
            objTransaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()

        End Try

    End Sub

#End Region

End Class
