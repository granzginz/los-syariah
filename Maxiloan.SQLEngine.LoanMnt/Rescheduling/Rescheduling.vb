
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region
Public Class Rescheduling : Inherits AccMntBase

    Private Const SPReschedulingSave As String = "spReschedulingSave"
    Private Const SPReschedulingSave2 As String = "spReschedulingSave2"
    Private Const SPReschedulingSave3 As String = "spReschedulingSave3"
    Private Const SPReschedulingSave4 As String = "spReschedulingSave4"
    Private Const SPReschedulingSave5 As String = "spReschedulingSave5"
    Private Const SPGETMINDUEDATE As String = "spGetMinDueDate"
    Private Const SPReschedulingBak As String = "spReschedulingBackupSave"

#Region "Request Rescheduling "
    Public Function GetMinDueDate(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oCustomClass.BranchId

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(2).Value = oCustomClass.BusinessDate

            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, SPGETMINDUEDATE, params)
            With oCustomClass
                If objread.Read Then
                    .SeqNo = CInt(objread("InsSeqNo"))
                    .DueDate = CDate(objread("DueDate"))
                    .Tenor = CInt(objread("Tenor"))
                    .MaxSeqNo = CInt(objread("MaxSeqNo"))
                    .NewNumInst = CInt(objread("NewNumInst"))
                End If
                objread.Close()
            End With
            Return oCustomClass
        Catch exp As Exception
            WriteException("AssetReplacement", "ReportListAgreement", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function SaveReschedulingData(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable, _
               ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim transaction As SqlTransaction
        Dim strApprovalNo As String
        Dim intLoop As Integer
        Dim Temp_PrincipleAmount As Double = 0
        Dim Temp_InterestAmount As Double = 0
        Dim Temp_InstallmentAmount As Double = 0
        Dim Temp_OutstandingPrincipal As Double = 0
        Dim Temp_OutstandingInterest As Double = 0
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            With oEntitiesApproval
                .ApprovalTransaction = transaction
                .BranchId = oCustomClass.BranchId
                .SchemeID = "RESC"
                .RequestDate = oCustomClass.EffectiveDate
                .TransactionNo = oCustomClass.ApplicationID
                .ApprovalValue = oCustomClass.TotalAmountToBePaid
                .UserRequest = oCustomClass.LoginId
                .UserApproval = oCustomClass.RequestTo
                .ApprovalNote = oCustomClass.Notes
                .AprovalType = Parameter.Approval.ETransactionType.Rescheduling_Approval
            End With
            strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

            'modify Nofi karena saat harus insert ke InstallmentScheduleResch dahulu.
            If oCustomClass.MydataSet.Tables("AmortTable").Rows.Count > 0 Then
                Dim params1() As SqlParameter = New SqlParameter(11) {}
                Dim output As String
                params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params1(2) = New SqlParameter("@InsSeqNo", SqlDbType.SmallInt)
                params1(3) = New SqlParameter("@DueDate", SqlDbType.VarChar, 10)
                params1(4) = New SqlParameter("@ReschSeqNo", SqlDbType.SmallInt)
                params1(5) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
                params1(6) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
                params1(7) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
                params1(8) = New SqlParameter("@OutstandingPrincipal", SqlDbType.Decimal)
                params1(9) = New SqlParameter("@OutstandingInterest", SqlDbType.Decimal)
                params1(10) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(11) = New SqlParameter("@Output", SqlDbType.VarChar, 50)
                For intLoop = 0 To oCustomClass.MydataSet.Tables("AmortTable").Rows.Count - 1
                    If intLoop > 0 Then
                        params1(0).Value = oCustomClass.BranchId
                        params1(1).Value = oCustomClass.ApplicationID
                        params1(2).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("No")
                        params1(3).Value = CType(oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("DueDate"), String)
                        'params1(4).Value = params(2).Value.ToString
                        params1(4).Value = 0
                        params1(5).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("Installment")
                        params1(6).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("Principal")
                        params1(7).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("Interest")
                        params1(8).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("PrincBalance")
                        params1(9).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("PrincInterest")
                        params1(10).Value = oCustomClass.BusinessDate
                        params1(11).Direction = ParameterDirection.Output
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave, params1)
                        output = CStr(params1(11).Value)
                        If output <> "" Then
                            transaction.Rollback()
                            oReturnValue.Output = output
                            Return oReturnValue
                        End If
                    End If
                Next
            End If
            '-============================================================================================================

            Dim params() As SqlParameter = New SqlParameter(55) {}
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter("@SeqNo ", SqlDbType.Int)
            params(2).Direction = ParameterDirection.Output '.Value = oCustomClass.SeqNo

            params(3) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
            params(3).Value = oCustomClass.CustomerID

            params(4) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
            params(4).Value = oCustomClass.EffectiveDate

            params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(5).Value = oCustomClass.BusinessDate

            params(6) = New SqlParameter("@FlatRate", SqlDbType.BigInt)
            params(6).Value = oCustomClass.FlatRate

            params(7) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params(7).Value = oCustomClass.PaymentFrequency

            params(8) = New SqlParameter("@InstallmentScheme", SqlDbType.Char, 2)
            params(8).Value = oCustomClass.InstallmentScheme

            params(9) = New SqlParameter("@NumOfInstallment", SqlDbType.SmallInt)
            params(9).Value = oCustomClass.NumOfInstallment

            params(10) = New SqlParameter("@OutstandingTenor", SqlDbType.SmallInt)
            params(10).Value = oCustomClass.OutstandingTenor

            params(11) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(11).Value = oCustomClass.Tenor

            params(12) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
            params(12).Value = oCustomClass.InstallmentAmount

            params(13) = New SqlParameter("@PartialPrepaymentAmount", SqlDbType.Decimal)
            params(13).Value = oCustomClass.PartialPrepaymentAmount

            params(14) = New SqlParameter("@AdministrationFee", SqlDbType.BigInt)
            params(14).Value = oCustomClass.AdministrationFee

            params(15) = New SqlParameter("@ContractPrepaidAmount", SqlDbType.Decimal)
            params(15).Value = oCustomClass.ContractPrepaidAmount

            params(16) = New SqlParameter("@OutstandingPrincipalNew", SqlDbType.Decimal)
            params(16).Value = oCustomClass.OutstandingPrincipalNew

            params(17) = New SqlParameter("@OutstandingInterestNew", SqlDbType.Decimal)
            params(17).Value = oCustomClass.OutstandingInterestNew

            params(18) = New SqlParameter("@OutstandingPrincipalOld", SqlDbType.Decimal)
            params(18).Value = oCustomClass.OutstandingPrincipalOld

            params(19) = New SqlParameter("@OutstandingInterestOld", SqlDbType.Decimal)
            params(19).Value = oCustomClass.OutstandingInterestOld

            params(20) = New SqlParameter("@OSInstallmentDue", SqlDbType.Decimal)
            params(20).Value = oCustomClass.InstallmentDue

            params(21) = New SqlParameter("@OSInsuranceDue", SqlDbType.Decimal)
            params(21).Value = oCustomClass.InsuranceDue

            params(22) = New SqlParameter("@OSLCInstallment", SqlDbType.Decimal)
            params(22).Value = oCustomClass.LcInstallment

            params(23) = New SqlParameter("@OSLCInsurance", SqlDbType.Decimal)
            params(23).Value = oCustomClass.LcInsurance

            params(24) = New SqlParameter("@OSInstallCollectionFee", SqlDbType.Decimal)
            params(24).Value = oCustomClass.InstallmentCollFee

            params(25) = New SqlParameter("@OSInsuranceCollectionFee", SqlDbType.Decimal)
            params(25).Value = oCustomClass.InsuranceCollFee

            params(26) = New SqlParameter("@OSPDCBounceFee", SqlDbType.Decimal)
            params(26).Value = oCustomClass.PDCBounceFee

            params(27) = New SqlParameter("@OSInsuranceClaimExpense", SqlDbType.Decimal)
            params(27).Value = oCustomClass.InsuranceClaimExpense

            params(28) = New SqlParameter("@OSCollectionExpense", SqlDbType.Decimal)
            params(28).Value = oCustomClass.CollectionExpense

            params(29) = New SqlParameter("@AccruedAmount", SqlDbType.Decimal)
            params(29).Value = oCustomClass.AccruedInterest

            params(30) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
            params(30).Value = strApprovalNo

            params(31) = New SqlParameter("@RequestTo", SqlDbType.VarChar, 20)
            params(31).Value = oCustomClass.RequestTo

            params(32) = New SqlParameter("@ReasonTypeID", SqlDbType.Char, 5)
            params(32).Value = oCustomClass.ReasonTypeID

            params(33) = New SqlParameter("@ReasonID", SqlDbType.VarChar, 10)
            params(33).Value = oCustomClass.ReasonID

            params(34) = New SqlParameter("@Tr_Nomor", SqlDbType.VarChar, 10)
            params(34).Value = oCustomClass.TR_Nomor

            params(35) = New SqlParameter("@Notes", SqlDbType.Text)
            params(35).Value = oCustomClass.Notes

            params(36) = New SqlParameter("@Status", SqlDbType.Char, 1)
            params(36).Value = oCustomClass.Status

            params(37) = New SqlParameter("@GuarantorID", SqlDbType.Char, 20)
            params(37).Value = oCustomClass.GuarantorID

            params(38) = New SqlParameter("@InterestType", SqlDbType.VarChar, 2)
            params(38).Value = oCustomClass.InterestType

            params(39) = New SqlParameter("@EffectiveRate", SqlDbType.Decimal)
            params(39).Value = oCustomClass.EffectiveRate

            params(40) = New SqlParameter("@OSSTNKRenewalFee", SqlDbType.Decimal)
            params(40).Value = oCustomClass.STNKRenewalFee

            params(41) = New SqlParameter("@LCInstallmentAmountDisc", SqlDbType.Decimal)
            params(41).Value = oCustomClass.LCInstallmentAmountDisc

            params(42) = New SqlParameter("@LCInsuranceAmountDisc", SqlDbType.Decimal)
            params(42).Value = oCustomClass.LCInsuranceAmountDisc

            params(43) = New SqlParameter("@InstallCollectionFeeDisc", SqlDbType.Decimal)
            params(43).Value = oCustomClass.InstallCollectionFeeDisc

            params(44) = New SqlParameter("@InsurCollectionFeeDisc", SqlDbType.Decimal)
            params(44).Value = oCustomClass.InsurCollectionFeeDisc

            params(45) = New SqlParameter("@PDCBounceFeeDisc", SqlDbType.Decimal)
            params(45).Value = oCustomClass.PDCBounceFeeDisc

            params(46) = New SqlParameter("@STNKFeeDisc", SqlDbType.Decimal)
            params(46).Value = oCustomClass.STNKFeeDisc

            params(47) = New SqlParameter("@InsuranceClaimExpenseDisc", SqlDbType.Decimal)
            params(47).Value = oCustomClass.InsuranceClaimExpenseDisc

            params(48) = New SqlParameter("@RepossesFeeDisc", SqlDbType.Decimal)
            params(48).Value = oCustomClass.RepossesFeeDisc

            params(49) = New SqlParameter("@TotalDiscount", SqlDbType.Decimal)
            params(49).Value = oCustomClass.TotalDiscount

            params(50) = New SqlParameter("@TotalAmountToBePaid", SqlDbType.Decimal)
            params(50).Value = oCustomClass.TotalAmountToBePaid

            params(51) = New SqlParameter("@TotalOSAR", SqlDbType.Decimal)
            params(51).Value = oCustomClass.TotalOSAR

            params(52) = New SqlParameter("@StepUpStepDownType", SqlDbType.VarChar, 2)
            params(52).Value = oCustomClass.StepUpStepDownType

            params(53) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            params(53).Direction = ParameterDirection.Output

            params(54) = New SqlParameter("@Penalty", SqlDbType.Decimal)
            params(54).Value = oCustomClass.Penalty

            params(55) = New SqlParameter("@TerminationPenalty", SqlDbType.Decimal)
            params(55).Value = oCustomClass.TerminationPenalty

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave2, params)


			'''' exec ke table agreementpphrestruct
			Dim paramspph() As SqlParameter = New SqlParameter(4) {}
			paramspph(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
			paramspph(0).Value = oCustomClass.BranchId
			paramspph(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
			paramspph(1).Value = oCustomClass.ApplicationID
			paramspph(2) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
			paramspph(2).Value = oCustomClass.CustomerID
			paramspph(3) = New SqlParameter("@AccruedInterest", SqlDbType.Decimal)
			paramspph(3).Value = oCustomClass.PphAccrued
			paramspph(4) = New SqlParameter("@LcAmount", SqlDbType.Decimal)
			paramspph(4).Value = oCustomClass.PphDenda
			SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spReschedulingSavePPH", paramspph)

			Dim params4() As SqlParameter = New SqlParameter(3) {}
            params4(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params4(0).Value = oCustomClass.BranchId
            params4(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params4(1).Value = oCustomClass.ApplicationID
            params4(2) = New SqlParameter("@ReschSeqNo", SqlDbType.Int)
            params4(2).Value = params(2).Value.ToString '.Value = oCustomClass.SeqNo
            params4(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params4(3).Value = oCustomClass.EffectiveDate
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingBak, params4)

            Dim params2() As SqlParameter = New SqlParameter(7) {}
            If oData1.Rows.Count > 0 Then
                params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params2(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params2(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params2(3) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params2(4) = New SqlParameter("@IsChecked", SqlDbType.Bit)
                params2(5) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params2(6) = New SqlParameter("@Notes", SqlDbType.Text)
                params2(7) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                For intLoop = 0 To oData1.Rows.Count - 1
                    params2(0).Value = oCustomClass.BranchId
                    params2(1).Value = oCustomClass.ApplicationID
                    params2(2).Value = params(2).Value.ToString
                    params2(3).Value = oData1.Rows(intLoop).Item("MasterTCID").ToString
                    params2(4).Value = CBool(oData1.Rows(intLoop).Item("Checked"))
                    params2(5).Value = oCustomClass.BusinessDate
                    params2(6).Value = oData1.Rows(intLoop).Item("Notes").ToString
                    params2(7).Value = params(53).Value.ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave3, params2)

                Next
            End If
            Dim params3() As SqlParameter = New SqlParameter(8) {}
            If oData2.Rows.Count > 0 Then
                params3(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params3(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params3(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params3(3) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params3(4) = New SqlParameter("@MasterTCCLSequenceNo", SqlDbType.Int)
                params3(5) = New SqlParameter("@IsChecked", SqlDbType.Bit)
                params3(6) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params3(7) = New SqlParameter("@Notes", SqlDbType.Text)
                params3(8) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                For intLoop = 0 To oData2.Rows.Count - 1
                    params3(0).Value = oCustomClass.BranchId
                    params3(1).Value = oCustomClass.ApplicationID.Trim
                    params3(2).Value = params(2).Value.ToString
                    params3(3).Value = Trim(oData2.Rows(intLoop).Item("MasterTCID").ToString)
                    params3(4).Value = oData2.Rows(intLoop).Item("MSTCCLSequenceNo")
                    params3(5).Value = CBool(oData2.Rows(intLoop).Item("Checked"))
                    params3(6).Value = oCustomClass.BusinessDate
                    params3(7).Value = Trim(oData2.Rows(intLoop).Item("Notes").ToString)
                    params3(8).Value = params(53).Value.ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave4, params3)
                Next
            End If

            Dim params5() As SqlParameter = New SqlParameter(4) {}
            If oData3.Rows.Count > 0 Then
                params5(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params5(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params5(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params5(3) = New SqlParameter("@CrossDefaultApplicationId", SqlDbType.VarChar, 20)
                params5(4) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                For intLoop = 0 To oData3.Rows.Count - 1
                    params5(0).Value = oCustomClass.BranchId
                    params5(1).Value = oCustomClass.ApplicationID.Trim
                    params5(2).Value = params(2).Value.ToString
                    params5(3).Value = Trim(oData3.Rows(intLoop).Item("CrossDefaultApplicationId").ToString)
                    params5(4).Value = params(53).Value.ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave5, params5)
                Next
            Else
                params5(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params5(0).Value = oCustomClass.BranchId
                params5(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params5(1).Value = oCustomClass.ApplicationID.Trim
                params5(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params5(2).Value = params(2).Value.ToString
                params5(3) = New SqlParameter("@CrossDefaultApplicationId", SqlDbType.VarChar, 20)
                params5(3).Value = oCustomClass.txtSearch
                params5(4) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                params5(4).Value = params(53).Value.ToString
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave5, params5)
            End If



            Dim params6() As SqlParameter = New SqlParameter(3) {}
            params6(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params6(0).Value = oCustomClass.BranchId
            params6(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params6(1).Value = oCustomClass.ApplicationID
            params6(2) = New SqlParameter("@ReschSeqNo", SqlDbType.Int)
            params6(2).Value = params(2).Value.ToString
            params6(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params6(3).Value = oCustomClass.EffectiveDate
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spReschedulingSave6", params6)

            transaction.Commit()
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccMnt.Rescheduling")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "Save ReschedulingIRR "
    Public Function SaveReschedulingIRR(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable, _
              ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim transaction As SqlTransaction
        Dim strApprovalNo As String
        Dim intLoop As Integer
        Dim Temp_PrincipleAmount As Double = 0
        Dim Temp_InterestAmount As Double = 0
        Dim Temp_InstallmentAmount As Double = 0
        Dim Temp_OutstandingPrincipal As Double = 0
        Dim Temp_OutstandingInterest As Double = 0
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            With oEntitiesApproval
                .ApprovalTransaction = transaction
                .BranchId = oCustomClass.BranchId
                .SchemeID = "RESC"
                .RequestDate = oCustomClass.EffectiveDate
                .TransactionNo = oCustomClass.ApplicationID
                .ApprovalValue = oCustomClass.TotalAmountToBePaid
                .UserRequest = oCustomClass.LoginId
                .UserApproval = oCustomClass.RequestTo
                .ApprovalNote = oCustomClass.Notes
                .AprovalType = Parameter.Approval.ETransactionType.Rescheduling_Approval
            End With
            strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

            Dim params() As SqlParameter = New SqlParameter(55) {}
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter("@SeqNo ", SqlDbType.Int)
            params(2).Direction = ParameterDirection.Output '.Value = oCustomClass.SeqNo

            params(3) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
            params(3).Value = oCustomClass.CustomerID

            params(4) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
            params(4).Value = oCustomClass.EffectiveDate

            params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(5).Value = oCustomClass.BusinessDate

            params(6) = New SqlParameter("@FlatRate", SqlDbType.BigInt)
            params(6).Value = oCustomClass.FlatRate

            params(7) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params(7).Value = oCustomClass.PaymentFrequency

            params(8) = New SqlParameter("@InstallmentScheme", SqlDbType.Char, 2)
            params(8).Value = oCustomClass.InstallmentScheme

            params(9) = New SqlParameter("@NumOfInstallment", SqlDbType.SmallInt)
            params(9).Value = oCustomClass.NumOfInstallment

            params(10) = New SqlParameter("@OutstandingTenor", SqlDbType.SmallInt)
            params(10).Value = oCustomClass.OutstandingTenor

            params(11) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(11).Value = oCustomClass.Tenor

            params(12) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
            params(12).Value = oCustomClass.InstallmentAmount

            params(13) = New SqlParameter("@PartialPrepaymentAmount", SqlDbType.Decimal)
            params(13).Value = oCustomClass.PartialPrepaymentAmount

            params(14) = New SqlParameter("@AdministrationFee", SqlDbType.BigInt)
            params(14).Value = oCustomClass.AdministrationFee

            params(15) = New SqlParameter("@ContractPrepaidAmount", SqlDbType.Decimal)
            params(15).Value = oCustomClass.ContractPrepaidAmount

            params(16) = New SqlParameter("@OutstandingPrincipalNew", SqlDbType.Decimal)
            params(16).Value = oCustomClass.OutstandingPrincipalNew

            params(17) = New SqlParameter("@OutstandingInterestNew", SqlDbType.Decimal)
            params(17).Value = oCustomClass.OutstandingInterestNew

            params(18) = New SqlParameter("@OutstandingPrincipalOld", SqlDbType.Decimal)
            params(18).Value = oCustomClass.OutstandingPrincipalOld

            params(19) = New SqlParameter("@OutstandingInterestOld", SqlDbType.Decimal)
            params(19).Value = oCustomClass.OutstandingInterestOld

            params(20) = New SqlParameter("@OSInstallmentDue", SqlDbType.Decimal)
            params(20).Value = oCustomClass.InstallmentDue

            params(21) = New SqlParameter("@OSInsuranceDue", SqlDbType.Decimal)
            params(21).Value = oCustomClass.InsuranceDue

            params(22) = New SqlParameter("@OSLCInstallment", SqlDbType.Decimal)
            params(22).Value = oCustomClass.LcInstallment

            params(23) = New SqlParameter("@OSLCInsurance", SqlDbType.Decimal)
            params(23).Value = oCustomClass.LcInsurance

            params(24) = New SqlParameter("@OSInstallCollectionFee", SqlDbType.Decimal)
            params(24).Value = oCustomClass.InstallmentCollFee

            params(25) = New SqlParameter("@OSInsuranceCollectionFee", SqlDbType.Decimal)
            params(25).Value = oCustomClass.InsuranceCollFee

            params(26) = New SqlParameter("@OSPDCBounceFee", SqlDbType.Decimal)
            params(26).Value = oCustomClass.PDCBounceFee

            params(27) = New SqlParameter("@OSInsuranceClaimExpense", SqlDbType.Decimal)
            params(27).Value = oCustomClass.InsuranceClaimExpense

            params(28) = New SqlParameter("@OSCollectionExpense", SqlDbType.Decimal)
            params(28).Value = oCustomClass.CollectionExpense

            params(29) = New SqlParameter("@AccruedAmount", SqlDbType.Decimal)
            params(29).Value = oCustomClass.AccruedInterest

            params(30) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
            params(30).Value = strApprovalNo

            params(31) = New SqlParameter("@RequestTo", SqlDbType.VarChar, 20)
            params(31).Value = oCustomClass.RequestTo

            params(32) = New SqlParameter("@ReasonTypeID", SqlDbType.Char, 5)
            params(32).Value = oCustomClass.ReasonTypeID

            params(33) = New SqlParameter("@ReasonID", SqlDbType.VarChar, 10)
            params(33).Value = oCustomClass.ReasonID

            params(34) = New SqlParameter("@Tr_Nomor", SqlDbType.VarChar, 10)
            params(34).Value = oCustomClass.TR_Nomor

            params(35) = New SqlParameter("@Notes", SqlDbType.Text)
            params(35).Value = oCustomClass.Notes

            params(36) = New SqlParameter("@Status", SqlDbType.Char, 1)
            params(36).Value = oCustomClass.Status

            params(37) = New SqlParameter("@GuarantorID", SqlDbType.Char, 20)
            params(37).Value = oCustomClass.GuarantorID

            params(38) = New SqlParameter("@InterestType", SqlDbType.VarChar, 2)
            params(38).Value = oCustomClass.InterestType

            params(39) = New SqlParameter("@EffectiveRate", SqlDbType.Decimal)
            params(39).Value = oCustomClass.EffectiveRate

            params(40) = New SqlParameter("@OSSTNKRenewalFee", SqlDbType.Decimal)
            params(40).Value = oCustomClass.STNKRenewalFee

            params(41) = New SqlParameter("@LCInstallmentAmountDisc", SqlDbType.Decimal)
            params(41).Value = oCustomClass.LCInstallmentAmountDisc

            params(42) = New SqlParameter("@LCInsuranceAmountDisc", SqlDbType.Decimal)
            params(42).Value = oCustomClass.LCInsuranceAmountDisc

            params(43) = New SqlParameter("@InstallCollectionFeeDisc", SqlDbType.Decimal)
            params(43).Value = oCustomClass.InstallCollectionFeeDisc

            params(44) = New SqlParameter("@InsurCollectionFeeDisc", SqlDbType.Decimal)
            params(44).Value = oCustomClass.InsurCollectionFeeDisc

            params(45) = New SqlParameter("@PDCBounceFeeDisc", SqlDbType.Decimal)
            params(45).Value = oCustomClass.PDCBounceFeeDisc

            params(46) = New SqlParameter("@STNKFeeDisc", SqlDbType.Decimal)
            params(46).Value = oCustomClass.STNKFeeDisc

            params(47) = New SqlParameter("@InsuranceClaimExpenseDisc", SqlDbType.Decimal)
            params(47).Value = oCustomClass.InsuranceClaimExpenseDisc

            params(48) = New SqlParameter("@RepossesFeeDisc", SqlDbType.Decimal)
            params(48).Value = oCustomClass.RepossesFeeDisc

            params(49) = New SqlParameter("@TotalDiscount", SqlDbType.Decimal)
            params(49).Value = oCustomClass.TotalDiscount

            params(50) = New SqlParameter("@TotalAmountToBePaid", SqlDbType.Decimal)
            params(50).Value = oCustomClass.TotalAmountToBePaid

            params(51) = New SqlParameter("@TotalOSAR", SqlDbType.Decimal)
            params(51).Value = oCustomClass.TotalOSAR

            params(52) = New SqlParameter("@StepUpStepDownType", SqlDbType.VarChar, 2)
            params(52).Value = oCustomClass.StepUpStepDownType

            params(53) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            params(53).Direction = ParameterDirection.Output

            params(54) = New SqlParameter("@Penalty", SqlDbType.Decimal)
            params(54).Value = oCustomClass.Penalty

            params(55) = New SqlParameter("@TerminationPenalty", SqlDbType.Decimal)
            params(55).Value = oCustomClass.TerminationPenalty

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave2, params)

            Dim params4() As SqlParameter = New SqlParameter(3) {}
            params4(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params4(0).Value = oCustomClass.BranchId
            params4(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params4(1).Value = oCustomClass.ApplicationID
            params4(2) = New SqlParameter("@ReschSeqNo", SqlDbType.Int)
            params4(2).Value = params(2).Value.ToString '.Value = oCustomClass.SeqNo
            params4(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params4(3).Value = oCustomClass.EffectiveDate
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingBak, params4)

            Dim params2() As SqlParameter = New SqlParameter(7) {}
            If oData1.Rows.Count > 0 Then
                params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params2(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params2(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params2(3) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params2(4) = New SqlParameter("@IsChecked", SqlDbType.Bit)
                params2(5) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params2(6) = New SqlParameter("@Notes", SqlDbType.Text)
                params2(7) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                For intLoop = 0 To oData1.Rows.Count - 1
                    params2(0).Value = oCustomClass.BranchId
                    params2(1).Value = oCustomClass.ApplicationID
                    params2(2).Value = params(2).Value.ToString
                    params2(3).Value = oData1.Rows(intLoop).Item("MasterTCID").ToString
                    params2(4).Value = CBool(oData1.Rows(intLoop).Item("Checked"))
                    params2(5).Value = oCustomClass.BusinessDate
                    params2(6).Value = oData1.Rows(intLoop).Item("Notes").ToString
                    params2(7).Value = params(53).Value.ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave3, params2)

                Next
            End If
            Dim params3() As SqlParameter = New SqlParameter(8) {}
            If oData2.Rows.Count > 0 Then
                params3(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params3(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params3(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params3(3) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params3(4) = New SqlParameter("@MasterTCCLSequenceNo", SqlDbType.Int)
                params3(5) = New SqlParameter("@IsChecked", SqlDbType.Bit)
                params3(6) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params3(7) = New SqlParameter("@Notes", SqlDbType.Text)
                params3(8) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                For intLoop = 0 To oData2.Rows.Count - 1
                    params3(0).Value = oCustomClass.BranchId
                    params3(1).Value = oCustomClass.ApplicationID.Trim
                    params3(2).Value = params(2).Value.ToString
                    params3(3).Value = Trim(oData2.Rows(intLoop).Item("MasterTCID").ToString)
                    params3(4).Value = oData2.Rows(intLoop).Item("MSTCCLSequenceNo")
                    params3(5).Value = CBool(oData2.Rows(intLoop).Item("Checked"))
                    params3(6).Value = oCustomClass.BusinessDate
                    params3(7).Value = Trim(oData2.Rows(intLoop).Item("Notes").ToString)
                    params3(8).Value = params(53).Value.ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave4, params3)
                Next
            End If

            Dim params5() As SqlParameter = New SqlParameter(4) {}
            If oData3.Rows.Count > 0 Then
                params5(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params5(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params5(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params5(3) = New SqlParameter("@CrossDefaultApplicationId", SqlDbType.VarChar, 20)
                params5(4) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                For intLoop = 0 To oData3.Rows.Count - 1
                    params5(0).Value = oCustomClass.BranchId
                    params5(1).Value = oCustomClass.ApplicationID.Trim
                    params5(2).Value = params(2).Value.ToString
                    params5(3).Value = Trim(oData3.Rows(intLoop).Item("CrossDefaultApplicationId").ToString)
                    params5(4).Value = params(53).Value.ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave5, params5)
                Next
            Else
                params5(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params5(0).Value = oCustomClass.BranchId
                params5(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params5(1).Value = oCustomClass.ApplicationID.Trim
                params5(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params5(2).Value = params(2).Value.ToString
                params5(3) = New SqlParameter("@CrossDefaultApplicationId", SqlDbType.VarChar, 20)
                params5(3).Value = oCustomClass.txtSearch
                params5(4) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                params5(4).Value = params(53).Value.ToString
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave5, params5)
            End If


            Dim params1() As SqlParameter = New SqlParameter(12) {}
            Dim objTable As New DataTable
            Dim objRow As DataRow
            Dim inc As Integer
            Dim paramsInsert(4) As SqlParameter
            objTable = oCustomClass.MydataSet.Tables("Installment")
            inc = 1
            For Each objRow In objTable.Rows
                paramsInsert(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
                paramsInsert(0).Value = oCustomClass.BranchId
                paramsInsert(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
                paramsInsert(1).Value = oCustomClass.AppID
                paramsInsert(2) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
                paramsInsert(2).Value = CDbl(objRow("Amount"))
                paramsInsert(3) = New SqlParameter("@InsSeqno", SqlDbType.Int)
                paramsInsert(3).Value = inc
                paramsInsert(4) = New SqlParameter("@Flag", SqlDbType.VarChar, 20)
                paramsInsert(4).Value = oCustomClass.Flag

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFinancialDataSave3", paramsInsert)
                inc = inc + 1
            Next

            params1(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params1(0).Value = oCustomClass.BranchId
            params1(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params1(1).Value = oCustomClass.AppID
            params1(2) = New SqlParameter("@NTF", SqlDbType.Decimal)
            params1(2).Value = oCustomClass.NTF
            params1(3) = New SqlParameter("@effRate", SqlDbType.Decimal)
            params1(3).Value = oCustomClass.EffectiveRate
            params1(4) = New SqlParameter("@SupplierRate", SqlDbType.Decimal)
            params1(4).Value = oCustomClass.SupplierRate
            params1(5) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params1(5).Value = oCustomClass.PaymentFrequency
            params1(6) = New SqlParameter("@InstallmentType", SqlDbType.Char, 2)
            params1(6).Value = oCustomClass.FirstInstallment
            params1(7) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params1(7).Value = oCustomClass.NumofActiveAgreement
            params1(8) = New SqlParameter("@GracePeriod", SqlDbType.TinyInt)
            params1(8).Value = oCustomClass.GracePeriod
            params1(9) = New SqlParameter("@GracePeriodType", SqlDbType.Char, 1)
            params1(9).Value = oCustomClass.GracePeriodType
            params1(10) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params1(10).Value = oCustomClass.BusDate
            params1(11) = New SqlParameter("@DiffRateAmount", SqlDbType.Decimal)
            params1(11).Value = oCustomClass.DiffRate
            params1(12) = New SqlParameter("@ReschSeqNo", SqlDbType.SmallInt)
            params1(12).Value = params(2).Value.ToString

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spSaveReschedulingIRR", params1)

            Dim params6() As SqlParameter = New SqlParameter(3) {}
            params6(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params6(0).Value = oCustomClass.BranchId
            params6(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params6(1).Value = oCustomClass.ApplicationID
            params6(2) = New SqlParameter("@ReschSeqNo", SqlDbType.Int)
            params6(2).Value = params(2).Value.ToString
            params6(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params6(3).Value = oCustomClass.EffectiveDate
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spReschedulingSave6", params6)

            transaction.Commit()
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccMnt.Rescheduling")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "Save Amortisasi Step Up Step Down "
    Public Function SaveReschedulingStepUpStepDown(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable, _
               ByVal oData2 As DataTable, ByVal oData3 As DataTable, ByVal oData4 As DataTable) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim transaction As SqlTransaction
        Dim strApprovalNo As String
        Dim intLoop As Integer
        Dim Temp_PrincipleAmount As Double = 0
        Dim Temp_InterestAmount As Double = 0
        Dim Temp_InstallmentAmount As Double = 0
        Dim Temp_OutstandingPrincipal As Double = 0
        Dim Temp_OutstandingInterest As Double = 0
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            With oEntitiesApproval
                .ApprovalTransaction = transaction
                .BranchId = oCustomClass.BranchId
                .SchemeID = "RESC"
                .RequestDate = oCustomClass.EffectiveDate
                .TransactionNo = oCustomClass.ApplicationID
                .ApprovalValue = oCustomClass.TotalAmountToBePaid
                .UserRequest = oCustomClass.LoginId
                .UserApproval = oCustomClass.RequestTo
                .ApprovalNote = oCustomClass.Notes
                .AprovalType = Parameter.Approval.ETransactionType.Rescheduling_Approval
            End With
            strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

            Dim params() As SqlParameter = New SqlParameter(58) {}
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
            params(2).Direction = ParameterDirection.Output '.Value = oCustomClass.SeqNo 

            params(3) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
            params(3).Value = oCustomClass.CustomerID

            params(4) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
            params(4).Value = oCustomClass.EffectiveDate

            params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(5).Value = oCustomClass.BusinessDate

            params(6) = New SqlParameter("@FlatRate", SqlDbType.BigInt)
            params(6).Value = oCustomClass.FlatRate

            params(7) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params(7).Value = oCustomClass.PaymentFrequency

            params(8) = New SqlParameter("@InstallmentScheme", SqlDbType.Char, 2)
            params(8).Value = oCustomClass.InstallmentScheme

            params(9) = New SqlParameter("@NumOfInstallment", SqlDbType.SmallInt)
            params(9).Value = oCustomClass.NumOfInstallment

            params(10) = New SqlParameter("@OutstandingTenor", SqlDbType.SmallInt)
            params(10).Value = oCustomClass.OutstandingTenor

            params(11) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(11).Value = oCustomClass.Tenor

            params(12) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
            params(12).Value = oCustomClass.InstallmentAmount

            params(13) = New SqlParameter("@PartialPrepaymentAmount", SqlDbType.Decimal)
            params(13).Value = oCustomClass.PartialPrepaymentAmount

            params(14) = New SqlParameter("@AdministrationFee", SqlDbType.BigInt)
            params(14).Value = oCustomClass.AdministrationFee

            params(15) = New SqlParameter("@ContractPrepaidAmount", SqlDbType.Decimal)
            params(15).Value = oCustomClass.ContractPrepaidAmount

            params(16) = New SqlParameter("@OutstandingPrincipalNew", SqlDbType.Decimal)
            params(16).Value = oCustomClass.OutstandingPrincipalNew

            params(17) = New SqlParameter("@OutstandingInterestNew", SqlDbType.Decimal)
            params(17).Value = oCustomClass.OutstandingInterestNew

            params(18) = New SqlParameter("@OutstandingPrincipalOld", SqlDbType.Decimal)
            params(18).Value = oCustomClass.OutstandingPrincipalOld

            params(19) = New SqlParameter("@OutstandingInterestOld", SqlDbType.Decimal)
            params(19).Value = oCustomClass.OutstandingInterestOld

            params(20) = New SqlParameter("@OSInstallmentDue", SqlDbType.Decimal)
            params(20).Value = oCustomClass.InstallmentDue

            params(21) = New SqlParameter("@OSInsuranceDue", SqlDbType.Decimal)
            params(21).Value = oCustomClass.InsuranceDue

            params(22) = New SqlParameter("@OSLCInstallment", SqlDbType.Decimal)
            params(22).Value = oCustomClass.LcInstallment

            params(23) = New SqlParameter("@OSLCInsurance", SqlDbType.Decimal)
            params(23).Value = oCustomClass.LcInsurance

            params(24) = New SqlParameter("@OSInstallCollectionFee", SqlDbType.Decimal)
            params(24).Value = oCustomClass.InstallmentCollFee

            params(25) = New SqlParameter("@OSInsuranceCollectionFee", SqlDbType.Decimal)
            params(25).Value = oCustomClass.InsuranceCollFee

            params(26) = New SqlParameter("@OSPDCBounceFee", SqlDbType.Decimal)
            params(26).Value = oCustomClass.PDCBounceFee

            params(27) = New SqlParameter("@OSInsuranceClaimExpense", SqlDbType.Decimal)
            params(27).Value = oCustomClass.InsuranceClaimExpense

            params(28) = New SqlParameter("@OSCollectionExpense", SqlDbType.Decimal)
            params(28).Value = oCustomClass.CollectionExpense

            params(29) = New SqlParameter("@AccruedAmount", SqlDbType.Decimal)
            params(29).Value = oCustomClass.AccruedInterest

            params(30) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
            params(30).Value = strApprovalNo

            params(31) = New SqlParameter("@RequestTo", SqlDbType.VarChar, 20)
            params(31).Value = oCustomClass.RequestTo

            params(32) = New SqlParameter("@ReasonTypeID", SqlDbType.Char, 5)
            params(32).Value = oCustomClass.ReasonTypeID

            params(33) = New SqlParameter("@ReasonID", SqlDbType.VarChar, 10)
            params(33).Value = oCustomClass.ReasonID

            params(34) = New SqlParameter("@Tr_Nomor", SqlDbType.VarChar, 10)
            params(34).Value = oCustomClass.TR_Nomor

            params(35) = New SqlParameter("@Notes", SqlDbType.Text)
            params(35).Value = oCustomClass.Notes

            params(36) = New SqlParameter("@Status", SqlDbType.Char, 1)
            params(36).Value = oCustomClass.Status

            params(37) = New SqlParameter("@GuarantorID", SqlDbType.Char, 20)
            params(37).Value = oCustomClass.GuarantorID

            params(38) = New SqlParameter("@InterestType", SqlDbType.VarChar, 2)
            params(38).Value = oCustomClass.InterestType

            params(39) = New SqlParameter("@EffectiveRate", SqlDbType.Decimal)
            params(39).Value = oCustomClass.EffectiveRate

            params(40) = New SqlParameter("@OSSTNKRenewalFee", SqlDbType.Decimal)
            params(40).Value = oCustomClass.STNKRenewalFee

            params(41) = New SqlParameter("@LCInstallmentAmountDisc", SqlDbType.Decimal)
            params(41).Value = oCustomClass.LCInstallmentAmountDisc

            params(42) = New SqlParameter("@LCInsuranceAmountDisc", SqlDbType.Decimal)
            params(42).Value = oCustomClass.LCInsuranceAmountDisc

            params(43) = New SqlParameter("@InstallCollectionFeeDisc", SqlDbType.Decimal)
            params(43).Value = oCustomClass.InstallCollectionFeeDisc

            params(44) = New SqlParameter("@InsurCollectionFeeDisc", SqlDbType.Decimal)
            params(44).Value = oCustomClass.InsurCollectionFeeDisc

            params(45) = New SqlParameter("@PDCBounceFeeDisc", SqlDbType.Decimal)
            params(45).Value = oCustomClass.PDCBounceFeeDisc

            params(46) = New SqlParameter("@STNKFeeDisc", SqlDbType.Decimal)
            params(46).Value = oCustomClass.STNKFeeDisc

            params(47) = New SqlParameter("@InsuranceClaimExpenseDisc", SqlDbType.Decimal)
            params(47).Value = oCustomClass.InsuranceClaimExpenseDisc

            params(48) = New SqlParameter("@RepossesFeeDisc", SqlDbType.Decimal)
            params(48).Value = oCustomClass.RepossesFeeDisc

            params(49) = New SqlParameter("@TotalDiscount", SqlDbType.Decimal)
            params(49).Value = oCustomClass.TotalDiscount

            params(50) = New SqlParameter("@TotalAmountToBePaid", SqlDbType.Decimal)
            params(50).Value = oCustomClass.TotalAmountToBePaid

            params(51) = New SqlParameter("@TotalOSAR", SqlDbType.Decimal)
            params(51).Value = oCustomClass.TotalOSAR

            params(52) = New SqlParameter("@StepUpStepDownType", SqlDbType.VarChar, 2)
            params(52).Value = oCustomClass.StepUpStepDownType

            params(53) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            params(53).Direction = ParameterDirection.Output

            params(54) = New SqlParameter("@PerjanjianNo", SqlDbType.VarChar, 50)
            params(54).Value = oCustomClass.PerjanjianNo

            params(55) = New SqlParameter("@PPh", SqlDbType.Decimal)
            params(55).Value = oCustomClass.PPh

            params(56) = New SqlParameter("@InvoiceNo", SqlDbType.Decimal)
            params(56).Value = oCustomClass.InvoiceNo

            params(57) = New SqlParameter("@Penalty", SqlDbType.Decimal)
            params(57).Value = oCustomClass.Penalty

            params(58) = New SqlParameter("@TerminationPenalty", SqlDbType.Decimal)
            params(58).Value = oCustomClass.TerminationPenalty

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave2, params)

            Dim params4() As SqlParameter = New SqlParameter(3) {}
            params4(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params4(0).Value = oCustomClass.BranchId
            params4(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params4(1).Value = oCustomClass.ApplicationID
            params4(2) = New SqlParameter("@ReschSeqNo", SqlDbType.Int)
            params4(2).Value = params(2).Value.ToString '.Value = oCustomClass.SeqNo
            params4(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params4(3).Value = oCustomClass.EffectiveDate
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingBak, params4)

            Dim params2() As SqlParameter = New SqlParameter(7) {}
            If oData1.Rows.Count > 0 Then
                params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params2(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params2(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params2(3) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params2(4) = New SqlParameter("@IsChecked", SqlDbType.Bit)
                params2(5) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params2(6) = New SqlParameter("@Notes", SqlDbType.Text)
                params2(7) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                For intLoop = 0 To oData1.Rows.Count - 1
                    params2(0).Value = oCustomClass.BranchId
                    params2(1).Value = oCustomClass.ApplicationID
                    params2(2).Value = params(2).Value.ToString
                    params2(3).Value = oData1.Rows(intLoop).Item("MasterTCID").ToString
                    params2(4).Value = CBool(oData1.Rows(intLoop).Item("Checked"))
                    params2(5).Value = oCustomClass.BusinessDate
                    params2(6).Value = oData1.Rows(intLoop).Item("Notes").ToString
                    params2(7).Value = params(53).Value.ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave3, params2)

                Next
            End If
            Dim params3() As SqlParameter = New SqlParameter(8) {}
            If oData2.Rows.Count > 0 Then
                params3(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params3(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params3(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params3(3) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params3(4) = New SqlParameter("@MasterTCCLSequenceNo", SqlDbType.Int)
                params3(5) = New SqlParameter("@IsChecked", SqlDbType.Bit)
                params3(6) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params3(7) = New SqlParameter("@Notes", SqlDbType.Text)
                params3(8) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                For intLoop = 0 To oData2.Rows.Count - 1
                    params3(0).Value = oCustomClass.BranchId
                    params3(1).Value = oCustomClass.ApplicationID.Trim
                    params3(2).Value = params(2).Value.ToString
                    params3(3).Value = Trim(oData2.Rows(intLoop).Item("MasterTCID").ToString)
                    params3(4).Value = oData2.Rows(intLoop).Item("MSTCCLSequenceNo")
                    params3(5).Value = CBool(oData2.Rows(intLoop).Item("Checked"))
                    params3(6).Value = oCustomClass.BusinessDate
                    params3(7).Value = Trim(oData2.Rows(intLoop).Item("Notes").ToString)
                    params3(8).Value = params(53).Value.ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave4, params3)
                Next
            End If

            Dim params5() As SqlParameter = New SqlParameter(4) {}
            If oData3.Rows.Count > 0 Then
                params5(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params5(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params5(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params5(3) = New SqlParameter("@CrossDefaultApplicationId", SqlDbType.VarChar, 20)
                params5(4) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                For intLoop = 0 To oData3.Rows.Count - 1
                    params5(0).Value = oCustomClass.BranchId
                    params5(1).Value = oCustomClass.ApplicationID.Trim
                    params5(2).Value = params(2).Value.ToString
                    params5(3).Value = Trim(oData3.Rows(intLoop).Item("CrossDefaultApplicationId").ToString)
                    params5(4).Value = params(53).Value.ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave5, params5)
                Next
            Else
                params5(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params5(0).Value = oCustomClass.BranchId
                params5(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params5(1).Value = oCustomClass.ApplicationID.Trim
                params5(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params5(2).Value = params(2).Value.ToString
                params5(3) = New SqlParameter("@CrossDefaultApplicationId", SqlDbType.VarChar, 20)
                params5(3).Value = oCustomClass.txtSearch
                params5(4) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                params5(4).Value = params(53).Value.ToString
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave5, params5)
            End If


            Dim paramsStep() As SqlParameter = New SqlParameter(9) {}
            If oData4.Rows.Count > 0 Then
                paramsStep(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                paramsStep(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                paramsStep(2) = New SqlParameter("@InsSeqNo", SqlDbType.SmallInt)
                paramsStep(3) = New SqlParameter("@DueDate", SqlDbType.DateTime)
                paramsStep(4) = New SqlParameter("@ReschSeqNo", SqlDbType.SmallInt)
                paramsStep(5) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
                paramsStep(6) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
                paramsStep(7) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
                paramsStep(8) = New SqlParameter("@OutstandingPrincipal", SqlDbType.Decimal)
                paramsStep(9) = New SqlParameter("@OutstandingInterest", SqlDbType.Decimal)

                For intLoop = 0 To oData4.Rows.Count - 1
                    paramsStep(0).Value = oCustomClass.BranchId
                    paramsStep(1).Value = oCustomClass.ApplicationID.Trim
                    paramsStep(2).Value = oData4.Rows(intLoop).Item("Seq")
                    paramsStep(3).Value = oData4.Rows(intLoop).Item("DueDate")
                    paramsStep(4).Value = params(2).Value.ToString
                    paramsStep(5).Value = oData4.Rows(intLoop).Item("InstallmentAmount")
                    paramsStep(6).Value = oData4.Rows(intLoop).Item("PrincipalAmount")
                    paramsStep(7).Value = oData4.Rows(intLoop).Item("InterestAmount")
                    paramsStep(8).Value = oData4.Rows(intLoop).Item("OutStandingPrincipal")
                    paramsStep(9).Value = oData4.Rows(intLoop).Item("OutStandingInterest")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spSaveReschStepUpStepDown", paramsStep)
                Next
            End If

            Dim params6() As SqlParameter = New SqlParameter(3) {}
            params6(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params6(0).Value = oCustomClass.BranchId
            params6(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params6(1).Value = oCustomClass.ApplicationID
            params6(2) = New SqlParameter("@ReschSeqNo", SqlDbType.Int)
            params6(2).Value = params(2).Value.ToString
            params6(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params6(3).Value = oCustomClass.EffectiveDate
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spReschedulingSave6", params6)

            transaction.Commit()
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccMnt.Rescheduling")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "Save Amortisasi Event Principal "

    Public Function SaveReschedulingEP(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable, _
             ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim transaction As SqlTransaction
        Dim strApprovalNo As String
        Dim intLoop As Integer
        Dim Temp_PrincipleAmount As Double = 0
        Dim Temp_InterestAmount As Double = 0
        Dim Temp_InstallmentAmount As Double = 0
        Dim Temp_OutstandingPrincipal As Double = 0
        Dim Temp_OutstandingInterest As Double = 0
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            With oEntitiesApproval
                .ApprovalTransaction = transaction
                .BranchId = oCustomClass.BranchId
                .SchemeID = "RESC"
                .RequestDate = oCustomClass.EffectiveDate
                .TransactionNo = oCustomClass.ApplicationID
                .ApprovalValue = oCustomClass.TotalAmountToBePaid
                .UserRequest = oCustomClass.LoginId
                .UserApproval = oCustomClass.RequestTo
                .ApprovalNote = oCustomClass.Notes
                .AprovalType = Parameter.Approval.ETransactionType.Rescheduling_Approval
            End With
            strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

            Dim params() As SqlParameter = New SqlParameter(55) {}
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter("@SeqNo ", SqlDbType.Int)
            params(2).Direction = ParameterDirection.Output '.Value = oCustomClass.SeqNo

            params(3) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
            params(3).Value = oCustomClass.CustomerID

            params(4) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
            params(4).Value = oCustomClass.EffectiveDate

            params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(5).Value = oCustomClass.BusinessDate

            params(6) = New SqlParameter("@FlatRate", SqlDbType.BigInt)
            params(6).Value = oCustomClass.FlatRate

            params(7) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params(7).Value = oCustomClass.PaymentFrequency

            params(8) = New SqlParameter("@InstallmentScheme", SqlDbType.Char, 2)
            params(8).Value = oCustomClass.InstallmentScheme

            params(9) = New SqlParameter("@NumOfInstallment", SqlDbType.SmallInt)
            params(9).Value = oCustomClass.NumOfInstallment

            params(10) = New SqlParameter("@OutstandingTenor", SqlDbType.SmallInt)
            params(10).Value = oCustomClass.OutstandingTenor

            params(11) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(11).Value = oCustomClass.Tenor

            params(12) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
            params(12).Value = oCustomClass.InstallmentAmount

            params(13) = New SqlParameter("@PartialPrepaymentAmount", SqlDbType.Decimal)
            params(13).Value = oCustomClass.PartialPrepaymentAmount

            params(14) = New SqlParameter("@AdministrationFee", SqlDbType.BigInt)
            params(14).Value = oCustomClass.AdministrationFee

            params(15) = New SqlParameter("@ContractPrepaidAmount", SqlDbType.Decimal)
            params(15).Value = oCustomClass.ContractPrepaidAmount

            params(16) = New SqlParameter("@OutstandingPrincipalNew", SqlDbType.Decimal)
            params(16).Value = oCustomClass.OutstandingPrincipalNew

            params(17) = New SqlParameter("@OutstandingInterestNew", SqlDbType.Decimal)
            params(17).Value = oCustomClass.OutstandingInterestNew

            params(18) = New SqlParameter("@OutstandingPrincipalOld", SqlDbType.Decimal)
            params(18).Value = oCustomClass.OutstandingPrincipalOld

            params(19) = New SqlParameter("@OutstandingInterestOld", SqlDbType.Decimal)
            params(19).Value = oCustomClass.OutstandingInterestOld

            params(20) = New SqlParameter("@OSInstallmentDue", SqlDbType.Decimal)
            params(20).Value = oCustomClass.InstallmentDue

            params(21) = New SqlParameter("@OSInsuranceDue", SqlDbType.Decimal)
            params(21).Value = oCustomClass.InsuranceDue

            params(22) = New SqlParameter("@OSLCInstallment", SqlDbType.Decimal)
            params(22).Value = oCustomClass.LcInstallment

            params(23) = New SqlParameter("@OSLCInsurance", SqlDbType.Decimal)
            params(23).Value = oCustomClass.LcInsurance

            params(24) = New SqlParameter("@OSInstallCollectionFee", SqlDbType.Decimal)
            params(24).Value = oCustomClass.InstallmentCollFee

            params(25) = New SqlParameter("@OSInsuranceCollectionFee", SqlDbType.Decimal)
            params(25).Value = oCustomClass.InsuranceCollFee

            params(26) = New SqlParameter("@OSPDCBounceFee", SqlDbType.Decimal)
            params(26).Value = oCustomClass.PDCBounceFee

            params(27) = New SqlParameter("@OSInsuranceClaimExpense", SqlDbType.Decimal)
            params(27).Value = oCustomClass.InsuranceClaimExpense

            params(28) = New SqlParameter("@OSCollectionExpense", SqlDbType.Decimal)
            params(28).Value = oCustomClass.CollectionExpense

            params(29) = New SqlParameter("@AccruedAmount", SqlDbType.Decimal)
            params(29).Value = oCustomClass.AccruedInterest

            params(30) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
            params(30).Value = strApprovalNo

            params(31) = New SqlParameter("@RequestTo", SqlDbType.VarChar, 20)
            params(31).Value = oCustomClass.RequestTo

            params(32) = New SqlParameter("@ReasonTypeID", SqlDbType.Char, 5)
            params(32).Value = oCustomClass.ReasonTypeID

            params(33) = New SqlParameter("@ReasonID", SqlDbType.VarChar, 10)
            params(33).Value = oCustomClass.ReasonID

            params(34) = New SqlParameter("@Tr_Nomor", SqlDbType.VarChar, 10)
            params(34).Value = oCustomClass.TR_Nomor

            params(35) = New SqlParameter("@Notes", SqlDbType.Text)
            params(35).Value = oCustomClass.Notes

            params(36) = New SqlParameter("@Status", SqlDbType.Char, 1)
            params(36).Value = oCustomClass.Status

            params(37) = New SqlParameter("@GuarantorID", SqlDbType.Char, 20)
            params(37).Value = oCustomClass.GuarantorID

            params(38) = New SqlParameter("@InterestType", SqlDbType.VarChar, 2)
            params(38).Value = oCustomClass.InterestType

            params(39) = New SqlParameter("@EffectiveRate", SqlDbType.Decimal)
            params(39).Value = oCustomClass.EffectiveRate

            params(40) = New SqlParameter("@OSSTNKRenewalFee", SqlDbType.Decimal)
            params(40).Value = oCustomClass.STNKRenewalFee

            params(41) = New SqlParameter("@LCInstallmentAmountDisc", SqlDbType.Decimal)
            params(41).Value = oCustomClass.LCInstallmentAmountDisc

            params(42) = New SqlParameter("@LCInsuranceAmountDisc", SqlDbType.Decimal)
            params(42).Value = oCustomClass.LCInsuranceAmountDisc

            params(43) = New SqlParameter("@InstallCollectionFeeDisc", SqlDbType.Decimal)
            params(43).Value = oCustomClass.InstallCollectionFeeDisc

            params(44) = New SqlParameter("@InsurCollectionFeeDisc", SqlDbType.Decimal)
            params(44).Value = oCustomClass.InsurCollectionFeeDisc

            params(45) = New SqlParameter("@PDCBounceFeeDisc", SqlDbType.Decimal)
            params(45).Value = oCustomClass.PDCBounceFeeDisc

            params(46) = New SqlParameter("@STNKFeeDisc", SqlDbType.Decimal)
            params(46).Value = oCustomClass.STNKFeeDisc

            params(47) = New SqlParameter("@InsuranceClaimExpenseDisc", SqlDbType.Decimal)
            params(47).Value = oCustomClass.InsuranceClaimExpenseDisc

            params(48) = New SqlParameter("@RepossesFeeDisc", SqlDbType.Decimal)
            params(48).Value = oCustomClass.RepossesFeeDisc

            params(49) = New SqlParameter("@TotalDiscount", SqlDbType.Decimal)
            params(49).Value = oCustomClass.TotalDiscount

            params(50) = New SqlParameter("@TotalAmountToBePaid", SqlDbType.Decimal)
            params(50).Value = oCustomClass.TotalAmountToBePaid

            params(51) = New SqlParameter("@TotalOSAR", SqlDbType.Decimal)
            params(51).Value = oCustomClass.TotalOSAR

            params(52) = New SqlParameter("@StepUpStepDownType", SqlDbType.VarChar, 2)
            params(52).Value = oCustomClass.StepUpStepDownType

            params(53) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            params(53).Direction = ParameterDirection.Output

            params(54) = New SqlParameter("@Penalty", SqlDbType.Decimal)
            params(54).Value = oCustomClass.Penalty

            params(55) = New SqlParameter("@TerminationPenalty", SqlDbType.Decimal)
            params(55).Value = oCustomClass.TerminationPenalty


            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave2, params)

            Dim params4() As SqlParameter = New SqlParameter(3) {}
            params4(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params4(0).Value = oCustomClass.BranchId
            params4(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params4(1).Value = oCustomClass.ApplicationID
            params4(2) = New SqlParameter("@ReschSeqNo", SqlDbType.Int)
            params4(2).Value = params(2).Value.ToString '.Value = oCustomClass.SeqNo
            params4(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params4(3).Value = oCustomClass.EffectiveDate
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingBak, params4)

            Dim params2() As SqlParameter = New SqlParameter(7) {}
            If oData1.Rows.Count > 0 Then
                params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params2(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params2(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params2(3) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params2(4) = New SqlParameter("@IsChecked", SqlDbType.Bit)
                params2(5) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params2(6) = New SqlParameter("@Notes", SqlDbType.Text)
                params2(7) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                For intLoop = 0 To oData1.Rows.Count - 1
                    params2(0).Value = oCustomClass.BranchId
                    params2(1).Value = oCustomClass.ApplicationID
                    params2(2).Value = params(2).Value.ToString
                    params2(3).Value = oData1.Rows(intLoop).Item("MasterTCID").ToString
                    params2(4).Value = CBool(oData1.Rows(intLoop).Item("Checked"))
                    params2(5).Value = oCustomClass.BusinessDate
                    params2(6).Value = oData1.Rows(intLoop).Item("Notes").ToString
                    params2(7).Value = params(53).Value.ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave3, params2)

                Next
            End If
            Dim params3() As SqlParameter = New SqlParameter(8) {}
            If oData2.Rows.Count > 0 Then
                params3(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params3(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params3(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params3(3) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params3(4) = New SqlParameter("@MasterTCCLSequenceNo", SqlDbType.Int)
                params3(5) = New SqlParameter("@IsChecked", SqlDbType.Bit)
                params3(6) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params3(7) = New SqlParameter("@Notes", SqlDbType.Text)
                params3(8) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                For intLoop = 0 To oData2.Rows.Count - 1
                    params3(0).Value = oCustomClass.BranchId
                    params3(1).Value = oCustomClass.ApplicationID.Trim
                    params3(2).Value = params(2).Value.ToString
                    params3(3).Value = Trim(oData2.Rows(intLoop).Item("MasterTCID").ToString)
                    params3(4).Value = oData2.Rows(intLoop).Item("MSTCCLSequenceNo")
                    params3(5).Value = CBool(oData2.Rows(intLoop).Item("Checked"))
                    params3(6).Value = oCustomClass.BusinessDate
                    params3(7).Value = Trim(oData2.Rows(intLoop).Item("Notes").ToString)
                    params3(8).Value = params(53).Value.ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave4, params3)
                Next
            End If

            Dim params5() As SqlParameter = New SqlParameter(4) {}
            If oData3.Rows.Count > 0 Then
                params5(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params5(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params5(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params5(3) = New SqlParameter("@CrossDefaultApplicationId", SqlDbType.VarChar, 20)
                params5(4) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                For intLoop = 0 To oData3.Rows.Count - 1
                    params5(0).Value = oCustomClass.BranchId
                    params5(1).Value = oCustomClass.ApplicationID.Trim
                    params5(2).Value = params(2).Value.ToString
                    params5(3).Value = Trim(oData3.Rows(intLoop).Item("CrossDefaultApplicationId").ToString)
                    params5(4).Value = params(53).Value.ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave5, params5)
                Next
            Else
                params5(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params5(0).Value = oCustomClass.BranchId
                params5(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params5(1).Value = oCustomClass.ApplicationID.Trim
                params5(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params5(2).Value = params(2).Value.ToString
                params5(3) = New SqlParameter("@CrossDefaultApplicationId", SqlDbType.VarChar, 20)
                params5(3).Value = oCustomClass.txtSearch
                params5(4) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                params5(4).Value = params(53).Value.ToString
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave5, params5)
            End If

            Dim paramsEP(13) As SqlParameter
            paramsEP(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            paramsEP(0).Value = oCustomClass.BranchId
            paramsEP(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            paramsEP(1).Value = oCustomClass.AppID
            paramsEP(2) = New SqlParameter("@NTF", SqlDbType.Decimal)
            paramsEP(2).Value = oCustomClass.NTF
            paramsEP(3) = New SqlParameter("@effRate", SqlDbType.Decimal)
            paramsEP(3).Value = oCustomClass.EffectiveRate
            paramsEP(4) = New SqlParameter("@SupplierRate", SqlDbType.Decimal)
            paramsEP(4).Value = oCustomClass.SupplierRate
            paramsEP(5) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            paramsEP(5).Value = oCustomClass.PaymentFrequency
            paramsEP(6) = New SqlParameter("@InstallmentType", SqlDbType.Char, 2)
            paramsEP(6).Value = oCustomClass.FirstInstallment
            paramsEP(7) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            paramsEP(7).Value = oCustomClass.NumofActiveAgreement
            paramsEP(8) = New SqlParameter("@GracePeriod", SqlDbType.TinyInt)
            paramsEP(8).Value = oCustomClass.GracePeriod
            paramsEP(9) = New SqlParameter("@GracePeriodType", SqlDbType.Char, 1)
            paramsEP(9).Value = oCustomClass.GracePeriodType
            paramsEP(10) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            paramsEP(10).Value = oCustomClass.BusDate
            paramsEP(11) = New SqlParameter("@DiffRateAmount", SqlDbType.Decimal)
            paramsEP(11).Value = oCustomClass.DiffRate
            paramsEP(12) = New SqlParameter("@ReschSeqNo", SqlDbType.Int)
            paramsEP(12).Value = params(2).Value.ToString
            paramsEP(13) = New SqlParameter("@strerror", SqlDbType.Char, 50)
            paramsEP(13).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spSaveReschedulingEP", paramsEP)

            Dim params6() As SqlParameter = New SqlParameter(3) {}
            params6(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params6(0).Value = oCustomClass.BranchId
            params6(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params6(1).Value = oCustomClass.ApplicationID
            params6(2) = New SqlParameter("@ReschSeqNo", SqlDbType.Int)
            params6(2).Value = params(2).Value.ToString
            params6(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params6(3).Value = oCustomClass.EffectiveDate
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spReschedulingSave6", params6)

            transaction.Commit()
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccMnt.Rescheduling")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "View Rescheduling"
    Public Function GetEffectiveDate(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oCustomClass.BranchId

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.RequestWONo

            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetEffectiveResch", params)
            With oCustomClass
                If objread.Read Then
                    .EffectiveDate = CDate(objread("EffectiveDate"))
                End If
                objread.Close()
            End With
            Return oCustomClass
        Catch exp As Exception
            WriteException("AssetReplacement", "ReportListAgreement", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ViewRescheduling(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID.Trim

            params(2) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.RequestWONo

            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewResch", params)
            With oCustomClass
                If objread.Read Then
                    .BranchId = objread("BranchID").ToString()
                    .ApplicationID = objread("ApplicationID").ToString()
                    .SeqNo = CInt(objread("SeqNo"))
                    .Notes = objread("Notes").ToString
                    .TR_Nomor = objread("Tr_nomor").ToString()
                    .ReasonTypeID = objread("ReasonTypeID").ToString()
                    .ReasonID = objread("ReasonID").ToString()
                    '.AccuredAmount = CDbl(objread("AccuredAmount"))
                    .CollectionExpense = CDbl(objread("RepossesFeeDisc"))
                    .InsuranceClaimExpense = CDbl(objread("InsuranceClaimExpenseDisc"))
                    .PDCBounceFee = CDbl(objread("PDCBounceFeeDisc"))
                    .InsuranceCollFee = CDbl(objread("InsurCollectionFeeDisc"))
                    .InstallmentCollFee = CDbl(objread("InstallCollectionFeeDisc"))
                    .LcInsurance = CDbl(objread("LCInsuranceAmountDisc"))
                    .LcInstallment = CDbl(objread("LCInstallmentAmountDisc"))
                    .InstallmentDue = CDbl(objread("OSInstallmentDue"))
                    .InsuranceDue = CDbl(objread("OSInsuranceDue"))
                    .OutstandingInterestNew = CDbl(objread("OutstandingInterestNew"))
                    .OutstandingPrincipalNew = CDbl(objread("OutstandingPrincipalNew"))
                    .ContractPrepaidAmount = CDbl(objread("ContractPrepaidAmount"))
                    .AdministrationFee = CDbl(objread("AdministrationFee"))
                    .PartialPrepaymentAmount = CDbl(objread("PartialPrepaymentAmount"))
                    .InstallmentAmount = CDbl(objread("InstallmentAmount"))
                    .Tenor = CInt(objread("Tenor"))
                    .NumOfInstallment = CInt(objread("NumOfInstallment"))
                    .PaymentFrequency = objread("PaymentFrequency").ToString()
                    .FlatRate = CDbl(objread("FlatRate"))
                    .EffectiveRate = CDbl(objread("EffectiveRate"))
                    .CustomerID = objread("CustomerID").ToString
                    .RequestTo = objread("RequestBy").ToString

                    .PerjanjianNo = objread("PerjanjianNo").ToString
                    .PPh = CDbl(objread("PPh"))
                    .Penalty = CDbl(objread("Penalty"))
                    .TerminationPenalty = CDbl(objread("TerminationPenalty"))
                End If
                objread.Close()
            End With
            oCustomClass.Data1 = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewReschTC", params).Tables(0)
            oCustomClass.data2 = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewReschTC2", params).Tables(0)
            oCustomClass.data3 = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewReschCD", params).Tables(0)
            Return oCustomClass
        Catch exp As Exception
            WriteException("Rescheduling", "ViewRescheduling", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.ApplicationSaveAdd")
        End Try

    End Function

    Public Function ViewReschedulingApproval(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.RequestWONo

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.BranchId.Replace("'", "")


            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewReschApproval", params)
            With oCustomClass
                If objread.Read Then
                    .BranchId = objread("BranchID").ToString()
                    .ApplicationID = objread("ApplicationID").ToString()
                    .SeqNo = CInt(objread("SeqNo"))
                    .Notes = objread("Notes").ToString
                    .TR_Nomor = objread("Tr_nomor").ToString()
                    .ReasonTypeID = objread("ReasonTypeID").ToString()
                    .ReasonID = objread("ReasonID").ToString()
                    '.AccuredAmount = CDbl(objread("AccuredAmount"))
                    .CollectionExpense = CDbl(objread("RepossesFeeDisc"))
                    .InsuranceClaimExpense = CDbl(objread("InsuranceClaimExpenseDisc"))
                    .PDCBounceFee = CDbl(objread("PDCBounceFeeDisc"))
                    .InsuranceCollFee = CDbl(objread("InsurCollectionFeeDisc"))
                    .InstallmentCollFee = CDbl(objread("InstallCollectionFeeDisc"))
                    .LcInsurance = CDbl(objread("LCInsuranceAmountDisc"))
                    .LcInstallment = CDbl(objread("LCInstallmentAmountDisc"))
                    .InstallmentDue = CDbl(objread("OSInstallmentDue"))
                    .InsuranceDue = CDbl(objread("OSInsuranceDue"))
                    .OutstandingInterestNew = CDbl(objread("OutstandingInterestNew"))
                    .OutstandingPrincipalNew = CDbl(objread("OutstandingPrincipalNew"))
                    .ContractPrepaidAmount = CDbl(objread("ContractPrepaidAmount"))
                    .AdministrationFee = CDbl(objread("AdministrationFee"))
                    .PartialPrepaymentAmount = CDbl(objread("PartialPrepaymentAmount"))
                    .installmentamount = CDbl(objread("InstallmentAmount"))
                    .Tenor = CInt(objread("Tenor"))
                    .NumOfInstallment = CInt(objread("NumOfInstallment"))
                    .PaymentFrequency = objread("PaymentFrequency").ToString()
                    .FlatRate = CDbl(objread("FlatRate"))
                    .EffectiveRate = CDbl(objread("EffectiveRate"))
                    .CustomerID = objread("CustomerID").ToString
                    '.RequestTo = objread("RequestBy").ToString
                    .RequestTo = objread("Name").ToString

                    .PerjanjianNo = objread("PerjanjianNo").ToString
                    .PPh = CDbl(objread("PPh"))
                    .Penalty = CDbl(objread("Penalty"))
                    .TerminationPenalty = CDbl(objread("TerminationPenalty"))
                    .EffectiveDate = objread("EffectiveDate").ToString()


                End If
                objread.Close()
            End With
            'oCustomClass.Data1 = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewReschTC", params).Tables(0)
            'oCustomClass.data2 = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewReschTC2", params).Tables(0)
            'oCustomClass.data3 = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewReschCD", params).Tables(0)
            Return oCustomClass
        Catch exp As Exception
            WriteException("Rescheduling", "ViewReschedulingApproval", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.ApplicationSaveAdd")
        End Try

    End Function

    Public Function ViewReschedulingIRR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim params(12) As SqlParameter
        Dim paramsInsert(4) As SqlParameter
        Dim transaction As SqlTransaction
        Dim objTable As New DataTable
        Dim objRow As DataRow
        Dim inc As Integer

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            objTable = oCustomClass.MydataSet.Tables("Installment")
            inc = 1
            For Each objRow In objTable.Rows
                paramsInsert(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
                paramsInsert(0).Value = oCustomClass.BranchId
                paramsInsert(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
                paramsInsert(1).Value = oCustomClass.AppID
                paramsInsert(2) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
                paramsInsert(2).Value = CDbl(objRow("Amount"))
                paramsInsert(3) = New SqlParameter("@InsSeqno", SqlDbType.Int)
                paramsInsert(3).Value = inc
                paramsInsert(4) = New SqlParameter("@Flag", SqlDbType.VarChar, 20)
                paramsInsert(4).Value = oCustomClass.Flag

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFinancialDataSave3", paramsInsert)
                inc = inc + 1
            Next

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.AppID
            params(2) = New SqlParameter("@NTF", SqlDbType.Decimal)
            params(2).Value = oCustomClass.NTF
            params(3) = New SqlParameter("@effRate", SqlDbType.Decimal)
            params(3).Value = oCustomClass.EffectiveRate
            params(4) = New SqlParameter("@SupplierRate", SqlDbType.Decimal)
            params(4).Value = oCustomClass.SupplierRate
            params(5) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params(5).Value = oCustomClass.PaymentFrequency
            params(6) = New SqlParameter("@InstallmentType", SqlDbType.Char, 2)
            params(6).Value = oCustomClass.FirstInstallment
            params(7) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(7).Value = oCustomClass.Tenor
            params(8) = New SqlParameter("@GracePeriod", SqlDbType.TinyInt)
            params(8).Value = oCustomClass.GracePeriod
            params(9) = New SqlParameter("@GracePeriodType", SqlDbType.Char, 1)
            params(9).Value = oCustomClass.GracePeriodType
            params(10) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(10).Value = oCustomClass.BusDate
            params(11) = New SqlParameter("@DiffRateAmount", SqlDbType.Decimal)
            params(11).Value = oCustomClass.DiffRate
            params(12) = New SqlParameter("@LastInstallment", SqlDbType.Char, 50)
            params(12).Direction = ParameterDirection.Output

            oCustomClass.Data1 = SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, "spViewAmortisationIRR", params).Tables(0)
            transaction.Commit()
            oReturnValue.installmentamount = CDbl(params(12).Value)
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            'WriteException("FinancialData", "SaveAmortisasi", exp.Message + exp.StackTrace)
            oReturnValue.Output = exp.Message
            oReturnValue.installmentamount = 0
            'Dim err As New eloanExceptions
            'err.WriteLog("FinancialData.vb - DA", "SaveFinancialData", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.SaveAmortisasi")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "Rescheduling Cancel "
    Public Sub ReschedulingCancel(ByVal ocustomClass As Parameter.FinancialData)
        Try
            Dim params() As SqlParameter = New SqlParameter(3) {}
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = ocustomClass.BranchId.Replace("'", "")
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = ocustomClass.ApplicationID
            params(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
            params(2).Value = ocustomClass.SeqNo
            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = ocustomClass.BusinessDate
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spReschedulingCancel", params)
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Sub
#End Region

#Region "Rescheduling Execution "
    Public Sub ReschedulingExecute(ByVal ocustomClass As Parameter.FinancialData)
        Try
            Dim params() As SqlParameter = New SqlParameter(4) {}
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = ocustomClass.BranchId.Replace("'", "")
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = ocustomClass.ApplicationID
            params(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
            params(2).Value = ocustomClass.SeqNo
            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = ocustomClass.BusinessDate
            params(4) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
            params(4).Value = ocustomClass.EffectiveDate

            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spReschedulingExecution", params)
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Sub
#End Region

#Region "Print Trial "
    Public Function ReschedulingPrintTrial(ByVal oCustomClass As Parameter.FinancialData) As System.Data.DataSet
        Dim params() As SqlParameter = New SqlParameter(19) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.ApplicationID
        params(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(2).Value = oCustomClass.BusinessDate
        params(3) = New SqlParameter("@InstallLC", SqlDbType.Decimal)
        params(3).Value = oCustomClass.LcInstallment
        params(4) = New SqlParameter("@InstallCF", SqlDbType.Decimal)
        params(4).Value = oCustomClass.InstallmentCollFee
        params(5) = New SqlParameter("@PDCBounceFee", SqlDbType.Decimal)
        params(5).Value = oCustomClass.PDCBounceFee
        params(6) = New SqlParameter("@ReposessionFee", SqlDbType.Decimal)
        params(6).Value = oCustomClass.RepossessionFee
        params(7) = New SqlParameter("@InsuranceLC", SqlDbType.Decimal)
        params(7).Value = oCustomClass.LcInsurance
        params(8) = New SqlParameter("@InsuranceCF", SqlDbType.Decimal)
        params(8).Value = oCustomClass.InsuranceCollFee
        params(9) = New SqlParameter("@STNKRenewal", SqlDbType.Decimal)
        params(9).Value = oCustomClass.STNKRenewalFee
        params(10) = New SqlParameter("@InsuranceClaimExpenxse", SqlDbType.Decimal)
        params(10).Value = oCustomClass.InsuranceClaimExpense
        params(11) = New SqlParameter("@PartialPrepayment", SqlDbType.Decimal)
        params(11).Value = oCustomClass.PartialPrepaymentAmount
        params(12) = New SqlParameter("@AdministrationFee", SqlDbType.Decimal)
        params(12).Value = oCustomClass.AdministrationFee
        params(13) = New SqlParameter("@NewPrincipalAmount", SqlDbType.Decimal)
        params(13).Value = oCustomClass.NewPrincipalAmount
        params(14) = New SqlParameter("@EffectiveRate", SqlDbType.Decimal)
        params(14).Value = oCustomClass.EffectiveRate
        params(15) = New SqlParameter("@NumberOfInstallment", SqlDbType.SmallInt)
        params(15).Value = oCustomClass.NumOfInstallment
        params(16) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
        params(16).Value = oCustomClass.InstallmentAmount
        params(17) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 50)
        params(17).Value = oCustomClass.PaymentFrequency
        params(18) = New SqlParameter("@FlatRate", SqlDbType.Decimal)
        params(18).Value = oCustomClass.FlatRate
        params(19) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
        params(19).Value = oCustomClass.Tenor

        Try
            Return SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spReschedulingPrintTrial", params)
        Catch exp As Exception
            WriteException("FullPrepayView", "PrintTrial", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region

#Region "View Product "
    Public Function ReschedulingProduct(ByVal strConnection As String, ByVal BranchId As String) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = BranchId

            Return SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, "spProduct", params).Tables(0)
        Catch exp As Exception
            WriteException("Rescheduling", "ReschedulingProduct", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region

#Region "Save Amortisasi Step Up Step Down Fact And MDKJ"
    Public Function SaveReschedulingStepUpStepDownFactAndMDKJ(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable,
               ByVal oData2 As DataTable, ByVal oData3 As DataTable, ByVal oData4 As DataTable) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim transaction As SqlTransaction
        Dim strApprovalNo As String
        Dim intLoop As Integer
        Dim Temp_PrincipleAmount As Double = 0
        Dim Temp_InterestAmount As Double = 0
        Dim Temp_InstallmentAmount As Double = 0
        Dim Temp_OutstandingPrincipal As Double = 0
        Dim Temp_OutstandingInterest As Double = 0
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            With oEntitiesApproval
                .ApprovalTransaction = transaction
                .BranchId = oCustomClass.BranchId
                .SchemeID = "RSFM"
                .RequestDate = oCustomClass.EffectiveDate
                .TransactionNo = oCustomClass.ApplicationID
                .ApprovalValue = oCustomClass.TotalAmountToBePaid
                .UserRequest = oCustomClass.LoginId
                .UserApproval = oCustomClass.RequestTo
                .ApprovalNote = oCustomClass.Notes
                .AprovalType = Parameter.Approval.ETransactionType.Rescheduling_Approval
            End With
            strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

            Dim params() As SqlParameter = New SqlParameter(63) {}
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter("@SeqNo ", SqlDbType.Int)
            params(2).Direction = ParameterDirection.Output '.Value = oCustomClass.SeqNo

            params(3) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
            params(3).Value = oCustomClass.CustomerID

            params(4) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
            params(4).Value = oCustomClass.EffectiveDate

            params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(5).Value = oCustomClass.BusinessDate

            params(6) = New SqlParameter("@FlatRate", SqlDbType.BigInt)
            params(6).Value = oCustomClass.FlatRate

            params(7) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params(7).Value = oCustomClass.PaymentFrequency

            params(8) = New SqlParameter("@InstallmentScheme", SqlDbType.Char, 2)
            params(8).Value = oCustomClass.InstallmentScheme

            params(9) = New SqlParameter("@NumOfInstallment", SqlDbType.SmallInt)
            params(9).Value = oCustomClass.NumOfInstallment

            params(10) = New SqlParameter("@OutstandingTenor", SqlDbType.SmallInt)
            params(10).Value = oCustomClass.OutstandingTenor

            params(11) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(11).Value = oCustomClass.Tenor

            params(12) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
            params(12).Value = oCustomClass.installmentamount

            params(13) = New SqlParameter("@PartialPrepaymentAmount", SqlDbType.Decimal)
            params(13).Value = oCustomClass.PartialPrepaymentAmount

            params(14) = New SqlParameter("@AdministrationFee", SqlDbType.BigInt)
            params(14).Value = oCustomClass.AdministrationFee

            params(15) = New SqlParameter("@ContractPrepaidAmount", SqlDbType.Decimal)
            params(15).Value = oCustomClass.ContractPrepaidAmount

            params(16) = New SqlParameter("@OutstandingPrincipalNew", SqlDbType.Decimal)
            params(16).Value = oCustomClass.OutstandingPrincipalNew

            params(17) = New SqlParameter("@OutstandingInterestNew", SqlDbType.Decimal)
            params(17).Value = oCustomClass.OutstandingInterestNew

            params(18) = New SqlParameter("@OutstandingPrincipalOld", SqlDbType.Decimal)
            params(18).Value = oCustomClass.OutstandingPrincipalOld

            params(19) = New SqlParameter("@OutstandingInterestOld", SqlDbType.Decimal)
            params(19).Value = oCustomClass.OutstandingInterestOld

            params(20) = New SqlParameter("@OSInstallmentDue", SqlDbType.Decimal)
            params(20).Value = oCustomClass.InstallmentDue

            params(21) = New SqlParameter("@OSInsuranceDue", SqlDbType.Decimal)
            params(21).Value = oCustomClass.InsuranceDue

            params(22) = New SqlParameter("@OSLCInstallment", SqlDbType.Decimal)
            params(22).Value = oCustomClass.LcInstallment

            params(23) = New SqlParameter("@OSLCInsurance", SqlDbType.Decimal)
            params(23).Value = oCustomClass.LcInsurance

            params(24) = New SqlParameter("@OSInstallCollectionFee", SqlDbType.Decimal)
            params(24).Value = oCustomClass.InstallmentCollFee

            params(25) = New SqlParameter("@OSInsuranceCollectionFee", SqlDbType.Decimal)
            params(25).Value = oCustomClass.InsuranceCollFee

            params(26) = New SqlParameter("@OSPDCBounceFee", SqlDbType.Decimal)
            params(26).Value = oCustomClass.PDCBounceFee

            params(27) = New SqlParameter("@OSInsuranceClaimExpense", SqlDbType.Decimal)
            params(27).Value = oCustomClass.InsuranceClaimExpense

            params(28) = New SqlParameter("@OSCollectionExpense", SqlDbType.Decimal)
            params(28).Value = oCustomClass.CollectionExpense

            params(29) = New SqlParameter("@AccruedAmount", SqlDbType.Decimal)
            params(29).Value = oCustomClass.AccruedInterest

            params(30) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
            params(30).Value = strApprovalNo

            params(31) = New SqlParameter("@RequestTo", SqlDbType.VarChar, 20)
            params(31).Value = oCustomClass.RequestTo

            params(32) = New SqlParameter("@ReasonTypeID", SqlDbType.Char, 5)
            params(32).Value = oCustomClass.ReasonTypeID

            params(33) = New SqlParameter("@ReasonID", SqlDbType.VarChar, 10)
            params(33).Value = oCustomClass.ReasonID

            params(34) = New SqlParameter("@Tr_Nomor", SqlDbType.VarChar, 10)
            params(34).Value = oCustomClass.TR_Nomor

            params(35) = New SqlParameter("@Notes", SqlDbType.Text)
            params(35).Value = oCustomClass.Notes

            params(36) = New SqlParameter("@Status", SqlDbType.Char, 1)
            params(36).Value = oCustomClass.Status

            params(37) = New SqlParameter("@GuarantorID", SqlDbType.Char, 20)
            params(37).Value = oCustomClass.GuarantorID

            params(38) = New SqlParameter("@InterestType", SqlDbType.VarChar, 2)
            params(38).Value = oCustomClass.InterestType

            params(39) = New SqlParameter("@EffectiveRate", SqlDbType.Decimal)
            params(39).Value = oCustomClass.EffectiveRate

            params(40) = New SqlParameter("@OSSTNKRenewalFee", SqlDbType.Decimal)
            params(40).Value = oCustomClass.STNKRenewalFee

            params(41) = New SqlParameter("@LCInstallmentAmountDisc", SqlDbType.Decimal)
            params(41).Value = oCustomClass.LCInstallmentAmountDisc

            params(42) = New SqlParameter("@LCInsuranceAmountDisc", SqlDbType.Decimal)
            params(42).Value = oCustomClass.LCInsuranceAmountDisc

            params(43) = New SqlParameter("@InstallCollectionFeeDisc", SqlDbType.Decimal)
            params(43).Value = oCustomClass.InstallCollectionFeeDisc

            params(44) = New SqlParameter("@InsurCollectionFeeDisc", SqlDbType.Decimal)
            params(44).Value = oCustomClass.InsurCollectionFeeDisc

            params(45) = New SqlParameter("@PDCBounceFeeDisc", SqlDbType.Decimal)
            params(45).Value = oCustomClass.PDCBounceFeeDisc

            params(46) = New SqlParameter("@STNKFeeDisc", SqlDbType.Decimal)
            params(46).Value = oCustomClass.STNKFeeDisc

            params(47) = New SqlParameter("@InsuranceClaimExpenseDisc", SqlDbType.Decimal)
            params(47).Value = oCustomClass.InsuranceClaimExpenseDisc

            params(48) = New SqlParameter("@RepossesFeeDisc", SqlDbType.Decimal)
            params(48).Value = oCustomClass.RepossesFeeDisc

            params(49) = New SqlParameter("@TotalDiscount", SqlDbType.Decimal)
            params(49).Value = oCustomClass.TotalDiscount

            params(50) = New SqlParameter("@TotalAmountToBePaid", SqlDbType.Decimal)
            params(50).Value = oCustomClass.TotalAmountToBePaid

            params(51) = New SqlParameter("@TotalOSAR", SqlDbType.Decimal)
            params(51).Value = oCustomClass.TotalOSAR

            params(52) = New SqlParameter("@StepUpStepDownType", SqlDbType.VarChar, 2)
            params(52).Value = oCustomClass.StepUpStepDownType

            params(53) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            params(53).Direction = ParameterDirection.Output

            params(54) = New SqlParameter("@chkPotong", SqlDbType.VarChar, 5)
            params(54).Value = oCustomClass.chkPotong

            params(55) = New SqlParameter("@AccruedInterestDisc", SqlDbType.Decimal)
            params(55).Value = oCustomClass.AccruedInterestDisc

            params(56) = New SqlParameter("@InstallmentDueDisc", SqlDbType.Decimal)
            params(56).Value = oCustomClass.InstallmentDueDisc

            params(57) = New SqlParameter("@OutstandingInterestDisc", SqlDbType.Decimal)
            params(57).Value = oCustomClass.OutstandingInterestDisc

            params(58) = New SqlParameter("@OutstandingPrincipalDisc", SqlDbType.Decimal)
            params(58).Value = oCustomClass.OutstandingPrincipalDisc

            params(59) = New SqlParameter("@Flag", SqlDbType.VarChar, 5)
            params(59).Value = oCustomClass.Flag

            params(60) = New SqlParameter("@PerjanjianNo", SqlDbType.VarChar, 50)
            params(60).Value = oCustomClass.PerjanjianNo

            params(61) = New SqlParameter("@PPh", SqlDbType.Decimal)
            params(61).Value = oCustomClass.PPh

            params(62) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
            params(62).Value = oCustomClass.InvoiceNo

            params(63) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(63).Value = oCustomClass.InvoiceSeqNo


            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave2, params)

            'Dim params4() As SqlParameter = New SqlParameter(3) {}
            'params4(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            'params4(0).Value = oCustomClass.BranchId
            'params4(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            'params4(1).Value = oCustomClass.ApplicationID
            'params4(2) = New SqlParameter("@ReschSeqNo", SqlDbType.Int)
            'params4(2).Value = params(2).Value.ToString
            'params4(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            'params4(3).Value = oCustomClass.EffectiveDate
            'SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingBak, params4) '

            'Dim params2() As SqlParameter = New SqlParameter(7) {}
            'If oData1.Rows.Count > 0 Then
            '    params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            '    params2(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            '    params2(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
            '    params2(3) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
            '    params2(4) = New SqlParameter("@IsChecked", SqlDbType.Bit)
            '    params2(5) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
            '    params2(6) = New SqlParameter("@Notes", SqlDbType.Text)
            '    params2(7) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            '    For intLoop = 0 To oData1.Rows.Count - 1
            '        params2(0).Value = oCustomClass.BranchId
            '        params2(1).Value = oCustomClass.ApplicationID
            '        params2(2).Value = params(2).Value.ToString
            '        params2(3).Value = oData1.Rows(intLoop).Item("MasterTCID").ToString
            '        params2(4).Value = CBool(oData1.Rows(intLoop).Item("Checked"))
            '        params2(5).Value = oCustomClass.BusinessDate
            '        params2(6).Value = oData1.Rows(intLoop).Item("Notes").ToString
            '        params2(7).Value = params(53).Value.ToString
            '        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave3, params2) '

            '    Next
            'End If
            'Dim params3() As SqlParameter = New SqlParameter(8) {}
            'If oData2.Rows.Count > 0 Then
            '    params3(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            '    params3(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            '    params3(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
            '    params3(3) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
            '    params3(4) = New SqlParameter("@MasterTCCLSequenceNo", SqlDbType.Int)
            '    params3(5) = New SqlParameter("@IsChecked", SqlDbType.Bit)
            '    params3(6) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
            '    params3(7) = New SqlParameter("@Notes", SqlDbType.Text)
            '    params3(8) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            '    For intLoop = 0 To oData2.Rows.Count - 1
            '        params3(0).Value = oCustomClass.BranchId
            '        params3(1).Value = oCustomClass.ApplicationID.Trim
            '        params3(2).Value = params(2).Value.ToString
            '        params3(3).Value = Trim(oData2.Rows(intLoop).Item("MasterTCID").ToString)
            '        params3(4).Value = oData2.Rows(intLoop).Item("MSTCCLSequenceNo")
            '        params3(5).Value = CBool(oData2.Rows(intLoop).Item("Checked"))
            '        params3(6).Value = oCustomClass.BusinessDate
            '        params3(7).Value = Trim(oData2.Rows(intLoop).Item("Notes").ToString)
            '        params3(8).Value = params(53).Value.ToString
            '        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave4, params3) '
            '    Next
            'End If

            Dim params5() As SqlParameter = New SqlParameter(4) {}
            If oData3.Rows.Count > 0 Then
                params5(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params5(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params5(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params5(3) = New SqlParameter("@CrossDefaultApplicationId", SqlDbType.VarChar, 20)
                params5(4) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                For intLoop = 0 To oData3.Rows.Count - 1
                    params5(0).Value = oCustomClass.BranchId
                    params5(1).Value = oCustomClass.ApplicationID.Trim
                    params5(2).Value = params(2).Value.ToString
                    params5(3).Value = Trim(oData3.Rows(intLoop).Item("CrossDefaultApplicationId").ToString)
                    params5(4).Value = params(53).Value.ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave5, params5) '
                Next
            Else
                params5(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params5(0).Value = oCustomClass.BranchId
                params5(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params5(1).Value = oCustomClass.ApplicationID.Trim
                params5(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params5(2).Value = params(2).Value.ToString
                params5(3) = New SqlParameter("@CrossDefaultApplicationId", SqlDbType.VarChar, 20)
                params5(3).Value = oCustomClass.txtSearch
                params5(4) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
                params5(4).Value = params(53).Value.ToString
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPReschedulingSave5, params5) '
            End If


            'Dim paramsStep() As SqlParameter = New SqlParameter(9) {}
            'If oData4.Rows.Count > 0 Then
            '    paramsStep(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            '    paramsStep(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            '    paramsStep(2) = New SqlParameter("@InsSeqNo", SqlDbType.SmallInt)
            '    paramsStep(3) = New SqlParameter("@DueDate", SqlDbType.DateTime)
            '    paramsStep(4) = New SqlParameter("@ReschSeqNo", SqlDbType.SmallInt)
            '    paramsStep(5) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
            '    paramsStep(6) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
            '    paramsStep(7) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
            '    paramsStep(8) = New SqlParameter("@OutstandingPrincipal", SqlDbType.Decimal)
            '    paramsStep(9) = New SqlParameter("@OutstandingInterest", SqlDbType.Decimal)

            '    For intLoop = 0 To oData4.Rows.Count - 1
            '        paramsStep(0).Value = oCustomClass.BranchId
            '        paramsStep(1).Value = oCustomClass.ApplicationID.Trim
            '        paramsStep(2).Value = oData4.Rows(intLoop).Item("Seq")
            '        paramsStep(3).Value = oData4.Rows(intLoop).Item("DueDate")
            '        paramsStep(4).Value = params(2).Value.ToString
            '        paramsStep(5).Value = oData4.Rows(intLoop).Item("InstallmentAmount")
            '        paramsStep(6).Value = oData4.Rows(intLoop).Item("PrincipalAmount")
            '        paramsStep(7).Value = oData4.Rows(intLoop).Item("InterestAmount")
            '        paramsStep(8).Value = oData4.Rows(intLoop).Item("OutStandingPrincipal")
            '        paramsStep(9).Value = oData4.Rows(intLoop).Item("OutStandingInterest")
            '        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spSaveReschStepUpStepDown", paramsStep) '
            '    Next
            'End If

            'Dim params6() As SqlParameter = New SqlParameter(3) {}
            'params6(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            'params6(0).Value = oCustomClass.BranchId
            'params6(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            'params6(1).Value = oCustomClass.ApplicationID
            'params6(2) = New SqlParameter("@ReschSeqNo", SqlDbType.Int)
            'params6(2).Value = params(2).Value.ToString
            'params6(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            'params6(3).Value = oCustomClass.EffectiveDate
            'SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spReschedulingSave6", params6) '

            transaction.Commit()
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccMnt.ReschedulingFActAndMDKJ")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region


    Public Function ViewRestrcutFactAndMU(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID.Trim

            params(2) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.RequestWONo

            params(3) = New SqlParameter("@StatusView", SqlDbType.VarChar, 20)
            params(3).Value = oCustomClass.StatusView

            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewReschFactAndMU", params)
            With oCustomClass
                If objread.Read Then
                    .BranchId = objread("BranchID").ToString()
                    .ApplicationID = objread("ApplicationID").ToString()
                    .SeqNo = CInt(objread("SeqNo"))
                    .Notes = objread("Notes").ToString
                    .TR_Nomor = objread("Tr_nomor").ToString()
                    .ReasonTypeID = objread("ReasonTypeID").ToString()
                    .ReasonID = objread("ReasonID").ToString()
                    '.AccuredAmount = CDbl(objread("AccuredAmount"))
                    .CollectionExpense = CDbl(objread("RepossesFeeDisc"))
                    .InsuranceClaimExpense = CDbl(objread("InsuranceClaimExpenseDisc"))
                    .PDCBounceFee = CDbl(objread("PDCBounceFeeDisc"))
                    .InsuranceCollFee = CDbl(objread("InsurCollectionFeeDisc"))
                    .InstallmentCollFee = CDbl(objread("InstallCollectionFeeDisc"))
                    .LcInsurance = CDbl(objread("LCInsuranceAmountDisc"))
                    .LcInstallment = CDbl(objread("LCInstallmentAmountDisc"))
                    .InstallmentDue = CDbl(objread("OSInstallmentDue"))
                    .InsuranceDue = CDbl(objread("OSInsuranceDue"))
                    .OutstandingInterestNew = CDbl(objread("OutstandingInterestNew"))
                    .OutstandingPrincipalNew = CDbl(objread("OutstandingPrincipalNew"))
                    .ContractPrepaidAmount = CDbl(objread("ContractPrepaidAmount"))
                    .AdministrationFee = CDbl(objread("AdministrationFee"))
                    .PartialPrepaymentAmount = CDbl(objread("PartialPrepaymentAmount"))
                    .installmentamount = CDbl(objread("InstallmentAmount"))
                    .Tenor = CInt(objread("Tenor"))
                    .NumOfInstallment = CInt(objread("NumOfInstallment"))
                    .PaymentFrequency = objread("PaymentFrequency").ToString()
                    .FlatRate = CDbl(objread("FlatRate"))
                    .EffectiveRate = CDbl(objread("EffectiveRate"))
                    .CustomerID = objread("CustomerID").ToString
                    .RequestTo = objread("RequestBy").ToString
                    .TotalDiscount = CDbl(objread("TotalDiscount"))
                    .AccruedInterestDisc = CDbl(objread("AccruedInterestDisc"))
                    .InstallmentDueDisc = CDbl(objread("InstallmentDueDisc"))
                    .OutstandingPrincipalDisc = CDbl(objread("OutstandingPrincipalDisc"))
                    .OutstandingInterestDisc = CDbl(objread("OutstandingInterestDisc"))
                    .LCInstallmentAmountDisc = CDbl(objread("LCInstallmentAmountDisc"))
                    .chkPotong = objread("IsIncludePrincipal").ToString()
                    .PPh = CDbl(objread("PPh"))
                    .PerjanjianNo = objread("PerjanjianNo").ToString()
                    .Penalty = CDbl(objread("Penalty"))
                    .TerminationPenalty = CDbl(objread("TerminationPenalty"))
                    .EffectiveDate = objread("EffectiveDate").ToString()
                End If
                objread.Close()
            End With
            'oCustomClass.Data1 = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewReschTC", params).Tables(0)
            'oCustomClass.data2 = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewReschTC2", params).Tables(0)
            'oCustomClass.data3 = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewReschCD", params).Tables(0)
            Return oCustomClass
        Catch exp As Exception
            WriteException("Rescheduling", "ViewRestrcutFactAndMU", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.ApplicationSaveAdd")
        End Try

    End Function

    Public Function ViewApprovalRestrcutFactAndMU(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try

            params(0) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.RequestWONo

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.BranchId.Replace("'", "")

            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewApprovalReschFactAndMU", params)
            With oCustomClass
                If objread.Read Then
                    .BranchId = objread("BranchID").ToString()
                    .ApplicationID = objread("ApplicationID").ToString()
                    .SeqNo = CInt(objread("SeqNo"))
                    .Notes = objread("Notes").ToString
                    .TR_Nomor = objread("Tr_nomor").ToString()
                    .ReasonTypeID = objread("ReasonTypeID").ToString()
                    .ReasonID = objread("ReasonID").ToString()
                    '.AccuredAmount = CDbl(objread("AccuredAmount"))
                    .CollectionExpense = CDbl(objread("RepossesFeeDisc"))
                    .InsuranceClaimExpense = CDbl(objread("InsuranceClaimExpenseDisc"))
                    .PDCBounceFee = CDbl(objread("PDCBounceFeeDisc"))
                    .InsuranceCollFee = CDbl(objread("InsurCollectionFeeDisc"))
                    .InstallmentCollFee = CDbl(objread("InstallCollectionFeeDisc"))
                    .LcInsurance = CDbl(objread("LCInsuranceAmountDisc"))
                    .LcInstallment = CDbl(objread("LCInstallmentAmountDisc"))
                    .InstallmentDue = CDbl(objread("OSInstallmentDue"))
                    .InsuranceDue = CDbl(objread("OSInsuranceDue"))
                    .OutstandingInterestNew = CDbl(objread("OutstandingInterestNew"))
                    .OutstandingPrincipalNew = CDbl(objread("OutstandingPrincipalNew"))
                    .ContractPrepaidAmount = CDbl(objread("ContractPrepaidAmount"))
                    .AdministrationFee = CDbl(objread("AdministrationFee"))
                    .PartialPrepaymentAmount = CDbl(objread("PartialPrepaymentAmount"))
                    .installmentamount = CDbl(objread("InstallmentAmount"))
                    .Tenor = CInt(objread("Tenor"))
                    .NumOfInstallment = CInt(objread("NumOfInstallment"))
                    .PaymentFrequency = objread("PaymentFrequency").ToString()
                    .FlatRate = CDbl(objread("FlatRate"))
                    .EffectiveRate = CDbl(objread("EffectiveRate"))
                    .CustomerID = objread("CustomerID").ToString
                    .RequestTo = objread("RequestBy").ToString
                    .TotalDiscount = CDbl(objread("TotalDiscount"))
                    .AccruedInterestDisc = CDbl(objread("AccruedInterestDisc"))
                    .InstallmentDueDisc = CDbl(objread("InstallmentDueDisc"))
                    .OutstandingPrincipalDisc = CDbl(objread("OutstandingPrincipalDisc"))
                    .OutstandingInterestDisc = CDbl(objread("OutstandingInterestDisc"))
                    .LCInstallmentAmountDisc = CDbl(objread("LCInstallmentAmountDisc"))
                    .chkPotong = objread("IsIncludePrincipal").ToString()
                    .PPh = CDbl(objread("PPh"))
                    .PerjanjianNo = objread("PerjanjianNo").ToString()
                    .Penalty = CDbl(objread("Penalty"))
                    .TerminationPenalty = CDbl(objread("TerminationPenalty"))
                    .EffectiveDate = objread("EffectiveDate").ToString()
                End If
                objread.Close()
            End With
            'oCustomClass.Data1 = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewReschTC", params).Tables(0)
            'oCustomClass.data2 = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewReschTC2", params).Tables(0)
            'oCustomClass.data3 = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewReschCD", params).Tables(0)
            Return oCustomClass
        Catch exp As Exception
            WriteException("Rescheduling", "ViewApprovalRestrcutFactAndMU", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.ApplicationSaveAdd")
        End Try

    End Function

#Region "Rescheduling Execution Fact And MDKJ "
    Public Sub RestructFactAndMUExecute(ByVal ocustomClass As Parameter.FinancialData)
        Try
            Dim params() As SqlParameter = New SqlParameter(6) {}
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = ocustomClass.BranchId.Replace("'", "")
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = ocustomClass.ApplicationID
            params(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
            params(2).Value = ocustomClass.SeqNo
            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = ocustomClass.BusinessDate
            params(4) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
            params(4).Value = ocustomClass.EffectiveDate
            params(5) = New SqlParameter("@InvoiceNo", SqlDbType.Char, 50)
            params(5).Value = ocustomClass.InvoiceNo
            params(6) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(6).Value = ocustomClass.InvoiceSeqNo

            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spRestructFactAndMUExecution", params)
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Sub
#End Region

    Public Function GetMinDueDateFactAndMDKJ(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oCustomClass.BranchId

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(2).Value = oCustomClass.BusinessDate

            params(3) = New SqlParameter("@InvoiceNo", SqlDbType.Char, 50)
            params(3).Value = oCustomClass.InvoiceNo

            params(4) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(4).Value = oCustomClass.InvoiceSeqNo

            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetMinDueDateFactAndMDKJ", params)
            With oCustomClass
                If objread.Read Then
                    .SeqNo = CInt(objread("InsSeqNo"))
                    .DueDate = CDate(objread("DueDate"))
                    .Tenor = CInt(objread("Tenor"))
                    .MaxSeqNo = CInt(objread("MaxSeqNo"))
                    .NewNumInst = CInt(objread("NewNumInst"))
                End If
                objread.Close()
            End With
            Return oCustomClass
        Catch exp As Exception
            WriteException("AssetReplacement", "ReportListAgreement", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub ReschedulingCancelFactAndMDKJ(ByVal ocustomClass As Parameter.FinancialData)
        Try
            Dim params() As SqlParameter = New SqlParameter(5) {}
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = ocustomClass.BranchId.Replace("'", "")
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = ocustomClass.ApplicationID
            params(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
            params(2).Value = ocustomClass.SeqNo
            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = ocustomClass.BusinessDate
            params(4) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
            params(4).Value = ocustomClass.InvoiceNo
            params(5) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(5).Value = ocustomClass.InvoiceSeqNo
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spReschedulingCancelFactAndMDKJ", params) ' //spReschedulingCancel
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Sub

End Class
