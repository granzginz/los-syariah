

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region
Public Class AssetReplacement : Inherits AccMntBase

    Public Function AssetReplacementPaging(ByVal customClass As Parameter.AssetReplacement) As Parameter.AssetReplacement

        Dim oReturnValue As New Parameter.AssetReplacement
        Dim spName As String

        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As MaxiloanExceptions
            Dim err As New MaxiloanExceptions
            err.WriteLog("AssetReplacement", "AssetReplacementPaging", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            WriteException("AssetReplacement", "AssetReplacementPaging", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetAssetRepSerial(ByVal oCustomClass As Parameter.AssetReplacement) As Parameter.AssetReplacement
        Dim oReturnValue As New Parameter.AssetReplacement
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@AssetID", SqlDbType.Char, 10)
            params(0).Value = oCustomClass.AssetID

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = oCustomClass.ApplicationID


            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spAssetReplacemeDataSerial", params)
            With oCustomClass
                If objread.Read Then
                    .AssetTypeID = CType(objread("AssetTypeID"), String)
                    .BranchId = CType(objread("BranchId"), String)
                    .AppID = CType(objread("ApplicationID"), String)
                    .Agreementno = CType(objread("agreementNo"), String)
                    .CustomerID = CType(objread("CustomerID"), String)
                    .ProductID = CType(objread("ProductID"), String)
                    .SupplierID = CType(objread("SupplierID"), String)
                    .Serial1 = CType(objread("SerialNo1"), String)
                    .Serial2 = CType(objread("SerialNo2"), String)
                End If
                objread.Close()
            End With
            Return oCustomClass
        Catch exp As Exception
            WriteException("AssetReplacement", "ReportListAgreement", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Sub AssetReplacementSaveAdd(ByVal oCustomClass As Parameter.AssetReplacement, _
           ByVal oAddress As Parameter.Address, ByVal oData1 As DataTable, _
           ByVal oData2 As DataTable)
        Dim oReturnValue As New Parameter.AssetReplacement
        Dim m_connection As New SqlConnection(oCustomClass.strConnection)
        Dim oApprovalID As New maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim transaction As SqlTransaction
        Dim ErrMessage As String = ""
        Dim intLoop As Integer
        Dim params() As SqlParameter = New SqlParameter(40) {}
        Dim params1() As SqlParameter = New SqlParameter(5) {}
        Dim params2() As SqlParameter = New SqlParameter(8) {}
        Dim strApprovalNo As String
        Try
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            transaction = m_connection.BeginTransaction
            With oEntitiesApproval
                .ApprovalTransaction = transaction
                .BranchId = oCustomClass.BranchId
                .SchemeID = "ASRP"
                .RequestDate = oCustomClass.requestdate
                .TransactionNo = oCustomClass.ApplicationID
                .ApprovalValue = oCustomClass.AssetReplacementFee
                .UserRequest = oCustomClass.LoginId
                .UserApproval = oCustomClass.approvalby
                .ApprovalNote = oCustomClass.Notes
                .AprovalType = Parameter.Approval.ETransactionType.AssetReplacement_Approval
            End With
            strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = oCustomClass.ApplicationID
            params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Decimal)
            params(2).Value = oCustomClass.assetseqNo
            params(3) = New SqlParameter("@RequestDate", SqlDbType.DateTime)
            params(3).Value = oCustomClass.requestdate
            params(4) = New SqlParameter("@Notes", SqlDbType.Text)
            params(4).Value = oCustomClass.Notes
            params(5) = New SqlParameter("@ReasonTypeID", SqlDbType.VarChar, 5)
            params(5).Value = oCustomClass.ReasonTypeID
            params(6) = New SqlParameter("@ReasonID", SqlDbType.VarChar, 10)
            params(6).Value = oCustomClass.ReasonID
            params(7) = New SqlParameter("@AssetCode", SqlDbType.VarChar, 50)
            params(7).Value = oCustomClass.AssetCode
            params(8) = New SqlParameter("@SerialNo1", SqlDbType.VarChar, 50)
            params(8).Value = oCustomClass.Serial1
            params(9) = New SqlParameter("@SerialNo2", SqlDbType.VarChar, 50)
            params(9).Value = oCustomClass.Serial2
            params(10) = New SqlParameter("@AssetUsage", SqlDbType.Char, 1)
            params(10).Value = oCustomClass.AssetUsage
            params(11) = New SqlParameter("@UsedNew", SqlDbType.Char, 1)
            params(11).Value = oCustomClass.UsedNew
            params(12) = New SqlParameter("@OwnerAsset", SqlDbType.VarChar, 50)
            params(12).Value = oCustomClass.OwnerAsset
            params(13) = New SqlParameter("@OwnerAddress", SqlDbType.VarChar, 100)
            params(13).Value = oAddress.Address
            params(14) = New SqlParameter("@OwnerKelurahan", SqlDbType.VarChar, 30)
            params(14).Value = oAddress.Kelurahan
            params(15) = New SqlParameter("@OwnerKecamatan", SqlDbType.VarChar, 30)
            params(15).Value = oAddress.Kecamatan
            params(16) = New SqlParameter("@OwnerCity", SqlDbType.VarChar, 30)
            params(16).Value = oAddress.City
            params(17) = New SqlParameter("@OwnerZipCode", SqlDbType.Char, 5)
            params(17).Value = oAddress.ZipCode
            params(18) = New SqlParameter("@AssetReplacementFee", SqlDbType.Decimal)
            params(18).Value = oCustomClass.AssetReplacementFee
            params(19) = New SqlParameter("@ApprovalOrderID", SqlDbType.VarChar, 50)
            params(19).Value = strApprovalNo
            params(20) = New SqlParameter("@Approvaldate", SqlDbType.DateTime)
            params(20).Value = oCustomClass.approvaldate
            params(21) = New SqlParameter("@ApprovalStatus", SqlDbType.Char, 1)
            params(21).Value = oCustomClass.approvalstatus
            params(22) = New SqlParameter("@TaxDates", SqlDbType.DateTime)
            params(22).Value = oCustomClass.TaxDate
            params(23) = New SqlParameter("@ApprovalBy", SqlDbType.VarChar, 50)
            params(23).Value = oCustomClass.approvalby
            params(24) = New SqlParameter("@InsuranceStatus", SqlDbType.Char, 2)
            params(24).Value = oCustomClass.InsuranceStatus
            params(25) = New SqlParameter("@ApprovalRecomendation", SqlDbType.Text)
            params(25).Value = oCustomClass.approvalrecomendation
            params(26) = New SqlParameter("@ManufacturingMonth", SqlDbType.Int)
            params(26).Value = oCustomClass.ManufacturingMonth
            params(27) = New SqlParameter("@ManufacturingYear", SqlDbType.Int)
            params(27).Value = oCustomClass.ManufacturingYear
            params(28) = New SqlParameter("@AssetTypeID", SqlDbType.VarChar, 10)
            params(28).Value = oCustomClass.AssetTypeID
            params(29) = New SqlParameter("@OwnerRT", SqlDbType.VarChar, 3)
            params(29).Value = oAddress.RT
            params(30) = New SqlParameter("@OwnerRW", SqlDbType.VarChar, 3)
            params(30).Value = oAddress.RW
            params(31) = New SqlParameter("@OwnerAreaPhone1", SqlDbType.VarChar, 4)
            params(31).Value = oAddress.AreaPhone1
            params(32) = New SqlParameter("@OwnerPhone1", SqlDbType.VarChar, 10)
            params(32).Value = oAddress.Phone1
            params(33) = New SqlParameter("@OwnerAreaPhone2", SqlDbType.VarChar, 4)
            params(33).Value = oAddress.AreaPhone2
            params(34) = New SqlParameter("@OwnerPhone2", SqlDbType.VarChar, 10)
            params(34).Value = oAddress.Phone2
            params(35) = New SqlParameter("@OwnerAreaFax", SqlDbType.VarChar, 4)
            params(35).Value = oAddress.AreaFax
            params(36) = New SqlParameter("@OwnerFax", SqlDbType.VarChar, 10)
            params(36).Value = oAddress.Fax
            params(37) = New SqlParameter("@SupplierID", SqlDbType.VarChar, 10)
            params(37).Value = oCustomClass.SupplierID
            params(38) = New SqlParameter("@OTR", SqlDbType.Int)
            params(38).Value = oCustomClass.OTR
            params(39) = New SqlParameter("@DP", SqlDbType.Int)
            params(39).Value = oCustomClass.DP
            params(40) = New SqlParameter("@AssetRepSeqNo", SqlDbType.Int)
            params(40).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spAssetReplacementSaveAdd", params)

            If oData1.Rows.Count > 0 Then
                params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params1(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
                params1(3) = New SqlParameter("@AssetRepSeqNo", SqlDbType.Int)
                params1(4) = New SqlParameter("@AttributeID", SqlDbType.Char, 10)
                params1(5) = New SqlParameter("@AttributeContent", SqlDbType.Char, 20)
                For intLoop = 0 To oData1.Rows.Count - 1
                    params1(0).Value = oCustomClass.BranchId
                    params1(1).Value = oCustomClass.ApplicationID
                    params1(2).Value = oCustomClass.assetseqNo
                    params1(3).Value = params(40).Value.ToString
                    params1(4).Value = oData1.Rows(intLoop).Item("AttributeID").ToString
                    params1(5).Value = oData1.Rows(intLoop).Item("Attribute").ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spAssetReplacementSaveAdd2", params1)
                Next
            End If

            If oData2.Rows.Count > 0 Then
                params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params2(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params2(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
                params2(3) = New SqlParameter("@AssetRepSeqNo", SqlDbType.Int)
                params2(4) = New SqlParameter("@AssetDocID", SqlDbType.Char, 10)
                params2(5) = New SqlParameter("@IsMainDoc", SqlDbType.Bit)
                params2(6) = New SqlParameter("@IsDocExist", SqlDbType.Bit)
                params2(7) = New SqlParameter("@DocumentNo", SqlDbType.VarChar, 50)
                params2(8) = New SqlParameter("@Notes", SqlDbType.Text)

                For intLoop = 0 To oData2.Rows.Count - 1
                    params2(0).Value = oCustomClass.BranchId
                    params2(1).Value = oCustomClass.ApplicationID.Trim
                    params2(2).Value = oCustomClass.assetseqNo
                    params2(3).Value = params(40).Value.ToString
                    params2(4).Value = Trim(oData2.Rows(intLoop).Item("AssetDocID").ToString)
                    params2(5).Value = oData2.Rows(intLoop).Item("IsMainDoc")
                    params2(6).Value = CBool(oData2.Rows(intLoop).Item("chk"))
                    params2(7).Value = Trim(oData2.Rows(intLoop).Item("Number").ToString)
                    params2(8).Value = Trim(oData2.Rows(intLoop).Item("Notes").ToString)

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spAssetReplacementSaveAdd3", params2)
                Next
            End If
            transaction.Commit()

        Catch exp As Exception
            transaction.Rollback()
            WriteException("AssetReplacement", "AssetDataSaveAdd", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.ApplicationSaveAdd")
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Sub
    Public Function AssetRepApproval(ByVal oCustomClass As Parameter.AssetReplacement) As Parameter.AssetReplacement
        Dim params() As SqlParameter = New SqlParameter(3) {}
        With oCustomClass
            params(0) = New SqlParameter("@ApprovalOrderID", SqlDbType.VarChar, 50)
            params(0).Value = .approvalorderid
            params(1) = New SqlParameter("@ApprovalStatus", SqlDbType.VarChar, 1)
            params(1).Value = .approvalstatus
            params(2) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(2).Value = .BranchId
            params(3) = New SqlParameter("BusinessDate", SqlDbType.DateTime)
            params(3).Value = .BusinessDate
        End With
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spAssetReplacementApproval", params)
            Return oCustomClass

        Catch exp As Exception
            WriteException("StopAccrued", "StopAccruedInquiry", exp.Message + exp.StackTrace)

        End Try
    End Function
    Public Function AssetInquiryReport(ByVal customclass As Parameter.AssetReplacement) As Parameter.AssetReplacement
        Dim params As SqlParameter
        params = New SqlParameter("@SearchBy", SqlDbType.VarChar, 2000)
        params.Value = customclass.Search

        Try
            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spAssetReplInquiryReport", params)
            Return customclass

        Catch exp As Exception
            WriteException("WriteOff", "WriteOffInquiry", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ViewAssetReplInquiry(ByVal oCustomClass As Parameter.AssetReplacement) As Parameter.AssetReplacement
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            If oCustomClass.BranchId Is Nothing Then
                params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
                params(0).Value = ""
            Else
                params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
                params(0).Value = oCustomClass.BranchId
            End If
            If oCustomClass.ApplicationID Is Nothing Then
                params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
                params(1).Value = ""
            Else
                params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
                params(1).Value = oCustomClass.ApplicationID.Trim
            End If
            params(2) = New SqlParameter("@AssetRepSeqNo", SqlDbType.Int)
            params(2).Value = oCustomClass.AssetReqNo

            params(3) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
            params(3).Value = oCustomClass.assetseqNo

            If oCustomClass.approvalorderid Is Nothing Then
                params(4) = New SqlParameter("@ApprovalOrderID", SqlDbType.VarChar, 50)
                params(4).Value = ""
            Else
                params(4) = New SqlParameter("@ApprovalOrderID", SqlDbType.VarChar, 50)
                params(4).Value = oCustomClass.approvalorderid.Trim
            End If

            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewAssetReplacement", params)
            With oCustomClass
                If objread.Read Then
                    .BranchId = objread("BranchID").ToString()
                    .ApplicationID = objread("ApplicationID").ToString()
                    .AssetReqNo = CInt(objread("AssetRepSeqNo"))
                    .assetseqNo = CInt(objread("AssetSeqNo"))
                    .requestdate = CDate(objread("RequestDate"))
                    .Notes = objread("Notes").ToString()
                    .ReasonTypeID = objread("ReasonTypeID").ToString()
                    .ReasonID = objread("ReasonID").ToString()
                    .AssetCode = objread("AssetCode").ToString()
                    .Serial1 = objread("SerialNo1").ToString()
                    .Serial2 = objread("SerialNo2").ToString()
                    .AssetUsage = objread("AssetUsage").ToString()
                    .UsedNew = objread("UsedNew").ToString()
                    .OwnerAsset = objread("OwnerAsset").ToString()
                    .Address = objread("OwnerAddress").ToString()
                    .Rt = objread("OwnerRT").ToString()
                    .Rw = objread("OwnerRW").ToString()
                    .AreaPhone1 = objread("OwnerAreaPhone1").ToString()
                    .Phone1 = objread("OwnerPhone1").ToString()
                    .AreaPhone2 = objread("OwnerAreaPhone2").ToString()
                    .Phone2 = objread("OwnerPhone2").ToString()
                    .AreaFax = objread("OwnerAreaFax").ToString()
                    .Fax = objread("OwnerFax").ToString()
                    .Kelurahan = objread("OwnerKelurahan").ToString()
                    .Kecamatan = objread("OwnerKecamatan").ToString()
                    .City = objread("OwnerCity").ToString()
                    .ZipCode = objread("OwnerZipCode").ToString()
                    .AssetReplacementFee = CDbl(objread("AssetReplacementFee"))
                    .approvalorderid = objread("ApprovalOrderID").ToString()
                    .approvaldate = CDate(objread("ApprovalDate"))
                    .approvalstatus = objread("ApprovalStatus").ToString()
                    .TaxDate = CDate(objread("TaxDates"))
                    .approvalby = objread("ApprovalBy").ToString()
                    .InsuranceStatus = objread("InsuranceStatus").ToString()
                    .approvalrecomendation = objread("ApprovalRecomendation").ToString()
                    .ManufacturingMonth = CInt(objread("ManufacturingMonth"))
                    .ManufacturingYear = CInt(objread("ManufacturingYear"))
                    .Agreementno = objread("Agreementno").ToString()
                    .CustomerID = objread("CustomerID").ToString()
                    .CustomerName = objread("Name").ToString()
                    .Description = objread("NewDescription").ToString()
                    .txtSearch = objread("OldDescription").ToString()
                    .License = objread("LicensePlate").ToString()
                    .insurancecoy = objread("InsuranceComBranchName").ToString()
                    .ClaimDate = CDate(IIf(IsDBNull(objread("ClaimDate")), "1/1/1990", objread("ClaimDate")))
                    .SupplierID = objread("SupplierID").ToString()
                    .SupplierName = objread("SupplierName").ToString()
                    .LKS = objread("ClaimFormNo").ToString()
                    .ClaimAmount = CDbl(objread("ClaimAmount"))
                    .OTR = CDec(objread("OTRPrice"))
                    .DP = CDec(objread("DPAmount"))
                    .Usage = objread("AssetUsage").ToString()
                    .UsedNew = objread("UsedNew").ToString()
                End If
                objread.Close()
            End With
            oCustomClass.DataAttribute = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewAssetReplacement2", params).Tables(0)
            oCustomClass.DataAssetdoc = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewAssetReplacement3", params).Tables(0)
            oCustomClass.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewPrevAsset", params).Tables(0)
            Return oCustomClass
        Catch exp As Exception
            WriteException("AssetReplacement", "AssetDataSaveAdd", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.ApplicationSaveAdd")
        End Try

    End Function
    Public Sub SaveAssetReplacementCancel(ByVal customClass As Parameter.AssetReplacement)

        Try

            Dim params() As SqlParameter = New SqlParameter(4) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customClass.ApplicationID

            params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
            params(2).Value = customClass.assetseqNo

            params(3) = New SqlParameter("@AssetRepSeqNo", SqlDbType.Int)
            params(3).Value = customClass.AssetReqNo

            params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(4).Value = customClass.BusinessDate
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, "spAssetReplacementCancel", params)

        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try

    End Sub

    Public Sub SaveAssetReplacementExecute(ByVal customClass As Parameter.AssetReplacement)

        Dim oConnection As New SqlConnection(customClass.strConnection)
        Dim objTransaction As SqlTransaction
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        objTransaction = oConnection.BeginTransaction
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = customClass.BranchId.Replace("'", "")

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(1).Value = customClass.ApplicationID

        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(2).Value = customClass.assetseqNo

        params(3) = New SqlParameter("@AssetRepSeqNo", SqlDbType.Int)
        params(3).Value = customClass.AssetReqNo

        params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
        params(4).Value = customClass.BusinessDate
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, "spAssetReplacementExecution", params)
            objTransaction.Commit()
        Catch exp As Exception
            WriteException("AssetReplacement", "SaveAssetReplacementExecute", exp.Message + exp.StackTrace)
            objTransaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try


    End Sub
End Class

