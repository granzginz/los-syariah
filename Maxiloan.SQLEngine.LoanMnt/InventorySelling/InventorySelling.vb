
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InventorySelling : Inherits Maxiloan.SQLEngine.LoanMnt.AccMntBase

#Region " Private Const "
    Private Const GET_INVENTORY_SELLING_RECEIVE As String = "spInventorySellingReceivePaging"
    Private Const INVENTORY_SELLING_RECEIVE_VIEW As String = "spInventorySellingReceiveView"
    Private Const PROC_INVENTORY_SELLING_RECEIVE As String = "spInventorySellingReceiveSaveTitipan"

    Private m_connection As SqlConnection

#End Region

#Region "GetInvSellingReceive"
    Public Function GetInvSellingReceive(ByVal customClass As Parameter.InvSelling) As Parameter.InvSelling
        Dim oReturnValue As New Parameter.InvSelling
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, GET_INVENTORY_SELLING_RECEIVE, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("InventorySelling", "GetInvSellingReceive", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetInvSellingReceiveApproval(ByVal customClass As Parameter.InvSelling) As Parameter.InvSelling
        Dim oReturnValue As New Parameter.InvSelling
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spInventorySellingReceiveRepoPaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("InventorySelling", "GetInvSellingReceive", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Function GetInvSellingReceiveRepo(ByVal customClass As Parameter.InvSelling) As Parameter.InvSelling
        Dim oReturnValue As New Parameter.InvSelling
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spInventorySellingReceiveRepoPaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("InventorySelling", "GetInvSellingReceive", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

    Public Function InvSellingReceiveViewRepo(ByVal customclass As Parameter.InvSelling) As Parameter.InvSelling
        Dim oReturnValue As New Parameter.InvSelling
        Dim params = New List(Of SqlParameter)
        params.Add(New SqlParameter("@BranchId", SqlDbType.Char, 3) With {.Value = customclass.BranchId}) 
        params.Add(New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20) With {.Value = customclass.ApplicationID.Trim})
        params.Add(New SqlParameter("@RepoId", SqlDbType.BigInt) With {.Value = customclass.RepoId})
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spInventorySellingReceiveRepoView", params.ToArray).Tables(0)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

        Return oReturnValue
    End Function



#Region "InvSellingReceiveView"
    Public Function InvSellingReceiveView(ByVal customclass As Parameter.InvSelling) As Parameter.InvSelling
        Dim objread As SqlDataReader
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID.Trim

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, INVENTORY_SELLING_RECEIVE_VIEW, params)

            With customclass
                If objread.Read Then
                    .BranchAgreement = CType(objread("BranchFullName"), String)
                    .Agreementno = CType(objread("AgreementNo"), String)
                    .CustomerName = CType(objread("Name"), String)
                    .CustomerID = CType(objread("CustomerID"), String)
                    .AssetDescription = CType(objread("AssetDescription"), String)
                    .AssetType = CType(objread("AssetType"), String)
                    .ChassisNo = CType(objread("chassisNo"), String)
                    .EngineNo = CType(objread("EngineNo"), String)
                    .LicensePlate = CType(objread("LicensePlate"), String)
                    .TaxDate = CType(objread("TaxDate"), Date)
                    .SellingDate = CType(objread("SellingDate"), Date)
                    .SellingAmount = CType(objread("SellingAmount"), Double)
                    .Buyer = CType(objread("Buyer"), String)
                    .SellingNotes = CType(objread("SellingNotes"), String)
                    .TitiapnPembeli = CType(objread("TitipanPembeli"), Double)
                    .BankAccountID = CType(objread("BankAccountID"), String)
                    .BankAccountName = CType(objread("BankAccountName"), String)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("InventorySelling", "InvSellingReceiveView", exp.Message + exp.StackTrace)
        End Try
    End Function



#End Region

#Region "ProsesInventorySellingReceive"

    Public Function SavingInventorySellingReceive(ByVal oCustomClass As Parameter.InvSelling) As String
        Dim objTrans As SqlTransaction
        Dim params1 = New List(Of SqlParameter)
        Dim params = New List(Of SqlParameter)
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            If oCustomClass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1.Add(New SqlParameter("@branchid", SqlDbType.VarChar, 3) With {.Value = oCustomClass.BranchId.Replace("'", "")})
                params1.Add(New SqlParameter("@BankAccountID", SqlDbType.Char, 10) With {.Value = oCustomClass.BankAccountID})
                Dim prmRef = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20) With {.Direction = ParameterDirection.Output}
                params1.Add(prmRef)
                params1.Add(New SqlParameter("@BusinessDate", SqlDbType.DateTime) With {.Value = oCustomClass.BusinessDate})
                params1.Add(New SqlParameter("@Flag", SqlDbType.Char, 1) With {.Value = "M"})
                SqlHelper.ExecuteScalar(objTrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1.ToArray)
                oCustomClass.ReferenceNo = CStr(prmRef.Value)
            End If

            params.Add(New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3) With {.Value = oCustomClass.BranchId})
            params.Add(New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12) With {.Value = oCustomClass.LoginId})
            params.Add(New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime) With {.Value = oCustomClass.BusinessDate})
            params.Add(New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime) With {.Value = oCustomClass.ValueDate})
            params.Add(New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50) With {.Value = oCustomClass.ReceivedFrom})
            params.Add(New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50) With {.Value = oCustomClass.ReferenceNo})
            params.Add(New SqlParameter(PARAM_AMOUNTRECEIVE, SqlDbType.Decimal) With {.Value = oCustomClass.AmountReceive})
            params.Add(New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10) With {.Value = oCustomClass.BankAccountID})
            params.Add(New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50) With {.Value = oCustomClass.Notes})
            params.Add(New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20) With {.Value = oCustomClass.ApplicationID})
            params.Add(New SqlParameter(PARAM_WOP, SqlDbType.VarChar, 2) With {.Value = oCustomClass.WOP})
            params.Add(New SqlParameter("@CompanyId", SqlDbType.VarChar, 3) With {.Value = oCustomClass.CoyID})
            params.Add(New SqlParameter("@BranchAgreement", SqlDbType.VarChar, 3) With {.Value = oCustomClass.BranchAgreement})

            Dim vno = New SqlParameter("@Err", SqlDbType.Char, 20) With {.Direction = ParameterDirection.Output}
            'Dim vno = New SqlParameter("@VoucherNo_", SqlDbType.Char, 20) With {.Direction = ParameterDirection.Output}
            params.Add(vno)

            'SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, PROC_INVENTORY_SELLING_RECEIVE, params.ToArray)

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spPreInventorySellingReceiveSaveTitipan", params.ToArray)
            If (vno.Value.ToString().Trim = "") Then
                objTrans.Commit()
            Else
                objTrans.Rollback()
            End If

            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

            Return vno.Value.ToString

        Catch exp As Exception
            objTrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("InventorySelling", "SavingInventorySellingReceive", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function SavingInventorySellingReceiveApproval(ByVal oCustomClass As Parameter.InvSelling) As String
        Dim objTrans As SqlTransaction
        Dim params1 = New List(Of SqlParameter)
        Dim params = New List(Of SqlParameter)
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            params.Add(New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3) With {.Value = oCustomClass.BranchId})
            params.Add(New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12) With {.Value = oCustomClass.LoginId})
            params.Add(New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime) With {.Value = oCustomClass.BusinessDate})
            params.Add(New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime) With {.Value = oCustomClass.ValueDate})
            params.Add(New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50) With {.Value = oCustomClass.ReceivedFrom})
            params.Add(New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50) With {.Value = oCustomClass.ReferenceNo})
            params.Add(New SqlParameter(PARAM_AMOUNTRECEIVE, SqlDbType.Decimal) With {.Value = oCustomClass.AmountReceive})
            params.Add(New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10) With {.Value = oCustomClass.BankAccountID})
            params.Add(New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50) With {.Value = oCustomClass.Notes})
            params.Add(New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20) With {.Value = oCustomClass.ApplicationID})
            params.Add(New SqlParameter(PARAM_WOP, SqlDbType.VarChar, 2) With {.Value = oCustomClass.WOP})
            params.Add(New SqlParameter("@CompanyId", SqlDbType.VarChar, 3) With {.Value = oCustomClass.CoyID})
            params.Add(New SqlParameter("@BranchAgreement", SqlDbType.VarChar, 3) With {.Value = oCustomClass.BranchAgreement})

            params.Add(New SqlParameter("@Repoid", SqlDbType.BigInt) With {.Value = oCustomClass.RepoId})
            params.Add(New SqlParameter("@Approval", SqlDbType.Char, 2) With {.Value = oCustomClass.Approval})

            Dim vno = New SqlParameter("@VoucherNo_", SqlDbType.Char, 20) With {.Direction = ParameterDirection.Output}
            params.Add(vno)

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, PROC_INVENTORY_SELLING_RECEIVE, params.ToArray)
             
            objTrans.Commit()
           
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

            Return vno.Value.ToString

        Catch exp As Exception
            objTrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("InventorySelling", "SavingInventorySellingReceive", exp.Message + exp.StackTrace)
        End Try



    End Function

    Public Sub SavingInventorySellingExec(ByVal oCustomClass As Parameter.InvSelling)
        Dim objTrans As SqlTransaction
        Dim params1() As SqlParameter = New SqlParameter(4) {}
        Dim params(7) As SqlParameter
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(1).Value = oCustomClass.LoginId
            params(2) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(2).Value = oCustomClass.BusinessDate
            params(3) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.BusinessDate
            params(4) = New SqlParameter(PARAM_AMOUNTRECEIVE, SqlDbType.Decimal)
            params(4).Value = oCustomClass.AmountReceive
            params(5) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(5).Value = oCustomClass.ApplicationID
            params(6) = New SqlParameter("@CompanyId", SqlDbType.VarChar, 3)
            params(6).Value = oCustomClass.CoyID
            params(7) = New SqlParameter("@BranchAgreement", SqlDbType.VarChar, 3)
            params(7).Value = oCustomClass.BranchAgreement


            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spInventorySellingReceiveSave", params)
            objTrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch exp As Exception
            objTrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            WriteException("InventorySelling", "SavingInventorySellingReceive", exp.Message + exp.StackTrace)
        End Try
    End Sub
#End Region
End Class
