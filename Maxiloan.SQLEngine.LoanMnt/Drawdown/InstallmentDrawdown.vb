
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter

#End Region

Public Class InstallmentDrawdown : Inherits Maxiloan.SQLEngine.DataAccessBase


    Function getApprovalSchemeID(cnn As String, appId As String) As String
        Dim par As IList(Of SqlParameter) = New List(Of SqlParameter)
        par.Add(New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20) With {.Value = appId})
        Dim reader = SqlHelper.ExecuteReader(cnn, CommandType.StoredProcedure, "GetAgreementSchemeID", par.ToArray)
        If reader.Read Then
            Return CType(reader("SchemeID"), String)
        End If

        Return ""
    End Function

    Public Function DrawdownApprovalList(ByVal customclass As Parameter.Drawdown) As Parameter.Drawdown
        Dim params(5) As SqlParameter
        With customclass
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = .SortBy
            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output
            params(5) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 20)
            params(5).Value = .AppID

        End With
        Try
            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spDrawdownApprovalList", params).Tables(0)
            customclass.TotalRecord = CType(params(4).Value, Int16)
            Return customclass
        Catch exp As Exception
            WriteException("Drawdown", "DrawdownApprovalList", exp.Message + exp.StackTrace)
            Return Nothing
        End Try
    End Function

    Public Function GetCboUserAprPCAll(ByVal customclass As Parameter.Drawdown) As Parameter.Drawdown
        Dim oReturnValue As New Parameter.Drawdown

        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@ApprType", SqlDbType.VarChar, 20)
        params(0).Value = customclass.ApprovalSchemeID
        params(1) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
        params(1).Value = customclass.AppID
        Try
            oReturnValue.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spApprovalListUser", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Drawdown", "GetCboUserAprPCAll", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCboUserApproval(ByVal customclass As Parameter.Drawdown) As Parameter.Drawdown
        Dim oReturnValue As New Parameter.Drawdown

        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
        params(0).Value = customclass.AppID
        params(1) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
        params(1).Value = customclass.LoginId

        Try
            oReturnValue.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGetCboUserApproval", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Drawdown", "GetCboUserApproval", exp.Message + exp.StackTrace)
        End Try
    End Function

    Function ApproveSaveToNextPerson(ByVal customclass As Parameter.Drawdown) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction

        If objConnection.State = ConnectionState.Closed Then objConnection.Open()
        objtrans = objConnection.BeginTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(13) {}
            params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.VarChar, 10)
            params(0).Value = customclass.ApprovalSchemeID
            params(1) = New SqlParameter("@DrawdownNo", SqlDbType.VarChar, 20)
            params(1).Value = customclass.DrawdownNo
            params(2) = New SqlParameter("@ApprovalResult", SqlDbType.VarChar, 1)
            params(2).Value = customclass.ApprovalResult
            params(3) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
            params(3).Value = customclass.BranchId
            params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(4).Value = customclass.BusinessDate
            params(5) = New SqlParameter("@ApprovedDate", SqlDbType.DateTime)
            params(5).Value = customclass.ApprovedDate
            params(6) = New SqlParameter("@notes", SqlDbType.VarChar, 1000)
            params(6).Value = customclass.notes
            params(7) = New SqlParameter("@SecurityCode", SqlDbType.VarChar, 50)
            params(7).Value = customclass.SecurityCode
            params(8) = New SqlParameter("@UserApproval", SqlDbType.VarChar, 255)
            params(8).Value = customclass.UserApproval
            params(9) = New SqlParameter("@UserSecurityCode", SqlDbType.VarChar, 255)
            params(9).Value = customclass.UserSecurityCode
            params(10) = New SqlParameter("@IsEverRejected", SqlDbType.Bit)
            params(10).Value = customclass.IsEverRejected
            params(11) = New SqlParameter("@LoginID", SqlDbType.VarChar, 255)
            params(11).Value = customclass.LoginId
            params(12) = New SqlParameter("@NextPersonApproval", SqlDbType.VarChar, 255)
            params(12).Value = customclass.NextPersonApproval
            params(13) = New SqlParameter("@ipaddress", SqlDbType.VarChar, 255)
            params(13).Value = ""

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spApprovalRequestToNextPersonDrawdown", params.ToArray)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Drawdown", "ApproveSaveToNextPerson", exp.Message + exp.StackTrace)
            Throw New Exception("ERROR: Can Not Process Aprrove Installment Drawdown. " + exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Public Function ApproveSave(ByVal customclass As Parameter.Drawdown) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(11) {}
            params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.VarChar, 10)
            params(0).Value = customclass.ApprovalSchemeID
            params(1) = New SqlParameter("@DrawdownNo", SqlDbType.VarChar, 20)
            params(1).Value = customclass.DrawdownNo
            params(2) = New SqlParameter("@ApprovalResult", SqlDbType.VarChar, 1)
            params(2).Value = customclass.ApprovalResult
            params(3) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
            params(3).Value = customclass.BranchId
            params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(4).Value = customclass.BusinessDate
            params(5) = New SqlParameter("@ApprovedDate", SqlDbType.DateTime)
            params(5).Value = customclass.ApprovedDate
            params(6) = New SqlParameter("@notes", SqlDbType.VarChar, 1000)
            params(6).Value = customclass.notes
            params(7) = New SqlParameter("@SecurityCode", SqlDbType.VarChar, 50)
            params(7).Value = customclass.SecurityCode
            params(8) = New SqlParameter("@UserApproval", SqlDbType.VarChar, 255)
            params(8).Value = customclass.UserApproval
            params(9) = New SqlParameter("@UserSecurityCode", SqlDbType.VarChar, 255)
            params(9).Value = customclass.UserSecurityCode
            params(10) = New SqlParameter("@IsEverRejected", SqlDbType.Bit)
            params(10).Value = customclass.IsEverRejected
            params(11) = New SqlParameter("@LoginID", SqlDbType.VarChar, 255)
            params(11).Value = customclass.LoginId

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spApprovalSaveFinalDrawdown", params.ToArray)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Drawdown ", "ApproveSave", exp.Message + exp.StackTrace)
            Throw New Exception("ERROR: Can Not Process Approve Installment Drawdown. " + exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Function

    Public Function ApproveisFinal(ByVal customclass As Parameter.Drawdown) As Parameter.Drawdown
        Dim objread As SqlDataReader
        Dim params(3) As SqlParameter
        With customclass
            params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.VarChar, 50)
            params(0).Value = .ApprovalSchemeID
            params(1) = New SqlParameter("@DrawdownNo", SqlDbType.VarChar, 20)
            params(1).Value = .DrawdownNo
            params(2) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
            params(2).Value = .LoginId
            params(3) = New SqlParameter("@IsReRequest", SqlDbType.Bit)
            params(3).Value = .IsReRequest

        End With
        Try
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spDrawdownApprovalScreenSetLimit", params)

            If Not IsNothing(objread) Then
                With customclass
                    If objread.Read Then
                        .IsFinal = CType(objread("IsFinal"), String)
                        .NextPersonApproval = CType(objread("NextPersonApproval"), String)
                    End If
                    objread.Close()
                End With
            End If

            Return customclass
        Catch exp As Exception
            WriteException("Drawdown", "ApproveisFinal", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ApproveIsValidApproval(ByVal customclass As Parameter.Drawdown) As Parameter.Drawdown
        Dim objread As SqlDataReader
        Dim params(1) As SqlParameter
        With customclass
            params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.VarChar, 50)
            params(0).Value = .ApprovalSchemeID
            params(1) = New SqlParameter("@DrawdownNo", SqlDbType.VarChar, 20)
            params(1).Value = .DrawdownNo
        End With
        Try
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spDrawdownApprovalScreenIsValidApproval", params)

            If Not IsNothing(objread) Then
                With customclass
                    If objread.Read Then
                        .UserApproval = CType(objread("UserApproval"), String)
                        .ApprovalNo = CType(objread("ApprovalNo"), String)
                    End If
                    objread.Close()
                End With
            End If

            Return customclass

        Catch exp As Exception
            WriteException("Drawdown", "ApproveIsValidApproval", exp.Message + exp.StackTrace)
        End Try
    End Function


End Class
