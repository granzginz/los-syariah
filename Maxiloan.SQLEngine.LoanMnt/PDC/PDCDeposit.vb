

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class PDCDeposit
    Inherits AccMntBase
    Private Const spListPDCDeposit As String = "spPDCDepositListNextPaging"
    Private Const spPDCDepositHeader As String = "spPDCDepositH"
    Private Const spPDCDepositHistory As String = "spPDCDeposit"
    Private Const spCheckDuplikasiPDCStatus As String = "spCheckDuplikasiPDCStatus"

    Private Const PARAM_STATUSDATE As String = "@statusdate"
    Private Const PARAM_GIRONO As String = "@girono"
    Private Const PARAM_PDCRECEIPTNO As String = "@pdcreceiptno"
    Private Const PARAM_BANKACCOUNTIDDEP As String = "@bankaccountid"
    Private Const PARAM_NOREFERENCE As String = "@ReferenceNo"


#Region "ListPDCDeposit"
    Public Function ListPDCDeposit(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.ListPDCD = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListPDCDeposit, params).Tables(0)
            'customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("PDCDeposit", "ListPDCDeposit", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region

#Region "SavePDCDeposit"

    Public Function SavePDCDeposit(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim i As Integer

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(6) {}
            Dim params1() As SqlParameter = New SqlParameter(4) {}

            If customclass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = customclass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = customclass.BankaccountIDDep
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = customclass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"
                SqlHelper.ExecuteScalar(objtrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                customclass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1) = New SqlParameter(PARAM_STATUSDATE, SqlDbType.DateTime)
            params(2) = New SqlParameter(PARAM_GIRONO, SqlDbType.VarChar, 20)
            params(3) = New SqlParameter(PARAM_PDCRECEIPTNO, SqlDbType.VarChar, 20)
            params(4) = New SqlParameter(PARAM_BANKACCOUNTIDDEP, SqlDbType.Char, 20)
            params(5) = New SqlParameter(PARAM_NOREFERENCE, SqlDbType.VarChar, 20)
            params(6) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)

            For i = 0 To customclass.ListPDCSave.Rows.Count - 1
                params(0).Value = customclass.BranchId.Replace("'", "")
                params(1).Value = customclass.BusinessDate
                params(2).Value = customclass.ListPDCSave.Rows(i).Item("GIRONO")
                params(3).Value = customclass.ListPDCSave.Rows(i).Item("PDCreceiptNo")
                params(4).Value = customclass.BankaccountIDDep
                params(5).Value = customclass.ReferenceNo
                params(6).Direction = ParameterDirection.Output
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spPDCDepositHeader, params)
            Next

            'If CheckDupPDCStatus(customclass) Then
            '    customclass.strError = "PDC already Deposit By Another User"

            'Else

            'End If
            objtrans.Commit()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
            Return customclass
        Catch exp As Exception
            objtrans.Rollback()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
            WriteException("PDCDeposit", "SavePDCDeposit", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region

#Region "CheckDupPDCStatus"
    Public Function CheckDupPDCStatus(ByVal customclass As Parameter.PDCReceive) As Boolean
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")

            params(1) = New SqlParameter(PARAM_PDCRECEIPTNO, SqlDbType.VarChar, 20)
            params(1).Value = customclass.GroupReceiptNo

            params(2) = New SqlParameter("@GiroNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.GroupPDCNo

            params(3) = New SqlParameter("@ISChange", SqlDbType.Bit)
            params(3).Direction = ParameterDirection.Output

            params(4) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(4).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spCheckDuplikasiPDCStatus, params)
            Return CBool(params(3).Value)
        Catch exp As Exception
            WriteException("PDCDeposit", "CheckDupPDCStatus", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Check PDC")
        End Try
    End Function
#End Region
End Class
