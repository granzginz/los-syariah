
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class PDCMTrans
    Inherits AccMntBase
    Private Const spListPDCReceive As String = "spGetPDCList"
    Private Const spCheckDuplikasi As String = "spCheckDuplikasiPDC"
    Private Const spSavePDCMultiTrans As String = "spPDCMultiTrans"
    Private Const spSavePDCMultiTransDet As String = "spPDCMultiTransDetail"
    Private Const spDeletePDCEdit As String = "spDeletePDCEdit"
    Private Const spGetLastPDCDetail As String = "spGetLastPDCDetail"
    Private Const spPDCHeaderEdit As String = "spPDCHeaderEdit"
    Private Const spPDCEditListMultiAgreement As String = "spPDCEditListMultiAgreement"
    Private Const SPPDCPRINTRECEIPTNO As String = "spPDCPrintReceiptNo"
    Private Const PARAM_GIRONO As String = "@girono"
    Private Const PARAM_ISEXISTS As String = "@IsExists"
    Private Const PARAM_PDCReceiptNo As String = "@PDCReceiptNo"

    Private Const PARAM_RECDATE As String = "@recdate"
    Private Const PARAM_RECFrom As String = "@receivefrom"


    Public Function ListPDCMtrans(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId


            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spListPDCReceive, params)
            If objread.Read Then
                With customclass
                    .ApplicationID = CType(objread("applicationid"), String)
                    .Agreementno = CType(objread("agreementno"), String)
                    .BranchId = CType(objread("branchid"), String)
                    .BranchName = CType(objread("branchname"), String)
                    .CustomerName = CType(objread("customername"), String)
                    .InstallmentAmount = CType(objread("installmentamount"), Double)
                    .NextInstallmentDate = CType(objread("NextInstallmentDate"), Date)
                    .NextInstallmentNumber = CType(objread("NextInstallmentNumber"), Integer)
                    .CustomerID = CType(objread("CustomerID"), String)
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("PDCMTrans", "ListPDCMtrans", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetPDCEditListMultiAgrementController(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@PDCreceiptNo", SqlDbType.VarChar, 20)
            params(0).Value = customclass.PDCReceiptNo

            params(1) = New SqlParameter("@GiroNo", SqlDbType.VarChar, 20)
            params(1).Value = customclass.GiroNo

            customclass.ListPDC = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spPDCEditListMultiAgreement, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("PDCMTrans", "GetPDCEditListMultiAgrementController", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function SavePDCMtrans(ByVal customclass As Parameter.PDCReceive, ByVal oData1 As DataTable) As String
        Dim params() As SqlParameter = New SqlParameter(18) {}
        Dim objCon As New SqlConnection(customclass.strConnection)

        Dim transaction As SqlTransaction
        Dim intLoopOmset As Integer
        Dim params2() As SqlParameter = New SqlParameter(5) {}
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID

            params(2) = New SqlParameter(PARAM_BRANCHAGREEMENT, SqlDbType.VarChar, 3)
            params(2).Value = customclass.BranchAgreement

            params(3) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 20)
            params(3).Value = customclass.LoginId

            params(4) = New SqlParameter(PARAM_RECFrom, SqlDbType.VarChar, 50)
            params(4).Value = customclass.ReceivedFrom

            params(5) = New SqlParameter(PARAM_RECDATE, SqlDbType.DateTime, 20)
            params(5).Value = customclass.BusinessDate

            params(6) = New SqlParameter("@GiroNo", SqlDbType.Char, 20)
            params(6).Value = customclass.GiroNo

            params(7) = New SqlParameter("@bankID", SqlDbType.Char, 5)
            params(7).Value = customclass.BankPDC

            params(8) = New SqlParameter("@PDCAmount", SqlDbType.Decimal)
            params(8).Value = customclass.PDCAmount


            params(9) = New SqlParameter("@PDCDueDate", SqlDbType.DateTime)
            params(9).Value = customclass.PDCDueDate

            params(10) = New SqlParameter("@PDCType", SqlDbType.Char, 1)
            params(10).Value = customclass.PDCType

            params(11) = New SqlParameter("@IsCumm", SqlDbType.Bit, 1)
            params(11).Value = customclass.IsCumm

            params(12) = New SqlParameter("@IsInkaso", SqlDbType.Bit, 1)
            params(12).Value = customclass.IsInkaso

            params(13) = New SqlParameter("@PDCStatus", SqlDbType.Char, 2)
            params(13).Value = "OP"

            params(14) = New SqlParameter("@FisikStatus", SqlDbType.Char, 2)
            params(14).Value = "OH"

            params(15) = New SqlParameter("@StatusDate", SqlDbType.DateTime)
            params(15).Value = customclass.BusinessDate

            params(16) = New SqlParameter("@NumOfPDC", SqlDbType.Int)
            params(16).Value = customclass.NumPDC

            params(17) = New SqlParameter("@TotalPDCAmount", SqlDbType.Decimal)
            params(17).Value = customclass.GrandTotPDCAmount

            params(18) = New SqlParameter(PARAM_PDCReceiptNo, SqlDbType.VarChar, 100)
            params(18).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSavePDCMultiTrans, params)

            params2(0) = New SqlParameter("@branchid", SqlDbType.Char, 3)
            params2(1) = New SqlParameter("@PDCReceiptNo", SqlDbType.Char, 20)
            params2(2) = New SqlParameter("@GiroNo", SqlDbType.Char, 20)
            params2(3) = New SqlParameter("@PDCAmountTrans", SqlDbType.Decimal)
            params2(4) = New SqlParameter("@PaymentAllocationID", SqlDbType.Char, 10)
            params2(5) = New SqlParameter("@Description", SqlDbType.VarChar, 50)

            If oData1.Rows.Count > 0 Then
                For intLoopOmset = 0 To oData1.Rows.Count - 1
                    params2(0).Value = customclass.BranchId
                    params2(1).Value = CStr(params(18).Value)
                    params2(2).Value = customclass.GiroNo
                    params2(3).Value = oData1.Rows(intLoopOmset).Item("Amount")
                    params2(4).Value = oData1.Rows(intLoopOmset).Item("PaymentAllocationID")
                    params2(5).Value = oData1.Rows(intLoopOmset).Item("Description")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSavePDCMultiTransDet, params2)
                Next
            End If
            transaction.Commit()
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
            Return CStr(params(18).Value)
        Catch exp As Exception
            transaction.Rollback()
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
            WriteException("PDCMTrans", "SavePDCMtrans", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function SavePDCEdit(ByVal customclass As Parameter.PDCReceive, ByVal oData1 As DataTable) As String
        Dim Params3() As SqlParameter = New SqlParameter(10) {}
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim params2() As SqlParameter = New SqlParameter(6) {}
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Dim intLoopOmset As Integer
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            '========== Update PDCHeader dan PDCReceipt ========================='
            Params3(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            Params3(0).Value = customclass.BranchId
            Params3(1) = New SqlParameter("@GiroNo", SqlDbType.Char, 20)
            Params3(1).Value = customclass.GiroNo
            Params3(2) = New SqlParameter("@bankID", SqlDbType.Char, 5)
            Params3(2).Value = customclass.BankPDC
            Params3(3) = New SqlParameter("@PDCDueDate", SqlDbType.DateTime)
            Params3(3).Value = customclass.PDCDueDate
            Params3(4) = New SqlParameter("@PDCType", SqlDbType.Char, 1)
            Params3(4).Value = customclass.PDCType
            Params3(5) = New SqlParameter("@IsCumm", SqlDbType.Bit, 1)
            Params3(5).Value = customclass.IsCumm
            Params3(6) = New SqlParameter("@IsInkaso", SqlDbType.Bit, 1)
            Params3(6).Value = customclass.IsInkaso
            Params3(7) = New SqlParameter(PARAM_PDCReceiptNo, SqlDbType.VarChar, 20)
            Params3(7).Value = customclass.PDCReceiptNo
            Params3(8) = New SqlParameter("@GiroSeqNo", SqlDbType.Int)
            Params3(8).Value = customclass.GiroSeqNo
            Params3(9) = New SqlParameter("@BranchPDC", SqlDbType.VarChar, 3)
            Params3(9).Direction = ParameterDirection.Output
            Params3(10) = New SqlParameter("@PDCAmountTrans", SqlDbType.Decimal)
            Params3(10).Value = customclass.PDCAmount

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spPDCHeaderEdit, Params3)
            '===================================== Delete PDCDetail ========================================='
            'params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            'params(0).Value = customclass.BranchId
            'params(1) = New SqlParameter(PARAM_GIRONO, SqlDbType.VarChar, 20)
            'params(1).Value = customclass.GiroNo
            'params(2) = New SqlParameter(PARAM_PDCReceiptNo, SqlDbType.VarChar, 20)
            'params(2).Value = customclass.PDCReceiptNo
            'SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spDeletePDCEdit, params)

            '===================================== Insert PDCDetail ========================================='
            params2(0) = New SqlParameter("@branchid", SqlDbType.Char, 3)
            params2(1) = New SqlParameter("@PDCReceiptNo", SqlDbType.Char, 20)
            params2(2) = New SqlParameter("@GiroNo", SqlDbType.Char, 20)
            params2(3) = New SqlParameter("@PDCAmountTrans", SqlDbType.Decimal)
            params2(4) = New SqlParameter("@PaymentAllocationID", SqlDbType.Char, 10)
            params2(5) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
            params2(6) = New SqlParameter("@GiroSeqNo", SqlDbType.Int)

            If oData1.Rows.Count > 0 Then
                For intLoopOmset = 0 To oData1.Rows.Count - 1
                    params2(0).Value = Params3(9).Value
                    params2(1).Value = customclass.PDCReceiptNo
                    params2(2).Value = customclass.GiroNo
                    'params2(3).Value = oData1.Rows(intLoopOmset).Item("Amount")
                    params2(3).Value = customclass.PDCAmount
                    params2(4).Value = oData1.Rows(intLoopOmset).Item("PaymentAllocationID")
                    params2(5).Value = oData1.Rows(intLoopOmset).Item("Description")
                    params2(6).Value = customclass.GiroSeqNo
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSavePDCMultiTransDet, params2)
                Next
            End If
            transaction.Commit()
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
            Return CStr(Params3(7).Value)
        Catch exp As Exception
            transaction.Rollback()
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
            WriteException("PDCMTrans", "SavePDCEdit", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function GetLastPDCDetail(ByVal customclass As Parameter.PDCReceive, ByVal oData1 As DataTable) As Parameter.PDCReceive
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter(PARAM_GIRONO, SqlDbType.VarChar, 20)
            params(1).Value = customclass.GiroNo
            params(2) = New SqlParameter(PARAM_PDCReceiptNo, SqlDbType.VarChar, 20)
            params(2).Value = customclass.PDCReceiptNo
            params(3) = New SqlParameter("@GiroSeqNo", SqlDbType.Int)
            params(3).Value = customclass.GiroSeqNo
            customclass.ListPDC = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spGetLastPDCDetail, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("PDCMTrans", "GetLastPDCDetail", exp.Message + exp.StackTrace)
        End Try
    End Function




    Public Function SavePDCReceiveBp(ByVal cnn As String, ByVal customclass As Parameter.PDCReceive) As String
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        Dim objCon As New SqlConnection(cnn)

        Dim transaction As SqlTransaction

        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params.Add(New SqlParameter("@branchid", SqlDbType.Char, 3) With {.Value = customclass.BranchId})
            params.Add(New SqlParameter("@Applicationid", SqlDbType.VarChar, 20) With {.Value = customclass.ApplicationID})
            params.Add(New SqlParameter("@branchagreement", SqlDbType.VarChar, 3) With {.Value = customclass.BranchAgreement})
            params.Add(New SqlParameter("@recdate", SqlDbType.DateTime, 20) With {.Value = customclass.BusinessDate})
            params.Add(New SqlParameter("@receivefrom", SqlDbType.VarChar, 50) With {.Value = customclass.ReceivedFrom})
            params.Add(New SqlParameter("@loginid", SqlDbType.VarChar, 20) With {.Value = customclass.LoginId})

            params.Add(New SqlParameter("@TotPDCAmount", SqlDbType.Decimal) With {.Value = customclass.GrandTotPDCAmount})
             

            params.Add(New SqlParameter("@NoGiro", SqlDbType.VarChar, 20) With {.Value = customclass.GiroNo})
            params.Add(New SqlParameter("@BankId", SqlDbType.VarChar, 5) With {.Value = customclass.BankPDC})
            params.Add(New SqlParameter("@PDCType", SqlDbType.VarChar, 1) With {.Value = customclass.PDCType})
            params.Add(New SqlParameter("@IsCumm ", SqlDbType.Bit) With {.Value = customclass.IsCumm})
            params.Add(New SqlParameter("@IsInkaso ", SqlDbType.Bit) With {.Value = customclass.IsInkaso})
            params.Add(New SqlParameter("@PDCStatus", SqlDbType.VarChar, 2) With {.Value = "OP"})
            params.Add(New SqlParameter("@FisikStatus", SqlDbType.VarChar, 2) With {.Value = "OH"})
          
            params.Add(New SqlParameter("@pDCReceive", SqlDbType.Structured) With {.Value =
                       Parameter.PDCReceive.ToDataTable(customclass.BusinessDate, "OP", "OH",
                                                        customclass.PDCSource)})

 
            Dim pDCReceiptNo = New SqlParameter("@PDCReceiptNo", SqlDbType.VarChar, 20) With {.Direction = ParameterDirection.Output}
            params.Add(pDCReceiptNo)
            Dim errPrm = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            params.Add(errPrm)

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPDCMultiReceiveBp", params.ToArray)
            Dim RecNo = CType(pDCReceiptNo.Value, String)
            If CType(errPrm.Value, String) <> "" Then
                Return CType(errPrm.Value, String)
            End If
            transaction.Commit()
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
            Return RecNo

        Catch exp As Exception
            transaction.Rollback()
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
            WriteException("PDCReceive", "SavePDCReceive", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update PDC:" & exp.Message)
        End Try
    End Function



    Public Function PrintReceiptNo(ByVal strConnection As String, ByVal PDCReceiptNo As String, ByVal BranchID As String) As DataSet
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = BranchID
            params(1) = New SqlParameter(PARAM_PDCReceiptNo, SqlDbType.VarChar, 20)
            params(1).Value = PDCReceiptNo
            Return SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, SPPDCPRINTRECEIPTNO, params)
        Catch exp As Exception
            WriteException("PDCMTrans", "PrintReceiptNo", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
