

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports System.DBNull
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class PDCStatus
    Inherits AccMntBase

    Private Const spListPDCStatus As String = "spPDCStatusPaging"
    Private Const spListPDCStatusDet As String = "spPDCChangeStatusDetail"
    Private Const spSavePDCStatus As String = "spPDCStatusH"
    Private Const PARAM_GIRONO As String = "@girono"
    Private Const PARAM_PDCRECEIPTNO As String = "@pdcreceiptno"
    Private Const PARAM_STATUS As String = "@pdcstatus"


    Public Function ListPDCStatus(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListPDC = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListPDCStatus, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("PDCStatus", "ListPDCStatus", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListPDCStatusDet(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_PDCRECEIPTNO, SqlDbType.VarChar, 20)
            params(1).Value = customclass.PDCReceiptNo

            params(2) = New SqlParameter(PARAM_GIRONO, SqlDbType.VarChar, 20)
            params(2).Value = customclass.GiroNo

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spListPDCStatusDet, params)
            If objread.Read Then
                With customclass
                    .BranchLocation = CType(objread("BranchLocation"), String)
                    .GiroNo = CType(objread("GiroNo"), String)
                    .BankPDC = CType(objread("bankID"), String)
                    .PDCAmount = CType(objread("TotalPDCAmount"), Double)
                    .PDCDueDate = CType(objread("PDCDueDate"), Date)
                    .PDCType = CType(objread("PDCType"), String)
                    .BankClearing = CType(objread("bankaccountid"), String)
                    .BankClearingName = CType(objread("bankaccountname"), String)
                    .IsInkaso = CType(objread("Isinkaso"), Boolean)
                    .IsCumm = CType(objread("Iscummulative"), Boolean)
                    .PDCStatus = CType(objread("PDCStatus"), String)
                    .PDCStatusDate = CType(objread("Statusdate"), Date)
                    .Bounce = CType(objread("numberofbounce"), Integer)
                    .PDCReceiptNo = CType(objread("PDCreceiptno"), String)
                    If IsDBNull(objread("holduntildate")) Then
                        .HoldUntildate = Nothing
                    Else

                        .HoldUntildate = CType(objread("holduntildate"), Date)
                    End If

                    .RecDate = CType(objread("receiveddate"), Date)
                    .BankPDCName = CType(objread("bankName"), String)
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("PDCStatus", "ListPDCStatusDet", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub SavePDCStatusDet(ByVal customclass As Parameter.PDCReceive)
        Dim params() As SqlParameter = New SqlParameter(8) {}
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@StatusDate", SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter(PARAM_GIRONO, SqlDbType.VarChar, 8000)
            params(2).Value = customclass.GiroNo

            params(3) = New SqlParameter(PARAM_REASONID, SqlDbType.VarChar, 10)
            params(3).Value = customclass.ReasonID

            params(4) = New SqlParameter(PARAM_REASONTYPEID, SqlDbType.Char, 5)
            params(4).Value = customclass.ReasonTypeID

            params(5) = New SqlParameter(PARAM_STATUS, SqlDbType.Char, 2)
            params(5).Value = customclass.PDCStatus

            params(6) = New SqlParameter("@holduntildate", SqlDbType.DateTime)
            params(6).Value = customclass.HoldUntildate


            params(7) = New SqlParameter(PARAM_NOTES, SqlDbType.Char, 16)
            params(7).Value = customclass.Notes

            params(8) = New SqlParameter("@PDCReceiptNo", SqlDbType.Char, 20)
            params(8).Value = customclass.PDCReceiptNo
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spSavePDCStatus, params)
            objtrans.Commit()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        Catch exp As Exception
            objtrans.Rollback()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
            WriteException("PDCStatus", "SavePDCStatusDet", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Sub

End Class
