

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class PDCRInquiry
    Inherits AccMntBase
#Region "PrivateConst"
    Private Const spListPDCRInquiry As String = "spPDCRInquiryPaging"
    Private Const spListPDCRInquiryH As String = "spPDCRInquiryH"
    Private Const spListPDCRInquiryDet As String = "spPDCRInquiryDet"
    Private Const spListPDCInquiryDetail As String = "spPDCInquiryListDetail"
    Private Const spListPDCInquiryPA As String = "spPDCPA"
    Private Const spListPDCInquiryHistory As String = "spPDCHistory"
    Private Const spListPDCInquiryStatus As String = "spPDCInqStatusPaging"
    Private Const spReportPDCRinq As String = "spPDCRInqReport"
    Private Const spReportPDCInqStatus As String = "spPDCInqStatusReport"
    Private Const spInqPDCIncomplete As String = "spInqPDCIncomplete"
    Private Const spPDCPAllocationList As String = "spPDCPAllocationList"
#End Region

#Region "PDC RInquiry"
    Public Function ListPDCRInquiry(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListPDC = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListPDCRInquiry, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("PDCRInquiry", "ListPDCRInquiry", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListPDCRInquiryV(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@PDCReceiptno", SqlDbType.VarChar, 20)
            params(0).Value = customclass.PDCReceiptNo

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spListPDCRInquiryH, params)
            If objread.Read Then
                With customclass
                    .ApplicationID = CType(objread("applicationid"), String)
                    .Agreementno = CType(objread("agreementno"), String)
                    .CustomerName = CType(objread("name"), String)
                    .BranchLocation = CType(objread("BranchLocation"), String)
                    .BranchLocationName = CType(objread("branchfullname"), String)
                    .PDCReceiptNo = CType(objread("PDCReceiptNo"), String)
                    .RecDate = CType(objread("ReceivedDate"), Date)
                    .ReceivedFrom = CType(objread("ReceivedFrom"), String)
                    .ReceivedBy = CType(objread("cashierid"), String)
                    .NumOfPrint = CType(objread("PrintedNum"), Double)
                    .CustomerID = CType(objread("Customerid"), String)
                    If .NumOfPrint > 0 Then
                        .LastPrintRecDate = CType(objread("LastPrintedDate"), Date)
                    End If
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("PDCRInquiry", "ListPDCRInquiry", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListPDCRInquiryDet(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@PDCReceiptno", SqlDbType.VarChar, 20)
            params(0).Value = customclass.PDCReceiptNo

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId
            customclass.ListPDC = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListPDCRInquiryDet, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("PDCRInquiry", "ListPDCRInquiryDet", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ReportPDCInq(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spReportPDCRinq, params)
            Return customclass
        Catch exp As Exception
            WriteException("PDCRInquiry", "ReportPDCInq", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ReportPDCInqStatus(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spReportPDCInqStatus, params)
            Return customclass
        Catch exp As Exception
            WriteException("PDCRInquiry", "ReportPDCInqStatus", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "PDCDetail RInquiry"
    Public Function ListPDCInquiryListDetail(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@PDCReceiptno", SqlDbType.VarChar, 20)
            params(0).Value = customclass.PDCReceiptNo

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            params(2) = New SqlParameter("@GiroNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.GiroNo

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spListPDCInquiryDetail, params)

            If objread.Read Then
                With customclass
                    .PDCReceiptNo = CType(objread("PDCReceiptNo"), String)
                    .RecDate = CType(objread("ReceivedDate"), Date)
                    .BranchLocation = CType(objread("BranchLocation"), String)
                    .BranchLocationName = CType(objread("branchfullname"), String)
                    .ApplicationID = CType(objread("applicationid"), String)
                    .Agreementno = CType(objread("agreementno"), String)
                    .CustomerName = CType(objread("name"), String)
                    .GiroNo = CType(objread("GiroNo"), String)
                    .BankPDC = CType(objread("BankID"), String)
                    .BankPDCName = CType(objread("BankName"), String)
                    .PDCAmount = CType(objread("TotalPDCAmount"), Double)
                    .PDCDueDate = CType(objread("PDCDueDate"), Date)
                    .PDCType = CType(objread("PDCTypeDesc"), String)
                    .BankClearing = CType(objread("bankaccountid"), String)
                    .CustomerID = CType(objread("customerid"), String)
                    If .BankClearing.Trim <> "-" Then

                        .BankClearingName = CType(objread("bankaccountname"), String)
                    Else
                        .BankClearingName = "-"
                    End If

                    .IsInkaso = CType(objread("isInkaso"), Boolean)
                    .IsCumm = CType(objread("IsCummulative"), Boolean)
                    .PDCStatus = CType(objread("PDCStatusDesc"), String)
                    .StatusDate = CType(objread("Statusdate"), Date)
                    .Bounce = CType(objread("numberofBounce"), Integer)
                    If .PDCStatus.Trim = "Hold" Then
                        .HoldUntildate = CType(objread("HoldUntilDate"), Date)
                    End If
                    .GiroSeqNo = CInt(objread("MaxGiroSeqNo"))
                    .ApplicationID = CType(objread("ApplicationId"), String)
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("PDCRInquiry", "ListPDCInquiryListDetail", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Function ListPDCInquiryDetail(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@PDCReceiptno", SqlDbType.VarChar, 20)
            params(0).Value = customclass.PDCReceiptNo

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            params(2) = New SqlParameter("@GiroNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.GiroNo


            customclass.listPDCPA = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListPDCInquiryPA, params).Tables(0)
            customclass.listPDCHistory = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListPDCInquiryHistory, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("PDCRInquiry", "ListPDCInquiryListDetail", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function PDCInquiryDetailwithGiroSeqNo(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter("@PDCReceiptno", SqlDbType.VarChar, 20)
            params(0).Value = customclass.PDCReceiptNo

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            params(2) = New SqlParameter("@GiroNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.GiroNo

            params(3) = New SqlParameter("@GiroSeqNo", SqlDbType.Int)
            params(3).Value = customclass.GiroSeqNo
            customclass.listPDCPA = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spPDCPAllocationList, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("PDCRInquiry", "ListPDCInquiryListDetail", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "PDC Inq Status"

    Public Function ListPDCInquiryStatus(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.listPDCStatus = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListPDCInquiryStatus, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("PDCRInquiry", "ListPDCInquiryStatus", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "InqPDCIncomplete"
    Public Function InqPDCIncomplete(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListPDC = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInqPDCIncomplete, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("PDCRInquiry", "InqPDCIncomplete", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
End Class
