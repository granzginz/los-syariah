

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class PDCClearRecon
    Inherits AccMntBase
    Private Const spListClearRecon As String = "spPDCCLearReconListNextPaging"
    Private Const spSaveClearRecon As String = "spPDCClearReconcile"
    Private Const spListClearReconC As String = "spPDCClearReconList"
    Private Const spWithInkaso As String = "spProcessPDCClearingWithInkaso"
    Private Const spCheckDuplikasiPDCStatus As String = "spCheckDuplikasiPDCClear"
    Private Const spCheckRecon As String = "spCheckDupPDCClearRecon"

#Region "ListPDCClearRecon"
    Public Function ListPDCClearRecon(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.listPDCClear = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListClearRecon, params).Tables(0)
            'customclass.TotalRecord = CInt(params(4).Value)

            Return customclass
        Catch exp As Exception
            WriteException("PDCClearRecon", "ListPDCClearRecon", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "ListPDCClearCRecon"
    Public Function ListPDCClearCRecon(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@Girono", SqlDbType.Char, 20)
            params(0).Value = customclass.GiroNo

            params(1) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId


            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spListClearReconC, params)
            If objread.Read Then
                With customclass
                    .ApplicationID = CType(objread("applicationid"), String)
                    .Agreementno = CType(objread("agreementno"), String)
                    .CustomerName = CType(objread("name"), String)
                    .GiroNo = CType(objread("GiroNo"), String)
                    .BankPDC = CType(objread("bankID"), String)
                    .BankPDCName = CType(objread("bankname"), String)
                    .PDCAmount = CType(objread("TotalPDCAmount"), Double)
                    .PDCDueDate = CType(objread("PDCDueDate"), Date)
                    .PDCType = CType(objread("PDCTypedesc"), String)
                    .BankClearing = CType(objread("bankaccountid"), String)
                    .BankClearingName = CType(objread("bankaccountname"), String)
                    .IsInkaso = CType(objread("Isinkaso"), Boolean)
                    .IsCumm = CType(objread("Iscummulative"), Boolean)
                    .BranchLocationName = CType(objread("branchfullname"), String)

                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("PDCClearRecon", "ListPDCClearCRecon", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "SavePDCClearRecon"
    Public Function SavePDCClearRecon(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive

        Dim params() As SqlParameter = New SqlParameter(8) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@businessdate", SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter("@girono", SqlDbType.VarChar, 8000)
            params(2).Value = customclass.GroupPDCNo

            params(3) = New SqlParameter("@reconcileby", SqlDbType.Char, 20)
            params(3).Value = customclass.LoginId

            params(4) = New SqlParameter("@strvoucherno", SqlDbType.VarChar, 8000)
            params(4).Value = customclass.GroupVoucherno

            params(5) = New SqlParameter("@strjournalno", SqlDbType.VarChar, 8000)
            params(5).Value = customclass.GroupJournalno

            params(6) = New SqlParameter("@PDCReceiptNo", SqlDbType.VarChar, 8000)
            params(6).Value = customclass.GroupReceiptNo

            params(7) = New SqlParameter("@LoginID", SqlDbType.VarChar, 12)
            params(7).Value = customclass.LoginId

            params(8) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(8).Direction = ParameterDirection.Output

            '    SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spSaveClearRecon, params)
            If CheckDupPDCClearR(customclass) Then
                customclass.strError = "PDC already Clear By Another User"
            Else
                SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spSaveClearRecon, params)
            End If

            Return customclass
        Catch exp As Exception
            WriteException("PDCClearRecon", "SavePDCClearRecon", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update PDC")
        End Try
    End Function
#End Region

#Region "SavePDCClearReconC"

    '==============spnya henry======================
    Public Sub SavePDCClearReconC(ByVal customclass As Parameter.PDCReceive)

        Dim params() As SqlParameter = New SqlParameter(7) {}
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@valuedate", SqlDbType.DateTime)
            params(1).Value = customclass.ivalueDate

            params(2) = New SqlParameter("@businessdate", SqlDbType.DateTime)
            params(2).Value = customclass.BusinessDate

            params(3) = New SqlParameter("@PDCReceiptNo", SqlDbType.VarChar, 20)
            params(3).Value = customclass.PDCReceiptNo

            params(4) = New SqlParameter("@girono", SqlDbType.VarChar, 20)
            params(4).Value = customclass.GiroNo

            params(5) = New SqlParameter("@AmountClear", SqlDbType.Decimal)
            params(5).Value = customclass.PDCAmount

            params(6) = New SqlParameter("@CashierID", SqlDbType.VarChar, 12)
            params(6).Value = customclass.LoginId

            params(7) = New SqlParameter("@notes", SqlDbType.Char, 16)
            params(7).Value = customclass.Notes

            If CheckDupPDCStatus(customclass) Then
                Throw New Exception("PDC already Clear By Another User")
            Else
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spWithInkaso, params)
            End If
            objtrans.Commit()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        Catch exp As Exception
            objtrans.Rollback()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
            WriteException("PDCClearRecon", "SavePDCClearReconC", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Sub

#End Region

#Region "CheckDupPDCStatus"

    Public Function CheckDupPDCStatus(ByVal customclass As Parameter.PDCReceive) As Boolean
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@GiroNo", SqlDbType.VarChar, 8000)
            params(1).Value = customclass.GiroNo

            params(2) = New SqlParameter("@PDCReceiptno", SqlDbType.VarChar, 20)
            params(2).Value = customclass.PDCReceiptNo

            params(3) = New SqlParameter("@ISChange", SqlDbType.Bit)
            params(3).Direction = ParameterDirection.Output

            params(4) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(4).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spCheckDuplikasiPDCStatus, params)
            Return CBool(params(3).Value)
        Catch exp As Exception
            WriteException("PDCClearRecon", "CheckDupPDCStatus", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Check PDC")
        End Try
    End Function
#End Region

#Region "CheckDupPDCClearR"
    Public Function CheckDupPDCClearR(ByVal customclass As Parameter.PDCReceive) As Boolean
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@PDCReceiptNo", SqlDbType.VarChar, 8000)
            params(1).Value = customclass.GroupReceiptNo

            params(2) = New SqlParameter("@GiroNo", SqlDbType.VarChar, 8000)
            params(2).Value = customclass.GroupPDCNo

            params(3) = New SqlParameter("@ISChange", SqlDbType.Bit)
            params(3).Direction = ParameterDirection.Output

            params(4) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(4).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spCheckRecon, params)
            Return CBool(params(3).Value)
        Catch exp As Exception
            WriteException("PDCClearRecon", "CheckDupPDCClearR", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update PDC")
        End Try
    End Function
#End Region

End Class
