

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class PDCBounce
    Inherits AccMntBase
    Private Const PARAM_CLEARDATE As String = "@ClearDate"
    Private Const SPPDCBOUNCELIST As String = "spPDCBounceList"
    Private Const SPPROCESSPDCBOUNCE As String = "spProcessPDCBounce"
    Private Const SPPDCBOUNCEVIEW As String = "spPDCBounceView"
    Private Const SPPDCBOUNCEPRINT As String = "spPDCBouncePrintReceipt"
    Private Const PARAM_PDCRECEIPTNO As String = "@PdcReceiptNo"
    Private Const PARAM_GIRONO As String = "@GiroNo"


    Public Function PDCBounceList(ByVal customclass As Parameter.PDCBounce) As Parameter.PDCBounce
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(1).Value = customclass.BankAccountID

            params(2) = New SqlParameter(PARAM_CLEARDATE, SqlDbType.VarChar, 10)
            params(2).Value = customclass.ClearDate.ToString("yyyyMMdd")

            params(3) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
            params(3).Value = customclass.WhereCond

            params(4) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 8000)
            params(4).Value = customclass.SortBy

            params(5) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(5).Value = customclass.CurrentPage

            params(6) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(6).Value = customclass.PageSize

            params(7) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
            params(7).Direction = ParameterDirection.Output
            customclass.ListPDCBounce = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPPDCBOUNCELIST, params).Tables(0)
            customclass.TotalRecord = CInt(params(7).Value)
            Return customclass
        Catch exp As Exception
            WriteException("PDCBounce", "PDCBounceList", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Sub ProcesPDCBounce(ByVal customclass As Parameter.PDCBounce)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(8) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_PDCRECEIPTNO, SqlDbType.VarChar, 8000)
            params(1).Value = customclass.PDCReceiptNo

            params(2) = New SqlParameter(PARAM_GIRONO, SqlDbType.VarChar, 8000)
            params(2).Value = customclass.GiroNo

            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = customclass.BusinessDate

            params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(4).Value = customclass.LoginId

            params(5) = New SqlParameter(PARAM_REASONID, SqlDbType.VarChar, 10)
            params(5).Value = customclass.ReasonID

            params(6) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 20)
            params(6).Value = customclass.ReferenceNo

            params(7) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(7).Value = customclass.ValueDate

            params(8) = New SqlParameter(PARAM_NOTES, SqlDbType.Text)
            params(8).Value = customclass.Notes

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPPROCESSPDCBOUNCE, params)
            objtrans.Commit()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        Catch exp As Exception
            WriteException("PDCBounce", "ProcesPDCBounce", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
            objtrans.Rollback()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub

    Public Function PDCBounceView(ByVal customclass As Parameter.PDCBounce) As Parameter.PDCBounce
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim objread As SqlDataReader
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter(PARAM_PDCRECEIPTNO, SqlDbType.VarChar, 20)
        params(1).Value = customclass.PDCReceiptNo

        params(2) = New SqlParameter(PARAM_GIRONO, SqlDbType.VarChar, 20)
        params(2).Value = customclass.GiroNo

        Try
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, SPPDCBOUNCEVIEW, params)
            If objread.Read Then
                With customclass
                    .BranchAgreement = objread("AgreementBranch").ToString
                    .Agreementno = objread("AgreementNo").ToString
                    .ApplicationID = objread("ApplicationID").ToString
                    .CustomerID = objread("CustomerID").ToString
                    .CustomerName = objread("CustomerName").ToString
                    .PDCReceiptNo = objread("PDCReceiptNo").ToString
                    .BankName = objread("BankName").ToString
                    .BankAccountName = objread("BankAccountName").ToString
                    .PDCDueDate = CDate(objread("PDCDueDate"))
                    .TotalPDCAmount = CDbl(objread("PDCAmount"))
                    .PDCType = objread("PDCType").ToString
                    .IsCummulative = CBool(objread("isCummulative"))
                    .IsInkaso = CBool(objread("isInkaso"))
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("PDCBounce", "PDCBounceView", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function PrintPDCBounce(ByVal customclass As Parameter.PDCBounce) As DataSet
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter(PARAM_PDCRECEIPTNO, SqlDbType.VarChar, 20)
        params(1).Value = customclass.PDCReceiptNo

        params(2) = New SqlParameter(PARAM_GIRONO, SqlDbType.VarChar, 20)
        params(2).Value = customclass.GiroNo
        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPPDCBOUNCEPRINT, params)
        Catch exp As Exception
            WriteException("PDCBounce", "PrintPDCBounce", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
End Class