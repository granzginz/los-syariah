

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class PDCReceive
    Inherits AccMntBase
    Private Const spListPDCReceive As String = "spGetPDCList"
    Private Const spGetHoliday As String = "spGetHoliday"
    Private Const spCheckDuplikasi As String = "spCheckDuplikasiPDC"
    Private Const spSavePDCReceive As String = "spPDCReceive"
    Private Const spSavePDCReceiveH As String = "spPDCReceiveH"
    Private Const SPPDCPRINTRECEIPTNO As String = "spPDCPrintReceiptNo"
    Private Const PARAM_GIRONO As String = "@girono"
    Private Const PARAM_ISEXISTS As String = "@IsExists"
    Private Const PARAM_PDCReceiptNo As String = "@PDCReceiptNo"

    Private Const PARAM_RECDATE As String = "@recdate"
    Private Const PARAM_RECFrom As String = "@receivefrom"


    Public Function ListPDCReceive(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spListPDCReceive, params)
            If objread.Read Then
                With customclass
                    .ApplicationID = CType(objread("applicationid"), String)
                    .Agreementno = CType(objread("agreementno"), String)
                    .BranchId = CType(objread("branchid"), String)
                    .BranchName = CType(objread("branchname"), String)
                    .CustomerName = CType(objread("customername"), String)
                    .InstallmentAmount = CType(objread("installmentamount"), Double)
                    .PaymentFrequency = CType(objread("paymentfrequency"), Integer)
                    .NextInstallmentDate = CType(objread("NextInstallmentDate"), Date)
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("PDCReceive", "ListPDCReceive", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetHoliday(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim objread As SqlDataReader
        Dim lObjHoliday As New Hashtable
        Try
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spGetHoliday)

            While objread.Read
                lObjHoliday.Add(CDate(objread("holidaydate")), "")
            End While
            objread.Close()
            customclass.HolidayDate = lObjHoliday
            Return customclass
        Catch exp As Exception
            WriteException("PDCReceive", "GetHoliday", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function CheckDuplikasiPDC(ByVal customClass As Parameter.PDCReceive) As Boolean
        Dim objread As SqlDataReader
        Dim lObjHoliday As New Hashtable
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customClass.BranchId

            params(1) = New SqlParameter(PARAM_GIRONO, SqlDbType.VarChar, 4000)
            params(1).Value = customClass.ListGiroNo

            params(2) = New SqlParameter(PARAM_ISEXISTS, SqlDbType.Bit)
            params(2).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, spCheckDuplikasi, params)
            Return CBool(params(2).Value)
        Catch exp As Exception
            WriteException("PDCReceive", "CheckDuplikasiPDC", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function SavePDCReceive(ByVal customclass As Parameter.PDCReceive, ByVal oData1 As DataTable) As String
        Dim params() As SqlParameter = New SqlParameter(8) {}
        Dim objCon As New SqlConnection(customclass.strConnection)

        Dim transaction As SqlTransaction
        Dim intLoopOmset As Integer
        Dim params2() As SqlParameter = New SqlParameter(15) {}
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID

            params(2) = New SqlParameter(PARAM_BRANCHAGREEMENT, SqlDbType.VarChar, 3)
            params(2).Value = customclass.BranchAgreement

            params(3) = New SqlParameter(PARAM_RECDATE, SqlDbType.DateTime, 20)
            params(3).Value = customclass.BusinessDate

            params(4) = New SqlParameter(PARAM_RECFrom, SqlDbType.VarChar, 50)
            params(4).Value = customclass.ReceivedFrom

            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 20)
            params(5).Value = customclass.LoginId

            params(6) = New SqlParameter("@NumOFPDC", SqlDbType.Int)
            params(6).Value = customclass.NumPDC

            params(7) = New SqlParameter("@TotPDCAmount", SqlDbType.Decimal)
            params(7).Value = customclass.GrandTotPDCAmount

            params(8) = New SqlParameter(PARAM_PDCReceiptNo, SqlDbType.VarChar, 100)
            params(8).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSavePDCReceive, params)


            params2(0) = New SqlParameter("@branchid", SqlDbType.Char, 3)
            params2(1) = New SqlParameter("@PDCReceiptNo", SqlDbType.VarChar, 20)
            params2(2) = New SqlParameter("@GiroNo", SqlDbType.Char, 20)
            params2(3) = New SqlParameter("@Recdate", SqlDbType.DateTime)
            params2(4) = New SqlParameter(PARAM_BRANCHAGREEMENT, SqlDbType.VarChar, 3)
            params2(5) = New SqlParameter("@Applicationid", SqlDbType.VarChar, 20)
            params2(6) = New SqlParameter("@bankID", SqlDbType.Char, 5)
            params2(7) = New SqlParameter("@PDCAmount", SqlDbType.Decimal)
            params2(8) = New SqlParameter("@PDCDueDate", SqlDbType.DateTime)
            params2(9) = New SqlParameter("@PDCType", SqlDbType.Char, 1)
            params2(10) = New SqlParameter("@IsCumm", SqlDbType.Bit, 1)
            params2(11) = New SqlParameter("@IsInkaso", SqlDbType.Bit, 1)
            params2(12) = New SqlParameter("@PDCStatus", SqlDbType.Char, 2)
            params2(13) = New SqlParameter("@FisikStatus", SqlDbType.Char, 2)
            params2(14) = New SqlParameter("@StatusDate", SqlDbType.DateTime)
            params2(15) = New SqlParameter("@PaymentAllocationID", SqlDbType.VarChar, 10)

            If oData1.Rows.Count > 0 Then
                For intLoopOmset = 0 To oData1.Rows.Count - 1

                    params2(0).Value = customclass.BranchId
                    params2(1).Value = CStr(params(8).Value)
                    params2(2).Value = oData1.Rows(intLoopOmset).Item("PDCNo")
                    params2(3).Value = CDate(customclass.BusinessDate)
                    params2(4).Value = customclass.BranchAgreement
                    params2(5).Value = customclass.ApplicationID
                    params2(6).Value = customclass.BankPDC
                    params2(7).Value = oData1.Rows(intLoopOmset).Item("PDCAmount")
                    params2(8).Value = CDate(oData1.Rows(intLoopOmset).Item("DueDate"))
                    params2(9).Value = oData1.Rows(intLoopOmset).Item("Type")
                    params2(10).Value = CBool(oData1.Rows(intLoopOmset).Item("IsCummulative"))
                    params2(11).Value = CBool(oData1.Rows(intLoopOmset).Item("IsInkaso"))
                    params2(12).Value = "OP"
                    params2(13).Value = "OH"
                    params2(14).Value = CDate(customclass.BusinessDate)
                    params2(15).Value = "INSTALLRCV"
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSavePDCReceiveH, params2)
                Next
            End If
            transaction.Commit()
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
            Return CStr(params(8).Value)
        Catch exp As Exception
            transaction.Rollback()
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
            WriteException("PDCReceive", "SavePDCReceive", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update PDC:" & exp.Message)
        End Try
    End Function

     

    Public Function SavePDCReceiveBp(ByVal cnn As String, ByVal customclass As Parameter.PDCReceive) As String
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        Dim objCon As New SqlConnection(cnn)

        Dim transaction As SqlTransaction
          
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open() 
            transaction = objCon.BeginTransaction

            params.Add(New SqlParameter("@branchid", SqlDbType.Char, 3) With {.Value = customclass.BranchId}) 
            params.Add(New SqlParameter("@Applicationid", SqlDbType.VarChar, 20) With {.Value = customclass.ApplicationID}) 
            params.Add(New SqlParameter("@branchagreement", SqlDbType.VarChar, 3) With {.Value = customclass.BranchAgreement})
            params.Add(New SqlParameter("@recdate", SqlDbType.DateTime, 20) With {.Value = customclass.BusinessDate})
            params.Add(New SqlParameter("@receivefrom", SqlDbType.VarChar, 50) With {.Value = customclass.ReceivedFrom})
            params.Add(New SqlParameter("@loginid", SqlDbType.VarChar, 20) With {.Value = customclass.LoginId})
            params.Add(New SqlParameter("@NumOFPDC", SqlDbType.Int) With {.Value = customclass.NumPDC}) 
            params.Add(New SqlParameter("@TotPDCAmount", SqlDbType.Decimal) With {.Value = customclass.GrandTotPDCAmount})
            params.Add(New SqlParameter("@PaymentAllocationID", SqlDbType.VarChar, 10) With {.Value = "INSTALLRCV"})
            params.Add(New SqlParameter("@pDCReceive", SqlDbType.Structured) With {.Value = Parameter.PDCReceive.ToDataTable(customclass.BusinessDate, "OP", "OH", customclass.PDCSource)})
            Dim pDCReceiptNo = New SqlParameter("@PDCReceiptNo", SqlDbType.VarChar, 20) With {.Direction = ParameterDirection.Output}
            params.Add(pDCReceiptNo)
            Dim errPrm = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            params.Add(errPrm)

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPDCReceiveBp", params.ToArray)
            Dim RecNo = CType(pDCReceiptNo.Value, String)
            If CType(errPrm.Value, String) <> "" Then
                Return CType(errPrm.Value, String)
            End If
            transaction.Commit()
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
            Return RecNo

        Catch exp As Exception
            transaction.Rollback()
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
            WriteException("PDCReceive", "SavePDCReceive", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update PDC:" & exp.Message)
        End Try
    End Function



    Public Function PrintReceiptNo(ByVal strConnection As String, ByVal PDCReceiptNo As String, ByVal BranchID As String) As DataSet
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = BranchID
            params(1) = New SqlParameter(PARAM_PDCReceiptNo, SqlDbType.VarChar, 20)
            params(1).Value = PDCReceiptNo
            Return SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, SPPDCPRINTRECEIPTNO, params)
        Catch exp As Exception
            WriteException("PDCReceive", "PrintReceiptNo", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
