
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region
Public Class PDCMultiAgreement
    Inherits AccMntBase
    Private Const SPPDCMULTIAGREEMENTLISTDEFAULT As String = "spPDCMultiAgreementListDefault"
    Private Const PARAM_CUSTOMERID As String = "@CustomerID"

    Private Const SPPDCMULTIAGREEMENTINFORMATION As String = "spPDCMultiAgreementInformation"

    Private Const SPPDCEDITMULTIAGREEMENTHEADERINFORMATION As String = "spPDCEditMultiAgreementInformationH"
    Private Const SPPDCEDITMULTIAGREEMENTDETAILINFORMATION As String = "spPDCEditMultiAgreementInformationD"
    Private Const SPPDCMULTIAGREEMENTSAVEH As String = "spPDCMultiAgreementSaveH"
    Private Const SPPDCMULTIAGREEMENTSAVED As String = "spPDCMultiAgreementSaveD"

    Private Const SPPDCEDITMULTIAGREEMENTSAVEH As String = "spPDCEditMultiAgreementSaveH"
    Private Const SPPDCEDITMULTIAGREEMENTSAVED As String = "spPDCEditMultiAgreementSaveD"

    Private Const PARAM_RECEIVEDFROM As String = "@ReceiveFrom"
    Private Const PARAM_GIRONO As String = "@GiroNo"
    Private Const PARAM_PDCRECEIPTNO As String = "@PDCReceiptNo"
    Private Const PARAM_PAYMENTAMOUNT As String = "@AmountPayment"
    Private Const PARAM_GIROSEQNO As String = "@GiroSeqNo"
    Private Const PARAM_BRANCHAGREMENT As String = "@BranchAgreement"
    Private Const PARAM_BRANCHPDC As String = "@BranchPDC"
    Private Const PARAM_BRANCHLOCATION As String = "@BranchLocation"
    Private Const PARAM_TOTALGROUPPDCAMOUNT As String = "@TotalPDCGroupAmount"

    Private Const PARAM_PDCDUEDATE As String = "@PDCDueDate"
    Private Const PARAM_PROCESSID As String = "@ProcessID"

    Private Const PARAM_ISCUMM As String = "@IsCumm"
    Private Const PARAM_ISINKASO As String = "@IsInkaso"
    Private Const PARAM_PDCTYPE As String = "@PDCType"

#Region "PDCMultiAgreement"
    Public Function ListAgreementCustomer(ByVal oCustomClass As Parameter.PDCMultiAgreement) As Parameter.PDCMultiAgreement
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_CUSTOMERID, SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.CustomerID
            oCustomClass.AgreementList = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, SPPDCMULTIAGREEMENTLISTDEFAULT, params).Tables(0)
            Return oCustomClass
        Catch exp As Exception
            WriteException("PDCMultiAgreement", "ListAgreementCustomer", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AgreementInformation(ByVal oCustomclass As Parameter.PDCMultiAgreement) As DataTable
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomclass.BranchId
            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = oCustomclass.ApplicationID
            params(2) = New SqlParameter(PARAM_GIRONO, SqlDbType.VarChar, 20)
            params(2).Value = oCustomclass.GiroNo
            params(3) = New SqlParameter("@NumPDC", SqlDbType.Int)
            params(3).Value = oCustomclass.NumPDC
            params(4) = New SqlParameter("@PDCDueDate", SqlDbType.Date)
            params(4).Value = oCustomclass.PDCDueDate
            params(5) = New SqlParameter("@AmountReceive", SqlDbType.Decimal)
            params(5).Value = oCustomclass.AmountReceive

            Return SqlHelper.ExecuteDataset(oCustomclass.strConnection, CommandType.StoredProcedure, SPPDCMULTIAGREEMENTINFORMATION, params).Tables(0)
        Catch exp As Exception
            WriteException("PDCMultiAgreement", "AgreementInformation", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub SavePDCMultiAgreement(ByVal oCustomclass As Parameter.PDCMultiAgreement)
        Dim paramsHeader() As SqlParameter = New SqlParameter(6) {}
        Dim paramsDetail() As SqlParameter = New SqlParameter(12) {}
        Dim objcon As New SqlConnection(oCustomclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim i As Integer
        Dim Temp_GiroNO As String
        Dim w As Integer = 0
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            paramsHeader(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            paramsHeader(0).Value = oCustomclass.BranchId

            paramsHeader(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            paramsHeader(1).Value = oCustomclass.BusinessDate

            paramsHeader(2) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            paramsHeader(2).Value = oCustomclass.LoginId

            paramsHeader(3) = New SqlParameter(PARAM_RECEIVEDFROM, SqlDbType.VarChar, 50)
            paramsHeader(3).Value = oCustomclass.ReceivedFrom

            paramsHeader(4) = New SqlParameter(PARAM_GIRONO, SqlDbType.VarChar, 20)
            paramsHeader(4).Value = oCustomclass.GiroNo

            paramsHeader(5) = New SqlParameter("@NumPDC", SqlDbType.Int)
            paramsHeader(5).Value = oCustomclass.NumPDC

            paramsHeader(6) = New SqlParameter(PARAM_PDCRECEIPTNO, SqlDbType.VarChar, 20)
            paramsHeader(6).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPPDCMULTIAGREEMENTSAVEH, paramsHeader)

            paramsDetail(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            paramsDetail(1) = New SqlParameter(PARAM_PDCRECEIPTNO, SqlDbType.VarChar, 20)
            paramsDetail(2) = New SqlParameter(PARAM_GIRONO, SqlDbType.VarChar, 20)
            paramsDetail(3) = New SqlParameter(PARAM_GIROSEQNO, SqlDbType.Int)
            paramsDetail(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            paramsDetail(5) = New SqlParameter(PARAM_BRANCHAGREMENT, SqlDbType.VarChar, 3)
            paramsDetail(6) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            paramsDetail(7) = New SqlParameter(PARAM_BANKID, SqlDbType.VarChar, 5)
            paramsDetail(8) = New SqlParameter(PARAM_PAYMENTAMOUNT, SqlDbType.Decimal)
            paramsDetail(9) = New SqlParameter(PARAM_PDCDUEDATE, SqlDbType.DateTime)
            paramsDetail(10) = New SqlParameter(PARAM_PDCTYPE, SqlDbType.VarChar, 1)
            paramsDetail(11) = New SqlParameter(PARAM_ISCUMM, SqlDbType.Bit)
            paramsDetail(12) = New SqlParameter(PARAM_ISINKASO, SqlDbType.Bit)

            For i = 0 To oCustomclass.TablePDC.Rows.Count - 1
                paramsDetail(0).Value = oCustomclass.BranchId
                'paramsDetail(1).Value = paramsHeader(5).Value
                paramsDetail(1).Value = paramsHeader(6).Value
                'paramsDetail(2).Value = oCustomclass.GiroNo
                paramsDetail(2).Value = oCustomclass.TablePDC.Rows(i).Item("GiroNo")
                'paramsDetail(3).Value = i + 1
                If Temp_GiroNO = oCustomclass.TablePDC.Rows(i).Item("GiroNo") Then
                    w = w + 1
                Else
                    w = 1
                End If
                paramsDetail(3).Value = w
                paramsDetail(4).Value = oCustomclass.BusinessDate
                paramsDetail(5).Value = oCustomclass.TablePDC.Rows(i).Item("BranchAgreement")
                paramsDetail(6).Value = oCustomclass.TablePDC.Rows(i).Item("ApplicationID")
                paramsDetail(7).Value = oCustomclass.BankPDC
                paramsDetail(8).Value = oCustomclass.TablePDC.Rows(i).Item("AmountPayment")
                'paramsDetail(9).Value = oCustomclass.PDCDueDate
                'paramsDetail(9).Value = oCustomclass.TablePDC.Rows(i).Item("PDCDueDate")
                paramsDetail(9).Value = Date.ParseExact(oCustomclass.TablePDC.Rows(i).Item("PDCDueDate"), "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                paramsDetail(10).Value = oCustomclass.PDCType
                paramsDetail(11).Value = oCustomclass.IsCumm
                paramsDetail(12).Value = oCustomclass.IsInkaso
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPPDCMULTIAGREEMENTSAVED, paramsDetail)

                Temp_GiroNO = oCustomclass.TablePDC.Rows(i).Item("GiroNo")

            Next
            objtrans.Commit()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        Catch exp As Exception
            objtrans.Rollback()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
            WriteException("PDCMultiAgreement", "SavePDCMultiAgreement", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Sub
#End Region
#Region "Edit PDC Multi Agreement"
    Public Sub SaveEditPDCMultiAgreement(ByVal oCustomclass As Parameter.PDCMultiAgreement)
        Dim paramsEdit() As SqlParameter = New SqlParameter(16) {}
        Dim objcon As New SqlConnection(oCustomclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim i As Integer
        Dim DataRow As DataRow()
        Dim TablePDC As New DataTable
        Dim OldTablePDC As New DataTable
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            TablePDC = oCustomclass.TablePDC
            OldTablePDC = oCustomclass.OldTablePDC

            paramsEdit(0) = New SqlParameter(PARAM_BRANCHPDC, SqlDbType.VarChar, 3)
            paramsEdit(1) = New SqlParameter(PARAM_PDCRECEIPTNO, SqlDbType.VarChar, 20)
            paramsEdit(2) = New SqlParameter(PARAM_GIRONO, SqlDbType.VarChar, 20)
            paramsEdit(3) = New SqlParameter(PARAM_GIROSEQNO, SqlDbType.Int)
            paramsEdit(4) = New SqlParameter(PARAM_BRANCHLOCATION, SqlDbType.Int)
            paramsEdit(5) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            paramsEdit(6) = New SqlParameter(PARAM_BRANCHAGREEMENT, SqlDbType.VarChar, 3)
            paramsEdit(7) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            paramsEdit(8) = New SqlParameter(PARAM_RECEIVEDFROM, SqlDbType.VarChar, 50)
            paramsEdit(9) = New SqlParameter(PARAM_ISCUMM, SqlDbType.Bit)
            paramsEdit(10) = New SqlParameter(PARAM_ISINKASO, SqlDbType.Bit)
            paramsEdit(11) = New SqlParameter(PARAM_BANKID, SqlDbType.VarChar, 5)
            paramsEdit(12) = New SqlParameter(PARAM_PAYMENTAMOUNT, SqlDbType.Decimal)
            paramsEdit(13) = New SqlParameter(PARAM_PDCDUEDATE, SqlDbType.DateTime)
            paramsEdit(14) = New SqlParameter(PARAM_PDCTYPE, SqlDbType.VarChar, 1)
            paramsEdit(15) = New SqlParameter(PARAM_TOTALGROUPPDCAMOUNT, SqlDbType.Decimal)
            paramsEdit(16) = New SqlParameter(PARAM_PROCESSID, SqlDbType.Char, 1)

            For i = 0 To OldTablePDC.Rows.Count - 1
                DataRow = TablePDC.Select(" ApplicationID  = '" & OldTablePDC.Rows(i).Item("ApplicationID").ToString.Trim & "'")
                If DataRow.Length = 0 Then
                    paramsEdit(0).Value = oCustomclass.BranchPDC
                    paramsEdit(1).Value = oCustomclass.PDCReceiptNo
                    paramsEdit(2).Value = oCustomclass.GiroNo
                    paramsEdit(3).Value = i + 1
                    paramsEdit(16).Value = "D"
                    SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPPDCEDITMULTIAGREEMENTSAVEH, paramsEdit)
                End If
            Next

            For i = 0 To TablePDC.Rows.Count - 1
                paramsEdit(0).Value = oCustomclass.BranchPDC
                paramsEdit(1).Value = oCustomclass.PDCReceiptNo
                paramsEdit(2).Value = oCustomclass.GiroNo
                paramsEdit(3).Value = i + 1
                paramsEdit(4).Value = oCustomclass.BranchLocation
                paramsEdit(5).Value = oCustomclass.BusinessDate
                paramsEdit(6).Value = oCustomclass.TablePDC.Rows(i).Item("BranchAgreement")
                paramsEdit(7).Value = oCustomclass.TablePDC.Rows(i).Item("ApplicationID")
                paramsEdit(8).Value = oCustomclass.ReceivedFrom
                paramsEdit(9).Value = oCustomclass.IsCumm
                paramsEdit(10).Value = oCustomclass.IsInkaso
                paramsEdit(11).Value = oCustomclass.BankPDC
                paramsEdit(12).Value = oCustomclass.TablePDC.Rows(i).Item("AmountPayment")
                paramsEdit(13).Value = oCustomclass.PDCDueDate
                paramsEdit(14).Value = oCustomclass.PDCType
                paramsEdit(15).Value = oCustomclass.TotalPDCGroupAmount
                paramsEdit(16).Value = ""
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPPDCEDITMULTIAGREEMENTSAVEH, paramsEdit)
            Next
            objtrans.Commit()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        Catch exp As Exception
            objtrans.Rollback()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
            WriteException("PDCMultiAgreement", "SaveEditPDCMultiAgreement", exp.Message)
            Throw New Exception(exp.Message)
        End Try
    End Sub
#End Region

    Public Function GetEditPDCMultiAgreement(ByVal oCustomClass As Parameter.PDCMultiAgreement) As Parameter.PDCMultiAgreement
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim objread As SqlDataReader
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_PDCRECEIPTNO, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.PDCReceiptNo
            params(2) = New SqlParameter(PARAM_GIRONO, SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.GiroNo
            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, SPPDCEDITMULTIAGREEMENTHEADERINFORMATION, params)
            If objread.Read Then
                With oCustomClass
                    .IsCumm = CBool(objread("IsCumm"))
                    .PDCType = objread("PDCType").ToString
                    .PDCAmount = CDbl(objread("PDCAmount"))
                    .BankID = objread("BankID").ToString
                    .PDCDueDate = CDate(objread("DueDate"))
                    .ReceivedFrom = objread("ReceivedFrom").ToString
                    .BranchPDC = objread("BranchPDC").ToString
                    .BranchLocation = objread("BranchLocation").ToString
                    .TotalPDCGroupAmount = CDbl(objread("TotalPDCGroupAmount"))
                End With
            End If
            objread.Close()
            oCustomClass.AgreementList = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, SPPDCEDITMULTIAGREEMENTDETAILINFORMATION, params).Tables(0)
            Return oCustomClass
        Catch exp As Exception
            WriteException("PDCMultiAgreement", "GetEditPDCMultiAgreement", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
