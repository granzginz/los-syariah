
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class PaymentHistory : Inherits AccMntBase
#Region "Procedure"
    Private Const PROC_LISTPAYMENTHISTORYHEADER As String = "spPaymentHistoryHeaderView"
    Private Const PROC_LISTPAYMENTHISTORYDETAIL As String = "spPaymentHistoryDetailView"
    Private Const PROC_LISTPAYMENTHISTORYDETAILOTOR As String = "spPaymentHistoryDetailViewOtor"
#End Region

#Region "Parameters"
    Private Const PARAM_HISTORYSEQUENCENO As String = "@HistorySequenceNo"
    Private Const PARAM_TRANSACTIONTYPE As String = "@TransactionType"
    Private Const PARAM_STATUS As String = "@Status"
    Private Const PARAM_STATUSDATE As String = "@StatusDate"
    Private Const PARAM_PAYMENTTYPEID As String = "@PaymentTypeID"
    Private Const PARAM_CUSTOMERID As String = "@CustomerId"

#End Region
    Public Function ListPaymentHistory(ByVal oCustomClass As Parameter.PaymentHistory) As Parameter.PaymentHistory
        Dim oEntities As New Parameter.PaymentHistory
        Dim params() As SqlParameter = New SqlParameter(9) {}
        Try
            'params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            'params(0).Value = oCustomClass.BranchId

            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.ApplicationID

            params(1) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(1).Value = oCustomClass.CurrentPage

            params(2) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(2).Value = oCustomClass.PageSize

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = oCustomClass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            params(5) = New SqlParameter(PARAM_CUSTOMERNAME, SqlDbType.VarChar, 50)
            params(5).Direction = ParameterDirection.Output

            params(6) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.VarChar, 20)
            params(6).Direction = ParameterDirection.Output

            params(7) = New SqlParameter(PARAM_AMOUNTRECEIVE, SqlDbType.Decimal)
            params(7).Direction = ParameterDirection.Output

            params(8) = New SqlParameter(PARAM_PREPAIDAMOUNT, SqlDbType.Decimal)
            params(8).Direction = ParameterDirection.Output

            params(9) = New SqlParameter(PARAM_CUSTOMERID, SqlDbType.VarChar, 20)
            params(9).Direction = ParameterDirection.Output

            oCustomClass.ListPaymentHistoryHeader = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, PROC_LISTPAYMENTHISTORYHEADER, params).Tables(0)
            oCustomClass.TotalRecord = CInt(params(4).Value)
            oCustomClass.CustomerName = CStr(params(5).Value)
            oCustomClass.Agreementno = CStr(params(6).Value)
            oCustomClass.AmountReceive = CDbl(params(7).Value)
            oCustomClass.Prepaid = CDbl(params(8).Value)
            oCustomClass.CustomerID = CStr(params(9).Value)
            Return oCustomClass
        Catch exp As Exception
            WriteException("PaymentHistory", "ListPaymentHistory", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListPaymentHistoryDetail(ByVal oCustomClass As Parameter.PaymentHistory) As Parameter.PaymentHistory
        Dim oEntities As New Parameter.PaymentHistory
        Dim params() As SqlParameter = New SqlParameter(22) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.ApplicationID

            params(1) = New SqlParameter(PARAM_HISTORYSEQUENCENO, SqlDbType.Int)
            params(1).Value = oCustomClass.HistorySequenceNo

            params(2) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(2).Value = oCustomClass.CurrentPage

            params(3) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(3).Value = oCustomClass.PageSize

            params(4) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(4).Value = oCustomClass.SortBy

            params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output

            params(6) = New SqlParameter(PARAM_CUSTOMERNAME, SqlDbType.VarChar, 50)
            params(6).Direction = ParameterDirection.Output

            params(7) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.VarChar, 20)
            params(7).Direction = ParameterDirection.Output

            params(8) = New SqlParameter(PARAM_TRANSACTIONTYPE, SqlDbType.VarChar, 50)
            params(8).Direction = ParameterDirection.Output

            params(9) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(9).Direction = ParameterDirection.Output

            params(10) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(10).Direction = ParameterDirection.Output

            params(11) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            params(11).Direction = ParameterDirection.Output

            params(12) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50)
            params(12).Direction = ParameterDirection.Output

            params(13) = New SqlParameter(PARAM_STATUS, SqlDbType.VarChar, 20)
            params(13).Direction = ParameterDirection.Output

            params(14) = New SqlParameter(PARAM_STATUSDATE, SqlDbType.VarChar, 10)
            params(14).Direction = ParameterDirection.Output

            params(15) = New SqlParameter(PARAM_PAYMENTTYPEID, SqlDbType.VarChar, 20)
            params(15).Direction = ParameterDirection.Output

            params(16) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 50)
            params(16).Direction = ParameterDirection.Output

            params(17) = New SqlParameter("@ReceiptNo", SqlDbType.VarChar, 50)
            params(17).Direction = ParameterDirection.Output

            params(18) = New SqlParameter("@PrintBy", SqlDbType.VarChar, 50)
            params(18).Direction = ParameterDirection.Output

            params(19) = New SqlParameter("@NumOfPrint", SqlDbType.Int)
            params(19).Direction = ParameterDirection.Output

            params(20) = New SqlParameter("@LastPrintDate", SqlDbType.Char, 10)
            params(20).Direction = ParameterDirection.Output

            params(21) = New SqlParameter("@BankName", SqlDbType.VarChar, 10)
            params(21).Direction = ParameterDirection.Output

            params(22) = New SqlParameter("@GiroNo", SqlDbType.VarChar, 20)
            params(22).Direction = ParameterDirection.Output

            With oCustomClass
                .ListPaymentHistoryDetail = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, PROC_LISTPAYMENTHISTORYDETAIL, params).Tables(0)
                .TotalRecord = CInt(params(5).Value)
                .CustomerName = CStr(params(6).Value)
                .Agreementno = CStr(params(7).Value)
                .TransactionType = CStr(params(8).Value)
                .BusinessDate = CDate(params(9).Value)
                .ValueDate = CDate(params(10).Value)
                .ReceivedFrom = CStr(params(11).Value)
                .ReferenceNo = CStr(params(12).Value)
                .Status = CStr(params(13).Value)
                .StatusDate = CStr(params(14).Value)
                .PaymentTypeID = CStr(params(15).Value)
                .BankAccountID = CStr(params(16).Value)
                .ReceiptNo = CStr(params(17).Value)
                .PrintBy = CStr(params(18).Value)
                .NumOfPrint = CInt(params(19).Value)
                .LastPrintDate = CStr(params(20).Value)
                .BankName = CStr(params(21).Value)
                .GiroNo = CStr(params(22).Value)
            End With
            Return oCustomClass
        Catch exp As Exception
            WriteException("PaymentHistory", "ListPaymentHistoryDetail", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Function ListPaymentHistoryDetailOtor(ByVal oCustomClass As Parameter.PaymentHistory) As Parameter.PaymentHistory
        Dim oEntities As New Parameter.PaymentHistory
        Dim params() As SqlParameter = New SqlParameter(24) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.ApplicationID

            params(1) = New SqlParameter(PARAM_HISTORYSEQUENCENO, SqlDbType.Int)
            params(1).Value = oCustomClass.HistorySequenceNo

            params(2) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(2).Value = oCustomClass.CurrentPage

            params(3) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(3).Value = oCustomClass.PageSize

            params(4) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(4).Value = oCustomClass.SortBy

            params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output

            params(6) = New SqlParameter(PARAM_CUSTOMERNAME, SqlDbType.VarChar, 50)
            params(6).Direction = ParameterDirection.Output

            params(7) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.VarChar, 20)
            params(7).Direction = ParameterDirection.Output

            params(8) = New SqlParameter(PARAM_TRANSACTIONTYPE, SqlDbType.VarChar, 50)
            params(8).Direction = ParameterDirection.Output

            params(9) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(9).Direction = ParameterDirection.Output

            params(10) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(10).Direction = ParameterDirection.Output

            params(11) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            params(11).Direction = ParameterDirection.Output

            params(12) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50)
            params(12).Direction = ParameterDirection.Output

            params(13) = New SqlParameter(PARAM_STATUS, SqlDbType.VarChar, 20)
            params(13).Direction = ParameterDirection.Output

            params(14) = New SqlParameter(PARAM_STATUSDATE, SqlDbType.VarChar, 10)
            params(14).Direction = ParameterDirection.Output

            params(15) = New SqlParameter(PARAM_PAYMENTTYPEID, SqlDbType.VarChar, 20)
            params(15).Direction = ParameterDirection.Output

            params(16) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 50)
            params(16).Direction = ParameterDirection.Output

            params(17) = New SqlParameter("@ReceiptNo", SqlDbType.VarChar, 50)
            params(17).Direction = ParameterDirection.Output

            params(18) = New SqlParameter("@PrintBy", SqlDbType.VarChar, 50)
            params(18).Direction = ParameterDirection.Output

            params(19) = New SqlParameter("@NumOfPrint", SqlDbType.Int)
            params(19).Direction = ParameterDirection.Output

            params(20) = New SqlParameter("@LastPrintDate", SqlDbType.Char, 10)
            params(20).Direction = ParameterDirection.Output

            params(21) = New SqlParameter("@BankName", SqlDbType.VarChar, 10)
            params(21).Direction = ParameterDirection.Output

            params(22) = New SqlParameter("@GiroNo", SqlDbType.VarChar, 20)
            params(22).Direction = ParameterDirection.Output

            params(23) = New SqlParameter("@ReferenceNoOut", SqlDbType.VarChar, 20)
            params(23).Direction = ParameterDirection.Output

            params(24) = New SqlParameter("@ReasonReversal", SqlDbType.VarChar, 20)
            params(24).Direction = ParameterDirection.Output

            With oCustomClass
                .ListPaymentHistoryDetail = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, PROC_LISTPAYMENTHISTORYDETAILOTOR, params).Tables(0)
                .TotalRecord = CInt(params(5).Value)
                .CustomerName = CStr(params(6).Value)
                .Agreementno = CStr(params(7).Value)
                .TransactionType = CStr(params(8).Value)
                .BusinessDate = CDate(params(9).Value)
                .ValueDate = CDate(params(10).Value)
                .ReceivedFrom = CStr(params(11).Value)
                .ReferenceNo = CStr(params(12).Value)
                .Status = CStr(params(13).Value)
                .StatusDate = CStr(params(14).Value)
                .PaymentTypeID = CStr(params(15).Value)
                .BankAccountID = CStr(params(16).Value)
                .ReceiptNo = CStr(params(17).Value)
                .PrintBy = CStr(params(18).Value)
                .NumOfPrint = CInt(params(19).Value)
                .LastPrintDate = CStr(params(20).Value)
                .BankName = CStr(params(21).Value)
                .GiroNo = CStr(params(22).Value)
                .ReferenceNoOut = CStr(params(23).Value)
                .ReasonReversal = CStr(params(24).Value)
            End With
            Return oCustomClass
        Catch exp As Exception
            WriteException("PaymentHistory", "ListPaymentHistoryDetail", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ReversalTransaksiLainnyaSave(ByVal m_oCustomClass As Parameter.PaymentHistory) As Parameter.PaymentHistory
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.PaymentHistory

        params(0) = New SqlParameter("@VoucherNo", SqlDbType.VarChar, 20)
        params(0).Value = m_oCustomClass.ReferenceNo

        params(1) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(1).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(m_oCustomClass.strConnection, CommandType.StoredProcedure, "spReversalTransaksiLainnyaSave", params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("spReversalTransaksiLainnyaSave")
        End Try
    End Function

    Public Function ListPaymentHistoryDetailFactoring(ByVal oCustomClass As Parameter.PaymentHistory) As Parameter.PaymentHistory
        Dim oEntities As New Parameter.PaymentHistory
        Dim params() As SqlParameter = New SqlParameter(24) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.ApplicationID

            params(1) = New SqlParameter(PARAM_HISTORYSEQUENCENO, SqlDbType.Int)
            params(1).Value = oCustomClass.HistorySequenceNo

            params(2) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(2).Value = oCustomClass.CurrentPage

            params(3) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(3).Value = oCustomClass.PageSize

            params(4) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(4).Value = oCustomClass.SortBy

            params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output

            params(6) = New SqlParameter(PARAM_CUSTOMERNAME, SqlDbType.VarChar, 50)
            params(6).Direction = ParameterDirection.Output

            params(7) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.VarChar, 20)
            params(7).Direction = ParameterDirection.Output

            params(8) = New SqlParameter(PARAM_TRANSACTIONTYPE, SqlDbType.VarChar, 50)
            params(8).Direction = ParameterDirection.Output

            params(9) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(9).Direction = ParameterDirection.Output

            params(10) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(10).Direction = ParameterDirection.Output

            params(11) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            params(11).Direction = ParameterDirection.Output

            params(12) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50)
            params(12).Direction = ParameterDirection.Output

            params(13) = New SqlParameter(PARAM_STATUS, SqlDbType.VarChar, 20)
            params(13).Direction = ParameterDirection.Output

            params(14) = New SqlParameter(PARAM_STATUSDATE, SqlDbType.VarChar, 10)
            params(14).Direction = ParameterDirection.Output

            params(15) = New SqlParameter(PARAM_PAYMENTTYPEID, SqlDbType.VarChar, 20)
            params(15).Direction = ParameterDirection.Output

            params(16) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 50)
            params(16).Direction = ParameterDirection.Output

            params(17) = New SqlParameter("@ReceiptNo", SqlDbType.VarChar, 50)
            params(17).Direction = ParameterDirection.Output

            params(18) = New SqlParameter("@PrintBy", SqlDbType.VarChar, 50)
            params(18).Direction = ParameterDirection.Output

            params(19) = New SqlParameter("@NumOfPrint", SqlDbType.Int)
            params(19).Direction = ParameterDirection.Output

            params(20) = New SqlParameter("@LastPrintDate", SqlDbType.Char, 10)
            params(20).Direction = ParameterDirection.Output

            params(21) = New SqlParameter("@BankName", SqlDbType.VarChar, 10)
            params(21).Direction = ParameterDirection.Output

            params(22) = New SqlParameter("@GiroNo", SqlDbType.VarChar, 20)
            params(22).Direction = ParameterDirection.Output

            params(23) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
            params(23).Value = oCustomClass.InvoiceNo.Trim

            params(24) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(24).Value = oCustomClass.InvoiceSeqNo

            With oCustomClass
                .ListPaymentHistoryDetail = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spPaymentHistoryDetailViewFactoring", params).Tables(0)
                .TotalRecord = CInt(params(5).Value)
                .CustomerName = CStr(params(6).Value)
                .Agreementno = CStr(params(7).Value)
                .TransactionType = CStr(params(8).Value)
                .BusinessDate = CDate(params(9).Value)
                .ValueDate = CDate(params(10).Value)
                .ReceivedFrom = CStr(params(11).Value)
                .ReferenceNo = CStr(params(12).Value)
                .Status = CStr(params(13).Value)
                .StatusDate = CStr(params(14).Value)
                .PaymentTypeID = CStr(params(15).Value)
                .BankAccountID = CStr(params(16).Value)
                .ReceiptNo = CStr(params(17).Value)
                .PrintBy = CStr(params(18).Value)
                .NumOfPrint = CInt(params(19).Value)
                .LastPrintDate = CStr(params(20).Value)
                .BankName = CStr(params(21).Value)
                .GiroNo = CStr(params(22).Value)
            End With
            Return oCustomClass
        Catch exp As Exception
            WriteException("PaymentHistory", "spPaymentHistoryDetailViewFactoring", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListPaymentHistoryDetailModalKerja(ByVal oCustomClass As Parameter.PaymentHistory) As Parameter.PaymentHistory
        Dim oEntities As New Parameter.PaymentHistory
        Dim params() As SqlParameter = New SqlParameter(22) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.ApplicationID

            params(1) = New SqlParameter(PARAM_HISTORYSEQUENCENO, SqlDbType.Int)
            params(1).Value = oCustomClass.HistorySequenceNo

            params(2) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(2).Value = oCustomClass.CurrentPage

            params(3) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(3).Value = oCustomClass.PageSize

            params(4) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(4).Value = oCustomClass.SortBy

            params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output

            params(6) = New SqlParameter(PARAM_CUSTOMERNAME, SqlDbType.VarChar, 50)
            params(6).Direction = ParameterDirection.Output

            params(7) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.VarChar, 20)
            params(7).Direction = ParameterDirection.Output

            params(8) = New SqlParameter(PARAM_TRANSACTIONTYPE, SqlDbType.VarChar, 50)
            params(8).Direction = ParameterDirection.Output

            params(9) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(9).Direction = ParameterDirection.Output

            params(10) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(10).Direction = ParameterDirection.Output

            params(11) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            params(11).Direction = ParameterDirection.Output

            params(12) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50)
            params(12).Direction = ParameterDirection.Output

            params(13) = New SqlParameter(PARAM_STATUS, SqlDbType.VarChar, 20)
            params(13).Direction = ParameterDirection.Output

            params(14) = New SqlParameter(PARAM_STATUSDATE, SqlDbType.VarChar, 10)
            params(14).Direction = ParameterDirection.Output

            params(15) = New SqlParameter(PARAM_PAYMENTTYPEID, SqlDbType.VarChar, 20)
            params(15).Direction = ParameterDirection.Output

            params(16) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 50)
            params(16).Direction = ParameterDirection.Output

            params(17) = New SqlParameter("@ReceiptNo", SqlDbType.VarChar, 50)
            params(17).Direction = ParameterDirection.Output

            params(18) = New SqlParameter("@PrintBy", SqlDbType.VarChar, 50)
            params(18).Direction = ParameterDirection.Output

            params(19) = New SqlParameter("@NumOfPrint", SqlDbType.Int)
            params(19).Direction = ParameterDirection.Output

            params(20) = New SqlParameter("@LastPrintDate", SqlDbType.Char, 10)
            params(20).Direction = ParameterDirection.Output

            params(21) = New SqlParameter("@BankName", SqlDbType.VarChar, 10)
            params(21).Direction = ParameterDirection.Output

            params(22) = New SqlParameter("@GiroNo", SqlDbType.VarChar, 20)
            params(22).Direction = ParameterDirection.Output

            With oCustomClass
                .ListPaymentHistoryDetail = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spPaymentHistoryDetailViewModalKerja", params).Tables(0)
                .TotalRecord = CInt(params(5).Value)
                .CustomerName = CStr(params(6).Value)
                .Agreementno = CStr(params(7).Value)
                .TransactionType = CStr(params(8).Value)
                .BusinessDate = CDate(params(9).Value)
                .ValueDate = CDate(params(10).Value)
                .ReceivedFrom = CStr(params(11).Value)
                .ReferenceNo = CStr(params(12).Value)
                .Status = CStr(params(13).Value)
                .StatusDate = CStr(params(14).Value)
                .PaymentTypeID = CStr(params(15).Value)
                .BankAccountID = CStr(params(16).Value)
                .ReceiptNo = CStr(params(17).Value)
                .PrintBy = CStr(params(18).Value)
                .NumOfPrint = CInt(params(19).Value)
                .LastPrintDate = CStr(params(20).Value)
                .BankName = CStr(params(21).Value)
                .GiroNo = CStr(params(22).Value)
            End With
            Return oCustomClass
        Catch exp As Exception
            WriteException("PaymentHistory", "spPaymentHistoryDetailViewModalKerja", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ReversalPembayaranTDPSave(ByVal m_oCustomClass As Parameter.PaymentHistory) As Parameter.PaymentHistory
        'Public Sub ReversalPembayaranTDPSave(ByVal m_oCustomClass As Parameter.PaymentHistory)
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.PaymentHistory
        Dim ErrMessage As String = ""
        Dim transaction As SqlTransaction = Nothing
        Dim conn As New SqlConnection(m_oCustomClass.strConnection)

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            params(0) = New SqlParameter("@VoucherNo", SqlDbType.VarChar, 20)
            params(0).Value = m_oCustomClass.ReferenceNo

            params(1) = New SqlParameter("Err", SqlDbType.VarChar, 50)
            params(1).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spReversalPembayaranTDPSave", params)

            ErrMessage = CType(params(1).Value, String)

            If ErrMessage <> "" Then
                oReturnValue.Err = ErrMessage
                transaction.Rollback()
            Else
                transaction.Commit()
            End If
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception("Error On spReversalPembayaranTDPSave" + exp.Message)
            Return oReturnValue
        Finally
            If conn.State = ConnectionState.Closed Then conn.Open()
            conn.Dispose()
        End Try

    End Function
End Class