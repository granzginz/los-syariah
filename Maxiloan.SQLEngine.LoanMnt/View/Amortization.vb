

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class Amortization
    Inherits AccMntBase
    Private Const spAmortization As String = "spAmortizationList"
    Public Function ListAmortization(ByVal customclass As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim params() As SqlParameter = New SqlParameter(6) {}
        Try

            params(0) = New SqlParameter("@Applicationid", SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter("@branchid", SqlDbType.Char, 3)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(2).Value = customclass.SortBy

            params(3) = New SqlParameter("@agreementno", SqlDbType.Char, 20)
            params(3).Direction = ParameterDirection.Output

            params(4) = New SqlParameter("@customerid", SqlDbType.Char, 20)
            params(4).Direction = ParameterDirection.Output

            params(5) = New SqlParameter("@Customertype", SqlDbType.Char, 1)
            params(5).Direction = ParameterDirection.Output

            params(6) = New SqlParameter("@Customername", SqlDbType.Char, 50)
            params(6).Direction = ParameterDirection.Output

            customclass.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAmortization, params).Tables(0)
            customclass.Agreementno = CStr(params(3).Value)
            customclass.CustomerID = CStr(params(4).Value)
            customclass.CustomerType = CStr(params(5).Value)
            customclass.CustomerName = CStr(params(6).Value)
            Return customclass
        Catch exp As Exception
            WriteException("Amortization", "ListAmortization", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
