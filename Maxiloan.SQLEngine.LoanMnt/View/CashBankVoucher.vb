
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class CashBankVoucher : Inherits DataAccessBase

    Public Function ListCashBankVoucher(ByVal oCustomClass As Parameter.CashBankVoucher) As Parameter.CashBankVoucher
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId

            params(1) = New SqlParameter("@VoucherNo", SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.VoucherNo

            params(2) = New SqlParameter("@OrderBy", SqlDbType.VarChar, 100)
            params(2).Value = oCustomClass.SortBy

            oCustomClass.ListCashBankVoucher = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewCashBankVoucher", params).Tables(0)
            Return oCustomClass
        Catch exp As Exception
            WriteException("CashBankVoucher", "ListCashBankVoucher", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
