﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions 

Public Class InstallRcv3rdDA : Inherits Maxiloan.SQLEngine.DataAccessBase


    Public Function DoUploadFileInstallment(cnn As String, rows As Parameter.InstallRcv3rd, sesId As String, spName As String) As String
        Try
            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter) 
            params.Add(New SqlParameter("@fileData", SqlDbType.Structured) With {.Value = rows.ToTable})
            params.Add(New SqlParameter("@sesionId", SqlDbType.VarChar, 50) With {.Value = sesId})
            Dim prmErr = New SqlParameter("@errMsg", SqlDbType.VarChar, 200) With {.Direction = ParameterDirection.Output}
            params.Add(prmErr) 
            SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, spName, params.ToArray) 
            If (prmErr.Value <> "") Then
                Throw New Exception(prmErr.Value)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

        Return ""
    End Function
    Function gUploadFilePage(cnn As String, params As List(Of SqlParameter), spName As String) As Parameter.InstallRcv3rd
        Dim oReturnValue As New Parameter.InstallRcv3rd
        Try
            Dim totParam = New SqlParameter("@TotalRecords", SqlDbType.Int) With {.Direction = ParameterDirection.Output}
            params.Add(totParam)
            oReturnValue.DataSetResult = SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, spName, params.ToArray)
            oReturnValue.TotalRecords = CType(totParam.Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetUploadFilePage(cnn As String, currentPage As Integer, pageSize As Integer, sesId As String, spName As String) As Parameter.InstallRcv3rd 
        Dim params = New List(Of SqlParameter)
        params.Add(New SqlParameter("@CurrentPage", SqlDbType.Int) With {.Value = currentPage})
        params.Add(New SqlParameter("@PageSize", SqlDbType.Int) With {.Value = pageSize})
        params.Add(New SqlParameter("@SessionId ", SqlDbType.VarChar, 20) With {.Value = sesId})
        Return gUploadFilePage(cnn, params, spName)
    End Function


    Public Function GetUploadFilePage(cnn As String, currentPage As Integer, pageSize As Integer, sesId As DateTime, spName As String) As Parameter.InstallRcv3rd
        Dim params = New List(Of SqlParameter)
        params.Add(New SqlParameter("@CurrentPage", SqlDbType.Int) With {.Value = currentPage})
        params.Add(New SqlParameter("@PageSize", SqlDbType.Int) With {.Value = pageSize})
        params.Add(New SqlParameter("@SessionId ", SqlDbType.DateTime) With {.Value = sesId})
        Return gUploadFilePage(cnn, params, spName)
    End Function

    Public Function PostingInstallmentUpload(cnn As String, compid As String, branchid As String, valuedate As DateTime, bsnDate As DateTime, login As String, spName As String) As String
         Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            params.Add(New SqlParameter("@CompanyID", SqlDbType.VarChar, 3) With {.Value = compid})
            params.Add(New SqlParameter("@branchid", SqlDbType.VarChar, 3) With {.Value = branchid})
            params.Add(New SqlParameter("@valuedate", SqlDbType.DateTime) With {.Value = valuedate})
            params.Add(New SqlParameter("@businessdate", SqlDbType.DateTime) With {.Value = bsnDate})
            params.Add(New SqlParameter("@loginid", SqlDbType.VarChar, 20) With {.Value = login})
            Dim prmErr = New SqlParameter("@msg", SqlDbType.VarChar, 200) With {.Direction = ParameterDirection.Output}
            params.Add(prmErr)
            SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, spName, params.ToArray)
            Return prmErr.Value
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return ""
    End Function

    Public Function GeneratePaymentGatewayFile(cnn As String, processDate As DateTime) As Parameter.InstallRcv3rd
        Dim returnVal = New Parameter.InstallRcv3rd
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            returnVal.UploadDate = processDate
            returnVal.PaymentGateway3rd = PaymentGateway3rd(cnn)

            params.Add(New SqlParameter("@nextInstallmentDate", SqlDbType.Date) With {.Value = processDate})
            Dim prmTot = New SqlParameter("@lastSeq", SqlDbType.Int) With {.Direction = ParameterDirection.Output}
            params.Add(prmTot)

            Dim dt As DataTable = SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, "spPaymentGatewayAgreementList", params.ToArray).Tables(0)
            Dim val = dt.AsEnumerable().Select(Function(r) New Parameter.InstallRcv3rdCols(
                                r.Field(Of String)("ContractNumber"),
                                r.Field(Of String)("NameOfCustomer"),
                                r.Field(Of String)("PoliceNumber"),
                                r.Field(Of Date)("DateOverDue"),
                                r.Field(Of Integer)("PeriodOfTennor"),
                                r.Field(Of Decimal)("installment"),
                                r.Field(Of Decimal)("Pinalty"))).ToList()

            returnVal.InstallRcv3rdRows = val
            returnVal.LastSeq = CType(prmTot.Value, Integer)

            Return returnVal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Function PaymentGateway3rd(cnn As String) As IList(Of Parameter.PaymentGateway3rd)
        Try
            Dim query = "select pgID , pgName, pgSettelmentBankAccount, pgFeeTrans , pgTanggugKonsumen , pgPpnAtasFee , pgPph23AtasFee , pgFeeSwitching , pgPpnAtasFeeSwitching , pgPph23AtasFeeSwitching from paymentgateway"
            Dim dt As DataTable = SqlHelper.ExecuteDataset(cnn, CommandType.Text, query).Tables(0)
            Dim result = New List(Of Parameter.PaymentGateway3rd)
            Return dt.AsEnumerable().Select(Function(r) New Parameter.PaymentGateway3rd(
                                                r.Field(Of String)("pgID"),
                                                r.Field(Of String)("pgName"),
                                                r.Field(Of String)("pgSettelmentBankAccount"),
                                                r.Field(Of Decimal)("pgFeeTrans"),
                                                r.Field(Of Decimal)("pgTanggugKonsumen"),
                                                r.Field(Of Decimal)("pgPpnAtasFee"),
                                                r.Field(Of Decimal)("pgPph23AtasFee"),
                                                r.Field(Of Decimal)("pgFeeSwitching"),
                                                r.Field(Of Decimal)("pgPpnAtasFeeSwitching"),
                                                r.Field(Of Decimal)("pgPph23AtasFeeSwitching"))
                                            ).ToList()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Function PaymentGateway3rd(cnn As String, pgId As String) As Parameter.PaymentGateway3rd
        Try
            Dim query = "select pgID , pgName, pgSettelmentBankAccount, pgFeeTrans , pgTanggugKonsumen , pgPpnAtasFee , pgPph23AtasFee , pgFeeSwitching , pgPpnAtasFeeSwitching , pgPph23AtasFeeSwitching, pgTanggungMF,  ARID, ARPPH23ID, AdminFeeID, ISNULL(APID,'') APID from paymentgateway where pgID=@pgID"
            Dim dt As DataTable = SqlHelper.ExecuteDataset(cnn, CommandType.Text, query, New SqlParameter("@pgID", SqlDbType.Char, 20) With {.Value = pgId}).Tables(0)
            Dim result = New List(Of Parameter.PaymentGateway3rd)
            Return dt.AsEnumerable().Select(Function(r) New Parameter.PaymentGateway3rd(
                                                r.Field(Of String)("pgID"),
                                                r.Field(Of String)("pgName"),
                                                r.Field(Of String)("pgSettelmentBankAccount"),
                                                r.Field(Of Decimal)("pgFeeTrans"),
                                                r.Field(Of Decimal)("pgTanggugKonsumen"),
                                                r.Field(Of Decimal)("pgPpnAtasFee"),
                                                r.Field(Of Decimal)("pgPph23AtasFee"),
                                                r.Field(Of Decimal)("pgFeeSwitching"),
                                                r.Field(Of Decimal)("pgPpnAtasFeeSwitching"),
                                                r.Field(Of Decimal)("pgPph23AtasFeeSwitching"),
                                                r.Field(Of Decimal)("pgTanggungMF"),
                                                r.Field(Of String)("ARID"),
                                                r.Field(Of String)("ARPPH23ID"),
                                                r.Field(Of String)("AdminFeeID"),
                                                r.Field(Of String)("APID")
                                                )
                                            ).ToList().First
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Sub PaymentGatewayAgreementLog(cnn As String, param As Parameter.InstallRcv3rd)
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            params.Add(New SqlParameter("@DateOfUpload", SqlDbType.Date) With {.Value = param.UploadDate})
            params.Add(New SqlParameter("@SequenceNo", SqlDbType.SmallInt) With {.Value = param.LastSeq})
            params.Add(New SqlParameter("@IndomaretFileName", SqlDbType.VarChar, 100) With {.Value = param.IndomaretFileName})
            params.Add(New SqlParameter("@FinnetFileName", SqlDbType.VarChar, 100) With {.Value = param.FinnetFileName})
            params.Add(New SqlParameter("@PtPosFileName", SqlDbType.VarChar, 100) With {.Value = param.PosFileName})
            params.Add(New SqlParameter("@InstallmentCount", SqlDbType.Int) With {.Value = param.InstallRcv3rdRows.Count})
            params.Add(New SqlParameter("@TotalInstallment", SqlDbType.Decimal) With {.Value = param.TotalInstallment})
            params.Add(New SqlParameter("@FileDirectory", SqlDbType.VarChar, 100) With {.Value = param.FileDirectory})
            params.Add(New SqlParameter("@PermataFileName", SqlDbType.VarChar, 100) With {.Value = param.PermataFileName})
            params.Add(New SqlParameter("@BCAFileName", SqlDbType.VarChar, 100) With {.Value = param.BCAFileName})
            SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, "spPaymentGatewayAgreement3rdLog", params.ToArray)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Function GetPaymentGatewayAgreementLog(cnn As String, processDate As DateTime) As DataTable
        Dim query = "select top 1 DateOfUpload,  IndomaretFileName, FinnetFileName, PtPosFileName, InstallmentCount, TotalInstallment,FileDirectory, PermataFileName, BCAFileName from PaymentGatewayAgreement3rdLog where DateOfUpload=@DateOfUpload order by id desc"
        Return SqlHelper.ExecuteDataset(cnn, CommandType.Text, query, New SqlParameter("@DateOfUpload", SqlDbType.Date) With {.Value = processDate}).Tables(0)
    End Function

    Function buildTable(data As IList(Of String)) As DataTable
        Dim dt = New DataTable()
        dt.Columns.Add("id", GetType(String))
        dt.Columns.Add("value1", GetType(String))
        dt.Columns.Add("value2", GetType(String))

        For Each v In data
            Dim row = dt.NewRow()
            row("id") = v
            row("value1") = v
            row("value1") = v
            dt.Rows.Add(row)
        Next

        Return dt
    End Function

    Public Function DoPostingInstallmentUpload(cnn As String, branchHo As String, processDate As DateTime, businessDate As DateTime, companyId As String, vtype As Parameter.EnViaType, listKey As IList(Of String)) As String ', vtype As Parameter.EnViaType 
        Try
            Dim sp_name As String
            Dim pgId As String
            Select Case vtype
                Case Parameter.EnViaType.Indomaret
                    sp_name = "spGetPostingInstallmentIndomaret"
                    pgId = "INDO"
                Case Parameter.EnViaType.POS
                    sp_name = "spGetPostingInstallmentPOS"
                    pgId = "POS"
                Case Parameter.EnViaType.Finnet
                    sp_name = "spGetPostingInstallmentFinnet"
                    pgId = "FINNET"
                Case Parameter.EnViaType.Permata
                    sp_name = "spGetPostingInstallmentPermata"
                    pgId = "PERMATA"
            End Select

            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
            Dim postParams As IList(Of SqlParameter) = New List(Of SqlParameter)
            Dim res = PaymentGateway3rd(cnn, pgId)
            postParams.Add(New SqlParameter("@LogIdNo", SqlDbType.Structured) With {.Value = buildTable(listKey)})
            postParams.Add(New SqlParameter("@valuedate", SqlDbType.DateTime) With {.Value = processDate})

            Dim dt = SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, sp_name, postParams.ToArray).Tables(0)

            Dim result = New List(Of Parameter.InstallRcv3rdUpladedLog)
            result = dt.AsEnumerable().Select(Function(r) New Parameter.InstallRcv3rdUpladedLog(
                                                r.Field(Of String)("Branchid"),
                                                r.Field(Of String)("ApplicationID"),
                                                r.Field(Of Decimal)("InstallmentAmount"),
                                                r.Field(Of String)("customerid"),
                                                r.Field(Of Decimal)("InsntNo"),
                                                r.Field(Of Date)("InstDate"),
                                                r.Field(Of String)("CustomerName"),
                                                r.Field(Of String)("LogId")
                                                )
                                            ).ToList()
            Dim tempTable = New Parameter.InstallRcv3rdTempTableHost(res, result).ToTableTempTable

            params.Add(New SqlParameter("@CompanyID", SqlDbType.Char, 3) With {.Value = companyId})
            params.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = branchHo})
            params.Add(New SqlParameter("@ValueDate", SqlDbType.Date) With {.Value = processDate})
            params.Add(New SqlParameter("@businessdate", SqlDbType.Date) With {.Value = businessDate})
            params.Add(New SqlParameter("@glTempTable", SqlDbType.Structured) With {.Value = tempTable})
            params.Add(New SqlParameter("@pgId", SqlDbType.Char, 20) With {.Value = pgId})
            Dim prmErr = New SqlParameter("@errMsg", SqlDbType.VarChar, 200) With {.Direction = ParameterDirection.Output}
            params.Add(prmErr)

            ' 
            SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, "spProsesCreateJournal3rd", params.ToArray)
            Return prmErr.Value

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        'End Select
        Return ""
    End Function




    Public Function DoUploadAgreementVirtualAccount(cnn As String, rows As Parameter.AgreementVirtualAcc) As String
        Try
            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
            params.Add(New SqlParameter("@fileData", SqlDbType.Structured) With {.Value = rows.ToTable})
            Dim prmErr = New SqlParameter("@errMsg", SqlDbType.VarChar, 200) With {.Direction = ParameterDirection.Output}
            params.Add(prmErr)
            SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, "spUploadAgreementVirtualAccount", params.ToArray)
            If (prmErr.Value <> "") Then
                Throw New Exception(prmErr.Value)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

        Return ""
    End Function
End Class
