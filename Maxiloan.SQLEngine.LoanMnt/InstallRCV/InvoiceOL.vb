
#Region "Imports"

Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InvoiceOL : Inherits DataAccessBase

    Private Const CETAKINVOICEOL_PAGING As String = "spCetakInvoiceOLPaging"
    Private Const CETAKINVOICEOL_DETAIL As String = "spCetakInvoiceOLDetail"
    Private Const CETAKINVOICEOL_SAVE As String = "spCetakInvoiceOLSave"
    Private Const PRINTKWITANSI_OPLS As String = "spPrintKwitansiOperatingLease"
    Private Const INVOICEOL_PAGING As String = "spInvoiceOLPaging"
    Private Const INVOICEOL_DETAIL As String = "spInvoiceOLDetail"
    Private Const INVOICEOL_EXECUTE As String = "spInvoiceOLExecute"
    Private Const CETAKBUKTIBAYAR_OPLS As String = "spInstallRCVOLPrint"
    Private m_connection As SqlConnection

    Public Function CetakInvoiceOLPaging(ByVal customClass As Parameter.InvoiceOL) As Parameter.InvoiceOL
        Dim oReturnValue As New Parameter.InvoiceOL
        Dim params(4) As SqlParameter

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, CETAKINVOICEOL_PAGING, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try

    End Function

    Public Function InvoiceOLPaging(ByVal customClass As Parameter.InvoiceOL) As Parameter.InvoiceOL
        Dim oReturnValue As New Parameter.InvoiceOL
        Dim params(4) As SqlParameter

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, INVOICEOL_PAGING, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try

    End Function

    Public Function CetakInvoiceOLDetail(ByVal customClass As Parameter.InvoiceOL) As DataTable
        Dim tbl As New DataTable
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.ApplicationID

        Try
            tbl = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, CETAKINVOICEOL_DETAIL, params).Tables(0)
            Return tbl
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function InvoiceOLDetail(ByVal customClass As Parameter.InvoiceOL) As DataTable
        Dim tbl As New DataTable
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.ApplicationID
        params(1) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 20)
        params(1).Value = customClass.InvoiceNo

        Try
            tbl = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, INVOICEOL_DETAIL, params).Tables(0)
            Return tbl
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function InvoiceOLExecute(ByVal customClass As Parameter.InvoiceOL) As Boolean
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.ApplicationID
        params(1) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 20)
        params(1).Value = customClass.InvoiceNo
        params(2) = New SqlParameter("@PaymentDate", SqlDbType.DateTime)
        params(2).Value = customClass.PaymentDate
        params(3) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10)
        params(3).Value = customClass.BankAccountID
        params(4) = New SqlParameter("@BayarDenda", SqlDbType.Decimal)
        params(4).Value = customClass.BayarDenda
        params(5) = New SqlParameter("@LoginID", SqlDbType.VarChar, 10)
        params(5).Value = customClass.LoginId

        Try
            SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, INVOICEOL_EXECUTE, params)
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function ReportKwitansi(ByVal customClass As Parameter.InvoiceOL) As DataTable
        Dim tbl As New DataTable
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.ApplicationID
        params(1) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 20)
        params(1).Value = customClass.InvoiceNo

        Try
            tbl = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, PRINTKWITANSI_OPLS, params).Tables(0)
            Return tbl
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function CetakInvoiceOLSave(ByVal oCustomClass As Parameter.InvoiceOL) As String
        Dim objTrans As SqlTransaction = Nothing
        Dim params() As SqlParameter = New SqlParameter(18) {}
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID
            params(2) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(2).Value = oCustomClass.BusinessDate
            params(3) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 20)
            params(3).Value = oCustomClass.LoginId
            params(4) = New SqlParameter("@InvoiceDate", SqlDbType.DateTime)
            params(4).Value = oCustomClass.InvoiceDate
            params(5) = New SqlParameter("@InvoiceDueDate", SqlDbType.DateTime)
            params(5).Value = oCustomClass.InvoiceDueDate
            params(6) = New SqlParameter("@Attn", SqlDbType.VarChar, 30)
            params(6).Value = oCustomClass.Attn
            params(7) = New SqlParameter("@SeqNo", SqlDbType.Int)
            params(7).Value = oCustomClass.SeqNo
            params(8) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
            params(8).Value = oCustomClass.Description
            params(9) = New SqlParameter("@BasicLease", SqlDbType.Decimal)
            params(9).Value = oCustomClass.BasicLease
            params(10) = New SqlParameter("@VATPercentage", SqlDbType.Decimal)
            params(10).Value = oCustomClass.VATPercentage
            params(11) = New SqlParameter("@VATAmount", SqlDbType.Decimal)
            params(11).Value = oCustomClass.VATAmount
            params(12) = New SqlParameter("@WHTPercentage", SqlDbType.Decimal)
            params(12).Value = oCustomClass.WHTPercentage
            params(13) = New SqlParameter("@WHTAmount", SqlDbType.Decimal)
            params(13).Value = oCustomClass.WHTAmount
            params(14) = New SqlParameter("@BillAmount", SqlDbType.Decimal)
            params(14).Value = oCustomClass.BillAmount
            params(15) = New SqlParameter("@PaidAmount", SqlDbType.Decimal)
            params(15).Value = oCustomClass.PaidAmount
            params(16) = New SqlParameter("@PaidStatus", SqlDbType.VarChar, 1)
            params(16).Value = oCustomClass.PaidStatus
            params(17) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 25)
            params(17).Direction = ParameterDirection.Output
            params(18) = New SqlParameter("@errMsg", SqlDbType.VarChar, 200)
            params(18).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, CETAKINVOICEOL_SAVE, params)
            Dim msg = params(18).Value
            Dim ret = params(17).Value

            If (msg <> "") Then
                oCustomClass.strError = params(18).Value
                objTrans.Rollback()
            Else
                oCustomClass.InvoiceNo = params(17).Value
                objTrans.Commit()
            End If
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

            Return ret
        Catch exp As Exception
            objTrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetInstallRCVPrint(ByVal customClass As Parameter.InvoiceOL) As DataTable
        Dim tbl As New DataTable
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.ApplicationID
        params(1) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 20)
        params(1).Value = customClass.InvoiceNo

        Try
            tbl = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, CETAKBUKTIBAYAR_OPLS, params).Tables(0)
            Return tbl
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class

