
#Region "Imports"

Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InstallRcv : Inherits Maxiloan.SQLEngine.LoanMnt.AccMntBase

    Private Const PROC_POSTINGPAYMENT As String = "spPostingInstallment"
    Private Const PROC_MAXBACKDATE As String = "spGetMaxBackDate"
    Private Const PROC_ISLASTPAYMENT As String = "spGetIsLastPayment"
    Private Const PROC_POSTINGPAYMENTMOBILE As String = "spPostingInstallmentMobile2"
    Private Const PARAM_HISTORYSEQUENCENO As String = "@HistorySequenceNo"
    Private m_connection As SqlConnection

    Public Function SavingPostingPayment(ByVal oCustomClass As Parameter.InstallRcv) As Integer
        Dim objTrans As SqlTransaction = Nothing
        Dim params() As SqlParameter = New SqlParameter(43) {}
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction
            Dim params1() As SqlParameter = New SqlParameter(4) {}
            If oCustomClass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = oCustomClass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = oCustomClass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = oCustomClass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"

                'SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                SqlHelper.ExecuteScalar(objTrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                oCustomClass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.CoyID
            params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.ApplicationID
            params(3) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.ValueDate
            params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(4).Value = oCustomClass.BusinessDate
            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId

            params(6) = New SqlParameter(PARAM_INSTALLMENTDUEPAID, SqlDbType.Decimal)
            params(6).Value = oCustomClass.InstallmentDue
            params(7) = New SqlParameter(PARAM_INSTALLMENTDUEDESC, SqlDbType.VarChar, 50)
            params(7).Value = oCustomClass.InstallmentDueDesc
            params(8) = New SqlParameter(PARAM_LCINSTALLMENTPAID, SqlDbType.Decimal)
            params(8).Value = oCustomClass.LcInstallment
            params(9) = New SqlParameter(PARAM_LCINSTALLMENTDESC, SqlDbType.VarChar, 50)
            params(9).Value = oCustomClass.LcInstallmentDesc
            params(10) = New SqlParameter(PARAM_INSTALLMENTCOLLPAID, SqlDbType.Decimal)
            params(10).Value = oCustomClass.InstallmentCollFee
            params(11) = New SqlParameter(PARAM_INSTALLMENTCOLLDESC, SqlDbType.VarChar, 50)
            params(11).Value = oCustomClass.InstallmentCollFeeDesc

            params(12) = New SqlParameter(PARAM_INSURANCEDUEPAID, SqlDbType.Decimal)
            params(12).Value = oCustomClass.InsuranceDue
            params(13) = New SqlParameter(PARAM_INSURANCEDUEDESC, SqlDbType.VarChar, 50)
            params(13).Value = oCustomClass.InsuranceDueDesc
            params(14) = New SqlParameter(PARAM_LCINSURANCEPAID, SqlDbType.Decimal)
            params(14).Value = oCustomClass.LcInsurance
            params(15) = New SqlParameter(PARAM_LCINSURANCEDESC, SqlDbType.VarChar, 50)
            params(15).Value = oCustomClass.LcInsuranceDesc
            params(16) = New SqlParameter(PARAM_INSURANCECOLLPAID, SqlDbType.Decimal)
            params(16).Value = oCustomClass.InsuranceCollFee
            params(17) = New SqlParameter(PARAM_INSURANCECOLLDESC, SqlDbType.VarChar, 50)
            params(17).Value = oCustomClass.InsuranceCollFeeDesc

            params(18) = New SqlParameter(PARAM_PDCBOUNCEFEEPAID, SqlDbType.Decimal)
            params(18).Value = oCustomClass.PDCBounceFee
            params(19) = New SqlParameter(PARAM_PDCBOUNCEFEEDESC, SqlDbType.VarChar, 50)
            params(19).Value = oCustomClass.PDCBounceFeeDesc
            params(20) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEPAID, SqlDbType.Decimal)
            params(20).Value = oCustomClass.InsuranceClaimExpense
            params(21) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEPAIDDESC, SqlDbType.VarChar, 50)
            params(21).Value = oCustomClass.InsuranceClaimExpenseDesc
            params(22) = New SqlParameter(PARAM_STNKRENEEWALFEEPAID, SqlDbType.Decimal)
            params(22).Value = oCustomClass.STNKRenewalFee
            params(23) = New SqlParameter(PARAM_STNKRENEEWALFEEDESC, SqlDbType.VarChar, 50)
            params(23).Value = oCustomClass.STNKRenewalFeeDesc
            params(24) = New SqlParameter(PARAM_REPOSSESSIONFEEPAID, SqlDbType.Decimal)
            params(24).Value = oCustomClass.RepossessionFee
            params(25) = New SqlParameter(PARAM_REPOSSESSIONFEEDESC, SqlDbType.VarChar, 50)
            params(25).Value = oCustomClass.RepossessionFeeDesc
            params(26) = New SqlParameter(PARAM_PREPAIDPAID, SqlDbType.Decimal)
            params(26).Value = oCustomClass.Prepaid
            params(27) = New SqlParameter(PARAM_PREPAIDDESC, SqlDbType.VarChar, 50)
            params(27).Value = oCustomClass.PrepaidDesc

            params(28) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            params(28).Value = oCustomClass.ReceivedFrom
            params(29) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50)
            params(29).Value = oCustomClass.ReferenceNo
            params(30) = New SqlParameter(PARAM_WOP, SqlDbType.VarChar, 2)
            params(30).Value = oCustomClass.WOP
            params(31) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(31).Value = oCustomClass.BankAccountID
            params(32) = New SqlParameter(PARAM_AMOUNTRECEIVE, SqlDbType.Decimal)
            params(32).Value = oCustomClass.AmountReceive
            params(33) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(33).Value = oCustomClass.Notes
            params(34) = New SqlParameter("@BayarDi", SqlDbType.VarChar, 50)
            params(34).Value = IIf(oCustomClass.BayarDi = "0", DBNull.Value, oCustomClass.BayarDi)
            params(35) = New SqlParameter("@HistorySeqNo_return", SqlDbType.Int)
            params(35).Direction = ParameterDirection.Output
            params(36) = New SqlParameter("@CollectorID", SqlDbType.VarChar, 12)
            params(36).Value = IIf(oCustomClass.CollectorID = "0", DBNull.Value, oCustomClass.CollectorID)
            params(37) = New SqlParameter("@Penyetor", SqlDbType.VarChar, 50)
            params(37).Value = oCustomClass.Penyetor
            params(38) = New SqlParameter("@NoKwitansi", SqlDbType.VarChar, 50)
            params(38).Value = oCustomClass.NoKwitansi

            params(39) = New SqlParameter("@PLL", SqlDbType.Decimal)
            params(39).Value = oCustomClass.PLL
            params(40) = New SqlParameter("@PLLDesc", SqlDbType.VarChar, 50)
            params(40).Value = oCustomClass.PLLDesc


            params(41) = New SqlParameter("@TitipanAngsuran", SqlDbType.Decimal)
            params(41).Value = oCustomClass.TitipanAngsuran
            params(42) = New SqlParameter("@TitipanAngsuranDesc", SqlDbType.VarChar, 50)
            params(42).Value = oCustomClass.TitipanAngsuranDesc


            params(43) = New SqlParameter("@errMsg", SqlDbType.VarChar, 200)
            params(43).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, PROC_POSTINGPAYMENT, params)
            '' SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, PROC_POSTINGPAYMENT, params)
            Dim ret = CInt(params(35).Value)

            If (ret <= 0) Then
                objTrans.Rollback()
                oCustomClass.strError = params(43).Value
            Else
                objTrans.Commit()
            End If 
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

            Return ret
        Catch exp As Exception
            objTrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            'WriteException("InstallRcv", "SavingPostingPayment", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function MaxBackDate(ByVal strConnection As String) As Integer
        Try
            Return CType(SqlHelper.ExecuteScalar(strConnection, CommandType.StoredProcedure, PROC_MAXBACKDATE), Integer)
        Catch exp As Exception
            WriteException("InstallRcv", "MaxBackDate", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function IsLastPayment(ByVal oCustomClass As Parameter.InstallRcv) As Boolean
        Try
            Dim params() As SqlParameter = New SqlParameter(2) {}
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter(PARAM_AMOUNTRECEIVE, SqlDbType.Decimal)
            params(2).Value = oCustomClass.AmountReceive

            Return CType(SqlHelper.ExecuteScalar(oCustomClass.strConnection, CommandType.StoredProcedure, PROC_ISLASTPAYMENT, params), Boolean)
        Catch exp As Exception
            WriteException("InstallRcv", "IsLastPayment", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function IsValidLastPayment(ByVal strConnection As String, ByVal ApplicationID As String, ByVal ValueDate As Date) As Boolean
        Try
            Dim params() As SqlParameter = New SqlParameter(2) {}
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = ApplicationID
            params(1) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(1).Value = ValueDate
            params(2) = New SqlParameter("@ISValid", SqlDbType.Bit)
            params(2).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(strConnection, CommandType.StoredProcedure, "spProcessPayment_IsValidLastPayment", params)
            Return CBool(params(2).Value)
        Catch exp As Exception
            WriteException("InstallRcv", "IsValidLastPayment", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Overloads Function PrintTTU(ByVal strConnection As String, _
                            ByVal ApplicationID As String, _
                            ByVal BranchID As String, _
                            ByVal BusinessDate As Date, ByVal HistorySequenceNo As Integer, ByVal LoginID As String) As DataSet
        Try
            Dim params() As SqlParameter = New SqlParameter(4) {}
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = BranchID
            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = ApplicationID
            params(2) = New SqlParameter(PARAM_HISTORYSEQUENCENO, SqlDbType.Int)
            params(2).Value = HistorySequenceNo
            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = BusinessDate
            params(4) = New SqlParameter("@LoginID", SqlDbType.VarChar, 255)
            params(4).Value = LoginID

            Return SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, "spPrintKwitansiIntallment", params)
        Catch exp As Exception
            WriteException("InstallRcv", "PrintTTU", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Overloads Function PrintTTU(ByVal strConnection As String, _
                            ByVal ApplicationID As String, _
                            ByVal BranchID As String, _
                            ByVal BusinessDate As Date, ByVal HistorySequenceNo As Integer, ByVal LoginID As String, ByVal InsSeqNo As Integer) As DataSet
        Try
            Dim params() As SqlParameter = New SqlParameter(5) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = BranchID
            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = ApplicationID
            params(2) = New SqlParameter(PARAM_HISTORYSEQUENCENO, SqlDbType.Int)
            params(2).Value = HistorySequenceNo
            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = BusinessDate
            params(4) = New SqlParameter("@LoginID", SqlDbType.VarChar, 255)
            params(4).Value = LoginID
            params(5) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(5).Value = InsSeqNo

            Return SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, "spPrintKwitansiIntallment", params)
        Catch exp As Exception
            WriteException("InstallRcv", "PrintTTU", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Overloads Function PrintTTU(ByVal strConnection As String, ByVal ApplicationID As String, ByVal BranchID As String, ByVal BusinessDate As Date, ByVal LoginID As String) As DataSet
        Try
            Dim params() As SqlParameter = New SqlParameter(3) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = BranchID
            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = ApplicationID
            params(2) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(2).Value = BusinessDate
            params(3) = New SqlParameter("@LoginID", SqlDbType.VarChar, 255)
            params(3).Value = LoginID

            Return SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, "spPrintKwitansiIntallment", params)
        Catch exp As Exception
            WriteException("InstallRcv", "PrintTTU", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Overloads Function PrintInstallmentReceive(ByVal strConnection As String, ByVal ApplicationID As String, ByVal BranchID As String, ByVal BusinessDate As Date, ByVal LoginID As String) As DataSet
        Try
            Dim params() As SqlParameter = New SqlParameter(3) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = BranchID
            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = ApplicationID
            params(2) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(2).Value = BusinessDate
            params(3) = New SqlParameter("@LoginID", SqlDbType.VarChar, 255)
            params(3).Value = LoginID

            Return SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, "spPrintKwitansiIntallment2", params)
        Catch exp As Exception
            WriteException("InstallRcv", "PrintTTU", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function ListKwitansiInstallment(ByVal oCustomClass As Parameter.InstallRcv) As DataSet
        Try
            Dim params() As SqlParameter = New SqlParameter(1) {}

            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
            params(0).Value = oCustomClass.WhereCond
            params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar)
            params(1).Value = oCustomClass.SortBy

            Return SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spListKwitansiInstallment", params)
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function SavePrintKwitansiInstallment(ByVal customclass As Parameter.InstallRcv) As Parameter.InstallRcv
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer

        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(5) {}
            oDataTable = customclass.ListData

            For intloop = 0 To oDataTable.Rows.Count - 1

                params(0) = New SqlParameter("@MailTransNo", SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("ReferenceNo")

                params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params(2).Value = customclass.BranchId

                params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                params(3).Value = customclass.BusinessDate

                params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                params(4).Value = customclass.LoginId

                params(5) = New SqlParameter("@InsSeqNo", SqlDbType.SmallInt)
                params(5).Value = oDataTable.Rows(intloop)("InsSeqNo")

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, "spSavePrintKwitansiInstallment", params)
            Next
            oTrans.Commit()
            customclass.Hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.Hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function

    Public Function ReportKwitansiInstallment(ByVal customclass As Parameter.InstallRcv) As DataSet
        Dim params() As SqlParameter = New SqlParameter(3) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@MultiApplicationID", SqlDbType.VarChar, 1000)
        params(1).Value = customclass.MultiApplicationID
        params(2) = New SqlParameter("@MultiAgreementNo", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.MultiAgreementNo
        params(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(3).Value = customclass.BranchId

        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spReportKwitansiInstallment", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try


    End Function

    Public Function ReportKwitansiInstallment2(ByVal customclass As Parameter.InstallRcv) As DataSet
        Dim params() As SqlParameter = New SqlParameter(3) {}

        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 30)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter(PARAM_HISTORYSEQUENCENO, SqlDbType.Int)
        params(2).Value = customclass.HistorySeqNo

        params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(3).Value = customclass.InsSeqNo

        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spReportKwitansiInstallment2", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function ReportKwitansiInstallmentWithVoucherNo(ByVal customclass As Parameter.InstallRcv) As DataSet
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter("@VoucherNo", SqlDbType.VarChar, 30)
        params(0).Value = customclass.VoucherNo

        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spReportKwitansiNonInstallment", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function



    Public Function InstallRcvInstallmentSchedulePaging(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim oReturnValue As New Parameter.GeneralPaging
        Dim params(5) As SqlParameter

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@ValueDate", SqlDbType.SmallDateTime)
        params(5).Value = customClass.BusinessDate


        Try

            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try


    End Function

    Public Function AdvanceInstallmet(ByVal CustomCLass As Parameter.InstallRcv) As Parameter.InstallRcv
        Dim oReturn As New Parameter.InstallRcv
        Dim param() As SqlParameter = New SqlParameter(0) {}
        param(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        param(0).Value = CustomCLass.ApplicationID
        Try
            oReturn.ListData = SqlHelper.ExecuteDataset(CustomCLass.strConnection, CommandType.StoredProcedure, "spGetAdvanceInstallment", param).Tables(0)
            Return oReturn
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    Public Function AlokasiAdvanceInstallmet(ByVal CustomClass As Parameter.InstallRcv) As String

        Dim params(3) As SqlParameter

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = CustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(1).Value = CustomClass.BranchId.Replace("'", "")
        params(2) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
        params(2).Value = CustomClass.BusinessDate
        params(3) = New SqlParameter("@Err", SqlDbType.Int)
        params(3).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(CustomClass.strConnection, CommandType.StoredProcedure, "spAlokasiAdvaceInstallment", params)
            'If params(3).Value > 0 Then
            '    Return "1"
            'Else
            '    Return "0"
            'End If
        Catch ex As Exception

        End Try
    End Function
    Public Function GetSP(ByVal customclass As Parameter.InstallRcv) As Parameter.InstallRcv
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, customclass.SPName, params).Tables(0)
        Return customclass
    End Function

    Public Function InstallmentReceiveAdvIns(ByVal oCustomClass As Parameter.InstallRcv) As Integer
        Dim objTrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(38) {}
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction
            Dim params1() As SqlParameter = New SqlParameter(4) {}
            If oCustomClass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = oCustomClass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = oCustomClass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = oCustomClass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"
                SqlHelper.ExecuteScalar(objTrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                oCustomClass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.CoyID
            params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.ApplicationID
            params(3) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.ValueDate
            params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(4).Value = oCustomClass.BusinessDate
            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId

            params(6) = New SqlParameter(PARAM_INSTALLMENTDUEPAID, SqlDbType.Decimal)
            params(6).Value = oCustomClass.InstallmentDue
            params(7) = New SqlParameter(PARAM_INSTALLMENTDUEDESC, SqlDbType.VarChar, 50)
            params(7).Value = oCustomClass.InstallmentDueDesc
            params(8) = New SqlParameter(PARAM_LCINSTALLMENTPAID, SqlDbType.Decimal)
            params(8).Value = oCustomClass.LcInstallment
            params(9) = New SqlParameter(PARAM_LCINSTALLMENTDESC, SqlDbType.VarChar, 50)
            params(9).Value = oCustomClass.LcInstallmentDesc
            params(10) = New SqlParameter(PARAM_INSTALLMENTCOLLPAID, SqlDbType.Decimal)
            params(10).Value = oCustomClass.InstallmentCollFee
            params(11) = New SqlParameter(PARAM_INSTALLMENTCOLLDESC, SqlDbType.VarChar, 50)
            params(11).Value = oCustomClass.InstallmentCollFeeDesc

            params(12) = New SqlParameter(PARAM_INSURANCEDUEPAID, SqlDbType.Decimal)
            params(12).Value = oCustomClass.InsuranceDue
            params(13) = New SqlParameter(PARAM_INSURANCEDUEDESC, SqlDbType.VarChar, 50)
            params(13).Value = oCustomClass.InsuranceDueDesc
            params(14) = New SqlParameter(PARAM_LCINSURANCEPAID, SqlDbType.Decimal)
            params(14).Value = oCustomClass.LcInsurance
            params(15) = New SqlParameter(PARAM_LCINSURANCEDESC, SqlDbType.VarChar, 50)
            params(15).Value = oCustomClass.LcInsuranceDesc
            params(16) = New SqlParameter(PARAM_INSURANCECOLLPAID, SqlDbType.Decimal)
            params(16).Value = oCustomClass.InsuranceCollFee
            params(17) = New SqlParameter(PARAM_INSURANCECOLLDESC, SqlDbType.VarChar, 50)
            params(17).Value = oCustomClass.InsuranceCollFeeDesc

            params(18) = New SqlParameter(PARAM_PDCBOUNCEFEEPAID, SqlDbType.Decimal)
            params(18).Value = oCustomClass.PDCBounceFee
            params(19) = New SqlParameter(PARAM_PDCBOUNCEFEEDESC, SqlDbType.VarChar, 50)
            params(19).Value = oCustomClass.PDCBounceFeeDesc
            params(20) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEPAID, SqlDbType.Decimal)
            params(20).Value = oCustomClass.InsuranceClaimExpense
            params(21) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEPAIDDESC, SqlDbType.VarChar, 50)
            params(21).Value = oCustomClass.InsuranceClaimExpenseDesc
            params(22) = New SqlParameter(PARAM_STNKRENEEWALFEEPAID, SqlDbType.Decimal)
            params(22).Value = oCustomClass.STNKRenewalFee
            params(23) = New SqlParameter(PARAM_STNKRENEEWALFEEDESC, SqlDbType.VarChar, 50)
            params(23).Value = oCustomClass.STNKRenewalFeeDesc
            params(24) = New SqlParameter(PARAM_REPOSSESSIONFEEPAID, SqlDbType.Decimal)
            params(24).Value = oCustomClass.RepossessionFee
            params(25) = New SqlParameter(PARAM_REPOSSESSIONFEEDESC, SqlDbType.VarChar, 50)
            params(25).Value = oCustomClass.RepossessionFeeDesc
            params(26) = New SqlParameter(PARAM_PREPAIDPAID, SqlDbType.Decimal)
            params(26).Value = oCustomClass.Prepaid
            params(27) = New SqlParameter(PARAM_PREPAIDDESC, SqlDbType.VarChar, 50)
            params(27).Value = oCustomClass.PrepaidDesc

            params(28) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            params(28).Value = oCustomClass.ReceivedFrom
            params(29) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50)
            params(29).Value = oCustomClass.ReferenceNo
            params(30) = New SqlParameter(PARAM_WOP, SqlDbType.VarChar, 2)
            params(30).Value = oCustomClass.WOP
            params(31) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(31).Value = oCustomClass.BankAccountID
            params(32) = New SqlParameter(PARAM_AMOUNTRECEIVE, SqlDbType.Decimal)
            params(32).Value = oCustomClass.AmountReceive
            params(33) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(33).Value = oCustomClass.Notes
            params(34) = New SqlParameter("@BayarDi", SqlDbType.VarChar, 50)
            params(34).Value = IIf(oCustomClass.BayarDi = "0", DBNull.Value, oCustomClass.BayarDi)
            params(35) = New SqlParameter("@HistorySeqNo_return", SqlDbType.Int)
            params(35).Direction = ParameterDirection.Output
            params(36) = New SqlParameter("@CollectorID", SqlDbType.VarChar, 12)
            params(36).Value = IIf(oCustomClass.CollectorID = "0", DBNull.Value, oCustomClass.CollectorID)
            params(37) = New SqlParameter("@Penyetor", SqlDbType.VarChar, 50)
            params(37).Value = oCustomClass.Penyetor
            params(38) = New SqlParameter("@ReferenceNo1", SqlDbType.VarChar, 30)
            params(38).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spInstallRCVAdvInsSave", params)
            objTrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

            oCustomClass.ReferenceNo1 = params(38).Value

            Return CInt(params(35).Value)
        Catch exp As Exception
            objTrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            'WriteException("InstallRcv", "InstallmentReceiveAdvIns", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function ReportKwitansiInstallmentAdvIns(ByVal customclass As Parameter.InstallRcv) As DataSet
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 30)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 30)
        params(2).Value = customclass.ReferenceNo

        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spReportKwitansiInstallmentAdvIns", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try



    End Function

    Public Function InstallRCVKorAngsSave(ByVal oCustomClass As Parameter.InstallRcv) As Integer
        Dim objTrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(42) {}
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction
            Dim params1() As SqlParameter = New SqlParameter(4) {}
            If oCustomClass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = oCustomClass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = oCustomClass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = oCustomClass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"
                SqlHelper.ExecuteScalar(objTrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                oCustomClass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.CoyID
            params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.ApplicationID
            params(3) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.ValueDate
            params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(4).Value = oCustomClass.BusinessDate
            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId

            params(6) = New SqlParameter(PARAM_INSTALLMENTDUEPAID, SqlDbType.Decimal)
            params(6).Value = oCustomClass.InstallmentDue
            params(7) = New SqlParameter(PARAM_INSTALLMENTDUEDESC, SqlDbType.VarChar, 50)
            params(7).Value = oCustomClass.InstallmentDueDesc
            params(8) = New SqlParameter(PARAM_LCINSTALLMENTPAID, SqlDbType.Decimal)
            params(8).Value = oCustomClass.LcInstallment
            params(9) = New SqlParameter(PARAM_LCINSTALLMENTDESC, SqlDbType.VarChar, 50)
            params(9).Value = oCustomClass.LcInstallmentDesc
            params(10) = New SqlParameter(PARAM_INSTALLMENTCOLLPAID, SqlDbType.Decimal)
            params(10).Value = oCustomClass.InstallmentCollFee
            params(11) = New SqlParameter(PARAM_INSTALLMENTCOLLDESC, SqlDbType.VarChar, 50)
            params(11).Value = oCustomClass.InstallmentCollFeeDesc

            params(12) = New SqlParameter(PARAM_INSURANCEDUEPAID, SqlDbType.Decimal)
            params(12).Value = oCustomClass.InsuranceDue
            params(13) = New SqlParameter(PARAM_INSURANCEDUEDESC, SqlDbType.VarChar, 50)
            params(13).Value = oCustomClass.InsuranceDueDesc
            params(14) = New SqlParameter(PARAM_LCINSURANCEPAID, SqlDbType.Decimal)
            params(14).Value = oCustomClass.LcInsurance
            params(15) = New SqlParameter(PARAM_LCINSURANCEDESC, SqlDbType.VarChar, 50)
            params(15).Value = oCustomClass.LcInsuranceDesc
            params(16) = New SqlParameter(PARAM_INSURANCECOLLPAID, SqlDbType.Decimal)
            params(16).Value = oCustomClass.InsuranceCollFee
            params(17) = New SqlParameter(PARAM_INSURANCECOLLDESC, SqlDbType.VarChar, 50)
            params(17).Value = oCustomClass.InsuranceCollFeeDesc

            params(18) = New SqlParameter(PARAM_PDCBOUNCEFEEPAID, SqlDbType.Decimal)
            params(18).Value = oCustomClass.PDCBounceFee
            params(19) = New SqlParameter(PARAM_PDCBOUNCEFEEDESC, SqlDbType.VarChar, 50)
            params(19).Value = oCustomClass.PDCBounceFeeDesc
            params(20) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEPAID, SqlDbType.Decimal)
            params(20).Value = oCustomClass.InsuranceClaimExpense
            params(21) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEPAIDDESC, SqlDbType.VarChar, 50)
            params(21).Value = oCustomClass.InsuranceClaimExpenseDesc
            params(22) = New SqlParameter(PARAM_STNKRENEEWALFEEPAID, SqlDbType.Decimal)
            params(22).Value = oCustomClass.STNKRenewalFee
            params(23) = New SqlParameter(PARAM_STNKRENEEWALFEEDESC, SqlDbType.VarChar, 50)
            params(23).Value = oCustomClass.STNKRenewalFeeDesc
            params(24) = New SqlParameter(PARAM_REPOSSESSIONFEEPAID, SqlDbType.Decimal)
            params(24).Value = oCustomClass.RepossessionFee
            params(25) = New SqlParameter(PARAM_REPOSSESSIONFEEDESC, SqlDbType.VarChar, 50)
            params(25).Value = oCustomClass.RepossessionFeeDesc
            params(26) = New SqlParameter(PARAM_PREPAIDPAID, SqlDbType.Decimal)
            params(26).Value = oCustomClass.Prepaid
            params(27) = New SqlParameter(PARAM_PREPAIDDESC, SqlDbType.VarChar, 50)
            params(27).Value = oCustomClass.PrepaidDesc

            params(28) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            params(28).Value = oCustomClass.ReceivedFrom
            params(29) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50)
            params(29).Value = oCustomClass.ReferenceNo
            params(30) = New SqlParameter(PARAM_WOP, SqlDbType.VarChar, 2)
            params(30).Value = oCustomClass.WOP
            params(31) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(31).Value = oCustomClass.BankAccountID
            params(32) = New SqlParameter(PARAM_AMOUNTRECEIVE, SqlDbType.Decimal)
            params(32).Value = oCustomClass.AmountReceive
            params(33) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(33).Value = oCustomClass.Notes
            params(34) = New SqlParameter("@BayarDi", SqlDbType.VarChar, 50)
            params(34).Value = IIf(oCustomClass.BayarDi = "0", DBNull.Value, oCustomClass.BayarDi)
            params(35) = New SqlParameter("@HistorySeqNo_return", SqlDbType.Int)
            params(35).Direction = ParameterDirection.Output
            params(36) = New SqlParameter("@CollectorID", SqlDbType.VarChar, 12)
            params(36).Value = IIf(oCustomClass.CollectorID = "0", DBNull.Value, oCustomClass.CollectorID)
            params(37) = New SqlParameter("@Penyetor", SqlDbType.VarChar, 50)
            params(37).Value = oCustomClass.Penyetor
            params(38) = New SqlParameter("@IsReversedOnBusinessDate", SqlDbType.Bit)
            params(38).Value = oCustomClass.IsReversedOnBusinessDate
            params(39) = New SqlParameter("@ReferenceNoLog", SqlDbType.Char, 20)
            params(39).Value = oCustomClass.ReferenceNoLog
            params(40) = New SqlParameter("@ApplicationIDLog", SqlDbType.Char, 20)
            params(40).Value = oCustomClass.ApplicationIDLog

            params(41) = New SqlParameter("@PLL", SqlDbType.Decimal)
            params(41).Value = oCustomClass.PLL
            params(42) = New SqlParameter("@PLLDesc", SqlDbType.VarChar, 100)
            params(42).Value = oCustomClass.PLLDesc

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spInstallRCVKorAngsSave", params)
            objTrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

            Return CInt(params(35).Value)
        Catch exp As Exception
            objTrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            'WriteException("InstallRcv", "InstallmentReceiveAdvIns", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function InstallRCVTemp(ByVal oCustomClass As Parameter.InstallRcv) As Integer
        Dim objTrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(41) {}
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction
            Dim params1() As SqlParameter = New SqlParameter(4) {}
            If oCustomClass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = oCustomClass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = oCustomClass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = oCustomClass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"
                SqlHelper.ExecuteScalar(objTrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                oCustomClass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.CoyID
            params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.ApplicationID
            params(3) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.ValueDate
            params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(4).Value = oCustomClass.BusinessDate
            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId

            params(6) = New SqlParameter(PARAM_INSTALLMENTDUEPAID, SqlDbType.Decimal)
            params(6).Value = oCustomClass.InstallmentDue
            params(7) = New SqlParameter(PARAM_INSTALLMENTDUEDESC, SqlDbType.VarChar, 50)
            params(7).Value = oCustomClass.InstallmentDueDesc
            params(8) = New SqlParameter(PARAM_LCINSTALLMENTPAID, SqlDbType.Decimal)
            params(8).Value = oCustomClass.LcInstallment
            params(9) = New SqlParameter(PARAM_LCINSTALLMENTDESC, SqlDbType.VarChar, 50)
            params(9).Value = oCustomClass.LcInstallmentDesc
            params(10) = New SqlParameter(PARAM_INSTALLMENTCOLLPAID, SqlDbType.Decimal)
            params(10).Value = oCustomClass.InstallmentCollFee
            params(11) = New SqlParameter(PARAM_INSTALLMENTCOLLDESC, SqlDbType.VarChar, 50)
            params(11).Value = oCustomClass.InstallmentCollFeeDesc

            params(12) = New SqlParameter(PARAM_INSURANCEDUEPAID, SqlDbType.Decimal)
            params(12).Value = oCustomClass.InsuranceDue
            params(13) = New SqlParameter(PARAM_INSURANCEDUEDESC, SqlDbType.VarChar, 50)
            params(13).Value = oCustomClass.InsuranceDueDesc
            params(14) = New SqlParameter(PARAM_LCINSURANCEPAID, SqlDbType.Decimal)
            params(14).Value = oCustomClass.LcInsurance
            params(15) = New SqlParameter(PARAM_LCINSURANCEDESC, SqlDbType.VarChar, 50)
            params(15).Value = oCustomClass.LcInsuranceDesc
            params(16) = New SqlParameter(PARAM_INSURANCECOLLPAID, SqlDbType.Decimal)
            params(16).Value = oCustomClass.InsuranceCollFee
            params(17) = New SqlParameter(PARAM_INSURANCECOLLDESC, SqlDbType.VarChar, 50)
            params(17).Value = oCustomClass.InsuranceCollFeeDesc

            params(18) = New SqlParameter(PARAM_PDCBOUNCEFEEPAID, SqlDbType.Decimal)
            params(18).Value = oCustomClass.PDCBounceFee
            params(19) = New SqlParameter(PARAM_PDCBOUNCEFEEDESC, SqlDbType.VarChar, 50)
            params(19).Value = oCustomClass.PDCBounceFeeDesc
            params(20) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEPAID, SqlDbType.Decimal)
            params(20).Value = oCustomClass.InsuranceClaimExpense
            params(21) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEPAIDDESC, SqlDbType.VarChar, 50)
            params(21).Value = oCustomClass.InsuranceClaimExpenseDesc
            params(22) = New SqlParameter(PARAM_STNKRENEEWALFEEPAID, SqlDbType.Decimal)
            params(22).Value = oCustomClass.STNKRenewalFee
            params(23) = New SqlParameter(PARAM_STNKRENEEWALFEEDESC, SqlDbType.VarChar, 50)
            params(23).Value = oCustomClass.STNKRenewalFeeDesc
            params(24) = New SqlParameter(PARAM_REPOSSESSIONFEEPAID, SqlDbType.Decimal)
            params(24).Value = oCustomClass.RepossessionFee
            params(25) = New SqlParameter(PARAM_REPOSSESSIONFEEDESC, SqlDbType.VarChar, 50)
            params(25).Value = oCustomClass.RepossessionFeeDesc
            params(26) = New SqlParameter(PARAM_PREPAIDPAID, SqlDbType.Decimal)
            params(26).Value = oCustomClass.Prepaid
            params(27) = New SqlParameter(PARAM_PREPAIDDESC, SqlDbType.VarChar, 50)
            params(27).Value = oCustomClass.PrepaidDesc

            params(28) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            params(28).Value = oCustomClass.ReceivedFrom
            params(29) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50)
            params(29).Value = oCustomClass.ReferenceNo
            params(30) = New SqlParameter(PARAM_WOP, SqlDbType.VarChar, 2)
            params(30).Value = oCustomClass.WOP
            params(31) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(31).Value = oCustomClass.BankAccountID
            params(32) = New SqlParameter(PARAM_AMOUNTRECEIVE, SqlDbType.Decimal)
            params(32).Value = oCustomClass.AmountReceive
            params(33) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(33).Value = oCustomClass.Notes
            params(34) = New SqlParameter("@BayarDi", SqlDbType.VarChar, 50)
            params(34).Value = IIf(oCustomClass.BayarDi = "0", DBNull.Value, oCustomClass.BayarDi)
            params(35) = New SqlParameter("@HistorySeqNo_return", SqlDbType.Int)
            params(35).Direction = ParameterDirection.Output
            params(36) = New SqlParameter("@CollectorID", SqlDbType.VarChar, 12)
            params(36).Value = IIf(oCustomClass.CollectorID = "0", DBNull.Value, oCustomClass.CollectorID)
            params(37) = New SqlParameter("@Penyetor", SqlDbType.VarChar, 50)
            params(37).Value = oCustomClass.Penyetor
            params(38) = New SqlParameter("@NoKwitansi", SqlDbType.VarChar, 50)
            params(38).Value = oCustomClass.NoKwitansi

            params(39) = New SqlParameter("@PLL", SqlDbType.Decimal)
            params(39).Value = oCustomClass.PLL
            params(40) = New SqlParameter("@PLLDesc", SqlDbType.VarChar, 50)
            params(40).Value = oCustomClass.PLLDesc
            params(41) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(41).Value = oCustomClass.InsSeqNo

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spInstallRCVTemp", params)
            objTrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

            Return CInt(params(35).Value)
        Catch exp As Exception
            objTrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            'WriteException("InstallRcv", "SavingPostingPayment", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function InstallRCVBATemp(ByVal oCustomClass As Parameter.InstallRcv) As Integer
        Dim objTrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(46) {}
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            Dim params1() As SqlParameter = New SqlParameter(4) {}
            If oCustomClass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = oCustomClass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = oCustomClass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = oCustomClass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"
                SqlHelper.ExecuteScalar(objTrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                oCustomClass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.CoyID
            params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.ApplicationID
            params(3) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.ValueDate
            params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(4).Value = oCustomClass.BusinessDate
            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId

            params(6) = New SqlParameter(PARAM_INSTALLMENTDUEPAID, SqlDbType.Decimal)
            params(6).Value = oCustomClass.InstallmentDue
            params(7) = New SqlParameter(PARAM_INSTALLMENTDUEDESC, SqlDbType.VarChar, 50)
            params(7).Value = oCustomClass.InstallmentDueDesc
            params(8) = New SqlParameter(PARAM_LCINSTALLMENTPAID, SqlDbType.Decimal)
            params(8).Value = oCustomClass.LcInstallment
            params(9) = New SqlParameter(PARAM_LCINSTALLMENTDESC, SqlDbType.VarChar, 50)
            params(9).Value = oCustomClass.LcInstallmentDesc
            params(10) = New SqlParameter(PARAM_INSTALLMENTCOLLPAID, SqlDbType.Decimal)
            params(10).Value = oCustomClass.InstallmentCollFee
            params(11) = New SqlParameter(PARAM_INSTALLMENTCOLLDESC, SqlDbType.VarChar, 50)
            params(11).Value = oCustomClass.InstallmentCollFeeDesc

            params(12) = New SqlParameter(PARAM_INSURANCEDUEPAID, SqlDbType.Decimal)
            params(12).Value = oCustomClass.InsuranceDue
            params(13) = New SqlParameter(PARAM_INSURANCEDUEDESC, SqlDbType.VarChar, 50)
            params(13).Value = oCustomClass.InsuranceDueDesc
            params(14) = New SqlParameter(PARAM_LCINSURANCEPAID, SqlDbType.Decimal)
            params(14).Value = oCustomClass.LcInsurance
            params(15) = New SqlParameter(PARAM_LCINSURANCEDESC, SqlDbType.VarChar, 50)
            params(15).Value = oCustomClass.LcInsuranceDesc
            params(16) = New SqlParameter(PARAM_INSURANCECOLLPAID, SqlDbType.Decimal)
            params(16).Value = oCustomClass.InsuranceCollFee
            params(17) = New SqlParameter(PARAM_INSURANCECOLLDESC, SqlDbType.VarChar, 50)
            params(17).Value = oCustomClass.InsuranceCollFeeDesc

            params(18) = New SqlParameter(PARAM_PDCBOUNCEFEEPAID, SqlDbType.Decimal)
            params(18).Value = oCustomClass.PDCBounceFee
            params(19) = New SqlParameter(PARAM_PDCBOUNCEFEEDESC, SqlDbType.VarChar, 50)
            params(19).Value = oCustomClass.PDCBounceFeeDesc
            params(20) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEPAID, SqlDbType.Decimal)
            params(20).Value = oCustomClass.InsuranceClaimExpense
            params(21) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEPAIDDESC, SqlDbType.VarChar, 50)
            params(21).Value = oCustomClass.InsuranceClaimExpenseDesc
            params(22) = New SqlParameter(PARAM_STNKRENEEWALFEEPAID, SqlDbType.Decimal)
            params(22).Value = oCustomClass.STNKRenewalFee
            params(23) = New SqlParameter(PARAM_STNKRENEEWALFEEDESC, SqlDbType.VarChar, 50)
            params(23).Value = oCustomClass.STNKRenewalFeeDesc
            params(24) = New SqlParameter(PARAM_REPOSSESSIONFEEPAID, SqlDbType.Decimal)
            params(24).Value = oCustomClass.RepossessionFee
            params(25) = New SqlParameter(PARAM_REPOSSESSIONFEEDESC, SqlDbType.VarChar, 50)
            params(25).Value = oCustomClass.RepossessionFeeDesc
            params(26) = New SqlParameter(PARAM_PREPAIDPAID, SqlDbType.Decimal)
            params(26).Value = oCustomClass.Prepaid
            params(27) = New SqlParameter(PARAM_PREPAIDDESC, SqlDbType.VarChar, 50)
            params(27).Value = oCustomClass.PrepaidDesc

            params(28) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            params(28).Value = oCustomClass.ReceivedFrom
            params(29) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50)
            params(29).Value = oCustomClass.ReferenceNo
            params(30) = New SqlParameter(PARAM_WOP, SqlDbType.VarChar, 2)
            params(30).Value = oCustomClass.WOP
            params(31) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(31).Value = oCustomClass.BankAccountID
            params(32) = New SqlParameter(PARAM_AMOUNTRECEIVE, SqlDbType.Decimal)
            params(32).Value = oCustomClass.AmountReceive
            params(33) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(33).Value = oCustomClass.Notes
            params(34) = New SqlParameter("@BayarDi", SqlDbType.VarChar, 50)
            params(34).Value = IIf(oCustomClass.BayarDi = "0", DBNull.Value, oCustomClass.BayarDi)
            params(35) = New SqlParameter("@HistorySeqNo_return", SqlDbType.Int)
            params(35).Direction = ParameterDirection.Output
            params(36) = New SqlParameter("@CollectorID", SqlDbType.VarChar, 12)
            params(36).Value = IIf(oCustomClass.CollectorID = "0", DBNull.Value, oCustomClass.CollectorID)
            params(37) = New SqlParameter("@Penyetor", SqlDbType.VarChar, 50)
            params(37).Value = oCustomClass.Penyetor
            params(38) = New SqlParameter("@NoKwitansi", SqlDbType.VarChar, 50)
            params(38).Value = oCustomClass.NoKwitansi

            params(39) = New SqlParameter("@PLL", SqlDbType.Decimal)
            params(39).Value = oCustomClass.PLL
            params(40) = New SqlParameter("@PLLDesc", SqlDbType.VarChar, 50)
            params(40).Value = oCustomClass.PLLDesc
            params(41) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(41).Value = oCustomClass.InsSeqNo
            params(42) = New SqlParameter("@FormID", SqlDbType.Char, 20)
            params(42).Value = oCustomClass.FormID
            params(43) = New SqlParameter("@Status", SqlDbType.VarChar, 10)
            params(43).Value = oCustomClass.Status
            params(44) = New SqlParameter("@VoucherNo", SqlDbType.Char, 20)
            params(44).Value = oCustomClass.VoucherNO

            params(45) = New SqlParameter("@TitipanAngsuran", SqlDbType.Decimal)
            params(45).Value = oCustomClass.TitipanAngsuran
            params(46) = New SqlParameter("@TitipanAngsuranDesc", SqlDbType.VarChar, 50)
            params(46).Value = oCustomClass.TitipanAngsuranDesc

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spInstallRCVBATemp", params)
            objTrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

            Return CInt(params(35).Value)
        Catch exp As Exception
            objTrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            'WriteException("InstallRcv", "SavingPostingPayment", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function InstallRCVBAFactoring(ByVal oCustomClass As Parameter.InstallRcv) As Integer
        Dim objTrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(21) {}
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            Dim params1() As SqlParameter = New SqlParameter(4) {}
            If oCustomClass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = oCustomClass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = oCustomClass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = oCustomClass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"
                SqlHelper.ExecuteScalar(objTrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                oCustomClass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.CoyID
            params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.ApplicationID
            params(3) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.ValueDate
            params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(4).Value = oCustomClass.BusinessDate
            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId

            params(6) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            params(6).Value = oCustomClass.ReceivedFrom
            params(7) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50)
            params(7).Value = oCustomClass.ReferenceNo
            params(8) = New SqlParameter(PARAM_WOP, SqlDbType.VarChar, 2)
            params(8).Value = oCustomClass.WOP
            params(9) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(9).Value = oCustomClass.BankAccountID
            params(10) = New SqlParameter("@PaidPrincipal", SqlDbType.Decimal)
            params(10).Value = oCustomClass.PaidPrincipal
            params(11) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(11).Value = oCustomClass.Notes
            params(12) = New SqlParameter("@BayarDi", SqlDbType.VarChar, 50)
            params(12).Value = IIf(oCustomClass.BayarDi = "0", DBNull.Value, oCustomClass.BayarDi)
            params(13) = New SqlParameter("@HistorySeqNo_return", SqlDbType.Int)
            params(13).Direction = ParameterDirection.Output

            params(14) = New SqlParameter("@FormID", SqlDbType.Char, 20)
            params(14).Value = oCustomClass.FormID
            params(15) = New SqlParameter("@Status", SqlDbType.VarChar, 10)
            params(15).Value = oCustomClass.Status

            params(16) = New SqlParameter("@PaidInterest", SqlDbType.Decimal)
            params(16).Value = oCustomClass.PaidInterest

            params(17) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
            params(17).Value = oCustomClass.InvoiceNo

            params(18) = New SqlParameter("@PaidRetensi", SqlDbType.Decimal)
            params(18).Value = oCustomClass.PaidRetensi

            params(19) = New SqlParameter("@DendaPaid", SqlDbType.Decimal)
            params(19).Value = oCustomClass.DendaPaid

            params(20) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(20).Value = oCustomClass.InsSeqNo

            params(21) = New SqlParameter("@Denda", SqlDbType.Decimal)
            params(21).Value = oCustomClass.Denda

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spInstallRCVBAFactoring", params)
            objTrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

            Return CInt(params(13).Value)
        Catch exp As Exception
            objTrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            'WriteException("InstallRcv", "SavingPostingPayment", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function InstallRCVPPH(ByVal oCustomClass As Parameter.InstallRcv) As Integer
        Dim objTrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(9) {}
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction


            params(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId

            params(1) = New SqlParameter("@applicationid", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.ApplicationID
            params(2) = New SqlParameter("@valuedate", SqlDbType.DateTime)
            params(2).Value = oCustomClass.ValueDate
            params(3) = New SqlParameter("@postingdate", SqlDbType.DateTime)
            params(3).Value = oCustomClass.BusinessDate
            params(4) = New SqlParameter("@loginid", SqlDbType.VarChar, 12)
            params(4).Value = oCustomClass.LoginId
            params(5) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 50)
            params(5).Value = oCustomClass.ReferenceNo
            params(6) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10)
            params(6).Value = oCustomClass.BankAccountID
            params(7) = New SqlParameter("@BayarPPh", SqlDbType.VarChar, 10)
            params(7).Value = oCustomClass.BayarPPh
            params(8) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(8).Value = oCustomClass.InsSeqNo
            params(9) = New SqlParameter("@HistorySeqNo_return", SqlDbType.Int)
            params(9).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spInstallRCVPPH", params)
            objTrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

            Return CInt(params(9).Value)
        Catch exp As Exception
            objTrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            'WriteException("InstallRcv", "SavingPostingPayment", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Sub SaveOtorisasiPPH(ByVal customclass As Parameter.InstallRcv)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(1) {}


        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@PPHNo", SqlDbType.VarChar, 20)
            params(0).Value = customclass.PPHNo

            params(1) = New SqlParameter("@loginid", SqlDbType.VarChar, 12)
            params(1).Value = customclass.LoginId

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spOtorisasiPPH", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("OtorisasiPPH", "SaveOtorisasiPPH", exp.Message + exp.StackTrace)
        End Try
    End Sub
    Public Sub RejectOtorisasiPPH(ByVal customclass As Parameter.InstallRcv)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(1) {}


        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@PPHNo", SqlDbType.VarChar, 20)
            params(0).Value = customclass.PPHNo

            params(1) = New SqlParameter("@loginid", SqlDbType.VarChar, 12)
            params(1).Value = customclass.LoginId

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spRejectOtorisasiPPH", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("OtorisasiPPH", "RejectOtorisasiPPH", exp.Message + exp.StackTrace)
        End Try
    End Sub

    Public Function InstallRCVBAModalKerja(ByVal oCustomClass As Parameter.InstallRcv) As Integer
        Dim objTrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(19) {}
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            Dim params1() As SqlParameter = New SqlParameter(4) {}
            If oCustomClass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = oCustomClass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = oCustomClass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = oCustomClass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"
                SqlHelper.ExecuteScalar(objTrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                oCustomClass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.CoyID
            params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.ApplicationID
            params(3) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.ValueDate
            params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(4).Value = oCustomClass.BusinessDate
            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId

            params(6) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            params(6).Value = oCustomClass.ReceivedFrom
            params(7) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50)
            params(7).Value = oCustomClass.ReferenceNo
            params(8) = New SqlParameter(PARAM_WOP, SqlDbType.VarChar, 2)
            params(8).Value = oCustomClass.WOP
            params(9) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(9).Value = oCustomClass.BankAccountID

            params(10) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(10).Value = oCustomClass.Notes
            params(11) = New SqlParameter("@BayarDi", SqlDbType.VarChar, 50)
            params(11).Value = IIf(oCustomClass.BayarDi = "0", DBNull.Value, oCustomClass.BayarDi)
            params(12) = New SqlParameter("@HistorySeqNo_return", SqlDbType.Int)
            params(12).Direction = ParameterDirection.Output

            params(13) = New SqlParameter("@FormID", SqlDbType.Char, 20)
            params(13).Value = oCustomClass.FormID
            params(14) = New SqlParameter("@Status", SqlDbType.VarChar, 10)
            params(14).Value = oCustomClass.Status

            params(15) = New SqlParameter("@PaidInterest", SqlDbType.Decimal)
            params(16) = New SqlParameter("@PaidPrincipal", SqlDbType.Decimal)
            params(17) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
            params(18) = New SqlParameter("@PaidRetensi", SqlDbType.Decimal)
            params(19) = New SqlParameter("@DendaPaid", SqlDbType.Decimal)

            params(15).Value = oCustomClass.PaidInterest
            params(16).Value = oCustomClass.PaidPrincipal
            params(17).Value = oCustomClass.InvoiceNo
            params(18).Value = oCustomClass.PaidRetensi
            params(19).Value = oCustomClass.DendaPaid


            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spInstallRCVBAModalKerja", params)
            objTrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

            Return CInt(params(12).Value)
        Catch exp As Exception
            objTrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            'WriteException("InstallRcv", "SavingPostingPayment", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function InstallRCVCollOtor(ByVal oCustomClass As Parameter.InstallRcv) As Integer
        Dim objTrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(44) {}
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            Dim params1() As SqlParameter = New SqlParameter(4) {}
            If oCustomClass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = oCustomClass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = oCustomClass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = oCustomClass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"
                SqlHelper.ExecuteScalar(objTrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                oCustomClass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.CoyID
            params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.ApplicationID
            params(3) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.ValueDate
            params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(4).Value = oCustomClass.BusinessDate
            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId

            params(6) = New SqlParameter(PARAM_INSTALLMENTDUEPAID, SqlDbType.Decimal)
            params(6).Value = oCustomClass.InstallmentDue
            params(7) = New SqlParameter(PARAM_INSTALLMENTDUEDESC, SqlDbType.VarChar, 50)
            params(7).Value = oCustomClass.InstallmentDueDesc
            params(8) = New SqlParameter(PARAM_LCINSTALLMENTPAID, SqlDbType.Decimal)
            params(8).Value = oCustomClass.LcInstallment
            params(9) = New SqlParameter(PARAM_LCINSTALLMENTDESC, SqlDbType.VarChar, 50)
            params(9).Value = oCustomClass.LcInstallmentDesc
            params(10) = New SqlParameter(PARAM_INSTALLMENTCOLLPAID, SqlDbType.Decimal)
            params(10).Value = oCustomClass.InstallmentCollFee
            params(11) = New SqlParameter(PARAM_INSTALLMENTCOLLDESC, SqlDbType.VarChar, 50)
            params(11).Value = oCustomClass.InstallmentCollFeeDesc

            params(12) = New SqlParameter(PARAM_INSURANCEDUEPAID, SqlDbType.Decimal)
            params(12).Value = oCustomClass.InsuranceDue
            params(13) = New SqlParameter(PARAM_INSURANCEDUEDESC, SqlDbType.VarChar, 50)
            params(13).Value = oCustomClass.InsuranceDueDesc
            params(14) = New SqlParameter(PARAM_LCINSURANCEPAID, SqlDbType.Decimal)
            params(14).Value = oCustomClass.LcInsurance
            params(15) = New SqlParameter(PARAM_LCINSURANCEDESC, SqlDbType.VarChar, 50)
            params(15).Value = oCustomClass.LcInsuranceDesc
            params(16) = New SqlParameter(PARAM_INSURANCECOLLPAID, SqlDbType.Decimal)
            params(16).Value = oCustomClass.InsuranceCollFee
            params(17) = New SqlParameter(PARAM_INSURANCECOLLDESC, SqlDbType.VarChar, 50)
            params(17).Value = oCustomClass.InsuranceCollFeeDesc

            params(18) = New SqlParameter(PARAM_PDCBOUNCEFEEPAID, SqlDbType.Decimal)
            params(18).Value = oCustomClass.PDCBounceFee
            params(19) = New SqlParameter(PARAM_PDCBOUNCEFEEDESC, SqlDbType.VarChar, 50)
            params(19).Value = oCustomClass.PDCBounceFeeDesc
            params(20) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEPAID, SqlDbType.Decimal)
            params(20).Value = oCustomClass.InsuranceClaimExpense
            params(21) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEPAIDDESC, SqlDbType.VarChar, 50)
            params(21).Value = oCustomClass.InsuranceClaimExpenseDesc
            params(22) = New SqlParameter(PARAM_STNKRENEEWALFEEPAID, SqlDbType.Decimal)
            params(22).Value = oCustomClass.STNKRenewalFee
            params(23) = New SqlParameter(PARAM_STNKRENEEWALFEEDESC, SqlDbType.VarChar, 50)
            params(23).Value = oCustomClass.STNKRenewalFeeDesc
            params(24) = New SqlParameter(PARAM_REPOSSESSIONFEEPAID, SqlDbType.Decimal)
            params(24).Value = oCustomClass.RepossessionFee
            params(25) = New SqlParameter(PARAM_REPOSSESSIONFEEDESC, SqlDbType.VarChar, 50)
            params(25).Value = oCustomClass.RepossessionFeeDesc
            params(26) = New SqlParameter(PARAM_PREPAIDPAID, SqlDbType.Decimal)
            params(26).Value = oCustomClass.Prepaid
            params(27) = New SqlParameter(PARAM_PREPAIDDESC, SqlDbType.VarChar, 50)
            params(27).Value = oCustomClass.PrepaidDesc

            params(28) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            params(28).Value = oCustomClass.ReceivedFrom
            params(29) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50)
            params(29).Value = oCustomClass.ReferenceNo
            params(30) = New SqlParameter(PARAM_WOP, SqlDbType.VarChar, 2)
            params(30).Value = oCustomClass.WOP
            params(31) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(31).Value = oCustomClass.BankAccountID
            params(32) = New SqlParameter(PARAM_AMOUNTRECEIVE, SqlDbType.Decimal)
            params(32).Value = oCustomClass.AmountReceive
            params(33) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(33).Value = oCustomClass.Notes
            params(34) = New SqlParameter("@BayarDi", SqlDbType.VarChar, 50)
            params(34).Value = IIf(oCustomClass.BayarDi = "0", DBNull.Value, oCustomClass.BayarDi)
            params(35) = New SqlParameter("@HistorySeqNo_return", SqlDbType.Int)
            params(35).Direction = ParameterDirection.Output
            params(36) = New SqlParameter("@CollectorID", SqlDbType.VarChar, 12)
            params(36).Value = IIf(oCustomClass.CollectorID = "0", DBNull.Value, oCustomClass.CollectorID)
            params(37) = New SqlParameter("@Penyetor", SqlDbType.VarChar, 50)
            params(37).Value = oCustomClass.Penyetor
            params(38) = New SqlParameter("@NoKwitansi", SqlDbType.VarChar, 50)
            params(38).Value = oCustomClass.NoKwitansi

            params(39) = New SqlParameter("@PLL", SqlDbType.Decimal)
            params(39).Value = oCustomClass.PLL
            params(40) = New SqlParameter("@PLLDesc", SqlDbType.VarChar, 50)
            params(40).Value = oCustomClass.PLLDesc
            params(41) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(41).Value = oCustomClass.InsSeqNo
            params(42) = New SqlParameter("@FormID", SqlDbType.Char, 20)
            params(42).Value = oCustomClass.FormID
            params(43) = New SqlParameter("@Status", SqlDbType.VarChar, 10)
            params(43).Value = oCustomClass.Status
            params(44) = New SqlParameter("@VoucherNo", SqlDbType.Char, 20)
            params(44).Value = oCustomClass.VoucherNO

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spInstallRCVCollOtor", params)
            objTrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

            Return CInt(params(35).Value)
        Catch exp As Exception
            objTrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            'WriteException("InstallRcv", "SavingPostingPayment", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Function GetGeneralPaging(ByVal CustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim oReturnValue As New Parameter.GeneralPaging
        Dim params(4) As SqlParameter

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function TransaksiPending(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim oReturnValue As New Parameter.GeneralPaging
        Dim params(4) As SqlParameter

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spTransPendingPaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function DetailTransaction(ByVal customClass As Parameter.InstallRcv) As DataTable
        Dim tbl As New DataTable
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@VoucherNO", SqlDbType.VarChar, 20)
        params(0).Value = customClass.VoucherNO

        Try
            tbl = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spCashBankTransactionDetail", params).Tables(0)
            Return tbl
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    Public Sub SaveOtorisasi(ByVal oCustomClass As Parameter.InstallRcv)
        Dim params(0) As SqlParameter
        Dim objTrans As SqlTransaction
        params(0) = New SqlParameter("@Otorisasi", SqlDbType.Structured)
        params(0).Value = oCustomClass.TableVoucherNO

        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spOtorisasiSave", params)
            objTrans.Commit()
        Catch exp As Exception
            objTrans.Rollback()
            WriteException("SaveOtorisasi", "SaveOtorisasi", exp.Message + exp.StackTrace)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Sub
    Public Sub RejectOtorisasi(ByVal oCustomClass As Parameter.InstallRcv)
        Dim params(0) As SqlParameter
        Dim objTrans As SqlTransaction
        params(0) = New SqlParameter("@VoucherNO", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.VoucherNO

        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spOtorisasiReject", params)
            objTrans.Commit()
        Catch exp As Exception
            objTrans.Rollback()
            WriteException("spOtorisasiReject", "spOtorisasiReject", exp.Message + exp.StackTrace)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Sub
    Public Sub EditOtorisasi(ByVal oCustomClass As Parameter.InstallRcv)
        Dim params(0) As SqlParameter
        Dim objTrans As SqlTransaction
        params(0) = New SqlParameter("@VoucherNO", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.VoucherNO

        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spOtorisasiEdit", params)
            objTrans.Commit()
        Catch exp As Exception
            objTrans.Rollback()
            WriteException("EditOtorisasi", "EditOtorisasi", exp.Message + exp.StackTrace)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Sub

    Public Function SavingPostingPaymentMobile(ByVal oCustomClass As Parameter.InstallRcv) As Integer
        Dim objTrans As SqlTransaction = Nothing
        Dim params() As SqlParameter = New SqlParameter(43) {}
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction
            Dim params1() As SqlParameter = New SqlParameter(4) {}
            If oCustomClass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = oCustomClass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = oCustomClass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = oCustomClass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"

                'SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                SqlHelper.ExecuteScalar(objTrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                oCustomClass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.CoyID
            params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.ApplicationID
            params(3) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.ValueDate
            params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(4).Value = oCustomClass.BusinessDate
            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId

            params(6) = New SqlParameter(PARAM_INSTALLMENTDUEPAID, SqlDbType.Decimal)
            params(6).Value = oCustomClass.InstallmentDue
            params(7) = New SqlParameter(PARAM_INSTALLMENTDUEDESC, SqlDbType.VarChar, 50)
            params(7).Value = oCustomClass.InstallmentDueDesc
            params(8) = New SqlParameter(PARAM_LCINSTALLMENTPAID, SqlDbType.Decimal)
            params(8).Value = oCustomClass.LcInstallment
            params(9) = New SqlParameter(PARAM_LCINSTALLMENTDESC, SqlDbType.VarChar, 50)
            params(9).Value = oCustomClass.LcInstallmentDesc
            params(10) = New SqlParameter(PARAM_INSTALLMENTCOLLPAID, SqlDbType.Decimal)
            params(10).Value = oCustomClass.InstallmentCollFee
            params(11) = New SqlParameter(PARAM_INSTALLMENTCOLLDESC, SqlDbType.VarChar, 50)
            params(11).Value = oCustomClass.InstallmentCollFeeDesc

            params(12) = New SqlParameter(PARAM_INSURANCEDUEPAID, SqlDbType.Decimal)
            params(12).Value = oCustomClass.InsuranceDue
            params(13) = New SqlParameter(PARAM_INSURANCEDUEDESC, SqlDbType.VarChar, 50)
            params(13).Value = oCustomClass.InsuranceDueDesc
            params(14) = New SqlParameter(PARAM_LCINSURANCEPAID, SqlDbType.Decimal)
            params(14).Value = oCustomClass.LcInsurance
            params(15) = New SqlParameter(PARAM_LCINSURANCEDESC, SqlDbType.VarChar, 50)
            params(15).Value = oCustomClass.LcInsuranceDesc
            params(16) = New SqlParameter(PARAM_INSURANCECOLLPAID, SqlDbType.Decimal)
            params(16).Value = oCustomClass.InsuranceCollFee
            params(17) = New SqlParameter(PARAM_INSURANCECOLLDESC, SqlDbType.VarChar, 50)
            params(17).Value = oCustomClass.InsuranceCollFeeDesc

            params(18) = New SqlParameter(PARAM_PDCBOUNCEFEEPAID, SqlDbType.Decimal)
            params(18).Value = oCustomClass.PDCBounceFee
            params(19) = New SqlParameter(PARAM_PDCBOUNCEFEEDESC, SqlDbType.VarChar, 50)
            params(19).Value = oCustomClass.PDCBounceFeeDesc
            params(20) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEPAID, SqlDbType.Decimal)
            params(20).Value = oCustomClass.InsuranceClaimExpense
            params(21) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEPAIDDESC, SqlDbType.VarChar, 50)
            params(21).Value = oCustomClass.InsuranceClaimExpenseDesc
            params(22) = New SqlParameter(PARAM_STNKRENEEWALFEEPAID, SqlDbType.Decimal)
            params(22).Value = oCustomClass.STNKRenewalFee
            params(23) = New SqlParameter(PARAM_STNKRENEEWALFEEDESC, SqlDbType.VarChar, 50)
            params(23).Value = oCustomClass.STNKRenewalFeeDesc
            params(24) = New SqlParameter(PARAM_REPOSSESSIONFEEPAID, SqlDbType.Decimal)
            params(24).Value = oCustomClass.RepossessionFee
            params(25) = New SqlParameter(PARAM_REPOSSESSIONFEEDESC, SqlDbType.VarChar, 50)
            params(25).Value = oCustomClass.RepossessionFeeDesc
            params(26) = New SqlParameter(PARAM_PREPAIDPAID, SqlDbType.Decimal)
            params(26).Value = oCustomClass.Prepaid
            params(27) = New SqlParameter(PARAM_PREPAIDDESC, SqlDbType.VarChar, 50)
            params(27).Value = oCustomClass.PrepaidDesc

            params(28) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            params(28).Value = oCustomClass.ReceivedFrom
            params(29) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50)
            params(29).Value = oCustomClass.ReferenceNo
            params(30) = New SqlParameter(PARAM_WOP, SqlDbType.VarChar, 2)
            params(30).Value = oCustomClass.WOP
            params(31) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(31).Value = oCustomClass.BankAccountID
            params(32) = New SqlParameter(PARAM_AMOUNTRECEIVE, SqlDbType.Decimal)
            params(32).Value = oCustomClass.AmountReceive
            params(33) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(33).Value = oCustomClass.Notes
            params(34) = New SqlParameter("@BayarDi", SqlDbType.VarChar, 50)
            params(34).Value = IIf(oCustomClass.BayarDi = "0", DBNull.Value, oCustomClass.BayarDi)
            params(35) = New SqlParameter("@HistorySeqNo_return", SqlDbType.Int)
            params(35).Direction = ParameterDirection.Output
            params(36) = New SqlParameter("@CollectorID", SqlDbType.VarChar, 12)
            params(36).Value = IIf(oCustomClass.CollectorID = "0", DBNull.Value, oCustomClass.CollectorID)
            params(37) = New SqlParameter("@Penyetor", SqlDbType.VarChar, 50)
            params(37).Value = oCustomClass.Penyetor
            params(38) = New SqlParameter("@NoKwitansi", SqlDbType.VarChar, 50)
            params(38).Value = oCustomClass.NoKwitansi

            params(39) = New SqlParameter("@PLL", SqlDbType.Decimal)
            params(39).Value = oCustomClass.PLL
            params(40) = New SqlParameter("@PLLDesc", SqlDbType.VarChar, 50)
            params(40).Value = oCustomClass.PLLDesc


            params(41) = New SqlParameter("@TitipanAngsuran", SqlDbType.Decimal)
            params(41).Value = oCustomClass.TitipanAngsuran
            params(42) = New SqlParameter("@TitipanAngsuranDesc", SqlDbType.VarChar, 50)
            params(42).Value = oCustomClass.TitipanAngsuranDesc


            params(43) = New SqlParameter("@errMsg", SqlDbType.VarChar, 200)
            params(43).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, PROC_POSTINGPAYMENTMOBILE, params)
            '' SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, PROC_POSTINGPAYMENT, params)
            Dim ret = CInt(params(35).Value)

            If (ret <= 0) Then
                objTrans.Rollback()
                oCustomClass.strError = params(43).Value
            Else
                objTrans.Commit()
            End If
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

            Return ret
        Catch exp As Exception
            objTrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            'WriteException("InstallRcv", "SavingPostingPayment", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetComboDepartemen(ByVal customclass As Parameter.InstallRcv) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSuspendAllocationGeneralByDepartement").Tables(0)
    End Function

    Public Function GetComboCabang(ByVal customclass As Parameter.InstallRcv) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSuspendAllocationGeneralByBranch").Tables(0)
    End Function

    Public Sub AlokasiPembNonARSaveEdit(ByVal ocustomClass As Parameter.InstallRcv)
        Dim params(6) As SqlParameter

        params(0) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.ReferenceNo
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(1).Value = ocustomClass.Description
        params(2) = New SqlParameter("@Amount", SqlDbType.Decimal)
        params(2).Value = ocustomClass.Amount
        params(3) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(3).Value = ocustomClass.BranchId
        params(4) = New SqlParameter("@PaymentAllocationID", SqlDbType.Char, 10)
        params(4).Value = ocustomClass.PaymentAllocationID.Trim
        params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(5).Value = ocustomClass.BusinessDate
        params(6) = New SqlParameter("@DepartementID", SqlDbType.Char, 3)
        params(6).Value = ocustomClass.DepartementID

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spAlokasiPembNonARSave", params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.AlokasiPembNonARSaveEdit.AlokasiPembNonARSaveEdit")
        End Try
    End Sub

    Public Function InstallKoreksiModalKerja(ByVal oCustomClass As Parameter.InstallRcv) As Integer
        Dim objTrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(12) {}
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction
            Dim params1() As SqlParameter = New SqlParameter(4) {}
            If oCustomClass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = oCustomClass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = oCustomClass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = oCustomClass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"
                SqlHelper.ExecuteScalar(objTrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                oCustomClass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.CoyID
            params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.ApplicationID
            params(3) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.ValueDate
            params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(4).Value = oCustomClass.BusinessDate
            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId
            params(6) = New SqlParameter("@PaidPrincipal", SqlDbType.Decimal)
            params(6).Value = oCustomClass.PaidPrincipal
            params(7) = New SqlParameter("@PaidInterest", SqlDbType.Decimal)
            params(7).Value = oCustomClass.PaidInterest
            params(8) = New SqlParameter("@DendaPaid", SqlDbType.Decimal)
            params(8).Value = oCustomClass.DendaPaid
            params(9) = New SqlParameter("@FormID", SqlDbType.VarChar, 12)
            params(9).Value = oCustomClass.FormID
            params(10) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 12)
            params(10).Value = oCustomClass.BankAccountID
            params(11) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 12)
            params(11).Value = oCustomClass.ReferenceNo
            params(12) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 12)
            params(12).Value = oCustomClass.InvoiceNo

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spInstallKoreksiModalKerja", params)
            objTrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

            Return CInt(params(12).Value)
        Catch exp As Exception
            objTrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            'WriteException("InstallRcv", "InstallKoreksiModalKerja", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function InstallKoreksiFactoring(ByVal oCustomClass As Parameter.InstallRcv) As Integer
        Dim objTrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(12) {}
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction
            Dim params1() As SqlParameter = New SqlParameter(4) {}
            If oCustomClass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = oCustomClass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = oCustomClass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = oCustomClass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"
                SqlHelper.ExecuteScalar(objTrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                oCustomClass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.CoyID
            params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.ApplicationID
            params(3) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.ValueDate
            params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(4).Value = oCustomClass.BusinessDate
            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId
            params(6) = New SqlParameter("@PaidPrincipal", SqlDbType.Decimal)
            params(6).Value = oCustomClass.PaidPrincipal
            params(7) = New SqlParameter("@PaidInterest", SqlDbType.Decimal)
            params(7).Value = oCustomClass.PaidInterest
            params(8) = New SqlParameter("@DendaPaid", SqlDbType.Decimal)
            params(8).Value = oCustomClass.DendaPaid
            params(9) = New SqlParameter("@FormID", SqlDbType.VarChar, 12)
            params(9).Value = oCustomClass.FormID
            params(10) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 12)
            params(10).Value = oCustomClass.BankAccountID
            params(11) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 12)
            params(11).Value = oCustomClass.ReferenceNo
            params(12) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 12)
            params(12).Value = oCustomClass.InvoiceNo

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spInstallKoreksiFactoring", params)
            objTrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

            Return CInt(params(12).Value)
        Catch exp As Exception
            objTrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            'WriteException("InstallRcv", "InstallKoreksiModalKerja", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Sub AlokasiPembTolakanBGSaveEdit(ByVal ocustomClass As Parameter.InstallRcv)
        Dim params(5) As SqlParameter

        params(0) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.ReferenceNo
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(1).Value = ocustomClass.Description
        params(2) = New SqlParameter("@Amount", SqlDbType.Decimal)
        params(2).Value = ocustomClass.Amount
        params(3) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(3).Value = ocustomClass.BranchId
        params(4) = New SqlParameter("@LoginID", SqlDbType.VarChar, 15)
        params(4).Value = ocustomClass.LoginId
        params(5) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(5).Value = ocustomClass.ApplicationID

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spAlokasiPembTolakanBGSave", params) 'spAlokasiPembNonARSave
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

End Class

