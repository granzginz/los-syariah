

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class AgreementList
    Inherits AccMntBase
    Private Const spListAgreement As String = "spAgreementListPaging"
    Private Const spListAgreementView As String = "spAgreementListPagingView"
    Private Const spListAgreementFactoring As String = "spAgreementListFactoringPaging"
    Private Const spListAgreementFactoringView As String = "spAgreementListFactoringPagingView"
    Private Const spListRCVPPH As String = "spRCVPPHPaging"
    Private Const spListAgreementModalKerja As String = "spAgreementListModalKerjaPaging"
    Private Const spListAgreementModalKerjaView As String = "spAgreementListModalKerjaPagingView"
    Private Const spListAgreementDrawdown As String = "spAgreementListDrawdownPaging"
    Private Const spAgreementListDisburseFactoringPaging As String = "spAgreementListDisburseFactoringPaging"
    Private Const spReportListAgreement As String = "spAgreementListReport"
    Private Const spListPrepayRequest As String = "spPrepayRequestList"
    Private Const spListAgreementFACT As String = "spAgreementListPagingFACT"
    Private Const spListAgreementMDKJ As String = "spAgreementListPagingMDKJ"
    Private Const spListAgreementnew As String = "spAgreementListPagingnew"

    Public Function ListAgreement(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListAgreement, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementList", "ListAgreement", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListAgreementView(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListAgreementView, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementListView", "ListAgreementView", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ListAgreementLinkage(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
		Dim params() As SqlParameter = New SqlParameter(4) {}
		Try
			params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
			params(0).Value = customclass.CurrentPage

			params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
			params(1).Value = customclass.PageSize

			params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
			params(2).Value = customclass.WhereCond

			params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
			params(3).Value = customclass.SortBy

			params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
			params(4).Direction = ParameterDirection.Output

			customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spAgreementListLinkAgePaging", params).Tables(0)
			customclass.TotalRecord = CInt(params(4).Value)
			Return customclass
		Catch exp As Exception
			WriteException("AgreementListLinkage", "ListAgreementLinkage", exp.Message + exp.StackTrace)
		End Try
	End Function
    Public Function ListAgreementFactoring(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListAgreementFactoring, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementListFactoring", "ListAgreementFactoring", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListAgreementFactoringView(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListAgreementFactoringView, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementListFactoringView", "ListAgreementFactoringView", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListRCVPPH(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListRCVPPH, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementListFactoring", "ListAgreementFactoring", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListAgreementModalKerja(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListAgreementModalKerja, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementListModalKerja", "ListAgreementModalKerja", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListAgreementModalKerjaView(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListAgreementModalKerjaView, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementListModalKerjaView", "ListAgreementModalKerjaView", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ReportListAgreement(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spReportListAgreement, params)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementList", "ReportListAgreement", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function PrepayRequestList(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListPrepayRequest, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementList", "PrepayRequestList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AgreementIdentifikasiPembayaran(ByVal customclass As Parameter.AgreementList, ByVal dtTable As DataTable) As Parameter.AgreementList
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@ip", SqlDbType.Structured)
            params(0).Value = dtTable


            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spIdentifikasiPemb", params).Tables(0)

            Return customclass
        Catch exp As Exception
            WriteException("AgreementIdentifikasiPembayaran", "spIdentifikasiPemb", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function UcAgreementList(ByVal oCustomClass As Parameter.AccMntBase) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.CustomerID

            Return SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spUcAgreementList", params).Tables(0)
        Catch exp As Exception
            WriteException("UcAgreementList", "spUcAgreementList", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Function AgreementListDrawdown(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar)
            params(4).Value = customclass.BranchId

            params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output

            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListAgreementDrawdown, params).Tables(0)
            customclass.TotalRecord = CInt(params(5).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementList", "AgreementListDrawdown", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AgreementListDisburseFactoring(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar)
            params(4).Value = customclass.BranchId

            params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output

            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAgreementListDisburseFactoringPaging, params).Tables(0)
            customclass.TotalRecord = CInt(params(5).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementList", "AgreementListDisburseFactoring", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function UcAgreementListFactoring(ByVal oCustomClass As Parameter.AccMntBase) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@NoFasilitas", SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.NoFasilitas

            Return SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spUcAgreementListFactbyFacility", params).Tables(0)
        Catch exp As Exception
            WriteException("UcAgreementListFactoring", "spUcAgreementListFactoring", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function UcAgreementListMdKj(ByVal oCustomClass As Parameter.AccMntBase) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@NoFasilitas", SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.NoFasilitas

            Return SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spUcAgreementListMDKJbyFacility", params).Tables(0)
        Catch exp As Exception
            WriteException("UcAgreementListMdKj", "spUcAgreementListMdKj", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function UcAgreementListInv(ByVal oCustomClass As Parameter.AccMntBase) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@NoFasilitas", SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.NoFasilitas

            Return SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spUcAgreementListINVbyFacility", params).Tables(0)
        Catch exp As Exception
            WriteException("UcAgreementListInv", "spUcAgreementListInv", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListAgreementFACT(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListAgreementFACT, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementList", "ListAgreement", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ListAgreementMDKJ(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListAgreementMDKJ, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementList", "ListAgreement", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AgreementListPrepaid(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spAgreementListPrepaidPaging", params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementList", "ListPrepaid", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function PrepaidAllocation(oCustomClass2 As Parameter.InstallRcv)
        Dim objTrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(36) {}

            'm_connection = New SqlConnection(oCustomClass2.strConnection)
            'If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            'objTrans = m_connection.BeginTransaction

            params(0) = New SqlParameter("@ValueDate", SqlDbType.Date)
            params(0).Value = oCustomClass2.ValueDate
            params(1) = New SqlParameter("@CoyID", SqlDbType.Char, 3)
            params(1).Value = oCustomClass2.CoyID
            params(2) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(2).Value = oCustomClass2.BranchId
            params(3) = New SqlParameter("@BusinessDate", SqlDbType.Date)
            params(3).Value = oCustomClass2.BusinessDate
            params(4) = New SqlParameter("@ReceivedFrom", SqlDbType.VarChar, 100)
            params(4).Value = oCustomClass2.ReceivedFrom
            params(5) = New SqlParameter("@WOP", SqlDbType.Char, 3)
            params(5).Value = oCustomClass2.WOP
            params(6) = New SqlParameter("@LoginId", SqlDbType.VarChar, 20)
            params(6).Value = oCustomClass2.LoginId
            params(7) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10)
            params(7).Value = oCustomClass2.BankAccountID
            params(8) = New SqlParameter("@Notes", SqlDbType.VarChar, 100)
            params(8).Value = oCustomClass2.Notes
            params(9) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(9).Value = oCustomClass2.ApplicationID
            params(10) = New SqlParameter("@InstallmentDue", SqlDbType.Decimal)
            params(10).Value = oCustomClass2.InstallmentDue
            params(11) = New SqlParameter("@InstallmentDueDesc", SqlDbType.VarChar, 100)
            params(11).Value = oCustomClass2.InstallmentDueDesc
            params(12) = New SqlParameter("@LcInstallment", SqlDbType.Decimal)
            params(12).Value = oCustomClass2.LcInstallment
            params(13) = New SqlParameter("@LcInstallmentDesc", SqlDbType.VarChar, 100)
            params(13).Value = oCustomClass2.LcInstallmentDesc
            params(14) = New SqlParameter("@InstallmentCollFee", SqlDbType.Decimal)
            params(14).Value = oCustomClass2.InstallmentCollFee
            params(15) = New SqlParameter("@InstallmentCollFeeDesc", SqlDbType.VarChar, 100)
            params(15).Value = oCustomClass2.InstallmentCollFeeDesc
            params(16) = New SqlParameter("@InsuranceDue", SqlDbType.Decimal)
            params(16).Value = oCustomClass2.InsuranceDue
            params(17) = New SqlParameter("@InsuranceDueDesc", SqlDbType.VarChar, 100)
            params(17).Value = oCustomClass2.InsuranceDueDesc
            params(18) = New SqlParameter("@LcInsurance", SqlDbType.Decimal)
            params(18).Value = oCustomClass2.LcInsurance
            params(19) = New SqlParameter("@LcInsuranceDesc", SqlDbType.VarChar, 100)
            params(19).Value = oCustomClass2.LcInsuranceDesc
            params(20) = New SqlParameter("@InsuranceCollFee", SqlDbType.Decimal)
            params(20).Value = oCustomClass2.InsuranceCollFee
            params(21) = New SqlParameter("@InsuranceCollFeeDesc", SqlDbType.VarChar, 100)
            params(21).Value = oCustomClass2.InsuranceCollFeeDesc
            params(22) = New SqlParameter("@PDCBounceFee", SqlDbType.Decimal)
            params(22).Value = oCustomClass2.PDCBounceFee
            params(23) = New SqlParameter("@PDCBounceFeeDesc", SqlDbType.VarChar, 100)
            params(23).Value = oCustomClass2.PDCBounceFeeDesc
            params(24) = New SqlParameter("@STNKRenewalFee", SqlDbType.Decimal)
            params(24).Value = oCustomClass2.STNKRenewalFee
            params(25) = New SqlParameter("@STNKRenewalFeeDesc", SqlDbType.VarChar, 100)
            params(25).Value = oCustomClass2.STNKRenewalFeeDesc
            params(26) = New SqlParameter("@InsuranceClaimExpense", SqlDbType.Decimal)
            params(26).Value = oCustomClass2.InsuranceClaimExpense
            params(27) = New SqlParameter("@InsuranceClaimExpenseDesc", SqlDbType.VarChar, 100)
            params(27).Value = oCustomClass2.InsuranceClaimExpenseDesc
            params(28) = New SqlParameter("@RepossessionFee", SqlDbType.Decimal)
            params(28).Value = oCustomClass2.RepossessionFee
            params(29) = New SqlParameter("@RepossessionFeeDesc", SqlDbType.VarChar, 100)
            params(29).Value = oCustomClass2.RepossessionFeeDesc
            params(30) = New SqlParameter("@Prepaid", SqlDbType.Decimal)
            params(30).Value = oCustomClass2.Prepaid
            params(31) = New SqlParameter("@PrepaidDesc", SqlDbType.VarChar, 100)
            params(31).Value = oCustomClass2.PrepaidDesc
            params(32) = New SqlParameter("@PLL", SqlDbType.Decimal)
            params(32).Value = oCustomClass2.PLL
            params(33) = New SqlParameter("@PLLDesc", SqlDbType.VarChar, 100)
            params(33).Value = oCustomClass2.PLLDesc
            params(34) = New SqlParameter("@TitipanAngsuran", SqlDbType.Decimal)
            params(34).Value = oCustomClass2.TitipanAngsuran
            params(35) = New SqlParameter("@TitipanAngsuranDesc", SqlDbType.VarChar, 100)
            params(35).Value = oCustomClass2.TitipanAngsuranDesc
            params(36) = New SqlParameter("@AmountReceive", SqlDbType.Decimal)
            params(36).Value = oCustomClass2.AmountReceive

            SqlHelper.ExecuteNonQuery(oCustomClass2.strConnection, CommandType.StoredProcedure, "spAllocationPrepaid", params)
            objTrans.Commit()

        Catch exp As Exception
            objTrans.Rollback()
            WriteException("PrepaidAllocation", "PrepaidAllocation", exp.Message + exp.StackTrace)
        Finally
            'If m_connection.State = ConnectionState.Open Then m_connection.Close()
            'm_connection.Dispose()
        End Try

    End Function

    Public Function ListAgreementnew(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@ModuleApplication", SqlDbType.VarChar, 100)
            params(4).Value = customclass.ModuleApplication

            params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output


            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListAgreementnew, params).Tables(0)
            customclass.TotalRecord = CInt(params(5).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementList", "ListAgreement", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class