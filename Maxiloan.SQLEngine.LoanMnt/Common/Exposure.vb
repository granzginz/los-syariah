
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region
Public Class Exposure : Inherits AccMntBase

    Private Const spAgreeEx As String = "spViewAgreementExposure"
    Private Const spAgreeTransfer As String = "spViewAgreementTransfer"
    Private Const spCustEx As String = "spViewCustomerExposure"

#Region "Agreement Exposure"
    Public Function GetAgreeEx(ByVal customclass As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID.Trim


            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spAgreeEx, params)

            With customclass
                If objread.Read Then

                    .MaxOutstandingBalance = CType(objread("MaxOutstandingBalance"), Double)
                    .OutstandingBalanceAR = CType(objread("OutstandingBalanceAR"), Double)
                    .OutstandingPrincipal = CType(objread("OutstandingPrincipal"), Double)
                    .OutStandingInterest = CType(objread("OutStandingInterest"), Double)
                    .NumOfAssetRepossed = CType(objread("NumOfAssetRepossed"), Integer)
                    .NumOfAssetInventoried = CType(objread("NumOfAssetInventoried"), Integer)
                    .NumOfRescheduling = CType(objread("NumOfRescheduling"), Integer)
                    .NumOfAgreementTransfer = CType(objread("NumOfAgreementTransfer"), Integer)
                    .NumOfAssetReplacement = CType(objread("NumOfAssetReplacement"), Integer)
                    .NumOfBounceCheque = CType(objread("NumOfBounceCheque"), Integer)
                    .Bucket1Principle = CType(objread("Bucket1_Principle"), String)
                    .Bucket2Principle = CType(objread("Bucket2_Principle"), String)
                    .Bucket3Principle = CType(objread("Bucket3_Principle"), String)
                    .Bucket4Principle = CType(objread("Bucket4_Principle"), String)
                    .Bucket5Principle = CType(objread("Bucket5_Principle"), String)
                    .Bucket6Principle = CType(objread("Bucket6_Principle"), String)
                    .Bucket7Principle = CType(objread("Bucket7_Principle"), String)
                    .Bucket8Principle = CType(objread("Bucket8_Principle"), String)
                    .Bucket9Principle = CType(objread("Bucket9_Principle"), String)
                    .Bucket10Principle = CType(objread("Bucket10_Principle"), String)
                    .Bucket1_gross = CType(objread("Bucket1_gross"), String)
                    .Bucket2_gross = CType(objread("Bucket2_gross"), String)
                    .Bucket3_gross = CType(objread("Bucket3_gross"), String)
                    .Bucket4_gross = CType(objread("Bucket4_gross"), String)
                    .Bucket5_gross = CType(objread("Bucket5_gross"), String)
                    .Bucket6_gross = CType(objread("Bucket6_gross"), String)
                    .Bucket7_gross = CType(objread("Bucket7_gross"), String)
                    .Bucket8_gross = CType(objread("Bucket8_gross"), String)
                    .Bucket9_gross = CType(objread("Bucket9_gross"), String)
                    .Bucket10_gross = CType(objread("Bucket10_gross"), String)
                    .TotalAllBucket = CType(objread("TotalAllBucket"), Decimal)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("Exposure", "GetAgreeEx", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListAgree(ByVal customclass As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@Applicationid", SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID
            customclass.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAgreeTransfer, params).Tables(0)
            'customclass.TotalRecord = CInt(params(4).Value)

            Return customclass
        Catch exp As Exception
            WriteException("Exposure", "ListAgree", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "Customer Exposure"
    Public Function GetCustomerEx(ByVal customclass As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@CustomerId", SqlDbType.VarChar, 20)
            params(0).Value = customclass.CustomerID.Trim


            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spCustEx, params)

            With customclass
                If objread.Read Then

                    ''OutstandingAmount
                    'lblOSBalance.Text = objReader.Item("OutstandingBalanceAR").ToString.Trim
                    'lblOSPrincipal.Text = objReader.Item("OutstandingPrincipal").ToString.Trim
                    'lblOSInterest.Text = objReader.Item("OutstandingInterest").ToString.Trim
                    ''Max Of
                    'LblMaxOfOSBalance.Text = FormatNumber(objReader.Item("maxOutStandingBalance").ToString.Trim, 0)
                    'LblMaxOfOverDueDays.Text = FormatNumber(objReader.Item("maxOverDuedays").ToString.Trim, 0)
                    'LblInstallmentAmount.Text = FormatNumber(objReader.Item("MaxInstallmentAmount").ToString.Trim, 0)
                    'LblOverdueAmount.Text = FormatNumber(objReader.Item("maxOverDueAmount").ToString.Trim, 0)
                    ''Number Of
                    'lblActiveAgreement.Text = FormatNumber(objReader.Item("NumofActiveAgreement").ToString.Trim, 0)
                    'lblAssetRepossessed.Text = FormatNumber(objReader.Item("NumOfAssetRepossed").ToString.Trim, 0)
                    'lblInProcessAgreement.Text = FormatNumber(objReader.Item("NumofInProcessAgreement").ToString.Trim, 0)
                    'lblAssetInventoried.Text = FormatNumber(objReader.Item("NumOfAssetInventoried").ToString.Trim, 0)
                    'lblAssetInFinancing.Text = FormatNumber(objReader.Item("NumOfAssetInFinancing").ToString.Trim, 0)
                    'lblWrittenOffAgreement.Text = FormatNumber(objReader.Item("NumOfWrittenOffAgreement").ToString.Trim, 0)
                    'LblRejectedAgreement.Text = FormatNumber(objReader.Item("NumOfRejectedAgreement").ToString.Trim, 0)
                    'LblNonAccrualAgreement.Text = FormatNumber(objReader.Item("NumOfNonAccrualAgreement").ToString.Trim, 0)
                    'LblCancelledAgreement.Text = FormatNumber(objReader.Item("NumOfCancelledAgreement").ToString.Trim, 0)
                    'LblBounceCheque.Text = FormatNumber(objReader.Item("NumOfBounceCheque").ToString.Trim, 0)


                    .OutstandingBalanceAR = CType(objread("OutstandingBalanceAR"), Double)
                    .OutstandingPrincipal = CType(objread("OutstandingPrincipal"), Double)
                    .OutStandingInterest = CType(objread("OutStandingInterest"), Double)

                    .MaxOutstandingBalance = CType(objread("MaxOutstandingBalance"), Double)
                    .MaxOverdueDays = CType(objread("maxOverDuedays"), Integer)
                    .MaximumInstallment = CType(objread("MaxInstallmentAmount"), Double)
                    .MaxOverDueAmount = CType(objread("maxOverDueAmount"), Double)


                    .NumofActiveAgreement = CType(objread("NumofActiveAgreement"), Integer)
                    .NumOfAssetRepossed = CType(objread("NumOfAssetRepossed"), Integer)
                    .NumofInProcessAgreement = CType(objread("NumofInProcessAgreement"), Integer)
                    .NumOfAssetInventoried = CType(objread("NumOfAssetInventoried"), Integer)
                    .NumOfAssetInFinancing = CType(objread("NumOfAssetInFinancing"), Integer)
                    .NumOfWrittenOffAgreement = CType(objread("NumOfWrittenOffAgreement"), Integer)
                    .NumOfRejectedAgreement = CType(objread("NumOfRejectedAgreement"), Integer)
                    .NumOfNonAccrualAgreement = CType(objread("NumOfNonAccrualAgreement"), Integer)
                    .NumOfCancelledAgreement = CType(objread("NumOfCancelledAgreement"), Integer)
                    .NumOfBounceCheque = CType(objread("NumOfBounceCheque"), Integer)
                    .Bucket1Principle = CType(objread("Bucket1_text"), String)
                    .Bucket2Principle = CType(objread("Bucket2_text"), String)
                    .Bucket3Principle = CType(objread("Bucket3_text"), String)
                    .Bucket4Principle = CType(objread("Bucket4_text"), String)
                    .Bucket5Principle = CType(objread("Bucket5_text"), String)
                    .Bucket6Principle = CType(objread("Bucket6_text"), String)
                    .Bucket7Principle = CType(objread("Bucket7_text"), String)
                    .Bucket8Principle = CType(objread("Bucket8_text"), String)
                    .Bucket9Principle = CType(objread("Bucket9_text"), String)
                    .Bucket10Principle = CType(objread("Bucket10_text"), String)
                    .Bucket1_gross = CType(objread("Bucket1_gross"), String)
                    .Bucket2_gross = CType(objread("Bucket2_gross"), String)
                    .Bucket3_gross = CType(objread("Bucket3_gross"), String)
                    .Bucket4_gross = CType(objread("Bucket4_gross"), String)
                    .Bucket5_gross = CType(objread("Bucket5_gross"), String)
                    .Bucket6_gross = CType(objread("Bucket6_gross"), String)
                    .Bucket7_gross = CType(objread("Bucket7_gross"), String)
                    .Bucket8_gross = CType(objread("Bucket8_gross"), String)
                    .Bucket9_gross = CType(objread("Bucket9_gross"), String)
                    .Bucket10_gross = CType(objread("Bucket10_gross"), String)
                    '.TotalAllBucket = CType(objread("TotalAllBucket"), Integer)
                    .TotalAllBucket = CType(objread("TotalAllBucket"), Decimal)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("Exposure", "GetCustomerEx", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
End Class
