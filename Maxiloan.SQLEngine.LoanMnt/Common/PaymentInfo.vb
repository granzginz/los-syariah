
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class PaymentInfo : Inherits AccMntBase
    Private Const PaymentInfo As String = "spPaymentInfo"
    Private Const PaymentInfoOL As String = "spPaymentInfoOL"
    Private Const PrePaymentInfo As String = "spPrePaymentInfo"
    Private Const PrePaymentInfoOL As String = "spPrePaymentInfoOL"
    Private Const PrePaymentInfoNormal As String = "spPrePaymentInfoNormal"

    Private Const OpenCashier As String = "spCashierTransactionOpen"
    Private Const CloseHeadCashier As String = "spCashierHeadClose"
    Private Const CloseCashier As String = "spCashierTransactionClose"
    Private Const SPCashierListClose As String = "spCashierListClose"
    Private Const spCashierHeadListClose As String = "spCashierHeadListClose"
    Private Const spCheckHeadCashier As String = "spCheckHeadCashier"
    Private Const spCheckAllCashierClose As String = "spCheckAllCashierClose"

    Private Const PARAM_ISHEADCASHIERCLOSE As String = "@IsHeadCashierClose"
    Private Const PARAM_HEADCASHIERBALANCE As String = "@HeadCashierBalance"
    Private Const PARAM_OPENINGSEQUENCE As String = "@openingsequence"
    Private Const PARAM_STATUS As String = "@status"
    Private Const PARAM_OPENINGAMOUNT As String = "@OpeningAmount"
    Private Const PARAM_AMOUNTPDC As String = "@PDCAmount"
    Private Const PARAM_NUMBERPDC As String = "@NumberPDC"

    Private Const PARAM_OPENINGDATE As String = "@openingdate"
    Private Const PARAM_ISCLOSED As String = "@isclosed"
    Private Const PARAM_CLOSINGAMOUNT As String = "@ClosingAmount"

    Private Const PARAM_PREPAYMENTTYPE As String = "@PrepaymentType"
    Private Const IsPaymentHaveOtor As String = "spGetIsPaymentHaveOtor"

#Region "Payment Info"
    Public Function GetPaymentInfo(ByVal customclass As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(0).Value = customclass.ValueDate

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID.Trim

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, PaymentInfo, params)

            If Not IsNothing(objread) Then
                With customclass
                    If objread.Read Then
                        .BranchAgreement = CType(objread("BranchFullName"), String)
                        .Agreementno = CType(objread("AgreementNo"), String)
                        .CustomerName = CType(objread("Name"), String)
                        .CustomerType = CType(objread("CustomerType"), String)
                        .CustomerID = CType(objread("CustomerID"), String)

                        .NextInstallmentDate = CType(objread("NextInstallmentDueDate"), Date)
                        .NextInstallmentNumber = CType(objread("NextInstallmentNumber"), Integer)
                        .InstallmentAmount = CType(objread("InstallmentAmount"), Double)

                        .InstallmentDue = CType(objread("InstallmentDue"), Double)
                        .LcInstallment = CType(objread("LCInstallment"), Double)
                        .InstallmentCollFee = CType(objread("InstallCollFee"), Double)

                        .InsuranceDue = CType(objread("InsuranceDue"), Double)
                        .LcInsurance = CType(objread("LCInsurance"), Double)
                        .InsuranceCollFee = CType(objread("InsuranceCollFee"), Double)

                        .PDCBounceFee = CType(objread("PDCBounceFee"), Double)
                        .STNKRenewalFee = CType(objread("STNKFee"), Double)
                        .InsuranceClaimExpense = CType(objread("InsuranceClaimExpense"), Double)
                        .RepossessionFee = CType(objread("CollectionExpense"), Double)
                        .Prepaid = CType(objread("ContractPrepaidAmount"), Double)
                        .MaximumInstallment = CType(objread("MaximumInstallmentAmount"), Double)
                        .OutStandingInterest = CType(objread("OutStandingInterestUnDue"), Double)
                        .OutstandingPrincipal = CType(objread("OutStandingPrincipalUnDue"), Double)
                        .ContractStatus = CType(objread("ContractStatus"), String)
                        .PrepaidHoldStatus = CType(objread("PrepaidHoldStatus"), String)
                        .PrepaidHoldStatusDesc = CType(objread("PrepaidHoldStatusDesc"), String)
                        .FundingCoyName = CType(objread("FundingCoyName"), String)
                        .FundingPledgeStatus = CType(objread("FundingPledgeStatus"), String)
                        .ResiduValue = CType(objread("ResiduValue"), Double)

                        .PrioritasPembayaran = CType(objread("prioritasPembayaran"), String)
                        .SPType = CType(objread("SPType"), String)
                        .BlokirBayar = CType(objread("BlokirBayar"), Boolean)
                        .AmountToBePaid = CType(objread("InstallmentAmount"), Double)
                        .TitipanAngsuran = CType(objread("TitipanAngsuran"), Double)
                        .ToleransiBayarKurang = CType(objread("BatasToleransiBawah"), String)
                        .ToleransiBayarLebih = CType(objread("BatasToleransiAtas"), String)
                        .isAkhirBayar = CType(objread("isAkhirBayar"), Boolean)
                    End If
                    objread.Close()
                End With
            End If

            Return customclass
        Catch exp As Exception
            WriteException("AgreementList", "ReportListAgreement", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetPaymentInfoOL(ByVal customclass As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(0).Value = customclass.ValueDate

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID.Trim

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, PaymentInfoOL, params)

            If Not IsNothing(objread) Then
                With customclass
                    If objread.Read Then
                        .BranchAgreement = CType(objread("BranchFullName"), String)
                        .Agreementno = CType(objread("AgreementNo"), String)
                        .CustomerName = CType(objread("Name"), String)
                        .CustomerType = CType(objread("CustomerType"), String)
                        .CustomerID = CType(objread("CustomerID"), String)

                        .NextInstallmentDate = CType(objread("NextInstallmentDueDate"), Date)
                        .NextInstallmentNumber = CType(objread("NextInstallmentNumber"), Integer)
                        .InstallmentAmount = CType(objread("InstallmentAmount"), Double)

                        .InstallmentDue = CType(objread("InstallmentDue"), Double)
                        .LcInstallment = CType(objread("LCInstallment"), Double)
                        .InstallmentCollFee = CType(objread("InstallCollFee"), Double)

                        .InsuranceDue = CType(objread("InsuranceDue"), Double)
                        .LcInsurance = CType(objread("LCInsurance"), Double)
                        .InsuranceCollFee = CType(objread("InsuranceCollFee"), Double)

                        .PDCBounceFee = CType(objread("PDCBounceFee"), Double)
                        .STNKRenewalFee = CType(objread("STNKFee"), Double)
                        .InsuranceClaimExpense = CType(objread("InsuranceClaimExpense"), Double)
                        .RepossessionFee = CType(objread("CollectionExpense"), Double)
                        .Prepaid = CType(objread("ContractPrepaidAmount"), Double)
                        .MaximumInstallment = CType(objread("MaximumInstallmentAmount"), Double)
                        .OutStandingInterest = CType(objread("OutStandingInterestUnDue"), Double)
                        .OutstandingPrincipal = CType(objread("OutStandingPrincipalUnDue"), Double)
                        .ContractStatus = CType(objread("ContractStatus"), String)
                        .PrepaidHoldStatus = CType(objread("PrepaidHoldStatus"), String)
                        .FundingCoyName = CType(objread("FundingCoyName"), String)
                        .FundingPledgeStatus = CType(objread("FundingPledgeStatus"), String)
                        .ResiduValue = CType(objread("ResiduValue"), Double)

                        .PrioritasPembayaran = CType(objread("prioritasPembayaran"), String)
                        .SPType = CType(objread("SPType"), String)
                        .BlokirBayar = CType(objread("BlokirBayar"), Boolean)
                        .AmountToBePaid = CType(objread("InstallmentAmount"), Double)
                        .TitipanAngsuran = CType(objread("TitipanAngsuran"), Double)
                        .ToleransiBayarKurang = CType(objread("BatasToleransiBawah"), String)
                        .ToleransiBayarLebih = CType(objread("BatasToleransiAtas"), String)
                    End If
                    objread.Close()
                End With
            End If
            Return customclass
        Catch exp As Exception
            WriteException("AgreementList", "ReportListAgreement", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetPaymentInfoFactAndMDKJ(ByVal customclass As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(0).Value = customclass.ValueDate

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID.Trim

            params(2) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.InvoiceNo.Trim

            params(3) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(3).Value = customclass.InvoiceSeqNo

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spPaymentInfoFactAndMDKJ", params)

            If Not IsNothing(objread) Then
                With customclass
                    If objread.Read Then
                        .BranchAgreement = CType(objread("BranchFullName"), String)
                        .Agreementno = CType(objread("AgreementNo"), String)
                        .CustomerName = CType(objread("Name"), String)
                        .CustomerType = CType(objread("CustomerType"), String)
                        .CustomerID = CType(objread("CustomerID"), String)
                        .NextInstallmentDate = CType(objread("NextInstallmentDueDate"), Date)
                        .NextInstallmentNumber = CType(objread("NextInstallmentNumber"), Integer)
                        .installmentamount = CType(objread("InstallmentAmount"), Double)
                        .InstallmentDue = CType(objread("InstallmentDue"), Double)
                        .LcInstallment = CType(objread("LCInstallment"), Double)
                        .InstallmentCollFee = CType(objread("InstallCollFee"), Double)
                        .InsuranceDue = CType(objread("InsuranceDue"), Double)
                        .LcInsurance = CType(objread("LCInsurance"), Double)
                        .InsuranceCollFee = CType(objread("InsuranceCollFee"), Double)
                        .PDCBounceFee = CType(objread("PDCBounceFee"), Double)
                        .STNKRenewalFee = CType(objread("STNKFee"), Double)
                        .InsuranceClaimExpense = CType(objread("InsuranceClaimExpense"), Double)
                        .RepossessionFee = CType(objread("CollectionExpense"), Double)
                        .Prepaid = CType(objread("ContractPrepaidAmount"), Double)
                        .MaximumInstallment = CType(objread("MaximumInstallmentAmount"), Double)
                        .OutStandingInterest = CType(objread("OutStandingInterestUnDue"), Double)
                        .OutstandingPrincipal = CType(objread("OutStandingPrincipalUnDue"), Double)
                        .ContractStatus = CType(objread("ContractStatus"), String)
                        .PrepaidHoldStatus = CType(objread("PrepaidHoldStatus"), String)
                        .PrepaidHoldStatusDesc = CType(objread("PrepaidHoldStatusDesc"), String)
                        .FundingCoyName = CType(objread("FundingCoyName"), String)
                        .FundingPledgeStatus = CType(objread("FundingPledgeStatus"), String)
                        .ResiduValue = CType(objread("ResiduValue"), Double)
                        .PrioritasPembayaran = CType(objread("prioritasPembayaran"), String)
                        .SPType = CType(objread("SPType"), String)
                        .BlokirBayar = CType(objread("BlokirBayar"), Boolean)
                        .AmountToBePaid = CType(objread("InstallmentAmount"), Double)
                        .TitipanAngsuran = CType(objread("TitipanAngsuran"), Double)
                        .ToleransiBayarKurang = CType(objread("BatasToleransiBawah"), String)
                        .ToleransiBayarLebih = CType(objread("BatasToleransiAtas"), String)
                        .isAkhirBayar = CType(objread("isAkhirBayar"), Boolean)
                    End If
                    objread.Close()
                End With
            End If
            Return customclass
        Catch exp As Exception
            WriteException("GetPaymentInfoFactAndMDKJ", "GetPaymentInfoFactAndMDKJ", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "Full Prepay Info"

    Public Function GetFullPrePaymentInfoNormal(ByVal customclass As Parameter.FullPrepay) As Parameter.FullPrepay
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(0).Value = customclass.ValueDate

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID.Trim

            params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(2).Value = customclass.BranchId.Trim

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, PrePaymentInfoNormal, params)

            With customclass
                If objread.Read Then
                    .Angsuran = CType(objread("angsuran"), Double)
                    .Denda = CType(objread("denda"), Double)
                    .BiayaSTNK = CType(objread("STNKFee"), Double)
                    .BiayaTarik = CType(objread("BiayaTarik"), Double)
                    .BiayaTolakanPDC = CType(objread("PDCBounceFee"), Double)
                    .BiayaTagihAngsuran = CType(objread("InsuranceCollFee"), Double)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("PaymentInfo", "GetFullPrePaymentInfoNormal", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetFullPrePaymentInfo(ByVal customclass As Parameter.FullPrepay) As Parameter.FullPrepay
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(0).Value = customclass.ValueDate

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID.Trim

            params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(2).Value = customclass.BranchId.Trim

            params(3) = New SqlParameter(PARAM_PREPAYMENTTYPE, SqlDbType.Char, 2)
            params(3).Value = customclass.PrepaymentType.Trim

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, PrePaymentInfo, params)

            With customclass
                If objread.Read Then
                    .BranchAgreement = CType(objread("BranchFullName"), String)
                    .Agreementno = CType(objread("AgreementNo"), String)
                    .CustomerName = CType(objread("Name"), String)
                    .CustomerType = CType(objread("CustomerType"), String)
                    .CustomerID = CType(objread("CustomerID"), String)

                    .NextInstallmentDate = CType(objread("NextInstallmentDueDate"), Date)
                    .InstallmentAmount = CType(objread("InstallmentAmount"), Double)
                    .NextInstallmentNumber = CType(objread("NextInstallmentDueNumber"), Integer)

                    .InstallmentDue = CType(objread("InstallmentDue"), Double)
                    .LcInstallment = CType(objread("LCInstallment"), Double)
                    .InstallmentCollFee = CType(objread("InstallCollFee"), Double)

                    .InsuranceDue = CType(objread("InsuranceDue"), Double)
                    .LcInsurance = CType(objread("LCInsurance"), Double)
                    .InsuranceCollFee = CType(objread("InsuranceCollFee"), Double)

                    .PDCBounceFee = CType(objread("PDCBounceFee"), Double)
                    .STNKRenewalFee = CType(objread("STNKFee"), Double)
                    .InsuranceClaimExpense = CType(objread("InsuranceClaimExpense"), Double)
                    .RepossessionFee = CType(objread("CollectionExpense"), Double)
                    .Prepaid = CType(objread("ContractPrepaidAmount"), Double)
                    .MaximumInstallment = CType(objread("MaximumInstallmentAmount"), Double)
                    .OutStandingInterest = CType(objread("OutStandingInterest"), Double)
                    .OutstandingPrincipal = CType(objread("OutStandingPrincipal"), Double)

                    .PrepaidHoldStatus = CType(objread("PrepaidHoldStatus"), String)
                    .AccruedInterest = CType(objread("AccruedAmount"), Double)
                    .UCI = CType(objread("UCI"), Double)
                    .ECI = CType(objread("ECI"), Double)
                    .TerminationPenalty = CType(objread("TerminationPenalty"), Double)
                    .InsurancePaidBy = CType(objread("InsurancePaidBy"), String)
                    .DefaultStatus = CType(objread("Defaultstatus"), String)
                    .ContractStatus = CType(objread("ContracStatus"), String)

                    .FundingCoyName = CType(objread("FundingCoyName"), String)
                    .FundingPledgeStatus = CType(objread("FundingPledgeStatus"), String)
                    .ResiduValue = CType(objread("ResiduValue"), Double)
                    .CrossDefault = CType(objread("CrossDefault"), Boolean)
                    .FaktorPengurang = CType(objread("FaktorPengurang"), Double)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("PaymentInfo", "GetFullPrePaymentInfo", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetFullPrePaymentInfoOL(ByVal customclass As Parameter.FullPrepay) As Parameter.FullPrepay
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(0).Value = customclass.ValueDate

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID.Trim

            params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(2).Value = customclass.BranchId.Trim

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, PrePaymentInfoOL, params)

            With customclass
                If objread.Read Then
                    .BranchAgreement = CType(objread("BranchFullName"), String)
                    .Agreementno = CType(objread("AgreementNo"), String)
                    .CustomerName = CType(objread("Name"), String)
                    .CustomerType = CType(objread("CustomerType"), String)
                    .CustomerID = CType(objread("CustomerID"), String)

                    .NextInstallmentDate = CType(objread("NextInstallmentDueDate"), Date)
                    .InstallmentAmount = CType(objread("InstallmentAmount"), Double)
                    .NextInstallmentNumber = CType(objread("NextInstallmentDueNumber"), Integer)

                    .InstallmentDue = CType(objread("InstallmentDue"), Double)
                    .LcInstallment = CType(objread("LCInstallment"), Double)
                    .InstallmentCollFee = CType(objread("InstallCollFee"), Double)

                    .InsuranceDue = CType(objread("InsuranceDue"), Double)
                    .LcInsurance = CType(objread("LCInsurance"), Double)
                    .InsuranceCollFee = CType(objread("InsuranceCollFee"), Double)

                    .PDCBounceFee = CType(objread("PDCBounceFee"), Double)
                    .STNKRenewalFee = CType(objread("STNKFee"), Double)
                    .InsuranceClaimExpense = CType(objread("InsuranceClaimExpense"), Double)
                    .RepossessionFee = CType(objread("CollectionExpense"), Double)
                    .Prepaid = CType(objread("ContractPrepaidAmount"), Double)
                    .MaximumInstallment = CType(objread("MaximumInstallmentAmount"), Double)
                    .OutStandingInterest = CType(objread("OutStandingInterest"), Double)
                    .OutstandingPrincipal = CType(objread("OutStandingPrincipal"), Double)

                    .PrepaidHoldStatus = CType(objread("PrepaidHoldStatus"), String)
                    .AccruedInterest = CType(objread("AccruedAmount"), Double)
                    .UCI = CType(objread("UCI"), Double)
                    .ECI = CType(objread("ECI"), Double)
                    .TerminationPenalty = CType(objread("TerminationPenalty"), Double)
                    .InsurancePaidBy = CType(objread("InsurancePaidBy"), String)
                    .DefaultStatus = CType(objread("Defaultstatus"), String)
                    .ContractStatus = CType(objread("ContracStatus"), String)

                    .FundingCoyName = CType(objread("FundingCoyName"), String)
                    .FundingPledgeStatus = CType(objread("FundingPledgeStatus"), String)
                    .ResiduValue = CType(objread("ResiduValue"), Double)
                    .CrossDefault = CType(objread("CrossDefault"), Boolean)
                    .FaktorPengurang = CType(objread("FaktorPengurang"), Double)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("PaymentInfo", "GetFullPrePaymentInfo", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetFullPrepaymentInfoFactAndMDKJ(ByVal customclass As Parameter.FullPrepay) As Parameter.FullPrepay
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(0).Value = customclass.ValueDate

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID.Trim

            params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(2).Value = customclass.BranchId.Trim

            params(3) = New SqlParameter(PARAM_PREPAYMENTTYPE, SqlDbType.Char, 2)
            params(3).Value = customclass.PrepaymentType.Trim

            params(4) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
            params(4).Value = customclass.InvoiceNo.Trim

            params(5) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(5).Value = customclass.InvoiceSeqNo

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spPrePaymentInfoFactAndMDKJ", params)

            With customclass
                If objread.Read Then
                    .BranchAgreement = CType(objread("BranchFullName"), String)
                    .Agreementno = CType(objread("AgreementNo"), String)
                    .CustomerName = CType(objread("Name"), String)
                    .CustomerType = CType(objread("CustomerType"), String)
                    .CustomerID = CType(objread("CustomerID"), String)

                    .NextInstallmentDate = CType(objread("NextInstallmentDueDate"), Date)
                    .installmentamount = CType(objread("InstallmentAmount"), Double)
                    .NextInstallmentNumber = CType(objread("NextInstallmentDueNumber"), Integer)

                    .InstallmentDue = CType(objread("InstallmentDue"), Double)
                    .LcInstallment = CType(objread("LCInstallment"), Double)
                    .InstallmentCollFee = CType(objread("InstallCollFee"), Double)

                    .InsuranceDue = CType(objread("InsuranceDue"), Double)
                    .LcInsurance = CType(objread("LCInsurance"), Double)
                    .InsuranceCollFee = CType(objread("InsuranceCollFee"), Double)

                    .PDCBounceFee = CType(objread("PDCBounceFee"), Double)
                    .STNKRenewalFee = CType(objread("STNKFee"), Double)
                    .InsuranceClaimExpense = CType(objread("InsuranceClaimExpense"), Double)
                    .RepossessionFee = CType(objread("CollectionExpense"), Double)
                    .Prepaid = CType(objread("ContractPrepaidAmount"), Double)
                    .MaximumInstallment = CType(objread("MaximumInstallmentAmount"), Double)
                    .OutStandingInterest = CType(objread("OutStandingInterest"), Double)
                    .OutstandingPrincipal = CType(objread("OutStandingPrincipal"), Double)

                    .PrepaidHoldStatus = CType(objread("PrepaidHoldStatus"), String)
                    .AccruedInterest = CType(objread("AccruedAmount"), Double)
                    .UCI = CType(objread("UCI"), Double)
                    .ECI = CType(objread("ECI"), Double)
                    .TerminationPenalty = CType(objread("TerminationPenalty"), Double)
                    .InsurancePaidBy = CType(objread("InsurancePaidBy"), String)
                    .DefaultStatus = CType(objread("Defaultstatus"), String)
                    .ContractStatus = CType(objread("ContracStatus"), String)

                    .FundingCoyName = CType(objread("FundingCoyName"), String)
                    .FundingPledgeStatus = CType(objread("FundingPledgeStatus"), String)
                    .ResiduValue = CType(objread("ResiduValue"), Double)
                    .CrossDefault = CType(objread("CrossDefault"), Boolean)
                    .FaktorPengurang = CType(objread("FaktorPengurang"), Double)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("PaymentInfo", "GetFullPrepaymentInfoFactAndMDKJ", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetFullPrePaymentInfoView(ByVal customclass As Parameter.FullPrepay) As Parameter.FullPrepay
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            'params(0) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            'params(0).Value = customclass.ValueDate
            params(0) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 30)
            params(0).Value = customclass.RequestNo.Trim

            'params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            'params(1).Value = customclass.ApplicationID.Trim

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId.Trim

            params(2) = New SqlParameter(PARAM_PREPAYMENTTYPE, SqlDbType.Char, 2)
            params(2).Value = customclass.PrepaymentType.Trim

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spPrePaymentInfoView", params)

            With customclass
                If objread.Read Then
                    .BranchAgreement = CType(objread("BranchFullName"), String)
                    .Agreementno = CType(objread("AgreementNo"), String)
                    .CustomerName = CType(objread("Name"), String)
                    .CustomerType = CType(objread("CustomerType"), String)
                    .CustomerID = CType(objread("CustomerID"), String)
                    .ApplicationID = CType(objread("ApplicationID"), String)
                    .NextInstallmentDate = CType(objread("NextInstallmentDueDate"), Date)
                    .installmentamount = CType(objread("InstallmentAmount"), Double)
                    .NextInstallmentNumber = CType(objread("NextInstallmentDueNumber"), Integer)
                    .ApplicationModule = CType(objread("ApplicationModule"), String)
                    .InvoiceNo = CType(objread("InvoiceNo"), String)
                    .InvoiceSeqNo = CType(objread("InvoiceSeqNo"), String)

                    .InstallmentDue = CType(objread("InstallmentDue"), Double)
                    .LcInstallment = CType(objread("LCInstallment"), Double)
                    .InstallmentCollFee = CType(objread("InstallCollFee"), Double)

                    .InsuranceDue = CType(objread("InsuranceDue"), Double)
                    .LcInsurance = CType(objread("LCInsurance"), Double)
                    .InsuranceCollFee = CType(objread("InsuranceCollFee"), Double)

                    .PDCBounceFee = CType(objread("PDCBounceFee"), Double)
                    .STNKRenewalFee = CType(objread("STNKFee"), Double)
                    .InsuranceClaimExpense = CType(objread("InsuranceClaimExpense"), Double)
                    .RepossessionFee = CType(objread("CollectionExpense"), Double)
                    .Prepaid = CType(objread("ContractPrepaidAmount"), Double)
                    .MaximumInstallment = CType(objread("MaximumInstallmentAmount"), Double)
                    .OutStandingInterest = CType(objread("OutStandingInterest"), Double)
                    .OutstandingPrincipal = CType(objread("OutStandingPrincipal"), Double)

                    .PrepaidHoldStatus = CType(objread("PrepaidHoldStatus"), String)
                    .AccruedInterest = CType(objread("AccruedAmount"), Double)
                    .UCI = CType(objread("UCI"), Double)
                    .ECI = CType(objread("ECI"), Double)
                    .TerminationPenalty = CType(objread("TerminationPenalty"), Double)
                    .InsurancePaidBy = CType(objread("InsurancePaidBy"), String)
                    .DefaultStatus = CType(objread("Defaultstatus"), String)
                    .ContractStatus = CType(objread("ContracStatus"), String)

                    .FundingCoyName = CType(objread("FundingCoyName"), String)
                    .FundingPledgeStatus = CType(objread("FundingPledgeStatus"), String)
                    .ResiduValue = CType(objread("ResiduValue"), Double)
                    .CrossDefault = CType(objread("CrossDefault"), Boolean)
                    .FaktorPengurang = CType(objread("FaktorPengurang"), Double)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("PaymentInfo", "GetFullPrePaymentInfo", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "IsHaveOtor"

    Public Function GetIsPaymentHaveOtor(ByVal customclass As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId.Trim

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID.Trim

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, IsPaymentHaveOtor, params)

            If Not IsNothing(objread) Then
                With customclass
                    If objread.Read Then
                        .IsHaveOtor = CType(objread("IsHaveOtor"), String)
                    End If
                    objread.Close()
                End With
            End If

            Return customclass
        Catch exp As Exception       
            WriteException("AgreementList", "ReportListAgreement", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

End Class
