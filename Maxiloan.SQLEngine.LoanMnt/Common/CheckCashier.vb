
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class CheckCashier
    Inherits AccMntBase

    Private Const spCheckCashier As String = "spCheckCashier"
    Private Const PARAM_ISOPEN As String = "@isOpen"
    Public Function CheckCashier(ByVal strConnection As String, ByVal LoginID As String, _
                    ByVal BranchID As String, ByVal BusinessDate As Date) As Boolean
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(0).Value = LoginID.Trim
            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = BranchID.Trim.Replace("'", "")
            params(2) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(2).Value = BusinessDate
            params(3) = New SqlParameter(PARAM_ISOPEN, SqlDbType.Bit)
            params(3).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(strConnection, CommandType.StoredProcedure, spCheckCashier, params)
        Return CType(params(3).Value, Boolean)
        Catch exp As Exception
            WriteException("CheckCashier", "CheckCashier", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
