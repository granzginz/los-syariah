
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class ADASetting
    Inherits AccMntBase

    Public Function UpdateADASetting(ByVal oCustomClass As Parameter.ADASetting) As Boolean
        Dim params() As SqlParameter = New SqlParameter(32) {}
        params(0) = New SqlParameter("@Bucket1From", SqlDbType.Int)
        params(0).Value = oCustomClass.Bucket1From
        params(1) = New SqlParameter("@Bucket2From", SqlDbType.Int)
        params(1).Value = oCustomClass.Bucket2From
        params(2) = New SqlParameter("@Bucket3From", SqlDbType.Int)
        params(2).Value = oCustomClass.Bucket3From
        params(3) = New SqlParameter("@Bucket4From", SqlDbType.Int)
        params(3).Value = oCustomClass.Bucket4From
        params(4) = New SqlParameter("@Bucket5From", SqlDbType.Int)
        params(4).Value = oCustomClass.Bucket5From
        params(5) = New SqlParameter("@Bucket6From", SqlDbType.Int)
        params(5).Value = oCustomClass.Bucket6From
        params(6) = New SqlParameter("@Bucket1To", SqlDbType.Int)
        params(6).Value = oCustomClass.Bucket1To

        params(7) = New SqlParameter("@Bucket2To", SqlDbType.Int)
        params(7).Value = oCustomClass.Bucket2To
        params(8) = New SqlParameter("@Bucket3To", SqlDbType.Int)
        params(8).Value = oCustomClass.Bucket3To
        params(9) = New SqlParameter("@Bucket4To", SqlDbType.Int)
        params(9).Value = oCustomClass.Bucket4To
        params(10) = New SqlParameter("@Bucket5To", SqlDbType.Int)
        params(10).Value = oCustomClass.Bucket5To
        params(11) = New SqlParameter("@Bucket6To", SqlDbType.Int)
        params(11).Value = oCustomClass.Bucket6To
        params(12) = New SqlParameter("@Bucket1Rate", SqlDbType.Decimal)
        params(12).Value = oCustomClass.Bucket1Rate
        params(13) = New SqlParameter("@Bucket2Rate", SqlDbType.Decimal)
        params(13).Value = oCustomClass.Bucket2Rate
        params(14) = New SqlParameter("@Bucket3Rate", SqlDbType.Decimal)
        params(14).Value = oCustomClass.Bucket3Rate
        params(15) = New SqlParameter("@Bucket4Rate", SqlDbType.Decimal)
        params(15).Value = oCustomClass.Bucket4Rate
        params(16) = New SqlParameter("@Bucket5Rate", SqlDbType.Decimal)
        params(16).Value = oCustomClass.Bucket5Rate
        params(17) = New SqlParameter("@Bucket6Rate", SqlDbType.Decimal)
        params(17).Value = oCustomClass.Bucket6Rate
        params(18) = New SqlParameter("@ReturnValue", SqlDbType.Bit)
        params(18).Direction = ParameterDirection.Output


        params(19) = New SqlParameter("@Bucket7From", SqlDbType.Int) With {.Value = oCustomClass.Bucket7From}
        params(20) = New SqlParameter("@Bucket7To", SqlDbType.Int) With {.Value = oCustomClass.Bucket7To}
        params(21) = New SqlParameter("@Bucket7Rate", SqlDbType.Decimal) With {.Value = oCustomClass.Bucket7Rate}

        params(22) = New SqlParameter("@Bucket8From", SqlDbType.Int) With {.Value = oCustomClass.Bucket8From}
        params(23) = New SqlParameter("@Bucket8To", SqlDbType.Int) With {.Value = oCustomClass.Bucket8To}
        params(24) = New SqlParameter("@Bucket8Rate", SqlDbType.Decimal) With {.Value = oCustomClass.Bucket8Rate}

        params(25) = New SqlParameter("@Bucket9From", SqlDbType.Int) With {.Value = oCustomClass.Bucket9From}
        params(26) = New SqlParameter("@Bucket9To", SqlDbType.Int) With {.Value = oCustomClass.Bucket9To}
        params(27) = New SqlParameter("@Bucket9Rate", SqlDbType.Decimal) With {.Value = oCustomClass.Bucket9Rate}

        params(28) = New SqlParameter("@Bucket10From", SqlDbType.Int) With {.Value = oCustomClass.Bucket10From}
        params(29) = New SqlParameter("@Bucket10To", SqlDbType.Int) With {.Value = oCustomClass.Bucket10To}
        params(30) = New SqlParameter("@Bucket10Rate", SqlDbType.Decimal) With {.Value = oCustomClass.Bucket10Rate}

        params(31) = New SqlParameter("@AydaRate", SqlDbType.Decimal) With {.Value = oCustomClass.Ayda1Rate}
        params(32) = New SqlParameter("@Ayda1Rate", SqlDbType.Decimal) With {.Value = oCustomClass.Ayda2Rate}
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, oCustomClass.SPName, params)
            Return CType(params(2).Value, Boolean)
        Catch exp As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("UpdateADASetting.vb", "UpdateADASetting", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
        End Try

    End Function

End Class
