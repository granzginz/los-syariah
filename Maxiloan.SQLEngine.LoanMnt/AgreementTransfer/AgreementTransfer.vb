
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions 
#End Region
Public Class AgreementTransfer
    Inherits AccMntBase

#Region "Constanta"

#Region "Constanta Request"
    Private Const spDocList As String = "spDocRecList"
    Private Const spCList As String = "spCustList"
    Private Const spTCReq As String = "spAgreementTransferReqTC"
    Private Const spTCReq2 As String = "spAgreementTransferReqTC2"
    Private Const spATRequest As String = "spAgreementTransferRequest"
#End Region

#Region "Constanta Execute"
    Private Const spListExecAT As String = "spAgreementTransferList"
    Private Const spATTC As String = "spAgreementTransferTC"
    Private Const spATTC2 As String = "spAgreementTransferTCCheckList"
    Private Const spSaveATCancel As String = "spAgreementTransferCancel"
    Private Const spSaveATExec As String = "spAgreementTransferExec"
#End Region

#End Region

#Region "Request"

#Region "AssetDocPaging"

    Public Function AssetDocPaging(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Try
            Dim params() As SqlParameter = New SqlParameter(1) {}

            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spDocList, params).Tables(0)
            'customclass.TotalRecord = CInt(params(4).Value)

            Return customclass
        Catch exp As Exception
            WriteException("AgreementTransfer", "AssetDocPaging", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "GetCust"
    Public Function GetCust(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@Customerid", SqlDbType.VarChar, 20)
            params(0).Value = customclass.CustomerID.Trim

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spCList, params)
            With customclass
                If objread.Read Then
                    .CustomerID = CType(objread("Customerid"), String)
                    .CustomerType = CType(objread("Customertype"), String)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("AgreementTransfer", "GetCust", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "SaveATRequest"
    'Public Sub SaveATRequest(ByVal customClass As Parameter.DChange, _
    '                         ByVal oGuarantorPersonal As Parameter.Personal, _
    '                         ByVal oGuarantorAddress As Parameter.AgreementGuarantor)

    Public Sub SaveATRequest(ByVal customClass As Parameter.DChange)

        Dim oConnection As New SqlConnection(customClass.strConnection)
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim objTransaction As SqlTransaction = Nothing
        Dim strApprovalNo As String
        Dim intLoopOmSet, intLoopOmSet2 As Integer
        Dim params() As SqlParameter = New SqlParameter(25) {}
        Dim paramsD() As SqlParameter = New SqlParameter(8) {}
        Dim paramsD2() As SqlParameter = New SqlParameter(8) {}
        Dim parGuarantor() As SqlParameter = New SqlParameter(20) {}
        Try

            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            objTransaction = oConnection.BeginTransaction
            With oEntitiesApproval
                .ApprovalTransaction = objTransaction
                .BranchId = customClass.BranchId
                .SchemeID = "AGTF"
                .RequestDate = customClass.BusinessDate
                .TransactionNo = customClass.ApplicationID
                .ApprovalValue = customClass.TotAmountToBePaid
                .UserRequest = customClass.LoginId
                .UserApproval = customClass.RequestTo
                .ApprovalNote = customClass.ChangeNotes
                .AprovalType = Parameter.Approval.ETransactionType.Agreement_Transfer
            End With
            strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)
            '@BranchID char(3),
            '@ApplicationId varchar(20),
            '@OldCustomerID	varchar(20),
            '@CustomerID  varchar(20),
            '@GuarantorID varchar(20),
            '@Requestdate datetime,
            '@EffectiveDate datetime,
            '@AdministrationFee dec(17,2),
            '@ContractPrepaidAmount dec(17,2),


            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = customClass.ApplicationID

            params(2) = New SqlParameter("@OldCustomerID", SqlDbType.VarChar, 20)
            params(2).Value = customClass.OldCustId.Trim

            params(3) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
            params(3).Value = customClass.CustomerID.Trim

            'params(4) = New SqlParameter("@GuarantorID", SqlDbType.VarChar, 20)
            'params(4).Value = customClass.GuarantorID.Trim

            params(4) = New SqlParameter("@Requestdate", SqlDbType.DateTime)
            params(4).Value = customClass.BusinessDate

            params(5) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
            params(5).Value = customClass.BusinessDate

            params(6) = New SqlParameter("@AdministrationFee", SqlDbType.Decimal)
            params(6).Value = customClass.AdminFee

            params(7) = New SqlParameter("@ContractPrepaidAmount", SqlDbType.Decimal)
            params(7).Value = customClass.Prepaid

            '@OutstandingPrincipal dec(17,2),
            '@OutstandingInterest dec(17,2),
            '@OSInstallmentDue  dec (17,2),
            '@OSInsuranceDue dec(17,2),
            '@OSLCInstallment dec(17,2),
            '@OSLCInsurance dec(17,2),
            '@OSInstallCollectionFee dec(17,2),
            '@OSInsuranceCollectionFee dec(17,2),


            params(8) = New SqlParameter("@OutstandingPrincipal", SqlDbType.Decimal)
            params(8).Value = customClass.OutstandingPrincipal

            params(9) = New SqlParameter("@OutstandingInterest", SqlDbType.Decimal)
            params(9).Value = customClass.OutStandingInterest

            params(10) = New SqlParameter("@OSInstallmentDue", SqlDbType.Decimal)
            params(10).Value = customClass.InstallmentDue

            params(11) = New SqlParameter("@OSInsuranceDue", SqlDbType.Decimal)
            params(11).Value = customClass.InsuranceDue

            params(12) = New SqlParameter("@OSLCInstallment", SqlDbType.Decimal)
            params(12).Value = customClass.LcInstallment

            params(13) = New SqlParameter("@OSLCInsurance", SqlDbType.Decimal)
            params(13).Value = customClass.LcInsurance

            params(14) = New SqlParameter("@OSInstallCollectionFee", SqlDbType.Decimal)
            params(14).Value = customClass.InstallmentCollFee

            params(15) = New SqlParameter("@OSInsuranceCollectionFee", SqlDbType.Decimal)
            params(15).Value = customClass.InsuranceCollFee

            '@OSPDCBounceFee dec(17,2),
            '@STNKFee dec(17,2),
            '@InsuranceClaim  dec(17,2),
            '@ReposessFee dec(17,2),
            '@ApprovalNo varchar(50),
            '@ReasonTypeID varchar(5),
            '@ReasonID varchar(10),
            '@Notes varchar(50),
            '@strerror varchar(100) output

            params(16) = New SqlParameter("@OSPDCBounceFee", SqlDbType.Decimal)
            params(16).Value = customClass.PDCBounceFee

            params(17) = New SqlParameter("@STNKFee", SqlDbType.Decimal)
            params(17).Value = customClass.STNKRenewalFee

            params(18) = New SqlParameter("@InsuranceClaim", SqlDbType.Decimal)
            params(18).Value = customClass.InsuranceClaimExpense

            params(19) = New SqlParameter("@ReposessFee", SqlDbType.Decimal)
            params(19).Value = customClass.RepossessionFee

            params(20) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
            params(20).Value = strApprovalNo

            params(21) = New SqlParameter("@ReasonTypeID", SqlDbType.VarChar, 10)
            params(21).Value = customClass.ReasonTypeID

            params(22) = New SqlParameter("@ReasonID", SqlDbType.VarChar, 10)
            params(22).Value = customClass.ReasonID

            params(23) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 100)
            params(23).Value = customClass.ChangeNotes

            params(24) = New SqlParameter("@SeqNo", SqlDbType.Int)
            params(24).Direction = ParameterDirection.Output

            params(25) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(25).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spATRequest, params)



            'parGuarantor(0) = New SqlParameter("@mode", SqlDbType.Bit)
            'parGuarantor(0).Value = 0
            parGuarantor(0) = New SqlParameter("@mode", SqlDbType.Bit)
            parGuarantor(0).Value = 1
            parGuarantor(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            parGuarantor(1).Value = customClass.ApplicationID
            parGuarantor(2) = New SqlParameter("@GuarantorName", SqlDbType.VarChar, 50)
            parGuarantor(2).Value = customClass.GuarantorName
            parGuarantor(3) = New SqlParameter("@GuarantorJobTitle", SqlDbType.VarChar, 50)
            parGuarantor(3).Value = customClass.GuarantorJobTitle
            parGuarantor(4) = New SqlParameter("@GuarantorAddress", SqlDbType.VarChar, 50)
            parGuarantor(4).Value = customClass.GuarantorAddress
            parGuarantor(5) = New SqlParameter("@GuarantorRT", SqlDbType.Char, 3)
            parGuarantor(5).Value = customClass.GuarantorRT
            parGuarantor(6) = New SqlParameter("@GuarantorRW", SqlDbType.Char, 3)
            parGuarantor(6).Value = customClass.GuarantorRW
            parGuarantor(7) = New SqlParameter("@GuarantorKelurahan", SqlDbType.VarChar, 30)
            parGuarantor(7).Value = customClass.GuarantorKelurahan
            parGuarantor(8) = New SqlParameter("@GuarantorKecamatan", SqlDbType.VarChar, 30)
            parGuarantor(8).Value = customClass.GuarantorKecamatan
            parGuarantor(9) = New SqlParameter("@GuarantorCity", SqlDbType.VarChar, 30)
            parGuarantor(9).Value = customClass.GuarantorCity
            parGuarantor(10) = New SqlParameter("@GuarantorZipCode", SqlDbType.Char, 5)
            parGuarantor(10).Value = customClass.GuarantorZipCode
            parGuarantor(11) = New SqlParameter("@GuarantorAreaPhone1", SqlDbType.Char, 4)
            parGuarantor(11).Value = customClass.GuarantorAreaPhone1
            parGuarantor(12) = New SqlParameter("@GuarantorPhone1", SqlDbType.VarChar, 10)
            parGuarantor(12).Value = customClass.GuarantorPhone1
            parGuarantor(13) = New SqlParameter("@GuarantorAreaPhone2", SqlDbType.Char, 4)
            parGuarantor(13).Value = customClass.GuarantorAreaPhone2
            parGuarantor(14) = New SqlParameter("@GuarantorPhone2", SqlDbType.Char, 10)
            parGuarantor(14).Value = customClass.GuarantorPhone2
            parGuarantor(15) = New SqlParameter("@GuarantorAreaFax", SqlDbType.Char, 4)
            parGuarantor(15).Value = customClass.GuarantorAreaFax
            parGuarantor(16) = New SqlParameter("@GuarantorFax", SqlDbType.VarChar, 10)
            parGuarantor(16).Value = customClass.GuarantorFax
            parGuarantor(17) = New SqlParameter("@GuarantorMobilePhone", SqlDbType.VarChar, 20)
            parGuarantor(17).Value = customClass.GuarantorMobilePhone
            parGuarantor(18) = New SqlParameter("@GuarantorEmail", SqlDbType.VarChar, 30)
            parGuarantor(18).Value = customClass.GuarantorEmail
            parGuarantor(19) = New SqlParameter("@GuarantorNotes", SqlDbType.Text)
            parGuarantor(19).Value = customClass.GuarantorNotes
            parGuarantor(20) = New SqlParameter("@GuarantorPenghasilan", SqlDbType.Money)
            parGuarantor(20).Value = customClass.GuarantorPenghasilan

            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "spApplicationSaveAddGuarantor", parGuarantor)



            '======================Data1================================================
            '@BranchId  varchar(3),
            '@ApplicationID varchar(20),
            '@MasterTCID varchar(20),
            '@IsChecked bit,
            '@IsMandatory bit,
            '@CheckedDate datetime,
            '@Notes varchar(50),
            '@strerror  varchar(100) output
            paramsD(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            paramsD(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            paramsD(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
            paramsD(3) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
            paramsD(4) = New SqlParameter("@IsMandatory", SqlDbType.Char, 1)
            paramsD(5) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
            paramsD(6) = New SqlParameter("@Notes", SqlDbType.VarChar, 50)
            paramsD(7) = New SqlParameter("@SeqNo", SqlDbType.Int)
            paramsD(8) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)

            If customClass.Data1.Rows.Count > 0 Then
                For intLoopOmSet = 0 To customClass.Data1.Rows.Count - 1
                    paramsD(0).Value = customClass.BranchId
                    paramsD(1).Value = customClass.ApplicationID
                    paramsD(2).Value = customClass.Data1.Rows(intLoopOmSet).Item("MasterTCID")
                    paramsD(3).Value = customClass.Data1.Rows(intLoopOmSet).Item("IsChecked")
                    paramsD(4).Value = customClass.Data1.Rows(intLoopOmSet).Item("IsMandatory")
                    paramsD(5).Value = customClass.BusinessDate
                    'paramsD(5).Value = customClass.Data1.Rows(intLoopOmSet).Item("CheckedDate")
                    paramsD(6).Value = customClass.Data1.Rows(intLoopOmSet).Item("Notes")
                    paramsD(7).Value = CInt(params(24).Value)
                    'paramsD(7).Value = customClass.Data1.Rows(intLoopOmSet).Item("SeqNo")
                    'paramsD(8).Value = params(25).Value
                    paramsD(8).Direction = ParameterDirection.Output
                    SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spTCReq, paramsD)
                Next
            End If


            '===========================================================================

            '====================================Data2=======================================

            '@BranchId  varchar(3),
            '@ApplicationID varchar(20),
            '@MasterTCID varchar(20),
            '@MasterTCLSequenceNo int,
            '@IsChecked bit,
            '@CheckedDate datetime,
            '@Notes varchar(50),
            '@strerror  varchar(100) output

            paramsD2(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            paramsD2(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            paramsD2(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
            paramsD2(3) = New SqlParameter("@MasterTCCLSequenceNo", SqlDbType.Int)
            paramsD2(4) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
            paramsD2(5) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
            paramsD2(6) = New SqlParameter("@Notes", SqlDbType.VarChar, 50)
            paramsD2(7) = New SqlParameter("@SeqNo", SqlDbType.Int)
            paramsD2(8) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)

            If customClass.Data2.Rows.Count > 0 Then
                For intLoopOmSet2 = 0 To customClass.Data2.Rows.Count - 1
                    paramsD2(0).Value = customClass.BranchId
                    paramsD2(1).Value = customClass.ApplicationID
                    paramsD2(2).Value = customClass.Data2.Rows(intLoopOmSet2).Item("MasterTCID")
                    paramsD2(3).Value = CInt(customClass.Data2.Rows(intLoopOmSet2).Item("AGTCCLSequenceNo"))
                    ' paramsD2(3).Value = customClass.Data2.Rows(intLoopOmSet2).Item("MasterTCLSequenceNo")
                    paramsD2(4).Value = customClass.Data2.Rows(intLoopOmSet2).Item("IsChecked")
                    'paramsD2(5).Value = customClass.Data2.Rows(intLoopOmSet2).Item("CheckedDate")
                    paramsD2(5).Value = customClass.BusinessDate

                    paramsD2(6).Value = customClass.Data2.Rows(intLoopOmSet2).Item("Notes")
                    'paramsD2(7).Value = customClass.Data2.Rows(intLoopOmSet2).Item("SeqNo")
                    paramsD2(7).Value = params(24).Value
                    paramsD2(8).Direction = ParameterDirection.Output

                    SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spTCReq2, paramsD2)
                Next
            End If

            '====================================Data2=======================================
            objTransaction.Commit()
        Catch exp As Exception
            WriteException("AgreementTransfer", "SaveAtRequest", exp.Message + exp.StackTrace)
            objTransaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Sub

#End Region

#End Region

#Region "Execute"
#Region "GetListAT"
    Public Function GetListAT(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            If Not customclass.ApplicationID Is Nothing Then
                params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
                params(0).Value = customclass.ApplicationID.Trim
            Else
                params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
                params(0).Value = ""
            End If
            If Not customclass.BranchId Is Nothing Then
                params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
                params(1).Value = customclass.BranchId
            Else
                params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
                params(1).Value = ""
            End If
            params(2) = New SqlParameter("@Seqno", SqlDbType.Int)
            params(2).Value = CInt(customclass.SeqNo)

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spListExecAT, params)
            With customclass
                If objread.Read Then
                    .BranchId = CType(objread("Branchid"), String)
                    .ApplicationID = CType(objread("ApplicationID"), String)
                    .Agreementno = CType(objread("Agreementno"), String)
                    .EffectiveDate = CType(objread("EffectiveDate"), Date)
                    .RequestDate = CType(objread("RequestDate"), Date)
                    .AdminFee = CType(objread("administrationFee"), Decimal)
                    .ReasonDescription = CType(objread("reasondesc"), String)
                    .ReasonID = CType(objread("ReasonID"), String)
                    .ReasonTypeID = CType(objread("ReasonTypeID"), String)
                    .ChangeNotes = CType(objread("notes"), String)
                    .ApprovedBy = CType(objread("userRequest"), String)
                    .Prepaid = CType(objread("contractprepaidAmount"), Decimal)
                    .PaymentFrequency = CType(objread("PaymentFrequency"), String)
                    .statusdesc = CType(objread("StatusDesc"), String)
                    .ATName = CType(objread("CustomerName"), String)
                    .ATTo = CType(objread("CustomerID"), String)
                    .NGuarantorID = CType(objread("GuarantorID"), String)
                    .NGuarantorName = CType(objread("guarantorname"), String)
                    .InstallmentDue = CType(objread("OSInstallmentDue"), Double)
                    .InsuranceDue = CType(objread("OSInsuranceDue"), Double)
                    .LcInstallment = CType(objread("OSLCInstallment"), Double)
                    .LcInsurance = CType(objread("OSLCInsurance"), Double)
                    .InstallmentCollFee = CType(objread("OSInstallCollectionFee"), Double)
                    .InsuranceCollFee = CType(objread("OSInsuranceCollectionFee"), Double)
                    .PDCBounceFee = CType(objread("OSPDCBounceFee"), Double)
                    .STNKRenewalFee = CType(objread("OSSTNKRenewalFee"), Double)
                    .InsuranceClaimExpense = CType(objread("OSInsuranceClaimExpense"), Double)
                    .RepossessionFee = CType(objread("OSRepossesFee"), Double)

                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("AgreementTransfer", "GetListAT", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "GetListTC"
    Public Function GetListTC(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID
            params(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
            params(2).Value = customclass.SeqNo
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spATTC, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementTransfer", "GetListTC", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "GetListTC2"
    Public Function GetListTC2(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
        params(1).Value = customclass.ApplicationID
        params(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
        params(2).Value = customclass.SeqNo
        Try
            customclass.Data1 = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spATTC2, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementTransfer", "GetListTC2", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "ATExec"
    Public Sub ATExec(ByVal customClass As Parameter.DChange)
        Dim oConnection As New SqlConnection(customClass.strConnection)
        Dim objTransaction As SqlTransaction
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        objTransaction = oConnection.BeginTransaction
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Dim intLoopOmset As Integer
        Dim i As Integer = 0

        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = customClass.BranchId

        params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
        params(1).Value = customClass.CoyID

        params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
        params(2).Value = customClass.ApplicationID

        params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
        params(3).Value = customClass.BusinessDate

        params(4) = New SqlParameter("@SeqNo", SqlDbType.Int)
        params(4).Value = customClass.SeqNo

        Try
            If customClass.flagDel = "1" Then
                SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spSaveATCancel, params)
            ElseIf customClass.flagDel = "2" Then
                SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spSaveATExec, params)
            End If
            objTransaction.Commit()
        Catch exp As Exception
            WriteException("AgreementTransfer", "ATExec", exp.Message + exp.StackTrace)
            objTransaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Sub
#End Region
#End Region

#Region "Endors AT Report"
    Public Function PrintEndorsAgreementTransfer(ByVal customClass As Parameter.DChange) As String
        Dim oConnection As New SqlConnection(customClass.strConnection)
        Dim objTransaction As SqlTransaction
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        objTransaction = oConnection.BeginTransaction
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Dim intLoopOmset As Integer
        Dim i As Integer = 0

        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = customClass.BranchId

        params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
        params(1).Value = customClass.ApplicationID

        params(2) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
        params(2).Value = customClass.BusinessDate

        params(3) = New SqlParameter("@EndorsmentLetter", SqlDbType.VarChar, 25)
        params(3).Direction = ParameterDirection.Output


        Try
            If customClass.ReferenceNo Is Nothing Then
                SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "spSaveAgreementTransferPrint", params)
            Else
                SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "spSaveChangeAddressPrint", params)
            End If

            objTransaction.Commit()
            Return CType(params(3).Value, String)
        Catch exp As Exception
            WriteException("AgreementTransfer", "ATExec", exp.Message + exp.StackTrace)
            objTransaction.Rollback()
            Throw New Exception(exp.Message)
            Return ""
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Function

#End Region
End Class
