

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class AccMntBase : Inherits DataAccessBase
    Protected Const PARAM_APPLICATIONID As String = "@applicationid"
    Protected Const PARAM_AGREEMENTNO As String = "@agreementno"
    Protected Const PARAM_BRANCHAGREEMENT As String = "@branchagreement"

    Protected Const PARAM_BANKID As String = "@bankid"
    Protected Const PARAM_ShortName As String = "@ShortName"
    Protected Const PARAM_BANKNAME As String = "@bankname"
    Protected Const PARAM_BANKACCOUNTID As String = "@bankaccountid"
    Protected Const PARAM_BANKACCOUNTNAME As String = "@bankaccountname"
    Protected Const PARAM_SandiKliring As String = "@SandiKliring"

    Protected Const PARAM_BANKBRANCH As String = "@bankbranch"
    Protected Const PARAM_ACCOUNTNAME As String = "@accountname"
    Protected Const PARAM_ACCOUNTNO As String = "@accountno"
    Protected Const PARAM_COA As String = "@coa"
    Protected Const PARAM_ENDINGBALANCE As String = "@endingbalance"
    Protected Const PARAM_BANKACCOUNTTYPE As String = "@bankaccounttype"
    Protected Const PARAM_BANKPURPOSE As String = "@bankpurpose"
    Protected Const PARAM_JOURNALCODE As String = "@JournalCode"

    Protected Const PARAM_CUSTOMERNAME As String = "@customername"

    Protected Const PARAM_CASHIERID As String = "@cashierid"

#Region "AGREEMENT RELATED"
    Protected Const PARAM_PREPAIDAMOUNT As String = "@prepaid"
    Protected Const PARAM_PREPAIDHOLDSTATUS As String = "@prepaidholdstatus"
#End Region
#Region "Installment Parameters"
    Protected Const PARAM_INSTALLMENTDUEPAID As String = "@installmentduepaid"
    Protected Const PARAM_INSTALLMENTDUEWAIVED As String = "@InstallmentAmountDisc"
    Protected Const PARAM_INSTALLMENTDUEDESC As String = "@installmentduedesc"

    Protected Const PARAM_LCINSTALLMENTPAID As String = "@lcinstallmentpaid"
    Protected Const PARAM_LCINSTALLMENTWAIVED As String = "@lcinstallmentAmountDisc"
    Protected Const PARAM_LCINSTALLMENTDESC As String = "@lcinstallmentdesc"

    Protected Const PARAM_INSTALLMENTCOLLPAID As String = "@installmentcollpaid"
    Protected Const PARAM_INSTALLMENTCOLLWAIVED As String = "@InstallCollectionFeeDisc"
    Protected Const PARAM_INSTALLMENTCOLLDESC As String = "@installmentcolldesc"
#End Region

#Region "Insurance Parameters"
    Protected Const PARAM_INSURANCEDUEPAID As String = "@insuranceduepaid"
    Protected Const PARAM_INSURANCEDUEWAIVED As String = "@InsuranceAmountDisc"
    Protected Const PARAM_INSURANCEDUEDESC As String = "@insuranceduedesc"

    Protected Const PARAM_LCINSURANCEPAID As String = "@Lcinsurancepaid"
    Protected Const PARAM_LCINSURANCEWAIVED As String = "@LCInsuranceAmountDisc"
    Protected Const PARAM_LCINSURANCEDESC As String = "@LCinsurancedesc"

    Protected Const PARAM_INSURANCECOLLPAID As String = "@insurancecollpaid"
    Protected Const PARAM_INSURANCECOLLWAIVED As String = "@InsurCollectionFeeDisc"
    Protected Const PARAM_INSURANCECOLLDESC As String = "@insurancecolldesc"
#End Region

#Region "Other Receive Parameters"
    Protected Const PARAM_PDCBOUNCEFEEPAID As String = "@pdcbouncefeepaid"
    Protected Const PARAM_PDCBOUNCEFEEWAIVED As String = "@PDCBounceFeeDisc"
    Protected Const PARAM_PDCBOUNCEFEEDESC As String = "@PDCBounceFeeDesc"

    Protected Const PARAM_INSURANCECLAIMEXPENSEPAID As String = "@insuranceclaimexpensepaid"
    Protected Const PARAM_INSURANCECLAIMEXPENSEWAIVED As String = "@InsuranceClaimExpenseDisc"
    Protected Const PARAM_INSURANCECLAIMEXPENSEPAIDDESC As String = "@insuranceclaimexpensepaiddesc"

    Protected Const PARAM_STNKRENEEWALFEEPAID As String = "@stnkrenewalpaid"
    Protected Const PARAM_STNKRENEEWALFEEWAIVED As String = "@stnkrenewalDisc"
    Protected Const PARAM_STNKRENEEWALFEEDESC As String = "@stnkrenewaldesc"

    Protected Const PARAM_REPOSSESSIONFEEPAID As String = "@repossessionfeepaid"
    Protected Const PARAM_REPOSSESSIONFEEWAIVED As String = "@RepossesFeeDisc"
    Protected Const PARAM_REPOSSESSIONFEEDESC As String = "@repossessionfeepaiddesc"

    Protected Const PARAM_PREPAIDPAID As String = "@prepaidpaid"
    Protected Const PARAM_PREPAIDDESC As String = "@prepaiddesc"
    Protected Const PARAM_TERMINATIONPENALTYWAIVED As String = "@PrepaymentFeeDisc"
#End Region

#Region "Parameters Receive Information"
    Protected Const PARAM_RECEIVEFROM As String = "@receivedfrom"
    Protected Const PARAM_REFERENCENO As String = "@referenceno"

    Protected Const PARAM_WOP As String = "@WOP"
    Protected Const PARAM_AMOUNTRECEIVE As String = "@amountreceive"
    Protected Const PARAM_NOTES As String = "@NOTES"
#End Region

End Class
