
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class DChange : Inherits AccMntBase

    Private Const spAmor As String = "spViewAmortization"
    Private Const spList As String = "spDProduct"
    Private Const spList1 As String = "spDProduct"
    Private Const spRequest As String = "spDueDateRequest"
    Private Const spGetDueDate As String = "spDuedateDiff"
    Private Const spListExec As String = "spChangeDueDateList"
    Private Const spSaveExec As String = "spProcessDueDateCExec"
    Private Const spSaveExecDet As String = "spProcessDueDateCExecDet"
    Private Const spSaveCancel As String = "spChangeDueDateCancel"

#Region "Request"
#Region "GetListAmor"
    Public Function GetListAmor(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@Applicationid", SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            customclass.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAmor, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("DChange", "GetListAmor", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function GetListAmorFACT(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@Applicationid", SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spViewAmortizationFACT", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("DChange", "GetListAmorFACT", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function GetListAmorMDKJ(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@Applicationid", SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spViewAmortizationMDKJ", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("DChange", "GetListAmorMDKJ", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region

#Region "GetList"
    Public Function GetList(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            If Not customclass.ApplicationID Is Nothing Then
                params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
                params(0).Value = customclass.ApplicationID.Trim
            Else
                params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
                params(0).Value = ""
            End If
            If Not customclass.BranchId Is Nothing Then
                params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                params(1).Value = customclass.BranchId
            Else
                params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                params(1).Value = ""
            End If

            'InterestTypeDesc

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spList, params)
            With customclass
                If objread.Read Then
                    .PaymentFrequency = CType(objread("PaymentFrequency"), String)
                    .ChangeDueDateFee = CType(objread("ChangeDueDateFee"), Double)
                    .ChangeDueDateBehaviour = CType(objread("ChangeDueDateFeeBehaviour"), String)
                    .EffectiveRate = CType(objread("EffectiveRate"), Decimal)
                    .Prepaid = CType(objread("ContractPrepaidAmount"), Decimal)
                    .ProductId = CType(objread("Productid"), String)
                    .AgreementTransferBehav = CType(objread("agreementTransferFeebehaviour"), String)
                    .GuarantorName = CType(objread("GuarantorName"), String)
                    .GuarantorID = CType(objread("GuarantorID"), String)
                    .IsCrossDefault = CType(objread("CrossDefaultApplicationID"), String)
                    .AgreementTransferFee = CType(objread("agreementtransferfee"), Double)
                    .Agreementno = CType(objread("AgreementNo"), String)
                    .CustomerID = CType(objread("Customerid"), String)
                    .CustomerName = CType(objread("Customername"), String)
                    .NextInstallmentDueDate = CType(objread("nextinstallmentduedate"), Date)
                    .InterestType = CType(objread("interestType"), String)
                    .InterestTypeDesc = CType(objread("interestTypeDesc"), String)
                    .InstallmentScheme = CType(objread("InstallmentScheme"), String)
                    .InstallmentSchemeDesc = CType(objread("InstallmentSchemeDesc"), String)
                    .FinanceType = CType(objread("FinanceType"), String)
                    .FinanceTypeDesc = CType(objread("FinanceTypeDesc"), String)
                    .NextInstallmentNumber = CType(objread("NextInstallmentDueNumber"), Integer)
                    .ReschedulingNo = CType(objread("rescNo"), Integer)
                    .ProductDesc = CType(objread("productdesc"), String)
                    .ProductOfferingDesc = CType(objread("productOfferingDesc"), String)
                    .NumOfInstallment = CType(objread("numofInstallment"), Integer)
                    .ReschedulingFee = CType(objread("ReschedulingFee"), Double)
                    .ReschedulingFeeBehaviour = objread("ReschedulingFeeBehaviour").ToString
                    .OutstandingPrincipal = CType(objread("OutstandingPrincipal"), Double)
                    .OutStandingInterest = CType(objread("OutstandingInterest"), Double)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("DChange", "GetList", exp.Message + exp.StackTrace)
        End Try

    End Function

#End Region

#Region "GetDueDate"
    Public Function GetDueDate(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID.Trim

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId.Trim

            params(2) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
            params(2).Value = customclass.EffectiveDate

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spGetDueDate, params)
            With customclass
                If objread.Read Then
                    .DueDateMax = CType(objread("duedatediff"), DateTime)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("DChange", "GetDueDate", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "Request Approval"
    Public Sub SaveChangeRequest(ByVal customClass As Parameter.DChange)
        Dim oConnection As New SqlConnection(customClass.strConnection)
        Dim oApprovalID As New maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim objTransaction As SqlTransaction
        Dim strApprovalNo As String

        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            objTransaction = oConnection.BeginTransaction
            With oEntitiesApproval
                .ApprovalTransaction = objTransaction
                .BranchId = customClass.BranchId
                .SchemeID = "ACDD"
                .RequestDate = customClass.BusinessDate
                .TransactionNo = customClass.ApplicationID
                .ApprovalValue = customClass.TotAmountToBePaid
                .UserRequest = customClass.LoginId
                .UserApproval = customClass.RequestTo
                .ApprovalNote = customClass.ChangeNotes
                .AprovalType = Parameter.Approval.ETransactionType.Agreement_ChangeDueDate
            End With
            strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)
            Dim params() As SqlParameter = New SqlParameter(10) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = customClass.ApplicationID

            params(2) = New SqlParameter("@Requestdate", SqlDbType.DateTime)
            params(2).Value = customClass.BusinessDate

            params(3) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
            params(3).Value = customClass.EffectiveDate

            params(4) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
            params(4).Value = customClass.InterestAmount

            params(5) = New SqlParameter("@AdministrationFee", SqlDbType.Decimal)
            params(5).Value = customClass.AdminFee

            params(6) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
            params(6).Value = strApprovalNo

            params(7) = New SqlParameter("@ReasonTypeID", SqlDbType.VarChar, 10)
            params(7).Value = customClass.ReasonTypeID

            params(8) = New SqlParameter("@ReasonID", SqlDbType.VarChar, 10)
            params(8).Value = customClass.ReasonID

            params(9) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 100)
            params(9).Value = customClass.ChangeNotes

            params(10) = New SqlParameter("@NextDueDate", SqlDbType.DateTime)
            params(10).Value = customClass.NextInstallmentDueDate

            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spRequest, params)
            objTransaction.Commit()
        Catch exp As Exception
            objTransaction.Rollback()
            WriteException("DChange", "SaveChangeRequest", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Sub
#End Region

#End Region

#Region "Execute"
#Region "GetListExec"
    Public Function GetListExec(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID.Trim

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            params(2) = New SqlParameter("@Seqno", SqlDbType.Int)
            params(2).Value = CInt(customclass.SeqNo)

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spListExec, params)
            With customclass
                If objread.Read Then
                    .BranchId = CType(objread("Branchid"), String)
                    .ApplicationID = CType(objread("ApplicationID"), String)
                    .Agreementno = CType(objread("Agreementno"), String)
                    .CustomerID = CType(objread("CustomerID"), String)
                    .CustomerName = CType(objread("Name"), String)
                    .EffectiveDate = CType(objread("EffectiveDate"), Date)
                    .RequestDate = CType(objread("RequestDate"), Date)
                    .AdminFee = CType(objread("administrationFee"), Decimal)
                    .ReasonDescription = CType(objread("reasondesc"), String)
                    .ReasonID = CType(objread("ReasonID"), String)
                    .ReasonTypeID = CType(objread("ReasonTypeID"), String)
                    .InterestAmount = CType(objread("InterestAmount"), Decimal)
                    .ChangeNotes = CType(objread("notes"), String)
                    .ApprovedBy = CType(objread("userRequest"), String)
                    .Prepaid = CType(objread("contractprepaidAmount"), Decimal)
                    .PaymentFrequency = CType(objread("PaymentFrequency"), String)
                    .TotAmountToBePaid = CType(objread("ApprovalValue"), Decimal)
                    .statusdesc = CType(objread("StatusDesc"), String)

                    .InstallmentDue = CType(objread("OSInstallmentDue"), Double)
                    .InsuranceDue = CType(objread("OSInsuranceDue"), Double)
                    .LcInstallment = CType(objread("OSLCInstallment"), Double)
                    .LcInsurance = CType(objread("OSLCInsurance"), Double)
                    .InstallmentCollFee = CType(objread("OSInstallCollectionFee"), Double)
                    .InsuranceCollFee = CType(objread("OSInsuranceCollectionFee"), Double)
                    .PDCBounceFee = CType(objread("OSPDCBounceFee"), Double)
                    .STNKRenewalFee = CType(objread("OSSTNKRenewalFee"), Double)
                    .InsuranceClaimExpense = CType(objread("OSInsuranceClaimExpense"), Double)
                    .RepossessionFee = CType(objread("OSRepossesFee"), Double)
                    .NextInstallmentDueDate = CType(objread("nextduedate"), Date)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("DChange", "GetListExec", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "ExecuteChangeDueDate"
    Public Sub SaveChangeExec(ByVal customClass As Parameter.DChange)
        Dim oConnection As New SqlConnection(customClass.strConnection)
        Dim objTransaction As SqlTransaction
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        objTransaction = oConnection.BeginTransaction
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Dim params2() As SqlParameter = New SqlParameter(4) {}
        Dim intLoopOmset As Integer
        Dim i As Integer = 0
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customClass.BranchId

            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = customClass.CoyID

            params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(2).Value = customClass.ApplicationID

            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = customClass.BusinessDate

            params(4) = New SqlParameter("@SeqNo", SqlDbType.Int)
            params(4).Value = customClass.SeqNo

            If customClass.flagDel = "1" Then
                SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spSaveCancel, params)
            ElseIf customClass.flagDel = "2" Then

                SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spSaveExec, params)

                params2(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params2(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
                params2(2) = New SqlParameter("@SeqNo", SqlDbType.Int)
                params2(3) = New SqlParameter("@DueDate", SqlDbType.VarChar, 10)
                params2(4) = New SqlParameter("@SeqNoA", SqlDbType.Int)
                ' params2(3) = New SqlParameter("@j", SqlDbType.Int)
                '  params2(4) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)

                If customClass.listdata.Rows.Count > 0 Then
                    For intLoopOmset = 0 To customClass.listdata.Rows.Count - 1
                        i = i + 1
                        params2(0).Value = customClass.BranchId
                        params2(1).Value = customClass.ApplicationID
                        params2(2).Value = CInt(customClass.listdata.Rows(intLoopOmset).Item("InSeqNo"))
                        params2(3).Value = customClass.listdata.Rows(intLoopOmset).Item("DueDate")
                        params2(4).Value = customClass.SeqNo
                        ' params2(3).Value = i
                        ' params2(4).Value = CDate(customClass.EffectiveDate)
                        SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spSaveExecDet, params2)
                    Next
                End If

            End If
            objTransaction.Commit()
        Catch exp As Exception
            WriteException("DChange", "SaveChangeExec", exp.Message + exp.StackTrace)
            objTransaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Sub
#End Region
#End Region

#Region "Request Due Change Modal Kerja"
	Public Function GetListAmorModalKerja(ByVal customclass As Parameter.DChange) As Parameter.DChange
		Dim params() As SqlParameter = New SqlParameter(2) {}
		Try
			params(0) = New SqlParameter("@Applicationid", SqlDbType.VarChar, 20)
			params(0).Value = customclass.ApplicationID

			params(1) = New SqlParameter("@RequestDate", SqlDbType.DateTime)
			params(1).Value = customclass.RequestDate

			params(2) = New SqlParameter("@effRate", SqlDbType.Decimal)
			params(2).Value = customclass.EffectiveRate

			customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spViewAmortizationModalKerja", params).Tables(0)
			Return customclass
		Catch exp As Exception
			WriteException("DChange", "GetListAmor", exp.Message + exp.StackTrace)
		End Try

	End Function

	Public Function GetListAmorModalKerjaFromTable(ByVal customclass As Parameter.DChange) As Parameter.DChange
		Dim params() As SqlParameter = New SqlParameter(1) {}
		Try
			params(0) = New SqlParameter("@Applicationid", SqlDbType.VarChar, 20)
			params(0).Value = customclass.ApplicationID

			params(1) = New SqlParameter("@ChangeSeqNo", SqlDbType.Int)
			params(1).Value = customclass.SeqNo

			customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGetAmortizationModalKerja", params).Tables(0)
			Return customclass
		Catch exp As Exception
			WriteException("DChange", "GetListAmor", exp.Message + exp.StackTrace)
		End Try

	End Function

	Public Function CreateAmorModalKerja(ByVal customclass As Parameter.DChange) As Parameter.DChange
		Dim params() As SqlParameter = New SqlParameter(3) {}
		Try
			params(0) = New SqlParameter("@Applicationid", SqlDbType.VarChar, 20)
			params(0).Value = customclass.ApplicationID

			params(1) = New SqlParameter("@RequestDate", SqlDbType.DateTime)
			params(1).Value = customclass.RequestDate

			params(2) = New SqlParameter("@effRate", SqlDbType.Decimal)
			params(2).Value = customclass.EffectiveRate

			params(3) = New SqlParameter("@UserUpd", SqlDbType.VarChar, 10)
			params(3).Value = customclass.LoginId

			SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spCreateAmortizationModalKerjaDueDateChange", params)
		Catch exp As Exception
			WriteException("DChangeModalKerja", "CreateAmorModalKerja", exp.Message + exp.StackTrace)
		End Try

	End Function

	Public Function GetListModalKerja(ByVal customclass As Parameter.DChange) As Parameter.DChange
		Dim objread As SqlDataReader
		Dim params() As SqlParameter = New SqlParameter(1) {}
		Try
			If Not customclass.ApplicationID Is Nothing Then
				params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
				params(0).Value = customclass.ApplicationID.Trim
			Else
				params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
				params(0).Value = ""
			End If
			If Not customclass.BranchId Is Nothing Then
				params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
				params(1).Value = customclass.BranchId
			Else
				params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
				params(1).Value = ""
			End If

			objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spDProductModalKerja", params)
			With customclass
				If objread.Read Then
					.PaymentFrequency = CType(objread("PaymentFrequency"), String)
					.ChangeDueDateFee = CType(objread("ChangeDueDateFee"), Double)
					.ChangeDueDateBehaviour = CType(objread("ChangeDueDateFeeBehaviour"), String)
					.EffectiveRate = CType(objread("EffectiveRate"), Decimal)
					.Prepaid = CType(objread("ContractPrepaidAmount"), Decimal)
					.ProductId = CType(objread("Productid"), String)
					.AgreementTransferBehav = CType(objread("agreementTransferFeebehaviour"), String)
					.GuarantorName = CType(objread("GuarantorName"), String)
					.GuarantorID = CType(objread("GuarantorID"), String)
					.IsCrossDefault = CType(objread("CrossDefaultApplicationID"), String)
					.AgreementTransferFee = CType(objread("agreementtransferfee"), Double)
					.Agreementno = CType(objread("AgreementNo"), String)
					.CustomerID = CType(objread("Customerid"), String)
					.CustomerName = CType(objread("Customername"), String)
					.NextInstallmentDueDate = CType(objread("nextinstallmentduedate"), Date)
					.InterestType = CType(objread("interestType"), String)
					.InterestTypeDesc = CType(objread("interestTypeDesc"), String)
					.InstallmentScheme = CType(objread("InstallmentScheme"), String)
					.InstallmentSchemeDesc = CType(objread("InstallmentSchemeDesc"), String)
					.FinanceType = CType(objread("FinanceType"), String)
					.FinanceTypeDesc = CType(objread("FinanceTypeDesc"), String)
					.NextInstallmentNumber = CType(objread("NextInstallmentDueNumber"), Integer)
					.ReschedulingNo = CType(objread("rescNo"), Integer)
					.ProductDesc = CType(objread("productdesc"), String)
					.ProductOfferingDesc = CType(objread("productOfferingDesc"), String)
					.NumOfInstallment = CType(objread("numofInstallment"), Integer)
					.ReschedulingFee = CType(objread("ReschedulingFee"), Double)
					.ReschedulingFeeBehaviour = objread("ReschedulingFeeBehaviour").ToString
					.OutstandingPrincipal = CType(objread("OutstandingPrincipal"), Double)
					.OutStandingInterest = CType(objread("OutstandingInterest"), Double)
				End If
				objread.Close()
			End With
			Return customclass
		Catch exp As Exception
			WriteException("DChange", "GetList", exp.Message + exp.StackTrace)
		End Try

	End Function

	Public Sub MDKJSaveChangeRequest(ByVal customClass As Parameter.DChange)
		Dim oConnection As New SqlConnection(customClass.strConnection)
		Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
		Dim oEntitiesApproval As New Parameter.Approval
		Dim objTransaction As SqlTransaction
		Dim strApprovalNo As String

		Try
			If oConnection.State = ConnectionState.Closed Then oConnection.Open()
			objTransaction = oConnection.BeginTransaction
			With oEntitiesApproval
				.ApprovalTransaction = objTransaction
				.BranchId = customClass.BranchId
				.SchemeID = "MCDD"
				.RequestDate = customClass.BusinessDate
				.TransactionNo = customClass.ApplicationID
				'.ApprovalValue = customClass.TotAmountToBePaid
				.ApprovalValue = customClass.OutstandingPrincipal
				.UserRequest = customClass.LoginId
				.UserApproval = customClass.RequestTo
				.ApprovalNote = customClass.ChangeNotes
				.AprovalType = Parameter.Approval.ETransactionType.Agreement_ChangeDueDate
			End With
			strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)
			Dim params() As SqlParameter = New SqlParameter(10) {}

			params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
			params(0).Value = customClass.BranchId.Replace("'", "")

			params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
			params(1).Value = customClass.ApplicationID

			params(2) = New SqlParameter("@Requestdate", SqlDbType.DateTime)
			params(2).Value = customClass.BusinessDate

			params(3) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
			params(3).Value = customClass.EffectiveDate

			params(4) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
			params(4).Value = customClass.InterestAmount

			params(5) = New SqlParameter("@AdministrationFee", SqlDbType.Decimal)
			params(5).Value = customClass.AdminFee

			params(6) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
			params(6).Value = strApprovalNo

			params(7) = New SqlParameter("@ReasonTypeID", SqlDbType.VarChar, 10)
			params(7).Value = customClass.ReasonTypeID

			params(8) = New SqlParameter("@ReasonID", SqlDbType.VarChar, 10)
			params(8).Value = customClass.ReasonID

			params(9) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 100)
			params(9).Value = customClass.ChangeNotes

			params(10) = New SqlParameter("@NextDueDate", SqlDbType.DateTime)
			params(10).Value = customClass.NextInstallmentDueDate

			SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "spMDKJDueDateRequest", params)
			objTransaction.Commit()
		Catch exp As Exception
			objTransaction.Rollback()
			WriteException("DChange", "SaveChangeRequest", exp.Message + exp.StackTrace)
			Throw New Exception(exp.Message)
		Finally
			If oConnection.State = ConnectionState.Open Then oConnection.Close()
			oConnection.Dispose()
		End Try
	End Sub

	Public Function GetListExecMDKJ(ByVal customclass As Parameter.DChange) As Parameter.DChange
		Dim objread As SqlDataReader
		Dim params() As SqlParameter = New SqlParameter(2) {}
		Try
			params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
			params(0).Value = customclass.ApplicationID.Trim

			params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
			params(1).Value = customclass.BranchId

			params(2) = New SqlParameter("@Seqno", SqlDbType.Int)
			params(2).Value = CInt(customclass.SeqNo)

			objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spChangeDueDateList", params)
			With customclass
				If objread.Read Then
					.BranchId = CType(objread("Branchid"), String)
					.ApplicationID = CType(objread("ApplicationID"), String)
					.Agreementno = CType(objread("Agreementno"), String)
					.CustomerID = CType(objread("CustomerID"), String)
					.CustomerName = CType(objread("Name"), String)
					.EffectiveDate = CType(objread("EffectiveDate"), Date)
					.RequestDate = CType(objread("RequestDate"), Date)
					.AdminFee = CType(objread("administrationFee"), Decimal)
					.ReasonDescription = CType(objread("reasondesc"), String)
					.ReasonID = CType(objread("ReasonID"), String)
					.ReasonTypeID = CType(objread("ReasonTypeID"), String)
					.InterestAmount = CType(objread("InterestAmount"), Decimal)
					.ChangeNotes = CType(objread("notes"), String)
					.ApprovedBy = CType(objread("userRequest"), String)
					.Prepaid = CType(objread("contractprepaidAmount"), Decimal)
					.PaymentFrequency = CType(objread("PaymentFrequency"), String)
					'.TotAmountToBePaid = CType(objread("ApprovalValue"), Decimal)
					.TotAmountToBePaid = CType(objread("TotAmountToBePaid"), Decimal)
					.statusdesc = CType(objread("StatusDesc"), String)

					.InstallmentDue = CType(objread("OSInstallmentDue"), Double)
					.InsuranceDue = CType(objread("OSInsuranceDue"), Double)
					.LcInstallment = CType(objread("OSLCInstallment"), Double)
					.LcInsurance = CType(objread("OSLCInsurance"), Double)
					.InstallmentCollFee = CType(objread("OSInstallCollectionFee"), Double)
					.InsuranceCollFee = CType(objread("OSInsuranceCollectionFee"), Double)
					.PDCBounceFee = CType(objread("OSPDCBounceFee"), Double)
					.STNKRenewalFee = CType(objread("OSSTNKRenewalFee"), Double)
					.InsuranceClaimExpense = CType(objread("OSInsuranceClaimExpense"), Double)
					.RepossessionFee = CType(objread("OSRepossesFee"), Double)
					.NextInstallmentDueDate = CType(objread("nextduedate"), Date)
				End If
				objread.Close()
			End With
			Return customclass
		Catch exp As Exception
			WriteException("DChange", "GetListExec", exp.Message + exp.StackTrace)
		End Try
	End Function

	Public Sub SaveChangeExecMDKJ(ByVal customClass As Parameter.DChange)
		Dim oConnection As New SqlConnection(customClass.strConnection)
		Dim objTransaction As SqlTransaction
		If oConnection.State = ConnectionState.Closed Then oConnection.Open()
		objTransaction = oConnection.BeginTransaction
		Dim params() As SqlParameter = New SqlParameter(4) {}
		Dim i As Integer = 0
		Try
			params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
			params(0).Value = customClass.BranchId

			params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
			params(1).Value = customClass.CoyID

			params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
			params(2).Value = customClass.ApplicationID

			params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
			params(3).Value = customClass.BusinessDate

			params(4) = New SqlParameter("@SeqNo", SqlDbType.Int)
			params(4).Value = customClass.SeqNo

			If customClass.flagDel = "1" Then
				SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spSaveCancel, params)
			ElseIf customClass.flagDel = "2" Then
				SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "spProcessDueDateCExecMDKJ", params)
			End If
			objTransaction.Commit()

		Catch exp As Exception
			WriteException("DChange", "SaveChangeExec", exp.Message + exp.StackTrace)
			objTransaction.Rollback()
			Throw New Exception(exp.Message)
		Finally
			If oConnection.State = ConnectionState.Open Then oConnection.Close()
			oConnection.Dispose()
		End Try
	End Sub
#End Region

#Region "Request Due Change Factoring"
	Public Function GetListAmorFactoring(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
			params(0) = New SqlParameter("@Applicationid", SqlDbType.VarChar, 20)
			params(0).Value = customclass.ApplicationID

			params(1) = New SqlParameter("@RequestDate", SqlDbType.DateTime)
			params(1).Value = customclass.RequestDate

			params(2) = New SqlParameter("@effRate", SqlDbType.Decimal)
			params(2).Value = customclass.EffectiveRate

			params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(3).Value = customclass.InsSeqNo

            params(4) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(4).Value = customclass.InvoiceSeqNo

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spViewInvoiceFactoring", params).Tables(0)
			Return customclass
		Catch exp As Exception
			WriteException("DChange", "GetListAmor", exp.Message + exp.StackTrace)
		End Try

	End Function

	Public Function GetListInvoiceFactoring(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
			params(0) = New SqlParameter("@Applicationid", SqlDbType.VarChar, 20)
			params(0).Value = customclass.ApplicationID
			params(1) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(1).Value = customclass.InsSeqNo
            params(2) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(2).Value = customclass.InvoiceSeqNo
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGetListInvoiceFactoring", params).Tables(0)
			Return customclass
		Catch exp As Exception
			WriteException("DChange", "GetListInvoiceFactoring", exp.Message + exp.StackTrace)
		End Try

	End Function

	Public Function GetListAmorFactoringFromTable(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter("@Applicationid", SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter("@ChangeSeqNo", SqlDbType.Int)
            params(1).Value = customclass.SeqNo

            params(2) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(2).Value = customclass.InsSeqNo

            params(3) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
            params(3).Value = customclass.InvoiceNo

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGetAmortizationFactoring", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("DChange", "GetListAmor", exp.Message + exp.StackTrace)
        End Try
    End Function

	Public Function CreateAmorFactoring(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
			params(0) = New SqlParameter("@Applicationid", SqlDbType.VarChar, 20)
			params(0).Value = customclass.ApplicationID

			params(1) = New SqlParameter("@RequestDate", SqlDbType.DateTime)
			params(1).Value = customclass.RequestDate

			params(2) = New SqlParameter("@effRate", SqlDbType.Decimal)
			params(2).Value = customclass.EffectiveRate

			params(3) = New SqlParameter("@UserUpd", SqlDbType.VarChar, 10)
			params(3).Value = customclass.LoginId

			params(4) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(4).Value = customclass.InsSeqNo

            params(5) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
            params(5).Value = customclass.InvoiceNo

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spCreateInvoiceFactoringDueDateChange", params)
		Catch exp As Exception
			WriteException("DChangeFactoring", "CreateAmorFactoring", exp.Message + exp.StackTrace)
		End Try

	End Function

	Public Function GetListFactoring(ByVal customclass As Parameter.DChange) As Parameter.DChange
		Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
			If Not customclass.ApplicationID Is Nothing Then
				params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
				params(0).Value = customclass.ApplicationID.Trim
			Else
				params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
				params(0).Value = ""
			End If
			If Not customclass.BranchId Is Nothing Then
				params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
				params(1).Value = customclass.BranchId
			Else
				params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
				params(1).Value = ""
			End If
            If Not customclass.InsSeqNo Is Nothing Then
                params(2) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
                params(2).Value = customclass.InsSeqNo
            Else
                params(2) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
                params(2).Value = ""
            End If
            If Not customclass.InvoiceNo Is Nothing Then
                params(3) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 20)
                params(3).Value = customclass.InvoiceNo
            Else
                params(3) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 20)
                params(3).Value = ""
            End If

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spDProductFactoring", params)
			With customclass
				If objread.Read Then
					.PaymentFrequency = CType(objread("PaymentFrequency"), String)
					.ChangeDueDateFee = CType(objread("ChangeDueDateFee"), Double)
					.ChangeDueDateBehaviour = CType(objread("ChangeDueDateFeeBehaviour"), String)
					.EffectiveRate = CType(objread("EffectiveRate"), Decimal)
					.Prepaid = CType(objread("ContractPrepaidAmount"), Decimal)
					.ProductId = CType(objread("Productid"), String)
					.AgreementTransferBehav = CType(objread("agreementTransferFeebehaviour"), String)
					.GuarantorName = CType(objread("GuarantorName"), String)
					.GuarantorID = CType(objread("GuarantorID"), String)
					.IsCrossDefault = CType(objread("CrossDefaultApplicationID"), String)
					.AgreementTransferFee = CType(objread("agreementtransferfee"), Double)
					.Agreementno = CType(objread("AgreementNo"), String)
					.CustomerID = CType(objread("Customerid"), String)
					.CustomerName = CType(objread("Customername"), String)
					.NextInstallmentDueDate = CType(objread("nextinstallmentduedate"), Date)
					.InterestType = CType(objread("interestType"), String)
					.InterestTypeDesc = CType(objread("interestTypeDesc"), String)
					.InstallmentScheme = CType(objread("InstallmentScheme"), String)
					.InstallmentSchemeDesc = CType(objread("InstallmentSchemeDesc"), String)
					.FinanceType = CType(objread("FinanceType"), String)
					.FinanceTypeDesc = CType(objread("FinanceTypeDesc"), String)
					.NextInstallmentNumber = CType(objread("NextInstallmentDueNumber"), Integer)
					.ReschedulingNo = CType(objread("rescNo"), Integer)
					.ProductDesc = CType(objread("productdesc"), String)
					.ProductOfferingDesc = CType(objread("productOfferingDesc"), String)
					.NumOfInstallment = CType(objread("numofInstallment"), Integer)
					.ReschedulingFee = CType(objread("ReschedulingFee"), Double)
					.ReschedulingFeeBehaviour = objread("ReschedulingFeeBehaviour").ToString
					.OutstandingPrincipal = CType(objread("OutstandingPrincipal"), Double)
					.OutStandingInterest = CType(objread("OutstandingInterest"), Double)
				End If
				objread.Close()
			End With
			Return customclass
		Catch exp As Exception
			WriteException("DChange", "GetList", exp.Message + exp.StackTrace)
		End Try

	End Function

	Public Sub SaveChangeRequestFactoring(ByVal customClass As Parameter.DChange)
		Dim oConnection As New SqlConnection(customClass.strConnection)
		Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
		Dim oEntitiesApproval As New Parameter.Approval
		Dim objTransaction As SqlTransaction
		Dim strApprovalNo As String

		Try
			If oConnection.State = ConnectionState.Closed Then oConnection.Open()
			objTransaction = oConnection.BeginTransaction
			With oEntitiesApproval
				.ApprovalTransaction = objTransaction
				.BranchId = customClass.BranchId
				.SchemeID = "FCDD"
				.RequestDate = customClass.BusinessDate
                .TransactionNo = customClass.InvoiceNo
                '* ApprovalValue diambil dari nilai Total Outstanding *'
                .ApprovalValue = customClass.OutstandingPrincipal
				'.ApprovalValue = customClass.TotAmountToBePaid
				.UserRequest = customClass.LoginId
				.UserApproval = customClass.RequestTo
				.ApprovalNote = customClass.ChangeNotes
				.AprovalType = Parameter.Approval.ETransactionType.Agreement_ChangeDueDate
			End With
			strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)
            Dim params() As SqlParameter = New SqlParameter(12) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
			params(0).Value = customClass.BranchId.Replace("'", "")

			params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
			params(1).Value = customClass.ApplicationID

			params(2) = New SqlParameter("@Requestdate", SqlDbType.DateTime)
			params(2).Value = customClass.BusinessDate

			params(3) = New SqlParameter("@EffectiveDate", SqlDbType.DateTime)
			params(3).Value = customClass.EffectiveDate

			params(4) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
			params(4).Value = customClass.InterestAmount

			params(5) = New SqlParameter("@AdministrationFee", SqlDbType.Decimal)
			params(5).Value = customClass.AdminFee

			params(6) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
			params(6).Value = strApprovalNo

			params(7) = New SqlParameter("@ReasonTypeID", SqlDbType.VarChar, 10)
			params(7).Value = customClass.ReasonTypeID

			params(8) = New SqlParameter("@ReasonID", SqlDbType.VarChar, 10)
			params(8).Value = customClass.ReasonID

			params(9) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 100)
			params(9).Value = customClass.ChangeNotes

			params(10) = New SqlParameter("@NextDueDate", SqlDbType.DateTime)
			params(10).Value = customClass.NextInstallmentDueDate

			params(11) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(11).Value = customClass.InsSeqNo

            params(12) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
            params(12).Value = customClass.InvoiceNo


            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "spDueDateRequestFactoring", params)
			objTransaction.Commit()
		Catch exp As Exception
			objTransaction.Rollback()
			WriteException("DChange", "SaveChangeRequest", exp.Message + exp.StackTrace)
			Throw New Exception(exp.Message)
		Finally
			If oConnection.State = ConnectionState.Open Then oConnection.Close()
			oConnection.Dispose()
		End Try
	End Sub

    Public Function GetListExecFactoring(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID.Trim

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            params(2) = New SqlParameter("@Seqno", SqlDbType.Int)
            params(2).Value = CInt(customclass.InsSeqNo)

            params(3) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(3).Value = CInt(customclass.InvoiceSeqNo)

            params(4) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
            params(4).Value = customclass.InvoiceNo

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spChangeDueDateListFactoring", params)
            With customclass
                If objread.Read Then
                    .BranchId = CType(objread("Branchid"), String)
                    .ApplicationID = CType(objread("ApplicationID"), String)
                    .Agreementno = CType(objread("Agreementno"), String)
                    .CustomerID = CType(objread("CustomerID"), String)
                    .CustomerName = CType(objread("Name"), String)
                    .EffectiveDate = CType(objread("EffectiveDate"), Date)
                    .RequestDate = CType(objread("RequestDate"), Date)
                    .AdminFee = CType(objread("administrationFee"), Decimal)
                    .ReasonDescription = CType(objread("reasondesc"), String)
                    .ReasonID = CType(objread("ReasonID"), String)
                    .ReasonTypeID = CType(objread("ReasonTypeID"), String)
                    .InterestAmount = CType(objread("InterestAmount"), Decimal)
                    .ChangeNotes = CType(objread("notes"), String)
                    .ApprovedBy = CType(objread("userRequest"), String)
                    .Prepaid = CType(objread("contractprepaidAmount"), Decimal)
                    .PaymentFrequency = CType(objread("PaymentFrequency"), String)
                    '.TotAmountToBePaid = CType(objread("ApprovalValue"), Decimal)
                    .TotAmountToBePaid = CType(objread("TotAmountToBePaid"), Decimal)
                    .statusdesc = CType(objread("StatusDesc"), String)

                    .InstallmentDue = CType(objread("OSInstallmentDue"), Double)
                    .InsuranceDue = CType(objread("OSInsuranceDue"), Double)
                    .LcInstallment = CType(objread("OSLCInstallment"), Double)
                    .LcInsurance = CType(objread("OSLCInsurance"), Double)
                    .InstallmentCollFee = CType(objread("OSInstallCollectionFee"), Double)
                    .InsuranceCollFee = CType(objread("OSInsuranceCollectionFee"), Double)
                    .PDCBounceFee = CType(objread("OSPDCBounceFee"), Double)
                    .STNKRenewalFee = CType(objread("OSSTNKRenewalFee"), Double)
                    .InsuranceClaimExpense = CType(objread("OSInsuranceClaimExpense"), Double)
                    .RepossessionFee = CType(objread("OSRepossesFee"), Double)
                    .NextInstallmentDueDate = CType(objread("nextduedate"), Date)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("DChange", "GetListExec", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetListExecFactoringApproval(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID.Trim

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            params(2) = New SqlParameter("@Seqno", SqlDbType.Int)
            params(2).Value = CInt(customclass.SeqNo)

            params(3) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(3).Value = CInt(customclass.InsSeqNo)

            params(4) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
            params(4).Value = customclass.InvoiceNo

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spChangeDueDateListFactoringApproval", params)
            With customclass
                If objread.Read Then
                    .BranchId = CType(objread("Branchid"), String)
                    .ApplicationID = CType(objread("ApplicationID"), String)
                    .Agreementno = CType(objread("Agreementno"), String)
                    .CustomerID = CType(objread("CustomerID"), String)
                    .CustomerName = CType(objread("Name"), String)
                    .EffectiveDate = CType(objread("EffectiveDate"), Date)
                    .RequestDate = CType(objread("RequestDate"), Date)
                    .AdminFee = CType(objread("administrationFee"), Decimal)
                    .ReasonDescription = CType(objread("reasondesc"), String)
                    .ReasonID = CType(objread("ReasonID"), String)
                    .ReasonTypeID = CType(objread("ReasonTypeID"), String)
                    .InterestAmount = CType(objread("InterestAmount"), Decimal)
                    .ChangeNotes = CType(objread("notes"), String)
                    .ApprovedBy = CType(objread("employeename"), String)
                    .Prepaid = CType(objread("contractprepaidAmount"), Decimal)
                    .PaymentFrequency = CType(objread("PaymentFrequency"), String)
                    '.TotAmountToBePaid = CType(objread("ApprovalValue"), Decimal)
                    .TotAmountToBePaid = CType(objread("TotAmountToBePaid"), Decimal)
                    .statusdesc = CType(objread("StatusDesc"), String)

                    .InstallmentDue = CType(objread("OSInstallmentDue"), Double)
                    .InsuranceDue = CType(objread("OSInsuranceDue"), Double)
                    .LcInstallment = CType(objread("OSLCInstallment"), Double)
                    .LcInsurance = CType(objread("OSLCInsurance"), Double)
                    .InstallmentCollFee = CType(objread("OSInstallCollectionFee"), Double)
                    .InsuranceCollFee = CType(objread("OSInsuranceCollectionFee"), Double)
                    .PDCBounceFee = CType(objread("OSPDCBounceFee"), Double)
                    .STNKRenewalFee = CType(objread("OSSTNKRenewalFee"), Double)
                    .InsuranceClaimExpense = CType(objread("OSInsuranceClaimExpense"), Double)
                    .RepossessionFee = CType(objread("OSRepossesFee"), Double)
                    .NextInstallmentDueDate = CType(objread("nextduedate"), Date)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("DChange", "GetListExec", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub SaveChangeExecFactoring(ByVal customClass As Parameter.DChange)
		Dim oConnection As New SqlConnection(customClass.strConnection)
		Dim objTransaction As SqlTransaction
		If oConnection.State = ConnectionState.Closed Then oConnection.Open()
		objTransaction = oConnection.BeginTransaction
        Dim params() As SqlParameter = New SqlParameter(6) {}
        Dim i As Integer = 0
		Try
			params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
			params(0).Value = customClass.BranchId

			params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
			params(1).Value = customClass.CoyID

			params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
			params(2).Value = customClass.ApplicationID

			params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
			params(3).Value = customClass.BusinessDate

			params(4) = New SqlParameter("@SeqNo", SqlDbType.Int)
			params(4).Value = customClass.SeqNo

			params(5) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(5).Value = customClass.InsSeqNo

            params(6) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
            params(6).Value = customClass.InvoiceNo

            If customClass.flagDel = "1" Then
				SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spSaveCancel, params)
			ElseIf customClass.flagDel = "2" Then
				SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "spProcessDueDateCExecFactoring", params)
			End If
			objTransaction.Commit()

		Catch exp As Exception
			WriteException("DChange", "SaveChangeExec", exp.Message + exp.StackTrace)
			objTransaction.Rollback()
			Throw New Exception(exp.Message)
		Finally
			If oConnection.State = ConnectionState.Open Then oConnection.Close()
			oConnection.Dispose()
		End Try
	End Sub
#End Region


    Public Function GetListFactAndMU(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            If Not customclass.ApplicationID Is Nothing Then
                params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
                params(0).Value = customclass.ApplicationID.Trim
            Else
                params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
                params(0).Value = ""
            End If
            If Not customclass.BranchId Is Nothing Then
                params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                params(1).Value = customclass.BranchId
            Else
                params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                params(1).Value = ""
            End If
            If Not customclass.InvoiceNo Is Nothing Then
                params(2) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
                params(2).Value = customclass.InvoiceNo
            Else
                params(2) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
                params(2).Value = ""
            End If

            params(3) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(3).Value = customclass.InvoiceSeqNo

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spDProductFactAndMU", params)
            With customclass
                If objread.Read Then
                    .PaymentFrequency = CType(objread("PaymentFrequency"), String)
                    .ChangeDueDateFee = CType(objread("ChangeDueDateFee"), Double)
                    .ChangeDueDateBehaviour = CType(objread("ChangeDueDateFeeBehaviour"), String)
                    .EffectiveRate = CType(objread("EffectiveRate"), Decimal)
                    .Prepaid = CType(objread("ContractPrepaidAmount"), Decimal)
                    .ProductId = CType(objread("Productid"), String)
                    .AgreementTransferBehav = CType(objread("agreementTransferFeebehaviour"), String)
                    .GuarantorName = CType(objread("GuarantorName"), String)
                    .GuarantorID = CType(objread("GuarantorID"), String)
                    .IsCrossDefault = CType(objread("CrossDefaultApplicationID"), String)
                    .AgreementTransferFee = CType(objread("agreementtransferfee"), Double)
                    .Agreementno = CType(objread("AgreementNo"), String)
                    .CustomerID = CType(objread("Customerid"), String)
                    .CustomerName = CType(objread("Customername"), String)
                    .NextInstallmentDueDate = CType(objread("nextinstallmentduedate"), Date)
                    .InterestType = CType(objread("interestType"), String)
                    .InterestTypeDesc = CType(objread("interestTypeDesc"), String)
                    .InstallmentScheme = CType(objread("InstallmentScheme"), String)
                    .InstallmentSchemeDesc = CType(objread("InstallmentSchemeDesc"), String)
                    .FinanceType = CType(objread("FinanceType"), String)
                    .FinanceTypeDesc = CType(objread("FinanceTypeDesc"), String)
                    .NextInstallmentNumber = CType(objread("NextInstallmentDueNumber"), Integer)
                    .ReschedulingNo = CType(objread("rescNo"), Integer)
                    .ProductDesc = CType(objread("productdesc"), String)
                    .ProductOfferingDesc = CType(objread("productOfferingDesc"), String)
                    .NumOfInstallment = CType(objread("numofInstallment"), Integer)
                    .ReschedulingFee = CType(objread("ReschedulingFee"), Double)
                    .ReschedulingFeeBehaviour = objread("ReschedulingFeeBehaviour").ToString
                    .OutstandingPrincipal = CType(objread("OutstandingPrincipal"), Double)
                    .OutStandingInterest = CType(objread("OutstandingInterest"), Double)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("DChange", "GetListFactAndMU", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function GetListReschedulingApproval(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            If Not customclass.ApplicationID Is Nothing Then
                params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
                params(0).Value = customclass.ApplicationID.Trim
            Else
                params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
                params(0).Value = ""
            End If
            If Not customclass.BranchId Is Nothing Then
                params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                params(1).Value = customclass.BranchId
            Else
                params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                params(1).Value = ""
            End If

            'InterestTypeDesc

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spList, params)
            With customclass
                If objread.Read Then
                    .PaymentFrequency = CType(objread("PaymentFrequency"), String)
                    .ChangeDueDateFee = CType(objread("ChangeDueDateFee"), Double)
                    .ChangeDueDateBehaviour = CType(objread("ChangeDueDateFeeBehaviour"), String)
                    .EffectiveRate = CType(objread("EffectiveRate"), Decimal)
                    .Prepaid = CType(objread("ContractPrepaidAmount"), Decimal)
                    .ProductId = CType(objread("Productid"), String)
                    .AgreementTransferBehav = CType(objread("agreementTransferFeebehaviour"), String)
                    .GuarantorName = CType(objread("GuarantorName"), String)
                    .GuarantorID = CType(objread("GuarantorID"), String)
                    .IsCrossDefault = CType(objread("CrossDefaultApplicationID"), String)
                    .AgreementTransferFee = CType(objread("agreementtransferfee"), Double)
                    .Agreementno = CType(objread("AgreementNo"), String)
                    .CustomerID = CType(objread("Customerid"), String)
                    .CustomerName = CType(objread("Customername"), String)
                    .NextInstallmentDueDate = CType(objread("nextinstallmentduedate"), Date)
                    .InterestType = CType(objread("interestType"), String)
                    .InterestTypeDesc = CType(objread("interestTypeDesc"), String)
                    .InstallmentScheme = CType(objread("InstallmentScheme"), String)
                    .InstallmentSchemeDesc = CType(objread("InstallmentSchemeDesc"), String)
                    .FinanceType = CType(objread("FinanceType"), String)
                    .FinanceTypeDesc = CType(objread("FinanceTypeDesc"), String)
                    .NextInstallmentNumber = CType(objread("NextInstallmentDueNumber"), Integer)
                    .ReschedulingNo = CType(objread("rescNo"), Integer)
                    .ProductDesc = CType(objread("productdesc"), String)
                    .ProductOfferingDesc = CType(objread("productOfferingDesc"), String)
                    .NumOfInstallment = CType(objread("numofInstallment"), Integer)
                    .ReschedulingFee = CType(objread("ReschedulingFee"), Double)
                    .ReschedulingFeeBehaviour = objread("ReschedulingFeeBehaviour").ToString
                    .OutstandingPrincipal = CType(objread("OutstandingPrincipal"), Double)
                    .OutStandingInterest = CType(objread("OutstandingInterest"), Double)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("DChange", "GetListReschedulingApproval", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function GetListFactoringView(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID.Trim

            params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            params(2) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(2).Value = customclass.InsSeqNo

            params(3) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
            params(3).Value = customclass.InvoiceNo.Trim

            params(4) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(4).Value = customclass.InvoiceSeqNo

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spGetFactoringView", params)
            With customclass
                If objread.Read Then
                    .PaymentFrequency = CType(objread("PaymentFrequency"), String)
                    .ChangeDueDateFee = CType(objread("ChangeDueDateFee"), Double)
                    .ChangeDueDateBehaviour = CType(objread("ChangeDueDateFeeBehaviour"), String)
                    .EffectiveRate = CType(objread("EffectiveRate"), Decimal)
                    .Prepaid = CType(objread("ContractPrepaidAmount"), Decimal)
                    .ProductId = CType(objread("Productid"), String)
                    .AgreementTransferBehav = CType(objread("agreementTransferFeebehaviour"), String)
                    .GuarantorName = CType(objread("GuarantorName"), String)
                    .GuarantorID = CType(objread("GuarantorID"), String)
                    .IsCrossDefault = CType(objread("CrossDefaultApplicationID"), String)
                    .AgreementTransferFee = CType(objread("agreementtransferfee"), Double)
                    .Agreementno = CType(objread("AgreementNo"), String)
                    .CustomerID = CType(objread("Customerid"), String)
                    .CustomerName = CType(objread("Customername"), String)
                    .NextInstallmentDueDate = CType(objread("nextinstallmentduedate"), Date)
                    .InterestType = CType(objread("interestType"), String)
                    .InterestTypeDesc = CType(objread("interestTypeDesc"), String)
                    .InstallmentScheme = CType(objread("InstallmentScheme"), String)
                    .InstallmentSchemeDesc = CType(objread("InstallmentSchemeDesc"), String)
                    .FinanceType = CType(objread("FinanceType"), String)
                    .FinanceTypeDesc = CType(objread("FinanceTypeDesc"), String)
                    .NextInstallmentNumber = CType(objread("NextInstallmentDueNumber"), Integer)
                    .ReschedulingNo = CType(objread("rescNo"), Integer)
                    .ProductDesc = CType(objread("productdesc"), String)
                    .ProductOfferingDesc = CType(objread("productOfferingDesc"), String)
                    .NumOfInstallment = CType(objread("numofInstallment"), Integer)
                    .ReschedulingFee = CType(objread("ReschedulingFee"), Double)
                    .ReschedulingFeeBehaviour = objread("ReschedulingFeeBehaviour").ToString
                    .OutstandingPrincipal = CType(objread("OutstandingPrincipal"), Double)
                    .OutStandingInterest = CType(objread("OutstandingInterest"), Double)
                    .RequestDate = CType(objread("RequestDate"), Date)
                    .NextDueDate = CType(objread("NextDueDate"), Date)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("GetListFactoringView", "GetListFactoringView", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetListFactoringViewApproval(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            'params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            'params(0).Value = customclass.ApplicationID.Trim

            'params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            'params(1).Value = customclass.BranchId

            'params(0) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            'params(0).Value = customclass.InsSeqNo

            params(0) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
            params(0).Value = customclass.InvoiceNo.Trim

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spGetFactoringViewApproval", params)
            With customclass
                If objread.Read Then
                    .PaymentFrequency = CType(objread("PaymentFrequency"), String)
                    .ChangeDueDateFee = CType(objread("ChangeDueDateFee"), Double)
                    .ChangeDueDateBehaviour = CType(objread("ChangeDueDateFeeBehaviour"), String)
                    .EffectiveRate = CType(objread("EffectiveRate"), Decimal)
                    .Prepaid = CType(objread("ContractPrepaidAmount"), Decimal)
                    .ProductId = CType(objread("Productid"), String)
                    .AgreementTransferBehav = CType(objread("agreementTransferFeebehaviour"), String)
                    .GuarantorName = CType(objread("GuarantorName"), String)
                    .GuarantorID = CType(objread("GuarantorID"), String)
                    .IsCrossDefault = CType(objread("CrossDefaultApplicationID"), String)
                    .AgreementTransferFee = CType(objread("agreementtransferfee"), Double)
                    .Agreementno = CType(objread("AgreementNo"), String)
                    .CustomerID = CType(objread("Customerid"), String)
                    .CustomerName = CType(objread("Customername"), String)
                    .NextInstallmentDueDate = CType(objread("nextinstallmentduedate"), Date)
                    .InterestType = CType(objread("interestType"), String)
                    .InterestTypeDesc = CType(objread("interestTypeDesc"), String)
                    .InstallmentScheme = CType(objread("InstallmentScheme"), String)
                    .InstallmentSchemeDesc = CType(objread("InstallmentSchemeDesc"), String)
                    .FinanceType = CType(objread("FinanceType"), String)
                    .FinanceTypeDesc = CType(objread("FinanceTypeDesc"), String)
                    .NextInstallmentNumber = CType(objread("NextInstallmentDueNumber"), Integer)
                    .ReschedulingNo = CType(objread("rescNo"), Integer)
                    .ProductDesc = CType(objread("productdesc"), String)
                    .ProductOfferingDesc = CType(objread("productOfferingDesc"), String)
                    .NumOfInstallment = CType(objread("numofInstallment"), Integer)
                    .ReschedulingFee = CType(objread("ReschedulingFee"), Double)
                    .ReschedulingFeeBehaviour = objread("ReschedulingFeeBehaviour").ToString
                    .OutstandingPrincipal = CType(objread("OutstandingPrincipal"), Double)
                    .OutStandingInterest = CType(objread("OutstandingInterest"), Double)
                    .RequestDate = CType(objread("RequestDate"), Date)
                    .NextDueDate = CType(objread("NextDueDate"), Date)
                    .ApplicationID = CType(objread("ApplicationID"), String)
                    .InvoiceSeqNo = CType(objread("InvoiceSeqNo"), String)
                    .BranchId = CType(objread("BranchID"), String)
                    .SeqNo = CType(objread("SeqNo"), String)
                    '.AdminFee = CType(objread("AdministrationFee"), String)

                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("GetListFactoringViewApproval", "GetListFactoringViewApproval", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetListInvoiceFactoringView(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@Applicationid", SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID
            params(1) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(1).Value = customclass.InsSeqNo
            params(2) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(2).Value = customclass.InvoiceSeqNo
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGetListInvoiceFactoringView", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("DChange", "GetListInvoiceFactoringView", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function GetListAmorFactoringView(ByVal customclass As Parameter.DChange) As Parameter.DChange
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@Applicationid", SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID

            params(1) = New SqlParameter("@RequestDate", SqlDbType.DateTime)
            params(1).Value = customclass.RequestDate

            params(2) = New SqlParameter("@effRate", SqlDbType.Decimal)
            params(2).Value = customclass.EffectiveRate

            params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(3).Value = customclass.InsSeqNo

            params(4) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(4).Value = customclass.InvoiceSeqNo

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spViewInvoiceFactoringView", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("DChange", "GetListAmor", exp.Message + exp.StackTrace)
        End Try

    End Function
End Class
