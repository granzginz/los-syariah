
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class OtherDisburseAgreementRelated
    Inherits AccMntBase
    Private Const spSaveReceiveH As String = "spOtherDisburseOtherAgreementRelatedSave"
    Private Const spSaveReceiveDet As String = "spOtherDisburseOtherAgreementRelatedDetailSave"

    Public Sub SaveOReceiveTrans(ByVal customclass As Parameter.OtherReceive)
        Dim params() As SqlParameter = New SqlParameter(14) {}
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Dim intLoopOmset As Integer

        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@bankaccountid", SqlDbType.VarChar, 10)
            params(1).Value = customclass.BankAccountID

            params(2) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 20)
            params(2).Value = customclass.LoginId

            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = customclass.BusinessDate

            params(4) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(4).Value = customclass.AmountReceive

            params(5) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(5).Value = customclass.ValueDate

            params(6) = New SqlParameter("@referenceNo", SqlDbType.VarChar, 20)
            params(6).Value = customclass.ReferenceNo

            params(7) = New SqlParameter("@Notes", SqlDbType.VarChar)
            params(7).Value = customclass.Notes


            params(8) = New SqlParameter("@CoyID", SqlDbType.VarChar, 3)
            params(8).Value = customclass.CoyID

            params(9) = New SqlParameter("@WOP", SqlDbType.Char, 2)
            params(9).Value = customclass.WOP

            params(10) = New SqlParameter("@ReceiveFrom", SqlDbType.VarChar, 20)
            params(10).Value = customclass.ReceivedFrom

            params(11) = New SqlParameter("@NumOfDetail", SqlDbType.Int)
            params(11).Value = customclass.NumTrans

            params(12) = New SqlParameter("@strID", SqlDbType.VarChar, 8000)
            params(12).Value = customclass.strId

            params(13) = New SqlParameter("@strDesc", SqlDbType.VarChar, 8000)
            params(13).Value = customclass.strDesc

            params(14) = New SqlParameter("@strAmount", SqlDbType.VarChar, 7000)
            params(14).Value = customclass.strAmount

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSaveReceiveH, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("OtherReceive", "SaveOReceiveTrans", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update Other Receive")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
End Class
