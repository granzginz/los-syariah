
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class OtherDisburse
    Inherits AccMntBase
    Private Const spSaveDisburse As String = "spOtherDisburse"

    Public Function SaveODisburseTrans(ByVal customclass As Parameter.OtherReceive, ByVal oData1 As DataTable) As String
        Dim params() As SqlParameter = New SqlParameter(20) {}
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction = Nothing

        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()

            transaction = objCon.BeginTransaction

            Dim params1() As SqlParameter = New SqlParameter(4) {}
            If customclass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = customclass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = customclass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = customclass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "K"
                SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                customclass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@bankaccountid", SqlDbType.VarChar, 10)
            params(1).Value = customclass.BankAccountID

            params(2) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 20)
            params(2).Value = customclass.LoginId

            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = customclass.BusinessDate

            params(4) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(4).Value = customclass.AmountReceive

            params(5) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(5).Value = customclass.ValueDate

            params(6) = New SqlParameter("@referenceNo", SqlDbType.VarChar, 20)
            params(6).Value = customclass.ReferenceNo

            params(7) = New SqlParameter("@Notes", SqlDbType.VarChar)
            params(7).Value = customclass.Notes


            params(8) = New SqlParameter("@CoyID", SqlDbType.VarChar, 3)
            params(8).Value = customclass.CoyID

            params(9) = New SqlParameter("@WOP", SqlDbType.Char, 2)
            params(9).Value = customclass.WOP

            params(10) = New SqlParameter("@ReceiveFrom", SqlDbType.VarChar, 20)
            params(10).Value = customclass.ReceivedFrom

            params(11) = New SqlParameter("@NumOfDetail", SqlDbType.Int)
            params(11).Value = customclass.NumTrans

            params(12) = New SqlParameter("@strID", SqlDbType.VarChar, 8000)
            params(12).Value = customclass.strId

            params(13) = New SqlParameter("@strDesc", SqlDbType.VarChar, 8000)
            params(13).Value = customclass.strDesc

            params(14) = New SqlParameter("@strAmount", SqlDbType.VarChar, 7000)
            params(14).Value = customclass.strAmount

            params(15) = New SqlParameter("@BGNo", SqlDbType.VarChar, 20)
            params(15).Value = customclass.BGNo

            params(16) = New SqlParameter("@BGDueDate", SqlDbType.DateTime)
            params(16).Value = customclass.BGDueDate

            params(17) = New SqlParameter("@strDepartmentID", SqlDbType.VarChar, 8000)
            params(17).Value = customclass.DepartementID

            params(18) = New SqlParameter("@BranchIDX", SqlDbType.VarChar, 3)
            params(18).Value = customclass.BranchIDX

            params(19) = New SqlParameter("@strisPotong", SqlDbType.VarChar, 8000)
            params(19).Value = customclass.strIsPotong

            params(20) = New SqlParameter("@vcrNo", SqlDbType.VarChar, 20)
            params(20).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSaveDisburse, params)
            transaction.Commit()
            Return params(20).Value
        Catch exp As Exception
            transaction.Rollback()
            WriteException("OtherDisburse", "SaveODisburseTrans", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update Other Disburse")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function

    Public Function OtherDisburseAdd(ByVal customclass As Parameter.OtherReceive, ByVal oData1 As DataTable) As Parameter.OtherReceive
        Dim params() As SqlParameter = New SqlParameter(21) {}
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction = Nothing

        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()

            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@bankaccountid", SqlDbType.VarChar, 10)
            params(1).Value = customclass.BankAccountID

            params(2) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 20)
            params(2).Value = customclass.LoginId

            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = customclass.BusinessDate

            params(4) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(4).Value = customclass.AmountReceive

            params(5) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(5).Value = customclass.ValueDate

            params(6) = New SqlParameter("@referenceNo", SqlDbType.VarChar, 20)
            params(6).Value = customclass.ReferenceNo

            params(7) = New SqlParameter("@Notes", SqlDbType.VarChar)
            params(7).Value = customclass.Notes

            params(8) = New SqlParameter("@CoyID", SqlDbType.VarChar, 3)
            params(8).Value = customclass.CoyID

            params(9) = New SqlParameter("@WOP", SqlDbType.Char, 2)
            params(9).Value = customclass.WOP

            params(10) = New SqlParameter("@ReceiveFrom", SqlDbType.VarChar, 20)
            params(10).Value = customclass.ReceivedFrom

            params(11) = New SqlParameter("@NumOfDetail", SqlDbType.Int)
            params(11).Value = customclass.NumTrans

            params(12) = New SqlParameter("@strID", SqlDbType.VarChar, 8000)
            params(12).Value = customclass.strId

            params(13) = New SqlParameter("@strDesc", SqlDbType.VarChar, 8000)
            params(13).Value = customclass.strDesc

            params(14) = New SqlParameter("@strAmount", SqlDbType.VarChar, 7000)
            params(14).Value = customclass.strAmount

            params(15) = New SqlParameter("@strDepartmentID", SqlDbType.VarChar, 8000)
            params(15).Value = customclass.DepartementID

            params(16) = New SqlParameter("@JenisTransfer", SqlDbType.VarChar, 4)
            params(16).Value = customclass.JenisTransfer

            params(17) = New SqlParameter("@BankID", SqlDbType.Char, 5)
            params(17).Value = customclass.BeneficiaryBankID

            params(18) = New SqlParameter("@BankBranchID", SqlDbType.SmallInt)
            params(18).Value = customclass.BeneficiaryBankBranchID

            params(19) = New SqlParameter("@AccountNoTo", SqlDbType.VarChar, 15)
            params(19).Value = customclass.BeneficiaryBankAccountNo

            params(20) = New SqlParameter("@AccountNameTo", SqlDbType.VarChar, 50)
            params(20).Value = customclass.BeneficiaryBankAccountName

            params(21) = New SqlParameter("@BranchIDX", SqlDbType.Char, 3)
            params(21).Value = customclass.BranchIDX

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spOtherDisburseAdd", params)
            transaction.Commit()

            customclass.ErrorMessage = ""
        Catch ex As Exception
            transaction.Rollback()
            customclass.ErrorMessage = ex.Message
        Finally            
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try

        Return customclass
    End Function
End Class
