


#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports System.DBNull
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class TransferFundReconcile
    Inherits AccMntBase

    Public Function ListTransfer(ByVal customclass As Parameter.TransferAccount) As Parameter.TransferAccount
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.Listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spTransferFundReconPaging", params).Tables(0)
            ' customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("TransferFundReconcile", "ListTransfer", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function SaveTransfer(ByVal customclass As Parameter.TransferAccount) As Parameter.TransferAccount
        Dim params() As SqlParameter = New SqlParameter(10) {}
        Dim params1() As SqlParameter = New SqlParameter(4) {}
        Dim intLoopOmset As Integer
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            If customclass.referenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = customclass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = customclass.bankAccountFrom
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = customclass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"
                SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                customclass.referenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(2) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
            params(3) = New SqlParameter("@BankAccountIDFromTo", SqlDbType.VarChar, 10)
            params(4) = New SqlParameter("@NumOfDetail", SqlDbType.Int)
            params(5) = New SqlParameter("@TransferNo", SqlDbType.VarChar, 20)
            params(6) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
            params(7) = New SqlParameter("@AmountTransfer", SqlDbType.Decimal)
            params(8) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
            params(9) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(10) = New SqlParameter("@Coyid", SqlDbType.VarChar, 3)

            If CheckDupReconcile(customclass) Then
                customclass.strError = "Transfer already Reconcile By Another User"
                Throw New Exception("Transfer Already Reconcile By Another User")
            Else
                params(0).Value = customclass.BranchId
                params(1).Value = CDate(customclass.BusinessDate)
                params(2).Value = customclass.LoginId
                params(3).Value = customclass.bankAccountFrom
                params(4).Value = customclass.TotalRecord
                params(5).Value = customclass.TransferNO
                params(6).Value = customclass.Desc
                params(7).Value = customclass.AmountTransfer
                params(8).Value = customclass.referenceNo
                params(9).Value = customclass.valueDate
                params(10).Value = customclass.CoyID
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spTransferFundReconcileSave", params)
            End If
            transaction.Commit()
            Return customclass
        Catch exp As Exception
            transaction.Rollback()
            WriteException("TransferFundReconcile", "SaveTransfer", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update Transfer Fund Reconcile")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function

    Public Function CheckDupReconcile(ByVal customclass As Parameter.TransferAccount) As Boolean
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@TransferNo", SqlDbType.VarChar, 8000)
            params(1).Value = customclass.TransferNO

            params(2) = New SqlParameter("@ISChange", SqlDbType.Bit)
            params(2).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spCheckDuplikasiTFRecon", params)
            Return CBool(params(2).Value)
        Catch exp As Exception
            WriteException("TransferFundReconcile", "CheckDupReconcile", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Check Transfer Fund Reconcile")
        End Try
    End Function

End Class
