
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter

#End Region
Public Class FullPrepayInquiry : Inherits maxiloan.SQLEngine.LoanMnt.AccMntBase
    Private Const PARAM_PREPAYMENTTYPE As String = "@PrepaymentType"
    Private Const PARAM_PREPAYMENTAMOUNT As String = "@PrepaymentAmount"
    Private Const PARAM_WOPENALTY As String = "@PrepaymentFeeDisc"
    Private Const PARAM_REQUESTBY As String = "@requestby"
    Private Const PARAM_APPROVALNO As String = "@ApprovalNo"
    Private Const PARAM_PREPAYMENTNO As String = "@PrepaymentNo"
    Private Const PARAM_INSURANCETERMINATIONFLAG As String = "@InsuranceTerminationFlag"
    Private Const PRINT_TRIAL_CALCULATION As String = "spPrepaymentPrintTrial"
    Private Const PRINT_TRIAL_CALCULATION_OL As String = "spPrepaymentPrintTrialOL"

    Private Const VIEWPREPAYMENT As String = "spPrepaymentView"
    Private Const spInqPrepayment As String = "spInqPrepayment"

#Region "View"
    Public Function FullPrepayView(ByVal customClass As Parameter.FullPrepay) As Parameter.FullPrepay
        Dim strApprovalNo As String
        Dim TotalDiscount As Double
        Dim objRead As SqlDataReader
        Try
            Dim params() As SqlParameter = New SqlParameter(1) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter(PARAM_PREPAYMENTNO, SqlDbType.VarChar, 20)
            params(1).Value = customClass.PrepaymentNo.Replace("'", "")

            objRead = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, VIEWPREPAYMENT, params)
            If objRead.Read Then
                With customClass
                    .Agreementno = objRead("AgreementNo").ToString
                    .ApplicationID = objRead("ApplicationID").ToString
                    .CustomerID = objRead("CustomerID").ToString
                    .CustomerName = objRead("CustomerName").ToString
                    .CustomerType = objRead("CustomerType").ToString
                    .CrossDefault = CBool(objRead("CrossDefault"))
                    .DefaultStatus = objRead("DefaultStatus").ToString
                    .ContractStatus = objRead("ContractStatus").ToString
                    .ValueDate = CDate(objRead("EfectiveDate"))
                    'modify ario 26-10-2018
                    .PaidDate = CDate(objRead("paiddate"))
                    .BusinessDate = CDate(objRead("RequestDate"))
                    .PrepaymentType = objRead("PrepaymentType").ToString
                    .AmountToBePaid = CDbl(objRead("TotalAmountToBePaid"))
                    .PrepaymentAmount = CDbl(objRead("PrepaymentAmount"))
                    .TerminationPenalty = CDbl(objRead("PrepaymentFee"))

                    .OutstandingPrincipal = CDbl(objRead("OutstandingPrincipal"))
                    .OutStandingInterest = CDbl(objRead("OutstandingInterest"))

                    .InstallmentDue = CDbl(objRead("OSInstallmentDue"))
                    .InsuranceDue = CDbl(objRead("OSInsuranceDue"))
                    .LcInstallment = CDbl(objRead("OSLCInstallment")) + CDbl(objRead("LCInstallmentCurrent"))
                    .LcInsurance = CDbl(objRead("OSLCInsurance")) + CDbl(objRead("LCInsuranceCurrent"))
                    .InstallmentCollFee = CDbl(objRead("OSInstallCollectionFee"))
                    .InsuranceCollFee = CDbl(objRead("OSInsurCollectionFee"))
                    .PDCBounceFee = CDbl(objRead("OSPDCBounceFee"))
                    .STNKRenewalFee = CDbl(objRead("OSSTNKRenewalFee"))
                    .RepossessionFee = CDbl(objRead("OSRepossesFee"))
                    .InsuranceClaimExpense = CDbl(objRead("OSInsuranceClaimExpense"))

                    .AccruedInterest = CDbl(objRead("AccruedInterest"))

                    .InstallmentWaived = CDbl(objRead("InstallmentAmountDisc"))
                    .InsuranceDueWaived = CDbl(objRead("InsuranceAmountDisc"))

                    .LcInsuranceWaived = CDbl(objRead("LCInsuranceAmountDisc"))
                    .LcInstallmentWaived = CDbl(objRead("LCInstallmentAmountDisc"))

                    .InstallmentCollFeeWaived = CDbl(objRead("InstallCollectionFeeDisc"))
                    .InsuranceCollFeeWaived = CDbl(objRead("InsurCollectionFeeDisc"))

                    .RepossessionFeeWaived = CDbl(objRead("RepossesFeeDisc"))
                    .PDCBounceFeeWaived = CDbl(objRead("PDCBounceFeeDisc"))
                    .STNKRenewalFeeWaived = CDbl(objRead("STNKFeeDisc"))
                    .InsuranceClaimExpenseWaived = CDbl(objRead("InsuranceClaimExpenseDisc"))
                    .TerminationPenaltyWaived = CDbl(objRead("PrepaymentFeeDisc"))

                    .Prepaid = CDbl(objRead("PrepaidAmount"))

                    .PrepaymentNotes = objRead("Notes").ToString
                    .ReasonTypeID = objRead("ReasonTypeID").ToString
                    .ReasonDescription = objRead("Description").ToString
                    .InsurancePaidBy = objRead("InsurancePaidBy").ToString
                    .InsuranceTerminationFlag = CStr(objRead("InsuranceTerminationFlag"))
                    .InsurancePaidBy = CStr(objRead("InsurancePaidBy"))
                    .UserApproval = CStr(objRead("UserApproval"))
                    .UserRequest = CStr(objRead("UserRequest"))
                    .RequestDate = CDate(objRead("RequestDate"))
                    .PrepaymentStatus = CStr(objRead("PrepaymentStatus"))
                    .StatusDate = CDate(objRead("StatusDate"))
                    .ApplicationID = CStr(objRead("ApplicationId"))
                    Try
                        .FundingCoyName = CType(objRead("FundingCoyName"), String)
                        .FundingPledgeStatus = CType(objRead("FundingPledgeStatus"), String)
                        .ResiduValue = CType(objRead("ResiduValue"), Double)

                        .NextInstallmentDate = CType(objRead("NextInstallmentDueDate"), Date)
                        .InstallmentAmount = CType(objRead("InstallmentAmount"), Double)
                        .NextInstallmentNumber = CType(objRead("NextInstallmentDueNumber"), Integer)


                    Catch ex As Exception
                        ex.Message.ToString()
                    End Try
                End With
            End If
            objRead.Close()
            Return customClass
        Catch exp As Exception
            WriteException("FullPrepayView", "FullPrepayView", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region
#Region "InqPrepayment"
    Public Function InqPrepayment(ByVal customclass As Parameter.FullPrepay) As Parameter.FullPrepay
        Dim params() As SqlParameter = New SqlParameter(4) {}
        With customclass
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = .SortBy
            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output
        End With
        Try
            customclass.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInqPrepayment, params).Tables(0)
            customclass.TotalRecord = CType(params(4).Value, Int16)
            Return customclass
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")
        Catch exp As Exception
            WriteException("FullPrepayView", "InqPrepayment", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "Prepayment Print Trial"
    Public Function PrintTrial(ByVal ocustomclass As Parameter.FullPrepay) As System.Data.DataSet
        Dim params() As SqlParameter = New SqlParameter(4) {}
        With ocustomclass
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = .BranchId
            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = .ApplicationID
            params(2) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(2).Value = .BusinessDate
            params(3) = New SqlParameter(PARAM_PREPAYMENTTYPE, SqlDbType.VarChar, 2)
            params(3).Value = .PrepaymentType
            params(4) = New SqlParameter("@TotalOthersReceivable", SqlDbType.Decimal)
            params(4).Value = .totalDiscount
        End With
        Try
            Return SqlHelper.ExecuteDataset(ocustomclass.strConnection, CommandType.StoredProcedure, PRINT_TRIAL_CALCULATION, params)
        Catch exp As Exception
            WriteException("FullPrepayView", "PrintTrial", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function PrintTrialOL(ByVal ocustomclass As Parameter.FullPrepay) As System.Data.DataSet
        Dim params() As SqlParameter = New SqlParameter(3) {}
        With ocustomclass
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = .BranchId
            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = .ApplicationID
            params(2) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(2).Value = .BusinessDate
            params(3) = New SqlParameter("@TotalOthersReceivable", SqlDbType.Decimal)
            params(3).Value = .totalDiscount
        End With
        Try
            Return SqlHelper.ExecuteDataset(ocustomclass.strConnection, CommandType.StoredProcedure, PRINT_TRIAL_CALCULATION_OL, params)
        Catch exp As Exception
            WriteException("FullPrepayView", "PrintTrial", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region
End Class
