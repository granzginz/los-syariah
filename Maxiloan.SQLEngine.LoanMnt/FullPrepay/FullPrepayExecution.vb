
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class FullPrepayExecution : Inherits maxiloan.SQLEngine.LoanMnt.AccMntBase
    Private Const PARAM_PREPAYMENTTYPE As String = "@PrepaymentType"
    Private Const PARAM_PREPAYMENTAMOUNT As String = "@PrepaymentAmount"
    Private Const PARAM_WOPENALTY As String = "@PrepaymentFeeDisc"
    Private Const PARAM_REQUESTBY As String = "@requestby"
    Private Const PARAM_APPROVALNO As String = "@ApprovalNo"
    Private Const PARAM_PREPAYMENTNO As String = "@PrepaymentNo"
    Private Const PARAM_INSURANCETERMINATIONFLAG As String = "@InsuranceTerminationFlag"
    Private Const CANCELPREPAYMENT As String = "spPrepayApprovalCancel"
    Private Const EXECUTEPREPAYMENT As String = "spProcessFullPrepayExecute"
    Private Const EXECUTEPREPAYMENTOL As String = "spProcessFullPrepayExecuteOL"

#Region "Cancel"
    Public Sub SaveFullPrepayCancel(ByVal customClass As Parameter.FullPrepay)

        Dim oConnection As New SqlConnection(customClass.strConnection)
        Dim oApprovalID As New maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim objTransaction As SqlTransaction
        Dim strApprovalNo As String
        Dim TotalDiscount As Double
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            objTransaction = oConnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(3) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = customClass.CoyID

            params(2) = New SqlParameter(PARAM_PREPAYMENTNO, SqlDbType.VarChar, 20)
            params(2).Value = customClass.PrepaymentNo.Trim

            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = customClass.BusinessDate
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, CANCELPREPAYMENT, params)
            objTransaction.Commit()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        Catch exp As Exception
            objTransaction.Rollback()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
            WriteException("FullPrepayExecution", "SaveFullPrepayCancel", exp.Message + exp.StackTrace)
            WriteException(exp.Message)
            Throw New Exception(exp.Message)
        End Try
    End Sub
#End Region

#Region "Execute"
    Public Sub SaveFullPrepayExecute(ByVal customClass As Parameter.FullPrepay)

        Dim oConnection As New SqlConnection(customClass.strConnection)
        Dim oApprovalID As New maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim objTransaction As SqlTransaction
        Dim strApprovalNo As String
        Dim TotalDiscount As Double
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            objTransaction = oConnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(3) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = customClass.CoyID

            params(2) = New SqlParameter(PARAM_PREPAYMENTNO, SqlDbType.VarChar, 20)
            params(2).Value = customClass.PrepaymentNo.Trim

            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = customClass.BusinessDate
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, EXECUTEPREPAYMENT, params)
            objTransaction.Commit()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        Catch exp As Exception
            objTransaction.Rollback()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
            'WriteException("FullPrepayExecution", "SaveFullPrepayCancel", exp.Message + exp.StackTrace)
            'WriteException(exp.Message)
            Throw New Exception(exp.Message)
        End Try
    End Sub

    Public Sub SaveFullPrepayExecuteOL(ByVal customClass As Parameter.FullPrepay)

        Dim oConnection As New SqlConnection(customClass.strConnection)
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim objTransaction As SqlTransaction
        Dim strApprovalNo As String
        Dim TotalDiscount As Double
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            objTransaction = oConnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(3) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = customClass.CoyID

            params(2) = New SqlParameter(PARAM_PREPAYMENTNO, SqlDbType.VarChar, 20)
            params(2).Value = customClass.PrepaymentNo.Trim

            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = customClass.BusinessDate
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, EXECUTEPREPAYMENTOL, params)
            objTransaction.Commit()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        Catch exp As Exception
            objTransaction.Rollback()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
            'WriteException("FullPrepayExecution", "SaveFullPrepayCancel", exp.Message + exp.StackTrace)
            'WriteException(exp.Message)
            Throw New Exception(exp.Message)
        End Try
    End Sub
#End Region
End Class
