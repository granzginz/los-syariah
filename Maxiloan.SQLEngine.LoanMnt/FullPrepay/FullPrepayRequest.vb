
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class FullPrepayRequest : Inherits maxiloan.SQLEngine.LoanMnt.AccMntBase
    Private Const PARAM_PREPAYMENTTYPE As String = "@PrepaymentType"
    Private Const PARAM_PREPAYMENTAMOUNT As String = "@PrepaymentAmount"
    Private Const PARAM_WOPENALTY As String = "@PrepaymentFeeDisc"
    Private Const PARAM_REQUESTBY As String = "@requestby"
    Private Const PARAM_APPROVALNO As String = "@ApprovalNo"
    Private Const PARAM_INSURANCETERMINATIONFLAG As String = "@InsuranceTerminationFlag"
    Private Const REQUESTPREPAYMENT As String = "spProcessFullPrepayRequest"
    Private Const REQUESTPREPAYMENTOL As String = "spProcessFullPrepayRequestOL"

#Region "Request"
    Public Sub SaveFullPrepayRequest(ByVal customClass As Parameter.FullPrepay)

        Dim oConnection As New SqlConnection(customClass.strConnection)
        Dim oApprovalID As New maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim objTransaction As SqlTransaction
        Dim strApprovalNo As String
        Dim TotalDiscount As Double
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            objTransaction = oConnection.BeginTransaction
            With customClass
                TotalDiscount = .InstallmentWaived + .InsuranceDueWaived + .LcInsuranceWaived + .LcInstallmentWaived + _
                .PDCBounceFeeWaived + .STNKRenewalFeeWaived + .RepossessionFeeWaived + .InsuranceClaimExpenseWaived + _
                .InsuranceCollFeeWaived + .InstallmentCollFeeWaived + .TerminationPenaltyWaived
            End With
            With oEntitiesApproval
                .ApprovalTransaction = objTransaction
                .BranchId = customClass.BranchId
                .SchemeID = "FPAY"
                .RequestDate = customClass.BusinessDate
                .TransactionNo = customClass.ApplicationID
                .ApprovalValue = TotalDiscount
                .UserRequest = customClass.LoginId
                .UserApproval = customClass.RequestTo
                .ApprovalNote = customClass.PrepaymentNotes
                .AprovalType = Parameter.Approval.ETransactionType.Prepayment_Approval
            End With
            strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

            Dim params() As SqlParameter = New SqlParameter(23) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = customClass.ApplicationID.Trim

            params(2) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(2).Value = customClass.ValueDate
            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = customClass.BusinessDate

            params(4) = New SqlParameter(PARAM_PREPAYMENTTYPE, SqlDbType.VarChar, 2)
            params(4).Value = customClass.PrepaymentType

            params(5) = New SqlParameter(PARAM_INSTALLMENTDUEWAIVED, SqlDbType.Decimal)
            params(5).Value = customClass.InstallmentWaived

            params(6) = New SqlParameter(PARAM_INSURANCEDUEWAIVED, SqlDbType.Decimal)
            params(6).Value = customClass.InsuranceDueWaived

            params(7) = New SqlParameter(PARAM_LCINSTALLMENTWAIVED, SqlDbType.Decimal)
            params(7).Value = customClass.LcInstallmentWaived

            params(8) = New SqlParameter(PARAM_LCINSURANCEWAIVED, SqlDbType.Decimal)
            params(8).Value = customClass.LcInsuranceWaived

            params(9) = New SqlParameter(PARAM_INSTALLMENTCOLLWAIVED, SqlDbType.Decimal)
            params(9).Value = customClass.InstallmentCollFeeWaived

            params(10) = New SqlParameter(PARAM_INSURANCECOLLWAIVED, SqlDbType.Decimal)
            params(10).Value = customClass.InsuranceCollFeeWaived

            params(11) = New SqlParameter(PARAM_PDCBOUNCEFEEWAIVED, SqlDbType.Decimal)
            params(11).Value = customClass.PDCBounceFeeWaived

            params(12) = New SqlParameter(PARAM_STNKRENEEWALFEEWAIVED, SqlDbType.Decimal)
            params(12).Value = customClass.STNKRenewalFeeWaived

            params(13) = New SqlParameter(PARAM_REPOSSESSIONFEEWAIVED, SqlDbType.Decimal)
            params(13).Value = customClass.RepossessionFeeWaived

            params(14) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEWAIVED, SqlDbType.Decimal)
            params(14).Value = customClass.InsuranceClaimExpenseWaived

            params(15) = New SqlParameter(PARAM_TERMINATIONPENALTYWAIVED, SqlDbType.Decimal)
            params(15).Value = customClass.TerminationPenaltyWaived

            params(16) = New SqlParameter(PARAM_REASONID, SqlDbType.VarChar, 10)
            params(16).Value = customClass.ReasonID

            params(17) = New SqlParameter(PARAM_REQUESTBY, SqlDbType.VarChar, 12)
            params(17).Value = customClass.LoginId

            params(18) = New SqlParameter(PARAM_APPROVALNO, SqlDbType.VarChar, 50)
            params(18).Value = strApprovalNo

            params(19) = New SqlParameter(PARAM_NOTES, SqlDbType.Text)
            params(19).Value = customClass.PrepaymentNotes

            params(20) = New SqlParameter(PARAM_INSURANCETERMINATIONFLAG, SqlDbType.Char, 1)
            params(20).Value = customClass.InsuranceTerminationFlag

            params(21) = New SqlParameter("@PrepaymentAmount", SqlDbType.Decimal)
            params(21).Value = customClass.PrepaymentAmount

            params(22) = New SqlParameter("@TotalDiscount", SqlDbType.Decimal)
            params(22).Value = TotalDiscount

            params(23) = New SqlParameter("@TerminationPenalty", SqlDbType.Decimal)
            params(23).Value = customClass.TerminationPenalty


            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, REQUESTPREPAYMENT, params)
            objTransaction.Commit()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        Catch exp As Exception
            objTransaction.Rollback()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
            WriteException("FullPrepayRequest", "SaveFullPrepayRequest", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Sub


    Public Sub SaveFullPrepayRequestOL(ByVal customClass As Parameter.FullPrepay)

        Dim oConnection As New SqlConnection(customClass.strConnection)
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim objTransaction As SqlTransaction
        Dim strApprovalNo As String
        Dim TotalDiscount As Double
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            objTransaction = oConnection.BeginTransaction
            With customClass
                TotalDiscount = .InstallmentWaived + .InsuranceDueWaived + .LcInsuranceWaived + .LcInstallmentWaived + _
                .PDCBounceFeeWaived + .STNKRenewalFeeWaived + .RepossessionFeeWaived + .InsuranceClaimExpenseWaived + _
                .InsuranceCollFeeWaived + .InstallmentCollFeeWaived + .TerminationPenaltyWaived
            End With
            With oEntitiesApproval
                .ApprovalTransaction = objTransaction
                .BranchId = customClass.BranchId
                .SchemeID = "FPOL"
                .RequestDate = customClass.BusinessDate
                .TransactionNo = customClass.ApplicationID
                .ApprovalValue = TotalDiscount
                .UserRequest = customClass.LoginId
                .UserApproval = customClass.RequestTo
                .ApprovalNote = customClass.PrepaymentNotes
                .AprovalType = Parameter.Approval.ETransactionType.Prepayment_Approval
            End With
            strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

            Dim params() As SqlParameter = New SqlParameter(23) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = customClass.ApplicationID.Trim

            params(2) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(2).Value = customClass.ValueDate
            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = customClass.BusinessDate

            params(4) = New SqlParameter(PARAM_PREPAYMENTTYPE, SqlDbType.VarChar, 2)
            params(4).Value = customClass.PrepaymentType

            params(5) = New SqlParameter(PARAM_INSTALLMENTDUEWAIVED, SqlDbType.Decimal)
            params(5).Value = customClass.InstallmentWaived

            params(6) = New SqlParameter(PARAM_INSURANCEDUEWAIVED, SqlDbType.Decimal)
            params(6).Value = customClass.InsuranceDueWaived

            params(7) = New SqlParameter(PARAM_LCINSTALLMENTWAIVED, SqlDbType.Decimal)
            params(7).Value = customClass.LcInstallmentWaived

            params(8) = New SqlParameter(PARAM_LCINSURANCEWAIVED, SqlDbType.Decimal)
            params(8).Value = customClass.LcInsuranceWaived

            params(9) = New SqlParameter(PARAM_INSTALLMENTCOLLWAIVED, SqlDbType.Decimal)
            params(9).Value = customClass.InstallmentCollFeeWaived

            params(10) = New SqlParameter(PARAM_INSURANCECOLLWAIVED, SqlDbType.Decimal)
            params(10).Value = customClass.InsuranceCollFeeWaived

            params(11) = New SqlParameter(PARAM_PDCBOUNCEFEEWAIVED, SqlDbType.Decimal)
            params(11).Value = customClass.PDCBounceFeeWaived

            params(12) = New SqlParameter(PARAM_STNKRENEEWALFEEWAIVED, SqlDbType.Decimal)
            params(12).Value = customClass.STNKRenewalFeeWaived

            params(13) = New SqlParameter(PARAM_REPOSSESSIONFEEWAIVED, SqlDbType.Decimal)
            params(13).Value = customClass.RepossessionFeeWaived

            params(14) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEWAIVED, SqlDbType.Decimal)
            params(14).Value = customClass.InsuranceClaimExpenseWaived

            params(15) = New SqlParameter(PARAM_TERMINATIONPENALTYWAIVED, SqlDbType.Decimal)
            params(15).Value = customClass.TerminationPenaltyWaived

            params(16) = New SqlParameter(PARAM_REASONID, SqlDbType.VarChar, 10)
            params(16).Value = customClass.ReasonID

            params(17) = New SqlParameter(PARAM_REQUESTBY, SqlDbType.VarChar, 12)
            params(17).Value = customClass.LoginId

            params(18) = New SqlParameter(PARAM_APPROVALNO, SqlDbType.VarChar, 50)
            params(18).Value = strApprovalNo

            params(19) = New SqlParameter(PARAM_NOTES, SqlDbType.Text)
            params(19).Value = customClass.PrepaymentNotes

            params(20) = New SqlParameter(PARAM_INSURANCETERMINATIONFLAG, SqlDbType.Char, 1)
            params(20).Value = customClass.InsuranceTerminationFlag

            params(21) = New SqlParameter("@PrepaymentAmount", SqlDbType.Decimal)
            params(21).Value = customClass.PrepaymentAmount

            params(22) = New SqlParameter("@TotalDiscount", SqlDbType.Decimal)
            params(22).Value = TotalDiscount

            params(23) = New SqlParameter("@TerminationPenalty", SqlDbType.Decimal)
            params(23).Value = customClass.TerminationPenalty


            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, REQUESTPREPAYMENT, params)
            objTransaction.Commit()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        Catch exp As Exception
            objTransaction.Rollback()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
            WriteException("FullPrepayRequest", "SaveFullPrepayRequest", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Sub
#End Region
End Class
