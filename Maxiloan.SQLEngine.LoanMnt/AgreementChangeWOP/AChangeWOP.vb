

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class AChangeWOP
    Inherits AccMntBase

    Private Const spAgree As String = "spAgreementList"
    Private Const spSaveWOP As String = "spWOPChange"

#Region "GetListWOP"
    Public Function GetListWOP(ByVal customclass As Parameter.AChangeWOP) As Parameter.AChangeWOP
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 20)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spAgree, params)
            With customclass
                If objread.Read Then
                    .Agreementno = CType(objread("AgreementNo"), String)
                    .CustomerName = CType(objread("name"), String)
                    .NextInstallmentDueDate = CType(objread("NextInstallmentDueDate"), Date)
                    .NextInstallmentNumber = CType(objread("NextInstallmentDueNumber"), Integer)
                    .InstallmentAmount = CType(objread("InstallmentAmount"), Double)
                    .WOP = CType(objread("WayOfPayment"), String)
                    .WOPDesc = CType(objread("WOPDesc"), String)
                    .AdminFee = CType(objread("gsvalue"), Double)
                    .Prepaid = CType(objread("contractprepaidamount"), Double)
                    .CustomerID = CType(objread("CustomerID"), String)
                    .BranchName = CType(objread("branchfullname"), String)
                End If
                objread.Close()
            End With
            Return customclass
        Catch exp As Exception
            WriteException("AgreementTransfer", "GetListWOP", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "WOPChange"
    Public Sub WOPChange(ByVal customClass As Parameter.AChangeWOP)
        Dim oConnection As New SqlConnection(customClass.strConnection)
        Dim objTransaction As SqlTransaction
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        objTransaction = oConnection.BeginTransaction
        Dim params() As SqlParameter = New SqlParameter(11) {}
        Dim intLoopOmset As Integer
        Dim i As Integer = 0

        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = customClass.BranchId

        params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
        params(1).Value = customClass.ApplicationID

        params(2) = New SqlParameter(PARAM_AGREEMENTNO, SqlDbType.VarChar, 20)
        params(2).Value = customClass.Agreementno

        params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
        params(3).Value = customClass.BusinessDate

        params(4) = New SqlParameter("@WayOfpayment", SqlDbType.VarChar, 3)
        params(4).Value = customClass.WOP

        params(5) = New SqlParameter("@WOPToBe", SqlDbType.VarChar, 3)
        params(5).Value = customClass.WOPToBe

        params(6) = New SqlParameter("@Notes", SqlDbType.VarChar, 100)
        params(6).Value = customClass.Notes

        params(7) = New SqlParameter("@NextInstallmentDueNumber", SqlDbType.Int)
        params(7).Value = customClass.NextInstallmentNumber

        params(8) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
        params(8).Direction = ParameterDirection.Output

        params(9) = New SqlParameter("@UserCreate", SqlDbType.VarChar, 20)
        params(9).Value = customClass.LoginId

        params(10) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
        params(10).Value = customClass.AdminFee

        params(11) = New SqlParameter("@CompanyID", SqlDbType.VarChar, 3)
        params(11).Value = customClass.CoyID
        Try
            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spSaveWOP, params)
            objTransaction.Commit()
        Catch exp As Exception
            WriteException("AgreementChangeWOP", "WOPChange", exp.Message + exp.StackTrace)
            objTransaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Sub
#End Region
End Class
