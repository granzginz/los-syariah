

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class PaymentReversal : Inherits Maxiloan.SQLEngine.LoanMnt.AccMntBase
    Private Const SPLISTPAYMENTREVERSAL As String = "spPaymentReversal"
    Private Const SPLISTPAYMENTREVERSALOTOR As String = "spPaymentReversalOtor"
    Private Const SPLISTPAYMENTREVERSALOL As String = "spPaymentReversalOL"
    'Private Const POSTINGPAYMENTREVERSAL As String = "spPostingPaymentReversal"
    Private Const POSTINGPAYMENTREVERSAL As String = "spBeforePostingPaymentReversal"
    'Private Const POSTINGPAYMENTREVERSALOTOR As String = "spPostingPaymentReversalOtor"
    Private Const POSTINGPAYMENTREVERSALOTOR As String = "spPostingPaymentReversalOtor"
    Private Const POSTINGREVERSALOTORFACTORING As String = "spPostingReversalOtorFactoring"
    Private Const POSTINGREVERSALOTORMODALKERJA As String = "spPostingReversalOtorModalKerja"
    Private Const POSTINGPAYMENTREVERSALOL As String = "spPostingPaymentReversalOL"
    Private Const SPLISTREVERSALLOG As String = "spReversalLogList"
    Private Const PARAM_BRANCHRECEIVEID As String = "@BranchReceiveID"
    Private Const PARAM_HISTORYSEQUENCENO As String = "@HistorySequenceNo"
    Private Const PARAM_REASONREVERSAL As String = "@ReasonReversal"
    Public Function PaymentReversalList(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.ApplicationID

            params(1) = New SqlParameter(PARAM_BRANCHRECEIVEID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.BranchReceivedID

            params(2) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(2).Value = oCustomClass.CurrentPage

            params(3) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(3).Value = oCustomClass.PageSize

            params(4) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(4).Value = oCustomClass.SortBy

            params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output

            oCustomClass.ListReversalPayment = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, SPLISTPAYMENTREVERSAL, params).Tables(0)
            oCustomClass.TotalRecord = CInt(params(5).Value)

            Return oCustomClass
        Catch exp As Exception
            WriteException("PaymentReversal", "PaymentReversalList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function PaymentReversalListOtor(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.ApplicationID

            params(1) = New SqlParameter(PARAM_BRANCHRECEIVEID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.BranchReceivedID

            params(2) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(2).Value = oCustomClass.CurrentPage

            params(3) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(3).Value = oCustomClass.PageSize

            params(4) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(4).Value = oCustomClass.SortBy

            params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output

            oCustomClass.ListReversalPayment = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, SPLISTPAYMENTREVERSALOTOR, params).Tables(0)
            oCustomClass.TotalRecord = CInt(params(5).Value)

            Return oCustomClass
        Catch exp As Exception
            WriteException("PaymentReversal", "PaymentReversalList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function PaymentReversalListOL(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.ApplicationID

            params(1) = New SqlParameter(PARAM_BRANCHRECEIVEID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.BranchReceivedID

            params(2) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(2).Value = oCustomClass.CurrentPage

            params(3) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(3).Value = oCustomClass.PageSize

            params(4) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(4).Value = oCustomClass.SortBy

            params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output

            oCustomClass.ListReversalPayment = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, SPLISTPAYMENTREVERSALOL, params).Tables(0)
            oCustomClass.TotalRecord = CInt(params(5).Value)

            Return oCustomClass
        Catch exp As Exception
            WriteException("PaymentReversal", "PaymentReversalList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub ProcessPaymentReversal(ByVal oCustomClass As Parameter.PaymentReversal)
        Dim objtrans As SqlTransaction
        Dim objcon As New SqlConnection(oCustomClass.strConnection)

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(6) {}
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchReceivedID

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter(PARAM_HISTORYSEQUENCENO, SqlDbType.Int)
            params(2).Value = oCustomClass.HistorySequenceNo

            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.BusinessDate

            params(4) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 20)
            params(4).Value = oCustomClass.ReferenceNo

            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId

            params(6) = New SqlParameter(PARAM_REASONREVERSAL, SqlDbType.VarChar, 2)
            params(6).Value = oCustomClass.ReasonReversal

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, POSTINGPAYMENTREVERSAL, params)

            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("PaymentReversal", "ProcessPaymentReversal", exp.Message + exp.StackTrace)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub

    Public Sub ProcessPaymentReversalOtor(ByVal oCustomClass As Parameter.PaymentReversal)
        Dim objtrans As SqlTransaction
        Dim objcon As New SqlConnection(oCustomClass.strConnection)

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(4) {}
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchReceivedID

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter(PARAM_HISTORYSEQUENCENO, SqlDbType.Int)
            params(2).Value = oCustomClass.HistorySequenceNo

            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.BusinessDate

            'params(4) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 20)
            'params(4).Value = oCustomClass.ReferenceNo

            params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(4).Value = oCustomClass.LoginId

            'params(6) = New SqlParameter(PARAM_REASONREVERSAL, SqlDbType.VarChar, 2)
            'params(6).Value = oCustomClass.ReasonReversal

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, POSTINGPAYMENTREVERSALOTOR, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("PaymentReversal", "ProcessPaymentReversal", exp.Message + exp.StackTrace)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub

    Public Sub ProcessReversalOtorFactoring(ByVal oCustomClass As Parameter.PaymentReversal)
        Dim objtrans As SqlTransaction
        Dim objcon As New SqlConnection(oCustomClass.strConnection)

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(8) {}
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchReceivedID

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter("@HistorySequenceNo", SqlDbType.Int)
            params(2).Value = oCustomClass.HistorySequenceNo

            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = oCustomClass.BusinessDate

            params(4) = New SqlParameter("@LoginID", SqlDbType.VarChar, 12)
            params(4).Value = oCustomClass.LoginId

            params(5) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
            params(5).Value = oCustomClass.ReferenceNo

            params(6) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 12)
            params(6).Value = oCustomClass.InvoiceNo

            params(7) = New SqlParameter("@InvoiceSeqNo", SqlDbType.VarChar, 2)
            params(7).Value = oCustomClass.InvoiceSeqNo

            params(8) = New SqlParameter("@ReasonReversal", SqlDbType.VarChar, 2)
            params(8).Value = oCustomClass.ReasonReversal

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, POSTINGREVERSALOTORFACTORING, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("PaymentReversal", "ProcessPaymentReversal", exp.Message + exp.StackTrace)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub

    Public Sub ProcessReversalOtorModalKerja(ByVal oCustomClass As Parameter.PaymentReversal)
        Dim objtrans As SqlTransaction
        Dim objcon As New SqlConnection(oCustomClass.strConnection)

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(8) {}
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchReceivedID

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter("@HistorySequenceNo", SqlDbType.Int)
            params(2).Value = oCustomClass.HistorySequenceNo

            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = oCustomClass.BusinessDate

            params(4) = New SqlParameter("@LoginID", SqlDbType.VarChar, 12)
            params(4).Value = oCustomClass.LoginId

            params(5) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
            params(5).Value = oCustomClass.ReferenceNo

            params(6) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 12)
            params(6).Value = oCustomClass.InvoiceNo

            params(7) = New SqlParameter("@InvoiceSeqNo", SqlDbType.VarChar, 2)
            params(7).Value = oCustomClass.InvoiceSeqNo

            params(8) = New SqlParameter("@ReasonReversal", SqlDbType.VarChar, 2)
            params(8).Value = oCustomClass.ReasonReversal

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, POSTINGREVERSALOTORMODALKERJA, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("PaymentReversal", "ProcessPaymentReversal", exp.Message + exp.StackTrace)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
    Public Function ReversalLogList(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = oCustomClass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = oCustomClass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = oCustomClass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = oCustomClass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, SPLISTREVERSALLOG, params).Tables(0)
            oCustomClass.TotalRecord = CInt(params(4).Value)
            Return oCustomClass
        Catch exp As Exception
            WriteException("PaymentReversal", "ReversalLogList", exp.Message + exp.StackTrace)
        End Try
    End Function

	Public Sub ProcessPaymentReversalOL(ByVal oCustomClass As Parameter.PaymentReversal)
		Dim objtrans As SqlTransaction
		Dim objcon As New SqlConnection(oCustomClass.strConnection)

		Try
			If objcon.State = ConnectionState.Closed Then objcon.Open()
			objtrans = objcon.BeginTransaction
			Dim params() As SqlParameter = New SqlParameter(6) {}
			params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
			params(0).Value = oCustomClass.BranchReceivedID

			params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
			params(1).Value = oCustomClass.ApplicationID

			params(2) = New SqlParameter(PARAM_HISTORYSEQUENCENO, SqlDbType.Int)
			params(2).Value = oCustomClass.HistorySequenceNo

			params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
			params(3).Value = oCustomClass.BusinessDate

			params(4) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 20)
			params(4).Value = oCustomClass.ReferenceNo

			params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
			params(5).Value = oCustomClass.LoginId

			params(6) = New SqlParameter(PARAM_REASONREVERSAL, SqlDbType.VarChar, 2)
			params(6).Value = oCustomClass.ReasonReversal

			SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, POSTINGPAYMENTREVERSALOL, params)
			objtrans.Commit()
		Catch exp As Exception
			objtrans.Rollback()
			WriteException("PaymentReversal", "ProcessPaymentReversal", exp.Message + exp.StackTrace)
		Finally
			If objcon.State = ConnectionState.Open Then objcon.Close()
			objcon.Dispose()
		End Try
	End Sub

#Region "ReversalFactoring"
	Public Function PaymentReversalListFactoring(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Try
			params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
			params(0).Value = oCustomClass.ApplicationID

			params(1) = New SqlParameter(PARAM_BRANCHRECEIVEID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.BranchReceivedID

            params(2) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
            params(2).Value = oCustomClass.InvoiceNo

            params(3) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(3).Value = oCustomClass.InvoiceSeqNo

            params(4) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(4).Value = oCustomClass.CurrentPage

            params(5) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(5).Value = oCustomClass.PageSize

            params(6) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(6).Value = oCustomClass.SortBy

            params(7) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(7).Direction = ParameterDirection.Output

            oCustomClass.ListReversalPayment = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spPaymentReversalFactoring", params).Tables(0)
			oCustomClass.TotalRecord = CInt(params(5).Value)

			Return oCustomClass
		Catch exp As Exception
			WriteException("PaymentReversal", "PaymentReversalListFactoring", exp.Message + exp.StackTrace)
		End Try
	End Function

	Public Sub ProcessPaymentReversalFactoring(ByVal oCustomClass As Parameter.PaymentReversal)
		Dim objtrans As SqlTransaction
		Dim objcon As New SqlConnection(oCustomClass.strConnection)

		Try
			If objcon.State = ConnectionState.Closed Then objcon.Open()
			objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(8) {}
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
			params(0).Value = oCustomClass.BranchReceivedID

			params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
			params(1).Value = oCustomClass.ApplicationID

			params(2) = New SqlParameter(PARAM_HISTORYSEQUENCENO, SqlDbType.Int)
			params(2).Value = oCustomClass.HistorySequenceNo

			params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
			params(3).Value = oCustomClass.BusinessDate

			params(4) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 20)
			params(4).Value = oCustomClass.ReferenceNo

			params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
			params(5).Value = oCustomClass.LoginId

			params(6) = New SqlParameter(PARAM_REASONREVERSAL, SqlDbType.VarChar, 2)
            params(6).Value = oCustomClass.ReasonReversal

            params(7) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
            params(7).Value = oCustomClass.InvoiceNo

            params(8) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(8).Value = oCustomClass.InvoiceSeqNo


            'SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spPostingPaymentReversalFactoring", params)
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spBeforePostingPaymentReversalFactoring", params)
            objtrans.Commit()
		Catch exp As Exception
			objtrans.Rollback()
			WriteException("PaymentReversal", "spPostingPaymentReversalFactoring", exp.Message + exp.StackTrace)
		Finally
			If objcon.State = ConnectionState.Open Then objcon.Close()
			objcon.Dispose()
		End Try
	End Sub
#End Region

#Region "ReversalModalKerja"
	Public Function PaymentReversalListModalKerja(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal
		Dim params() As SqlParameter = New SqlParameter(5) {}
		Try
			params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
			params(0).Value = oCustomClass.ApplicationID

			params(1) = New SqlParameter(PARAM_BRANCHRECEIVEID, SqlDbType.VarChar, 3)
			params(1).Value = oCustomClass.BranchReceivedID

			params(2) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
			params(2).Value = oCustomClass.CurrentPage

			params(3) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
			params(3).Value = oCustomClass.PageSize

			params(4) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
			params(4).Value = oCustomClass.SortBy

			params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
			params(5).Direction = ParameterDirection.Output

			oCustomClass.ListReversalPayment = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spPaymentReversalModalKerja", params).Tables(0)
			oCustomClass.TotalRecord = CInt(params(5).Value)

			Return oCustomClass
		Catch exp As Exception
			WriteException("PaymentReversal", "spPaymentReversalModalKerja", exp.Message + exp.StackTrace)
		End Try
	End Function

	Public Sub ProcessPaymentReversalModalKerja(ByVal oCustomClass As Parameter.PaymentReversal)
		Dim objtrans As SqlTransaction
		Dim objcon As New SqlConnection(oCustomClass.strConnection)

		Try
			If objcon.State = ConnectionState.Closed Then objcon.Open()
			objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(8) {}
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
			params(0).Value = oCustomClass.BranchReceivedID

			params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
			params(1).Value = oCustomClass.ApplicationID

			params(2) = New SqlParameter(PARAM_HISTORYSEQUENCENO, SqlDbType.Int)
			params(2).Value = oCustomClass.HistorySequenceNo

			params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
			params(3).Value = oCustomClass.BusinessDate

			params(4) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 20)
			params(4).Value = oCustomClass.ReferenceNo

			params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
			params(5).Value = oCustomClass.LoginId

			params(6) = New SqlParameter(PARAM_REASONREVERSAL, SqlDbType.VarChar, 2)
            params(6).Value = oCustomClass.ReasonReversal

            params(7) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
            params(7).Value = oCustomClass.InvoiceNo

            params(8) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(8).Value = oCustomClass.InvoiceSeqNo

            'SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spPostingPaymentReversalModalKerja", params)
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spBeforePostingPaymentReversalModalKerja", params)
            objtrans.Commit()
		Catch exp As Exception
			objtrans.Rollback()
			WriteException("PaymentReversal", "spPostingPaymentReversalModalKerja", exp.Message + exp.StackTrace)
		Finally
			If objcon.State = ConnectionState.Open Then objcon.Close()
			objcon.Dispose()
		End Try
	End Sub
#End Region
    Public Sub ProcessReversalTolakanBGOtor(ByVal oCustomClass As Parameter.PaymentReversal)
        Dim objtrans As SqlTransaction
        Dim objcon As New SqlConnection(oCustomClass.strConnection)

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(5) {}
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchReceivedID

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter(PARAM_HISTORYSEQUENCENO, SqlDbType.Int)
            params(2).Value = oCustomClass.HistorySequenceNo

            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.BusinessDate

            params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(4).Value = oCustomClass.LoginId

            params(5) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
            params(5).Value = oCustomClass.ReferenceNo

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spPostingReversalTolakanBGOtor", params) 'spPostingPaymentReversalOtor
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("PaymentReversal", "ProcessPaymentReversal", exp.Message + exp.StackTrace)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub

End Class
