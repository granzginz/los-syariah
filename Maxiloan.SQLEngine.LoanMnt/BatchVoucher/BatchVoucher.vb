
#Region "Imports"
Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class BatchVoucher : Inherits AccMntBase
    Private Const spBatchVoucherAdd As String = "spBatchVoucherAdd"
    Private Const spBatchVoucherDelete As String = "spBatchVoucherDelete"
    Private Const spBatchVoucherEdit As String = "spBatchVoucherEdit"
    Private Const spBatchVoucherView As String = "spBatchVoucherView"
    Private Const spBatchVoucherPaging As String = "spBatchVoucherPaging"
    Private Const spBatchVoucherPosting As String = "spBatchVoucherPosting"

    Private Const PARAM_BATCHNO As String = "@batchno"
    Private Const PARAM_BATCHDESCRIPTION As String = "@batchdescription"
    Private Const PARAM_RECEIVEDISBURSEFLAG As String = "@receivedisburseflag"
    Private Const PARAM_PROCESSID As String = "@ProcessID"
    Private Const PARAM_BGNO As String = "@BGNo"
    Private Const PARAM_BGDUEDATE As String = "@BGDueDate"

    Private Const PARAM_SEQUENCEBATCH As String = "@SequenceBatch"
    Private Const PARAM_PAYMENTALLOCATIONID As String = "@paymentallocationid"
    Private Const PARAM_AMOUNTPAYMENT As String = "@AmountPayment"
    Private Const PARAM_DESCRIPTION As String = "@Description"
    Private Const PARAM_DEPARTMENTID As String = "@DepartmentID"

    Private Const spCashBankBatchVoucherPaging As String = "spCashBankBatchVoucherPaging"
    Private Const spCashBankBatchVoucherAddHeader As String = "spCashBankBatchVoucherAddHeader"
    Private Const spCashBankBatchVoucherEditHeader As String = "spCashBankBatchVoucherEditHeader"
    Private Const spCashBankBatchVoucherAddDetail As String = "spCashBankBatchVoucherAddDetail"
    Private Const spCashBankBatchVoucherEditDetail As String = "spCashBankBatchVoucherEditDetail"

    Private Const spCashBankBatchVoucherDelete As String = "spCashBankBatchVoucherDelete"
    Private Const spCashBankBatchVoucherViewHeader As String = "spCashBankBatchVoucherViewHeader"
    Private Const spCashBankBatchVoucherViewDetail As String = "spCashBankBatchVoucherViewDetail"

    Private Const spPostingBatchVoucherView As String = "spPostingBatchVoucherView"
    Private Const spBatchVoucherPostingPaymentRcv As String = "spBatchVoucherPostingPaymentRcv"

#Region "Batch Voucher"
    Public Function BatchVoucherPaging(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = oCustomClass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = oCustomClass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = oCustomClass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = oCustomClass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            oCustomClass.BatchList = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spBatchVoucherPaging, params).Tables(0)
            oCustomClass.TotalRecord = CInt(params(4).Value)
            Return oCustomClass
        Catch exp As Exception
            WriteException("BatchVoucher", "BatchVoucherPaging", exp.Message + exp.StackTrace)

        End Try
    End Function

    Public Sub BatchVoucherAdd(ByVal oCustomClass As Parameter.BatchVoucher)
        Dim objConnection As New SqlConnection(oCustomClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Dim params() As SqlParameter
        If oCustomClass.ReceiveDisburseFlag = "R" Then
            params = New SqlParameter(9) {}
        Else
            params = New SqlParameter(11) {}
        End If

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId

            params(1) = New SqlParameter(PARAM_BATCHNO, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.BatchNo

            params(2) = New SqlParameter(PARAM_BATCHDESCRIPTION, SqlDbType.VarChar, 50)
            params(2).Value = oCustomClass.BatchDescription

            params(3) = New SqlParameter(PARAM_RECEIVEDISBURSEFLAG, SqlDbType.VarChar, 50)
            params(3).Value = oCustomClass.ReceiveDisburseFlag

            params(4) = New SqlParameter(PARAM_PROCESSID, SqlDbType.VarChar, 10)
            params(4).Value = oCustomClass.ProcessID

            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId

            params(6) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(6).Value = oCustomClass.BusinessDate

            params(7) = New SqlParameter(PARAM_WOP, SqlDbType.VarChar, 2)
            params(7).Value = oCustomClass.WayOfPayment

            params(8) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(8).Value = oCustomClass.BankAccountID

            params(9) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(9).Value = oCustomClass.Notes.Trim

            If oCustomClass.ReceiveDisburseFlag = "D" Then
                params(10) = New SqlParameter(PARAM_BGNO, SqlDbType.VarChar, 20)
                params(10).Value = oCustomClass.BGNo

                params(11) = New SqlParameter(PARAM_BGDUEDATE, SqlDbType.DateTime)
                params(11).Value = oCustomClass.BGDueDate
            End If

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spBatchVoucherAdd, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub
    Public Sub BatchVoucherEdit(ByVal oCustomClass As Parameter.BatchVoucher)
        Dim objConnection As New SqlConnection(oCustomClass.strConnection)
        Dim objtrans As SqlTransaction
        Dim params() As SqlParameter
        If oCustomClass.ReceiveDisburseFlag = "R" Then
            params = New SqlParameter(9) {}
        Else
            params = New SqlParameter(11) {}
        End If

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId

            params(1) = New SqlParameter(PARAM_BATCHNO, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.BatchNo

            params(2) = New SqlParameter(PARAM_BATCHDESCRIPTION, SqlDbType.VarChar, 50)
            params(2).Value = oCustomClass.BatchDescription

            params(3) = New SqlParameter(PARAM_RECEIVEDISBURSEFLAG, SqlDbType.VarChar, 50)
            params(3).Value = oCustomClass.ReceiveDisburseFlag

            params(4) = New SqlParameter(PARAM_PROCESSID, SqlDbType.VarChar, 10)
            params(4).Value = oCustomClass.ProcessID

            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId

            params(6) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(6).Value = oCustomClass.BusinessDate

            params(7) = New SqlParameter(PARAM_WOP, SqlDbType.VarChar, 2)
            params(7).Value = oCustomClass.WayOfPayment

            params(8) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(8).Value = oCustomClass.BankAccountID

            params(9) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(9).Value = oCustomClass.Notes.Trim

            If oCustomClass.ReceiveDisburseFlag = "D" Then
                params(10) = New SqlParameter(PARAM_BGNO, SqlDbType.VarChar, 20)
                params(10).Value = oCustomClass.BGNo

                params(11) = New SqlParameter(PARAM_BGDUEDATE, SqlDbType.DateTime)
                params(11).Value = oCustomClass.BGDueDate
            End If


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spBatchVoucherEdit, params)
            objtrans.Commit()

        Catch exp As Exception
            objtrans.Rollback()
            WriteException("BatchVoucher", "BatchVoucherEdit", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub
    Public Sub BatchVoucherDelete(ByVal oCustomClass As Parameter.BatchVoucher)
        Dim objConnection As New SqlConnection(oCustomClass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(1) {}
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_BATCHNO, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.BatchNo

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spBatchVoucherDelete, params)
            objtrans.Commit()

        Catch exp As Exception
            objtrans.Rollback()
            WriteException("BatchVoucher", "BatchVoucherDelete", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Public Sub BatchVoucherPosted(ByVal oCustomClass As Parameter.BatchVoucher)
        Dim objConnection As New SqlConnection(oCustomClass.strConnection)
        Dim objtrans As SqlTransaction
        Dim dtBatchVoucher As New DataTable
        Dim nCounter As Integer
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim paramsDetail() As SqlParameter = New SqlParameter(4) {}
        If objConnection.State = ConnectionState.Closed Then objConnection.Open()
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = oCustomClass.BranchId
        params(1) = New SqlParameter(PARAM_BATCHNO, SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.BatchNo
        dtBatchVoucher = SqlHelper.ExecuteDataset(objConnection, CommandType.StoredProcedure, spPostingBatchVoucherView, params).Tables(0)

        paramsDetail(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        paramsDetail(1) = New SqlParameter(PARAM_BATCHNO, SqlDbType.VarChar, 20)
        paramsDetail(2) = New SqlParameter(PARAM_SEQUENCEBATCH, SqlDbType.Int)
        paramsDetail(3) = New SqlParameter(PARAM_BRANCHAGREEMENT, SqlDbType.VarChar, 3)
        paramsDetail(4) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
        If dtBatchVoucher.Rows.Count > 0 Then
            For nCounter = 0 To dtBatchVoucher.Rows.Count - 1
                Try
                    If objConnection.State = ConnectionState.Closed Then objConnection.Open()
                    objtrans = objConnection.BeginTransaction
                    If CStr(dtBatchVoucher.Rows(nCounter).Item("ReceiveDisburseFlag")) = "R" And CStr(dtBatchVoucher.Rows(nCounter).Item("ProcessID")) = "PAYMENTRCV" Then
                        paramsDetail(0).Value = dtBatchVoucher.Rows(nCounter).Item("BranchID")
                        paramsDetail(1).Value = dtBatchVoucher.Rows(nCounter).Item("BatchNo")
                        paramsDetail(2).Value = dtBatchVoucher.Rows(nCounter).Item("SequenceBatch")
                        paramsDetail(3).Value = dtBatchVoucher.Rows(nCounter).Item("BranchAgreement")
                        paramsDetail(4).Value = dtBatchVoucher.Rows(nCounter).Item("ApplicationID")
                        SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spBatchVoucherPostingPaymentRcv, paramsDetail)
                    End If
                    objtrans.Commit()
                Catch exp As Exception
                    objtrans.Rollback()
                    WriteException("BatchVoucher", "BatchVoucherDelete", exp.Message + exp.StackTrace)
                    Exit For
                End Try
            Next
        End If
        If objConnection.State = ConnectionState.Open Then objConnection.Close()
        objConnection.Dispose()
    End Sub
    Public Function BatchVoucherView(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim objread As SqlDataReader

        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = oCustomClass.BranchId
        params(1) = New SqlParameter(PARAM_BATCHNO, SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.BatchNo

        Try
            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, spBatchVoucherView, params)
            If objread.Read Then
                With oCustomClass
                    .BatchNo = objread("BatchNo").ToString
                    .BatchDescription = objread("BatchDescription").ToString
                    .BusinessDate = CDate(objread("BatchDate"))
                    .LoginId = objread("CashierID").ToString
                    .Status = objread("Status").ToString
                    .StatusDate = objread("StatusDate").ToString
                    .PostedCashierID = objread("PostedCashierID").ToString
                    .OpeningSequenceNo = CInt(objread("OpeningSequenceNo"))
                    .PostedDate = objread("PostedDate").ToString
                    .WayOfPayment = objread("WOP").ToString
                    .WayOfPaymentName = objread("WayOfPayment").ToString
                    .BankAccountID = objread("BankAccountID").ToString
                    .BankAccountName = objread("BankAccountName").ToString
                    .Notes = objread("Notes").ToString
                    .BGNo = objread("BGNo").ToString
                    .BGDueDate = objread("BGDueDate").ToString
                    .ProcessID = objread("ProcessID").ToString.Trim
                    .ReceiveDisburseFlag = objread("ReceiveDisburseFlag").ToString.Trim
                    .BatchDate = CDate(objread("BatchDate"))
                End With
            End If
            objread.Close()
            Return oCustomClass
        Catch exp As Exception
            WriteException("BatchVoucher", "BatchVoucherView", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "Cash Bank Batch Voucher"
    Public Function CashBankBatchVoucherPaging(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = oCustomClass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = oCustomClass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = oCustomClass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = oCustomClass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            oCustomClass.BatchList = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spCashBankBatchVoucherPaging, params).Tables(0)
            oCustomClass.TotalRecord = CInt(params(4).Value)
            Return oCustomClass
        Catch exp As Exception
            WriteException("BatchVoucher", "BatchVoucherPaging", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function CashBankBatchVoucherView(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim objread As SqlDataReader

        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = oCustomClass.BranchId
        params(1) = New SqlParameter(PARAM_BATCHNO, SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.BatchNo
        params(2) = New SqlParameter(PARAM_SEQUENCEBATCH, SqlDbType.Int)
        params(2).Value = oCustomClass.BatchSequenceNo
        Try
            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, spCashBankBatchVoucherViewHeader, params)
            If objread.Read Then
                With oCustomClass
                    .BatchDate = CDate(objread("BatchDate"))
                    .BranchAgreement = objread("BranchAgreement").ToString
                    .ApplicationID = objread("ApplicationID").ToString
                    .Agreementno = objread("AgreementNo").ToString
                    .ReceivedFrom = CStr(objread("ReceivedFrom"))
                    .ReferenceNo = objread("ReferenceNo").ToString
                    .ValueDate = CDate(objread("ValueDate"))
                    .BatchDescription = objread("CBBatchVoucherDescription").ToString
                    .AmountReceive = CDbl(objread("AmountCBBatchVoucher"))
                    .Notes = CStr(objread("Notes"))
                End With
            End If
            objread.Close()
            oCustomClass.TableBatch = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spCashBankBatchVoucherViewDetail, params).Tables(0)
            Return oCustomClass
        Catch exp As Exception
            WriteException("BatchVoucher", "CashBankBatchVoucherView", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Sub CashBankBatchVoucherEdit(ByVal oCustomClass As Parameter.BatchVoucher)
        Dim paramsHeader() As SqlParameter = New SqlParameter(11) {}
        Dim paramsDetail() As SqlParameter = New SqlParameter(6) {}
        Dim objcon As New SqlConnection(oCustomClass.strConnection)
        Dim objtrans As SqlTransaction
        Dim i As Integer
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            paramsHeader(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            paramsHeader(0).Value = oCustomClass.BranchId

            paramsHeader(1) = New SqlParameter(PARAM_BATCHNO, SqlDbType.VarChar, 20)
            paramsHeader(1).Value = oCustomClass.BatchNo

            paramsHeader(2) = New SqlParameter(PARAM_SEQUENCEBATCH, SqlDbType.Int)
            paramsHeader(2).Value = oCustomClass.BatchSequenceNo

            paramsHeader(3) = New SqlParameter(PARAM_BRANCHAGREEMENT, SqlDbType.VarChar, 3)
            paramsHeader(3).Value = oCustomClass.BranchAgreement

            paramsHeader(4) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            paramsHeader(4).Value = oCustomClass.ApplicationID

            paramsHeader(5) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            paramsHeader(5).Value = oCustomClass.ReceivedFrom

            paramsHeader(6) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 20)
            paramsHeader(6).Value = oCustomClass.ReferenceNo

            paramsHeader(7) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            paramsHeader(7).Value = oCustomClass.BusinessDate

            paramsHeader(8) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            paramsHeader(8).Value = oCustomClass.ValueDate

            paramsHeader(9) = New SqlParameter(PARAM_AMOUNTRECEIVE, SqlDbType.Decimal)
            paramsHeader(9).Value = oCustomClass.AmountReceive

            paramsHeader(10) = New SqlParameter(PARAM_DESCRIPTION, SqlDbType.VarChar, 50)
            paramsHeader(10).Value = oCustomClass.BatchDescription

            paramsHeader(11) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            paramsHeader(11).Value = oCustomClass.Notes

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCashBankBatchVoucherEditHeader, paramsHeader)

            paramsDetail(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            paramsDetail(1) = New SqlParameter(PARAM_BATCHNO, SqlDbType.VarChar, 20)
            paramsDetail(2) = New SqlParameter(PARAM_SEQUENCEBATCH, SqlDbType.VarChar, 20)
            paramsDetail(3) = New SqlParameter(PARAM_PAYMENTALLOCATIONID, SqlDbType.VarChar, 10)
            paramsDetail(4) = New SqlParameter(PARAM_AMOUNTPAYMENT, SqlDbType.Decimal)
            paramsDetail(5) = New SqlParameter(PARAM_DESCRIPTION, SqlDbType.VarChar, 50)
            paramsDetail(6) = New SqlParameter(PARAM_DEPARTMENTID, SqlDbType.VarChar, 10)
            For i = 0 To oCustomClass.TableBatch.Rows.Count - 1
                paramsDetail(0).Value = oCustomClass.BranchId
                paramsDetail(1).Value = oCustomClass.BatchNo
                paramsDetail(2).Value = oCustomClass.BatchSequenceNo
                paramsDetail(3).Value = oCustomClass.TableBatch.Rows(i).Item("PaymentAllocationID")
                paramsDetail(4).Value = oCustomClass.TableBatch.Rows(i).Item("AmountTransaction")
                paramsDetail(5).Value = oCustomClass.TableBatch.Rows(i).Item("TransactionDescription")
                paramsDetail(6).Value = oCustomClass.TableBatch.Rows(i).Item("DepartmentID")
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCashBankBatchVoucherAddDetail, paramsDetail)
            Next
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("PDCMultiAgreement", "CashBankBatchVoucherEdit", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
    Public Sub CashBankBatchVoucherAdd(ByVal oCustomClass As Parameter.BatchVoucher)
        Dim paramsHeader() As SqlParameter = New SqlParameter(11) {}
        Dim paramsDetail() As SqlParameter = New SqlParameter(6) {}
        Dim objcon As New SqlConnection(oCustomClass.strConnection)
        Dim objtrans As SqlTransaction
        Dim i As Integer
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            paramsHeader(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            paramsHeader(0).Value = oCustomClass.BranchId

            paramsHeader(1) = New SqlParameter(PARAM_BATCHNO, SqlDbType.VarChar, 20)
            paramsHeader(1).Value = oCustomClass.BatchNo

            paramsHeader(2) = New SqlParameter(PARAM_BRANCHAGREEMENT, SqlDbType.VarChar, 3)
            paramsHeader(2).Value = oCustomClass.BranchAgreement

            paramsHeader(3) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            paramsHeader(3).Value = oCustomClass.ApplicationID

            paramsHeader(4) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            paramsHeader(4).Value = oCustomClass.ReceivedFrom

            paramsHeader(5) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 20)
            paramsHeader(5).Value = oCustomClass.ReferenceNo

            paramsHeader(6) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            paramsHeader(6).Value = oCustomClass.BusinessDate

            paramsHeader(7) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            paramsHeader(7).Value = oCustomClass.ValueDate

            paramsHeader(8) = New SqlParameter(PARAM_AMOUNTRECEIVE, SqlDbType.Decimal)
            paramsHeader(8).Value = oCustomClass.AmountReceive

            paramsHeader(9) = New SqlParameter(PARAM_DESCRIPTION, SqlDbType.VarChar, 50)
            paramsHeader(9).Value = oCustomClass.BatchDescription

            paramsHeader(10) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            paramsHeader(10).Value = oCustomClass.Notes

            paramsHeader(11) = New SqlParameter(PARAM_SEQUENCEBATCH, SqlDbType.Int)
            paramsHeader(11).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCashBankBatchVoucherAddHeader, paramsHeader)

            paramsDetail(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            paramsDetail(1) = New SqlParameter(PARAM_BATCHNO, SqlDbType.VarChar, 20)
            paramsDetail(2) = New SqlParameter(PARAM_SEQUENCEBATCH, SqlDbType.VarChar, 20)
            paramsDetail(3) = New SqlParameter(PARAM_PAYMENTALLOCATIONID, SqlDbType.VarChar, 10)
            paramsDetail(4) = New SqlParameter(PARAM_AMOUNTPAYMENT, SqlDbType.Decimal)
            paramsDetail(5) = New SqlParameter(PARAM_DESCRIPTION, SqlDbType.VarChar, 50)
            paramsDetail(6) = New SqlParameter(PARAM_DEPARTMENTID, SqlDbType.VarChar, 10)
            For i = 0 To oCustomClass.TableBatch.Rows.Count - 1
                paramsDetail(0).Value = oCustomClass.BranchId
                paramsDetail(1).Value = oCustomClass.BatchNo
                paramsDetail(2).Value = paramsHeader(11).Value
                paramsDetail(3).Value = oCustomClass.TableBatch.Rows(i).Item("PaymentAllocationID")
                paramsDetail(4).Value = oCustomClass.TableBatch.Rows(i).Item("AmountTransaction")
                paramsDetail(5).Value = oCustomClass.TableBatch.Rows(i).Item("TransactionDescription")
                paramsDetail(6).Value = oCustomClass.TableBatch.Rows(i).Item("DepartmentID")
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCashBankBatchVoucherAddDetail, paramsDetail)
            Next
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("PDCMultiAgreement", "CashBankBatchVoucherAdd", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
    Public Sub CashBankBatchVoucherDelete(ByVal oCustomClass As Parameter.BatchVoucher)
        Dim objConnection As New SqlConnection(oCustomClass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(2) {}
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_BATCHNO, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.BatchNo
            params(2) = New SqlParameter(PARAM_SEQUENCEBATCH, SqlDbType.Int)
            params(2).Value = oCustomClass.BatchSequenceNo
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCashBankBatchVoucherDelete, params)
            objtrans.Commit()

        Catch exp As Exception
            objtrans.Rollback()
            WriteException("BatchVoucher", "CashBankBatchVoucherDelete", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Public Function getTransactionBatchUpload(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
        Dim oReturnValue As New Parameter.BatchVoucher
        Dim params(3) As SqlParameter

        params(0) = New SqlParameter("@BatchNo", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.BatchNo
        params(1) = New SqlParameter("@SheetName", SqlDbType.VarChar)
        params(1).Value = "Sheet1$"
        params(2) = New SqlParameter("@FilePath", SqlDbType.VarChar)
        params(2).Value = oCustomClass.ExcelFilePath
        params(3) = New SqlParameter("@HDR", SqlDbType.VarChar)
        params(3).Value = "Yes"

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, _
                CommandType.StoredProcedure, "spgetTransactionBatchUpload", params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function getCompleteBatch(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
        Dim oReturnValue As New Parameter.BatchVoucher
        Dim par() As SqlParameter = New SqlParameter(1) {}
        Try
            par(0) = New SqlParameter("@BatchNo", SqlDbType.VarChar, 50)
            par(0).Value = oCustomClass.BatchNo
            par(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par(1).Value = oCustomClass.BranchId
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, _
                CommandType.StoredProcedure, "spGetCompleteBatch", par).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Sub saveSingleBatch(ByVal oCustomClass As Parameter.BatchVoucher)
        Dim objcon As New SqlConnection(oCustomClass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            'comment sementara
            'SqlHelper.ExecuteNonQuery(objtrans, CommandType.Text, "TRUNCATE TABLE PaymentReceiveBatchTemp")
            Dim i As Integer
            Dim par() As SqlParameter = New SqlParameter(5) {}
            For i = 0 To oCustomClass.ListData.Rows.Count - 1
                par(0) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
                par(0).Value = oCustomClass.ListData.Rows(i).Item("nokontrak")
                par(1) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                par(1).Value = oCustomClass.ListData.Rows(i).Item("referenceno")
                par(2) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
                par(2).Value = oCustomClass.ListData.Rows(i).Item("valuedate")
                par(3) = New SqlParameter("@Amount", SqlDbType.Money)
                par(3).Value = oCustomClass.ListData.Rows(i).Item("amount")
                par(4) = New SqlParameter("@BatchNo", SqlDbType.VarChar, 50)
                par(4).Value = oCustomClass.ListData.Rows(i).Item("BatchNo")
                par(5) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                par(5).Value = oCustomClass.ListData.Rows(i).Item("BranchID")
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spSaveSingleBatch", par)
            Next
            objtrans.Commit()
        Catch ex As Exception
            objtrans.Rollback()
            objtrans.Dispose()
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Public Sub DeleteUploadedBatch(ByVal oCustomClass As Parameter.BatchVoucher)
        Dim objcon As New SqlConnection(oCustomClass.strConnection)
        Dim objtrans As SqlTransaction
        Dim par() As SqlParameter = New SqlParameter(1) {}
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            par(0) = New SqlParameter("@BatchNo", SqlDbType.VarChar, 50)
            par(0).Value = oCustomClass.BatchNo
            par(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par(1).Value = oCustomClass.BranchId
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.Text, "DELETE FROM PaymentReceiveBatchTemp where BatchNo = @BatchNo AND BranchID = @BranchID", par)
            objtrans.Commit()
        Catch ex As Exception
            objtrans.Rollback()
            objtrans.Dispose()
            Throw New Exception(ex.Message)
        End Try
    End Sub
#End Region



End Class
