
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InsClaimReceive : Inherits AccMntBase

#Region "Insurance Claim Info"
    Public Function InsClaimInfo(ByVal customclass As Parameter.InsClaimReceive) As Parameter.InsClaimReceive
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID.Trim

            params(2) = New SqlParameter("@InsSequenceNo", SqlDbType.Int)
            params(2).Value = customclass.InsSequenceNo

            params(3) = New SqlParameter("@AssetSeqno", SqlDbType.Int)
            params(3).Value = customclass.AssetSeqNo

            params(4) = New SqlParameter("@InsClaimSeqNo", SqlDbType.Int)
            params(4).Value = customclass.InsClaimSeqNo

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spInsuranceClaimInfo", params)

            With customclass
                If objread.Read Then
                    .BranchAgreement = CType(objread("BranchFullName"), String)
                    .CustomerID = CType(objread("CustomerID"), String)
                    .Agreementno = CType(objread("AgreementNo"), String)
                    .CustomerName = CType(objread("Name"), String)
                    .InsuranceCoy = CType(objread("MaskAssBranchName"), String)
                    .ClaimType = CType(objread("ClaimType"), String)
                    .Description = CType(objread("Description"), String)
                    .InsurancePaidBy = CType(objread("InsurancepaidBy"), String)
                    .ClaimDocNo = CType(objread("ClaimDocNo"), String)
                    .ClaimDate = CType(objread("ClaimDate"), DateTime)
                    .EventDate = CType(objread("EventDate"), String)
                    .ClaimAmount = CType(objread("ClaimAmount"), Double)
                    .ReportedBy = CType(objread("ReportedBy"), String)
                    .ReportDate = CType(objread("ReportDate"), String)
                    .Notes = CType(objread("Notes"), String)
                End If
                objread.Close()

            End With
            Return customclass
        Catch exp As Exception
            WriteException("Insurance Claim", "Insurance Claim Info", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

    Public Function SaveInsClaimReceive(ByVal customClass As Parameter.InsClaimReceive) As Parameter.InsClaimReceive
        Dim oConnection As New SqlConnection(customClass.strConnection)
        Dim objTransaction As SqlTransaction
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            objTransaction = oConnection.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(13) {}
            Dim params1() As SqlParameter = New SqlParameter(4) {}

            If customClass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = customClass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = customClass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = customClass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"
                SqlHelper.ExecuteScalar(objTransaction, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                customClass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customClass.ApplicationID.Trim

            params(2) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(2).Value = customClass.ValueDate

            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = customClass.BusinessDate

            params(4) = New SqlParameter("@CompanyID", SqlDbType.VarChar, 3)
            params(4).Value = customClass.CompanyID

            params(5) = New SqlParameter("@BranchAgreement", SqlDbType.VarChar, 3)
            params(5).Value = customClass.BranchAgreement

            params(6) = New SqlParameter("@LoginID", SqlDbType.VarChar, 10)
            params(6).Value = customClass.LoginId

            params(7) = New SqlParameter("@AmountReceive", SqlDbType.Decimal)
            params(7).Value = customClass.AmountReceive

            params(8) = New SqlParameter("@ReceivedFrom", SqlDbType.VarChar, 50)
            params(8).Value = customClass.ReceivedFrom

            params(9) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 50)
            params(9).Value = customClass.ReferenceNo

            params(10) = New SqlParameter("@WOP", SqlDbType.VarChar, 7)
            params(10).Value = customClass.WOP

            params(11) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10)
            params(11).Value = customClass.BankAccountID

            params(12) = New SqlParameter("@Notes", SqlDbType.VarChar, 100)
            params(12).Value = customClass.Notes

            params(13) = New SqlParameter("@InsClaimSeqNo", SqlDbType.Int)
            params(13).Value = customClass.InsClaimSeqNo

             
            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "spInsClaimReceiveSave", params)
            objTransaction.Commit()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
            Return customClass
        Catch exp As Exception
            objTransaction.Rollback()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
            'WriteException("FullPrepayRequest", "SaveFullPrepayRequest", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
End Class
