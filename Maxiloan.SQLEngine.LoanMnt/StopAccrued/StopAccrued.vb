
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports System.DBNull
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region
Public Class StopAccrued
    Inherits AccMntBase

    Private Const PARAM_REQUESTBY As String = "@requestby"
    Private Const PARAM_APPROVALNO As String = "@ApprovalNo"
    Private Const REQUESTSTOPACCRUED As String = "spStopAccruedRequest"
    Private Const REVERSALSTOPACCRUED As String = "spStopAccuredReversal"
    Private Const GETPASTDUEDAYS As String = "spStopAccruedGetPastDueDays"
    Private Const SPSTOPACCRUEDVIEW As String = "spStopAccruedView"
    Private Const PARAM_REQUESTNANO As String = "@RequestNANo"
    Private Const spStopAccruedInquiry As String = "spStopAccruedInquiry"
    Private Const SPSTOPACCURUEDREVERSALLIST As String = "spStopAccuredreversalList"

    Public Sub StopAccruedRequest(ByVal oCustomClass As Parameter.StopAccrued)
        Dim oConnection As New SqlConnection(oCustomClass.strConnection)
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim objTransaction As SqlTransaction
        Dim strApprovalNo As String
        Dim NAAmount As Double
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            objTransaction = oConnection.BeginTransaction
            With oEntitiesApproval
                .ApprovalTransaction = objTransaction
                .BranchId = oCustomClass.BranchId
                .SchemeID = "NA__"
                .RequestDate = oCustomClass.BusinessDate
                .TransactionNo = oCustomClass.ApplicationID
                .ApprovalValue = oCustomClass.NAAmount
                .UserRequest = oCustomClass.LoginId
                .UserApproval = oCustomClass.RequestTo
                .ApprovalNote = oCustomClass.Notes
                .AprovalType = Parameter.Approval.ETransactionType.StopAccrued_Approval
            End With
            strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

            Dim params() As SqlParameter = New SqlParameter(7) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID.Trim

            params(2) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(2).Value = oCustomClass.BusinessDate

            params(3) = New SqlParameter(PARAM_REASONTYPEID, SqlDbType.VarChar, 10)
            params(3).Value = oCustomClass.ReasonTypeID

            params(4) = New SqlParameter(PARAM_REASONID, SqlDbType.VarChar, 10)
            params(4).Value = oCustomClass.ReasonID

            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId

            params(6) = New SqlParameter(PARAM_APPROVALNO, SqlDbType.VarChar, 50)
            params(6).Value = strApprovalNo

            params(7) = New SqlParameter(PARAM_NOTES, SqlDbType.Text)
            params(7).Value = oCustomClass.Notes


            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, REQUESTSTOPACCRUED, params)
            objTransaction.Commit()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        Catch exp As Exception
            objTransaction.Rollback()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
            WriteException("FullPrepayRequest", "SaveFullPrepayRequest", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Sub

    Public Function StopAccruedGetPastDueDays(ByVal oCustomClass As Parameter.StopAccrued) As Parameter.StopAccrued

        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim objread As SqlDataReader
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = oCustomClass.BranchId.Replace("'", "")

        params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.ApplicationID.Trim

        params(2) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
        params(2).Value = oCustomClass.BusinessDate

        objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, GETPASTDUEDAYS, params)
        If objread.Read Then
            With oCustomClass
                .PastDueDays = CInt(objread("PastDueDays"))
                .LastPayment = CDate(objread("LastPayment"))
            End With
        End If
        objread.Close()
        Return oCustomClass
    End Function

    Public Function StopAccruedView(ByVal oCustomClass As Parameter.StopAccrued) As Parameter.StopAccrued

        Dim params() As SqlParameter = New SqlParameter(0) {}
        Dim objread As SqlDataReader
        params(0) = New SqlParameter(PARAM_REQUESTNANO, SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.RequestNoNa

        objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, SPSTOPACCRUEDVIEW, params)
        If objread.Read Then
            With oCustomClass
                .RequestNoNa = objread("RequestNaNo").ToString
                .Agreementno = objread("AgreementNo").ToString
                .ApplicationID = objread("ApplicationID").ToString
                .CustomerID = objread("CustomerID").ToString
                .CustomerName = objread("CustomerName").ToString
                .CustomerType = objread("CustomerType").ToString
                .CrossDefault = CBool(objread("CrossDefault"))
                .DefaultStatus = objread("DefaultStatus").ToString
                .ValueDate = CDate(objread("RequestlDate"))

                .NAAmount = CDbl(objread("NaTotalAmount"))

                .OutstandingPrincipal = CDbl(objread("OSPrincipalAmount"))
                .OutStandingInterest = CDbl(objread("OSInterestAmount"))

                .InstallmentDue = CDbl(objread("OSInstallmentDue"))
                .InsuranceDue = CDbl(objread("OSInsuranceDue"))
                .LcInstallment = CDbl(objread("OSLCInstallment"))
                .LcInsurance = CDbl(objread("OSLCInsurance"))
                .InstallmentCollFee = CDbl(objread("OSInstallCollectionFee"))
                .InsuranceCollFee = CDbl(objread("OSInsurCollectionFee"))
                .PDCBounceFee = CDbl(objread("OSPDCBounceFee"))
                .STNKRenewalFee = CDbl(objread("OSSTNKRenewalFee"))
                .RepossessionFee = CDbl(objread("OSRepossesFee"))
                .InsuranceClaimExpense = CDbl(objread("OSInsuranceClaimExpense"))

                .AccruedInterest = CDbl(objread("AccruedInterest"))
                .ContractStatus = objread("ContractStatus").ToString

                .Notes = objread("Notes").ToString
                .ReasonTypeID = objread("ReasonTypeID").ToString
                .ReasonDescription = objread("Description").ToString
                .UserApproval = CStr(objread("UserApproval"))
                .UserRequest = CStr(objread("UserRequest"))

                .NAType = CStr(objread("ProcessID"))
                .ApprovalStatus = CStr(objread("ApprovalStatus"))
                .StatusDate = CDate(objread("StatusDate"))
                .PastDueDays = CInt(objread("PastDueDays"))
                .LastPayment = CDate(objread("LastPayment"))
            End With
        End If
        objread.Close()
        Return oCustomClass
    End Function

    Public Function StopAccruedInquiry(ByVal oCustomClass As Parameter.StopAccrued) As Parameter.StopAccrued
        Dim params() As SqlParameter = New SqlParameter(4) {}
        With oCustomClass
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = .SortBy
            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output
        End With
        Try
            oCustomClass.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spStopAccruedInquiry, params).Tables(0)
            oCustomClass.TotalRecord = CType(params(4).Value, Int16)
            Return oCustomClass
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")
        Catch exp As Exception
            WriteException("StopAccrued", "StopAccruedInquiry", exp.Message + exp.StackTrace)

        End Try
    End Function

    Public Sub StopAccruedReversal(ByVal oCustomClass As Parameter.StopAccrued)
        Dim oConnection As New SqlConnection(oCustomClass.strConnection)
        Dim objTransaction As SqlTransaction
        Dim strApprovalNo As String
        Dim NAAmount As Double
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            objTransaction = oConnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(4) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID.Trim

            params(2) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(2).Value = oCustomClass.BusinessDate

            params(3) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.ValueDate

            params(4) = New SqlParameter(PARAM_NOTES, SqlDbType.Text)
            params(4).Value = oCustomClass.Notes


            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, REVERSALSTOPACCRUED, params)
            objTransaction.Commit()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        Catch exp As Exception
            objTransaction.Rollback()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
            'WriteException("FullPrepayRequest", "SaveFullPrepayRequest", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Sub
    Public Function StopAccruedReversalList(ByVal oCustomClass As Parameter.StopAccrued) As Parameter.StopAccrued

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim objread As SqlDataReader
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID.Trim
            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, SPSTOPACCURUEDREVERSALLIST, params)
            If objread.Read Then
                With oCustomClass
                    .NAAmount = CDbl(objread("NaTotalAmount"))
                    .OutstandingPrincipal = CDbl(objread("OSPrincipalAmount"))
                    .OutStandingInterest = CDbl(objread("OSInterestAmount"))
                    .InstallmentDue = CDbl(objread("OSInstallmentDue"))
                    .InsuranceDue = CDbl(objread("OSInsuranceDue"))
                    .LcInstallment = CDbl(objread("OSLCInstallment"))
                    .LcInsurance = CDbl(objread("OSLCInsurance"))
                    .InstallmentCollFee = CDbl(objread("OSInstallCollectionFee"))
                    .InsuranceCollFee = CDbl(objread("OSInsurCollectionFee"))
                    .PDCBounceFee = CDbl(objread("OSPDCBounceFee"))
                    .STNKRenewalFee = CDbl(objread("OSSTNKRenewalFee"))
                    .RepossessionFee = CDbl(objread("OSRepossesFee"))
                    .InsuranceClaimExpense = CDbl(objread("OSInsuranceClaimExpense"))
                    .AccruedInterest = CDbl(objread("AccruedInterest"))
                    .Notes = objread("Notes").ToString
                    .PastDueDays = CInt(objread("PastDueDays"))
                    .LastPayment = CDate(objread("LastPayment"))
                    .Agreementno = objread("AgreementNo").ToString
                    .CustomerName = objread("Name").ToString
                    .NADate = CDate(objread("NaDate"))
                    .CustomerID = objread("CustomerID").ToString


                End With
            End If
            objread.Close()
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
End Class

