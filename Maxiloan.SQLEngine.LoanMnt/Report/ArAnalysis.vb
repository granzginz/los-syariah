
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class ArAnalysis
    Inherits AccMntBase
    Public Function ArAnalysisReport(ByVal customclass As Parameter.ArAnalysis) As DataSet
        Dim objread As SqlDataReader
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@pichrTahun", SqlDbType.VarChar, 4)
            params(0).Value = customclass.Periode
            params(1) = New SqlParameter("@pivchBranch", SqlDbType.VarChar, 4)
            params(1).Value = customclass.BranchId
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spArAnalysisReport", params)
        Catch exp As Exception
            WriteException("AgreementTransfer", "GetCust", exp.Message + exp.StackTrace)
        End Try
    End Function

#Region "TerminationReport"
    Public Function TerminationReport(ByVal customclass As Parameter.ArAnalysis) As Parameter.ArAnalysis
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@Branch", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@PODateFrom", SqlDbType.Char, 10)
        params(1).Value = customclass.PODateFrom

        params(2) = New SqlParameter("@PODateTo", SqlDbType.Char, 10)
        params(2).Value = customclass.PODateTo

        Try
            customclass.ListDataReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, customclass.SpName, params)
            Return customclass

        Catch exp As Exception
            WriteException("AssetDocument", "AssetDocumentReport", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
End Class
