

#Region "Imports "
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class WriteOff
    Inherits AccMntBase

    Private Const GETNAAMOUNT As String = "spWOGetNAAmount"
    Private Const REQUESTWO As String = "spWriteOffRequest"
    Private Const PARAM_APPROVALNO As String = "@ApprovalNo"
    Private Const VIEWWRITEOFF As String = "spViewWO"

    Public Function GetGeneralPagingWO(ByVal customClass As Parameter.WriteOff) As Parameter.WriteOff

        Dim oReturnValue As New Parameter.WriteOff
        Dim spName As String

        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(5).Value = customClass.BusinessDate
        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As MaxiloanExceptions
            Dim err As New MaxiloanExceptions
            err.WriteLog("WriteOff", "GetGeneralPagingWO", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            WriteException("WriteOff", "GetGeneralPagingWO", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function WriteOffGetSAAmount(ByVal oCustomClass As Parameter.WriteOff) As Parameter.WriteOff

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim objread As SqlDataReader
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = oCustomClass.BranchId.Replace("'", "")

        params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.ApplicationID.Trim

        objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, GETNAAMOUNT, params)
        If objread.Read Then
            With oCustomClass
                .NADate = CDate(objread("StatusDate"))
                .NAAmount = CDec(objread("NaTotalAMount"))
            End With
        End If
        objread.Close()
        Return oCustomClass
    End Function

    Public Sub WORequest(ByVal oCustomClass As Parameter.WriteOff)
        Dim oConnection As New SqlConnection(oCustomClass.strConnection)
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim objTransaction As SqlTransaction
        Dim strApprovalNo As String
        Dim NAAmount As Double
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            objTransaction = oConnection.BeginTransaction
            With oEntitiesApproval
                .ApprovalTransaction = objTransaction
                .BranchId = oCustomClass.BranchId
                .SchemeID = "WO__"
                .RequestDate = oCustomClass.BusinessDate
                .TransactionNo = oCustomClass.ApplicationID
                .ApprovalValue = oCustomClass.NAAmount
                .UserRequest = oCustomClass.LoginId
                .UserApproval = oCustomClass.RequestTo
                .ApprovalNote = oCustomClass.Notes
                .AprovalType = Parameter.Approval.ETransactionType.WriteOff_Approval
            End With
            strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

            Dim params() As SqlParameter = New SqlParameter(7) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID.Trim

            params(2) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(2).Value = oCustomClass.BusinessDate

            params(3) = New SqlParameter(PARAM_REASONTYPEID, SqlDbType.VarChar, 10)
            params(3).Value = oCustomClass.ReasonTypeID

            params(4) = New SqlParameter(PARAM_REASONID, SqlDbType.VarChar, 10)
            params(4).Value = oCustomClass.ReasonID

            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId

            params(6) = New SqlParameter(PARAM_APPROVALNO, SqlDbType.VarChar, 50)
            params(6).Value = strApprovalNo

            params(7) = New SqlParameter(PARAM_NOTES, SqlDbType.Text)
            params(7).Value = oCustomClass.Notes


            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, REQUESTWO, params)
            objTransaction.Commit()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        Catch exp As Exception
            objTransaction.Rollback()
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
            WriteException("FullPrepayRequest", "SaveFullPrepayRequest", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Sub
    Public Function WriteOffInquiry(ByVal oCustomClass As Parameter.WriteOff) As Parameter.WriteOff
        Dim params() As SqlParameter = New SqlParameter(4) {}
        With oCustomClass
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = .SortBy
            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output
        End With
        Try
            oCustomClass.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, oCustomClass.SpName, params).Tables(0)
            oCustomClass.TotalRecord = CType(params(4).Value, Int16)
            Return oCustomClass
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")
        Catch exp As Exception
            WriteException("StopAccrued", "StopAccruedInquiry", exp.Message + exp.StackTrace)

        End Try
    End Function
    Public Function WOInquiryReport(ByVal customclass As Parameter.WriteOff) As Parameter.WriteOff
        Dim params As SqlParameter
        Dim SPName As String
        params = New SqlParameter("@SearchBy", SqlDbType.VarChar, 2000)
        params.Value = customclass.Search

        Try
            customclass.ListDataReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, customclass.SpName, params)
            Return customclass

        Catch exp As Exception
            WriteException("WriteOff", "WriteOffInquiry", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function WriteOffView(ByVal oCustomClass As Parameter.WriteOff) As Parameter.WriteOff

        Dim params() As SqlParameter = New SqlParameter(0) {}
        Dim objread As SqlDataReader
        params(0) = New SqlParameter("@RequestWONo", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.RequestWONo

        objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, VIEWWRITEOFF, params)
        If objread.Read Then
            With oCustomClass
                .BranchId = objread("BranchID").ToString
                .CustomerName = objread("Name").ToString
                .BranchName = objread("BranchFullName").ToString
                .Agreementno = objread("AgreementNo").ToString
                .CustomerID = objread("CustomerID").ToString
                .CustomerType = objread("CustomerType").ToString
                .CrossDefault = CBool(objread("CrossDefault"))
                .DefaultStatus = objread("DefaultStatus").ToString
                .ApplicationID = objread("ApplicationID").ToString
                .ValueDate = CDate(objread("RequestDate"))
                .OutstandingPrincipal = CDbl(objread("OSPrincipalAmount"))
                .InstallmentDue = CDbl(objread("OSInstallmentDue"))
                .InstallmentCollFee = CDbl(objread("OSInstallCollectionFee"))
                .InsuranceCollFee = CDbl(objread("OSInsurCollectionFee"))
                .PDCBounceFee = CDbl(objread("OSPDCBounceFee"))
                .STNKRenewalFee = CDbl(objread("OSSTNKRenewalFee"))
                .InsuranceClaimExpense = CDbl(objread("OSInsuranceClaimExpense"))
                .RepossessionFee = CDbl(objread("OSRepossesFee"))
                .OutStandingInterest = CDbl(objread("OSInterestAmount"))
                .AccruedInterest = CDbl(objread("AccruedInterest"))
                .LcInstallment = CDbl(objread("OSLCInstallment"))
                .LcInsurance = CDbl(objread("OSLCInsurance"))
                .InsuranceDue = CDbl(objread("OSInsuranceDue"))
                .TotalAmount = CDbl(objread("WOTotalAmount"))
                .ContractStatus = objread("ContractStatus").ToString
                .Notes = objread("Notes").ToString
                .ReasonTypeID = objread("ReasonTypeID").ToString
                .ReasonDescription = objread("Description").ToString
                .UserApproval = CStr(objread("UserApproval"))
                .UserRequest = CStr(objread("UserRequest"))
                .ApprovalStatus = CStr(objread("ApprovalStatus"))
                .LastPayment = CDate(objread("LastPayment"))
                .RequestBy = objread("RequestBy").ToString
                .StatusDate = CDate(objread("StatusDate"))
                .NAAmount = CDbl(objread("OSNAAmount"))
                .PastDueDays = CInt(objread("PastDueDays"))
                .RequestWONo = CStr(objread("RequestWONo"))
            End With
        End If
        objread.Close()
        Return oCustomClass
    End Function

    Public Sub WriteOffEditPotensiTagih(ByVal oCustomClass As Parameter.WriteOff)
        Dim params(2) As SqlParameter
        Try
            With oCustomClass
                params(0) = New SqlParameter("@RequestWONo", SqlDbType.Char, 20)
                params(0).Value = .RequestWONo
                params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params(1).Value = .ApplicationID
                params(2) = New SqlParameter("@PotensiTagih", SqlDbType.Char, 1)
                params(2).Value = .PotensiTagih
            End With
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spWOPotensiTagihEdit", params)
        Catch exp As Exception
            WriteException("CustomClass", "WriteOffEditPotensiTagih", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to edit the selected record")
        End Try

    End Sub
End Class
