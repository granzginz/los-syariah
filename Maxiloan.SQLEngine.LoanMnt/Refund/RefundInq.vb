

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class RefundInq
    Inherits AccMntBase
    Private Const spRefundReport As String = "spRefundInqReport"
    Private Const spRefundView As String = "spRefundViewList"
    Private Const spRefundViewApproval As String = "spRefundViewListApproval"

#Region "ReportRefundInq"
    Public Function ReportRefundInq(ByVal customclass As Parameter.Refund) As Parameter.Refund
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRefundReport, params)
            Return customclass
        Catch exp As Exception
            WriteException("RefundInq", "ReportRefundInq", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "RefundView"

    Public Function RefundView(ByVal customclass As Parameter.Refund) As Parameter.Refund
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            If Not customclass.BranchId Is Nothing Then
                params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
                params(0).Value = customclass.BranchId
            Else
                params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
                params(0).Value = ""
            End If
            If Not customclass.RefundTo Is Nothing Then
                params(1) = New SqlParameter("@RefundCustomerNo", SqlDbType.VarChar, 20)
                params(1).Value = customclass.RefundNo
            Else
                params(1) = New SqlParameter("@RefundCustomerNo", SqlDbType.VarChar, 20)
                params(1).Value = ""
            End If
            If Not customclass.ApplicationID Is Nothing Then
                params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
                params(2).Value = customclass.ApplicationID
            Else
                params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
                params(2).Value = ""
            End If


            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spRefundView, params)



            If objread.Read Then

                With customclass
                    .RefundNo = CType(objread("RefundCustomerNo"), String)
                    .BranchId = CType(objread("branchid"), String)
                    .RequestDate = CType(objread("RefundRequestDate"), Date)
                    .RefundAmount = CType(objread("RefundAmount"), Double)
                    .ApplicationID = CType(objread("Applicationid"), String)

                    .BankID = CType(objread("BankID"), String)
                    .bankName = CType(objread("BankName"), String)
                    .BankBranchID = CType(objread("BankBranchID"), Integer)
                    .BankBranchName = CType(objread("BankBranch"), String)
                    .bankAccountNo = CType(objread("bankAccountNo"), String)
                    .BankAccountName = CType(objread("BankAccountName"), String)
                    .ReasonDesc = CType(objread("reasondesc"), String)
                    .Notes = CType(objread("Notes"), String)
                    .StatusRefund = CType(objread("RefundStatusdesc"), String)
                    .StatusDate = CType(objread("StatusDate"), Date)
                    .Requestby = CType(objread("RequestBy"), String)
                    .Agreementno = CType(objread("agreementno"), String)
                    .CustomerID = CType(objread("customerid"), String)
                    .CustomerName = CType(objread("customername"), String)
                    .contractprepaidamount = CType(objread("contractprepaidamount"), Double)
                    .PrepaidRefundable = CType(objread("Prepaidrefundable"), Double)
                    .InstallmentDue = CType(objread("InstallmentDue"), Double)
                    .InsuranceDue = CType(objread("InsuranceDue"), Double)
                    .LcInstallment = CType(objread("LCInstall"), Double)
                    .LcInsurance = CType(objread("LCInsur"), Double)
                    .InstallmentCollFee = CType(objread("OSInstallCollectionFee"), Double)
                    .InsuranceCollFee = CType(objread("OSInsurCollectionFee"), Double)
                    .PDCBounceFee = CType(objread("OSPDCBounceFee"), Double)
                    .STNKRenewalFee = CType(objread("OSSTNKRenewalFee"), Double)
                    .InsuranceClaimExpense = CType(objread("OSInsuranceClaimExpense"), Double)
                    .RepossessionFee = CType(objread("OSRepossesFee"), Double)
                    .ApprovedBy = CType(objread("userapproval"), String)
                    .RefundNo = CType(objread("RefundCustomerNo"), String)
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("RefundInq", "ReportRefundInq", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function RefundViewApproval(ByVal ocustomclass As Parameter.Refund) As Parameter.Refund
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try

            params(0) = New SqlParameter("@RefundCustomerNo", SqlDbType.VarChar, 20)
            params(0).Value = ocustomclass.RefundNo

            'If Not customclass.ApplicationID Is Nothing Then
            '    params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            '    params(2).Value = customclass.ApplicationID
            'Else
            '    params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            '    params(2).Value = ""
            'End If


            objread = SqlHelper.ExecuteReader(ocustomclass.strConnection, CommandType.StoredProcedure, spRefundViewApproval, params)



            If objread.Read Then

                With ocustomclass
                    .RefundNo = CType(objread("RefundCustomerNo"), String)
                    .BranchId = CType(objread("branchid"), String)
                    .RequestDate = CType(objread("RefundRequestDate"), Date)
                    .RefundAmount = CType(objread("RefundAmount"), Double)
                    .ApplicationID = CType(objread("Applicationid"), String)

                    .BankID = CType(objread("BankID"), String)
                    .bankName = CType(objread("BankName"), String)
                    .BankBranchID = CType(objread("BankBranchID"), Integer)
                    .BankBranchName = CType(objread("BankBranch"), String)
                    .bankAccountNo = CType(objread("bankAccountNo"), String)
                    .BankAccountName = CType(objread("BankAccountName"), String)
                    .ReasonDesc = CType(objread("reasondesc"), String)
                    .Notes = CType(objread("Notes"), String)
                    .StatusRefund = CType(objread("RefundStatusdesc"), String)
                    .StatusDate = CType(objread("StatusDate"), Date)
                    .Requestby = CType(objread("RequestBy"), String)
                    .Agreementno = CType(objread("agreementno"), String)
                    .CustomerID = CType(objread("customerid"), String)
                    .CustomerName = CType(objread("customername"), String)
                    .contractprepaidamount = CType(objread("contractprepaidamount"), Double)
                    .PrepaidRefundable = CType(objread("Prepaidrefundable"), Double)
                    .InstallmentDue = CType(objread("InstallmentDue"), Double)
                    .InsuranceDue = CType(objread("InsuranceDue"), Double)
                    .LcInstallment = CType(objread("LCInstall"), Double)
                    .LcInsurance = CType(objread("LCInsur"), Double)
                    .InstallmentCollFee = CType(objread("OSInstallCollectionFee"), Double)
                    .InsuranceCollFee = CType(objread("OSInsurCollectionFee"), Double)
                    .PDCBounceFee = CType(objread("OSPDCBounceFee"), Double)
                    .STNKRenewalFee = CType(objread("OSSTNKRenewalFee"), Double)
                    .InsuranceClaimExpense = CType(objread("OSInsuranceClaimExpense"), Double)
                    .RepossessionFee = CType(objread("OSRepossesFee"), Double)
                    .ApprovedBy = CType(objread("userapproval"), String)
                    .RefundNo = CType(objread("RefundCustomerNo"), String)
                End With
            End If
            objread.Close()
            Return ocustomclass
        Catch exp As Exception
            WriteException("RefundInq", "ReportRefundInq", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
End Class
