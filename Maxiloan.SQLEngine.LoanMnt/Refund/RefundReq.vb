


#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class RefundReq
    Inherits AccMntBase
#Region "Constanta"
    Private Const spListRequest As String = "spAgreementList"
    Private Const spSaveRequest As String = "spRefundReq"
    Private Const spListRequestCust As String = "spRefundReqListCust"
#End Region

#Region "RefundReqList"

    Public Function RefundReqList(ByVal customclass As Parameter.Refund) As Parameter.Refund
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            params(0) = New SqlParameter("@applicationid", SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID


            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spListRequest, params)
            If objread.Read Then

                With customclass
                    .Agreementno = CType(objread("AgreementNo"), String)
                    .CustomerName = CType(objread("Name"), String)
                    .CustomerID = CType(objread("CustomerID"), String)
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("RefundReq", "RefundReqList", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "RefundReqListCust"

    Public Function RefundReqListCust(ByVal customclass As Parameter.Refund) As Parameter.Refund
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            params(0) = New SqlParameter("@applicationid", SqlDbType.VarChar, 20)
            params(0).Value = customclass.ApplicationID


            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spListRequestCust, params)

            If objread.Read Then

                With customclass
                    .BankID = CType(objread("BankID"), String)
                    .BankBranchID = CType(objread("BankBranchID"), String)
                    .bankAccountNo = CType(objread("AccountNo"), String)
                    .BankAccountName = CType(objread("AccountName"), String)
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("RefundReq", "RefundReqList", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "Not Use"
    Public Function SaveRefundReq(ByVal customclass As Parameter.Refund) As Parameter.Refund
        'Dim params() As SqlParameter = New SqlParameter(6) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim oApprovalID As New maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim objTransaction As SqlTransaction
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            objTransaction = oConnection.BeginTransaction
            With oEntitiesApproval
                .ApprovalTransaction = objTransaction
                .BranchId = customclass.BranchId
                .SchemeID = "RFND"
                .RequestDate = customclass.BusinessDate
                .UserRequest = customclass.Requestby
                .TransactionNo = customclass.ApplicationID
                .ApprovalNote = customclass.Notes
                .ApprovalValue = customclass.RefundAmount
                .UserRequest = customclass.LoginId
                .UserApproval = customclass.Requestby
                .ApprovalNote = customclass.Notes
                .AprovalType = Parameter.Approval.ETransactionType.ReFund_Approval
            End With
            customclass.approvalno = oApprovalID.RequestForApproval(oEntitiesApproval)
        Catch exp As Exception
            WriteException("RefundReq", "SaveRefundReq", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update Approval")
        Finally

        End Try
    End Function
#End Region

#Region "SaveRefundReqH"

    Public Function SaveRefundReqH(ByVal customclass As Parameter.Refund) As Parameter.Refund
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim oApprovalID As New maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim objTransaction As SqlTransaction = Nothing

        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            objTransaction = oConnection.BeginTransaction
            With oEntitiesApproval
                .ApprovalTransaction = objTransaction
                .BranchId = customclass.BranchId
                .SchemeID = "RFND"
                .RequestDate = customclass.BusinessDate
                .TransactionNo = customclass.ApplicationID
                .ApprovalValue = customclass.RefundAmount
                .UserRequest = customclass.Requestby
                .UserApproval = customclass.ApprovedBy
                .ApprovalNote = customclass.Notes
                .AprovalType = Parameter.Approval.ETransactionType.ReFund_Approval
            End With
            customclass.approvalno = oApprovalID.RequestForApproval(oEntitiesApproval)

            Dim params() As SqlParameter = New SqlParameter(16) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@RefundRequestDate", SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter("@RefundAmount", SqlDbType.Decimal)
            params(2).Value = customclass.RefundAmount

            params(3) = New SqlParameter("@Applicationid", SqlDbType.VarChar, 20)
            params(3).Value = customclass.ApplicationID

            params(4) = New SqlParameter("@BankID", SqlDbType.VarChar, 50)
            params(4).Value = customclass.BankID

            params(5) = New SqlParameter("@BankBranchID", SqlDbType.SmallInt)
            params(5).Value = customclass.BankBranchID

            params(6) = New SqlParameter("@BankAccountNo", SqlDbType.VarChar, 50)
            params(6).Value = customclass.bankAccountNo

            params(7) = New SqlParameter("@BankAccountName", SqlDbType.VarChar, 50)
            params(7).Value = customclass.BankAccountName

            params(8) = New SqlParameter("@ReasonTypeID", SqlDbType.Char, 5)
            params(8).Value = customclass.ReasonTypeID

            params(9) = New SqlParameter("@ReasonID", SqlDbType.VarChar, 10)
            params(9).Value = customclass.ReasonID

            params(10) = New SqlParameter("@Notes", SqlDbType.VarChar, 100)
            params(10).Value = customclass.Notes

            params(11) = New SqlParameter("@StatusDate", SqlDbType.DateTime)
            params(11).Value = customclass.BusinessDate

            params(12) = New SqlParameter("@RequestBy", SqlDbType.VarChar, 20)
            params(12).Value = customclass.Requestby

            params(13) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
            params(13).Value = customclass.approvalno

            params(14) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(14).Direction = ParameterDirection.Output

            params(15) = New SqlParameter("@BankName", SqlDbType.VarChar, 50)
            params(15).Value = customclass.bankName

            params(16) = New SqlParameter("@BankBranchName", SqlDbType.VarChar, 50)
            params(16).Value = customclass.BankBranchName

            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spSaveRequest, params)
            objTransaction.Commit()

        Catch exp As Exception
            objTransaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Function

#End Region
End Class
