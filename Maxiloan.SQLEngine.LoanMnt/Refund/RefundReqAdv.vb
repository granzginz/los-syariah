
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class RefundReqAdv
    Inherits AccMntBase
#Region "Constanta"
    Private Const spListRequestRefundAdv As String = "spRequestRefundAdvance"
    Private Const spSelectedRequestRefundAdv As String = "spRequestRefundAdvanceSelected"
    Private Const spGetProduct As String = "spRequestRefundAdvGetProduct"
    Private Const spSaveH As String = "spRequestRefundAdvSaveH"
    Private Const spSaveD As String = "spRequestRefundAdvSaveD"
    Private Const spGetRequestNo As String = "spRequestRefundAdvGetRequestNo"
    Private Const spListInqRefundAdv As String = "spInquiryRefundAdvance"
    Private Const spListInqRefundAdvByOSRefund As String = "spInquiryRefundAdvanceByOSRefund"
    Private Const spListReportByRefund As String = "spListingReportByRefund"
    Private Const spListReportByOSRefund As String = "spListingReportByOSRefund"
    Private Const spView1 As String = "spViewRefundAdvance1"
    Private Const spView2 As String = "spViewRefundAdvance2"
#End Region

    '========================== Request ===================================
#Region "RefundReqListAdv"
    Public Function RefundReqListAdv(ByVal customclass As Parameter.Refund) As Parameter.Refund
        Dim objread As SqlDataReader
        Dim oReturnValue As New Parameter.Refund
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 3000)
            params(0).Value = customclass.WhereCond
            params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 3000)
            params(1).Value = customclass.SortBy

            oReturnValue.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListRequestRefundAdv, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("RefundReqAdvance", "RefundReqAdvanceList", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "SelectedRefundReqAdv"
    Public Function SelectedRefundReqAdv(ByVal customclass As Parameter.Refund) As Parameter.Refund
        Dim objread As SqlDataReader
        Dim oReturnValue As New Parameter.Refund
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 3000)
            params(0).Value = customclass.WhereCond

            oReturnValue.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spSelectedRequestRefundAdv, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("RefundReqAdvanceSelected", "RefundReqAdvanceSelected", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "GetProduct"
    Public Function GetProduct(ByVal customclass As Parameter.Refund) As Parameter.Refund
        Dim oReturnValue As New Parameter.Refund
        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spGetProduct).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("GetProduct", "GetProduct", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region
#Region "SaveRefundReqAdv"
    Public Function SaveRefundReqAdv(ByVal customclass As Parameter.Refund, ByVal oData1 As DataTable) As Parameter.Refund
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim oApprovalID As New maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim objTransaction As SqlTransaction
        Dim intloop As Integer
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            objTransaction = oConnection.BeginTransaction
            Dim params3() As SqlParameter = New SqlParameter(2) {}
            params3(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params3(0).Value = customclass.BranchId
            params3(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params3(1).Value = customclass.BusinessDate
            params3(2) = New SqlParameter("@RequestNo", SqlDbType.Char, 20)
            params3(2).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spGetRequestNo, params3)

            With oEntitiesApproval
                .ApprovalTransaction = objTransaction
                .BranchId = customclass.BranchId
                .SchemeID = "RADV"
                .RequestDate = customclass.BusinessDate
                .TransactionNo = CStr(params3(2).Value)
                .ApprovalValue = customclass.TotalRefund
                .UserRequest = customclass.Requestby
                .UserApproval = customclass.ApprovedBy
                .ApprovalNote = customclass.Notes
                .AprovalType = Parameter.Approval.ETransactionType.Refund_Advance
            End With
            customclass.approvalno = oApprovalID.RequestForApproval(oEntitiesApproval)

            Dim params() As SqlParameter = New SqlParameter(14) {}
            Dim params1() As SqlParameter = New SqlParameter(5) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@RequestDate", SqlDbType.DateTime)
            params(1).Value = customclass.RequestDate

            params(2) = New SqlParameter("@BankID", SqlDbType.Char, 5)
            params(2).Value = customclass.BankID

            params(3) = New SqlParameter("@BankBranchID", SqlDbType.VarChar, 50)
            params(3).Value = customclass.BankBranchID

            params(4) = New SqlParameter("@BankAccountNo", SqlDbType.VarChar, 50)
            params(4).Value = customclass.bankAccountNo

            params(5) = New SqlParameter("@BankAccountName", SqlDbType.VarChar, 50)
            params(5).Value = customclass.BankAccountName

            params(6) = New SqlParameter("@TotalRefund", SqlDbType.Decimal)
            params(6).Value = customclass.TotalRefund

            params(7) = New SqlParameter("@RefundStatus", SqlDbType.Char, 1)
            params(7).Value = customclass.RefundStatus

            params(8) = New SqlParameter("@Notes", SqlDbType.VarChar, 100)
            params(8).Value = customclass.Notes

            params(9) = New SqlParameter("@StatusDate", SqlDbType.DateTime)
            params(9).Value = customclass.BusinessDate

            params(10) = New SqlParameter("@RequestBy", SqlDbType.VarChar, 20)
            params(10).Value = customclass.Requestby

            params(11) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
            params(11).Value = customclass.approvalno

            params(12) = New SqlParameter("@RefundToName", SqlDbType.VarChar, 50)
            params(12).Value = customclass.RefundToName

            params(13) = New SqlParameter("@RequestNo", SqlDbType.Char, 20)
            params(13).Value = CStr(params3(2).Value)

            params(14) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 50)
            params(14).Value = customclass.ReferenceNo
            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spSaveH, params)

            Dim strResult As String
            If oData1.Rows.Count > 0 Then
                params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params1(1) = New SqlParameter("@RequestNo", SqlDbType.Char, 20)
                params1(2) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params1(3) = New SqlParameter("@RefundAmount", SqlDbType.Decimal)
                params1(4) = New SqlParameter("@OSRefundAmount", SqlDbType.Decimal)
                params1(5) = New SqlParameter("@OS1", SqlDbType.Decimal)

                For intloop = 0 To oData1.Rows.Count - 1
                    params1(0).Value = customclass.BranchId
                    params1(1).Value = params3(2).Value
                    params1(2).Value = oData1.Rows(intloop).Item("ApplicationID")
                    params1(3).Value = oData1.Rows(intloop).Item("RefundAmount")
                    params1(4).Value = oData1.Rows(intloop).Item("OSRefundAmount")
                    params1(5).Value = oData1.Rows(intloop).Item("OS1")
                    SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spSaveD, params1)
                    strResult = CType(params1(5).Value, String)
                Next
            End If

            objTransaction.Commit()
        Catch exp As Exception
            objTransaction.Rollback()
            WriteException("SaveRefundReqAdv", "SaveRefundReq", exp.Message + exp.StackTrace)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Function
#End Region

    '========================== Inquiry ===================================
#Region "RefundInqListAdv"
    Public Function RefundInqListAdv(ByVal customclass As Parameter.Refund) As Parameter.Refund
        Dim objread As SqlDataReader
        Dim oReturnValue As New Parameter.Refund
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage
            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize
            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond
            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 1000)
            params(3).Value = customclass.SortBy
            params(4) = New SqlParameter("@TotalRecord", SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output

            oReturnValue.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListInqRefundAdv, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Integer)
            Return oReturnValue
        Catch exp As Exception
            WriteException("RefundInqAdvance", "RefundInqListAdv", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "RefundInqListAdvByOSRefund"
    Public Function RefundInqListAdvByOSRefund(ByVal customclass As Parameter.Refund) As Parameter.Refund
        Dim objread As SqlDataReader
        Dim oReturnValue As New Parameter.Refund
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage
            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize
            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond
            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 1000)
            params(3).Value = customclass.SortBy
            params(4) = New SqlParameter("@TotalRecord", SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output

            oReturnValue.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListInqRefundAdvByOSRefund, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Integer)
            Return oReturnValue
        Catch exp As Exception
            WriteException("RefundInqAdvanceByOSRefund", "RefundInqListAdvByOSRefund", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "ListingReportByRefund"
    Public Function ListingReportByRefund(ByVal customclass As Parameter.Refund) As Parameter.Refund
        Dim objread As SqlDataReader
        Dim oReturnValue As New Parameter.Refund
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond
            params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 1000)
            params(1).Value = customclass.SortBy

            oReturnValue.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListReportByRefund, params)
            Return oReturnValue
        Catch exp As Exception
            WriteException("ListingReportByRefund", "ListingReportByRefund", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "ListingReportByOSRefund"
    Public Function ListingReportByOSRefund(ByVal customclass As Parameter.Refund) As Parameter.Refund
        Dim objread As SqlDataReader
        Dim oReturnValue As New Parameter.Refund
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond
            params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 1000)
            params(1).Value = customclass.SortBy

            oReturnValue.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListReportByOSRefund, params)
            Return oReturnValue
        Catch exp As Exception
            WriteException("ListingReportByOSRefund", "ListingReportByOSRefund", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "ViewRefundAdvance1"
    Public Function ViewRefundAdvance1(ByVal customclass As Parameter.Refund) As Parameter.Refund
        Dim objread As SqlDataReader
        Dim oReturnValue As New Parameter.Refund
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond
            params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 1000)
            params(1).Value = customclass.SortBy

            oReturnValue.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spView1, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("View1", "View1", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "ViewRefundAdvance2"
    Public Function ViewRefundAdvance2(ByVal customclass As Parameter.Refund) As Parameter.Refund
        Dim objreader As SqlDataReader
        Dim oReturnValue As New Parameter.Refund
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            objreader = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spView2, params)
            If objreader.Read Then
                With oReturnValue
                    .RefundToName = objreader("RefundToName").ToString
                    .BankID = objreader("BankID").ToString
                    .BankBranchID = objreader("BankBranchID").ToString
                    .bankAccountNo = objreader("AccountNo").ToString
                    .BankAccountName = objreader("AccountName").ToString
                    .ReferenceNo = objreader("ReferenceNo").ToString
                    .Notes = objreader("Notes").ToString
                End With
            End If
            objreader.Close()
            Return oReturnValue
        Catch exp As Exception
            WriteException("View2", "View2", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "ViewRefundDetail"
    Public Function ViewRefundDetail(ByVal customclass As Parameter.Refund) As Parameter.Refund
        Dim objreader As SqlDataReader
        Dim oReturnValue As New Parameter.Refund
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            oReturnValue.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spRequestRefundAdvanceDetailView", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("ViewRefundDetail", "ViewRefundDetail", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
End Class
