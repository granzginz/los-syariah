

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class SuspendReversal
    Inherits AccMntBase
    Private Const spListSuspendRev As String = "spSuspendReversalPaging"
    Private Const spListSuspendReverse As String = "spSuspendReversalList"
    Private Const spSaveSuspendReverse As String = "spSuspendReversal"
    Private Const spCheckStatus As String = "spCheckDuplikasiSuspendR"

    Public Function SuspendReversalList(ByVal customclass As Parameter.SuspendReceive) As Parameter.SuspendReceive
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListSuspendRev, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("SuspendReversal", "SuspendReversalList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function SuspendReverse(ByVal customclass As Parameter.SuspendReceive) As Parameter.SuspendReceive
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@SuspendNO", SqlDbType.VarChar, 20)
            params(0).Value = customclass.SuspendNo

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spListSuspendReverse, params)
            If objread.Read Then
                With customclass
                    .SuspendNo = CType(objread("SuspendNo"), String)
                    .postingdate = CType(objread("Postingdate"), Date)
                    .BankAccountID = CType(objread("bankaccountid"), String)
                    .bankAccountName = CType(objread("bankaccountname"), String)
                    .ReferenceNo = CType(objread("TransferRefNo"), String)
                    .ValueDate = CType(objread("valuedate"), Date)
                    .AmountRec = CType(objread("amount"), Double)
                    .description = CType(objread("description"), String)
                    .StatusDate = CType(objread("statusdate"), Date)
                    .VoucherNo = CType(objread("VoucherNoRcv"), String)
                    .Agreementno = CType(objread("agreementno"), String)
                    .CustomerName = CType(objread("Customername"), String)
                    .SuspendStatus = CType(objread("SuspendStatus"), String)
                    .SuspendStatusdesc = CType(objread("SuspendStatusdesc"), String)

                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("SuspendReversal", "SuspendReverse", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub SaveSuspendReverse(ByVal customclass As Parameter.SuspendReceive)
        Dim params() As SqlParameter = New SqlParameter(9) {}
        Dim params1() As SqlParameter = New SqlParameter(4) {}
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            If customclass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = customclass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = customclass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = customclass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"
                SqlHelper.ExecuteScalar(objtrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                customclass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(1).Value = customclass.BankAccountID

            params(2) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(2).Value = customclass.LoginId

            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = customclass.BusinessDate

            params(4) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(4).Value = customclass.AmountRec

            params(5) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(5).Value = customclass.ValueDate

            params(6) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.Char, 20)
            params(6).Value = customclass.ReferenceNo

            params(7) = New SqlParameter("@Notes", SqlDbType.Char, 50)
            params(7).Value = customclass.description

            params(8) = New SqlParameter(PARAM_COMPANYID, SqlDbType.Char, 3)
            params(8).Value = customclass.CoyID

            params(9) = New SqlParameter("@SuspendNo", SqlDbType.VarChar, 20)
            params(9).Value = customclass.SuspendNo



            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spSaveSuspendReverse, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("SuspendReversal", "SaveSuspendReverse", exp.Message + exp.StackTrace)
        End Try
    End Sub

    Public Function SplitTitipanList(ByVal customclass As Parameter.SuspendReceive) As Parameter.SuspendReceive
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSplitTitipanPaging", params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("SuspendReversal", "spSplitTitipanPaging", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
