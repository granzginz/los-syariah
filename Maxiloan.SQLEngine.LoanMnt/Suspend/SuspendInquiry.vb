
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class SuspendInquiry
    Inherits AccMntBase
    Private Const spListSuspendRev As String = "spSuspendReversalPaging"
    Private Const spSuspendReport As String = "spSuspendInqReport"
    Private Const spGetBankAccountAll As String = "spGetBankAccountAll"

    Public Function SuspendInquiryList(ByVal customclass As Parameter.SuspendReceive) As Parameter.SuspendReceive
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListSuspendRev, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("SuspendInquiry", "SuspendInquiryList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ReportSuspendInq(ByVal customclass As Parameter.SuspendReceive) As Parameter.SuspendReceive
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spSuspendReport, params)
            Return customclass
        Catch exp As Exception
            WriteException("SuspendInquiry", "SuspendInquiryList", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetBankAccountAll(ByVal customclass As Parameter.SuspendReceive) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.BranchId

        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spGetBankAccountAll, params).Tables(0)
        Catch exp As Exception
            WriteException("SuspendInquiry", "GetBankAccountAll", exp.Message + exp.StackTrace)
        End Try
    End Function

End Class
