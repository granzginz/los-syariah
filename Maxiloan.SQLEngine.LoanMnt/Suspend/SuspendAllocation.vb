
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class SuspendAllocation : Inherits maxiloan.SQLEngine.LoanMnt.AccMntBase

    Private Const PROC_SUSPENDALLOCATION As String = "spSuspendAllocation"
    Private Const spCheckStatus As String = "spCheckDuplikasiSuspend"
    Private Const _APPLICATIONID As String = "@applicationid"
    Private Const _VALUEDATE As String = "@valuedate"
    Private Const _BUSINESSDATE As String = "@businessdate"
    Private Const _BRANCHID As String = "@branchid"
#Region "Const : Received From"
    Private Const _RECEIVEDFROM As String = "@receivedfrom"
    Private Const _COLLECTORID As String = "@collectorid"
    Private Const _TTSNO As String = "@ttsno"
#End Region

    Private Const _SUBSYSTEM As String = "@subsystem"
    Private Const _WOP As String = "@wop"
    Private Const _BANKACCOUNTID As String = "@bankaccountid"
    Private Const _REFERENCENO As String = "@referenceno"

#Region "Const : Currency"
    Private Const _CURRENCYRATE As String = "@currencyrate"
    Private Const _CURRENCYID As String = "@currencyid"
#End Region

#Region "Const : Amount Pay"
    Private Const _AMOUNTTRANSFER As String = "@amounttransfer"

    Private Const _INSTALLMENTDUEPAID As String = "@installmentduepaid"
    Private Const _INSURANCEDUEPAID As String = "@insuranceduepaid"
    Private Const _LCINSURANCEPAID As String = "@lcinsurancepaid"
    Private Const _LCINSTALLMENTPAID As String = "@lcinstallmentpaid"
    Private Const _OTHERINCOME As String = "@otherincome"
    Private Const _PREPAID As String = "@prepaid"
#End Region

#Region "Const: Credit Card"
    Private Const _CARDNAME As String = "@cardname"
    Private Const _CARDID As String = "@cardid"
    Private Const _CARDNUMBER As String = "@cardnumber"
    Private Const _AUTHORIZENUMBER As String = "@authorizenumber"
    Private Const _CHARGERATETOCUST As String = "@chargeratetocust"
    Private Const _CHARGEAMTTOCUST As String = "@chargeamttocust"
    Private Const _PAIDAMTINCLUDECARD As String = "@paidamtincludecardchg"
#End Region

    Private Const _LOGINID As String = "@loginid"

    Private Const _TRANSACTIONID As String = "@transactionid"
    Private Const _NOTES As String = "@notes"

    Private Const _PROCESSTYPE As String = "@processtype"

    Private m_connection As SqlConnection

    Public Sub SuspendAllocationSave(ByVal oCustomClass As Parameter.InstallRcv)
        Dim objTrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(36) {}

            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.CoyID
            params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.ApplicationID
            params(3) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.ValueDate
            params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(4).Value = oCustomClass.BusinessDate
            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId

            params(6) = New SqlParameter(PARAM_INSTALLMENTDUEPAID, SqlDbType.Decimal)
            params(6).Value = oCustomClass.InstallmentDue
            params(7) = New SqlParameter(PARAM_INSTALLMENTDUEDESC, SqlDbType.VarChar, 50)
            params(7).Value = oCustomClass.InstallmentDueDesc
            params(8) = New SqlParameter(PARAM_LCINSTALLMENTPAID, SqlDbType.Decimal)
            params(8).Value = oCustomClass.LcInstallment
            params(9) = New SqlParameter(PARAM_LCINSTALLMENTDESC, SqlDbType.VarChar, 50)
            params(9).Value = oCustomClass.LcInstallmentDesc
            params(10) = New SqlParameter(PARAM_INSTALLMENTCOLLPAID, SqlDbType.Decimal)
            params(10).Value = oCustomClass.InstallmentCollFee
            params(11) = New SqlParameter(PARAM_INSTALLMENTCOLLDESC, SqlDbType.VarChar, 50)
            params(11).Value = oCustomClass.InstallmentCollFeeDesc

            params(12) = New SqlParameter(PARAM_INSURANCEDUEPAID, SqlDbType.Decimal)
            params(12).Value = oCustomClass.InsuranceDue
            params(13) = New SqlParameter(PARAM_INSURANCEDUEDESC, SqlDbType.VarChar, 50)
            params(13).Value = oCustomClass.InsuranceDueDesc
            params(14) = New SqlParameter(PARAM_LCINSURANCEPAID, SqlDbType.Decimal)
            params(14).Value = oCustomClass.LcInsurance
            params(15) = New SqlParameter(PARAM_LCINSURANCEDESC, SqlDbType.VarChar, 50)
            params(15).Value = oCustomClass.LcInsuranceDesc
            params(16) = New SqlParameter(PARAM_INSURANCECOLLPAID, SqlDbType.Decimal)
            params(16).Value = oCustomClass.InsuranceCollFee
            params(17) = New SqlParameter(PARAM_INSURANCECOLLDESC, SqlDbType.VarChar, 50)
            params(17).Value = oCustomClass.InsuranceCollFeeDesc

            params(18) = New SqlParameter(PARAM_PDCBOUNCEFEEPAID, SqlDbType.Decimal)
            params(18).Value = oCustomClass.PDCBounceFee
            params(19) = New SqlParameter(PARAM_PDCBOUNCEFEEDESC, SqlDbType.VarChar, 50)
            params(19).Value = oCustomClass.PDCBounceFeeDesc
            params(20) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEPAID, SqlDbType.Decimal)
            params(20).Value = oCustomClass.InsuranceClaimExpense
            params(21) = New SqlParameter(PARAM_INSURANCECLAIMEXPENSEPAIDDESC, SqlDbType.VarChar, 50)
            params(21).Value = oCustomClass.InsuranceClaimExpenseDesc
            params(22) = New SqlParameter(PARAM_STNKRENEEWALFEEPAID, SqlDbType.Decimal)
            params(22).Value = oCustomClass.STNKRenewalFee
            params(23) = New SqlParameter(PARAM_STNKRENEEWALFEEDESC, SqlDbType.VarChar, 50)
            params(23).Value = oCustomClass.STNKRenewalFeeDesc
            params(24) = New SqlParameter(PARAM_REPOSSESSIONFEEPAID, SqlDbType.Decimal)
            params(24).Value = oCustomClass.RepossessionFee
            params(25) = New SqlParameter(PARAM_REPOSSESSIONFEEDESC, SqlDbType.VarChar, 50)
            params(25).Value = oCustomClass.RepossessionFeeDesc
            params(26) = New SqlParameter(PARAM_PREPAIDPAID, SqlDbType.Decimal)
            params(26).Value = oCustomClass.Prepaid
            params(27) = New SqlParameter(PARAM_PREPAIDDESC, SqlDbType.VarChar, 50)
            params(27).Value = oCustomClass.PrepaidDesc

            params(28) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            params(28).Value = oCustomClass.ReceivedFrom
            params(29) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50)
            params(29).Value = oCustomClass.ReferenceNo
            params(30) = New SqlParameter(PARAM_WOP, SqlDbType.VarChar, 2)
            params(30).Value = oCustomClass.WOP
            params(31) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(31).Value = oCustomClass.BankAccountID
            params(32) = New SqlParameter(PARAM_AMOUNTRECEIVE, SqlDbType.Decimal)
            params(32).Value = oCustomClass.AmountReceive
            params(33) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(33).Value = oCustomClass.Notes
            params(34) = New SqlParameter("@SuspendNo", SqlDbType.VarChar, 20)
            params(34).Value = oCustomClass.SuspendNo

            params(35) = New SqlParameter("@PLL", SqlDbType.Decimal)
            params(35).Value = oCustomClass.PLL
            params(36) = New SqlParameter("@PLLDesc", SqlDbType.VarChar, 100)
            params(36).Value = oCustomClass.PLLDesc

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, PROC_SUSPENDALLOCATION, params)
            objTrans.Commit()

        Catch exp As Exception
            objTrans.Rollback()
            WriteException("SuspendAllocation", "SuspendAllocationSave", exp.Message + exp.StackTrace)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Sub
    Public Function SuspendDetail(oCustom As Parameter.SuspendReceive) As Parameter.SuspendReceive
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SuspendReceive
        params(0) = New SqlParameter("@SuspendNo", SqlDbType.VarChar, 20)
        params(0).Value = oCustom.SuspendNo
        Try
            reader = SqlHelper.ExecuteReader(oCustom.strConnection, CommandType.StoredProcedure, "spGetDetailSuspend", params)
            If reader.Read Then
                With oReturnValue
                    .postingdate = CDate(reader("PostingDate").ToString)
                    .ValueDate = CDate(reader("ValueDate").ToString)
                    .AmountRec = CDbl(reader("Amount").ToString)
                    .BankAccountName = reader("BankAccountName").ToString
                    .description = reader("description").ToString
                End With
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Sub SuspendSplit(oCustomClass As Parameter.SuspendReceive)
        Dim objTrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(4) {}

            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction
            params(0) = New SqlParameter("@Amount", SqlDbType.Int)
            params(0).Value = oCustomClass.AmountRec
            params(1) = New SqlParameter("@Desc", SqlDbType.VarChar, 100)
            params(1).Value = oCustomClass.description
            params(2) = New SqlParameter("@SuspendNORef", SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.SuspendNo
            params(3) = New SqlParameter("@branchid", SqlDbType.Char, 3)
            params(3).Value = oCustomClass.BranchId
            params(4) = New SqlParameter("@businessdate", SqlDbType.DateTime)
            params(4).Value = oCustomClass.BusinessDate

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spSuspendSplit", params)
            objTrans.Commit()

        Catch exp As Exception
            objTrans.Rollback()
            WriteException("SuspendSplit", "SuspendSplit", exp.Message + exp.StackTrace)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try

    End Sub

    'penambahan alokasi suspend fact & mdkj
    Public Sub SuspendAllocationSaveFACT(ByVal oCustomClass As Parameter.InstallRcv)
        Dim objTrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(17) {}

            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.CoyID
            params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.ApplicationID
            params(3) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.ValueDate
            params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(4).Value = oCustomClass.BusinessDate
            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId

            params(6) = New SqlParameter("@PaidPrincipal", SqlDbType.Decimal)
            params(6).Value = oCustomClass.PaidPrincipal
            params(7) = New SqlParameter("@PaidInterest", SqlDbType.Decimal)
            params(7).Value = oCustomClass.PaidInterest
            params(8) = New SqlParameter("@PaidRetensi", SqlDbType.Decimal)
            params(8).Value = oCustomClass.PaidRetensi
            params(9) = New SqlParameter("@DendaPaid", SqlDbType.Decimal)
            params(9).Value = oCustomClass.DendaPaid

            params(10) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            params(10).Value = oCustomClass.ReceivedFrom
            params(11) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50)
            params(11).Value = oCustomClass.ReferenceNo
            params(12) = New SqlParameter(PARAM_WOP, SqlDbType.VarChar, 2)
            params(12).Value = oCustomClass.WOP
            params(13) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(13).Value = oCustomClass.BankAccountID
            params(14) = New SqlParameter(PARAM_AMOUNTRECEIVE, SqlDbType.Decimal)
            params(14).Value = oCustomClass.AmountReceive
            params(15) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(15).Value = oCustomClass.Notes
            params(16) = New SqlParameter("@SuspendNo", SqlDbType.VarChar, 20)
            params(16).Value = oCustomClass.SuspendNo
            params(17) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 20)
            params(17).Value = oCustomClass.InvoiceNo

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spSuspendAllocationFACT", params)
            objTrans.Commit()

        Catch exp As Exception
            objTrans.Rollback()
            WriteException("SuspendAllocation", "SuspendAllocationSaveFACT", exp.Message + exp.StackTrace)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Sub
    Public Sub SuspendAllocationSaveMDKJ(ByVal oCustomClass As Parameter.InstallRcv)
        Dim objTrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(17) {}

            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter(PARAM_COMPANYID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.CoyID
            params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.ApplicationID
            params(3) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(3).Value = oCustomClass.ValueDate
            params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(4).Value = oCustomClass.BusinessDate
            params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(5).Value = oCustomClass.LoginId

            params(6) = New SqlParameter("@PaidPrincipal", SqlDbType.Decimal)
            params(6).Value = oCustomClass.PaidPrincipal
            params(7) = New SqlParameter("@PaidInterest", SqlDbType.Decimal)
            params(7).Value = oCustomClass.PaidInterest
            params(8) = New SqlParameter("@PaidRetensi", SqlDbType.Decimal)
            params(8).Value = oCustomClass.PaidRetensi
            params(9) = New SqlParameter("@DendaPaid", SqlDbType.Decimal)
            params(9).Value = oCustomClass.DendaPaid

            params(10) = New SqlParameter(PARAM_RECEIVEFROM, SqlDbType.VarChar, 50)
            params(10).Value = oCustomClass.ReceivedFrom
            params(11) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 50)
            params(11).Value = oCustomClass.ReferenceNo
            params(12) = New SqlParameter(PARAM_WOP, SqlDbType.VarChar, 2)
            params(12).Value = oCustomClass.WOP
            params(13) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(13).Value = oCustomClass.BankAccountID
            params(14) = New SqlParameter(PARAM_AMOUNTRECEIVE, SqlDbType.Decimal)
            params(14).Value = oCustomClass.AmountReceive
            params(15) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(15).Value = oCustomClass.Notes
            params(16) = New SqlParameter("@SuspendNo", SqlDbType.VarChar, 20)
            params(16).Value = oCustomClass.SuspendNo
            params(17) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 20)
            params(17).Value = oCustomClass.InvoiceNo

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spSuspendAllocationMDKJ", params)
            objTrans.Commit()

        Catch exp As Exception
            objTrans.Rollback()
            WriteException("SuspendAllocation", "SuspendAllocationSave", exp.Message + exp.StackTrace)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Sub

    Public Function TitipanDetail(oCustom As Parameter.SuspendReceive) As Parameter.SuspendReceive
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.SuspendReceive
        params(0) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
        params(0).Value = oCustom.ReferenceNo
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(1).Value = oCustom.ApplicationID
        Try
            reader = SqlHelper.ExecuteReader(oCustom.strConnection, CommandType.StoredProcedure, "spGetDetailTitipan", params)
            If reader.Read Then
                With oReturnValue
                    .postingdate = CDate(reader("ReversalDate").ToString)
                    .ValueDate = CDate(reader("ValueDate").ToString)
                    .AmountRec = CDbl(reader("Amount").ToString)
                    .BankAccountName = reader("BankAccountName").ToString
                End With
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub TitipanSplit(oCustomClass As Parameter.SuspendReceive)
        Dim objTrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(6) {}

            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction
            params(0) = New SqlParameter("@Amount", SqlDbType.Int)
            params(0).Value = oCustomClass.AmountRec
            params(1) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ReferenceNo
            params(2) = New SqlParameter("@branchid", SqlDbType.Char, 3)
            params(2).Value = oCustomClass.BranchId
            params(3) = New SqlParameter("@businessdate", SqlDbType.DateTime)
            params(3).Value = oCustomClass.BusinessDate
            params(4) = New SqlParameter("@Nomor", SqlDbType.Int)
            params(4).Value = oCustomClass.Nomor
            params(5) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(5).Value = oCustomClass.ApplicationID

            params(6) = New SqlParameter("@txtReferenceNo", SqlDbType.VarChar, 30)
            params(6).Value = oCustomClass.txtReferenceNo

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spTitipanSplit", params)
            objTrans.Commit()

        Catch exp As Exception
            objTrans.Rollback()
            WriteException("SuspendSplit", "TitipanSplit", exp.Message + exp.StackTrace)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try

    End Sub
End Class
