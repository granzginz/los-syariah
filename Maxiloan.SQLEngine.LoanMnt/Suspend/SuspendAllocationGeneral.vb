﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class SuspendAllocationGeneral : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSuspendAllocationGeneralPaging"
    Private Const LIST_UPDATE As String = "spSuspendAllocationGeneralSave"
    Private Const LIST_SELECT_BY_SUSPENDNO As String = "spSuspendAllocationGeneralView"
    Private Const LIST_SELECT_BY_BRANCH As String = "spSuspendAllocationGeneralByBranch"
    Private Const LIST_SELECT_BY_DEPARTEMEN As String = "spSuspendAllocationGeneralByDepartement"
    Private Const IsSuspendIsBlock As String = "spGetSuspendIsBlock"
    Private Const LIST_SELECT_BLOKIR As String = "spSuspendAllocationGeneralBlokirPaging"
    Private Const LIST_UPDATE_BLOKIR As String = "spSuspendReleaseBlock"
#End Region

    Public Function GetSuspendAllocationGeneral(ByVal oCustomClass As Parameter.SuspendAllocationGeneral) As Parameter.SuspendAllocationGeneral
        Dim oReturnValue As New Parameter.SuspendAllocationGeneral
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.SuspendAllocationGeneral.GetSuspendAllocationGeneral")
        End Try
    End Function

    Public Sub SuspendAllocationGeneralSaveEdit(ByVal ocustomClass As Parameter.SuspendAllocationGeneral)
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@SuspendNo", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.SuspendNo
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(1).Value = ocustomClass.Description
        params(2) = New SqlParameter("@Amount", SqlDbType.Decimal)
        params(2).Value = ocustomClass.Amount
        params(3) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(3).Value = ocustomClass.BranchId
        params(4) = New SqlParameter("@PaymentAllocationID", SqlDbType.Char, 10)
        params(4).Value = ocustomClass.PaymentAllocationID.Trim
        params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(5).Value = ocustomClass.BusinessDate

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.SuspendAllocationGeneral.SuspendAllocationGeneralSaveEdit")
        End Try
    End Sub

    Public Function GetSuspendAllocationGeneralList(ByVal ocustomClass As Parameter.SuspendAllocationGeneral) As Parameter.SuspendAllocationGeneral
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SuspendAllocationGeneral
        params(0) = New SqlParameter("@SuspendNo", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.SuspendNo

        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT_BY_SUSPENDNO, params)
            If reader.Read Then
                oReturnValue.SuspendNo = ocustomClass.SuspendNo
                oReturnValue.BranchId = reader("BranchId").ToString
                oReturnValue.Description = reader("Description").ToString
                oReturnValue.Amount = CDec(reader("Amount").ToString)
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerCases.GetApplicatioList")
        End Try
    End Function

    Public Function GetComboDepartemen(ByVal customclass As Parameter.SuspendAllocationGeneral) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, LIST_SELECT_BY_DEPARTEMEN).Tables(0)
    End Function

    Public Function GetComboCabang(ByVal customclass As Parameter.SuspendAllocationGeneral) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, LIST_SELECT_BY_BRANCH).Tables(0)
    End Function

    Public Function GetIsBlock(ByVal customclass As Parameter.SuspendAllocationGeneral) As Parameter.SuspendAllocationGeneral
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId.Trim

            params(1) = New SqlParameter("@SuspendNo", SqlDbType.Char, 20)
            params(1).Value = customclass.SuspendNo.Trim

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, IsSuspendIsBlock, params)

            If Not IsNothing(objread) Then
                With customclass
                    If objread.Read Then
                        .IsBlock = CType(objread("IsBlock"), String)
                    End If
                    objread.Close()
                End With
            End If

            Return customclass
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerCases.GetApplicatioList")
        End Try
    End Function

    Public Function GetSuspendIsBlockList(ByVal customclass As Parameter.SuspendAllocationGeneral) As Parameter.SuspendAllocationGeneral
        Dim oReturnValue As New Parameter.SuspendAllocationGeneral
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customclass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customclass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, LIST_SELECT_BLOKIR, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.SuspendAllocationGeneral.GetSuspendIsBlockList")
        End Try
    End Function

    Public Sub ReleaseBlokirSuspend(ByVal customclass As Parameter.SuspendAllocationGeneral)
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = customclass.BranchId.Trim

        params(1) = New SqlParameter("@SuspendNo", SqlDbType.Char, 20)
        params(1).Value = customclass.SuspendNo.Trim

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, LIST_UPDATE_BLOKIR, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.SuspendAllocationGeneral.SuspendAllocationGeneralSaveEdit")
        End Try
    End Sub
End Class
