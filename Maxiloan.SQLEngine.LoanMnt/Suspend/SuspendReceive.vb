#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class SuspendReceive : Inherits AccMntBase
    Private Const spSaveSuspendReceive As String = "spSuspendReceive"

    Public Sub SaveSuspendReceive(ByVal customclass As Parameter.SuspendReceive)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(8) {}
        Dim params1() As SqlParameter = New SqlParameter(4) {}

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            If customclass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = customclass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = customclass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = customclass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "K"
                SqlHelper.ExecuteScalar(objtrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                customclass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@bankaccountid", SqlDbType.VarChar, 10)
            params(1).Value = customclass.BankAccountID

            params(2) = New SqlParameter("@loginid", SqlDbType.VarChar, 12)
            params(2).Value = customclass.LoginId

            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = customclass.BusinessDate

            params(4) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(4).Value = customclass.AmountRec

            params(5) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(5).Value = customclass.ValueDate

            params(6) = New SqlParameter("@referenceNo", SqlDbType.Char, 20)
            params(6).Value = customclass.ReferenceNo

            params(7) = New SqlParameter("@Notes", SqlDbType.Char, 50)
            params(7).Value = customclass.description

            params(8) = New SqlParameter("@CoyID", SqlDbType.Char, 3)
            params(8).Value = customclass.CoyID


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spSaveSuspendReceive, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("SuspendReceive", "SaveSuspendReceive", exp.Message + exp.StackTrace)
        End Try
    End Sub
    Public Sub SaveSuspendTransactionImplementasi(ByVal customclass As Parameter.SuspendReceive)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(8) {}
        Dim params1() As SqlParameter = New SqlParameter(4) {}

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            If customclass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = customclass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = customclass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = customclass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "K"
                SqlHelper.ExecuteScalar(objtrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                customclass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@bankaccountid", SqlDbType.VarChar, 10)
            params(1).Value = customclass.BankAccountID

            params(2) = New SqlParameter("@loginid", SqlDbType.VarChar, 12)
            params(2).Value = customclass.LoginId

            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = customclass.BusinessDate

            params(4) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(4).Value = customclass.AmountRec

            params(5) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(5).Value = customclass.ValueDate

            params(6) = New SqlParameter("@referenceNo", SqlDbType.Char, 20)
            params(6).Value = customclass.ReferenceNo

            params(7) = New SqlParameter("@Notes", SqlDbType.Char, 50)
            params(7).Value = customclass.description

            params(8) = New SqlParameter("@CoyID", SqlDbType.Char, 3)
            params(8).Value = customclass.CoyID


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spSuspendReceiveImplementasi", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("SuspendReceive", "SaveSuspendReceive", exp.Message + exp.StackTrace)
        End Try
    End Sub
    Public Sub SaveSuspendOtorisasi(ByVal customclass As Parameter.SuspendReceive)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(1) {}


        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@SuspendNo", SqlDbType.VarChar, 20)
            params(0).Value = customclass.SuspendNo

            params(1) = New SqlParameter("@loginid", SqlDbType.VarChar, 12)
            params(1).Value = customclass.LoginId

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spSuspendOtorisasi", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("SuspendOtorisasi", "SaveSuspendOtorisasi", exp.Message + exp.StackTrace)
        End Try
    End Sub

    Public Sub SaveReversalOtorisasi(ByVal customclass As Parameter.SuspendReceive)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(1) {}


        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
            params(0).Value = customclass.SuspendNo

            params(1) = New SqlParameter("@loginid", SqlDbType.VarChar, 12)
            params(1).Value = customclass.LoginId

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spReversalOtorisasi", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("SuspendOtorisasi", "SaveReversalOtorisasi", exp.Message + exp.StackTrace)
        End Try
    End Sub
End Class
