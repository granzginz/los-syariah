
#Region "Import"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
#End Region

Public Class PCReimburse
    Inherits AccMntBase
#Region "Constanta"


    Private Const PC_List As String = "spPettyCashPaging"
    Private Const PC_Reimburse_Save As String = "spPettyCashReimburseSave"
    Private Const SP_RECONCILE_PC As String = "spReconcilePettyCash"
    Private Const SP_RECONCILE_PR As String = "spReconcilePaymentRequest"
    Private Const spAprPettyCashReimburse As String = "spAprPettyCashReimburse"
    Private Const spAprPettyCashReimburseisFinal As String = "spPCApprovalScreenSetLimit"
    Private Const spAprPettyCashReimburseIsValidApproval As String = "spPCApprovalScreenIsValidApproval"
    Private Const spGetCboUser As String = "spGetCboUserApprovalPC"
    Private Const spGetCboUserAll As String = "spApprovalListUser"
#End Region
#Region "PCReimburseList"
    Public Function PCReimburseList(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash

        Dim params(4) As SqlParameter

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oET.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oET.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oET.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oET.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oET.ListPC = SqlHelper.ExecuteDataset(oET.strConnection, CommandType.StoredProcedure, PC_List, params).Tables(0)
            Return oET
        Catch exp As Exception
            WriteException("PCReimburse", "PCReimburseList", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "PCReimburseSave"
    Public Sub PCReimburseSave(ByVal oET As Parameter.PettyCash)

        Dim params() As SqlParameter = New SqlParameter(7) {}

        Dim objCon As New SqlConnection(oET.strConnection)

        Dim transaction As SqlTransaction
        Dim intLoopOmset As Integer
        Dim params2() As SqlParameter = New SqlParameter(5) {}

        '       @BranchID char(3),
        '@BusinessDate datetime,
        '@LoginID varchar(20),
        '@BankAccountID varchar(12),
        '@Description varchar(500),
        '@TotalAmount numeric(17,2)

        Try
            If CheckDupPCreimburseStatus(oET) Then
                oET.strDesc = "Petty Cash already Reimburse By Another User"
            Else
                If objCon.State = ConnectionState.Closed Then objCon.Open()
                transaction = objCon.BeginTransaction
                params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(0).Value = oET.BranchId

                params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params(1).Value = oET.BusinessDate

                params(2) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
                params(2).Value = oET.LoginId

                params(3) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 12)
                params(3).Value = oET.BankAccountID

                params(4) = New SqlParameter("@Description", SqlDbType.VarChar, 500)
                params(4).Value = oET.Description

                params(5) = New SqlParameter("@TotalAmount", SqlDbType.Decimal)
                params(5).Value = oET.TotalAmount

                params(6) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 100)
                params(6).Direction = ParameterDirection.Output

                params(7) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
                params(7).Direction = ParameterDirection.Output

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, PC_Reimburse_Save, params)
                params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params2(1) = New SqlParameter("@PettyCashNo", SqlDbType.Char, 20)
                params2(2) = New SqlParameter("@RequestNo", SqlDbType.Char, 20)
                params2(3) = New SqlParameter("@LoginId", SqlDbType.Char, 20)
                params2(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params2(5) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)

                If oET.NDtTable.Rows.Count > 0 Then
                    For intLoopOmset = 0 To oET.NDtTable.Rows.Count - 1
                        '@BranchID char(3),
                        '	@PettyCashNo varchar(20),
                        '	@RequestNo varchar(20)
                        '                       @LoginId varchar(20),
                        '@BusinessDate datetime
                        params2(0).Value = oET.BranchId
                        params2(1).Value = oET.NDtTable.Rows(intLoopOmset).Item("PettyCashNo")
                        params2(2).Value = CStr(params(6).Value)
                        params2(3).Value = oET.LoginId
                        params2(4).Value = oET.BusinessDate
                        params2(5).Value = ParameterDirection.Output
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPettyCashReimburseDetailSave", params2)
                    Next
                End If
                transaction.Commit()
                oET.strDesc = CStr(params2(2).Value)
            End If
        Catch exp As Exception
            transaction.Rollback()
            WriteException("PCReimburse", "PCReimburseSave", exp.Message + exp.StackTrace)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
#End Region

    Public Function CheckDupPCreimburseStatus(ByVal customclass As Parameter.PettyCash) As Boolean
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@pettyCashNo", SqlDbType.VarChar, 8000)
            params(1).Value = customclass.strID

            params(2) = New SqlParameter("@ISChange", SqlDbType.Bit)
            params(2).Direction = ParameterDirection.Output

            params(3) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
            params(3).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spCheckDuplikasiPCReimburse", params)
            Return CBool(params(2).Value)
        Catch exp As Exception
            WriteException("PCReimburse", "CheckDupPCreimburseStatus", exp.Message + exp.StackTrace)
        End Try
    End Function

#Region "PCReimburseReconcile"
    ''' <summary>
    ''' '''''''''''''''''
    ''' </summary>
    ''' <param name="customClass"></param>
    ''' <remarks></remarks>
    Public Sub PCReimburseReconcile(ByVal customClass As Parameter.PettyCashReconcile)
        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction
        Dim spname As String

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            If customClass.ReconcileMode = "PC" Then
                spname = SP_RECONCILE_PC
                Dim params(5) As SqlParameter
                params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
                params(0).Value = customClass.BranchId
                params(1) = New SqlParameter("@RequestNo", SqlDbType.Char, 20)
                params(1).Value = customClass.RequestNo
                params(2) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params(2).Value = customClass.BankAccountID
                params(3) = New SqlParameter("@LoginID", SqlDbType.Char, 40)
                params(3).Value = customClass.LoginId
                params(4) = New SqlParameter("@CoyID", SqlDbType.Char, 3)
                params(4).Value = customClass.CoyID
                params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params(5).Value = customClass.BusinessDate
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spname, params)
                objtrans.Commit()

            Else
                Dim params1() As SqlParameter = New SqlParameter(4) {}
                If customClass.strDesc = "" Then
                    ' ambil reference no otomatis jika no reference blank
                    params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                    params1(0).Value = customClass.BranchId.Replace("'", "")
                    params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                    params1(1).Value = customClass.BankAccountID
                    params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                    params1(2).Direction = ParameterDirection.Output
                    params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                    params1(3).Value = customClass.BusinessDate
                    params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                    params1(4).Value = "M"
                    SqlHelper.ExecuteScalar(objtrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                    customClass.strDesc = CStr(params1(2).Value)
                End If
                'spname = SP_RECONCILE_PR 

                If customClass.ReconcileMode = "EPR" Then
                    Dim params(8) As SqlParameter
                    params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
                    params(0).Value = customClass.BranchId
                    params(1) = New SqlParameter("@RequestNo", SqlDbType.Char, 20)
                    params(1).Value = customClass.RequestNo
                    params(2) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                    params(2).Value = customClass.BankAccountID
                    params(3) = New SqlParameter("@LoginID", SqlDbType.Char, 40)
                    params(3).Value = customClass.LoginId
                    params(4) = New SqlParameter("@CoyID", SqlDbType.Char, 3)
                    params(4).Value = customClass.CoyID
                    params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                    params(5).Value = customClass.BusinessDate
                    params(6) = New SqlParameter("@RefNo", SqlDbType.Char, 20)
                    params(6).Value = customClass.strDesc 'RefNo
                    params(7) = New SqlParameter("@Amount", SqlDbType.Decimal)
                    params(7).Value = customClass.Amount
                    params(8) = New SqlParameter("@Detail", SqlDbType.Structured)
                    params(8).Value = customClass.NDtTable
                    'transactionname itu sebenarnya nama storedprocedurenya
                    SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, customClass.TransactionName, params)
                Else
                    Dim params(6) As SqlParameter
                    params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
                    params(0).Value = customClass.BranchId
                    params(1) = New SqlParameter("@RequestNo", SqlDbType.Char, 20)
                    params(1).Value = customClass.RequestNo
                    params(2) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                    params(2).Value = customClass.BankAccountID
                    params(3) = New SqlParameter("@LoginID", SqlDbType.Char, 40)
                    params(3).Value = customClass.LoginId
                    params(4) = New SqlParameter("@CoyID", SqlDbType.Char, 3)
                    params(4).Value = customClass.CoyID
                    params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                    params(5).Value = customClass.BusinessDate
                    params(6) = New SqlParameter("@RefNo", SqlDbType.Char, 20)
                    params(6).Value = customClass.strDesc 'RefNo
                    'transactionname itu sebenarnya nama storedprocedurenya
                    SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, customClass.TransactionName, params)
                End If
                objtrans.Commit()


            End If


        Catch exp As Exception
            objtrans.Rollback()
            Dim err As New MaxiloanExceptions
            err.WriteLog("PCReimbures.vb", "PCReimburseReconcile", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            WriteException("PCReimburse", "PCReimburseReconcile", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub
#End Region



    Public Function PCreimburseStatusUpdate(ByVal customclass As Parameter.PettyCash) As String
        Dim objconnection As New SqlConnection(customclass.strConnection)
        Dim objtransaction As SqlTransaction = Nothing
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim strApprovalNo As String
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Dim params1 As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objtransaction = objconnection.BeginTransaction



            If (customclass.PCStatus.Trim = "ACC") Then
                With oEntitiesApproval
                    .ApprovalTransaction = objtransaction
                    .BranchId = customclass.BranchId
                    .SchemeID = "PCR"
                    .RequestDate = customclass.BusinessDate
                    .TransactionNo = customclass.PettyCashNo
                    .ApprovalValue = customclass.strAmount
                    .UserRequest = customclass.ApprovalFrom
                    .UserApproval = customclass.ApprovalBy
                    .ApprovalNote = customclass.notes
                    .AprovalType = Parameter.Approval.ETransactionType.InvoiceToBePaid_approval
                End With
                strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

                params.Add(New SqlParameter("@RequestNo", SqlDbType.VarChar, 20) With {.Value = customclass.PettyCashNo})
                params.Add(New SqlParameter("@STATUS", SqlDbType.VarChar, 3) With {.Value = customclass.PCStatus})
                Dim err = New SqlParameter("@errMsg", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
                params.Add(err)

                SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "PettyCashreimburseUpdate", params.ToArray)

                objtransaction.Commit()
                Return err.Value
            End If

            If (customclass.PCStatus.Trim = "CAB") Then

                params1.Add(New SqlParameter("@PCStatus", SqlDbType.VarChar, 3) With {.Value = customclass.PCStatus})
                params1.Add(New SqlParameter("@PCDStatus", SqlDbType.VarChar, 1) With {.Value = customclass.PCDStatus})
                params1.Add(New SqlParameter("@PettyCashNoDetail", SqlDbType.VarChar, 20) With {.Value = customclass.PettyCashNoPDC})
                params1.Add(New SqlParameter("@RequestNo", SqlDbType.VarChar, 20) With {.Value = customclass.PettyCashNo})
                Dim err1 = New SqlParameter("@errMsg", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
                params1.Add(err1)
                SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "PettyCashDetailreimburseUpdate", params1.ToArray)

                objtransaction.Commit()
                Return err1.Value
            End If


        Catch exp As Exception
            objtransaction.Rollback()
            WriteException("PCReimburse", "PCreimburseStatusUpdate", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update AP Disburse")

        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try

    End Function

    Public Function PCreimburseStatusUpdateACC(ByVal customclass As Parameter.PettyCash) As String
        Dim objconnection As New SqlConnection(customclass.strConnection)
        Dim objtransaction As SqlTransaction = Nothing
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim strApprovalNo As String
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objtransaction = objconnection.BeginTransaction



            If (customclass.PCStatus.Trim = "APR") Then
                With oEntitiesApproval
                    .ApprovalTransaction = objtransaction
                    .BranchId = customclass.BranchId
                    .SchemeID = "PCR"
                    .RequestDate = customclass.BusinessDate
                    '.TransactionNo = customclass.PettyCashNoPDC + customclass.SequenceNo
                    .TransactionNo = customclass.PettyCashNoPDC
                    .ApprovalValue = customclass.strAmount
                    .UserRequest = customclass.ApprovalFrom
                    .UserApproval = customclass.ApprovalBy
                    .ApprovalNote = customclass.notes
                    .AprovalType = Parameter.Approval.ETransactionType.InvoiceToBePaid_approval                    
                End With
                strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)
            End If


            'params.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = customclass.BranchId.Replace("'", "")})
            params.Add(New SqlParameter("@RequestNo", SqlDbType.VarChar, 20) With {.Value = customclass.PettyCashNo})
            params.Add(New SqlParameter("@PettyCashNo", SqlDbType.VarChar, 20) With {.Value = customclass.PettyCashNoPDC})
            params.Add(New SqlParameter("@SequenceNo", SqlDbType.VarChar, 20) With {.Value = customclass.SequenceNo})
            params.Add(New SqlParameter("@STATUS", SqlDbType.VarChar, 3) With {.Value = customclass.PCStatus})
            params.Add(New SqlParameter("@StatusPCD", SqlDbType.VarChar, 3) With {.Value = customclass.PCDStatus})
            Dim err = New SqlParameter("@errMsg", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            params.Add(err)

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "PettyCashreimburseUpdateACC", params.ToArray)

            objtransaction.Commit()
            Return err.Value

        Catch exp As Exception
            objtransaction.Rollback()
            WriteException("PCReimburse", "PCreimburseStatusUpdateACC", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update AP Disburse")

        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try

    End Function

    Public Function PCreimburseStatusUpdateACCReject(ByVal customclass As Parameter.PettyCash) As String
        Dim objconnection As New SqlConnection(customclass.strConnection)
        Dim objtransaction As SqlTransaction = Nothing
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim strApprovalNo As String
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objtransaction = objconnection.BeginTransaction

            'If (customclass.PCStatus.Trim = "ACC") Then
            If (customclass.PCStatus.Trim = "APR") Then
                params.Add(New SqlParameter("@RequestNo", SqlDbType.VarChar, 20) With {.Value = customclass.PettyCashNo})
                params.Add(New SqlParameter("@PettyCashNo", SqlDbType.VarChar, 20) With {.Value = customclass.PettyCashNoPDC})
                params.Add(New SqlParameter("@SequenceNo", SqlDbType.VarChar, 20) With {.Value = customclass.SequenceNo})
                params.Add(New SqlParameter("@STATUS", SqlDbType.VarChar, 3) With {.Value = customclass.PCStatus})
                params.Add(New SqlParameter("@StatusPCD", SqlDbType.VarChar, 3) With {.Value = customclass.PCDStatus})
                Dim err = New SqlParameter("@errMsg", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
                params.Add(err)
                SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "PettyCashreimburseUpdateACCReject", params.ToArray)

                objtransaction.Commit()
                Return err.Value

            End If
        Catch exp As Exception
            objtransaction.Rollback()
            WriteException("PCReimburse", "PCreimburseStatusUpdateACCReject", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update AP Disburse")

        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try

    End Function
    Public Function GetPettyCashFormKuning(ByVal customClass As IList(Of String), cnn As String) As DataSet 
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Dim dt = New DataTable()

        dt.Columns.Add("id", GetType(String))
        dt.Columns.Add("value1", GetType(String))
        dt.Columns.Add("value2", GetType(String))
        For Each v In customClass 
            Dim row = dt.NewRow() 
            row("id") = v
            row("value1") = ""
            row("value1") = ""
            dt.Rows.Add(row)
        Next 
        params(0) = New SqlParameter("@PettyCashNo", SqlDbType.Structured)
        params(0).Value = dt

        Try
            Return SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, "spPettyCashReimburseFormKuning", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function AprPCReimburse(ByVal customclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(5) As SqlParameter
        With customclass
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = .SortBy
            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output
            params(5) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 20)
            'params(5).Value = .AppID
            params(5).Value = getReferenceDBName()

        End With
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAprPettyCashReimburse, params).Tables(0)
            customclass.TotalRecord = CType(params(4).Value, Int16)
            Return customclass
        Catch exp As Exception
            WriteException("PettyCash", "AprPCReimburse", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AprPCReimburseisFinal(ByVal customclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim objread As SqlDataReader
        Dim params(3) As SqlParameter
        With customclass
            params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.VarChar, 50)
            params(0).Value = .ApprovalSchemeID
            params(1) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            params(1).Value = .RequestNo
            params(2) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
            params(2).Value = .LoginId
            params(3) = New SqlParameter("@IsReRequest", SqlDbType.Bit)
            params(3).Value = .IsReRequest

        End With
        Try            
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spAprPettyCashReimburseisFinal, params)

            If Not IsNothing(objread) Then
                With customclass
                    If objread.Read Then
                        .IsFinal = CType(objread("IsFinal"), String)
                        .NextPersonApproval = CType(objread("NextPersonApproval"), String)
                    End If
                    objread.Close()
                End With
            End If

            Return customclass
        Catch exp As Exception
            WriteException("PettyCash", "AprPCReimburseisFinal", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AprPCReimburseIsValidApproval(ByVal customclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim objread As SqlDataReader
        Dim params(1) As SqlParameter
        With customclass
            params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.VarChar, 50)
            params(0).Value = .ApprovalSchemeID
            params(1) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            params(1).Value = .RequestNo
        End With
        Try
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spAprPettyCashReimburseIsValidApproval, params)

            If Not IsNothing(objread) Then
                With customclass
                    If objread.Read Then
                        .UserApproval = CType(objread("UserApproval"), String)
                    End If
                    objread.Close()
                End With
            End If

            Return customclass

        Catch exp As Exception
            WriteException("PettyCash", "AprPCReimburseIsValidApproval", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCboUserAprPC(ByVal customclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim oReturnValue As New Parameter.PettyCash

        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
        'params(0).Value = customclass.AppID
        params(0).Value = getReferenceDBName()
        params(1) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
        params(1).Value = customclass.LoginId

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spGetCboUser, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("PettyCash", "GetCboUserAprPC", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function PCAprReimburseSave(ByVal customclass As Parameter.PettyCash) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(11) {}
            params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.VarChar, 10)
            params(0).Value = customclass.ApprovalSchemeID
            params(1) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            params(1).Value = customclass.RequestNo
            params(2) = New SqlParameter("@ApprovalResult", SqlDbType.VarChar, 1)
            params(2).Value = customclass.ApprovalResult
            params(3) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
            params(3).Value = customclass.BranchId
            params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(4).Value = customclass.BusinessDate
            params(5) = New SqlParameter("@ApprovalDate", SqlDbType.DateTime)
            params(5).Value = customclass.ApprovalDate
            params(6) = New SqlParameter("@notes", SqlDbType.VarChar, 1000)
            params(6).Value = customclass.notes
            params(7) = New SqlParameter("@SecurityCode", SqlDbType.VarChar, 50)
            params(7).Value = customclass.SecurityCode
            params(8) = New SqlParameter("@UserApproval", SqlDbType.VarChar, 255)
            params(8).Value = customclass.UserApproval
            params(9) = New SqlParameter("@UserSecurityCode", SqlDbType.VarChar, 255)
            params(9).Value = customclass.UserSecurityCode
            params(10) = New SqlParameter("@IsEverRejected", SqlDbType.Bit)
            params(10).Value = customclass.IsEverRejected
            params(11) = New SqlParameter("@LoginID", SqlDbType.VarChar, 255)
            params(11).Value = customclass.LoginId

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spApprovalSaveFinalPC", params.ToArray)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("PettyCash ", "PCAprReimburseSave", exp.Message + exp.StackTrace)
            Throw New Exception("ERROR: Can Not Process Aprrove Reimburse Petty Cash. " + exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Function

    Function PCAprReimburseSaveToNextPerson(ByVal customclass As Parameter.PettyCash) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(13) {}
            params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.VarChar, 10)
            params(0).Value = customclass.ApprovalSchemeID
            params(1) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            params(1).Value = customclass.RequestNo
            params(2) = New SqlParameter("@ApprovalResult", SqlDbType.VarChar, 1)
            params(2).Value = customclass.ApprovalResult
            params(3) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
            params(3).Value = customclass.BranchId
            params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(4).Value = customclass.BusinessDate
            params(5) = New SqlParameter("@ApprovalDate", SqlDbType.DateTime)
            params(5).Value = customclass.ApprovalDate
            params(6) = New SqlParameter("@notes", SqlDbType.VarChar, 1000)
            params(6).Value = customclass.notes
            params(7) = New SqlParameter("@SecurityCode", SqlDbType.VarChar, 50)
            params(7).Value = customclass.SecurityCode
            params(8) = New SqlParameter("@UserApproval", SqlDbType.VarChar, 255)
            params(8).Value = customclass.UserApproval
            params(9) = New SqlParameter("@UserSecurityCode", SqlDbType.VarChar, 255)
            params(9).Value = customclass.UserSecurityCode
            params(10) = New SqlParameter("@IsEverRejected", SqlDbType.Bit)
            params(10).Value = customclass.IsEverRejected
            params(11) = New SqlParameter("@LoginID", SqlDbType.VarChar, 255)
            params(11).Value = customclass.LoginId
            params(12) = New SqlParameter("@NextPersonApproval", SqlDbType.VarChar, 255)
            params(12).Value = customclass.NextPersonApproval
            params(13) = New SqlParameter("@ipaddress", SqlDbType.VarChar, 255)
            params(13).Value = ""

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spApprovalRequestToNextPersonPC", params.ToArray                                      )
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("PettyCash ", "PCAprReimburseSaveToNextPerson", exp.Message + exp.StackTrace)
            Throw New Exception("ERROR: Can Not Process Aprrove Reimburse Petty Cash. " + exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Public Function GetCboUserAprPCAll(ByVal customclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim oReturnValue As New Parameter.PettyCash

        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@ApprType", SqlDbType.VarChar, 20)
        params(0).Value = customclass.ApprovalSchemeID
        params(1) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
        'params(1).Value = customclass.AppID
        params(1).Value = getReferenceDBName()

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spGetCboUserAll, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("PettyCash", "GetCboUserAprPCAll", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function PCreimburseStatusReject(ByVal customclass As Parameter.PettyCash) As String
        Dim objconnection As New SqlConnection(customclass.strConnection)
        Dim objtransaction As SqlTransaction = Nothing
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim strApprovalNo As String
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objtransaction = objconnection.BeginTransaction

            If (customclass.PCStatus.Trim = "ACC") Then
                With oEntitiesApproval
                    .ApprovalTransaction = objtransaction
                    .BranchId = customclass.BranchId
                    .SchemeID = "PCR"
                    .RequestDate = customclass.BusinessDate
                    .TransactionNo = customclass.PettyCashNo
                    .ApprovalValue = customclass.strAmount
                    .UserRequest = customclass.ApprovalFrom
                    .UserApproval = customclass.ApprovalBy
                    .ApprovalNote = customclass.notes

                    .AprovalType = Parameter.Approval.ETransactionType.InvoiceToBePaid_approval
                End With
                strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)
            End If
            params.Add(New SqlParameter("@RequestNo", SqlDbType.VarChar, 20) With {.Value = customclass.PettyCashNo})
            params.Add(New SqlParameter("@StatusPettyCash", SqlDbType.VarChar, 3) With {.Value = customclass.PettyCashStatus})
            params.Add(New SqlParameter("@IsReimburse", SqlDbType.Bit) With {.Value = customclass.IsReimburse})
            params.Add(New SqlParameter("@STATUS", SqlDbType.VarChar, 3) With {.Value = customclass.PCStatus})
            params.Add(New SqlParameter("@Note", SqlDbType.VarChar, 255) With {.Value = customclass.notes})
            params.Add(New SqlParameter("@BranchId", SqlDbType.VarChar, 3) With {.Value = customclass.BranchId})
            params.Add(New SqlParameter("@RejectDate", SqlDbType.DateTime) With {.Value = customclass.RejectDate})
            params.Add(New SqlParameter("@RejectBy", SqlDbType.VarChar, 255) With {.Value = customclass.RejectBy})
            Dim err = New SqlParameter("@errMsg", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            params.Add(err)

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "PettyCashreimburseReject", params.ToArray)

            objtransaction.Commit()
            Return err.Value

        Catch exp As Exception
            objtransaction.Rollback()
            WriteException("PCReimburse", "PCreimburseStatusReject", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update AP Disburse")

        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try

    End Function
    Public Function PCreimburseCOAUpdate(ByVal customclass As Parameter.PettyCash) As String
        Dim objconnection As New SqlConnection(customclass.strConnection)
        Dim objtransaction As SqlTransaction = Nothing
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objtransaction = objconnection.BeginTransaction

            params.Add(New SqlParameter("@PettyCashNo", SqlDbType.VarChar, 50) With {.Value = customclass.PettyCashNo})
            params.Add(New SqlParameter("@dtPettycashTrans", SqlDbType.Structured) With {.Value = customclass.ListData})


            Dim err = New SqlParameter("@errMsg", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            params.Add(err)

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "PettyCashreimburseUpdateCOA", params.ToArray)

            objtransaction.Commit()
            Return err.Value

        Catch exp As Exception
            objtransaction.Rollback()
            WriteException("PCReimburse", "PettyCashreimburseUpdateCOA", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update AP Disburse")

        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try

    End Function
End Class
