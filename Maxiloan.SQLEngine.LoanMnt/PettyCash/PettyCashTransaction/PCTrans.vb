

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region


Public Class PettyCashTrans : Inherits DataAccessBase

    Public Function SavePCTrans(ByVal customclass As Parameter.PettyCash, ByVal ds As DataTable) As String
        Dim params() As SqlParameter = New SqlParameter(17) {}
        Dim params2() As SqlParameter = New SqlParameter(7) {}
        Dim params3() As SqlParameter = New SqlParameter(3) {}
        Dim transaction As SqlTransaction
        Dim intLoopOmset As Integer
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim PettyCashNo As String

        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
            params(2).Value = customclass.LoginId

            params(3) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 12)
            params(3).Value = customclass.BankAccountID

            params(4) = New SqlParameter("@DepartementID", SqlDbType.VarChar, 10)
            params(4).Value = customclass.DepartementId

            params(5) = New SqlParameter("@EmployeeID", SqlDbType.Char, 20)
            params(5).Value = customclass.EmployeeId

            params(6) = New SqlParameter("@TotalAmount", SqlDbType.Decimal)
            params(6).Value = customclass.TotalAmount

            params(7) = New SqlParameter("@BGNo", SqlDbType.Char, 1)
            params(7).Value = customclass.BGNo

            params(8) = New SqlParameter("@BGDueDate", SqlDbType.DateTime)
            params(8).Value = customclass.BGDueDate

            params(9) = New SqlParameter("@Desc", SqlDbType.VarChar, 500)
            params(9).Value = customclass.Description

            params(10) = New SqlParameter("@amount", SqlDbType.Decimal)
            params(10).Value = customclass.Amount

            params(11) = New SqlParameter("@NumOfDetail", SqlDbType.Int)
            params(11).Value = customclass.NumPC

            params(12) = New SqlParameter("@strID", SqlDbType.VarChar, 8000)
            params(12).Value = customclass.strID

            params(13) = New SqlParameter("@strDesc", SqlDbType.VarChar, 8000)
            params(13).Value = customclass.strDesc

            params(14) = New SqlParameter("@strAmount", SqlDbType.VarChar, 7000)
            params(14).Value = customclass.strAmount

            params(15) = New SqlParameter("@bankaccountype", SqlDbType.Char, 1)
            params(15).Value = customclass.BankAccountType

            params(16) = New SqlParameter("@PettyCashNo", SqlDbType.VarChar, 20)
            params(16).Direction = ParameterDirection.Output

            params(17) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
            params(17).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPettyCashTransSave", params)

            PettyCashNo = params(16).Value

            params2(0) = New SqlParameter("@branchid", SqlDbType.Char, 3)
            params2(1) = New SqlParameter("@PettyCashNo", SqlDbType.VarChar, 20)
            params2(2) = New SqlParameter("@PaymentAllocationID", SqlDbType.Char, 10)
            params2(3) = New SqlParameter("@description", SqlDbType.VarChar, 500)
            params2(4) = New SqlParameter("@amount", SqlDbType.Decimal)
            params2(5) = New SqlParameter("@ValueDate", SqlDbType.Date)
            params2(6) = New SqlParameter("@DepartementID", SqlDbType.VarChar, 10)
            params2(7) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)

            If ds.Rows.Count > 0 Then
                For intLoopOmset = 0 To ds.Rows.Count - 1

                    params2(0).Value = customclass.BranchId
                    params2(1).Value = CStr(params(16).Value)
                    params2(2).Value = ds.Rows(intLoopOmset).Item("TransactionID")
                    params2(3).Value = ds.Rows(intLoopOmset).Item("Description")
                    params2(4).Value = ds.Rows(intLoopOmset).Item("Amount")
                    params2(5).Value = ds.Rows(intLoopOmset).Item("ValueDate")
                    params2(6).Value = customclass.DepartementId
                    params2(7).Direction = ParameterDirection.Output
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPettyCashTransDetailSave", params2)

                Next
                customclass.PettyCashNo = PettyCashNo
            End If


            '*********************************************************************
            '** PROSES JOURNAL DI PINDAH KE  TERIMA TRANSFER PETTYCASH  **'
            '*********************************************************************
            '
            'params3(0) = New SqlParameter("PettyCashNo", SqlDbType.VarChar, 20)
            'params3(0).Value = PettyCashNo
            'params3(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            'params3(1).Value = customclass.BranchId
            'params3(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            'params3(2).Value = customclass.BusinessDate
            'params3(3) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            'params3(3).Value = customclass.BusinessDate 
            'SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPettyCashTransSaveGournal", params3)
            '
            '*********************************************************************


            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("PettyCashTrans", "SavePCTrans", exp.Message) '+ exp.StackTrace)
            Throw New Exception("Failed On Update PC Transaction")
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function

    Public Function PCReimburseSave(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim params() As SqlParameter = New SqlParameter(7) {}

        Dim objCon As New SqlConnection(oET.strConnection)

        Dim transaction As SqlTransaction
        Dim intLoopOmset As Integer
        Dim params2() As SqlParameter = New SqlParameter(5) {}

        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oET.BranchId

            params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(1).Value = oET.BusinessDate

            params(2) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
            params(2).Value = oET.LoginId

            params(3) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 12)
            params(3).Value = oET.BankAccountID

            params(4) = New SqlParameter("@Description", SqlDbType.VarChar, 500)
            params(4).Value = oET.LoginId

            params(5) = New SqlParameter("@TotalAmount", SqlDbType.Decimal)
            params(5).Value = oET.TotalAmount

            params(6) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 100)
            params(6).Direction = ParameterDirection.Output

            params(7) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
            params(7).Direction = ParameterDirection.Output

            If CheckDupPCreimburseStatus(oET) Then
                oET.strDesc = "Petty Cash already Reimburse By Another User"
            Else
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPettyCashReimburseSave", params)
                params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params2(1) = New SqlParameter("@PettyCashNo", SqlDbType.Char, 20)
                params2(2) = New SqlParameter("@RequestNo", SqlDbType.Char, 20)
                params2(3) = New SqlParameter("@LoginId", SqlDbType.Char, 20)
                params2(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params2(5) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)

                If oET.NDtTable.Rows.Count > 0 Then
                    For intLoopOmset = 0 To oET.NDtTable.Rows.Count - 1
                        '@BranchID char(3),
                        '	@PettyCashNo varchar(20),
                        '	@RequestNo varchar(20)
                        '                       @LoginId varchar(20),
                        '@BusinessDate datetime
                        params2(0).Value = oET.BranchId
                        params2(1).Value = oET.NDtTable.Rows(intLoopOmset).Item("PettyCashNo")
                        params2(2).Value = CStr(params(6).Value)
                        params2(3).Value = oET.LoginId
                        params2(4).Value = oET.BusinessDate
                        params2(5).Value = ParameterDirection.Output
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPettyCashReimburseDetailSave", params2)
                    Next
                End If
                transaction.Commit()

            End If
        Catch exp As Exception
            transaction.Rollback()
            WriteException("PettyCashTrans", "PCReimburseSave", exp.Message + exp.StackTrace)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function

    Public Function CheckDupPCreimburseStatus(ByVal customclass As Parameter.PettyCash) As Boolean
        Dim params() As SqlParameter = New SqlParameter(3) {}
        '@BranchID char(3),
        '@pettyCashNo varchar(8000),
        '@ISChange bit output,
        '@strerror varchar(100) output
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@pettyCashNo", SqlDbType.VarChar, 8000)
            params(1).Value = customclass.strID

            params(2) = New SqlParameter("@ISChange", SqlDbType.Bit)
            params(2).Direction = ParameterDirection.Output

            params(3) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
            params(3).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spCheckDuplikasiPCReimburse", params)
            Return CBool(params(2).Value)
        Catch exp As Exception
            WriteException("PettyCashTrans", "CheckDupPCreimburseStatus", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
End Class