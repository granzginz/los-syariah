
Imports System
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions


Public Class PettyCash : Inherits DataAccessBase
#Region "PrivateConst"
    Private STR_SP_PAGING As String = "spPettyCashPaging"
    Private STR_SP_GET_A_RECORD As String = "spPettyCashInquiryView"
    Private STR_SP_GET_DETAIL As String = "spPettyCashDetail"

    Private STR_SP_GET_A_RECORDACC As String = "spPettyCashInquiryViewACC"
    Private STR_SP_GET_DETAILACC As String = "spPettyCashDetailACC"
    Private STR_SP_PROCESS_PC_REVERSAL As String = "spProcessPettyCashReversal"

    '**** Fields definition
    Private Const FLD_NM_BRANCH_ID As String = "BranchId"
    Private Const FLD_TYP_BRANCH_ID As SqlDbType = SqlDbType.Char
    Private Const FLD_LEN_BRANCH_ID As Integer = 3

    Private Const FLD_NM_PETTY_CASH_NO As String = "PettyCashNo"
    Private Const FLD_TYP_PETTY_CASH_NO As SqlDbType = SqlDbType.Char
    Private Const FLD_LEN_PETTY_CASH_NO As Integer = 20

    Private Const FLD_NM_DEPT_ID As String = "DepartementId"
    Private Const FLD_TYP_DEPT_ID As SqlDbType = SqlDbType.Char
    Private Const FLD_LEN_DEPT_ID As Integer = 3

    Private Const FLD_NM_DEPT_NAME As String = "DepartementName"
    Private Const FLD_TYP_DEPT_NAME As SqlDbType = SqlDbType.VarChar
    Private Const FLD_LEN_DEPT_NAME As Integer = 50

    Private Const FLD_NM_EMP_ID As String = "EmployeeId"
    Private Const FLD_TYP_EMP_ID As SqlDbType = SqlDbType.Char
    Private Const FLD_LEN_EMP_ID As Integer = 10

    Private Const FLD_NM_EMP_NAME As String = "EmployeeName"
    Private Const FLD_TYP_EMP_NAME As SqlDbType = SqlDbType.VarChar
    Private Const FLD_LEN_EMP_NAME As Integer = 50

    Private Const FLD_NM_DESC As String = "Description"
    Private Const FLD_TYP_DESC As SqlDbType = SqlDbType.VarChar
    Private Const FLD_LEN_DESC As Integer = 50

    Private Const FLD_NM_PC_AMOUNT As String = "Amount"
    Private Const FLD_TYP_PC_AMOUNT As SqlDbType = SqlDbType.Decimal

    Private Const FLD_NM_PC_DATE As String = "PettyCashDate"
    Private Const FLD_TYP_PC_DATE As SqlDbType = SqlDbType.SmallDateTime

    Private Const FLD_NM_PC_STATUS As String = "PettyCashStatus"
    Private Const FLD_TYP_PC_STATUS As SqlDbType = SqlDbType.VarChar
    Private Const FLD_LEN_PC_STATUS As Integer = 10

    '*** PCInquiry Detail Additional fields
    Private Const FLD_NM_VOUCHER_NO As String = "PCVoucherNo"
    Private Const FLD_TYP_VOUCHER_NO As SqlDbType = SqlDbType.Char
    Private Const FLD_LEN_VOUCHER_NO As Integer = 20

    Private Const FLD_NM_BANK_ACC_ID As String = "BankAccountId"
    Private Const FLD_TYP_BANK_ACC_ID As SqlDbType = SqlDbType.Char
    Private Const FLD_LEN_BANK_ACC_ID As Integer = 10

    Private Const FLD_NM_BANK_ACC_NM As String = "BankAccountName"
    Private Const FLD_TYP_BANK_ACC_NM As SqlDbType = SqlDbType.VarChar
    Private Const FLD_LEN_BANK_ACC_NM As Integer = 50

    Private Const FLD_NM_TX_NM As String = "TransactionName"
    Private Const FLD_TYP_TX_NM As SqlDbType = SqlDbType.VarChar
    Private Const FLD_LEN_TX_NM As Integer = 50

    Private Const FLD_NM_PCD_DESC As String = "PCDDescription"
    Private Const FLD_TYP_PCD_DESC As SqlDbType = SqlDbType.VarChar
    Private Const FLD_LEN_PCD_DESC As Integer = 50

    Private Const FLD_NM_PCD_AMOUNT As String = "PCDAmount"
    Private Const FLD_TYP_PCD_AMOUNT As SqlDbType = SqlDbType.Decimal

    Private Const FLD_NM_PC_REVERSAL_VOUCHER_NO As String = "ReversalVoucherNo"
    Private Const FLD_TYP_PC_REVERSAL_VOUCHER_NO As SqlDbType = SqlDbType.Char
    Private Const FLD_LEN_PC_REVERSAL_VOUCHER_NO As Integer = 20

    Private Const FLD_NM_CASHIER_NM_TRANSACTION As String = "CashierNameTransaction"
    Private Const FLD_TYP_CASHIER_NM_TRANSACTION As SqlDbType = SqlDbType.VarChar
    Private Const FLD_LEN_CASHIER_NM_TRANSACTION As Integer = 50

    Private Const FLD_NM_CASHIER_NM_REVERSAL As String = "CashierNameReversal"
    Private Const FLD_TYP_CASHIER_NM_REVERSAL As SqlDbType = SqlDbType.VarChar
    Private Const FLD_LEN_CASHIER_NM_REVERSAL As Integer = 50

    Private Const FLD_NM_PC_STATUS_DATE As String = "StatusDate"
    Private Const FLD_TYP_PC_STATUS_DATE As SqlDbType = SqlDbType.SmallDateTime

    Private Const PRM_NM_CURR_PAGE As String = "CurrentPage"
    Private Const PRM_TYP_CURR_PAGE As SqlDbType = SqlDbType.Int

    Private Const PRM_NM_PAGE_SIZE As String = "PageSize"
    Private Const PRM_TYP_PAGE_SIZE As SqlDbType = SqlDbType.Int

    Private Const PRM_NM_WHERE_COND As String = "WhereCond"
    Private Const PRM_TYP_WHERE_COND As SqlDbType = SqlDbType.VarChar
    Private Const PRM_LEN_WHERE_COND As Integer = 1000

    Private Const PRM_NM_SORT_BY As String = "SortBy"
    Private Const PRM_TYP_SORT_BY As SqlDbType = SqlDbType.VarChar
    Private Const PRM_LEN_SORT_BY As Integer = 100

    Private Const PRM_NM_LOGIN_ID As String = "LoginId"
    Private Const PRM_TYP_LOGIN_ID As SqlDbType = SqlDbType.VarChar
    Private Const PRM_LEN_LOGIN_ID As Integer = 12

    Private Const PRM_NM_TOTAL_RECORDS As String = "TotalRecords"
    Private Const PRM_TYP_TOTAL_RECORDS As SqlDbType = SqlDbType.Int

    Private Const PRM_NM_BUSINESS_DATE As String = "BusinessDate"
    Private Const PRM_TYP_BUSINESS_DATE As SqlDbType = SqlDbType.DateTime

    Private Const PRM_NM_WOP As String = "WOP"
    Private Const PRM_TYP_WOP As SqlDbType = SqlDbType.VarChar
    Private Const PRM_LEN_WOP As Integer = 5

    Private Const PRM_NM_AMOUNT As String = "AmountReceive"
    Private Const PRM_TYP_AMOUNT As SqlDbType = SqlDbType.Decimal

    Private Const PRM_NM_COMPANY_ID As String = "CompanyId"
    Private Const PRM_TYP_COMPANY_ID As SqlDbType = SqlDbType.Char
    Private Const PRM_LEN_COMPANY_ID As Integer = 3

    Private Const FLD_NM_PAYMENTALLOCATIONID As String = "PaymentAllocDesc"
    Private Const FLD_TYP_PAYMENTALLOCATIONID As SqlDbType = SqlDbType.VarChar
    Private Const FLD_LN_PAYMENTALLOCATIONID As Integer = 20

    Private Const FLD_NM_NOTE As String = "Note"
    Private Const FLD_TYP_NOTE As SqlDbType = SqlDbType.VarChar
    Private Const FLD_LN_NOTE As Integer = 255

    Private Const spInqPettyCashReimburse As String = "spInqPettyCashReimburse"
    Private Const spInqPettyCashReimburseACC As String = "spInqPettyCashReimburseACC"
    Private Const spViewPCReimburseLabel As String = "spViewPCReimburseLabel"
    Private Const spViewPCReimburseGrid As String = "spViewPCReimburseGrid"
    Private Const spViewPCReimburseGridACC As String = "spViewPCReimburseGridACC"
    Private Const spViewPCReimburseGridHistoryReject As String = "spViewPCReimburseGridHistoryReject"

#End Region

    Public Function GetPagingTable(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(4) As SqlParameter
        With oET
            params(0) = New SqlParameter("@" & PRM_NM_CURR_PAGE, PRM_TYP_CURR_PAGE)
            params(0).Value = .CurrentPage

            params(1) = New SqlParameter("@" & PRM_NM_PAGE_SIZE, PRM_TYP_PAGE_SIZE)
            params(1).Value = .PageSize

            params(2) = New SqlParameter("@" & PRM_NM_WHERE_COND, PRM_TYP_WHERE_COND, PRM_LEN_WHERE_COND)
            params(2).Value = .WhereCond

            params(3) = New SqlParameter("@" & PRM_NM_SORT_BY, PRM_TYP_SORT_BY, PRM_LEN_SORT_BY)
            params(3).Value = .SortBy

            params(4) = New SqlParameter("@" & PRM_NM_TOTAL_RECORDS, PRM_TYP_TOTAL_RECORDS)
            params(4).Direction = ParameterDirection.Output

            Try
                .PagingTable = SqlHelper.ExecuteDataset(.strConnection, CommandType.StoredProcedure, STR_SP_PAGING, params).Tables(0)
            Catch exp As Exception
                WriteException("PettyCash", "GetPagingTable", exp.Message + exp.StackTrace)
            End Try
            .TotalRecord = CInt(params(4).Value)
        End With
        Return oET

    End Function
    Public Function GetARecord(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(0) As SqlParameter
        Dim objReader As SqlDataReader

        Try
            With oET
                params(0) = New SqlParameter("@" & FLD_NM_PETTY_CASH_NO, FLD_TYP_PETTY_CASH_NO, FLD_LEN_PETTY_CASH_NO)
                params(0).Value = .PettyCashNo

                objReader = SqlHelper.ExecuteReader(.strConnection, CommandType.StoredProcedure, STR_SP_GET_A_RECORD, params)
            End With

            If objReader.Read Then
                With oET
                    .PettyCashNo = CType(objReader(FLD_NM_PETTY_CASH_NO), String)
                    .EmployeeName = CType(objReader(FLD_NM_EMP_NAME), String)
                    .BankAccountName = CType(objReader(FLD_NM_BANK_ACC_NM), String)
                    .DepartementName = CType(objReader(FLD_NM_DEPT_NAME), String)
                    .Description = CType(objReader(FLD_NM_DESC), String)
                    .PCVoucherNo = CType(objReader(FLD_NM_VOUCHER_NO), String)
                    .PCDate = CType(objReader(FLD_NM_PC_DATE), Date)
                    .PCAmount = CType(objReader(FLD_NM_PC_AMOUNT), Double)
                    .PCReversalVoucherNo = CType(objReader(FLD_NM_PC_REVERSAL_VOUCHER_NO), String)
                    .PCStatus = CType(objReader(FLD_NM_PC_STATUS), String)
                    .CashierNameTransaction = CType(objReader(FLD_NM_CASHIER_NM_TRANSACTION), String)
                    .CashierNameReversal = CType(IIf(objReader(FLD_NM_CASHIER_NM_REVERSAL) Is DBNull.Value, "-", objReader(FLD_NM_CASHIER_NM_REVERSAL)), String)
                    .PCStatusDate = CType(objReader(FLD_NM_PC_STATUS_DATE), Date)
                    .IsReimburse = CType(IIf(CType(objReader("IsReimburse"), Integer) = 0, False, True), Boolean)
                    .PaymentAllocationID = CType(objReader(FLD_NM_PAYMENTALLOCATIONID), String)
                    .notes = CType(objReader(FLD_NM_NOTE), String)
                End With
            End If
            objReader.Close()
            Return oET
        Catch exp As Exception
            WriteException("PettyCash", "GetPagingTable", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetARecordACC(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(0) As SqlParameter
        Dim objReader As SqlDataReader

        Try
            With oET
                'params(0) = New SqlParameter("@" & FLD_NM_BRANCH_ID, FLD_TYP_BRANCH_ID, FLD_LEN_BRANCH_ID)
                'params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                'params(0).Value = .BranchId

                params(0) = New SqlParameter("@" & FLD_NM_PETTY_CASH_NO, FLD_TYP_PETTY_CASH_NO, FLD_LEN_PETTY_CASH_NO)
                params(0).Value = .PettyCashNo

                objReader = SqlHelper.ExecuteReader(.strConnection, CommandType.StoredProcedure, STR_SP_GET_A_RECORDACC, params)
            End With

            If objReader.Read Then
                With oET
                    .PettyCashNo = CType(objReader(FLD_NM_PETTY_CASH_NO), String)
                    .EmployeeName = CType(objReader(FLD_NM_EMP_NAME), String)
                    .BankAccountName = CType(objReader(FLD_NM_BANK_ACC_NM), String)
                    .DepartementName = CType(objReader(FLD_NM_DEPT_NAME), String)
                    .Description = CType(objReader(FLD_NM_DESC), String)
                    .PCVoucherNo = CType(objReader(FLD_NM_VOUCHER_NO), String)
                    .PCDate = CType(objReader(FLD_NM_PC_DATE), Date)
                    .PCAmount = CType(objReader(FLD_NM_PC_AMOUNT), Double)
                    .PCReversalVoucherNo = CType(objReader(FLD_NM_PC_REVERSAL_VOUCHER_NO), String)
                    .PCStatus = CType(objReader(FLD_NM_PC_STATUS), String)
                    .CashierNameTransaction = CType(objReader(FLD_NM_CASHIER_NM_TRANSACTION), String)
                    .CashierNameReversal = CType(IIf(objReader(FLD_NM_CASHIER_NM_REVERSAL) Is DBNull.Value, "-", objReader(FLD_NM_CASHIER_NM_REVERSAL)), String)
                    .PCStatusDate = CType(objReader(FLD_NM_PC_STATUS_DATE), Date)
                    .IsReimburse = CType(IIf(CType(objReader("IsReimburse"), Integer) = 0, False, True), Boolean)
                End With
            End If
            objReader.Close()
            Return oET
        Catch exp As Exception
            WriteException("PettyCash", "GetPagingTable", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetReportDataSet(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(4) As SqlParameter

        Try
            With oET
                params(0) = New SqlParameter("@" & PRM_NM_CURR_PAGE, PRM_TYP_CURR_PAGE)
                params(0).Value = -1

                params(1) = New SqlParameter("@" & PRM_NM_PAGE_SIZE, SqlDbType.Int)
                params(1).Value = .PageSize

                params(2) = New SqlParameter("@" & PRM_NM_WHERE_COND, SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond

                params(3) = New SqlParameter("@" & PRM_NM_SORT_BY, SqlDbType.VarChar, 100)
                params(3).Value = .SortBy

                params(4) = New SqlParameter("@" & PRM_NM_TOTAL_RECORDS, SqlDbType.Int)
                params(4).Direction = ParameterDirection.Output

                .ReportDataSet = SqlHelper.ExecuteDataset(.strConnection, CommandType.StoredProcedure, STR_SP_PAGING, params)
                .TotalRecord = CInt(params(4).Value)
            End With
            Return oET
        Catch exp As Exception
            WriteException("PettyCash", "GetPagingTable", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetDetailTable(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(0) As SqlParameter
        Dim objReader As SqlDataReader

        Try
            With oET
                'params(0) = New SqlParameter("@" & FLD_NM_BRANCH_ID, FLD_TYP_BRANCH_ID, FLD_LEN_BRANCH_ID)
                'params(0).Value = .BranchId

                params(0) = New SqlParameter("@" & FLD_NM_PETTY_CASH_NO, FLD_TYP_PETTY_CASH_NO, FLD_LEN_PETTY_CASH_NO)
                params(0).Value = .PettyCashNo
                .PagingTable = SqlHelper.ExecuteDataset(.strConnection, CommandType.StoredProcedure, STR_SP_GET_DETAIL, params).Tables(0)
            End With
            Return oET
        Catch exp As Exception
            WriteException("PettyCash", "GetDetailTable", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetDetailTableACC(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(0) As SqlParameter
        Dim objReader As SqlDataReader

        Try
            With oET
                'params(0) = New SqlParameter("@" & FLD_NM_BRANCH_ID, FLD_TYP_BRANCH_ID, FLD_LEN_BRANCH_ID)
                'params(0).Value = .BranchId

                params(0) = New SqlParameter("@" & FLD_NM_PETTY_CASH_NO, FLD_TYP_PETTY_CASH_NO, FLD_LEN_PETTY_CASH_NO)
                params(0).Value = .PettyCashNo
                .PagingTable = SqlHelper.ExecuteDataset(.strConnection, CommandType.StoredProcedure, STR_SP_GET_DETAILACC, params).Tables(0)
            End With
            Return oET
        Catch exp As Exception
            WriteException("PettyCash", "GetDetailTableACC", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetARecordAndDetailTable(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(1) As SqlParameter
        Dim objReader As SqlDataReader
        Dim etAll As Parameter.PettyCash
        Dim etDetail As Parameter.PettyCash

        Try
            etAll = GetARecord(oET)
            etDetail = GetDetailTable(oET)
            etAll.PagingTable = etDetail.PagingTable
            Return etAll
        Catch exp As Exception
            WriteException("PettyCash", "GetARecordAndDetailTable", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetARecordAndDetailTableACC(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(0) As SqlParameter
        Dim objReader As SqlDataReader
        Dim etAll As Parameter.PettyCash
        Dim etDetail As Parameter.PettyCash

        Try
            etAll = GetARecordACC(oET)
            etDetail = GetDetailTableACC(oET)
            etAll.PagingTable = etDetail.PagingTable
            Return etAll
        Catch exp As Exception
            WriteException("PettyCash", "GetARecordAndDetailTableACC", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function InqPCReimburse(ByVal customclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(4) As SqlParameter
        With customclass
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = .SortBy
            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output
        End With
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInqPettyCashReimburse, params).Tables(0)
            customclass.TotalRecord = CType(params(4).Value, Int16)
            Return customclass
        Catch exp As Exception
            WriteException("PettyCash", "InqPCReimburse", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function InqPCReimburseACC(ByVal customclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(4) As SqlParameter
        With customclass
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = .SortBy
            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output
        End With
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInqPettyCashReimburseACC, params).Tables(0)
            customclass.TotalRecord = CType(params(4).Value, Int16)
            Return customclass
        Catch exp As Exception
            WriteException("PettyCash", "InqPCReimburseACC", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function getViewPCReimburseLabel(ByVal customclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
        params(0).Value = customclass.WhereCond
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewPCReimburseLabel, params).Tables(0)
            Return customclass
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")
        Catch exp As Exception
            WriteException("PettyCash", "getViewPCReimburseLabel", exp.Message + exp.StackTrace)
        End Try

    End Function
    Public Function GetViewPCReimburseGrid(ByVal customclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(1) As SqlParameter
        With customclass
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = .WhereCond
            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(1).Value = .SortBy
        End With
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewPCReimburseGrid, params).Tables(0)
            Return customclass
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")
        Catch exp As Exception
            WriteException("PettyCash", "GetViewPCReimburseGrid", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetViewPCReimburseGridACC(ByVal customclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(1) As SqlParameter
        With customclass
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = .WhereCond
            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(1).Value = .SortBy
        End With
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewPCReimburseGridACC, params).Tables(0)
            Return customclass
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")
        Catch exp As Exception
            WriteException("PettyCash", "GetViewPCReimburseGrid", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub SavePettyCashReversal(ByVal oET As Parameter.PettyCash)
        Dim objConnection As New SqlConnection(oET.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(8) {}

            With oET
                params(0) = New SqlParameter("@" & FLD_NM_BRANCH_ID, FLD_TYP_BRANCH_ID, FLD_LEN_BRANCH_ID)
                params(0).Value = .BranchId

                '                params(1) = New SqlParameter("@" & FLD_NM_BANK_ACC_ID, FLD_TYP_BANK_ACC_ID, FLD_LEN_BANK_ACC_ID)
                '                params(1).Value = .BankAccountID

                params(1) = New SqlParameter("@" & FLD_NM_PETTY_CASH_NO, FLD_TYP_PETTY_CASH_NO, FLD_LEN_PETTY_CASH_NO)
                params(1).Value = .PettyCashNo

                params(2) = New SqlParameter("@" & PRM_NM_LOGIN_ID, PRM_TYP_LOGIN_ID, PRM_LEN_LOGIN_ID)
                params(2).Value = .LoginId

                params(3) = New SqlParameter("@" & PRM_NM_BUSINESS_DATE, PRM_TYP_BUSINESS_DATE)
                params(3).Value = .BusinessDate

                '               params(5) = New SqlParameter("@" & PRM_NM_WOP, PRM_TYP_WOP, PRM_LEN_WOP)
                '                params(5).Value = .WOP

                params(4) = New SqlParameter("@" & PRM_NM_AMOUNT, PRM_TYP_AMOUNT)
                params(4).Value = .Amount

                params(5) = New SqlParameter("@" & PRM_NM_COMPANY_ID, PRM_TYP_COMPANY_ID)
                params(5).Value = .CoyID

                params(6) = New SqlParameter("@PC_Date", SqlDbType.DateTime)
                params(6).Value = .PCDate

                params(7) = New SqlParameter("@PC_Description", SqlDbType.VarChar, 50)
                params(7).Value = .Description

                params(8) = New SqlParameter("@PC_VoucherNo", SqlDbType.Char, 20)
                params(8).Value = .PCVoucherNo

                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, STR_SP_PROCESS_PC_REVERSAL, params)
            End With
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("PettyCash", "SavePettyCashReversal", exp.Message + exp.StackTrace)
            Throw New Exception("ERROR: Can not process petty cash reversal. " + exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Public Function GetPettyCashVoucher(ByVal customClass As Parameter.PettyCash) As DataSet
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter("@PettyCashNo", SqlDbType.Char, 20)
        params(0).Value = customClass.PettyCashNo

        Try
            Return SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spPettyCashVoucherPrint", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Sub EditCOAPettyCash(ByVal oET As Parameter.PettyCash)

        Try

            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
            params.Add(New SqlParameter("@BranchId", SqlDbType.Char, 3) With {.Value = oET.BranchId})
            params.Add(New SqlParameter("@PettyCashNo", SqlDbType.Char, 20) With {.Value = oET.PettyCashNo})

            params.Add(New SqlParameter("@CoaTrans", SqlDbType.Structured) With {.Value = oET.ListData})
            Dim pmsg = New SqlParameter("@errMsg", SqlDbType.VarChar, 500)
            pmsg.Direction = ParameterDirection.Output
            params.Add(pmsg)

            SqlHelper.ExecuteNonQuery(oET.strConnection, CommandType.StoredProcedure, "spPettyCashPaymentAllocationUpdate", params.ToArray)

            If (pmsg.Value.ToString().Trim.ToUpper <> "OK") Then
                Throw New Exception(pmsg.Value.ToString().Trim)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Function GetDetailTableWithSeq(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(2) As SqlParameter
        Dim objReader As SqlDataReader

        Try
            With oET
                params(0) = New SqlParameter("@" & FLD_NM_BRANCH_ID, FLD_TYP_BRANCH_ID, FLD_LEN_BRANCH_ID)
                params(0).Value = .BranchId

                params(1) = New SqlParameter("@" & FLD_NM_PETTY_CASH_NO, FLD_TYP_PETTY_CASH_NO, FLD_LEN_PETTY_CASH_NO)
                params(1).Value = .PettyCashNo
                params(2) = New SqlParameter("@SequenceNo",SqlDbType.Int)
                params(2).Value = .SequenceNo

                .PagingTable = SqlHelper.ExecuteDataset(.strConnection, CommandType.StoredProcedure, "spPettyCashDetailSeq", params).Tables(0)
            End With
            Return oET
        Catch exp As Exception
            WriteException("PettyCash", "GetDetailTable", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetDetailTableACCWithSeq(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(1) As SqlParameter
        Dim objReader As SqlDataReader

        Try
            With oET
                params(0) = New SqlParameter("@" & FLD_NM_PETTY_CASH_NO, FLD_TYP_PETTY_CASH_NO, FLD_LEN_PETTY_CASH_NO)
                params(0).Value = .PettyCashNo
                params(1) = New SqlParameter("@SequenceNo", SqlDbType.Int)
                params(1).Value = .SequenceNo

                .PagingTable = SqlHelper.ExecuteDataset(.strConnection, CommandType.StoredProcedure, "spPettyCashDetailACCWithSeq", params).Tables(0)
            End With
            Return oET
        Catch exp As Exception
            WriteException("PettyCash", "GetDetailTableACC", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetARecordAndDetailTableWithSeq(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(1) As SqlParameter
        Dim objReader As SqlDataReader
        Dim etAll As Parameter.PettyCash
        Dim etDetail As Parameter.PettyCash

        Try
            etAll = GetARecord(oET)
            etDetail = GetDetailTableWithSeq(oET)
            etAll.PagingTable = etDetail.PagingTable
            Return etAll
        Catch exp As Exception
            WriteException("PettyCash", "GetARecordAndDetailTable", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetARecordAndDetailTableACCWithSeq(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(0) As SqlParameter
        Dim objReader As SqlDataReader
        Dim etAll As Parameter.PettyCash
        Dim etDetail As Parameter.PettyCash

        Try
            etAll = GetARecordACC(oET)
            etDetail = GetDetailTableACCWithSeq(oET)
            etAll.PagingTable = etDetail.PagingTable
            Return etAll
        Catch exp As Exception
            WriteException("PettyCash", "GetARecordAndDetailTableACC", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetViewPCReimburseGridHistoryReject(ByVal customclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(1) As SqlParameter
        With customclass
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = .WhereCond
            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(1).Value = .SortBy
        End With
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewPCReimburseGridHistoryReject, params).Tables(0)
            Return customclass
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")
        Catch exp As Exception
            WriteException("PettyCash", "GetViewPCReimburseGrid", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetViewPCReimburseGridHistoryApprovel(ByVal customclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim params(1) As SqlParameter
        With customclass
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = .WhereCond
            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(1).Value = .SortBy
        End With
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "sppettycashreimburseHistoryApproval", params).Tables(0)
            Return customclass
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")
        Catch exp As Exception
            WriteException("PettyCash", "GetViewPCReimburseGrid", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
