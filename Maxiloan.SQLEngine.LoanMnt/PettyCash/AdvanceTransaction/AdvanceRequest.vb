


Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions


Public Class AdvanceRequest : Inherits DataAccessBase
#Region "PrivateConst"
    Private Const STR_SP_PROCESS_PETTY_CASH As String = "spProcessPettyCashAdvance"
    Private Const STR_SP_PETTY_CASH_ADVANCE_PAGING As String = "spPettyCashAdvancePaging"
    Private Const STR_SP_PETTY_CASH_ADVANCE_INQUIRY_VIEW As String = "spPettyCashAdvanceInquiryView"
    Private Const STR_SPT_PETTY_CASH_ADVANCE_RPT As String = "spPettyCashAdvanceRpt"
    Private Const STR_SP_PROCESS_ADVANCE_RTN As String = "spProcessPettyCashAdvanceReturn"

    Private Const STR_BRANCH_ID As String = "BranchID"
    Private Const STR_BANK_ACC_ID As String = "BankAccountID"
    Private Const STR_LOGIN_ID As String = "LoginID"
    Private Const STR_ADVANCE_NO As String = "AdvanceNo"
    Private Const STR_EMP_NAME As String = "EmployeeName"
    Private Const STR_BANK_ACC_NAME As String = "BankAccountName"
    Private Const STR_DESCRIPTION As String = "Description"
    Private Const STR_VOUCHER_NO_ADV As String = "VoucherNoAdv"
    Private Const STR_ADVANCE_DATE As String = "AdvanceDate"
    Private Const STR_AMOUNT As String = "Amount"
    Private Const STR_BANK_ACC_ID_RET As String = "BankAccountIdReturn"
    Private Const STR_CASHIER_ADV As String = "CashierAdvance"
    Private Const STR_CASHIER_RET As String = "CashierReturn"
    Private Const STR_STATUS_DATE As String = "StatusDate"
    Private Const STR_VOUCHER_NO_RTN As String = "VoucherNoRtn"

    Private Const STR_BUSINESS_DATE As String = "BusinessDate"
    Private Const STR_WOP As String = "WOP"
    Private Const STR_AMNT_RECEIVE As String = "AmountReceive"
    Private Const STR_COMPANY_ID As String = "CoyID"
    Private Const STR_TX_ID As String = "TransactionID"
    Private Const STR_VALUE_DATE As String = "ValueDate"
    Private Const STR_REF_NO As String = "ReferenceNo"
    Private Const STR_FLAG As String = "Flag"
    Private Const STR_PROCESS_ID As String = "ProcessID"
    Private Const STR_RECV_FROM As String = "ReceiveFrom"
    Private Const STR_APP_ID As String = "ApplicationID"
    Private Const STR_NOTES As String = "Notes"
    Private Const STR_BGNO As String = "BGNo"
    Private Const STR_BGDUE_DATE As String = "BGDueDate"
    Private Const STR_EMP_ID As String = "EmployeeId"
    Private Const STR_OPENING_SEQ As String = "OpeningSequence"
    Private Const STR_JOURNAL_CODE As String = "JournalCode"
    Private Const STR_VOUCHER_NO As String = "VoucherNo"
    Private Const STR_ERROR As String = "strError"

    '*** Petty Cash Adavnce Paging
    Private Const STR_CURR_PAGE As String = "CurrentPage"
    Private Const STR_PAGE_SIZE As String = "PageSize"
    Private Const STR_WHERE_COND As String = "WhereCond"
    Private Const STR_SORT_BY As String = "SortBy"
    Private Const STR_TOTAL_RECS As String = "TotalRecords"


    Private Const STR_ACC_TYPE_BANK As String = "B"
    Private Const STR_ACC_TYPE_CASH As String = "C"
    Private Const STR_WOP_BANK As String = "BA"
    Private Const STR_WOP_CASH As String = "CA"
    Private Const STR_ADV_REQ_TX_ID As String = "ADV"
    Private Const STR_ADV_REQ_PROCESS_ID As String = "ADVREQ"
    Private Const STR_ADV_REQ_FLAG As String = "D"
    Private Const STR_ADV_RET_TX_ID As String = "ADR"
    Private Const STR_ADV_RET_PROCESS_ID As String = "ADVRTN"
    Private Const STR_ADV_RET_FLAG As String = "R"
#End Region

    Public Sub SaveAdvanceTransaction(ByVal oETAdvanceRequest As Parameter.AdvanceRequest)
        Dim objConnection As New SqlConnection(oETAdvanceRequest.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(21) {}

            params(0) = New SqlParameter("@" & STR_BRANCH_ID, SqlDbType.VarChar, 3)
            params(0).Value = oETAdvanceRequest.BranchId

            params(1) = New SqlParameter("@" & STR_BANK_ACC_ID, SqlDbType.VarChar, 10)
            params(1).Value = oETAdvanceRequest.BankAccountID

            params(2) = New SqlParameter("@" & STR_LOGIN_ID, SqlDbType.VarChar, 10)
            params(2).Value = oETAdvanceRequest.LoginId

            params(3) = New SqlParameter("@" & STR_BUSINESS_DATE, SqlDbType.DateTime)
            params(3).Value = oETAdvanceRequest.BusinessDate

            params(4) = New SqlParameter("@" & STR_WOP, SqlDbType.VarChar, 7)
            If oETAdvanceRequest.BankAccountType.ToUpper() = "B" Then
                params(4).Value = STR_WOP_BANK
            Else
                params(4).Value = STR_WOP_CASH
            End If

            params(5) = New SqlParameter("@" & STR_AMNT_RECEIVE, SqlDbType.Decimal)
            params(5).Value = CType(oETAdvanceRequest.AmountString, Double)

            params(6) = New SqlParameter("@" & STR_COMPANY_ID, SqlDbType.VarChar, 3)
            params(6).Value = oETAdvanceRequest.CoyID

            params(7) = New SqlParameter("@" & STR_TX_ID, SqlDbType.VarChar, 10)
            params(7).Value = STR_ADV_REQ_TX_ID

            params(8) = New SqlParameter("@" & STR_VALUE_DATE, SqlDbType.DateTime)
            params(8).Value = oETAdvanceRequest.BusinessDate

            params(9) = New SqlParameter("@" & STR_REF_NO, SqlDbType.VarChar, 20)
            params(9).Value = ""

            params(10) = New SqlParameter("@" & STR_FLAG, SqlDbType.Char, 1)
            params(10).Value = STR_ADV_REQ_FLAG

            params(11) = New SqlParameter("@" & STR_PROCESS_ID, SqlDbType.VarChar, 10)
            params(11).Value = STR_ADV_REQ_PROCESS_ID

            params(12) = New SqlParameter("@" & STR_RECV_FROM, SqlDbType.VarChar, 50)
            params(12).Value = ""

            params(13) = New SqlParameter("@" & STR_APP_ID, SqlDbType.VarChar, 20)
            params(13).Value = oETAdvanceRequest.AppID

            params(14) = New SqlParameter("@" & STR_NOTES, SqlDbType.VarChar, 100)
            params(14).Value = oETAdvanceRequest.Description

            params(15) = New SqlParameter("@" & STR_BGNO, SqlDbType.Char, 20)
            params(15).Value = oETAdvanceRequest.BilyetGiroNo

            params(16) = New SqlParameter("@" & STR_BGDUE_DATE, SqlDbType.DateTime)
            params(16).Value = oETAdvanceRequest.BilyetGiroDueDate

            params(17) = New SqlParameter("@" & STR_EMP_ID, SqlDbType.Char, 20)
            params(17).Value = oETAdvanceRequest.EmployeeId

            params(18) = New SqlParameter("@" & STR_OPENING_SEQ, SqlDbType.Int)
            params(18).Direction = ParameterDirection.Output

            params(19) = New SqlParameter("@" & STR_JOURNAL_CODE, SqlDbType.VarChar, 20)
            params(19).Direction = ParameterDirection.Output

            params(20) = New SqlParameter("@" & STR_VOUCHER_NO, SqlDbType.VarChar, 50)
            params(20).Direction = ParameterDirection.Output

            params(21) = New SqlParameter("@" & STR_ERROR, SqlDbType.VarChar, 100)
            params(21).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, STR_SP_PROCESS_PETTY_CASH, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("AdvanceRequest", "SaveAdvanceTransaction", exp.Message + exp.StackTrace)
            Throw New Exception("ERROR: Can not process petty cash advance. " + exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Public Function GetPettyCashAdvanceList(ByVal oETAdvReq As Parameter.AdvanceRequest) As Parameter.AdvanceRequest

        Dim params(5) As SqlParameter

        Try

            params(0) = New SqlParameter("@" & STR_CURR_PAGE, SqlDbType.Int)
            params(0).Value = oETAdvReq.CurrentPage

            params(1) = New SqlParameter("@" & STR_PAGE_SIZE, SqlDbType.Int)
            params(1).Value = oETAdvReq.PageSize

            params(2) = New SqlParameter("@" & STR_WHERE_COND, SqlDbType.VarChar, 1000)
            params(2).Value = oETAdvReq.WhereCond

            params(3) = New SqlParameter("@" & STR_SORT_BY, SqlDbType.VarChar, 100)
            params(3).Value = oETAdvReq.SortBy

            params(4) = New SqlParameter("@" & STR_BUSINESS_DATE, SqlDbType.DateTime)
            params(4).Value = oETAdvReq.BusinessDate

            params(5) = New SqlParameter("@" & STR_TOTAL_RECS, SqlDbType.Int)
            params(5).Direction = ParameterDirection.Output

            oETAdvReq.PettyCashAdvanceList = SqlHelper.ExecuteDataset(oETAdvReq.strConnection, CommandType.StoredProcedure, STR_SP_PETTY_CASH_ADVANCE_PAGING, params).Tables(0)
            oETAdvReq.TotalRecord = CInt(params(5).Value)

            Return oETAdvReq
        Catch exp As Exception
            WriteException("AdvanceRequest", "GetPettyCashAdvanceList", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetAnAdvanceTransactionRecord(ByVal oETAdvReq As Parameter.AdvanceRequest) As Parameter.AdvanceRequest
        Dim params(1) As SqlParameter
        Dim objReader As SqlDataReader

        Try
            params(0) = New SqlParameter("@" & STR_BRANCH_ID, SqlDbType.VarChar, 3)
            params(0).Value = oETAdvReq.BranchId

            params(1) = New SqlParameter("@" & STR_ADVANCE_NO, SqlDbType.Char, 20)
            params(1).Value = oETAdvReq.AdvanceNo

            objReader = SqlHelper.ExecuteReader(oETAdvReq.strConnection, CommandType.StoredProcedure, STR_SP_PETTY_CASH_ADVANCE_INQUIRY_VIEW, params)
            If objReader Is Nothing Then
                Return Nothing
            End If

            If objReader.Read Then
                With oETAdvReq
                    .AdvanceNo = CType(objReader(STR_ADVANCE_NO), String)
                    .EmployeeId = CType(objReader(STR_EMP_ID), String)
                    .EmployeeName = CType(objReader(STR_EMP_NAME), String)
                    .BankAccountName = CType(objReader(STR_BANK_ACC_NAME), String)
                    .Description = CType(objReader(STR_DESCRIPTION), String)
                    .VoucherNoAdvance = CType(objReader(STR_VOUCHER_NO_ADV), String)
                    .AdvanceDate = CType(objReader(STR_ADVANCE_DATE), Date)
                    .AmountString = CType(objReader(STR_AMOUNT), String)
                    .ReturnToAccount = CType(objReader(STR_BANK_ACC_ID_RET), String)
                    .CashierAdvance = CType(objReader(STR_CASHIER_ADV), String)
                    .VoucherNoReturn = CType(objReader(.FLD_NM_VOUCHER_NO_RTN), String)
                    .AdvanceStatus = CType(objReader(.FLD_NM_ADVANCE_STATUS), String)
                    .CashierReturn = CType(objReader(STR_CASHIER_RET), String)
                    .StatusDate = CType(objReader(STR_STATUS_DATE), Date)
                End With
            End If
            objReader.Close()
            Return oETAdvReq
        Catch exp As Exception
            WriteException("AdvanceRequest", "GetAnAdvanceTransactionRecord", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function GetPettyCashAdvanceRpt(ByVal oETAdvReq As Parameter.AdvanceRequest) As Parameter.AdvanceRequest

        Dim params(2) As SqlParameter

        Try
            params(0) = New SqlParameter("@" & STR_WHERE_COND, SqlDbType.VarChar, 1000)
            params(0).Value = oETAdvReq.WhereCond

            params(1) = New SqlParameter("@" & STR_SORT_BY, SqlDbType.VarChar, 100)
            params(1).Value = oETAdvReq.SortBy

            params(2) = New SqlParameter("@" & STR_BUSINESS_DATE, SqlDbType.DateTime)
            params(2).Value = oETAdvReq.BusinessDate

            oETAdvReq.PettyCashAdvanceDataSet = SqlHelper.ExecuteDataset(oETAdvReq.strConnection, CommandType.StoredProcedure, STR_SPT_PETTY_CASH_ADVANCE_RPT, params)

            Return oETAdvReq
        Catch exp As Exception
            WriteException("AdvanceRequest", "GetPettyCashAdvanceRpt", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub SaveAdvanceReturnTransaction(ByVal oETAdvanceRequest As Parameter.AdvanceRequest)
        Dim objConnection As New SqlConnection(oETAdvanceRequest.strConnection)
        Dim objtrans As SqlTransaction
        Try

            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(19) {}

            params(0) = New SqlParameter("@" & STR_ADVANCE_NO, SqlDbType.Char, 20)
            params(0).Value = oETAdvanceRequest.AdvanceNo

            params(1) = New SqlParameter("@" & STR_EMP_NAME, SqlDbType.VarChar, 50)
            params(1).Value = oETAdvanceRequest.EmployeeName

            params(2) = New SqlParameter("@" & STR_BRANCH_ID, SqlDbType.VarChar, 3)
            params(2).Value = oETAdvanceRequest.BranchId

            params(3) = New SqlParameter("@" & STR_BANK_ACC_ID, SqlDbType.VarChar, 10)
            params(3).Value = oETAdvanceRequest.BankAccountID

            params(4) = New SqlParameter("@" & STR_LOGIN_ID, SqlDbType.VarChar, 10)
            params(4).Value = oETAdvanceRequest.LoginId

            params(5) = New SqlParameter("@" & STR_BUSINESS_DATE, SqlDbType.DateTime)
            params(5).Value = oETAdvanceRequest.BusinessDate

            params(6) = New SqlParameter("@" & STR_WOP, SqlDbType.VarChar, 7)
            If oETAdvanceRequest.BankAccountType.ToUpper() = "B" Then
                params(6).Value = STR_WOP_BANK
            Else
                params(6).Value = STR_WOP_CASH
            End If

            params(7) = New SqlParameter("@" & STR_AMNT_RECEIVE, SqlDbType.Decimal)
            params(7).Value = CType(oETAdvanceRequest.AmountString, Double)

            params(8) = New SqlParameter("@" & STR_COMPANY_ID, SqlDbType.VarChar, 3)
            params(8).Value = oETAdvanceRequest.CoyID

            params(9) = New SqlParameter("@" & STR_TX_ID, SqlDbType.VarChar, 10)
            params(9).Value = STR_ADV_RET_TX_ID

            params(10) = New SqlParameter("@" & STR_VALUE_DATE, SqlDbType.DateTime)
            params(10).Value = oETAdvanceRequest.BusinessDate

            params(11) = New SqlParameter("@" & STR_REF_NO, SqlDbType.VarChar, 20)
            params(11).Value = oETAdvanceRequest.AdvanceNo

            params(12) = New SqlParameter("@" & STR_FLAG, SqlDbType.Char, 1)
            params(12).Value = STR_ADV_RET_FLAG

            params(13) = New SqlParameter("@" & STR_PROCESS_ID, SqlDbType.VarChar, 10)
            params(13).Value = STR_ADV_RET_PROCESS_ID

            params(14) = New SqlParameter("@" & STR_RECV_FROM, SqlDbType.VarChar, 50)
            params(14).Value = oETAdvanceRequest.EmployeeName

            params(15) = New SqlParameter("@" & STR_APP_ID, SqlDbType.VarChar, 20)
            params(15).Value = ""

            params(16) = New SqlParameter("@" & STR_NOTES, SqlDbType.VarChar, 100)
            params(16).Value = oETAdvanceRequest.Description

            params(17) = New SqlParameter("@" & STR_BGNO, SqlDbType.Char, 20)
            params(17).Value = ""

            params(18) = New SqlParameter("@" & STR_BGDUE_DATE, SqlDbType.DateTime)
            params(18).Value = DBNull.Value

            params(19) = New SqlParameter("@" & STR_EMP_ID, SqlDbType.Char, 10)
            params(19).Value = oETAdvanceRequest.EmployeeId

            'params(20) = New SqlParameter("@" & STR_OPENING_SEQ, SqlDbType.Int)
            'params(20).Direction = ParameterDirection.Output

            'params(21) = New SqlParameter("@" & STR_JOURNAL_CODE, SqlDbType.VarChar, 20)
            'params(21).Direction = ParameterDirection.Output

            'params(22) = New SqlParameter("@" & STR_VOUCHER_NO, SqlDbType.VarChar, 50)
            'params(22).Direction = ParameterDirection.Output

            'params(23) = New SqlParameter("@" & STR_ERROR, SqlDbType.VarChar, 100)
            'params(23).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, STR_SP_PROCESS_ADVANCE_RTN, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("AdvanceRequest", "SaveAdvanceReturnTransaction", exp.Message + exp.StackTrace)
            Throw New Exception("ERROR: Can not process petty cash advance. " + exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub
    Public Function CreateAdvanceTransactionDetail(cnn As String, advanceNo As String, atDetail As IList(Of Parameter.AdvanceDetail)) As String
        Dim transaction As SqlTransaction = Nothing
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@atDetail", SqlDbType.Structured) With {.Value = Parameter.AdvanceDetail.ToDataTable(atDetail)})
        params.Add(New SqlParameter("@AdvanceNo", SqlDbType.VarChar, 20) With {.Value = advanceNo})
        Dim errPrm = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
        params.Add(errPrm)

        Dim conn As New SqlConnection(cnn)
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spAdvanceTransactionDetailSave", params.ToArray())
            If CType(errPrm.Value, String) <> "" Then
                Return CType(errPrm.Value, String)
            End If
            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()
            WriteException("AdvanceTransactionDetail", "AdvanceTransactionDetailSave", exp.Message + exp.StackTrace)
            Throw New Exception("AdvanceTransactionDetailSave")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
End Class
