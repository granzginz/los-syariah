

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class Cashier : Inherits AccMntBase
    Private Const spListCashier As String = "spCashierPaging"
    Private Const spCashierAdd As String = "spCashierAdd"
    Private Const spCashierDelete As String = "spCashierDelete"
    Private Const spCashierEdit As String = "spCashierUpdate"
    Private Const spCashierReport As String = "spCashierReport"
    Private Const spBranchListCashier As String = "spCashierList"

    Private Const PARAM_STATUS As String = "@cashierstatus"
    Private Const PARAM_CASHIERTITLE As String = "@cashiertitle"
    
    Public Function ListCashier(ByVal customclass As Parameter.Cashier) As Parameter.Cashier
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListCashier = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListCashier, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)

            Return customclass
        Catch exp As Exception
            WriteException("Cashier", "ListCashier", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub CashierAdd(ByVal customclass As Parameter.Cashier)
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(4) {}
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 5)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_CASHIERID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.CashierID

            params(2) = New SqlParameter(PARAM_CASHIERTITLE, SqlDbType.Char, 1)
            params(2).Value = customclass.CashierTitle

            params(3) = New SqlParameter(PARAM_STATUS, SqlDbType.Bit)
            params(3).Value = customclass.CashierStatus

            params(4) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
            params(4).Value = getReferenceDBName()



            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCashierAdd, params)
            objtrans.Commit()

        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Cashier", "CashierAdd", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Public Sub CashierEdit(ByVal customclass As Parameter.Cashier)
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim strError As String
        Try
            Dim params() As SqlParameter = New SqlParameter(3) {}
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 5)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_CASHIERID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.CashierID

            params(2) = New SqlParameter(PARAM_CASHIERTITLE, SqlDbType.Char, 1)
            params(2).Value = customclass.CashierTitle

            params(3) = New SqlParameter(PARAM_STATUS, SqlDbType.Bit)
            params(3).Value = customclass.CashierStatus
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCashierEdit, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Cashier", "CashierEdit", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Public Sub CashierDelete(ByVal customclass As Parameter.Cashier)
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(2) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 5)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_CASHIERID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.CashierID

            params(2) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
            params(2).Value = getReferenceDBName()

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCashierDelete, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Cashier", "CashierDelete", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Public Function CashierReport(ByVal customclass As Parameter.Cashier) As Parameter.Cashier
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCashierReport, params)
            Return customclass
        Catch exp As Exception
            WriteException("Cashier", "CashierReport", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
