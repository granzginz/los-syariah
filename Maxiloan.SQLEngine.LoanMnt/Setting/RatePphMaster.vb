

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class RatePphMaster : Inherits AccMntBase
    Private Const spListRatePphMaster As String = "spRatePphPaging"
    'Private Const spReportRatePphMaster As String = "spRatePphReport"
    Private Const spRatePphMasterAdd As String = "spRatePphAdd"
    Private Const spRatePphMasterUpdate As String = "spRatePphUpdate"
    Private Const spRatePphMasterDelete As String = "spRatePphDelete"


    'Private Const PARAM_STATUS As String = "@isactive"

    Public Function ListRatePphMaster(ByVal customclass As Parameter.RatePphMaster) As Parameter.RatePphMaster
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            If customclass.SortBy.Contains("From") Or customclass.SortBy.Contains("To") Then
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                If customclass.SortBy.Contains("From") Then
                    params(3).Value = customclass.SortBy.Insert(0, "[").Insert(5, "]")
                Else
                    params(3).Value = customclass.SortBy.Insert(0, "[").Insert(3, "]")
                End If

            Else
                    params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                params(3).Value = customclass.SortBy
            End If


            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListRatePphMaster = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListRatePphMaster, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            'WriteException("RatePphMaster", "ListRatePphMaster", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function AddRatePphMaster(ByVal customclass As Parameter.RatePphMaster) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(5) {}
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            params(0) = New SqlParameter("@level", SqlDbType.SmallInt, 5)
            params(0).Value = customclass.Level

            params(1) = New SqlParameter("@from", SqlDbType.Decimal, 17)
            params(1).Value = customclass.AmntFrom

            params(2) = New SqlParameter("@to", SqlDbType.Decimal, 17)
            params(2).Value = customclass.AmntTo

            params(3) = New SqlParameter("@rate_npwp", SqlDbType.SmallInt, 30)
            params(3).Value = customclass.RateNpwp

            params(4) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(4).Direction = ParameterDirection.Output

            params(5) = New SqlParameter("@rate_non_npwp", SqlDbType.SmallInt, 30)
            params(5).Value = customclass.RateNonNpwp

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spRatePphMasterAdd, params)
            objtrans.Commit()
            Return CStr(params(4).Value)
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("RatePphMaster", "ListRatePphMaster", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Public Function UpdateRatePphMaster(ByVal customclass As Parameter.RatePphMaster) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(5) {}
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            params(0) = New SqlParameter("@level", SqlDbType.SmallInt, 5)
            params(0).Value = customclass.Level

            params(1) = New SqlParameter("@from", SqlDbType.Decimal, 50)
            params(1).Value = customclass.AmntFrom

            params(2) = New SqlParameter("@to", SqlDbType.Decimal)
            params(2).Value = customclass.AmntTo

            params(3) = New SqlParameter("@rate_npwp", SqlDbType.SmallInt, 30)
            params(3).Value = customclass.RateNpwp

            params(4) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(4).Direction = ParameterDirection.Output

            params(5) = New SqlParameter("@rate_non_npwp", SqlDbType.SmallInt, 7)
            params(5).Value = customclass.RateNonNpwp

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spRatePphMasterUpdate, params)

            objtrans.Commit()
            Return CStr(params(4).Value)
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("RatePphMaster", "UpdateRatePphMaster", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Function

    Public Function DeleteRatePphMaster(ByVal customclass As Parameter.RatePphMaster) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(1) {}
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            params(0) = New SqlParameter("@level", SqlDbType.SmallInt, 5)
            params(0).Value = customclass.Level

            params(1) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(1).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spRatePphMasterDelete, params)
            objtrans.Commit()
            Return CStr(params(1).Value)
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("RatePphMaster", "DeleteRatePphMaster", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    'Public Function ReportRatePphMaster(ByVal customclass As Parameter.RatePphMaster) As Parameter.RatePphMaster
    '    Dim params() As SqlParameter = New SqlParameter(1) {}
    '    Try
    '        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
    '        params(0).Value = customclass.WhereCond

    '        params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
    '        params(1).Value = customclass.SortBy

    '        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spReportRatePph, params)
    '        Return customclass
    '    Catch exp As Exception
    '        WriteException("RatePphMaster", "ReportRatePphMaster", exp.Message + exp.StackTrace)
    '    End Try
    'End Function
End Class

