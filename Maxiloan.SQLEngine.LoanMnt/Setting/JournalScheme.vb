
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region


Public Class JournalScheme : Inherits Maxiloan.SQLEngine.DataAccessBase
    Private Const LIST_SELECT As String = "spJournalSchemePaging"
    Private Const LIST_ADDHDR As String = "spJournalSchemeAddHdr"
    Private Const LIST_ADDDTL As String = "spJournalSchemeAddDtl"
    Private Const LIST_EDITDTL As String = "spJournalSchemeEditDtl"
    Private Const LIST_DELETE As String = "spJournalSchemeDelete"
    Private Const LIST_EDITHDR As String = "spJournalSchemeEditHdr"
    Private Const LIST_SAVEEDITHDR As String = "spJournalSchemeSaveEditHdr"
    Private Const LIST_SAVEADDHDR As String = "spJournalSchemeSaveAddHdr"
    Private Const LIST_COPYFROM As String = "spJournalSchemeCopyFrom"
    Private Const LIST_REPORT As String = "spJournalSchemeReport"
    Private oDBTransaction As SqlTransaction

    Public Function GetJournalScheme(ByVal oCustomClass As Parameter.JournalScheme) As Parameter.JournalScheme

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = oCustomClass.CurrentPage
            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = oCustomClass.PageSize
            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
            params(2).Value = oCustomClass.WhereCond
            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
            params(3).Value = oCustomClass.SortBy
            params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oCustomClass.TotalRecords = CType(params(4).Value, Int64)
            Return oCustomClass
        Catch exp As Exception
            Throw New Exception("Failed On Get Journal Scheme")
            WriteException("JournalScheme", "GetJournalScheme", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetJournalSchemeReport(ByVal oCustomClass As Parameter.JournalScheme) As Parameter.JournalScheme
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
            params(0).Value = oCustomClass.WhereCond
            params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar)
            params(1).Value = oCustomClass.SortBy
            oCustomClass.ListDataReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oCustomClass
        Catch exp As Exception
            Throw New Exception("Failed On Get Journal Scheme Report")
            WriteException("JournalScheme", "GetJournalSchemeReport", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetJournalSchemeEditHdr(ByVal oCustomClass As Parameter.JournalScheme) As Parameter.JournalScheme
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = oCustomClass.Id
        Try
            reader = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_EDITHDR, params)
            If reader.Read Then
                oCustomClass.Id = reader("JournalSchemeID").ToString
                oCustomClass.Description = reader("Description").ToString
                oCustomClass.Status = reader("Status").ToString
            End If
            reader.Close()
            Return oCustomClass
        Catch exp As Exception
            Throw New Exception("Failed On Get Journal Scheme Header")
            WriteException("JournalScheme", "GetJournalSchemeEditHdr", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetJournalSchemeEditDtl(ByVal oCustomClass As Parameter.JournalScheme) As Parameter.JournalScheme
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = oCustomClass.Id
        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_EDITDTL, params).Tables(0)
            Return oCustomClass
        Catch exp As Exception
            Throw New Exception("Failed On Get Journal SchemeDetail")
            WriteException("JournalScheme", "GetJournalSchemeEditDtl", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetJournalSchemeAddDtl(ByVal oCustomClass As Parameter.JournalScheme) As Parameter.JournalScheme
        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_ADDDTL).Tables(0)
            Return oCustomClass
        Catch exp As Exception
            Throw New Exception("Failed On Get Journal SchemeDetail")
            WriteException("JournalScheme", "GetJournalSchemeEditDtl", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetJournalSchemeCopyFrom(ByVal oCustomClass As Parameter.JournalScheme) As Parameter.JournalScheme
        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_COPYFROM).Tables(0)
            Return oCustomClass
        Catch exp As Exception
            Throw New Exception("Failed On Copy Data")
            WriteException("JournalScheme", "GetJournalSchemeCopyFrom", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function JournalSchemeSaveAdd(ByVal ocustomClass As Parameter.JournalScheme) As String
        Dim intLoop As Integer

        Dim oCommand As New SqlCommand
        Dim Err As String = ""
        Dim arrPaymentAllocationID() As String
        Dim objCon As New SqlConnection(ocustomClass.strConnection)

        Dim arrCOA() As String
        Dim spName As String = "spJournalSchemeSaveAdddtl"
        arrPaymentAllocationID = Split(ocustomClass.PaymentAllocationID, ",")
        arrCOA = Split(ocustomClass.COA, ",")
        Dim trans As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            trans = objCon.BeginTransaction
            oCommand.Connection = objCon
            oCommand.Transaction = trans

            oCommand.CommandText = LIST_SAVEADDHDR
            oCommand.CommandType = CommandType.StoredProcedure
            With oCommand
                .Parameters.Add(New SqlParameter("@Id", SqlDbType.Char, 10)).Value = ocustomClass.Id
                .Parameters.Add(New SqlParameter("@Description", SqlDbType.VarChar, 100)).Value = ocustomClass.Description
                .Parameters.Add(New SqlParameter("@Status", SqlDbType.VarChar, 100)).Value = ocustomClass.Status
                .Parameters.Add(New SqlParameter("@Err", SqlDbType.VarChar, 50)).Direction = ParameterDirection.Output
                .ExecuteNonQuery()
            End With
            Err = CType(oCommand.Parameters.Item("@Err").Value, String)
            If Err <> "" Then
                trans.Rollback()
                Return Err
            End If
            oCommand.Parameters.Clear()

            oCommand.CommandText = spName
            oCommand.CommandType = CommandType.StoredProcedure
            oCommand.Parameters.Add("@ID", SqlDbType.VarChar, 10)
            oCommand.Parameters.Add("@PaymentAllocationID", SqlDbType.VarChar, 10)
            oCommand.Parameters.Add("@COA", SqlDbType.VarChar, 25)
            For intLoop = 0 To UBound(arrPaymentAllocationID) - 1
                With oCommand
                    .Parameters("@ID").Value = ocustomClass.Id
                    .Parameters("@PaymentAllocationID").Value = arrPaymentAllocationID(intLoop)
                    .Parameters("@COA").Value = arrCOA(intLoop)
                    .ExecuteNonQuery()
                End With
            Next
            trans.Commit()
            Return ""
        Catch exp As Exception
            trans.Rollback()
            WriteException("JournalScheme", "JournalSchemeSaveAdd", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Insert Journal Scheme")
        Finally
            oCommand.Parameters.Clear()
            oCommand.Dispose()
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function

    Public Sub JournalSchemeSaveEdit(ByVal ocustomClass As Parameter.JournalScheme)
        Dim intLoop As Integer
        Dim arrPaymentAllocationID() As String
        Dim objcon As New SqlConnection(ocustomClass.strConnection)
        Dim trans As SqlTransaction
        Dim oCommand As New SqlCommand
        Dim arrCOA() As String
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            trans = objcon.BeginTransaction
            Dim spName As String = "spJournalSchemeSaveEditdtl"
            arrPaymentAllocationID = Split(ocustomClass.PaymentAllocationID, ",")
            arrCOA = Split(ocustomClass.COA, ",")

            oCommand.Connection = objcon
            oCommand.Transaction = trans

            oCommand.CommandText = LIST_SAVEEDITHDR
            oCommand.CommandType = CommandType.StoredProcedure
            With oCommand
                .Parameters.Add(New SqlParameter("@Id", SqlDbType.Char, 10)).Value = ocustomClass.Id
                .Parameters.Add(New SqlParameter("@Description", SqlDbType.VarChar, 100)).Value = ocustomClass.Description
                .Parameters.Add(New SqlParameter("@Status", SqlDbType.VarChar, 100)).Value = ocustomClass.Status
                .ExecuteNonQuery()
            End With
            oCommand.Parameters.Clear()
            oCommand.CommandText = spName
            oCommand.CommandType = CommandType.StoredProcedure
            For intLoop = 0 To UBound(arrPaymentAllocationID) - 1
                With oCommand
                    .Parameters.Add(New SqlParameter("@Id", SqlDbType.Char, 10)).Value = ocustomClass.Id
                    .Parameters.Add(New SqlParameter("@PaymentAllocationID", SqlDbType.Char, 10)).Value = arrPaymentAllocationID(intLoop)
                    .Parameters.Add(New SqlParameter("@COA", SqlDbType.Char, 25)).Value = arrCOA(intLoop)
                    .ExecuteNonQuery()
                End With
                oCommand.Parameters.Clear()
            Next
            trans.Commit()
        Catch exp As Exception
            trans.Rollback()
            WriteException("JournalScheme", "JournalSchemeSaveEdit", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update Journal Scheme")
        Finally
            oCommand.Parameters.Clear()
            oCommand.Dispose()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub

    Public Sub JournalSchemeDelete(ByVal ocustomClass As Parameter.JournalScheme)
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = ocustomClass.Id
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
        Catch exp As Exception
            WriteException("JournalScheme", "JournalSchemeDelete", exp.Message + exp.StackTrace)
            Throw New Exception("Record Has Been Use By Another Process")
        End Try
    End Sub
End Class
