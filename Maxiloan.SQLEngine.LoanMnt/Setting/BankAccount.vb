

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class BankAccount : Inherits AccMntBase
    Private Const spListBankAccount As String = "spBankAccountPaging"
    Private Const spReportBankAccount As String = "spBankAccountReport"
    Private Const spBankAccountAdd As String = "spBankAccountAdd"
    Private Const spBankAccountUpdate As String = "spBankAccountUpdate"
    Private Const spBankAccountUpdateH As String = "spBankAccountUpdateH"
    Private Const spBankAccountDelete As String = "spBankAccountDelete"
    Private Const spBankAccountInformasi As String = "spBankAccountInformasi"
    Private Const spBankAccountList As String = "spBankAccountList"

    Private Const PARAM_STATUS As String = "@status"
    Private Const PARAM_ISGENERATE As String = "@isgenerate"

    Public Function ListBankAccount(ByVal customclass As Parameter.BankAccount) As Parameter.BankAccount
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListBankAccount = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListBankAccount, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)

            Return customclass
        Catch exp As Exception
            WriteException("BankAccount", "ListBankAccount", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AddBankAccount(ByVal customclass As Parameter.BankAccount, _
                                    ByVal oClassAddress As Parameter.Address, _
                                    ByVal oClassPersonal As Parameter.Personal) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(32) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(1).Value = customclass.BankAccountID

            params(2) = New SqlParameter(PARAM_BANKACCOUNTNAME, SqlDbType.VarChar, 50)
            params(2).Value = customclass.BankAccountName

            params(3) = New SqlParameter(PARAM_BANKID, SqlDbType.VarChar, 5)
            params(3).Value = customclass.BankID

            params(4) = New SqlParameter(PARAM_BANKBRANCH, SqlDbType.VarChar, 50)
            params(4).Value = customclass.BankBranch

            params(5) = New SqlParameter(PARAM_ACCOUNTNAME, SqlDbType.VarChar, 50)
            params(5).Value = customclass.AccountName

            params(6) = New SqlParameter(PARAM_ACCOUNTNO, SqlDbType.VarChar, 50)
            params(6).Value = customclass.AccountNo

            params(7) = New SqlParameter(PARAM_COA, SqlDbType.VarChar, 25)
            params(7).Value = customclass.Coa

            params(8) = New SqlParameter(PARAM_BANKACCOUNTTYPE, SqlDbType.VarChar, 5)
            params(8).Value = customclass.BankAccountType

            params(9) = New SqlParameter(PARAM_BANKPURPOSE, SqlDbType.VarChar, 5)
            params(9).Value = customclass.BankPurpose

            params(10) = New SqlParameter(PARAM_ADDRESS, SqlDbType.VarChar, 100)
            params(10).Value = oClassAddress.Address

            params(11) = New SqlParameter(PARAM_RT, SqlDbType.VarChar, 3)
            params(11).Value = oClassAddress.RT

            params(12) = New SqlParameter(PARAM_RW, SqlDbType.VarChar, 3)
            params(12).Value = oClassAddress.RW

            params(13) = New SqlParameter(PARAM_KELURAHAN, SqlDbType.VarChar, 30)
            params(13).Value = oClassAddress.Kelurahan

            params(14) = New SqlParameter(PARAM_KECAMATAN, SqlDbType.VarChar, 30)
            params(14).Value = oClassAddress.Kecamatan

            params(15) = New SqlParameter(PARAM_CITY, SqlDbType.VarChar, 30)
            params(15).Value = oClassAddress.City

            params(16) = New SqlParameter(PARAM_ZIPCODE, SqlDbType.VarChar, 5)
            params(16).Value = oClassAddress.ZipCode

            params(17) = New SqlParameter(PARAM_AREAPHONE1, SqlDbType.VarChar, 4)
            params(17).Value = oClassAddress.AreaPhone1

            params(18) = New SqlParameter(PARAM_PHONE1, SqlDbType.VarChar, 10)
            params(18).Value = oClassAddress.Phone1

            params(19) = New SqlParameter(PARAM_AREAPHONE2, SqlDbType.VarChar, 4)
            params(19).Value = oClassAddress.AreaPhone2

            params(20) = New SqlParameter(PARAM_PHONE2, SqlDbType.VarChar, 10)
            params(20).Value = oClassAddress.AreaPhone2

            params(21) = New SqlParameter(PARAM_AREAFAX, SqlDbType.VarChar, 4)
            params(21).Value = oClassAddress.AreaFax

            params(22) = New SqlParameter(PARAM_FAX, SqlDbType.VarChar, 10)
            params(22).Value = oClassAddress.Fax

            params(23) = New SqlParameter(PARAM_EMAIL, SqlDbType.VarChar, 30)
            params(23).Value = oClassPersonal.Email

            params(24) = New SqlParameter(PARAM_MOBILEPHONE, SqlDbType.VarChar, 20)
            params(24).Value = oClassPersonal.MobilePhone

            params(25) = New SqlParameter(PARAM_CONTACTPERSONNAME, SqlDbType.VarChar, 50)
            params(25).Value = oClassPersonal.PersonName

            params(26) = New SqlParameter(PARAM_CONTACTPERSONTITLE, SqlDbType.VarChar, 50)
            params(26).Value = oClassPersonal.PersonTitle

            params(27) = New SqlParameter(PARAM_STATUS, SqlDbType.Bit)
            params(27).Value = customclass.Status

            params(28) = New SqlParameter(PARAM_ISGENERATE, SqlDbType.Bit)
            params(28).Value = customclass.IsGenerate

            params(29) = New SqlParameter(PARAM_ERRORNO, SqlDbType.SmallInt)
            params(29).Direction = ParameterDirection.Output

            params(30) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(30).Direction = ParameterDirection.Output

            params(31) = New SqlParameter("@BankAccountDefault", SqlDbType.Bit)
            params(31).Value = customclass.BankAccountDefault

            params(32) = New SqlParameter("@SweepOtomatis", SqlDbType.Bit)
            params(32).Value = customclass.SweepOtomatis

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spBankAccountAdd, params)
            objtrans.Commit()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            Return CStr(params(30).Value)

        Catch exp As Exception
            objtrans.Rollback()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            WriteException("BankAccount", "AddBankAccount", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function UpdateBankAccount(ByVal customclass As Parameter.BankAccount, _
                                    ByVal oClassAddress As Parameter.Address, _
                                    ByVal oClassPersonal As Parameter.Personal) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(34) {}
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(1).Value = customclass.BankAccountID

            params(2) = New SqlParameter(PARAM_BANKACCOUNTNAME, SqlDbType.VarChar, 50)
            params(2).Value = customclass.BankAccountName

            params(3) = New SqlParameter(PARAM_BANKID, SqlDbType.VarChar, 5)
            params(3).Value = customclass.BankID

            params(4) = New SqlParameter(PARAM_BANKBRANCH, SqlDbType.VarChar, 50)
            params(4).Value = customclass.BankBranch

            params(5) = New SqlParameter(PARAM_ACCOUNTNAME, SqlDbType.VarChar, 50)
            params(5).Value = customclass.AccountName

            params(6) = New SqlParameter(PARAM_ACCOUNTNO, SqlDbType.VarChar, 50)
            params(6).Value = customclass.AccountNo

            params(7) = New SqlParameter(PARAM_COA, SqlDbType.VarChar, 25)
            params(7).Value = customclass.Coa

            params(8) = New SqlParameter(PARAM_BANKACCOUNTTYPE, SqlDbType.VarChar, 5)
            params(8).Value = customclass.BankAccountType

            params(9) = New SqlParameter(PARAM_BANKPURPOSE, SqlDbType.VarChar, 5)
            If IsNothing(customclass.BankPurpose) Then
                params(9).Value = ""
            Else
                params(9).Value = customclass.BankPurpose
            End If


            params(10) = New SqlParameter(PARAM_ADDRESS, SqlDbType.VarChar, 100)
            params(10).Value = oClassAddress.Address

            params(11) = New SqlParameter(PARAM_RT, SqlDbType.VarChar, 3)
            params(11).Value = oClassAddress.RT

            params(12) = New SqlParameter(PARAM_RW, SqlDbType.VarChar, 3)
            params(12).Value = oClassAddress.RW

            params(13) = New SqlParameter(PARAM_KELURAHAN, SqlDbType.VarChar, 30)
            params(13).Value = oClassAddress.Kelurahan

            params(14) = New SqlParameter(PARAM_KECAMATAN, SqlDbType.VarChar, 30)
            params(14).Value = oClassAddress.Kecamatan

            params(15) = New SqlParameter(PARAM_CITY, SqlDbType.VarChar, 30)
            params(15).Value = oClassAddress.City

            params(16) = New SqlParameter(PARAM_ZIPCODE, SqlDbType.VarChar, 5)
            params(16).Value = oClassAddress.ZipCode

            params(17) = New SqlParameter(PARAM_AREAPHONE1, SqlDbType.VarChar, 4)
            params(17).Value = oClassAddress.AreaPhone1

            params(18) = New SqlParameter(PARAM_PHONE1, SqlDbType.VarChar, 10)
            params(18).Value = oClassAddress.Phone1

            params(19) = New SqlParameter(PARAM_AREAPHONE2, SqlDbType.VarChar, 4)
            params(19).Value = oClassAddress.AreaPhone2

            params(20) = New SqlParameter(PARAM_PHONE2, SqlDbType.VarChar, 10)
            params(20).Value = oClassAddress.AreaPhone2

            params(21) = New SqlParameter(PARAM_AREAFAX, SqlDbType.VarChar, 4)
            params(21).Value = oClassAddress.AreaFax

            params(22) = New SqlParameter(PARAM_FAX, SqlDbType.VarChar, 10)
            params(22).Value = oClassAddress.Fax

            params(23) = New SqlParameter(PARAM_EMAIL, SqlDbType.VarChar, 30)
            params(23).Value = oClassPersonal.Email

            params(24) = New SqlParameter(PARAM_MOBILEPHONE, SqlDbType.VarChar, 20)
            params(24).Value = oClassPersonal.MobilePhone

            params(25) = New SqlParameter(PARAM_CONTACTPERSONNAME, SqlDbType.VarChar, 50)
            params(25).Value = oClassPersonal.PersonName

            params(26) = New SqlParameter(PARAM_CONTACTPERSONTITLE, SqlDbType.VarChar, 50)
            params(26).Value = oClassPersonal.PersonTitle

            params(27) = New SqlParameter(PARAM_STATUS, SqlDbType.Bit)
            params(27).Value = customclass.Status

            params(28) = New SqlParameter(PARAM_ISGENERATE, SqlDbType.Bit)
            params(28).Value = customclass.IsGenerate

            params(29) = New SqlParameter(PARAM_ERRORNO, SqlDbType.SmallInt)
            params(29).Direction = ParameterDirection.Output

            params(30) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(30).Direction = ParameterDirection.Output

            params(31) = New SqlParameter("@BankBranchId", SqlDbType.Int)
            If IsDBNull(customclass.BankBranchId) Or customclass.BankBranchId = "" Then
                params(31).Value = 0
            Else
                params(31).Value = customclass.BankBranchId
            End If


            params(32) = New SqlParameter("@TarifSKN", SqlDbType.Decimal)
            params(32).Value = customclass.TarifSKN

            params(33) = New SqlParameter("@TarifRTGS", SqlDbType.Decimal)
            params(33).Value = customclass.TarifRTGS

            params(34) = New SqlParameter("@BankAccountDefault", SqlDbType.Bit)
            params(34).Value = customclass.BankAccountDefault

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spBankAccountUpdate, params)
            objtrans.Commit()

            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            Return CStr(params(30).Value)
        Catch exp As Exception
            objtrans.Rollback()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            WriteException("BankAccount", "UpdateBankAccount", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function UpdateBankAccountH(ByVal customclass As Parameter.BankAccount) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(9) {}
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(1).Value = customclass.BankAccountID

            params(2) = New SqlParameter(PARAM_BANKACCOUNTNAME, SqlDbType.VarChar, 50)
            params(2).Value = customclass.BankAccountName

            params(3) = New SqlParameter(PARAM_COA, SqlDbType.VarChar, 25)
            params(3).Value = customclass.Coa

            params(4) = New SqlParameter(PARAM_BANKACCOUNTTYPE, SqlDbType.VarChar, 5)
            params(4).Value = customclass.BankAccountType

            params(5) = New SqlParameter(PARAM_BANKPURPOSE, SqlDbType.VarChar, 5)
            params(5).Value = customclass.BankPurpose

            params(6) = New SqlParameter(PARAM_STATUS, SqlDbType.Bit)
            params(6).Value = customclass.Status

            params(7) = New SqlParameter(PARAM_ISGENERATE, SqlDbType.Bit)
            params(7).Value = customclass.IsGenerate

            params(8) = New SqlParameter("@BankAccountDefault", SqlDbType.Bit)
            params(8).Value = customclass.BankAccountDefault

            params(9) = New SqlParameter("@SweepOtomatis", SqlDbType.Bit)
            params(9).Value = customclass.SweepOtomatis

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spBankAccountUpdateH, params)
            objtrans.Commit()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        Catch exp As Exception
            objtrans.Rollback()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            WriteException("BankAccount", "UpdateBankAccountH", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Function DeleteBankAccount(ByVal customclass As Parameter.BankAccount) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(3) {}
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            params(0) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(0).Value = customclass.BankAccountID

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(1).Value = customclass.BranchId

            params(2) = New SqlParameter(PARAM_ERRORNO, SqlDbType.SmallInt)
            params(2).Direction = ParameterDirection.Output

            params(3) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(3).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spBankAccountDelete, params)
            objtrans.Commit()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            Return CStr(params(3).Value)
        Catch exp As Exception
            objtrans.Rollback()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            WriteException("BankAccount", "DeleteBankAccount", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function BankAccountInformasi(ByVal strConnection As String, _
                                        ByVal strBankAccountID As String, _
                                        ByVal strBranchId As String) As DataTable

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(0).Value = strBankAccountID

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = strBranchId

            Return SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, spBankAccountInformasi, params).Tables(0)
        Catch exp As Exception
            WriteException("BankAccount", "BankAccountInformasi", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function ReportBankAccount(ByVal customclass As Parameter.BankAccount) As Parameter.BankAccount

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spReportBankAccount, params)
            Return customclass
        Catch exp As Exception
            WriteException("BankAccount", "ReportBankAccount", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function BankAccountListByType(ByVal oCustom As Parameter.BankAccount) As DataTable

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BankAccountType", SqlDbType.VarChar, 5)
            params(0).Value = oCustom.BankAccountType

            params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(1).Value = oCustom.BranchId

            Return SqlHelper.ExecuteDataset(oCustom.strConnection, CommandType.StoredProcedure, spBankAccountList, params).Tables(0)
        Catch exp As Exception
            WriteException("BankAccount", "BankAccountList", exp.Message + exp.StackTrace)
        End Try

    End Function
End Class
