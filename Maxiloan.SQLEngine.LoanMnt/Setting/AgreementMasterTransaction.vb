﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class AgreementMasterTransaction : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name        
    Private Const LIST_SELECT As String = "spAgreementMasterTransactionPaging"
    Private Const LIST_BY_ID As String = "spAgreementMasterTransactionView"
    Private Const LIST_ADD As String = "spAgreementMasterTransactionSaveAdd"
    Private Const LIST_UPDATE As String = "spAgreementMasterTransactionSaveEdit"
    Private Const LIST_DELETE As String = "spAgreementMasterTransactionDelete"    
#End Region

    Public Function GetAgreementMasterTransaction(ByVal oCustomClass As Parameter.AgreementMasterTransaction) As Parameter.AgreementMasterTransaction
        Dim oReturnValue As New Parameter.AgreementMasterTransaction
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.AgreementMasterTransaction.GetAgreementMasterTransaction")
        End Try
    End Function

    Public Function GetAgreementMasterTransactionList(ByVal ocustomClass As Parameter.AgreementMasterTransaction) As Parameter.AgreementMasterTransaction
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.AgreementMasterTransaction
        params(0) = New SqlParameter("@TransID", SqlDbType.Char, 3)
        params(0).Value = ocustomClass.TransID
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.TransID = ocustomClass.TransID
                oReturnValue.TransDesc = reader("TransDesc").ToString
                oReturnValue.Isactive = CBool(reader("IsActive").ToString)
                oReturnValue.NoUrutTrans = CInt(reader("NoUrutTransaksi"))

                'If reader("BungaNett") = "" Then
                '    oReturnValue.BungaNet = ""
                'Else
                '    oReturnValue.BungaNet = CStr(reader("BungaNett")).Trim
                'End If
                oReturnValue.BungaNet = reader("BungaNett").ToString
                oReturnValue.PaymentAllocationIDSupplier = reader("PaymentAllocationIDSupplier").ToString
                oReturnValue.isDistribuTable = CBool(reader("isDistributable").ToString)
                oReturnValue.defaultValueTagihanKeSupplier = reader("DefaultValue").ToString
                oReturnValue.isReadOnly = CBool(reader("isReadOnly"))
                oReturnValue.GroupTransID = reader("GroupTransID").ToString
                oReturnValue.SupplierEmployeePosition = reader("SupplierEmployeePosition").ToString
            End If
                reader.Close()
                Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.AgreementMasterTransaction.GetAgreementMasterTransactionList")
        End Try
    End Function

    Public Function AgreementMasterTransactionSaveAdd(ByVal ocustomClass As Parameter.AgreementMasterTransaction) As String
        Dim ErrMessage As String = ""
        Dim params(11) As SqlParameter
        params(0) = New SqlParameter("@TransID", SqlDbType.Char, 3)
        params(0).Value = ocustomClass.TransIDEdit
        params(1) = New SqlParameter("@TransDesc", SqlDbType.VarChar, 50)
        params(1).Value = ocustomClass.TransDesc
        params(2) = New SqlParameter("@IsActive", SqlDbType.Bit)
        params(2).Value = ocustomClass.Isactive
        params(3) = New SqlParameter("@PaymentAllocationIDSupplier", SqlDbType.Char, 10)
        params(3).Value = ocustomClass.PaymentAllocationIDSupplier
        params(4) = New SqlParameter("@NoUrutTransaksi", SqlDbType.SmallInt)
        params(4).Value = ocustomClass.NoUrutTrans
        params(5) = New SqlParameter("@BungaNett", SqlDbType.VarChar, 4)
        params(5).Value = ocustomClass.BungaNet
        params(6) = New SqlParameter("@isDistributable", SqlDbType.Bit)
        params(6).Value = ocustomClass.isDistribuTable
        params(7) = New SqlParameter("@DefaultValue", SqlDbType.Char, 1)
        params(7).Value = ocustomClass.defaultValueTagihanKeSupplier
        params(8) = New SqlParameter("@isReadOnly", SqlDbType.Bit)
        params(8).Value = ocustomClass.isReadOnly        

        params(9) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(9).Direction = ParameterDirection.Output

        params(10) = New SqlParameter("@GroupTransID", SqlDbType.Char, 3)
        params(10).Value = ocustomClass.GroupTransID
        params(11) = New SqlParameter("@SupplierEmployeePosition", SqlDbType.VarChar, 10)
        params(11).Value = ocustomClass.SupplierEmployeePosition
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(9).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.AgreementMasterTransaction.AgreementMasterTransactionSaveAdd")
        End Try
    End Function

    Public Sub AgreementMasterTransactionSaveEdit(ByVal ocustomClass As Parameter.AgreementMasterTransaction)
        Dim params(11) As SqlParameter
        params(0) = New SqlParameter("@TransID", SqlDbType.Char, 3)
        params(0).Value = ocustomClass.TransID
        params(1) = New SqlParameter("@TransDesc", SqlDbType.VarChar, 50)
        params(1).Value = ocustomClass.TransDesc
        params(2) = New SqlParameter("@IsActive", SqlDbType.Bit)
        params(2).Value = ocustomClass.Isactive
        params(3) = New SqlParameter("@PaymentAllocationIDSupplier", SqlDbType.Char, 10)
        params(3).Value = ocustomClass.PaymentAllocationIDSupplier        
        params(4) = New SqlParameter("@TransIDEdit", SqlDbType.Char, 3)
        params(4).Value = ocustomClass.TransIDEdit
        params(5) = New SqlParameter("@NoUrutTransaksi", SqlDbType.SmallInt)
        params(5).Value = ocustomClass.NoUrutTrans
        params(6) = New SqlParameter("@BungaNett", SqlDbType.VarChar, 4)
        params(6).Value = ocustomClass.BungaNet
        params(7) = New SqlParameter("@isDistributable", SqlDbType.Bit)
        params(7).Value = ocustomClass.isDistribuTable

        params(8) = New SqlParameter("@DefaultValue", SqlDbType.Char, 1)
        params(8).Value = ocustomClass.defaultValueTagihanKeSupplier

        params(9) = New SqlParameter("@isReadOnly", SqlDbType.Bit)
        params(9).Value = ocustomClass.isReadOnly

        params(10) = New SqlParameter("@GroupTransID", SqlDbType.Char, 3)
        params(10).Value = ocustomClass.GroupTransID
        params(11) = New SqlParameter("@SupplierEmployeePosition", SqlDbType.VarChar, 10)
        params(11).Value = ocustomClass.SupplierEmployeePosition
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.AgreementMasterTransaction.AgreementMasterTransactionSaveEdit")
        End Try
    End Sub

    Public Function AgreementMasterTransactionDelete(ByVal ocustomClass As Parameter.AgreementMasterTransaction) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@id", SqlDbType.Char, 3)
        params(0).Value = ocustomClass.TransID
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

End Class
