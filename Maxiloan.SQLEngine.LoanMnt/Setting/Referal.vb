﻿

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class Referal : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spPagingReferal"
    Private Const LIST_BY_ID As String = "spReferalEdit"
    Private Const LIST_ADD As String = "spReferalSaveAdd"
    Private Const LIST_UPDATE As String = "spReferalSaveEdit"
    Private Const LIST_DELETE As String = "spReferalDelete"
    Private Const LIST_REPORT As String = "spReferalReport"
    Private Const LIST_SECTOR As String = "spReferalSector"
#End Region

    Public Function GetReferal(ByVal oCustomClass As Parameter.Referal) As Parameter.Referal
        Dim oReturnValue As New Parameter.Referal
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Referal.GetReferal")
        End Try
    End Function

    Public Function GetReferalReport(ByVal oCustomClass As Parameter.Referal) As Parameter.Referal
        Dim oReturnValue As New Parameter.Referal
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CmdWhere", SqlDbType.VarChar, 1000)
        params(0).Value = oCustomClass.WhereCond
        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Referal.GetReferalReport")
        End Try
    End Function

    Public Function GetSector(ByVal oCustomClass As Parameter.Referal) As Parameter.Referal
        Dim oReturnValue As New Parameter.Referal
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SECTOR).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Referal.GetSector")
        End Try
    End Function

    Public Function GetReferalEdit(ByVal ocustomClass As Parameter.Referal) As Parameter.Referal
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.Referal
        params(0) = New SqlParameter("@NPP", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.NPP
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.Nama = reader("Nama").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Referal.GetReferalEdit")
        End Try
    End Function

    Public Function ReferalSaveAdd(ByVal ocustomClass As Parameter.Referal) As String
        Dim ErrMessage As String = ""
        Dim params(11) As SqlParameter
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = ocustomClass.BranchID
        params(1) = New SqlParameter("@NPP", SqlDbType.Char, 20)
        params(1).Value = ocustomClass.NPP
        params(2) = New SqlParameter("@UnitBisnisID", SqlDbType.SmallInt)
        params(2).Value = ocustomClass.UnitBisnisID
        params(3) = New SqlParameter("@Lokasi", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.Lokasi
        params(4) = New SqlParameter("@Nama", SqlDbType.VarChar, 50)
        params(4).Value = ocustomClass.Nama
        params(5) = New SqlParameter("@NPWP", SqlDbType.VarChar, 25)
        params(5).Value = ocustomClass.NPWP
        params(6) = New SqlParameter("@BankID", SqlDbType.VarChar, 5)
        params(6).Value = ocustomClass.BankID
        params(7) = New SqlParameter("@BankBranch", SqlDbType.VarChar, 30)
        params(7).Value = ocustomClass.BankBranch
        params(8) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 25)
        params(8).Value = ocustomClass.AccountNo
        params(9) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
        params(9).Value = ocustomClass.AccountName
        params(10) = New SqlParameter("@BankBranchId", SqlDbType.VarChar, 5)
        params(10).Value = ocustomClass.BankBranchId
        params(11) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(11).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(6).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Referal.ReferalSaveAdd")
        End Try
    End Function

    Public Sub ReferalSaveEdit(ByVal ocustomClass As Parameter.Referal)
        Dim params(10) As SqlParameter
        params(0) = New SqlParameter("@NPP", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.NPP
        params(1) = New SqlParameter("@UnitBisnisID", SqlDbType.SmallInt)
        params(1).Value = ocustomClass.UnitBisnisID
        params(2) = New SqlParameter("@Lokasi", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.Lokasi
        params(3) = New SqlParameter("@Nama", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.Nama
        params(4) = New SqlParameter("@NPWP", SqlDbType.VarChar, 25)
        params(4).Value = ocustomClass.NPWP
        params(5) = New SqlParameter("@BankID", SqlDbType.VarChar, 5)
        params(5).Value = ocustomClass.BankID
        params(6) = New SqlParameter("@BankBranch", SqlDbType.VarChar, 30)
        params(6).Value = ocustomClass.BankBranch
        params(7) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 25)
        params(7).Value = ocustomClass.AccountNo
        params(8) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
        params(8).Value = ocustomClass.AccountName
        params(9) = New SqlParameter("@BankBranchId", SqlDbType.VarChar, 5)
        params(9).Value = ocustomClass.BankBranchId
        params(10) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(10).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Referal.ReferalSaveEdit")
        End Try
    End Sub

    Public Function ReferalDelete(ByVal ocustomClass As Parameter.Referal) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@NPP", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.NPP
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            'Throw New Exception("Error On DataAccess.Setting.Referal.ReferalDelete")
        End Try
    End Function
End Class


