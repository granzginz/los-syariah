

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class BankMaster : Inherits AccMntBase
    Private Const spListBankMaster As String = "spBankMasterPaging"
    Private Const spReportBankMaster As String = "spBankMasterReport"
    Private Const spBankMasterAdd As String = "spBankMasterAdd"
    Private Const spBankMasterUpdate As String = "spBankMasterUpdate"
    Private Const spBankMasterDelete As String = "spBankMasterDelete"


    Private Const PARAM_STATUS As String = "@isactive"

    Public Function ListBankMaster(ByVal customclass As Parameter.BankMaster) As Parameter.BankMaster
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListBankMaster = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListBankMaster, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("BankMaster", "ListBankMaster", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AddBankMaster(ByVal customclass As Parameter.BankMaster) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(5) {}
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            params(0) = New SqlParameter(PARAM_BANKID, SqlDbType.VarChar, 5)
            params(0).Value = customclass.BankID

            params(1) = New SqlParameter(PARAM_BANKNAME, SqlDbType.VarChar, 50)
            params(1).Value = customclass.BankName

            params(2) = New SqlParameter(PARAM_STATUS, SqlDbType.Bit)
            params(2).Value = customclass.Status

            params(3) = New SqlParameter(PARAM_ShortName, SqlDbType.VarChar, 30)
            params(3).Value = customclass.ShortName

            params(4) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(4).Direction = ParameterDirection.Output

            params(5) = New SqlParameter(PARAM_SandiKliring, SqlDbType.VarChar, 7)
            params(5).Value = customclass.SandiKliring

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spBankMasterAdd, params)
            objtrans.Commit()
            Return CStr(params(4).Value)
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("BankMaster", "ListBankMaster", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Public Function UpdateBankMaster(ByVal customclass As Parameter.BankMaster) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(5) {}
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            params(0) = New SqlParameter(PARAM_BANKID, SqlDbType.VarChar, 5)
            params(0).Value = customclass.BankID

            params(1) = New SqlParameter(PARAM_BANKNAME, SqlDbType.VarChar, 50)
            params(1).Value = customclass.BankName

            params(2) = New SqlParameter(PARAM_STATUS, SqlDbType.Bit)
            params(2).Value = customclass.Status

            params(3) = New SqlParameter(PARAM_ShortName, SqlDbType.VarChar, 30)
            params(3).Value = customclass.ShortName

            params(4) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(4).Direction = ParameterDirection.Output

            params(5) = New SqlParameter(PARAM_SandiKliring, SqlDbType.VarChar, 7)
            params(5).Value = customclass.SandiKliring

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spBankMasterUpdate, params)

            objtrans.Commit()
            Return CStr(params(4).Value)
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("BankMaster", "UpdateBankMaster", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Function

    Public Function DeleteBankMaster(ByVal customclass As Parameter.BankMaster) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(1) {}
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            params(0) = New SqlParameter(PARAM_BANKID, SqlDbType.VarChar, 5)
            params(0).Value = customclass.BankID

            params(1) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(1).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spBankMasterDelete, params)
            objtrans.Commit()
            Return CStr(params(1).Value)
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("BankMaster", "DeleteBankMaster", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Public Function ReportBankMaster(ByVal customclass As Parameter.BankMaster) As Parameter.BankMaster
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spReportBankMaster, params)
            Return customclass
        Catch exp As Exception
            WriteException("BankMaster", "ReportBankMaster", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class

