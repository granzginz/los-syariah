
#Region "Imports"

Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

#End Region

Public Class PaymentAllocation : Inherits DataAccessBase
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spPaymentAllocationPaging"
    Private Const LIST_BY_ID As String = "spGetPaymentAllocationId"
    Private Const LIST_ADD As String = "spPaymentAllocationAdd"
    Private Const LIST_UPDATE As String = "spPaymentAllocationEdit"
    Private Const LIST_DELETE As String = "spPaymentAllocationDelete"
    Private Const LIST_REPORT As String = "spPaymentAllocationReport"


    Public Function GetPaymentAllocation(ByVal oCustomClass As Parameter.PaymentAllocation) As Parameter.PaymentAllocation

        Dim params(4) As SqlParameter
        With oCustomClass
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
            params(3).Value = .SortBy
            params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output
        End With
        Try
            Dim oReturnValue As New Parameter.PaymentAllocation
            oReturnValue.ListPaymentAllocation = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int32)
            Return oReturnValue
        Catch exp As Exception
            WriteException("PaymentAllocation", "GetPaymentAllocation", exp.Message + exp.StackTrace)
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
        End Try
    End Function

    Public Function GetPaymentAllocationId(ByVal strConnection As String, ByVal id As String) As Parameter.PaymentAllocation

        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@PaymentAllocationId", SqlDbType.Char, 10)
        params(0).Value = id

        Dim reader As SqlDataReader
        Try
            Dim oReturnValue As New Parameter.PaymentAllocation

            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.PaymentAllocationId = reader("PaymentAllocationId").ToString
                oReturnValue.Description = reader("Description").ToString
                oReturnValue.COA = reader("COA").ToString
                oReturnValue.IsSystem = CType(reader("IsSystem"), Boolean)
                oReturnValue.IsScheme = CType(reader("isScheme"), Boolean)
                oReturnValue.IsAgreement = CType(reader("isAgreement"), Boolean)
                oReturnValue.IsPaymentReceive = CType(reader("IsPaymentReceive"), Boolean)
                oReturnValue.IsPettyCash = CType(reader("isPettyCash"), Boolean)
                oReturnValue.IsHeadOffice = CType(reader("isHOTransaction"), Boolean)
                oReturnValue.Status = CType(reader("isActive"), Boolean)
            End If
            Return oReturnValue
        Catch exp As Exception
            WriteException("PaymentAllocation", "GetPaymentAllocationId", exp.Message + exp.StackTrace)
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
        End Try

    End Function

    Public Sub PaymentAllocationAdd(ByVal customClass As Parameter.PaymentAllocation)
        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            Dim params(10) As SqlParameter
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            params(0) = New SqlParameter("@PaymentAllocationId", SqlDbType.Char, 10)
            params(0).Value = UCase(customClass.PaymentAllocationId)
            params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
            params(1).Value = customClass.Description
            params(2) = New SqlParameter("@COA", SqlDbType.VarChar, 25)
            params(2).Value = customClass.COA
            params(3) = New SqlParameter("@IsSystem", SqlDbType.Bit)
            params(3).Value = customClass.IsSystem
            params(4) = New SqlParameter("@IsScheme", SqlDbType.Bit)
            params(4).Value = customClass.IsScheme
            params(5) = New SqlParameter("@IsAgreement", SqlDbType.Bit)
            params(5).Value = customClass.IsAgreement
            params(6) = New SqlParameter("@IsPettyCash", SqlDbType.Bit)
            params(6).Value = customClass.IsPettyCash
            params(7) = New SqlParameter("@IsHoTransaction", SqlDbType.Bit)
            params(7).Value = customClass.IsHeadOffice
            params(8) = New SqlParameter("@IsActive", SqlDbType.Bit)
            params(8).Value = customClass.Status
            params(9) = New SqlParameter("@IsPaymentReceive", SqlDbType.Bit)
            params(9).Value = customClass.IsPaymentReceive
            params(10) = New SqlParameter("@isPaymentRequest", SqlDbType.Bit)
            params(10).Value = customClass.isPaymentRequest

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, LIST_ADD, params)
            objtrans.Commit()
        Catch exp As SqlException
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            objtrans.Rollback()
            If exp.Number = 2627 Then
                Throw New Exception("Info Record Already Exists")
            Else
                Throw New Exception("Info Failed On Add Record")
            End If
            WriteException("PaymentAllocation", "PaymentAllocationAdd", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Public Sub PaymentAllocationEdt(ByVal customClass As Parameter.PaymentAllocation)
        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            Dim params(10) As SqlParameter
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            params(0) = New SqlParameter("@PaymentAllocationId", SqlDbType.Char, 10)
            params(0).Value = customClass.PaymentAllocationId
            params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
            params(1).Value = customClass.Description
            params(2) = New SqlParameter("@COA", SqlDbType.VarChar, 25)
            params(2).Value = customClass.COA
            params(3) = New SqlParameter("@IsSystem", SqlDbType.Bit)
            params(3).Value = customClass.IsSystem
            params(4) = New SqlParameter("@IsScheme", SqlDbType.Bit)
            params(4).Value = customClass.IsScheme
            params(5) = New SqlParameter("@IsAgreement", SqlDbType.Bit)
            params(5).Value = customClass.IsAgreement
            params(6) = New SqlParameter("@IsPettyCash", SqlDbType.Bit)
            params(6).Value = customClass.IsPettyCash
            params(7) = New SqlParameter("@IsHoTransaction", SqlDbType.Bit)
            params(7).Value = customClass.IsHeadOffice
            params(8) = New SqlParameter("@IsActive", SqlDbType.Bit)
            params(8).Value = customClass.Status
            params(9) = New SqlParameter("@IsPaymentReceive", SqlDbType.Bit)
            params(9).Value = customClass.IsPaymentReceive
            params(10) = New SqlParameter("@isPaymentRequest", SqlDbType.Bit)
            params(10).Value = customClass.isPaymentRequest
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, LIST_UPDATE, params)
            objtrans.Commit()
        Catch exp As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            objtrans.Rollback()
            Throw New Exception("Failed On Update Record")
            WriteException("PaymentAllocation", "PaymentAllocationEdt", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Public Sub PaymentAllocationDelete(ByVal customclass As Parameter.PaymentAllocation)
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            Dim params(0) As SqlParameter

            params(0) = New SqlParameter("@PaymentAllocationId", SqlDbType.Char, 10)
            params(0).Value = customclass.PaymentAllocationId

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, LIST_DELETE, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("PaymentAllocation", "PaymentAllocationDelete", exp.Message + exp.StackTrace)
            Throw New Exception("Record Already Used By Another Table")
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub
    Public Function GetPaymentAllocationReport(ByVal oCustomClass As Parameter.PaymentAllocation) As Parameter.PaymentAllocation

        Dim params(1) As SqlParameter

        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 100)
        params(0).Value = oCustomClass.WhereCond

        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.SortBy

        Try
            oCustomClass.ListReportPaymentAllocation = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return (oCustomClass)
        Catch exp As Exception
            WriteException("PaymentAllocation", "GetPaymentAllocationReport", exp.Message + exp.StackTrace)
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
        End Try
    End Function
End Class
