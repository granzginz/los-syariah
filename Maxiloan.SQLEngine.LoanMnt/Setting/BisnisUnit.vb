﻿

#Region "Imports"
Imports System
    Imports System.Collections
    Imports System.ComponentModel
    Imports System.Data
    Imports System.Text
    Imports System.Data.SqlClient
    Imports Maxiloan.Framework.SQLEngine
    Imports Maxiloan.Exceptions
#End Region

Public Class BisnisUnit : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spPagingBisnisUnit"
    Private Const LIST_BY_ID As String = "spUnitBisnisEdit"
    Private Const LIST_ADD As String = "spBisnisUnitSaveAdd"
    Private Const LIST_UPDATE As String = "spBisnisUnitSaveEdit"
    Private Const LIST_DELETE As String = "spUnitBisnisDelete"
    Private Const LIST_REPORT As String = "spBisnisUnitReport"
    Private Const LIST_SECTOR As String = "spBisnisUnitSector"
#End Region

    Public Function GetBisnisUnit(ByVal oCustomClass As Parameter.BisnisUnit) As Parameter.BisnisUnit
        Dim oReturnValue As New Parameter.BisnisUnit
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.BisnisUnit.GetBisnisUnit")
        End Try
    End Function

    Public Function GetBisnisUnitReport(ByVal oCustomClass As Parameter.BisnisUnit) As Parameter.BisnisUnit
        Dim oReturnValue As New Parameter.BisnisUnit
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CmdWhere", SqlDbType.VarChar, 1000)
        params(0).Value = oCustomClass.WhereCond
        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.BisnisUnit.GetBisnisUnitReport")
        End Try
    End Function

    Public Function GetSector(ByVal oCustomClass As Parameter.BisnisUnit) As Parameter.BisnisUnit
        Dim oReturnValue As New Parameter.BisnisUnit
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SECTOR).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.BisnisUnit.GetSector")
        End Try
    End Function

    Public Function GetBisnisUnitEdit(ByVal ocustomClass As Parameter.BisnisUnit) As Parameter.BisnisUnit
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.BisnisUnit
        params(0) = New SqlParameter("@UnitBisnisID", SqlDbType.SmallInt)
        params(0).Value = ocustomClass.UnitBisnisID
        params(1) = New SqlParameter("@Nama", SqlDbType.VarChar, 50)
        params(1).Value = ocustomClass.Nama
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.Nama = reader("Nama").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.BisnisUnit.GetBisnisUnitEdit")
        End Try
    End Function

    Public Function BisnisUnitSaveAdd(ByVal ocustomClass As Parameter.BisnisUnit) As String
        Dim ErrMessage As String = ""
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@UnitBisnisID", SqlDbType.SmallInt)
        params(0).Value = ocustomClass.UnitBisnisID
        params(1) = New SqlParameter("@Nama", SqlDbType.VarChar, 50)
        params(1).Value = ocustomClass.Nama
        params(2) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(2).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(2).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.BisnisUnit.BisnisUnitSaveAdd")
        End Try
    End Function

    Public Sub BisnisUnitSaveEdit(ByVal ocustomClass As Parameter.BisnisUnit)
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@UnitBisnisID", SqlDbType.SmallInt)
        params(0).Value = ocustomClass.UnitBisnisID
        params(1) = New SqlParameter("@Nama", SqlDbType.VarChar, 50)
        params(1).Value = ocustomClass.Nama

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)

        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.BisnisUnit.BisnisUnitSaveEdit")
        End Try
    End Sub

    Public Function BisnisUnitDelete(ByVal ocustomClass As Parameter.BisnisUnit) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@UnitBisnisID", SqlDbType.SmallInt)
        params(0).Value = ocustomClass.UnitBisnisID
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            'Throw New Exception("Error On DataAccess.Setting.BisnisUnit.BisnisUnitDelete")
        End Try
    End Function
End Class


