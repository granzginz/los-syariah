﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class BankBranchMaster : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name        
    Private Const LIST_SELECT As String = "spBankBranchMasterPaging"
    Private Const LIST_BY_ID As String = "spBankBranchMasterSaveView"
    Private Const LIST_ADD As String = "spBankBranchMasterSaveAdd"
    Private Const LIST_UPDATE As String = "spBankBranchMasterSaveEdit"
    Private Const LIST_DELETE As String = "spBankBranchMasterDelete"
    Private Const LIST_REPORT As String = "spBankBranchMasterReport"
#End Region

    Public Function GetBankBranchMaster(ByVal oCustomClass As Parameter.BankBranchMaster) As Parameter.BankBranchMaster
        Dim oReturnValue As New Parameter.BankBranchMaster
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.BankBranchMaster.GetBankBranchMaster")
        End Try
    End Function

    Public Function GetBankBranchMasterReport(ByVal oCustomClass As Parameter.BankBranchMaster) As Parameter.BankBranchMaster
        Dim oReturnValue As New Parameter.BankBranchMaster
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CmdWhere", SqlDbType.VarChar, 1000)
        params(0).Value = oCustomClass.WhereCond
        Try
            oReturnValue.ListdataReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.BankBranchMaster.GetBankBranchMasterReport")
        End Try
    End Function

    Public Function GetBankBranchMasterList(ByVal ocustomClass As Parameter.BankBranchMaster) As Parameter.BankBranchMaster
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.BankBranchMaster
        params(0) = New SqlParameter("@BankBranchid", SqlDbType.Int, 4)
        params(0).Value = ocustomClass.BankBranchid
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.BankBranchid = ocustomClass.BankBranchid
                oReturnValue.BankCode = reader("BankCode").ToString
                oReturnValue.BankBranchName = reader("BankBranchName").ToString
                oReturnValue.BankID = reader("BankID").ToString
                oReturnValue.IsActive = CBool(reader("IsActive").ToString)
                oReturnValue.City = reader("City").ToString
                oReturnValue.Address = reader("Address").ToString
                oReturnValue.BankName = reader("BankName").ToString
                oReturnValue.ShortName = reader("ShortName").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.BankBranchMaster.GetBankBranchMasterList")
        End Try
    End Function

    Public Function BankBranchMasterSaveAdd(ByVal ocustomClass As Parameter.BankBranchMaster) As String
        Dim ErrMessage As String = ""
        Dim params(8) As SqlParameter
        params(0) = New SqlParameter("@BankCode", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.BankCode
        params(1) = New SqlParameter("@BankBranchName", SqlDbType.VarChar, 100)
        params(1).Value = ocustomClass.BankBranchName
        params(2) = New SqlParameter("@BankID", SqlDbType.Char, 5)
        params(2).Value = ocustomClass.BankID
        params(3) = New SqlParameter("@IsActive", SqlDbType.Bit)
        params(3).Value = ocustomClass.IsActive
        params(4) = New SqlParameter("@City", SqlDbType.VarChar, 50)
        params(4).Value = ocustomClass.City
        params(5) = New SqlParameter("@LoginId", SqlDbType.VarChar, 20)
        params(5).Value = ocustomClass.LoginId
        params(6) = New SqlParameter("@businessdate", SqlDbType.DateTime)
        params(6).Value = ocustomClass.BusinessDate
        params(7) = New SqlParameter("@Address", SqlDbType.VarChar, 255)
        params(7).Value = ocustomClass.Address
        params(8) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(8).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(8).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.BankBranchMaster.BankBranchMasterSaveAdd")
        End Try
    End Function

    Public Sub BankBranchMasterSaveEdit(ByVal ocustomClass As Parameter.BankBranchMaster)
        Dim params(8) As SqlParameter
        params(0) = New SqlParameter("@BankBranchid", SqlDbType.Int, 4)
        params(0).Value = ocustomClass.BankBranchid
        params(1) = New SqlParameter("@BankCode", SqlDbType.Char, 20)
        params(1).Value = ocustomClass.BankCode
        params(2) = New SqlParameter("@BankBranchName", SqlDbType.VarChar, 100)
        params(2).Value = ocustomClass.BankBranchName
        params(3) = New SqlParameter("@BankID", SqlDbType.Char, 5)
        params(3).Value = ocustomClass.BankID
        params(4) = New SqlParameter("@IsActive", SqlDbType.Bit)
        params(4).Value = ocustomClass.IsActive
        params(5) = New SqlParameter("@City", SqlDbType.VarChar, 50)
        params(5).Value = ocustomClass.City
        params(6) = New SqlParameter("@LoginId", SqlDbType.VarChar, 20)
        params(6).Value = ocustomClass.LoginId
        params(7) = New SqlParameter("@businessdate", SqlDbType.DateTime)
        params(7).Value = ocustomClass.BusinessDate
        params(8) = New SqlParameter("@Address", SqlDbType.VarChar, 255)
        params(8).Value = ocustomClass.Address
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.BankBranchMaster.BankBranchMasterSaveEdit")
        End Try
    End Sub

    Public Function BankBranchMasterDelete(ByVal ocustomClass As Parameter.BankBranchMaster) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@BankBranchid", SqlDbType.Int, 4)
        params(0).Value = ocustomClass.BankBranchid
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"            
        End Try
    End Function

End Class
