﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class D02 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSLIKD02List"
    Private Const SAVE_EDIT As String = "spSLIKD02SaveEdit"
    Private Const LIST_EDIT As String = "spSLIKD02EditList"
    Private Const SAVE_ADD As String = "spSLIKD02SaveAdd"
    Private Const DELETE As String = "spSLIKD02Delete"
    Private Const FILL_CBO As String = "spGetCboKodeBadanUsaha"
    Private Const FILL_CBO1 As String = "spGetCboKodeHubunganpelapor"
    Private Const FILL_CBO2 As String = "spGetCboLembagaPemeringkat"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
#End Region

    Public Function GetD02(ByVal oCustomClass As Parameter.D02) As Parameter.D02
        Dim oReturnValue As New Parameter.D02
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.D02.GetD02")
        End Try
    End Function

    Public Function GetSelectD02(oCustomClass As Parameter.D02) As Parameter.D02
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@CIF", SqlDbType.VarChar, 100)
            params(0).Value = oCustomClass.CIF
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetD02Edit(ByVal oCustomClass As Parameter.D02) As Parameter.D02
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.D02

        params(0) = New SqlParameter("ID", SqlDbType.BigInt)
        params(0).Value = oCustomClass.ID

        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.D02.GetD02Edit")
        End Try
    End Function

    Public Function GetD02Add(ByVal oCustomClass As Parameter.D02) As Parameter.D02
        Dim params(30) As SqlParameter
        Dim oReturnValue As New Parameter.D02

        params(0) = New SqlParameter("Bulandata", SqlDbType.VarChar, 100)
        params(0).Value = oCustomClass.BulanData
        params(1) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
        params(1).Value = oCustomClass.CIF
        params(2) = New SqlParameter("NoIdentitasBadanUsaha", SqlDbType.VarChar, 100)
        params(2).Value = oCustomClass.NoIdentitasBadanUsaha
        params(3) = New SqlParameter("NamaBadanUsaha", SqlDbType.VarChar, 100)
        params(3).Value = oCustomClass.NamaBadanUsaha
        params(4) = New SqlParameter("KodeJenisBadanUsaha", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.KodeJenisBadanUsaha
        params(5) = New SqlParameter("TempatPendirian", SqlDbType.VarChar, 100)
        params(5).Value = oCustomClass.TempatPendirian
        params(6) = New SqlParameter("NoAktePendirian", SqlDbType.VarChar, 100)
        params(6).Value = oCustomClass.NoAktePendirian
        params(7) = New SqlParameter("TanggalAktePendirian", SqlDbType.VarChar, 100)
        params(7).Value = oCustomClass.TanggalAktePendirian
        params(8) = New SqlParameter("NoAktePerubahanTerakhir", SqlDbType.VarChar, 100)
        params(8).Value = oCustomClass.NoAktePerubahanTerakhir
        params(9) = New SqlParameter("TanggalAktePerubahanTerakhir", SqlDbType.VarChar, 100)
        params(9).Value = oCustomClass.TanggalAktePerubahanTerakhir
        params(10) = New SqlParameter("Telepon", SqlDbType.VarChar, 100)
        params(10).Value = oCustomClass.Telepon
        params(11) = New SqlParameter("Hp", SqlDbType.VarChar, 100)
        params(11).Value = oCustomClass.Hp
        params(12) = New SqlParameter("AlamatEmail", SqlDbType.VarChar, 100)
        params(12).Value = oCustomClass.AlamatEmail
        params(13) = New SqlParameter("Alamat", SqlDbType.VarChar, 250)
        params(13).Value = oCustomClass.Alamat
        params(14) = New SqlParameter("Kelurahan", SqlDbType.VarChar, 100)
        params(14).Value = oCustomClass.Kelurahan
        params(15) = New SqlParameter("Kecamatan", SqlDbType.VarChar, 100)
        params(15).Value = oCustomClass.Kecamatan
        params(16) = New SqlParameter("City", SqlDbType.VarChar, 100)
        params(16).Value = oCustomClass.KodeSandiKab
        params(17) = New SqlParameter("ZipCode", SqlDbType.VarChar, 100)
        params(17).Value = oCustomClass.KodePos
        params(18) = New SqlParameter("KodeNegaraDomisili", SqlDbType.VarChar, 100)
        params(18).Value = oCustomClass.KodeNegaraDomisili
        params(19) = New SqlParameter("KodeBidangUsaha", SqlDbType.VarChar, 100)
        params(19).Value = oCustomClass.KodeBidangUsaha
        params(20) = New SqlParameter("KodeHubungandenganLJK", SqlDbType.VarChar, 100)
        params(20).Value = oCustomClass.KodeHubungandenganLJK
        params(21) = New SqlParameter("MelanggarBMPK", SqlDbType.VarChar, 100)
        params(21).Value = oCustomClass.MelanggarBMPK
        params(22) = New SqlParameter("MelampauiBMPK", SqlDbType.VarChar, 100)
        params(22).Value = oCustomClass.MelampauiBMPK
        params(23) = New SqlParameter("GoPublic", SqlDbType.VarChar, 100)
        params(23).Value = oCustomClass.GoPublic
        params(24) = New SqlParameter("KodeGolonganDebitur", SqlDbType.VarChar, 100)
        params(24).Value = oCustomClass.KodeGolonganDebitur
        params(25) = New SqlParameter("Peringkat", SqlDbType.VarChar, 100)
        params(25).Value = oCustomClass.Peringkat
        params(26) = New SqlParameter("LembagaPemeringkat", SqlDbType.VarChar, 100)
        params(26).Value = oCustomClass.LembagaPemeringkat
        params(27) = New SqlParameter("TanggalPemeringkatan", SqlDbType.VarChar, 100)
        params(27).Value = oCustomClass.TanggalPemeringkatan
        params(28) = New SqlParameter("NamaGroupDebitur", SqlDbType.VarChar, 100)
        params(28).Value = oCustomClass.NamaGroupDebitur
        params(29) = New SqlParameter("KodeKantorCabang", SqlDbType.VarChar, 100)
        params(29).Value = oCustomClass.KodeKantorCabang
        params(30) = New SqlParameter("OperasiData", SqlDbType.VarChar, 100)
        params(30).Value = oCustomClass.OperasiData
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE_ADD, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.D02.GetD02Add")
        End Try
    End Function

    Public Function GetD02Save(ByVal oCustomClass As Parameter.D02) As Parameter.D02
        Dim params(31) As SqlParameter
        Dim oReturnValue As New Parameter.D02

        params(31) = New SqlParameter("ID", SqlDbType.BigInt)
        params(31).Value = oCustomClass.ID
        params(30) = New SqlParameter("Bulandata", SqlDbType.VarChar, 100)
        params(30).Value = oCustomClass.BulanData
        params(30) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
        params(30).Value = oCustomClass.CIF
        params(29) = New SqlParameter("NoIdentitasBadanUsaha", SqlDbType.VarChar, 100)
        params(29).Value = oCustomClass.NoIdentitasBadanUsaha
        params(28) = New SqlParameter("NamaBadanUsaha", SqlDbType.VarChar, 100)
        params(28).Value = oCustomClass.NamaBadanUsaha
        params(27) = New SqlParameter("KodeJenisBadanUsaha", SqlDbType.VarChar, 100)
        params(27).Value = oCustomClass.KodeJenisBadanUsaha
        params(26) = New SqlParameter("TempatPendirian", SqlDbType.VarChar, 100)
        params(26).Value = oCustomClass.TempatPendirian
        params(25) = New SqlParameter("NoAktePendirian", SqlDbType.VarChar, 100)
        params(25).Value = oCustomClass.NoAktePendirian
        params(24) = New SqlParameter("TanggalAktePendirian", SqlDbType.VarChar, 100)
        params(24).Value = oCustomClass.TanggalAktePendirian
        params(23) = New SqlParameter("NoAktePerubahanTerakhir", SqlDbType.VarChar, 100)
        params(23).Value = oCustomClass.NoAktePerubahanTerakhir
        params(22) = New SqlParameter("TanggalAktePerubahanTerakhir", SqlDbType.VarChar, 100)
        params(22).Value = oCustomClass.TanggalAktePerubahanTerakhir
        params(21) = New SqlParameter("Telepon", SqlDbType.VarChar, 100)
        params(21).Value = oCustomClass.Telepon
        params(20) = New SqlParameter("Hp", SqlDbType.VarChar, 100)
        params(20).Value = oCustomClass.Hp
        params(19) = New SqlParameter("AlamatEmail", SqlDbType.VarChar, 100)
        params(19).Value = oCustomClass.AlamatEmail
        params(18) = New SqlParameter("Alamat", SqlDbType.VarChar, 250)
        params(18).Value = oCustomClass.Alamat
        params(17) = New SqlParameter("Kelurahan", SqlDbType.VarChar, 100)
        params(17).Value = oCustomClass.Kelurahan
        params(16) = New SqlParameter("Kecamatan", SqlDbType.VarChar, 100)
        params(16).Value = oCustomClass.Kecamatan
        params(15) = New SqlParameter("City", SqlDbType.VarChar, 100)
        params(15).Value = oCustomClass.KodeSandiKab
        params(14) = New SqlParameter("ZipCode", SqlDbType.VarChar, 100)
        params(14).Value = oCustomClass.KodePos
        params(13) = New SqlParameter("KodeNegaraDomisili", SqlDbType.VarChar, 100)
        params(13).Value = oCustomClass.KodeNegaraDomisili
        params(12) = New SqlParameter("KodeBidangUsaha", SqlDbType.VarChar, 100)
        params(12).Value = oCustomClass.KodeBidangUsaha
        params(11) = New SqlParameter("KodeHubungandenganLJK", SqlDbType.VarChar, 100)
        params(11).Value = oCustomClass.KodeHubungandenganLJK
        params(10) = New SqlParameter("MelanggarBMPK", SqlDbType.VarChar, 100)
        params(10).Value = oCustomClass.MelanggarBMPK
        params(9) = New SqlParameter("MelampauiBMPK", SqlDbType.VarChar, 100)
        params(9).Value = oCustomClass.MelampauiBMPK
        params(8) = New SqlParameter("GoPublic", SqlDbType.VarChar, 100)
        params(8).Value = oCustomClass.GoPublic
        params(7) = New SqlParameter("KodeGolonganDebitur", SqlDbType.VarChar, 100)
        params(7).Value = oCustomClass.KodeGolonganDebitur
        params(6) = New SqlParameter("Peringkat", SqlDbType.VarChar, 100)
        params(6).Value = oCustomClass.Peringkat
        params(5) = New SqlParameter("LembagaPemeringkat", SqlDbType.VarChar, 100)
        params(5).Value = oCustomClass.LembagaPemeringkat
        params(4) = New SqlParameter("TanggalPemeringkatan", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.TanggalPemeringkatan
        params(3) = New SqlParameter("NamaGroupDebitur", SqlDbType.VarChar, 100)
        params(3).Value = oCustomClass.NamaGroupDebitur
        params(2) = New SqlParameter("KodeKantorCabang", SqlDbType.VarChar, 100)
        params(2).Value = oCustomClass.KodeKantorCabang
        params(1) = New SqlParameter("OperasiData", SqlDbType.VarChar, 100)
        params(1).Value = oCustomClass.OperasiData

        params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE_EDIT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.D02.GetD02Save")
        End Try
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.D02) As Parameter.D02
        Dim oReturnValue As New Parameter.D02
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.D02.GetCbo")
        End Try
    End Function

    Public Function GetCbo1(ByVal oCustomClass As Parameter.D02) As Parameter.D02
        Dim oReturnValue As New Parameter.D02
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO1, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.D02.GetCbo1")
        End Try
    End Function

    Public Function GetCbo2(ByVal oCustomClass As Parameter.D02) As Parameter.D02
        Dim oReturnValue As New Parameter.D02
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO2, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.D02.GetCbo2")
        End Try
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.D02) As Parameter.D02
        Dim oReturnValue As New Parameter.D02
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.D02.GetCboBulandataSIPP")
        End Try
    End Function

    Public Function GetD02Delete(ByVal oCustomClass As Parameter.D02) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function
End Class
