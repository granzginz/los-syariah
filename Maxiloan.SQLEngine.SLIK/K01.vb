﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class K01 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
	'Stored Procedure name    
	Private Const LIST_SELECT As String = "spSLIKK01List"
	Private Const SAVE_EDIT As String = "spSLIKK01SaveEdit"
	Private Const LIST_EDIT As String = "spSLIKK01EditList"
	Private Const SAVE_ADD As String = "spSLIKK01SaveAdd"
	Private Const DELETE As String = "spSLIKK01Delete"
	Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
#End Region

	Public Function GetK01(ByVal oCustomClass As Parameter.K01) As Parameter.K01
		Dim oReturnValue As New Parameter.K01
		Dim params(4) As SqlParameter
		params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
		params(0).Value = oCustomClass.CurrentPage
		params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
		params(1).Value = oCustomClass.PageSize
		params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
		params(2).Value = oCustomClass.WhereCond
		params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
		params(3).Value = oCustomClass.SortBy
		params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
		params(4).Direction = ParameterDirection.Output
		Try
			oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
			oReturnValue.TotalRecord = CType(params(4).Value, Int64)
			Return oReturnValue
		Catch ex As Exception
			Throw New Exception("Error On DataAccess.SLIK.K01.GetK01")
		End Try
	End Function

	Public Function GetSelectK01(ocustomClass As Parameter.K01) As Parameter.K01
		Dim params(1) As SqlParameter
		Try
			params(0) = New SqlParameter("@CustomerId", SqlDbType.VarChar, 100)
			params(0).Value = ocustomClass.CustomerId
			params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
			params(1).Value = ""

			ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
			Return ocustomClass
		Catch ex As Exception

		End Try
		'Throw New NotImplementedException()
	End Function

	Public Function GetK01Edit(ByVal ocustomClass As Parameter.K01) As Parameter.K01
		Dim params(0) As SqlParameter
		Dim oReturnValue As New Parameter.K01


		params(0) = New SqlParameter("ID", SqlDbType.BigInt)
		params(0).Value = ocustomClass.ID

		Try
			ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
			Return ocustomClass
		Catch ex As Exception
			Throw New Exception("Error On DataAccess.SLIK.K01.GetK01Edit")
		End Try
	End Function

	Public Function GetK01Save(ByVal ocustomClass As Parameter.K01) As Parameter.K01
		Dim params(33) As SqlParameter
		Dim oReturnValue As New Parameter.K01

		params(33) = New SqlParameter("@ID", SqlDbType.VarChar, 50)
		params(33).Value = ocustomClass.ID
		params(32) = New SqlParameter("@FlagDetail", SqlDbType.VarChar, 1)
		params(32).Value = ocustomClass.FlagDetail
		params(31) = New SqlParameter("@CustomerId", SqlDbType.VarChar, 20)
		params(31).Value = ocustomClass.CustomerId
		params(30) = New SqlParameter("@TglLaporanKeuangan", SqlDbType.VarChar, 10)
		params(30).Value = ocustomClass.TglLaporanKeuangan
		params(29) = New SqlParameter("@AssetIDR", SqlDbType.VarChar, 50)
		params(29).Value = ocustomClass.AssetIDR
		params(28) = New SqlParameter("@AssetLancar", SqlDbType.VarChar, 50)
		params(28).Value = ocustomClass.AssetLancar
		params(27) = New SqlParameter("@KasSetaraKasAsetLancar", SqlDbType.VarChar, 50)
		params(27).Value = ocustomClass.KasSetaraKasAsetLancar
		params(26) = New SqlParameter("@PiutangUsahAsetLancar", SqlDbType.VarChar, 50)
		params(26).Value = ocustomClass.PiutangUsahAsetLancar
		params(25) = New SqlParameter("@InvestasiLainnyaAsetLancar", SqlDbType.VarChar, 50)
		params(25).Value = ocustomClass.InvestasiLainnyaAsetLancar
		params(24) = New SqlParameter("@AsetLancarLainnya", SqlDbType.VarChar, 50)
		params(24).Value = ocustomClass.AsetLancarLainnya
		params(23) = New SqlParameter("@AsetTidakLancar", SqlDbType.VarChar, 50)
		params(23).Value = ocustomClass.AsetTidakLancar
		params(22) = New SqlParameter("@PiutangUsahaAsetTidakLancar", SqlDbType.VarChar, 50)
		params(22).Value = ocustomClass.PiutangUsahaAsetTidakLancar
		params(21) = New SqlParameter("@InvestasiLainnyaAsetTidakLancar", SqlDbType.VarChar, 50)
		params(21).Value = ocustomClass.InvestasiLainnyaAsetTidakLancar
		params(20) = New SqlParameter("@AsetTidakLancarLainnya", SqlDbType.VarChar, 50)
		params(20).Value = ocustomClass.AsetTidakLancarLainnya
		params(19) = New SqlParameter("@Liabilitas", SqlDbType.VarChar, 50)
		params(19).Value = ocustomClass.Liabilitas
		params(18) = New SqlParameter("@LiabilitasJangkaPendek", SqlDbType.VarChar, 50)
		params(18).Value = ocustomClass.LiabilitasJangkaPendek
		params(17) = New SqlParameter("@PinjamanJangkaPendek", SqlDbType.VarChar, 50)
		params(17).Value = ocustomClass.PinjamanJangkaPendek
		params(16) = New SqlParameter("@UtangUsahaJangkaPendek", SqlDbType.VarChar, 50)
		params(16).Value = ocustomClass.UtangUsahaJangkaPendek
		params(15) = New SqlParameter("@LiabilitasJangkaPendekLainnya", SqlDbType.VarChar, 50)
		params(15).Value = ocustomClass.LiabilitasJangkaPendekLainnya
		params(14) = New SqlParameter("@LiabilitasJangkaPanjang", SqlDbType.VarChar, 50)
		params(14).Value = ocustomClass.LiabilitasJangkaPanjang
		params(13) = New SqlParameter("@PinjamanJangkaPanjang", SqlDbType.VarChar, 50)
		params(13).Value = ocustomClass.PinjamanJangkaPanjang
		params(12) = New SqlParameter("@UtangUsahaJangkaPanjang", SqlDbType.VarChar, 50)
		params(12).Value = ocustomClass.UtangUsahaJangkaPanjang
		params(11) = New SqlParameter("@LiabilitasJangkaPanjangLainnya", SqlDbType.VarChar, 50)
		params(11).Value = ocustomClass.LiabilitasJangkaPanjangLainnya
		params(10) = New SqlParameter("@Ekuitas", SqlDbType.VarChar, 50)
		params(10).Value = ocustomClass.Ekuitas
		params(9) = New SqlParameter("@PendapatanUsahaOpr", SqlDbType.VarChar, 50)
		params(9).Value = ocustomClass.PendapatanUsahaOpr
		params(8) = New SqlParameter("@BebanPokokPendapatanOpr", SqlDbType.VarChar, 50)
		params(8).Value = ocustomClass.BebanPokokPendapatanOpr
		params(7) = New SqlParameter("@LabaRugiBruto", SqlDbType.VarChar, 50)
		params(7).Value = ocustomClass.LabaRugiBruto
		params(6) = New SqlParameter("@PLLNonOpr", SqlDbType.VarChar, 50)
		params(6).Value = ocustomClass.PLLNonOpr
		params(5) = New SqlParameter("@BebanLainLainNonOpr", SqlDbType.VarChar, 50)
		params(5).Value = ocustomClass.BebanLainLainNonOpr
		params(4) = New SqlParameter("@LabaRugiSebelumPajak", SqlDbType.VarChar, 50)
		params(4).Value = ocustomClass.LabaRugiSebelumPajak
		params(3) = New SqlParameter("@LabaRugiTahunBerjalan", SqlDbType.VarChar, 50)
		params(3).Value = ocustomClass.LabaRugiTahunBerjalan
		params(2) = New SqlParameter("@KodeKantorCabang", SqlDbType.VarChar, 50)
		params(2).Value = ocustomClass.KodeKantorCabang
		params(1) = New SqlParameter("@OperasiData", SqlDbType.VarChar, 50)
		params(1).Value = ocustomClass.OperasiData
		params(0) = New SqlParameter("@BulanData", SqlDbType.VarChar, 8)
		params(0).Value = ocustomClass.BulanData


		Try
			SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SAVE_EDIT, params)
			Return oReturnValue
		Catch ex As Exception
			Throw New Exception("Error On DataAccess.SLIK.K01.GetK01Save")
		End Try
	End Function

	Public Function GetCbo(ByVal oCustomClass As Parameter.K01) As Parameter.K01
		Dim oReturnValue As New Parameter.K01
		Dim params() As SqlParameter = New SqlParameter(0) {}
		params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
		params(0).Value = oCustomClass.Table
		Try
			oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetCboLembagaPemeringkat", params).Tables(0)
			Return oReturnValue
		Catch exp As Exception
			Throw New Exception("Error On DataAccess.SLIK.K01.GetCbo")
		End Try
	End Function

	Public Function GetK01Add(ByVal oCustomClass As Parameter.K01) As Parameter.K01
		Dim params(32) As SqlParameter
		Dim oReturnValue As New Parameter.K01

		params(0) = New SqlParameter("@FlagDetail", SqlDbType.VarChar, 1)
		params(0).Value = oCustomClass.FlagDetail
		params(1) = New SqlParameter("@CustomerId", SqlDbType.VarChar, 20)
		params(1).Value = oCustomClass.CustomerId
		params(2) = New SqlParameter("@TglLaporanKeuangan", SqlDbType.VarChar, 10)
		params(2).Value = oCustomClass.TglLaporanKeuangan
		params(3) = New SqlParameter("@AssetIDR", SqlDbType.VarChar, 50)
		params(3).Value = oCustomClass.AssetIDR
		params(4) = New SqlParameter("@AssetLancar", SqlDbType.VarChar, 50)
		params(4).Value = oCustomClass.AssetLancar
		params(5) = New SqlParameter("@KasSetaraKasAsetLancar", SqlDbType.VarChar, 50)
		params(5).Value = oCustomClass.KasSetaraKasAsetLancar
		params(6) = New SqlParameter("@PiutangUsahAsetLancar", SqlDbType.VarChar, 50)
		params(6).Value = oCustomClass.PiutangUsahAsetLancar
		params(7) = New SqlParameter("@InvestasiLainnyaAsetLancar", SqlDbType.VarChar, 50)
		params(7).Value = oCustomClass.InvestasiLainnyaAsetLancar
		params(8) = New SqlParameter("@AsetLancarLainnya", SqlDbType.VarChar, 50)
		params(8).Value = oCustomClass.AsetLancarLainnya
		params(9) = New SqlParameter("@AsetTidakLancar", SqlDbType.VarChar, 50)
		params(9).Value = oCustomClass.AsetTidakLancar
		params(10) = New SqlParameter("@PiutangUsahaAsetTidakLancar", SqlDbType.VarChar, 50)
		params(10).Value = oCustomClass.PiutangUsahaAsetTidakLancar
		params(11) = New SqlParameter("@InvestasiLainnyaAsetTidakLancar", SqlDbType.VarChar, 50)
		params(11).Value = oCustomClass.InvestasiLainnyaAsetTidakLancar
		params(12) = New SqlParameter("@AsetTidakLancarLainnya", SqlDbType.VarChar, 50)
		params(12).Value = oCustomClass.AsetTidakLancarLainnya
		params(13) = New SqlParameter("@Liabilitas", SqlDbType.VarChar, 50)
		params(13).Value = oCustomClass.Liabilitas
		params(14) = New SqlParameter("@LiabilitasJangkaPendek", SqlDbType.VarChar, 50)
		params(14).Value = oCustomClass.LiabilitasJangkaPendek
		params(15) = New SqlParameter("@PinjamanJangkaPendek", SqlDbType.VarChar, 50)
		params(15).Value = oCustomClass.PinjamanJangkaPendek
		params(16) = New SqlParameter("@UtangUsahaJangkaPendek", SqlDbType.VarChar, 50)
		params(16).Value = oCustomClass.UtangUsahaJangkaPendek
		params(17) = New SqlParameter("@LiabilitasJangkaPendekLainnya", SqlDbType.VarChar, 50)
		params(17).Value = oCustomClass.LiabilitasJangkaPendekLainnya
		params(18) = New SqlParameter("@LiabilitasJangkaPanjang", SqlDbType.VarChar, 50)
		params(18).Value = oCustomClass.LiabilitasJangkaPanjang
		params(19) = New SqlParameter("@PinjamanJangkaPanjang", SqlDbType.VarChar, 50)
		params(19).Value = oCustomClass.PinjamanJangkaPanjang
		params(20) = New SqlParameter("@UtangUsahaJangkaPanjang", SqlDbType.VarChar, 50)
		params(20).Value = oCustomClass.UtangUsahaJangkaPanjang
		params(21) = New SqlParameter("@LiabilitasJangkaPanjangLainnya", SqlDbType.VarChar, 50)
		params(21).Value = oCustomClass.LiabilitasJangkaPanjangLainnya
		params(22) = New SqlParameter("@Ekuitas", SqlDbType.VarChar, 50)
		params(22).Value = oCustomClass.Ekuitas
		params(23) = New SqlParameter("@PendapatanUsahaOpr", SqlDbType.VarChar, 50)
		params(23).Value = oCustomClass.PendapatanUsahaOpr
		params(24) = New SqlParameter("@BebanPokokPendapatanOpr", SqlDbType.VarChar, 50)
		params(24).Value = oCustomClass.BebanPokokPendapatanOpr
		params(25) = New SqlParameter("@LabaRugiBruto", SqlDbType.VarChar, 50)
		params(25).Value = oCustomClass.LabaRugiBruto
		params(26) = New SqlParameter("@PLLNonOpr", SqlDbType.VarChar, 50)
		params(26).Value = oCustomClass.PLLNonOpr
		params(27) = New SqlParameter("@BebanLainLainNonOpr", SqlDbType.VarChar, 50)
		params(27).Value = oCustomClass.BebanLainLainNonOpr
		params(28) = New SqlParameter("@LabaRugiSebelumPajak", SqlDbType.VarChar, 50)
		params(28).Value = oCustomClass.LabaRugiSebelumPajak
		params(29) = New SqlParameter("@LabaRugiTahunBerjalan", SqlDbType.VarChar, 50)
		params(29).Value = oCustomClass.LabaRugiTahunBerjalan
		params(30) = New SqlParameter("@KodeKantorCabang", SqlDbType.VarChar, 50)
		params(30).Value = oCustomClass.KodeKantorCabang
		params(31) = New SqlParameter("@OperasiData", SqlDbType.VarChar, 50)
		params(31).Value = oCustomClass.OperasiData
		params(32) = New SqlParameter("@BulanData", SqlDbType.VarChar, 8)
		params(32).Value = oCustomClass.BulanData


		Try
			SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE_ADD, params)
			Return oReturnValue
		Catch ex As Exception
			Throw New Exception("Error On DataAccess.SIPP.K01.GetK01Add")
		End Try
	End Function

	Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.K01) As Parameter.K01
		Dim oReturnValue As New Parameter.K01
		Dim params(0) As SqlParameter

		params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
		params(0).Value = oCustomClass.Table
		Try
			oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
			Return oReturnValue
		Catch exp As Exception
			Throw New Exception("Error On DataAccess.SIPP.K01.GetCboBulandataSIPP")
		End Try
	End Function

	Public Function GetK01Delete(ByVal oCustomClass As Parameter.K01) As String
		Dim Err As Integer
		Dim params(0) As SqlParameter
		params(0) = New SqlParameter("@ID", SqlDbType.Int)
		params(0).Value = oCustomClass.ID
		Try
			Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
			Return ""
		Catch ex As Exception
			Return "ERROR: Unable to delete the selected record. Record already used!"
		End Try
	End Function

End Class


