﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class A01 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
	'Stored Procedure name    
	Private Const LIST_SELECT As String = "spSLIKA01List"
	Private Const SAVE_EDIT As String = "spSLIKA01SaveEdit"
	Private Const LIST_EDIT As String = "spSLIKA01EditList"
	Private Const SAVE_ADD As String = "spSLIKA01SaveAdd"
	Private Const DELETE As String = "spSLIKA01Delete"
	Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
#End Region

	Public Function GetA01(ByVal oCustomClass As Parameter.A01) As Parameter.A01
		Dim oReturnValue As New Parameter.A01
		Dim params(4) As SqlParameter
		params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
		params(0).Value = oCustomClass.CurrentPage
		params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
		params(1).Value = oCustomClass.PageSize
		params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
		params(2).Value = oCustomClass.WhereCond
		params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
		params(3).Value = oCustomClass.SortBy
		params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
		params(4).Direction = ParameterDirection.Output
		Try
			oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
			oReturnValue.TotalRecord = CType(params(4).Value, Int64)
			Return oReturnValue
		Catch ex As Exception
			Throw New Exception("Error On DataAccess.SLIK.A01.GetA01")
		End Try
	End Function

	Public Function GetSelectA01(ocustomClass As Parameter.A01) As Parameter.A01
		Dim params(1) As SqlParameter
		Try
			params(0) = New SqlParameter("@CIF", SqlDbType.VarChar, 100)
			params(0).Value = ocustomClass.CIF
			params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
			params(1).Value = ""

			ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
			Return ocustomClass
		Catch ex As Exception

		End Try
		'Throw New NotImplementedException()
	End Function

	Public Function GetA01Edit(ByVal ocustomClass As Parameter.A01) As Parameter.A01
		Dim params(0) As SqlParameter
		Dim oReturnValue As New Parameter.A01


		params(0) = New SqlParameter("ID", SqlDbType.BigInt)
		params(0).Value = ocustomClass.ID

		Try
			ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
			Return ocustomClass
		Catch ex As Exception
			Throw New Exception("Error On DataAccess.SLIK.A01.GetA01Edit")
		End Try
	End Function

	Public Function GetA01Save(ByVal ocustomClass As Parameter.A01) As Parameter.A01
		Dim params(29) As SqlParameter
		Dim oReturnValue As New Parameter.A01

		params(29) = New SqlParameter("@ID", SqlDbType.VarChar, 50)
		params(29).Value = ocustomClass.ID
		params(28) = New SqlParameter("@FlagDetail", SqlDbType.VarChar, 1)
		params(28).Value = ocustomClass.FlagDetail
		params(27) = New SqlParameter("@NoAgunan", SqlDbType.VarChar, 20)
		params(27).Value = ocustomClass.NoAgunan
		params(26) = New SqlParameter("@NoRekFasilitas", SqlDbType.VarChar, 20)
		params(26).Value = ocustomClass.NoRekFasilitas
		params(25) = New SqlParameter("@CIF", SqlDbType.VarChar, 20)
		params(25).Value = ocustomClass.CIF
		params(24) = New SqlParameter("@KodeJenisSegmen", SqlDbType.VarChar, 3)
		params(24).Value = ocustomClass.KodeJenisSegmen
		params(23) = New SqlParameter("@KodeStatusAgunan", SqlDbType.VarChar, 1)
		params(23).Value = ocustomClass.KodeStatusAgunan
		params(22) = New SqlParameter("@KodeJenisAgunan", SqlDbType.VarChar, 3)
		params(22).Value = ocustomClass.KodeJenisAgunan
		params(21) = New SqlParameter("@PeringkatAgunan", SqlDbType.VarChar, 3)
		params(21).Value = ocustomClass.PeringkatAgunan
		params(20) = New SqlParameter("@KodelembagaPemeringkat", SqlDbType.VarChar, 2)
		params(20).Value = ocustomClass.KodelembagaPemeringkat
		params(19) = New SqlParameter("@KodeJenisPengikatan", SqlDbType.VarChar, 2)
		params(19).Value = ocustomClass.KodeJenisPengikatan
		params(18) = New SqlParameter("@TanggalPengikatan", SqlDbType.VarChar, 10)
		params(18).Value = ocustomClass.TanggalPengikatan
		params(17) = New SqlParameter("@NamaPemilikAgunan", SqlDbType.VarChar, 100)
		params(17).Value = ocustomClass.NamaPemilikAgunan
		params(16) = New SqlParameter("@BuktiKepemilikan", SqlDbType.VarChar, 50)
		params(16).Value = ocustomClass.BuktiKepemilikan
		params(15) = New SqlParameter("@AlamatAgunan", SqlDbType.VarChar, 150)
		params(15).Value = ocustomClass.AlamatAgunan
		params(14) = New SqlParameter("@KodeKabKota", SqlDbType.VarChar, 4)
		params(14).Value = ocustomClass.KodeKabKota
		params(13) = New SqlParameter("@NilaiAgunanNJOP", SqlDbType.VarChar, 50)
		params(13).Value = ocustomClass.NilaiAgunanNJOP
		params(12) = New SqlParameter("@NilaiAgunanPelapor", SqlDbType.VarChar, 50)
		params(12).Value = ocustomClass.NilaiAgunanPelapor
		params(11) = New SqlParameter("@TanggalPenilaianAgunanPelapor", SqlDbType.VarChar, 10)
		params(11).Value = ocustomClass.TanggalPenilaianAgunanPelapor
		params(10) = New SqlParameter("@NilaiAgunanPenilaiIndipenden", SqlDbType.VarChar, 50)
		params(10).Value = ocustomClass.NilaiAgunanPenilaiIndipenden
		params(9) = New SqlParameter("@NamaPenilaiIndependen", SqlDbType.VarChar, 100)
		params(9).Value = ocustomClass.NamaPenilaiIndependen
		params(8) = New SqlParameter("@TanggalPenilaianIndependen", SqlDbType.VarChar, 10)
		params(8).Value = ocustomClass.TanggalPenilaianIndependen
		params(7) = New SqlParameter("@StatusParipasu", SqlDbType.VarChar, 1)
		params(7).Value = ocustomClass.StatusParipasu
		params(6) = New SqlParameter("@PersentaseParipasu", SqlDbType.VarChar, 10)
		params(6).Value = ocustomClass.PersentaseParipasu
		params(5) = New SqlParameter("@PembiayaanJointAccount", SqlDbType.VarChar, 1)
		params(5).Value = ocustomClass.PembiayaanJointAccount
		params(4) = New SqlParameter("@Diasuransikan", SqlDbType.VarChar, 1)
		params(4).Value = ocustomClass.Diasuransikan
		params(3) = New SqlParameter("@Keterangan", SqlDbType.VarChar, 150)
		params(3).Value = ocustomClass.Keterangan
		params(2) = New SqlParameter("@KodeKantorCabang", SqlDbType.VarChar, 3)
		params(2).Value = ocustomClass.KodeKantorCabang
		params(1) = New SqlParameter("@OperasiData", SqlDbType.VarChar, 1)
		params(1).Value = ocustomClass.OperasiData
		params(0) = New SqlParameter("@BulanData", SqlDbType.VarChar, 10)
		params(0).Value = ocustomClass.Bulandata

		Try
			SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SAVE_EDIT, params)
			Return oReturnValue
		Catch ex As Exception
			Throw New Exception("Error On DataAccess.SLIK.A01.GetA01Save")
		End Try
	End Function

	Public Function GetCbo(ByVal oCustomClass As Parameter.A01) As Parameter.A01
		Dim oReturnValue As New Parameter.A01
		Dim params() As SqlParameter = New SqlParameter(0) {}
		params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
		params(0).Value = oCustomClass.Table
		Try
			oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetCboLembagaPemeringkat", params).Tables(0)
			Return oReturnValue
		Catch exp As Exception
			Throw New Exception("Error On DataAccess.SLIK.A01.GetCbo")
		End Try
	End Function

	Public Function GetA01Add(ByVal oCustomClass As Parameter.A01) As Parameter.A01
		Dim params(28) As SqlParameter
		Dim oReturnValue As New Parameter.A01

		params(0) = New SqlParameter("@FlagDetail", SqlDbType.VarChar, 1)
		params(0).Value = oCustomClass.FlagDetail
		params(1) = New SqlParameter("@NoAgunan", SqlDbType.VarChar, 20)
		params(1).Value = oCustomClass.NoAgunan
		params(2) = New SqlParameter("@NoRekFasilitas", SqlDbType.VarChar, 20)
		params(2).Value = oCustomClass.NoRekFasilitas
		params(3) = New SqlParameter("@CIF", SqlDbType.VarChar, 20)
		params(3).Value = oCustomClass.CIF
		params(4) = New SqlParameter("@KodeJenisSegmen", SqlDbType.VarChar, 3)
		params(4).Value = oCustomClass.KodeJenisSegmen
		params(5) = New SqlParameter("@KodeStatusAgunan", SqlDbType.VarChar, 1)
		params(5).Value = oCustomClass.KodeStatusAgunan
		params(6) = New SqlParameter("@KodeJenisAgunan", SqlDbType.VarChar, 3)
		params(6).Value = oCustomClass.KodeJenisAgunan
		params(7) = New SqlParameter("@PeringkatAgunan", SqlDbType.VarChar, 3)
		params(7).Value = oCustomClass.PeringkatAgunan
		params(8) = New SqlParameter("@KodelembagaPemeringkat", SqlDbType.VarChar, 2)
		params(8).Value = oCustomClass.KodelembagaPemeringkat
		params(9) = New SqlParameter("@KodeJenisPengikatan", SqlDbType.VarChar, 2)
		params(9).Value = oCustomClass.KodeJenisPengikatan
		params(10) = New SqlParameter("@TanggalPengikatan", SqlDbType.VarChar, 10)
		params(10).Value = oCustomClass.TanggalPengikatan
		params(11) = New SqlParameter("@NamaPemilikAgunan", SqlDbType.VarChar, 100)
		params(11).Value = oCustomClass.NamaPemilikAgunan
		params(12) = New SqlParameter("@BuktiKepemilikan", SqlDbType.VarChar, 50)
		params(12).Value = oCustomClass.BuktiKepemilikan
		params(13) = New SqlParameter("@AlamatAgunan", SqlDbType.VarChar, 150)
		params(13).Value = oCustomClass.AlamatAgunan
		params(14) = New SqlParameter("@KodeKabKota", SqlDbType.VarChar, 4)
		params(14).Value = oCustomClass.KodeKabKota
		params(15) = New SqlParameter("@NilaiAgunanNJOP", SqlDbType.VarChar, 50)
		params(15).Value = oCustomClass.NilaiAgunanNJOP
		params(16) = New SqlParameter("@NilaiAgunanPelapor", SqlDbType.VarChar, 50)
		params(16).Value = oCustomClass.NilaiAgunanPelapor
		params(17) = New SqlParameter("@TanggalPenilaianAgunanPelapor", SqlDbType.VarChar, 10)
		params(17).Value = oCustomClass.TanggalPenilaianAgunanPelapor
		params(18) = New SqlParameter("@NilaiAgunanPenilaiIndipenden", SqlDbType.VarChar, 50)
		params(18).Value = oCustomClass.NilaiAgunanPenilaiIndipenden
		params(19) = New SqlParameter("@NamaPenilaiIndependen", SqlDbType.VarChar, 100)
		params(19).Value = oCustomClass.NamaPenilaiIndependen
		params(20) = New SqlParameter("@TanggalPenilaianIndependen", SqlDbType.VarChar, 10)
		params(20).Value = oCustomClass.TanggalPenilaianIndependen
		params(21) = New SqlParameter("@StatusParipasu", SqlDbType.VarChar, 1)
		params(21).Value = oCustomClass.StatusParipasu
		params(22) = New SqlParameter("@PersentaseParipasu", SqlDbType.VarChar, 10)
		params(22).Value = oCustomClass.PersentaseParipasu
		params(23) = New SqlParameter("@PembiayaanJointAccount", SqlDbType.VarChar, 1)
		params(23).Value = oCustomClass.PembiayaanJointAccount
		params(24) = New SqlParameter("@Diasuransikan", SqlDbType.VarChar, 1)
		params(24).Value = oCustomClass.Diasuransikan
		params(25) = New SqlParameter("@Keterangan", SqlDbType.VarChar, 150)
		params(25).Value = oCustomClass.Keterangan
		params(26) = New SqlParameter("@KodeKantorCabang", SqlDbType.VarChar, 3)
		params(26).Value = oCustomClass.KodeKantorCabang
		params(27) = New SqlParameter("@OperasiData", SqlDbType.VarChar, 1)
		params(27).Value = oCustomClass.OperasiData
		params(28) = New SqlParameter("@BulanData", SqlDbType.VarChar, 10)
		params(28).Value = oCustomClass.Bulandata

		Try
			SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE_ADD, params)
			Return oReturnValue
		Catch ex As Exception
			Throw New Exception("Error On DataAccess.SIPP.A01.GetA01Add")
		End Try
	End Function

	Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.A01) As Parameter.A01
		Dim oReturnValue As New Parameter.A01
		Dim params(0) As SqlParameter

		params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
		params(0).Value = oCustomClass.Table
		Try
			oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
			Return oReturnValue
		Catch exp As Exception
			Throw New Exception("Error On DataAccess.SIPP.A01.GetCboBulandataSIPP")
		End Try
	End Function

	Public Function GetA01Delete(ByVal oCustomClass As Parameter.A01) As String
		Dim Err As Integer
		Dim params(0) As SqlParameter
		params(0) = New SqlParameter("@ID", SqlDbType.Int)
		params(0).Value = oCustomClass.ID
		Try
			Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
			Return ""
		Catch ex As Exception
			Return "ERROR: Unable to delete the selected record. Record already used!"
		End Try
	End Function

End Class


