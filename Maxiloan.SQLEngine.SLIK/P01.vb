﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class P01 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSLIKP01List"
    Private Const SAVE_EDIT As String = "spSLIKP01SaveEdit"
    Private Const LIST_EDIT As String = "spSLIKP01EditList"
    Private Const SAVE_ADD As String = "spSLIKP01SaveAdd"
    Private Const DELETE As String = "spSLIKP01Delete"
    Private Const spFillCbo As String = "spGetCboKodeHubunganPelapor"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
#End Region

    Public Function GetP01(ByVal oCustomClass As Parameter.P01) As Parameter.P01
        Dim oReturnValue As New Parameter.P01
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.P01.GetP01")
        End Try
    End Function

    Public Function GetSelectP01(oCustomClass As Parameter.P01) As Parameter.P01
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@ID", SqlDbType.BigInt)
            params(0).Value = oCustomClass.CIF
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetP01Edit(ByVal ocustomClass As Parameter.P01) As Parameter.P01
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.P01

        params(0) = New SqlParameter("ID", SqlDbType.BigInt)
        params(0).Value = ocustomClass.ID

        Try
            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.P01.GetP01Edit")
        End Try
    End Function

    Public Function GetP01Save(ByVal oCustomClass As Parameter.P01) As Parameter.P01
        Dim params(16) As SqlParameter
        Dim oReturnValue As New Parameter.P01

        params(16) = New SqlParameter("ID", SqlDbType.BigInt)
        params(16).Value = oCustomClass.ID
        params(15) = New SqlParameter("BulanData", SqlDbType.VarChar, 100)
        params(15).Value = oCustomClass.BulanData
        params(14) = New SqlParameter("Flagdetail", SqlDbType.VarChar, 100)
        params(14).Value = oCustomClass.FlagDetail
        params(13) = New SqlParameter("Nomor_Identitas_Penjamin", SqlDbType.VarChar, 100)
        params(13).Value = oCustomClass.Nomor_Identitas_Penjamin
        params(12) = New SqlParameter("Nomor_Rekening_Fasilitas", SqlDbType.VarChar, 100)
        params(12).Value = oCustomClass.Nomor_Rekening_Fasilitas
        params(11) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
        params(11).Value = oCustomClass.CIF
        params(10) = New SqlParameter("Kode_Jenis_Segmen_Fasilitas", SqlDbType.VarChar, 100)
        params(10).Value = oCustomClass.Kode_Jenis_Segmen_Fasilitas
        params(9) = New SqlParameter("Kode_Jenis_Identitas_Penjamin", SqlDbType.VarChar, 100)
        params(9).Value = oCustomClass.Kode_Jenis_Identitas_Penjamin
        params(8) = New SqlParameter("Nama_Penjamin_Sesuai_Identitas", SqlDbType.VarChar, 100)
        params(8).Value = oCustomClass.Nama_Penjamin_Sesuai_Identitas
        params(7) = New SqlParameter("Nama_Lengkap_Penjamin", SqlDbType.VarChar, 100)
        params(7).Value = oCustomClass.Nama_Lengkap_Penjamin
        params(6) = New SqlParameter("Kode_Golongan_Penjamin", SqlDbType.VarChar, 100)
        params(6).Value = oCustomClass.Kode_Golongan_Penjamin
        params(5) = New SqlParameter("Alamat_Penjamin", SqlDbType.VarChar, 250)
        params(5).Value = oCustomClass.Alamat_Penjamin
        params(4) = New SqlParameter("Persentase_Fasilitas_yang_Dijamin", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.Persentase_Fasilitas_yang_Dijamin
        params(3) = New SqlParameter("Keterangan", SqlDbType.VarChar, 100)
        params(3).Value = oCustomClass.Keterangan
        params(2) = New SqlParameter("Kode_Kantor_Cabang", SqlDbType.VarChar, 100)
        params(2).Value = oCustomClass.Kode_Kantor_Cabang
        params(1) = New SqlParameter("Operasi_Data", SqlDbType.VarChar, 1)
        params(1).Value = oCustomClass.Operasi_Data

        params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE_EDIT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.P01.GetP01Save")
        End Try
    End Function

    Public Function GetP01Add(ByVal oCustomClass As Parameter.P01) As Parameter.P01
        Dim params(14) As SqlParameter
        Dim oReturnValue As New Parameter.P01

        params(0) = New SqlParameter("BulanData", SqlDbType.VarChar, 100)
        params(0).Value = oCustomClass.BulanData
        params(1) = New SqlParameter("Flagdetail", SqlDbType.VarChar, 100)
        params(1).Value = oCustomClass.FlagDetail
        params(2) = New SqlParameter("Nomor_Identitas_Penjamin", SqlDbType.VarChar, 100)
        params(2).Value = oCustomClass.Nomor_Identitas_Penjamin
        params(3) = New SqlParameter("Nomor_Rekening_Fasilitas", SqlDbType.VarChar, 100)
        params(3).Value = oCustomClass.Nomor_Rekening_Fasilitas
        params(4) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.CIF
        params(5) = New SqlParameter("Kode_Jenis_Segmen_Fasilitas", SqlDbType.VarChar, 100)
        params(5).Value = oCustomClass.Kode_Jenis_Segmen_Fasilitas
        params(6) = New SqlParameter("Kode_Jenis_Identitas_Penjamin", SqlDbType.VarChar, 100)
        params(6).Value = oCustomClass.Kode_Jenis_Identitas_Penjamin
        params(7) = New SqlParameter("Nama_Penjamin_Sesuai_Identitas", SqlDbType.VarChar, 100)
        params(7).Value = oCustomClass.Nama_Penjamin_Sesuai_Identitas
        params(8) = New SqlParameter("Nama_Lengkap_Penjamin", SqlDbType.VarChar, 100)
        params(8).Value = oCustomClass.Nama_Lengkap_Penjamin
        params(9) = New SqlParameter("Kode_Golongan_Penjamin", SqlDbType.VarChar, 100)
        params(9).Value = oCustomClass.Kode_Golongan_Penjamin
        params(10) = New SqlParameter("Alamat_Penjamin", SqlDbType.VarChar, 250)
        params(10).Value = oCustomClass.Alamat_Penjamin
        params(11) = New SqlParameter("Persentase_Fasilitas_yang_Dijamin", SqlDbType.VarChar, 100)
        params(11).Value = oCustomClass.Persentase_Fasilitas_yang_Dijamin
        params(12) = New SqlParameter("Keterangan", SqlDbType.VarChar, 100)
        params(12).Value = oCustomClass.Keterangan
        params(13) = New SqlParameter("Kode_Kantor_Cabang", SqlDbType.VarChar, 100)
        params(13).Value = oCustomClass.Kode_Kantor_Cabang
        params(14) = New SqlParameter("Operasi_Data", SqlDbType.VarChar, 1)
        params(14).Value = oCustomClass.Operasi_Data

        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE_ADD, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.P01.GetP01Add")
        End Try
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.P01) As Parameter.P01
        Dim oReturnValue As New Parameter.P01
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.P01.GetCbo")
        End Try
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.P01) As Parameter.P01
        Dim oReturnValue As New Parameter.P01
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.P01.GetCboBulandataSIPP")
        End Try
    End Function

    Public Function GetP01Delete(ByVal oCustomClass As Parameter.P01) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function


End Class


