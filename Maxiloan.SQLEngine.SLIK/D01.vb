﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class D01 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSLIKD01List"
    Private Const SAVE_EDIT As String = "spSLIKD01SaveEdit"
    Private Const LIST_EDIT As String = "spSLIKD01EditList"
    Private Const SAVE_ADD As String = "spSLIKD01SaveAdd"
    Private Const DELETE As String = "spSLIKD01Delete"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
    Private Const spFillCbo As String = "spGetCboKodeHubunganPelapor"
    Private Const spFillCbo1 As String = "spGetCboKodePekerjaan"
#End Region

    Public Function GetD01(ByVal oCustomClass As Parameter.D01) As Parameter.D01
        Dim oReturnValue As New Parameter.D01
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.D01.GetD01")
        End Try
    End Function

    Public Function GetSelectD01(ocustomClass As Parameter.D01) As Parameter.D01
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@CIF", SqlDbType.VarChar, 100)
            params(0).Value = ocustomClass.CIF
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception

        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetD01Edit(ByVal ocustomClass As Parameter.D01) As Parameter.D01
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.D01


        params(0) = New SqlParameter("ID", SqlDbType.BigInt)
        params(0).Value = ocustomClass.ID

        Try
            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.D01.GetD01Edit")
        End Try
    End Function

    Public Function GetD01Save(ByVal ocustomClass As Parameter.D01) As Parameter.D01
        Dim params(39) As SqlParameter
        Dim oReturnValue As New Parameter.D01

        params(39) = New SqlParameter("ID", SqlDbType.BigInt)
        params(39).Value = ocustomClass.ID
        params(38) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
        params(38).Value = ocustomClass.CIF
        params(37) = New SqlParameter("JenisIdentitas", SqlDbType.VarChar, 100)
        params(37).Value = ocustomClass.JenisIdentitas
        params(36) = New SqlParameter("NIK", SqlDbType.VarChar, 100)
        params(36).Value = ocustomClass.NIK
        params(35) = New SqlParameter("NamaIdentitas", SqlDbType.VarChar, 100)
        params(35).Value = ocustomClass.NamaIdentitas
        params(34) = New SqlParameter("NamaLengkap", SqlDbType.VarChar, 100)
        params(34).Value = ocustomClass.NamaLengkap
        params(33) = New SqlParameter("Gelar", SqlDbType.VarChar, 100)
        params(33).Value = ocustomClass.Gelar
        params(32) = New SqlParameter("JenisKelamin", SqlDbType.VarChar, 100)
        params(32).Value = ocustomClass.JenisKelamin
        params(31) = New SqlParameter("TempatLahir", SqlDbType.VarChar, 100)
        params(31).Value = ocustomClass.TempatLahir
        params(30) = New SqlParameter("TanggalLahir", SqlDbType.VarChar, 100)
        params(30).Value = ocustomClass.TanggalLahir
        params(29) = New SqlParameter("NPWP", SqlDbType.VarChar, 100)
        params(29).Value = ocustomClass.NPWP
        params(28) = New SqlParameter("Alamat", SqlDbType.VarChar, 100)
        params(28).Value = ocustomClass.Alamat
        params(27) = New SqlParameter("Kelurahan", SqlDbType.VarChar, 100)
        params(27).Value = ocustomClass.Kelurahan
        params(26) = New SqlParameter("Kecamatan", SqlDbType.VarChar, 100)
        params(26).Value = ocustomClass.Kecamatan
        params(25) = New SqlParameter("Kota", SqlDbType.VarChar, 100)
        params(25).Value = ocustomClass.Kota
        params(24) = New SqlParameter("KodePos", SqlDbType.VarChar, 100)
        params(24).Value = ocustomClass.KodePos
        params(23) = New SqlParameter("Telp", SqlDbType.VarChar, 100)
        params(23).Value = ocustomClass.Telp
        params(22) = New SqlParameter("Hp", SqlDbType.VarChar, 100)
        params(22).Value = ocustomClass.Hp
        params(21) = New SqlParameter("Email", SqlDbType.VarChar, 100)
        params(21).Value = ocustomClass.Email
        params(20) = New SqlParameter("KodeNegara", SqlDbType.VarChar, 100)
        params(20).Value = ocustomClass.KodeNegara
        params(19) = New SqlParameter("KodePekerjaan", SqlDbType.VarChar, 100)
        params(19).Value = ocustomClass.KodePekerjaan
        params(18) = New SqlParameter("TempatBekerja", SqlDbType.VarChar, 255)
        params(18).Value = ocustomClass.TempatBekerja
        params(17) = New SqlParameter("KodeBidangUsaha", SqlDbType.VarChar, 100)
        params(17).Value = ocustomClass.KodeBidangUsaha
        params(16) = New SqlParameter("AlamatTempatBekerja", SqlDbType.VarChar, 100)
        params(16).Value = ocustomClass.AlamatTempatBekerja
        params(15) = New SqlParameter("PenghasilanPertahun", SqlDbType.VarChar, 100)
        params(15).Value = ocustomClass.PenghasilanPertahun
        params(14) = New SqlParameter("KodeSumberPenghasilan", SqlDbType.VarChar, 100)
        params(14).Value = ocustomClass.KodeSumberPenghasilan
        params(13) = New SqlParameter("JumlahTanggungan", SqlDbType.VarChar, 100)
        params(13).Value = ocustomClass.JumlahTanggungan
        params(12) = New SqlParameter("KodeHubunganLJK", SqlDbType.VarChar, 100)
        params(12).Value = ocustomClass.KodeHubunganLJK
        params(11) = New SqlParameter("KodeGolonganDebitur", SqlDbType.VarChar, 100)
        params(11).Value = ocustomClass.KodeGolonganDebitur
        params(10) = New SqlParameter("StatusPerkawinanDebitur", SqlDbType.VarChar, 100)
        params(10).Value = ocustomClass.StatusPerkawinanDebitur
        params(9) = New SqlParameter("NikPaspor", SqlDbType.VarChar, 100)
        params(9).Value = ocustomClass.NikPaspor
        params(8) = New SqlParameter("NamaPasangan", SqlDbType.VarChar, 100)
        params(8).Value = ocustomClass.NamaPasangan
        params(7) = New SqlParameter("TanggalLahirPasangan", SqlDbType.VarChar, 100)
        params(7).Value = ocustomClass.TanggalLahirPasangan
        params(6) = New SqlParameter("PerjanjianPisahHarta", SqlDbType.VarChar, 100)
        params(6).Value = ocustomClass.PerjanjianPisahHarta
        params(5) = New SqlParameter("MelanggarBMPK", SqlDbType.VarChar, 100)
        params(5).Value = ocustomClass.MelanggarBMPK
        params(4) = New SqlParameter("MelampauiBMPK", SqlDbType.VarChar, 100)
        params(4).Value = ocustomClass.MelampauiBMPK
        params(3) = New SqlParameter("NamaGadisIbuKandung", SqlDbType.VarChar, 100)
        params(3).Value = ocustomClass.NamaGadisIbuKandung
        params(2) = New SqlParameter("KodeKantorCabang", SqlDbType.VarChar, 100)
        params(2).Value = ocustomClass.KodeKantorCabang
        params(1) = New SqlParameter("OperasiData", SqlDbType.VarChar, 100)
        params(1).Value = ocustomClass.OperasiData
        'params(0) = New SqlParameter("AB", SqlDbType.VarChar, 255)
        'params(0).Value = ocustomClass.AB
        params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SAVE_EDIT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.D01.GetD01Save")
        End Try
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.D01) As Parameter.D01
        Dim oReturnValue As New Parameter.D01
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.D01.GetCbo")
        End Try
    End Function
    Public Function GetCbo1(ByVal oCustomClass As Parameter.D01) As Parameter.D01
        Dim oReturnValue As New Parameter.D01
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo1, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.D01.GetCbo")
        End Try
    End Function

    Public Function GetD01Add(ByVal oCustomClass As Parameter.D01) As Parameter.D01
        Dim params(38) As SqlParameter
        Dim oReturnValue As New Parameter.D01

        params(0) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
        params(0).Value = oCustomClass.CIF
        params(1) = New SqlParameter("JenisIdentitas", SqlDbType.VarChar, 100)
        params(1).Value = oCustomClass.JenisIdentitas
        params(2) = New SqlParameter("NIK", SqlDbType.VarChar, 100)
        params(2).Value = oCustomClass.NIK
        params(3) = New SqlParameter("NamaIdentitas", SqlDbType.VarChar, 100)
        params(3).Value = oCustomClass.NamaIdentitas
        params(4) = New SqlParameter("NamaLengkap", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.NamaLengkap
        params(5) = New SqlParameter("Gelar", SqlDbType.VarChar, 100)
        params(5).Value = oCustomClass.Gelar
        params(6) = New SqlParameter("JenisKelamin", SqlDbType.VarChar, 100)
        params(6).Value = oCustomClass.JenisKelamin
        params(7) = New SqlParameter("TempatLahir", SqlDbType.VarChar, 100)
        params(7).Value = oCustomClass.TempatLahir
        params(8) = New SqlParameter("TanggalLahir", SqlDbType.VarChar, 100)
        params(8).Value = oCustomClass.TanggalLahir
        params(9) = New SqlParameter("NPWP", SqlDbType.VarChar, 100)
        params(9).Value = oCustomClass.NPWP
        params(10) = New SqlParameter("Alamat", SqlDbType.VarChar, 100)
        params(10).Value = oCustomClass.Alamat
        params(11) = New SqlParameter("Kelurahan", SqlDbType.VarChar, 100)
        params(11).Value = oCustomClass.Kelurahan
        params(12) = New SqlParameter("Kecamatan", SqlDbType.VarChar, 100)
        params(12).Value = oCustomClass.Kecamatan
        params(13) = New SqlParameter("Kota", SqlDbType.VarChar, 100)
        params(13).Value = oCustomClass.Kota
        params(14) = New SqlParameter("KodePos", SqlDbType.VarChar, 100)
        params(14).Value = oCustomClass.KodePos
        params(15) = New SqlParameter("Telp", SqlDbType.VarChar, 100)
        params(15).Value = oCustomClass.Telp
        params(16) = New SqlParameter("Hp", SqlDbType.VarChar, 100)
        params(16).Value = oCustomClass.Hp
        params(17) = New SqlParameter("Email", SqlDbType.VarChar, 100)
        params(17).Value = oCustomClass.Email
        params(18) = New SqlParameter("KodeNegara", SqlDbType.VarChar, 100)
        params(18).Value = oCustomClass.KodeNegara
        params(19) = New SqlParameter("KodePekerjaan", SqlDbType.VarChar, 100)
        params(19).Value = oCustomClass.KodePekerjaan
        params(20) = New SqlParameter("TempatBekerja", SqlDbType.VarChar, 255)
        params(20).Value = oCustomClass.TempatBekerja
        params(21) = New SqlParameter("KodeBidangUsaha", SqlDbType.VarChar, 100)
        params(21).Value = oCustomClass.KodeBidangUsaha
        params(22) = New SqlParameter("AlamatTempatBekerja", SqlDbType.VarChar, 100)
        params(22).Value = oCustomClass.AlamatTempatBekerja
        params(23) = New SqlParameter("PenghasilanPertahun", SqlDbType.VarChar, 100)
        params(23).Value = oCustomClass.PenghasilanPertahun
        params(24) = New SqlParameter("KodeSumberPenghasilan", SqlDbType.VarChar, 100)
        params(24).Value = oCustomClass.KodeSumberPenghasilan
        params(25) = New SqlParameter("JumlahTanggungan", SqlDbType.VarChar, 100)
        params(25).Value = oCustomClass.JumlahTanggungan
        params(26) = New SqlParameter("KodeHubunganLJK", SqlDbType.VarChar, 100)
        params(26).Value = oCustomClass.KodeHubunganLJK
        params(27) = New SqlParameter("KodeGolonganDebitur", SqlDbType.VarChar, 100)
        params(27).Value = oCustomClass.KodeGolonganDebitur
        params(28) = New SqlParameter("StatusPerkawinanDebitur", SqlDbType.VarChar, 100)
        params(28).Value = oCustomClass.StatusPerkawinanDebitur
        params(29) = New SqlParameter("NikPaspor", SqlDbType.VarChar, 100)
        params(29).Value = oCustomClass.NikPaspor
        params(30) = New SqlParameter("NamaPasangan", SqlDbType.VarChar, 100)
        params(30).Value = oCustomClass.NamaPasangan
        params(31) = New SqlParameter("TanggalLahirPasangan", SqlDbType.VarChar, 100)
        params(31).Value = oCustomClass.TanggalLahirPasangan
        params(32) = New SqlParameter("PerjanjianPisahHarta", SqlDbType.VarChar, 100)
        params(32).Value = oCustomClass.PerjanjianPisahHarta
        params(33) = New SqlParameter("MelanggarBMPK", SqlDbType.VarChar, 100)
        params(33).Value = oCustomClass.MelanggarBMPK
        params(34) = New SqlParameter("MelampauiBMPK", SqlDbType.VarChar, 100)
        params(34).Value = oCustomClass.MelampauiBMPK
        params(35) = New SqlParameter("NamaGadisIbuKandung", SqlDbType.VarChar, 100)
        params(35).Value = oCustomClass.NamaGadisIbuKandung
        params(36) = New SqlParameter("KodeKantorCabang", SqlDbType.VarChar, 100)
        params(36).Value = oCustomClass.KodeKantorCabang
        params(37) = New SqlParameter("OperasiData", SqlDbType.VarChar, 100)
        params(37).Value = oCustomClass.OperasiData
        params(38) = New SqlParameter("Bulandata", SqlDbType.VarChar, 10)
        params(38).Value = oCustomClass.BulanData

        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE_ADD, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.D01.GetD01Add")
        End Try
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.D01) As Parameter.D01
        Dim oReturnValue As New Parameter.D01
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.D01.GetCboBulandataSIPP")
        End Try
    End Function

    Public Function GetD01Delete(ByVal oCustomClass As Parameter.D01) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

End Class


