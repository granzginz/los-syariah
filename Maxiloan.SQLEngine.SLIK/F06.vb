﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class F06
    Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSLIKF06List"
    Private Const SAVE_EDIT As String = "spSLIKF06SaveEdit"
    Private Const LIST_EDIT As String = "spSLIKF06EditList"
    Private Const SAVE_ADD As String = "spSLIKF06SaveAdd"
    Private Const DELETE As String = "spSLIKF06Delete"
    Private Const FILL_CBO As String = "spGetCboJenisFasilitasLain"
    Private Const FILL_CBO1 As String = "spGetCboKodeValuta"
    Private Const FILL_CBO2 As String = "spGetCboKodeKualitas"
    Private Const FILL_CBO3 As String = "spGetCboSebabMacet"
    Private Const FILL_CBO4 As String = "spGetCboKodeKondisi"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
#End Region

    Public Function GetF06(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim oReturnValue As New Parameter.F06
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.F06.GetF06")
        End Try
    End Function

    Public Function GetSelectF06(oCustomClass As Parameter.F06) As Parameter.F06
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@CIF", SqlDbType.VarChar, 100)
            params(0).Value = oCustomClass.CIF
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetF06Edit(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.F06

        params(0) = New SqlParameter("ID", SqlDbType.BigInt)
        params(0).Value = oCustomClass.ID

        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.F06.GetF06Edit")
        End Try
    End Function

    Public Function GetF06Save(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim params(21) As SqlParameter
        Dim oReturnValue As New Parameter.F06

        params(21) = New SqlParameter("ID", SqlDbType.BigInt)
        params(21).Value = oCustomClass.ID
        params(20) = New SqlParameter("NoRekeningFasilitas", SqlDbType.VarChar, 100)
        params(20).Value = oCustomClass.NoRekeningFasilitas
        params(19) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
        params(19).Value = oCustomClass.CIF
        params(18) = New SqlParameter("KodeJenisFasilitasLain", SqlDbType.VarChar, 100)
        params(18).Value = oCustomClass.KodeJenisFasilitasLain
        params(17) = New SqlParameter("SumberDana", SqlDbType.VarChar, 100)
        params(17).Value = oCustomClass.SumberDana
        params(16) = New SqlParameter("TanggalMulai", SqlDbType.VarChar, 100)
        params(16).Value = oCustomClass.TanggalMulai
        params(15) = New SqlParameter("TanggalJatuhTempo", SqlDbType.VarChar, 100)
        params(15).Value = oCustomClass.TanggalJatuhTempo
        params(14) = New SqlParameter("SukuBungaatauImbalan", SqlDbType.VarChar, 100)
        params(14).Value = oCustomClass.SukuBungaatauImbalan
        params(13) = New SqlParameter("KodeValuta", SqlDbType.VarChar, 100)
        params(13).Value = oCustomClass.KodeValuta
        params(12) = New SqlParameter("Nominal", SqlDbType.VarChar, 100)
        params(12).Value = oCustomClass.Nominal
        params(11) = New SqlParameter("NilaiDalamMataUangAsal", SqlDbType.VarChar, 100)
        params(11).Value = oCustomClass.NilaiDalamMataUangAsal
        params(10) = New SqlParameter("KodeKualitas", SqlDbType.VarChar, 100)
        params(10).Value = oCustomClass.KodeKualitas
        params(9) = New SqlParameter("TanggalMacet", SqlDbType.VarChar, 100)
        params(9).Value = oCustomClass.TanggalMacet
        params(8) = New SqlParameter("KodeSebabMacet", SqlDbType.VarChar, 100)
        params(8).Value = oCustomClass.KodeSebabMacet
        params(7) = New SqlParameter("Tunggakan", SqlDbType.VarChar, 100)
        params(7).Value = oCustomClass.Tunggakan
        params(6) = New SqlParameter("JumlahHariTunggakan", SqlDbType.VarChar, 100)
        params(6).Value = oCustomClass.JumlahHariTunggakan
        params(5) = New SqlParameter("KodeKondisi", SqlDbType.VarChar, 250)
        params(5).Value = oCustomClass.KodeKondisi
        params(4) = New SqlParameter("TanggalKondisi", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.TanggalKondisi
        params(3) = New SqlParameter("Keterangan", SqlDbType.VarChar, 100)
        params(3).Value = oCustomClass.Keterangan
        params(2) = New SqlParameter("KodeKantorCabang", SqlDbType.VarChar, 100)
        params(2).Value = oCustomClass.KodeKantorCabang
        params(1) = New SqlParameter("OperasiData", SqlDbType.VarChar, 1)
        params(1).Value = oCustomClass.OperasiData

        params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE_EDIT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.F06.GetF06Save")
        End Try
    End Function

    Public Function GetF06Add(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim params(20) As SqlParameter
        Dim oReturnValue As New Parameter.F06

        params(0) = New SqlParameter("Bulandata", SqlDbType.VarChar, 100)
        params(0).Value = oCustomClass.BulanData
        params(1) = New SqlParameter("NoRekeningFasilitas", SqlDbType.VarChar, 100)
        params(1).Value = oCustomClass.NoRekeningFasilitas
        params(2) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
        params(2).Value = oCustomClass.CIF
        params(3) = New SqlParameter("KodeJenisFasilitasLain", SqlDbType.VarChar, 100)
        params(3).Value = oCustomClass.KodeJenisFasilitasLain
        params(4) = New SqlParameter("SumberDana", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.SumberDana
        params(5) = New SqlParameter("TanggalMulai", SqlDbType.VarChar, 100)
        params(5).Value = oCustomClass.TanggalMulai
        params(6) = New SqlParameter("TanggalJatuhTempo", SqlDbType.VarChar, 100)
        params(6).Value = oCustomClass.TanggalJatuhTempo
        params(7) = New SqlParameter("SukuBungaatauImbalan", SqlDbType.VarChar, 100)
        params(7).Value = oCustomClass.SukuBungaatauImbalan
        params(8) = New SqlParameter("KodeValuta", SqlDbType.VarChar, 100)
        params(8).Value = oCustomClass.KodeValuta
        params(9) = New SqlParameter("Nominal", SqlDbType.VarChar, 100)
        params(9).Value = oCustomClass.Nominal
        params(10) = New SqlParameter("NilaiDalamMataUangAsal", SqlDbType.VarChar, 100)
        params(10).Value = oCustomClass.NilaiDalamMataUangAsal
        params(11) = New SqlParameter("KodeKualitas", SqlDbType.VarChar, 100)
        params(11).Value = oCustomClass.KodeKualitas
        params(12) = New SqlParameter("TanggalMacet", SqlDbType.VarChar, 100)
        params(12).Value = oCustomClass.TanggalMacet
        params(13) = New SqlParameter("KodeSebabMacet", SqlDbType.VarChar, 100)
        params(13).Value = oCustomClass.KodeSebabMacet
        params(14) = New SqlParameter("Tunggakan", SqlDbType.VarChar, 100)
        params(14).Value = oCustomClass.Tunggakan
        params(15) = New SqlParameter("JumlahHariTunggakan", SqlDbType.VarChar, 100)
        params(15).Value = oCustomClass.JumlahHariTunggakan
        params(16) = New SqlParameter("KodeKondisi", SqlDbType.VarChar, 250)
        params(16).Value = oCustomClass.KodeKondisi
        params(17) = New SqlParameter("TanggalKondisi", SqlDbType.VarChar, 100)
        params(17).Value = oCustomClass.TanggalKondisi
        params(18) = New SqlParameter("Keterangan", SqlDbType.VarChar, 100)
        params(18).Value = oCustomClass.Keterangan
        params(19) = New SqlParameter("KodeKantorCabang", SqlDbType.VarChar, 100)
        params(19).Value = oCustomClass.KodeKantorCabang
        params(20) = New SqlParameter("OperasiData", SqlDbType.VarChar, 1)
        params(20).Value = oCustomClass.OperasiData
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE_ADD, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.F06.GetF06Add")
        End Try
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim oReturnValue As New Parameter.F06
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.F06.GetCbo")
        End Try
    End Function

    Public Function GetCbo1(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim oReturnValue As New Parameter.F06
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO1, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.F06.GetCbo1")
        End Try
    End Function

    Public Function GetCbo2(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim oReturnValue As New Parameter.F06
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO2, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.F06.GetCbo2")
        End Try
    End Function

    Public Function GetCbo3(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim oReturnValue As New Parameter.F06
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO3, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.F06.GetCbo3")
        End Try
    End Function

    Public Function GetCbo4(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim oReturnValue As New Parameter.F06
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO4, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.F06.GetCbo4")
        End Try
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim oReturnValue As New Parameter.F06
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.F06.GetCboBulandataSIPP")
        End Try
    End Function

    Public Function GetF06Delete(ByVal oCustomClass As Parameter.F06) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

End Class
