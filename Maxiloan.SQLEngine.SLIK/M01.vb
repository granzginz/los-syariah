﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class M01
    Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSLIKM01List"
    Private Const SAVE_EDIT As String = "spSLIKM01SaveEdit"
    Private Const LIST_EDIT As String = "spSLIKM01EditList"
    Private Const SAVE_ADD As String = "spSLIKM01SaveAdd"
    Private Const DELETE As String = "spSLIKM01Delete"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
#End Region

    Public Function GetM01(ByVal oCustomClass As Parameter.M01) As Parameter.M01
        Dim oReturnValue As New Parameter.M01
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.M01.GetM01")
        End Try
    End Function

    Public Function GetSelectM01(oCustomClass As Parameter.M01) As Parameter.M01
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@CIF", SqlDbType.VarChar, 100)
            params(0).Value = oCustomClass.CIF
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetM01Edit(ByVal oCustomClass As Parameter.M01) As Parameter.M01
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.M01

        params(0) = New SqlParameter("ID", SqlDbType.BigInt)
        params(0).Value = oCustomClass.ID

        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.M01.GetM01Edit")
        End Try
    End Function

    Public Function GetM01Save(ByVal oCustomClass As Parameter.M01) As Parameter.M01
        Dim params(15) As SqlParameter
        Dim oReturnValue As New Parameter.M01

        params(15) = New SqlParameter("ID", SqlDbType.BigInt)
        params(15).Value = oCustomClass.ID
        params(14) = New SqlParameter("NoIDPengurus", SqlDbType.VarChar, 100)
        params(14).Value = oCustomClass.NoIDPengurus
        params(13) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
        params(13).Value = oCustomClass.CIF
        params(12) = New SqlParameter("KodeJenisIdentitas", SqlDbType.VarChar, 100)
        params(12).Value = oCustomClass.KodeJenisIdentitas
        params(11) = New SqlParameter("NamaPengurus", SqlDbType.VarChar, 100)
        params(11).Value = oCustomClass.NamaPengurus
        params(10) = New SqlParameter("JenisKelamin", SqlDbType.VarChar, 100)
        params(10).Value = oCustomClass.JenisKelamin
        params(9) = New SqlParameter("Alamat", SqlDbType.VarChar, 100)
        params(9).Value = oCustomClass.Alamat
        params(8) = New SqlParameter("Kelurahan", SqlDbType.VarChar, 100)
        params(8).Value = oCustomClass.Kelurahan
        params(7) = New SqlParameter("Kecamatan", SqlDbType.VarChar, 100)
        params(7).Value = oCustomClass.Kecamatan
        params(6) = New SqlParameter("KodeKota", SqlDbType.VarChar, 100)
        params(6).Value = oCustomClass.KodeKota
        params(5) = New SqlParameter("KodeJabatan", SqlDbType.VarChar, 100)
        params(5).Value = oCustomClass.KodeJabatan
        params(4) = New SqlParameter("PangsaKepemilikan", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.PangsaKepemilikan
        params(3) = New SqlParameter("StatusPengurus", SqlDbType.VarChar, 100)
        params(3).Value = oCustomClass.StatusPengurus
        params(2) = New SqlParameter("KodeCabang", SqlDbType.VarChar, 100)
        params(2).Value = oCustomClass.KodeCabang
        params(1) = New SqlParameter("OperasiData", SqlDbType.VarChar, 100)
        params(1).Value = oCustomClass.OperasiData

        params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE_EDIT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.M01.GetM01Save")
        End Try
    End Function

    Public Function GetM01Add(ByVal oCustomClass As Parameter.M01) As Parameter.M01
        Dim params(15) As SqlParameter
        Dim oReturnValue As New Parameter.M01

        params(0) = New SqlParameter("NoIDPengurus", SqlDbType.VarChar, 100)
        params(0).Value = oCustomClass.NoIDPengurus
        params(1) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
        params(1).Value = oCustomClass.CIF
        params(2) = New SqlParameter("KodeJenisIdentitas", SqlDbType.VarChar, 100)
        params(2).Value = oCustomClass.KodeJenisIdentitas
        params(3) = New SqlParameter("NamaPengurus", SqlDbType.VarChar, 100)
        params(3).Value = oCustomClass.NamaPengurus
        params(4) = New SqlParameter("JenisKelamin", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.JenisKelamin
        params(5) = New SqlParameter("Alamat", SqlDbType.VarChar, 100)
        params(5).Value = oCustomClass.Alamat
        params(6) = New SqlParameter("Kelurahan", SqlDbType.VarChar, 100)
        params(6).Value = oCustomClass.Kelurahan
        params(7) = New SqlParameter("Kecamatan", SqlDbType.VarChar, 100)
        params(7).Value = oCustomClass.Kecamatan
        params(8) = New SqlParameter("KodeKota", SqlDbType.VarChar, 100)
        params(8).Value = oCustomClass.KodeKota
        params(9) = New SqlParameter("KodeJabatan", SqlDbType.VarChar, 100)
        params(9).Value = oCustomClass.KodeJabatan
        params(10) = New SqlParameter("PangsaKepemilikan", SqlDbType.VarChar, 100)
        params(10).Value = oCustomClass.PangsaKepemilikan
        params(11) = New SqlParameter("StatusPengurus", SqlDbType.VarChar, 100)
        params(11).Value = oCustomClass.StatusPengurus
        params(12) = New SqlParameter("KodeCabang", SqlDbType.VarChar, 100)
        params(12).Value = oCustomClass.KodeCabang
        params(13) = New SqlParameter("OperasiData", SqlDbType.VarChar, 100)
        params(13).Value = oCustomClass.OperasiData
        params(14) = New SqlParameter("Bulandata", SqlDbType.VarChar, 10)
        params(14).Value = oCustomClass.BulanData
        params(15) = New SqlParameter("FlagDetail", SqlDbType.VarChar, 1)
        params(15).Value = oCustomClass.FlagDetail

        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE_ADD, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.M01.GetM01Add")
        End Try
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.M01) As Parameter.M01
        Dim oReturnValue As New Parameter.M01
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.M01.GetCboBulandataSIPP")
        End Try
    End Function

    Public Function GetM01Delete(ByVal oCustomClass As Parameter.M01) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function
End Class
