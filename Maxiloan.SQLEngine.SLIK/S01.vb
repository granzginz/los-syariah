﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class S01 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSLIKS01List"
    Private Const SAVE_EDIT As String = "spSLIKS01SaveEdit"
    Private Const LIST_EDIT As String = "spSLIKS01EditList"
    Private Const SAVE_ADD As String = "spSLIKS01SaveAdd"
    Private Const DELETE As String = "spSLIKS01Delete"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
    Private Const spFillCbo As String = "spGetCboKodeHubunganPelapor"
    Private Const spFillCbo1 As String = "spGetCboKodePekerjaan"
#End Region

    Public Function GetS01(ByVal oCustomClass As Parameter.S01) As Parameter.S01
        Dim oReturnValue As New Parameter.S01
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.S01.GetS01")
        End Try
    End Function

    Public Function GetSelectS01(ocustomClass As Parameter.S01) As Parameter.S01
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@CIF", SqlDbType.VarChar, 100)
            params(0).Value = ocustomClass.CIF
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception

        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetS01Edit(ByVal ocustomClass As Parameter.S01) As Parameter.S01
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.S01


        params(0) = New SqlParameter("ID", SqlDbType.BigInt)
        params(0).Value = ocustomClass.ID

        Try
            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.S01.GetS01Edit")
        End Try
    End Function

    Public Function GetS01Save(ByVal ocustomClass As Parameter.S01) As Parameter.S01
        Dim params(30) As SqlParameter
        Dim oReturnValue As New Parameter.S01

        params(30) = New SqlParameter("JML_HARI_TUNGGAKAN_12", SqlDbType.VarChar, 100)
        params(30).Value = ocustomClass.JML_HARI_TUNGGAKAN_12
        params(29) = New SqlParameter("KODE_KUALITAS_12", SqlDbType.VarChar, 100)
        params(29).Value = ocustomClass.KODE_KUALITAS_12
        params(28) = New SqlParameter("JML_HARI_TUNGGAKAN_11", SqlDbType.VarChar, 100)
        params(28).Value = ocustomClass.JML_HARI_TUNGGAKAN_11
        params(27) = New SqlParameter("KODE_KUALITAS_11", SqlDbType.VarChar, 100)
        params(27).Value = ocustomClass.KODE_KUALITAS_11
        params(26) = New SqlParameter("JML_HARI_TUNGGAKAN_10", SqlDbType.VarChar, 100)
        params(26).Value = ocustomClass.JML_HARI_TUNGGAKAN_10
        params(25) = New SqlParameter("KODE_KUALITAS_10", SqlDbType.VarChar, 100)
        params(25).Value = ocustomClass.KODE_KUALITAS_10
        params(24) = New SqlParameter("JML_HARI_TUNGGAKAN_9", SqlDbType.VarChar, 100)
        params(24).Value = ocustomClass.JML_HARI_TUNGGAKAN_9
        params(23) = New SqlParameter("KODE_KUALITAS_9", SqlDbType.VarChar, 100)
        params(23).Value = ocustomClass.KODE_KUALITAS_9
        params(22) = New SqlParameter("JML_HARI_TUNGGAKAN_8", SqlDbType.VarChar, 100)
        params(22).Value = ocustomClass.JML_HARI_TUNGGAKAN_8
        params(21) = New SqlParameter("KODE_KUALITAS_8", SqlDbType.VarChar, 100)
        params(21).Value = ocustomClass.KODE_KUALITAS_8
        params(20) = New SqlParameter("JML_HARI_TUNGGAKAN_7", SqlDbType.VarChar, 100)
        params(20).Value = ocustomClass.JML_HARI_TUNGGAKAN_7
        params(19) = New SqlParameter("KODE_KUALITAS_7", SqlDbType.VarChar, 100)
        params(19).Value = ocustomClass.KODE_KUALITAS_7
        params(18) = New SqlParameter("JML_HARI_TUNGGAKAN_6", SqlDbType.VarChar, 100)
        params(18).Value = ocustomClass.JML_HARI_TUNGGAKAN_6
        params(17) = New SqlParameter("KODE_KUALITAS_6", SqlDbType.VarChar, 100)
        params(17).Value = ocustomClass.KODE_KUALITAS_6
        params(16) = New SqlParameter("JML_HARI_TUNGGAKAN_5", SqlDbType.VarChar, 100)
        params(16).Value = ocustomClass.JML_HARI_TUNGGAKAN_5
        params(15) = New SqlParameter("KODE_KUALITAS_5", SqlDbType.VarChar, 100)
        params(15).Value = ocustomClass.KODE_KUALITAS_5
        params(14) = New SqlParameter("JML_HARI_TUNGGAKAN_4", SqlDbType.VarChar, 100)
        params(14).Value = ocustomClass.JML_HARI_TUNGGAKAN_4
        params(13) = New SqlParameter("KODE_KUALITAS_4", SqlDbType.VarChar, 100)
        params(13).Value = ocustomClass.KODE_KUALITAS_4
        params(12) = New SqlParameter("JML_HARI_TUNGGAKAN_3", SqlDbType.VarChar, 100)
        params(12).Value = ocustomClass.JML_HARI_TUNGGAKAN_3
        params(11) = New SqlParameter("KODE_KUALITAS_3", SqlDbType.VarChar, 100)
        params(11).Value = ocustomClass.KODE_KUALITAS_3
        params(10) = New SqlParameter("JML_HARI_TUNGGAKAN_2", SqlDbType.VarChar, 100)
        params(10).Value = ocustomClass.JML_HARI_TUNGGAKAN_2
        params(9) = New SqlParameter("KODE_KUALITAS_2", SqlDbType.VarChar, 100)
        params(9).Value = ocustomClass.KODE_KUALITAS_2
        params(8) = New SqlParameter("JML_HARI_TUNGGAKAN_1", SqlDbType.VarChar, 100)
        params(8).Value = ocustomClass.JML_HARI_TUNGGAKAN_1
        params(7) = New SqlParameter("KODE_KUALITAS_1", SqlDbType.VarChar, 100)
        params(7).Value = ocustomClass.KODE_KUALITAS_1
        params(6) = New SqlParameter("KODE_JENIS_SEGMEN", SqlDbType.VarChar, 100)
        params(6).Value = ocustomClass.KODE_JENIS_SEGMEN
        params(5) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
        params(5).Value = ocustomClass.CIF
        params(4) = New SqlParameter("NO_REK_FASILITAS", SqlDbType.VarChar, 100)
        params(4).Value = ocustomClass.NO_REK_FASILITAS
        params(3) = New SqlParameter("FLAG", SqlDbType.VarChar, 100)
        params(3).Value = ocustomClass.FLAG
        params(2) = New SqlParameter("BULANDATA", SqlDbType.VarChar, 100)
        params(2).Value = ocustomClass.BULANDATA
        params(1) = New SqlParameter("ID", SqlDbType.VarChar, 100)
        params(1).Value = ocustomClass.ID
        params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SAVE_EDIT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.S01.GetS01Save")
        End Try
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.S01) As Parameter.S01
        Dim oReturnValue As New Parameter.S01
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.S01.GetCbo")
        End Try
    End Function
    Public Function GetCbo1(ByVal oCustomClass As Parameter.S01) As Parameter.S01
        Dim oReturnValue As New Parameter.S01
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo1, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.S01.GetCbo")
        End Try
    End Function

    Public Function GetS01Add(ByVal oCustomClass As Parameter.S01) As Parameter.S01
        Dim params(28) As SqlParameter
        Dim oReturnValue As New Parameter.S01

        params(0) = New SqlParameter("KODE_KUALITAS_12", SqlDbType.VarChar, 100)
        params(0).Value = oCustomClass.KODE_KUALITAS_12
        params(1) = New SqlParameter("JML_HARI_TUNGGAKAN_12", SqlDbType.VarChar, 100)
        params(1).Value = oCustomClass.JML_HARI_TUNGGAKAN_12
        params(2) = New SqlParameter("BULANDATA", SqlDbType.VarChar, 100)
        params(2).Value = oCustomClass.BULANDATA
        params(3) = New SqlParameter("FLAG", SqlDbType.VarChar, 100)
        params(3).Value = oCustomClass.FLAG
        params(4) = New SqlParameter("NO_REK_FASILITAS", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.NO_REK_FASILITAS
        params(5) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
        params(5).Value = oCustomClass.CIF
        params(6) = New SqlParameter("KODE_JENIS_SEGMEN", SqlDbType.VarChar, 100)
        params(6).Value = oCustomClass.KODE_JENIS_SEGMEN
        params(7) = New SqlParameter("KODE_KUALITAS_1", SqlDbType.VarChar, 100)
        params(7).Value = oCustomClass.KODE_KUALITAS_1
        params(8) = New SqlParameter("JML_HARI_TUNGGAKAN_1", SqlDbType.VarChar, 100)
        params(8).Value = oCustomClass.JML_HARI_TUNGGAKAN_1
        params(9) = New SqlParameter("KODE_KUALITAS_2", SqlDbType.VarChar, 100)
        params(9).Value = oCustomClass.KODE_KUALITAS_2
        params(10) = New SqlParameter("JML_HARI_TUNGGAKAN_2", SqlDbType.VarChar, 100)
        params(10).Value = oCustomClass.JML_HARI_TUNGGAKAN_2
        params(11) = New SqlParameter("KODE_KUALITAS_3", SqlDbType.VarChar, 100)
        params(11).Value = oCustomClass.KODE_KUALITAS_3
        params(12) = New SqlParameter("JML_HARI_TUNGGAKAN_3", SqlDbType.VarChar, 100)
        params(12).Value = oCustomClass.JML_HARI_TUNGGAKAN_3
        params(13) = New SqlParameter("KODE_KUALITAS_4", SqlDbType.VarChar, 100)
        params(13).Value = oCustomClass.KODE_KUALITAS_4
        params(14) = New SqlParameter("JML_HARI_TUNGGAKAN_4", SqlDbType.VarChar, 100)
        params(14).Value = oCustomClass.JML_HARI_TUNGGAKAN_4
        params(15) = New SqlParameter("KODE_KUALITAS_5", SqlDbType.VarChar, 100)
        params(15).Value = oCustomClass.KODE_KUALITAS_5
        params(16) = New SqlParameter("JML_HARI_TUNGGAKAN_5", SqlDbType.VarChar, 100)
        params(16).Value = oCustomClass.JML_HARI_TUNGGAKAN_5
        params(17) = New SqlParameter("KODE_KUALITAS_6", SqlDbType.VarChar, 100)
        params(17).Value = oCustomClass.KODE_KUALITAS_6
        params(18) = New SqlParameter("JML_HARI_TUNGGAKAN_6", SqlDbType.VarChar, 100)
        params(18).Value = oCustomClass.JML_HARI_TUNGGAKAN_6
        params(19) = New SqlParameter("KODE_KUALITAS_7", SqlDbType.VarChar, 100)
        params(19).Value = oCustomClass.KODE_KUALITAS_7
        params(20) = New SqlParameter("JML_HARI_TUNGGAKAN_7", SqlDbType.VarChar, 100)
        params(20).Value = oCustomClass.JML_HARI_TUNGGAKAN_7
        params(21) = New SqlParameter("KODE_KUALITAS_8", SqlDbType.VarChar, 100)
        params(21).Value = oCustomClass.KODE_KUALITAS_8
        params(22) = New SqlParameter("JML_HARI_TUNGGAKAN_8", SqlDbType.VarChar, 100)
        params(22).Value = oCustomClass.JML_HARI_TUNGGAKAN_8
        params(23) = New SqlParameter("KODE_KUALITAS_9", SqlDbType.VarChar, 100)
        params(23).Value = oCustomClass.KODE_KUALITAS_9
        params(24) = New SqlParameter("JML_HARI_TUNGGAKAN_9", SqlDbType.VarChar, 100)
        params(24).Value = oCustomClass.JML_HARI_TUNGGAKAN_9
        params(25) = New SqlParameter("KODE_KUALITAS_10", SqlDbType.VarChar, 100)
        params(25).Value = oCustomClass.KODE_KUALITAS_10
        params(26) = New SqlParameter("JML_HARI_TUNGGAKAN_10", SqlDbType.VarChar, 100)
        params(26).Value = oCustomClass.JML_HARI_TUNGGAKAN_10
        params(27) = New SqlParameter("KODE_KUALITAS_11", SqlDbType.VarChar, 100)
        params(27).Value = oCustomClass.KODE_KUALITAS_11
        params(28) = New SqlParameter("JML_HARI_TUNGGAKAN_11", SqlDbType.VarChar, 100)
        params(28).Value = oCustomClass.JML_HARI_TUNGGAKAN_11

        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE_ADD, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.S01.GetS01Add")
        End Try
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.S01) As Parameter.S01
        Dim oReturnValue As New Parameter.S01
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.S01.GetCboBulandataSIPP")
        End Try
    End Function

    Public Function GetS01Delete(ByVal oCustomClass As Parameter.S01) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

End Class



