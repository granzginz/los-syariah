﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region
Public Class F01 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    Private Const LIST_SELECT As String = "spSLIKF01List"
    Private Const SAVE_EDIT As String = "spSLIKF01SaveEdit"
    Private Const LIST_EDIT As String = "spSLIKF01EditList"
    Private Const SAVE_ADD As String = "spSLIKF01SaveAdd"
    Private Const DELETE As String = "spSLIKF01Delete"
    Private Const spFillCbo As String = "spGetCboKodeValuta"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
#End Region


    Public Function GetF01(ocustomClass As Parameter.F01) As Parameter.F01
        Dim oReturnValue As New Parameter.F01
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = ocustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = ocustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = ocustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = ocustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.D01.GetF01")
        End Try
    End Function
    Public Function GetSelectF01(ocustomClass As Parameter.F01) As Parameter.F01
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@Nomor_Rekening_Fasilitas", SqlDbType.VarChar, 100)
            params(0).Value = ocustomClass.Nomor_Rekening_Fasilitas
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception

        End Try
        'Throw New NotImplementedException()
    End Function
    Public Function GetF01Edit(ByVal ocustomClass As Parameter.F01) As Parameter.F01
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.F01
        params(0) = New SqlParameter("ID", SqlDbType.BigInt)
        params(0).Value = ocustomClass.ID
        Try

            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.D01.GetF01Edit")
        End Try
    End Function
    Public Function GetF01Save(ByVal ocustomClass As Parameter.F01) As Parameter.F01
        Dim params(48) As SqlParameter
        Dim oReturnValue As New Parameter.F01

        params(48) = New SqlParameter("ID", SqlDbType.BigInt)
        params(48).Value = ocustomClass.ID
        params(47) = New SqlParameter("Nomor_Rekening_Fasilitas", SqlDbType.VarChar, 255)
        params(47).Value = ocustomClass.Nomor_Rekening_Fasilitas
        params(46) = New SqlParameter("Nomor_CIF_Debitur", SqlDbType.VarChar, 255)
        params(46).Value = ocustomClass.Nomor_CIF_Debitur
        params(45) = New SqlParameter("Kode_Sifat_Kredit_Pembiayaan", SqlDbType.VarChar, 255)
        params(45).Value = ocustomClass.Kode_Sifat_Kredit_Pembiayaan
        params(44) = New SqlParameter("Kode_Jenis_Kredit_Pembiayaan", SqlDbType.VarChar, 255)
        params(44).Value = ocustomClass.Kode_Jenis_Kredit_Pembiayaan
        params(43) = New SqlParameter("Kode_Skim_Akad_Pembiayaan", SqlDbType.VarChar, 255)
        params(43).Value = ocustomClass.Kode_Skim_Akad_Pembiayaan
        params(42) = New SqlParameter("Nomor_Akad_Awal", SqlDbType.VarChar, 255)
        params(42).Value = ocustomClass.Nomor_Akad_Awal
        params(41) = New SqlParameter("Tanggal_Akad_Awal", SqlDbType.VarChar, 255)
        params(41).Value = ocustomClass.Tanggal_Akad_Awal
        params(40) = New SqlParameter("Nomor_Akad_Akhir", SqlDbType.VarChar, 255)
        params(40).Value = ocustomClass.Nomor_Akad_Akhir
        params(39) = New SqlParameter("Tanggal_Akad_Akhir", SqlDbType.VarChar, 255)
        params(39).Value = ocustomClass.Tanggal_Akad_Akhir
        params(38) = New SqlParameter("Baru_Perpanjangan", SqlDbType.VarChar, 255)
        params(38).Value = ocustomClass.Baru_Perpanjangan
        params(37) = New SqlParameter("Tanggal_Awal_Kredit", SqlDbType.VarChar, 255)
        params(37).Value = ocustomClass.Tanggal_Awal_Kredit
        params(36) = New SqlParameter("Tanggal_Mulai", SqlDbType.VarChar, 255)
        params(36).Value = ocustomClass.Tanggal_Mulai
        params(35) = New SqlParameter("Tanggal_Jatuh_Tempo", SqlDbType.VarChar, 255)
        params(35).Value = ocustomClass.Tanggal_Jatuh_Tempo
        params(34) = New SqlParameter("Kode_Kategori_Debitur", SqlDbType.VarChar, 255)
        params(34).Value = ocustomClass.Kode_Kategori_Debitur
        params(33) = New SqlParameter("Kode_Jenis_Penggunaan", SqlDbType.VarChar, 255)
        params(33).Value = ocustomClass.Kode_Jenis_Penggunaan
        params(32) = New SqlParameter("Kode_Orientasi_Penggunaan", SqlDbType.VarChar, 255)
        params(32).Value = ocustomClass.Kode_Orientasi_Penggunaan
        params(31) = New SqlParameter("Kode_Sektor_Ekonomi", SqlDbType.VarChar, 255)
        params(31).Value = ocustomClass.Kode_Sektor_Ekonomi
        params(30) = New SqlParameter("Kode_Dati_Kota", SqlDbType.VarChar, 255)
        params(30).Value = ocustomClass.Kode_Dati_Kota
        params(29) = New SqlParameter("Nilai_Proyek", SqlDbType.VarChar, 255)
        params(29).Value = ocustomClass.Nilai_Proyek
        params(28) = New SqlParameter("Kode_Valuta", SqlDbType.VarChar, 255)
        params(28).Value = ocustomClass.Kode_Valuta
        params(27) = New SqlParameter("Persentase_Suku_Bunga_Imbalan", SqlDbType.VarChar, 255)
        params(27).Value = ocustomClass.Persentase_Suku_Bunga_Imbalan
        params(26) = New SqlParameter("Jenis_Suku_Bunga_Imbalan", SqlDbType.VarChar, 255)
        params(26).Value = ocustomClass.Jenis_Suku_Bunga_Imbalan
        params(25) = New SqlParameter("Kredit_Program_Pemerintah", SqlDbType.VarChar, 255)
        params(25).Value = ocustomClass.Kredit_Program_Pemerintah
        params(24) = New SqlParameter("Takeover_Dari", SqlDbType.VarChar, 255)
        params(24).Value = ocustomClass.Takeover_Dari
        params(23) = New SqlParameter("Sumber_Dana", SqlDbType.VarChar, 255)
        params(23).Value = ocustomClass.Sumber_Dana
        params(22) = New SqlParameter("Plafon_Awal", SqlDbType.VarChar, 255)
        params(22).Value = ocustomClass.Plafon_Awal
        params(21) = New SqlParameter("Plafon", SqlDbType.VarChar, 255)
        params(21).Value = ocustomClass.Plafon
        params(20) = New SqlParameter("Realisasi_Pencairan_Bulan_Berjalan", SqlDbType.VarChar, 255)
        params(20).Value = ocustomClass.Realisasi_Pencairan_Bulan_Berjalan
        params(19) = New SqlParameter("Denda", SqlDbType.VarChar, 255)
        params(19).Value = ocustomClass.Denda
        params(18) = New SqlParameter("Baki_Debet", SqlDbType.VarChar, 255)
        params(18).Value = ocustomClass.Baki_Debet
        params(17) = New SqlParameter("Nilai_Dalam_Mata_Uang_Asal", SqlDbType.VarChar, 255)
        params(17).Value = ocustomClass.Nilai_Dalam_Mata_Uang_Asal
        params(16) = New SqlParameter("Kode_Kolektibilitas", SqlDbType.VarChar, 255)
        params(16).Value = ocustomClass.Kode_Kolektibilitas
        params(15) = New SqlParameter("Tanggal_Macet", SqlDbType.VarChar, 255)
        params(15).Value = ocustomClass.Tanggal_Macet
        params(14) = New SqlParameter("Kode_Sebab_Macet", SqlDbType.VarChar, 255)
        params(14).Value = ocustomClass.Kode_Sebab_Macet
        params(13) = New SqlParameter("Tunggakan_Pokok", SqlDbType.VarChar, 255)
        params(13).Value = ocustomClass.Tunggakan_Pokok
        params(12) = New SqlParameter("Tunggakan_Bunga", SqlDbType.VarChar, 255)
        params(12).Value = ocustomClass.Tunggakan_Bunga
        params(11) = New SqlParameter("Jumlah_Hari_Tunggakan", SqlDbType.VarChar, 255)
        params(11).Value = ocustomClass.Jumlah_Hari_Tunggakan
        params(10) = New SqlParameter("Frekuensi_Tunggakan", SqlDbType.VarChar, 255)
        params(10).Value = ocustomClass.Frekuensi_Tunggakan
        params(9) = New SqlParameter("Frekuensi_Restrukturisasi", SqlDbType.VarChar, 255)
        params(9).Value = ocustomClass.Frekuensi_Restrukturisasi
        params(8) = New SqlParameter("Tanggal_Restrukturisasi_Awal", SqlDbType.VarChar, 255)
        params(8).Value = ocustomClass.Tanggal_Restrukturisasi_Awal
        params(7) = New SqlParameter("Tanggal_Restrukturisasi_Akhir", SqlDbType.VarChar, 255)
        params(7).Value = ocustomClass.Tanggal_Restrukturisasi_Akhir
        params(6) = New SqlParameter("Kode_Cara_Restrukturisasi", SqlDbType.VarChar, 255)
        params(6).Value = ocustomClass.Kode_Cara_Restrukturisasi
        params(5) = New SqlParameter("Kode_Kondisi", SqlDbType.VarChar, 255)
        params(5).Value = ocustomClass.Kode_Kondisi
        params(4) = New SqlParameter("Tanggal_Kondisi", SqlDbType.VarChar, 255)
        params(4).Value = ocustomClass.Tanggal_Kondisi
        params(3) = New SqlParameter("Keterangan", SqlDbType.VarChar, 255)
        params(3).Value = ocustomClass.Keterangan
        params(2) = New SqlParameter("Kode_Kantor_Cabang", SqlDbType.VarChar, 255)
        params(2).Value = ocustomClass.Kode_Kantor_Cabang
        params(1) = New SqlParameter("Operasi_Data", SqlDbType.VarChar, 255)
        params(1).Value = ocustomClass.Operasi_Data
        params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SAVE_EDIT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.F01.GetF01Save")
        End Try
    End Function

    Public Function GetF01Add(ByVal oCustomClass As Parameter.F01) As Parameter.F01
        Dim params(47) As SqlParameter
        Dim oReturnValue As New Parameter.F01

        params(0) = New SqlParameter("Bulandata", SqlDbType.VarChar, 100)
        params(0).Value = oCustomClass.BulanData
        params(1) = New SqlParameter("Nomor_Rekening_Fasilitas", SqlDbType.VarChar, 255)
        params(1).Value = oCustomClass.Nomor_Rekening_Fasilitas
        params(2) = New SqlParameter("Nomor_CIF_Debitur", SqlDbType.VarChar, 255)
        params(2).Value = oCustomClass.Nomor_CIF_Debitur
        params(3) = New SqlParameter("Kode_Sifat_Kredit_Pembiayaan", SqlDbType.VarChar, 255)
        params(3).Value = oCustomClass.Kode_Sifat_Kredit_Pembiayaan
        params(4) = New SqlParameter("Kode_Jenis_Kredit_Pembiayaan", SqlDbType.VarChar, 255)
        params(4).Value = oCustomClass.Kode_Jenis_Kredit_Pembiayaan
        params(5) = New SqlParameter("Kode_Skim_Akad_Pembiayaan", SqlDbType.VarChar, 255)
        params(5).Value = oCustomClass.Kode_Skim_Akad_Pembiayaan
        params(6) = New SqlParameter("Nomor_Akad_Awal", SqlDbType.VarChar, 255)
        params(6).Value = oCustomClass.Nomor_Akad_Awal
        params(7) = New SqlParameter("Tanggal_Akad_Awal", SqlDbType.VarChar, 255)
        params(7).Value = oCustomClass.Tanggal_Akad_Awal
        params(8) = New SqlParameter("Nomor_Akad_Akhir", SqlDbType.VarChar, 255)
        params(8).Value = oCustomClass.Nomor_Akad_Akhir
        params(9) = New SqlParameter("Tanggal_Akad_Akhir", SqlDbType.VarChar, 255)
        params(9).Value = oCustomClass.Tanggal_Akad_Akhir
        params(10) = New SqlParameter("Baru_Perpanjangan", SqlDbType.VarChar, 255)
        params(10).Value = oCustomClass.Baru_Perpanjangan
        params(11) = New SqlParameter("Tanggal_Awal_Kredit", SqlDbType.VarChar, 255)
        params(11).Value = oCustomClass.Tanggal_Awal_Kredit
        params(12) = New SqlParameter("Tanggal_Mulai", SqlDbType.VarChar, 255)
        params(12).Value = oCustomClass.Tanggal_Mulai
        params(13) = New SqlParameter("Tanggal_Jatuh_Tempo", SqlDbType.VarChar, 255)
        params(13).Value = oCustomClass.Tanggal_Jatuh_Tempo
        params(14) = New SqlParameter("Kode_Kategori_Debitur", SqlDbType.VarChar, 255)
        params(14).Value = oCustomClass.Kode_Kategori_Debitur
        params(15) = New SqlParameter("Kode_Jenis_Penggunaan", SqlDbType.VarChar, 255)
        params(15).Value = oCustomClass.Kode_Jenis_Penggunaan
        params(16) = New SqlParameter("Kode_Orientasi_Penggunaan", SqlDbType.VarChar, 255)
        params(16).Value = oCustomClass.Kode_Orientasi_Penggunaan
        params(17) = New SqlParameter("Kode_Sektor_Ekonomi", SqlDbType.VarChar, 255)
        params(17).Value = oCustomClass.Kode_Sektor_Ekonomi
        params(18) = New SqlParameter("Kode_Dati_Kota", SqlDbType.VarChar, 255)
        params(18).Value = oCustomClass.Kode_Dati_Kota
        params(19) = New SqlParameter("Nilai_Proyek", SqlDbType.VarChar, 255)
        params(19).Value = oCustomClass.Nilai_Proyek
        params(20) = New SqlParameter("Kode_Valuta", SqlDbType.VarChar, 255)
        params(20).Value = oCustomClass.Kode_Valuta
        params(21) = New SqlParameter("Persentase_Suku_Bunga_Imbalan", SqlDbType.VarChar, 255)
        params(21).Value = oCustomClass.Persentase_Suku_Bunga_Imbalan
        params(22) = New SqlParameter("Jenis_Suku_Bunga_Imbalan", SqlDbType.VarChar, 255)
        params(22).Value = oCustomClass.Jenis_Suku_Bunga_Imbalan
        params(23) = New SqlParameter("Kredit_Program_Pemerintah", SqlDbType.VarChar, 255)
        params(23).Value = oCustomClass.Kredit_Program_Pemerintah
        params(24) = New SqlParameter("Takeover_Dari", SqlDbType.VarChar, 255)
        params(24).Value = oCustomClass.Takeover_Dari
        params(25) = New SqlParameter("Sumber_Dana", SqlDbType.VarChar, 255)
        params(25).Value = oCustomClass.Sumber_Dana
        params(26) = New SqlParameter("Plafon_Awal", SqlDbType.VarChar, 255)
        params(26).Value = oCustomClass.Plafon_Awal
        params(27) = New SqlParameter("Plafon", SqlDbType.VarChar, 255)
        params(27).Value = oCustomClass.Plafon
        params(28) = New SqlParameter("Realisasi_Pencairan_Bulan_Berjalan", SqlDbType.VarChar, 255)
        params(28).Value = oCustomClass.Realisasi_Pencairan_Bulan_Berjalan
        params(29) = New SqlParameter("Denda", SqlDbType.VarChar, 255)
        params(29).Value = oCustomClass.Denda
        params(30) = New SqlParameter("Baki_Debet", SqlDbType.VarChar, 255)
        params(30).Value = oCustomClass.Baki_Debet
        params(31) = New SqlParameter("Nilai_Dalam_Mata_Uang_Asal", SqlDbType.VarChar, 255)
        params(31).Value = oCustomClass.Nilai_Dalam_Mata_Uang_Asal
        params(32) = New SqlParameter("Kode_Kolektibilitas", SqlDbType.VarChar, 255)
        params(32).Value = oCustomClass.Kode_Kolektibilitas
        params(33) = New SqlParameter("Tanggal_Macet", SqlDbType.VarChar, 255)
        params(33).Value = oCustomClass.Tanggal_Macet
        params(34) = New SqlParameter("Kode_Sebab_Macet", SqlDbType.VarChar, 255)
        params(34).Value = oCustomClass.Kode_Sebab_Macet
        params(35) = New SqlParameter("Tunggakan_Pokok", SqlDbType.VarChar, 255)
        params(35).Value = oCustomClass.Tunggakan_Pokok
        params(36) = New SqlParameter("Tunggakan_Bunga", SqlDbType.VarChar, 255)
        params(36).Value = oCustomClass.Tunggakan_Bunga
        params(37) = New SqlParameter("Jumlah_Hari_Tunggakan", SqlDbType.VarChar, 255)
        params(37).Value = oCustomClass.Jumlah_Hari_Tunggakan
        params(38) = New SqlParameter("Frekuensi_Tunggakan", SqlDbType.VarChar, 255)
        params(38).Value = oCustomClass.Frekuensi_Tunggakan
        params(39) = New SqlParameter("Frekuensi_Restrukturisasi", SqlDbType.VarChar, 255)
        params(39).Value = oCustomClass.Frekuensi_Restrukturisasi
        params(40) = New SqlParameter("Tanggal_Restrukturisasi_Awal", SqlDbType.VarChar, 255)
        params(40).Value = oCustomClass.Tanggal_Restrukturisasi_Awal
        params(41) = New SqlParameter("Tanggal_Restrukturisasi_Akhir", SqlDbType.VarChar, 255)
        params(41).Value = oCustomClass.Tanggal_Restrukturisasi_Akhir
        params(42) = New SqlParameter("Kode_Cara_Restrukturisasi", SqlDbType.VarChar, 255)
        params(42).Value = oCustomClass.Kode_Cara_Restrukturisasi
        params(43) = New SqlParameter("Kode_Kondisi", SqlDbType.VarChar, 255)
        params(43).Value = oCustomClass.Kode_Kondisi
        params(44) = New SqlParameter("Tanggal_Kondisi", SqlDbType.VarChar, 255)
        params(44).Value = oCustomClass.Tanggal_Kondisi
        params(45) = New SqlParameter("Keterangan", SqlDbType.VarChar, 255)
        params(45).Value = oCustomClass.Keterangan
        params(46) = New SqlParameter("Kode_Kantor_Cabang", SqlDbType.VarChar, 255)
        params(46).Value = oCustomClass.Kode_Kantor_Cabang
        params(47) = New SqlParameter("Operasi_Data", SqlDbType.VarChar, 255)
        params(47).Value = oCustomClass.Operasi_Data

        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE_ADD, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.F01.GetF01Add")
        End Try
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.F01) As Parameter.F01
        Dim oReturnValue As New Parameter.F01
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.F01.GetCbo")
        End Try
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.F01) As Parameter.F01
        Dim oReturnValue As New Parameter.F01
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.F01.GetCboBulandataSIPP")
        End Try
    End Function

    Public Function GetF01Delete(ByVal oCustomClass As Parameter.F01) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function
End Class