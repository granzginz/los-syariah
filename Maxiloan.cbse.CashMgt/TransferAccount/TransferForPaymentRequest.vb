

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class TransferForPaymentRequest : Inherits ComponentBase
    Implements ITransferForPaymentRequest

    Public Function ListRequest(ByVal customclass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest Implements [Interface].ITransferForPaymentRequest.ListRequest
        Dim DATransferRequest As New SQLEngine.CashMgt.TransferForPaymentRequest
        Return DATransferRequest.ListRequest(customclass)
    End Function

    Public Function ListPaymentDetail(ByVal customclass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest Implements [Interface].ITransferForPaymentRequest.ListPaymentDetail
        Dim DATransferRequest As New SQLEngine.CashMgt.TransferForPaymentRequest
        Return DATransferRequest.ListPaymentDetail(customclass)
    End Function

    Public Sub SavePaymentRequest(ByVal customclass As Parameter.TransferForPaymentRequest) Implements [Interface].ITransferForPaymentRequest.SavePaymentRequest
        Dim DATransferRequest As New SQLEngine.CashMgt.TransferForPaymentRequest
        DATransferRequest.SavePaymentRequest(customclass)
    End Sub

    Public Function EbankPYREQAdd(ByVal oCustomClass As Parameter.TransferForPaymentRequest) As String Implements [Interface].ITransferForPaymentRequest.EbankPYREQAdd
        Dim DATransferRequest As New SQLEngine.CashMgt.TransferForPaymentRequest
        Return DATransferRequest.EbankPYREQAdd(oCustomClass)
    End Function


    Public Function ListRequestOtorisasi(ByVal customclass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest Implements [Interface].ITransferForPaymentRequest.ListRequestOtorisasi
        Dim DATransferRequest As New SQLEngine.CashMgt.TransferForPaymentRequest
        Return DATransferRequest.ListRequestOtorisasi(customclass)
    End Function

    Public Function ListPaymentDetailOtorisasi(ByVal customclass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest Implements [Interface].ITransferForPaymentRequest.ListPaymentDetailOtorisasi
        Dim DATransferRequest As New SQLEngine.CashMgt.TransferForPaymentRequest
        Return DATransferRequest.ListPaymentDetailOtorisasi(customclass)
    End Function

    Public Sub SavePaymentRequestOtorisasi(ByVal customclass As Parameter.TransferForPaymentRequest) Implements [Interface].ITransferForPaymentRequest.SavePaymentRequestOtorisasi
        Dim DATransferRequest As New SQLEngine.CashMgt.TransferForPaymentRequest
        DATransferRequest.SavePaymentRequestOtorisasi(customclass)
    End Sub

    Public Function ListPaymentHistoryReject(ByVal customclass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest Implements [Interface].ITransferForPaymentRequest.ListPaymentHistoryReject
        Dim DATransferRequest As New SQLEngine.CashMgt.TransferForPaymentRequest
        Return DATransferRequest.ListPaymentHistoryReject(customclass)
    End Function

End Class
