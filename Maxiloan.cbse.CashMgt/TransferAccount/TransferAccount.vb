


#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class TransferAccount : Inherits ComponentBase

    Implements ITransferAccount

    Public Function GetBankType(ByVal customclass As Parameter.TransferAccount) As Parameter.TransferAccount Implements [Interface].ITransferAccount.GetBankType
        Dim DAGetBankType As New SQLEngine.CashMgt.TransferAccount
        Return DAGetBankType.GetBankType(customclass)
    End Function
    Public Sub SaveTrasnferAccount(ByVal customclass As Parameter.TransferAccount) Implements [Interface].ITransferAccount.SaveTrasnferAccount
        Dim DASaveTA As New SQLEngine.CashMgt.TransferAccount
        DASaveTA.SaveTransferAccount(customclass)
    End Sub
    Public Sub SaveTransferAccountOtorisasi(ByVal customclass As Parameter.TransferAccount) Implements [Interface].ITransferAccount.SaveTransferAccountOtorisasi
        Dim DASaveTA As New SQLEngine.CashMgt.TransferAccount
        DASaveTA.SaveTransferAccountOtorisasi(customclass)
    End Sub
    Public Function InqTransferFund(ByVal customclass As Parameter.TransferFundInquiry) As Parameter.TransferFundInquiry Implements [Interface].ITransferAccount.InqTransferFund
        Dim DATransferFund As New SQLEngine.CashMgt.TransferAccount
        Return DATransferFund.InqTransferFund(customclass)
    End Function
    Public Function ViewRptTransferFund(ByVal customclass As Parameter.TransferFundInquiry) As Parameter.TransferFundInquiry Implements [Interface].ITransferAccount.ViewRptTransferFund
        Dim DARptTransferFund As New SQLEngine.CashMgt.TransferAccount
        Return DARptTransferFund.ViewRptTransferFund(customclass)
    End Function
    Public Function GetViewTransferFund(ByVal customclass As Parameter.TransferFundInquiry) As Parameter.TransferFundInquiry Implements [Interface].ITransferAccount.GetViewTransferFund
        Dim DAViewTransferFund As New SQLEngine.CashMgt.TransferAccount
        Return DAViewTransferFund.GetViewTransferFund(customclass)
    End Function

    Public Function EbankTRACCOUNTAdd(ByVal customclass As Parameter.TransferAccount) As Parameter.TransferAccount Implements [Interface].ITransferAccount.EbankTRACCOUNTAdd
        Dim DASaveTA As New SQLEngine.CashMgt.TransferAccount
        Return DASaveTA.EbankTRACCOUNTAdd(customclass)
    End Function
End Class
