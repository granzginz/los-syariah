#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class TransferHOBranch : Inherits ComponentBase
    Implements ITransferHOBranch

    Public Function GetBankType(ByVal customclass As Parameter.TransferAccount) As Parameter.TransferAccount Implements [Interface].ITransferHOBranch.GetBankType
        Dim DAGetBankType As New SQLEngine.CashMgt.TransferHOBranch
        Return DAGetBankType.GetBankType(customclass)
    End Function

    Public Sub SaveTransferHOBranch(ByVal customclass As Parameter.TransferAccount) Implements [Interface].ITransferHOBranch.SaveTransferHOBranch
        Dim DASaveHB As New SQLEngine.CashMgt.TransferHOBranch
        DASaveHB.SaveTransferHOBranch(customclass)
    End Sub

    Public Function EBankTRFUNDAdd(ByVal customclass As Parameter.TransferAccount) As Parameter.TransferAccount Implements [Interface].ITransferHOBranch.EBankTRFUNDAdd
        Dim DASaveHB As New SQLEngine.CashMgt.TransferHOBranch
        Return DASaveHB.EBankTRFUNDAdd(customclass)
    End Function
    Public Sub PenarikanDanaCabangSave(customclass As Parameter.TransferAccount) Implements [Interface].ITransferHOBranch.PenarikanDanaCabangSave
        Dim DASaveHB As New SQLEngine.CashMgt.TransferHOBranch
        DASaveHB.PenarikanDanaCabangSave(customclass)
    End Sub

    Public Sub OtorisasiPenarikanDanaCabangSave(customclass As Parameter.TransferAccount) Implements [Interface].ITransferHOBranch.OtorisasiPenarikanDanaCabangSave
        Dim DASaveHB As New SQLEngine.CashMgt.TransferHOBranch
        DASaveHB.OtorisasiPenarikanDanaCabangSave(customclass)
    End Sub

    Public Sub OtorisasiPenarikanDanaCabangReject(customclass As Parameter.TransferAccount) Implements [Interface].ITransferHOBranch.OtorisasiPenarikanDanaCabangReject
        Dim DASaveHB As New SQLEngine.CashMgt.TransferHOBranch
        DASaveHB.OtorisasiPenarikanDanaCabangReject(customclass)
    End Sub
End Class
