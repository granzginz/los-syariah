

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class TransferForReimbursePC : Inherits ComponentBase
    Implements ITransferForReimbursePC

    Public Function ListPCReimburse(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC Implements [Interface].ITransferForReimbursePC.ListPCReimburse
        Dim DATRFundPC As New SQLEngine.CashMgt.TransferForReimbursePC
        Return DATRFundPC.ListTransferForReimbursePC(customclass)
    End Function

    Public Function ListBranchHO(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC Implements [Interface].ITransferForReimbursePC.ListBranchHO
        Dim DATRFundPC As New SQLEngine.CashMgt.TransferForReimbursePC
        Return DATRFundPC.ListBranchHO(customclass)
    End Function

    Public Function ListBankAccount(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC Implements [Interface].ITransferForReimbursePC.ListBankAccount
        Dim DATRFundPC As New SQLEngine.CashMgt.TransferForReimbursePC
        Return DATRFundPC.ListBankAccount(customclass)
    End Function

    Public Function ListPettyCash(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC Implements [Interface].ITransferForReimbursePC.ListPettyCash
        Dim DATRFundPC As New SQLEngine.CashMgt.TransferForReimbursePC
        Return DATRFundPC.ListPettyCash(customclass)
    End Function

    Public Function ListBilyetGiro(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC Implements [Interface].ITransferForReimbursePC.ListBilyetGiro
        Dim DATRFundPC As New SQLEngine.CashMgt.TransferForReimbursePC
        Return DATRFundPC.ListBilyetGiro(customclass)
    End Function

    Public Sub SaveReimbursePC(ByVal customclass As Parameter.TransferForReimbursePC) Implements [Interface].ITransferForReimbursePC.SaveReimbursePC
        Dim DATRFundPC As New SQLEngine.CashMgt.TransferForReimbursePC
        DATRFundPC.SaveReimbursePC(customclass)
    End Sub

    Public Function CheckEndingBalance(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC Implements [Interface].ITransferForReimbursePC.CheckEndingBalance
        Dim DATRFundPC As New SQLEngine.CashMgt.TransferForReimbursePC
        Return DATRFundPC.CheckEndingBalance(customclass)
    End Function

    Public Function EbankPCREIAdd(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC Implements [Interface].ITransferForReimbursePC.EbankPCREIAdd
        Dim DATRFundPC As New SQLEngine.CashMgt.TransferForReimbursePC
        Return DATRFundPC.EbankPCREIAdd(customclass)
    End Function

    Public Function ListPCReimburseOtorisasi(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC Implements [Interface].ITransferForReimbursePC.ListPCReimburseOtorisasi
        Dim DATRFundPC As New SQLEngine.CashMgt.TransferForReimbursePC
        Return DATRFundPC.ListTransferForReimbursePCOtorisasi(customclass)
    End Function

    Public Function ListPettyCashOtorisasi(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC Implements [Interface].ITransferForReimbursePC.ListPettyCashOtorisasi
        Dim DATRFundPC As New SQLEngine.CashMgt.TransferForReimbursePC
        Return DATRFundPC.ListPettyCashOtorisasi(customclass)
    End Function

    Public Sub SaveReimbursePCOtorisasi(ByVal customclass As Parameter.TransferForReimbursePC) Implements [Interface].ITransferForReimbursePC.SaveReimbursePCOtorisasi
        Dim DATRFundPC As New SQLEngine.CashMgt.TransferForReimbursePC
        DATRFundPC.SaveReimbursePCOtorisasi(customclass)
    End Sub

    Public Function ListPettyCashApr(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC Implements [Interface].ITransferForReimbursePC.ListPettyCashApr
        Dim DATRFundPC As New SQLEngine.CashMgt.TransferForReimbursePC
        Return DATRFundPC.ListPettyCashApr(customclass)
    End Function

    Public Function ListBankAccount2(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC Implements [Interface].ITransferForReimbursePC.ListBankAccount2
        Dim DATRFundPC As New SQLEngine.CashMgt.TransferForReimbursePC
        Return DATRFundPC.ListBankAccount2(customclass)
    End Function
End Class
