

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region

Public Class CashAdvance
    Implements ICashAdvance


    Dim SQLCashAdvance As New SQLEngine.CashMgt.CashAdvance
    Public Function GetListCashAdvance(oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance Implements ICashAdvance.GetListCashAdvance
        Return SQLCashAdvance.GetListCashAdvance(oCustomClass)
    End Function

    Public Function GetPagingCashAdvance(oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance Implements ICashAdvance.GetPagingCashAdvance
        Return SQLCashAdvance.GetPagingCashAdvance(oCustomClass)
    End Function

    Public Function SaveCashAdvance(oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance Implements ICashAdvance.SaveCashAdvance
        Return SQLCashAdvance.SaveCashAdvance(oCustomClass)
    End Function

    Public Function SaveCashAdvanceRealisasiDetail(oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance Implements ICashAdvance.SaveCashAdvanceRealisasiDetail
        Return SQLCashAdvance.SaveCashAdvanceRealisasiDetail(oCustomClass)
    End Function

    Public Function GetListCashAdvancedetail(oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance Implements ICashAdvance.GetListCashAdvanceDetail
        Return SQLCashAdvance.GetListCashAdvanceDetail(oCustomClass)
    End Function

    Public Function ReverseReversalCashAdvanced(oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance Implements ICashAdvance.ReverseReversalCashAdvanced
        Return SQLCashAdvance.ReverseReversalCashAdvanced(oCustomClass)
    End Function

    Public Function SaveReverseReversalCashAdvanced(oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance Implements ICashAdvance.SaveReverseReversalCashAdvanced
        Return SQLCashAdvance.SaveReverseReversalCashAdvanced(oCustomClass)
    End Function
End Class
