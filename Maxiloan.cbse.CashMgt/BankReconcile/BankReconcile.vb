


#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class BankReconcile : Inherits ComponentBase

    Implements IBankReconcile
    Public Function ListBankRecon(ByVal customclass As Parameter.BankReconcile) As Parameter.BankReconcile Implements [Interface].IBankReconcile.ListBankRecon
        Dim DAListBankRecon As New SQLEngine.CashMgt.BankReconcile
        Return DAListBankRecon.ListPDCDeposit(customclass)
    End Function

    Public Function ListBankReconDet(ByVal customclass As Parameter.BankReconcile) As Parameter.BankReconcile Implements [Interface].IBankReconcile.ListBankReconDet
        Dim DAListBankReconDet As New SQLEngine.CashMgt.BankReconcile
        Return DAListBankReconDet.ListPDCDepositDet(customclass)
    End Function

    Public Sub SaveBankRecon(ByVal customclass As Parameter.BankReconcile) Implements [Interface].IBankReconcile.SaveBankRecon
        Dim DASaveBankRecon As New SQLEngine.CashMgt.BankReconcile
        DASaveBankRecon.SaveBankRecon(customclass)

    End Sub
    Public Function InfoReconBalance(ByVal customclass As Parameter.BankReconcile) As Parameter.BankReconcile Implements [Interface].IBankReconcile.InfoReconBalance
        Dim DAListBankReconDet As New SQLEngine.CashMgt.BankReconcile
        Return DAListBankReconDet.InfoReconBalance(customclass)
    End Function

End Class
