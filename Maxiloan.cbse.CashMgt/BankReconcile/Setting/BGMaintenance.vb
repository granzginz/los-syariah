

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class BGMaintenance : Inherits ComponentBase
    Implements IBGMnt

    Private isError As Boolean
    Public Function ListBGMnt(ByVal customclass As Parameter.BGMnt) As Parameter.BGMnt Implements [Interface].IBGMnt.ListBGMnt
        Dim DAListBGMnt As New SQLEngine.CashMgt.BGMnt
        Return DAListBGMnt.ListBGMnt(customclass)
    End Function

 
    Public Function BGMntCmbBankAccount(ByVal customclass As Parameter.BGMnt) As Parameter.BGMnt Implements [Interface].IBGMnt.BGMntCmbBankAccount
        Dim DAAddBGMnt As New SQLEngine.CashMgt.BGMnt
        Return DAAddBGMnt.BGMntCmbBankAccount(customclass)
    End Function

    Public Function ReportBGMnt(ByVal customclass As Parameter.BGMnt) As Parameter.BGMnt Implements [Interface].IBGMnt.ReportBGMnt
        Dim DAReportBGMnt As New SQLEngine.CashMgt.BGMnt
        Return DAReportBGMnt.ReportBGMnt(customclass)
    End Function

#Region "CekValidation"
    Private Function Is_ValidBG(ByVal CustomClass As Parameter.BGMnt) As Parameter.BGMnt
        Dim lObjBG As New ArrayList
        Dim lStrBGNo As String = CustomClass.BGNo
        Dim lStrBGCheck As String = ""
        Dim lBytEnd As Integer = lStrBGNo.Length - 1
        Dim lBytCounter As Integer
        Dim lStrPrefix As String = ""
        Dim lStrBG As String = ""
        Dim lStrZero As String = ""
        Dim lStrCheck As String
        Dim lBlnValid As Boolean = True
        Dim lIntBGNo As Int64

        For lBytCounter = 0 To lBytEnd
            If IsNumeric(lStrBGNo.Chars(lBytCounter)) Then
                lStrBG &= lStrBGNo.Chars(lBytCounter)
            Else
                If lStrBG <> "" Then CustomClass.IsValidate = False
                lStrPrefix &= lStrBGNo.Chars(lBytCounter)
            End If
        Next
        If lStrBG = "" Then CustomClass.IsValidate = False

        If lBlnValid Then
            'Check PDC
            lBytEnd = CustomClass.NumOfBG
            lStrZero = lStrZero.PadLeft(lStrBG.Length, CType("0", Char))

            For lBytCounter = 1 To lBytEnd
                lIntBGNo = CLng(lStrBG) + lBytCounter - 1
                If lIntBGNo.ToString.Length >= lStrZero.Length Then
                    lStrCheck = lStrPrefix & lIntBGNo
                Else
                    lStrCheck = lStrPrefix & lStrZero.Substring(0, lStrZero.Length - lIntBGNo.ToString.Length) & lIntBGNo
                End If
                If lObjBG.Contains(lStrCheck.ToUpper) Then
                    CustomClass.IsValidate = False
                    Exit For
                Else
                    lStrBGCheck &= CStr(IIf(lStrBGCheck = "", "", ", ")) & "'" & lStrCheck & "'"
                End If
            Next
        End If

        Dim DAPdcCheckDuplikasi As New SQLEngine.CashMgt.BGMnt
        CustomClass.BGNoDummy = lStrBGCheck
        If Not DAPdcCheckDuplikasi.BGMntCekKey(CustomClass) Then
            CustomClass.IsValidate = True
            CustomClass.BGNumber = lStrBG
            CustomClass.BGPrefix = lStrPrefix
        Else
            CustomClass.IsValidate = False
        End If
        Return CustomClass
    End Function
    Private Function Is_ValidBGManual(ByVal CustomClass As Parameter.BGMnt) As Parameter.BGMnt
        Dim lObjBG As New ArrayList
        Dim lStrBGNo As String = CustomClass.BGNo
        Dim lStrBGCheck As String = ""
        Dim lBytEnd As Integer = lStrBGNo.Length - 1
        Dim lBytCounter As Integer
        Dim lStrPrefix As String = ""
        Dim lStrBG As String = ""
        Dim lStrZero As String = ""
        Dim lStrCheck As String
        Dim lBlnValid As Boolean = True
        Dim lIntBGNo As Int64

        For lBytCounter = 0 To lBytEnd
            If IsNumeric(lStrBGNo.Chars(lBytCounter)) Then
                lStrBG &= lStrBGNo.Chars(lBytCounter)
            Else
                If lStrBG <> "" Then CustomClass.IsValidate = False
                lStrPrefix &= lStrBGNo.Chars(lBytCounter)
            End If
        Next
        If lStrBG = "" Then CustomClass.IsValidate = False

        If lBlnValid Then
            'Check PDC
            lBytEnd = CustomClass.NumOfBG
            lStrZero = lStrZero.PadLeft(lStrBG.Length, CType("0", Char))

            For lBytCounter = 1 To lBytEnd
                lIntBGNo = CLng(lStrBG) + lBytCounter - 1
                If lIntBGNo.ToString.Length >= lStrZero.Length Then
                    lStrCheck = lStrPrefix & lIntBGNo
                Else
                    lStrCheck = lStrPrefix & lStrZero.Substring(0, lStrZero.Length - lIntBGNo.ToString.Length) & lIntBGNo
                End If
                If lObjBG.Contains(lStrCheck.ToUpper) Then
                    CustomClass.IsValidate = False
                    Exit For
                Else
                    lStrBGCheck &= CStr(IIf(lStrBGCheck = "", "", ", ")) & "'" & lStrCheck & "'"
                End If
            Next
        End If

        Dim DAPdcCheckDuplikasi As New SQLEngine.CashMgt.BGMnt
        CustomClass.BGNoDummy = lStrBGCheck
        If Not DAPdcCheckDuplikasi.BGMntCekKey(CustomClass) Then
            CustomClass.IsValidate = True
            CustomClass.BGNumber = lStrBG
            CustomClass.BGPrefix = lStrPrefix
        Else
            CustomClass.IsValidate = False
        End If
        Return CustomClass
    End Function

#End Region

#Region "WritingBGNo"
    Public Function GetStructBGNo() As DataSet
        Dim lObjDataTable As New DataSet
        Dim dtTable As New DataTable("BG")

        lObjDataTable.Tables.Add(dtTable)

        lObjDataTable.Tables("BG").Columns.Add("BranchID", System.Type.GetType("System.String"))
        lObjDataTable.Tables("BG").Columns.Add("delxml", System.Type.GetType("System.String"))
        'lObjDataTable.Tables("BG").Columns.Add("BankAccountID", System.Type.GetType("System.String"))
        lObjDataTable.Tables("BG").Columns.Add("BGNo", System.Type.GetType("System.String"))
        'lObjDataTable.Tables("BG").Columns.Add("Status", System.Type.GetType("System.String"))
        'lObjDataTable.Tables("BG").Columns.Add("StatusTanggal", System.Type.GetType("System.String"))
        Return lObjDataTable
    End Function

    Private Function Generate_Table(ByVal CustomClass As Parameter.BGMnt, ByVal dsBG As DataSet) As DataSet
        Dim lCounter As Integer
        Dim lBytCounter As Integer
        Dim IBytEnd As Integer
        Dim lStrBGNo As String = CustomClass.BGNo
        Dim lBytEnd As Integer = lStrBGNo.Length - 1
        Dim lStrBG As String = ""
        Dim lStrPrefix As String = ""
        Dim strZero As String = ""
        Dim SerialBGNo As Int64
        Dim strBGNo As String
        Dim lObjDataRow As DataRow

        For lBytCounter = 0 To lBytEnd
            If IsNumeric(lStrBGNo.Chars(lBytCounter)) Then
                lStrBG &= lStrBGNo.Chars(lBytCounter)
            Else
                'If lStrPDC <> "" Then CustomClass.IsValidPDC = False
                lStrPrefix &= lStrBGNo.Chars(lBytCounter)
            End If
        Next
        With CustomClass
            strZero = strZero.PadLeft(lStrBG.Trim.Length, CType("0", Char))

            For lCounter = 1 To .NumOfBG
                SerialBGNo = CLng(lStrBG) + lCounter - 1
                If SerialBGNo.ToString.Length >= strZero.Length Then
                    strBGNo = lStrPrefix & SerialBGNo
                Else
                    strBGNo = lStrPrefix & strZero.Substring(0, strZero.Length - SerialBGNo.ToString.Length) & SerialBGNo
                End If
                lObjDataRow = dsBG.Tables("BG").NewRow()
                ' lObjDataRow = dsBG.Tables("BG").NewRow()
                lObjDataRow("BranchID") = .BranchId
                lObjDataRow("delxml") = .delxml

                'lObjDataRow("BankAccountID") = .BankAccountID
                lObjDataRow("BGNo") = strBGNo
                'lObjDataRow("Status") = .Status
                'lObjDataRow("StatusTanggal") = .StatusDate
                dsBG.Tables("BG").Rows.Add(lObjDataRow)
            Next
        End With

        Return dsBG
    End Function

    Private Function Generate_TableManual(ByVal CustomClass As Parameter.BGMnt, ByVal dsBG As DataSet) As DataSet
        Dim lCounter As Integer
        Dim lBytCounter As Integer
        Dim IBytEnd As Integer
        Dim lStrNumOfBG As String = CustomClass.NumOfBG

        ' Dim lBytEnd As Integer = lStrNumOfBG.Length - 1
        Dim lStrBG As String = ""
        Dim lStrPrefix As String = ""
        Dim strZero As String = ""
        Dim SerialBGNo As Integer
        Dim strBGNo As String = String.Empty
        Dim lObjDataRow As DataRow
        Dim sudahAdaIsi As Boolean

        If CustomClass.listBGno Is Nothing Then
            sudahAdaIsi = False
        Else
            sudahAdaIsi = True
        End If

        With CustomClass
            strZero = strZero.PadLeft(lStrBG.Trim.Length, CType("0", Char))

            For lCounter = 1 To .NumOfBG

                lObjDataRow = dsBG.Tables("BG").NewRow()

                lObjDataRow("BranchID") = .BranchId
                lObjDataRow("delxml") = lCounter - 1

                If sudahAdaIsi Then
                    lObjDataRow("BGNo") = CustomClass.listBGno(lCounter - 1)
                Else
                    lObjDataRow("BGNo") = strBGNo
                End If

                dsBG.Tables("BG").Rows.Add(lObjDataRow)
            Next
        End With

        Return dsBG
    End Function
    Public Function GetTableBGMnt(ByVal customclass As Parameter.BGMnt, ByVal strFile As String) As Parameter.BGMnt Implements [Interface].IBGMnt.GetTableBGMnt
        Dim CheckBGNo As DataRow()
        Dim lObjHoliday As New Hashtable
        Dim lObjPDC As New ArrayList
        Dim lCounter As Integer

        Dim dsBG As New DataSet


        Dim pstrFile As String
        Dim gStrPath As String

        isError = False

        Dim CustomBG As New Parameter.BGMnt
        With customclass
            pstrFile = "C:\temp\" + strFile & "_M_BG"
            If .FlagStatus = False Then

               
                If customclass.Mode = "AddMode" Then
                    CustomBG = Is_ValidBG(customclass)

                Else
                    CustomBG.IsValidate = True
                End If

                If CustomBG.IsValidate Then
                    File.Delete(pstrFile & ".xsd")
                    File.Delete(pstrFile & ".xml")
                    If Not File.Exists(pstrFile & ".xsd") Then
                        dsBG = GetStructBGNo()
                        dsBG.WriteXmlSchema(pstrFile & ".xsd")
                        dsBG.ReadXmlSchema(pstrFile & ".xsd")
                    End If
                    If Not File.Exists(pstrFile & ".xml") Then
                        dsBG.WriteXml(pstrFile & ".xml")
                        dsBG.AcceptChanges()
                    Else
                        dsBG.ReadXml(pstrFile & ".xml")
                        dsBG.AcceptChanges()
                        CheckBGNo = dsBG.Tables("BG").Select("BGNo = '" & .BGNo & "'")
                        If CheckBGNo.Length > 0 And Not customclass.Mode = "AddModeManual" Then
                            .IsValidate = False
                            Return customclass
                        End If
                    End If

                    If customclass.Mode = "AddMode" Then
                        dsBG = Generate_Table(customclass, dsBG)
                    Else
                        dsBG = Generate_TableManual(customclass, dsBG)

                    End If

                    If Not isError Then
                        dsBG.WriteXml(pstrFile & ".xml")
                        dsBG.AcceptChanges()
                        customclass.ListBGMnt = dsBG.Tables("BG")
                    Else
                        Dim dsNewPDC As New DataSet
                        dsNewPDC.ReadXml(pstrFile & ".xml")
                        customclass.ListBGMnt = dsNewPDC.Tables("BG")
                        customclass.IsValidate = False
                    End If
                Else
                    customclass.IsValidate = False
                End If
            Else
                If Not File.Exists(pstrFile & ".xsd") Then
                    dsBG = GetStructBGNo()
                    dsBG.WriteXmlSchema(pstrFile & ".xsd")
                    dsBG.ReadXmlSchema(pstrFile & ".xsd")
                End If
                If Not File.Exists(pstrFile & ".xml") Then
                    dsBG.WriteXml(pstrFile & ".xml")
                    dsBG.AcceptChanges()
                Else
                    dsBG.ReadXml(pstrFile & ".xml")
                    dsBG.AcceptChanges()
                End If

                dsBG = Generate_TableDelete(customclass, dsBG)
                dsBG.WriteXml(pstrFile & ".xml")
                dsBG.AcceptChanges()
                customclass.ListBGMnt = dsBG.Tables("BG")
                If dsBG.Tables("BG").Rows.Count = 0 Then
                    File.Delete(pstrFile & ".xsd")
                    File.Delete(pstrFile & ".xml")
                End If
            End If
        End With
        Return customclass
    End Function


    Private Function Generate_TableDelete(ByVal CustomClass As Parameter.BGMnt, ByVal dsBG As DataSet) As DataSet
        With CustomClass
            dsBG.Tables("BG").Rows.RemoveAt(CInt(CustomClass.IndexDelete))            
            Return dsBG
        End With
    End Function

    Public Function SaveBGMnt(ByVal customclass As Parameter.BGMnt, ByVal strName As String) As String Implements [Interface].IBGMnt.SaveBGMnt
        Dim DASaveBG As New SQLEngine.CashMgt.BGMnt
        With customclass
            Dim pstrFile As String = "C:\temp\" + strName & "_M_BG"
            Dim dsNewBG As New DataSet
            dsNewBG.ReadXml(pstrFile & ".xml")
            Return DASaveBG.AddBGMnt(customclass, dsNewBG.Tables("BG"))
        End With
    End Function

    Public Function UpdateBGMnt(ByVal customclass As Parameter.BGMnt) As String Implements [Interface].IBGMnt.UpdateBGMnt
        Dim DAAddBGMnt As New SQLEngine.CashMgt.BGMnt
        Return DAAddBGMnt.UpdateBGMnt(customclass)
    End Function
#End Region

#Region "AP Selection"
    Public Function ListAPSele(ByVal customclass As Parameter.BGMnt) As Parameter.BGMnt Implements [Interface].IBGMnt.ListAPSele
        Dim DAListAPSele As New SQLEngine.CashMgt.BGMnt
        Return DAListAPSele.ListAPSele(customclass)
    End Function
#End Region

End Class
