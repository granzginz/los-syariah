﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class CKonfirmasiEbankExec : Inherits ComponentBase
    Implements IEKonfirmasiEbankExec


    Public Function GetListing(oCostom As Parameter.PEKonfirmasiEbankExec) As Parameter.PEKonfirmasiEbankExec Implements [Interface].IEKonfirmasiEbankExec.GetListing
        Dim sql As New SQLEngine.CashMgt.QEKonfirmasiEbankExec
        Return sql.GetListing(oCostom)
    End Function

    Public Function Saving(oCustom As Parameter.PEKonfirmasiEbankExec) As String Implements [Interface].IEKonfirmasiEbankExec.Saving
        Dim sql As New SQLEngine.CashMgt.QEKonfirmasiEbankExec
        Return sql.Saving(oCustom)
    End Function
End Class
