Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

Public Class AgentFeeDisbursement : Inherits ComponentBase
    Implements IAgentFeeDisbursement


    Public Function getAgentFeeDisbursementList(ByVal customClass As Parameter.AgentFeeDisbursement) As Parameter.AgentFeeDisbursement Implements [Interface].IAgentFeeDisbursement.getAgentFeeDisbursementList
        Dim oSQLE As New SQLEngine.CashMgt.AgentFeeDisbursement
        Return oSQLE.getAgentFeeDisbursementList(customClass)
    End Function

    Public Function saveAgentFeeDisbursement(ByVal customClass As Parameter.AgentFeeDisbursement) As String Implements [Interface].IAgentFeeDisbursement.saveAgentFeeDisbursement
        Dim oSQLE As New SQLEngine.CashMgt.AgentFeeDisbursement
        Return oSQLE.saveAgentFeeDisbursement(customClass)
    End Function
End Class
