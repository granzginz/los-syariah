

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class APDisbInq : Inherits ComponentBase
    Implements IAPDisbInq

    Public Function ListAPDisbInq(ByVal customclass As Parameter.APDisbInq) As Parameter.APDisbInq Implements [Interface].IAPDisbInq.ListAPDisbInq
        Dim DAListAPDisbInq As New SQLEngine.CashMgt.APDisbInq
        Return DAListAPDisbInq.ListAPDisbInq(customclass)
    End Function

    Public Function ReportAPDisbInq(ByVal customclass As Parameter.APDisbInq) As Parameter.APDisbInq Implements [Interface].IAPDisbInq.ReportAPDisbInq
        Dim DAReportAPDisbInq As New SQLEngine.CashMgt.APDisbInq
        Return DAReportAPDisbInq.ReportAPDisbInq(customclass)
    End Function

#Region "PV Inquiry"
    Public Function ListPVInquiry(ByVal customclass As Parameter.APDisbInq) As Parameter.APDisbInq Implements [Interface].IAPDisbInq.ListPVInquiry
        Dim DAListPVInquiry As New SQLEngine.CashMgt.APDisbInq
        Return DAListPVInquiry.ListPVInquiry(customclass)
    End Function
    Public Function ReportPVInquiry(ByVal customclass As Parameter.APDisbInq) As Parameter.APDisbInq Implements [Interface].IAPDisbInq.ReportPVInquiry
        Dim DAReportPVInquiry As New SQLEngine.CashMgt.APDisbInq
        Return DAReportPVInquiry.ReportPVInquiry(customclass)
    End Function
#End Region

    Public Function ListAPDisbInqPrintSelection(ByVal customclass As Parameter.APDisbInq) As Parameter.APDisbInq Implements [Interface].IAPDisbInq.ListAPDisbInqPrintSelection
        Dim DAListAPDisbInq As New SQLEngine.CashMgt.APDisbInq
        Return DAListAPDisbInq.ListAPDisbInqPrintSelection(customclass)
    End Function

    Public Sub savePrintSelection(ByVal customclass As Parameter.APDisbInq) Implements [Interface].IAPDisbInq.savePrintSelection
        Dim DAListAPDisbInq As New SQLEngine.CashMgt.APDisbInq
        DAListAPDisbInq.savePrintSelection(customclass)
    End Sub
End Class
