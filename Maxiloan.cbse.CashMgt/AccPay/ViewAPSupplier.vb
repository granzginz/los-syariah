

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class ViewAPSupplier : Inherits ComponentBase

    Implements IViewAPSupplier
    Public Function ViewList(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec Implements [Interface].IViewAPSupplier.ViewList
        Dim DAView As New SQLEngine.CashMgt.ViewAPSupplier
        Return DAView.ListView(customclass)
    End Function
    Public Function ListViewAP(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec Implements [Interface].IViewAPSupplier.ListViewAP
        Dim DAViewAP As New SQLEngine.CashMgt.ViewAPSupplier
        Return DAViewAP.ListViewAP(customclass)
    End Function

    Public Function ViewListPO(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec Implements [Interface].IViewAPSupplier.ViewListPO
        Dim DAView As New SQLEngine.CashMgt.ViewAPSupplier
        Return DAView.ListViewPO(customclass)
    End Function

    Public Function ViewInvoiceDeduction(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec Implements [Interface].IViewAPSupplier.ViewInvoiceDeduction
        Dim DAView As New SQLEngine.CashMgt.ViewAPSupplier
        Return DAView.ViewInvoiceDeduction(customclass)
    End Function
End Class
