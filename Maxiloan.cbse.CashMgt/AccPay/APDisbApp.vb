

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region

Public Class APDisbApp : Inherits ComponentBase
    Implements IAPDisbApp

    Public Function ListApOtorTrans(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp Implements [Interface].IAPDisbApp.ListApOtorTrans
        Dim DAListAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        Return DAListAPDisbApp.ListApOtorTrans(customclass)
    End Function
    Public Function ListAPDisbApp(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp Implements [Interface].IAPDisbApp.ListAPDisbApp
        Dim DAListAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        Return DAListAPDisbApp.ListAPDisbApp(customclass)
    End Function
    Public Function InformasiAPDisbApp(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp Implements [Interface].IAPDisbApp.InformasiAPDisbApp
        Dim DAInformasiAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        Return DAInformasiAPDisbApp.InformasiAPDisbApp(customclass)
    End Function

    Public Function UpdateAPDisbApp(ByVal customclass As Parameter.APDisbApp) As String Implements [Interface].IAPDisbApp.UpdateAPDisbApp
        Dim DAUpdateAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        Return DAUpdateAPDisbApp.UpdateAPDisbApp(customclass)
    End Function

    Public Function UpdateAPDisbApp2(ByVal customclass As Parameter.APDisbApp) As String Implements [Interface].IAPDisbApp.UpdateAPDisbApp2
        Dim DAUpdateAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        Return DAUpdateAPDisbApp.UpdateAPDisbApp2(customclass)
    End Function

    Public Function ListAPDisbApp2(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp Implements [Interface].IAPDisbApp.ListAPDisbApp2
        Dim DAListAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        Return DAListAPDisbApp.ListAPDisbApp2(customclass)
    End Function

    Public Function ListAPDisbApp3(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp Implements [Interface].IAPDisbApp.ListAPDisbApp3
        Dim DAListAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        Return DAListAPDisbApp.ListAPDisbApp3(customclass)
    End Function

    Public Function ListAPDisbApp4(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp Implements [Interface].IAPDisbApp.ListAPDisbApp4
        Dim DAListAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        Return DAListAPDisbApp.ListAPDisbApp4(customclass)
    End Function

    Public Function MemoPembayaranReport(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp Implements [Interface].IAPDisbApp.MemoPembayaranReport
        Dim DAListAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        Return DAListAPDisbApp.MemoPembayaranReport(customclass)
    End Function
    Public Function MemoPembayaranDetail1Report(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp Implements [Interface].IAPDisbApp.MemoPembayaranDetail1Report
        Dim DAListAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        Return DAListAPDisbApp.MemoPembayaranDetail1Report(customclass)
    End Function
    Public Function MemoPembayaranDetail2Report(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp Implements [Interface].IAPDisbApp.MemoPembayaranDetail2Report
        Dim DAListAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        Return DAListAPDisbApp.MemoPembayaranDetail2Report(customclass)
    End Function


#Region "APDisbCash"
    Public Sub UpdateAPDisbCash(ByVal customclass As Parameter.APDisbApp, Optional isApDisbTransBeforeDisburse As Boolean = True) Implements [Interface].IAPDisbApp.UpdateAPDisbCash
        Dim DAUpdateAPDisbCash As New SQLEngine.CashMgt.APDisbApp
        If isApDisbTransBeforeDisburse Then
            DAUpdateAPDisbCash.UpdateApDisbTransBeforeDisburse(customclass)
            Return
        End If
        DAUpdateAPDisbCash.UpdateAPDisbCash(customclass)
    End Sub
#End Region

    Public Function GetReleaseEbanking(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp Implements [Interface].IAPDisbApp.GetReleaseEbanking
        Dim DAListAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        Return DAListAPDisbApp.GetReleaseEbanking(customclass)
    End Function

    Public Sub ReleaseEbankingUpdateStatus(ByVal ocustomClass As Parameter.APDisbApp) Implements [Interface].IAPDisbApp.ReleaseEbankingUpdateStatus
        Dim DAListAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        DAListAPDisbApp.ReleaseEbankingUpdateStatus(ocustomClass)
    End Sub
    Public Sub declinceReleaseEbanking(ByVal ocustomClass As Parameter.APDisbApp) Implements [Interface].IAPDisbApp.declinceReleaseEbanking
        Dim DAListAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        DAListAPDisbApp.declinceReleaseEbanking(ocustomClass)
    End Sub
    Public Function UpdateMultiAPDisbApp(cnn As String, status As String, branchId As String, datas As IList(Of Parameter.ValueTextObject), ByVal ParamArray bankSelected() As String) As String Implements [Interface].IAPDisbApp.UpdateMultiAPDisbApp
        Dim DAListAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        Return DAListAPDisbApp.UpdateMultiAPDisbApp(cnn, status, branchId, datas, bankSelected)
    End Function

    Public Function LoadComboRekeningBank(cnn As String, caraBayar As String) As IList(Of Parameter.CommonValueText) Implements [Interface].IAPDisbApp.LoadComboRekeningBank
        Dim DAListAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        Return DAListAPDisbApp.LoadComboRekeningBank(cnn, caraBayar)
    End Function
    Public Function APgantiCaraBayar(customclass As Parameter.APDisbApp) As String Implements IAPDisbApp.APgantiCaraBayar
        Dim DAUpdateAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        Return DAUpdateAPDisbApp.APgantiCaraBayar(customclass)
    End Function

    Public Function UpdateMultiAPDisbApp2(ocustomClass As Parameter.APDisbApp) As Parameter.APDisbApp Implements IAPDisbApp.UpdateMultiAPDisbApp2
        Dim DAListAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        Return DAListAPDisbApp.UpdateMultiAPDisbApp2(ocustomClass)
    End Function
    Public Function UpdateMultiAPDisbApp2_Report(ocustomClass As Parameter.APDisbApp) As Parameter.APDisbApp Implements IAPDisbApp.UpdateMultiAPDisbApp2_Report
        Dim DAListAPDisbApp As New SQLEngine.CashMgt.APDisbApp
        Return DAListAPDisbApp.UpdateMultiAPDisbApp2_Report(ocustomClass)
    End Function
End Class
