

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class EBankTransfer : Inherits ComponentBase
    Implements IEBankTranfer


    Public Function EBankTransferSave(ByVal listdata As ArrayList, ByVal pvdata As Parameter.APDisbApp) As String Implements [Interface].IEBankTranfer.EBankTransferSave
        Dim DAEBankTransfer As New SQLEngine.CashMgt.EBankTransfer
        Return DAEBankTransfer.EBankTransferSave(listdata, pvdata)
    End Function

    Public Function EBankTransferList(ByVal customclass As Parameter.EBankTransfer) As Parameter.EBankTransfer Implements [Interface].IEBankTranfer.EBankTransferList
        Dim DAEBankTransfer As New SQLEngine.CashMgt.EBankTransfer
        Return DAEBankTransfer.EBankTransferList(customclass)
    End Function

    Public Function EBankTransferList2(ByVal customclass As Parameter.EBankTransfer) As Parameter.EBankTransfer Implements [Interface].IEBankTranfer.EBankTransferList2
        Dim DAEBankTransfer As New SQLEngine.CashMgt.EBankTransfer
        Return DAEBankTransfer.EBankTransferList2(customclass)
    End Function

    'Public Function EBankExecute(ByVal customclass As Parameter.APDisbApp) As String Implements [Interface].IEBankTranfer.EBankExecute
    '    Dim DAEBankTransfer As New SQLEngine.CashMgt.EBankTransfer
    '    Return DAEBankTransfer.EBankExecute(customclass)
    'End Function

    Public Function EBankTransferChangeStatus(ByVal customclass As Parameter.EBankTransfer) As String Implements [Interface].IEBankTranfer.EBankTransferChangeStatus
        Dim DAEBankTransfer As New SQLEngine.CashMgt.EBankTransfer
        Return DAEBankTransfer.EBankTransferChangeStatus(customclass)
    End Function

    Public Function EBankRejectSave(ByVal customclass As Parameter.EBankReject) As String Implements [Interface].IEBankTranfer.EBankRejectSave
        Dim DAEBankTransfer As New SQLEngine.CashMgt.EBankTransfer
        Return DAEBankTransfer.EBankRejectSave(customclass)
    End Function

    Public Function EBankExecuteReject(ByVal customclass As Parameter.APDisbApp) As String Implements [Interface].IEBankTranfer.EBankExecuteReject
        Dim DAEBankTransfer As New SQLEngine.CashMgt.EBankTransfer
        Return DAEBankTransfer.EBankExecuteReject(customclass)
    End Function

    'Public Function EBankPYREQExec(ByVal customclass As Parameter.APDisbApp) As String Implements [Interface].IEBankTranfer.EBankPYREQExec
    '    Dim DAEBankTransfer As New SQLEngine.CashMgt.EBankTransfer
    '    Return DAEBankTransfer.EBankPYREQExec(customclass)
    'End Function

    'Public Function EBankTRFAAExec(ByVal customclass As Parameter.APDisbApp) As String Implements [Interface].IEBankTranfer.EBankTRFAAExec
    '    Dim DAEBankTransfer As New SQLEngine.CashMgt.EBankTransfer
    '    Return DAEBankTransfer.EBankTRFAAExec(customclass)
    'End Function

    'Public Function EBankTRFUNDExec(ByVal customclass As Parameter.APDisbApp) As String Implements [Interface].IEBankTranfer.EBankTRFUNDExec
    '    Dim DAEBankTransfer As New SQLEngine.CashMgt.EBankTransfer
    '    Return DAEBankTransfer.EBankTRFUNDExec(customclass)
    'End Function

    'Public Function EBankPCREIExec(ByVal customclass As Parameter.APDisbApp) As String Implements [Interface].IEBankTranfer.EBankPCREIExec
    '    Dim DAEBankTransfer As New SQLEngine.CashMgt.EBankTransfer
    '    Return DAEBankTransfer.EBankPCREIExec(customclass)
    'End Function

    Public Function EBankExecuteNew(ByVal listdata As ArrayList, ByVal ebankdata As Parameter.EBankTransfer) As String Implements [Interface].IEBankTranfer.EBankExecuteNew
        Dim DAEBankTransfer As New SQLEngine.CashMgt.EBankTransfer
        Return DAEBankTransfer.EBankExecuteNew(listdata, ebankdata)
    End Function

    Public Function GetRequestEdit(ByVal customclass As Parameter.EBankTransfer) As Parameter.EBankTransfer Implements [Interface].IEBankTranfer.GetRequestEdit
        Dim DAEBankTransfer As New SQLEngine.CashMgt.EBankTransfer
        Return DAEBankTransfer.GetRequestEdit(customclass)
    End Function
End Class
