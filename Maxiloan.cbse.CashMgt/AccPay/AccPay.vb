Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

Public Class AccPay : Inherits ComponentBase
    Implements IAccPay

    Public Function getPencairanPinjamaList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter Implements [Interface].IAccPay.getPencairanPinjamaList
        Dim oSQLE As New SQLEngine.CashMgt.AccPaySQLE
        Return oSQLE.getPencairanPinjamanList(customClass)
    End Function

    Public Function savePencairanPinjamanHO(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter Implements [Interface].IAccPay.SavePencairanPinjamanHO
        Dim oSQLE As New SQLEngine.CashMgt.AccPaySQLE
        Return oSQLE.savePencairanPinjamanHO(customClass)
    End Function

    Public Function getPencairanPinjamanCustomerList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter Implements [Interface].IAccPay.getPencairanPinjamanCustomerList
        Dim oSQLE As New SQLEngine.CashMgt.AccPaySQLE
        Return oSQLE.getPencairanPinjamanCustomerList(customClass)
    End Function

    Public Function getInquiryPencairanPinjamanList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter Implements [Interface].IAccPay.getInquiryPencairanPinjamanList
        Dim oSQLE As New SQLEngine.CashMgt.AccPaySQLE
        Return oSQLE.getInquiryPencairanPinjamanList(customClass)
    End Function

    Public Function getOutstandingPencairanPinjamanList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter Implements [Interface].IAccPay.getOutstandingPencairanPinjamanList
        Dim oSQLE As New SQLEngine.CashMgt.AccPaySQLE
        Return oSQLE.getOutstandingPencairanPinjamanList(customClass)
    End Function

    Public Sub SavePencairanPinjamanCustomer(ByVal customClass As Parameter.AccPayParameter) Implements [Interface].IAccPay.SavePencairanPinjamanCustomer
        Dim oSQLE As New SQLEngine.CashMgt.AccPaySQLE
        oSQLE.savePencairanPinjamanCustomer(customClass)
    End Sub

    Public Function getBuktiKasKeluarList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter Implements [Interface].IAccPay.getBuktiKasKeluarList
        Dim oSQLE As New SQLEngine.CashMgt.AccPaySQLE
        Return oSQLE.getBuktiKasKeluarList(customClass)
    End Function

    Public Sub saveBiayaProses(ByVal customClass As Parameter.AccPayParameter) Implements [Interface].IAccPay.saveBiayaProses 
        Dim oSQLE As New SQLEngine.CashMgt.AccPaySQLE
        oSQLE.saveBiayaProses(customClass)
    End Sub

    Public Function BiayaProsesReportDetail(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter Implements [Interface].IAccPay.BiayaProsesReportDetail
        Dim oSQLE As New SQLEngine.CashMgt.AccPaySQLE
        Return oSQLE.BiayaProsesReportDetail(customClass)
    End Function

    Public Function BiayaProsesReportSummary(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter Implements [Interface].IAccPay.BiayaProsesReportSummary
        Dim oSQLE As New SQLEngine.CashMgt.AccPaySQLE
        Return oSQLE.BiayaProsesReportSummary(customClass)
    End Function
End Class
