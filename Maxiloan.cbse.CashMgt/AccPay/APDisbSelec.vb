

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class APDisbSelec : Inherits ComponentBase
    Implements IAPDisbSelec


    Public Function ListAPSele(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec Implements [Interface].IAPDisbSelec.ListAPSele
        Dim DAListAPSele As New SQLEngine.CashMgt.APDisbSelec
        Return DAListAPSele.ListAPSele(customclass)
    End Function

    Public Function APSelectionList(ByVal oCustomClass As Parameter.APDisbSelec) As System.Data.DataTable Implements [Interface].IAPDisbSelec.APSelectionList
        Dim DAListAPSele As New SQLEngine.CashMgt.APDisbSelec
        Return DAListAPSele.APSelectionList(oCustomClass)
    End Function
    Public Function APGroupSelectionList(ByVal oCustomClass As Parameter.APDisbSelec) As Parameter.APDisbSelec Implements [Interface].IAPDisbSelec.APGroupSelectionList
        Dim DAListAPSele As New SQLEngine.CashMgt.APDisbSelec
        Return DAListAPSele.APGroupSelectionList(oCustomClass)
    End Function

    Public Function SavingToPaymentVoucher(ByVal oCustomClass As Parameter.APDisbSelec) As Parameter.APDisbSelec Implements [Interface].IAPDisbSelec.SavingToPaymentVoucher
        Dim DAListAPSele As New SQLEngine.CashMgt.APDisbSelec
        Return DAListAPSele.SavingToPaymentVoucher(oCustomClass)
    End Function

    Public Sub SavingChangeLoc(ByVal oCustomClass As Parameter.APDisbSelec) Implements [Interface].IAPDisbSelec.SavingChangeLoc
        Dim SaveLoc As New SQLEngine.CashMgt.APDisbSelec
        SaveLoc.SaveChangeLoc(oCustomClass)
    End Sub
    Public Function ListEBanking(ByVal oCustomClass As Parameter.APDisbSelec) As System.Data.DataTable Implements [Interface].IAPDisbSelec.ListEBanking
        Dim DAListAPSele As New SQLEngine.CashMgt.APDisbSelec
        Return DAListAPSele.ListEBanking(oCustomClass)
    End Function
End Class
