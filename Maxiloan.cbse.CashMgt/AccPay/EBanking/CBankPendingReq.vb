﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class CBankPendingReq : Inherits ComponentBase
    Implements IEBankPendingReq
    Public Function GetDetailPay(oCostom As Parameter.PEBankPendingReq) As Parameter.PEBankPendingReq Implements [Interface].IEBankPendingReq.GetDetailPay
        Dim sql As New SQLEngine.CashMgt.QEBankPendingReq
        Return sql.GetDetailPay(oCostom)
    End Function

    Public Function GetListing(oCostom As Parameter.PEBankPendingReq) As Parameter.PEBankPendingReq Implements [Interface].IEBankPendingReq.GetListing
        Dim sql As New SQLEngine.CashMgt.QEBankPendingReq
        Return sql.GetListing(oCostom)
    End Function

    Public Function SaveUpdate(oCostom As Parameter.PEBankPendingReq) As String Implements [Interface].IEBankPendingReq.SaveUpdate
        Dim sql As New SQLEngine.CashMgt.QEBankPendingReq
        Return sql.SaveUpdate(oCostom)
    End Function
End Class
