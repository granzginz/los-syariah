

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class BIReport
    Implements IBIReport
    Public Sub BIReportSave(ByVal strConnection As String, ByVal Tahun As String, ByVal Bulan As String) Implements [Interface].IBIReport.BIReportSave
        Dim DABIReportSave As New SQLEngine.CashMgt.BIReport
        DABIReportSave.BIReportSave(strConnection, Tahun, Bulan)
    End Sub

    Public Function BIReportViewer(ByVal strConnection As String, ByVal Tahun As String, ByVal Bulan As String) As System.Data.DataSet Implements [Interface].IBIReport.BIReportViewer
        Dim DABIReportViewer As New SQLEngine.CashMgt.BIReport
        Return DABIReportViewer.BIReportViewer(strConnection, Tahun, Bulan)
    End Function
    Public Function BIReportSemiAnual(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp Implements [Interface].IBIReport.BIReportSemiAnual
        Dim DA As New SQLEngine.CashMgt.BIReport
        Return DA.BIReportSemiAnual(customclass)
    End Function

End Class
