Imports Maxiloan.SQLEngine.CashMgt
Imports Maxiloan.Interface
Public Class CashMgtPrintImpl
    Implements ICashMgtPrint
    Public Function getInstallmentDueDate(ByVal customClass As Parameter.InstallmentDueDate) As Parameter.InstallmentDueDate Implements [Interface].ICashMgtPrint.getInstallmentDueDate        
        Dim oSQLE As New SQLEngine.CashMgt.CashMgtPrintSQLE
        Return oSQLE.execGetInstallmentDueDate(customClass)
    End Function
    Public Function getInstallmentDueDateCollection(ByVal customClass As Parameter.InstallmentDueDate) As Parameter.InstallmentDueDate Implements [Interface].ICashMgtPrint.getInstallmentDueDateCollection
        Dim oSQLE As New SQLEngine.CashMgt.CashMgtPrintSQLE
        Return oSQLE.execGetInstallmentDueDateCollection(customClass)
    End Function
End Class
