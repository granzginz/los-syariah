﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
#End Region

Public Class AgunanLain : Inherits ComponentBase
    Implements IAgunanLain

    Public Function JenisAgunanList(ByVal co As Parameter.AgunanLain) As Parameter.AgunanLain Implements [Interface].IAgunanLain.JenisAgunanList
        Dim DA As New SQLEngine.AgunanLain.AgunanLain
        Return DA.JenisAgunanList(co)
    End Function

	Public Function JenisAgunanAdd(ByVal co As Parameter.AgunanLain) As String Implements [Interface].IAgunanLain.JenisAgunanAdd
		Try
			Dim DA As New SQLEngine.AgunanLain.AgunanLain
			DA.JenisAgunanAdd(co)
		Catch ex As Exception
			Throw ex
		End Try
	End Function

	Public Function JenisAgunanEdit(ByVal co As Parameter.AgunanLain) As String Implements [Interface].IAgunanLain.JenisAgunanEdit
		Try
			Dim DA As New SQLEngine.AgunanLain.AgunanLain
			DA.JenisAgunanEdit(co)
		Catch ex As Exception
			Throw ex
		End Try
	End Function

	Public Function JenisAgunanDelete(ByVal co As Parameter.AgunanLain) As String Implements [Interface].IAgunanLain.JenisAgunanDelete
		Try
			Dim DA As New SQLEngine.AgunanLain.AgunanLain
			DA.JenisAgunanDelete(co)
		Catch ex As Exception
			Throw ex
		End Try
	End Function

	Public Function ListAgunan(ByVal co As Parameter.AgunanLain) As Parameter.AgunanLain Implements [Interface].IAgunanLain.ListAgunan
        Dim DA As New SQLEngine.AgunanLain.AgunanLain
        Return DA.ListAgunan(co)
    End Function

	Public Function getDataLandBuildidng(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain Implements [Interface].IAgunanLain.getDataLandBuildidng
		Dim DA As New SQLEngine.AgunanLain.AgunanLain
		Return DA.getDataLandBuildidng(oCustomClass)
	End Function

	Public Function getDataInvoice(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain Implements [Interface].IAgunanLain.getDataInvoice
		Dim DA As New SQLEngine.AgunanLain.AgunanLain
		Return DA.getDataInvoice(oCustomClass)
	End Function

	Public Function getDataBPKB(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain Implements [Interface].IAgunanLain.getDataBPKB
		Dim DA As New SQLEngine.AgunanLain.AgunanLain
		Return DA.getDataBPKB(oCustomClass)
	End Function

	Public Function GetCboCollateralType(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain Implements [Interface].IAgunanLain.GetCboCollateralType
		Dim DA As New SQLEngine.AgunanLain.AgunanLain
		Return DA.GetCboCollateralType(oCustomClass)
	End Function

	Public Function getAgunanNett(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain Implements [Interface].IAgunanLain.getAgunanNett
		Dim DA As New SQLEngine.AgunanLain.AgunanLain
		Return DA.getAgunanNett(oCustomClass)
	End Function

	Public Function saveEditDataBPKB(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain Implements [Interface].IAgunanLain.saveEditDataBPKB
		Dim DA As New SQLEngine.AgunanLain.AgunanLain
		Return DA.saveEditDataBPKB(oCustomClass)
	End Function

	Public Function saveAddDataBPKB(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain Implements [Interface].IAgunanLain.saveAddDataBPKB
		Dim DA As New SQLEngine.AgunanLain.AgunanLain
		Return DA.saveAddDataBPKB(oCustomClass)
	End Function

	Public Function saveEditDataInvoice(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain Implements [Interface].IAgunanLain.saveEditDataInvoice
		Dim DA As New SQLEngine.AgunanLain.AgunanLain
		Return DA.SaveEditDataInvoice(oCustomClass)
	End Function

	Public Function saveAddDataInvoice(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain Implements [Interface].IAgunanLain.saveAddDataInvoice
		Dim DA As New SQLEngine.AgunanLain.AgunanLain
		Return DA.SaveAddDataInvoice(oCustomClass)
	End Function

	Public Function saveEditDataLandBuilding(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain Implements [Interface].IAgunanLain.saveEditDataLandBuilding
		Dim DA As New SQLEngine.AgunanLain.AgunanLain
		Return DA.SaveEditDataLandBuilding(oCustomClass)
	End Function

	Public Function saveAddDataLandBuilding(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain Implements [Interface].IAgunanLain.saveAddDataLandBuilding
		Dim DA As New SQLEngine.AgunanLain.AgunanLain
		Return DA.SaveAddDataLandBuilding(oCustomClass)
	End Function
	Public Function GetCollateralId(ByVal oCustomClass As Parameter.AgunanLain) As String Implements [Interface].IAgunanLain.GetCollateralId
		Dim DA As New SQLEngine.AgunanLain.AgunanLain
		Return DA.GetCollateralId(oCustomClass)
	End Function

	Public Function AddCollateral(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain Implements [Interface].IAgunanLain.AddCollateral
		Dim DA As New SQLEngine.AgunanLain.AgunanLain
		Return DA.AddCollateral(oCustomClass)
	End Function

	Public Function PledgingAdd(ByVal co As Parameter.AgunanLain) As String Implements [Interface].IAgunanLain.PledgingAdd
		Try
			Dim DA As New SQLEngine.AgunanLain.AgunanLain
			DA.PledgingAdd(co)
		Catch ex As Exception
			Throw ex
		End Try
	End Function

    Public Function GetDetailPledging(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain Implements [Interface].IAgunanLain.GetDetailPledging
        Dim DA As SQLEngine.AgunanLain.AgunanLain
        Try
            DA = New SQLEngine.AgunanLain.AgunanLain
            Return DA.GetDetailPledging(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
