
Imports Maxiloan.Interface
Imports Maxiloan.Parameter

Public Class ApplicationChanneling : Inherits ComponentBase
    Implements [Interface].IApplicationChanneling

    Public Sub EksekusiDelete(ByVal oCustomClass As Parameter.UploadChanneling) Implements [Interface].IApplicationChanneling.EksekusiDelete
        Dim DA As New SQLEngine.Implementasi.Implementasi
        DA.EksekusiDelete(oCustomClass)
    End Sub
    Public Sub EksekusiSave(ByVal oCustomClass As Parameter.UploadChanneling) Implements [Interface].IApplicationChanneling.EksekusiSave
        Dim DA As New SQLEngine.Implementasi.Implementasi
        DA.EksekusiSave(oCustomClass)
    End Sub
End Class
