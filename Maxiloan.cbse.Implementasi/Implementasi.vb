
Imports Maxiloan.Interface
Imports Maxiloan.Parameter

Public Class Implementasi : Inherits ComponentBase
    Implements [Interface].IImplementasi

    Public Function GetGeneralPaging(CustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging Implements [Interface].IImplementasi.GetGeneralPaging
        Dim DA As New SQLEngine.Implementasi.Implementasi
        Return DA.GetGeneralPaging(CustomClass)
    End Function

    Public Function GetSP(customclass As Parameter.Implementasi) As Parameter.Implementasi Implements [Interface].IImplementasi.GetSP
        Dim DA As New SQLEngine.Implementasi.Implementasi
        Return DA.GetSP(customclass)
    End Function

    Public Sub UpdateOutstandingPokok(oCustomClass As Parameter.Implementasi) Implements [Interface].IImplementasi.UpdateOutstandingPokok
        Dim DA As New SQLEngine.Implementasi.Implementasi
        DA.UpdateOutstandingPokok(oCustomClass)
    End Sub

    Public Sub UpdateBiayaTarik(oCustomClass As Parameter.Implementasi) Implements [Interface].IImplementasi.UpdateBiayaTarik
        Dim DA As New SQLEngine.Implementasi.Implementasi
        DA.UpdateBiayaTarik(oCustomClass)
    End Sub

    Public Sub UpdateNDT(oCustomClass As Parameter.Implementasi) Implements [Interface].IImplementasi.UpdateNDT
        Dim DA As New SQLEngine.Implementasi.Implementasi
        DA.UpdateNDT(oCustomClass)
    End Sub

    Public Function GetPeriodJournal(ByVal customClass As Parameter.Implementasi) As Parameter.Implementasi Implements [Interface].IImplementasi.GetPeriodJournal
        Try
            Dim PRDA As New SQLEngine.Implementasi.Implementasi
            Return PRDA.GetPeriodJournal(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function VaGenerateSettingLog(cnn As String, processDate() As DateTime) As String() Implements [Interface].IImplementasi.VaGenerateSettingLog
        Dim DA As New SQLEngine.Implementasi.AgreementVirtualAccount
        Return DA.VaGenerateSettingLog(cnn, processDate)
    End Function
    Public Function GetVirtualAccount(strConn As String) As DataSet Implements [Interface].IImplementasi.GetVirtualAccount
        Dim DA As New SQLEngine.Implementasi.AgreementVirtualAccount
        Return DA.GetVirtualAccount(strConn)
    End Function

    Public Function GenerateVitualAccount(ByVal cnn As String, vabankid As IList(Of String), ByVal oDataTable As DataTable) As IList(Of Parameter.GeneratedVaToExcellLog) Implements [Interface].IImplementasi.GenerateVitualAccount
        Dim DA As New SQLEngine.Implementasi.AgreementVirtualAccount
        Return DA.GenerateVitualAccount(cnn, vabankid, oDataTable)
    End Function

    Public Function GetVirtualAccountInsAgreementLog(cnn As String, processId As String) As Parameter.VirtualAccountInsAgreementLog Implements [Interface].IImplementasi.GetVirtualAccountInsAgreementLog
        Dim DA As New SQLEngine.Implementasi.AgreementVirtualAccount
        Return DA.GetVirtualAccountInsAgreementLog(cnn, processId)
    End Function
    Public Function GeneratePaymentGatewayFile(cnn As String, processid As String, dir As String, UploadDate As Date) As Boolean Implements [Interface].IImplementasi.GeneratePaymentGatewayFile

        Dim _installRcv3rd As New SQLEngine.Implementasi.AgreementVirtualAccount
        Dim result = _installRcv3rd.GeneratePaymentGatewayFile(cnn, processid)
        result.FileDirectory = dir
        result.UploadDate = UploadDate
        Try
            result.BuildUploadFiles()
            _installRcv3rd.PaymentGatewayAgreementLog(cnn, result)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetBankAccountCabang(oCustomClass As Parameter.Implementasi) As Parameter.Implementasi Implements IImplementasi.GetBankAccountCabang
        Dim PRDA As New SQLEngine.Implementasi.Implementasi
        Return PRDA.GetBankAccountCabang(oCustomClass)
    End Function
    Public Function GetBankAccountFunding(ByVal strConnection As String) As DataTable Implements [Interface].IImplementasi.GetBankAccountFunding
        Dim DA As New SQLEngine.Implementasi.Implementasi
        Return DA.GetBankAccountFunding(strConnection)
    End Function
    Public Function GetFundingCoyID(ByVal strConnection As String, FundingCoyID As String) As DataTable Implements [Interface].IImplementasi.GetFundingCoyID
        Dim DA As New SQLEngine.Implementasi.Implementasi
        Return DA.GetFundingCoyID(strConnection, FundingCoyID)
    End Function
    Public Function GetFundingBatch(ByVal strConnection As String, FundingContractNo As String) As DataTable Implements [Interface].IImplementasi.GetFundingBatch
        Dim DA As New SQLEngine.Implementasi.Implementasi
        Return DA.GetFundingBatch(strConnection, FundingContractNo)
    End Function

    Public Sub SavePembayaranTDP(oCustomClass As Parameter.Implementasi) Implements IImplementasi.SavePembayaranTDP
        Dim da As New SQLEngine.Implementasi.Implementasi
        da.SavePembayaranTDP(oCustomClass)
    End Sub
    Public Function GetBankAccountHOTransfer(strConnection As String, BankID As String) As DataTable Implements IImplementasi.GetBankAccountHOTransfer
        Dim sqlBankHO As New SQLEngine.Implementasi.Implementasi
        Return sqlBankHO.GetBankAccountHOTransfer(strConnection, BankID)
    End Function

    Public Sub SaveTerimaTDP(ByVal customclass As Parameter.TerimaTDP) Implements [Interface].IImplementasi.SaveTerimaTDP
        Dim DASaveTerimaTDP As New SQLEngine.Implementasi.Implementasi
        DASaveTerimaTDP.SaveTerimaTDP(customclass)
    End Sub
    Public Sub SaveOtorisasiTDP(ByVal oEntities As Parameter.TerimaTDP) Implements [Interface].IImplementasi.SaveOtorisasiTDP
        Dim DASaveOtorisasiTDP As New SQLEngine.Implementasi.Implementasi
        DASaveOtorisasiTDP.SaveOtorisasiTDP(oEntities)
    End Sub
    Public Function TDPReversalList(ByVal oCustomClass As Parameter.TerimaTDP) As Parameter.TerimaTDP Implements [Interface].IImplementasi.TDPReversalList
        Dim DATDPReversalList As New SQLEngine.Implementasi.Implementasi
        Return DATDPReversalList.TDPReversalList(oCustomClass)
    End Function
    Public Function ReverseTDP(ByVal oCustomClass As Parameter.TerimaTDP) As Parameter.TerimaTDP Implements [Interface].IImplementasi.ReverseTDP
        Dim DAReverseTDP As New SQLEngine.Implementasi.Implementasi
        Return DAReverseTDP.ReverseTDP(oCustomClass)
    End Function
    Public Sub SaveTDPReverse(ByVal customclass As Parameter.TerimaTDP) Implements [Interface].IImplementasi.SaveTDPReverse
        Dim DASaveTDPReverse As New SQLEngine.Implementasi.Implementasi
        DASaveTDPReverse.SaveTDPReverse(customclass)
    End Sub
    Public Sub TDPSplit(oCustomClass As Parameter.TerimaTDP) Implements [Interface].IImplementasi.TDPSplit
        Dim DATDPAllocation As New SQLEngine.Implementasi.Implementasi
        DATDPAllocation.TDPSplit(oCustomClass)
    End Sub


    Public Function PaymentRequestFAList(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset Implements [Interface].IImplementasi.PaymentRequestFixedAssetList
		Dim DAPaymentRequestFAList As New SQLEngine.Implementasi.Implementasi
		Return DAPaymentRequestFAList.PaymentRequestFAList(oCustomClass)
	End Function

	Public Function PaymentRequestFAOtor(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset Implements [Interface].IImplementasi.PaymentRequestFixedAssetListOtor
		Dim DAPaymentRequestFAOtor As New SQLEngine.Implementasi.Implementasi
		Return DAPaymentRequestFAOtor.PaymentRequestFAOtor(oCustomClass)
	End Function

	Public Function PaymentRequestFAOtorSave(ByVal oCustomclass As Parameter.TransFixedAsset) Implements [Interface].IImplementasi.PaymentRequestFixedAssetOtorSave
		Dim DAPaymentRequestFAOtorSave As New SQLEngine.Implementasi.Implementasi
		Return DAPaymentRequestFAOtorSave.PaymentRequestFAOtorSave(oCustomclass)
	End Function

	Public Function PaymentRequestFASave(ByVal oCustomclass As Parameter.TransFixedAsset) Implements [Interface].IImplementasi.PaymentRequestFixedAssetSave
		Dim DAPaymentRequestFASave As New SQLEngine.Implementasi.Implementasi
		Return DAPaymentRequestFASave.PaymentRequestFASave(oCustomclass)
	End Function

	Public Function RequestReceiveFAList(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset Implements [Interface].IImplementasi.RequestReceiveFixedAssetList
		Dim DARequestReceiveFAList As New SQLEngine.Implementasi.Implementasi
		Return DARequestReceiveFAList.RequestReceiveFAList(oCustomClass)
	End Function

	Public Function RequestReceiveFAOtor(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset Implements [Interface].IImplementasi.RequestReceiveFixedAssetListOtor
		Dim DARequestReceiveFAOtor As New SQLEngine.Implementasi.Implementasi
		Return DARequestReceiveFAOtor.RequestReceiveFAOtor(oCustomClass)
	End Function

	Public Function RequestReceiveFAOtorSave(ByVal oCustomclass As Parameter.TransFixedAsset) Implements [Interface].IImplementasi.RequestReceiveFixedAssetOtorSave
		Dim DARequestReceiveFAOtorSave As New SQLEngine.Implementasi.Implementasi
		Return DARequestReceiveFAOtorSave.RequestReceiveFAOtorSave(oCustomclass)
	End Function

    Public Function RequestReceiveFASave(ByVal oCustomclass As Parameter.TransFixedAsset) Implements [Interface].IImplementasi.RequestReceiveFixedAssetSave
        Dim DARequestReceiveFASave As New SQLEngine.Implementasi.Implementasi
        Return DARequestReceiveFASave.RequestReceiveFASave(oCustomclass)
    End Function

    Public Function PenjualanFAList(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset Implements [Interface].IImplementasi.PenjualanFixedAssetList
        Dim DAPaymentRequestFAList As New SQLEngine.Implementasi.Implementasi
        Return DAPaymentRequestFAList.PenjualanFAList(oCustomClass)
    End Function

    Public Function PenjualanFixedAssetListOtor(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset Implements [Interface].IImplementasi.PenjualanFixedAssetListOtor
        Dim DAPaymentRequestFAOtor As New SQLEngine.Implementasi.Implementasi
        Return DAPaymentRequestFAOtor.PenjualanFixedAssetListOtor(oCustomClass)
    End Function

    Public Function PenjualanFAOtorSave(ByVal oCustomclass As Parameter.TransFixedAsset) Implements [Interface].IImplementasi.PenjualanFixedAssetOtorSave
        Dim DAPaymentRequestFAOtorSave As New SQLEngine.Implementasi.Implementasi
        Return DAPaymentRequestFAOtorSave.PenjualanFAOtorSave(oCustomclass)
    End Function

    Public Function PenjualanFASave(ByVal oCustomclass As Parameter.TransFixedAsset) Implements [Interface].IImplementasi.PenjualanFixedAssetSave
        Dim DAPaymentRequestFASave As New SQLEngine.Implementasi.Implementasi
        Return DAPaymentRequestFASave.PenjualanFASave(oCustomclass)
    End Function

    Public Function PenghapusanFAList(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset Implements [Interface].IImplementasi.PenghapusanFixedAssetList
        Dim DAPaymentRequestFAList As New SQLEngine.Implementasi.Implementasi
        Return DAPaymentRequestFAList.PenghapusanFAList(oCustomClass)
    End Function

    Public Function PenghapusanFAOtor(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset Implements [Interface].IImplementasi.PenghapusanFixedAssetListOtor
        Dim DAPaymentRequestFAOtor As New SQLEngine.Implementasi.Implementasi
        Return DAPaymentRequestFAOtor.PenghapusanFAOtor(oCustomClass)
    End Function

    Public Function PenghapusanFAOtorSave(ByVal oCustomclass As Parameter.TransFixedAsset) Implements [Interface].IImplementasi.PenghapusanFixedAssetOtorSave
        Dim DAPaymentRequestFAOtorSave As New SQLEngine.Implementasi.Implementasi
        Return DAPaymentRequestFAOtorSave.PenghapusanFAOtorSave(oCustomclass)
    End Function

    Public Function PenghapusanFASave(ByVal oCustomclass As Parameter.TransFixedAsset) Implements [Interface].IImplementasi.PenghapusanFixedAssetSave
        Dim DAPaymentRequestFASave As New SQLEngine.Implementasi.Implementasi
        Return DAPaymentRequestFASave.PenghapusanFASave(oCustomclass)
    End Function


    Public Function PaymentRequestAgreementUnitExpenseList(ByVal oCustomClass As Parameter.AgreementUnitExpense) As Parameter.AgreementUnitExpense Implements [Interface].IImplementasi.PaymentRequestAgreementUnitExpenseList
        Dim DAPaymentRequestAgreementUnitExpenseList As New SQLEngine.Implementasi.Implementasi
        Return DAPaymentRequestAgreementUnitExpenseList.PaymentRequestAgreementUnitExpenseList(oCustomClass)
    End Function

    'Public Function CetakKartuPiutangPaging(ByVal oCustomClass As Parameter.CetakKartuPiutang) As Parameter.CetakKartuPiutang Implements [Interface].IImplementasi.CetakKartuPiutangPaging
    '    Dim DA As New SQLEngine.Implementasi.Implementasi
    '    Return DA.CetakKartuPiutangPaging(oCustomClass)
    'End Function
End Class
