
Imports Maxiloan.Interface
Imports Maxiloan.Parameter

Public Class TransaksiTDP : Inherits ComponentBase
    Implements [Interface].ITransaksiTDP

    Public Sub SaveTerimaTDP(ByVal customclass As Parameter.TerimaTDP) Implements [Interface].ITransaksiTDP.SaveTerimaTDP
        Dim DASaveTerimaTDP As New SQLEngine.Implementasi.Implementasi
        DASaveTerimaTDP.SaveTerimaTDP(customclass)
    End Sub
    Public Sub SaveOtorisasiTDP(ByVal oEntities As Parameter.TerimaTDP) Implements [Interface].ITransaksiTDP.SaveOtorisasiTDP
        Dim DASaveOtorisasiTDP As New SQLEngine.Implementasi.Implementasi
        DASaveOtorisasiTDP.SaveOtorisasiTDP(oEntities)
    End Sub
    Public Function TDPReversalList(ByVal oCustomClass As Parameter.TerimaTDP) As Parameter.TerimaTDP Implements [Interface].ITransaksiTDP.TDPReversalList
        Dim DATDPReversalList As New SQLEngine.Implementasi.Implementasi
        Return DATDPReversalList.TDPReversalList(oCustomClass)
    End Function
    Public Function ReverseTDP(ByVal oCustomClass As Parameter.TerimaTDP) As Parameter.TerimaTDP Implements [Interface].ITransaksiTDP.ReverseTDP
        Dim DAReverseTDP As New SQLEngine.Implementasi.Implementasi
        DAReverseTDP.ReverseTDP(oCustomClass)
    End Function
End Class
