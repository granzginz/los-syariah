﻿Imports Maxiloan.Interface

Public Class SimulasiPerhitunganPembiayaan : Inherits ComponentBase
    Implements ISimulasiPerhitunganPembiayaan

    Public Function SimulasiPerhitunganPembiayaanPaging(ByVal oCustomClass As Parameter.SimulasiPerhitunganPembiayaan) As Parameter.SimulasiPerhitunganPembiayaan Implements [Interface].ISimulasiPerhitunganPembiayaan.SimulasiPerhitunganPembiayaanPaging
        Dim DA As New SQLEngine.Implementasi.SimulasiPerhitunganPembiayaan
        Return DA.SimulasiPerhitunganPembiayaanPaging(oCustomClass)
    End Function
    Public Function GenerateSimulasiPerhitunganPembiayaan(ByVal oCustomClass As Parameter.SimulasiPerhitunganPembiayaan) As Parameter.SimulasiPerhitunganPembiayaan Implements [Interface].ISimulasiPerhitunganPembiayaan.GenerateSimulasiPerhitunganPembiayaan
        Dim DA As New SQLEngine.Implementasi.SimulasiPerhitunganPembiayaan
        Return DA.GenerateSimulasiPerhitunganPembiayaan(oCustomClass)
    End Function

    Public Function CetakKartuPiutangList(ByVal oCustomClass As Parameter.CetakKartuPiutang) As Parameter.CetakKartuPiutang Implements [Interface].ISimulasiPerhitunganPembiayaan.CetakKartuPiutangList
        Dim DA As New SQLEngine.Implementasi.SimulasiPerhitunganPembiayaan
        Return DA.CetakKartuPiutangList(oCustomClass)
    End Function
End Class
