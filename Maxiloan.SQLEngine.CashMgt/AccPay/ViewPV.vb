

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class ViewPV : Inherits DataAccessBase

    Public Function ListViewPV(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            customclass.listview = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spViewPV", params).Tables(0)

            Return customclass
        Catch exp As Exception
            WriteException("ViewPV", "ListViewPV", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListView(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@PVNo", SqlDbType.VarChar, 20)
            params(0).Value = customclass.PaymentVoucherNo

            params(1) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spViewPVList", params)

            If objread.Read Then
                With customclass

                    .BankNameTo = CType(objread("banknameto"), String)
                    .BankBranchTo = CType(objread("bankbranchto"), String)
                    .AccountNameTo = CType(objread("accountnameto"), String)
                    .AccountNoTo = CType(objread("accountnoto"), String)
                    .APType = CType(objread("APType"), String)
                    .PaymentVoucherNo = CType(objread("paymentvoucherno"), String)
                    .BGNo = CType(objread("bgno"), String)
                    If .BGNo <> "" And .BGNo <> "-" Then
                        .BGDueDate = CType(objread("bgduedate"), Date)
                    End If
                    .recipientname = CType(objread("recipientname"), String)
                    .PaymentNotes = CType(objread("paymentnotes"), String)
                    .APType = CType(objread("APTypedesc"), String)
                    .PaymentVoucherDate = CType(objread("paymentvoucherDate"), Date)
                    .BankAccountTo = CType(objread("bankaccountto"), String)
                    .APTo = CType(objread("APTO"), String)
                    .pvduedate = CType(objread("PVDueDate"), Date)
                    .Status = CType(objread("pvstatusdesc"), String)
                    .StatusDate = CType(objread("statusdate"), Date)
                    .RequestBy = CType(objread("requestby"), String)
                    .ApprovalBy = CType(objread("approvalby"), String)
                    .ApprovalDate = CType(objread("approvaldate"), Date)
                    .ApprovalNotes = CType(objread("approvalnotes"), String)
                    .PaymentTypeID = CType(objread("paymnettypedesc"), String)

                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("ViewPV", "ListView", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
