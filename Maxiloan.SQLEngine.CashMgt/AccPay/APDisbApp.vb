

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class APDisbApp : Inherits DataAccessBase
    Private Const spAPDisbApp As String = "spAPDisbApp"
    Private Const spAPDisbAppInformasi As String = "spAPDisbAppInformasi"
    Private Const spAPDisbAppUpdate As String = "spAPDisbAppUpdate"
    Private Const spAPDisbCashUpdate As String = "spAPDisbCashUpdate"
    Private Const spAPDisbApp2 As String = "spAPDisbApp2_"
    Private Const spAPDisbApp3 As String = "spAPDisbApp3"
    Private Const spAPDisbApp4 As String = "spAPDisbApp4"
    Private Const spAPDisbAppUpdate2 As String = "spAPDisbAppUpdate2"
    Private Const spAPDisbAppConfirmUpdate As String = "spAPDisbAppConfirmUpdate"
    Private Const spMemoPembayaran As String = "spMemoPembayaran"
    Private Const spMemoPembayaranDetail1Report As String = "spMemoPembayaranDetail1Report"
    Private Const spMemoPembayaranDetail2Report As String = "spMemoPembayaranDetail2Report"
    Private m_connection As SqlConnection

    Private Function buildPageParam(ByVal customclass As Parameter.APDisbApp) As IList(Of SqlParameter)
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@CurrentPage", SqlDbType.Int) With {.Value = customclass.CurrentPage})
        params.Add(New SqlParameter("@PageSize", SqlDbType.Int) With {.Value = customclass.PageSize})
        params.Add(New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000) With {.Value = customclass.WhereCond})
        params.Add(New SqlParameter("@SortBy", SqlDbType.VarChar, 100) With {.Value = customclass.SortBy})
        Return params
    End Function

    Public Function ListApOtorTrans(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
        Try
            Dim params As IList(Of SqlParameter) = buildPageParam(customclass)
            Dim paramOut = New SqlParameter("@TotalRecords", SqlDbType.SmallInt) With {.Direction = ParameterDirection.Output}
            params.Add(paramOut)
            customclass.ListAPAppr = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spAPOtorisasiTrans", params.ToArray).Tables(0)
            customclass.TotalRecord = CInt(paramOut.Value)
        Catch exp As Exception
            WriteException("APDisbApp", "ListApOtorTrans", exp.Message + exp.StackTrace)
        End Try
        Return customclass
    End Function

    Public Function ListAPDisbApp(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp

        Dim params As IList(Of SqlParameter) = buildPageParam(customclass)
        Try
            params.Add(New SqlParameter("@Flags", SqlDbType.VarChar, 100) With {.Value = customclass.Flags})
            Dim paramOut = New SqlParameter("@TotalRecords", SqlDbType.SmallInt) With {.Direction = ParameterDirection.Output}
            params.Add(paramOut)
            customclass.ListAPAppr = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAPDisbApp, params.ToArray).Tables(0)
            customclass.TotalRecord = CInt(paramOut.Value)
        Catch exp As Exception
            WriteException("APDisbApp", "ListAPDisbApp", exp.Message + exp.StackTrace)
        End Try
        Return customclass

        'params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        'params(0).Value = customclass.CurrentPage

        'params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        'params(1).Value = customclass.PageSize

        'params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        'params(2).Value = customclass.WhereCond

        'params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        'params(3).Value = customclass.SortBy

        'params(4) = New SqlParameter("@Flags", SqlDbType.VarChar, 100)
        'params(4).Value = customclass.Flags

        '    params.Add(New SqlParameter("@Flags", SqlDbType.VarChar, 100) With {.Value = customclass.Flags})
        '    Dim paramOut = New SqlParameter("@TotalRecords", SqlDbType.SmallInt) With {.Direction = ParameterDirection.Output}
        '    params.Add(paramOut)
        '    customclass.ListAPAppr = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAPDisbApp, params.ToArray).Tables(0)
        '    customclass.TotalRecord = CInt(paramOut.Value) 
        'Catch exp As Exception
        '    WriteException("APDisbApp", "ListAPDisbApp", exp.Message + exp.StackTrace)
        'End Try
        'Return customclass
    End Function

    Public Function InformasiAPDisbApp(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@PaymentVoucherNo", SqlDbType.Char)
            params(0).Value = customclass.PaymentVoucherNo

            params(1) = New SqlParameter("@BranchID", SqlDbType.Char)
            params(1).Value = customclass.BranchId

            customclass.ListAPAppr = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAPDisbAppInformasi, params).Tables(0)

            Return customclass
        Catch exp As Exception
            WriteException("APDisbApp", "InformasiAPDisbApp", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function UpdateMultiAPDisbApp(cnn As String, status As String, branchId As String, datas As IList(Of Parameter.ValueTextObject), ByVal ParamArray bankSelected() As String) As String
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@BranchID", SqlDbType.VarChar, 3) With {.Value = branchId})
        params.Add(New SqlParameter("@PVStatus", SqlDbType.VarChar, 3) With {.Value = status})
        params.Add(New SqlParameter("@typ", SqlDbType.Structured) With {.Value = ToDataTable(datas)})

        If (status.ToUpper = "B") Then
            'BANKSELECTED() ==>  0 = PaymentTypeId 1 = PaidBankAccountId 2 = PostingDate
            params.Add(New SqlParameter("@PaymentTypeId", SqlDbType.VarChar, 3) With {.Value = bankSelected(0)})
            params.Add(New SqlParameter("@PaidBankAccountId", SqlDbType.VarChar, 10) With {.Value = bankSelected(1)})
            'modify ario 12-02-2019 
            'params.Add(New SqlParameter("@BankPostDate", SqlDbType.SmallDateTime, 4) With {.Value = CType(bankSelected(2), Date)})

        End If


        Dim errPrm = New SqlParameter("@strerror", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
        params.Add(errPrm)
        Dim objconnection As New SqlConnection(cnn)
        Dim objtransaction As SqlTransaction = Nothing
        Dim ret As String = ""
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objtransaction = objconnection.BeginTransaction
            SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, "spAPDisbAppMultipleUpdate", params.ToArray)
            objtransaction.Commit()
            ret = CStr(errPrm.Value)
        Catch exp As Exception
            objtransaction.Rollback()
            ret = exp.Message
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
        Return ret
    End Function



    Public Function MemoPembayaranReport(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp

        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@PaymentVoucherNo", SqlDbType.Char)
            params(0).Value = customclass.PaymentVoucherNo
            customclass.ListAPReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMemoPembayaran, params)

            Return customclass
        Catch exp As Exception
            WriteException("APDisbApp", "spMemoPembayaran", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function MemoPembayaranDetail1Report(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp

        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond
            customclass.ListAPReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMemoPembayaranDetail1Report, params)

            Return customclass
        Catch exp As Exception
            WriteException("APDisbApp", "spMemoPembayaranDetail1Report", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function MemoPembayaranDetail2Report(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp

        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond1
            customclass.ListAPReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMemoPembayaranDetail2Report, params)

            Return customclass
        Catch exp As Exception
            WriteException("APDisbApp", "spMemoPembayaranDetail2Report", exp.Message + exp.StackTrace)
        End Try
    End Function

    Function UpdateAPDisbAppConfirm(ByVal CustomClass As Parameter.APDisbApp) As String
        Dim objconnection As New SqlConnection(CustomClass.strConnection)
        Dim objtransaction As SqlTransaction = Nothing
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim strApprovalNo As String

        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objtransaction = objconnection.BeginTransaction
            With oEntitiesApproval
                .ApprovalTransaction = objtransaction
                .BranchId = CustomClass.BranchId
                .SchemeID = CustomClass.ApprovalSchemaId
                .RequestDate = CustomClass.BusinessDate
                .TransactionNo = CustomClass.PaymentVoucherNo
                .ApprovalValue = CustomClass.NilaiApproval
                .UserRequest = CustomClass.RequestTo
                .UserApproval = CustomClass.approvalBy
                .ApprovalNote = CustomClass.Notes
                .AprovalType = Parameter.Approval.ETransactionType.Disbursement_Approval
            End With
            strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)
            params.Add(New SqlParameter("@BranchID", SqlDbType.VarChar, 3) With {.Value = CustomClass.BranchId})
            params.Add(New SqlParameter("@PaymentVoucherNo", SqlDbType.VarChar, 20) With {.Value = CustomClass.PaymentVoucherNo})
            'params.Add(New SqlParameter("@PaymentNotes", SqlDbType.VarChar, 50) With {.Value = CustomClass.Notes})
            'buat text
            params.Add(New SqlParameter("@PaymentNotes", SqlDbType.Text) With {.Value = CustomClass.Notes})
            params.Add(New SqlParameter("@ApprovalBy", SqlDbType.VarChar, 30) With {.Value = CustomClass.approvalBy})
            params.Add(New SqlParameter("@ApprovalDate", SqlDbType.SmallDateTime, 4) With {.Value = CustomClass.BusinessDate})


            Dim strerror = New SqlParameter("@strerror", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            params.Add(strerror)
            SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, spAPDisbAppConfirmUpdate, params.ToArray())

            Dim params2() As SqlParameter = New SqlParameter(4) {}
            params2(0) = New SqlParameter("BranchID", SqlDbType.VarChar, 3)
            params2(1) = New SqlParameter("PaymentVoucherNO", SqlDbType.VarChar, 20)
            params2(2) = New SqlParameter("AccountPayableNo", SqlDbType.VarChar, 20)
            params2(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params2(4) = New SqlParameter("PaymentAmount", SqlDbType.Decimal)
            If CustomClass.isVerifikasiHO = True Then
                For i = 0 To CustomClass.APDetail.Rows.Count - 1
                    params2(0).Value = CustomClass.BranchId
                    params2(1).Value = CustomClass.PaymentVoucherNo
                    params2(2).Value = CustomClass.APDetail.Rows(i).Item("AccountPayableNo")
                    params2(3).Value = CustomClass.BusinessDate
                    params2(4).Value = CustomClass.APDetail.Rows(i).Item("PaymentAmount")
                    SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, "spAPDisbAppUpdateHO", params2)
                Next
            End If

            objtransaction.Commit()

            Return CStr(strerror.Value)
        Catch exp As Exception
            objtransaction.Rollback()
            WriteException("APDisbApp", "UpdateAPDisbApp", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update AP Disburse")
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try


    End Function
    Function ToDataTable(datas As IList(Of Parameter.ValueTextObject)) As DataTable
        Dim dt = New DataTable()
        dt.Columns.Add("id", GetType(String))
        dt.Columns.Add("value1", GetType(String))
        dt.Columns.Add("value2", GetType(String))
        For Each v In datas
            Dim row = dt.NewRow()
            row("id") = v.Value
            row("value1") = v.Text
            row("value2") = v.ExtText
            dt.Rows.Add(row)
        Next
        Return dt
    End Function
    Public Function UpdateMultiAPDisbApp2(ByVal CustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(CustomClass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(3) {}
            oDataTable = CustomClass.ListAPAppr
            For intloop = 0 To oDataTable.Rows.Count - 1


                params(0) = New SqlParameter("@PaymentVoucherNo", SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("PaymentVoucherNo")
                params(1) = New SqlParameter("@GM1", SqlDbType.Char, 50)
                params(1).Value = oDataTable.Rows(intloop)("GM1")
                params(2) = New SqlParameter("@GM2", SqlDbType.Char, 50)
                params(2).Value = oDataTable.Rows(intloop)("GM2")
                params(3) = New SqlParameter("@Direksi", SqlDbType.Char, 50)
                params(3).Value = oDataTable.Rows(intloop)("Direksi")

                CustomClass.ListAPAppr = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, "spAPDisbAppMultipleUpdate_22", params).Tables(0)
                Return CustomClass
                'SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, "spAPDisbAppMultipleUpdate_22", params)
            Next
            oTrans.Commit()
            CustomClass.hasil = 1
            Return CustomClass
        Catch ex As Exception
            oTrans.Rollback()
            CustomClass.hasil = 0
            Return CustomClass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function


    Public Function UpdateAPDisbApp(ByVal CustomClass As Parameter.APDisbApp) As String


        If CustomClass.UpdateMode = "C" Then
            Return UpdateAPDisbAppConfirm(CustomClass)
        End If

        Dim objconnection As New SqlConnection(CustomClass.strConnection)
        Dim objtransaction As SqlTransaction = Nothing
        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objtransaction = objconnection.BeginTransaction
        Try

            Dim params() As SqlParameter = New SqlParameter(7) {}

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = CustomClass.BranchId

            params(1) = New SqlParameter("@PaymentVoucherNo", SqlDbType.VarChar, 20)
            params(1).Value = CustomClass.PaymentVoucherNo

            params(2) = New SqlParameter("@PVStatus", SqlDbType.VarChar, 1)
            params(2).Value = CustomClass.PVStatus

            'params(3) = New SqlParameter("@PaymentNotes", SqlDbType.VarChar, 50)
            'params(3).Value = CustomClass.Notes
            params(3) = New SqlParameter("@PaymentNotes", SqlDbType.Text)
            params(3).Value = CustomClass.Notes

            params(4) = New SqlParameter("@ApprovalBy", SqlDbType.VarChar, 30)
            params(4).Value = CustomClass.LoginId

            params(5) = New SqlParameter("@ApprovalDate", SqlDbType.SmallDateTime, 4)
            params(5).Value = CustomClass.BusinessDate

            params(6) = New SqlParameter("@UpdateMode", SqlDbType.VarChar, 1)
            params(6).Value = CustomClass.UpdateMode

            params(7) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
            params(7).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, spAPDisbAppUpdate, params)

            objtransaction.Commit()
            Return CStr(params(7).Value)
        Catch exp As Exception
            objtransaction.Rollback()
            WriteException("APDisbApp", "UpdateAPDisbApp", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update AP Disburse")
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
    End Function

    Public Function UpdateAPDisbApp2(ByVal CustomClass As Parameter.APDisbApp) As String
        Dim objconnection As New SqlConnection(CustomClass.strConnection)
        Dim objtransaction As SqlTransaction = Nothing
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objtransaction = objconnection.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = CustomClass.BranchId

            params(1) = New SqlParameter("@PaymentVoucherNo", SqlDbType.Structured)
            params(1).Value = CustomClass.paymentvoucherno_DT

            params(2) = New SqlParameter("@PVStatus", SqlDbType.VarChar, 1)
            params(2).Value = CustomClass.PVStatus

            'params(3) = New SqlParameter("@PaymentNotes", SqlDbType.VarChar, 50)
            'params(3).Value = CustomClass.Notes
            params(3) = New SqlParameter("@PaymentNotes", SqlDbType.Text)
            params(3).Value = CustomClass.Notes

            params(4) = New SqlParameter("@ApprovalBy", SqlDbType.VarChar, 30)
            params(4).Value = CustomClass.LoginId

            params(5) = New SqlParameter("@ApprovalDate", SqlDbType.SmallDateTime, 4)
            params(5).Value = CustomClass.BusinessDate

            params(6) = New SqlParameter("@UpdateMode", SqlDbType.VarChar, 1)
            params(6).Value = CustomClass.UpdateMode

            params(7) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
            params(7).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, spAPDisbAppUpdate2, params)
            objtransaction.Commit()
            Return CStr(params(7).Value)
        Catch exp As Exception
            objtransaction.Rollback()
            WriteException("APDisbApp", "UpdateAPDisbApp", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Update AP Disburse")
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
    End Function

    Public Function ListAPDisbApp3(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@Flags", SqlDbType.VarChar, 100)
            params(4).Value = customclass.Flags

            params(5) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output

            params(6) = New SqlParameter("@WhereCond1", SqlDbType.VarChar, 8000)
            params(6).Value = customclass.WhereCond1

            params(7) = New SqlParameter("@WhereCond2", SqlDbType.VarChar, 8000)
            params(7).Value = customclass.WhereCond2

            customclass.ListAPAppr = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAPDisbApp3, params).Tables(0)
            customclass.TotalRecord = CInt(params(5).Value)
            Return customclass
        Catch exp As Exception
            WriteException("APDisbApp", "ListAPDisbApp", exp.Message + exp.StackTrace)
        End Try
        Return Nothing
    End Function

    'for EbankPreparation
    Public Function ListAPDisbApp4(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@Flags", SqlDbType.VarChar, 100)
            params(4).Value = customclass.Flags

            params(5) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output

            params(6) = New SqlParameter("@WhereCond1", SqlDbType.VarChar, 8000)
            params(6).Value = customclass.WhereCond1

            params(7) = New SqlParameter("@SortBy1", SqlDbType.VarChar, 100)
            params(7).Value = customclass.SortBy1


            customclass.ListAPAppr = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAPDisbApp4, params).Tables(0)
            customclass.TotalRecord = CInt(params(5).Value)
            Return customclass
        Catch exp As Exception
            WriteException("APDisbApp", "ListAPDisbApp", exp.Message + exp.StackTrace)
        End Try
        Return Nothing
    End Function

    Public Function ListAPDisbApp2(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@Flags", SqlDbType.VarChar, 100)
            params(4).Value = customclass.Flags

            params(5) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output


            customclass.ListAPAppr = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAPDisbApp2, params).Tables(0)
            customclass.TotalRecord = CInt(params(5).Value)
            Return customclass
        Catch exp As Exception
            WriteException("APDisbApp", "ListAPDisbApp", exp.Message + exp.StackTrace)
        End Try
        Return Nothing
    End Function


#Region "APDisbCash"

    Public Sub UpdateApDisbTransBeforeDisburse(ByVal CustomClass As Parameter.APDisbApp)

        Dim params As New List(Of SqlParameter)
        Try
            params.Add(New SqlParameter("@BranchID", SqlDbType.VarChar, 3) With {.Value = CustomClass.BranchId})
            params.Add(New SqlParameter("@PaymentVoucherNo", SqlDbType.VarChar, 20) With {.Value = CustomClass.PaymentVoucherNo})
            params.Add(New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10) With {.Value = CustomClass.BankAccountID})
            params.Add(New SqlParameter("@BGNo", SqlDbType.VarChar, 20) With {.Value = CustomClass.BGNo})

            params.Add(New SqlParameter("@RecipientName", SqlDbType.VarChar, 30) With {.Value = CustomClass.RecipientName})
            'params.Add(New SqlParameter("@PaymentNotes", SqlDbType.VarChar, 50) With {.Value = CustomClass.Notes})
            params.Add(New SqlParameter("@PaymentNotes", SqlDbType.Text) With {.Value = CustomClass.Notes})
            params.Add(New SqlParameter("@WOP", SqlDbType.VarChar, 3) With {.Value = CustomClass.WOP})
            params.Add(New SqlParameter("@BusinessDate", SqlDbType.DateTime) With {.Value = CustomClass.BusinessDate})
            params.Add(New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20) With {.Value = CustomClass.ReferenceNo})
            params.Add(New SqlParameter("@UserInput", SqlDbType.VarChar, 30) With {.Value = CustomClass.LoginId})
            params.Add(New SqlParameter("@DateInput", SqlDbType.SmallDateTime, 4) With {.Value = CustomClass.BusinessDate})


            If CustomClass.BGDueDate <> CDate("1/1/1900") Then
                params.Add(New SqlParameter("@BGDueDate", SqlDbType.DateTime) With {.Value = CustomClass.BGDueDate})
            End If

            Dim errPrm = New SqlParameter("@errMsg", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            params.Add(errPrm)

            SqlHelper.ExecuteNonQuery(CustomClass.strConnection, CommandType.StoredProcedure, "spApDisbTransBeforeDisburseUpdate", params.ToArray)
            Dim Err = CType(errPrm.Value, String)

            If (Err <> "OK") Then
                Throw New Exception("Gagal Execute spApDisbTransBeforeDisburseUpdate " + Err)
            End If

        Catch exp As Exception

            WriteException("ABM", "UpdateApDisbTransBeforeDisburse", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)


        End Try
    End Sub

    Private Function buildTable(data As IList(Of String)) As DataTable
        Dim dt = New DataTable()
        dt.Columns.Add("id", GetType(String))
        dt.Columns.Add("value1", GetType(String))
        dt.Columns.Add("value2", GetType(String))

        For Each v In data
            Dim row = dt.NewRow()
            row("id") = v
            row("value1") = v
            row("value1") = v
            dt.Rows.Add(row)
        Next

        Return dt
    End Function


    Public Sub UpdateAPDisbCash(ByVal CustomClass As Parameter.APDisbApp)
        Dim params As New List(Of SqlParameter)
        Try
            params.Add(New SqlParameter("@pvNo", SqlDbType.Structured) With {.Value = buildTable(CustomClass.OtorPvNoList)})
            params.Add(New SqlParameter("@type", SqlDbType.VarChar, 2) With {.Value = CustomClass.Flag})

            Dim errPrm = New SqlParameter("@errMsg", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            params.Add(errPrm)

            SqlHelper.ExecuteNonQuery(CustomClass.strConnection, CommandType.StoredProcedure, "spAPOtorTransUpdate", params.ToArray)
            Dim Err = CType(errPrm.Value, String)

            If (Err <> "OK") Then
                Throw New Exception("Gagal Execute spApDisbTransBeforeDisburseUpdate " + Err)
            End If

        Catch ex As Exception
            Throw (New Exception(ex.Message))
        End Try
    End Sub




    '    Dim objConnection As New SqlConnection(CustomClass.strConnection)
    '    Dim objtransaction As SqlTransaction
    '    Try
    '        If objConnection.State = ConnectionState.Closed Then objConnection.Open()
    '        objtransaction = objConnection.BeginTransaction

    '        Dim params0 As IList(Of SqlParameter) = New List(Of SqlParameter)
    '        If CustomClass.ReferenceNo = "" Then
    '            ' ambil reference no otomatis jika no reference blank
    '            params0.Add(New SqlParameter("@branchid", SqlDbType.VarChar, 3) With {.Value = CustomClass.BranchId.Replace("'", "")})
    '            params0.Add(New SqlParameter("@BankAccountID", SqlDbType.Char, 10) With {.Value = CustomClass.BankAccountID})
    '            Dim refParam = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20) With {.Direction = ParameterDirection.Output}
    '            params0.Add(refParam)
    '            params0.Add(New SqlParameter("@BusinessDate", SqlDbType.DateTime) With {.Value = CustomClass.BusinessDate})
    '            params0.Add(New SqlParameter("@Flag", SqlDbType.Char, 1) With {.Value = "K"})

    '            SqlHelper.ExecuteScalar(objtransaction, CommandType.StoredProcedure, "spGetNoTransactionKM", params0.ToArray)
    '            CustomClass.ReferenceNo = CStr(refParam.Value)
    '        End If

    '        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

    '        params.Add(New SqlParameter("@BranchID", SqlDbType.VarChar, 3) With {.Value = CustomClass.BranchId})
    '        params.Add(New SqlParameter("@PaymentVoucherNo", SqlDbType.VarChar, 20) With {.Value = CustomClass.PaymentVoucherNo})
    '        params.Add(New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10) With {.Value = CustomClass.BankAccountID})
    '        params.Add(New SqlParameter("@BGNo", SqlDbType.VarChar, 20) With {.Value = CustomClass.BGNo})
    '        params.Add(New SqlParameter("@PVStatus", SqlDbType.VarChar, 1) With {.Value = CustomClass.PVStatus})
    '        params.Add(New SqlParameter("@RecipientName", SqlDbType.VarChar, 30) With {.Value = CustomClass.RecipientName})
    '        params.Add(New SqlParameter("@PaymentNotes", SqlDbType.VarChar, 50) With {.Value = CustomClass.Notes})
    '        params.Add(New SqlParameter("@ApprovalBy", SqlDbType.VarChar, 30) With {.Value = CustomClass.LoginId})
    '        params.Add(New SqlParameter("@ApprovalDate", SqlDbType.SmallDateTime, 4) With {.Value = CustomClass.BusinessDate})
    '        params.Add(New SqlParameter("@WOP", SqlDbType.VarChar, 3) With {.Value = CustomClass.WOP})
    '        params.Add(New SqlParameter("@PVAmount", SqlDbType.Decimal) With {.Value = CustomClass.PVAmount})
    '        params.Add(New SqlParameter("@APType", SqlDbType.VarChar, 10) With {.Value = CustomClass.APType})
    '        params.Add(New SqlParameter("@BusinessDate", SqlDbType.DateTime) With {.Value = CustomClass.BusinessDate})
    '        params.Add(New SqlParameter("@PVDate", SqlDbType.DateTime) With {.Value = CustomClass.PVDate})
    '        params.Add(New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20) With {.Value = CustomClass.ApplicationID})
    '        params.Add(New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20) With {.Value = CustomClass.ReferenceNo})

    '        If CustomClass.BGDueDate <> CDate("1/1/1900") Then
    '            params.Add(New SqlParameter("@BGDueDate", SqlDbType.DateTime) With {.Value = CustomClass.BGDueDate})
    '        End If

    '        SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, spAPDisbCashUpdate, params.ToArray)
    '        objtransaction.Commit()
    '    Catch exp As Exception
    '        objtransaction.Rollback()
    '        WriteException("APDisbApp", "UpdateAPDisbCash", exp.Message + exp.StackTrace)
    '    Finally
    '        If objConnection.State = ConnectionState.Open Then objConnection.Close()
    '        objConnection.Dispose()
    '    End Try
    'End Sub
#End Region

    Public Function GetReleaseEbanking(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim params() As SqlParameter = New SqlParameter(6) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            'params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            'params(2).Value = customclass.WhereCond

            'params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            'params(3).Value = customclass.SortBy

            params(2) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(2).Direction = ParameterDirection.Output

            'params(5) = New SqlParameter("@WhereCond1", SqlDbType.VarChar, 8000)
            'params(5).Value = customclass.WhereCond1

            'params(6) = New SqlParameter("@WhereCond2", SqlDbType.VarChar, 8000)
            'params(6).Value = customclass.WhereCond2

            params(3) = New SqlParameter("@pvno", SqlDbType.VarChar, 20)
            params(3).Value = customclass.PaymentVoucherNo

            params(4) = New SqlParameter("@valuedate", SqlDbType.Date)
            params(4).Value = customclass.PVDate

            params(5) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
            params(5).Value = customclass.BranchId

            params(6) = New SqlParameter("@APType", SqlDbType.VarChar, 10)
            params(6).Value = customclass.APType

            customclass.ListAPAppr = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spReleaseEbanking", params).Tables(0)
            customclass.TotalRecord = CInt(params(2).Value)
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
            'WriteException(exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Sub declinceReleaseEbanking(ByVal ocustomClass As Parameter.APDisbApp)
        Dim params(3) As SqlParameter

        params(0) = New SqlParameter("@PaymentVoucherNo", SqlDbType.VarChar)
        params(0).Value = ocustomClass.PaymentVoucherNo
        params(1) = New SqlParameter("@DeclineType", SqlDbType.Char, 1)
        params(1).Value = ocustomClass.declineType
        params(2) = New SqlParameter("@Alasan", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.declineReason
        params(3) = New SqlParameter("@Desc", SqlDbType.VarChar)
        params(3).Value = ocustomClass.Notes

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spDeclinceReleaseEbanking", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Public Sub ReleaseEbankingUpdateStatus(ByVal ocustomClass As Parameter.APDisbApp)
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = ocustomClass.WhereCond
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spReleaseEbankingUpdateStatus", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Function LoadComboRekeningBank(cnn As String, caraBayar As String) As IList(Of Parameter.CommonValueText)
        Dim Str As String
        If caraBayar = "CA" Then
            'Str = "select rtrim(bankaccountid) +';'+ bankaccountid as Value, BankAccountName as Text from BankAccount where branchid='000' and BankAccountType='C' and BankPurpose ='FD' order by BankAccountName "
            'Modify by Wira 20180210-->remove hardcode
            Str = "select rtrim(bankaccountid) +';'+ bankaccountid as Value, BankAccountName as Text from BankAccount where branchid = (select BranchID from Branch WHERE IsHeadOffice = 1) and BankAccountType='C' and BankPurpose ='FD' order by BankAccountName "

        ElseIf caraBayar = "BA" Then
            'Str = "select rtrim(BankId)+';'+bankaccountid as Value, BankAccountName as Text from BankAccount where branchid='000' and BankAccountType='B' and BankPurpose ='FD' order by BankAccountName "
            'Modify by Wira 20180210
            'Str = "select rtrim(BankId)+';'+bankaccountid as Value, BankAccountName as Text from BankAccount where branchid = (select BranchID from Branch WHERE IsHeadOffice = 1) and BankAccountType='B' and BankPurpose ='FD' order by BankAccountName "
            Str = "select rtrim(BankId)+';'+bankaccountid as Value, BankAccountName as Text from BankAccount where branchid = (select BranchID from Branch WHERE IsHeadOffice = 1) and BankAccountType='B' and BankPurpose ='EC' order by BankAccountName "
            'Modify by Wira 20180223-->add for Ebanking
            'modify nofi 27022019 terkait kontrak tutup dan buka account baru
        ElseIf caraBayar = "NCA" Then
            Str = " select rtrim(bankaccountid) +';'+ bankaccountid as Value, BankAccountName as Text from BankAccount where BankAccountID='NCA099' "
        Else
            Str = "select rtrim(bankaccountid) +';'+ bankaccountid as Value, BankAccountName as Text from BankAccount where IsEBanking = 1 order by BankAccountName "

        End If

        Dim result = New List(Of Parameter.CommonValueText)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.Text, Str)


        If (reader.HasRows) Then
            If (reader Is Nothing) Then
                Return Nothing
            End If
            If (reader.IsClosed) Then
                Return Nothing
            End If
            Dim _cID = reader.GetOrdinal("Value")
            Dim _desc = reader.GetOrdinal("Text")
            While (reader.Read())
                result.Add(New Parameter.CommonValueText(IIf(reader.IsDBNull(_cID), String.Empty, reader.GetString(_cID)), IIf(reader.IsDBNull(_desc), "-", reader.GetString(_desc).ToString())))
            End While

        End If
        Return result
    End Function

    Public Function APgantiCaraBayar(ByVal CustomClass As Parameter.APDisbApp) As String
        Dim objconnection As New SqlConnection(CustomClass.strConnection)
        Dim objtransaction As SqlTransaction = Nothing
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()

            objtransaction = objconnection.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = CustomClass.BranchId

            params(1) = New SqlParameter("@PaymentVoucherNo", SqlDbType.Structured)
            params(1).Value = CustomClass.paymentvoucherno_DT

            params(2) = New SqlParameter("@WOP", SqlDbType.Char, 2)
            params(2).Value = CustomClass.WOP

            params(3) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
            params(3).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, "spUpdateGantiCaraBayar", params)
            objtransaction.Commit()
            Return ""

        Catch exp As Exception
            objtransaction.Rollback()
            WriteException("APDisbApp", "UpdateAPDisbApp", exp.Message + exp.StackTrace)
            Return "Proses ganti cara bayar gagal!"
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
    End Function

    Public Function UpdateMultiAPDisbApp2_Report(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@PaymentVoucherNo", SqlDbType.VarChar, 30)
        params(0).Value = customclass.PaymentVoucherNo
        params(1) = New SqlParameter("@GM1", SqlDbType.Char, 50)
        params(1).Value = customclass.GM1
        params(2) = New SqlParameter("@GM2", SqlDbType.Char, 50)
        params(2).Value = customclass.GM2
        params(3) = New SqlParameter("@GM3", SqlDbType.Char, 50)
        params(3).Value = customclass.GM3
        params(4) = New SqlParameter("@Direksi", SqlDbType.Char, 50)
        params(4).Value = customclass.Direksi

        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spAPDisbAppMultipleUpdate_22", params)
        Return customclass
    End Function

End Class
