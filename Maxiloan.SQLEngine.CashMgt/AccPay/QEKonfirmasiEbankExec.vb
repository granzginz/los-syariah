﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

Public Class QEKonfirmasiEbankExec : Inherits DataAccessBase
    Public Function GetListing(ByVal oCostom As Parameter.PEKonfirmasiEbankExec) As Parameter.PEKonfirmasiEbankExec

        Dim oReturnValue As New Parameter.PEKonfirmasiEbankExec
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCostom.strConnection, CommandType.StoredProcedure, "spPaymentVoucherExec").Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function Saving(ByVal oCustom As Parameter.PEKonfirmasiEbankExec) As String
        Dim param() As SqlParameter = New SqlParameter(1) {}

        Try
            param(0) = New SqlParameter("@businnesDate", SqlDbType.DateTime)
            param(0).Value = oCustom.BusinessDate

            param(1) = New SqlParameter("@paymentVoucherNo", SqlDbType.Char, 20)
            param(1).Value = oCustom.PaymentVoucherNo

            SqlHelper.ExecuteNonQuery(oCustom.strConnection, CommandType.StoredProcedure, "spPaymentVoucherExecSave", param)

            Return ""
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
End Class
