Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

Public Class AgentFeeDisbursement : Inherits DataAccessBase
    Private Const spGetAgentFeeDisbursementList As String = "spGetAgentFeeDisbursementList"
    Private Const spAgentFeeDisbursement As String = "spAgentFeeDisbursement"

    Public Function getAgentFeeDisbursementList(ByVal customClass As Parameter.AgentFeeDisbursement) As Parameter.AgentFeeDisbursement
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
        params(1).Value = customClass.SortBy

        customClass.Dataset = SqlHelper.ExecuteDataset(customClass.strConnection, _
        CommandType.StoredProcedure, spGetAgentFeeDisbursementList, params)

        Return customClass
    End Function    

    Public Function saveAgentFeeDisbursement(ByVal customclass As Parameter.AgentFeeDisbursement) As String        
        Dim params() As SqlParameter = New SqlParameter(12) {}
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Dim intLoopOmset As Integer

        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@bankaccountid", SqlDbType.Char, 10)
            params(1).Value = customclass.BankAccountID
            params(2) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
            params(2).Value = customclass.LoginId
            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = customclass.BusinessDate
            params(4) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(4).Value = customclass.Amount
            params(5) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(5).Value = customclass.ValueDate
            params(6) = New SqlParameter("@referenceNo", SqlDbType.Char, 20)
            params(6).Value = customclass.ReferenceNo
            params(7) = New SqlParameter("@Notes", SqlDbType.Char, 50)
            params(7).Value = customclass.Notes
            params(8) = New SqlParameter("@CoyID", SqlDbType.VarChar, 3)
            params(8).Value = customclass.CoyID
            params(9) = New SqlParameter("@WOP", SqlDbType.Char, 2)
            params(9).Value = customclass.WOP
            params(10) = New SqlParameter("@strDepartmentID", SqlDbType.Char, 2)
            params(10).Value = customclass.DepartementID
            params(11) = New SqlParameter("@agreementNo", SqlDbType.Char, 20)
            params(11).Value = customclass.AgreementNo
            params(12) = New SqlParameter("@SupplierName", SqlDbType.VarChar, 50)
            params(12).Value = customclass.RecepientName

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spAgentFeeDisbursement, params)
            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()            
            Return exp.Message.ToString
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function
End Class
