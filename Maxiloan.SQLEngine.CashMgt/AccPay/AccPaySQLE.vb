Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

Public Class AccPaySQLE : Inherits DataAccessBase
    Public Function getPencairanPinjamanList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
        params(1).Value = customClass.SortBy

        customClass.Dataset = SqlHelper.ExecuteDataset(customClass.strConnection, _
        CommandType.StoredProcedure, "dbo.spGetPencairanPinjamanList", params)

        Return customClass
    End Function
    Public Function savePencairanPinjamanHO(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
        Dim objtrans As SqlTransaction
        Dim objcon As New SqlConnection(customClass.strConnection)
        Dim params() As SqlParameter = New SqlParameter(2) {}

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@applicationID", SqlDbType.VarChar, 4000)
            params(0).Value = customClass.SelectedApplicationIDs
            params(1) = New SqlParameter("@Error", SqlDbType.NVarChar, 4000)
            params(1).Direction = ParameterDirection.Output
            params(2) = New SqlParameter("@referenceNo", SqlDbType.Char, 20)
            params(2).Value = customClass.ReferenceNo

            If CBool(SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, _
            "spPencairanPinjamanHOSave", params)) Then
                objtrans.Commit()
                customClass.ErrorMessage = params(1).Value.ToString
            Else
                objtrans.Rollback()
                customClass.ErrorMessage = ""
            End If

            Return customClass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        Finally
            objcon.Dispose()
        End Try
    End Function
    Public Function getPencairanPinjamanCustomerList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
        params(1).Value = customClass.SortBy

        customClass.Dataset = SqlHelper.ExecuteDataset(customClass.strConnection, _
        CommandType.StoredProcedure, "dbo.spGetPencairanPinjamanCustomerList", params)

        Return customClass
    End Function
    Public Sub savePencairanPinjamanCustomer(ByVal customClass As Parameter.AccPayParameter)
        Dim objtrans As SqlTransaction
        Dim objcon As New SqlConnection(customClass.strConnection)
        Dim params() As SqlParameter = New SqlParameter(14) {}

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@bankaccountid", SqlDbType.Char, 10)
            params(1).Value = customClass.BankAccountID
            params(2) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
            params(2).Value = customClass.LoginId
            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = customClass.BusinessDate
            params(4) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(4).Value = customClass.Amount
            params(5) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(5).Value = customClass.ValueDate
            params(6) = New SqlParameter("@referenceNo", SqlDbType.Char, 20)
            params(6).Value = customClass.ReferenceNo
            params(7) = New SqlParameter("@Notes", SqlDbType.Char, 50)
            params(7).Value = customClass.Notes
            params(8) = New SqlParameter("@CoyID", SqlDbType.VarChar, 3)
            params(8).Value = customClass.CoyID
            params(9) = New SqlParameter("@WOP", SqlDbType.Char, 2)
            params(9).Value = customClass.WOP
            params(10) = New SqlParameter("@strDepartmentID", SqlDbType.Char, 2)
            params(10).Value = customClass.DepartementID
            params(11) = New SqlParameter("@agreementNo", SqlDbType.Char, 20)
            params(11).Value = customClass.AgreementNo
            params(12) = New SqlParameter("@SupplierName", SqlDbType.VarChar, 50)
            params(12).Value = customClass.RecepientName
            params(13) = New SqlParameter("@applicationID", SqlDbType.Char, 20)
            params(13).Value = customClass.SelectedApplicationIDs
            params(14) = New SqlParameter("@Error", SqlDbType.NVarChar, 4000)
            params(14).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, _
                        "spPencairanPinjamanCustomerSave", params)
            objtrans.Commit()
        Catch exp As Exception
            Throw New Exception(exp.Message)
        Finally
            objcon.Dispose()
        End Try
    End Sub
    Public Function getInquiryPencairanPinjamanList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
        params(1).Value = customClass.SortBy

        customClass.Dataset = SqlHelper.ExecuteDataset(customClass.strConnection, _
        CommandType.StoredProcedure, "dbo.spGetInquiryPencairanPinjamanList", params)

        Return customClass
    End Function
    Public Function getOutstandingPencairanPinjamanList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
        params(1).Value = customClass.SortBy

        customClass.Dataset = SqlHelper.ExecuteDataset(customClass.strConnection, _
        CommandType.StoredProcedure, "dbo.spGetOutstandingPencairanPinjamanList", params)

        Return customClass
    End Function
    Public Function getBuktiKasKeluarList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@referenceNo", SqlDbType.Char, 20)
        params(0).Value = customClass.ReferenceNo
        params(1) = New SqlParameter("@applicationIDs", SqlDbType.VarChar, 4000)
        params(1).Value = customClass.SelectedApplicationIDs

        customClass.Dataset = SqlHelper.ExecuteDataset(customClass.strConnection, _
        CommandType.StoredProcedure, "dbo.spBuktiKasKeluarList", params)

        Return customClass
    End Function
    Public Sub saveBiayaProses(ByVal customClass As Parameter.AccPayParameter)
        Dim objtrans As SqlTransaction
        Dim objcon As New SqlConnection(customClass.strConnection)
        Dim params() As SqlParameter = New SqlParameter(1) {}

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            params(0) = New SqlParameter("@applicationID", SqlDbType.Char, 20)
            params(0).Value = customClass.SelectedApplicationIDs
            params(1) = New SqlParameter("@Error", SqlDbType.NVarChar, 4000)
            params(1).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, _
                        "spBiayaProsesSave", params)
            objtrans.Commit()
        Catch exp As Exception
            Throw New Exception(exp.Message)
        Finally
            objcon.Dispose()
        End Try
    End Sub



    Public Function BiayaProsesReportDetail(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@StartDate", SqlDbType.DateTime)
        params(1).Value = customClass.StartDate
        params(2) = New SqlParameter("@EndDate", SqlDbType.DateTime)
        params(2).Value = customClass.EndDate

        customClass.Dataset = SqlHelper.ExecuteDataset(customClass.strConnection, _
        CommandType.StoredProcedure, "spBiayaProsesDetailReport", params)

        Return customClass
    End Function

    Public Function BiayaProsesReportSummary(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@StartDate", SqlDbType.DateTime)
        params(1).Value = customClass.StartDate
        params(2) = New SqlParameter("@EndDate", SqlDbType.DateTime)
        params(2).Value = customClass.EndDate

        customClass.Dataset = SqlHelper.ExecuteDataset(customClass.strConnection, _
        CommandType.StoredProcedure, "spBiayaProsesSummaryReport", params)

        Return customClass
    End Function
End Class
