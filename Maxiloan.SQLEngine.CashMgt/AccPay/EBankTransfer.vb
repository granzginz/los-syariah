

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class EBankTransfer : Inherits DataAccessBase
    Private Const spEBankTransferSave As String = "spEBankTransferSave"
    Private Const spEBankTransferSave_Update As String = "spEBankTransferSave_Update"
    Private Const spEBankTransferList As String = "spEBankTransferList"
    Private Const spEBankTransferList2 As String = "spEBankTransferList2"
    Private Const spAPDisbEBankUpdate As String = "spAPDisbEBankUpdate"
    Private Const spEBankTransferChangeStatus As String = "spEBankTransferChangeStatus"
    Private Const spEBankRejectSave As String = "spEBankRejectSave"
    Private Const spAPDisbEBankReject As String = "spAPDisbEBankReject"
    Private Const spAPDisbAppUpdate2 As String = "spAPDisbAppUpdate2"    
    Private Const spGetRequestEdit As String = "spGetRequestEdit"
    Private m_connection As SqlConnection

    Public Function EBankTransferSave(ByVal listdata As ArrayList, ByVal PVdata As Parameter.APDisbApp) As String
        Dim data As Parameter.EBankTransfer
        Dim objconnection As New SqlConnection(PVdata.strConnection)
        Dim objtransaction As SqlTransaction = Nothing
        Dim params() As SqlParameter = New SqlParameter(30) {}
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objtransaction = objconnection.BeginTransaction

            For i As Integer = 0 To listdata.Count - 1
                data = listdata(i)
                If PVdata.PVStatus = "F" Then
                    params = DataToParams_forEdit(data)
                    SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, spEBankTransferSave_Update, params)
                Else
                    params = DataToParams(data)
                    SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, spEBankTransferSave, params)
                End If
            Next

            ChangeStatusPV(PVdata, objtransaction)

            objtransaction.Commit()
            Return String.Empty
        Catch exp As Exception
            objtransaction.Rollback()
            Throw New Exception("Failed On Saving EBankTransfer")
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
    End Function

    Private Function ChangeStatusPV(ByVal customclass As Parameter.APDisbApp, ByVal objtransaction As SqlTransaction) As String

        Dim params() As SqlParameter = New SqlParameter(7) {}

        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@PaymentVoucherNo", SqlDbType.Structured)
        params(1).Value = customclass.paymentvoucherno_DT

        params(2) = New SqlParameter("@PVStatus", SqlDbType.VarChar, 1)
        params(2).Value = customclass.PVStatus

        params(3) = New SqlParameter("@PaymentNotes", SqlDbType.VarChar, 50)
        params(3).Value = customclass.Notes

        params(4) = New SqlParameter("@ApprovalBy", SqlDbType.VarChar, 30)
        params(4).Value = customclass.LoginId

        params(5) = New SqlParameter("@ApprovalDate", SqlDbType.SmallDateTime, 4)
        params(5).Value = customclass.BusinessDate

        params(6) = New SqlParameter("@UpdateMode", SqlDbType.VarChar, 1)
        params(6).Value = customclass.UpdateMode

        params(7) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
        params(7).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, spAPDisbAppUpdate2, params)
        'objtransaction.Commit()
        'Return CStr(params(7).Value)
        Return String.Empty

    End Function

    Public Function EBankTransferList(ByVal customclass As Parameter.EBankTransfer) As Parameter.EBankTransfer
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
           

            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter("@PVnoDT", SqlDbType.Structured)
            params(1).Value = customclass.PVnoDT

            params(2) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(2).Value = customclass.SortBy

            params(3) = New SqlParameter("@TotalRecords", SqlDbType.Int)
            params(3).Direction = ParameterDirection.Output

            customclass.ListAPAppr = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spEBankTransferList, params).Tables(0)
            customclass.TotalRecord = CInt(customclass.ListAPAppr.Rows.Count)
            Return customclass
        Catch exp As Exception
            WriteException("EBankTransfer", "EBankTransferList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function EBankTransferList2(ByVal customclass As Parameter.EBankTransfer) As Parameter.EBankTransfer
        'Dim params() As SqlParameter = New SqlParameter(5) {}
        'Try

        '    params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        '    params(0).Value = customclass.WhereCond

        '    params(1) = New SqlParameter("@PVnoDT", SqlDbType.Structured)
        '    params(1).Value = customclass.PVnoDT

        '    params(2) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        '    params(2).Value = customclass.SortBy

        '    params(3) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        '    params(3).Direction = ParameterDirection.Output

        '    params(4) = New SqlParameter("@WhereCond1", SqlDbType.VarChar, 1000)
        '    params(4).Value = customclass.WhereCond1

        '    params(5) = New SqlParameter("@WhereCond2", SqlDbType.VarChar, 1000)
        '    params(5).Value = customclass.WhereCond2

        '    'params(6) = New SqlParameter("@APType", SqlDbType.VarChar, 4)
        '    'params(6).Value = customclass.APType

        '    customclass.ListAPAppr = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spEBankTransferList2, params).Tables(0)

        '    customclass.TotalRecord = CInt(customclass.ListAPAppr.Rows.Count)
        '    Return customclass
        'Catch exp As Exception
        '    WriteException("EBankTransfer", "EBankTransferList", exp.Message + exp.StackTrace)
        '    Return Nothing
        'End Try

        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@APType", SqlDbType.VarChar, 10)
            params(0).Value = customclass.APType

            params(1) = New SqlParameter("@valuedate", SqlDbType.Date)
            params(1).Value = customclass.ValueDate

            params(2) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
            params(2).Value = customclass.BranchId

            customclass.ListAPAppr = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spEbangkingExecutionList", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("EBankTransfer", "EBankTransferList", exp.Message + exp.StackTrace)
            Return Nothing
        End Try

    End Function

    Private Function DataToParams(ByVal data As Parameter.EBankTransfer) As SqlParameter()
        Dim params() As SqlParameter = New SqlParameter(33) {}
        params(0) = New SqlParameter("@TransactionRefNo", SqlDbType.VarChar)
        params(1) = New SqlParameter("@ValueDate", SqlDbType.VarChar)
        params(2) = New SqlParameter("@Currency", SqlDbType.VarChar)
        params(3) = New SqlParameter("@Amount", SqlDbType.VarChar)
        params(4) = New SqlParameter("@OrderingParty", SqlDbType.VarChar)
        params(5) = New SqlParameter("@OrderingPartyAddress", SqlDbType.VarChar)
        params(6) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(7) = New SqlParameter("@BankRoutingMethod", SqlDbType.VarChar)
        params(8) = New SqlParameter("@BankCode", SqlDbType.VarChar)
        params(9) = New SqlParameter("@BeneficiaryBankName", SqlDbType.VarChar)
        params(10) = New SqlParameter("@BeneficiaryAccNo", SqlDbType.VarChar)
        params(11) = New SqlParameter("@BeneficiaryName", SqlDbType.VarChar)
        params(12) = New SqlParameter("@BeneficiaryAddress1", SqlDbType.VarChar)
        params(13) = New SqlParameter("@BeneficiaryAddress2", SqlDbType.VarChar)
        params(14) = New SqlParameter("@BeneficiaryAddress3", SqlDbType.VarChar)
        params(15) = New SqlParameter("@InterBankRoutingMethod", SqlDbType.VarChar)
        params(16) = New SqlParameter("@InterBankSwiftCode", SqlDbType.VarChar)
        params(17) = New SqlParameter("@IntermediaryBankName", SqlDbType.VarChar)
        params(18) = New SqlParameter("@PaymentDetail1", SqlDbType.VarChar)
        params(19) = New SqlParameter("@PaymentDetail2", SqlDbType.VarChar)
        params(20) = New SqlParameter("@PaymentDetail3", SqlDbType.VarChar)
        params(21) = New SqlParameter("@PaymentDetail4", SqlDbType.VarChar)
        params(22) = New SqlParameter("@BtoBInfoCitibank", SqlDbType.VarChar)
        params(23) = New SqlParameter("@BtoBInfoBeneBank", SqlDbType.VarChar)
        params(24) = New SqlParameter("@BtoBInfoInterBank", SqlDbType.VarChar)
        params(25) = New SqlParameter("@Charges", SqlDbType.VarChar)
        params(26) = New SqlParameter("@EndFile", SqlDbType.VarChar)
        params(27) = New SqlParameter("@DtmUpdate", SqlDbType.VarChar)
        params(28) = New SqlParameter("@UserUpdate", SqlDbType.VarChar)

        params(0).Value = data.TransactionRefNo
        params(1).Value = data.ValueDate
        params(2).Value = data.Currency
        params(3).Value = data.Amount
        params(4).Value = data.OrderingParty
        params(5).Value = data.OrderingPartyAddress
        params(6).Value = data.BranchId
        params(7).Value = data.BankRoutingMethod
        params(8).Value = data.BankCode
        params(9).Value = data.BeneficiaryBankName
        params(10).Value = data.BeneficiaryAccNo
        params(11).Value = data.BeneficiaryName
        params(12).Value = data.BeneficiaryAddress1
        params(13).Value = data.BeneficiaryAddress2
        params(14).Value = data.BeneficiaryAddress3
        params(15).Value = data.InterBankRoutingMethod
        params(16).Value = data.InterBankSwiftCode
        params(17).Value = data.IntermediaryBankName
        params(18).Value = data.PaymentDetail1
        params(19).Value = data.PaymentDetail2
        params(20).Value = data.PaymentDetail3
        params(21).Value = data.PaymentDetail4
        params(22).Value = data.BtoBInfoCitibank
        params(23).Value = data.BtoBInfoBeneBank
        params(24).Value = data.BtoBInfoInterBank
        params(25).Value = data.Charges
        params(26).Value = data.EndFile
        params(27).Value = data.DtmUpdate
        params(28).Value = data.UserUpdate
        params(28).Value = data.UserUpdate

        params(29) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
        params(29).Direction = ParameterDirection.Output

        params(30) = New SqlParameter("@BeneficiaryBankAddress", SqlDbType.VarChar, 20)
        params(30).Value = data.BeneficiaryBankAddress

        params(31) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10)
        params(31).Value = data.BankAccountId

        params(32) = New SqlParameter("@APType", SqlDbType.Char, 4)
        params(32).Value = data.APType

        params(33) = New SqlParameter("@BranchIdPv", SqlDbType.Char, 3)
        params(33).Value = data.BranchId

        Return params

    End Function

    Public Function EBankExecuteNew(ByVal datalist As ArrayList, ByVal EBankdata As Parameter.EBankTransfer) As String
        Dim retval As String = String.Empty
        Dim objconnection As New SqlConnection(EBankdata.strConnection)
        Dim objtransaction As SqlTransaction
        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objtransaction = objconnection.BeginTransaction

        Try

            For i As Integer = 0 To datalist.Count - 1
                Dim data As Parameter.APDisbApp = datalist(i)

                Select Case data.APType
                    Case "PYREQ"
                        retval = EBankPYREQExec(data, objtransaction)
                    Case "TRFAA"
                        retval = EBankTRFAAExec(data, objtransaction)
                    Case "TRFFD"
                        retval = EBankTRFUNDExec(data, objtransaction)
                    Case "PCREI"
                        retval = EBankPCREIExec(data, objtransaction)
                    Case "ODB"
                        retval = EBankODBExec(data, objtransaction)
                    Case Else
                        retval = EBankExecute(data, objtransaction)
                End Select
            Next

            EBankTransferChangeStatus2(EBankdata, objtransaction)

            objtransaction.Commit()
            Return ""
        Catch exp As Exception
            objtransaction.Rollback()
            'WriteException("EBankExecuteNew", "EBankExecuteNew", exp.Message + exp.StackTrace)
            Return exp.Message
            Throw New Exception(exp.Message)
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()

        End Try

        Return retval
    End Function

    Private Function EBankExecute(ByVal data As Parameter.APDisbApp, ByVal objtransaction As SqlTransaction) As String
        'Dim objconnection As New SqlConnection(data.strConnection)
        'Dim objtransaction As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(15) {}
        'Try
        'If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        'objtransaction = objconnection.BeginTransaction

        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(0).Value = data.BranchId

        params(1) = New SqlParameter("@PaymentVoucherNo", SqlDbType.VarChar, 20)
        params(1).Value = data.PaymentVoucherNo

        params(2) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 20)
        params(2).Value = data.BankAccountID

        params(3) = New SqlParameter("@BGNo", SqlDbType.VarChar, 20)
        params(3).Value = data.BGNo

        params(4) = New SqlParameter("@PVStatus", SqlDbType.VarChar, 20)
        params(4).Value = data.PVStatus

        params(5) = New SqlParameter("@RecipientName", SqlDbType.VarChar, 30)
        params(5).Value = data.RecipientName

        params(6) = New SqlParameter("@PaymentNotes", SqlDbType.VarChar, 100)
        params(6).Value = data.PaymentNote

        params(7) = New SqlParameter("@ApprovalBy", SqlDbType.VarChar, 50)
        params(7).Value = data.approvalBy


        params(8) = New SqlParameter("@WOP", SqlDbType.VarChar, 2)
        params(8).Value = data.WOP

        params(9) = New SqlParameter("@PVAmount", SqlDbType.Decimal)
        params(9).Value = data.PVAmount

        params(10) = New SqlParameter("@BGDueDate", SqlDbType.DateTime)
        params(10).Value = data.PVDate ' DBNull.Value

        params(11) = New SqlParameter("@APType", SqlDbType.VarChar, 10)
        params(11).Value = data.APType

        params(12) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(12).Value = data.BusinessDate

        params(13) = New SqlParameter("@PVDate", SqlDbType.DateTime)
        params(13).Value = data.PVDate

        params(14) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(14).Value = data.ApplicationID

        params(15) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
        params(15).Value = data.ReferenceNo

        SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, spAPDisbEBankUpdate, params)

        '    objtransaction.Commit()
        '    Return CStr(params(15).Value)
        'Catch exp As Exception
        '    objtransaction.Rollback()
        '    WriteException("EBankTransfer", "EBankTransferSave", exp.Message + exp.StackTrace)
        '    Throw New Exception("Failed On Saving EBankTransfer")
        'Finally
        '    If objconnection.State = ConnectionState.Open Then objconnection.Close()
        '    objconnection.Dispose()
        'End Try
    End Function

    Public Function EBankTransferChangeStatus(ByVal data As Parameter.EBankTransfer) As String
        Dim objconnection As New SqlConnection(data.strConnection)
        Dim objtransaction As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(12) {}
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objtransaction = objconnection.BeginTransaction

            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = data.WhereCond

            params(1) = New SqlParameter("@PVnoDT", SqlDbType.Structured)
            params(1).Value = data.PVnoDT

            params(2) = New SqlParameter("@StatusFlag", SqlDbType.VarChar, 2)
            params(2).Value = data.StatusFlag

            params(3) = New SqlParameter("@GenerateBy", SqlDbType.VarChar, 20)
            params(3).Value = data.GenerateBy
            params(4) = New SqlParameter("@GenerateDate", SqlDbType.DateTime)
            params(4).Value = data.GenerateDate

            params(5) = New SqlParameter("@PaidBy", SqlDbType.VarChar, 20)
            params(5).Value = data.PaidBy
            params(6) = New SqlParameter("@PaidDate", SqlDbType.DateTime)
            params(6).Value = data.PaidDate

            params(7) = New SqlParameter("@RejectBy", SqlDbType.VarChar, 20)
            params(7).Value = data.RejectBy
            params(8) = New SqlParameter("@RejectDate", SqlDbType.DateTime)
            params(8).Value = data.RejectDate

            params(9) = New SqlParameter("@DeleteBy", SqlDbType.VarChar, 20)
            params(9).Value = data.DeleteBy
            params(10) = New SqlParameter("@DeleteDate", SqlDbType.DateTime)
            params(10).Value = data.DeleteDate

            params(11) = New SqlParameter("@EditedBy", SqlDbType.VarChar, 20)
            params(11).Value = data.EditedBy
            params(12) = New SqlParameter("@EditedDate", SqlDbType.DateTime)
            params(12).Value = data.EditedDate

            SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, spEBankTransferChangeStatus, params)
            objtransaction.Commit()
            Return ""
        Catch exp As Exception
            objtransaction.Rollback()
            WriteException("EBankTransfer", "EBankTransferSave", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Saving EBankTransfer")
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
    End Function

    Private Function EBankTransferChangeStatus2(ByVal data As Parameter.EBankTransfer, ByVal objtransaction As SqlTransaction) As String
        'Dim objconnection As New SqlConnection(data.strConnection)
        'Dim objtransaction As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(12) {}
        'Try
        '    If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        '    objtransaction = objconnection.BeginTransaction

        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = data.WhereCond

        params(1) = New SqlParameter("@PVnoDT", SqlDbType.Structured)
        params(1).Value = data.PVnoDT

        params(2) = New SqlParameter("@StatusFlag", SqlDbType.VarChar, 1)
        params(2).Value = data.StatusFlag

        params(3) = New SqlParameter("@GenerateBy", SqlDbType.VarChar, 20)
        params(3).Value = data.GenerateBy
        params(4) = New SqlParameter("@GenerateDate", SqlDbType.DateTime)
        params(4).Value = data.GenerateDate

        params(5) = New SqlParameter("@PaidBy", SqlDbType.VarChar, 20)
        params(5).Value = data.PaidBy
        params(6) = New SqlParameter("@PaidDate", SqlDbType.DateTime)
        params(6).Value = data.PaidDate

        params(7) = New SqlParameter("@RejectBy", SqlDbType.VarChar, 20)
        params(7).Value = data.RejectBy
        params(8) = New SqlParameter("@RejectDate", SqlDbType.DateTime)
        params(8).Value = data.RejectDate

        params(9) = New SqlParameter("@DeleteBy", SqlDbType.VarChar, 20)
        params(9).Value = data.DeleteBy
        params(10) = New SqlParameter("@DeleteDate", SqlDbType.DateTime)
        params(10).Value = data.DeleteDate

        params(11) = New SqlParameter("@EditedBy", SqlDbType.VarChar, 20)
        params(11).Value = data.EditedBy
        params(12) = New SqlParameter("@EditedDate", SqlDbType.DateTime)
        params(12).Value = data.EditedDate

        SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, spEBankTransferChangeStatus, params)
        'objtransaction.Commit()
        'Return ""
        'Catch exp As Exception
        '    objtransaction.Rollback()
        '    WriteException("EBankTransfer", "EBankTransferSave", exp.Message + exp.StackTrace)
        '    Throw New Exception("Failed On Saving EBankTransfer")
        'Finally
        '    If objconnection.State = ConnectionState.Open Then objconnection.Close()
        '    objconnection.Dispose()
        'End Try
    End Function

    

#Region "EBankReject"
    Private Function DataToParams_EBankReject(ByVal data As Parameter.EBankReject) As SqlParameter()
        Dim params() As SqlParameter = New SqlParameter(11) {}
        params(0) = new SqlParameter("@TransactionRefNo", SqlDbType.VarChar)
        params(1) = New SqlParameter("@JenisPembayaran", SqlDbType.VarChar)
        params(2) = New SqlParameter("@Beneficiary", SqlDbType.VarChar)
        params(3) = New SqlParameter("@Jumlah", SqlDbType.Decimal)
        params(4) = New SqlParameter("@Bank", SqlDbType.VarChar)
        params(5) = New SqlParameter("@Cabang", SqlDbType.VarChar)
        params(6) = New SqlParameter("@NoRekening", SqlDbType.VarChar)
        params(7) = New SqlParameter("@NamaRekening", SqlDbType.VarChar)
        params(8) = New SqlParameter("@NoKontrak", SqlDbType.VarChar)
        params(9) = New SqlParameter("@NamaCustomer", SqlDbType.VarChar)
        params(10) = New SqlParameter("@AlasanPenolakan", SqlDbType.VarChar)


       params(0).value = data.TransactionRefNo
        params(1).Value = data.JenisPembayaran
        params(2).Value = data.Beneficiary
        params(3).Value = data.Jumlah
        params(4).Value = data.Bank
        params(5).Value = data.Cabang
        params(6).Value = data.NoRekening
        params(7).Value = data.NamaRekening
        params(8).Value = data.NoKontrak
        params(9).Value = data.NamaCustomer
        params(10).Value = data.AlasanPenolakan

        params(11) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
        params(11).Direction = ParameterDirection.Output

        Return params

    End Function

    Public Function EBankRejectSave(ByVal data As Parameter.EBankReject) As String
        Dim objconnection As New SqlConnection(data.strConnection)
        Dim objtransaction As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(11) {}
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objtransaction = objconnection.BeginTransaction

            params = DataToParams_EBankReject(data)

            SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, spEBankRejectSave, params)
            objtransaction.Commit()
            Return CStr(params(11).Value)
        Catch exp As Exception
            objtransaction.Rollback()
            WriteException("EBankTransfer", "EBankTransferSave", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Saving EBankTransfer")
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
    End Function
#End Region

    Public Function EBankExecuteReject(ByVal data As Parameter.APDisbApp) As String
        Dim objconnection As New SqlConnection(data.strConnection)
        Dim objtransaction As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(15) {}
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objtransaction = objconnection.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = data.BranchId

            params(1) = New SqlParameter("@PaymentVoucherNo", SqlDbType.VarChar, 20)
            params(1).Value = data.PaymentVoucherNo

            params(2) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 20)
            params(2).Value = data.BankAccountID

            params(3) = New SqlParameter("@BGNo", SqlDbType.VarChar, 20)
            params(3).Value = data.BGNo

            params(4) = New SqlParameter("@PVStatus", SqlDbType.VarChar, 20)
            params(4).Value = data.PVStatus

            params(5) = New SqlParameter("@RecipientName", SqlDbType.VarChar, 30)
            params(5).Value = data.RecipientName

            params(6) = New SqlParameter("@PaymentNotes", SqlDbType.VarChar, 100)
            params(6).Value = data.PaymentNote

            params(7) = New SqlParameter("@ApprovalBy", SqlDbType.VarChar, 50)
            params(7).Value = data.approvalBy


            params(8) = New SqlParameter("@WOP", SqlDbType.VarChar, 2)
            params(8).Value = data.WOP

            params(9) = New SqlParameter("@PVAmount", SqlDbType.Decimal)
            params(9).Value = data.PVAmount

            params(10) = New SqlParameter("@BGDueDate", SqlDbType.DateTime)
            params(10).Value = DBNull.Value

            params(11) = New SqlParameter("@APType", SqlDbType.VarChar, 10)
            params(11).Value = data.APType

            params(12) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(12).Value = data.BusinessDate

            params(13) = New SqlParameter("@PVDate", SqlDbType.DateTime)
            params(13).Value = data.PVDate

            params(14) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(14).Value = data.ApplicationID

            params(15) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
            params(15).Value = data.ReferenceNo

            SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, spAPDisbEBankReject, params)
            objtransaction.Commit()
            Return CStr(params(15).Value)
        Catch exp As Exception
            objtransaction.Rollback()
            WriteException("EBankTransfer", "EBankTransferSave", exp.Message + exp.StackTrace)
            Throw New Exception("Failed On Saving EBankTransfer")
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
    End Function

    Public Function EBankPYREQExec(ByVal data As Parameter.APDisbApp, ByVal objtransaction As SqlTransaction) As String
        ' Dim objconnection As New SqlConnection(data.strConnection)
        'Dim objtransaction As SqlTransaction = Nothing
        Dim params() As SqlParameter = New SqlParameter(3) {}

        'Try
        'If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        'objtransaction = objconnection.BeginTransaction

        params(0) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
        params(0).Value = data.ReferenceNo

        params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(1).Value = data.BusinessDate

        params(2) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
        params(2).Value = data.LoginId

        params(3) = New SqlParameter("@CompanyID", SqlDbType.VarChar, 3)
        params(3).Value = data.CompanyID

        SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, "spEbankPYREQExec", params)
        'objtransaction.Commit()
        'Return ""
        ''Catch exp As Exception
        ''    objtransaction.Rollback()
        ''    Return exp.Message
        ''Finally
        '    If objconnection.State = ConnectionState.Open Then objconnection.Close()
        '    objconnection.Dispose()
        'End Try
    End Function

    Public Function EBankTRFAAExec(ByVal data As Parameter.APDisbApp, ByVal objtransaction As SqlTransaction) As String
        'Dim objconnection As New SqlConnection(data.strConnection)
        'Dim objtransaction As SqlTransaction = Nothing
        Dim params() As SqlParameter = New SqlParameter(3) {}

        'Try
        'If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        'objtransaction = objconnection.BeginTransaction

        params(0) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
        params(0).Value = data.ReferenceNo

        params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(1).Value = data.BusinessDate

        params(2) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
        params(2).Value = data.LoginId

        params(3) = New SqlParameter("@CompanyID", SqlDbType.VarChar, 3)
        params(3).Value = data.CompanyID

        SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, "spEbankTRFAAExec", params)
        'objtransaction.Commit()
        'Return ""
        'Catch exp As Exception
        '    objtransaction.Rollback()
        '    Return exp.Message
        'Finally
        '    If objconnection.State = ConnectionState.Open Then objconnection.Close()
        '    objconnection.Dispose()
        'End Try
    End Function

    Public Function EBankTRFUNDExec(ByVal data As Parameter.APDisbApp, ByVal objtransaction As SqlTransaction) As String
        'Dim objconnection As New SqlConnection(data.strConnection)
        'Dim objtransaction As SqlTransaction = Nothing
        Dim params() As SqlParameter = New SqlParameter(3) {}

        'Try
        '    If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        '    objtransaction = objconnection.BeginTransaction

        params(0) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
        params(0).Value = data.ReferenceNo

        params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(1).Value = data.BusinessDate

        params(2) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
        params(2).Value = data.LoginId

        params(3) = New SqlParameter("@CompanyID", SqlDbType.VarChar, 3)
        params(3).Value = data.CompanyID

        SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, "spEBankTRFUNDExec", params)
        'objtransaction.Commit()
        'Return ""
        'Catch exp As Exception
        '    objtransaction.Rollback()
        '    Return exp.Message
        'Finally
        '    If objconnection.State = ConnectionState.Open Then objconnection.Close()
        '    objconnection.Dispose()
        'End Try
    End Function

    Public Function EBankPCREIExec(ByVal data As Parameter.APDisbApp, ByVal objtransaction As SqlTransaction) As String
        'Dim objconnection As New SqlConnection(data.strConnection)
        'Dim objtransaction As SqlTransaction = Nothing
        Dim params() As SqlParameter = New SqlParameter(3) {}

        'Try
        '    If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        '    objtransaction = objconnection.BeginTransaction

        params(0) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
        params(0).Value = data.ReferenceNo

        params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(1).Value = data.BusinessDate

        params(2) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
        params(2).Value = data.LoginId

        params(3) = New SqlParameter("@CompanyID", SqlDbType.VarChar, 3)
        params(3).Value = data.CompanyID

        SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, "spEBankPCREIExec", params)
        'objtransaction.Commit()
        'Return ""
        'Catch exp As Exception
        '    objtransaction.Rollback()
        '    Return exp.Message
        'Finally
        '    If objconnection.State = ConnectionState.Open Then objconnection.Close()
        '    objconnection.Dispose()
        'End Try
    End Function

    Public Function EBankODBExec(ByVal data As Parameter.APDisbApp, ByVal objtransaction As SqlTransaction) As String
        'Dim objconnection As New SqlConnection(data.strConnection)
        'Dim objtransaction As SqlTransaction = Nothing
        Dim params() As SqlParameter = New SqlParameter(3) {}

        'Try
        '    If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        '    objtransaction = objconnection.BeginTransaction

        params(0) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
        params(0).Value = data.ReferenceNo

        params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(1).Value = data.BusinessDate

        params(2) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
        params(2).Value = data.LoginId

        params(3) = New SqlParameter("@CompanyID", SqlDbType.VarChar, 3)
        params(3).Value = data.CompanyID

        SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, "spEBankODBExec", params)
        'objtransaction.Commit()
        'Return ""
        'Catch exp As Exception
        '    objtransaction.Rollback()
        '    Return exp.Message
        'Finally
        '    If objconnection.State = ConnectionState.Open Then objconnection.Close()
        '    objconnection.Dispose()
        'End Try
    End Function

    Private Function DataToParams_forEdit(ByVal data As Parameter.EBankTransfer) As SqlParameter()
        Dim params() As SqlParameter = New SqlParameter(13) {}
        params(0) = New SqlParameter("@TransactionRefNo", SqlDbType.VarChar)
        params(1) = New SqlParameter("@ValueDate", SqlDbType.VarChar)
        params(2) = New SqlParameter("@BeneficiaryAccNo", SqlDbType.VarChar)
        params(3) = New SqlParameter("@BeneficiaryName", SqlDbType.VarChar)
        params(0).Value = data.TransactionRefNo
        params(1).Value = data.ValueDate
        params(2).Value = data.BeneficiaryAccNo
        params(3).Value = data.BeneficiaryName

        params(4) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@StatusFlag", SqlDbType.VarChar)
        params(5).Value = data.StatusFlag


        params(6) = New SqlParameter("@AccountNameTo", SqlDbType.VarChar, 50)
        params(7) = New SqlParameter("@AccountNoTo", SqlDbType.VarChar, 50)
        params(8) = New SqlParameter("@BankNameTo", SqlDbType.VarChar, 50)
        params(9) = New SqlParameter("@BankBranchTo", SqlDbType.VarChar, 50)
        params(10) = New SqlParameter("@BankIdTo", SqlDbType.Char, 5)
        params(11) = New SqlParameter("@BankBranchID", SqlDbType.Int)
        params(12) = New SqlParameter("@AccountPayableNo", SqlDbType.Char, 20)
        params(13) = New SqlParameter("@AccountPayableBranchID", SqlDbType.Char, 3)

        
        params(6).Value = data.AccountNameTo
        params(7).Value = data.AccountNoTo
        params(8).Value = data.BankNameTo
        params(9).Value = data.BankBranchTo
        params(10).Value = data.BankIdTo
        params(11).Value = data.BankBranchID
        params(12).Value = data.AccountPayAbleno
        params(13).Value = data.AccountPayAbleBranchID

        Return params
    End Function

    Public Function GetRequestEdit(ByVal customclass As Parameter.EBankTransfer) As Parameter.EBankTransfer
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter("@WhereCond1", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond1
        params(1) = New SqlParameter("@WhereCond2", SqlDbType.VarChar, 1000)
        params(1).Value = customclass.WhereCond2
        params(2) = New SqlParameter("@WhereCond3", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond3

        Try

            customclass.ListAPAppr = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spGetRequestEdit, params).Tables(0)
            customclass.TotalRecord = CInt(customclass.ListAPAppr.Rows.Count)
            Return customclass
        Catch exp As Exception
            WriteException("EBankTransfer", "GetRequestEdit", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
