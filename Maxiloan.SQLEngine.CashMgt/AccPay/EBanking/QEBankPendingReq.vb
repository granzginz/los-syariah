﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Public Class QEBankPendingReq : Inherits DataAccessBase
    Private Const spListing As String = "spBankPayment"
    Private Const spListingDetail As String = "spBankPaymentPendingDetail"
    Private Const spSaveBankPaymentPending As String = "spSaveBankPaymentPending"

    Public Function GetListing(ByVal oCostom As Parameter.PEBankPendingReq) As Parameter.PEBankPendingReq
        Dim params(4) As SqlParameter
        Dim oReturnValue As New Parameter.PEBankPendingReq

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)


        params(0).Value = oCostom.CurrentPage
        params(1).Value = oCostom.PageSize
        params(2).Value = oCostom.WhereCond
        params(3).Value = oCostom.SortBy
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCostom.strConnection, CommandType.StoredProcedure, spListing, params).Tables(0)
            oReturnValue.RecordCount = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetDetailPay(ByVal oCostom As Parameter.PEBankPendingReq) As Parameter.PEBankPendingReq
        Dim param As SqlParameter
        Dim oReturnValue As New Parameter.PEBankPendingReq

        param = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        param.Value = oCostom.WhereCond
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCostom.strConnection, CommandType.StoredProcedure, spListingDetail, param).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function SaveUpdate(ByVal oCostom As Parameter.PEBankPendingReq) As String
        Dim err As String = ""
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@PaymentVoucherNo", SqlDbType.Char)
        params(1) = New SqlParameter("@AlasanPenundaan", SqlDbType.Char)
        params(2) = New SqlParameter("@UsrUpd", SqlDbType.VarChar)
        params(3) = New SqlParameter("@DtmUpd", SqlDbType.Date)

        params(0).Value = oCostom.PaymentVoucherNo
        params(1).Value = oCostom.AlasanPenundaan
        params(2).Value = oCostom.UsrUpd
        params(3).Value = oCostom.DtmUpd

        SqlHelper.ExecuteNonQuery(oCostom.strConnection, CommandType.StoredProcedure, spSaveBankPaymentPending, params)

        Return err
    End Function
End Class
