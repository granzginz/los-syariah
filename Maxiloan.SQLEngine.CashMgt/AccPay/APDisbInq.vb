

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class APDisbInq : Inherits DataAccessBase
    Private Const spAPDisbInq As String = "spAPDisbInquiryPaging"
    Private Const spAPDisbInqReport As String = "spAPDisbInquiryReport"

    Public Function ListAPDisbInq(ByVal customclass As Parameter.APDisbInq) As Parameter.APDisbInq

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output

            customclass.ListAPInq = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAPDisbInq, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("APDisbInq", "ListAPDisbInq", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ReportAPDisbInq(ByVal customclass As Parameter.APDisbInq) As Parameter.APDisbInq
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.ReportAPInq = SqlHelper.ExecuteDataset(customclass.strConnection, _
            CommandType.StoredProcedure, spAPDisbInqReport, params)
            Return customclass
        Catch exp As Exception
            WriteException("APDisbInq", "ListAPDisbInq", exp.Message + exp.StackTrace)
        End Try
    End Function

#Region "PV Inquiry"
    Private Const spPVInquiryPaging As String = "spPVInquiryPaging"
    Private Const spPVInquiryReport As String = "spPVInquiryReport"

    Public Function ListPVInquiry(ByVal customclass As Parameter.APDisbInq) As Parameter.APDisbInq

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListPVInquiry = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spPVInquiryPaging, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("APDisbInq", "ListPVInquiry", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ReportPVInquiry(ByVal customclass As Parameter.APDisbInq) As Parameter.APDisbInq

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.ReportPVInquiry = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spPVInquiryReport, params)
            Return customclass
        Catch exp As Exception
            WriteException("APDisbInq", "ReportPVInquiry", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

    Public Function ListAPDisbInqPrintSelection(ByVal customclass As Parameter.APDisbInq) As Parameter.APDisbInq

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output

            customclass.ListAPInq = SqlHelper.ExecuteDataset(customclass.strConnection, _
                CommandType.StoredProcedure, "spAPDisbInquiryPrintSelectionPaging", params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("APDisbInq", "spAPDisbInquiryPrintSelectionPaging", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub savePrintSelection(ByVal customclass As Parameter.APDisbInq)
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@APNo", SqlDbType.VarChar, 8000)
            params(0).Value = customclass.SelectedAPNo

            SqlHelper.ExecuteNonQuery(customclass.strConnection, _
                CommandType.StoredProcedure, "spAPDisbInqSavePrintSelection", params)
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Sub
End Class
