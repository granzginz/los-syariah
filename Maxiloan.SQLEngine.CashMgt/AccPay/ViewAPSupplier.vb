
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class ViewAPSupplier : Inherits DataAccessBase

    Public Function ListViewPO(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            customclass.listview = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spViewAPPaging", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("ViewAPSupplier", "ListViewPO", exp.Message)
        End Try
    End Function

    Public Function ListView(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@AccountPayableNo", SqlDbType.VarChar, 20)
            params(0).Value = customclass.AccountPayableNo

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spAPView", params)
            If objread.Read Then
                With customclass
                    .ApplicationID = CType(objread("applicationid"), String)
                    .BranchId = CType(objread("branchid"), String)
                    .APTo = CType(objread("apto"), String)
                    .AccountNameTo = CType(objread("accountnameto"), String)
                    .AccountNoTo = CType(objread("AccountNoTo"), String)
                    .BankNameTo = CType(objread("banknameto"), String)
                    .BankBranchTo = CType(objread("bankbranchto"), String)
                    .InvoiceNo = CType(objread("invoiceno"), String)
                    .InvoiceDate = CType(objread("invoicedate"), Date)
                    .PONo = CType(objread("pono"), String)
                    .PODate = CType(objread("podate"), Date)
                    .CustomerName = CType(objread("CustomerName"), String)
                    .AssetDesc = CType(objread("assetdesc"), String)
                    .otrprice = CType(objread("otrprice"), Double)
                    .Customerid = CType(objread("customerid"), String)
                    .AgreementNo = CType(objread("agreementNo"), String)
                    .ContractPrepaidAmount = CType(objread("ContractPrepaidAmount"), Double)
                    .ApprovalNotes = CType(objread("Notes"), String)
                    .DeductionAmount = CType(objread("DeductionAmount"), Decimal)
                    .APAmount = CType(objread("APAmount"), Decimal)
                    .TotalAmount = CType(objread("TotalAmount"), Decimal)
                    .BankAccountName = CType(objread("BankAccountName"), String)
                    .BankBranch = CType(objread("BankBranch"), String)
                    .APDate = CType(objread("APDate"), Date)
                    .AttachedFile = CType(objread("AttachedFile"), String)
                    .DueDate = CType(objread("duedate"), Date)
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("ViewAPSupplier", "ListView", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ListViewAP(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID
            params(1) = New SqlParameter("@APType", SqlDbType.Char, 5)
            params(1).Value = customclass.APType

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spViewAPType", params)
            If objread.Read Then
                With customclass
                    .Name = CType(objread("Name"), String)
                    .NPWP = CType(objread("NPWP"), String)
                    .TDP = CType(objread("TDP"), String)
                    .SIUP = CType(objread("SIUP"), String)
                    .Address = CType(objread("Address"), String)
                    .RTRW = CType(objread("RTRW"), String)
                    .Kelurahan = CType(objread("Kelurahan"), String)
                    .Kecamatan = CType(objread("Kecamatan"), String)

                    .Kota = CType(objread("Kota"), String)
                    .KodePos = CType(objread("KodePos"), String)
                    .Tlp1 = CType(objread("Tlp1"), String)
                    .Tlp2 = CType(objread("Tlp2"), String)
                    .Fax = CType(objread("Fax"), String)
                    .ContactPersonName = CType(objread("ContactPersonName"), String)
                    .ContactPersonTitle = CType(objread("ContactPersonTitle"), String)
                    .EMail = CType(objread("EMail"), String)

                    .MobilePhone = CType(objread("MobilePhone"), String)
                    .BankAccountName = CType(objread("BankAccountName"), String)
                    .BankBranch = CType(objread("BankBranch"), String)
                    .AccountNo = CType(objread("AccountNo"), String)
                    .AccountName = CType(objread("AccountName"), String)
                    .APType = CType(objread("APType"), String)
                   
                   
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("ViewAPSupplier", "ViewAPType", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Function ViewInvoiceDeduction(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@AccountPayableNo", SqlDbType.VarChar, 20)
            params(0).Value = customclass.AccountPayableNo
        customclass.listview = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spViewInvoiceDeduction", params).Tables(0)
        Return customclass
        Catch exp As Exception
            WriteException("ViewInvoiceDeduction", "ViewInvoice", exp.Message)
        End Try
    End Function
End Class
