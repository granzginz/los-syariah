

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class APDisbSelec : Inherits DataAccessBase
    Private Const spAPDisbSelec As String = "spAPDisbSelec"
    Private Const spAPSelection As String = "spAPSelectionList"
    Private Const spAPGroupSelection As String = "spAPGroupList"
    Private Const spAPSaveToPV As String = "spAPSaveToPV"
    Private Const spAPSaveToPVItem As String = "spAPSaveToPVItem"
    Private Const spSaveAPCheckAPBalance As String = "spSaveAPCheckAPBalance"
    Private Const spSaveChangeLoc As String = "spAPchangeLoc"
    Private Const spListEBanking As String = "spEBankingList"

    Private Const PARAM_STATUS As String = "@status"
    Private Const PARAM_ACCOUNTPAYABLENO As String = "@accountpayableno"
    Private Const PARAM_PAYMENTVOUCHERNO As String = "@paymentvoucherno"
    Private Const PARAM_DUEDATE As String = "@duedate"
    Private Const PARAM_APPLICATIONID As String = "@applicationid"
    Private Const PARAM_PAYMENTLOCATION As String = "@paymentlocation"
    Private Const PARAM_PVAMOUNT As String = "@pvamount"
    Private Const PARAM_COA As String = "@coa"
    Private Const PARAM_APTYPE As String = "@aptype"
    Private Const PARAM_APTO As String = "@apto"
    Private Const PARAM_ACCOUNTNAMETO As String = "@accountnameto"
    Private Const PARAM_ACCOUNTNOTO As String = "@accountnoto"
    Private Const PARAM_BANKNAMETO As String = "@banknameto"
    Private Const PARAM_BANKBRANCHTO As String = "@bankbranchto"
    Private Const PARAM_BANKACCOUNTTO As String = "@bankaccountto"
    Private Const PARAM_BANKACCOUNTID As String = "@bankaccountid"
    Private Const PARAM_PAYMENTTYPEID As String = "@paymenttypeid"
    Private Const PARAM_REQUESTBY As String = "@requestby"
    Private Const PARAM_APBALANCE As String = "@apbalance"
    Private Const PARAM_PAYMENTAMOUNT As String = "@paymentamount"
    Private Const PARAM_ISCHANGE As String = "@IsChange"
    Private Const PARAM_LASTAPBALANCE As String = "@LastAPBalance"
    Private Const PARAM_PAYMENTNOTE As String = "@PaymentNote"
    Private Const PARAM_JENISTRANSFER As String = "@JenisTransfer"


#Region "AP Disb Select"

    Public Function ListAPSele(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.ListAPSele = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAPDisbSelec, params).Tables(0)

            Return customclass
        Catch exp As Exception
            WriteException("APDisbSelec", "ListAPSele", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function APSelectionList(ByVal customclass As Parameter.APDisbSelec) As System.Data.DataTable

        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_ACCOUNTPAYABLENO, SqlDbType.VarChar, 8000)
            params(0).Value = customclass.AccountPayableNo

            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAPSelection, params).Tables(0)
        Catch exp As Exception
            WriteException("APDisbSelec", "ListAPSele", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function APGroupSelectionList(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter(PARAM_STATUS, SqlDbType.VarChar, 50)
            params(0).Value = customclass.Status

            params(1) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
            params(1).Value = customclass.WhereCond
            customclass.ListAPGroupSelection = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAPGroupSelection, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("APDisbSelec", "APGroupSelectionList", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function SavingToPaymentVoucher(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec
        Dim params() As SqlParameter = New SqlParameter(16) {}
        Dim params2() As SqlParameter = New SqlParameter(5) {}
        Dim params3() As SqlParameter = New SqlParameter(2) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim oTransaction As SqlTransaction

        Dim i As Integer
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oTransaction = oConnection.BeginTransaction
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter(PARAM_DUEDATE, SqlDbType.DateTime)
            params(2).Value = customclass.DueDate

            params(3) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
            params(3).Value = customclass.ApplicationID

            params(4) = New SqlParameter(PARAM_PVAMOUNT, SqlDbType.Decimal)
            params(4).Value = customclass.PVAmount

            params(5) = New SqlParameter(PARAM_APTYPE, SqlDbType.VarChar, 10)
            params(5).Value = customclass.APType

            params(6) = New SqlParameter(PARAM_APTO, SqlDbType.VarChar, 50)
            params(6).Value = customclass.APTo

            params(7) = New SqlParameter(PARAM_ACCOUNTNAMETO, SqlDbType.VarChar, 50)
            params(7).Value = customclass.AccountNameTo

            params(8) = New SqlParameter(PARAM_ACCOUNTNOTO, SqlDbType.VarChar, 50)
            params(8).Value = customclass.AccountNoTo
            params(9) = New SqlParameter(PARAM_BANKNAMETO, SqlDbType.VarChar, 50)
            params(9).Value = customclass.BankNameTo
            params(10) = New SqlParameter(PARAM_BANKBRANCHTO, SqlDbType.VarChar, 50)
            params(10).Value = customclass.BankBranchTo

            params(11) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(11).Value = customclass.BankAccountID
            params(12) = New SqlParameter(PARAM_PAYMENTTYPEID, SqlDbType.VarChar, 2)
            params(12).Value = customclass.PaymentTypeID
            params(13) = New SqlParameter(PARAM_REQUESTBY, SqlDbType.VarChar, 12)
            params(13).Value = customclass.LoginId
            params(14) = New SqlParameter(PARAM_PAYMENTVOUCHERNO, SqlDbType.VarChar, 20)
            params(14).Direction = ParameterDirection.Output

            params(15) = New SqlParameter(PARAM_PAYMENTNOTE, SqlDbType.VarChar, 35)
            params(15).Value = customclass.PaymentNote
            params(16) = New SqlParameter(PARAM_JENISTRANSFER, SqlDbType.VarChar, 4)
            params(16).Value = customclass.JenisTransfer

            SqlHelper.ExecuteNonQuery(oTransaction, CommandType.StoredProcedure, spAPSaveToPV, params)

            params2(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params2(1) = New SqlParameter(PARAM_PAYMENTVOUCHERNO, SqlDbType.VarChar, 20)
            params2(2) = New SqlParameter(PARAM_ACCOUNTPAYABLENO, SqlDbType.VarChar, 20)
            params2(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params2(4) = New SqlParameter(PARAM_APBALANCE, SqlDbType.Decimal)
            params2(5) = New SqlParameter(PARAM_PAYMENTAMOUNT, SqlDbType.Decimal)

            params3(0) = New SqlParameter(PARAM_ACCOUNTPAYABLENO, SqlDbType.VarChar, 20)
            params3(1) = New SqlParameter(PARAM_LASTAPBALANCE, SqlDbType.Decimal)
            params3(2) = New SqlParameter(PARAM_ISCHANGE, SqlDbType.Bit)

            For i = 0 To customclass.APDetail.Rows.Count - 1
                params3(0).Value = customclass.APDetail.Rows(i).Item("AccountPayableNo")
                params3(1).Value = customclass.APDetail.Rows(i).Item("APBalance")
                params3(2).Direction = ParameterDirection.Output
                SqlHelper.ExecuteNonQuery(oTransaction, CommandType.StoredProcedure, spSaveAPCheckAPBalance, params3)
                If Not CBool(params3(2).Value) Then
                    params2(0).Value = customclass.BranchId
                    params2(1).Value = CStr(params(14).Value)
                    params2(2).Value = customclass.APDetail.Rows(i).Item("AccountPayableNo")
                    params2(3).Value = customclass.BusinessDate
                    params2(4).Value = customclass.APDetail.Rows(i).Item("APBalance")
                    params2(5).Value = customclass.APDetail.Rows(i).Item("PaymentAmount")
                    SqlHelper.ExecuteNonQuery(oTransaction, CommandType.StoredProcedure, spAPSaveToPVItem, params2)
                    customclass.StrError = ""
                Else
                    customclass.StrError = "AP Already execute by another user, please restart transaction again"
                    oTransaction.Rollback()
                    Return customclass
                    Exit Function
                End If
            Next
            oTransaction.Commit()
            Return customclass
        Catch exp As Exception
            oTransaction.Rollback()
            WriteException("APDisbSelec", "SavingToPaymentVoucher", exp.Message + exp.StackTrace)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try

    End Function

#End Region

#Region "APChangeLoc"
    Public Sub SaveChangeLoc(ByVal customClass As Parameter.APDisbSelec)
        Dim oConnection As New SqlConnection(customClass.strConnection)
        Dim objTransaction As SqlTransaction
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        objTransaction = oConnection.BeginTransaction
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = customClass.BranchId

        params(1) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
        params(1).Value = customClass.WhereCond

        params(2) = New SqlParameter("@paymentLocation", SqlDbType.VarChar, 3)
        params(2).Value = customClass.PaymentLocation


        Try
            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spSaveChangeLoc, params)
            objTransaction.Commit()
        Catch exp As Exception
            objTransaction.Rollback()
            WriteException("APDisbSelec", "SaveChangeLoc", exp.Message + exp.StackTrace)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Sub

#End Region
    Public Function ListEBanking(ByVal customclass As Parameter.APDisbSelec) As System.Data.DataTable

        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListEBanking, params).Tables(0)
        Catch exp As Exception
            WriteException("APDisbSelec", "ListEBanking", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
