


#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class BankReconcile : Inherits DataAccessBase

    Private Const spListBankRecon As String = "spBankReconListPaging"
    Private Const spListBankReconDet As String = "spBankReconDetPaging"
    Private Const spSaveBankRecon As String = "spBankReconSave"
    Private Const spInfoBalance As String = "spInfoBalance"

    Public Function ListPDCDeposit(ByVal customclass As Parameter.BankReconcile) As Parameter.BankReconcile

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.listBankRecon = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListBankRecon, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)

            Return customclass
        Catch exp As Exception
            WriteException("BankReconcile", "ListPDCDeposit", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListPDCDepositDet(ByVal customclass As Parameter.BankReconcile) As Parameter.BankReconcile
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy
            customclass.listBankReconDet = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListBankReconDet, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("BankReconcile", "ListPDCDepositDet", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub SaveBankRecon(ByVal customclass As Parameter.BankReconcile)
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            params(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@businessdate", SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter("@Voucherno", SqlDbType.VarChar, 8000)
            params(2).Value = customclass.GroupVoucherno

            params(3) = New SqlParameter("@JournalNo", SqlDbType.VarChar, 8000)
            params(3).Value = customclass.GroupJournalno

            params(4) = New SqlParameter("@reconcileby", SqlDbType.VarChar, 20)
            params(4).Value = customclass.LoginId

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spSaveBankRecon, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("BankReconcile", "SaveBankRecon", exp.Message + exp.StackTrace)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()

        End Try
    End Sub
    Public Function InfoReconBalance(ByVal customclass As Parameter.BankReconcile) As Parameter.BankReconcile
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim objRead As SqlDataReader
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 20)
            params(1).Value = customclass.BankAccountID.Trim

            objRead = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spInfoBalance, params)
            With customclass
                If objRead.Read Then

                    .DebitAmount = CType(objRead("DebitAmount"), Double)
                    .CreditAmount = CType(objRead("CreditAmount"), Double)
                    .BeforeBalance = CType(objRead("BeforeBalance"), Double)
                    .CurrentBalance = CType(objRead("CurrentBalance"), Double)

                End If
            End With
            objRead.Close()
            Return customclass
            ' customclass.listBankReconDet = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInfoBalance, params).Tables(0)

        Catch exp As Exception
            WriteException("BankReconcile", "InfoBalance", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
