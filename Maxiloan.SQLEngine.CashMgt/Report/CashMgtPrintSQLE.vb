Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Public Class CashMgtPrintSQLE : Inherits DataAccessBase
    Dim spGetInstallmentDueDate As String = "spGetInstallmentDueDate"

    Public Function execGetInstallmentDueDate(ByVal customClass As Parameter.InstallmentDueDate) As Parameter.InstallmentDueDate
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter("@BranchID", SqlDbType.char, 3)
        params(0).Value = customClass.BranchId        
        params(1) = New SqlParameter("@StartDate", SqlDbType.DateTime)
        params(1).Value = customClass.StartDate
        params(2) = New SqlParameter("@EndDate", SqlDbType.DateTime)
        params(2).Value = customClass.EndDate

        customClass.Dataset = SqlHelper.ExecuteDataset(customClass.strConnection, _
        CommandType.StoredProcedure, spGetInstallmentDueDate, params)

        Return customClass
    End Function

    Public Function execGetInstallmentDueDateCollection(ByVal customClass As Parameter.InstallmentDueDate) As Parameter.InstallmentDueDate
        Dim params() As SqlParameter = New SqlParameter(3) {}

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@StartDate", SqlDbType.DateTime)
        params(1).Value = customClass.StartDate
        params(2) = New SqlParameter("@EndDate", SqlDbType.DateTime)
        params(2).Value = customClass.EndDate
        params(3) = New SqlParameter("@CGID", SqlDbType.Char, 3)
        params(3).Value = customClass.SelectedCGID

        customClass.Dataset = SqlHelper.ExecuteDataset(customClass.strConnection, _
        CommandType.StoredProcedure, "spInstallmentDueDateCollection", params)

        Return customClass
    End Function
End Class
