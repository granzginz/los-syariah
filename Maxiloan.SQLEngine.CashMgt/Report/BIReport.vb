

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class BIReport : Inherits DataAccessBase
    Private Const PARAM_MONTH As String = "@Bulan"
    Private Const PARAM_YEAR As String = "@tahun"
    Private Const SPBIIREPORTTABLE As String = "spBIReportTable"
    Private Const SPBIIREPORTCETAK As String = "spBIReportCetak"

    Public Sub BIReportSave(ByVal strConnection As String, ByVal Tahun As String, ByVal Bulan As String)
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim objcon As New SqlConnection(strConnection)
        Dim objtrans As SqlTransaction
        params(0) = New SqlParameter(PARAM_MONTH, SqlDbType.VarChar, 2)
        params(0).Value = Bulan

        params(1) = New SqlParameter(PARAM_YEAR, SqlDbType.VarChar, 4)
        params(1).Value = Tahun
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPBIIREPORTTABLE, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("BIReport", "BIReportSave", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Sub

    Public Function BIReportViewer(ByVal strConnection As String, ByVal Tahun As String, ByVal Bulan As String) As DataSet
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter(PARAM_MONTH, SqlDbType.VarChar, 2)
        params(0).Value = Bulan

        params(1) = New SqlParameter(PARAM_YEAR, SqlDbType.VarChar, 4)
        params(1).Value = Tahun
        Try
            Return SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, SPBIIREPORTCETAK, params)
        Catch exp As Exception
            WriteException("BIReport", "BIReportViewer", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function BIReportSemiAnual(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim params() As SqlParameter = New SqlParameter(3) {}
        With customclass
            params(0) = New SqlParameter("@BulanFrom", SqlDbType.VarChar, 2)
            params(0).Value = .BulanFrom

            params(1) = New SqlParameter("@BulanTo", SqlDbType.VarChar, 2)
            params(1).Value = .BulanTo

            params(2) = New SqlParameter("@TahunFrom", SqlDbType.VarChar, 4)
            params(2).Value = .TahunFrom

            params(3) = New SqlParameter("@TahunTo", SqlDbType.VarChar, 4)
            params(3).Value = .TahunTo

        End With

        Try
            customclass.ListAPReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spBIReportSemiAnual", params)
            Return customclass
        Catch exp As Exception
            WriteException("BIReport", "BIReportViewer", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
End Class
