

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class TransferForReimbursePC : Inherits DataAccessBase
    Private Const spListTransferReimbursePC As String = "spTransferForReimbursePCPaging"
    Private Const spListTransferReimbursePCOtorisasi As String = "spTransferForReimbursePCOtorisasiPaging"

    Private Const spListBranchHO As String = "spListBranchHO"
    Private Const spListBankAccount As String = "spListBankAccountBranch"
    Private Const spListBankAccount2 As String = "spListBankAccountBranch2"
    Private Const spListPettyCash As String = "spListPettyCash"
    Private Const spListPettyCashApr As String = "spListPettyCashApr"
    Private Const spListPettyCashOtorisasi As String = "spListPettyCashOtorisasi"

    Private Const spListBilyetGiro As String = "spListBilyetGiro"
    Private Const spSaveReimburcePC As String = "spTransferForReimbursePCSave"
    Private Const spSaveReimburcePCDtl As String = "spTransferForReimbursePCDetailSave"
    Private Const spCheckEndingBalance As String = "spCheckEndingBalance"

    Private Const spSaveReimburcePCApprove As String = "spTransferForReimbursePCSaveApprove"

    Private Const PARAM_REQUESTNO As String = "@RequestNo"
    Private Const PARAM_BANKACCOUNTID As String = "@BankAccountID"
    Private Const PARAM_REFERENCENO As String = "@ReferenceNo"
    Private Const PARAM_BANKACCOUNTFROM As String = "@BankAccountFrom"
    Private Const PARAM_BRANCHIDTO As String = "@BranchIDTo"
    Private Const PARAM_AMOUNT As String = "@Amount"
    Private Const PARAM_NOTES As String = "@Notes"
    Private Const PARAM_BANKTYPE As String = "@BankType"
    Private Const PARAM_BGNO As String = "@BGNo"
    Private Const PARAM_DUEDATE As String = "@DueDate"
    Private Const PARAM_PETTYCASHNO As String = "@PettyCashNo"
    Private Const PARAM_ISTRANSFER As String = "@IsTransfer"
    Private Const PARAM_STR As String = "@str"
    Private Const PARAM_PAYMENTTYPEID As String = "@PaymentTypeID"
    Private Const PARAM_JENISTRANSFER As String = "@JenisTransfer"
    Private Const PARAM_TRANSFERNO As String = "@TransferNo"
    Private Const PARAM_OTORISASI As String = "@Otorisasi"


#Region "List TransferForReimbursePC"
    Public Function ListTransferForReimbursePC(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListTRFund = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spListTransferReimbursePC, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("TransferForReimbursePC", "ListTransferForReimbursePC", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListTransferForReimbursePCOtorisasi(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListTRFund = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spListTransferReimbursePCOtorisasi, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("OtorisasiTransferForReimbursePC", "ListTransferForReimbursePCOtorisasi", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "List Branch HO"
    Public Function ListBranchHO(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            customclass.ListTRFund = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spListBranchHO).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("TransferForReimbursePC", "ListBranchHO", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "List Bank Account"
    Public Function ListBankAccount(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            Dim oConnection As New SqlConnection(customclass.strConnection)
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            customclass.ListTRFund = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spListBankAccount, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("TransferForReimbursePC", "ListBankAccount", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListBankAccount2(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            Dim oConnection As New SqlConnection(customclass.strConnection)
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            customclass.ListTRFund = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spListBankAccount2, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("TransferForReimbursePC", "ListBankAccount", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "List PettyCash"
    Public Function ListPettyCash(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_REQUESTNO, SqlDbType.Char, 20)
            params(1).Value = customclass.RequestNo

            params(2) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.SortBy

            customclass.ListTRFund = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spListPettyCash, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("TransferForReimbursePC", "ListPettyCash", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListPettyCashOtorisasi(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_REQUESTNO, SqlDbType.Char, 20)
            params(1).Value = customclass.RequestNo

            params(2) = New SqlParameter(PARAM_TRANSFERNO, SqlDbType.Char, 20)
            params(2).Value = customclass.TransferNo

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = customclass.SortBy

            customclass.ListTRFund = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spListPettyCashOtorisasi, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("OtorisasiTransferForReimbursePC", "ListPettyCashOtorisasi", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "List Bilyet Giro"
    Public Function ListBilyetGiro(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.Char, 10)
            params(1).Value = customclass.BankAccountID

            customclass.ListTRFund = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spListBilyetGiro, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("TransferForReimbursePC", "ListBilyetGiro", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "Save Reimburse PC "
    Public Sub SaveReimbursePC(ByVal customclass As Parameter.TransferForReimbursePC)
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim intloop As Integer
        Dim oTrans As SqlTransaction
        Dim oDatatable As New DataTable
        Dim str As String = Nothing
        Try
            Dim params() As SqlParameter = New SqlParameter(16) {}
            Dim params0() As SqlParameter = New SqlParameter(4) {}

            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oTrans = oConnection.BeginTransaction

            If customclass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params0(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params0(0).Value = customclass.BranchId.Replace("'", "")
                params0(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params0(1).Value = customclass.BankAccountID
                params0(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params0(2).Direction = ParameterDirection.Output
                params0(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params0(3).Value = customclass.BusinessDate
                params0(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params0(4).Value = "K"
                SqlHelper.ExecuteScalar(oTrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params0)
                customclass.ReferenceNo = CStr(params0(2).Value)
            End If

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.Char, 8)
            params(2).Value = customclass.ValueDate

            params(3) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 20)
            params(3).Value = customclass.ReferenceNo

            params(4) = New SqlParameter(PARAM_BANKACCOUNTFROM, SqlDbType.VarChar, 10)
            params(4).Value = customclass.BankAccountFrom

            params(5) = New SqlParameter(PARAM_BRANCHIDTO, SqlDbType.Char, 3)
            params(5).Value = customclass.BranchIDTo

            params(6) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(6).Value = customclass.BankAccountID

            'params(7) = New SqlParameter(PARAM_AMOUNT, SqlDbType.Int)
            'params(7).Value = customclass.Amount
            params(7) = New SqlParameter(PARAM_AMOUNT, SqlDbType.Decimal)
            params(7).Value = customclass.Amount

            params(8) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(8).Value = customclass.Notes

            params(9) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 20)
            params(9).Value = customclass.LoginId

            params(10) = New SqlParameter(PARAM_BANKTYPE, SqlDbType.VarChar, 5)
            params(10).Value = customclass.BankType

            params(11) = New SqlParameter(PARAM_BGNO, SqlDbType.VarChar, 20)
            params(11).Value = customclass.BGNo

            params(12) = New SqlParameter(PARAM_DUEDATE, SqlDbType.Char, 10)
            If customclass.DueDate <> "" Then
                params(12).Value = customclass.DueDate
            Else
                params(12).Value = DBNull.Value
            End If

            params(13) = New SqlParameter(PARAM_REQUESTNO, SqlDbType.VarChar, 20)
            params(13).Value = customclass.RequestNo

            params(14) = New SqlParameter(PARAM_COMPANYID, SqlDbType.Char, 3)
            params(14).Value = customclass.CompanyID

            params(15) = New SqlParameter(PARAM_PAYMENTTYPEID, SqlDbType.Char, 2)
            params(15).Value = customclass.PaymentTypeID

            params(16) = New SqlParameter(PARAM_JENISTRANSFER, SqlDbType.VarChar, 4)
            params(16).Value = customclass.JenisTransfer

            SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSaveReimburcePC, params)
            Dim paramsdtl() As SqlParameter = New SqlParameter(4) {}
            oDatatable = customclass.ListTRFund
            paramsdtl(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            paramsdtl(1) = New SqlParameter(PARAM_REQUESTNO, SqlDbType.VarChar, 20)
            paramsdtl(2) = New SqlParameter(PARAM_PETTYCASHNO, SqlDbType.VarChar, 20)
            paramsdtl(3) = New SqlParameter(PARAM_ISTRANSFER, SqlDbType.Bit)
            paramsdtl(4) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            For intloop = 0 To oDatatable.Rows.Count - 1
                paramsdtl(0).Value = customclass.BranchIDTo
                paramsdtl(1).Value = customclass.RequestNo
                paramsdtl(2).Value = oDatatable.Rows(intloop)("PettyCashNo")
                paramsdtl(3).Value = oDatatable.Rows(intloop)("IsTransfer")
                paramsdtl(4).Value = oDatatable.Rows(intloop)("Notes")
                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSaveReimburcePCDtl, paramsdtl)
            Next
            oTrans.Commit()
        Catch exp As Exception
            oTrans.Rollback()
            WriteException("TransferForReimbursePC", "SaveReimbursePC", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Sub

    Public Sub SaveReimbursePCOtorisasi(ByVal customclass As Parameter.TransferForReimbursePC)
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim oTrans As SqlTransaction
        Dim oDatatable As New DataTable
        Dim str As String = Nothing
        Try
            Dim params() As SqlParameter = New SqlParameter(6) {}

            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oTrans = oConnection.BeginTransaction

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter(PARAM_TRANSFERNO, SqlDbType.VarChar, 20)
            params(2).Value = customclass.TransferNo

            params(3) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 20)
            params(3).Value = customclass.ReferenceNo

            params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 20)
            params(4).Value = customclass.LoginId

            params(5) = New SqlParameter(PARAM_OTORISASI, SqlDbType.VarChar, 1)
            params(5).Value = customclass.Otorisasi

            params(6) = New SqlParameter(PARAM_COMPANYID, SqlDbType.Char, 3)
            params(6).Value = customclass.CompanyID

            SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSaveReimburcePCApprove, params)

            oTrans.Commit()
        Catch exp As Exception
            oTrans.Rollback()
            WriteException("OtorisasiTransferForReimbursePC", "SaveReimbursePCApprove", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Sub
#End Region

    Public Function CheckEndingBalance(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 20)
            params(1).Value = customclass.BankAccountID

            'params(2) = New SqlParameter(PARAM_AMOUNT, SqlDbType.Int)
            'params(2).Value = customclass.Amount
            params(2) = New SqlParameter(PARAM_AMOUNT, SqlDbType.Decimal)
            params(2).Value = customclass.Amount

            params(3) = New SqlParameter(PARAM_STR, SqlDbType.VarChar, 20)
            params(3).Direction = ParameterDirection.Output

            SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spCheckEndingBalance, params)
            customclass.str = CType(params(3).Value, String)
            Return customclass
        Catch exp As Exception
            WriteException("TransferForReimbursePC", "CheckEndingBalance", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function EbankPCREIAdd(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim intloop As Integer
        Dim oTrans As SqlTransaction = Nothing
        Dim oDatatable As New DataTable
        Dim oClass As New Parameter.TransferForReimbursePC

        Try
            Dim params() As SqlParameter = New SqlParameter(13) {}
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()

            oTrans = oConnection.BeginTransaction

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.Char, 8)
            params(2).Value = customclass.ValueDate

            params(3) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 20)
            params(3).Value = customclass.ReferenceNo

            params(4) = New SqlParameter(PARAM_BANKACCOUNTFROM, SqlDbType.VarChar, 10)
            params(4).Value = customclass.BankAccountFrom

            params(5) = New SqlParameter(PARAM_BRANCHIDTO, SqlDbType.Char, 3)
            params(5).Value = customclass.BranchIDTo

            params(6) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(6).Value = customclass.BankAccountID

            params(7) = New SqlParameter(PARAM_AMOUNT, SqlDbType.Int)
            params(7).Value = customclass.Amount

            params(8) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(8).Value = customclass.Notes

            params(9) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 20)
            params(9).Value = customclass.LoginId

            params(10) = New SqlParameter(PARAM_REQUESTNO, SqlDbType.VarChar, 20)
            params(10).Value = customclass.RequestNo

            params(11) = New SqlParameter(PARAM_COMPANYID, SqlDbType.Char, 3)
            params(11).Value = customclass.CompanyID

            params(12) = New SqlParameter(PARAM_PAYMENTTYPEID, SqlDbType.Char, 2)
            params(12).Value = customclass.PaymentTypeID

            params(13) = New SqlParameter(PARAM_JENISTRANSFER, SqlDbType.VarChar, 4)
            params(13).Value = customclass.JenisTransfer

            SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, "spEBankPCREIAdd", params)

            Dim paramsdtl() As SqlParameter = New SqlParameter(4) {}

            oDatatable = customclass.ListTRFund
            paramsdtl(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            paramsdtl(1) = New SqlParameter(PARAM_REQUESTNO, SqlDbType.VarChar, 20)
            paramsdtl(2) = New SqlParameter(PARAM_PETTYCASHNO, SqlDbType.VarChar, 20)
            paramsdtl(3) = New SqlParameter(PARAM_ISTRANSFER, SqlDbType.Bit)
            paramsdtl(4) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)

            For intloop = 0 To oDatatable.Rows.Count - 1
                paramsdtl(0).Value = customclass.BranchIDTo
                paramsdtl(1).Value = customclass.RequestNo
                paramsdtl(2).Value = oDatatable.Rows(intloop)("PettyCashNo")
                paramsdtl(3).Value = oDatatable.Rows(intloop)("IsTransfer")
                paramsdtl(4).Value = oDatatable.Rows(intloop)("Notes")

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSaveReimburcePCDtl, paramsdtl)
            Next
            oTrans.Commit()
            oClass.str = ""
        Catch exp As Exception
            oTrans.Rollback()
            WriteException("TransferForReimbursePC", "SaveReimbursePC", exp.Message + exp.StackTrace)
            oClass.str = exp.Message
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try

        Return oClass
    End Function

    Public Function ListPettyCashApr(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_REQUESTNO, SqlDbType.Char, 20)
            params(1).Value = customclass.RequestNo

            params(2) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.SortBy

            customclass.ListTRFund = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spListPettyCashApr, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("TransferForReimbursePC", "ListPettyCashApr", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
