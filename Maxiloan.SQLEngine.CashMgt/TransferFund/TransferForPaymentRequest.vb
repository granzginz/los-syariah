

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class TransferForPaymentRequest : Inherits DataAccessBase
    Private Const spListRequest As String = "spTransferForPaymentRequestPaging"
    Private Const spListRequestOtorisasi As String = "spTransferForPaymentRequestOtorisasiPaging"

    Private Const spListPaymentDetail As String = "spListPaymentRequestDtl"
    Private Const spListPaymentDetailOtorisasi As String = "spListPaymentRequestOtorisasiDtl"

    Private Const spSavePaymentRequest As String = "spTransferForPaymentRequestSave"
    Private Const spSavePaymentRequestOtorisasi As String = "spTransferForPaymentRequestSaveApprove"

    Private Const spSavePaymentRequestDtl As String = "spTransferPaymentRequestDtlSave"

    Private Const PARAM_REQUESTNO As String = "@RequestNo"
    Private Const PARAM_BANKACCOUNTID As String = "@BankAccountID"
    Private Const PARAM_REFERENCENO As String = "@ReferenceNo"
    Private Const PARAM_BANKACCOUNTFROM As String = "@BankAccountFrom"
    Private Const PARAM_BRANCHIDTO As String = "@BranchIDTo"
    Private Const PARAM_AMOUNT As String = "@Amount"
    Private Const PARAM_NOTES As String = "@Notes"
    Private Const PARAM_BANKTYPE As String = "@BankType"
    Private Const PARAM_BGNO As String = "@BGNo"
    Private Const PARAM_DUEDATE As String = "@DueDate"
    Private Const PARAM_PETTYCASHNO As String = "@PettyCashNo"
    Private Const PARAM_ISTRANSFER As String = "@IsTransfer"
    Private Const PARAM_PAYMENTTYPEID As String = "@PaymentTypeID"
    Private Const PARAM_JENISTRANSFER As String = "@JenisTransfer"
    Private Const PARAM_TRANSFERNO As String = "@TransferNo"
    Private Const PARAM_OTORISASI As String = "@Otorisasi"

    Private Const PARAM_PAYMENTALLOCATIONID As String = "@PaymentAllocationID"
    Private Const PARAM_STR As String = "@str"
    Private Const spListPaymentHistoryReject As String = "spListPaymentRequestHistoryReject"


#Region "List TransferForPaymentRequest"
    Public Function ListRequest(ByVal customclass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListRequest = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spListRequest, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("TransferForPaymentRequest", "ListRequest", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function ListRequestOtorisasi(ByVal customclass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListRequest = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spListRequestOtorisasi, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("OtorisasiTransferForPaymentRequest", "ListRequestOtorisasi", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region

#Region "List PaymentDetail"
    Public Function ListPaymentDetail(ByVal customclass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_REQUESTNO, SqlDbType.Char, 20)
            params(1).Value = customclass.RequestNo

            params(2) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.SortBy

            customclass.ListRequest = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spListPaymentDetail, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("TransferForPaymentRequest", "ListPaymentDetail", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function ListPaymentDetailOtorisasi(ByVal customclass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_REQUESTNO, SqlDbType.Char, 20)
            params(1).Value = customclass.RequestNo

            params(2) = New SqlParameter(PARAM_TRANSFERNO, SqlDbType.Char, 20)
            params(2).Value = customclass.TransferNo

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = customclass.SortBy

            customclass.ListRequest = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spListPaymentDetailOtorisasi, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("OtorisasiTransferForPaymentRequest", "ListPaymentDetailOtorisasi", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region

#Region "Save Payment Request"
    Public Sub SavePaymentRequest(ByVal customclass As Parameter.TransferForPaymentRequest)
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim intloop As Integer
        Dim oTrans As SqlTransaction
        Dim oDatatable As New DataTable        
        Try
            Dim params() As SqlParameter = New SqlParameter(16) {}
            Dim params0() As SqlParameter = New SqlParameter(4) {}           

            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oTrans = oConnection.BeginTransaction

            If customclass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params0(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params0(0).Value = customclass.BranchId.Replace("'", "")
                params0(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params0(1).Value = customclass.BankAccountID
                params0(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params0(2).Direction = ParameterDirection.Output
                params0(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params0(3).Value = customclass.BusinessDate
                params0(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params0(4).Value = "K"
                SqlHelper.ExecuteScalar(oTrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params0)
                customclass.ReferenceNo = CStr(params0(2).Value)
            End If


            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.Char, 8)
            params(2).Value = customclass.ValueDate

            params(3) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 20)
            params(3).Value = customclass.ReferenceNo

            params(4) = New SqlParameter(PARAM_BANKACCOUNTFROM, SqlDbType.VarChar, 10)
            params(4).Value = customclass.BankAccountFrom

            params(5) = New SqlParameter(PARAM_BRANCHIDTO, SqlDbType.Char, 3)
            params(5).Value = customclass.BranchIDTo

            params(6) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(6).Value = customclass.BankAccountID

            params(7) = New SqlParameter(PARAM_AMOUNT, SqlDbType.Money)
            params(7).Value = customclass.Amount

            params(8) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(8).Value = customclass.Notes

            params(9) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 20)
            params(9).Value = customclass.LoginId

            params(10) = New SqlParameter(PARAM_BANKTYPE, SqlDbType.VarChar, 5)
            params(10).Value = customclass.BankType

            params(11) = New SqlParameter(PARAM_BGNO, SqlDbType.VarChar, 20)
            params(11).Value = customclass.BGNo

            params(12) = New SqlParameter(PARAM_DUEDATE, SqlDbType.Char, 8)

            If customclass.DueDate <> "" Then
                params(12).Value = customclass.DueDate
            Else
                params(12).Value = DBNull.Value
            End If

            params(13) = New SqlParameter(PARAM_REQUESTNO, SqlDbType.VarChar, 20)
            params(13).Value = customclass.RequestNo

            params(14) = New SqlParameter(PARAM_COMPANYID, SqlDbType.Char, 3)
            params(14).Value = customclass.CompanyID

            params(15) = New SqlParameter(PARAM_PAYMENTTYPEID, SqlDbType.Char, 2)
            params(15).Value = customclass.PaymentTypeID

            params(16) = New SqlParameter(PARAM_JENISTRANSFER, SqlDbType.VarChar, 4)
            params(16).Value = customclass.JenisTransfer

            SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePaymentRequest, params)

            'Detail save on approval / authorization process
            Dim paramsdtl() As SqlParameter = New SqlParameter(4) {}
            paramsdtl(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            paramsdtl(1) = New SqlParameter(PARAM_REQUESTNO, SqlDbType.VarChar, 20)
            'paramsdtl(2) = New SqlParameter(PARAM_PAYMENTALLOCATIONID, SqlDbType.Char, 10)
            paramsdtl(2) = New SqlParameter("@SequenceNo", SqlDbType.Int)
            paramsdtl(3) = New SqlParameter(PARAM_AMOUNT, SqlDbType.Money)
            paramsdtl(4) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 100)
            oDatatable = customclass.ListRequest
            For intloop = 0 To oDatatable.Rows.Count - 1
                paramsdtl(0).Value = customclass.BranchIDTo
                paramsdtl(1).Value = customclass.RequestNo
                'paramsdtl(2).Value = oDatatable.Rows(intloop)("PaymentAllocationID")
                paramsdtl(2).Value = CInt(oDatatable.Rows(intloop)("SequenceNo"))
                paramsdtl(3).Value = oDatatable.Rows(intloop)("Amount")
                paramsdtl(4).Value = oDatatable.Rows(intloop)("Notes")
                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePaymentRequestDtl, paramsdtl)
            Next
            oTrans.Commit()
        Catch exp As Exception
            oTrans.Rollback()
            WriteException("TransferForPaymentRequest", "ListPaymentDetail", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Sub

    Public Sub SavePaymentRequestOtorisasi(ByVal customclass As Parameter.TransferForPaymentRequest)
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim intloop As Integer
        Dim oTrans As SqlTransaction
        Dim oDatatable As New DataTable
        Try
            Dim params() As SqlParameter = New SqlParameter(6) {}
            Dim params0() As SqlParameter = New SqlParameter(4) {}

            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oTrans = oConnection.BeginTransaction

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter(PARAM_TRANSFERNO, SqlDbType.VarChar, 20)
            params(2).Value = customclass.TransferNo

            params(3) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 20)
            params(3).Value = customclass.ReferenceNo

            params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 20)
            params(4).Value = customclass.LoginId

            params(5) = New SqlParameter(PARAM_OTORISASI, SqlDbType.VarChar, 1)
            params(5).Value = customclass.Otorisasi

            params(6) = New SqlParameter(PARAM_COMPANYID, SqlDbType.Char, 3)
            params(6).Value = customclass.CompanyID

            SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePaymentRequestOtorisasi, params)

            'Dim paramsdtl() As SqlParameter = New SqlParameter(4) {}
            'paramsdtl(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            'paramsdtl(1) = New SqlParameter(PARAM_REQUESTNO, SqlDbType.VarChar, 20)
            ''paramsdtl(2) = New SqlParameter(PARAM_PAYMENTALLOCATIONID, SqlDbType.Char, 10)
            'paramsdtl(2) = New SqlParameter("@SequenceNo", SqlDbType.Int)
            'paramsdtl(3) = New SqlParameter(PARAM_AMOUNT, SqlDbType.Money)
            'paramsdtl(4) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 100)
            'oDatatable = customclass.ListRequest
            'For intloop = 0 To oDatatable.Rows.Count - 1
            '    paramsdtl(0).Value = customclass.BranchIDTo
            '    paramsdtl(1).Value = customclass.RequestNo
            '    'paramsdtl(2).Value = oDatatable.Rows(intloop)("PaymentAllocationID")
            '    paramsdtl(2).Value = CInt(oDatatable.Rows(intloop)("SequenceNo"))
            '    paramsdtl(3).Value = oDatatable.Rows(intloop)("Amount")
            '    paramsdtl(4).Value = oDatatable.Rows(intloop)("Notes")
            '    SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePaymentRequestDtl, paramsdtl)
            'Next
            oTrans.Commit()
        Catch exp As Exception
            oTrans.Rollback()
            WriteException("TransferForPaymentRequest", "ListPaymentDetail", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Sub

#End Region

    Public Function EbankPYREQAdd(ByVal customclass As Parameter.TransferForPaymentRequest) As String
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Dim intloop As Integer
        Dim oTrans As SqlTransaction = Nothing
        Dim oDatatable As New DataTable

        Try
            Dim params() As SqlParameter = New SqlParameter(13) {}

            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oTrans = oConnection.BeginTransaction

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = customclass.BusinessDate

            params(2) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.Char, 8)
            params(2).Value = customclass.ValueDate

            params(3) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.VarChar, 20)
            params(3).Value = customclass.ReferenceNo

            params(4) = New SqlParameter(PARAM_BANKACCOUNTFROM, SqlDbType.VarChar, 10)
            params(4).Value = customclass.BankAccountFrom

            params(5) = New SqlParameter(PARAM_BRANCHIDTO, SqlDbType.Char, 3)
            params(5).Value = customclass.BranchIDTo

            params(6) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(6).Value = customclass.BankAccountID

            params(7) = New SqlParameter(PARAM_AMOUNT, SqlDbType.Money)
            params(7).Value = customclass.Amount

            params(8) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 50)
            params(8).Value = customclass.Notes

            params(9) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 20)
            params(9).Value = customclass.LoginId

            params(10) = New SqlParameter(PARAM_REQUESTNO, SqlDbType.VarChar, 20)
            params(10).Value = customclass.RequestNo

            params(11) = New SqlParameter(PARAM_COMPANYID, SqlDbType.Char, 3)
            params(11).Value = customclass.CompanyID

            params(12) = New SqlParameter(PARAM_PAYMENTTYPEID, SqlDbType.Char, 2)
            params(12).Value = customclass.PaymentTypeID

            params(13) = New SqlParameter(PARAM_JENISTRANSFER, SqlDbType.VarChar, 4)
            params(13).Value = customclass.JenisTransfer

            SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, "spEbankPYREQAdd", params)

            Dim paramsdtl() As SqlParameter = New SqlParameter(4) {}

            paramsdtl(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            paramsdtl(1) = New SqlParameter(PARAM_REQUESTNO, SqlDbType.VarChar, 20)
            paramsdtl(2) = New SqlParameter("@SequenceNo", SqlDbType.Int)
            paramsdtl(3) = New SqlParameter(PARAM_AMOUNT, SqlDbType.Money)
            paramsdtl(4) = New SqlParameter(PARAM_NOTES, SqlDbType.VarChar, 100)

            oDatatable = customclass.ListRequest

            For intloop = 0 To oDatatable.Rows.Count - 1
                paramsdtl(0).Value = customclass.BranchIDTo
                paramsdtl(1).Value = customclass.RequestNo
                paramsdtl(2).Value = CInt(oDatatable.Rows(intloop)("SequenceNo"))
                paramsdtl(3).Value = oDatatable.Rows(intloop)("Amount")
                paramsdtl(4).Value = oDatatable.Rows(intloop)("Notes")

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePaymentRequestDtl, paramsdtl)
            Next

            oTrans.Commit()
            Return ""
        Catch exp As Exception
            oTrans.Rollback()
            Throw New Exception(exp.Message)
            Return exp.Message
        End Try
    End Function

    Public Function ListPaymentHistoryReject(ByVal customclass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim oConnection As New SqlConnection(customclass.strConnection)
        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_REQUESTNO, SqlDbType.Char, 20)
            params(1).Value = customclass.RequestNo

            params(2) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.SortBy

            customclass.ListRequest = SqlHelper.ExecuteDataset(oConnection, CommandType.StoredProcedure, spListPaymentHistoryReject, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("TransferForPaymentRequest", "ListPaymentHistoryReject", exp.Message + exp.StackTrace)
        End Try

    End Function

End Class
