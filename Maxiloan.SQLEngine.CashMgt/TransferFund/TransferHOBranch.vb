

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class TransferHOBranch : Inherits DataAccessBase
    Private Const spGetBankType As String = "spBankAccountType"
    Private Const spSaveTRHOBranchD As String = "spTransferHOBranch"
    Private Const spSaveTRHOBranch As String = "spTransferFundHOBranch"

    Public Function GetBankType(ByVal customclass As Parameter.TransferAccount) As Parameter.TransferAccount
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@bankaccountid", SqlDbType.Char, 10)
            params(0).Value = customclass.bankaccountid

            params(1) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spGetBankType, params)
            If objread.Read Then
                With customclass
                    .bankAccountType = CType(objread("bankaccounttype"), String)

                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("TransferHOBranch", "GetBankType", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub PenarikanDanaCabangSave(ByVal customclass As Parameter.TransferAccount)
        Dim objtrans As SqlTransaction
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim params0() As SqlParameter = New SqlParameter(4) {}
        Dim params() As SqlParameter = New SqlParameter(11) {}

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            If customclass.referenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params0(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params0(0).Value = customclass.BranchId.Replace("'", "")
                params0(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params0(1).Value = customclass.bankAccountFrom
                params0(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params0(2).Direction = ParameterDirection.Output
                params0(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params0(3).Value = customclass.BusinessDate
                params0(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params0(4).Value = "K"
                SqlHelper.ExecuteScalar(objtrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params0)
                customclass.referenceNo = CStr(params0(2).Value)
            End If

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@bankaccountidTo", SqlDbType.VarChar, 10)
            params(1).Value = customclass.bankAccountTo

            params(2) = New SqlParameter("@bankaccountidfrom", SqlDbType.VarChar, 10)
            params(2).Value = customclass.bankAccountFrom

            params(3) = New SqlParameter("@loginid", SqlDbType.VarChar, 12)
            params(3).Value = customclass.LoginId

            params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(4).Value = customclass.BusinessDate

            params(5) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(5).Value = customclass.Amount

            params(6) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(6).Value = customclass.valueDate

            params(7) = New SqlParameter("@referenceNo", SqlDbType.Char, 20)
            params(7).Value = customclass.referenceNo

            params(8) = New SqlParameter("@Notes", SqlDbType.Char, 50)
            params(8).Value = customclass.Notes

            params(9) = New SqlParameter("@CoyID", SqlDbType.Char, 3)
            params(9).Value = customclass.CompanyID

            params(10) = New SqlParameter("@BranchIDFrom", SqlDbType.VarChar, 3)
            params(10).Value = customclass.BranchIDfrom

            params(11) = New SqlParameter("@BranchIDTo", SqlDbType.Char, 3)
            params(11).Value = customclass.BranchIDTo

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spPenarikanDanaCabang", params)

            objtrans.Commit()
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objcon.Dispose()
        Catch exp As Exception
            objtrans.Rollback()
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objcon.Dispose()
            WriteException("PenarikanDanaCabangSave", "PenarikanDanaCabangSave", exp.Message + exp.StackTrace)
        End Try
    End Sub

    Public Sub OtorisasiPenarikanDanaCabangSave(ByVal customclass As Parameter.TransferAccount)
        Dim objCommand As New SqlCommand
        Dim objcon As New SqlConnection(customclass.strConnection)

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Connection = objcon
            objCommand.CommandText = "spOtorisasiPenarikanDanaCabang"
            objCommand.Parameters.Add("@CompanyID", SqlDbType.Char, 3).Value = customclass.CompanyID
            objCommand.Parameters.Add("@TransferNo", SqlDbType.VarChar, 50).Value = customclass.TransferNO
            objCommand.Parameters.Add("@BusinessDate", SqlDbType.DateTime).Value = customclass.BusinessDate
            objCommand.Parameters.Add("@loginid", SqlDbType.Char, 12).Value = customclass.LoginId
            objCommand.CommandTimeout = 30
            objCommand.ExecuteNonQuery()
        Catch exp As Exception
            WriteException("TransferAccountOtorisasi", "SaveTransferAccountOtorisasi", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Sub

    Public Sub OtorisasiPenarikanDanaCabangReject(ByVal customclass As Parameter.TransferAccount)

        Dim params() As SqlParameter = New SqlParameter(3) {}
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@CompanyID", SqlDbType.Char, 3)
            params(0).Value = customclass.CompanyID
            params(1) = New SqlParameter("@TransferNo", SqlDbType.VarChar, 50)
            params(1).Value = customclass.TransferNO
            params(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(2).Value = customclass.BusinessDate
            params(3) = New SqlParameter("@loginid", SqlDbType.Char, 12)
            params(3).Value = customclass.LoginId


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spOtorisasiPenarikanDanaCabangReject", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("TransferAccountOtorisasi", "SaveTransferAccountOtorisasi", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Sub

    Public Sub SaveTransferHOBranch(ByVal customclass As Parameter.TransferAccount)
        Dim objtrans As SqlTransaction
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim params0() As SqlParameter = New SqlParameter(4) {}
        Dim params() As SqlParameter = New SqlParameter(17) {}
        Dim params2() As SqlParameter = New SqlParameter(16) {}
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            If customclass.referenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params0(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params0(0).Value = customclass.BranchId.Replace("'", "")
                params0(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params0(1).Value = customclass.bankAccountFrom
                params0(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params0(2).Direction = ParameterDirection.Output
                params0(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params0(3).Value = customclass.BusinessDate
                params0(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params0(4).Value = "K"
                SqlHelper.ExecuteScalar(objtrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params0)
                customclass.referenceNo = CStr(params0(2).Value)
            End If

            params(0) = New SqlParameter("@BranchIDTo", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchIDTo

            params(1) = New SqlParameter("@bankaccountidTo", SqlDbType.VarChar, 10)
            params(1).Value = customclass.bankAccountTo

            params(2) = New SqlParameter("@bankaccountidfrom", SqlDbType.VarChar, 10)
            params(2).Value = customclass.bankAccountFrom

            params(3) = New SqlParameter("@loginid", SqlDbType.VarChar, 12)
            params(3).Value = customclass.LoginId

            params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(4).Value = customclass.BusinessDate

            params(5) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(5).Value = customclass.Amount

            params(6) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(6).Value = customclass.valueDate

            params(7) = New SqlParameter("@referenceNo", SqlDbType.Char, 20)
            params(7).Value = customclass.referenceNo

            params(8) = New SqlParameter("@Notes", SqlDbType.Char, 50)
            params(8).Value = customclass.Notes

            params(9) = New SqlParameter("@CoyID", SqlDbType.Char, 3)
            params(9).Value = "001"

            params(10) = New SqlParameter("@bgno", SqlDbType.Char, 20)
            params(10).Value = customclass.bgNo

            params(11) = New SqlParameter("@bgduedate", SqlDbType.DateTime)
            params(11).Value = customclass.bgDate

            params(12) = New SqlParameter("@TransactionIDfrom", SqlDbType.VarChar, 10)
            params(12).Value = customclass.TransactionIDfrom

            params(13) = New SqlParameter("@TransactionIDTo", SqlDbType.VarChar, 10)
            params(13).Value = customclass.TransactionIDTo

            params(14) = New SqlParameter("@branchidfrom", SqlDbType.VarChar, 3)
            params(14).Value = customclass.BranchIDfrom

            params(15) = New SqlParameter("@TransferType", SqlDbType.VarChar, 3)
            params(15).Value = customclass.TransferType


            params(16) = New SqlParameter("@InVoucherNoFrom", SqlDbType.VarChar, 20)
            params(16).Direction = ParameterDirection.Output

            params(17) = New SqlParameter("@InVoucherNoTo", SqlDbType.Char, 20)
            params(17).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spSaveTRHOBranchD, params)

            params2(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
            params2(0).Value = customclass.BranchId.Replace("'", "")

            params2(1) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params2(1).Value = customclass.valueDate

            params2(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params2(2).Value = customclass.BusinessDate

            params2(3) = New SqlParameter("@referenceNo", SqlDbType.Char, 20)
            params2(3).Value = customclass.referenceNo

            params2(4) = New SqlParameter("@bankaccountidfrom", SqlDbType.Char, 10)
            params2(4).Value = customclass.bankAccountFrom

            params2(5) = New SqlParameter("@bankaccountidTo", SqlDbType.Char, 10)
            params2(5).Value = customclass.bankAccountTo

            params2(6) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params2(6).Value = customclass.Amount

            params2(7) = New SqlParameter("@BranchIDFrom", SqlDbType.Char, 3)
            params2(7).Value = customclass.BranchIDfrom

            params2(8) = New SqlParameter("@BranchIDTo", SqlDbType.Char, 3)
            params2(8).Value = customclass.BranchIDTo

            params2(9) = New SqlParameter("@loginid", SqlDbType.Char, 12)
            params2(9).Value = customclass.LoginId

            params2(10) = New SqlParameter("@VoucherNoFrom", SqlDbType.Char, 20)
            params2(10).Value = params(16).Value

            params2(11) = New SqlParameter("@VoucherNoTo", SqlDbType.Char, 20)
            params2(11).Value = params(17).Value

            params2(12) = New SqlParameter("@Notes", SqlDbType.Char, 50)
            params2(12).Value = customclass.Notes

            params2(13) = New SqlParameter("@bgno", SqlDbType.Char, 20)
            params2(13).Value = customclass.bgNo

            params2(14) = New SqlParameter("@TransferType", SqlDbType.VarChar, 10)
            params2(14).Value = customclass.TransferTypeDesc

            params2(15) = New SqlParameter("@PaymentTypeID", SqlDbType.Char, 2)
            params2(15).Value = customclass.PaymentTypeID

            params2(16) = New SqlParameter("@JenisTransfer", SqlDbType.VarChar, 4)
            params2(16).Value = customclass.JenisTransfer

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spSaveTRHOBranch, params2)
            objtrans.Commit()
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objcon.Dispose()
        Catch exp As Exception
            objtrans.Rollback()
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objcon.Dispose()
            WriteException("TransferHOBranch", "SaveTransferHOBranch", exp.Message + exp.StackTrace)
        End Try
    End Sub

    Public Function EBankTRFUNDAdd(ByVal customclass As Parameter.TransferAccount) As Parameter.TransferAccount
        Dim objtrans As SqlTransaction = Nothing
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim params() As SqlParameter = New SqlParameter(11) {}
        Dim params2() As SqlParameter = New SqlParameter(17) {}
        Dim oClass As New Parameter.TransferAccount

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@BranchIDTo", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchIDTo

            params(1) = New SqlParameter("@bankaccountidTo", SqlDbType.VarChar, 10)
            params(1).Value = customclass.bankAccountTo

            params(2) = New SqlParameter("@bankaccountidfrom", SqlDbType.VarChar, 10)
            params(2).Value = customclass.bankAccountFrom

            params(3) = New SqlParameter("@loginid", SqlDbType.VarChar, 12)
            params(3).Value = customclass.LoginId

            params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(4).Value = customclass.BusinessDate

            params(5) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(5).Value = customclass.Amount

            params(6) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(6).Value = customclass.valueDate

            params(7) = New SqlParameter("@referenceNo", SqlDbType.Char, 20)
            params(7).Value = customclass.referenceNo

            params(8) = New SqlParameter("@Notes", SqlDbType.Char, 50)
            params(8).Value = customclass.Notes

            params(9) = New SqlParameter("@CompanyID", SqlDbType.Char, 3)
            params(9).Value = customclass.CompanyID

            params(10) = New SqlParameter("@branchidfrom", SqlDbType.VarChar, 3)
            params(10).Value = customclass.BranchIDfrom

            params(11) = New SqlParameter("@JenisTransfer", SqlDbType.VarChar, 3)
            params(11).Value = customclass.JenisTransfer

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spEBankTRFUNDAdd", params)

            params2(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
            params2(0).Value = customclass.BranchId.Replace("'", "")

            params2(1) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params2(1).Value = customclass.valueDate

            params2(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params2(2).Value = customclass.BusinessDate

            params2(3) = New SqlParameter("@referenceNo", SqlDbType.Char, 20)
            params2(3).Value = customclass.referenceNo

            params2(4) = New SqlParameter("@bankaccountidfrom", SqlDbType.Char, 10)
            params2(4).Value = customclass.bankAccountFrom

            params2(5) = New SqlParameter("@bankaccountidTo", SqlDbType.Char, 10)
            params2(5).Value = customclass.bankAccountTo

            params2(6) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params2(6).Value = customclass.Amount

            params2(7) = New SqlParameter("@BranchIDFrom", SqlDbType.Char, 3)
            params2(7).Value = customclass.BranchIDfrom

            params2(8) = New SqlParameter("@BranchIDTo", SqlDbType.Char, 3)
            params2(8).Value = customclass.BranchIDTo

            params2(9) = New SqlParameter("@loginid", SqlDbType.Char, 12)
            params2(9).Value = customclass.LoginId

            params2(10) = New SqlParameter("@VoucherNoFrom", SqlDbType.Char, 20)
            params2(10).Value = "-"

            params2(11) = New SqlParameter("@VoucherNoTo", SqlDbType.Char, 20)
            params2(11).Value = "-"

            params2(12) = New SqlParameter("@Notes", SqlDbType.Char, 50)
            params2(12).Value = customclass.Notes

            params2(13) = New SqlParameter("@bgno", SqlDbType.Char, 20)
            params2(13).Value = "-"

            params2(14) = New SqlParameter("@TransferType", SqlDbType.VarChar, 10)
            params2(14).Value = customclass.TransferTypeDesc

            params2(15) = New SqlParameter("@PaymentTypeID", SqlDbType.Char, 2)
            params2(15).Value = customclass.PaymentTypeID

            params2(16) = New SqlParameter("@JenisTransfer", SqlDbType.VarChar, 4)
            params2(16).Value = customclass.JenisTransfer

            params2(17) = New SqlParameter("@StatusTransferEBanking", SqlDbType.Char, 1)
            params2(17).Value = "G"

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spSaveTRHOBranch, params2)
            objtrans.Commit()
            oClass.strError = ""
            Return oClass
        Catch exp As Exception
            objtrans.Rollback()
            oClass.strError = exp.Message
            Return oClass
        Finally
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objcon.Dispose()
        End Try
    End Function

End Class
