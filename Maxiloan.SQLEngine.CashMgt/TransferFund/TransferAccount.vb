

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class TransferAccount : Inherits DataAccessBase

    Private Const spGetBankType As String = "spBankAccountType"
    Private Const spSaveTRACCOUNTDisburse As String = "spTransferAccount"
    Private Const spSaveTRACCOUNT As String = "spTransferFundAccount"
    Private Const spInqTransferFund As String = "spInqTransferFund"
    Private Const spViewTransferFund As String = "spViewTransferFund"
    Private Const spRptTransferFund As String = "spRptTransferFund"


    Public Function GetBankType(ByVal customclass As Parameter.TransferAccount) As Parameter.TransferAccount
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@bankaccountid", SqlDbType.Char, 10)
            params(0).Value = customclass.bankaccountid

            params(1) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
            params(1).Value = customclass.BranchId

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, spGetBankType, params)
            If objread.Read Then
                With customclass
                    .bankAccountType = CType(objread("bankaccounttype"), String)

                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("TransferAccount", "GetBankType", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Sub SaveTransferAccount(ByVal customclass As Parameter.TransferAccount)
        'Dim params0() As SqlParameter = New SqlParameter(4) {}
        'Dim params1() As SqlParameter = New SqlParameter(4) {}
        'Dim params() As SqlParameter = New SqlParameter(14) {}
        Dim params2() As SqlParameter = New SqlParameter(16) {}
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            'If customclass.referenceNo = "" Then
            '    ' ambil reference no otomatis jika no reference blank
            '    params0(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
            '    params0(0).Value = customclass.BranchId.Replace("'", "")
            '    params0(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
            '    params0(1).Value = customclass.bankAccountFrom
            '    params0(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
            '    params0(2).Direction = ParameterDirection.Output
            '    params0(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            '    params0(3).Value = customclass.BusinessDate
            '    params0(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
            '    params0(4).Value = "K"
            '    SqlHelper.ExecuteScalar(objtrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params0)
            '    customclass.referenceNo = CStr(params0(2).Value)
            'End If

            'If customclass.NoBonMerah = "" Then
            '    ' ambil reference no otomatis jika no reference blank
            '    params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
            '    params1(0).Value = customclass.BranchId.Replace("'", "")
            '    params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
            '    params1(1).Value = customclass.bankAccountFrom
            '    params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
            '    params1(2).Direction = ParameterDirection.Output
            '    params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            '    params1(3).Value = customclass.BusinessDate
            '    params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
            '    params1(4).Value = "M"
            '    SqlHelper.ExecuteScalar(objtrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
            '    customclass.NoBonMerah = CStr(params1(2).Value)
            'End If

            'params(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
            'params(0).Value = customclass.BranchId.Replace("'", "")

            'params(1) = New SqlParameter("@bankaccountidTo", SqlDbType.Char, 10)
            'params(1).Value = customclass.bankAccountTo

            'params(2) = New SqlParameter("@bankaccountidfrom", SqlDbType.Char, 10)
            'params(2).Value = customclass.bankAccountFrom

            'params(3) = New SqlParameter("@loginid", SqlDbType.Char, 12)
            'params(3).Value = customclass.LoginId

            'params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            'params(4).Value = customclass.BusinessDate

            'params(5) = New SqlParameter("@Amount", SqlDbType.Decimal)
            'params(5).Value = customclass.Amount

            'params(6) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            'params(6).Value = customclass.valueDate

            'params(7) = New SqlParameter("@referenceNo", SqlDbType.Char, 20)
            'params(7).Value = customclass.referenceNo

            'params(8) = New SqlParameter("@Notes", SqlDbType.Char, 50)
            'params(8).Value = customclass.Notes

            'params(9) = New SqlParameter("@CoyID", SqlDbType.Char, 3)
            'params(9).Value = "001"

            'params(10) = New SqlParameter("@bgno", SqlDbType.Char, 20)
            'params(10).Value = customclass.bgNo

            'params(11) = New SqlParameter("@bgduedate", SqlDbType.DateTime)
            'params(11).Value = customclass.bgDate


            'params(12) = New SqlParameter("@InVoucherNoFrom", SqlDbType.Char, 20)
            'params(12).Direction = ParameterDirection.Output

            'params(13) = New SqlParameter("@InVoucherNoTo", SqlDbType.Char, 20)
            'params(13).Direction = ParameterDirection.Output

            'params(14) = New SqlParameter("@NoBonMerah", SqlDbType.Char, 20)
            'params(14).Value = customclass.NoBonMerah


            'SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spSaveTRACCOUNTDisburse, params)

            params2(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
            params2(0).Value = customclass.BranchId.Replace("'", "")

            params2(1) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params2(1).Value = customclass.valueDate

            params2(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params2(2).Value = customclass.BusinessDate

            params2(3) = New SqlParameter("@referenceNo", SqlDbType.Char, 20)
            params2(3).Value = customclass.referenceNo

            params2(4) = New SqlParameter("@bankaccountidfrom", SqlDbType.Char, 10)
            params2(4).Value = customclass.bankAccountFrom

            params2(5) = New SqlParameter("@bankaccountidTo", SqlDbType.Char, 10)
            params2(5).Value = customclass.bankAccountTo

            params2(6) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params2(6).Value = customclass.Amount

            params2(7) = New SqlParameter("@loginid", SqlDbType.Char, 12)
            params2(7).Value = customclass.LoginId

            params2(8) = New SqlParameter("@VoucherNoFrom", SqlDbType.Char, 20)
            params2(8).Value = "-"

            params2(9) = New SqlParameter("@VoucherNoTo", SqlDbType.Char, 20)
            params2(9).Value = "-"

            params2(10) = New SqlParameter("@Notes", SqlDbType.Char, 50)
            params2(10).Value = customclass.Notes

            params2(11) = New SqlParameter("@bgno", SqlDbType.Char, 20)
            params2(11).Value = customclass.bgNo

            params2(12) = New SqlParameter("@PaymentTypeID", SqlDbType.Char, 2)
            params2(12).Value = customclass.PaymentTypeID

            params2(13) = New SqlParameter("@JenisTransfer", SqlDbType.VarChar, 4)
            params2(13).Value = customclass.JenisTransfer

            params2(14) = New SqlParameter("@NoBonMerah", SqlDbType.VarChar, 20)
            params2(14).Value = customclass.NoBonMerah

            params2(15) = New SqlParameter("@BGDueDate", SqlDbType.DateTime)
            params2(15).Value = customclass.bgDate

            params2(16) = New SqlParameter("@TranNo", SqlDbType.VarChar, 50)
            params2(16).Value = customclass.TransferNO

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spSaveTRACCOUNT, params2)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("TransferAccount", "SaveTransferAccount", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Sub

    Public Sub SaveTransferAccountOtorisasi(ByVal customclass As Parameter.TransferAccount)

        Dim params() As SqlParameter = New SqlParameter(3) {}
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@CompanyID", SqlDbType.Char, 3)
            params(0).Value = customclass.CompanyID

            params(1) = New SqlParameter("@TransferNo", SqlDbType.VarChar, 50)
            params(1).Value = customclass.TransferNO



            params(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(2).Value = customclass.BusinessDate



            params(3) = New SqlParameter("@loginid", SqlDbType.Char, 12)
            params(3).Value = customclass.LoginId


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spEbankTRACCOUNTOtorisasiOtorisasiAdd", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("TransferAccountOtorisasi", "SaveTransferAccountOtorisasi", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Sub
    Public Function InqTransferFund(ByVal customclass As Parameter.TransferFundInquiry) As Parameter.TransferFundInquiry
        Dim params(4) As SqlParameter
        With customclass
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = .SortBy
            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output
        End With
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInqTransferFund, params).Tables(0)
            customclass.TotalRecord = CType(params(4).Value, Int16)
            Return customclass
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")
        Catch exp As Exception
            WriteException("TransferAccount", "SaveTransferAccount", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetViewTransferFund(ByVal customclass As Parameter.TransferFundInquiry) As Parameter.TransferFundInquiry
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
        params(0).Value = customclass.WhereCond
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spViewTransferFund, params).Tables(0)
            Return customclass
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")
        Catch exp As Exception
            WriteException("TransferAccount", "GetViewTransferFund", exp.Message + exp.StackTrace)
        End Try

    End Function
    Public Function ViewRptTransferFund(ByVal customclass As Parameter.TransferFundInquiry) As Parameter.TransferFundInquiry
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
        params(0).Value = customclass.WhereCond
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRptTransferFund, params).Tables(0)
            Return customclass
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")
        Catch exp As Exception
            WriteException("TransferAccount", "ViewRptTransferFund", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function EbankTRACCOUNTAdd(ByVal customclass As Parameter.TransferAccount) As Parameter.TransferAccount
        Dim oPar As New Parameter.TransferAccount
        Dim params() As SqlParameter = New SqlParameter(9) {}
        Dim params2() As SqlParameter = New SqlParameter(14) {}
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@bankaccountidTo", SqlDbType.Char, 10)
            params(1).Value = customclass.bankAccountTo

            params(2) = New SqlParameter("@bankaccountidfrom", SqlDbType.Char, 10)
            params(2).Value = customclass.bankAccountFrom

            params(3) = New SqlParameter("@loginid", SqlDbType.Char, 12)
            params(3).Value = customclass.LoginId

            params(4) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(4).Value = customclass.Amount

            params(5) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(5).Value = customclass.valueDate

            params(6) = New SqlParameter("@referenceNo", SqlDbType.Char, 20)
            params(6).Value = customclass.referenceNo

            params(7) = New SqlParameter("@Notes", SqlDbType.Char, 50)
            params(7).Value = customclass.Notes

            params(8) = New SqlParameter("@CompanyID", SqlDbType.Char, 3)
            params(8).Value = customclass.CompanyID

            params(9) = New SqlParameter("@JenisTransfer", SqlDbType.VarChar, 4)
            params(9).Value = customclass.JenisTransfer

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spEbankTRACCOUNTAdd", params)

            params2(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
            params2(0).Value = customclass.BranchId.Replace("'", "")

            params2(1) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params2(1).Value = customclass.valueDate

            params2(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params2(2).Value = customclass.BusinessDate

            params2(3) = New SqlParameter("@referenceNo", SqlDbType.Char, 20)
            params2(3).Value = customclass.referenceNo

            params2(4) = New SqlParameter("@bankaccountidfrom", SqlDbType.Char, 10)
            params2(4).Value = customclass.bankAccountFrom

            params2(5) = New SqlParameter("@bankaccountidTo", SqlDbType.Char, 10)
            params2(5).Value = customclass.bankAccountTo

            params2(6) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params2(6).Value = customclass.Amount

            params2(7) = New SqlParameter("@loginid", SqlDbType.Char, 12)
            params2(7).Value = customclass.LoginId

            params2(8) = New SqlParameter("@VoucherNoFrom", SqlDbType.Char, 20)
            params2(8).Value = "-"

            params2(9) = New SqlParameter("@VoucherNoTo", SqlDbType.Char, 20)
            params2(9).Value = "-"

            params2(10) = New SqlParameter("@Notes", SqlDbType.Char, 50)
            params2(10).Value = customclass.Notes

            params2(11) = New SqlParameter("@bgno", SqlDbType.Char, 20)
            params2(11).Value = customclass.bgNo

            params2(12) = New SqlParameter("@PaymentTypeID", SqlDbType.Char, 2)
            params2(12).Value = customclass.PaymentTypeID

            params2(13) = New SqlParameter("@JenisTransfer", SqlDbType.VarChar, 4)
            params2(13).Value = customclass.JenisTransfer

            params2(14) = New SqlParameter("@StatusTransferEBanking", SqlDbType.Char, 1)
            params2(14).Value = "G"

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spSaveTRACCOUNT, params2)

            objtrans.Commit()
            oPar.strError = ""

        Catch exp As Exception
            objtrans.Rollback()
            oPar.strError = exp.Message
        End Try

        Return oPar
    End Function
End Class
