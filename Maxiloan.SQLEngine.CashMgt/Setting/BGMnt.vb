

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class BGMnt : Inherits DataAccessBase
    Private Const spBGMntPaging As String = "spBGMntPaging"
    Private Const spBGMntAdd As String = "spBGMntAdd"
    Private Const spBGMntInformasi As String = "spBGMntInformasi"
    Private Const spBGMntUpdate As String = "spBGMntUpdate"
    Private Const spBGMntCekKey As String = "spBGMntCekKey"
    Private Const spBGMntReport As String = "spBGMntReport"
    Private Const spAPDisbSelec As String = "spAPDisbSelec"

    Private m_connection As SqlConnection

#Region "BGMaintenance"
    Public Function BGMntCekKey(ByVal customClass As Parameter.BGMnt) As Boolean
        Dim objread As SqlDataReader
        Dim lObjHoliday As New Hashtable
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
            params(1).Value = customClass.BankAccountID

            params(2) = New SqlParameter("@BGNo", SqlDbType.VarChar, 1000)
            params(2).Value = customClass.BGNoDummy

            params(3) = New SqlParameter("@istrue", SqlDbType.VarChar, 20)
            params(3).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, spBGMntCekKey, params)
            Return CBool(params(3).Value)
        Catch exp As Exception
            WriteException("BGMnt", "BGMntCekKey", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function ListBGMnt(ByVal customclass As Parameter.BGMnt) As Parameter.BGMnt

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListBGMnt = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spBGMntPaging, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)

            Return customclass
        Catch exp As Exception
            WriteException("BGMnt", "ListBGMnt", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function BGMntCmbBankAccount(ByVal customclass As Parameter.BGMnt) As Parameter.BGMnt
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            customclass.BGMntCmbBankAccount = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spBGMntInformasi, params).Tables(0)

            Return customclass
        Catch exp As Exception
            WriteException("BGMnt", "BGMntCmbBankAccount", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AddBGMnt(ByVal customclass As Parameter.BGMnt, ByVal dsBG As DataTable) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim Row As Integer
        If customclass.Mode = "AddMode" Then
            Try
                Dim params() As SqlParameter = New SqlParameter(5) {}
                If objConnection.State = ConnectionState.Closed Then objConnection.Open()
                objtrans = objConnection.BeginTransaction
                params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params(2) = New SqlParameter("@BGNo", SqlDbType.VarChar, 20)
                params(3) = New SqlParameter("@Status", SqlDbType.Char, 1)
                params(4) = New SqlParameter("@StatusDate", SqlDbType.SmallDateTime, 4)
                params(5) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
                With customclass
                    If dsBG.Rows.Count > 0 Then
                        For Row = 0 To dsBG.Rows.Count - 1
                            params(0).Value = .BranchId
                            params(1).Value = .BankAccountID
                            params(2).Value = dsBG.Rows(Row).Item("BGNo")
                            params(3).Value = .Status
                            params(4).Value = .StatusDate
                            params(5).Direction = ParameterDirection.Output
                            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spBGMntAdd, params)
                        Next
                    End If
                End With
                objtrans.Commit()
                Return CStr(params(5).Value)
            Catch exp As Exception
                WriteException("BGMnt", "AddBGMnt", exp.Message + exp.StackTrace)
                objtrans.Rollback()
                Return Nothing
            Finally
                If objConnection.State = ConnectionState.Open Then objConnection.Close()
                objConnection.Dispose()
            End Try


        Else
            'customclass.Mode = "AddModeManual" Then
            Try
                Dim params() As SqlParameter = New SqlParameter(5) {}
                If objConnection.State = ConnectionState.Closed Then objConnection.Open()
                objtrans = objConnection.BeginTransaction
                params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params(2) = New SqlParameter("@BGNo", SqlDbType.VarChar, 20)
                params(3) = New SqlParameter("@Status", SqlDbType.Char, 1)
                params(4) = New SqlParameter("@StatusDate", SqlDbType.SmallDateTime, 4)
                params(5) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
                With customclass
                    If dsBG.Rows.Count > 0 Then
                        For Row = 0 To dsBG.Rows.Count - 1
                            params(0).Value = .BranchId
                            params(1).Value = .BankAccountID
                            params(2).Value = dsBG.Rows(Row).Item("BGNo")
                            params(3).Value = .Status
                            params(4).Value = .StatusDate
                            params(5).Direction = ParameterDirection.Output
                            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spBGMntAdd, params)
                        Next
                    End If
                End With
                objtrans.Commit()
                Return CStr(params(5).Value)
            Catch exp As Exception
                WriteException("BGMnt", "AddBGMnt", exp.Message + exp.StackTrace)
                objtrans.Rollback()
                Return Nothing
            Finally
                If objConnection.State = ConnectionState.Open Then objConnection.Close()
                objConnection.Dispose()
            End Try

        End If
    End Function

    Public Function UpdateBGMnt(ByVal CustomClass As Parameter.BGMnt) As String
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = CustomClass.BranchId

            params(1) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10)
            params(1).Value = CustomClass.BankAccountID

            params(2) = New SqlParameter("@BGNo", SqlDbType.VarChar, 20)
            params(2).Value = CustomClass.BGNo

            params(3) = New SqlParameter("@Status", SqlDbType.Char, 1)
            params(3).Value = CustomClass.Status

            params(4) = New SqlParameter("@StatusDate", SqlDbType.SmallDateTime, 4)
            params(4).Value = CustomClass.StatusDate

            params(5) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
            params(5).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(CustomClass.strConnection, CommandType.StoredProcedure, spBGMntUpdate, params)
            'SqlHelper.ExecuteNonQuery(CustomClass.strConnection, CommandType.StoredProcedure, spBankMasterUpdate, params)

            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
            Return CStr(params(5).Value)
        Catch exp As Exception
            WriteException("BGMnt", "UpdateBGMnt", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function ReportBGMnt(ByVal CustomClass As Parameter.BGMnt) As Parameter.BGMnt
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = CustomClass.WhereCond

            params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(1).Value = CustomClass.SortBy

            CustomClass.ListReport = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, spBGMntReport, params)
            Return CustomClass
        Catch exp As Exception
            WriteException("BGMnt", "ReportBGMnt", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "AP Selection"
    Public Function ListAPSele(ByVal customclass As Parameter.BGMnt) As Parameter.BGMnt

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = customclass.WhereCond

            params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(1).Value = customclass.SortBy

            customclass.ListAPSele = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAPDisbSelec, params).Tables(0)

            Return customclass
        Catch exp As Exception
            WriteException("BGMnt", "ListAPSele", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
End Class
