

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class CashAdvance : Inherits DataAccessBase

    Private Const spCashAdvancePettyCashPaging As String = "spCashAdvancePettyCashPaging"
    Private Const spCashAdvancePaging As String = "spCashAdvancePaging"
    Private Const spCashAdvanceSaveMode As String = "spCashAdvanceSaveMode"
    Private Const spReverseCashAdvance As String = "spReversalCashAdvance"
    Private Const SaveCashAdvanceRealisasiDetailSaveMode As String = "spCashAdvanceSaveDetail"
    Private Const spCashAdvanceDetail As String = "spCashAdvanceDetail"

    Public Function GetListCashAdvance(ByVal oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance

        Try
            Dim params = New List(Of SqlParameter) From {
               New SqlParameter("@CurrentPage", SqlDbType.Int) With {.Value = oCustomClass.CurrentPage},
               New SqlParameter("@PageSize", SqlDbType.Int) With {.Value = oCustomClass.PageSize},
               New SqlParameter("@WhereCond", SqlDbType.VarChar) With {.Value = oCustomClass.WhereCond},
               New SqlParameter("@SortBy", SqlDbType.VarChar) With {.Value = oCustomClass.SortBy},
               New SqlParameter("@SelectedMenu", SqlDbType.VarChar) With {.Value = oCustomClass.SelectedMenu},
               New SqlParameter("@TotalRecords", SqlDbType.SmallInt) With {.Direction = ParameterDirection.Output}
             }

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spCashAdvancePettyCashPaging, params.ToArray).Tables(0)
            oCustomClass.TotalRecord = CInt(params(5).Value)
        Catch ex As Exception
            Throw New Exception
        End Try

        Return oCustomClass
    End Function

    Public Function GetPagingCashAdvance(ByVal oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance

        Try
            Dim params = New List(Of SqlParameter) From {
               New SqlParameter("@CurrentPage", SqlDbType.Int) With {.Value = oCustomClass.CurrentPage},
               New SqlParameter("@PageSize", SqlDbType.Int) With {.Value = oCustomClass.PageSize},
               New SqlParameter("@WhereCond", SqlDbType.VarChar) With {.Value = oCustomClass.WhereCond},
               New SqlParameter("@SortBy", SqlDbType.VarChar) With {.Value = oCustomClass.SortBy},
               New SqlParameter("@SelectedMenu", SqlDbType.VarChar) With {.Value = oCustomClass.SelectedMenu},
               New SqlParameter("@TotalRecords", SqlDbType.SmallInt) With {.Direction = ParameterDirection.Output}
             }

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spCashAdvancePaging, params.ToArray).Tables(0)
            oCustomClass.TotalRecord = CInt(params(5).Value)
        Catch ex As Exception
            Throw New Exception
        End Try

        Return oCustomClass
    End Function
    'Public Function SaveCashAdvance(ByVal oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance

    '    '
    '    'New SqlParameter("@TanggalCA", SqlDbType.DateTime, 8) With {.Value = oCustomClass.TanggalCA},
    '    Try
    '        Dim params = New List(Of SqlParameter) From {
    '         New SqlParameter("@NoCA", SqlDbType.VarChar, 20) With {.Value = oCustomClass.NoCA},
    '         New SqlParameter("@BranchID", SqlDbType.VarChar, 4) With {.Value = oCustomClass.BranchId},
    '         New SqlParameter("@TanggalDisburse", SqlDbType.DateTime, 8) With {.Value = oCustomClass.TanggalDisburse},
    '         New SqlParameter("@Jumlah", SqlDbType.Decimal, 9) With {.Value = oCustomClass.Jumlah},
    '         New SqlParameter("@CaraBayar", SqlDbType.VarChar, 3) With {.Value = oCustomClass.CaraBayar},
    '         New SqlParameter("@Penerima", SqlDbType.VarChar, 255) With {.Value = oCustomClass.Penerima},
    '         New SqlParameter("@Penggunaan", SqlDbType.VarChar, 255) With {.Value = oCustomClass.Penggunaan},
    '         New SqlParameter("@Status", SqlDbType.VarChar, 255) With {.Value = oCustomClass.Status},
    '         New SqlParameter("@Bank", SqlDbType.VarChar, 50) With {.Value = oCustomClass.Bank},
    '         New SqlParameter("@CabangBank", SqlDbType.VarChar, 50) With {.Value = oCustomClass.CabangBank},
    '         New SqlParameter("@NomorRekening", SqlDbType.VarChar, 50) With {.Value = oCustomClass.NomorRekening},
    '         New SqlParameter("@AtasNama", SqlDbType.VarChar, 50) With {.Value = oCustomClass.AtasNama},
    '         New SqlParameter("@DariRekening", SqlDbType.VarChar, 50) With {.Value = oCustomClass.DariRekening},
    '         New SqlParameter("@OpsiTransfer", SqlDbType.VarChar, 50) With {.Value = oCustomClass.OpsiTransfer},
    '         New SqlParameter("@BiayaTransfer", SqlDbType.VarChar, 50) With {.Value = oCustomClass.BiayaTransfer},
    '         New SqlParameter("@BebanBiayaTransfer", SqlDbType.VarChar, 50) With {.Value = oCustomClass.BebanBiayaTransfer},
    '         New SqlParameter("@AttachMent", SqlDbType.VarChar, 50) With {.Value = oCustomClass.Attachment},
    '         New SqlParameter("@Pengembalian", SqlDbType.VarChar, 50) With {.Value = oCustomClass.Pengembalian},
    '         New SqlParameter("@TanggalRealisasi", SqlDbType.DateTime, 8) With {.Value = oCustomClass.TanggalRealisasi},
    '         New SqlParameter("@LoginID", SqlDbType.VarChar, 20) With {.Value = oCustomClass.LoginId},
    '         New SqlParameter("@Notes", SqlDbType.Text) With {.Value = oCustomClass.Notes},
    '         New SqlParameter("@SaveMenu", SqlDbType.Char, 20) With {.Value = oCustomClass.SaveMenu},
    '         New SqlParameter("@ErrMsg", SqlDbType.VarChar, 20) With {.Direction = ParameterDirection.Output},
    '         New SqlParameter("@IDPenerima", SqlDbType.VarChar, 20) With {.Value = oCustomClass.IDPenerima},
    '         New SqlParameter("@BankBranchID", SqlDbType.VarChar, 20) With {.Value = oCustomClass.BankBranchID},
    '         New SqlParameter("@BankBranchName", SqlDbType.VarChar, 100) With {.Value = oCustomClass.BankBranchName}
    '        }

    '        SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, spCashAdvanceSaveMode, params.ToArray)
    '        oCustomClass.Msg = CStr(params(22).Value)
    '    Catch ex As Exception
    '        Throw New Exception
    '    End Try

    '    Return oCustomClass

    'End Function

    Public Function SaveCashAdvance(ByVal oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance

        '
        'New SqlParameter("@TanggalCA", SqlDbType.DateTime, 8) With {.Value = oCustomClass.TanggalCA},
        Try
            Dim params = New List(Of SqlParameter) From {
             New SqlParameter("@NoCA", SqlDbType.VarChar, 20) With {.Value = oCustomClass.NoCA},
             New SqlParameter("@BranchID", SqlDbType.VarChar, 4) With {.Value = oCustomClass.BranchId},
             New SqlParameter("@TanggalDisburse", SqlDbType.DateTime) With {.Value = oCustomClass.TanggalDisburse},
             New SqlParameter("@Jumlah", SqlDbType.Decimal) With {.Value = oCustomClass.Jumlah},
             New SqlParameter("@Penerima", SqlDbType.VarChar, 255) With {.Value = oCustomClass.Penerima},
             New SqlParameter("@Penggunaan", SqlDbType.VarChar, 255) With {.Value = oCustomClass.Penggunaan},
             New SqlParameter("@TanggalRealisasi", SqlDbType.DateTime) With {.Value = oCustomClass.TanggalRealisasi},
             New SqlParameter("@LoginID", SqlDbType.VarChar, 20) With {.Value = oCustomClass.LoginId},
             New SqlParameter("@SaveMenu", SqlDbType.Char, 20) With {.Value = oCustomClass.SaveMenu},
             New SqlParameter("@IDPenerima", SqlDbType.VarChar, 20) With {.Value = oCustomClass.IDPenerima},
             New SqlParameter("@JournalBank", SqlDbType.VarChar, 20) With {.Value = oCustomClass.JournalBank},
             New SqlParameter("@LamaRealisasi", SqlDbType.VarChar, 3) With {.Value = oCustomClass.LamaRealisasi},
             New SqlParameter("@Department", SqlDbType.VarChar, 20) With {.Value = oCustomClass.Department},
             New SqlParameter("@StatusApproval", SqlDbType.VarChar, 1) With {.Value = oCustomClass.Status},
             New SqlParameter("@Notes", SqlDbType.VarChar, 200) With {.Value = oCustomClass.Notes},
             New SqlParameter("@ErrMsg", SqlDbType.VarChar, 20) With {.Direction = ParameterDirection.Output}
            }
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, spCashAdvanceSaveMode, params.ToArray)
            oCustomClass.Msg = CStr(params(15).Value)
        Catch ex As Exception
            Throw New Exception
        End Try

        Return oCustomClass

    End Function


    Public Function SaveCashAdvanceRealisasiDetail(ByVal oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance
        Try
            Dim params = New List(Of SqlParameter) From {
             New SqlParameter("@NoCA", SqlDbType.VarChar, 20) With {.Value = oCustomClass.NoCA},
             New SqlParameter("@BranchID", SqlDbType.VarChar, 4) With {.Value = oCustomClass.BranchId},
             New SqlParameter("@PaymentAllocationID", SqlDbType.VarChar, 15) With {.Value = oCustomClass.PaymentAllocationIDCOA},
             New SqlParameter("@Description", SqlDbType.VarChar, 100) With {.Value = oCustomClass.KeteranganCOA},
             New SqlParameter("@Amount", SqlDbType.Decimal) With {.Value = oCustomClass.AmountTotalCOA},
             New SqlParameter("@LoginID", SqlDbType.VarChar, 20) With {.Value = oCustomClass.LoginId},
             New SqlParameter("@SeqNo", SqlDbType.Int) With {.Value = oCustomClass.seqNo},
             New SqlParameter("@DepartmentID", SqlDbType.Int) With {.Value = oCustomClass.Department},
             New SqlParameter("@ErrMsg", SqlDbType.VarChar, 20) With {.Direction = ParameterDirection.Output}
            }

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SaveCashAdvanceRealisasiDetailSaveMode, params.ToArray)

            oCustomClass.Msg = CStr(params(5).Value)

        Catch ex As Exception
            Throw New Exception
        End Try

        Return oCustomClass
    End Function

    Public Function GetListCashAdvanceDetail(ByVal oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance

        Try
            Dim params = New List(Of SqlParameter) From {
               New SqlParameter("@NoCA", SqlDbType.VarChar, 20) With {.Value = oCustomClass.NoCA}
             }

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spCashAdvanceDetail, params.ToArray).Tables(0)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception
        End Try

        'Dim params(0) As SqlParameter
        'Dim reader As SqlDataReader
        'Dim oReturnValue As New Parameter.CashAdvance
        'params(0) = New SqlParameter("@NoCA", SqlDbType.Char, 20)
        'params(0).Value = oCustomClass.NoCA
        'Try
        '    reader = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetHasilSurvey", params)
        '    If reader.Read Then
        '        With oReturnValue
        '            .BranchId = CDate(reader("BranchID").ToString)
        '            .NoCA = reader("NoCA").ToString
        '            .Jumlah = CDbl(reader("Amount").ToString)
        '            .Notes = reader("Description").ToString
        '            .PaymentAllocationIDCOA = reader("PaymentAllocationID").ToString
        '            .Department = reader("Department").ToString
        '        End With
        '    End If
        '    reader.Close()
        '    Return oReturnValue
        'Catch ex As Exception
        '    Throw New Exception(ex.Message)
        'End Try
    End Function

    Public Function ReverseReversalCashAdvanced(ByVal oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@NoCA", SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.NoCA

            'params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            'params(1).Value = oCustomClass.BranchId

            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spReversalCashAdvancedList", params)
            If objread.Read Then
                With oCustomClass
                    .NoCA = CType(objread("NoCA"), String)
                    '.reversalno = CType(objread(""), String)
                    .TanggalDisburse = CType(objread("TanggalDisburse"), DateTime)
                    '.BankAccountID = CType(objread("bankaccountid"), String)
                    .NomorRekening = CType(objread("nomorrekening"), String)

                    .TanggalCA = CType(objread("TanggalCA"), DateTime)
                    .Jumlah = CType(objread("Jumlah"), Double)
                    .description = CType(objread("CaraBayar"), String)
                    .Penerima = CType(objread("Penerima"), String)
                    .Department = CType(objread("DepartmentID"), String)
                    .BankBranchName = CType(objread("BankBranchName"), String)
                    .Penggunaan = CType(objread("Penggunaan"), String)
                    .JournalBank = CType(objread("JournalBank"), String)
                    .Department = CType(objread("DepartmentName"), String)
                    '.StatusDate = CType(objread("statusdate"), Date)
                    '.VoucherNo = CType(objread("VoucherNoRcv"), String)
                    '.Agreementno = CType(objread("agreementno"), String)
                    '.CustomerName = CType(objread("Customername"), String)
                    '.TDPStatus = CType(objread("TDPReceiveStatus"), String)
                    '.TDPStatusdesc = CType(objread("TDPstatusdesc"), String)

                End With
            End If
            objread.Close()
            Return oCustomClass
        Catch exp As Exception
            WriteException("ReverseReversalCashAdvanced", "ReverseReversalCashAdvanced", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function SaveReverseReversalCashAdvanced(ByVal ocustomclass As Parameter.CashAdvance)
        Dim params() As SqlParameter = New SqlParameter(11) {}
        Dim objcon As New SqlConnection(ocustomclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            params(0) = New SqlParameter("@NoCA", SqlDbType.VarChar, 20)
            params(0).Value = ocustomclass.NoCA

            params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 4)
            params(1).Value = ocustomclass.BranchId

            params(2) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
            params(2).Value = ocustomclass.LoginId

            'params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            'params(3).Value = ocustomclass.BusinessDate

            params(3) = New SqlParameter("@TanggalDisburse", SqlDbType.DateTime)
            params(3).Value = ocustomclass.TanggalDisburse

            params(4) = New SqlParameter("@Jumlah", SqlDbType.Decimal, 9)
            params(4).Value = ocustomclass.Jumlah

            params(5) = New SqlParameter("@Penerima", SqlDbType.VarChar, 255)
            params(5).Value = ocustomclass.Penerima

            params(6) = New SqlParameter("@NomorRekening", SqlDbType.VarChar, 20)
            params(6).Value = ocustomclass.NomorRekening

            params(7) = New SqlParameter("@TanggalCA", SqlDbType.DateTime)
            params(7).Value = ocustomclass.TanggalCA

            params(8) = New SqlParameter("@JournalBank", SqlDbType.VarChar, 20)
            params(8).Value = ocustomclass.JournalBank

            params(9) = New SqlParameter("@BankBranchName", SqlDbType.VarChar, 20)
            params(9).Value = ocustomclass.BankBranchName

            params(10) = New SqlParameter("@Notes", SqlDbType.VarChar, 20)
            params(10).Value = ocustomclass.Notes

            params(11) = New SqlParameter("@Penggunaan", SqlDbType.VarChar, 255)
            params(11).Value = ocustomclass.Penggunaan

            params(12) = New SqlParameter("@Department", SqlDbType.VarChar, 20)
            params(12).Value = ocustomclass.Department

            'Dim params = New List(Of SqlParameter) From {
            ' New SqlParameter("@NoCA", SqlDbType.VarChar, 20) With {.Value = ocustomclass.NoCA},
            ' New SqlParameter("@BranchID", SqlDbType.VarChar, 4) With {.Value = ocustomclass.BranchId},
            ' New SqlParameter("@TanggalDisburse", SqlDbType.DateTime, 8) With {.Value = ocustomclass.TanggalDisburse},
            ' New SqlParameter("@Jumlah", SqlDbType.Decimal, 9) With {.Value = ocustomclass.Jumlah},
            ' New SqlParameter("@Penerima", SqlDbType.VarChar, 255) With {.Value = ocustomclass.Penerima},
            ' New SqlParameter("@Penggunaan", SqlDbType.VarChar, 255) With {.Value = ocustomclass.Penggunaan},
            ' New SqlParameter("@TanggalRealisasi", SqlDbType.DateTime, 8) With {.Value = ocustomclass.TanggalRealisasi},
            ' New SqlParameter("@LoginID", SqlDbType.VarChar, 20) With {.Value = ocustomclass.LoginId},
            ' New SqlParameter("@SaveMenu", SqlDbType.Char, 20) With {.Value = ocustomclass.SaveMenu},
            ' New SqlParameter("@IDPenerima", SqlDbType.VarChar, 20) With {.Value = ocustomclass.IDPenerima},
            ' New SqlParameter("@JournalBank", SqlDbType.VarChar, 20) With {.Value = ocustomclass.JournalBank},
            ' New SqlParameter("@LamaRealisasi", SqlDbType.VarChar, 3) With {.Value = ocustomclass.LamaRealisasi},
            ' New SqlParameter("@Department", SqlDbType.VarChar, 20) With {.Value = ocustomclass.Department},
            ' New SqlParameter("@ErrMsg", SqlDbType.VarChar, 20) With {.Direction = ParameterDirection.Output}
            '}
            SqlHelper.ExecuteNonQuery(ocustomclass.strConnection, CommandType.StoredProcedure, spReverseCashAdvance, params.ToArray)
            ocustomclass.Msg = CStr(params(11).Value)
        Catch ex As Exception
            Throw New Exception
        End Try

        '    Dim params() As SqlParameter = New SqlParameter(9) {}
        '    Dim params1() As SqlParameter = New SqlParameter(4) {}
        '    Dim objcon As New SqlConnection(ocustomclass.strConnection)
        '    Dim objtrans As SqlTransaction
        '    Try
        '        If objcon.State = ConnectionState.Closed Then objcon.Open()
        '        objtrans = objcon.BeginTransaction

        '        'If ocustomclass.ReferenceNo = "" Then
        '        ' ambil reference no otomatis jika no reference blank
        '        params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
        '            params1(0).Value = ocustomclass.BranchId.Replace("'", "")
        '        'params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
        '        'params1(1).Value = ocustomclass.BankAccountID
        '        params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
        '            params1(2).Direction = ParameterDirection.Output
        '            params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        '            params1(3).Value = ocustomclass.BusinessDate
        '            params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
        '            params1(4).Value = "M"
        '            SqlHelper.ExecuteScalar(objtrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
        '        '    ocustomclass.ReferenceNo = CStr(params1(2).Value)
        '        'End If

        '        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        '        params(0).Value = ocustomclass.BranchId

        '        'params(1) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
        '        'params(1).Value = ocustomclass.BankAccountID

        '        params(2) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
        '        params(2).Value = ocustomclass.LoginId

        '        params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
        '        params(3).Value = ocustomclass.BusinessDate

        '        'params(4) = New SqlParameter("@Amount", SqlDbType.Decimal)
        '        'params(4).Value = ocustomclass.AmountRec

        '        'params(5) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
        '        'params(5).Value = ocustomclass.ValueDate

        '        'params(6) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.Char, 20)
        '        'params(6).Value = ocustomclass.ReferenceNo

        '        params(7) = New SqlParameter("@Notes", SqlDbType.Char, 50)
        '        params(7).Value = ocustomclass.description

        '        params(8) = New SqlParameter(PARAM_COMPANYID, SqlDbType.Char, 3)
        '        params(8).Value = ocustomclass.CoyID

        '        'params(9) = New SqlParameter("@TDPReceiveNo", SqlDbType.VarChar, 20)
        '        'params(9).Value = ocustomclass.TDPReceiveNo



        '        SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spTDPReversal", params)
        '        objtrans.Commit()
        '    Catch exp As Exception
        '        objtrans.Rollback()
        '        WriteException("Cash Advanced Reverse", "SaveCashAdvancedReverse", exp.Message + exp.StackTrace)
        '    End Try
    End Function
End Class
