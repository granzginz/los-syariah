﻿#Region "Imports"
Imports System.Configuration
Imports Microsoft.Win32.Registry
Imports Microsoft.Win32
'Imports System.Web
#End Region

Public MustInherit Class DataAccessBase
    Const REFERENCE_DATA_CONNECTION_STRING_KEY As String = "Reference.Connection"
    Const BUSINESS_DATA_CONNECTION_STRING_KEY As String = "Business.Connection"

    Protected Const PARAM_BUSINESSDATE As String = "@businessdate"
    Protected Const PARAM_VALUEDATE As String = "@valuedate"
    Protected Const PARAM_BRANCHID As String = "@branchid"
    Protected Const PARAM_COMPANYID As String = "@companyid"
    Protected Const PARAM_CURRENCYID As String = "@currencyid"

    Protected Const PARAM_CURRENTPAGE As String = "@currentpage"
    Protected Const PARAM_PAGESIZE As String = "@pagesize"
    Protected Const PARAM_WHERECOND As String = "@wherecond"
    Protected Const PARAM_SORTBY As String = "@sortby"
    Protected Const PARAM_TOTALRECORD As String = "@totalrecords"
    Protected Const PARAM_STRERROR As String = "@strerror"
    Protected Const PARAM_ERRORNO As String = "@errorno"

    Protected Const PARAM_REASONID As String = "@reasonid"
    Protected Const PARAM_REASONTYPEID As String = "@reasontypeid"

    Protected Const PARAM_USRUPD As String = "@usrupd"
    Protected Const PARAM_DTMUPD As String = "@dtmupd"

    Shared oDecr As New Decrypt.am_futility

#Region "Address"
    Protected Const PARAM_ADDRESS As String = "@address"
    Protected Const PARAM_RT As String = "@rt"
    Protected Const PARAM_RW As String = "@rw"
    Protected Const PARAM_KELURAHAN As String = "@kelurahan"
    Protected Const PARAM_KECAMATAN As String = "@kecamatan"
    Protected Const PARAM_CITY As String = "@city"

    Protected Const PARAM_ZIPCODE As String = "@zipcode"
    Protected Const PARAM_AREAPHONE1 As String = "@areaphone1"
    Protected Const PARAM_PHONE1 As String = "@phone1"
    Protected Const PARAM_AREAPHONE2 As String = "@areaphone2"
    Protected Const PARAM_PHONE2 As String = "@phone2"
    Protected Const PARAM_AREAFAX As String = "@areafax"
    Protected Const PARAM_FAX As String = "@fax"
#End Region

#Region "Personal Customer"
    Protected Const PARAM_CONTACTPERSONNAME As String = "@contactpersonname"
    Protected Const PARAM_CONTACTPERSONTITLE As String = "@contactpersontitle"
    Protected Const PARAM_EMAIL As String = "@email"
    Protected Const PARAM_MOBILEPHONE As String = "@mobilephone"
#End Region

#Region "Param Authorize"
    Protected Const PARAM_FORMID As String = "@formid"
    Protected Const PARAM_FEATUREID As String = "@featureid"
    Protected Const PARAM_LOGINID As String = "@loginid"
    Protected Const PARAM_ISAUTHORIZE As String = "@isauthorize"
    Protected Const PARAM_APPID As String = "@appid"
#End Region

    Public Shared MethodTrace As BooleanSwitch = New BooleanSwitch("MethodTrace", "Trace entering and leaving of method")

    Public Shared Function getReferenceServerName() As String
        Dim rk As RegistryKey = Registry.LocalMachine.OpenSubKey("Software\" & ConfigurationManager.AppSettings("RegKey"), False)
        'Return oDecr.encryptto(rk.GetValue("ServerName", "").ToString.Trim, "1")
        Return rk.GetValue("ServerName", "").ToString.Trim
    End Function


    Public Shared Function getReferenceDBName() As String
        Dim rk As RegistryKey = Registry.LocalMachine.OpenSubKey("Software\" & ConfigurationManager.AppSettings("RegKey"), False)
        Return oDecr.encryptto(rk.GetValue("DbName", "").ToString.Trim, "1")
    End Function


    Public Shared Function getReferenceUserID() As String
        Dim rk As RegistryKey = Registry.LocalMachine.OpenSubKey("Software\" & ConfigurationManager.AppSettings("RegKey"), False)
        Return oDecr.encryptto(rk.GetValue("UserID", "").ToString.Trim, "1")
    End Function

    Public Shared Function getReferencePassword() As String
        Dim rk As RegistryKey = Registry.LocalMachine.OpenSubKey("Software\" & ConfigurationManager.AppSettings("RegKey"), False)
        Return oDecr.encryptto(rk.GetValue("Password", "").ToString.Trim, "1")
    End Function

    Public Shared Function GetReferenceDataConnectionString() As String          
        Return "server=" & getReferenceServerName() & ";database=" & getReferenceDBName() & ";user id=" & getReferenceUserID() & ";password=" & getReferencePassword() & ";Max Pool Size=200;Pooling=true;Connection Lifetime=120"
    End Function

    Public Shared Function GetPassKeyPrefix() As String
        Dim rk As RegistryKey = Registry.LocalMachine.OpenSubKey("Software\" & ConfigurationManager.AppSettings("RegKey"), False)
        Return oDecr.encryptto(rk.GetValue("PassKeyPrefix", "").ToString.Trim, "1")
    End Function

    Public Shared Function getPoolSize() As String
        Dim rk As RegistryKey = Registry.LocalMachine.OpenSubKey("Software\" & ConfigurationManager.AppSettings("RegKey"), False)
        Return rk.GetValue("PoolSize", "").ToString.Trim
    End Function

    Public Shared Function EncodeSQLStringValue(ByVal sValue As String) As String
        Return sValue.Replace("'", "''")
    End Function

    Public Shared Function EncodeSQLBooleanValue(ByVal sValue As Boolean) As Byte
        If (sValue) Then
            Return 1
        Else
            Return 0
        End If
    End Function

    Protected Overloads Sub WriteException(ByVal strMessage As String)
        If Not (EventLog.SourceExists("MAXILOAN")) Then
            EventLog.CreateEventSource("MAXILOAN", "Application")
        End If
        EventLog.WriteEntry("MAXILOAN", strMessage, EventLogEntryType.Error, 999)
    End Sub
    Protected Overloads Sub WriteException(ByVal strModuleName As String, ByVal strMessage As String)
        If Not (EventLog.SourceExists("MAXILOAN")) Then
            EventLog.CreateEventSource("MAXILOAN", "Application")
        End If
        EventLog.WriteEntry("MAXILOAN", strModuleName + ":" + strMessage, EventLogEntryType.Error, 999)
    End Sub
    Protected Overloads Sub WriteException(ByVal strClass As String, ByVal strSub As String, ByVal strMessage As String)
        Throw New Exception(strMessage)
    End Sub
End Class

