﻿

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class MitraMultifinance : Inherits DataAccessBase
    Private Const spMITRA_LIST As String = "spMitraMultifinanceList"
    Private Const spMITRA_BYID As String = "spMitraMultifinanceByID"
    Private Const spMITRA_ADD As String = "spMitraMultifinanceAdd"
    Private Const spMITRA_EDIT As String = "spMitraMultifinanceEdit"
    Private Const spMITRA_DEL As String = "spMitraMultifinanceDelete"

    Private Const spRekMITRA_LIST As String = "spRekMitraMultifinanceList"
    Private Const spRekMITRA_BYID As String = "spRekMitraMultifinanceByID"
    Private Const spRekMITRA_ADD As String = "spRekMitraMultifinanceAdd"
    Private Const spRekMITRA_EDIT As String = "spRekMitraMultifinanceEdit"
    Private Const spRekMITRA_DEL As String = "spRekMitraMultifinanceDelete"


#Region "MITRA MULTIFINANCE"

#Region "MitraList"
    Public Function MitraList(ByVal customclass As Parameter.MitraMultifinance) As Parameter.MitraMultifinance

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMITRA_LIST, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("Mitra Multifinance", "MitraMultifinancePaging", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "MitrabyID"
    Public Function ListMitraByID(ByVal customclass As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond

        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMITRA_BYID, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("Mitra Multifinance", "ListMitraByID", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region " MitraAdd"
    Public Sub MitraAdd(ByVal oMitra As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address)

        Dim params(17) As SqlParameter
        Dim objcon As New SqlConnection(oMitra.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@MfCode", SqlDbType.Char, 10)
            params(0).Value = oMitra.MitraID
            params(1) = New SqlParameter("@FullName", SqlDbType.VarChar, 50)
            params(1).Value = oMitra.MitraFullName
            params(2) = New SqlParameter("@InitialName", SqlDbType.Char, 20)
            params(2).Value = oMitra.MitraInitialName
            params(3) = New SqlParameter("@Address", SqlDbType.Char, 100)
            params(3).Value = oClassAddress.Address
            params(4) = New SqlParameter("@RT", SqlDbType.Char, 3)
            params(4).Value = oClassAddress.RT
            params(5) = New SqlParameter("@RW", SqlDbType.Char, 3)
            params(5).Value = oClassAddress.RW
            params(6) = New SqlParameter("@VillageName", SqlDbType.VarChar, 50)
            params(6).Value = oClassAddress.Kelurahan
            params(7) = New SqlParameter("@District", SqlDbType.VarChar, 50)
            params(7).Value = oClassAddress.Kecamatan
            params(8) = New SqlParameter("@City", SqlDbType.VarChar, 50)
            params(8).Value = oClassAddress.City
            params(9) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
            params(9).Value = oClassAddress.ZipCode
            params(10) = New SqlParameter("@ProvinceName", SqlDbType.Char, 50)
            params(10).Value = oMitra.Provinsi
            params(11) = New SqlParameter("@Phone1", SqlDbType.Char, 20)
            params(11).Value = oMitra.Phone1
            params(12) = New SqlParameter("@Phone2", SqlDbType.Char, 20)
            params(12).Value = oMitra.Phone2
            params(13) = New SqlParameter("@Fax", SqlDbType.Char, 20)
            params(13).Value = oMitra.Fax
            params(14) = New SqlParameter("@Website", SqlDbType.VarChar, 50)
            params(14).Value = oMitra.WebSite
            params(15) = New SqlParameter("@JoinDate", SqlDbType.SmallDateTime)
            params(15).Value = oMitra.JoinDate
            params(16) = New SqlParameter("@Notes", SqlDbType.VarChar, 500)
            params(16).Value = oMitra.Notes
            params(17) = New SqlParameter("@UsrCrt", SqlDbType.Char, 20)
            params(17).Value = oMitra.LoginId

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spMITRA_ADD, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Mitra Multifinance", "MitraAdd", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region " MitraEdit"
    Public Sub MitraEdit(ByVal oMitra As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address)
        Dim params(17) As SqlParameter
        Dim objcon As New SqlConnection(oMitra.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@MfCode", SqlDbType.Char, 10)
            params(0).Value = oMitra.MitraID
            params(1) = New SqlParameter("@FullName", SqlDbType.VarChar, 50)
            params(1).Value = oMitra.MitraFullName
            params(2) = New SqlParameter("@InitialName", SqlDbType.Char, 20)
            params(2).Value = oMitra.MitraInitialName
            params(3) = New SqlParameter("@Address", SqlDbType.Char, 100)
            params(3).Value = oClassAddress.Address
            params(4) = New SqlParameter("@RT", SqlDbType.Char, 3)
            params(4).Value = oClassAddress.RT
            params(5) = New SqlParameter("@RW", SqlDbType.Char, 3)
            params(5).Value = oClassAddress.RW
            params(6) = New SqlParameter("@VillageName", SqlDbType.VarChar, 50)
            params(6).Value = oClassAddress.Kelurahan
            params(7) = New SqlParameter("@District", SqlDbType.VarChar, 50)
            params(7).Value = oClassAddress.Kecamatan
            params(8) = New SqlParameter("@City", SqlDbType.VarChar, 50)
            params(8).Value = oClassAddress.City
            params(9) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
            params(9).Value = oClassAddress.ZipCode
            params(10) = New SqlParameter("@ProvinceName", SqlDbType.Char, 50)
            params(10).Value = oMitra.Provinsi
            params(11) = New SqlParameter("@Phone1", SqlDbType.Char, 20)
            params(11).Value = oMitra.Phone1
            params(12) = New SqlParameter("@Phone2", SqlDbType.Char, 20)
            params(12).Value = oMitra.Phone2
            params(13) = New SqlParameter("@Fax", SqlDbType.Char, 20)
            params(13).Value = oMitra.Fax
            params(14) = New SqlParameter("@Website", SqlDbType.VarChar, 50)
            params(14).Value = oMitra.WebSite
            params(15) = New SqlParameter("@JoinDate", SqlDbType.DateTime)
            params(15).Value = oMitra.JoinDate
            params(16) = New SqlParameter("@Notes", SqlDbType.VarChar, 500)
            params(16).Value = oMitra.Notes
            params(17) = New SqlParameter("@UsrCrt", SqlDbType.Char, 20)
            params(17).Value = oMitra.LoginId


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spMITRA_EDIT, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Mitra Multifinance", "MitraEdit", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region "MitraDelete"
    Public Sub MitraDelete(ByVal oMitra As Parameter.MitraMultifinance)

        Dim params(0) As SqlParameter
        Dim objcon As New SqlConnection(oMitra.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            With oMitra
                params(0) = New SqlParameter("@MfCode", SqlDbType.Char, 10)
                params(0).Value = .MitraID

            End With
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spMITRA_DEL, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Mitra Multifinance", "MitraDelete", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region "RekeningMitraList"

    Public Function RekeningMitraList(ByVal customclass As Parameter.MitraMultifinance) As Parameter.MitraMultifinance

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRekMITRA_LIST, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("Mitra Multifinance", "MitraMultifinancePaging", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "RekeningMitraListByID"
    Public Function RekeningMitraListByID(ByVal customclass As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond

        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRekMITRA_BYID, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("Mitra Multifinance", "RekeningListMitraByID", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region " RekeningMitraAdd"
    Public Sub RekeningMitraAdd(ByVal oMitra As Parameter.MitraMultifinance)

        Dim params(6) As SqlParameter
        Dim objcon As New SqlConnection(oMitra.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@MfCode", SqlDbType.Char, 10)
            params(0).Value = oMitra.MitraID
            params(1) = New SqlParameter("@BankID", SqlDbType.Char, 3)
            params(1).Value = oMitra.BankID
            params(2) = New SqlParameter("@BranchCode", SqlDbType.Char, 10)
            params(2).Value = oMitra.BranchCode
            params(3) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
            params(3).Value = oMitra.AccountName
            params(4) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 25)
            params(4).Value = oMitra.AccountNo
            params(5) = New SqlParameter("@AccountType", SqlDbType.Char, 1)
            params(5).Value = oMitra.AccountType
            params(6) = New SqlParameter("@UsrCrt", SqlDbType.Char, 20)
            params(6).Value = oMitra.LoginId

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spRekMITRA_ADD, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Mitra Multifinance", "MitraAdd", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region "RekeningMitraEdit"
    Public Sub RekeningMitraEdit(ByVal oMitra As Parameter.MitraMultifinance)
        Dim params(6) As SqlParameter
        Dim objcon As New SqlConnection(oMitra.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@MfCode", SqlDbType.Char, 10)
            params(0).Value = oMitra.MitraID
            params(1) = New SqlParameter("@BankID", SqlDbType.Char, 3)
            params(1).Value = oMitra.BankID
            params(2) = New SqlParameter("@BranchCode", SqlDbType.Char, 10)
            params(2).Value = oMitra.BranchCode
            params(3) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
            params(3).Value = oMitra.AccountName
            params(4) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 25)
            params(4).Value = oMitra.AccountNo
            params(5) = New SqlParameter("@AccountType", SqlDbType.Char, 1)
            params(5).Value = oMitra.AccountType
            params(6) = New SqlParameter("@UsrUpd", SqlDbType.Char, 20)
            params(6).Value = oMitra.LoginId


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spRekMITRA_EDIT, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Mitra Multifinance", "MitraEdit", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region "RekeningMitraDelete"
    Public Sub RekeningMitraDelete(ByVal oMitra As Parameter.MitraMultifinance)

        Dim params(1) As SqlParameter
        Dim objcon As New SqlConnection(oMitra.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            With oMitra
                params(0) = New SqlParameter("@MfCode", SqlDbType.Char, 10)
                params(0).Value = .MitraID
                params(1) = New SqlParameter("@AccountNo", SqlDbType.Char, 25)
                params(1).Value = .AccountNo

            End With
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spRekMITRA_DEL, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Mitra Multifinance", "MitraDelete", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region
#End Region

    Public Function uploadlist(ByVal customclass As Parameter.Lending) As Parameter.Lending
        Try
            customclass.uploadlist = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spLendingDrawdownRequestList").Tables(0)
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Private Const spLendingRateListPaging As String = "spLendingDueInquiry"
    Public Function PaymentByDueDateListPaging(ByVal customclass As Parameter.Lending) As Parameter.Lending

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@StartDate", SqlDbType.Date)
            params(0).Value = customclass.StartDate

            params(1) = New SqlParameter("@EndDate", SqlDbType.Date)
            params(1).Value = customclass.EndDat

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spLendingRateListPaging, params).Tables(0)

            Return customclass
        Catch exp As Exception
            WriteException("Mitra Multifinance", "MitraMultifinancePaging", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function batchlist(ByVal customclass As Parameter.Lending) As Parameter.Lending
        Try
            customclass.batchlist = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "splendingdrawdownapprovallist").Tables(0)
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function lendingdrawdownrequest(ByVal ocls As Parameter.Lending) As Parameter.Lending

        Dim params(2) As SqlParameter
        Dim objcon As New SqlConnection(ocls.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            With ocls
                params(0) = New SqlParameter("@mfid", SqlDbType.Char, 20)
                params(0).Value = .mfid
                params(1) = New SqlParameter("@mffacilityno", SqlDbType.Char, 20)
                params(1).Value = .mffacilityno
                params(2) = New SqlParameter("@mfbatchno", SqlDbType.Char, 20)
                params(2).Value = .mfbatchno
            End With
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spLendingDrawdownRequest", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
        Return ocls
    End Function
    Public Function lendingdrawdownapproval(ByVal ocls As Parameter.Lending) As Parameter.Lending

        Dim params(4) As SqlParameter
        Dim objcon As New SqlConnection(ocls.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            With ocls
                params(0) = New SqlParameter("@mfid", SqlDbType.Char, 20)
                params(0).Value = .mfid
                params(1) = New SqlParameter("@mffacilityno", SqlDbType.Char, 20)
                params(1).Value = .mffacilityno
                params(2) = New SqlParameter("@mfbatchno", SqlDbType.Char, 20)
                params(2).Value = .mfbatchno
                params(3) = New SqlParameter("@BranchID", .BranchId)
                params(4) = New SqlParameter("@BusinessDate", .BusinessDate)
            End With
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spLendingDrawdownapproval", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
        Return ocls
    End Function

    Public Function disburselist(ByVal customclass As Parameter.Lending) As Parameter.Lending
        Try
            customclass.disburselist = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spLendingDisburseList").Tables(0)
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function disburse(ByVal ocls As Parameter.Lending) As Parameter.Lending

        Dim params(11) As SqlParameter
        Dim objcon As New SqlConnection(ocls.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            With ocls
                params(0) = New SqlParameter("@udt_disburselist", .disburselist)
                params(1) = New SqlParameter("@BranchID", .BranchId)
                params(2) = New SqlParameter("@valuedate", .ValueDate)
                params(3) = New SqlParameter("@BusinessDate", .BusinessDate)
                params(4) = New SqlParameter("@LoginID", .LoginId)
                params(5) = New SqlParameter("@WOP", .wop)
                params(6) = New SqlParameter("@RecipientName", .mfname)
                params(7) = New SqlParameter("@ReferenceNo", .disburserefno)
                params(8) = New SqlParameter("@BankAccountID", .bankaccountid)
                params(9) = New SqlParameter("@Notes", .disbursenotes)
                params(10) = New SqlParameter("@acc", .totalaccounts)
                params(11) = New SqlParameter("@Amount", .disburseamount)

            End With
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "splendingdisburse", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
        Return ocls
    End Function


#Region "DisbursementHeaderList"
    Public Function DisbursementHeaderList(ByVal strConnection As String) As DataTable
        Dim oDataTable As New DataTable

        Try
            oDataTable = SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, "spDisbursementHeaderList").Tables(0)
        Catch exp As Exception
            WriteException("MitraMultifinance", "DisbursementHeaderList", exp.Message + exp.StackTrace)
        End Try

        Return oDataTable
    End Function

#End Region

#Region "LendingValidationList"
    Public Function LendingValidationList(ByVal strConnection As String) As List(Of Parameter.AgreementToValidate)
        Dim oAgreementToValidateList As New List(Of AgreementToValidate)

        Try
            Dim oDataTable As New DataTable
            oDataTable = SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, "spLendingValidationList").Tables(0)

            For Each row As DataRow In oDataTable.Rows
                Dim oAgreementToValidate As New AgreementToValidate

                'Agreement
                oAgreementToValidate.MFID = Convert.ToString(row("MFID"))
                oAgreementToValidate.MFBatchNo = Convert.ToString(row("MFBatchNo"))
                oAgreementToValidate.MFFacilityNo = Convert.ToString(row("MFFacilityNo"))
                oAgreementToValidate.MFAgreementNo = Convert.ToString(row("MFAgreementNo"))
                oAgreementToValidate.AgreementDate = Convert.ToDateTime(row("AgreementDate"))
                oAgreementToValidate.EffectiveDate = Convert.ToDateTime(row("EffectiveDate"))
                oAgreementToValidate.MaturityDate = Convert.ToDateTime(row("MaturityDate"))
                oAgreementToValidate.TotalOTR = Convert.ToDecimal(row("TotalOTR"))
                oAgreementToValidate.NTFToMF = Convert.ToDecimal(row("NTFToMF"))
                oAgreementToValidate.TenorDebtor = Convert.ToInt32(row("TenorDebtor"))
                oAgreementToValidate.DownPayment = Convert.ToDecimal(row("DownPayment"))
                oAgreementToValidate.TenorMF = Convert.ToInt32(row("TenorMF"))

                'Debitur
                oAgreementToValidate.BirthDate = Convert.ToDateTime(row("BirthDate"))
                oAgreementToValidate.MaritalStatus = Convert.ToString(row("MaritalStatus"))
                oAgreementToValidate.PersonalCustType = Convert.ToString(row("PersonalCustType"))
                oAgreementToValidate.DebtorType = Convert.ToString(row("DebtorType"))
                oAgreementToValidate.NPWP = Convert.ToString(row("NPWP"))
                oAgreementToValidate.NoTDP = Convert.ToString(row("NoTDP"))
                oAgreementToValidate.NoSIUP = Convert.ToString(row("NoSIUP"))

                'Asset
                oAgreementToValidate.MfgCountry = Convert.ToString(row("MfgCountry"))
                oAgreementToValidate.Merk = Convert.ToString(row("Merk"))
                oAgreementToValidate.AssetDescription = Convert.ToString(row("AssetDescription"))
                oAgreementToValidate.AssetCondition = Convert.ToString(row("AssetCondition"))
                oAgreementToValidate.AssetConditionFacility = Convert.ToString(row("AssetConditionFacility"))
                oAgreementToValidate.DrawdownStartDate = Convert.ToDateTime(row("DrawdownStartDate"))
                oAgreementToValidate.DrawdownEndDate = Convert.ToDateTime(row("DrawdownEndDate"))
                oAgreementToValidate.AssetUsage = Convert.ToString(row("AssetUsage"))
                oAgreementToValidate.MfgYear = Convert.ToString(row("MfgYear"))

                'Facility
                oAgreementToValidate.MaxTenorFacility = Convert.ToString(row("MaxTenorFacility"))
                oAgreementToValidate.MfgCountryFacility = Convert.ToString(row("MfgCountryFacility"))
                oAgreementToValidate.AssetBrandFacility = Convert.ToString(row("AssetBrandFacility"))
                oAgreementToValidate.MaxAssetAgeFacility = Convert.ToString(row("MaxAssetAgeFacility"))
                oAgreementToValidate.MinDownPaymentFacility = Convert.ToString(row("MinDownPaymentFacility"))

                oAgreementToValidateList.Add(oAgreementToValidate)
            Next
        Catch exp As Exception
            WriteException("MitraMultifinance", "LendingValidationList", exp.Message + exp.StackTrace)
        End Try

        Return oAgreementToValidateList
    End Function

#End Region

#Region "DisburseAgreementApproveReject"
    Public Function DisburseAgreementApproveReject(ByVal oDisburseAgreement As Parameter.DisburseAgreement) As Boolean
        Dim oReturnValue As Boolean = False

        Dim params(5) As SqlParameter
        Dim objcon As New SqlConnection(oDisburseAgreement.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            With oDisburseAgreement
                params(0) = New SqlParameter("@MFID", SqlDbType.Char, 10)
                params(0).Value = .MFID
                params(1) = New SqlParameter("@MFBatchNo", SqlDbType.Char, 20)
                params(1).Value = .MFBatchNo
                params(2) = New SqlParameter("@MFAgreementNo", SqlDbType.Char, 20)
                params(2).Value = .MFAgreementNo
                params(3) = New SqlParameter("@MFFacilityNo", SqlDbType.Char, 20)
                params(3).Value = .MFFacilityNo
                params(4) = New SqlParameter("@Status", SqlDbType.Char, 1)
                params(4).Value = .Status
                params(5) = New SqlParameter("@StatusReason", SqlDbType.VarChar, 250)
                params(5).Value = .StatusReason
            End With
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spDisburseAgreementApproveReject", params)
            objtrans.Commit()
            oReturnValue = True
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Mitra Multifinance", "DisburseAgreementApproveReject", exp.Message + exp.StackTrace)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try

        Return oReturnValue
    End Function
#End Region

#Region "DisburseHeaderUpdate"
    Public Function DisburseHeaderUpdate(ByVal oDisburseAgreement As Parameter.DisburseAgreement) As Boolean
        Dim oReturnValue As Boolean = False

        Dim params(2) As SqlParameter
        Dim objcon As New SqlConnection(oDisburseAgreement.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            With oDisburseAgreement
                params(0) = New SqlParameter("@MFID", SqlDbType.Char, 10)
                params(0).Value = .MFID
                params(1) = New SqlParameter("@MFBatchNo", SqlDbType.Char, 20)
                params(1).Value = .MFBatchNo
                params(2) = New SqlParameter("@MFFacilityNo", SqlDbType.Char, 20)
                params(2).Value = .MFFacilityNo
            End With
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spDisburseHeaderUpdate", params)
            objtrans.Commit()
            oReturnValue = True
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Mitra Multifinance", "DisburseAgreementApproveReject", exp.Message + exp.StackTrace)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try

        Return oReturnValue
    End Function

    Public Function lendingpayment(ByVal ocls As Parameter.Lending) As Parameter.Lending
        Dim params(9) As SqlParameter
        Dim objcon As New SqlConnection(ocls.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            With ocls
                params(0) = New SqlParameter("@mfid", .mfid)
                params(1) = New SqlParameter("@mffacilityno", .mffacilityno)
                params(2) = New SqlParameter("@mfreferenceno", .paymentreferenceno)
                params(3) = New SqlParameter("@businessdate", .BusinessDate)
                params(4) = New SqlParameter("@valuedate", .ValueDate)
                params(5) = New SqlParameter("@wop", .wop)
                params(6) = New SqlParameter("@bankaccountid", .bankaccountid)
                params(7) = New SqlParameter("@referenceno", .referenceno)
                params(8) = New SqlParameter("@notes", .paymentnotes)
                params(9) = New SqlParameter("@t1", .paymentlist)
            End With
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "splendingpaymentsave", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
        Return ocls
    End Function

#Region "GetLendingFacilityByCode"
    Public Function GetLendingFacilityByCode(ByVal mfCode As String, ByVal facilityNo As String, ByVal strConnection As String) As Parameter.LendingFacility
        Dim oReturnValue As New Parameter.LendingFacility
        Dim oDataTable As New DataTable

        Dim params(5) As SqlParameter

        params(0) = New SqlParameter("@MFCode", SqlDbType.Char, 10)
        params(0).Value = mfCode

        params(0) = New SqlParameter("@MFCode", SqlDbType.Char, 10)
        params(0).Value = mfCode

        Try
            oDataTable = SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, "spGetLendingFacilityByCode", params).Tables(0)
        Catch exp As Exception
            WriteException("MitraMultifinance", "DisbursementHeaderList", exp.Message + exp.StackTrace)
        End Try

        Return oReturnValue
    End Function
#End Region
#End Region

    Public Function lendinginstallmentinfo(ByVal ocls As Parameter.Lending) As Parameter.Lending
        Dim params(2) As SqlParameter
        Dim objcon As New SqlConnection(ocls.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            With ocls
                params(0) = New SqlParameter("@startdate", .duedatestart)
                params(1) = New SqlParameter("@enddate", .duedateend)
                params(2) = New SqlParameter("@ispaid", .isinstallmentpaid)
                .installmentinfo = SqlHelper.ExecuteDataset(objtrans, CommandType.StoredProcedure, "spLendingInstallmentInfo", params)
            End With
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
        Return ocls
    End Function
End Class
