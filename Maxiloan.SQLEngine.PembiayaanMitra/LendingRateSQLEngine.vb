﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Parameter
Imports Maxiloan.Parameter.Lending
#End Region

Public Class LendingRateSQLEngine : Inherits DataAccessBase
    Private Const spLendingRateListPaging As String = "spLendingRateListPaging"
    Private Const spLendingRateDML As String = "spLendingRateDML"
    Private Const spLendingBatchListPaging As String = "spLendingBatchListPaging"

    Public Function DraftSoftcopy(ByVal oCompany As Parameter.Lending) As DataSet
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@MFID", SqlDbType.VarChar, 10)
        params(0).Value = oCompany.mfid

        params(1) = New SqlParameter("@MFBatchNo", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.mfbatchno

        Try
            Return SqlHelper.ExecuteDataset(oCompany.strConnection, CommandType.StoredProcedure, "spMFID_260340", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function lendingBatchListPaging(ByVal customclass As Parameter.Lending) As Parameter.Lending

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spLendingBatchListPaging, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("Mitra Multifinance", "MitraMultifinancePaging", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function lendingRateListPaging(ByVal customclass As LendingRateProperty) As LendingRateProperty

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spLendingRateListPaging, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("Mitra Multifinance", "MitraMultifinancePaging", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub lendingRateDML(ByVal dml As String, ByVal customclass As LendingRateProperty)

        Dim params(8) As SqlParameter
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            With customclass
                params(0) = New SqlParameter("@DML", dml)
                params(1) = New SqlParameter("@MFCode", .MFCode)
                params(2) = New SqlParameter("@MFFacilityNo", .MFFacilityNo)
                params(3) = New SqlParameter("@TenorFrom", .TenorFrom)
                params(4) = New SqlParameter("@TenorTo", .TenorTo)
                params(5) = New SqlParameter("@UsedRate", .UsedRate)
                params(6) = New SqlParameter("@NewRate", .NewRate)
                params(7) = New SqlParameter("@UserUpdate", .UserUpdate)
                params(8) = New SqlParameter("@DateUpdate", .DateUpdate)
            End With

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spLendingRateDML, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
End Class
