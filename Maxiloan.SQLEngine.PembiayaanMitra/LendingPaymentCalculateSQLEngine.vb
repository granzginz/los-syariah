﻿Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Parameter

Public Class LendingPaymentCalculateSQLEngine : Inherits DataAccessBase
    Private Const spLendingPrepaymentCalculate As String = "spLendingPrepaymentCalculate"
    Private Const spLendingPrepaymentRequest As String = "spLendingPrepaymentRequest"
    Private Const spProcessCreateCashBankTransactionsLending As String = "spProcessCreateCashBankTransactionsLending "

    Public Function lendingPrepaymentCalculate(ByVal customclass As PaymentCalculateProperty) As PaymentCalculateProperty
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@mfagreementno", SqlDbType.NVarChar, 1000)
            params(0).Value = customclass.MFAGREEMENTNO

            params(1) = New SqlParameter("@mfid", SqlDbType.NVarChar, 1000)
            params(1).Value = customclass.MFID

            params(2) = New SqlParameter("@mfbatchno", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.MFBATCHNO

            params(3) = New SqlParameter("@mffacilityno", SqlDbType.VarChar, 100)
            params(3).Value = customclass.MFFACILITYNO

            params(4) = New SqlParameter("@tanggalefektif", SqlDbType.Date)
            params(4).Value = customclass.TANGGALEFEKTIF

            With customclass
                .ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spLendingPrepaymentCalculate, params).Tables(0)
                .SISAPOKOK = customclass.ListData.Rows(0)("osp")
                .OSINTEREST = customclass.ListData.Rows(0)("osi")
                .PAIDSEQNO = customclass.ListData.Rows(0)("tenorpelunasan")
                .BUNGABERJALAN = customclass.ListData.Rows(0)("accruedinterest")
                .DENDAPERUSAHAAN = customclass.ListData.Rows(0)("prepaymentpenalty")
                .totalpelunasan = customclass.ListData.Rows(0)("prepaymentamount")
            End With

            Return customclass
        Catch exp As Exception
            WriteException("Mitra Multifinance", "PaymentCalculate", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Sub setLendingCalculate(ByVal customclass As PaymentCalculateProperty)
        Dim isSuccess As Boolean
        isSuccess = False

        Dim params(7) As SqlParameter
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            With customclass
                params(0) = New SqlParameter("@mfagreementno", .MFAGREEMENTNO)
                params(1) = New SqlParameter("@effectivedate", .TANGGALEFEKTIF)
                params(2) = New SqlParameter("@insseqno", .PAIDSEQNO)
                params(3) = New SqlParameter("@osp", .SISAPOKOK)
                params(4) = New SqlParameter("@osi", .OSINTEREST)
                params(5) = New SqlParameter("@accruedinterest", .BUNGABERJALAN)
                params(6) = New SqlParameter("@penalty", .DENDAPERUSAHAAN)
                params(7) = New SqlParameter("@prepaymentamount", .totalpelunasan)
            End With

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spLendingPrepaymentRequest, params)
            objtrans.Commit()
            isSuccess = True

        Catch exp As Exception
            objtrans.Rollback()
            isSuccess = False
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()

            If isSuccess Then
                setCashBankTransactionLending(customclass)
            End If

        End Try
    End Sub

    Public Sub setCashBankTransactionLending(ByVal customclass As PaymentCalculateProperty)
        Dim params(10) As SqlParameter
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            With customclass
                params(0) = New SqlParameter("@udt_disburselist", .DisbList)
                params(1) = New SqlParameter("@BranchID", .BranchId)
                params(2) = New SqlParameter("@BusinessDate", .BusinessDate)
                params(3) = New SqlParameter("@LoginID", .LoginId)
                params(4) = New SqlParameter("@WOP", .BankAccountID)
                params(5) = New SqlParameter("@RecipientName", .RecipientName)
                params(6) = New SqlParameter("@ReferenceNo", .ReferenceNo)
                params(7) = New SqlParameter("@BankAccountID", .BankAccountID)
                params(8) = New SqlParameter("@Amount", .totalpelunasan)
                params(9) = New SqlParameter("@Notes", .Notes)
                params(10) = New SqlParameter("@ValueDate", .DateValue)
            End With

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spProcessCreateCashBankTransactionsLending, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub

End Class
