﻿
Imports System.Data.SqlClient
Imports System.Transactions
Imports Maxiloan.Parameter

Public Class LendingProcessDataAccess : Inherits Maxiloan.SQLEngine.DataAccessBase

    Public Function ProcessUploadCsvLending(cnn As String, data As Dictionary(Of String, IList(Of AbsDisb))) As String
        Dim result As String = ""
        Dim cmdText As String = ""
        Try
            ' Create the TransactionScope to execute the commands, guaranteeing that both commands can commit or roll back as a single unit of work.
            Using scope As New TransactionScope()
                Using connection1 As New SqlConnection(cnn)
                    connection1.Open()
                    For Each disb In data
                        For Each disbItem In disb.Value
                            Dim command1 As SqlCommand = disbItem.command(connection1)
                            cmdText = command1.CommandText
                            command1.ExecuteNonQuery()
                        Next
                    Next
                End Using
                scope.Complete()
            End Using
        Catch ex As TransactionAbortedException
            result = $"TransactionAbortedException Message: {ex.Message}"
            Throw New TransactionAbortedException(result)
        Catch ex As ApplicationException
            result = $"ApplicationException  Message: {ex.Message}"
            Throw New ApplicationException(result)
        Catch ex As Exception
            result = $"Exception {cmdText } Message: {ex.Message}"
            Throw New Exception(result)
        End Try

        Return result
    End Function
End Class
