﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Parameter
#End Region

Public Class JFDocument : Inherits DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spMitraMFProductDocumentList"
    Private Const LIST_BY_ID As String = "spMitraMFProductDocumentEdit"
    Private Const LIST_ADD As String = "spMitraMFProductDocumentSaveAdd"
    Private Const LIST_UPDATE As String = "spMitraMFProductDocumentSaveEdit"
    Private Const LIST_DELETE As String = "spMitraMFProductDocumentDelete"
#End Region
    Public Function GetJFDocument(ByVal customclass As Parameter.JFDocument) As Parameter.JFDocument

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customclass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customclass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
        Catch ex As Exception
        End Try

        Return customclass
    End Function
    Public Function GetSelectJFDocument(ocustomClass As Parameter.JFDocument) As Parameter.JFDocument
        Dim params(2) As SqlParameter
        Try
            params(0) = New SqlParameter("@ProductCode", SqlDbType.VarChar, 20)
            params(0).Value = ocustomClass.ProductCode
            params(1) = New SqlParameter("@DocumentCode", SqlDbType.VarChar, 20)
            params(1).Value = ocustomClass.DocumentCode
            params(2) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(2).Value = ""

            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, ocustomClass.spName, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception

        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetJFDocumentEdit(ByVal ocustomClass As Parameter.JFDocument) As Parameter.JFDocument
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.JFDocument
        params(0) = New SqlParameter("@ProductCode", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.ProductCode
        params(1) = New SqlParameter("@DocumentCode", SqlDbType.Char, 20)
        params(1).Value = ocustomClass.DocumentCode
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.DocumentName = reader("DocumentName").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Document.GetJFDocumentEdit")
        End Try
    End Function

    Public Function JFDocumentSaveAdd(ByVal ocustomClass As Parameter.JFDocument) As String
        Dim ErrMessage As String = ""
        Dim params(11) As SqlParameter

        params(0) = New SqlParameter("@ProductCode", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ProductCode
        params(1) = New SqlParameter("@DocumentCode", SqlDbType.VarChar, 20)
        params(1).Value = ocustomClass.DocumentCode
        params(2) = New SqlParameter("@DocumentName", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.DocumentName
        params(3) = New SqlParameter("@IsPersonal", SqlDbType.Bit)
        params(3).Value = ocustomClass.IsPersonal
        params(4) = New SqlParameter("@IsCompany", SqlDbType.Bit)
        params(4).Value = ocustomClass.IsCompany
        params(5) = New SqlParameter("@IsMandatory", SqlDbType.Bit)
        params(5).Value = ocustomClass.IsMandatory
        params(6) = New SqlParameter("@PersonalGender", SqlDbType.VarChar, 1)
        params(6).Value = ocustomClass.PersonalGender
        params(7) = New SqlParameter("@CreatedBy", SqlDbType.VarChar, 50)
        params(7).Value = ocustomClass.CreatedBy
        params(8) = New SqlParameter("@CreatedDate", SqlDbType.DateTime)
        params(8).Value = ocustomClass.CreatedDate
        params(9) = New SqlParameter("@ChangedBy", SqlDbType.VarChar, 20)
        params(9).Value = ocustomClass.ChangedBy
        params(10) = New SqlParameter("@ChangedDate", SqlDbType.DateTime)
        params(10).Value = ocustomClass.ChangedDate

        params(11) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(11).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(11).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Document.JFDocumentSaveAdd")
        End Try
    End Function

    Public Sub JFDocumentSaveEdit(ByVal ocustomClass As Parameter.JFDocument)
        Dim ErrMessage As String = ""
        Dim params(11) As SqlParameter
        params(0) = New SqlParameter("@ProductCode", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ProductCode
        params(1) = New SqlParameter("@DocumentCode", SqlDbType.VarChar, 20)
        params(1).Value = ocustomClass.DocumentCode
        params(2) = New SqlParameter("@DocumentName", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.DocumentName
        params(3) = New SqlParameter("@IsPersonal", SqlDbType.Bit)
        params(3).Value = ocustomClass.IsPersonal
        params(4) = New SqlParameter("@IsCompany", SqlDbType.Bit)
        params(4).Value = ocustomClass.IsCompany
        params(5) = New SqlParameter("@IsMandatory", SqlDbType.Bit)
        params(5).Value = ocustomClass.IsMandatory
        params(6) = New SqlParameter("@PersonalGender", SqlDbType.VarChar, 1)
        params(6).Value = ocustomClass.PersonalGender
        params(7) = New SqlParameter("@CreatedBy", SqlDbType.VarChar, 50)
        params(7).Value = ocustomClass.CreatedBy
        params(8) = New SqlParameter("@CreatedDate", SqlDbType.DateTime)
        params(8).Value = ocustomClass.CreatedDate
        params(9) = New SqlParameter("@ChangedBy", SqlDbType.VarChar, 20)
        params(9).Value = ocustomClass.ChangedBy
        params(10) = New SqlParameter("@ChangedDate", SqlDbType.DateTime)
        params(10).Value = ocustomClass.ChangedDate

        params(11) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(11).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Document.JFDocumentSaveEdit")
        End Try
    End Sub

    Public Function JFDocumentDelete(ByVal ocustomClass As Parameter.JFDocument) As String
        Dim Err As Integer
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ProductCode", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ProductCode
        params(1) = New SqlParameter("@DocumentCode", SqlDbType.VarChar, 20)
        params(1).Value = ocustomClass.DocumentCode
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            'Throw New Exception("Error On DataAccess.Setting.Referal.ReferalDelete")
        End Try
    End Function
End Class
