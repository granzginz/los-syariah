﻿

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class MitraMultifinance : Inherits DataAccessBase
    Private Const spMITRA_LIST As String = "spMitraMultifinanceList"
    Private Const spMITRA_BYID As String = "spMitraMultifinanceByID"
    Private Const spMITRA_ADD As String = "spMitraMultifinanceAdd"
    Private Const spMITRA_EDIT As String = "spMitraMultifinanceEdit"
    Private Const spMITRA_DEL As String = "spMitraMultifinanceDelete"

    Private Const spRekMITRA_LIST As String = "spRekMitraMultifinanceList"
    Private Const spRekMITRA_BYID As String = "spRekMitraMultifinanceByID"
    Private Const spRekMITRA_ADD As String = "spRekMitraMultifinanceAdd"
    Private Const spRekMITRA_EDIT As String = "spRekMitraMultifinanceEdit"
    Private Const spRekMITRA_DEL As String = "spRekMitraMultifinanceDelete"


#Region "MITRA MULTIFINANCE"

#Region "MitraList"
    Public Function MitraList(ByVal customclass As Parameter.MitraMultifinance) As Parameter.MitraMultifinance

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMITRA_LIST, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("Mitra Multifinance", "MitraMultifinancePaging", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "MitrabyID"
    Public Function ListMitraByID(ByVal customclass As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond

        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMITRA_BYID, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("Mitra Multifinance", "ListMitraByID", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region " MitraAdd"
    Public Sub MitraAdd(ByVal oMitra As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address)

        Dim params(17) As SqlParameter
        Dim objcon As New SqlConnection(oMitra.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@MfCode", SqlDbType.Char, 10)
            params(0).Value = oMitra.MitraID
            params(1) = New SqlParameter("@FullName", SqlDbType.VarChar, 50)
            params(1).Value = oMitra.MitraFullName
            params(2) = New SqlParameter("@InitialName", SqlDbType.Char, 20)
            params(2).Value = oMitra.MitraInitialName
            params(3) = New SqlParameter("@Address", SqlDbType.Char, 100)
            params(3).Value = oClassAddress.Address
            params(4) = New SqlParameter("@RT", SqlDbType.Char, 3)
            params(4).Value = oClassAddress.RT
            params(5) = New SqlParameter("@RW", SqlDbType.Char, 3)
            params(5).Value = oClassAddress.RW
            params(6) = New SqlParameter("@VillageName", SqlDbType.VarChar, 50)
            params(6).Value = oClassAddress.Kelurahan
            params(7) = New SqlParameter("@District", SqlDbType.VarChar, 50)
            params(7).Value = oClassAddress.Kecamatan
            params(8) = New SqlParameter("@City", SqlDbType.VarChar, 50)
            params(8).Value = oClassAddress.City
            params(9) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
            params(9).Value = oClassAddress.ZipCode
            params(10) = New SqlParameter("@ProvinceName", SqlDbType.Char, 50)
            params(10).Value = oMitra.Provinsi
            params(11) = New SqlParameter("@Phone1", SqlDbType.Char, 20)
            params(11).Value = oMitra.Phone1
            params(12) = New SqlParameter("@Phone2", SqlDbType.Char, 20)
            params(12).Value = oMitra.Phone2
            params(13) = New SqlParameter("@Fax", SqlDbType.Char, 20)
            params(13).Value = oMitra.Fax
            params(14) = New SqlParameter("@Website", SqlDbType.VarChar, 50)
            params(14).Value = oMitra.WebSite
            params(15) = New SqlParameter("@JoinDate", SqlDbType.SmallDateTime)
            params(15).Value = oMitra.JoinDate
            params(16) = New SqlParameter("@Notes", SqlDbType.VarChar, 500)
            params(16).Value = oMitra.Notes
            params(17) = New SqlParameter("@UsrCrt", SqlDbType.Char, 20)
            params(17).Value = oMitra.LoginId

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spMITRA_ADD, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Mitra Multifinance", "MitraAdd", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region " MitraEdit"
    Public Sub MitraEdit(ByVal oMitra As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address)
        Dim params(17) As SqlParameter
        Dim objcon As New SqlConnection(oMitra.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@MfCode", SqlDbType.Char, 10)
            params(0).Value = oMitra.MitraID
            params(1) = New SqlParameter("@FullName", SqlDbType.VarChar, 50)
            params(1).Value = oMitra.MitraFullName
            params(2) = New SqlParameter("@InitialName", SqlDbType.Char, 20)
            params(2).Value = oMitra.MitraInitialName
            params(3) = New SqlParameter("@Address", SqlDbType.Char, 100)
            params(3).Value = oClassAddress.Address
            params(4) = New SqlParameter("@RT", SqlDbType.Char, 3)
            params(4).Value = oClassAddress.RT
            params(5) = New SqlParameter("@RW", SqlDbType.Char, 3)
            params(5).Value = oClassAddress.RW
            params(6) = New SqlParameter("@VillageName", SqlDbType.VarChar, 50)
            params(6).Value = oClassAddress.Kelurahan
            params(7) = New SqlParameter("@District", SqlDbType.VarChar, 50)
            params(7).Value = oClassAddress.Kecamatan
            params(8) = New SqlParameter("@City", SqlDbType.VarChar, 50)
            params(8).Value = oClassAddress.City
            params(9) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
            params(9).Value = oClassAddress.ZipCode
            params(10) = New SqlParameter("@ProvinceName", SqlDbType.Char, 50)
            params(10).Value = oMitra.Provinsi
            params(11) = New SqlParameter("@Phone1", SqlDbType.Char, 20)
            params(11).Value = oMitra.Phone1
            params(12) = New SqlParameter("@Phone2", SqlDbType.Char, 20)
            params(12).Value = oMitra.Phone2
            params(13) = New SqlParameter("@Fax", SqlDbType.Char, 20)
            params(13).Value = oMitra.Fax
            params(14) = New SqlParameter("@Website", SqlDbType.VarChar, 50)
            params(14).Value = oMitra.WebSite
            params(15) = New SqlParameter("@JoinDate", SqlDbType.DateTime)
            params(15).Value = oMitra.JoinDate
            params(16) = New SqlParameter("@Notes", SqlDbType.VarChar, 500)
            params(16).Value = oMitra.Notes
            params(17) = New SqlParameter("@UsrCrt", SqlDbType.Char, 20)
            params(17).Value = oMitra.LoginId


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spMITRA_EDIT, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Mitra Multifinance", "MitraEdit", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region "MitraDelete"
    Public Sub MitraDelete(ByVal oMitra As Parameter.MitraMultifinance)

        Dim params(0) As SqlParameter
        Dim objcon As New SqlConnection(oMitra.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            With oMitra
                params(0) = New SqlParameter("@MfCode", SqlDbType.Char, 10)
                params(0).Value = .MitraID

            End With
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spMITRA_DEL, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Mitra Multifinance", "MitraDelete", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region "RekeningMitraList"

    Public Function RekeningMitraList(ByVal customclass As Parameter.MitraMultifinance) As Parameter.MitraMultifinance

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRekMITRA_LIST, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("Mitra Multifinance", "MitraMultifinancePaging", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "RekeningMitraListByID"
    Public Function RekeningMitraListByID(ByVal customclass As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond

        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRekMITRA_BYID, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("Mitra Multifinance", "RekeningListMitraByID", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region " RekeningMitraAdd"
    Public Sub RekeningMitraAdd(ByVal oMitra As Parameter.MitraMultifinance)

        Dim params(6) As SqlParameter
        Dim objcon As New SqlConnection(oMitra.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@MfCode", SqlDbType.Char, 10)
            params(0).Value = oMitra.MitraID
            params(1) = New SqlParameter("@BankID", SqlDbType.Char, 3)
            params(1).Value = oMitra.BankID
            params(2) = New SqlParameter("@BranchCode", SqlDbType.Char, 10)
            params(2).Value = oMitra.BranchCode
            params(3) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
            params(3).Value = oMitra.AccountName
            params(4) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 25)
            params(4).Value = oMitra.AccountNo
            params(5) = New SqlParameter("@AccountType", SqlDbType.Char, 1)
            params(5).Value = oMitra.AccountType
            params(6) = New SqlParameter("@UsrCrt", SqlDbType.Char, 20)
            params(6).Value = oMitra.LoginId

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spRekMITRA_ADD, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Mitra Multifinance", "MitraAdd", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region "RekeningMitraEdit"
    Public Sub RekeningMitraEdit(ByVal oMitra As Parameter.MitraMultifinance)
        Dim params(6) As SqlParameter
        Dim objcon As New SqlConnection(oMitra.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@MfCode", SqlDbType.Char, 10)
            params(0).Value = oMitra.MitraID
            params(1) = New SqlParameter("@BankID", SqlDbType.Char, 3)
            params(1).Value = oMitra.BankID
            params(2) = New SqlParameter("@BranchCode", SqlDbType.Char, 10)
            params(2).Value = oMitra.BranchCode
            params(3) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
            params(3).Value = oMitra.AccountName
            params(4) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 25)
            params(4).Value = oMitra.AccountNo
            params(5) = New SqlParameter("@AccountType", SqlDbType.Char, 1)
            params(5).Value = oMitra.AccountType
            params(6) = New SqlParameter("@UsrUpd", SqlDbType.Char, 20)
            params(6).Value = oMitra.LoginId


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spRekMITRA_EDIT, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Mitra Multifinance", "MitraEdit", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region "RekeningMitraDelete"
    Public Sub RekeningMitraDelete(ByVal oMitra As Parameter.MitraMultifinance)

        Dim params(1) As SqlParameter
        Dim objcon As New SqlConnection(oMitra.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            With oMitra
                params(0) = New SqlParameter("@MfCode", SqlDbType.Char, 10)
                params(0).Value = .MitraID
                params(1) = New SqlParameter("@AccountNo", SqlDbType.Char, 25)
                params(1).Value = .AccountNo

            End With
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spRekMITRA_DEL, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Mitra Multifinance", "MitraDelete", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region
#End Region


End Class
