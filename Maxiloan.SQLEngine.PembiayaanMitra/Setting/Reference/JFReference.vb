﻿

#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region


Public Class JFReference : Inherits DataAccessBase

    Private Const spJFReferenceList As String = "spJFReferenceList"
    Private Const spJFReferenceAdd As String = "spJFReferenceAdd"
    Private Const spJFReferenceEdit As String = "spJFReferenceEdit"
    Private Const spJFReferenceDelete As String = "spJFReferenceDelete"
    Private Const spJFReferenceListByID As String = "spJFReferenceListByID"

    Public Function JFReferenceList(ByVal customclass As Parameter.JFReference) As Parameter.JFReference

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customclass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customclass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spJFReferenceList, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
        Catch ex As Exception
        End Try

        Return customclass
    End Function

    Public Sub JFReferenceAdd(ByVal customclass As Parameter.JFReference)
        Dim oSaveTransaction As SqlTransaction
        Dim oConnection As New SqlConnection(customclass.strConnection)

        Dim params() As SqlParameter = New SqlParameter(3) {}

        params(0) = New SqlParameter("@GroupID", SqlDbType.VarChar, 30)
        params(0).Value = customclass.GroupID
        params(1) = New SqlParameter("@ReferenceKey", SqlDbType.VarChar, 30)
        params(1).Value = customclass.ReferenceKey
        params(2) = New SqlParameter("@ReferenceDescription", SqlDbType.VarChar, 200)
        params(2).Value = customclass.ReferenceDescription
        params(3) = New SqlParameter("@LoginId", SqlDbType.Char, 12)
        params(3).Value = customclass.LoginId

        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spJFReferenceAdd, params)
            oSaveTransaction.Commit()
        Catch ex As Exception
            oSaveTransaction.Rollback()
            Throw ex
        End Try
    End Sub

    Public Sub JFReferenceEdit(ByVal customclass As Parameter.JFReference)
        Dim oSaveTransaction As SqlTransaction
        Dim oConnection As New SqlConnection(customclass.strConnection)

        Dim params() As SqlParameter = New SqlParameter(3) {}

        params(0) = New SqlParameter("@GroupID", SqlDbType.VarChar, 30)
        params(0).Value = customclass.GroupID
        params(1) = New SqlParameter("@ReferenceKey", SqlDbType.VarChar, 30)
        params(1).Value = customclass.ReferenceKey
        params(2) = New SqlParameter("@ReferenceDescription", SqlDbType.VarChar, 200)
        params(2).Value = customclass.ReferenceDescription
        params(3) = New SqlParameter("@LoginId", SqlDbType.Char, 12)
        params(3).Value = customclass.LoginID

        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spJFReferenceEdit, params)
            oSaveTransaction.Commit()
        Catch ex As Exception
            oSaveTransaction.Rollback()
            Throw ex
        End Try

    End Sub

    Public Sub JFReferenceDelete(ByVal customclass As Parameter.JFReference)
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@GroupID", SqlDbType.VarChar, 30)
        params(0).Value = customclass.GroupID
        params(1) = New SqlParameter("@ReferenceKey", SqlDbType.VarChar, 30)
        params(1).Value = customclass.ReferenceKey

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spJFReferenceDelete, params)
        Catch ex As Exception

        End Try

    End Sub
    Public Function JFReferenceListByID(ByVal customclass As Parameter.JFReference) As Parameter.JFReference
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond

        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spJFReferenceListByID, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("Mitra Multifinance", "JFReferenceListByID", exp.Message + exp.StackTrace)
        End Try
    End Function

End Class
