﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Parameter
#End Region

Public Class JFCriteria : Inherits DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spMitraMFProductCriteriaRACList"
    Private Const LIST_BY_ID As String = "spMitraMFProductCriteriaRACEdit"
    Private Const LIST_ADD As String = "spMitraMFProductCriteriaRACSaveAdd"
    Private Const LIST_UPDATE As String = "spMitraMFProductCriteriaRACSaveEdit"
    Private Const LIST_DELETE As String = "spMitraMFProductCriteriaRACDelete"
#End Region
    Public Function GetJFCriteria(ByVal customclass As Parameter.JFCriteria) As Parameter.JFCriteria

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customclass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customclass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
        Catch ex As Exception
        End Try

        Return customclass
    End Function
    Public Function GetSelectJFCriteria(ocustomClass As Parameter.JFCriteria) As Parameter.JFCriteria
        Dim params(2) As SqlParameter
        Try
            params(0) = New SqlParameter("@ProductCode", SqlDbType.VarChar, 20)
            params(0).Value = ocustomClass.ProductCode
            params(1) = New SqlParameter("@ComponentId", SqlDbType.VarChar, 10)
            params(1).Value = ocustomClass.ComponentId
            params(2) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(2).Value = ""

            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, ocustomClass.spName, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception

        End Try
        'Throw New NotImplementedException()
    End Function
    Public Function GetJFCriteriaEdit(ByVal ocustomClass As Parameter.JFCriteria) As Parameter.JFCriteria
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.JFCriteria
        params(0) = New SqlParameter("@ProductCode", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.ProductCode
        params(1) = New SqlParameter("@ComponentId", SqlDbType.Char, 10)
        params(1).Value = ocustomClass.ComponentId
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.CriteriaValue = reader("CriteriaValue").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Document.GetJFDocumentEdit")
        End Try
    End Function
    Public Function JFCriteriaSaveAdd(ByVal ocustomClass As Parameter.JFCriteria) As String
        Dim ErrMessage As String = ""
        Dim params(9) As SqlParameter

        params(0) = New SqlParameter("@ProductCode", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ProductCode
        params(1) = New SqlParameter("@ComponentId", SqlDbType.VarChar, 10)
        params(1).Value = ocustomClass.ComponentId
        params(2) = New SqlParameter("@Active", SqlDbType.Bit)
        params(2).Value = ocustomClass.Active
        params(3) = New SqlParameter("@Opertaor", SqlDbType.VarChar, 5)
        params(3).Value = ocustomClass.Opertaor
        params(4) = New SqlParameter("@CriteriaValue", SqlDbType.VarChar, 200)
        params(4).Value = ocustomClass.CriteriaValue
        params(5) = New SqlParameter("@CreatedBy", SqlDbType.VarChar, 50)
        params(5).Value = ocustomClass.CreatedBy
        params(6) = New SqlParameter("@CreatedDate", SqlDbType.DateTime)
        params(6).Value = ocustomClass.CreatedDate
        params(7) = New SqlParameter("@ChangedBy", SqlDbType.VarChar, 20)
        params(7).Value = ocustomClass.ChangedBy
        params(8) = New SqlParameter("@ChangedDate", SqlDbType.DateTime)
        params(8).Value = ocustomClass.ChangedDate

        params(9) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(9).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(9).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Criteria.JFCriteriaSaveAdd")
        End Try
    End Function
    Public Sub JFCriteriaSaveEdit(ByVal ocustomClass As Parameter.JFCriteria)
        Dim ErrMessage As String = ""
        Dim params(9) As SqlParameter

        params(0) = New SqlParameter("@ProductCode", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ProductCode
        params(1) = New SqlParameter("@ComponentId", SqlDbType.VarChar, 10)
        params(1).Value = ocustomClass.ComponentId
        params(2) = New SqlParameter("@Active", SqlDbType.Bit)
        params(2).Value = ocustomClass.Active
        params(3) = New SqlParameter("@Opertaor", SqlDbType.VarChar, 5)
        params(3).Value = ocustomClass.Opertaor
        params(4) = New SqlParameter("@CriteriaValue", SqlDbType.VarChar, 200)
        params(4).Value = ocustomClass.CriteriaValue
        params(5) = New SqlParameter("@CreatedBy", SqlDbType.VarChar, 50)
        params(5).Value = ocustomClass.CreatedBy
        params(6) = New SqlParameter("@CreatedDate", SqlDbType.DateTime)
        params(6).Value = ocustomClass.CreatedDate
        params(7) = New SqlParameter("@ChangedBy", SqlDbType.VarChar, 20)
        params(7).Value = ocustomClass.ChangedBy
        params(8) = New SqlParameter("@ChangedDate", SqlDbType.DateTime)
        params(8).Value = ocustomClass.ChangedDate

        params(9) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(9).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Criteria.JFCriteriaSaveEdit")
        End Try
    End Sub
    Public Function JFCriteriaDelete(ByVal ocustomClass As Parameter.JFCriteria) As String
        Dim Err As Integer
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ProductCode", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ProductCode
        params(1) = New SqlParameter("@ComponentId", SqlDbType.VarChar, 10)
        params(1).Value = ocustomClass.ComponentId
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            'Throw New Exception("Error On DataAccess.Setting.Referal.ReferalDelete")
        End Try
    End Function
End Class
