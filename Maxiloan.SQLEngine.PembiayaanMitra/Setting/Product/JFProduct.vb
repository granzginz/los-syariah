﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Parameter
#End Region


Public Class JFProduct : Inherits DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spJFProductList"
    Private Const LIST_BY_ID As String = "spMitraMFProductEdit"
    Private Const LIST_ADD As String = "spMitraMFProductSaveAdd"
    Private Const LIST_UPDATE As String = "spMitraMFProductSaveEdit"
    Private Const LIST_DELETE As String = "spMitraMFProductDelete"
    Private Const LIST_REPORT As String = "spMitraMFProductReport"
    Private Const LIST_SECTOR As String = "spMitraMFProductSector"
#End Region
    'Private Const spJFProductList As String = "spJFProductList"

    Public Function GetJFProduct(ByVal customclass As Parameter.JFProduct) As Parameter.JFProduct

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customclass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customclass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
        Catch ex As Exception
        End Try

        Return customclass
    End Function

    Public Function GetSelectJFProduct(ocustomClass As Parameter.JFProduct) As Parameter.JFProduct
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@ProductCode", SqlDbType.VarChar, 20)
            params(0).Value = ocustomClass.ProductCode
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, ocustomClass.SpName, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception

        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetJFProductEdit(ByVal ocustomClass As Parameter.JFProduct) As Parameter.JFProduct
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.JFProduct
        params(0) = New SqlParameter("@ProductCode", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.ProductCode
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.ProductName = reader("ProductName").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Product.GetJFProductEdit")
        End Try
    End Function

    Public Function JFProductSaveAdd(ByVal ocustomClass As Parameter.JFProduct) As String
        Dim ErrMessage As String = ""
        Dim params(21) As SqlParameter

        params(0) = New SqlParameter("@ProductCode", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ProductCode
        params(1) = New SqlParameter("@ProductName", SqlDbType.VarChar, 50)
        params(1).Value = ocustomClass.ProductName
        params(2) = New SqlParameter("@InitialName", SqlDbType.VarChar, 20)
        params(2).Value = ocustomClass.InitialName
        params(3) = New SqlParameter("@AssetTypeCode", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.AssetTypeCode
        params(4) = New SqlParameter("@DisburseApprovalCode", SqlDbType.VarChar, 10)
        params(4).Value = ocustomClass.DisburseApprovalCode
        params(5) = New SqlParameter("@JournalSchemeCode", SqlDbType.VarChar, 10)
        params(5).Value = ocustomClass.JournalSchemeCode
        params(6) = New SqlParameter("@CriteriaRacCode", SqlDbType.VarChar, 20)
        params(6).Value = ocustomClass.CriteriaRacCode
        params(7) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal, 6, 4)
        params(7).Value = ocustomClass.ProvisionFee
        params(8) = New SqlParameter("@AdminFee", SqlDbType.Decimal, 17, 2)
        params(8).Value = ocustomClass.AdminFee
        params(9) = New SqlParameter("@ServiceFee", SqlDbType.Decimal, 6, 4)
        params(9).Value = ocustomClass.ServiceFee
        params(10) = New SqlParameter("@CollateralManagementFee", SqlDbType.Decimal, 17, 2)
        params(10).Value = ocustomClass.CollateralManagementFee
        params(11) = New SqlParameter("@PrepaymentPenaltyFixed", SqlDbType.Decimal, 17, 2)
        params(11).Value = ocustomClass.PrepaymentPenaltyFixed
        params(12) = New SqlParameter("@PrepaymentPenaltyRate", SqlDbType.Decimal, 6, 4)
        params(12).Value = ocustomClass.PrepaymentPenaltyRate
        params(13) = New SqlParameter("@LateChargesRate", SqlDbType.Decimal, 6, 4)
        params(13).Value = ocustomClass.LateChargesRate
        params(14) = New SqlParameter("@LateChargesGracePeriod", SqlDbType.SmallInt)
        params(14).Value = ocustomClass.LateChargesGracePeriod
        params(15) = New SqlParameter("@PaymentPriority", SqlDbType.VarChar, 255)
        params(15).Value = ocustomClass.PaymentPriority
        params(16) = New SqlParameter("@AccruedInterestCalculationType", SqlDbType.VarChar, 1)
        params(16).Value = ocustomClass.AccruedInterestCalculationType
        params(17) = New SqlParameter("@CreatedBy", SqlDbType.VarChar, 50)
        params(17).Value = ocustomClass.CreatedBy
        params(18) = New SqlParameter("@CreatedDate", SqlDbType.DateTime)
        params(18).Value = ocustomClass.CreatedDate
        params(19) = New SqlParameter("@ChangedBy", SqlDbType.VarChar, 20)
        params(19).Value = ocustomClass.ChangedBy
        params(20) = New SqlParameter("@ChangedDate", SqlDbType.DateTime)
        params(20).Value = ocustomClass.ChangedDate

        params(21) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(21).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(21).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Product.JFProductSaveAdd")
        End Try
    End Function
    Public Sub JFProductSaveEdit(ByVal ocustomClass As Parameter.JFProduct)
        Dim ErrMessage As String = ""
        Dim params(21) As SqlParameter
        params(0) = New SqlParameter("@ProductCode", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ProductCode
        params(1) = New SqlParameter("@ProductName", SqlDbType.VarChar, 50)
        params(1).Value = ocustomClass.ProductName
        params(2) = New SqlParameter("@InitialName", SqlDbType.VarChar, 20)
        params(2).Value = ocustomClass.InitialName
        params(3) = New SqlParameter("@AssetTypeCode", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.AssetTypeCode
        params(4) = New SqlParameter("@DisburseApprovalCode", SqlDbType.VarChar, 10)
        params(4).Value = ocustomClass.DisburseApprovalCode
        params(5) = New SqlParameter("@JournalSchemeCode", SqlDbType.VarChar, 10)
        params(5).Value = ocustomClass.JournalSchemeCode
        params(6) = New SqlParameter("@CriteriaRacCode", SqlDbType.VarChar, 20)
        params(6).Value = ocustomClass.CriteriaRacCode
        params(7) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal, 6, 4)
        params(7).Value = ocustomClass.ProvisionFee
        params(8) = New SqlParameter("@AdminFee", SqlDbType.Decimal, 17, 2)
        params(8).Value = ocustomClass.AdminFee
        params(9) = New SqlParameter("@ServiceFee", SqlDbType.Decimal, 6, 4)
        params(9).Value = ocustomClass.ServiceFee
        params(10) = New SqlParameter("@CollateralManagementFee", SqlDbType.Decimal, 17, 2)
        params(10).Value = ocustomClass.CollateralManagementFee
        params(11) = New SqlParameter("@PrepaymentPenaltyFixed", SqlDbType.Decimal, 17, 2)
        params(11).Value = ocustomClass.PrepaymentPenaltyFixed
        params(12) = New SqlParameter("@PrepaymentPenaltyRate", SqlDbType.Decimal, 6, 4)
        params(12).Value = ocustomClass.PrepaymentPenaltyRate
        params(13) = New SqlParameter("@LateChargesRate", SqlDbType.Decimal, 6, 4)
        params(13).Value = ocustomClass.LateChargesRate
        params(14) = New SqlParameter("@LateChargesGracePeriod", SqlDbType.SmallInt)
        params(14).Value = ocustomClass.LateChargesGracePeriod
        params(15) = New SqlParameter("@PaymentPriority", SqlDbType.VarChar, 255)
        params(15).Value = ocustomClass.PaymentPriority
        params(16) = New SqlParameter("@AccruedInterestCalculationType", SqlDbType.VarChar, 1)
        params(16).Value = ocustomClass.AccruedInterestCalculationType
        params(17) = New SqlParameter("@CreatedBy", SqlDbType.VarChar, 50)
        params(17).Value = ocustomClass.CreatedBy
        params(18) = New SqlParameter("@CreatedDate", SqlDbType.DateTime)
        params(18).Value = ocustomClass.CreatedDate
        params(19) = New SqlParameter("@ChangedBy", SqlDbType.VarChar, 20)
        params(19).Value = ocustomClass.ChangedBy
        params(20) = New SqlParameter("@ChangedDate", SqlDbType.DateTime)
        params(20).Value = ocustomClass.ChangedDate

        params(21) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(21).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Products.Product.JFProductSaveEdit")
        End Try
    End Sub
    Public Function JFProductDelete(ByVal ocustomClass As Parameter.JFProduct) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ProductCode", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ProductCode
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            'Throw New Exception("Error On DataAccess.Setting.Referal.ReferalDelete")
        End Try
    End Function




End Class
