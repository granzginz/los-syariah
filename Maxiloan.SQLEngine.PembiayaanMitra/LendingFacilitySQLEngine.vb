﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Parameter
#End Region

Public Class LendingFacilitySQLEngine : Inherits DataAccessBase

    Private Const spLendingFacilityDML As String = "spLendingFacilityDML"

#Region "lendingFacilityDML"
    Public Sub lendingFacilityDML(ByVal dml As String, ByVal customclass As LendingFacility)

	
        Dim params(15) As SqlParameter
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            With customclass
                params(0) = New SqlParameter("@DML", dml)
                params(1) = New SqlParameter("@MFCode", .MFCode)
                params(2) = New SqlParameter("@MFFacilityNo", .MFFacilityNo)
                params(3) = New SqlParameter("@MFFacilityName", .MFFacilityName)
                params(4) = New SqlParameter("@JenisFacility", .JenisFacility)
                params(5) = New SqlParameter("@Plafond", .Plafond)
                params(6) = New SqlParameter("@OSPlafond", .OSPlafond)
                params(7) = New SqlParameter("@SifatFacility", .SifatFacility)
                params(8) = New SqlParameter("@LenderPortion", .LenderPortion)
                params(9) = New SqlParameter("@AssetCondition", .AssetCondition)
                params(10) = New SqlParameter("@DrawdownStartDate", .DrawdownStartDate)
                params(11) = New SqlParameter("@DrawdownEndDate", .DrawdownEndDate)
                params(12) = New SqlParameter("@LateCharges", .LateCharges)
                params(13) = New SqlParameter("@Penalty", .Penalty)
                params(14) = New SqlParameter("@FacilityDate", .FacilityDate)
                params(15) = New SqlParameter("@MinDrawDownAmount", .MinDrawDownAmount)
            End With

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spLendingFacilityDML, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region
End Class
