﻿<Serializable()> _
Public Class ValueTextObject
    Implements IComparable
    Private _value As String
    Private _text As String
    Private _extText As String
    Public Sub New()
    End Sub

    Public Sub New(value As String, text As String)
        _value = value
        _text = text
    End Sub

    Public Property Value() As String
        Get
            Return _value
        End Get
        Set(value As String)
            _value = value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return _text
        End Get
        Set(value As String)
            _text = value
        End Set
    End Property


    Property ExtText As String
        Get
            Return _extText
        End Get
        Set(ByVal value As String)
            _extText = Value
        End Set
    End Property

    Public Function CompareTo(obj As Object) As Integer Implements IComparable.CompareTo
        Return [String].CompareOrdinal(Text, TryCast(obj, ValueTextObject).Text)
    End Function

    Public Overrides Function ToString() As String
        Return String.Format("{0} {1}", _value, _text)
    End Function

End Class



'<Serializable()>
'Public Class ValueTextObject
'    Implements IComparable
'    Private _value As String
'    Private _text As String
'    Private _extText As String
'    Public Sub New()

'    End Sub

'    Public Sub New(value As String, text As String)
'        _value = value
'        _text = text
'    End Sub
'    Property Value As String
'        Get
'            Return _value
'        End Get
'        Set(ByVal value As String)
'            _value = value
'        End Set
'    End Property

'    Property Text As String
'        Get
'            Return _text
'        End Get
'        Set(ByVal value As String)
'            _text = Value
'        End Set
'    End Property


'    Property ExtText As String
'        Get
'            Return _extText
'        End Get
'        Set(ByVal value As String)
'            _extText = Value
'        End Set
'    End Property

'    Public Overloads Function CompareTo(ByVal obj As Object) As Integer _
'          Implements IComparable.CompareTo
'        Dim _obj = CType(obj, ValueTextObject)
'        Return System.String.Compare(Text, _obj.Text)
'    End Function

'    Public Overrides Function ToString() As String
'        Return Text
'    End Function
'End Class

'Public Class GlJournalPostAvailable
'    Inherits ValueTextObject
'    Public Sub New(value As DateTime, text As String, balance As Decimal)
'        MyBase.New(value.ToString(), text)
'        _value = balance
'        _pDate = value
'    End Sub
'    Private _value As Decimal
'    Private _pDate As DateTime
'    Public ReadOnly Property PostDate As DateTime
'        Get
'            Return _pDate
'        End Get
'    End Property
'    Public ReadOnly Property Balance As Decimal
'        Get
'            Return _value
'        End Get
'    End Property
'    Public ReadOnly Property IsBalance As Boolean
'        Get
'            Return _value = 0
'        End Get
'    End Property
'End Class