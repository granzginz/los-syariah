﻿<Serializable()> _
Public MustInherit Class BrokenRules
    Private ReadOnly m_brokenRules As New List(Of BusinessRule)()
    Protected MustOverride Sub validate()

    Protected Sub addBrokenRule(businessRule As BusinessRule)
        m_brokenRules.Add(businessRule)
    End Sub

    Public Overridable Function GetBrokenRules() As IEnumerable(Of BusinessRule)
        m_brokenRules.Clear()
        validate()
        Return m_brokenRules
    End Function
End Class
