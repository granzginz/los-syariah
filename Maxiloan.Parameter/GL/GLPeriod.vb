﻿<Serializable()>
Public Class GlPeriod : Inherits Common
    Private Const MonthFormat As String = "MMMM"
    Private Const YearFormat As String = "yyyy"
    Public Shared ReadOnly NullDate As New DateTime(&H76C, 1, 1)

    Private m_changeActiveMonthOn As DateTime
    Private m_closedOn As DateTime
    Private m_dateRange As DateRange
    Private m_everChangedActiveMonth As Boolean
    Private m_everClosed As Boolean

    Private m_status As PeriodStatus
    Private m_year As Integer

    Private m_name As String
    Private _companyId As String
    Private _branchId As String

    Public Property PeriodId As String
    Public Property PeriodName As String
    Public Property PeriodSts As String
    Public Property PeriodStart As DateTime
    Public Property PeriodEnd As DateTime
    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64

    Public Sub New()
        Me.New("0", Nothing, DateRange.Past, DateRange.Future, PeriodStatus.Open)
    End Sub

    Public Sub New(id__1 As String, year As Integer, start As DateTime, [end] As DateTime, status As PeriodStatus, companyId As String, _
     branchid As String)

        Id = id__1

        SetDateRangeYear(start, [end], year)
        m_status = status
        m_closedOn = NullDate
        m_everClosed = False
        m_changeActiveMonthOn = NullDate
        m_everChangedActiveMonth = False
        m_year = year
        _companyId = companyId
        _branchId = branchid
    End Sub

    Public Sub New(id__1 As String, year As GlPeriodYear, start As DateTime, [end] As DateTime, status As PeriodStatus)
        Id = id__1

        SetDateRangeYear(start, [end], year.YearId)
        m_status = status
        m_closedOn = NullDate
        m_everClosed = False
        m_changeActiveMonthOn = NullDate
        m_everChangedActiveMonth = False
        m_year = year.YearId
        _companyId = year.CompanyId
        _branchId = year.BranchId
    End Sub
    Public Property Id() As String
        Get
            Return m_Id
        End Get
        Private Set(value As String)
            m_Id = Value
        End Set
    End Property
    Private m_Id As String
    Public Property Name() As String
        Get
            Return m_name
        End Get
        Set(value As String)
            m_name = value
        End Set
    End Property
    Public ReadOnly Property CompanyId() As String
        Get
            Return _companyId
        End Get
    End Property
    Public ReadOnly Property BranchId() As String
        Get
            Return _branchId
        End Get
    End Property
    Public Function Includes([date] As DateTime) As Boolean
        Return m_dateRange.Includes([date])
    End Function

    Public Function InterSection(dateRange As IDateRange) As IDateRange
        Return m_dateRange.InterSection(dateRange)
    End Function

    Private Function _isClosed() As Boolean
        Return m_status.Equals(PeriodStatus.Close)
    End Function

    Private Function isCurrent() As Boolean
        Return m_status.Equals(PeriodStatus.Current)
    End Function

    Public Function IsInRange([date] As DateTime) As Boolean
        Return m_dateRange.IsInRange([date])
    End Function

    Public Function Overlaps(dateRange As IDateRange) As Boolean
        Return m_dateRange.Overlaps(dateRange)
    End Function

    Public Sub SetDateRange(start As DateTime, [end] As DateTime)
        Dim time As New DateTime(start.Year, start.Month, start.Day, 0, 0, 0)
        Dim time2 As New DateTime([end].Year, [end].Month, [end].Day, &H17, &H3B, &H3B)
        m_dateRange = New DateRange(time, time2)
    End Sub

    Public Sub SetDateRangeYear(start As DateTime, [end] As DateTime, year As Integer)
        Dim time As New DateTime(start.Year, start.Month, start.Day, 0, 0, 0)
        Dim time2 As New DateTime([end].Year, [end].Month, [end].Day, &H17, &H3B, &H3B)
        m_dateRange = New DateRange(time, time2)
        m_year = year
        setName()
    End Sub



    Private Sub setName()
        m_name = Start.[Date].ToString("MMMM") & " " & Start.[Date].ToString("yyyy")
    End Sub

    Public Overrides Function ToString() As String
        Return Name
    End Function

    Public Property ChangeActiveMonthOn() As DateTime
        Get
            Return New DateTime(m_changeActiveMonthOn.Year, m_changeActiveMonthOn.Month, m_changeActiveMonthOn.Day, m_changeActiveMonthOn.Hour, m_changeActiveMonthOn.Minute, m_changeActiveMonthOn.Second)
        End Get
        Set(value As DateTime)
            m_changeActiveMonthOn = value
        End Set
    End Property

    Public Property ClosedOn() As DateTime
        Get
            Return New DateTime(m_closedOn.Year, m_closedOn.Month, m_closedOn.Day, m_closedOn.Hour, m_closedOn.Minute, m_closedOn.Second)
        End Get
        Set(value As DateTime)
            m_closedOn = value
        End Set
    End Property

    Public ReadOnly Property [End]() As DateTime
        Get
            Return m_dateRange.EndDate
        End Get
    End Property

    Public Property EverChangeActiveMonth() As Boolean
        Get
            Return m_everChangedActiveMonth
        End Get
        Set(value As Boolean)
            m_everChangedActiveMonth = value
        End Set
    End Property

    Public Property EverClosed() As Boolean
        Get
            Return m_everClosed
        End Get
        Set(value As Boolean)
            m_everClosed = value
        End Set
    End Property


    Public ReadOnly Property IsClosed() As Boolean
        Get
            Return _isClosed()
        End Get
    End Property

    Protected Property DateRange() As DateRange
        Get
            Return m_dateRange
        End Get
        Set(value As DateRange)
            m_dateRange = value
        End Set
    End Property

    Public ReadOnly Property Start() As DateTime
        Get
            Return m_dateRange.StartDate
        End Get
    End Property

    Public Property Status() As PeriodStatus
        Get
            Return m_status
        End Get
        Set(value As PeriodStatus)
            m_status = value
        End Set
    End Property

    Public Property Year() As Integer
        Get
            Return m_year
        End Get
        Set(value As Integer)
            m_year = value
        End Set
    End Property

End Class
