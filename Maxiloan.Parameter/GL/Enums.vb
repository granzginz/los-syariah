﻿

Public Enum EnumMasterAccType
    Asset = 1
    Liabilities = 2
    Equity = 3
    Revenue = 4
    Bank_Loan = 5
    Expenses = 6
    Biaya_Lainnya = 7
    Pendapatan_Lainnya = 8
End Enum
 

Public Enum GlFunctions 
    Receivables
    Payables
    Fixed_Assets
    Accumulated_Depreciation
    Work_in_Process
    Cash_Bank 
End Enum
Public Enum PeriodStatus 
    Open
    Current
    Close 
End Enum


Public Enum GLBalanceSheetAccountType
    Debit = 1
    Credit = 2
End Enum


 
