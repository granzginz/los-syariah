﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Data
Imports System.Data.SqlClient

<Serializable()> _
Public Class JournalApproval
    Inherits BrokenRules

    Private ReadOnly m_items As IList(Of JournalApprovalItem) = New List(Of JournalApprovalItem)()

    Public Property CompanyId() As String
        Get
            Return m_CompanyId
        End Get
        Set(value As String)
            m_CompanyId = value
        End Set
    End Property
    Private m_CompanyId As String
    Public Property BranchId() As String
        Get
            Return m_BranchId
        End Get
        Set(value As String)
            m_BranchId = value
        End Set
    End Property
    Private m_BranchId As String
    Public Property TransactionNo() As String
        Get
            Return tr_nomor
        End Get
        Set(value As String)
            tr_nomor = value
        End Set
    End Property
    Public Property TransactionDate() As DateTime
        Get
            Return m_transactionDate
        End Get
        Set(value As DateTime)
            m_transactionDate = value
        End Set
    End Property
    Public Property ValueDate() As DateTime
        Get
            Return m_valueDate
        End Get
        Set(value As DateTime)
            m_valueDate = value
        End Set
    End Property
    Public Property TransactionType() As String
        Get
            Return m_transactionType
        End Get
        Set(value As String)
            m_transactionType = value
        End Set
    End Property


    Public Property Description() As String
        Get
            Return m_description
        End Get
        Set(value As String)
            m_description = value
        End Set
    End Property

    Private m_transactionType As String
    Private tr_nomor As String
    Private m_description As String
    Private m_transactionDate As DateTime
    Private m_valueDate As DateTime

    Public Sub New()
    End Sub
    Public Sub New(trNo As String, trDate As DateTime, transactionType As String, description As String, valuedate As DateTime)
        tr_nomor = trNo
        Me.m_description = description
        m_transactionDate = trDate
        Me.m_transactionType = transactionType
        m_valueDate = valuedate
    End Sub

    Public ReadOnly Property Items() As IList(Of JournalApprovalItem)
        Get
            Return m_items
        End Get
    End Property

    Public Sub AddItem(item As JournalApprovalItem)
        m_items.Add(item)
    End Sub

    Public ReadOnly Property TotalDebit() As Decimal
        Get
            Return m_items.Sum(Function(e) e.DebitAmount)
        End Get
    End Property

    Public ReadOnly Property TotalCredit() As Decimal
        Get
            Return m_items.Sum(Function(e) e.CreditAmount)
        End Get
    End Property

    Public ReadOnly Property IsBalance() As Boolean
        Get
            Return TotalDebit - TotalCredit = 0
        End Get
    End Property

    Public ReadOnly Property CanApprove() As Boolean
        Get
            Return IsBalance And m_items.All(Function(item) item.CoaName <> String.Empty AndAlso item.CoaName.ToUpper() <> "N/A")
        End Get
    End Property

    Private ReadOnly imbalances As IList(Of ValueTextObject) = New List(Of ValueTextObject)()

    Public ReadOnly Property ImbalanceTrNo() As IList(Of ValueTextObject)
        Get
            Return imbalances
        End Get
    End Property

    Protected Overrides Sub validate()
        If Not IsBalance Then
            addBrokenRule(New BusinessRule(String.Format("- Journal Transaksi tidak Balance.")))
        End If

        Dim na = m_items.All(Function(item) item.CoaName <> String.Empty AndAlso item.CoaName.ToUpper() <> "N/A")

        If Not na Then
            addBrokenRule(New BusinessRule(String.Format("- Terdapat Master COA belum terdaftar.")))
        End If
    End Sub

    Public Sub ReadRecord(record As IDataRecord)

        '0            1          2           3    4      5            6         7     8          9
        'tr_nomor	tr_Date	tr_TypeDesc	tr_desc	CoAId	Description	Debit	Credit	AddFree ValueDate
        'New(coaid__1 As String, coaDesc As String, debit As Decimal, credit As Decimal, trd As String, addf As String, valueDate as DateTime)
        m_items.Add(New JournalApprovalItem(record.GetString(4), record.GetString(5), record.GetDecimal(6), record.GetDecimal(7), record.GetString(3), "", record.GetDateTime(10)))
    End Sub


    Public Shared Function Builder(reader As SqlDataReader) As IList(Of JournalApproval)
        Dim result As IList(Of JournalApproval) = New List(Of JournalApproval)()

        For Each item As IDataRecord In reader.Cast(Of IDataRecord)().ToList()
            Dim r = result.SingleOrDefault(Function(p) p.TransactionNo = item.GetString(0))
            If r Is Nothing Then
                Dim japr = New JournalApproval(item.GetString(0), item.GetDateTime(1), item.GetString(2), item.GetString(3), item.GetDateTime(10))
                japr.ReadRecord(item)
                result.Add(japr)
            Else
                r.ReadRecord(item)
            End If
        Next
        Return result

    End Function
    Public Property TrNomor As String
    Public Property TrTypeDesc As String
    Public Property User As String
End Class
