﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text.RegularExpressions
Imports System.Text
 

<Serializable()> _
Public Class GlJournalVoucher
    Private _journalVoucherItem As IList(Of JournalVoucherItem)

    Public Sub New()
        _journalVoucherItem = New List(Of JournalVoucherItem)()
    End Sub
    Public Property CompanyId() As String
        Get
            Return m_CompanyId
        End Get
        Set(value As String)
            m_CompanyId = value
        End Set
    End Property
    Private m_CompanyId As String
    Public Property BranchId() As String
        Get
            Return m_BranchId
        End Get
        Set(value As String)
            m_BranchId = value
        End Set
    End Property
    Private m_BranchId As String
    Public Property TransactionDate() As DateTime
        Get
            Return m_TransactionDate
        End Get
        Set(value As DateTime)
            m_TransactionDate = value
        End Set
    End Property
    Private m_TransactionDate As DateTime
    Public Property TransactionNomor() As String
        Get
            Return m_TransactionNomor
        End Get
        Set(value As String)
            m_TransactionNomor = value
        End Set
    End Property
    Private m_TransactionNomor As String
    Public Property ReffNomor() As String
        Get
            Return m_ReffNomor
        End Get
        Set(value As String)
            m_ReffNomor = value
        End Set
    End Property
    Private m_ReffNomor As String
    Public Property ReffDate() As DateTime
        Get
            Return m_ReffDate
        End Get
        Set(value As DateTime)
            m_ReffDate = value
        End Set
    End Property
    Private m_ReffDate As DateTime

    Public ReadOnly Property PeroidYear() As String
        Get
            Return TransactionDate.Year.ToString()
        End Get
    End Property

    Public ReadOnly Property PeriodMonth() As String
        Get
            Return TransactionDate.Month.ToString()
        End Get
    End Property

    Public Property TransactionId() As String
        Get
            Return m_TransactionId
        End Get
        Set(value As String)
            m_TransactionId = value
        End Set
    End Property
    Private m_TransactionId As String
    Public Property TransactionDesc() As String
        Get
            Return m_TransactionDesc
        End Get
        Set(value As String)
            m_TransactionDesc = value
        End Set
    End Property
    Private m_TransactionDesc As String

    Public ReadOnly Property JournalAmount() As Decimal
        Get
            Return getAmount("D")
        End Get
    End Property

    Public ReadOnly Property JournalItems() As IList(Of JournalVoucherItem)
        Get
            Return _journalVoucherItem
        End Get
    End Property

    Public ReadOnly Property IsBalance() As Boolean
        Get
            Return getAmount("D") = getAmount("C")
        End Get
    End Property

    Public ReadOnly Property IsItemValid() As Boolean
        Get
            Dim result As Boolean = True
            For Each item In From item1 In _journalVoucherItem Where (item1.Message.Trim <> "")
                result = False
            Next
            Return result
        End Get
    End Property

    Public ReadOnly Property IsItemCoaValid() As Boolean
        Get
            Return _journalVoucherItem.All(Function(m) Not m.CoaId.Trim().Equals(String.Empty))
        End Get
    End Property

    Private Function getAmount(post As String) As Decimal
        Return _journalVoucherItem.Where(Function(w) w.Post = post).Sum(Function(e) e.Amount)
    End Function

    Public Sub AddItem(item As JournalVoucherItem)
        _journalVoucherItem.Add(item)
    End Sub

    Public Shared Function ParseCOACsv(list As IList(Of ValueTextObject)) As String
        Dim csv As New StringBuilder()
        csv.AppendLine("""COAID"",""COAName""")
        For Each faculty In list
            csv.Append(String.Format("""{0}"",""{1}""", faculty.Value, faculty.Text))
            csv.AppendLine()
        Next

        Return csv.ToString()
    End Function
    Public Shared Function ParseJournalCsv(reader As System.IO.StreamReader, compId As String, branch As String) As IList(Of GlJournalVoucher)

        '    '0          1         2        3        4       5       6           7         8            9     10     11   12 
        '    'Branchid,Tr_Nomor,SequenceNo,Tr_Date,Reff_Date,CoaId,AddFree,TransactionID,Header_Desc,Tr_Desc,Post,Amount,PaymentAllocationId
        Dim trNo = ""

        Dim result = New List(Of GlJournalVoucher)()
        Dim jv = New GlJournalVoucher()
        While Not reader.EndOfStream
            Dim originalLine = reader.ReadLine()
            '        Dim OriSplit() As String = Split(originalLine, ",", """", False)
            Dim oriSplit = split(originalLine, ",", """", False)
            If oriSplit(0).Replace("""", "").ToUpper().Contains("BRANCHID") Then
                Continue While
            End If

            Dim seq As Long = 1
            If oriSplit(1).Replace("""", "") <> "" Then

                If seq = Int16.Parse(oriSplit(2).Replace("""", "")) Then
                    trNo = oriSplit(1).Replace("""", "")
                    jv = New GlJournalVoucher() With {
                    .TransactionNomor = oriSplit(1).Replace("""", ""),
                    .CompanyId = compId,
                    .BranchId = oriSplit(0).Replace("""", ""),
                    .TransactionDate = stringToDate(oriSplit(3).Replace("""", "")),
                    .ReffDate = stringToDate(oriSplit(3).Replace("""", "")),
                    .TransactionId = oriSplit(6).Replace("""", ""),
                    .TransactionDesc = oriSplit(7).Replace("""", "")
                }

                    result.Add(jv)
                End If


                If oriSplit(1).Replace("""", "") <> "" Then


                    Dim jItem = New JournalVoucherItem() With {
                    .CompanyId = "001",
                    .BranchId = oriSplit(0).Replace("""", ""),
                    .Tr_Nomor = oriSplit(1).Replace("""", ""),
                    .SequenceNo = Int16.Parse(oriSplit(2).Replace("""", "")),
                    .CoaCo = compId,
                    .CoaBranch = oriSplit(0).Replace("""", ""),
                    .CoaId = oriSplit(5).Replace("""", ""),
                    .TransactionId = oriSplit(6).Replace("""", ""),
                    .TransactionDesc = oriSplit(8).Replace("""", ""),
                    .Post = oriSplit(9).Replace("""", ""),
                    .Amount = Decimal.Parse(oriSplit(10).Replace("""", "")),
                    .CoaIdX = "-",
                    .PaymentAllocationId = oriSplit(11).Replace("""", "")
                        }


                    jv.AddItem(jItem)
                End If
            End If


        End While
        Return result

    End Function

    Private Shared Function stringToDate(strDate As String) As DateTime
        Dim dt = strDate.Split("/"c)
        Return New DateTime(Int16.Parse(dt(2)), Int16.Parse(dt(1)), Int16.Parse(dt(0)))
    End Function

    Private Shared Function split(expression As String, delimiter As String, qualifier As String, ignoreCase As Boolean) As String()
        Dim statement = [String].Format("{0}(?=(?: [^{1}]*{1}[^{1}]*{1})*(?![^{1}]*{1}))", Regex.Escape(delimiter), Regex.Escape(qualifier))
        Dim options = RegexOptions.Compiled Or RegexOptions.Multiline
        If ignoreCase Then
            options = options Or RegexOptions.IgnoreCase
        End If
        Return New Regex(statement, options).Split(expression)
    End Function
    Public Property BusinessDate() As DateTime
        Get
            Return m_BusinessDate
        End Get
        Set(value As DateTime)
            m_BusinessDate = value
        End Set
    End Property
    Private m_BusinessDate As DateTime
    Public Property WhereCond() As String
    Private _businessDate As DateTime
    Public Sub ValidateGlPeriod(periods As IList(Of GlPeriod))
        Dim prd = periods.SingleOrDefault(Function(r) r.IsInRange(TransactionDate))
        If prd Is Nothing Then
            Throw New Exception("Journal Periode tidak ditemukan")
        End If

        If prd.Status = PeriodStatus.Close Then
            Throw New Exception("Journal Periode telah di close")
        End If

        'if (prd.Status != PeriodStatus.Current )
        '    throw new Exception("Tanggal transaksi bukan periode Journal."); 
    End Sub
    Public Property ReffDesc() As String
        Get
            Return m_ReffDesc
        End Get
        Set(value As String)
            m_ReffDesc = value
        End Set
    End Property
    Private m_ReffDesc As String
End Class
