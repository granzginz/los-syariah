﻿<Serializable()> _
Public Class GlPeriodYear
    Private m_dateRange As DateRange
    Private m_periods As IList(Of GlPeriod)
    Private Const SEQ_FORMAT As String = "00"

    Private id As Integer
    Private code As String
    Private name As String

    Private _company As String
    Public Sub New()
        Me.New(0, "", "")
    End Sub

    Public Sub New(yearid As Integer, code As String, name As String)
        Me.New(yearid, code, name, DateRange.Past, DateRange.Future, "001", _
         "")
    End Sub

    Public Sub New(yearid As Integer, code As String, name As String, start As DateTime, [end] As DateTime, company As String, _
     branchId__1 As String)
        id = yearid
        Me.code = code
        Me.name = name
        m_periods = New List(Of GlPeriod)()
        SetDateRange(start, [end])
        _company = company
        BranchId = branchId__1
    End Sub


    Public ReadOnly Property YearId() As Integer
        Get
            Return id
        End Get
    End Property
    Public ReadOnly Property YearCode() As String
        Get
            Return code
        End Get
    End Property
    Public Property YearName() As String
        Get
            Return name
        End Get
        Set(value As String)
            name = value
        End Set
    End Property
    Public ReadOnly Property CompanyId() As String
        Get
            Return _company
        End Get
    End Property
    Public Property BranchId() As String
        Get
            Return m_BranchId
        End Get
        Private Set(value As String)
            m_BranchId = Value
        End Set
    End Property
    Private m_BranchId As String
    Public Sub SetDateRange(start As DateTime, [end] As DateTime)
        Dim time = New DateTime(start.Year, start.Month, start.Day, 0, 0, 0)
        Dim time2 = New DateTime([end].Year, [end].Month, [end].Day, &H17, &H3B, &H3B)
        m_dateRange = New DateRange(time, time2)
    End Sub
    Public Overridable Sub GeneratePeriods()
        m_periods.Clear()
        Dim start__1 = StartDate
        Dim i As Short = 1
        While i <= 12
            Dim period = New GlPeriod(String.Format("{0}{1}-{2}-{3}", _company, BranchId, id, i), Me, start__1, getEndPeriod(start__1), PeriodStatus.Open)
            m_periods.Add(period)
            start__1 = period.Start.AddMonths(1)
            i = CShort(i + 1)
        End While
    End Sub
    Private Function getEndPeriod(startPeriod As DateTime) As DateTime
        Return startPeriod.AddMonths(1).AddDays(-1.0)
    End Function
    Public Overridable ReadOnly Property StartDate As DateTime
        Get
            Return m_dateRange.StartDate
        End Get
    End Property
    Public Overridable ReadOnly Property EndDate As DateTime
        Get
            Return m_dateRange.EndDate
        End Get
    End Property
    Public Function GetPeriods() As IList(Of GlPeriod)
        Return m_periods
    End Function



End Class
