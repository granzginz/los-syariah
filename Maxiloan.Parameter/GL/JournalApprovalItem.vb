﻿<Serializable()> _
Public Class JournalApprovalItem
    Public Sub New()
    End Sub

    Public Sub New(coaid__1 As String, coaDesc As String, debit As Decimal, credit As Decimal, trd As String, addf As String, valDate As Date)
        CoaId = coaid__1
        CoaName = coaDesc
        DebitAmount = debit
        CreditAmount = credit
        TrDesc = trd
        AddFree = addf
        ValueDate = valDate
    End Sub
    Private m_valudate As Date
    Public Property ValueDate As Date
        Get
            Return m_valudate
        End Get
        Set(value As Date)
            m_valudate = value
        End Set
    End Property

    Public Property CoaId() As String
        Get
            Return m_CoaId
        End Get
        Private Set(value As String)
            m_CoaId = Value
        End Set
    End Property
    Private m_CoaId As String
    Public Property CoaName() As String
        Get
            Return m_CoaName
        End Get
        Private Set(value As String)
            m_CoaName = Value
        End Set
    End Property
    Private m_CoaName As String
    Public Property AddFree() As String
        Get
            Return m_AddFree
        End Get
        Private Set(value As String)
            m_AddFree = value
        End Set
    End Property
    Private m_AddFree As String
    Public Property DebitAmount() As Decimal
        Get
            Return m_DebitAmount
        End Get
        Private Set(value As Decimal)
            m_DebitAmount = Value
        End Set
    End Property
    Private m_DebitAmount As Decimal
    Public Property CreditAmount() As Decimal
        Get
            Return m_CreditAmount
        End Get
        Private Set(value As Decimal)
            m_CreditAmount = Value
        End Set
    End Property
    Private m_CreditAmount As Decimal
    Public Property TrDesc() As String
        Get
            Return m_TrDesc
        End Get
        Private Set(value As String)
            m_TrDesc = Value
        End Set
    End Property
    Private m_TrDesc As String

End Class
