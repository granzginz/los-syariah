<Serializable()> _
Public Class BalanceSheetGroupDetail
    Private _reportGroupId As Integer
    Private _groupId As Integer
    Private _accountType As GLBalanceSheetAccountType
    Private _groupDescription As String

    Public Sub New()
    End Sub

    Public Sub New(reportGroupId As Int32, groupid As Int32, accountTp As GLBalanceSheetAccountType, groupDesc As String)
        _reportGroupId = reportGroupId
        _groupId = groupid
        _accountType = accountTp
        _groupDescription = groupDesc
    End Sub

    Public Property ReportGroupId() As Integer
        Get
            Return _reportGroupId
        End Get
        Set(value As Integer)
            _reportGroupId = value
        End Set
    End Property

    Public Property GroupID() As Integer
        Get
            Return _groupId
        End Get
        Set(value As Integer)
            _groupId = value
        End Set
    End Property

    Public Property GroupDescription() As String
        Get
            Return _groupDescription
        End Get
        Set(value As String)
            _groupDescription = value
        End Set
    End Property

    Public Property accType() As GLBalanceSheetAccountType
        Get
            Return _accountType
        End Get
        Set(value As GLBalanceSheetAccountType)
            _accountType = value
        End Set
    End Property

End Class
