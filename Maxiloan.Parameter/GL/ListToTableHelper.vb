﻿Public Module ListToTableHelper
     
    <System.Runtime.CompilerServices.Extension()> _
    Public Function ToDataTable(data As IList(Of InitialBudget)) As DataTable
        Dim dt = New DataTable()
        For Each s In New String() {"companyId", "branchid", "Year", "coaid", "JAN", "FEB", "MAR", "APR", "MEI", "JUN", "JUL", "AGT", "SEP", "OKT", "NOV", "DES"}
            dt.Columns.Add(New DataColumn(s))
        Next

        For Each item In data
            Dim row = dt.NewRow()
            row("companyId") = item.CompanyId
            row("branchid") = item.BranchId
            row("Year") = item.Year
            row("coaid") = item.CoAId
            row("JAN") = item.Jan
            row("FEB") = item.Feb
            row("MAR") = item.Mar
            row("APR") = item.Apr
            row("MEI") = item.Mei
            row("JUN") = item.Jun
            row("JUL") = item.Jul
            row("AGT") = item.Agt
            row("SEP") = item.Sep
            row("OKT") = item.Okt
            row("NOV") = item.Nov
            row("DES") = item.Des
            dt.Rows.Add(row)
        Next
        Return dt
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function ToDataTable(data As IList(Of String)) As DataTable
        Dim dt = New DataTable()

        Dim strArray As String() = New String() {"id", "value1", "value2"}
        For Each s In strArray
            dt.Columns.Add(New DataColumn(s))
        Next
 
        For Each v In data
            Dim row = dt.NewRow()
            row("id") = v.Trim()
            row("value1") = ""
            row("value2") = ""
            dt.Rows.Add(row)
        Next
        Return dt
    End Function



    <System.Runtime.CompilerServices.Extension()> _
    Public Function ToDataTable(data As IList(Of GlJournalVoucher)) As DataTable
        Dim dt = New DataTable()
        For Each s In New String() {"CompanyID", "BranchID", "Tr_Nomor", "PeriodYear", "PeriodMonth", "TransactionID",
   "Tr_Date", "Reff_Date", "Tr_DescH", "JournalAmount", "BatchID", "Status_Tr",
   "IsActive", "Flag", "SequenceNo", "CoaCo", "CoaId", "Tr_Desc",
   "Post", "Amount", "PaymentAllocationId", "Jenis_Reff"} ', "AddFree"}
            dt.Columns.Add(New DataColumn(s))
        Next

        Dim i = 1
        For Each v In data
            For Each item In v.JournalItems
                Dim row = dt.NewRow()
                row("CompanyID") = v.CompanyId.Trim()
                row("BranchID") = v.BranchId.Trim()
                row("Tr_Nomor") = v.TransactionNomor.Trim()
                row("PeriodYear") = v.PeroidYear.Trim()
                row("PeriodMonth") = v.PeriodMonth.Trim()
                row("TransactionID") = v.TransactionId.Trim()
                row("Tr_Date") = v.TransactionDate
                row("Reff_Date") = v.ReffDate
                row("Tr_DescH") = v.TransactionDesc.Trim()
                row("JournalAmount") = v.JournalAmount
                row("BatchID") = "-"
                row("Status_Tr") = "OP"
                row("IsActive") = "1"
                row("Flag") = "O"
                row("SequenceNo") = System.Math.Max(System.Threading.Interlocked.Increment(i), i - 1)
                row("CoaCo") = v.CompanyId.Trim()
                row("CoaId") = item.CoaId.Trim()
                row("Tr_Desc") = item.TransactionDesc.Trim()
                row("Post") = item.Post
                row("Amount") = item.Amount
                row("PaymentAllocationId") = If((String.IsNullOrEmpty(item.PaymentAllocationId)), [String].Empty, item.PaymentAllocationId.Trim())
                row("Jenis_Reff") = v.ReffDesc
                'row("AddFree") = item.AddFree
                dt.Rows.Add(row)
            Next
        Next

        Return dt
    End Function



    <System.Runtime.CompilerServices.Extension()> _
    Public Function ToDataTable(data As IList(Of GlPeriod)) As DataTable
        Dim dt = New DataTable()


        dt.Columns.Add("CompanyId", GetType(String))
        dt.Columns.Add("BranchID", GetType(String))
        dt.Columns.Add("PeriodId", GetType(String))
        dt.Columns.Add("PeriodName", GetType(String))
        dt.Columns.Add("PeriodStart", GetType(DateTime))
        dt.Columns.Add("PeriodEnd", GetType(DateTime))
        dt.Columns.Add("Year", GetType(Integer))
        dt.Columns.Add("PeriodStatus", GetType(Boolean))
        dt.Columns.Add("PeriodClosedOn", GetType(DateTime))
        dt.Columns.Add("PeriodEverClosed", GetType(Boolean))
        dt.Columns.Add("PeriodChangeActiveMonthOn", GetType(DateTime))
        dt.Columns.Add("PeriodEverChangeActiveMonth", GetType(Boolean))


        For Each v In data
            Dim row = dt.NewRow()
            row("CompanyId") = v.CompanyId
            row("BranchID") = v.BranchId
            row("PeriodId") = v.Id
            row("PeriodName") = v.Name
            row("PeriodStart") = v.Start
            row("PeriodEnd") = v.[End]
            row("Year") = v.Year
            row("PeriodStatus") = v.Status
            row("PeriodClosedOn") = v.ClosedOn
            row("PeriodEverClosed") = v.EverChangeActiveMonth
            row("PeriodChangeActiveMonthOn") = v.ChangeActiveMonthOn
            row("PeriodEverChangeActiveMonth") = v.EverChangeActiveMonth

            dt.Rows.Add(row)
        Next

        Return dt
    End Function
End Module
