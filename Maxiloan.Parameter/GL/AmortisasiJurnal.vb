﻿Imports Maxiloan.Parameter

<Serializable()>
Public Class AmortisasiJurnal : Inherits Common

    Private _ListData As DataTable
    Private _TotalRecords As Int64
    Private _SeqNo As Int64
    Private _ID As Int64
    Private _Nama As String
    Private _CoaDebit As String
    Private _CoaCredit As String
    Private _AmountRec As Double
    Private _Nilai As Double
    Private _TglMulai As Date
    Private _TglAkhir As Date
    Private _TglEfektif As Date
    Private _NoReference As String
    Private _JangkaWaktu As Int64
    Private _AbNo As String

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property
    Public Property TotalRecords As Long
        Get
            Return _TotalRecords
        End Get
        Set(value As Long)
            _TotalRecords = value
        End Set
    End Property
    Public Property SeqNo As Long
        Get
            Return _SeqNo
        End Get
        Set(value As Long)
            _SeqNo = value
        End Set
    End Property
    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property
    Public Property Nama As String
        Get
            Return _Nama
        End Get
        Set(value As String)
            _Nama = value
        End Set
    End Property
    Public Property CoaDebit As String
        Get
            Return _CoaDebit
        End Get
        Set(value As String)
            _CoaDebit = value
        End Set
    End Property
    Public Property CoaCredit As String
        Get
            Return _CoaCredit
        End Get
        Set(value As String)
            _CoaCredit = value
        End Set
    End Property
    Public Property AmountRec As Double
        Get
            Return _AmountRec
        End Get
        Set(value As Double)
            _AmountRec = value
        End Set
    End Property
    Public Property Nilai As Double
        Get
            Return _Nilai
        End Get
        Set(value As Double)
            _Nilai = value
        End Set
    End Property
    Public Property TglMulai As Date
        Get
            Return _TglMulai
        End Get
        Set(value As Date)
            _TglMulai = value
        End Set
    End Property
    Public Property TglAkhir As Date
        Get
            Return _TglAkhir
        End Get
        Set(value As Date)
            _TglAkhir = value
        End Set
    End Property
    Public Property TglEfektif As Date
        Get
            Return _TglEfektif
        End Get
        Set(value As Date)
            _TglEfektif = value
        End Set
    End Property
    Public Property NoReference As String
        Get
            Return _NoReference
        End Get
        Set(value As String)
            _NoReference = value
        End Set
    End Property
    Public Property JangkaWaktu As String
        Get
            Return _JangkaWaktu
        End Get
        Set(value As String)
            _JangkaWaktu = value
        End Set
    End Property
    Public Property AbNo As String
        Get
            Return _AbNo
        End Get
        Set(value As String)
            _AbNo = value
        End Set
    End Property

    Public Property Approval As Approval
    Public lblNo As String
    Public lblDate As Date
    Public lblAmount As Double
    Public lblDEBET As String
    Public lblDEBETDESC As String
    Public lblCREDIT As String
    Public lblCREDITDESC As String
    Public StartDate As Date
    Public EndDate As Date
    Public ID1 As String
End Class
