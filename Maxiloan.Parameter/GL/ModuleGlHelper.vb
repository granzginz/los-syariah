﻿Imports System.Runtime.CompilerServices
Imports System.Linq
'Public Module ModuleGlHelper
'    Public Function GetEnumMasterAccTypeList() As IList(Of ValueTextObject)
'        Dim list = New List(Of ValueTextObject)
'        list.Add(New ValueTextObject("0", "ALL"))
'        For Each e In [Enum].GetValues(GetType(EnumMasterAccType))
'            list.Add(New ValueTextObject(e, e.ToString().Replace("_", " ")))
'        Next
'        Return list
'    End Function

'    Public Function YearsList() As IList(Of ValueTextObject)
'        Dim list = New List(Of ValueTextObject) 
'        For e As Integer = 2010 To DateTime.Now.Year + 8 
'            list.Add(New ValueTextObject(e, e.ToString()))
'        Next
'        Return list
'    End Function
'    <Extension()>
'    Public Function ToDataTable(ByVal data As IList(Of InitialBudget)) As DataTable

'        Dim dt As DataTable = New DataTable()
'        Dim row As DataRow
'        Dim strArray As String() = New String() {"companyId", "branchid", "Year", "coaid", "JAN", "FEB", "MAR", "APR", "MEI", "JUN", "JUL", "AGT", "SEP", "OKT", "NOV", "DES"}
'        For Each s In strArray
'            dt.Columns.Add(New DataColumn(s))
'        Next


'        For Each item In data
'            row = dt.NewRow()
'            row("companyId") = item.CompanyId
'            row("branchid") = item.BranchId 
'            row("Year") = item.Year
'            row("coaid") = item.CoAId
'            row("JAN") = item.Jan
'            row("FEB") = item.Feb
'            row("MAR") = item.Mar
'            row("APR") = item.Apr
'            row("MEI") = item.Mei
'            row("JUN") = item.Jun
'            row("JUL") = item.Jul
'            row("AGT") = item.Agt
'            row("SEP") = item.Sep
'            row("OKT") = item.Okt
'            row("NOV") = item.Nov
'            row("DES") = item.Des 
'            dt.Rows.Add(row)
'        Next

'        Return dt
'    End Function

'    <Extension()>
'    Public Function ToDataTable(ByVal data As IList(Of GlJournalVoucher)) As DataTable
'        Dim dt As DataTable = New DataTable()
'        Dim row As DataRow
'        Dim strArray As String() = New String() {"CompanyID", "BranchID", "Tr_Nomor", "PeriodYear", "PeriodMonth", "TransactionID",
'                                                 "Tr_Date", "Reff_Date", "Tr_DescH", "JournalAmount", "BatchID", "Status_Tr", "IsActive",
'                                                 "Flag", "SequenceNo", "CoaCo", "CoaId", "Tr_Desc", "Post", "Amount", "PaymentAllocationId"}

'        For Each s In strArray
'            dt.Columns.Add(New DataColumn(s))
'        Next

'        For Each v In data
'            For Each item In v.JournalItems
'                row = dt.NewRow()
'                row("CompanyID") = v.CompanyId.Trim()
'                row("BranchID") = v.BranchId.Trim()
'                row("Tr_Nomor") = v.TransactionNomor.Trim()
'                row("PeriodYear") = v.PeroidYear.Trim()
'                row("PeriodMonth") = v.PeriodMonth.Trim()
'                row("TransactionID") = v.TransactionId.Trim()
'                row("Tr_Date") = v.TransactionDate
'                row("Reff_Date") = v.ReffDate
'                row("Tr_DescH") = v.TransactionDesc.Trim()
'                row("JournalAmount") = v.JournalAmount

'                row("BatchID") = "-"
'                row("Status_Tr") = "OP"
'                row("IsActive") = "1"
'                row("Flag") = "O"



'                row("SequenceNo") = item.SequenceNo
'                row("CoaCo") = v.CompanyId.Trim()

'                row("CoaId") = item.CoaId.Trim()

'                row("Tr_Desc") = item.TransactionDesc.Trim()
'                row("Post") = item.Post

'                row("Amount") = item.Amount
'                row("PaymentAllocationId") = IIf(item.PaymentAllocationId Is Nothing, String.Empty, item.PaymentAllocationId.Trim())


'                dt.Rows.Add(row)
'            Next
'        Next

'        Return dt
'    End Function


'End Module
Public NotInheritable Class ModuleGlHelper
    Private Sub New()
    End Sub
    Public Shared Function GetEnumMasterAccTypeList() As IList(Of ValueTextObject)
        Dim list = New List(Of ValueTextObject)()
        list.Add(New ValueTextObject("0", "ALL"))

        For Each e In DirectCast([Enum].GetValues(GetType(EnumMasterAccType)), IList(Of EnumMasterAccType)).AsEnumerable()
            list.Add(New ValueTextObject(String.Format("{0}", DirectCast(e, Integer)), e.ToString().Replace("_", " ")))
        Next
        Return list
    End Function


    Public Shared Function GetGlFunctionsList() As IList(Of ValueTextObject)
        Dim list = New List(Of ValueTextObject)()
        list.Add(New ValueTextObject("0", "SELECT"))

        For Each e In DirectCast([Enum].GetValues(GetType(GlFunctions)), IList(Of GlFunctions)).AsEnumerable()
            list.Add(New ValueTextObject(String.Format("{0}", DirectCast(e, Integer)), e.ToString().Replace("_", " ")))
        Next
        Return list
    End Function



    Public Shared Function YearsList() As IList(Of ValueTextObject)
        Dim list = New List(Of ValueTextObject)()
        Dim e = 2010
        While e < DateTime.Now.Year + 8
            list.Add(New ValueTextObject(e.ToString(), e.ToString()))
            System.Math.Max(System.Threading.Interlocked.Increment(e), e - 1)
        End While
        Return list
    End Function


    Public Shared Function GetGLBalanceSheetAccTypeList() As IList(Of ValueTextObject)
        Dim list = New List(Of ValueTextObject)()
        For Each e In DirectCast([Enum].GetValues(GetType(GLBalanceSheetAccountType)), IList(Of GLBalanceSheetAccountType)).AsEnumerable()
            list.Add(New ValueTextObject(String.Format("{0}", CInt(e)), e.ToString()))
        Next
        Return list
    End Function

End Class
