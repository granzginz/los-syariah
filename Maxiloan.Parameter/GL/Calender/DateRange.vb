﻿<Serializable()> _
Public Class DateRange
    Implements IDateRange
    Private ReadOnly m_privateOnly As Boolean
    Private m_end As DateTime
    Private m_start As DateTime
    Public Shared ReadOnly Empty As New DateRange(Past, Past.AddDays(1.0), True)

    Public Shared ReadOnly Future As New DateTime(&HBB8, 1, 1)
    Public Shared ReadOnly Past As DateTime = DateAdapter.NullDate

    Public Sub New()
        Me.New(Past, Future)
    End Sub

    Public Sub New(start As DateTime, [end] As DateTime)
        m_start = setDate(start)
        m_end = setDate([end])
        assertIsStartLessThanEnd()
    End Sub

    Private Sub New(start As DateTime, [end] As DateTime, privateOnly As Boolean)
        m_privateOnly = privateOnly
        m_start = setDate(start)
        m_end = setDate([end])
    End Sub

    Private Sub assertIsStartLessThanEnd()
        If IsEmpty() Then
            Throw New CalendarException("Start date cannot greather than end date.")
        End If
    End Sub

    Public Overrides Function Equals(arg As Object) As Boolean
        If Not (TypeOf arg Is DateRange) Then
            Return False
        End If
        Dim range = DirectCast(arg, DateRange)
        Return (m_start.Equals(range.StartDate) AndAlso m_end.Equals(range.EndDate))
    End Function

    Public Overrides Function GetHashCode() As Integer
        Return m_start.GetHashCode()
    End Function

    Public Function GetSpan() As TimeSpan Implements IDateRange.GetSpan
        Return If(IsEmpty(), New TimeSpan(0, 0, 0, 0), m_end.AddDays(1.0).Subtract(m_start))
    End Function

    Public Function Includes(range As IDateRange) As Boolean Implements IDateRange.Includes
        Return (Includes(range.StartDate) AndAlso Includes(range.EndDate))
    End Function

    Public Function Includes([date] As DateTime) As Boolean Implements IDateRange.Includes
        Return (([date] >= m_start) AndAlso ([date] <= m_end))
    End Function

    Public Function InterSection(range As IDateRange) As IDateRange Implements IDateRange.InterSection
        Return If(Not Overlaps(range), Empty, interSectionEvaluation(range))
    End Function

    Private Function interSectionEvaluation(range As IDateRange) As IDateRange
        If Includes(range) Then
            Return range
        End If
        If range.Includes(Me) Then
            Return Me
        End If
        Return If(Includes(range.StartDate), New DateRange(range.StartDate, m_end), New DateRange(m_start, range.EndDate))
    End Function

    Public Function IsEmpty() As Boolean Implements IDateRange.IsEmpty
        Return (m_start > m_end)
    End Function

    Public Function IsInRange(range As IDateRange) As Boolean Implements IDateRange.IsInRange
        Return Overlaps(range)
    End Function

    Public Function IsInRange(mdate As DateTime) As Boolean Implements IDateRange.IsInRange
        Return Includes(mdate)
    End Function

    Public Function Overlaps(range As IDateRange) As Boolean Implements IDateRange.Overlaps
        If Not range.Includes(m_start) AndAlso Not range.Includes(m_end) Then
            Return Includes(range)
        End If
        Return True
    End Function

    Private Function setDate(mdate As DateTime) As DateTime
        Return New DateTime(mdate.Year, mdate.Month, mdate.Day, mdate.Hour, mdate.Minute, mdate.Second)
    End Function

    Public Shared Function StartingOn(start As DateTime) As DateRange
        Return New DateRange(start, Future)
    End Function

    Public Overrides Function ToString() As String
        If IsEmpty() Then
            Return "Empty Date Range"
        End If
        Return (Convert.ToString(m_start) & " - " & Convert.ToString(m_end))
    End Function

    Public Shared Function UpTo(endDate As DateTime) As DateRange
        Return New DateRange(Past, endDate)
    End Function

    Public ReadOnly Property Days() As Integer Implements IDateRange.Days
        Get
            Return GetSpan().Days
        End Get
    End Property

    Public ReadOnly Property EndDate As DateTime Implements IDateRange.EndDate
        Get
            Return m_end
        End Get
    End Property


    Public ReadOnly Property StartDate As DateTime Implements IDateRange.StartDate
        Get
            Return m_start
        End Get
    End Property
End Class
