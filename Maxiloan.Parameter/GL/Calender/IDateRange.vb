﻿Public Interface IDateRange
    Function GetSpan() As TimeSpan
    Function Includes(range As IDateRange) As Boolean
    Function Includes(mDate As DateTime) As Boolean
    Function InterSection(range As IDateRange) As IDateRange
    Function IsEmpty() As Boolean
    Function IsInRange(range As IDateRange) As Boolean
    Function IsInRange(mdate As DateTime) As Boolean
    Function Overlaps(range As IDateRange) As Boolean
    ReadOnly Property Days As Integer
    ReadOnly Property EndDate As DateTime
    ReadOnly Property StartDate As DateTime
End Interface
