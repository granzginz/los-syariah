﻿
<Serializable()>
Public Class DateAdapter


    Private m_date As DateTime
    Public Shared ReadOnly NullDate As DateTime = New DateTime(&H76C, 1, 1)
    Public Sub New()

    End Sub
    Public Sub New(mdate As DateTime)
        Me.New(mdate.Year, mdate.Month, mdate.Day)
    End Sub

    Public Sub New(year As Integer, month As Integer, day As Integer)
        m_date = New DateTime(year, month, day)
    End Sub
 
    Public Function CopySystemTime() As DateAdapter
        Return CopyTime(DateTime.Now)
    End Function
 
    Public Function CopyTime(dateTime As DateTime) As DateAdapter
        Return New DateAdapter(dateTime) With {.m_date = New DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second)}
    End Function

    Public Overrides Function Equals(obj As Object) As Boolean

        Return ((TypeOf obj Is DateAdapter) And m_date.Equals(CType(obj, DateAdapter).m_date))

    End Function
        

    Public Function EqualTo(mDate As DateAdapter) As Boolean
        Return (m_date = mDate.m_date)
    End Function

    Public Overrides Function GetHashCode() As Integer
        Return m_date.GetHashCode()
    End Function
          
    Public Function GreatherThan(mdate As DateAdapter) As Boolean
        Return (m_date > mdate.m_date)
    End Function
     
    Public Function GreatherThanOrEqualTo(mdate As DateAdapter) As Boolean
        Return (m_date >= mdate.m_date)
    End Function 
    Public Function LessThan(mdate As DateAdapter) As Boolean
        Return (m_date < mdate.m_date)
    End Function 
    Public Function LessThanOrEqualTo(mdate As DateAdapter) As Boolean
        Return (m_date <= mdate.m_date)
    End Function 
    Public Overrides Function ToString() As String
        Return m_date.ToShortDateString()
    End Function 

    Public Property AdapterDate As DateTime
        Get
            Return m_date
        End Get
        Set(value As DateTime)
            m_date = value
        End Set
    End Property
 
End Class
