﻿
Imports System
Imports System.Runtime.Serialization
<Serializable()>
Public Class CalendarException
    Inherits ApplicationException
    Public Sub New()
    End Sub
    Public Sub New(root As Exception)
        MyBase.New("An exception occured in calendar modul", root)
    End Sub
    Public Sub New(message As String)
        MyBase.New(message)
    End Sub
    Public Sub New(info As SerializationInfo, context As StreamingContext)
        MyBase.New(info, context)
    End Sub
    Public Sub New(message As String, e As Exception)
        MyBase.New(message, e)
    End Sub

End Class
