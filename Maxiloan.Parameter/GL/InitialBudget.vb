﻿<Serializable()> _
Public Class InitialBudget
    Implements IComparable

    Public Sub New()
    End Sub

    Public Sub New(companyId As String, branchId As String, year As Int32, coAId As String)
        Me.New(companyId, branchId, year, coAId, "", 0, _
         0, 0, 0, 0, 0, 0, _
         0, 0, 0, 0, 0)
    End Sub

    Public Sub New(companyId__1 As String, branchId__2 As String, year__3 As Int32, coAId__4 As String, coaName__5 As String, jan__6 As Decimal, _
     feb__7 As Decimal, mar__8 As Decimal, apr__9 As Decimal, mei__10 As Decimal, jun__11 As Decimal, jul__12 As Decimal, _
     agt__13 As Decimal, sep__14 As Decimal, okt__15 As Decimal, nov__16 As Decimal, des__17 As Decimal)
        CompanyId = companyId__1
        BranchId = branchId__2
        Year = year__3
        CoAId = coAId__4
        CoaName = coaName__5
        Jan = jan__6
        Feb = feb__7
        Mar = mar__8
        Apr = apr__9
        Mei = mei__10
        Jun = jun__11
        Jul = jul__12
        Agt = agt__13
        Sep = sep__14
        Okt = okt__15
        Nov = nov__16
        Des = des__17
    End Sub

    Public ReadOnly Property BdKey() As String
        Get
            Return [String].Format("{0};{1};{2};{3}", CompanyId.Trim(), BranchId.Trim(), Year, CoAId.Trim())
        End Get
    End Property

    Public Property CompanyId() As String
        Get
            Return m_CompanyId
        End Get
        Set(value As String)
            m_CompanyId = Value
        End Set
    End Property
    Private m_CompanyId As String
    Public Property BranchId() As String
        Get
            Return m_BranchId
        End Get
        Set(value As String)
            m_BranchId = Value
        End Set
    End Property
    Private m_BranchId As String
    Public Property Year() As Int32
        Get
            Return m_Year
        End Get
        Set(value As Int32)
            m_Year = Value
        End Set
    End Property
    Private m_Year As Int32
    Public Property CoAId() As String
        Get
            Return m_CoAId
        End Get
        Set(value As String)
            m_CoAId = Value
        End Set
    End Property
    Private m_CoAId As String

    Public Property CoaName() As String
        Get
            Return m_CoaName
        End Get
        Set(value As String)
            m_CoaName = Value
        End Set
    End Property
    Private m_CoaName As String

    Public Property Jan() As Decimal
        Get
            Return m_Jan
        End Get
        Set(value As Decimal)
            m_Jan = Value
        End Set
    End Property
    Private m_Jan As Decimal
    Public Property Feb() As Decimal
        Get
            Return m_Feb
        End Get
        Set(value As Decimal)
            m_Feb = Value
        End Set
    End Property
    Private m_Feb As Decimal
    Public Property Mar() As Decimal
        Get
            Return m_Mar
        End Get
        Set(value As Decimal)
            m_Mar = Value
        End Set
    End Property
    Private m_Mar As Decimal
    Public Property Apr() As Decimal
        Get
            Return m_Apr
        End Get
        Set(value As Decimal)
            m_Apr = Value
        End Set
    End Property
    Private m_Apr As Decimal
    Public Property Mei() As Decimal
        Get
            Return m_Mei
        End Get
        Set(value As Decimal)
            m_Mei = Value
        End Set
    End Property
    Private m_Mei As Decimal
    Public Property Jun() As Decimal
        Get
            Return m_Jun
        End Get
        Set(value As Decimal)
            m_Jun = Value
        End Set
    End Property
    Private m_Jun As Decimal
    Public Property Jul() As Decimal
        Get
            Return m_Jul
        End Get
        Set(value As Decimal)
            m_Jul = Value
        End Set
    End Property
    Private m_Jul As Decimal
    Public Property Agt() As Decimal
        Get
            Return m_Agt
        End Get
        Set(value As Decimal)
            m_Agt = Value
        End Set
    End Property
    Private m_Agt As Decimal
    Public Property Sep() As Decimal
        Get
            Return m_Sep
        End Get
        Set(value As Decimal)
            m_Sep = Value
        End Set
    End Property
    Private m_Sep As Decimal
    Public Property Okt() As Decimal
        Get
            Return m_Okt
        End Get
        Set(value As Decimal)
            m_Okt = Value
        End Set
    End Property
    Private m_Okt As Decimal
    Public Property Nov() As Decimal
        Get
            Return m_Nov
        End Get
        Set(value As Decimal)
            m_Nov = Value
        End Set
    End Property
    Private m_Nov As Decimal
    Public Property Des() As Decimal
        Get
            Return m_Des
        End Get
        Set(value As Decimal)
            m_Des = Value
        End Set
    End Property
    Private m_Des As Decimal

    Public Function CompareTo(obj As Object) As Integer Implements IComparable.CompareTo
        Return [String].CompareOrdinal(BdKey, TryCast(obj, InitialBudget).BdKey)
    End Function

    Public Shared Function ParseInitialBudgetCsv(reader As System.IO.StreamReader, compId As String, branch As String) As IList(Of InitialBudget)
        Dim result = New List(Of InitialBudget)()

        While Not reader.EndOfStream
            Dim originalLine = reader.ReadLine()
            Dim oriSplit = originalLine.Split(","c)

            If originalLine.Contains("YEAR") OrElse originalLine.Contains("COA") OrElse originalLine.Contains("JAN") Then
                Continue While
            End If
            If oriSplit.Length < 14 Then
                Throw New Exception("Format data file csv tidak valid.")
            End If

            Dim jan = Decimal.Parse(oriSplit(2).Replace("""", ""))
            Dim Feb = Decimal.Parse(oriSplit(3).Replace("""", ""))
            Dim Mar = Decimal.Parse(oriSplit(4).Replace("""", ""))
            Dim Apr = Decimal.Parse(oriSplit(5).Replace("""", ""))
            Dim Mei = Decimal.Parse(oriSplit(6).Replace("""", ""))
            Dim Jun = Decimal.Parse(oriSplit(7).Replace("""", ""))
            Dim Jul = Decimal.Parse(oriSplit(8).Replace("""", ""))
            Dim Agt = Decimal.Parse(oriSplit(9).Replace("""", ""))
            Dim Sep = Decimal.Parse(oriSplit(10).Replace("""", ""))
            Dim Okt = Decimal.Parse(oriSplit(11).Replace("""", ""))
            Dim Nov = Decimal.Parse(oriSplit(12).Replace("""", ""))
            Dim Des = Decimal.Parse(oriSplit(13).Replace("""", ""))
            Dim year = Integer.Parse(oriSplit(0).Replace("""", ""))

            result.Add(New InitialBudget(compId, branch, year, oriSplit(1).Replace("""", ""), "", jan, _
             Feb, Mar, Apr, Mei, Jun, Jul, _
             Agt, Sep, Okt, Nov, Des))
        End While
        Return result
    End Function

End Class



'Public Class InitialBudget
'    Implements IComparable

'    Public Sub New()

'    End Sub
'    Public Sub New(_companyId As String, _branchId As String, _year As Integer, _coAId As String)
'        Me.New(_companyId, _branchId, _year, _coAId, "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
'    End Sub
'    Public Sub New(_companyId As String, _branchId As String, _year As Integer, _coAId As String, _coaName As String,_Jan As Decimal, _Feb As Decimal, _Mar As Decimal, _Apr As Decimal, _Mei As Decimal, _Jun As Decimal, _Jul As Decimal, _Agt As Decimal, _Sep As Decimal, _Okt As Decimal, _Nov As Decimal, _Des As Decimal)
'        CompanyId = _companyId
'        BranchId = _branchId
'        Year = _year
'        CoAId = _coAId
'        CoaName = _coaName
'        Jan = _Jan
'        Feb = _Feb
'        Mar = _Mar
'        Apr = _Apr
'        Mei = _Mei
'        Jun = _Jun
'        Jul = _Jul
'        Agt = _Agt
'        Sep = _Sep
'        Okt = _Okt
'        Nov = _Nov
'        Des = _Des
'    End Sub

'    Public ReadOnly Property BdKey
'        Get
'            Return String.Format("{0};{1};{2};{3}", CompanyId.trim(), BranchId.trim(), Year, CoAId.trim())
'        End Get
'    End Property
'    Public Property CompanyId
'    Public Property BranchId
'    Public Property Year As Integer
'    Public Property CoAId
'    Public Property CoaName
'    Public Property Jan As Decimal
'    Public Property Feb As Decimal
'    Public Property Mar As Decimal
'    Public Property Apr As Decimal
'    Public Property Mei As Decimal
'    Public Property Jun As Decimal
'    Public Property Jul As Decimal
'    Public Property Agt As Decimal
'    Public Property Sep As Decimal
'    Public Property Okt As Decimal
'    Public Property Nov As Decimal
'    Public Property Des As Decimal

'    Public Function CompareTo(ByVal obj As Object) As Integer Implements IComparable.CompareTo
'        Dim _obj = CType(obj, InitialBudget)
'        Return System.String.Compare(BdKey, _obj.BdKey)
'    End Function


'    Public Shared Function ParseInitialBudgetCsv(reader As IO.StreamReader, compId As String, branch As String) As IList(Of InitialBudget)
'        Dim originalLine As String


'        Dim result As IList(Of InitialBudget) = New List(Of InitialBudget)()


'        While (Not reader.EndOfStream)
'            originalLine = reader.ReadLine.ToString

'            Dim OriSplit() As String = Split(originalLine, ",")

'            If (originalLine.Contains("YEAR") Or originalLine.Contains("COA") Or originalLine.Contains("JAN")) Then
'                Continue While
'            End If
'            If (OriSplit.Length < 14) Then
'                Throw New Exception("Format data file csv tidak valid.")
'            End If
'            Dim jan = CType(OriSplit(2).Replace("""", ""), Decimal)
'            Dim Feb = CType(OriSplit(3).Replace("""", ""), Decimal)
'            Dim Mar = CType(OriSplit(4).Replace("""", ""), Decimal)
'            Dim Apr = CType(OriSplit(5).Replace("""", ""), Decimal)
'            Dim Mei = CType(OriSplit(6).Replace("""", ""), Decimal)
'            Dim Jun = CType(OriSplit(7).Replace("""", ""), Decimal)
'            Dim Jul = CType(OriSplit(8).Replace("""", ""), Decimal)
'            Dim Agt = CType(OriSplit(9).Replace("""", ""), Decimal)
'            Dim Sep = CType(OriSplit(10).Replace("""", ""), Decimal)
'            Dim Okt = CType(OriSplit(11).Replace("""", ""), Decimal)
'            Dim Nov = CType(OriSplit(12).Replace("""", ""), Decimal)
'            Dim Des = CType(OriSplit(13).Replace("""", ""), Decimal)

'            Dim initb = New InitialBudget(compId, branch, CInt(OriSplit(0).Replace("""", "")), OriSplit(1).Replace("""", ""), "", jan, Feb, Mar, Apr, Mei, Jun, Jul, Agt, Sep, Okt, Nov, Des)
'            result.Add(initb)



'        End While
'        Return result


'    End Function

'End Class
