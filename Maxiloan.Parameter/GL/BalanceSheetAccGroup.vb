<Serializable()> _
Public Class BalanceSheetAccGroup
    Implements IComparable
    Private _reportGroupId As Integer
    Private _reportId As String
    Private _groupDesc As String
    Private _parentGroupName As String
    Private _isBar As Boolean
    Private _layoutSeq As Integer
    Private _groupType As String

    Public Sub New()
    End Sub

    Public Sub New(reportGroupId As Int32, reportId As String, groupDescription As String, parentGroupName As String, isbar As Boolean, layoutSeq As Integer, _
     grouptype As String, level__1 As Int16)
        _reportGroupId = reportGroupId
        _reportId = reportId
        _groupDesc = groupDescription
        _parentGroupName = parentGroupName
        _isBar = isbar
        _layoutSeq = layoutSeq
        _groupType = grouptype
        Level = level__1
    End Sub

    Public Sub New(reportGroupId As Int32, reportId As String, groupDescription As String, parentGroupName As String, isbar As Boolean, layoutSeq As Integer, _
     grouptype As String, level As Int16, userCr As String, userUp As String)
        Me.New(reportGroupId, reportId, groupDescription, parentGroupName, isbar, layoutSeq, _
         grouptype, 0)
        UserCreate = userCr
        UserUpdate = userUp
    End Sub

    Public Property UserCreate() As String
        Get
            Return m_UserCreate
        End Get
        Private Set(value As String)
            m_UserCreate = value
        End Set
    End Property
    Private m_UserCreate As String

    Public Property UserUpdate() As String
        Get
            Return m_UserUpdate
        End Get
        Private Set(value As String)
            m_UserUpdate = value
        End Set
    End Property
    Private m_UserUpdate As String
    Public Property Level() As Short
        Get
            Return m_Level
        End Get
        Private Set(value As Short)
            m_Level = value
        End Set
    End Property
    Private m_Level As Short

    Public Property ReportGroupId() As Integer
        Get
            Return _reportGroupId
        End Get
        Set(value As Integer)
            _reportGroupId = value
        End Set
    End Property

    Public Property ReportId() As String
        Get
            Return _reportId
        End Get
        Set(value As String)
            _reportId = value
        End Set
    End Property

    Public Property groupDesc() As String
        Get
            Return _groupDesc
        End Get
        Set(value As String)
            _groupDesc = value
        End Set
    End Property

    Public Property groupType() As String
        Get
            Return _groupType
        End Get
        Set(value As String)
            _groupType = value
        End Set
    End Property

    Public Property LayoutSequence() As Integer
        Get
            Return _layoutSeq
        End Get
        Set(value As Integer)
            _layoutSeq = value
        End Set
    End Property

    Public Property IsBar() As Boolean
        Get
            Return _isBar
        End Get
        Set(value As Boolean)
            _isBar = value
        End Set
    End Property

    Public ReadOnly Property gDesc() As String
        Get
            Return _groupDesc.Replace(" ", "&nbsp;")
        End Get
    End Property

    Public Property ParentGroup() As Integer
        Get
            Return m_ParentGroup
        End Get
        Set(value As Integer)
            m_ParentGroup = value
        End Set
    End Property
    Private m_ParentGroup As Integer
    Public ReadOnly Property ParentGroupName() As String
        Get
            Return _parentGroupName
        End Get
    End Property

    Public Function CompareTo(obj As Object) As Integer Implements IComparable.CompareTo
        Return System.[String].CompareOrdinal(_parentGroupName, TryCast(obj, BalanceSheetAccGroup)._parentGroupName)
    End Function

End Class