﻿
Imports System.Text.RegularExpressions

<Serializable()> _
Public Class JournalVoucher_header : Inherits Common
    Public Property CompanyID As String

    Public Property Tr_Nomor As String
    Public Property PeriodYear As String
    Public Property PeriodMonth As String
    Public Property TransactionID As String
    Public Property tr_Date1 As DateTime
    Public Property Tr_Date As DateTime
    Public Property Reff_No As String
    Public Property Reff_Date As DateTime
    Public Property Tr_Desc As String
    Public Property JournalAmount As Decimal
    Public Property BatchID As String

    Public Property Status_Tr As String
    Public Property IsActive As Boolean
    Public Property Flag As Char
    Public Property Status As Char
    Public Property IsValid As Boolean
    Public Property DepresiasiTenor As Integer
    Public Property SisaTenor As Integer
    Public Property listDetails As DataTable


    Public Property Periode1 As DateTime
    Public Property Periode2 As DateTime
End Class
'<Serializable()> _
'Public Class GlJournalVoucher
'    Private _journalVoucherItem As IList(Of JournalVoucherItem)

'    Sub New()
'        _journalVoucherItem = New List(Of JournalVoucherItem)
'    End Sub

'    Public Property CompanyId As String

'    Public Property BranchId As String

'    Public Property TransactionDate As DateTime

'    Public Property TransactionNomor As String

'    Public Property ReffNomor As String
'    Public Property ReffDate As DateTime

'    Public ReadOnly Property PeroidYear As String
'        Get
'            Return TransactionDate.Year.ToString()
'        End Get
'    End Property

'    Public ReadOnly Property PeriodMonth As String
'        Get
'            Return TransactionDate.Month.ToString()
'        End Get
'    End Property

'    Public Property TransactionId As String

'    Public Property TransactionDesc As String

'    Public ReadOnly Property JournalAmount As Decimal
'        Get
'            Return getAmount("D")
'        End Get
'    End Property
'    Public ReadOnly Property JournalItems As IList(Of JournalVoucherItem)
'        Get
'            Return _journalVoucherItem
'        End Get
'    End Property
'    Public ReadOnly Property IsBalance As Boolean
'        Get
'            Return getAmount("D") = getAmount("C")
'        End Get
'    End Property

'    Public ReadOnly Property IsItemValid As Boolean
'        Get
'            Dim result As Boolean = True
'            For Each item In From item1 In _journalVoucherItem Where (item1.Message.Trim <> "")
'                result = False
'            Next
'            Return result
'        End Get
'    End Property
'    Public ReadOnly Property IsItemCoaValid As Boolean
'        Get
'            Dim result As Boolean = True
'            For Each item In From item1 In _journalVoucherItem Where (item1.CoaId.Trim = "")
'                result = False
'            Next
'            Return result
'        End Get
'    End Property

'    Private Function getAmount(post As String) As Decimal
'        Return (From e In _journalVoucherItem Where (e.Post = post)).Sum(Function(e) e.Amount)
'    End Function
'    Public Sub AddItem(item As JournalVoucherItem)
'        _journalVoucherItem.Add(item)
'    End Sub

'    Public Shared Function ParseJournalCsv(reader As IO.StreamReader, compId As String, branch As String) As IList(Of GlJournalVoucher)

'        Dim originalLine As String
'        '0          1         2        3        4       5            6           7      8    9     10
'        'Tr_Nomor,SequenceNo,Tr_Date,Reff_Date,CoaId,TransactionID,Header_Desc,Tr_Desc,Post,Amount,PaymentAllocationId
'        Dim trNo As String = ""
'        Dim result As IList(Of GlJournalVoucher) = New List(Of GlJournalVoucher)()

'        Dim jv As GlJournalVoucher
'        While (Not reader.EndOfStream)
'            originalLine = reader.ReadLine.ToString
'            Dim OriSplit() As String = Split(originalLine, ",", """", False)

'            If (OriSplit(0).Replace("""", "").Contains("Tr_Nomor")) Then
'                Continue While
'            End If


'            If (trNo <> OriSplit(0).Replace("""", "")) Then
'                trNo = OriSplit(0).Replace("""", "")
'                jv = New GlJournalVoucher()
'                jv.TransactionNomor = OriSplit(0).Replace("""", "")
'                jv.CompanyId = compId
'                jv.BranchId = branch
'                jv.TransactionDate = stringToDate(OriSplit(2).Replace("""", ""))
'                jv.ReffDate = stringToDate(OriSplit(2).Replace("""", ""))
'                jv.TransactionId = OriSplit(5).Replace("""", "")
'                jv.TransactionDesc = OriSplit(6).Replace("""", "")
'                result.Add(jv)
'            End If

'            Dim jItem = New JournalVoucherItem()

'            jItem.SequenceNo = CInt(OriSplit(1).Replace("""", ""))
'            jItem.CoaId = OriSplit(4).Replace("""", "")
'            jItem.TransactionId = OriSplit(5).Replace("""", "")
'            jItem.TransactionDesc = OriSplit(7).Replace("""", "")
'            jItem.Post = OriSplit(8).Replace("""", "")
'            jItem.Amount = CType(OriSplit(9).Replace("""", ""), Decimal)
'            jItem.CoaIdX = "-"
'            jItem.PaymentAllocationId = OriSplit(10).Replace("""", "")

'            jv.AddItem(jItem)

'        End While
'        Return result
'    End Function

'    Shared Function stringToDate(strDate As String) As DateTime
'        Dim dt() As String = strDate.Split("/")
'        Return New DateTime(CInt(dt(2)), CInt(dt(1)), CInt(dt(0)))
'    End Function

'    Shared Function Split(ByVal expression As String, ByVal delimiter As String, ByVal qualifier As String, ByVal ignoreCase As Boolean) As String()
'        Dim _Statement As String = String.Format("{0}(?=(?:[^{1}]*{1}[^{1}]*{1})*(?![^{1}]*{1}))", Regex.Escape(delimiter), Regex.Escape(qualifier))

'        Dim _Options As RegexOptions = RegexOptions.Compiled Or RegexOptions.Multiline
'        If ignoreCase Then _Options = _Options Or RegexOptions.IgnoreCase

'        Dim _Expression As Regex = New Regex(_Statement, _Options)
'        Return _Expression.Split(expression)
'    End Function
'End Class