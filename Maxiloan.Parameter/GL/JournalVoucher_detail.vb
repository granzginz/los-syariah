﻿<Serializable()> _
Public Class JournalVoucher_detail : Inherits Common
    Public Property Tr_Nomor As String
    Public Property SequenceNo As Integer
    Public Property CoaCo As String
    Public Property CoaBranch As String
    Public Property CoaId As String
    Public Property TransactionID As String
    Public Property Tr_Desc As String
    Public Property Post As Char
    Public Property Amount As Decimal
    Public Property CoaId_X As String
    Public Property PaymentAllocationId As String
    Public Property ProductId As String 
End Class
'<Serializable()> _
'Public Class JournalVoucherItem
'    Implements IComparable

'    Public Sub New()
'        _paymentAllocationId = ""
'    End Sub
'    Public Sub New(compId As String, branch As String, sNo As Integer, cid As String, cName As String, trDesc As String, p As String, amnt As Decimal)
'        CompanyId = compId
'        BranchId = Branch
'        SequenceNo = sNo
'        CoaId = cid
'        CoaName = cName 
'        TransactionDesc = trDesc
'        Post = p
'        Amount = amnt
'        _paymentAllocationId = ""
'    End Sub

'    Public ReadOnly Property ItemKey
'        Get
'            Return String.Format("{0}{1}", CoaId.Trim(), SequenceNo)
'        End Get
'    End Property

'    Public Property SequenceNo As Integer

'    Public Property CoaId As String

'    Public Property CoaName As String

'    Public Property TransactionDesc As String

'    Public Property TransactionId As String

'    Public Property Post As Char

'    Public Property Amount As Decimal

'    Public Property CoaIdX As String

'    Private _paymentAllocationId As String
'    Public Property PaymentAllocationId As String
'        Get
'            Return _paymentAllocationId
'        End Get
'        Set(value As String)
'            _paymentAllocationId = value
'        End Set
'    End Property

'    Public Property Message As String

'    Public Property CompanyId As String

'    Public Property BranchId As String

'    Public ReadOnly Property PerCabCoA
'        Get
'            Return String.Format("{0}/{1}/{2}", CompanyId, BranchId, CoaId)
'        End Get
'    End Property

'    Public Function CompareTo(obj As Object) As Integer Implements IComparable.CompareTo
'        Dim _obj = CType(obj, JournalVoucherItem)
'        Return System.String.Compare(String.Format("{0}{1}", PerCabCoA, SequenceNo), String.Format("{0}{1}", obj.PerCabCoA, obj.SequenceNo))
'    End Function
'End Class