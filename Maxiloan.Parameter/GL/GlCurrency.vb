﻿<Serializable()> _
Public Class GlCurrency
    Implements IComparable

    Public Property CurrencyId() As String
        Get
            Return m_CurrencyId
        End Get
        Set(value As String)
            m_CurrencyId = Value
        End Set
    End Property
    Private m_CurrencyId As String
    Public Property CurrencyCode() As String
        Get
            Return m_CurrencyCode
        End Get
        Set(value As String)
            m_CurrencyCode = Value
        End Set
    End Property
    Private m_CurrencyCode As String
    Public Property CurrencyName() As String
        Get
            Return m_CurrencyName
        End Get
        Set(value As String)
            m_CurrencyName = Value
        End Set
    End Property
    Private m_CurrencyName As String
    Public Property CurrencyRounding() As Int16
        Get
            Return m_CurrencyRounding
        End Get
        Set(value As Int16)
            m_CurrencyRounding = Value
        End Set
    End Property
    Private m_CurrencyRounding As Int16
    Public Property CurrencySymbol() As String
        Get
            Return m_CurrencySymbol
        End Get
        Set(value As String)
            m_CurrencySymbol = Value
        End Set
    End Property
    Private m_CurrencySymbol As String
    Public Property CurrencyDecimalPlace() As Int16
        Get
            Return m_CurrencyDecimalPlace
        End Get
        Set(value As Int16)
            m_CurrencyDecimalPlace = Value
        End Set
    End Property
    Private m_CurrencyDecimalPlace As Int16
    Public Property IsHomeCurrency() As Boolean
        Get
            Return m_IsHomeCurrency
        End Get
        Set(value As Boolean)
            m_IsHomeCurrency = Value
        End Set
    End Property
    Private m_IsHomeCurrency As Boolean
    Public Property IsLocalCurrency() As Boolean
        Get
            Return m_IsLocalCurrency
        End Get
        Set(value As Boolean)
            m_IsLocalCurrency = Value
        End Set
    End Property
    Private m_IsLocalCurrency As Boolean
    Public Property IsBaseExchangeRate() As Boolean
        Get
            Return m_IsBaseExchangeRate
        End Get
        Set(value As Boolean)
            m_IsBaseExchangeRate = Value
        End Set
    End Property
    Private m_IsBaseExchangeRate As Boolean
    Public Property IsDefault() As Boolean
        Get
            Return m_IsDefault
        End Get
        Set(value As Boolean)
            m_IsDefault = Value
        End Set
    End Property
    Private m_IsDefault As Boolean
    Public Property MaxRate() As Decimal
        Get
            Return m_MaxRate
        End Get
        Set(value As Decimal)
            m_MaxRate = Value
        End Set
    End Property
    Private m_MaxRate As Decimal
    Public Property MinRate() As Decimal
        Get
            Return m_MinRate
        End Get
        Set(value As Decimal)
            m_MinRate = Value
        End Set
    End Property
    Private m_MinRate As Decimal

    Public Function CompareTo(obj As Object) As Integer Implements IComparable.CompareTo
        Return [String].CompareOrdinal([String].Format("{0}{1}", CurrencyCode, CurrencyName), [String].Format("{0}{1}", TryCast(obj, GlCurrency).CurrencyCode, TryCast(obj, GlCurrency).CurrencyName))
    End Function
End Class

