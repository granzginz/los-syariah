﻿<Serializable()> _
Public Class JournalVoucherItem
    Implements IComparable
    Private m_paymentAllocationId As String

    Public Sub New()
        m_paymentAllocationId = ""
    End Sub
    Private m_Id As Long
    Public Sub New(id As Long, compId As String, branch As String, sNo As Long, cid As String, cName As String, trDesc As String,
     p As String, amnt As Decimal, addf As String)
        Me.New(compId, branch, sNo, cid, cName, trDesc, p, amnt, addf)
        m_Id = id
    End Sub

    Public Sub New(compId As String, branch As String, sNo As Long, cid As String, cName As String, trDesc As String,
     p As String, amnt As Decimal, addf As String)
        CompanyId = compId
        BranchId = branch
        _sequenceNo = sNo
        CoaId = cid
        CoaName = cName
        TransactionDesc = trDesc
        Post = p
        Amount = amnt
        m_paymentAllocationId = ""
        AddFree = addf


    End Sub

    Public ReadOnly Property ItemKey() As String
        Get
            Return String.Format("{0}{1}{2}", CoaId.Trim(), SequenceNo, AddFree)
        End Get
    End Property

    Private _sequenceNo As Long
    Public Property SequenceNo() As Long
        Get
            Return _sequenceNo
        End Get
        Set(value As Long)
            _sequenceNo = value
        End Set
    End Property
    Public Property CoaId() As String
        Get
            Return m_CoaId
        End Get
        Set(value As String)
            m_CoaId = Value
        End Set
    End Property
    Private m_CoaId As String
    Public Property CoaName() As String
        Get
            Return m_CoaName
        End Get
        Set(value As String)
            m_CoaName = Value
        End Set
    End Property
    Private m_CoaName As String
    Public Property TransactionDesc() As String
        Get
            Return m_TransactionDesc
        End Get
        Set(value As String)
            m_TransactionDesc = Value
        End Set
    End Property
    Private m_TransactionDesc As String
    Public Property TransactionId() As String
        Get
            Return m_TransactionId
        End Get
        Set(value As String)
            m_TransactionId = Value
        End Set
    End Property
    Private m_TransactionId As String
    Public Property Post() As String
        Get
            Return m_Post
        End Get
        Set(value As String)
            m_Post = Value
        End Set
    End Property
    Private m_Post As String
    Public Property Amount() As Decimal
        Get
            Return m_Amount
        End Get
        Set(value As Decimal)
            m_Amount = Value
        End Set
    End Property

    Private m_Amount As Decimal
    Public Property CoaIdX() As String
        Get
            Return m_CoaIdX
        End Get
        Set(value As String)
            m_CoaIdX = Value
        End Set
    End Property
    Private m_CoaIdX As String

    Public Property PaymentAllocationId() As String
        Get
            Return m_paymentAllocationId
        End Get
        Set(value As String)
            m_paymentAllocationId = value
        End Set
    End Property

    Public Property Message() As String
        Get
            Return m_Message
        End Get
        Set(value As String)
            m_Message = Value
        End Set
    End Property
    Private m_Message As String
    Public Property CompanyId() As String
        Get
            Return m_CompanyId
        End Get
        Set(value As String)
            m_CompanyId = Value
        End Set
    End Property
    Public Property CoaCo As String
    Public Property Tr_Nomor As String
    Public Property CoaBranch As String
    Public Property UsrDtm As String
    Public Property DtmUpd As DateTime
    Private m_CompanyId As String
    Public Property BranchId() As String
        Get
            Return m_BranchId
        End Get
        Set(value As String)
            m_BranchId = value
        End Set
    End Property
    Private m_BranchId As String

    Public Property AddFree() As String
        Get
            Return m_AddFree
        End Get
        Set(value As String)
            m_AddFree = value
        End Set
    End Property
    Private m_AddFree As String

    Public ReadOnly Property PerCabCoA() As String
        Get
            Return [String].Format("{0}/{1}/{2}/{3}", CompanyId, BranchId, CoaId, AddFree)
        End Get
    End Property

    Public Function CompareTo(obj As Object) As Integer Implements IComparable.CompareTo
        Return [String].CompareOrdinal([String].Format("{0}{1}", PerCabCoA, SequenceNo), [String].Format("{0}{1}", TryCast(obj, JournalVoucherItem).PerCabCoA, TryCast(obj, JournalVoucherItem).SequenceNo))
    End Function

End Class
