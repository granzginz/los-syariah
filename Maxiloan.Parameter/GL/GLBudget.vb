﻿<Serializable()> _
Public Class GlBudget
    Implements IComparable

    Public ReadOnly Property BdgKey() As String
        Get
            Return [String].Format("{0};{1};{2}", CompanyId, BranchId, CoaId)
        End Get
    End Property


    Public Property CompanyId() As String
        Get
            Return m_CompanyId
        End Get
        Set(value As String)
            m_CompanyId = Value
        End Set
    End Property
    Private m_CompanyId As String
    Public Property BranchId() As String
        Get
            Return m_BranchId
        End Get
        Set(value As String)
            m_BranchId = Value
        End Set
    End Property
    Private m_BranchId As String
    Public Property CoaId() As String
        Get
            Return m_CoaId
        End Get
        Set(value As String)
            m_CoaId = Value
        End Set
    End Property
    Private m_CoaId As String
    Public Property CoaName() As String
        Get
            Return m_CoaName
        End Get
        Set(value As String)
            m_CoaName = Value
        End Set
    End Property
    Private m_CoaName As String
    Public Property Type() As EnumMasterAccType
        Get
            Return m_Type
        End Get
        Set(value As EnumMasterAccType)
            m_Type = Value
        End Set
    End Property
    Private m_Type As EnumMasterAccType


    Public Property Year() As Int32
        Get
            Return m_Year
        End Get
        Set(value As Int32)
            m_Year = Value
        End Set
    End Property
    Private m_Year As Int32
    Public Property Jan() As Decimal
        Get
            Return m_Jan
        End Get
        Set(value As Decimal)
            m_Jan = Value
        End Set
    End Property
    Private m_Jan As Decimal
    Public Property Feb() As Decimal
        Get
            Return m_Feb
        End Get
        Set(value As Decimal)
            m_Feb = Value
        End Set
    End Property
    Private m_Feb As Decimal
    Public Property Mar() As Decimal
        Get
            Return m_Mar
        End Get
        Set(value As Decimal)
            m_Mar = Value
        End Set
    End Property
    Private m_Mar As Decimal
    Public Property Apr() As Decimal
        Get
            Return m_Apr
        End Get
        Set(value As Decimal)
            m_Apr = Value
        End Set
    End Property
    Private m_Apr As Decimal
    Public Property Mei() As Decimal
        Get
            Return m_Mei
        End Get
        Set(value As Decimal)
            m_Mei = Value
        End Set
    End Property
    Private m_Mei As Decimal
    Public Property Jun() As Decimal
        Get
            Return m_Jun
        End Get
        Set(value As Decimal)
            m_Jun = Value
        End Set
    End Property
    Private m_Jun As Decimal
    Public Property Jul() As Decimal
        Get
            Return m_Jul
        End Get
        Set(value As Decimal)
            m_Jul = Value
        End Set
    End Property
    Private m_Jul As Decimal
    Public Property Agt() As Decimal
        Get
            Return m_Agt
        End Get
        Set(value As Decimal)
            m_Agt = Value
        End Set
    End Property
    Private m_Agt As Decimal
    Public Property Sep() As Decimal
        Get
            Return m_Sep
        End Get
        Set(value As Decimal)
            m_Sep = Value
        End Set
    End Property
    Private m_Sep As Decimal
    Public Property Okt() As Decimal
        Get
            Return m_Okt
        End Get
        Set(value As Decimal)
            m_Okt = Value
        End Set
    End Property
    Private m_Okt As Decimal
    Public Property Nov() As Decimal
        Get
            Return m_Nov
        End Get
        Set(value As Decimal)
            m_Nov = Value
        End Set
    End Property
    Private m_Nov As Decimal
    Public Property Des() As Decimal
        Get
            Return m_Des
        End Get
        Set(value As Decimal)
            m_Des = Value
        End Set
    End Property
    Private m_Des As Decimal

    Public Function CompareTo(obj As Object) As Integer Implements IComparable.CompareTo
        Return [String].CompareOrdinal(CoaId, DirectCast(obj, GlBudget).CoaId)
    End Function
End Class



'Public Class GLBudget
'    Implements IComparable


'    Public ReadOnly Property BdgKey
'        Get
'            Return String.Format("{0};{1};{2}", CompanyId, BranchId, CoaId)
'        End Get
'    End Property

'    Public Property CompanyId 
'    Public Property BranchId 
'    Public Property CoaId As String 
'    Public Property CoaName As String 
'    Public Property Type As EnumMasterAccType

'    Public Property Year As Integer

'    Public Property Jan As Decimal
'    Public Property Feb As Decimal
'    Public Property Mar As Decimal
'    Public Property Apr As Decimal
'    Public Property Mei As Decimal
'    Public Property Jun As Decimal
'    Public Property Jul As Decimal
'    Public Property Agt As Decimal
'    Public Property Sep As Decimal
'    Public Property Okt As Decimal
'    Public Property Nov As Decimal
'    Public Property Des As Decimal


'    Public Function CompareTo(ByVal obj As Object) As Integer Implements IComparable.CompareTo
'        Dim _obj = CType(obj, GLBudget)
'        Return System.String.Compare(CoaId, _obj.CoaId)
'    End Function
'End Class



