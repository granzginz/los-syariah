﻿<Serializable()> _
Public Class MasterAccountObject
    Implements IComparable
    Private _coaId As String = String.Empty
    Private _description As String
    Private _type As EnumMasterAccType
    Private _currency As String
    Private _isActive As Boolean
    Private _fullyQualifiedName As String
    Private _coaIdTax As String
    Private _coaIdTaxAddFree As String
    Private func As GlFunctions
    Private _AddFreeNo As String
    Private _AddFreeDesc As String
    Private _LBU As String
    Private _SIPP As String


    Public Sub New()
    End Sub

    Public Sub New(coaId As String, desc As String, type As EnumMasterAccType, curr As String, fullName As String, branch__1 As String, _
     level__2 As Int16, ad As String, ads As String)
        Branch = branch__1
        _coaId = If(coaId = Nothing, "", coaId)
        _description = desc
        _type = type
        _currency = curr
        _fullyQualifiedName = fullName
        Level = level__2
        _AddFreeNo = ad
        _AddFreeDesc = ads
    End Sub

    Public Sub New(coaId As String, desc As String, type As EnumMasterAccType, curr As String, fullName As String, branch As String, ad As String, ads As String,
     isActive As Boolean, userCr As String, userUp As String, LBU As String, SIPP As String)
        Me.New(coaId, desc, type, curr, "", branch,
         0, ad, ads)
        _isActive = isActive
        UserCreate = userCr
        UserUpdate = userUp
        _LBU = LBU
        _SIPP = SIPP
    End Sub

    Public Sub New(coaId As String, desc As String, type As EnumMasterAccType, curr As String, fullName As String, branch As String, ad As String, ads As String,
     level As Int16, func As GlFunctions)
        Me.New(coaId, desc, type, curr, "", branch,
         level, ad, ads)
        Me.func = func
    End Sub

    Public Property CoaIdTax() As [String]
        Get
            Return _coaIdTax
        End Get
        Set(value As [String])
            _coaIdTax = value
        End Set
    End Property
    Public Property CoaTaxDesc() As String
        Get
            Return m_CoaTaxDesc
        End Get
        Set(value As String)
            m_CoaTaxDesc = Value
        End Set
    End Property
    Private m_CoaTaxDesc As String

    Public Property CoaIdTaxAddFree() As [String]
        Get
            Return _coaIdTaxAddFree
        End Get
        Set(value As [String])
            _coaIdTaxAddFree = value
        End Set
    End Property
    Public Property CoaTaxAddFreeDesc() As String
        Get
            Return m_CoaTaxAddFreeDesc
        End Get
        Set(value As String)
            m_CoaTaxAddFreeDesc = value
        End Set
    End Property
    Private m_CoaTaxAddFreeDesc As String

    Public Property CompanyId() As [String]
        Get
            Return m_CompanyId
        End Get
        Set(value As [String])
            m_CompanyId = Value
        End Set
    End Property
    Private m_CompanyId As [String]
    Public Property Level() As Short
        Get
            Return m_Level
        End Get
        Private Set(value As Short)
            m_Level = Value
        End Set
    End Property
    Private m_Level As Short

    Public Property IsActive() As Boolean
        Get
            Return _isActive
        End Get
        Set(value As Boolean)
            _isActive = value
        End Set
    End Property

    Public Property UserCreate() As String
        Get
            Return m_UserCreate
        End Get
        Private Set(value As String)
            m_UserCreate = Value
        End Set
    End Property
    Private m_UserCreate As String

    Public Property UserUpdate() As String
        Get
            Return m_UserUpdate
        End Get
        Private Set(value As String)
            m_UserUpdate = Value
        End Set
    End Property
    Private m_UserUpdate As String

    Public Property AddFreeDesc() As String
        Get
            Return _AddFreeDesc
        End Get
        Set(value As String)
            _AddFreeDesc = value
        End Set
    End Property

    Public Property AddFreeNo() As String
        Get
            Return _AddFreeNo
        End Get
        Set(value As String)
            _AddFreeNo = value
        End Set
    End Property

    Public Property Branch() As String
        Get
            Return m_Branch
        End Get
        Private Set(value As String)
            m_Branch = Value
        End Set
    End Property
    Private m_Branch As String

    Public Property CoaId() As String
        Get
            Return _coaId
        End Get
        Set(value As String)
            _coaId = value
        End Set
    End Property

    Public ReadOnly Property Coa() As String
        Get
            Return _coaId.Replace(" ", "&nbsp;")
        End Get
    End Property

    Public ReadOnly Property BranchCOA() As String
        Get
            Return [String].Format("{0};{1}", _coaId, Branch)
        End Get
    End Property

    Public Property Description() As String
        Get
            Return _description.TrimEnd()
        End Get
        Set(value As String)
            _description = value
        End Set
    End Property
    Public Property Type() As EnumMasterAccType
        Get
            Return _type
        End Get
        Set(value As EnumMasterAccType)
            _type = value
        End Set
    End Property
    Public Property [Function]() As GlFunctions
        Get
            Return func
        End Get
        Set(value As GlFunctions)
            func = value
        End Set
    End Property

    Public Property Currency() As String
        Get
            Return _currency
        End Get
        Set(value As String)
            _currency = value
        End Set
    End Property
    Public Property IsForeignCurr() As Boolean
        Get
            Return m_IsForeignCurr
        End Get
        Set(value As Boolean)
            m_IsForeignCurr = Value
        End Set
    End Property
    Private m_IsForeignCurr As Boolean
    Public Property ParentCoa() As String
        Get
            Return m_ParentCoa
        End Get
        Set(value As String)
            m_ParentCoa = Value
        End Set
    End Property
    Private m_ParentCoa As String

    Public Property ParentAddFree() As String
        Get
            Return m_ParentAddFree
        End Get
        Set(value As String)
            m_ParentAddFree = value
        End Set
    End Property
    Private m_ParentAddFree As String

    Public Property ParentAddFreeDesc() As String
        Get
            Return m_ParentAddFreeDesc
        End Get
        Set(value As String)
            m_ParentAddFreeDesc = value
        End Set
    End Property
    Private m_ParentAddFreeDesc As String

    Public Property ParentDesc() As String
        Get
            Return m_ParentDesc
        End Get
        Set(value As String)
            m_ParentDesc = Value
        End Set
    End Property
    Private m_ParentDesc As String

    Public ReadOnly Property FullyQualifiedName() As String
        Get
            Return _fullyQualifiedName
        End Get
    End Property

    Public Property LBU() As String
        Get
            Return _LBU
        End Get
        Set(value As String)
            _LBU = value
        End Set
    End Property
    Public Property SIPP() As String
        Get
            Return _SIPP
        End Get
        Set(value As String)
            _SIPP = value
        End Set
    End Property

    Public Sub AssertTaxId()
        If _coaId.Trim().Equals(_coaIdTax.Trim()) Then
            Throw New Exception("COA Pasangan tidak boleh sama dengan COA")
        End If
    End Sub
    Public Function CompareTo(obj As Object) As Integer Implements IComparable.CompareTo
        Return System.[String].CompareOrdinal(_fullyQualifiedName, TryCast(obj, MasterAccountObject)._fullyQualifiedName)
    End Function
End Class
