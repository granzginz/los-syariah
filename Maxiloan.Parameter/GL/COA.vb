
<Serializable()> _
Public Class COA : Inherits Common
    Public Property GLAccountNo As String
    Public Property GLAccountName As String
    Public Property CurrencyId As String
    Public Property AccountType As Integer
    Public Property SubAccount As Boolean
    Public Property ParentAccount As String
    Public Property FirstParentAccount As String
    Public Property isActive As Boolean

End Class

<Serializable()> _
Public Class MasterAcc : Inherits MasterAccBase
    Public Property DtTable As DataTable
    Public Property ListMasterAcc As List(Of MasterAccBase)
End Class
<Serializable()> _
Public Class MasterAccBase : Inherits Common
    Public Property Seq As Long
    Public Property CoAID As String
    Public Property DisplayCoAID As String
    Public Property Description As String
    Public Property TypeId As Integer
    Public Property Type As EnumMasterAccType
    Public Property Currency As String
    Public Property Parent As String
    Public Property IsLeaf As Boolean
    Public Property IsActive As Boolean
    Public Property UserCreate As String
    Public Property UserUpdate As String
    Public Property AddFreeNo As String
    Public Property AddFreeDesc As String
    Public Property Tr_Nomor As String

End Class
