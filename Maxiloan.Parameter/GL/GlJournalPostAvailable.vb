﻿Imports System.Collections.Generic
Imports System.Globalization
Imports System.Linq


<Serializable()> _
Public Class GlJournalPostAvailable
    Inherits BrokenRules

    Private _value As String
    Private _text As String

    Private _dBvalue As Decimal
    Private _cRvalue As Decimal
    Private _pDate As DateTime
    Private trCnt As Int16
    Private isPeriodValid As Boolean = False

    Public Sub New()
    End Sub


    Public Sub SetGlJournalPostAvailable(value As DateTime, text As String, debit As Decimal, credit As Decimal, openCnt As Int16)
        _text = text
        _dBvalue = debit
        _cRvalue = credit
        _pDate = value
        trCnt = openCnt
    End Sub
    Public ReadOnly Property Value() As String
        Get
            Return _pDate.ToString("dd/MM/yyyy")
        End Get
    End Property


    Public ReadOnly Property Text() As String
        Get
            Return _text
        End Get
    End Property
    Public Property PostDate() As DateTime
        Get
            Return _pDate
        End Get
        Set(value As DateTime)
            _pDate = value
        End Set
    End Property

    Public ReadOnly Property Debit() As Decimal
        Get
            Return _dBvalue
        End Get
    End Property

    Public ReadOnly Property Credit() As Decimal
        Get
            Return _cRvalue
        End Get
    End Property

    Private ReadOnly Property isBalance() As Boolean
        Get
            Return (_dBvalue - _cRvalue) = 0
        End Get
    End Property



    Private _schedules As DateTime()

    Public Property SetPostSchedule() As DateTime()
        Get
            Return _schedules
        End Get
        Set(value As DateTime())
            _schedules = value
        End Set
    End Property

    Public Function PostSchedule() As String()
        Return _schedules.[Select](Function(d) d.ToString("dd/MM/yyyy")).ToArray()
    End Function


    Public Property IsNoTrans() As Boolean
        Get
            Return m_IsNoTrans
        End Get
        Set(value As Boolean)
            m_IsNoTrans = value
        End Set
    End Property
    Private m_IsNoTrans As Boolean
    Public ReadOnly Property ShowTrans() As Boolean
        Get
            Return isPeriodValid
        End Get
    End Property

    Private periods As IList(Of GlPeriod)
    Public Sub SetValidateGlPeriod(periods As IList(Of GlPeriod))
        Me.periods = periods
    End Sub

    Protected Overrides Sub validate()

        If periods Is Nothing Then
            addBrokenRule(New BusinessRule(String.Format("Journal Periode tidak ditemukan. GLYear atau GLPeriod belum tersedia, Silahkan Generate dan ulang lagi.")))
            Return
        End If

        If Debit = 0 Then
            addBrokenRule(New BusinessRule(String.Format("Tidak Ada Transaksi.")))
            Return
        End If
        isPeriodValid = True
        If trCnt > 0 Then
            addBrokenRule(New BusinessRule(String.Format("Masih Ada {0} Transaksi yang belum di Audit/Approve.", trCnt)))
        End If

        If Not isBalance Then
            addBrokenRule(New BusinessRule(String.Format("Journal Tidak Balance.")))
        End If
        Dim prd = periods.SingleOrDefault(Function(r) r.IsInRange(_pDate))

        If prd Is Nothing Then
            addBrokenRule(New BusinessRule(String.Format("Journal Periode GL {0} {1} Tidak tergenerate.", _pDate.ToString("MMMM"), _pDate.Year)))
            Return
        End If

        If prd.Status = PeriodStatus.Close Then
            addBrokenRule(New BusinessRule(String.Format("Journal Periode {0} {1} telah di close", _pDate.ToString("MMMM"), _pDate.Year)))
        End If

    End Sub
End Class 
