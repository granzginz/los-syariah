<Serializable()> _
Public Class BalanceSheetAccDetail
    Private _reportGroupId As Integer
    Private _coaId As String
    Private _accountType As GLBalanceSheetAccountType
    Private _coADescription As String
    Private _IsPositif As Integer

    Public Sub New()
    End Sub

    Public Sub New(reportGroupId As Int32, coaId As String, accountTp As GLBalanceSheetAccountType, coaDesc As String, IsPositif As Int32)
        _reportGroupId = reportGroupId
        _coaId = coaId
        _accountType = accountTp
        _coADescription = coaDesc
        _IsPositif = IsPositif
    End Sub

    Public Property ReportGroupId() As Integer
        Get
            Return _reportGroupId
        End Get
        Set(value As Integer)
            _reportGroupId = value
        End Set
    End Property

    Public Property CoaId() As String
        Get
            Return _coaId
        End Get
        Set(value As String)
            _coaId = value
        End Set
    End Property

    Public Property CoaDescription() As String
        Get
            Return _coADescription
        End Get
        Set(value As String)
            _coADescription = value
        End Set
    End Property
    Public Property IsPositif() As Integer
        Get
            Return _IsPositif
        End Get
        Set(value As Integer)
            _IsPositif = value
        End Set
    End Property

    Public Property accType() As GLBalanceSheetAccountType
        Get
            Return _accountType
        End Get
        Set(value As GLBalanceSheetAccountType)
            _accountType = value
        End Set
    End Property

End Class
