﻿<Serializable()>
Public Class DataJurnalCrossPeriode : Inherits Common
	Private _TglPeriode1 As Date
    Private _TglPeriode2 As Date
    Private _PeriodMonth As String
    Private _TglJournal As Date
    Private _strError As String
    Private _TR_DATE As Date

    Private _ListData As DataTable
	Private _ListDataReport As DataSet
	Private _TotalRecords As Int64
    Private _Table As String
    Private _Error As String

    Public Property TglPeriode1 As Date
		Get
			Return _TglPeriode1
		End Get
		Set(value As Date)
			_TglPeriode1 = value
		End Set
	End Property

    Public Property TglPeriode2 As Date
        Get
            Return _TglPeriode2
        End Get
        Set(value As Date)
            _TglPeriode2 = value
        End Set
    End Property

    Public Property PeriodMonth As String
        Get
            Return _PeriodMonth
        End Get
        Set(value As String)
            PeriodMonth = value
        End Set
    End Property

    Public Property strError() As String
        Get
            Return CType(_strError, String)
        End Get
        Set(ByVal Value As String)
            _strError = Value
        End Set
    End Property

    Public Property TglJournal As Date
        Get
            Return _TglJournal
        End Get
        Set(value As Date)
            _TglJournal = value
        End Set
    End Property

    Public Property TR_DATE As Date
        Get
            Return _TR_DATE
        End Get
        Set(value As Date)
            TR_DATE = value
        End Set
    End Property

    Public Property ListData As DataTable
		Get
			Return _ListData
		End Get
		Set(value As DataTable)
			_ListData = value
		End Set
	End Property

	Public Property ListDataReport As DataSet
		Get
			Return _ListDataReport
		End Get
		Set(value As DataSet)
			_ListDataReport = value
		End Set
	End Property

	Public Property TotalRecords As Long
		Get
			Return _TotalRecords
		End Get
		Set(value As Long)
			_TotalRecords = value
		End Set
	End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property
    Public Property Err() As String
        Get
            Return _Error
        End Get
        Set(ByVal Value As String)
            _Error = Value
        End Set
    End Property
End Class
