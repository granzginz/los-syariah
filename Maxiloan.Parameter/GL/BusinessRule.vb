﻿<Serializable()> _
Public Class BusinessRule
    Private ReadOnly m_ruleDescription As String

    Public Sub New(ruleDescription As String)
        m_ruleDescription = ruleDescription
    End Sub

    Public ReadOnly Property RuleDescription() As String
        Get
            Return m_ruleDescription
        End Get
    End Property
End Class

