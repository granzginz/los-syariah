<Serializable()> _
Public Class BalanceSheetSetup
    Private _reportId As String
    Private _reportTitle As String
    Private _reportText1 As String
    Private _reportText2 As String
    Private _printLastMonth As Boolean
    Private _printVariance As Boolean

    Public Sub New()
    End Sub

    Public Sub New(reportId As String, reportTitle As String, reportText1 As String, reportText2 As String, printLastMonth As Boolean, printVariance As Boolean, _
     compId As String)
        CompanyId = compId
        _reportId = reportId
        _reportTitle = reportTitle
        _reportText1 = reportText1
        _reportText2 = reportText2
        _printLastMonth = printLastMonth
        _printVariance = printVariance
    End Sub

    Public Sub New(reportId As String, reportTitle As String, reportText1 As String, reportText2 As String, isPrintLastMonth As Boolean, isPrintVariance As Boolean, _
     compId As String, userCr As String, userUp As String)
        Me.New(reportId, reportTitle, reportText1, reportText2, isPrintLastMonth, isPrintVariance, _
         compId)
        UserCreate = userCr
        UserUpdate = userUp
    End Sub

    Public Property CompanyId() As [String]
        Get
            Return m_CompanyId
        End Get
        Set(value As [String])
            m_CompanyId = value
        End Set
    End Property
    Private m_CompanyId As [String]

    Public Property IsPrintLastMonth() As Boolean
        Get
            Return _printLastMonth
        End Get
        Set(value As Boolean)
            _printLastMonth = value
        End Set
    End Property

    Public Property IsPrintVariance() As Boolean
        Get
            Return _printVariance
        End Get
        Set(value As Boolean)
            _printVariance = value
        End Set
    End Property

    Public Property UserCreate() As String
        Get
            Return m_UserCreate
        End Get
        Private Set(value As String)
            m_UserCreate = value
        End Set
    End Property
    Private m_UserCreate As String

    Public Property UserUpdate() As String
        Get
            Return m_UserUpdate
        End Get
        Private Set(value As String)
            m_UserUpdate = value
        End Set
    End Property
    Private m_UserUpdate As String

    Public Property ReportId() As String
        Get
            Return _reportId
        End Get
        Set(value As String)
            _reportId = value
        End Set
    End Property

    Public Property ReportTitle() As String
        Get
            Return _reportTitle.TrimEnd()
        End Get
        Set(value As String)
            _reportTitle = value
        End Set
    End Property

    Public Property ReportText1() As String
        Get
            Return _reportText1.TrimEnd()
        End Get
        Set(value As String)
            _reportText1 = value
        End Set
    End Property

    Public Property ReportText2() As String
        Get
            Return _reportText2.TrimEnd()
        End Get
        Set(value As String)
            _reportText2 = value
        End Set
    End Property


End Class
