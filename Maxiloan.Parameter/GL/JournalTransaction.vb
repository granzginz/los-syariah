﻿<Serializable()> _
Public Class JournalTransaction : Inherits AccMntBase
    Private Property _TransactionNo As String
    Private Property _TransactionType As String

    Public Property TransactionNo As String
        Get
            Return _TransactionNo
        End Get
        Set(value As String)
            _TransactionNo = value
        End Set
    End Property
    Public Property TransactionType As String
        Get
            Return _TransactionType
        End Get
        Set(value As String)
            _TransactionType = value
        End Set
    End Property
End Class
