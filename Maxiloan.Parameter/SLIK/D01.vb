﻿<Serializable()>
Public Class D01 : Inherits Common
    Private _ID As Int64
    Private _BulanData As String
    Private _CIF As String
    Private _NIK As String
    Private _NamaIdentitas As String
    Private _NamaLengkap As String
    Private _Alamat As String
    Private _Flag As String
    Private _JenisIdentitas As String
    Private _Gelar As String
    Private _JenisKelamin As String
    Private _TempatLahir As String
    Private _TanggalLahir As String
    Private _NPWP As String
    Private _Kelurahan As String
    Private _Kecamatan As String
    Private _Kota As String
    Private _KodePos As String
    Private _Telp As String
    Private _Hp As String
    Private _Email As String
    Private _KodeNegara As String
    Private _KodePekerjaan As String
    Private _TempatBekerja As String
    Private _KodeBidangUsaha As String
    Private _AlamatTempatBekerja As String
    Private _PenghasilanPertahun As String
    Private _KodeSumberPenghasilan As String
    Private _JumlahTanggungan As String
    Private _KodeHubunganLJK As String
    Private _KodeGolonganDebitur As String
    Private _StatusPerkawinanDebitur As String
    Private _NikPaspor As String
    Private _NamaPasangan As String
    Private _TanggalLahirPasangan As String
    Private _PerjanjianPisahHarta As String
    Private _MelanggarBMPK As String
    Private _MelampauiBMPK As String
    Private _NamaGadisIbuKandung As String
    Private _KodeKantorCabang As String
    Private _OperasiData As String
    Private _AB As String

    Private _ListData As DataTable
    Private _ListDataReport As DataSet
    Private _TotalRecords As Int64
    Private _Table As String

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property BulanData As String
        Get
            Return _BulanData
        End Get
        Set(value As String)
            _BulanData = value
        End Set
    End Property

    Public Property CIF As String
        Get
            Return _CIF
        End Get
        Set(value As String)
            _CIF = value
        End Set
    End Property

    Public Property NIK As String
        Get
            Return _NIK
        End Get
        Set(value As String)
            _NIK = value
        End Set
    End Property

    Public Property NamaIdentitas As String
        Get
            Return _NamaIdentitas
        End Get
        Set(value As String)
            _NamaIdentitas = value
        End Set
    End Property

    Public Property NamaLengkap As String
        Get
            Return _NamaLengkap
        End Get
        Set(value As String)
            _NamaLengkap = value
        End Set
    End Property

    Public Property Alamat As String
        Get
            Return _Alamat
        End Get
        Set(value As String)
            _Alamat = value
        End Set
    End Property

    Public Property Flag As String
        Get
            Return _Flag
        End Get
        Set(value As String)
            _Flag = value
        End Set
    End Property

    Public Property JenisIdentitas As String
        Get
            Return _JenisIdentitas
        End Get
        Set(value As String)
            _JenisIdentitas = value
        End Set
    End Property

    Public Property Gelar As String
        Get
            Return _Gelar
        End Get
        Set(value As String)
            _Gelar = value
        End Set
    End Property

    Public Property JenisKelamin As String
        Get
            Return _JenisKelamin
        End Get
        Set(value As String)
            _JenisKelamin = value
        End Set
    End Property

    Public Property TempatLahir As String
        Get
            Return _TempatLahir
        End Get
        Set(value As String)
            _TempatLahir = value
        End Set
    End Property

    Public Property TanggalLahir As String
        Get
            Return _TanggalLahir
        End Get
        Set(value As String)
            _TanggalLahir = value
        End Set
    End Property

    Public Property NPWP As String
        Get
            Return _NPWP
        End Get
        Set(value As String)
            _NPWP = value
        End Set
    End Property

    Public Property Kelurahan As String
        Get
            Return _Kelurahan
        End Get
        Set(value As String)
            _Kelurahan = value
        End Set
    End Property

    Public Property Kecamatan As String
        Get
            Return _Kecamatan
        End Get
        Set(value As String)
            _Kecamatan = value
        End Set
    End Property

    Public Property Kota As String
        Get
            Return _Kota
        End Get
        Set(value As String)
            _Kota = value
        End Set
    End Property

    Public Property KodePos As String
        Get
            Return _KodePos
        End Get
        Set(value As String)
            _KodePos = value
        End Set
    End Property

    Public Property Telp As String
        Get
            Return _Telp
        End Get
        Set(value As String)
            _Telp = value
        End Set
    End Property

    Public Property Hp As String
        Get
            Return _Hp
        End Get
        Set(value As String)
            _Hp = value
        End Set
    End Property

    Public Property Email As String
        Get
            Return _Email
        End Get
        Set(value As String)
            _Email = value
        End Set
    End Property

    Public Property KodeNegara As String
        Get
            Return _KodeNegara
        End Get
        Set(value As String)
            _KodeNegara = value
        End Set
    End Property

    Public Property KodePekerjaan As String
        Get
            Return _KodePekerjaan
        End Get
        Set(value As String)
            _KodePekerjaan = value
        End Set
    End Property

    Public Property TempatBekerja As String
        Get
            Return _TempatBekerja
        End Get
        Set(value As String)
            _TempatBekerja = value
        End Set
    End Property

    Public Property KodeBidangUsaha As String
        Get
            Return _KodeBidangUsaha
        End Get
        Set(value As String)
            _KodeBidangUsaha = value
        End Set
    End Property

    Public Property AlamatTempatBekerja As String
        Get
            Return _AlamatTempatBekerja
        End Get
        Set(value As String)
            _AlamatTempatBekerja = value
        End Set
    End Property

    Public Property PenghasilanPertahun As String
        Get
            Return _PenghasilanPertahun
        End Get
        Set(value As String)
            _PenghasilanPertahun = value
        End Set
    End Property

    Public Property KodeSumberPenghasilan As String
        Get
            Return _KodeSumberPenghasilan
        End Get
        Set(value As String)
            _KodeSumberPenghasilan = value
        End Set
    End Property

    Public Property JumlahTanggungan As String
        Get
            Return _JumlahTanggungan
        End Get
        Set(value As String)
            _JumlahTanggungan = value
        End Set
    End Property

    Public Property KodeHubunganLJK As String
        Get
            Return _KodeHubunganLJK
        End Get
        Set(value As String)
            _KodeHubunganLJK = value
        End Set
    End Property

    Public Property KodeGolonganDebitur As String
        Get
            Return _KodeGolonganDebitur
        End Get
        Set(value As String)
            _KodeGolonganDebitur = value
        End Set
    End Property

    Public Property StatusPerkawinanDebitur As String
        Get
            Return _StatusPerkawinanDebitur
        End Get
        Set(value As String)
            _StatusPerkawinanDebitur = value
        End Set
    End Property

    Public Property NikPaspor As String
        Get
            Return _NikPaspor
        End Get
        Set(value As String)
            _NikPaspor = value
        End Set
    End Property

    Public Property NamaPasangan As String
        Get
            Return _NamaPasangan
        End Get
        Set(value As String)
            _NamaPasangan = value
        End Set
    End Property

    Public Property TanggalLahirPasangan As String
        Get
            Return _TanggalLahirPasangan
        End Get
        Set(value As String)
            _TanggalLahirPasangan = value
        End Set
    End Property

    Public Property PerjanjianPisahHarta As String
        Get
            Return _PerjanjianPisahHarta
        End Get
        Set(value As String)
            _PerjanjianPisahHarta = value
        End Set
    End Property

    Public Property MelanggarBMPK As String
        Get
            Return _MelanggarBMPK
        End Get
        Set(value As String)
            _MelanggarBMPK = value
        End Set
    End Property

    Public Property MelampauiBMPK As String
        Get
            Return _MelampauiBMPK
        End Get
        Set(value As String)
            _MelampauiBMPK = value
        End Set
    End Property

    Public Property NamaGadisIbuKandung As String
        Get
            Return _NamaGadisIbuKandung
        End Get
        Set(value As String)
            _NamaGadisIbuKandung = value
        End Set
    End Property

    Public Property KodeKantorCabang As String
        Get
            Return _KodeKantorCabang
        End Get
        Set(value As String)
            _KodeKantorCabang = value
        End Set
    End Property

    Public Property OperasiData As String
        Get
            Return _OperasiData
        End Get
        Set(value As String)
            _OperasiData = value
        End Set
    End Property

    Public Property AB As String
        Get
            Return _AB
        End Get
        Set(value As String)
            _AB = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property

    Public Property ListDataReport As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(value As DataSet)
            _ListDataReport = value
        End Set
    End Property

    Public Property TotalRecords As Long
        Get
            Return _TotalRecords
        End Get
        Set(value As Long)
            _TotalRecords = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property
End Class
