﻿<Serializable()>
Public Class F01 : Inherits Common
    Private _ID As Int64
    Private _BulanData As String
    Private _Nomor_Rekening_Fasilitas As String
    Private _Nomor_CIF_Debitur As String
    Private _Kode_Sifat_Kredit_Pembiayaan As String
    Private _Kode_Jenis_Kredit_Pembiayaan As String
    Private _Kode_Skim_Akad_Pembiayaan As String
    Private _Nomor_Akad_Awal As String
    Private _Tanggal_Akad_Awal As String
    Private _Nomor_Akad_Akhir As String
    Private _Tanggal_Akad_Akhir As String
    Private _Baru_Perpanjangan As String
    Private _Tanggal_Awal_Kredit As String
    Private _Tanggal_Mulai As String
    Private _Tanggal_Jatuh_Tempo As String
    Private _Kode_Kategori_Debitur As String
    Private _Kode_Jenis_Penggunaan As String
    Private _Kode_Orientasi_Penggunaan As String
    Private _Kode_Sektor_Ekonomi As String
    Private _Kode_Dati_Kota As String
    Private _Nilai_Proyek As String
    Private _Kode_Valuta As String
    Private _Persentase_Suku_Bunga_Imbalan As String
    Private _Jenis_Suku_Bunga_Imbalan As String
    Private _Kredit_Program_Pemerintah As String
    Private _Takeover_Dari As String
    Private _Sumber_Dana As String
    Private _Plafon_Awal As String
    Private _Plafon As String
    Private _Realisasi_Pencairan_Bulan_Berjalan As String
    Private _Denda As String
    Private _Baki_Debet As String
    Private _Nilai_Dalam_Mata_Uang_Asal As String
    Private _Kode_Kolektibilitas As String
    Private _Tanggal_Macet As String
    Private _Kode_Sebab_Macet As String
    Private _Tunggakan_Pokok As String
    Private _Tunggakan_Bunga As String
    Private _Jumlah_Hari_Tunggakan As String
    Private _Frekuensi_Tunggakan As String
    Private _Frekuensi_Restrukturisasi As String
    Private _Tanggal_Restrukturisasi_Awal As String
    Private _Tanggal_Restrukturisasi_Akhir As String
    Private _Kode_Cara_Restrukturisasi As String
    Private _Kode_Kondisi As String
    Private _Tanggal_Kondisi As String
    Private _Keterangan As String
    Private _Kode_Kantor_Cabang As String
    Private _Operasi_Data As String


    Private _ListData As DataTable
    Private _ListDataReport As DataSet
    Private _TotalRecords As Int64
    Private _Table As String

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property BulanData As String
        Get
            Return _BulanData
        End Get
        Set(value As String)
            _BulanData = value
        End Set
    End Property

    Public Property Nomor_CIF_Debitur As String
        Get
            Return _Nomor_CIF_Debitur
        End Get
        Set(value As String)
            _Nomor_CIF_Debitur = value
        End Set
    End Property

    Public Property Kode_Sifat_Kredit_Pembiayaan As String
        Get
            Return _Kode_Sifat_Kredit_Pembiayaan
        End Get
        Set(value As String)
            _Kode_Sifat_Kredit_Pembiayaan = value
        End Set
    End Property

    Public Property Kode_Jenis_Kredit_Pembiayaan As String
        Get
            Return _Kode_Jenis_Kredit_Pembiayaan
        End Get
        Set(value As String)
            _Kode_Jenis_Kredit_Pembiayaan = value
        End Set
    End Property

    Public Property Kode_Skim_Akad_Pembiayaan As String
        Get
            Return _Kode_Skim_Akad_Pembiayaan
        End Get
        Set(value As String)
            _Kode_Skim_Akad_Pembiayaan = value
        End Set
    End Property

    Public Property Nomor_Akad_Awal As String
        Get
            Return _Nomor_Akad_Awal
        End Get
        Set(value As String)
            _Nomor_Akad_Awal = value
        End Set
    End Property

    Public Property Tanggal_Akad_Awal As String
        Get
            Return _Tanggal_Akad_Awal
        End Get
        Set(value As String)
            _Tanggal_Akad_Awal = value
        End Set
    End Property

    Public Property Nomor_Akad_Akhir As String
        Get
            Return _Nomor_Akad_Akhir
        End Get
        Set(value As String)
            _Nomor_Akad_Akhir = value
        End Set
    End Property

    Public Property Tanggal_Akad_Akhir As String
        Get
            Return _Tanggal_Akad_Akhir
        End Get
        Set(value As String)
            _Tanggal_Akad_Akhir = value
        End Set
    End Property

    Public Property Baru_Perpanjangan As String
        Get
            Return _Baru_Perpanjangan
        End Get
        Set(value As String)
            _Baru_Perpanjangan = value
        End Set
    End Property

    Public Property Tanggal_Awal_Kredit As String
        Get
            Return _Tanggal_Awal_Kredit
        End Get
        Set(value As String)
            _Tanggal_Awal_Kredit = value
        End Set
    End Property

    Public Property Tanggal_Mulai As String
        Get
            Return _Tanggal_Mulai
        End Get
        Set(value As String)
            _Tanggal_Mulai = value
        End Set
    End Property

    Public Property Tanggal_Jatuh_Tempo As String
        Get
            Return _Tanggal_Jatuh_Tempo
        End Get
        Set(value As String)
            _Tanggal_Jatuh_Tempo = value
        End Set
    End Property

    Public Property Kode_Kategori_Debitur As String
        Get
            Return _Kode_Kategori_Debitur
        End Get
        Set(value As String)
            _Kode_Kategori_Debitur = value
        End Set
    End Property

    Public Property Kode_Jenis_Penggunaan As String
        Get
            Return _Kode_Jenis_Penggunaan
        End Get
        Set(value As String)
            _Kode_Jenis_Penggunaan = value
        End Set
    End Property

    Public Property Kode_Orientasi_Penggunaan As String
        Get
            Return _Kode_Orientasi_Penggunaan
        End Get
        Set(value As String)
            _Kode_Orientasi_Penggunaan = value
        End Set
    End Property

    Public Property Kode_Sektor_Ekonomi As String
        Get
            Return _Kode_Sektor_Ekonomi
        End Get
        Set(value As String)
            _Kode_Sektor_Ekonomi = value
        End Set
    End Property

    Public Property Kode_Dati_Kota As String
        Get
            Return _Kode_Dati_Kota
        End Get
        Set(value As String)
            _Kode_Dati_Kota = value
        End Set
    End Property

    Public Property Nilai_Proyek As String
        Get
            Return _Nilai_Proyek
        End Get
        Set(value As String)
            _Nilai_Proyek = value
        End Set
    End Property

    Public Property Kode_Valuta As String
        Get
            Return _Kode_Valuta
        End Get
        Set(value As String)
            _Kode_Valuta = value
        End Set
    End Property

    Public Property Persentase_Suku_Bunga_Imbalan As String
        Get
            Return _Persentase_Suku_Bunga_Imbalan
        End Get
        Set(value As String)
            _Persentase_Suku_Bunga_Imbalan = value
        End Set
    End Property

    Public Property Jenis_Suku_Bunga_Imbalan As String
        Get
            Return _Jenis_Suku_Bunga_Imbalan
        End Get
        Set(value As String)
            _Jenis_Suku_Bunga_Imbalan = value
        End Set
    End Property

    Public Property Kredit_Program_Pemerintah As String
        Get
            Return _Kredit_Program_Pemerintah
        End Get
        Set(value As String)
            _Kredit_Program_Pemerintah = value
        End Set
    End Property

    Public Property Takeover_Dari As String
        Get
            Return _Takeover_Dari
        End Get
        Set(value As String)
            _Takeover_Dari = value
        End Set
    End Property

    Public Property Sumber_Dana As String
        Get
            Return _Sumber_Dana
        End Get
        Set(value As String)
            _Sumber_Dana = value
        End Set
    End Property

    Public Property Plafon_Awal As String
        Get
            Return _Plafon_Awal
        End Get
        Set(value As String)
            _Plafon_Awal = value
        End Set
    End Property

    Public Property Plafon As String
        Get
            Return _Plafon
        End Get
        Set(value As String)
            _Plafon = value
        End Set
    End Property

    Public Property Realisasi_Pencairan_Bulan_Berjalan As String
        Get
            Return _Realisasi_Pencairan_Bulan_Berjalan
        End Get
        Set(value As String)
            _Realisasi_Pencairan_Bulan_Berjalan = value
        End Set
    End Property

    Public Property Denda As String
        Get
            Return _Denda
        End Get
        Set(value As String)
            _Denda = value
        End Set
    End Property

    Public Property Baki_Debet As String
        Get
            Return _Baki_Debet
        End Get
        Set(value As String)
            _Baki_Debet = value
        End Set
    End Property

    Public Property Nilai_Dalam_Mata_Uang_Asal As String
        Get
            Return _Nilai_Dalam_Mata_Uang_Asal
        End Get
        Set(value As String)
            _Nilai_Dalam_Mata_Uang_Asal = value
        End Set
    End Property

    Public Property Kode_Kolektibilitas As String
        Get
            Return _Kode_Kolektibilitas
        End Get
        Set(value As String)
            _Kode_Kolektibilitas = value
        End Set
    End Property

    Public Property Tanggal_Macet As String
        Get
            Return _Tanggal_Macet
        End Get
        Set(value As String)
            _Tanggal_Macet = value
        End Set
    End Property

    Public Property Kode_Sebab_Macet As String
        Get
            Return _Kode_Sebab_Macet
        End Get
        Set(value As String)
            _Kode_Sebab_Macet = value
        End Set
    End Property

    Public Property Tunggakan_Pokok As String
        Get
            Return _Tunggakan_Pokok
        End Get
        Set(value As String)
            _Tunggakan_Pokok = value
        End Set
    End Property

    Public Property Tunggakan_Bunga As String
        Get
            Return _Tunggakan_Bunga
        End Get
        Set(value As String)
            _Tunggakan_Bunga = value
        End Set
    End Property

    Public Property Jumlah_Hari_Tunggakan As String
        Get
            Return _Jumlah_Hari_Tunggakan
        End Get
        Set(value As String)
            _Jumlah_Hari_Tunggakan = value
        End Set
    End Property

    Public Property Frekuensi_Tunggakan As String
        Get
            Return _Frekuensi_Tunggakan
        End Get
        Set(value As String)
            _Frekuensi_Tunggakan = value
        End Set
    End Property

    Public Property Frekuensi_Restrukturisasi As String
        Get
            Return _Frekuensi_Restrukturisasi
        End Get
        Set(value As String)
            _Frekuensi_Restrukturisasi = value
        End Set
    End Property

    Public Property Tanggal_Restrukturisasi_Awal As String
        Get
            Return _Tanggal_Restrukturisasi_Awal
        End Get
        Set(value As String)
            _Tanggal_Restrukturisasi_Awal = value
        End Set
    End Property

    Public Property Tanggal_Restrukturisasi_Akhir As String
        Get
            Return _Tanggal_Restrukturisasi_Akhir
        End Get
        Set(value As String)
            _Tanggal_Restrukturisasi_Akhir = value
        End Set
    End Property

    Public Property Kode_Cara_Restrukturisasi As String
        Get
            Return _Kode_Cara_Restrukturisasi
        End Get
        Set(value As String)
            _Kode_Cara_Restrukturisasi = value
        End Set
    End Property

    Public Property Kode_Kondisi As String
        Get
            Return _Kode_Kondisi
        End Get
        Set(value As String)
            _Kode_Kondisi = value
        End Set
    End Property

    Public Property Tanggal_Kondisi As String
        Get
            Return _Tanggal_Kondisi
        End Get
        Set(value As String)
            _Tanggal_Kondisi = value
        End Set
    End Property

    Public Property Keterangan As String
        Get
            Return _Keterangan
        End Get
        Set(value As String)
            _Keterangan = value
        End Set
    End Property

    Public Property Kode_Kantor_Cabang As String
        Get
            Return _Kode_Kantor_Cabang
        End Get
        Set(value As String)
            _Kode_Kantor_Cabang = value
        End Set
    End Property

    Public Property Operasi_Data As String
        Get
            Return _Operasi_Data
        End Get
        Set(value As String)
            _Operasi_Data = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property

    Public Property ListDataReport As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(value As DataSet)
            _ListDataReport = value
        End Set
    End Property

    Public Property TotalRecords As Long
        Get
            Return _TotalRecords
        End Get
        Set(value As Long)
            _TotalRecords = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property

    Public Property Nomor_Rekening_Fasilitas As String
        Get
            Return _Nomor_Rekening_Fasilitas
        End Get
        Set(value As String)
            _Nomor_Rekening_Fasilitas = value
        End Set
    End Property
End Class
