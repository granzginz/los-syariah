﻿<Serializable()>
Public Class D02 : Inherits Common
    Private _ID As Int64
    Private _BulanData As String
    Private _FlagDetail As String
    Private _CIF As String
    Private _NoIdentitasBadanUsaha As String
    Private _NamaBadanUsaha As String
    Private _KodeJenisBadanUsaha As String
    Private _TempatPendirian As String
    Private _NoAktePendirian As String
    Private _TanggalAktePendirian As String
    Private _NoAktePerubahanTerakhir As String
    Private _TanggalAktePerubahanTerakhir As String
    Private _Telepon As String
    Private _Hp As String
    Private _AlamatEmail As String
    Private _Alamat As String
    Private _Kelurahan As String
    Private _Kecamatan As String
    Private _KodeSandiKab As String
    Private _KodePos As String
    Private _KodeNegaraDomisili As String
    Private _KodeBidangUsaha As String
    Private _KodeHubungandenganLJK As String
    Private _MelanggarBMPK As String
    Private _MelampauiBMPK As String
    Private _GoPublic As String
    Private _KodeGolonganDebitur As String
    Private _Peringkat As String
    Private _LembagaPemeringkat As String
    Private _TanggalPemeringkatan As String
    Private _NamaGroupDebitur As String
    Private _KodeKantorCabang As String
    Private _OperasiData As String


    Private _DatiID As String
    Private _NamaKabKot As String

    Private _ListData As DataTable
    Private _ListDataReport As DataSet
    Private _TotalRecords As Int64
    Private _Table As String

    Public Property FlagDetail As String
        Get
            Return _FlagDetail
        End Get
        Set(value As String)
            _FlagDetail = value
        End Set
    End Property

    Public Property CIF As String
        Get
            Return _CIF
        End Get
        Set(value As String)
            _CIF = value
        End Set
    End Property

    Public Property NoIdentitasBadanUsaha As String
        Get
            Return _NoIdentitasBadanUsaha
        End Get
        Set(value As String)
            _NoIdentitasBadanUsaha = value
        End Set
    End Property

    Public Property NamaBadanUsaha As String
        Get
            Return _NamaBadanUsaha
        End Get
        Set(value As String)
            _NamaBadanUsaha = value
        End Set
    End Property

    Public Property KodeJenisBadanUsaha As String
        Get
            Return _KodeJenisBadanUsaha
        End Get
        Set(value As String)
            _KodeJenisBadanUsaha = value
        End Set
    End Property

    Public Property TempatPendirian As String
        Get
            Return _TempatPendirian
        End Get
        Set(value As String)
            _TempatPendirian = value
        End Set
    End Property

    Public Property NoAktePendirian As String
        Get
            Return _NoAktePendirian
        End Get
        Set(value As String)
            _NoAktePendirian = value
        End Set
    End Property

    Public Property TanggalAktePendirian As String
        Get
            Return _TanggalAktePendirian
        End Get
        Set(value As String)
            _TanggalAktePendirian = value
        End Set
    End Property

    Public Property NoAktePerubahanTerakhir As String
        Get
            Return _NoAktePerubahanTerakhir
        End Get
        Set(value As String)
            _NoAktePerubahanTerakhir = value
        End Set
    End Property

    Public Property TanggalAktePerubahanTerakhir As String
        Get
            Return _TanggalAktePerubahanTerakhir
        End Get
        Set(value As String)
            _TanggalAktePerubahanTerakhir = value
        End Set
    End Property

    Public Property Telepon As String
        Get
            Return _Telepon
        End Get
        Set(value As String)
            _Telepon = value
        End Set
    End Property

    Public Property Hp As String
        Get
            Return _Hp
        End Get
        Set(value As String)
            _Hp = value
        End Set
    End Property

    Public Property AlamatEmail As String
        Get
            Return _AlamatEmail
        End Get
        Set(value As String)
            _AlamatEmail = value
        End Set
    End Property

    Public Property Alamat As String
        Get
            Return _Alamat
        End Get
        Set(value As String)
            _Alamat = value
        End Set
    End Property

    Public Property Kelurahan As String
        Get
            Return _Kelurahan
        End Get
        Set(value As String)
            _Kelurahan = value
        End Set
    End Property

    Public Property Kecamatan As String
        Get
            Return _Kecamatan
        End Get
        Set(value As String)
            _Kecamatan = value
        End Set
    End Property

    Public Property KodeSandiKab As String
        Get
            Return _KodeSandiKab
        End Get
        Set(value As String)
            _KodeSandiKab = value
        End Set
    End Property

    Public Property KodePos As String
        Get
            Return _KodePos
        End Get
        Set(value As String)
            _KodePos = value
        End Set
    End Property

    Public Property KodeNegaraDomisili As String
        Get
            Return _KodeNegaraDomisili
        End Get
        Set(value As String)
            _KodeNegaraDomisili = value
        End Set
    End Property

    Public Property KodeBidangUsaha As String
        Get
            Return _KodeBidangUsaha
        End Get
        Set(value As String)
            _KodeBidangUsaha = value
        End Set
    End Property

    Public Property KodeHubungandenganLJK As String
        Get
            Return _KodeHubungandenganLJK
        End Get
        Set(value As String)
            _KodeHubungandenganLJK = value
        End Set
    End Property

    Public Property MelanggarBMPK As String
        Get
            Return _MelanggarBMPK
        End Get
        Set(value As String)
            _MelanggarBMPK = value
        End Set
    End Property

    Public Property MelampauiBMPK As String
        Get
            Return _MelampauiBMPK
        End Get
        Set(value As String)
            _MelampauiBMPK = value
        End Set
    End Property

    Public Property GoPublic As String
        Get
            Return _GoPublic
        End Get
        Set(value As String)
            _GoPublic = value
        End Set
    End Property

    Public Property KodeGolonganDebitur As String
        Get
            Return _KodeGolonganDebitur
        End Get
        Set(value As String)
            _KodeGolonganDebitur = value
        End Set
    End Property

    Public Property Peringkat As String
        Get
            Return _Peringkat
        End Get
        Set(value As String)
            _Peringkat = value
        End Set
    End Property

    Public Property LembagaPemeringkat As String
        Get
            Return _LembagaPemeringkat
        End Get
        Set(value As String)
            _LembagaPemeringkat = value
        End Set
    End Property

    Public Property TanggalPemeringkatan As String
        Get
            Return _TanggalPemeringkatan
        End Get
        Set(value As String)
            _TanggalPemeringkatan = value
        End Set
    End Property

    Public Property NamaGroupDebitur As String
        Get
            Return _NamaGroupDebitur
        End Get
        Set(value As String)
            _NamaGroupDebitur = value
        End Set
    End Property

    Public Property KodeKantorCabang As String
        Get
            Return _KodeKantorCabang
        End Get
        Set(value As String)
            _KodeKantorCabang = value
        End Set
    End Property

    Public Property OperasiData As String
        Get
            Return _OperasiData
        End Get
        Set(value As String)
            _OperasiData = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property

    Public Property ListDataReport As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(value As DataSet)
            _ListDataReport = value
        End Set
    End Property

    Public Property TotalRecords As Int64
        Get
            Return _TotalRecords
        End Get
        Set(value As Int64)
            _TotalRecords = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property BulanData As String
        Get
            Return _BulanData
        End Get
        Set(value As String)
            _BulanData = value
        End Set
    End Property

    Public Property DatiID As String
        Get
            Return _DatiID
        End Get
        Set(value As String)
            _DatiID = value
        End Set
    End Property

    Public Property NamaKabKot As String
        Get
            Return _NamaKabKot
        End Get
        Set(value As String)
            _NamaKabKot = value
        End Set
    End Property
End Class
