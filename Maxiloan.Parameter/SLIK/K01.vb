﻿<Serializable()>
Public Class K01 : Inherits Common
	Private _ID As String
	Private _BulanData As String
	Private _FlagDetail As String
	Private _CustomerId As String
	Private _TglLaporanKeuangan As String
	Private _AssetIDR As String
	Private _AssetLancar As String
	Private _KasSetaraKasAsetLancar As String
	Private _PiutangUsahAsetLancar As String
	Private _InvestasiLainnyaAsetLancar As String
	Private _AsetLancarLainnya As String
	Private _AsetTidakLancar As String
	Private _PiutangUsahaAsetTidakLancar As String
	Private _InvestasiLainnyaAsetTidakLancar As String
	Private _AsetTidakLancarLainnya As String
	Private _Liabilitas As String
	Private _LiabilitasJangkaPendek As String
	Private _PinjamanJangkaPendek As String
	Private _UtangUsahaJangkaPendek As String
	Private _LiabilitasJangkaPendekLainnya As String
	Private _LiabilitasJangkaPanjang As String
	Private _PinjamanJangkaPanjang As String
	Private _UtangUsahaJangkaPanjang As String
	Private _LiabilitasJangkaPanjangLainnya As String
	Private _Ekuitas As String
	Private _PendapatanUsahaOpr As String
	Private _BebanPokokPendapatanOpr As String
	Private _LabaRugiBruto As String
	Private _PLLNonOpr As String
	Private _BebanLainLainNonOpr As String
	Private _LabaRugiSebelumPajak As String
	Private _LabaRugiTahunBerjalan As String
	Private _KodeKantorCabang As String
	Private _OperasiData As String

	Private _ListData As DataTable
	Private _ListDataReport As DataSet
	Private _TotalRecords As Int64
	Private _Table As String

	Public Property ID As String
		Get
			Return _ID
		End Get
		Set(value As String)
			_ID = value
		End Set
	End Property

	Public Property FlagDetail As String
		Get
			Return _FlagDetail
		End Get
		Set(value As String)
			_FlagDetail = value
		End Set
	End Property

	Public Property CustomerId As String
		Get
			Return _CustomerId
		End Get
		Set(value As String)
			_CustomerId = value
		End Set
	End Property

	Public Property TglLaporanKeuangan As String
		Get
			Return _TglLaporanKeuangan
		End Get
		Set(value As String)
			_TglLaporanKeuangan = value
		End Set
	End Property

	Public Property AssetIDR As String
		Get
			Return _AssetIDR
		End Get
		Set(value As String)
			_AssetIDR = value
		End Set
	End Property

	Public Property AssetLancar As String
		Get
			Return _AssetLancar
		End Get
		Set(value As String)
			_AssetLancar = value
		End Set
	End Property

	Public Property KasSetaraKasAsetLancar As String
		Get
			Return _KasSetaraKasAsetLancar
		End Get
		Set(value As String)
			_KasSetaraKasAsetLancar = value
		End Set
	End Property

	Public Property PiutangUsahAsetLancar As String
		Get
			Return _PiutangUsahAsetLancar
		End Get
		Set(value As String)
			_PiutangUsahAsetLancar = value
		End Set
	End Property

	Public Property InvestasiLainnyaAsetLancar As String
		Get
			Return _InvestasiLainnyaAsetLancar
		End Get
		Set(value As String)
			_InvestasiLainnyaAsetLancar = value
		End Set
	End Property

	Public Property AsetLancarLainnya As String
		Get
			Return _AsetLancarLainnya
		End Get
		Set(value As String)
			_AsetLancarLainnya = value
		End Set
	End Property

	Public Property AsetTidakLancar As String
		Get
			Return _AsetTidakLancar
		End Get
		Set(value As String)
			_AsetTidakLancar = value
		End Set
	End Property

	Public Property PiutangUsahaAsetTidakLancar As String
		Get
			Return _PiutangUsahaAsetTidakLancar
		End Get
		Set(value As String)
			_PiutangUsahaAsetTidakLancar = value
		End Set
	End Property

	Public Property InvestasiLainnyaAsetTidakLancar As String
		Get
			Return _InvestasiLainnyaAsetTidakLancar
		End Get
		Set(value As String)
			_InvestasiLainnyaAsetTidakLancar = value
		End Set
	End Property

	Public Property AsetTidakLancarLainnya As String
		Get
			Return _AsetTidakLancarLainnya
		End Get
		Set(value As String)
			_AsetTidakLancarLainnya = value
		End Set
	End Property

	Public Property Liabilitas As String
		Get
			Return _Liabilitas
		End Get
		Set(value As String)
			_Liabilitas = value
		End Set
	End Property

	Public Property LiabilitasJangkaPendek As String
		Get
			Return _LiabilitasJangkaPendek
		End Get
		Set(value As String)
			_LiabilitasJangkaPendek = value
		End Set
	End Property

	Public Property PinjamanJangkaPendek As String
		Get
			Return _PinjamanJangkaPendek
		End Get
		Set(value As String)
			_PinjamanJangkaPendek = value
		End Set
	End Property

	Public Property UtangUsahaJangkaPendek As String
		Get
			Return _UtangUsahaJangkaPendek
		End Get
		Set(value As String)
			_UtangUsahaJangkaPendek = value
		End Set
	End Property

	Public Property LiabilitasJangkaPendekLainnya As String
		Get
			Return _LiabilitasJangkaPendekLainnya
		End Get
		Set(value As String)
			_LiabilitasJangkaPendekLainnya = value
		End Set
	End Property

	Public Property LiabilitasJangkaPanjang As String
		Get
			Return _LiabilitasJangkaPanjang
		End Get
		Set(value As String)
			_LiabilitasJangkaPanjang = value
		End Set
	End Property

	Public Property PinjamanJangkaPanjang As String
		Get
			Return _PinjamanJangkaPanjang
		End Get
		Set(value As String)
			_PinjamanJangkaPanjang = value
		End Set
	End Property

	Public Property UtangUsahaJangkaPanjang As String
		Get
			Return _UtangUsahaJangkaPanjang
		End Get
		Set(value As String)
			_UtangUsahaJangkaPanjang = value
		End Set
	End Property

	Public Property LiabilitasJangkaPanjangLainnya As String
		Get
			Return _LiabilitasJangkaPanjangLainnya
		End Get
		Set(value As String)
			_LiabilitasJangkaPanjangLainnya = value
		End Set
	End Property

	Public Property Ekuitas As String
		Get
			Return _Ekuitas
		End Get
		Set(value As String)
			_Ekuitas = value
		End Set
	End Property

	Public Property PendapatanUsahaOpr As String
		Get
			Return _PendapatanUsahaOpr
		End Get
		Set(value As String)
			_PendapatanUsahaOpr = value
		End Set
	End Property

	Public Property BebanPokokPendapatanOpr As String
		Get
			Return _BebanPokokPendapatanOpr
		End Get
		Set(value As String)
			_BebanPokokPendapatanOpr = value
		End Set
	End Property

	Public Property LabaRugiBruto As String
		Get
			Return _LabaRugiBruto
		End Get
		Set(value As String)
			_LabaRugiBruto = value
		End Set
	End Property

	Public Property PLLNonOpr As String
		Get
			Return _PLLNonOpr
		End Get
		Set(value As String)
			_PLLNonOpr = value
		End Set
	End Property

	Public Property BebanLainLainNonOpr As String
		Get
			Return _BebanLainLainNonOpr
		End Get
		Set(value As String)
			_BebanLainLainNonOpr = value
		End Set
	End Property

	Public Property LabaRugiSebelumPajak As String
		Get
			Return _LabaRugiSebelumPajak
		End Get
		Set(value As String)
			_LabaRugiSebelumPajak = value
		End Set
	End Property

	Public Property LabaRugiTahunBerjalan As String
		Get
			Return _LabaRugiTahunBerjalan
		End Get
		Set(value As String)
			_LabaRugiTahunBerjalan = value
		End Set
	End Property

	Public Property KodeKantorCabang As String
		Get
			Return _KodeKantorCabang
		End Get
		Set(value As String)
			_KodeKantorCabang = value
		End Set
	End Property

	Public Property OperasiData As String
		Get
			Return _OperasiData
		End Get
		Set(value As String)
			_OperasiData = value
		End Set
	End Property

	Public Property ListData As DataTable
		Get
			Return _ListData
		End Get
		Set(value As DataTable)
			_ListData = value
		End Set
	End Property

	Public Property ListDataReport As DataSet
		Get
			Return _ListDataReport
		End Get
		Set(value As DataSet)
			_ListDataReport = value
		End Set
	End Property

	Public Property TotalRecords As Long
		Get
			Return _TotalRecords
		End Get
		Set(value As Long)
			_TotalRecords = value
		End Set
	End Property

	Public Property Table As String
		Get
			Return _Table
		End Get
		Set(value As String)
			_Table = value
		End Set
	End Property

	Public Property BulanData As String
		Get
			Return _BulanData
		End Get
		Set(value As String)
			_BulanData = value
		End Set
	End Property
End Class
