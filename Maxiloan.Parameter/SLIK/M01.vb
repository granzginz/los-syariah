﻿<Serializable()>
Public Class M01 : Inherits Common
    Private _ID As Int64
    Private _BulanData As String
    Private _FlagDetail As String
    Private _NoIDPengurus As String
    Private _CIF As String
    Private _KodeJenisIdentitas As String
    Private _NamaPengurus As String
    Private _JenisKelamin As String
    Private _Alamat As String
    Private _Kelurahan As String
    Private _Kecamatan As String
    Private _KodeKota As String
    Private _KodeJabatan As String
    Private _PangsaKepemilikan As String
    Private _StatusPengurus As String
    Private _KodeCabang As String
    Private _OperasiData As String

    Private _ListData As DataTable
    Private _ListDataReport As DataSet
    Private _TotalRecords As Int64
    Private _Table As String

    Public Property FlagDetail As String
        Get
            Return _FlagDetail
        End Get
        Set(value As String)
            _FlagDetail = value
        End Set
    End Property

    Public Property NoIDPengurus As String
        Get
            Return _NoIDPengurus
        End Get
        Set(value As String)
            _NoIDPengurus = value
        End Set
    End Property

    Public Property CIF As String
        Get
            Return _CIF
        End Get
        Set(value As String)
            _CIF = value
        End Set
    End Property

    Public Property KodeJenisIdentitas As String
        Get
            Return _KodeJenisIdentitas
        End Get
        Set(value As String)
            _KodeJenisIdentitas = value
        End Set
    End Property

    Public Property NamaPengurus As String
        Get
            Return _NamaPengurus
        End Get
        Set(value As String)
            _NamaPengurus = value
        End Set
    End Property

    Public Property JenisKelamin As String
        Get
            Return _JenisKelamin
        End Get
        Set(value As String)
            _JenisKelamin = value
        End Set
    End Property

    Public Property Alamat As String
        Get
            Return _Alamat
        End Get
        Set(value As String)
            _Alamat = value
        End Set
    End Property

    Public Property Kelurahan As String
        Get
            Return _Kelurahan
        End Get
        Set(value As String)
            _Kelurahan = value
        End Set
    End Property

    Public Property Kecamatan As String
        Get
            Return _Kecamatan
        End Get
        Set(value As String)
            _Kecamatan = value
        End Set
    End Property

    Public Property KodeKota As String
        Get
            Return _KodeKota
        End Get
        Set(value As String)
            _KodeKota = value
        End Set
    End Property

    Public Property KodeJabatan As String
        Get
            Return _KodeJabatan
        End Get
        Set(value As String)
            _KodeJabatan = value
        End Set
    End Property

    Public Property PangsaKepemilikan As String
        Get
            Return _PangsaKepemilikan
        End Get
        Set(value As String)
            _PangsaKepemilikan = value
        End Set
    End Property

    Public Property StatusPengurus As String
        Get
            Return _StatusPengurus
        End Get
        Set(value As String)
            _StatusPengurus = value
        End Set
    End Property

    Public Property KodeCabang As String
        Get
            Return _KodeCabang
        End Get
        Set(value As String)
            _KodeCabang = value
        End Set
    End Property

    Public Property OperasiData As String
        Get
            Return _OperasiData
        End Get
        Set(value As String)
            _OperasiData = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property

    Public Property ListDataReport As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(value As DataSet)
            _ListDataReport = value
        End Set
    End Property

    Public Property TotalRecords As Long
        Get
            Return _TotalRecords
        End Get
        Set(value As Long)
            _TotalRecords = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property BulanData As String
        Get
            Return _BulanData
        End Get
        Set(value As String)
            _BulanData = value
        End Set
    End Property
End Class
