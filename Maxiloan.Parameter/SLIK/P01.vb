﻿<Serializable()>
Public Class P01 : Inherits Common
    Private _ID As Int64
    Private _BulanData As String
    Private _FlagDetail As String
    Private _Nomor_Identitas_Penjamin As String
    Private _Nomor_Rekening_Fasilitas As String
    Private _CIF As String
    Private _Kode_Jenis_Segmen_Fasilitas As String
    Private _Kode_Jenis_Identitas_Penjamin As String
    Private _Nama_Penjamin_Sesuai_Identitas As String
    Private _Nama_Lengkap_Penjamin As String
    Private _Kode_Golongan_Penjamin As String
    Private _Alamat_Penjamin As String
    Private _Persentase_Fasilitas_yang_Dijamin As String
    Private _Keterangan As String
    Private _Kode_Kantor_Cabang As String
    Private _Operasi_Data As String

    Private _ListData As DataTable
    Private _ListDataReport As DataSet
    Private _TotalRecords As Int64
    Private _Table As String

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property BulanData As String
        Get
            Return _BulanData
        End Get
        Set(value As String)
            _BulanData = value
        End Set
    End Property

    Public Property FlagDetail As String
        Get
            Return _FlagDetail
        End Get
        Set(value As String)
            _FlagDetail = value
        End Set
    End Property

    Public Property Nomor_Identitas_Penjamin As String
        Get
            Return _Nomor_Identitas_Penjamin
        End Get
        Set(value As String)
            _Nomor_Identitas_Penjamin = value
        End Set
    End Property

    Public Property Nomor_Rekening_Fasilitas As String
        Get
            Return _Nomor_Rekening_Fasilitas
        End Get
        Set(value As String)
            _Nomor_Rekening_Fasilitas = value
        End Set
    End Property

    Public Property CIF As String
        Get
            Return _CIF
        End Get
        Set(value As String)
            _CIF = value
        End Set
    End Property

    Public Property Kode_Jenis_Segmen_Fasilitas As String
        Get
            Return _Kode_Jenis_Segmen_Fasilitas
        End Get
        Set(value As String)
            _Kode_Jenis_Segmen_Fasilitas = value
        End Set
    End Property

    Public Property Kode_Jenis_Identitas_Penjamin As String
        Get
            Return _Kode_Jenis_Identitas_Penjamin
        End Get
        Set(value As String)
            _Kode_Jenis_Identitas_Penjamin = value
        End Set
    End Property

    Public Property Nama_Penjamin_Sesuai_Identitas As String
        Get
            Return _Nama_Penjamin_Sesuai_Identitas
        End Get
        Set(value As String)
            _Nama_Penjamin_Sesuai_Identitas = value
        End Set
    End Property

    Public Property Nama_Lengkap_Penjamin As String
        Get
            Return _Nama_Lengkap_Penjamin
        End Get
        Set(value As String)
            _Nama_Lengkap_Penjamin = value
        End Set
    End Property

    Public Property Kode_Golongan_Penjamin As String
        Get
            Return _Kode_Golongan_Penjamin
        End Get
        Set(value As String)
            _Kode_Golongan_Penjamin = value
        End Set
    End Property

    Public Property Alamat_Penjamin As String
        Get
            Return _Alamat_Penjamin
        End Get
        Set(value As String)
            _Alamat_Penjamin = value
        End Set
    End Property

    Public Property Persentase_Fasilitas_yang_Dijamin As String
        Get
            Return _Persentase_Fasilitas_yang_Dijamin
        End Get
        Set(value As String)
            _Persentase_Fasilitas_yang_Dijamin = value
        End Set
    End Property

    Public Property Keterangan As String
        Get
            Return _Keterangan
        End Get
        Set(value As String)
            _Keterangan = value
        End Set
    End Property

    Public Property Kode_Kantor_Cabang As String
        Get
            Return _Kode_Kantor_Cabang
        End Get
        Set(value As String)
            _Kode_Kantor_Cabang = value
        End Set
    End Property

    Public Property Operasi_Data As String
        Get
            Return _Operasi_Data
        End Get
        Set(value As String)
            _Operasi_Data = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property

    Public Property ListDataReport As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(value As DataSet)
            _ListDataReport = value
        End Set
    End Property

    Public Property TotalRecords As Long
        Get
            Return _TotalRecords
        End Get
        Set(value As Long)
            _TotalRecords = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property
End Class
