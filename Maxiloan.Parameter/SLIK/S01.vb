﻿<Serializable()>
Public Class S01 : Inherits Common
    Private _ID As Int64
    Private _BULANDATA As String
    Private _FLAG As String
    Private _NO_REK_FASILITAS As String
    Private _CIF As String
    Private _KODE_JENIS_SEGMEN As String
    Private _KODE_KUALITAS_1 As String
    Private _JML_HARI_TUNGGAKAN_1 As String
    Private _KODE_KUALITAS_2 As String
    Private _JML_HARI_TUNGGAKAN_2 As String
    Private _KODE_KUALITAS_3 As String
    Private _JML_HARI_TUNGGAKAN_3 As String
    Private _KODE_KUALITAS_4 As String
    Private _JML_HARI_TUNGGAKAN_4 As String
    Private _KODE_KUALITAS_5 As String
    Private _JML_HARI_TUNGGAKAN_5 As String
    Private _KODE_KUALITAS_6 As String
    Private _JML_HARI_TUNGGAKAN_6 As String
    Private _KODE_KUALITAS_7 As String
    Private _JML_HARI_TUNGGAKAN_7 As String
    Private _KODE_KUALITAS_8 As String
    Private _JML_HARI_TUNGGAKAN_8 As String
    Private _KODE_KUALITAS_9 As String
    Private _JML_HARI_TUNGGAKAN_9 As String
    Private _KODE_KUALITAS_10 As String
    Private _JML_HARI_TUNGGAKAN_10 As String
    Private _KODE_KUALITAS_11 As String
    Private _JML_HARI_TUNGGAKAN_11 As String
    Private _KODE_KUALITAS_12 As String
    Private _JML_HARI_TUNGGAKAN_12 As String

    Private _ListData As DataTable
    Private _ListDataReport As DataSet
    Private _TotalRecords As Int64
    Private _Table As String

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property

    Public Property FLAG As String
        Get
            Return _FLAG
        End Get
        Set(value As String)
            _FLAG = value
        End Set
    End Property

    Public Property NO_REK_FASILITAS As String
        Get
            Return _NO_REK_FASILITAS
        End Get
        Set(value As String)
            _NO_REK_FASILITAS = value
        End Set
    End Property

    Public Property CIF As String
        Get
            Return _CIF
        End Get
        Set(value As String)
            _CIF = value
        End Set
    End Property

    Public Property KODE_JENIS_SEGMEN As String
        Get
            Return _KODE_JENIS_SEGMEN
        End Get
        Set(value As String)
            _KODE_JENIS_SEGMEN = value
        End Set
    End Property

    Public Property KODE_KUALITAS_1 As String
        Get
            Return _KODE_KUALITAS_1
        End Get
        Set(value As String)
            _KODE_KUALITAS_1 = value
        End Set
    End Property

    Public Property JML_HARI_TUNGGAKAN_1 As String
        Get
            Return _JML_HARI_TUNGGAKAN_1
        End Get
        Set(value As String)
            _JML_HARI_TUNGGAKAN_1 = value
        End Set
    End Property

    Public Property KODE_KUALITAS_2 As String
        Get
            Return _KODE_KUALITAS_2
        End Get
        Set(value As String)
            _KODE_KUALITAS_2 = value
        End Set
    End Property

    Public Property JML_HARI_TUNGGAKAN_2 As String
        Get
            Return _JML_HARI_TUNGGAKAN_2
        End Get
        Set(value As String)
            _JML_HARI_TUNGGAKAN_2 = value
        End Set
    End Property

    Public Property KODE_KUALITAS_3 As String
        Get
            Return _KODE_KUALITAS_3
        End Get
        Set(value As String)
            _KODE_KUALITAS_3 = value
        End Set
    End Property

    Public Property JML_HARI_TUNGGAKAN_3 As String
        Get
            Return _JML_HARI_TUNGGAKAN_3
        End Get
        Set(value As String)
            _JML_HARI_TUNGGAKAN_3 = value
        End Set
    End Property

    Public Property KODE_KUALITAS_4 As String
        Get
            Return _KODE_KUALITAS_4
        End Get
        Set(value As String)
            _KODE_KUALITAS_4 = value
        End Set
    End Property

    Public Property JML_HARI_TUNGGAKAN_4 As String
        Get
            Return _JML_HARI_TUNGGAKAN_4
        End Get
        Set(value As String)
            _JML_HARI_TUNGGAKAN_4 = value
        End Set
    End Property

    Public Property KODE_KUALITAS_5 As String
        Get
            Return _KODE_KUALITAS_5
        End Get
        Set(value As String)
            _KODE_KUALITAS_5 = value
        End Set
    End Property

    Public Property JML_HARI_TUNGGAKAN_5 As String
        Get
            Return _JML_HARI_TUNGGAKAN_5
        End Get
        Set(value As String)
            _JML_HARI_TUNGGAKAN_5 = value
        End Set
    End Property

    Public Property KODE_KUALITAS_6 As String
        Get
            Return _KODE_KUALITAS_6
        End Get
        Set(value As String)
            _KODE_KUALITAS_6 = value
        End Set
    End Property

    Public Property JML_HARI_TUNGGAKAN_6 As String
        Get
            Return _JML_HARI_TUNGGAKAN_6
        End Get
        Set(value As String)
            _JML_HARI_TUNGGAKAN_6 = value
        End Set
    End Property

    Public Property KODE_KUALITAS_7 As String
        Get
            Return _KODE_KUALITAS_7
        End Get
        Set(value As String)
            _KODE_KUALITAS_7 = value
        End Set
    End Property

    Public Property JML_HARI_TUNGGAKAN_7 As String
        Get
            Return _JML_HARI_TUNGGAKAN_7
        End Get
        Set(value As String)
            _JML_HARI_TUNGGAKAN_7 = value
        End Set
    End Property

    Public Property KODE_KUALITAS_8 As String
        Get
            Return _KODE_KUALITAS_8
        End Get
        Set(value As String)
            _KODE_KUALITAS_8 = value
        End Set
    End Property

    Public Property JML_HARI_TUNGGAKAN_8 As String
        Get
            Return _JML_HARI_TUNGGAKAN_8
        End Get
        Set(value As String)
            _JML_HARI_TUNGGAKAN_8 = value
        End Set
    End Property

    Public Property KODE_KUALITAS_9 As String
        Get
            Return _KODE_KUALITAS_9
        End Get
        Set(value As String)
            _KODE_KUALITAS_9 = value
        End Set
    End Property

    Public Property JML_HARI_TUNGGAKAN_9 As String
        Get
            Return _JML_HARI_TUNGGAKAN_9
        End Get
        Set(value As String)
            _JML_HARI_TUNGGAKAN_9 = value
        End Set
    End Property

    Public Property KODE_KUALITAS_10 As String
        Get
            Return _KODE_KUALITAS_10
        End Get
        Set(value As String)
            _KODE_KUALITAS_10 = value
        End Set
    End Property

    Public Property JML_HARI_TUNGGAKAN_10 As String
        Get
            Return _JML_HARI_TUNGGAKAN_10
        End Get
        Set(value As String)
            _JML_HARI_TUNGGAKAN_10 = value
        End Set
    End Property

    Public Property KODE_KUALITAS_11 As String
        Get
            Return _KODE_KUALITAS_11
        End Get
        Set(value As String)
            _KODE_KUALITAS_11 = value
        End Set
    End Property

    Public Property JML_HARI_TUNGGAKAN_11 As String
        Get
            Return _JML_HARI_TUNGGAKAN_11
        End Get
        Set(value As String)
            _JML_HARI_TUNGGAKAN_11 = value
        End Set
    End Property

    Public Property KODE_KUALITAS_12 As String
        Get
            Return _KODE_KUALITAS_12
        End Get
        Set(value As String)
            _KODE_KUALITAS_12 = value
        End Set
    End Property

    Public Property JML_HARI_TUNGGAKAN_12 As String
        Get
            Return _JML_HARI_TUNGGAKAN_12
        End Get
        Set(value As String)
            _JML_HARI_TUNGGAKAN_12 = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property

    Public Property ListDataReport As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(value As DataSet)
            _ListDataReport = value
        End Set
    End Property

    Public Property TotalRecords As Long
        Get
            Return _TotalRecords
        End Get
        Set(value As Long)
            _TotalRecords = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property
End Class
