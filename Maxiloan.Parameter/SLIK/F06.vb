﻿<Serializable()>
Public Class F06 : Inherits Common
    Private _ID As Int64
    Private _BulanData As String
    Private _Flagdetail As String
    Private _NoRekeningFasilitas As String
    Private _CIF As String
    Private _KodeJenisFasilitasLain As String
    Private _SumberDana As String
    Private _TanggalMulai As String
    Private _TanggalJatuhTempo As String
    Private _SukuBungaatauImbalan As String
    Private _KodeValuta As String
    Private _Nominal As String
    Private _NilaiDalamMataUangAsal As String
    Private _KodeKualitas As String
    Private _TanggalMacet As String
    Private _KodeSebabMacet As String
    Private _Tunggakan As String
    Private _JumlahHariTunggakan As String
    Private _KodeKondisi As String
    Private _TanggalKondisi As String
    Private _Keterangan As String
    Private _KodeKantorCabang As String
    Private _OperasiData As String

    Private _ListData As DataTable
    Private _ListDataReport As DataSet
    Private _TotalRecords As Int64
    Private _Table As String

    Public Property Flagdetail As String
        Get
            Return _Flagdetail
        End Get
        Set(value As String)
            _Flagdetail = value
        End Set
    End Property

    Public Property NoRekeningFasilitas As String
        Get
            Return _NoRekeningFasilitas
        End Get
        Set(value As String)
            _NoRekeningFasilitas = value
        End Set
    End Property

    Public Property CIF As String
        Get
            Return _CIF
        End Get
        Set(value As String)
            _CIF = value
        End Set
    End Property

    Public Property KodeJenisFasilitasLain As String
        Get
            Return _KodeJenisFasilitasLain
        End Get
        Set(value As String)
            _KodeJenisFasilitasLain = value
        End Set
    End Property

    Public Property SumberDana As String
        Get
            Return _SumberDana
        End Get
        Set(value As String)
            _SumberDana = value
        End Set
    End Property

    Public Property TanggalMulai As String
        Get
            Return _TanggalMulai
        End Get
        Set(value As String)
            _TanggalMulai = value
        End Set
    End Property

    Public Property TanggalJatuhTempo As String
        Get
            Return _TanggalJatuhTempo
        End Get
        Set(value As String)
            _TanggalJatuhTempo = value
        End Set
    End Property

    Public Property SukuBungaatauImbalan As String
        Get
            Return _SukuBungaatauImbalan
        End Get
        Set(value As String)
            _SukuBungaatauImbalan = value
        End Set
    End Property

    Public Property KodeValuta As String
        Get
            Return _KodeValuta
        End Get
        Set(value As String)
            _KodeValuta = value
        End Set
    End Property

    Public Property Nominal As String
        Get
            Return _Nominal
        End Get
        Set(value As String)
            _Nominal = value
        End Set
    End Property

    Public Property NilaiDalamMataUangAsal As String
        Get
            Return _NilaiDalamMataUangAsal
        End Get
        Set(value As String)
            _NilaiDalamMataUangAsal = value
        End Set
    End Property

    Public Property KodeKualitas As String
        Get
            Return _KodeKualitas
        End Get
        Set(value As String)
            _KodeKualitas = value
        End Set
    End Property

    Public Property TanggalMacet As String
        Get
            Return _TanggalMacet
        End Get
        Set(value As String)
            _TanggalMacet = value
        End Set
    End Property

    Public Property KodeSebabMacet As String
        Get
            Return _KodeSebabMacet
        End Get
        Set(value As String)
            _KodeSebabMacet = value
        End Set
    End Property

    Public Property Tunggakan As String
        Get
            Return _Tunggakan
        End Get
        Set(value As String)
            _Tunggakan = value
        End Set
    End Property

    Public Property JumlahHariTunggakan As String
        Get
            Return _JumlahHariTunggakan
        End Get
        Set(value As String)
            _JumlahHariTunggakan = value
        End Set
    End Property

    Public Property KodeKondisi As String
        Get
            Return _KodeKondisi
        End Get
        Set(value As String)
            _KodeKondisi = value
        End Set
    End Property

    Public Property TanggalKondisi As String
        Get
            Return _TanggalKondisi
        End Get
        Set(value As String)
            _TanggalKondisi = value
        End Set
    End Property

    Public Property Keterangan As String
        Get
            Return _Keterangan
        End Get
        Set(value As String)
            _Keterangan = value
        End Set
    End Property

    Public Property KodeKantorCabang As String
        Get
            Return _KodeKantorCabang
        End Get
        Set(value As String)
            _KodeKantorCabang = value
        End Set
    End Property

    Public Property OperasiData As String
        Get
            Return _OperasiData
        End Get
        Set(value As String)
            _OperasiData = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property

    Public Property ListDataReport As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(value As DataSet)
            _ListDataReport = value
        End Set
    End Property

    Public Property TotalRecords As Long
        Get
            Return _TotalRecords
        End Get
        Set(value As Long)
            _TotalRecords = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property BulanData As String
        Get
            Return _BulanData
        End Get
        Set(value As String)
            _BulanData = value
        End Set
    End Property
End Class
