﻿<Serializable()>
Public Class A01 : Inherits Common
	Private _ID As String
	Private _Bulandata As String
	Private _FlagDetail As String
	Private _NoAgunan As String
	Private _NoRekFasilitas As String
	Private _CIF As String
	Private _KodeJenisSegmen As String
	Private _KodeStatusAgunan As String
	Private _KodeJenisAgunan As String
	Private _PeringkatAgunan As String
	Private _KodelembagaPemeringkat As String
	Private _KodeJenisPengikatan As String
	Private _TanggalPengikatan As String
	Private _NamaPemilikAgunan As String
	Private _BuktiKepemilikan As String
	Private _AlamatAgunan As String
	Private _KodeKabKota As String
	Private _NilaiAgunanNJOP As String
	Private _NilaiAgunanPelapor As String
	Private _TanggalPenilaianAgunanPelapor As String
	Private _NilaiAgunanPenilaiIndipenden As String
	Private _NamaPenilaiIndependen As String
	Private _TanggalPenilaianIndependen As String
	Private _StatusParipasu As String
	Private _PersentaseParipasu As String
	Private _PembiayaanJointAccount As String
	Private _Diasuransikan As String
	Private _Keterangan As String
	Private _KodeKantorCabang As String
	Private _OperasiData As String

	Private _ListData As DataTable
	Private _ListDataReport As DataSet
	Private _TotalRecords As Int64
	Private _Table As String
	Public Property FlagDetail As String
		Get
			Return _FlagDetail
		End Get
		Set(value As String)
			_FlagDetail = value
		End Set
	End Property

	Public Property NoAgunan As String
		Get
			Return _NoAgunan
		End Get
		Set(value As String)
			_NoAgunan = value
		End Set
	End Property

	Public Property NoRekFasilitas As String
		Get
			Return _NoRekFasilitas
		End Get
		Set(value As String)
			_NoRekFasilitas = value
		End Set
	End Property

	Public Property CIF As String
		Get
			Return _CIF
		End Get
		Set(value As String)
			_CIF = value
		End Set
	End Property

	Public Property KodeJenisSegmen As String
		Get
			Return _KodeJenisSegmen
		End Get
		Set(value As String)
			_KodeJenisSegmen = value
		End Set
	End Property

	Public Property KodeStatusAgunan As String
		Get
			Return _KodeStatusAgunan
		End Get
		Set(value As String)
			_KodeStatusAgunan = value
		End Set
	End Property

	Public Property KodeJenisAgunan As String
		Get
			Return _KodeJenisAgunan
		End Get
		Set(value As String)
			_KodeJenisAgunan = value
		End Set
	End Property

	Public Property PeringkatAgunan As String
		Get
			Return _PeringkatAgunan
		End Get
		Set(value As String)
			_PeringkatAgunan = value
		End Set
	End Property

	Public Property KodelembagaPemeringkat As String
		Get
			Return _KodelembagaPemeringkat
		End Get
		Set(value As String)
			_KodelembagaPemeringkat = value
		End Set
	End Property

	Public Property KodeJenisPengikatan As String
		Get
			Return _KodeJenisPengikatan
		End Get
		Set(value As String)
			_KodeJenisPengikatan = value
		End Set
	End Property

	Public Property TanggalPengikatan As String
		Get
			Return _TanggalPengikatan
		End Get
		Set(value As String)
			_TanggalPengikatan = value
		End Set
	End Property

	Public Property NamaPemilikAgunan As String
		Get
			Return _NamaPemilikAgunan
		End Get
		Set(value As String)
			_NamaPemilikAgunan = value
		End Set
	End Property

	Public Property BuktiKepemilikan As String
		Get
			Return _BuktiKepemilikan
		End Get
		Set(value As String)
			_BuktiKepemilikan = value
		End Set
	End Property

	Public Property AlamatAgunan As String
		Get
			Return _AlamatAgunan
		End Get
		Set(value As String)
			_AlamatAgunan = value
		End Set
	End Property

	Public Property KodeKabKota As String
		Get
			Return _KodeKabKota
		End Get
		Set(value As String)
			_KodeKabKota = value
		End Set
	End Property

	Public Property NilaiAgunanNJOP As String
		Get
			Return _NilaiAgunanNJOP
		End Get
		Set(value As String)
			_NilaiAgunanNJOP = value
		End Set
	End Property

	Public Property NilaiAgunanPelapor As String
		Get
			Return _NilaiAgunanPelapor
		End Get
		Set(value As String)
			_NilaiAgunanPelapor = value
		End Set
	End Property

	Public Property TanggalPenilaianAgunanPelapor As String
		Get
			Return _TanggalPenilaianAgunanPelapor
		End Get
		Set(value As String)
			_TanggalPenilaianAgunanPelapor = value
		End Set
	End Property

	Public Property NilaiAgunanPenilaiIndipenden As String
		Get
			Return _NilaiAgunanPenilaiIndipenden
		End Get
		Set(value As String)
			_NilaiAgunanPenilaiIndipenden = value
		End Set
	End Property

	Public Property NamaPenilaiIndependen As String
		Get
			Return _NamaPenilaiIndependen
		End Get
		Set(value As String)
			_NamaPenilaiIndependen = value
		End Set
	End Property

	Public Property TanggalPenilaianIndependen As String
		Get
			Return _TanggalPenilaianIndependen
		End Get
		Set(value As String)
			_TanggalPenilaianIndependen = value
		End Set
	End Property

	Public Property StatusParipasu As String
		Get
			Return _StatusParipasu
		End Get
		Set(value As String)
			_StatusParipasu = value
		End Set
	End Property

	Public Property PersentaseParipasu As String
		Get
			Return _PersentaseParipasu
		End Get
		Set(value As String)
			_PersentaseParipasu = value
		End Set
	End Property

	Public Property PembiayaanJointAccount As String
		Get
			Return _PembiayaanJointAccount
		End Get
		Set(value As String)
			_PembiayaanJointAccount = value
		End Set
	End Property

	Public Property Diasuransikan As String
		Get
			Return _Diasuransikan
		End Get
		Set(value As String)
			_Diasuransikan = value
		End Set
	End Property

	Public Property Keterangan As String
		Get
			Return _Keterangan
		End Get
		Set(value As String)
			_Keterangan = value
		End Set
	End Property

	Public Property KodeKantorCabang As String
		Get
			Return _KodeKantorCabang
		End Get
		Set(value As String)
			_KodeKantorCabang = value
		End Set
	End Property

	Public Property OperasiData As String
		Get
			Return _OperasiData
		End Get
		Set(value As String)
			_OperasiData = value
		End Set
	End Property

	Public Property ListData As DataTable
		Get
			Return _ListData
		End Get
		Set(value As DataTable)
			_ListData = value
		End Set
	End Property

	Public Property ListDataReport As DataSet
		Get
			Return _ListDataReport
		End Get
		Set(value As DataSet)
			_ListDataReport = value
		End Set
	End Property

	Public Property TotalRecords As Long
		Get
			Return _TotalRecords
		End Get
		Set(value As Long)
			_TotalRecords = value
		End Set
	End Property

	Public Property Table As String
		Get
			Return _Table
		End Get
		Set(value As String)
			_Table = value
		End Set
	End Property

	Public Property Bulandata As String
		Get
			Return _Bulandata
		End Get
		Set(value As String)
			_Bulandata = value
		End Set
	End Property

	Public Property ID As String
		Get
			Return _ID
		End Get
		Set(value As String)
			_ID = value
		End Set
	End Property
End Class
