
<Serializable()> _
Public Class ApprovalScheme : Inherits Approval
#Region "Constanta Approval Scheme Master"
    Private _approvalschemeid As String
    Private _approvalschemename As String
    Private _islimitneeded As Boolean
    Private _isautoscale As Boolean
    Private _isstaging As Boolean
    Private _statusoptions As String
    Private _transactionnolabel As String
    Private _transactionnolink As String
    Private _optionallabel1 As String
    Private _optionallabel2 As String
    Private _optionallabel3 As String
    Private _optionallink1 As String
    Private _optionallink2 As String
    Private _optionalsql1 As String
    Private _optionalsql2 As String
    Private _notelabel As String
    Private _notesqlcmd As String
    Private _limitlabel As String
    Private _limitsqlcmd As String
    Private _linklabel1 As String
    Private _linklabel2 As String
    Private _linklabel3 As String
    Private _linklabel4 As String
    Private _linklabel5 As String
    Private _linkaddress1 As String
    Private _linkaddress2 As String
    Private _linkaddress3 As String
    Private _linkaddress4 As String
    Private _linkaddress5 As String
    Private _issystem As Boolean
    Private _approvaltypeid As String
#End Region

#Region "Constanta Approval Path"
    Private _approvalparentseqnum As Integer
    Private _approvaldescription As String
    Private _maxlimit As Double
    Private _minlimit As Double
    Private _limit As Double
    Private _level As Double
    Private _canfinalreject As Boolean
    Private _canfinalapprove As Boolean
    Private _canescalation As Boolean
    Private _rejectaction As String
    Private _approvalseqno As Integer
    Private _typelimit As String
    Private _duration As Integer
    Private _autouser As String
    Private _nextperson As String

#End Region

    Public Property ApprovalTypeID() As String
        Get
            Return _approvaltypeid
        End Get
        Set(ByVal Value As String)
            _approvaltypeid = Value
        End Set
    End Property
    Public Property ApprovalSchemeID() As String
        Get
            Return _approvalschemeid
        End Get
        Set(ByVal Value As String)
            _approvalschemeid = Value
        End Set
    End Property

    Public Property ApprovalSchemeName() As String
        Get
            Return _approvalschemename
        End Get
        Set(ByVal Value As String)
            _approvalschemename = Value
        End Set
    End Property
    Public Property IsStaging() As Boolean
        Get
            Return _isstaging
        End Get
        Set(ByVal Value As Boolean)
            _isstaging = Value
        End Set
    End Property
    Public Property IsLimitNeeded() As Boolean
        Get
            Return _islimitneeded
        End Get
        Set(ByVal Value As Boolean)
            _islimitneeded = Value
        End Set
    End Property
    Public Property IsAutoScale() As Boolean
        Get
            Return _isautoscale
        End Get
        Set(ByVal Value As Boolean)
            _isautoscale = Value
        End Set
    End Property
    Public Property StatusOptions() As String
        Get
            Return _statusoptions
        End Get
        Set(ByVal Value As String)
            _statusoptions = Value
        End Set
    End Property
    Public Property TransactionNoLabel() As String
        Get
            Return _transactionnolabel
        End Get
        Set(ByVal Value As String)
            _transactionnolabel = Value
        End Set
    End Property
    Public Property TransactionNoLink() As String
        Get
            Return _transactionnolink
        End Get
        Set(ByVal Value As String)
            _transactionnolink = Value
        End Set
    End Property
    Public Property OptionalLabel1() As String
        Get
            Return _optionallabel1
        End Get
        Set(ByVal Value As String)
            _optionallabel1 = Value
        End Set
    End Property
    Public Property OptionalLabel2() As String
        Get
            Return _optionallabel2
        End Get
        Set(ByVal Value As String)
            _optionallabel2 = Value
        End Set
    End Property
    Public Property OptionalLabel3() As String
        Get
            Return _optionallabel3
        End Get
        Set(ByVal Value As String)
            _optionallabel3 = Value
        End Set
    End Property
    Public Property OptionalLink1() As String
        Get
            Return _optionallink1
        End Get
        Set(ByVal Value As String)
            _optionallink1 = Value
        End Set
    End Property
    Public Property OptionalLink2() As String
        Get
            Return _optionallink2
        End Get
        Set(ByVal Value As String)
            _optionallink2 = Value
        End Set
    End Property
    Public Property OptionalSQL1() As String
        Get
            Return _optionalsql1
        End Get
        Set(ByVal Value As String)
            _optionalsql1 = Value
        End Set
    End Property
    Public Property OptionalSQL2() As String
        Get
            Return _optionalsql2
        End Get
        Set(ByVal Value As String)
            _optionalsql2 = Value
        End Set
    End Property
    Public Property NoteLabel() As String
        Get
            Return _notelabel
        End Get
        Set(ByVal Value As String)
            _notelabel = Value
        End Set
    End Property
    Public Property NoteSQLCmd() As String
        Get
            Return _notesqlcmd
        End Get
        Set(ByVal Value As String)
            _notesqlcmd = Value
        End Set
    End Property
    Public Property LimitLabel() As String
        Get
            Return _limitlabel
        End Get
        Set(ByVal Value As String)
            _limitlabel = Value
        End Set
    End Property
    Public Property LimitSQLCmd() As String
        Get
            Return _limitsqlcmd
        End Get
        Set(ByVal Value As String)
            _limitsqlcmd = Value
        End Set
    End Property
    Public Property LinkLabel1() As String
        Get
            Return _linklabel1
        End Get
        Set(ByVal Value As String)
            _linklabel1 = Value
        End Set
    End Property
    Public Property LinkLabel2() As String
        Get
            Return _linklabel2
        End Get
        Set(ByVal Value As String)
            _linklabel2 = Value
        End Set
    End Property
    Public Property LinkLabel3() As String
        Get
            Return _linklabel3
        End Get
        Set(ByVal Value As String)
            _linklabel3 = Value
        End Set
    End Property
    Public Property LinkLabel4() As String
        Get
            Return _linklabel4
        End Get
        Set(ByVal Value As String)
            _linklabel4 = Value
        End Set
    End Property
    Public Property LinkLabel5() As String
        Get
            Return _linklabel5
        End Get
        Set(ByVal Value As String)
            _linklabel5 = Value
        End Set
    End Property
    Public Property LinkAddress1() As String
        Get
            Return _linkaddress1
        End Get
        Set(ByVal Value As String)
            _linkaddress1 = Value
        End Set
    End Property
    Public Property LinkAddress2() As String
        Get
            Return _linkaddress2
        End Get
        Set(ByVal Value As String)
            _linkaddress2 = Value
        End Set
    End Property
    Public Property LinkAddress3() As String
        Get
            Return _linkaddress3
        End Get
        Set(ByVal Value As String)
            _linkaddress3 = Value
        End Set
    End Property
    Public Property LinkAddress4() As String
        Get
            Return _linkaddress4
        End Get
        Set(ByVal Value As String)
            _linkaddress4 = Value
        End Set
    End Property
    Public Property LinkAddress5() As String
        Get
            Return _linkaddress5
        End Get
        Set(ByVal Value As String)
            _linkaddress5 = Value
        End Set
    End Property
    Public Property IsSystem() As Boolean
        Get
            Return _issystem
        End Get
        Set(ByVal Value As Boolean)
            _issystem = Value
        End Set
    End Property

    Public Property ApprovalSeqNo() As Integer
        Get
            Return _approvalseqno
        End Get
        Set(ByVal Value As Integer)
            _approvalseqno = Value
        End Set
    End Property
    Public Property ApprovalParentSeqNum() As Integer
        Get
            Return _approvalparentseqnum
        End Get
        Set(ByVal Value As Integer)
            _approvalparentseqnum = Value
        End Set
    End Property
    Public Property ApprovalDescription() As String
        Get
            Return _approvaldescription
        End Get
        Set(ByVal Value As String)
            _approvaldescription = Value
        End Set
    End Property

    Public Property Level() As Double
        Get
            Return _level
        End Get
        Set(ByVal Value As Double)
            _level = Value
        End Set
    End Property
    Public Property CanFinalReject() As Boolean
        Get
            Return _canfinalreject
        End Get
        Set(ByVal Value As Boolean)
            _canfinalreject = Value
        End Set
    End Property
    Public Property CanFinalApprove() As Boolean
        Get
            Return _canfinalapprove
        End Get
        Set(ByVal Value As Boolean)
            _canfinalapprove = Value
        End Set
    End Property
    Public Property CanEscalation() As Boolean
        Get
            Return _canescalation
        End Get
        Set(ByVal Value As Boolean)
            _canescalation = Value
        End Set
    End Property
    Public Property RejectAction() As String
        Get
            Return _rejectaction
        End Get
        Set(ByVal Value As String)
            _rejectaction = Value
        End Set
    End Property
    Public Property TypeLimit() As String
        Get
            Return _typelimit
        End Get
        Set(ByVal Value As String)
            _typelimit = Value
        End Set
    End Property

    Public Property MaxLimit() As Double
        Get
            Return _maxlimit
        End Get
        Set(ByVal Value As Double)
            _maxlimit = Value
        End Set
    End Property

    Public Property MinLimit() As Double
        Get
            Return _minlimit
        End Get
        Set(ByVal Value As Double)
            _minlimit = Value
        End Set
    End Property

    Public Property Limit() As Double
        Get
            Return _limit
        End Get
        Set(ByVal Value As Double)
            _limit = Value
        End Set
    End Property

    Public Property Duration() As Integer
        Get
            Return _duration
        End Get
        Set(ByVal Value As Integer)
            _duration = Value
        End Set
    End Property

    Public Property AutoUser() As String
        Get
            Return _autouser
        End Get
        Set(ByVal Value As String)
            _autouser = Value
        End Set
    End Property

    Public Property NextPerson() As String
        Get
            Return _nextperson
        End Get
        Set(ByVal Value As String)
            _nextperson = Value
        End Set
    End Property

    'add
    Public Property ApprovalSeqNum As Integer
    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
End Class

