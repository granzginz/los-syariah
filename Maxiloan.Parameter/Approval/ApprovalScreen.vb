
Public Class ApprovalScreen : Inherits ApprovalScheme
    Private _approvaltype As String
    Private _requestfrom As String
    Private _approveby As String
    Private _approvalstatus As String
    Private _approvalresult As String
    Private _approvallist As DataTable
    Private _approvalno As String
    Private _approvalhistory As DataTable

    Public Property ApprovalType() As String
        Get
            Return _approvaltype
        End Get
        Set(ByVal Value As String)
            _approvaltype = Value
        End Set
    End Property

    Public Property RequestFrom() As String
        Get
            Return _requestfrom
        End Get
        Set(ByVal Value As String)
            _requestfrom = Value
        End Set
    End Property

    Public Property ApproveBy() As String
        Get
            Return _approveby
        End Get
        Set(ByVal Value As String)
            _approveby = Value
        End Set
    End Property

    Public Property ApprovalStatus() As String
        Get
            Return _approvalstatus
        End Get
        Set(ByVal Value As String)
            _approvalstatus = Value
        End Set
    End Property
    Public Property ApprovalResult() As String
        Get
            Return _approvalresult
        End Get
        Set(ByVal Value As String)
            _approvalresult = Value
        End Set
    End Property
    Public Property ApprovalList() As DataTable
        Get
            Return _approvallist
        End Get
        Set(ByVal Value As DataTable)
            _approvallist = Value
        End Set
    End Property

    Public Property ApprovalHistory() As DataTable
        Get
            Return _approvalhistory
        End Get
        Set(ByVal Value As DataTable)
            _approvalhistory = Value
        End Set
    End Property
End Class
