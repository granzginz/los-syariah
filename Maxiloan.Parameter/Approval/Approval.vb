
<Serializable()> _
Public Class Approval : Inherits Common
    Private _schemeid As String
    Private _Amount As Double
    Private _ApprovalType As ETransactionType
    Private _approvalid As String
    Private _approvalno As String
    Private _isfinal As Boolean
    Private _approvalnote As String
    Private _userapproval As String
    Private _userrequest As String
    Private _approvalvalue As Double
    Private _approvalresult As String
    Private _TransactionNo As String
    Private _iseverrejected As Boolean
    Private _isrerequest As Boolean
    Private _requestdate As Date
    Private _transaction As SqlClient.SqlTransaction

    Public Enum ETransactionType
        RCA_Approval = 1
        Prepayment_Approval = 2
        ReFund_Approval = 3
        Bonus_Approval = 4
        StopAccrued_Approval = 5
        WriteOff_Approval = 6
        Rescheduling_Approval = 7
        Repossess_Approval = 8
        NonStandardTransaction_Approval = 9
        AssetReplacement_Approval = 10
        DF_Approval = 11
        ContinueCredit_Approval = 12
        ChangeINVDueDate_Approval = 13
        Agreement_ChangeDueDate = 14
        Waive_Transaction = 15
        Agreement_Transfer = 16
        Collection_Expense = 17
        Refund_Advance = 18
        InventorySelling_Approval = 19
        AdvanceClaimCost_Approval = 20
        InvoiceToBePaid_approval = 21
        PaymentRequest_approval = 22
        Disbursement_Approval = 23
        PettyCashReiumbers_Approval = 24
        PaymentRequestFA_Approval = 25
        AmortisasiBiaya_Approval = 26
    End Enum

    Public Property SchemeID() As String
        Get
            Return CType(_schemeid, String)
        End Get
        Set(ByVal Value As String)
            _schemeid = Value
        End Set
    End Property

    Public Property AmountApproval() As Double
        Get
            Return CType(_Amount, Double)
        End Get
        Set(ByVal Value As Double)
            _Amount = Value
        End Set
    End Property

    Public Property AprovalType() As ETransactionType
        Get
            Return CType(_ApprovalType, ETransactionType)
        End Get
        Set(ByVal Value As ETransactionType)
            _ApprovalType = Value
        End Set
    End Property

    Public Property ApprovalID() As String
        Get
            Return _approvalid
        End Get
        Set(ByVal Value As String)
            _approvalid = Value
        End Set
    End Property

    Public Property ApprovalTransaction() As SqlClient.SqlTransaction
        Get
            Return _transaction
        End Get
        Set(ByVal Value As SqlClient.SqlTransaction)
            _transaction = Value
        End Set
    End Property

    Public Property UserApproval() As String
        Get
            Return _userapproval
        End Get
        Set(ByVal Value As String)
            _userapproval = Value
        End Set
    End Property
    Public Property RequestDate() As Date
        Get
            Return _requestdate
        End Get
        Set(ByVal Value As Date)
            _requestdate = Value
        End Set
    End Property

    Public Property UserRequest() As String
        Get
            Return _userrequest
        End Get
        Set(ByVal Value As String)
            _userrequest = Value
        End Set
    End Property

    Public Property ApprovalValue() As Double
        Get
            Return _approvalvalue
        End Get
        Set(ByVal Value As Double)
            _approvalvalue = Value
        End Set
    End Property

    Public Property TransactionNo() As String
        Get
            Return _TransactionNo
        End Get
        Set(ByVal Value As String)
            _TransactionNo = Value
        End Set
    End Property

    Public Property ApprovalNote() As String
        Get
            Return _approvalnote
        End Get
        Set(ByVal Value As String)
            _approvalnote = Value
        End Set
    End Property

    Private _Argumentasi As String

    Public Property Argumentasi() As String
        Get
            Return _Argumentasi
        End Get
        Set(ByVal Value As String)
            _Argumentasi = Value
        End Set
    End Property

    Public Property ApprovalNo() As String
        Get
            Return _approvalno
        End Get
        Set(ByVal Value As String)
            _approvalno = Value
        End Set
    End Property

    Public Property IsEverRejected() As Boolean
        Get
            Return _iseverrejected
        End Get
        Set(ByVal Value As Boolean)
            _iseverrejected = Value
        End Set
    End Property

    Public Property IsReRequest() As Boolean
        Get
            Return _isrerequest
        End Get
        Set(ByVal Value As Boolean)
            _isrerequest = Value
        End Set
    End Property

    Public Property IsFinal() As Boolean
        Get
            Return _isfinal
        End Get
        Set(ByVal Value As Boolean)
            _isfinal = Value
        End Set
    End Property

End Class

