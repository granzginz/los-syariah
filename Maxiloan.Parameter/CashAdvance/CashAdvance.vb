﻿<Serializable()>
Public Class CashAdvance : Inherits Common
    Public Property NoCA() As String
    Public Property TanggalCA() As DateTime
    Public Property TanggalDisburse() As DateTime
    Public Property Jumlah() As Decimal
    Public Property CaraBayar() As String
    Public Property Penerima() As String
    Public Property IDPenerima() As String
    Public Property Penggunaan() As String
    Public Property Status() As String
    Public Property Bank() As String
    Public Property CabangBank() As String
    Public Property NomorRekening() As String
    Public Property AtasNama() As String
    Public Property description() As String
    Public Property reversalno() As String

    Public Property PagingTable As DataTable


    Public Property ListData As DataTable
    Public Property CreateDate As DateTime
    Public Property CreateUser As String
    Public Property UpdateDate As DateTime
    Public Property UpdateUser As String

    Public Property SelectedMenu() As String
    Public Property SaveMenu() As String
    Public Property Msg() As String

    Public Property DariRekening() As String
    Public Property OpsiTransfer() As String
    Public Property BiayaTransfer() As Decimal
    Public Property BebanBiayaTransfer() As String
    Public Property Attachment() As String
    Public Property Pengembalian() As Decimal
    Public Property TanggalRealisasi() As DateTime
    Public Property Notes() As String

    Public Property PaymentAllocationIDCOA() As String
    Public Property AmountTotalCOA() As Decimal
    Public Property KeteranganCOA() As String

    Public Property seqNo() As Integer
    Public Property BankBranchID() As String
    Public Property BankBranchName() As String

    Public Property Department() As String
    Public Property LamaRealisasi() As String
    Public Property JournalBank() As String


End Class
