
<Serializable()> _
Public Class GenerateReport : Inherits Common
    Private _storeprocedure As String
    Private _listdataset As DataSet
    Public Property ListDataSet() As DataSet
        Get
            Return (CType(_listdataset, DataSet))
        End Get
        Set(ByVal Value As DataSet)
            _listdataset = Value
        End Set
    End Property

    Public Property StoreProcedure() As String
        Get
            Return (CType(_storeprocedure, String))
        End Get
        Set(ByVal Value As String)
            _storeprocedure = Value
        End Set
    End Property
End Class
