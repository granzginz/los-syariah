
<Serializable()> _
Public Class LookUpSupplier : Inherits Common
    Private _listdata As DataTable
    Private _totalrecords As Int64
    Private _applicationid As String
    Private _assetusednew As String
    Private _TotalSupplierID As String

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property TotalSupplierID() As String
        Get
            Return _TotalSupplierID
        End Get
        Set(ByVal Value As String)
            _TotalSupplierID = Value
        End Set
    End Property

    Public Property ApplicationId() As String
        Get
            Return _applicationid
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property

    Public Property AssetUsedNew() As String
        Get
            Return _assetusednew
        End Get
        Set(ByVal Value As String)
            _assetusednew = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

End Class
