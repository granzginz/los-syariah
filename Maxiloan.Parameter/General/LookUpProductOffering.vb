
<Serializable()> _
Public Class LookUpProductOffering : Inherits Common
    Private _listdata As DataTable
    Private _totalrecords As Int64
    Private _businessdate As Date

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property BusDate() As Date
        Get
            Return _businessdate
        End Get
        Set(ByVal Value As Date)
            _businessdate = Value
        End Set
    End Property

End Class


