
<Serializable()> _
Public Class LookUpAsset : Inherits Common
    Private _listdata As DataTable
    Private _totalrecords As Int64
    Private _applicationid As String
    Private _TotalAsset As String
    Private _assettypeid As String

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property ApplicationId() As String
        Get
            Return _applicationid
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property

    Public Property AssetTypeID() As String
        Get
            Return _assettypeid
        End Get
        Set(ByVal Value As String)
            _assettypeid = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalAsset() As String
        Get
            Return _TotalAsset
        End Get
        Set(ByVal Value As String)
            _TotalAsset = Value
        End Set
    End Property


End Class