
<Serializable()> _
Public Class GeneralSetting : Inherits Common
    Private _listdata As DataTable
    Private _totalrecords As Int64
    Private _GSID As String
    Private _ModuleID As String
    Private _GsName As String
    Private _GsValue As String
    Private _IsUpdatable As Boolean
    Private _ListDataReport As DataSet

    Public Property ListDataReport() As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(ByVal Value As DataSet)
            _ListDataReport = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property GSID() As String
        Get
            Return _GSID
        End Get
        Set(ByVal Value As String)
            _GSID = Value
        End Set
    End Property

    Public Property ModuleID() As String
        Get
            Return _ModuleID
        End Get
        Set(ByVal Value As String)
            _ModuleID = Value
        End Set
    End Property

    Public Property GsName() As String
        Get
            Return _GsName
        End Get
        Set(ByVal Value As String)
            _GsName = Value
        End Set
    End Property

    Public Property IsUpdatable() As Boolean
        Get
            Return _IsUpdatable
        End Get
        Set(ByVal Value As Boolean)
            _IsUpdatable = Value
        End Set
    End Property

    Public Property GsValue() As String
        Get
            Return _GsValue
        End Get
        Set(ByVal Value As String)
            _GsValue = Value
        End Set
    End Property



End Class
