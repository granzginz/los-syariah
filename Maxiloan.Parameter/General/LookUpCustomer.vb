
<Serializable()> _
Public Class LookUpCustomer : Inherits Common
    Private _listdata As DataTable
    Private _totalrecords As Decimal
    Public Property TotalRecords() As Decimal
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Decimal)
            _totalrecords = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

End Class
