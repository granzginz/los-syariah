
<Serializable()> _
Public Class Agency : Inherits Maxiloan.Parameter.AccMntBase

    Private _agentid As String
    Private _agentName As String
    Private _agentaddress As String
    Private _isactive As Boolean
    Private _listdata As DataTable
    Private _bankBranchID As Integer

    Public Property AgentId() As String
        Get
            Return _agentid
        End Get
        Set(ByVal Value As String)
            _agentid = Value
        End Set
    End Property
    Public Property AgentName() As String
        Get
            Return _agentName
        End Get
        Set(ByVal Value As String)
            _agentName = Value
        End Set
    End Property
    Public Property AgentAddress() As String
        Get
            Return _agentaddress
        End Get
        Set(ByVal Value As String)
            AgentAddress = Value
        End Set
    End Property
    Public Property IsActive() As Boolean
        Get
            Return _isactive
        End Get
        Set(ByVal Value As Boolean)
            _isactive = Value
        End Set
    End Property

    Public Property BankBranchID() As Integer
        Get
            Return _bankBranchID
        End Get
        Set(ByVal Value As Integer)
            _bankBranchID = Value
        End Set
    End Property





End Class
