
<Serializable()> _
Public Class STNKRenewal : Inherits Maxiloan.Parameter.Common
    Private _BranchAgreement As String
    Private _AssetDescription As String
    Private _SerialNo1 As String
    Private _SerialNo2 As String
    Private _AssetType As String
    Private _ManufacturingYear As String
    Private _OwnerName As String
    Private _OwnerAddress As String
    Private _Color As String
    Private _LicensePlate As String
    Private _TaxDate As String
    Private _Notes As String
    Private _ErrorSave As Boolean
    Private _requestno As String
    Private _agentid As String
    Private _applicationid As String
    Private _reffno As String
    Private _valuedate As Date
    Private _dp As Double
    Private _agentname As String
    Private _admfee As Double
    Private _assetseqno As Integer
    Private _renewalseqno As Integer
    Private _STNKFee As Double
    Private _OtherFee As Double
    Private _AgentFee As Double
    Private _TaxRate As Double
    Private _TaxAmount As Double
    Private _InvoiceNo As String
    Private _InvoiceDate As Date
    Private _RegisterNotes As String
    Private _Requestdate As Date
    Private _APAmountToAgent As Double
    Private _StatusDate As Date
    Private _RequestNotes As String
    Private _TotalAP As Double
    Private _customerid As String
    Private _agreementNo As String
    Private _customerName As String
    Private _listreport As DataSet
    Private _spname As String
    Private _totalrecords As Int64
    Private _listData As DataTable
    Private _STNKDate As DateTime
    Private _renewalType As String
    Private _custReq As String
    Private _estimasiPajak As Double
    Private _estimasiDenda As Double
    Private _estimasiJasa As Double
    Private _estimasiByLain As Double
    Private _TglRencanaBayar As DateTime

    Public Property TglRencanaBayar As DateTime
        Get
            Return _TglRencanaBayar
        End Get
        Set(value As DateTime)
            _TglRencanaBayar = value
        End Set
    End Property

    Public Property BranchAgreement() As String
        Get
            Return _BranchAgreement
        End Get
        Set(ByVal Value As String)
            _BranchAgreement = Value
        End Set
    End Property
    Public Property STNKDate() As DateTime
        Get
            Return _STNKDate
        End Get
        Set(ByVal Value As DateTime)
            _STNKDate = Value
        End Set
    End Property
    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property listData() As DataTable
        Get
            Return _listData
        End Get
        Set(ByVal Value As DataTable)
            _listData = Value
        End Set
    End Property

    Public Property SpName() As String
        Get
            Return _spname
        End Get
        Set(ByVal Value As String)
            _spname = Value
        End Set
    End Property
    Public Property ListReport() As DataSet
        Get
            Return (CType(_listreport, DataSet))
        End Get
        Set(ByVal Value As DataSet)
            _listreport = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return _customerName
        End Get
        Set(ByVal Value As String)
            _customerName = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return _agreementNo
        End Get
        Set(ByVal Value As String)
            _agreementNo = Value
        End Set
    End Property
    Public Property CustomerID() As String
        Get
            Return _customerid
        End Get
        Set(ByVal Value As String)
            _customerid = Value
        End Set
    End Property
    Public Property StatusDate() As Date
        Get
            Return _StatusDate
        End Get
        Set(ByVal Value As Date)
            _StatusDate = Value
        End Set
    End Property

    Public Property RequestNotes() As String
        Get
            Return _RequestNotes
        End Get
        Set(ByVal Value As String)
            _RequestNotes = Value
        End Set
    End Property

    Public Property RegisterNotes() As String
        Get
            Return _RegisterNotes
        End Get
        Set(ByVal Value As String)
            _RegisterNotes = Value
        End Set
    End Property
    Public Property RequestDate() As Date
        Get
            Return _Requestdate
        End Get
        Set(ByVal Value As Date)
            _Requestdate = Value
        End Set
    End Property
    Public Property APAmountToAgent() As Double
        Get
            Return _APAmountToAgent
        End Get
        Set(ByVal Value As Double)
            _APAmountToAgent = Value
        End Set
    End Property
    Public Property AssetSeqNo() As Integer
        Get
            Return _assetseqno
        End Get
        Set(ByVal Value As Integer)
            _assetseqno = Value
        End Set
    End Property
    Public Property RenewalSeqNo() As Integer
        Get
            Return _renewalseqno
        End Get
        Set(ByVal Value As Integer)
            _renewalseqno = Value
        End Set
    End Property
    Public Property STNKFee() As Double
        Get
            Return _STNKFee
        End Get
        Set(ByVal Value As Double)
            _STNKFee = Value
        End Set
    End Property
    Public Property OtherFee() As Double
        Get
            Return _OtherFee
        End Get
        Set(ByVal Value As Double)
            _OtherFee = Value
        End Set
    End Property
    Public Property AgentFee() As Double
        Get
            Return _AgentFee
        End Get
        Set(ByVal Value As Double)
            _AgentFee = Value
        End Set
    End Property
    Public Property TotalAP() As Double
        Get
            Return _TotalAP
        End Get
        Set(ByVal Value As Double)
            _TotalAP = Value
        End Set
    End Property
    Public Property TaxRate() As Double
        Get
            Return _TaxRate
        End Get
        Set(ByVal Value As Double)
            _TaxRate = Value
        End Set
    End Property

    Public Property TaxAmount() As Double
        Get
            Return _TaxAmount
        End Get
        Set(ByVal Value As Double)
            _TaxAmount = Value
        End Set
    End Property


    Public Property AgentName() As String
        Get
            Return _agentname
        End Get
        Set(ByVal Value As String)
            _agentname = Value
        End Set
    End Property
    Public Property AdmFee() As Double
        Get
            Return (CType(_admfee, Double))
        End Get
        Set(ByVal Value As Double)
            _admfee = Value
        End Set
    End Property
    Public Property DP() As Double
        Get
            Return (CType(_dp, Double))
        End Get
        Set(ByVal Value As Double)
            _dp = Value
        End Set
    End Property
    Public Property ErrorSave() As Boolean
        Get
            Return _ErrorSave
        End Get
        Set(ByVal Value As Boolean)
            _ErrorSave = Value
        End Set
    End Property
    Public Property RequestNo() As String
        Get
            Return _requestno
        End Get
        Set(ByVal Value As String)
            _requestno = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return _applicationid
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property
    Public Property ReffNo() As String
        Get
            Return _reffno
        End Get
        Set(ByVal Value As String)
            _reffno = Value
        End Set
    End Property
    Public Property AgentID() As String
        Get
            Return _agentid
        End Get
        Set(ByVal Value As String)
            _agentid = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property
    Public Property AssetDescription() As String
        Get
            Return _AssetDescription
        End Get
        Set(ByVal Value As String)
            _AssetDescription = Value
        End Set
    End Property
    Public Property SerialNo1() As String
        Get
            Return _SerialNo1
        End Get
        Set(ByVal Value As String)
            _SerialNo1 = Value
        End Set
    End Property
    Public Property SerialNo2() As String
        Get
            Return _SerialNo2
        End Get
        Set(ByVal Value As String)
            _SerialNo2 = Value
        End Set
    End Property
    Public Property AssetType() As String
        Get
            Return _AssetType
        End Get
        Set(ByVal Value As String)
            _AssetType = Value
        End Set
    End Property
    Public Property ManufacturingYear() As String
        Get
            Return _ManufacturingYear
        End Get
        Set(ByVal Value As String)
            _ManufacturingYear = Value
        End Set
    End Property
    Public Property OwnerName() As String
        Get
            Return _OwnerName
        End Get
        Set(ByVal Value As String)
            _OwnerName = Value
        End Set
    End Property
    Public Property OwnerAddress() As String
        Get
            Return _OwnerAddress
        End Get
        Set(ByVal Value As String)
            _OwnerAddress = Value
        End Set
    End Property
    Public Property Color() As String
        Get
            Return _Color
        End Get
        Set(ByVal Value As String)
            _Color = Value
        End Set
    End Property
    Public Property LicensePlate() As String
        Get
            Return _LicensePlate
        End Get
        Set(ByVal Value As String)
            _LicensePlate = Value
        End Set
    End Property
    Public Property TaxDate() As String
        Get
            Return _TaxDate
        End Get
        Set(ByVal Value As String)
            _TaxDate = Value
        End Set
    End Property
    Public Property ValueDate() As Date
        Get
            Return _valuedate
        End Get
        Set(ByVal Value As Date)
            _valuedate = Value
        End Set
    End Property
    Public Property InvoiceNo() As String
        Get
            Return _InvoiceNo
        End Get
        Set(ByVal Value As String)
            _InvoiceNo = Value
        End Set
    End Property
    Public Property InvoiceDate() As Date
        Get
            Return _InvoiceDate
        End Get
        Set(ByVal Value As Date)
            _InvoiceDate = Value
        End Set
    End Property

    Public Property RenewalType() As String
        Get
            Return _renewalType
        End Get
        Set(ByVal Value As String)
            _renewalType = Value
        End Set
    End Property

    Public Property CustReq() As String
        Get
            Return _custReq
        End Get
        Set(ByVal Value As String)
            _custReq = Value
        End Set
    End Property

    Public Property EstimasiPajak() As Double
        Get
            Return _estimasiPajak
        End Get
        Set(ByVal Value As Double)
            _estimasiPajak = Value
        End Set
    End Property

    Public Property EstimasiDenda() As Double
        Get
            Return _estimasiDenda
        End Get
        Set(ByVal Value As Double)
            _estimasiDenda = Value
        End Set
    End Property

    Public Property EstimasiJasa() As Double
        Get
            Return _estimasiJasa
        End Get
        Set(ByVal Value As Double)
            _estimasiJasa = Value
        End Set
    End Property

    Public Property EstimasiByLain() As Double
        Get
            Return _estimasiByLain
        End Get
        Set(ByVal Value As Double)
            _estimasiByLain = Value
        End Set
    End Property

    Public Property STNKFeePaid As Decimal
End Class
