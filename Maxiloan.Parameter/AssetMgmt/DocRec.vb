
<Serializable()> _
Public Class DocRec
    Inherits Maxiloan.Parameter.Common
#Region "Constanta"
#Region "DocReceive"

    Private _listDoc As DataTable
    Private _listData As DataTable
    Private _listReport As DataSet
    Private _strError As String
    Private _sesBranchID As String
    Private _assetDesc As String
    Private _Supplierid As String
    Private _SupplierName As String
    Private _ReceiveDate As Date
    Private _ReceiveFrom As String
    Private _ChasisNo As String
    Private _EngineNo As String
    Private _Color As String
    Private _LicensePlate As String
    Private _taxdate As Date
    Private _hasil As Integer
    Private _RackLoc As String
    Private _FillingLoc As String
    Private _AssetTypeID As String
    Private _AssetCode As String
    Private _AssetStatus As String
    Private _AssetDocStatus As String
    Private _AssetStatusdesc As String
    Private _AssetDocStatDesc As String
    Private _ApplicationId As String
    Private _AgreementNo As String
    Private _seriallabel1 As String
    Private _seriallabel2 As String
    Private _Customername As String
    Private _CustomerID As String
    Private _IsMainDoc As Byte
    Private _isDocExist As Byte
    Private _strGroupId As String
    Private _AssetSeqNo As Integer
    Private _RackLocID As String
    Private _FLocID As String
    Private _FundingCoName As String
    Private _FundingPledgeStatus As String

    Private _OwnerAsset As String
    Private _OwnerAddress As String
    Private _OwnerRT As String
    Private _OwnerRW As String
    Private _OwnerKelurahan As String
    Private _OwnerKecamatan As String
    Private _OwnerCity As String
    Private _OwnerZipCode As String

    Private _BranchInTransit As String
    Private _Period As DateTime
    Private _LabelName As String
    Private _ContentName As String
    Private _RackDesc As String
    Private _FillingDesc As String
    Private _DocumentNo As String
    Private _DocumentDate As DateTime
    Private _ValueDate As DateTime
    Private _tDate As String
    Private _AssetUsage As String
    Private _AssetUsageDesc As String
    'Private _IsIzinTrayek As Byte
#End Region

#Region "DocBorrow"
    Private _BorrowDate As Date
    Private _BorrowBy As String
    Private _Notes As String
    Private _ReturnDate As Date
    Private _Itaxdate As String
#End Region
#Region "DocRelease"
    Private _CrossDefault As String
    Private _ReleaseDate As Date
#End Region
#Region "Setting"
    Private _IsActive As Byte
    Private _IsFunding As Byte
    Private _IsAdd As String
    Private _Desc As String
#End Region
#Region "DocPledging"
    Private _ArrApplicationId As String()
    Private _TotalAgreementPledge As Integer
    Private _SpName As String
    Private _totalrecords As Int64
    Private _ListDataReport As DataSet
    Private _FundingBatchNo As String
    Private _FundingContractNo As String
    Private _FundingCoyID As String

#End Region
#End Region

#Region "DocReceive"
    Public Property tDate() As String
        Get
            Return CType(_ValueDate, String)
        End Get
        Set(ByVal Value As String)
            _tDate = Value
        End Set
    End Property
    Public Property ValueDate() As DateTime
        Get
            Return CType(_ValueDate, DateTime)
        End Get
        Set(ByVal Value As DateTime)
            _ValueDate = Value
        End Set
    End Property
    Public Property DocumentDate() As DateTime
        Get
            Return CType(_DocumentDate, DateTime)
        End Get
        Set(ByVal Value As DateTime)
            _DocumentDate = Value
        End Set
    End Property
    Public Property DocumentNo() As String
        Get
            Return CType(_DocumentNo, String)
        End Get
        Set(ByVal Value As String)
            _DocumentNo = Value
        End Set
    End Property
    Public Property RackDesc() As String
        Get
            Return CType(_RackDesc, String)
        End Get
        Set(ByVal Value As String)
            _RackDesc = Value
        End Set
    End Property
    Public Property FillingDesc() As String
        Get
            Return CType(_FillingDesc, String)
        End Get
        Set(ByVal Value As String)
            _FillingDesc = Value
        End Set
    End Property
    Public Property hasil() As Integer
        Get
            Return CType(_hasil, Integer)
        End Get
        Set(ByVal Value As Integer)
            _hasil = Value
        End Set
    End Property
    Public Property sesBranchID() As String
        Get
            Return CType(_sesBranchID, String)
        End Get
        Set(ByVal Value As String)
            _sesBranchID = Value
        End Set
    End Property
    Public Property LabelName() As String
        Get
            Return CType(_LabelName, String)
        End Get
        Set(ByVal Value As String)
            _LabelName = Value
        End Set
    End Property
    Public Property ContentName() As String
        Get
            Return CType(_ContentName, String)
        End Get
        Set(ByVal Value As String)
            _ContentName = Value
        End Set
    End Property
    Public Property Period() As DateTime
        Get
            Return CType(_Period, DateTime)
        End Get
        Set(ByVal Value As DateTime)
            _Period = Value
        End Set
    End Property
    Public Property listDoc() As DataTable
        Get
            Return CType(_listDoc, DataTable)
        End Get
        Set(ByVal Value As DataTable)
            _listDoc = Value
        End Set
    End Property
    Public Property listReport() As DataSet
        Get
            Return CType(_listReport, DataSet)
        End Get
        Set(ByVal Value As DataSet)
            _listReport = Value
        End Set
    End Property
    Public Property listData() As DataTable
        Get
            Return CType(_listData, DataTable)
        End Get
        Set(ByVal Value As DataTable)
            _listData = Value
        End Set
    End Property

    Public Property strError() As String
        Get
            Return CType(_strError, String)
        End Get
        Set(ByVal Value As String)
            _strError = Value
        End Set
    End Property
    Public Property BranchInTransit() As String
        Get
            Return CType(_BranchInTransit, String)
        End Get
        Set(ByVal Value As String)
            _BranchInTransit = Value
        End Set
    End Property

    Public Property assetDesc() As String
        Get
            Return CType(_assetDesc, String)
        End Get
        Set(ByVal Value As String)
            _assetDesc = Value
        End Set
    End Property

    Public Property Supplierid() As String
        Get
            Return CType(_Supplierid, String)
        End Get
        Set(ByVal Value As String)
            _Supplierid = Value
        End Set
    End Property

    Public Property SupplierName() As String
        Get
            Return CType(_SupplierName, String)
        End Get
        Set(ByVal Value As String)
            _SupplierName = Value
        End Set
    End Property


    Public Property ReceiveDate() As Date
        Get
            Return CType(_ReceiveDate, Date)
        End Get
        Set(ByVal Value As Date)
            _ReceiveDate = Value
        End Set
    End Property
    Private _TanggalKembali As Date
    Public Property TanggalKembali() As Date
        Get
            Return CType(_TanggalKembali, Date)
        End Get
        Set(ByVal Value As Date)
            _TanggalKembali = Value
        End Set
    End Property
    Public Property ReceiveFrom() As String
        Get
            Return CType(_ReceiveFrom, String)
        End Get
        Set(ByVal Value As String)
            _ReceiveFrom = Value
        End Set
    End Property

    Public Property ChasisNo() As String
        Get
            Return CType(_ChasisNo, String)
        End Get
        Set(ByVal Value As String)
            _ChasisNo = Value
        End Set
    End Property

    Public Property EngineNo() As String
        Get
            Return CType(_EngineNo, String)
        End Get
        Set(ByVal Value As String)
            _EngineNo = Value
        End Set
    End Property
    Public Property Color() As String
        Get
            Return CType(_Color, String)
        End Get
        Set(ByVal Value As String)
            _Color = Value
        End Set
    End Property
    Public Property LicensePlate() As String
        Get
            Return CType(_LicensePlate, String)
        End Get
        Set(ByVal Value As String)
            _LicensePlate = Value
        End Set
    End Property

    Public Property taxdate() As Date
        Get
            Return CType(_taxdate, Date)
        End Get
        Set(ByVal Value As Date)
            _taxdate = Value
        End Set
    End Property

    Public Property RackLoc() As String
        Get
            Return CType(_RackLoc, String)
        End Get
        Set(ByVal Value As String)
            _RackLoc = Value
        End Set
    End Property

    Public Property FillingLoc() As String
        Get
            Return CType(_FillingLoc, String)
        End Get
        Set(ByVal Value As String)
            _FillingLoc = Value
        End Set
    End Property

    Public Property AssetTypeID() As String
        Get
            Return CType(_AssetTypeID, String)
        End Get
        Set(ByVal Value As String)
            _AssetTypeID = Value
        End Set
    End Property

    Public Property AssetCode() As String
        Get
            Return CType(_AssetCode, String)
        End Get
        Set(ByVal Value As String)
            _AssetCode = Value
        End Set
    End Property

    Public Property AssetStatus() As String
        Get
            Return CType(_AssetStatus, String)
        End Get
        Set(ByVal Value As String)
            _AssetStatus = Value
        End Set
    End Property

    Public Property AssetDocStatus() As String
        Get
            Return CType(_AssetDocStatus, String)
        End Get
        Set(ByVal Value As String)
            _AssetDocStatus = Value
        End Set
    End Property

    Public Property AssetStatusdesc() As String
        Get
            Return CType(_AssetStatusdesc, String)
        End Get
        Set(ByVal Value As String)
            _AssetStatusdesc = Value
        End Set
    End Property


    Public Property AssetDocStatDesc() As String
        Get
            Return CType(_AssetDocStatDesc, String)
        End Get
        Set(ByVal Value As String)
            _AssetDocStatDesc = Value
        End Set
    End Property

    Public Property ApplicationId() As String
        Get
            Return CType(_ApplicationId, String)
        End Get
        Set(ByVal Value As String)
            _ApplicationId = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return CType(_AgreementNo, String)
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property

    Public Property seriallabel1() As String
        Get
            Return CType(_seriallabel1, String)
        End Get
        Set(ByVal Value As String)
            _seriallabel1 = Value
        End Set
    End Property

    Public Property seriallabel2() As String
        Get
            Return CType(_seriallabel2, String)
        End Get
        Set(ByVal Value As String)
            _seriallabel2 = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return CType(_CustomerID, String)
        End Get
        Set(ByVal Value As String)
            _CustomerID = Value
        End Set
    End Property

    Public Property Customername() As String
        Get
            Return CType(_Customername, String)
        End Get
        Set(ByVal Value As String)
            _Customername = Value
        End Set
    End Property

    Public Property isMainDoc() As Byte
        Get
            Return CType(_IsMainDoc, Byte)
        End Get
        Set(ByVal Value As Byte)
            _IsMainDoc = Value
        End Set
    End Property
    Public Property isDocExist() As Byte
        Get
            Return CType(_isDocExist, Byte)
        End Get
        Set(ByVal Value As Byte)
            _isDocExist = Value
        End Set
    End Property

    Public Property strGroupId() As String
        Get
            Return CType(_strGroupId, String)
        End Get
        Set(ByVal Value As String)
            _strGroupId = Value
        End Set
    End Property

    Public Property AssetSeqNo() As Integer
        Get
            Return CType(_AssetSeqNo, Integer)
        End Get
        Set(ByVal Value As Integer)
            _AssetSeqNo = Value
        End Set
    End Property

    Public Property FLocID() As String
        Get
            Return CType(_FLocID, String)
        End Get
        Set(ByVal Value As String)
            _FLocID = Value
        End Set
    End Property

    Public Property RackLocID() As String
        Get
            Return CType(_RackLocID, String)
        End Get
        Set(ByVal Value As String)
            _RackLocID = Value
        End Set
    End Property

    Public Property FundingCoName() As String
        Get
            Return CType(_FundingCoName, String)
        End Get
        Set(ByVal Value As String)
            _FundingCoName = Value
        End Set
    End Property

    Public Property FundingPledgeStatus() As String
        Get
            Return CType(_FundingPledgeStatus, String)
        End Get
        Set(ByVal Value As String)
            _FundingPledgeStatus = Value
        End Set
    End Property
    Public Property OwnerAsset() As String
        Get
            Return CType(_OwnerAsset, String)
        End Get
        Set(ByVal Value As String)
            _OwnerAsset = Value
        End Set
    End Property
    Public Property OwnerAddress() As String
        Get
            Return CType(_OwnerAddress, String)
        End Get
        Set(ByVal Value As String)
            _OwnerAddress = Value
        End Set
    End Property
    Public Property OwnerRT() As String
        Get
            Return CType(_OwnerRT, String)
        End Get
        Set(ByVal Value As String)
            _OwnerRT = Value
        End Set
    End Property
    Public Property OwnerRW() As String
        Get
            Return CType(_OwnerRW, String)
        End Get
        Set(ByVal Value As String)
            _OwnerRW = Value
        End Set
    End Property
    Public Property OwnerKelurahan() As String
        Get
            Return CType(_OwnerKelurahan, String)
        End Get
        Set(ByVal Value As String)
            _OwnerKelurahan = Value
        End Set
    End Property
    Public Property OwnerKecamatan() As String
        Get
            Return CType(_OwnerKecamatan, String)
        End Get
        Set(ByVal Value As String)
            _OwnerKecamatan = Value
        End Set
    End Property
    Public Property OwnerCity() As String
        Get
            Return CType(_OwnerCity, String)
        End Get
        Set(ByVal Value As String)
            _OwnerCity = Value
        End Set
    End Property
    Public Property OwnerZipCode() As String
        Get
            Return CType(_OwnerZipCode, String)
        End Get
        Set(ByVal Value As String)
            _OwnerZipCode = Value
        End Set
    End Property

    Public Property AssetUsage() As String
        Get
            Return CType(_AssetUsage, String)
        End Get
        Set(ByVal Value As String)
            _AssetUsage = Value
        End Set
    End Property

    Public Property AssetUsageDesc() As String
        Get
            Return CType(_AssetUsageDesc, String)
        End Get
        Set(ByVal Value As String)
            _AssetUsageDesc = Value
        End Set
    End Property

    'Public Property IsIzinTrayek() As Byte
    '    Get
    '        Return CType(_IsIzinTrayek, Byte)
    '    End Get
    '    Set(ByVal Value As Byte)
    '        _IsIzinTrayek = Value
    '    End Set
    'End Property
#End Region

#Region "DocBorrow"
    Public Property BorrowDate() As Date
        Get
            Return CType(_BorrowDate, Date)
        End Get
        Set(ByVal Value As Date)
            _BorrowDate = Value
        End Set
    End Property

    Public Property BorrowBy() As String
        Get
            Return CType(_BorrowBy, String)
        End Get
        Set(ByVal Value As String)
            _BorrowBy = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return CType(_Notes, String)
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property

    Public Property ReturnDate() As Date
        Get
            Return CType(_ReturnDate, Date)
        End Get
        Set(ByVal Value As Date)
            _ReturnDate = Value
        End Set
    End Property

    Public Property Itaxdate() As String
        Get
            Return CType(_Itaxdate, String)
        End Get
        Set(ByVal Value As String)
            _Itaxdate = Value
        End Set
    End Property

#End Region
#Region "DocRelease"
    Public Property CrossDefault() As String
        Get
            Return CType(_CrossDefault, String)
        End Get
        Set(ByVal Value As String)
            _CrossDefault = Value
        End Set
    End Property

    Public Property ReleaseDate() As Date
        Get
            Return CType(_ReleaseDate, Date)
        End Get
        Set(ByVal Value As Date)
            _ReleaseDate = Value
        End Set
    End Property
#End Region

#Region "Setting"
    Public Property IsActive() As Byte
        Get
            Return CType(_IsActive, Byte)
        End Get
        Set(ByVal Value As Byte)
            _IsActive = Value
        End Set
    End Property
    Public Property IsFunding() As Byte
        Get
            Return CType(_IsFunding, Byte)
        End Get
        Set(ByVal Value As Byte)
            _IsFunding = Value
        End Set
    End Property
    Public Property IsAdd() As String
        Get
            Return CType(_IsAdd, String)
        End Get
        Set(ByVal Value As String)
            _IsAdd = Value
        End Set
    End Property


    Public Property Desc() As String
        Get
            Return CType(_Desc, String)
        End Get
        Set(ByVal Value As String)
            _Desc = Value
        End Set
    End Property
#End Region
#Region "DOCPledging"
    Public Property ArrApplicationId() As String()
        Get
            Return CType(_ArrApplicationId, String())
        End Get
        Set(ByVal Value As String())
            _ArrApplicationId = Value
        End Set
    End Property
    Public Property totalAgreementPledge() As Integer
        Get
            Return CType(_TotalAgreementPledge, Integer)
        End Get
        Set(ByVal Value As Integer)
            _TotalAgreementPledge = Value
        End Set
    End Property
    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property SpName() As String
        Get
            Return _SpName
        End Get
        Set(ByVal Value As String)
            _SpName = Value
        End Set
    End Property
    Public Property ListDataReport() As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(ByVal Value As DataSet)
            _ListDataReport = Value
        End Set
    End Property
    Public Property FundingBatchNo() As String
        Get
            Return _FundingBatchNo
        End Get
        Set(ByVal Value As String)
            _FundingBatchNo = Value
        End Set
    End Property
    Public Property FundingContractNo() As String
        Get
            Return _FundingContractNo
        End Get
        Set(ByVal Value As String)
            _FundingContractNo = Value
        End Set
    End Property
    Public Property FundingCoyID() As String
        Get
            Return _FundingCoyID
        End Get
        Set(ByVal Value As String)
            _FundingCoyID = Value
        End Set
    End Property

#End Region

    Public Property NamaBPKBSama As Boolean
    Public Property IsIzinTrayek As Boolean
    Public Property HasilCekBPKB As String
    Public Property HasilCekNote As String
    Public Property DocInFunding As Boolean
    Public Property KondisiAset As String
    
    Public Property DocPinjamDariBank As String 'GF => Pinjam dari Bank ; FG=> Kembali ke Bank

    Public Property IsTbo As Boolean
    Public Property BranchOnStatus As String

End Class
