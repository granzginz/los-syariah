﻿
<Serializable()>
Public Class StockOpDoc
    Inherits Maxiloan.Parameter.Common
    Public Property DataSetResult As DataSet
    Public Property TotalRecords As Long

    Public Property StockOpnameNo As String
    Public Property OpnameDate As DateTime
    Private _DocInformation As DataTable

    Public Property DocInformation() As DataTable
        Get
            Return CType(_DocInformation, DataTable)
        End Get
        Set(ByVal Value As DataTable)
            _DocInformation = Value
        End Set
    End Property
End Class
