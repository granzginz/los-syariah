
<Serializable()> _
Public Class InsEndorsment : Inherits Common
    Private _datadetil As DataTable
    Private _refundtocust As Decimal
    Private _newpremicust As Decimal
    Private _additionalcust As Decimal
    Private _additionalinsco As Decimal
    Private _endorsmentletter As String
    Private _insseqno As Integer
    Private _counter As Integer
    Private _applicationtype As String
    Private _customerid As String
    Private _assetseqno As Integer
    Private _listdata As DataTable
    Private _listdataset As DataSet
    Private _totalrecords As Int64
    Private _spName As String
    Private _MaskAssBranchID As String
    Private _PolicyNumber As String
    Private _ManufacturingYear As String
    Private _Usage As String
    Private _AssetUsage As String
    Private _SerialNo1Label As String
    Private _SerialNo2Label As String
    Private _SerialNo1 As String
    Private _SerialNo2 As String
    Private _InsLength As String
    Private _InsNotes As String
    Private _AccNotes As String
    Private _StartDate As String
    Private _EndDate As String
    Private _BDEndorsDate As DateTime
    Private _ContractPrepaidAmount As Decimal
    Private _InsuranceType As String
    Private _UsedNew As String
    Private _agreementno As String
    Private _name As String
    Private _Tenor As String
    Private _MainPremiumToCust As Decimal
    Private _MainPremiumToInsCo As Decimal
    Private _PremiumAmountByCust As Decimal
    Private _PremiumAmountToInsCo As Decimal
    Private _NewRateToCust As Decimal
    Private _YearActive As String
    Private _New_BasicToCust As Decimal
    Private _Refund_BasicToCust As Decimal
    Private _New_SRCC As Decimal
    Private _New_Flood As Decimal
    Private _AdminFee As Decimal
    Private _MateraiFee As Decimal
    Private _SRCC_OK As Boolean
    Private _Flood_OK As Boolean
    Private _CoverageType As String
    Private _OldCoverageType As String
    Private _SumInsured As Decimal
    Private _OldSumInsured As Decimal
    Private _YearNum As Integer
    Private _description As String
    Private _discount As Decimal
    Private _refundtoinsco As Decimal


    Public Property EndorsmentLetter() As String
        Get
            Return _endorsmentletter
        End Get
        Set(ByVal Value As String)
            _endorsmentletter = Value
        End Set
    End Property
    Public Property RefundToCust() As Decimal
        Get
            Return _refundtocust
        End Get
        Set(ByVal Value As Decimal)
            _refundtocust = Value
        End Set
    End Property
    Public Property NewPremiCust() As Decimal
        Get
            Return _newpremicust
        End Get
        Set(ByVal Value As Decimal)
            _newpremicust = Value
        End Set
    End Property
    Public Property AdditionalCust() As Decimal
        Get
            Return _additionalcust
        End Get
        Set(ByVal Value As Decimal)
            _additionalcust = Value
        End Set
    End Property
    Public Property AdditionalInsco() As Decimal
        Get
            Return _additionalinsco
        End Get
        Set(ByVal Value As Decimal)
            _additionalinsco = Value
        End Set
    End Property
    Public Property Discount() As Decimal
        Get
            Return _discount
        End Get
        Set(ByVal Value As Decimal)
            _discount = Value
        End Set
    End Property
    Public Property Counter() As Integer
        Get
            Return _counter
        End Get
        Set(ByVal Value As Integer)
            _counter = Value
        End Set
    End Property
    Public Property InsSeqNo() As Integer
        Get
            Return _insseqno
        End Get
        Set(ByVal Value As Integer)
            _insseqno = Value
        End Set
    End Property
    Public Property AssetSeqNo() As Integer
        Get
            Return _assetseqno
        End Get
        Set(ByVal Value As Integer)
            _assetseqno = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal Value As String)
            _name = Value
        End Set
    End Property
    Public Property RefundToInsCo() As Decimal
        Get
            Return _refundtoinsco
        End Get
        Set(ByVal Value As Decimal)
            _refundtoinsco = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return _agreementno
        End Get
        Set(ByVal Value As String)
            _agreementno = Value
        End Set
    End Property
    Public Property CustomerId() As String
        Get
            Return _customerid
        End Get
        Set(ByVal Value As String)
            _customerid = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property CoverageType() As String
        Get
            Return _CoverageType
        End Get
        Set(ByVal Value As String)
            _CoverageType = Value
        End Set
    End Property
    Public Property OldCoverageType() As String
        Get
            Return _OldCoverageType
        End Get
        Set(ByVal Value As String)
            _OldCoverageType = Value
        End Set
    End Property
    Public Property SumInsured() As Decimal
        Get
            Return _SumInsured
        End Get
        Set(ByVal Value As Decimal)
            _SumInsured = Value
        End Set
    End Property
    Public Property OldSumInsured() As Decimal
        Get
            Return _OldSumInsured
        End Get
        Set(ByVal Value As Decimal)
            _OldSumInsured = Value
        End Set
    End Property
    Public Property YearNum() As Integer
        Get
            Return _YearNum
        End Get
        Set(ByVal Value As Integer)
            _YearNum = Value
        End Set
    End Property

    Public Property Flood_OK() As Boolean
        Get
            Return _Flood_OK
        End Get
        Set(ByVal Value As Boolean)
            _Flood_OK = Value
        End Set
    End Property
    Public Property SRCC_OK() As Boolean
        Get
            Return _SRCC_OK
        End Get
        Set(ByVal Value As Boolean)
            _SRCC_OK = Value
        End Set
    End Property


    Public Property New_SRCC() As Decimal
        Get
            Return _New_SRCC
        End Get
        Set(ByVal Value As Decimal)
            _New_SRCC = Value
        End Set
    End Property
    Public Property New_Flood() As Decimal
        Get
            Return _New_Flood
        End Get
        Set(ByVal Value As Decimal)
            _New_Flood = Value
        End Set
    End Property
    Public Property AdminFee() As Decimal
        Get
            Return _AdminFee
        End Get
        Set(ByVal Value As Decimal)
            _AdminFee = Value
        End Set
    End Property
    Public Property MateraiFee() As Decimal
        Get
            Return _MateraiFee
        End Get
        Set(ByVal Value As Decimal)
            _MateraiFee = Value
        End Set
    End Property
    Public Property ApplicationType() As String
        Get
            Return _applicationtype
        End Get
        Set(ByVal Value As String)
            _applicationtype = Value
        End Set
    End Property

    Public Property YearActive() As String
        Get
            Return _YearActive
        End Get
        Set(ByVal Value As String)
            _YearActive = Value
        End Set
    End Property

    Public Property MainPremiumToCust() As Decimal
        Get
            Return _MainPremiumToCust
        End Get
        Set(ByVal Value As Decimal)
            _MainPremiumToCust = Value
        End Set
    End Property

    Public Property New_BasicToCust() As Decimal
        Get
            Return _New_BasicToCust
        End Get
        Set(ByVal Value As Decimal)
            _New_BasicToCust = Value
        End Set
    End Property

    Public Property Refund_BasicToCust() As Decimal
        Get
            Return _Refund_BasicToCust
        End Get
        Set(ByVal Value As Decimal)
            _Refund_BasicToCust = Value
        End Set
    End Property

    Public Property MainPremiumToInsCo() As Decimal
        Get
            Return _MainPremiumToInsCo
        End Get
        Set(ByVal Value As Decimal)
            _MainPremiumToInsCo = Value
        End Set
    End Property
    Public Property PremiumAmountByCust() As Decimal
        Get
            Return _PremiumAmountByCust
        End Get
        Set(ByVal Value As Decimal)
            _PremiumAmountByCust = Value
        End Set
    End Property

    Public Property PremiumAmountToInsCo() As Decimal
        Get
            Return _PremiumAmountToInsCo
        End Get
        Set(ByVal Value As Decimal)
            _PremiumAmountToInsCo = Value
        End Set
    End Property

    Public Property NewRateToCust() As Decimal
        Get
            Return _NewRateToCust
        End Get
        Set(ByVal Value As Decimal)
            _NewRateToCust = Value
        End Set
    End Property

    Public Property Tenor() As String
        Get
            Return _Tenor
        End Get
        Set(ByVal Value As String)
            _Tenor = Value
        End Set
    End Property

    Public Property UsedNew() As String
        Get
            Return _UsedNew
        End Get
        Set(ByVal Value As String)
            _UsedNew = Value
        End Set
    End Property
    Public Property spName() As String
        Get
            Return _spName
        End Get
        Set(ByVal Value As String)
            _spName = Value
        End Set
    End Property
    Public Property InsuranceType() As String
        Get
            Return _InsuranceType
        End Get
        Set(ByVal Value As String)
            _InsuranceType = Value
        End Set
    End Property
    Public Property MaskAssBranchID() As String
        Get
            Return _MaskAssBranchID
        End Get
        Set(ByVal Value As String)
            _MaskAssBranchID = Value
        End Set
    End Property
    Public Property PolicyNumber() As String
        Get
            Return _PolicyNumber
        End Get
        Set(ByVal Value As String)
            _PolicyNumber = Value
        End Set
    End Property
    Public Property ManufacturingYear() As String
        Get
            Return _ManufacturingYear
        End Get
        Set(ByVal Value As String)
            _ManufacturingYear = Value
        End Set
    End Property
    Public Property Usage() As String
        Get
            Return _Usage
        End Get
        Set(ByVal Value As String)
            _Usage = Value
        End Set
    End Property
    Public Property AssetUsage() As String
        Get
            Return _AssetUsage
        End Get
        Set(ByVal Value As String)
            _AssetUsage = Value
        End Set
    End Property
    Public Property SerialNo1Label() As String
        Get
            Return _SerialNo1Label
        End Get
        Set(ByVal Value As String)
            _SerialNo1Label = Value
        End Set
    End Property
    Public Property SerialNo2Label() As String
        Get
            Return _SerialNo2Label
        End Get
        Set(ByVal Value As String)
            _SerialNo2Label = Value
        End Set
    End Property
    Public Property SerialNo1() As String
        Get
            Return _SerialNo1
        End Get
        Set(ByVal Value As String)
            _SerialNo1 = Value
        End Set
    End Property
    Public Property SerialNo2() As String
        Get
            Return _SerialNo2
        End Get
        Set(ByVal Value As String)
            _SerialNo2 = Value
        End Set
    End Property
    Public Property InsLength() As String
        Get
            Return _InsLength
        End Get
        Set(ByVal Value As String)
            _InsLength = Value
        End Set
    End Property
    Public Property InsNotes() As String
        Get
            Return _InsNotes
        End Get
        Set(ByVal Value As String)
            _InsNotes = Value
        End Set
    End Property
    Public Property AccNotes() As String
        Get
            Return _AccNotes
        End Get
        Set(ByVal Value As String)
            _AccNotes = Value
        End Set
    End Property
    Public Property StartDate() As String
        Get
            Return _StartDate
        End Get
        Set(ByVal Value As String)
            _StartDate = Value
        End Set
    End Property
    Public Property EndDate() As String
        Get
            Return _EndDate
        End Get
        Set(ByVal Value As String)
            _EndDate = Value
        End Set
    End Property
    Public Property BDEndorsDate() As DateTime
        Get
            Return _BDEndorsDate
        End Get
        Set(ByVal Value As DateTime)
            _BDEndorsDate = Value
        End Set
    End Property
    Public Property ContractPrepaidAmount() As Decimal
        Get
            Return _ContractPrepaidAmount
        End Get
        Set(ByVal Value As Decimal)
            _ContractPrepaidAmount = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property DataDetil() As DataTable
        Get
            Return _datadetil
        End Get
        Set(ByVal Value As DataTable)
            _datadetil = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property ListDataSet() As DataSet
        Get
            Return _listdataset
        End Get
        Set(ByVal Value As DataSet)
            _listdataset = Value
        End Set
    End Property


End Class
