﻿ 
<Serializable()> _
Public Class MaskAsuransi
    Private _insuranceType As IList(Of InsuranceCoverType) = New List(Of InsuranceCoverType)
    'Private _MaskAssID As String
    'Private _MaskAssBranchID As String
    'Private _MaskAssBranchName As String
    'Private _AdminFee As String
    'Private _StampDutyFee As String
    'Private _LoadingFeeAllRisk As String
    'Private _LoadingFeeTLO As String
    'Private _AllRiskOptions As String
    'Private _TLOOptions As String
    'Private _Komisi As String
    'Private _ImbalanJasa As String
    'Private _PPh23 As String
    'Private _PPN As String
    'Private _PPh23CertifiedByIns As String
    'Private _PPNCertifiedByIns As String


    Public Property MaskAssID As String
    Public Property MaskAssBranchID As String
    Public Property MaskAssBranchName As String
    Public Property MaskAssBranchNameDesc As String
    Public Property MaskAssBranchCity As String

    Public Property AdminFee As Double
    Public Property StampDutyFee As Double
    Public Property LoadingFeeAllRisk As Integer
    Public Property LoadingFeeTLO As Integer
    Public Property AllRiskOptions As String
    Public Property TLOOptions As String
    Public Property Komisi As Double
    Public Property ImbalanJasa As Double
    Public Property PPh23 As Double
    Public Property PPN As Double
    Public Property PPh23CertifiedByIns As Boolean
    Public Property PPNCertifiedByIns As Boolean

    Public Property MAINPREMIUM As Double
    Public Property SRCCPREMIUM As Double
    Public Property TPLPREMIUM As Double
    Public Property FLOODPREMIUM As Double
    Public Property LOADINGFEE As Double
    Public Property PAPREMIUM As Double
    Public Property TOTAL As Double
    Public Property PADriverPremium As Double
    Public Property EQVETPremium As Double
    Public Property TerrorismPremium As Double
     
    Public Property InsuranceCoverTypes As IList(Of InsuranceCoverType)
        Get
            Return _insuranceType
        End Get
        Set(value As IList(Of InsuranceCoverType))
            _insuranceType = value
        End Set
    End Property 
End Class

<Serializable()> _
Public Class InsuranceCoverType
    Public Property YEARInsurance As Integer
    Public Property COVERAGETYPE As String
    Public Property PREMIUM As Double
    Public Property PAIDBYCUST As String

    Public Property SRCC As Boolean
    Public Property TPL As Boolean
    Public Property FLOOD As Boolean
    Public Property Riot As Boolean
    Public Property PA As Boolean
    Public Property PADriver As Boolean
    Public Property EQVET As Boolean
    Public Property TERROR As Boolean


    Public Property SRCCAMount As Double
    Public Property TPLAmount As Double
    Public Property FloodAmount As Double
    Public Property LoadingFee As Double
    Public Property RiotAmount As Double
    Public Property PAAmount As Double
    Public Property PADriverAmount As Double
    Public Property TOTAL As Double

End Class


<Serializable()> _
Public Class MaskAssCalcEng
    Private _maskAsuransi As IList(Of MaskAsuransi) = New List(Of MaskAsuransi) 
    Private _insurancecvrType As IList(Of InsuranceCoverType) = New List(Of InsuranceCoverType)
    Public strConnection As String

    Public Property MaskAss As IList(Of MaskAsuransi)
        Get
            Return _maskAsuransi
        End Get
        Set(value As IList(Of MaskAsuransi))
            _maskAsuransi = value
        End Set 
    End Property

    Public Property InsuranceCoverType As IList(Of InsuranceCoverType)
        Get
            Return _insurancecvrType
        End Get
        Set(value As IList(Of InsuranceCoverType))
            _insurancecvrType = value
        End Set
    End Property

    Public Property BranchId As String
    Public Property ApplicationId As String
    Public Property MaskAssBranchIDChoosed As String 
    Public Property ApplicationType As String

    Public Property UsageID As String
    Public Property NewUsed As String
    Public Property AmountCoverage As Decimal
    Public Property InsuranceType As String
     
    Public Property IsPageSourceCompanyCustomer As Boolean

    Public Property DiscountToCust As Decimal
    Public Property PaidAmountByCust As Decimal

    Public Property LastInsSequenceNo As Integer
    Public Property LastAssetSeqNo As Integer
    Public Property WilayahPTG As String
    Public Property ManufacturingYear As Integer
    Public Property ProductofferingId As String
    Public Property Tenor As Integer
    Public Property PaketProduct As String
    Public Property BusinessDate As Date
    Public Property SeatCapacity As Integer
    Public Property SelilihTahunBusinessDateDanTahunManufacturing As Integer
    Public Property DiscountMin As Double
    Public Property DiscountMax As Double

    Public Property TglDariKoreksi As Date
    Public Property TglSampaiKoreksi As Date
    Public Property CoverPeriodKoreksi As String

    Public Sub InitialValue(row As DataRow)
        LastInsSequenceNo = CInt(row.Item("LastInsSequenceNo")) 
        LastAssetSeqNo = CInt(row.Item("LastAssetSeqNo"))
        WilayahPTG = row.Item("WIlayahPTG")
        ManufacturingYear = CInt(row.Item("manufacturingyear"))
        ProductofferingId = row.Item("productofferingid")
        Tenor = CInt(row.Item("Tenor"))
        PaketProduct = row.Item("PaketProduct")
        BusinessDate = CDate(row.Item("BusinessDate"))
        SeatCapacity = CInt(row.Item("SeatCapacity"))
        SelilihTahunBusinessDateDanTahunManufacturing = CInt(row.Item("SelilihTahunBusinessDateDanTahunManufacturing"))


        DiscountMin = CDbl(row.Item("DiscountMin"))
        DiscountMax = CDbl(row.Item("DiscountMin"))
    End Sub

    Public Sub InitMaskapai(maskapai As DataTable)
        For Each row In maskapai.Rows
            _maskAsuransi.Add(
                New MaskAsuransi() With
                {
                    .MaskAssID = row("MaskAssID"),
                    .MaskAssBranchID = row("MaskAssBranchID"),
                    .MaskAssBranchName = row("MaskAssBranchName"),
                    .MaskAssBranchNameDesc = row("MaskAssBranchNameDesc"),
                    .MaskAssBranchCity = row("MaskAssBranchCity"),
                    .AdminFee = CDbl(row("AdminFee")),
                    .StampDutyFee = CDbl(row("StampDutyFee")),
                    .LoadingFeeAllRisk = CInt(row("LoadingFeeAllRisk")),
                    .LoadingFeeTLO = CInt(row("LoadingFeeTLO")),
                    .AllRiskOptions = row("AllRiskOptions"),
                    .TLOOptions = row("TLOOptions"),
                    .Komisi = CDbl(row("Komisi")),
                    .ImbalanJasa = CDbl(row("ImbalanJasa")),
                    .PPh23 = CDbl(row("PPh23")),
                    .PPN = CDbl(row("PPN")),
                    .PPh23CertifiedByIns = CBool(row("PPh23CertifiedByIns")),
                    .PPNCertifiedByIns = CBool(row("PPNCertifiedByIns"))
                }
            )
        Next


    End Sub

    Public Sub InitInsuranceCoverType(table As DataTable)
        For Each row In table.Rows
            _insurancecvrType.Add(
                New InsuranceCoverType() With
                {
                    .YEARInsurance = CInt(row("YEARInsurance")),
                    .COVERAGETYPE = row("COVERAGETYPE"),
                    .PREMIUM = CDbl(row("PREMIUM")),
                    .SRCC = CBool(row("SRCC")),
                    .SRCCAMount = CBool(row("SRCCAMount")),
                    .TPL = CBool(row("TPL")),
                    .TPLAmount = CDbl(row("TPLAmount")),
                    .FLOOD = CBool(row("FLOOD")),
                    .FloodAmount = CDbl(row("FloodAmount")),
                    .TOTAL = CDbl(row("TOTAL")),
                    .PAIDBYCUST = row("PAIDBYCUST"),
                    .LoadingFee = CDbl(row("LoadingFee")),
                    .Riot = CBool(row("Riot")),
                    .RiotAmount = CDbl(row("RiotAmount")),
                    .PA = CBool(row("PA")),
                    .PAAmount = CDbl(row("PAAmount")),
                    .PADriver = CBool(row("PADriver")),
                    .PADriverAmount = CDbl(row("PADriverAmount"))
                }
            )
        Next
    End Sub

     
    Public Function ToDataTable() As DataTable
        Dim dt As DataTable = New DataTable()
        Dim row As DataRow
        Dim strArray As String() = New String() { "YearNum", "CoverageType", "IsTPL", "BolFlood", "BoolEQVET", "BoolSRCC", "BolTerrorism", "BolPA", "IsPADriver", "TPLAmountToCust", "PAAmountToCust", "PADriverAmountToCust", "PaidByCustStatus", "YearNumRate" } 
        For Each s In strArray
            dt.Columns.Add(New DataColumn(s))
        Next
         
        For Each item In _insurancecvrType
            row = dt.NewRow()

            row("YearNum") = item.YEARInsurance
            row("CoverageType") = item.COVERAGETYPE
            row("IsTPL") = item.TPL
            row("BolFlood") = item.FLOOD
            row("BoolEQVET") = item.EQVET
            row("BoolSRCC") = item.SRCC
            row("BolTerrorism") = item.TERROR 
            row("BolPA") = item.PA

            row("IsPADriver") = item.PADriver
            row("TPLAmountToCust") = item.TPLAmount
            row("PAAmountToCust") = item.PAAmount
            row("PADriverAmountToCust") = item.PADriverAmount
            row("PaidByCustStatus") = 0
            row("YearNumRate") = 0

            dt.Rows.Add(row)
        Next 
        Return dt
    End Function

End Class
