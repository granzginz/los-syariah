
<Serializable()> _
Public Class InsInqEntities : Inherits Common
    Private _listReport As DataSet
    Private _ListData As DataTable
    Private _AOSupervisor As String
    Private _AO As String
    Private _AssetStatus As String
    Private _sdate As Date
    Private _edate As Date
    Private _daytoexpired As Integer
    Private _WhereCond2 As String
    Private _StatisticYear As String
    Public Property ListReport() As DataSet
        Get
            Return _listReport
        End Get
        Set(ByVal Value As DataSet)
            _listReport = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal Value As DataTable)
            _ListData = Value
        End Set
    End Property
    Public Property AOSupervisor() As String
        Get
            Return _AOSupervisor
        End Get
        Set(ByVal Value As String)
            _AOSupervisor = Value
        End Set
    End Property
    Public Property AO() As String
        Get
            Return _AO
        End Get
        Set(ByVal Value As String)
            _AO = Value
        End Set
    End Property

    Public Property AssetStatus() As String
        Get
            Return _AssetStatus
        End Get
        Set(ByVal Value As String)
            _AssetStatus = Value
        End Set
    End Property

    Public Property sdate() As Date
        Get
            Return _sdate
        End Get
        Set(ByVal Value As Date)
            _sdate = Value
        End Set
    End Property

    Public Property edate() As Date
        Get
            Return _edate
        End Get
        Set(ByVal Value As Date)
            _edate = Value
        End Set
    End Property
    Public Property DayToExpired() As Integer
        Get
            Return _daytoexpired
        End Get
        Set(ByVal Value As Integer)
            _daytoexpired = Value
        End Set
    End Property
    Public Property WhereCond2() As String
        Get
            Return _WhereCond2
        End Get
        Set(ByVal Value As String)
            _WhereCond2 = Value
        End Set
    End Property
    Public Property StatisticYear() As String
        Get
            Return _StatisticYear
        End Get
        Set(ByVal Value As String)
            _StatisticYear = Value
        End Set
    End Property
End Class
