
<Serializable()> _
Public Class InsInqRefund : Inherits Maxiloan.Parameter.Common
    Private _listdata As DataTable
    Private _strApplicationID As String
    Private _InsSequenceNo As Integer
    Private _criteria As String
    Private _assetseqno As Integer

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property AssetSequenceNo() As Integer
        Get
            Return _assetseqno
        End Get
        Set(ByVal Value As Integer)
            _assetseqno = Value
        End Set
    End Property
    Public Property InsSequenceNo() As Integer
        Get
            Return _InsSequenceNo
        End Get
        Set(ByVal Value As Integer)
            _InsSequenceNo = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return _strApplicationID
        End Get
        Set(ByVal Value As String)
            _strApplicationID = Value
        End Set
    End Property
    Public Property Criteria() As String
        Get
            Return _criteria
        End Get
        Set(ByVal Value As String)
            _criteria = Value
        End Set
    End Property


End Class
