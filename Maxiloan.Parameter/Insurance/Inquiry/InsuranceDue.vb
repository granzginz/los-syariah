<Serializable()> _
Public Class InsuranceDue : Inherits Common
    Private _listdata As DataTable
    Private _listreport As DataSet
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property ListReport() As DataSet
        Get
            Return _listreport
        End Get
        Set(ByVal Value As DataSet)
            _listreport = Value
        End Set
    End Property


End Class
