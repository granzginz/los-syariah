

Public Class InsuranceStandardPremium : Inherits Common
    Private _Branch As String
    Private _listdata As DataTable
    Private _listdatareport As DataSet
    Private _TPLAmount As String
    Private _TPLPremium As String
    Private _InsType As String
    Private _UsageID As String
    Private _NewUsed As String
    Private _CoverageType As String
    Private _YearNum As String
    Private _Rate As String
    Private _SRCCRate As String
    Private _FloodRate As String
    Private _LoadingRate As String
    Private _SumIns As String
    Private _totalrecords As Int64
    Private _Error As String
    Private _Page As String
    Private _BranchDestination As String
    Private _BranchSource As String
    Private _InsuranceTypeDescr As String
    Private _PageSource As String
    Private _excelFilePath As String
    Private _dtpolis As DataTable
    Private _UserID As String
    Public Property UserID() As String
        Get
            Return _UserID
        End Get
        Set(ByVal Value As String)
            _UserID = Value
        End Set
    End Property
    Public Property dtPolis() As DataTable
        Get
            Return _dtpolis
        End Get
        Set(ByVal Value As DataTable)
            _dtpolis = Value
        End Set
    End Property

    Public Property ExcelFilePath() As String
        Get
            Return _excelFilePath
        End Get
        Set(ByVal Value As String)
            _excelFilePath = Value
        End Set
    End Property

    Public Property PageSource() As String
        Get
            Return _PageSource
        End Get
        Set(ByVal Value As String)
            _PageSource = Value
        End Set
    End Property


    Public Property InsuranceTypeDescr() As String
        Get
            Return _InsuranceTypeDescr
        End Get
        Set(ByVal Value As String)
            _InsuranceTypeDescr = Value
        End Set
    End Property


    Public Property BranchDestination() As String
        Get
            Return _BranchDestination
        End Get
        Set(ByVal Value As String)
            _BranchDestination = Value
        End Set
    End Property
    Public Property BranchSource() As String
        Get
            Return _BranchSource
        End Get
        Set(ByVal Value As String)
            _BranchSource = Value
        End Set
    End Property

    Public Property Branch() As String
        Get
            Return _Branch
        End Get
        Set(ByVal Value As String)
            _Branch = Value
        End Set
    End Property
    Public Property Page() As String
        Get
            Return _Page
        End Get
        Set(ByVal Value As String)
            _Page = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property ListDataReport() As DataSet
        Get
            Return _listdatareport
        End Get
        Set(ByVal Value As DataSet)
            _listdatareport = Value
        End Set
    End Property
    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property TPLAmount() As String
        Get
            Return _TPLAmount
        End Get
        Set(ByVal Value As String)
            _TPLAmount = Value
        End Set
    End Property
    Public Property TPLPremium() As String
        Get
            Return _TPLPremium
        End Get
        Set(ByVal Value As String)
            _TPLPremium = Value
        End Set
    End Property
    Public Property InsType() As String
        Get
            Return _InsType
        End Get
        Set(ByVal Value As String)
            _InsType = Value
        End Set
    End Property
    Public Property UsageID() As String
        Get
            Return _UsageID
        End Get
        Set(ByVal Value As String)
            _UsageID = Value
        End Set
    End Property
    Public Property NewUsed() As String
        Get
            Return _NewUsed
        End Get
        Set(ByVal Value As String)
            _NewUsed = Value
        End Set
    End Property
    Public Property CoverageType() As String
        Get
            Return _CoverageType
        End Get
        Set(ByVal Value As String)
            _CoverageType = Value
        End Set
    End Property
    Public Property YearNum() As String
        Get
            Return _YearNum
        End Get
        Set(ByVal Value As String)
            _YearNum = Value
        End Set
    End Property
    Public Property Rate() As String
        Get
            Return _Rate
        End Get
        Set(ByVal Value As String)
            _Rate = Value
        End Set
    End Property
    Public Property SRCCRate() As String
        Get
            Return _SRCCRate
        End Get
        Set(ByVal Value As String)
            _SRCCRate = Value
        End Set
    End Property
    Public Property FloodRate() As String
        Get
            Return _FloodRate
        End Get
        Set(ByVal Value As String)
            _FloodRate = Value
        End Set
    End Property
    Public Property LoadingRate() As String
        Get
            Return _LoadingRate
        End Get
        Set(ByVal Value As String)
            _LoadingRate = Value
        End Set
    End Property
    Public Property SumIns() As String
        Get
            Return _SumIns
        End Get
        Set(ByVal Value As String)
            _SumIns = Value
        End Set
    End Property
    Public Property Err() As String
        Get
            Return _Error
        End Get
        Set(ByVal Value As String)
            _Error = Value
        End Set
    End Property

    Public Property ApplicationID As String
    Public Property AssetSeqNo As Integer
    Public Property InsSequenceNo As Integer
    Public Property NoNotaAsuransi As String
    Public Property TglNotaAsuransi As DateTime
    Public Property PremiGross As Decimal
    Public Property Komisi As Decimal
    Public Property PremiNet As Decimal
    Public Property PPN As Decimal
    Public Property PPH As Decimal
    Public Property AdminFee As Decimal
    Public Property MeteraiFee As Decimal
    Public Property Selisih As Boolean
    Public Property PayAmount As Decimal
    Public Property BillAmount As Decimal
    Public Property PolicyNumber As String
    Public Property PolicyReceiveDate As DateTime
    Public Property ProductInsurance As String

End Class
