<Serializable()> _
Public Class InsCoBranch : Inherits InsCo
    Private _InsCoBranchID As String
    Private _InsCoBranchName As String
    Private _TermOfPayment As String
    Private _AdminFee As Double
    Private _StampDutyFee As Double
    Private _BiayaPolisAdditional As Double
    Private _InsuranceRateAdditional As Double
    Private _InsuranceProductID As String
    Private _Tenor As Integer
 
    Public Property InsCoBranchID() As String
        Get
            Return _InsCoBranchID
        End Get
        Set(ByVal Value As String)
            _InsCoBranchID = Value
        End Set
    End Property

    Public Property InsCoBranchName() As String
        Get
            Return _InsCoBranchName
        End Get
        Set(ByVal Value As String)
            _InsCoBranchName = Value
        End Set
    End Property


    Public Property TermOfPayment() As String
        Get
            Return _TermOfPayment
        End Get
        Set(ByVal Value As String)
            _TermOfPayment = Value
        End Set
    End Property

    Public Property AdminFee() As Double
        Get
            Return _AdminFee
        End Get
        Set(ByVal Value As Double)
            _AdminFee = Value
        End Set
    End Property

    Public Property StampDutyFee() As Double
        Get
            Return _StampDutyFee
        End Get
        Set(ByVal Value As Double)
            _StampDutyFee = Value
        End Set
    End Property

    Public Property BiayaPolisAdditional() As Double
        Get
            Return _BiayaPolisAdditional
        End Get
        Set(ByVal Value As Double)
            _BiayaPolisAdditional = Value
        End Set
    End Property

    Public Property InsuranceRateAdditional() As Double
        Get
            Return _InsuranceRateAdditional
        End Get
        Set(ByVal Value As Double)
            _InsuranceRateAdditional = Value
        End Set
    End Property

    Public Property InsuranceProductID() As String
        Get
            Return _InsuranceProductID
        End Get
        Set(value As String)
            _InsuranceProductID = value
        End Set
    End Property

    Public Property Tenor() As Integer
        Get
            Return _Tenor
        End Get
        Set(value As Integer)
            _Tenor = value
        End Set
    End Property

End Class

