<Serializable()>
Public Class InsCoProduct : Inherits Common


    Private _maskId As String
    Private _IDProd As String
    Private _InsCoProdName As String
    Private _IsActive As Boolean

    Private _listProduct As DataTable

    Public Property ListProduct() As DataTable
        Get
            Return (CType(_listProduct, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listProduct = Value
        End Set
    End Property

    Public Property MaskAssID() As String
        Get
            Return _maskId
        End Get
        Set(ByVal value As String)
            _maskId = value
        End Set
    End Property
    Public Property IDProd() As String
        Get
            Return _IDProd
        End Get
        Set(ByVal value As String)
            _IDProd = value
        End Set
    End Property

    Public Property ProdName() As String
        Get
            Return _InsCoProdName
        End Get
        Set(ByVal value As String)
            _InsCoProdName = value
        End Set
    End Property

    Public Property IsActive() As Boolean
        Get
            Return _IsActive
        End Get
        Set(ByVal value As Boolean)
            _IsActive = value
        End Set
    End Property


End Class
