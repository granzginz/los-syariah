<Serializable()> _
Public Class InsCoRate : Inherits InsCoBranch
    Private _insAssettype As String
    Private _AssetUssage As String
    Private _newused As String
    Private _CoverageType As String
    Private _yearnum As Int16
    Private _ratetoInsco As Decimal
    Private _srccRate As Decimal
    Private _FloodRate As Decimal
    Private _loadingrate As Decimal
    Private _sumInsuranced As Decimal
    Private _InsCoBranchIDTo As String
    Private _InsCoBranchIDFrom As String


    Public Property insAssettype() As String
        Get
            Return _insAssettype
        End Get
        Set(ByVal Value As String)
            _insAssettype = Value
        End Set
    End Property

    Public Property AssetUssage() As String
        Get
            Return _AssetUssage
        End Get
        Set(ByVal Value As String)
            _AssetUssage = Value
        End Set
    End Property

    Public Property newused() As String
        Get
            Return _newused
        End Get
        Set(ByVal Value As String)
            _newused = Value
        End Set
    End Property

    Public Property CoverageType() As String
        Get
            Return _CoverageType
        End Get
        Set(ByVal Value As String)
            _CoverageType = Value
        End Set
    End Property

    Public Property yearnum() As Int16
        Get
            Return _yearnum
        End Get
        Set(ByVal Value As Int16)
            _yearnum = Value
        End Set
    End Property

    Public Property ratetoInsco() As Decimal
        Get
            Return _ratetoInsco
        End Get
        Set(ByVal Value As Decimal)
            _ratetoInsco = Value
        End Set
    End Property

    Public Property srccRate() As Decimal
        Get
            Return _srccRate
        End Get
        Set(ByVal Value As Decimal)
            _srccRate = Value
        End Set
    End Property

    Public Property FloodRate() As Decimal
        Get
            Return _FloodRate
        End Get
        Set(ByVal Value As Decimal)
            _FloodRate = Value
        End Set
    End Property

    Public Property loadingrate() As Decimal
        Get
            Return _loadingrate
        End Get
        Set(ByVal Value As Decimal)
            _loadingrate = Value
        End Set
    End Property


    Public Property sumInsuranced() As Decimal
        Get
            Return _sumInsuranced
        End Get
        Set(ByVal Value As Decimal)
            _sumInsuranced = Value
        End Set
    End Property


    Public Property InsCoBranchIDTo() As String
        Get
            Return _InsCoBranchIDTo
        End Get
        Set(ByVal Value As String)
            _InsCoBranchIDTo = Value
        End Set
    End Property

    Public Property InsCoBranchIDFrom() As String
        Get
            Return _InsCoBranchIDFrom
        End Get
        Set(ByVal Value As String)
            _InsCoBranchIDFrom = Value
        End Set
    End Property


End Class
