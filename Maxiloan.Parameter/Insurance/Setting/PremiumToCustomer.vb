
<Serializable()> _
Public Class PremiumToCustomer : Inherits Common
    Private _Branch As String
    Private _listdata As DataTable
    Private _listdatareport As DataSet
    Private _TPLAmount As String
    Private _TPLPremium As String
    Private _InsType As String
    Private _UsageID As String
    Private _NewUsed As String
    Private _CoverageType As String
    Private _YearNum As String
    Private _RateGroup As String
    Private _RateNonGroup As String
    Private _RateRepeatOrder As String
    Private _SRCCRate As String
    Private _FloodRate As String
    Private _LoadingRate As String
    Private _SumIns As String
    Private _totalrecords As Int64
    Private _Error As String
    Private _Page As String
    Private _BranchDestination As String
    Private _BranchSource As String
    Public Property BranchDestination() As String
        Get
            Return _BranchDestination
        End Get
        Set(ByVal Value As String)
            _BranchDestination = Value
        End Set
    End Property
    Public Property BranchSource() As String
        Get
            Return _BranchSource
        End Get
        Set(ByVal Value As String)
            _BranchSource = Value
        End Set
    End Property

    Public Property Branch() As String
        Get
            Return _Branch
        End Get
        Set(ByVal Value As String)
            _Branch = Value
        End Set
    End Property
    Public Property Page() As String
        Get
            Return _Page
        End Get
        Set(ByVal Value As String)
            _Page = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property ListDataReport() As DataSet
        Get
            Return _listdatareport
        End Get
        Set(ByVal Value As DataSet)
            _listdatareport = Value
        End Set
    End Property
    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property TPLAmount() As String
        Get
            Return _TPLAmount
        End Get
        Set(ByVal Value As String)
            _TPLAmount = Value
        End Set
    End Property
    Public Property TPLPremium() As String
        Get
            Return _TPLPremium
        End Get
        Set(ByVal Value As String)
            _TPLPremium = Value
        End Set
    End Property
    Public Property InsType() As String
        Get
            Return _InsType
        End Get
        Set(ByVal Value As String)
            _InsType = Value
        End Set
    End Property
    Public Property UsageID() As String
        Get
            Return _UsageID
        End Get
        Set(ByVal Value As String)
            _UsageID = Value
        End Set
    End Property
    Public Property NewUsed() As String
        Get
            Return _NewUsed
        End Get
        Set(ByVal Value As String)
            _NewUsed = Value
        End Set
    End Property
    Public Property CoverageType() As String
        Get
            Return _CoverageType
        End Get
        Set(ByVal Value As String)
            _CoverageType = Value
        End Set
    End Property
    Public Property YearNum() As String
        Get
            Return _YearNum
        End Get
        Set(ByVal Value As String)
            _YearNum = Value
        End Set
    End Property
    Public Property RateGroup() As String
        Get
            Return _RateGroup
        End Get
        Set(ByVal Value As String)
            _RateGroup = Value
        End Set
    End Property
    Public Property RateNonGroup() As String
        Get
            Return _RateNonGroup
        End Get
        Set(ByVal Value As String)
            _RateNonGroup = Value
        End Set
    End Property
    Public Property RateRepeatOrder() As String
        Get
            Return _RateRepeatOrder
        End Get
        Set(ByVal Value As String)
            _RateRepeatOrder = Value
        End Set
    End Property
    Public Property SRCCRate() As String
        Get
            Return _SRCCRate
        End Get
        Set(ByVal Value As String)
            _SRCCRate = Value
        End Set
    End Property
    Public Property FloodRate() As String
        Get
            Return _FloodRate
        End Get
        Set(ByVal Value As String)
            _FloodRate = Value
        End Set
    End Property
    Public Property LoadingRate() As String
        Get
            Return _LoadingRate
        End Get
        Set(ByVal Value As String)
            _LoadingRate = Value
        End Set
    End Property
    Public Property SumIns() As String
        Get
            Return _SumIns
        End Get
        Set(ByVal Value As String)
            _SumIns = Value
        End Set
    End Property
    Public Property Err() As String
        Get
            Return _Error
        End Get
        Set(ByVal Value As String)
            _Error = Value
        End Set
    End Property
End Class
