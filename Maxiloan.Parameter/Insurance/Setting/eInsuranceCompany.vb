<Serializable()>
Public Class eInsuranceCompany : Inherits Common
    Private _InsCoID As String
    Private _InsCoName As String

    Private _BranchIDTo As String
    Private _BranchIDFrom As String

    Private _InsCoIDFrom As String
    Private _InsCoIDTo As String

    Private _listdata As DataTable
    Private _recordcount As Int64

    Private _DtmUpd As DateTime
    Private _mode As String

    Private _InsCoBranchID As String
    Private _InsCoBranchName As String
    Private _TermOfPayment As String
    Private _AdminFee As Double
    Private _StampDutyFee As Double

    Private _PolisFeeCP As Double
    Private _PolisFeeJK As Double


    Private _insAssettype As String
    Private _AssetUssage As String
    Private _newused As String
    Private _CoverageType As String
    Private _suminsuredfrom As Decimal
    Private _suminsuredto As Decimal
    Private _InsCoBranchIDTo As String
    Private _InsCoBranchIDFrom As String
    Private _RateCardType As String

    Private _TPLAmount As Double
    Private _TPLPremium As Double

    Private _strCoverageID As String
    Private _strCoverageRate As String
    Private _RowCount As Integer

    Private _Command As String
    Private _Output As String

    Private _Statebranch As String
    Private _NumRows As Integer
    Private _strID As String
    Private _strBranch As String
    Private _strState As String
    Private _Discount As Decimal
    Private _DeviderDays As Integer
    Private _MaskAssID As String
    Private _MaskAssBranchID As String
    Private _MaskAssBranchName As String
    Public Property MaskAssID() As String
        Get
            Return _MaskAssID
        End Get
        Set(ByVal Value As String)
            _MaskAssID = Value
        End Set
    End Property
    Public Property MaskAssBranchID() As String
        Get
            Return _MaskAssBranchID
        End Get
        Set(ByVal Value As String)
            _MaskAssBranchID = Value
        End Set
    End Property
    Public Property MaskAssBranchName() As String
        Get
            Return _MaskAssBranchName
        End Get
        Set(ByVal Value As String)
            _MaskAssBranchName = Value
        End Set
    End Property

    Public Property DeviderDays() As Integer
        Get
            Return _DeviderDays
        End Get
        Set(ByVal Value As Integer)
            _DeviderDays = Value
        End Set
    End Property

    Public Property InsCoIDTo() As String
        Get
            Return _InsCoIDTo
        End Get
        Set(ByVal Value As String)
            _InsCoIDTo = Value
        End Set
    End Property

    Public Property BranchIDTo() As String
        Get
            Return _BranchIDTo
        End Get
        Set(ByVal Value As String)
            _BranchIDTo = Value
        End Set
    End Property

    Public Property BranchIDFrom() As String
        Get
            Return _BranchIDFrom
        End Get
        Set(ByVal Value As String)
            _BranchIDFrom = Value
        End Set
    End Property

    Public Property InsCoIDFrom() As String
        Get
            Return _InsCoIDFrom
        End Get
        Set(ByVal Value As String)
            _InsCoIDFrom = Value
        End Set
    End Property


    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property RecordCount() As Int64
        Get
            Return _recordcount
        End Get
        Set(ByVal Value As Int64)
            _recordcount = Value
        End Set
    End Property

    Public Property DtmUpd() As DateTime
        Get
            Return _DtmUpd
        End Get
        Set(ByVal Value As DateTime)
            _DtmUpd = Value
        End Set
    End Property


    Public Property InsCoID() As String
        Get
            Return _InsCoID
        End Get
        Set(ByVal Value As String)
            _InsCoID = Value
        End Set
    End Property

    Public Property InsCoName() As String
        Get
            Return _InsCoName
        End Get
        Set(ByVal Value As String)
            _InsCoName = Value
        End Set
    End Property

    Public Property mode() As String
        Get
            Return _mode
        End Get
        Set(ByVal Value As String)
            _mode = Value
        End Set
    End Property

    Public Property InsCoBranchID() As String
        Get
            Return _InsCoBranchID
        End Get
        Set(ByVal Value As String)
            _InsCoBranchID = Value
        End Set
    End Property

    Public Property InsCoBranchName() As String
        Get
            Return _InsCoBranchName
        End Get
        Set(ByVal Value As String)
            _InsCoBranchName = Value
        End Set
    End Property


    Public Property TermOfPayment() As String
        Get
            Return _TermOfPayment
        End Get
        Set(ByVal Value As String)
            _TermOfPayment = Value
        End Set
    End Property

    Public Property AdminFee() As Double
        Get
            Return _AdminFee
        End Get
        Set(ByVal Value As Double)
            _AdminFee = Value
        End Set
    End Property

    Public Property StampDutyFee() As Double
        Get
            Return _StampDutyFee
        End Get
        Set(ByVal Value As Double)
            _StampDutyFee = Value
        End Set
    End Property


    Public Property PolisFeeCp() As Double
        Get
            Return _PolisFeeCP
        End Get
        Set(ByVal Value As Double)
            _PolisFeeCP = Value
        End Set
    End Property

    Public Property PolisFeeJk() As Double
        Get
            Return _PolisFeeJK
        End Get
        Set(ByVal Value As Double)
            _PolisFeeJK = Value
        End Set
    End Property


    Public Property insAssettype() As String
        Get
            Return _insAssettype
        End Get
        Set(ByVal Value As String)
            _insAssettype = Value
        End Set
    End Property

    Public Property AssetUssage() As String
        Get
            Return _AssetUssage
        End Get
        Set(ByVal Value As String)
            _AssetUssage = Value
        End Set
    End Property

    Public Property newused() As String
        Get
            Return _newused
        End Get
        Set(ByVal Value As String)
            _newused = Value
        End Set
    End Property

    Public Property CoverageType() As String
        Get
            Return _CoverageType
        End Get
        Set(ByVal Value As String)
            _CoverageType = Value
        End Set
    End Property

    Public Property SumInsuredFrom() As Decimal
        Get
            Return _suminsuredfrom
        End Get
        Set(ByVal Value As Decimal)
            _suminsuredfrom = Value
        End Set
    End Property


    Public Property SumInsuredTo() As Decimal
        Get
            Return _suminsuredto
        End Get
        Set(ByVal Value As Decimal)
            _suminsuredto = Value
        End Set
    End Property


    Public Property InsCoBranchIDTo() As String
        Get
            Return _InsCoBranchIDTo
        End Get
        Set(ByVal Value As String)
            _InsCoBranchIDTo = Value
        End Set
    End Property

    Public Property InsCoBranchIDFrom() As String
        Get
            Return _InsCoBranchIDFrom
        End Get
        Set(ByVal Value As String)
            _InsCoBranchIDFrom = Value
        End Set
    End Property

    Public Property Command() As String
        Get
            Return _Command
        End Get
        Set(ByVal Value As String)
            _Command = Value
        End Set
    End Property

    Public Property output() As String
        Get
            Return _Output
        End Get
        Set(ByVal Value As String)
            _Output = Value
        End Set
    End Property

    Public Property RateCardType() As String
        Get
            Return _RateCardType
        End Get
        Set(ByVal Value As String)
            _RateCardType = Value
        End Set
    End Property

    Public Property TPLAmount() As Double
        Get
            Return _TPLAmount
        End Get
        Set(ByVal Value As Double)
            _TPLAmount = Value
        End Set
    End Property

    Public Property TPLPremium() As Double
        Get
            Return _TPLPremium
        End Get
        Set(ByVal Value As Double)
            _TPLPremium = Value
        End Set
    End Property

    Public Property strCoverageRate() As String
        Get
            Return _strCoverageRate
        End Get
        Set(ByVal Value As String)
            _strCoverageRate = Value
        End Set
    End Property

    Public Property strCoverageID() As String
        Get
            Return _strCoverageID
        End Get
        Set(ByVal Value As String)
            _strCoverageID = Value
        End Set
    End Property

    Public Property RowCount() As Integer
        Get
            Return _RowCount
        End Get
        Set(ByVal Value As Integer)
            _RowCount = Value
        End Set
    End Property

    Public Property StateBranch() As String
        Get
            Return _Statebranch
        End Get
        Set(ByVal Value As String)
            _Statebranch = Value
        End Set
    End Property

    Public Property NumRows() As Integer
        Get
            Return _NumRows
        End Get
        Set(ByVal Value As Integer)
            _NumRows = Value
        End Set
    End Property

    Public Property strID() As String
        Get
            Return _strID
        End Get
        Set(ByVal Value As String)
            _strID = Value
        End Set
    End Property

    Public Property strBranch() As String
        Get
            Return _strBranch
        End Get
        Set(ByVal Value As String)
            _strBranch = Value
        End Set
    End Property

    Public Property strState() As String
        Get
            Return _strState
        End Get
        Set(ByVal Value As String)
            _strState = Value
        End Set
    End Property

    Public Property Discount() As Decimal
        Get
            Return _Discount
        End Get
        Set(ByVal Value As Decimal)
            _Discount = Value
        End Set
    End Property

    Public Property LoadingFeeAllRisk As Integer
    Public Property LoadingFeeTLO As Integer
    Public Property IsUtama As Boolean

    Public Property strInsuranceAreaId As String
    Public Property strCoverageType As String
    Public Property strRateBottom As String
    Public Property strRateTop As String

    Public Property Komisi As Double
    Public Property Fee As Double
    Public Property PPh23 As Double
    Public Property PPN As Double
    Public Property Dist As Double
    Public Property BiayaOpex As Double
    Public Property NoClaimBonus As Double
    Public Property PPh23CertifiedByIns As Boolean
    Public Property PPNCertifiedByIns As Boolean
    Public Property AllRiskOptions As String
    Public Property TLOOptions As String

    Public Property CategoryType As String
    Public Property UsiaAsset As Integer
    Public Property LoadingRate As Decimal
    Public Property CategoryTypeNew As String
    Public Property UsiaAssetNew As Integer
    Public Property LoadingRateNew As Decimal
    Public Property AccountAP As String
    Public Property AmountFrom As Double
    Public Property AmountTo As Double
    Public Property Rate As Double
    Public Property InsRateCategory As String
End Class
