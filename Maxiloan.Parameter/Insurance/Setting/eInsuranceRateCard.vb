Public Class eInsuranceRateCard
    Inherits Maxiloan.Parameter.Common

#Region "Constant"

    Private _listdata As DataTable
    Private _totalrecords As Int64
    Private _NumRows As Integer
    Private _Output As String
    Private _Command As String
    Private _CardID As String
    Private _CardIDSource As String
    Private _CardDesc As String
    Private _IsActive As Boolean
    Private _InsType As String
    Private _UsageID As String
    Private _NewUsed As String
    Private _CoverageType As String
    Private _YearNum As Integer
    Private _RateGroup As Double
    Private _RateNonGroup As Double
    Private _RateRepeatOrder As Double
    Private _RateLoadingPenggunaan As Double
    Private _SRCCRate As Double
    Private _FloodRate As Double
    Private _LoadingRate As Double
    Private _SumIns As Double
    Private _MasterRate As String
    Private _SumFrom As Double
    Private _SumTo As Double
    Private _TPLAmount As Double
    Private _TPLPremium As Double
    Private _OtherCoverage As String
    Private _CoverageRate As Double
    Private _SeqNo As Integer
#End Region

    Public Property Command() As String
        Get
            Return _Command
        End Get
        Set(ByVal Value As String)
            _Command = Value
        End Set
    End Property
    Public Property CardID() As String
        Get
            Return _CardID
        End Get
        Set(ByVal Value As String)
            _CardID = Value
        End Set
    End Property
    Public Property CardIDSource() As String
        Get
            Return _CardIDSource
        End Get
        Set(ByVal Value As String)
            _CardIDSource = Value
        End Set
    End Property
    Public Property CardDesc() As String
        Get
            Return _CardDesc
        End Get
        Set(ByVal Value As String)
            _CardDesc = Value
        End Set
    End Property
    Public Property IsActive() As Boolean
        Get
            Return _IsActive
        End Get
        Set(ByVal Value As Boolean)
            _IsActive = Value
        End Set
    End Property

    Public Property output() As String
        Get
            Return _Output
        End Get
        Set(ByVal Value As String)
            _Output = Value
        End Set
    End Property


    Public Property NumRows() As Integer
        Get
            Return _NumRows
        End Get
        Set(ByVal Value As Integer)
            _NumRows = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property InsType() As String
        Get
            Return _InsType
        End Get
        Set(ByVal Value As String)
            _InsType = Value
        End Set
    End Property
    Public Property UsageID() As String
        Get
            Return _UsageID
        End Get
        Set(ByVal Value As String)
            _UsageID = Value
        End Set
    End Property
    Public Property NewUsed() As String
        Get
            Return _NewUsed
        End Get
        Set(ByVal Value As String)
            _NewUsed = Value
        End Set
    End Property
    Public Property CoverageType() As String
        Get
            Return _CoverageType
        End Get
        Set(ByVal Value As String)
            _CoverageType = Value
        End Set
    End Property
    Public Property YearNum() As Integer
        Get
            Return _YearNum
        End Get
        Set(ByVal Value As Integer)
            _YearNum = Value
        End Set
    End Property
    Public Property RateGroup() As Double
        Get
            Return _RateGroup
        End Get
        Set(ByVal Value As Double)
            _RateGroup = Value
        End Set
    End Property
    Public Property RateNonGroup() As Double
        Get
            Return _RateNonGroup
        End Get
        Set(ByVal Value As Double)
            _RateNonGroup = Value
        End Set
    End Property
    Public Property RateRepeatOrder() As Double
        Get
            Return _RateRepeatOrder
        End Get
        Set(ByVal Value As Double)
            _RateRepeatOrder = Value
        End Set
    End Property
    Public Property RateLoadingPenggunaan() As Double
        Get
            Return _RateLoadingPenggunaan
        End Get
        Set(ByVal Value As Double)
            _RateLoadingPenggunaan = Value
        End Set
    End Property
    Public Property SRCCRate() As Double
        Get
            Return _SRCCRate
        End Get
        Set(ByVal Value As Double)
            _SRCCRate = Value
        End Set
    End Property
    Public Property FloodRate() As Double
        Get
            Return _FloodRate
        End Get
        Set(ByVal Value As Double)
            _FloodRate = Value
        End Set
    End Property
    Public Property LoadingRate() As Double
        Get
            Return _LoadingRate
        End Get
        Set(ByVal Value As Double)
            _LoadingRate = Value
        End Set
    End Property
    Public Property SumIns() As Double
        Get
            Return _SumIns
        End Get
        Set(ByVal Value As Double)
            _SumIns = Value
        End Set
    End Property
    Public Property MasterRate() As String
        Get
            Return _MasterRate
        End Get
        Set(ByVal Value As String)
            _MasterRate = Value
        End Set
    End Property
    Public Property SumFrom() As Double
        Get
            Return _SumFrom
        End Get
        Set(ByVal Value As Double)
            _SumFrom = Value
        End Set
    End Property
    Public Property SumTo() As Double
        Get
            Return _SumTo
        End Get
        Set(ByVal Value As Double)
            _SumTo = Value
        End Set
    End Property
    Public Property TPLAmount() As Double
        Get
            Return _TPLAmount
        End Get
        Set(ByVal Value As Double)
            _TPLAmount = Value
        End Set
    End Property
    Public Property TPLPremium() As Double
        Get
            Return _TPLPremium
        End Get
        Set(ByVal Value As Double)
            _TPLPremium = Value
        End Set
    End Property
    Public Property OtherCoverage() As String
        Get
            Return _OtherCoverage
        End Get
        Set(ByVal Value As String)
            _OtherCoverage = Value
        End Set
    End Property
    Public Property CoverageRate() As Double
        Get
            Return _CoverageRate
        End Get
        Set(ByVal Value As Double)
            _CoverageRate = Value
        End Set
    End Property
    Public Property InsuranceAreaId As String
    Public Property OJKSumInsuredFrom As Double
    Public Property OJKSumInsuredTo As Double
    Public Property RateBottom As Double
    Public Property RateTop As Double

    Public Property SeqNo() As Integer
        Get
            Return _SeqNo
        End Get
        Set(ByVal Value As Integer)
            _SeqNo = Value
        End Set
    End Property

End Class
