<Serializable()> _
Public Class InsCoTPL : Inherits InsCoBranch
    Private _TPLAmount As Double
    Private _TPLPremium As Double

    Public Property tplamount() As Double
        Get
            Return _TPLAmount
        End Get
        Set(ByVal Value As Double)
            _TPLAmount = Value
        End Set
    End Property

    Public Property TPLPremium() As Double
        Get
            Return _TPLPremium
        End Get
        Set(ByVal Value As Double)
            _TPLPremium = Value
        End Set
    End Property

End Class
