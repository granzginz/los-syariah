<Serializable()>
Public Class InsCoRateAdditional : Inherits InsCoBranch

    Private _tenor As String
    Private _masassBranchId As String
    Private _productId As String
    Private _isActive As Boolean
    Private _currRate As String
    Public Property IsActive() As Boolean
        Get
            Return _isActive
        End Get
        Set(ByVal value As Boolean)
            _isActive = value
        End Set
    End Property

    Public Property ProductID() As String
        Get
            Return _productId
        End Get
        Set(ByVal value As String)
            _productId = value
        End Set
    End Property

    Public Property MaskAssId() As String
        Get
            Return _masassBranchId
        End Get
        Set(ByVal value As String)
            _masassBranchId = value
        End Set
    End Property
    Public Property Tenor() As String
        Get
            Return _tenor
        End Get
        Set(ByVal value As String)
            _tenor = value
        End Set
    End Property

    Public Property CurrRate() As String
        Get
            Return _currRate
        End Get
        Set(ByVal value As String)
            _currRate = value
        End Set
    End Property

    Public Property Rate As Double
End Class
