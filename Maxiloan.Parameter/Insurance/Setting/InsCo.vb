<Serializable()> _
Public Class InsCo : Inherits Common
    Private _InsCoID As String
    Private _InsCoName As String

    Private _BranchIDTo As String
    Private _BranchIDFrom As String

    Private _InsCoIDFrom As String
    Private _InsCoIDTo As String

    Private _listdata As DataTable
    Private _recordcount As Int64

    Private _DtmUpd As DateTime
    Private _mode As String

    Public Property InsCoIDTo() As String
        Get
            Return _InsCoIDTo
        End Get
        Set(ByVal Value As String)
            _InsCoIDTo = Value
        End Set
    End Property

    Public Property BranchIDTo() As String
        Get
            Return _BranchIDTo
        End Get
        Set(ByVal Value As String)
            _BranchIDTo = Value
        End Set
    End Property

    Public Property BranchIDFrom() As String
        Get
            Return _BranchIDFrom
        End Get
        Set(ByVal Value As String)
            _BranchIDFrom = Value
        End Set
    End Property

    Public Property InsCoIDFrom() As String
        Get
            Return _InsCoIDFrom
        End Get
        Set(ByVal Value As String)
            _InsCoIDFrom = Value
        End Set
    End Property


    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property RecordCount() As Int64
        Get
            Return _recordcount
        End Get
        Set(ByVal Value As Int64)
            _recordcount = Value
        End Set
    End Property

    Public Property DtmUpd() As DateTime
        Get
            Return _DtmUpd
        End Get
        Set(ByVal Value As DateTime)
            _DtmUpd = Value
        End Set
    End Property


    Public Property InsCoID() As String
        Get
            Return _InsCoID
        End Get
        Set(ByVal Value As String)
            _InsCoID = Value
        End Set
    End Property

    Public Property InsCoName() As String
        Get
            Return _InsCoName
        End Get
        Set(ByVal Value As String)
            _InsCoName = Value
        End Set
    End Property



    Public Property mode() As String
        Get
            Return _mode
        End Get
        Set(ByVal Value As String)
            _mode = Value
        End Set
    End Property



End Class
