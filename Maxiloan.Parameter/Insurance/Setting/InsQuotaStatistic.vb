


Public Class InsQuotaStatistic : Inherits Common

    Private _listdata As DataTable
    Private _totalrecords As Int64
    Private _BranchID As String
    Private _InsuranceComBranchName As String
    Private _MaskAssID As String
    Private _InsuranceComBranchID As String
    Private _StatisticMonth As Int16
    Private _StrMonth As String
    Private _StatisticYear As Int16
    Private _QuotaAmont As Decimal
    Private _QuotaPercentage As Decimal
    Private _Page As String
    Private _ListReport As DataSet

    Public Property ListReport() As DataSet
        Get
            Return (CType(_ListReport, DataSet))
        End Get
        Set(ByVal Value As DataSet)
            _ListReport = Value
        End Set
    End Property
    Public Property InsuranceComBranchName() As String
        Get
            Return _InsuranceComBranchName
        End Get
        Set(ByVal Value As String)
            _InsuranceComBranchName = Value
        End Set
    End Property
    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property


    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property MaskAssID() As String
        Get
            Return _MaskAssID
        End Get
        Set(ByVal Value As String)
            _MaskAssID = Value
        End Set
    End Property

    Public Property InsuranceComBranchID() As String
        Get
            Return _InsuranceComBranchID
        End Get
        Set(ByVal Value As String)
            _InsuranceComBranchID = Value
        End Set
    End Property

    Public Property StatisticMonth() As Int16
        Get
            Return _StatisticMonth
        End Get
        Set(ByVal Value As Int16)
            _StatisticMonth = Value
        End Set
    End Property

    Public Property StrMonth() As String
        Get
            Return _StrMonth
        End Get
        Set(ByVal Value As String)
            _StrMonth = Value
        End Set
    End Property

    Public Property StatisticYear() As Int16
        Get
            Return _StatisticYear
        End Get
        Set(ByVal Value As Int16)
            _StatisticYear = Value
        End Set
    End Property

    Public Property QuotaAmont() As Decimal
        Get
            Return _QuotaAmont
        End Get
        Set(ByVal Value As Decimal)
            _QuotaAmont = Value
        End Set
    End Property
    Public Property QuotaPercentage() As Decimal
        Get
            Return _QuotaPercentage
        End Get
        Set(ByVal Value As Decimal)
            _QuotaPercentage = Value
        End Set
    End Property
    Public Property Page() As String
        Get
            Return _Page
        End Get
        Set(ByVal Value As String)
            _Page = Value
        End Set
    End Property


End Class
