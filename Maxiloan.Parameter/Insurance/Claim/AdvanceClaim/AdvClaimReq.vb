
<Serializable()> _
Public Class AdvClaimReq : Inherits Maxiloan.Parameter.Common
    Private _listdata As DataTable
    Private _viewgrid As DataTable

    Private _applicationid As String
    Private _agreementno As String
    Private _customername As String
    Private _assetdescription As String
    Private _InsuranceComBranchid As String
    Private _policynumber As String
    Private _maincoverage As String
    Private _customerid As String
    Private _status As String
    Private _assetseqno As Integer
    Private _inssequenceno As Integer
    Private _insclaimseqno As Integer
    Private _canrequest As Boolean
    Private _requestdate As Date
    Private _claimcost As Double
    Private _notes As String
    Private _approveby As String
    Private _approvalno As String
    Private _bankaccountid As String
    Public Property BankAccountId() As String
        Get
            Return _bankaccountid
        End Get
        Set(ByVal Value As String)
            _bankaccountid = Value
        End Set
    End Property
    Public Property ApprovalNo() As String
        Get
            Return _approvalno
        End Get
        Set(ByVal Value As String)
            _approvalno = Value
        End Set
    End Property
    Public Property ApproveBy() As String
        Get
            Return _approveby
        End Get
        Set(ByVal Value As String)
            _approveby = Value

        End Set
    End Property



    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property

    Public Property ClaimCost() As Double
        Get
            Return _claimcost
        End Get
        Set(ByVal Value As Double)
            _claimcost = Value

        End Set
    End Property


    Public Property RequestDate() As Date
        Get
            Return _requestdate
        End Get
        Set(ByVal Value As Date)
            _requestdate = Value
        End Set
    End Property
    Public Property CanRequest() As Boolean
        Get
            Return _canrequest
        End Get
        Set(ByVal Value As Boolean)
            _canrequest = Value
        End Set
    End Property
    Public Property ViewGrid() As DataTable
        Get
            Return _viewgrid
        End Get
        Set(ByVal Value As DataTable)
            _viewgrid = Value

        End Set
    End Property
    Public Property AssetSeqNo() As Integer
        Get
            Return _assetseqno
        End Get
        Set(ByVal Value As Integer)
            _assetseqno = Value

        End Set
    End Property
    Public Property InsSequnceNo() As Integer
        Get
            Return _inssequenceno
        End Get
        Set(ByVal Value As Integer)
            _inssequenceno = Value
        End Set
    End Property
    Public Property InsClaimSeqNo() As Integer
        Get
            Return _insclaimseqno
        End Get
        Set(ByVal Value As Integer)
            _insclaimseqno = Value

        End Set
    End Property


    Public Property ApplicationId() As String
        Get
            Return _applicationid
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property

    Public Property CustomerId() As String
        Get
            Return _customerid
        End Get
        Set(ByVal Value As String)
            _customerid = Value
        End Set
    End Property
    Public Property MainCoverage() As String
        Get
            Return _maincoverage
        End Get
        Set(ByVal Value As String)
            _maincoverage = Value

        End Set
    End Property
    Public Property PolicyNumber() As String
        Get
            Return _policynumber
        End Get
        Set(ByVal Value As String)
            _policynumber = Value
        End Set
    End Property

    Public Property InsuranceComBranchId() As String
        Get
            Return _InsuranceComBranchid
        End Get
        Set(ByVal Value As String)
            _InsuranceComBranchid = Value
        End Set
    End Property

    Public Property AssetDescription() As String
        Get
            Return _assetdescription
        End Get
        Set(ByVal Value As String)
            _assetdescription = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return _customername
        End Get
        Set(ByVal Value As String)
            _customername = Value
        End Set
    End Property


    Public Property AgreementNo() As String
        Get
            Return _agreementno
        End Get
        Set(ByVal Value As String)
            _agreementno = Value

        End Set
    End Property

    Public Property listData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property


End Class
