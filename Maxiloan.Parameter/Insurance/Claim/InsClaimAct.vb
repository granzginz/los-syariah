


<Serializable()> _
Public Class InsClaimAct : Inherits Maxiloan.Parameter.ClaimRequest
    Private _strInsClaimSeqNo As String
    Private _ActivityDate As String
    Private _Activity As String
    Private _Result As String
    Private _Notes As String
    Private _ReceivedFrom As String
    Private _ReceivedocDate As String
    Private _OriginalDoc As String
    Private _CopyDoc As String
    Private _InsDocID As String
    Private _InsAssetType As String
    Private _LetterFromKaditserseDate As String
    Private _OKInscoDate As String
    Private _OKCustDate As String
    Private _InvoiceNo As String
    Private _ActSeqNo As String
    Private _ApprovalStatus As String
    Private _ApprovalNotes As String

    Public Property ActSeqNo() As String
        Get
            Return _ActSeqNo
        End Get
        Set(ByVal Value As String)
            _ActSeqNo = Value
        End Set
    End Property

    Public Property InvoiceNo() As String
        Get
            Return _InvoiceNo
        End Get
        Set(ByVal Value As String)
            _InvoiceNo = Value
        End Set
    End Property

    Public Property OKInscoDate() As String
        Get
            Return _OKInscoDate
        End Get
        Set(ByVal Value As String)
            _OKInscoDate = convertdate(Value)
        End Set
    End Property

    Public Property OKCustDate() As String
        Get
            Return _OKCustDate
        End Get
        Set(ByVal Value As String)
            _OKCustDate = convertdate(Value)
        End Set
    End Property


    Public Property LetterFromKaditserseDate() As String
        Get
            Return _LetterFromKaditserseDate
        End Get
        Set(ByVal Value As String)
            _LetterFromKaditserseDate = convertdate(Value)
        End Set
    End Property

    Public Property InsAssetType() As String
        Get
            Return _InsAssetType
        End Get
        Set(ByVal Value As String)
            _InsAssetType = Value
        End Set
    End Property

    Public Property InsDocID() As String
        Get
            Return _InsDocID
        End Get
        Set(ByVal Value As String)
            _InsDocID = Value
        End Set
    End Property
    Public Property CopyDoc() As String
        Get
            Return _CopyDoc
        End Get
        Set(ByVal Value As String)
            _CopyDoc = Value
        End Set
    End Property

    Public Property OriginalDoc() As String
        Get
            Return _OriginalDoc
        End Get
        Set(ByVal Value As String)
            _OriginalDoc = Value
        End Set
    End Property

    Public Property ReceivedocDate() As String
        Get
            Return _ReceivedocDate
        End Get
        Set(ByVal Value As String)
            _ReceivedocDate = Value
        End Set
    End Property
    Public Property ReceivedFrom() As String
        Get
            Return _ReceivedFrom
        End Get
        Set(ByVal Value As String)
            _ReceivedFrom = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property
    Public Property Result() As String
        Get
            Return _Result
        End Get
        Set(ByVal Value As String)
            _Result = Value
        End Set
    End Property
    Public Property Activity() As String
        Get
            Return _Activity
        End Get
        Set(ByVal Value As String)
            _Activity = Value
        End Set
    End Property
    Public Property ActivityDate() As String
        Get
            Return _ActivityDate
        End Get
        Set(ByVal Value As String)
            _ActivityDate = convertdate(Value)
        End Set
    End Property


    Public Property InsClaimSeqNo() As String
        Get
            Return _strInsClaimSeqNo
        End Get
        Set(ByVal Value As String)
            _strInsClaimSeqNo = Value
        End Set
    End Property

    Public Property ApprovalStatus() As String
        Get
            Return _ApprovalStatus
        End Get
        Set(ByVal Value As String)
            _ApprovalStatus = Value
        End Set
    End Property

    Public Property ApprovalNotes() As String
        Get
            Return _ApprovalNotes
        End Get
        Set(ByVal Value As String)
            _ApprovalNotes = Value
        End Set
    End Property

    Private Function convertdate(ByVal strd As String) As String
        Dim arrdate() As String
        convertdate = strd
        If Not strd.Trim = "" Then
            arrdate = Split(strd, "/")
            convertdate = CType(arrdate(1), String) & "/" & CType(arrdate(0), String) & "/" & CType(arrdate(2), String)
        End If
        Return convertdate

    End Function
End Class
