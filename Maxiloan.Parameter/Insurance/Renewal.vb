

Public Class Renewal
    Inherits InsuranceAssetDetail

    Private _LastTotalMainPremium As Double
    Private _MainPremium As Double
    Private _SRCCPremium As Double
    Private _TPLPremium As Double
    Private _FloodPremium As Double
    Private _LoadingFee As Double

    Public Property LoadingFee() As Double
        Get
            Return _LoadingFee
        End Get
        Set(ByVal Value As Double)
            _LoadingFee = Value
        End Set
    End Property

    Public Property FloodPremium() As Double
        Get
            Return _FloodPremium
        End Get
        Set(ByVal Value As Double)
            _FloodPremium = Value
        End Set
    End Property

    Public Property TPLPremium() As Double
        Get
            Return _TPLPremium
        End Get
        Set(ByVal Value As Double)
            _TPLPremium = Value
        End Set
    End Property

    Public Property SRCCPremium() As Double
        Get
            Return _SRCCPremium
        End Get
        Set(ByVal Value As Double)
            _SRCCPremium = Value
        End Set
    End Property

    Public Property MainPremium() As Double
        Get
            Return _MainPremium
        End Get
        Set(ByVal Value As Double)
            _MainPremium = Value
        End Set
    End Property

    Public Property LastTotalMainPremium() As Double
        Get
            Return _LastTotalMainPremium
        End Get
        Set(ByVal Value As Double)
            _LastTotalMainPremium = Value
        End Set
    End Property




End Class
