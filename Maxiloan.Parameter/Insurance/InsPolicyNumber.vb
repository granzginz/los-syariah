
<Serializable()> _
Public Class InsPolicyNumber : Inherits Common
    Private _listdata As DataTable
    Private _page As String
    Private _totalrecords As Int64
    'Private _branchid As String
    Private _applicationid As String
    Private _assetseqid As Integer
    Private _insseqno As Integer
    Private _customerid As String
    Private _policynumber As String
    Private _oldpolicynumber As String
    Private _yearnum As String

    Public Property OldPolicyNumber() As String
        Get
            Return _oldpolicynumber
        End Get
        Set(ByVal Value As String)
            _oldpolicynumber = Value
        End Set
    End Property
    Public Property Yearnum() As String
        Get
            Return _yearnum
        End Get
        Set(ByVal Value As String)
            _yearnum = Value
        End Set
    End Property


    Public Property ApplicationId() As String
        Get
            Return _applicationid
        End Get
        Set(ByVal Value As String)
            _applicationid = Value

        End Set
    End Property
    Public Property PolicyNumber() As String
        Get
            Return _policynumber
        End Get
        Set(ByVal Value As String)
            _policynumber = Value

        End Set
    End Property
    Public Property AssetSeqId() As Integer
        Get
            Return _assetseqid
        End Get
        Set(ByVal Value As Integer)
            _assetseqid = Value
        End Set
    End Property
    Public Property InsSeqNo() As Integer
        Get
            Return _insseqno
        End Get
        Set(ByVal Value As Integer)
            _insseqno = Value
        End Set
    End Property
    Public Property CustomerId() As String
        Get
            Return _customerid
        End Get
        Set(ByVal Value As String)
            _customerid = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property Page() As String
        Get
            Return _page
        End Get
        Set(ByVal Value As String)
            _page = Value
        End Set
    End Property
    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

End Class
