
<Serializable()> _
Public Class InsuranceAsset : Inherits Common

    Private _ApplicationID As String
    Private _ApplicationType As String
    Private _MainPremimToCust As Double
    Private _LoadingFeeToCust As Double
    Private _TPLToCust As Double
    Private _FloodToCust As Double
    Private _PremiumAmountByCust As Double
    Private _stdPremium As Double
    Private _CapitalizedAmount As Double
    Private _flagInsActivation As String
    Private _flagReNew As String
    Private _flagInsStats As String
    Private _SumInsured As Double
    Private _SellingRate As Int16
    Private _DiscToCustAmount As Int16
    Private _AdminFeeToCust As Double
    Private _MeteraiFeeToCust As Double


    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property


End Class
