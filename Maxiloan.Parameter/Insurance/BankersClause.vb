﻿<Serializable()> _
Public Class BankersClause : Inherits Common

    Public Property NoClause As String
    Public Property TglClause As DateTime
    Public Property InsuranceComBranchID As String
    Public Property ApplicationID As String
    Public Property ContactPersonName As String

    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64

End Class
