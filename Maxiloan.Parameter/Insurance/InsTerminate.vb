
<Serializable()> _
Public Class InsTerminate : Inherits Maxiloan.Parameter.Common
    Private _listdata As DataTable
    Private _strApplicationID As String
    Private _intYearNum As Integer
    Private _ClaimType As String
    Private _AgreementNo As String
    Private _CustomerName As String
    Private _DefectPart As String
    Private _BengkelName As String
    Private _BengkelAddress As String
    Private _BengkelPhone As String
    Private _BengkelPIC As String
    Private _TerminationDate As Date
    Private _EventDate As String
    Private _ReportDate As String
    Private _ReportedBy As String
    Private _ReportedAs As String
    Private _EventLocation As String
    Private _ClaimAmount As Decimal
    Private _ClaimStatus As String
    Private _notes As String
    Private _SurveyDate As String
    Private _ProsesDate As String
    Private _SaveStatus As Boolean
    Private _InsSequenceNo As Integer
    Private _InsClaimNo As String
    Private __AssetSeqNo As Integer
    Private _totalrefundtocust As Decimal
    Private _refunddeduct As Decimal

    Public Property InsClaimNo() As String
        Get
            Return _InsClaimNo
        End Get
        Set(ByVal Value As String)
            _InsClaimNo = Value
        End Set
    End Property
    Public Property AssetSequenceNo() As Integer
        Get
            Return __AssetSeqNo
        End Get
        Set(ByVal Value As Integer)
            __AssetSeqNo = Value
        End Set
    End Property
    Public Property InsSequenceNo() As Integer
        Get
            Return _InsSequenceNo
        End Get
        Set(ByVal Value As Integer)
            _InsSequenceNo = Value
        End Set
    End Property
    Public Property SaveStatus() As Boolean
        Get
            Return _SaveStatus
        End Get
        Set(ByVal Value As Boolean)
            _SaveStatus = Value
        End Set
    End Property
    Public Property ProsesDate() As String
        Get
            Return _ProsesDate
        End Get
        Set(ByVal Value As String)
            _ProsesDate = convertdate(Value)
        End Set
    End Property
    Public Property SurveyDate() As String
        Get
            Return _SurveyDate
        End Get
        Set(ByVal Value As String)
            _SurveyDate = convertdate(Value)
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property

    Public Property ClaimStatus() As String
        Get
            Return _ClaimStatus
        End Get
        Set(ByVal Value As String)
            _ClaimStatus = Value
        End Set
    End Property

    Public Property ClaimAmount() As Decimal
        Get
            Return _ClaimAmount
        End Get
        Set(ByVal Value As Decimal)
            _ClaimAmount = Value
        End Set
    End Property
    Public Property TotalRefundTocust() As Decimal
        Get
            Return _totalrefundtocust
        End Get
        Set(ByVal Value As Decimal)
            _totalrefundtocust = Value
        End Set
    End Property
    Public Property EventLocation() As String
        Get
            Return _EventLocation
        End Get
        Set(ByVal Value As String)
            _EventLocation = Value
        End Set
    End Property
    Public Property ReportedAs() As String
        Get
            Return _ReportedAs
        End Get
        Set(ByVal Value As String)
            _ReportedAs = Value
        End Set
    End Property

    Public Property ReportedBy() As String
        Get
            Return _ReportedBy
        End Get
        Set(ByVal Value As String)
            _ReportedBy = Value
        End Set
    End Property


    Public Property ReportDate() As String
        Get
            Return _ReportDate
        End Get
        Set(ByVal Value As String)
            _ReportDate = convertdate(Value)
        End Set
    End Property

    Public Property EventDate() As String
        Get
            Return _EventDate
        End Get
        Set(ByVal Value As String)
            _EventDate = convertdate(Value)
        End Set
    End Property
    Public Property TerminationDate() As Date
        Get
            Return _TerminationDate
        End Get
        Set(ByVal Value As Date)
            _TerminationDate = Value
        End Set
    End Property


    Public Property BengkelPIC() As String
        Get
            Return _BengkelPIC
        End Get
        Set(ByVal Value As String)
            _BengkelPIC = Value
        End Set
    End Property

    Public Property BengkelPhone() As String
        Get
            Return _BengkelPhone
        End Get
        Set(ByVal Value As String)
            _BengkelPhone = Value
        End Set
    End Property

    Public Property BengkelAddress() As String
        Get
            Return _BengkelAddress
        End Get
        Set(ByVal Value As String)
            _BengkelAddress = Value
        End Set
    End Property

    Public Property BengkelName() As String
        Get
            Return _BengkelName
        End Get
        Set(ByVal Value As String)
            _BengkelName = Value
        End Set
    End Property


    Public Property DefectPart() As String
        Get
            Return _DefectPart
        End Get
        Set(ByVal Value As String)
            _DefectPart = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return _CustomerName
        End Get
        Set(ByVal Value As String)
            _CustomerName = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property
    Public Property ClaimType() As String
        Get
            Return _ClaimType
        End Get
        Set(ByVal Value As String)
            _ClaimType = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property YearNum() As Integer
        Get
            Return _intYearNum
        End Get
        Set(ByVal Value As Integer)
            _intYearNum = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return _strApplicationID
        End Get
        Set(ByVal Value As String)
            _strApplicationID = Value
        End Set
    End Property

    Public Property RefundDeduct() As Decimal
        Get
            Return _refunddeduct
        End Get
        Set(ByVal Value As Decimal)
            _refunddeduct = Value
        End Set
    End Property
    Private Function convertdate(ByVal strd As String) As String
        Dim arrdate() As String
        convertdate = strd
        If Not strd.Trim = "" Then
            arrdate = Split(strd, "/")
            convertdate = CType(arrdate(1), String) & "/" & CType(arrdate(0), String) & "/" & CType(arrdate(2), String)
        End If
        Return convertdate
    End Function
End Class