
<Serializable()> _
Public Class SPPA : Inherits Common
    Private _ListData As DataTable
    Private _PageSource As String
    Private _AgreementNo As String
    Private _BulkOfApplicationID As String
    Private _SPPANo As String
    Private _MaskAssBranchID As String
    Private _MaskAssBranchName As String
    Private _TermOfPayment As String
    Private _ProductAsuransi As String
    Public Property TermOfPayment() As String
        Get
            Return _TermOfPayment
        End Get
        Set(ByVal Value As String)
            _TermOfPayment = Value
        End Set
    End Property
    Public Property MaskAssBranchName() As String
        Get
            Return _MaskAssBranchName
        End Get
        Set(ByVal Value As String)
            _MaskAssBranchName = Value
        End Set
    End Property

    Public Property BulkOfApplicationID() As String
        Get
            Return _BulkOfApplicationID
        End Get
        Set(ByVal Value As String)
            _BulkOfApplicationID = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property


    Public Property PageSource() As String
        Get
            Return _PageSource
        End Get
        Set(ByVal Value As String)
            _PageSource = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal Value As DataTable)
            _ListData = Value
        End Set
    End Property
    Public Property SPPANo() As String
        Get
            Return _SPPANo
        End Get
        Set(ByVal Value As String)
            _SPPANo = Value
        End Set
    End Property
    Public Property MaskAssBranchID() As String
        Get
            Return _MaskAssBranchID
        End Get
        Set(ByVal Value As String)
            _MaskAssBranchID = Value
        End Set
    End Property
    Public Property ProductAsuransi() As String
        Get
            Return _ProductAsuransi
        End Get
        Set(ByVal Value As String)
            _ProductAsuransi = Value
        End Set
    End Property


End Class
