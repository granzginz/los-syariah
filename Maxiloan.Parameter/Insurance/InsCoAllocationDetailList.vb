
<Serializable()> _
Public Class InsCoAllocationDetailList : Inherits InsCoAllocation

    Private _ApplicationID As String
    Private _CustomerID As String
    Private _CustomerName As String
    Private _GoLiveDate As Date
    Private _AgreementNo As String
    Private _InsuredByDescr As String
    Private _PaidByDescr As String
    Private _SumInsured As Double
    Private _AmountCoverage As Double
    Private _MainPremiumToCust As Double
    Private _SRCCToCust As Double
    Private _TPLToCust As Double
    Private _FloodToCust As Double
    Private _LoadingFeeToCust As Double
    Private _AdminFeeToCust As Double
    Private _MeteraiFeeToCust As Double
    Private _AssetTypeDescr As String
    Private _AssetMasterDescr As String
    Private _ApplicationTypeDescr As String
    Private _InsuranceTypeDescr As String
    Private _AssetUsageDescr As String
    Private _StatMonth As Int16
    Private _StatYear As Int16
    Private _UsedNew As String
    Private _TotalStdPremium As Double
    Private _DataExist As Boolean
    Private _OutPutCoverPeriodSelection As Boolean
    Private _CoverPeriodSelection As String
    Private _StartDate As Date
    Private _EndDate As Date
    Private _InsCoSelectionDate As Date
    Private _InsuranceComBranchID As String
    Private _InsuranceComBranchName As String
    Private _RateToInsco As Double
    Private _MainPremiumToInsco As Double
    Private _RSCCToInsco As Double
    Private _TPLToInsco As Double
    Private _FloodToInsco As Double
    Private _LoadingFeeToInsco As Double
    Private _TotalPremiumToInsco As Double
    Private _TotalAdminFee As Double
    Private _AdminFee As Double
    Private _MeteraiFee As Double
    Private _TotalStampDutyFee As Double
    Private _SPPADate As Date
    Private _SPPANo As String
    Private _AccNotes As String
    Private _InsNotes As String
    Private _PolicyNumber As String
    Private _PolicyReceiveDate As Date
    Private _PremiumAmountByCust As Double
    Private _PremiumAmountByCustBeforeDisc As Double
    Private _PaidAmountByCust As Double
    Private _PremiumAmountToInsco As Double
    Private _PaidAmountToInsco As Double
    Private _BDEndorsDocNo As String
    Private _BDEndorsPolicyNo As String
    Private _BDEndorsDate As Date
    Private _InvoiceNo As String
    Private _InvoiceDate As Date
    Private _DueDate As Date
    Private _TotalInvoiceAmount As Double
    Private _PremiumToBePaid As Double
    Private _PremiumEndorsmentToInsco As Double
    Private _DiscToCustAmount As Double
    Private _FlagInsActivation As String
    Private _AssetseqNo As Int16
    Private _InsseqNo As Int16
    Private _SerialNo1 As String
    Private _SerialNo2 As String
    Private _ApplicationType As String
    Private _ManufacturingYear As String
    Private _CoverageType As String
    Private _TotalMainPremium As Double
    Private _MessageSave As String
    Private _StartDateRenewal As Date
    Private _MaturityDate As Date
    Private _MaxYearNum As Int16
    Private _Tenor As Int16
    Private _Tenor2 As Int16
    Private _AssetYear As String
    Private _EffectiveDate As Date
    Private _PerluasanPA As Boolean

    Public Property EffectiveDate() As Date
        Get
            Return _EffectiveDate
        End Get
        Set(ByVal Value As Date)
            _EffectiveDate = Value
        End Set
    End Property

    Public Property AssetYear() As String
        Get
            Return _AssetYear
        End Get
        Set(ByVal Value As String)
            _AssetYear = Value
        End Set
    End Property

    Public Property Tenor() As Int16
        Get
            Return _Tenor
        End Get
        Set(ByVal Value As Int16)
            _Tenor = Value
        End Set
    End Property
    Public Property Tenor2() As Int16
        Get
            Return _Tenor2
        End Get
        Set(ByVal Value As Int16)
            _Tenor2 = Value
        End Set
    End Property


    Public Property MaxYearNum() As Int16
        Get
            Return _MaxYearNum
        End Get
        Set(ByVal Value As Int16)
            _MaxYearNum = Value
        End Set
    End Property

    Public Property MaturityDate() As Date
        Get
            Return _MaturityDate
        End Get
        Set(ByVal Value As Date)
            _MaturityDate = Value
        End Set
    End Property

    Public Property StartDateRenewal() As Date
        Get
            Return _StartDateRenewal
        End Get
        Set(ByVal Value As Date)
            _StartDateRenewal = Value
        End Set
    End Property

    Public Property MessageSave() As String
        Get
            Return _MessageSave
        End Get
        Set(ByVal Value As String)
            _MessageSave = Value
        End Set
    End Property

    Public Property TotalMainPremium() As Double
        Get
            Return _TotalMainPremium
        End Get
        Set(ByVal Value As Double)
            _TotalMainPremium = Value
        End Set
    End Property

    Public Property CoverageType() As String
        Get
            Return _CoverageType
        End Get
        Set(ByVal Value As String)
            _CoverageType = Value
        End Set
    End Property


    Public Property ManufacturingYear() As String
        Get
            Return _ManufacturingYear
        End Get
        Set(ByVal Value As String)
            _ManufacturingYear = Value
        End Set
    End Property

    Public Property ApplicationType() As String
        Get
            Return _ApplicationType
        End Get
        Set(ByVal Value As String)
            _ApplicationType = Value
        End Set
    End Property



    Public Property SerialNo1() As String
        Get
            Return _SerialNo1
        End Get
        Set(ByVal Value As String)
            _SerialNo1 = Value
        End Set
    End Property

    Public Property SerialNo2() As String
        Get
            Return _SerialNo2
        End Get
        Set(ByVal Value As String)
            _SerialNo2 = Value
        End Set
    End Property

    Public Property InsseqNo() As Int16
        Get
            Return _InsseqNo
        End Get
        Set(ByVal Value As Int16)
            _InsseqNo = Value
        End Set
    End Property


    Public Property AssetseqNo() As Int16
        Get
            Return _AssetseqNo
        End Get
        Set(ByVal Value As Int16)
            _AssetseqNo = Value
        End Set
    End Property


    Public Property FlagInsActivation() As String
        Get
            Return _FlagInsActivation
        End Get
        Set(ByVal Value As String)
            _FlagInsActivation = Value
        End Set
    End Property

    Public Property DiscToCustAmount() As Double
        Get
            Return _DiscToCustAmount
        End Get
        Set(ByVal Value As Double)
            _DiscToCustAmount = Value
        End Set
    End Property

    Public Property PremiumEndorsmentToInsco() As Double
        Get
            Return _PremiumEndorsmentToInsco
        End Get
        Set(ByVal Value As Double)
            _PremiumEndorsmentToInsco = Value
        End Set
    End Property


    Public Property PremiumToBePaid() As Double
        Get
            Return _PremiumToBePaid
        End Get
        Set(ByVal Value As Double)
            _PremiumToBePaid = Value
        End Set
    End Property

    Public Property TotalInvoiceAmount() As Double
        Get
            Return _TotalInvoiceAmount
        End Get
        Set(ByVal Value As Double)
            _TotalInvoiceAmount = Value
        End Set
    End Property


    Public Property InvoiceDate() As Date
        Get
            Return _InvoiceDate
        End Get
        Set(ByVal Value As Date)
            _InvoiceDate = Value
        End Set
    End Property

    Public Property DueDate() As Date
        Get
            Return _DueDate
        End Get
        Set(ByVal Value As Date)
            _DueDate = Value
        End Set
    End Property
    Public Property InvoiceNo() As String
        Get
            Return _InvoiceNo
        End Get
        Set(ByVal Value As String)
            _InvoiceNo = Value
        End Set
    End Property
    Public Property BDEndorsDate() As Date
        Get
            Return _BDEndorsDate
        End Get
        Set(ByVal Value As Date)
            _BDEndorsDate = Value
        End Set
    End Property

    Public Property BDEndorsPolicyNo() As String
        Get
            Return _BDEndorsPolicyNo
        End Get
        Set(ByVal Value As String)
            _BDEndorsPolicyNo = Value
        End Set
    End Property



    Public Property BDEndorsDocNo() As String
        Get
            Return _BDEndorsDocNo
        End Get
        Set(ByVal Value As String)
            _BDEndorsDocNo = Value
        End Set
    End Property


    Public Property PremiumAmountToInsco() As Double
        Get
            Return _PremiumAmountToInsco
        End Get
        Set(ByVal Value As Double)
            _PremiumAmountToInsco = Value
        End Set
    End Property


    Public Property PaidAmountToInsco() As Double
        Get
            Return _PaidAmountToInsco
        End Get
        Set(ByVal Value As Double)
            _PaidAmountToInsco = Value
        End Set
    End Property

    Public Property PaidAmountByCust() As Double
        Get
            Return _PaidAmountByCust
        End Get
        Set(ByVal Value As Double)
            _PaidAmountByCust = Value
        End Set
    End Property

    Public Property PremiumAmountByCust() As Double
        Get
            Return _PremiumAmountByCust
        End Get
        Set(ByVal Value As Double)
            _PremiumAmountByCust = Value
        End Set
    End Property
    Public Property PremiumAmountByCustBeforeDisc() As Double
        Get
            Return _PremiumAmountByCustBeforeDisc
        End Get
        Set(ByVal Value As Double)
            _PremiumAmountByCustBeforeDisc = Value
        End Set
    End Property


    Public Property PolicyReceiveDate() As Date
        Get
            Return _PolicyReceiveDate
        End Get
        Set(ByVal Value As Date)
            _PolicyReceiveDate = Value
        End Set
    End Property

    Public Property PolicyNumber() As String
        Get
            Return _PolicyNumber
        End Get
        Set(ByVal PolicyNumber As String)
            _PolicyNumber = PolicyNumber
        End Set
    End Property

    Public Property SPPANo() As String
        Get
            Return _SPPANo
        End Get
        Set(ByVal Value As String)
            _SPPANo = Value
        End Set
    End Property


    Public Property InsuranceTypeDescr() As String
        Get
            Return _InsuranceTypeDescr
        End Get
        Set(ByVal Value As String)
            _InsuranceTypeDescr = Value
        End Set
    End Property

    Public Property InsNotes() As String
        Get
            Return _InsNotes
        End Get
        Set(ByVal Value As String)
            _InsNotes = Value
        End Set
    End Property

    Public Property AccNotes() As String
        Get
            Return _AccNotes
        End Get
        Set(ByVal Value As String)
            _AccNotes = Value
        End Set
    End Property

    Public Property SPPADate() As Date
        Get
            Return _SPPADate
        End Get
        Set(ByVal Value As Date)
            _SPPADate = Value
        End Set
    End Property

    Public Property InsCoSelectionDate() As Date
        Get
            Return _InsCoSelectionDate
        End Get
        Set(ByVal Value As Date)
            _InsCoSelectionDate = Value
        End Set
    End Property

    Public Property SumInsured() As Double
        Get
            Return _SumInsured
        End Get
        Set(ByVal Value As Double)
            _SumInsured = Value
        End Set
    End Property


    Public Property RSCCToInsco() As Double
        Get
            Return _RSCCToInsco
        End Get
        Set(ByVal Value As Double)
            _RSCCToInsco = Value
        End Set
    End Property


    Public Property TPLToInsco() As Double
        Get
            Return _TPLToInsco
        End Get
        Set(ByVal Value As Double)
            _TPLToInsco = Value
        End Set
    End Property

    Public Property FloodToInsco() As Double
        Get
            Return _FloodToInsco
        End Get
        Set(ByVal Value As Double)
            _FloodToInsco = Value
        End Set
    End Property


    Public Property LoadingFeeToInsco() As Double
        Get
            Return _LoadingFeeToInsco
        End Get
        Set(ByVal Value As Double)
            _LoadingFeeToInsco = Value
        End Set
    End Property


    Public Property TotalPremiumToInsco() As Double
        Get
            Return _TotalPremiumToInsco
        End Get
        Set(ByVal Value As Double)
            _TotalPremiumToInsco = Value
        End Set
    End Property

    Public Property TotalAdminFee() As Double
        Get
            Return _TotalAdminFee
        End Get
        Set(ByVal Value As Double)
            _TotalAdminFee = Value
        End Set
    End Property

    Public Property AdminFee() As Double
        Get
            Return _AdminFee
        End Get
        Set(ByVal Value As Double)
            _AdminFee = Value
        End Set
    End Property


    Public Property TotalStampDutyFee() As Double
        Get
            Return _TotalStampDutyFee
        End Get
        Set(ByVal Value As Double)
            _TotalStampDutyFee = Value
        End Set
    End Property

    Public Property MeteraiFee() As Double
        Get
            Return _MeteraiFee
        End Get
        Set(ByVal Value As Double)
            _MeteraiFee = Value
        End Set
    End Property



    Public Property MainPremiumToInsco() As Double
        Get
            Return _MainPremiumToInsco
        End Get
        Set(ByVal Value As Double)
            _MainPremiumToInsco = Value
        End Set
    End Property

    Public Property RateToInsco() As Double
        Get
            Return _RateToInsco
        End Get
        Set(ByVal Value As Double)
            _RateToInsco = Value
        End Set
    End Property

    Public Property InsuranceComBranchName() As String
        Get
            Return _InsuranceComBranchName
        End Get
        Set(ByVal Value As String)
            _InsuranceComBranchName = Value
        End Set
    End Property


    Public Property InsuranceComBranchID() As String
        Get
            Return _InsuranceComBranchID
        End Get
        Set(ByVal Value As String)
            _InsuranceComBranchID = Value
        End Set
    End Property

    Public Property StartDate() As Date
        Get
            Return _StartDate
        End Get
        Set(ByVal Value As Date)
            _StartDate = Value
        End Set
    End Property

    Public Property EndDate() As Date
        Get
            Return _EndDate
        End Get
        Set(ByVal Value As Date)
            _EndDate = Value
        End Set
    End Property

    Public Property CoverPeriodSelection() As String
        Get
            Return _CoverPeriodSelection
        End Get
        Set(ByVal Value As String)
            _CoverPeriodSelection = Value
        End Set
    End Property

    Public Property DataExist() As Boolean
        Get
            Return _DataExist
        End Get
        Set(ByVal Value As Boolean)
            _DataExist = Value
        End Set
    End Property


    Public Property OutPutCoverPeriodSelection() As Boolean
        Get
            Return _OutPutCoverPeriodSelection
        End Get
        Set(ByVal Value As Boolean)
            _OutPutCoverPeriodSelection = Value
        End Set
    End Property

    Public Property TotalStdPremium() As Double
        Get
            Return _TotalStdPremium
        End Get
        Set(ByVal Value As Double)
            _TotalStdPremium = Value
        End Set
    End Property

    Public Property UsedNew() As String
        Get
            Return _UsedNew
        End Get
        Set(ByVal Value As String)
            _UsedNew = Value
        End Set
    End Property

    Public Property StatMonth() As Int16
        Get
            Return _StatMonth
        End Get
        Set(ByVal Value As Int16)
            _StatMonth = Value
        End Set
    End Property

    Public Property StatYear() As Int16
        Get
            Return _StatYear
        End Get
        Set(ByVal Value As Int16)
            _StatYear = Value
        End Set
    End Property

    Public Property AssetMasterDescr() As String
        Get
            Return _AssetMasterDescr
        End Get
        Set(ByVal Value As String)
            _AssetMasterDescr = Value
        End Set
    End Property



    Public Property AssetUsageDescr() As String
        Get
            Return _AssetUsageDescr
        End Get
        Set(ByVal Value As String)
            _AssetUsageDescr = Value
        End Set
    End Property


    Public Property MainPremiumToCust() As Double
        Get
            Return _MainPremiumToCust
        End Get
        Set(ByVal Value As Double)
            _MainPremiumToCust = Value
        End Set
    End Property

    Public Property SRCCToCust() As Double
        Get
            Return _SRCCToCust
        End Get
        Set(ByVal Value As Double)
            _SRCCToCust = Value
        End Set
    End Property

    Public Property TPLToCust() As Double
        Get
            Return _TPLToCust
        End Get
        Set(ByVal Value As Double)
            _TPLToCust = Value
        End Set
    End Property


    Public Property FloodToCust() As Double
        Get
            Return _FloodToCust
        End Get
        Set(ByVal Value As Double)
            _FloodToCust = Value
        End Set
    End Property


    Public Property LoadingFeeToCust() As Double
        Get
            Return _LoadingFeeToCust
        End Get
        Set(ByVal Value As Double)
            _LoadingFeeToCust = Value
        End Set
    End Property


    Public Property AdminFeeToCust() As Double
        Get
            Return _AdminFeeToCust
        End Get
        Set(ByVal Value As Double)
            _AdminFeeToCust = Value
        End Set
    End Property


    Public Property MeteraiFeeToCust() As Double
        Get
            Return _MeteraiFeeToCust
        End Get
        Set(ByVal Value As Double)
            _MeteraiFeeToCust = Value
        End Set
    End Property

    Public Property AssetTypeDescr() As String
        Get
            Return _AssetTypeDescr
        End Get
        Set(ByVal Value As String)
            _AssetTypeDescr = Value
        End Set
    End Property
    Public Property ApplicationTypeDescr() As String
        Get
            Return _ApplicationTypeDescr
        End Get
        Set(ByVal Value As String)
            _ApplicationTypeDescr = Value
        End Set
    End Property
    Public Property AmountCoverage() As Double
        Get
            Return _AmountCoverage
        End Get
        Set(ByVal Value As Double)
            _AmountCoverage = Value
        End Set
    End Property
    Public Property PaidByDescr() As String
        Get
            Return _PaidByDescr
        End Get
        Set(ByVal Value As String)
            _PaidByDescr = Value
        End Set
    End Property
    Public Property InsuredByDescr() As String
        Get
            Return _InsuredByDescr
        End Get
        Set(ByVal Value As String)
            _InsuredByDescr = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property
    Public Property GoLiveDate() As Date
        Get
            Return _GoLiveDate
        End Get
        Set(ByVal Value As Date)
            _GoLiveDate = Value
        End Set
    End Property
    Public Property CustomerID() As String
        Get
            Return _CustomerID
        End Get
        Set(ByVal Value As String)
            _CustomerID = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return _CustomerName
        End Get
        Set(ByVal Value As String)
            _CustomerName = Value
        End Set
    End Property
    Public Property PerluasanPA() As Boolean
        Get
            Return _PerluasanPA
        End Get
        Set(value As Boolean)
            _PerluasanPA = value
        End Set
    End Property

    Public Property IsInscoSelectOnEntriApp As Boolean
    Public Property PAPassanger As Double
    Public Property PADriver As Double
    Public Property BiayaPolis As Double

    Public Property strWithTPL As String
    Public Property strWithTPLAmount As String
    Public Property strWithFlood As String
    Public Property strWithEQVET As String
    Public Property strWithSRCC As String
    Public Property strWithTerrorism As String
    Public Property strWithHH As String
    Public Property strWithPA As String
    Public Property strWithPAAmount As String
    Public Property strWithPADriver As String
    Public Property strWithPADriverAmount As String
    Public Property strCoverageType As String
    Public Property NumRows As Int32    
    Public Property PAToCust As Double
    Public Property EQVETToCust As Double
    Public Property TERRORISMToCust As Double
    Public Property PADriverToCust As Double
    Public Property PAToInsco As Double
    Public Property EQVETToInsco As Double
    Public Property TERRORISMToInsco As Double
    Public Property PADriverToInsco As Double            
    Public Property InsSequenceNo As Integer
    Public Property MaskAssID As String
    Public Property AccountPayableNo As String    
    Public Property DueDateEntry As DateTime    
    Public Property TotalTagihanItem As Decimal    
    Public Property TotalTagihanAll As Decimal
    Public Property TotalPremiManual As Double
    Public Property DataTablePaid As DataTable
    Public Property InsAmountToNPV As Double
    Public Property InsuranceType As String
    Public Property UsageID As String

    Public Property PaidByCreditProtection As String
    Public Property PremiAdditionalSignCreditProtection As String
    Public Property PremiAdditionalAmountCreditProtection As Double
    Public Property PremiNettCreditProtection As Double

    Public Property PaidByJaminanCredit As String
    Public Property PremiAdditionalSignJaminanCredit As String
    Public Property PremiAdditionalAmountJaminanCredit As Double
    Public Property PremiNettJaminanCredit As Double

End Class
