

<Serializable()> _
Public Class InsuranceTermination : Inherits Common
#Region "Constanta"
    Private _listdata As DataTable
    Private _listdataset As DataSet
    Private _totalrecords As Int64
    Private _spName As String
    Private _inslength As Integer
    Private _mainpremiumtocustdetail As Integer
    Private _mainpremiumtoinsco As Integer
    Private _mainpremiumtocustomer As Integer
    Private _premiumamounttoinsco As Integer
    Private _premiumamountbycust As Integer
    Private _isrefundable As String
    Private _yearnumaktif As Integer
    Private _startdate As String
    Private _enddate As String
    Private _enddateaktif As String
    Private _enddatedetail As String
    Private _terminatedate As String
    Private _contractstatus As String
    Private _applicationid As String
    Private _refundtocust As Double
    Private _refundfrominsco As Double
    Private _restofinsperiodbytfs As Integer
    Private _restofinsperiodbycust As Integer
    Private _deductionpercentage As Integer
    Private _deductionamount As Double
    Private _totalrefundtocust As Double
    Private _gainloss As Double
    Private _flag As String
#End Region

#Region "Property"
    Public Property spName() As String
        Get
            Return _spName
        End Get
        Set(ByVal Value As String)
            _spName = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property ListDataSet() As DataSet
        Get
            Return _listdataset
        End Get
        Set(ByVal Value As DataSet)
            _listdataset = Value
        End Set
    End Property

    Public Property InsLength() As Integer
        Get
            Return _inslength
        End Get
        Set(ByVal Value As Integer)
            _inslength = Value
        End Set
    End Property

    Public Property MainPremiumToCustDetail() As Integer
        Get
            Return _mainpremiumtocustdetail
        End Get
        Set(ByVal Value As Integer)
            _mainpremiumtocustdetail = Value
        End Set
    End Property

    Public Property MainPremiumToInsCo() As Integer
        Get
            Return _mainpremiumtoinsco
        End Get
        Set(ByVal Value As Integer)
            _mainpremiumtoinsco = Value
        End Set
    End Property

    Public Property MainPremiumToCustomer() As Integer
        Get
            Return _mainpremiumtocustomer
        End Get
        Set(ByVal Value As Integer)
            _mainpremiumtocustomer = Value
        End Set
    End Property

    Public Property PremiumAmountToInsCo() As Integer
        Get
            Return _premiumamounttoinsco
        End Get
        Set(ByVal Value As Integer)
            _premiumamounttoinsco = Value
        End Set
    End Property

    Public Property PremiumAmountByCustomer() As Integer
        Get
            Return _premiumamountbycust
        End Get
        Set(ByVal Value As Integer)
            _premiumamountbycust = Value
        End Set
    End Property

    Public Property IsRefundable() As String
        Get
            Return _isrefundable
        End Get
        Set(ByVal Value As String)
            _isrefundable = Value
        End Set
    End Property

    Public Property ContractStatus() As String
        Get
            Return _contractstatus
        End Get
        Set(ByVal Value As String)
            _contractstatus = Value
        End Set
    End Property

    Public Property YearNumAktif() As Integer
        Get
            Return _yearnumaktif
        End Get
        Set(ByVal Value As Integer)
            _yearnumaktif = Value
        End Set
    End Property

    Public Property RestofInsPeriodByTFS() As Integer
        Get
            Return _restofinsperiodbytfs
        End Get
        Set(ByVal Value As Integer)
            _restofinsperiodbytfs = Value
        End Set
    End Property

    Public Property RestofInsPeriodByCust() As Integer
        Get
            Return _restofinsperiodbycust
        End Get
        Set(ByVal Value As Integer)
            _restofinsperiodbycust = Value
        End Set
    End Property

    Public Property DeductionPercentage() As Integer
        Get
            Return _deductionpercentage
        End Get
        Set(ByVal Value As Integer)
            _deductionpercentage = Value
        End Set
    End Property

    Public Property StartDate() As String
        Get
            Return _startdate
        End Get
        Set(ByVal Value As String)
            _startdate = Value
        End Set
    End Property

    Public Property EndDate() As String
        Get
            Return _enddate
        End Get
        Set(ByVal Value As String)
            _enddate = Value
        End Set
    End Property

    Public Property EndDateAktif() As String
        Get
            Return _enddateaktif
        End Get
        Set(ByVal Value As String)
            _enddateaktif = Value
        End Set
    End Property

    Public Property EndDateDetail() As String
        Get
            Return _enddatedetail
        End Get
        Set(ByVal Value As String)
            _enddatedetail = Value
        End Set
    End Property

    Public Property TerminateDate() As String
        Get
            Return _terminatedate
        End Get
        Set(ByVal Value As String)
            _terminatedate = Value
        End Set
    End Property

    Public Property ApplicationId() As String
        Get
            Return _applicationid
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property

    Public Property Flag() As String
        Get
            Return _flag
        End Get
        Set(ByVal Value As String)
            _flag = Value
        End Set
    End Property

    Public Property RefundToCust() As Double
        Get
            Return _refundtocust
        End Get
        Set(ByVal Value As Double)
            _refundtocust = Value
        End Set
    End Property

    Public Property RefundFromInsCo() As Double
        Get
            Return _refundfrominsco
        End Get
        Set(ByVal Value As Double)
            _refundfrominsco = Value
        End Set
    End Property

    Public Property DeductionAmount() As Double
        Get
            Return _deductionamount
        End Get
        Set(ByVal Value As Double)
            _deductionamount = Value
        End Set
    End Property

    Public Property TotalRefundToCust() As Double
        Get
            Return _totalrefundtocust
        End Get
        Set(ByVal Value As Double)
            _totalrefundtocust = Value
        End Set
    End Property

    Public Property GainLoss() As Double
        Get
            Return _gainloss
        End Get
        Set(ByVal Value As Double)
            _gainloss = Value
        End Set
    End Property
#End Region

End Class
