


<Serializable()> _
Public Class InsEndorsmentReport : Inherits Maxiloan.Parameter.Common
    Private _listdata As DataSet
    Private _strApplicationID As String

    Private _wherecond As String


    Public Property ListData() As DataSet
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataSet)
            _listdata = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return _strApplicationID
        End Get
        Set(ByVal Value As String)
            _strApplicationID = Value
        End Set
    End Property

End Class