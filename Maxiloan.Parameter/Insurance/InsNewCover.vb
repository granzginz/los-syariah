
<Serializable()> _
Public Class InsNewCover : Inherits Maxiloan.Parameter.Common
    Private _InsAdminFee As Double
    Private _InsAdminFeeBehaviour As String
    Private _InsStampDutyFee As Double
    Private _InsStampDutyFeeBehaviour As String
    Private _TotalOTR As Double
    Private _AssetUsageID As String
    Private _AssetUsageDescr As String
    Private _Tenor As Double
    Private _AssetMasterDescr As String
    Private _InsuranceAssetDescr As String
    Private _AssetUsageNewUsed As String
    Private _MinimumTenor As Int16
    Private _MaximumTenor As Int16
    Private _PBMaximumTenor As Int16
    Private _PBMinimumTenor As Int16
    Private _PMaximumTenor As Int16
    Private _PMinimumTenor As Int16
    Private _MaskAssID As String
    Private _InsuranceComBranchID As String
    Private _InsuranceComBranchName As String
    Private _YearInsurance As Int16
    Private _CoverageType As String
    Private _Premium As Double
    Private _SRCC As Boolean
    Private _SRCCAmount As Double
    Private _TPL As Double
    Private _TPLAmount As Double
    Private _Flood As Boolean
    Private _FloodAmount As Double
    Private _Total As Double
    Private _PaidByCust As String
    Private _JmlGrid As Int16
    Private _InsuranceType As String
    Private _ManufacturingYear As String
    Private _applicationtypedescr As String
    Private _refundtosupplier As Double
    Private _prepaid As Double
    Private _applicationid As String
    Private _customername As String
    Private _customerid As String
    Private _insassetinsuredbyname As String
    Private _insassetpaidbyname As String
    Private _spname As String
    Private _maturitydate As DateTime
    Private _businessdate As DateTime
    Private _listdata As DataTable
    Private _inslength As Integer
    Private _amountcoverage As Decimal
    Private _adminfeetocust As Decimal
    Private _meteraifeetocust As Decimal
    Private _disctocustamount As Decimal
    Private _accnotes As String
    Private _insnotes As String
    Private _premiumtocustamount As Decimal
    Private _suminsured As Decimal
    Private _policyno As String
    Private _expireddate As Date
    Private _insurancecompanyname As String
    Private _ispagesource As Boolean

    Public Property IsPageSource() As Boolean
        Get
            Return _ispagesource
        End Get
        Set(ByVal Value As Boolean)
            _ispagesource = Value
        End Set
    End Property

    Public Property InsuranceCompanyName() As String
        Get
            Return _insurancecompanyname
        End Get
        Set(ByVal Value As String)
            _insurancecompanyname = Value
        End Set
    End Property

    Public Property ExpiredDate() As Date
        Get
            Return _expireddate
        End Get
        Set(ByVal Value As Date)
            _expireddate = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property MaturityDate() As DateTime
        Get
            Return _maturitydate
        End Get
        Set(ByVal Value As DateTime)
            _maturitydate = Value
        End Set
    End Property

    Public Property AmountCoverage() As Decimal
        Get
            Return _amountcoverage
        End Get
        Set(ByVal Value As Decimal)
            _amountcoverage = Value
        End Set
    End Property
    Public Property SumInsured() As Decimal
        Get
            Return _suminsured
        End Get
        Set(ByVal Value As Decimal)
            _suminsured = Value
        End Set
    End Property
    Public Property AdminFeeToCust() As Decimal
        Get
            Return _adminfeetocust
        End Get
        Set(ByVal Value As Decimal)
            _adminfeetocust = Value
        End Set
    End Property
    Public Property DiscToCustAmount() As Decimal
        Get
            Return _disctocustamount
        End Get
        Set(ByVal Value As Decimal)
            _disctocustamount = Value
        End Set
    End Property
    Public Property MeteraiFeeToCust() As Decimal
        Get
            Return _meteraifeetocust
        End Get
        Set(ByVal Value As Decimal)
            _meteraifeetocust = Value
        End Set
    End Property
    Public Property PremiumToCustAmount() As Decimal
        Get
            Return _premiumtocustamount
        End Get
        Set(ByVal Value As Decimal)
            _premiumtocustamount = Value
        End Set
    End Property


    Public Property Prepaid() As Double
        Get
            Return _prepaid
        End Get
        Set(ByVal Value As Double)
            _prepaid = Value
        End Set
    End Property
    Public Property RefundToSupplier() As Double
        Get
            Return _refundtosupplier
        End Get
        Set(ByVal Value As Double)
            _refundtosupplier = Value
        End Set
    End Property
    Public Property PolicyNo() As String
        Get
            Return _policyno
        End Get
        Set(ByVal Value As String)
            _policyno = Value
        End Set
    End Property
    Public Property SpName() As String
        Get
            Return _spname
        End Get
        Set(ByVal Value As String)
            _spname = Value
        End Set
    End Property
    Public Property AccNotes() As String
        Get
            Return _accnotes
        End Get
        Set(ByVal Value As String)
            _accnotes = Value
        End Set
    End Property
    Public Property InsNotes() As String
        Get
            Return _insnotes
        End Get
        Set(ByVal Value As String)
            _insnotes = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return _applicationid
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property
    Public Property CustomerId() As String
        Get
            Return _customerid
        End Get
        Set(ByVal Value As String)
            _customerid = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return _customername
        End Get
        Set(ByVal Value As String)
            _customername = Value
        End Set
    End Property
    Public Property InsAssetInsuredByName() As String
        Get
            Return _insassetinsuredbyname
        End Get
        Set(ByVal Value As String)
            _insassetinsuredbyname = Value
        End Set
    End Property
    Public Property InsAssetPaidByName() As String
        Get
            Return _insassetpaidbyname
        End Get
        Set(ByVal Value As String)
            _insassetpaidbyname = Value
        End Set
    End Property
    Public Property ApplicationTypeDescr() As String
        Get
            Return _applicationtypedescr
        End Get
        Set(ByVal Value As String)
            _applicationtypedescr = Value
        End Set
    End Property

    Public Property ManufacturingYear() As String
        Get
            Return _ManufacturingYear
        End Get
        Set(ByVal Value As String)
            _ManufacturingYear = Value
        End Set
    End Property

    Public Property JmlGrid() As Int16
        Get
            Return _JmlGrid
        End Get
        Set(ByVal Value As Int16)
            _JmlGrid = Value
        End Set
    End Property
    Public Property InsLength() As Integer
        Get
            Return _inslength
        End Get
        Set(ByVal Value As Integer)
            _inslength = Value
        End Set
    End Property

    Public Property YearInsurance() As Int16
        Get
            Return _YearInsurance
        End Get
        Set(ByVal Value As Int16)
            _YearInsurance = Value
        End Set
    End Property

    Public Property CoverageType() As String
        Get
            Return _CoverageType
        End Get
        Set(ByVal Value As String)
            _CoverageType = Value
        End Set
    End Property

    Public Property Premium() As Double
        Get
            Return _Premium
        End Get
        Set(ByVal Value As Double)
            _Premium = Value
        End Set
    End Property

    Public Property SRCC() As Boolean
        Get
            Return _SRCC
        End Get
        Set(ByVal Value As Boolean)
            _SRCC = Value
        End Set
    End Property

    Public Property SRCCAmount() As Double
        Get
            Return _SRCCAmount
        End Get
        Set(ByVal Value As Double)
            _SRCCAmount = Value
        End Set
    End Property

    Public Property TPL() As Double
        Get
            Return _TPL
        End Get
        Set(ByVal Value As Double)
            _TPL = Value
        End Set
    End Property


    Public Property TPLAmount() As Double
        Get
            Return _TPLAmount
        End Get
        Set(ByVal Value As Double)
            _TPLAmount = Value
        End Set
    End Property



    Public Property Flood() As Boolean
        Get
            Return _Flood
        End Get
        Set(ByVal Value As Boolean)
            _Flood = Value
        End Set
    End Property

    Public Property FloodAmount() As Double
        Get
            Return _FloodAmount
        End Get
        Set(ByVal Value As Double)
            _FloodAmount = Value
        End Set
    End Property

    Public Property Total() As Double
        Get
            Return _Total
        End Get
        Set(ByVal Value As Double)
            _Total = Value
        End Set
    End Property

    Public Property PaidByCust() As String
        Get
            Return _PaidByCust
        End Get
        Set(ByVal Value As String)
            _PaidByCust = Value
        End Set
    End Property

    Public Property InsuranceComBranchName() As String
        Get
            Return _InsuranceComBranchName
        End Get
        Set(ByVal Value As String)
            _InsuranceComBranchName = Value
        End Set
    End Property

    Public Property MaskAssID() As String
        Get
            Return _MaskAssID
        End Get
        Set(ByVal Value As String)
            _MaskAssID = Value
        End Set
    End Property

    Public Property InsuranceComBranchID() As String
        Get
            Return _InsuranceComBranchID
        End Get
        Set(ByVal Value As String)
            _InsuranceComBranchID = Value
        End Set
    End Property
    Public Property PBMaximumTenor() As Int16
        Get
            Return _PBMaximumTenor
        End Get
        Set(ByVal Value As Int16)
            _PBMaximumTenor = Value
        End Set
    End Property
    Public Property PBMinimumTenor() As Int16
        Get
            Return _PBMinimumTenor
        End Get
        Set(ByVal Value As Int16)
            _PBMinimumTenor = Value
        End Set
    End Property
    Public Property PMaximumTenor() As Int16
        Get
            Return _PMaximumTenor
        End Get
        Set(ByVal Value As Int16)
            _PMaximumTenor = Value
        End Set
    End Property

    Public Property PMinimumTenor() As Int16
        Get
            Return _PMinimumTenor
        End Get
        Set(ByVal Value As Int16)
            _PMinimumTenor = Value
        End Set
    End Property
    Public Property MaximumTenor() As Int16
        Get
            Return _MaximumTenor
        End Get
        Set(ByVal Value As Int16)
            _MaximumTenor = Value
        End Set
    End Property
    Public Property MinimumTenor() As Int16
        Get
            Return _MinimumTenor
        End Get
        Set(ByVal Value As Int16)
            _MinimumTenor = Value
        End Set
    End Property
    Public Property InsAdminFee() As Double
        Get
            Return _InsAdminFee
        End Get
        Set(ByVal Value As Double)
            _InsAdminFee = Value
        End Set
    End Property

    Public Property InsAdminFeeBehaviour() As String
        Get
            Return _InsAdminFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _InsAdminFeeBehaviour = Value
        End Set
    End Property

    Public Property InsStampDutyFee() As Double
        Get
            Return _InsStampDutyFee
        End Get
        Set(ByVal Value As Double)
            _InsStampDutyFee = Value
        End Set
    End Property

    Public Property InsStampDutyFeeBehaviour() As String
        Get
            Return _InsStampDutyFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _InsStampDutyFeeBehaviour = Value
        End Set
    End Property
    Public Property TotalOTR() As Double
        Get
            Return _TotalOTR
        End Get
        Set(ByVal Value As Double)
            _TotalOTR = Value
        End Set
    End Property


    Public Property AssetUsageID() As String
        Get
            Return _AssetUsageID
        End Get
        Set(ByVal Value As String)
            _AssetUsageID = Value
        End Set
    End Property


    Public Property AssetUsageDescr() As String
        Get
            Return _AssetUsageDescr
        End Get
        Set(ByVal Value As String)
            _AssetUsageDescr = Value
        End Set
    End Property
    Public Property Tenor() As Double
        Get
            Return _Tenor
        End Get
        Set(ByVal Value As Double)
            _Tenor = Value
        End Set
    End Property

    Public Property AssetMasterDescr() As String
        Get
            Return _AssetMasterDescr
        End Get
        Set(ByVal Value As String)
            _AssetMasterDescr = Value
        End Set
    End Property


    Public Property InsuranceAssetDescr() As String
        Get
            Return _InsuranceAssetDescr
        End Get
        Set(ByVal Value As String)
            _InsuranceAssetDescr = Value
        End Set
    End Property


    Public Property AssetUsageNewUsed() As String
        Get
            Return _AssetUsageNewUsed
        End Get
        Set(ByVal Value As String)
            _AssetUsageNewUsed = Value
        End Set
    End Property


    Public Property InsuranceType() As String
        Get
            Return _InsuranceType
        End Get
        Set(ByVal Value As String)
            _InsuranceType = Value
        End Set
    End Property

End Class
