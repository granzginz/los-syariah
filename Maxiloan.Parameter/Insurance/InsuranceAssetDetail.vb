
<Serializable()> _
Public Class InsuranceAssetDetail
    Inherits InsCoAllocationDetailList

    Private _AssetCode As String
    Private _AssetMasterDescription As String
    Private _PageSource As String
    Private _InsActivateDate As Date
    Private _PremiumBaseForRefundSupp As Double
    Private _BolSRCC As Boolean
    Private _BolFlood As Boolean
    Private _AccountPayableNo As String
    Private _CoverPeriod As String
    Private _TotalRate As Double
    Private _TotalPremiCP As Double
    Private _TotalPremiJK As Double

    Public Property BolFlood() As Boolean
        Get
            Return _BolFlood
        End Get
        Set(ByVal Value As Boolean)
            _BolFlood = Value
        End Set
    End Property


    Public Property BolSRCC() As Boolean
        Get
            Return _BolSRCC
        End Get
        Set(ByVal Value As Boolean)
            _BolSRCC = Value
        End Set
    End Property


    Public Property PremiumBaseForRefundSupp() As Double
        Get
            Return _PremiumBaseForRefundSupp
        End Get
        Set(ByVal Value As Double)
            _PremiumBaseForRefundSupp = Value
        End Set
    End Property




    Public Property InsActivateDate() As Date
        Get
            Return _InsActivateDate
        End Get
        Set(ByVal Value As Date)
            _InsActivateDate = Value
        End Set
    End Property

    Public Property AssetCode() As String
        Get
            Return _AssetCode
        End Get
        Set(ByVal Value As String)
            _AssetCode = Value
        End Set
    End Property


    Public Property AssetMasterDescription() As String
        Get
            Return _AssetMasterDescription
        End Get
        Set(ByVal Value As String)
            _AssetMasterDescription = Value
        End Set
    End Property

    Public Property PageSource() As String
        Get
            Return _PageSource
        End Get
        Set(ByVal Value As String)
            _PageSource = Value
        End Set
    End Property

    Public Property AccountPayableNo() As String
        Get
            Return _AccountPayableNo
        End Get
        Set(ByVal Value As String)
            _AccountPayableNo = Value
        End Set
    End Property

    Public Property Coverperiod() As String
        Get
            Return _CoverPeriod
        End Get
        Set(ByVal Value As String)
            _CoverPeriod = Value
        End Set
    End Property

    Public Property TotalRate() As Double
        Get
            Return _TotalRate
        End Get
        Set(ByVal Value As Double)
            _TotalRate = Value
        End Set
    End Property

    Public Property TotalPremiCP() As Double
        Get
            Return _TotalPremiCP
        End Get
        Set(ByVal Value As Double)
            _TotalPremiCP = Value
        End Set
    End Property

    Public Property TotalPremiJK() As Double
        Get
            Return _TotalPremiJK
        End Get
        Set(ByVal Value As Double)
            _TotalPremiJK = Value
        End Set
    End Property
End Class
