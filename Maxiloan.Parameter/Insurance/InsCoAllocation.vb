
<Serializable()> _
Public Class InsCoAllocation : Inherits Common

    Private _listdata As DataTable
    Private _listdataset As DataSet
    Private _totalrecords As Int64
    Private _spName As String
    Public Property spName() As String
        Get
            Return _spName
        End Get
        Set(ByVal Value As String)
            _spName = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property ListDataSet() As DataSet
        Get
            Return _listdataset
        End Get
        Set(ByVal Value As DataSet)
            _listdataset = Value
        End Set
    End Property

    Public Property ErrStr As String
    Public Property ListData2 As DataTable

End Class
