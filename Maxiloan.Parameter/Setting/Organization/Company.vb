
<Serializable()> _
Public Class Company : Inherits Common
    Private _CompanyID As String
    Private _CompanyFullName As String
    Private _CompanyShortName As String
    Private _CompanyInitialName As String
    Private _listData As DataTable
    Private _TotRec As Integer

    Private _NPWP As String
    Private _TDP As String
    Private _SIUP As String

    Public Property CompanyID() As String
        Get
            Return _CompanyID
        End Get
        Set(ByVal Value As String)
            _CompanyID = Value
        End Set
    End Property

    Public Property CompanyFullName() As String
        Get
            Return _CompanyFullName
        End Get
        Set(ByVal Value As String)
            _CompanyFullName = Value
        End Set
    End Property

    Public Property CompanyShortName() As String
        Get
            Return _CompanyShortName
        End Get
        Set(ByVal Value As String)
            _CompanyShortName = Value
        End Set
    End Property


    Public Property CompanyInitialName() As String
        Get
            Return _CompanyInitialName
        End Get
        Set(ByVal Value As String)
            _CompanyInitialName = Value
        End Set
    End Property


    Public Property NPWP() As String
        Get
            Return _NPWP
        End Get
        Set(ByVal Value As String)
            _NPWP = Value
        End Set
    End Property


    Public Property TDP() As String
        Get
            Return _TDP
        End Get
        Set(ByVal Value As String)
            _TDP = Value
        End Set
    End Property

    Public Property SIUP() As String
        Get
            Return _SIUP
        End Get
        Set(ByVal Value As String)
            _SIUP = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listData
        End Get
        Set(ByVal Value As DataTable)
            _listData = Value
        End Set
    End Property

    Public Property TotRec() As Integer
        Get
            Return _TotRec
        End Get
        Set(ByVal Value As Integer)
            _TotRec = Value
        End Set
    End Property

End Class
