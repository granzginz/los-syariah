﻿<Serializable()> _
Public Class AuthorizedSigner : Inherits Common
    'Public Property BranchID As String
    Public Property DocumentID As String
    Public Property Signer1 As String
    Public Property Signer2 As String
    Public Property Signer3 As String
    Public Property JabatanSigner1 As String
    Public Property JabatanSigner2 As String
    Public Property JabatanSigner3 As String

    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
End Class
