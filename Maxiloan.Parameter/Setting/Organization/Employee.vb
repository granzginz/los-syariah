

Public Class Employee : Inherits Common
    Private _EmployeeID As String
    Private _EmployeeName As String
    Private _Position As String
    Private _AOLevel As String
    Private _AOSupervisor As String
    Private _AOSales As String
    Private _AOFirstDate As String
    Private _EmployeeIncentiveCard As String
    Private _CASupervisor As String
    Private _OverduePenaltyCard As String
    Private _listdata As DataTable
    Private _isActive As Boolean
    Public Property Inisial As String
    Private _DestBranchId As String

#Region "Constanta Bank Account"
    Private _BankID As String
    Private _BankBranch As String
    Private _AccountNo As String
    Private _AccountName As String
    Private _BankBranchId As String
#End Region

#Region "Bank Account"

    Public Property BankID() As String
        Get
            Return _BankID
        End Get
        Set(ByVal Value As String)
            _BankID = Value
        End Set
    End Property
    Public Property BankBranch() As String
        Get
            Return _BankBranch
        End Get
        Set(ByVal Value As String)
            _BankBranch = Value
        End Set
    End Property
    Public Property AccountNo() As String
        Get
            Return _AccountNo
        End Get
        Set(ByVal Value As String)
            _AccountNo = Value
        End Set
    End Property
    Public Property AccountName() As String
        Get
            Return _AccountName
        End Get
        Set(ByVal Value As String)
            _AccountName = Value
        End Set
    End Property
    Public Property BankBranchId() As String
        Get
            Return _BankBranchId
        End Get
        Set(ByVal Value As String)
            _BankBranchId = Value
        End Set
    End Property

#End Region
    Public Property EmployeeID() As String
        Get
            Return _EmployeeID
        End Get
        Set(ByVal Value As String)
            _EmployeeID = Value
        End Set
    End Property

    Public Property EmployeeName() As String
        Get
            Return _EmployeeName
        End Get
        Set(ByVal Value As String)
            _EmployeeName = Value
        End Set
    End Property

    Public Property Position() As String
        Get
            Return _Position
        End Get
        Set(ByVal Value As String)
            _Position = Value
        End Set
    End Property

    Public Property AOLevel() As String
        Get
            Return _AOLevel
        End Get
        Set(ByVal Value As String)
            _AOLevel = Value
        End Set
    End Property

    Public Property AOSupervisor() As String
        Get
            Return _AOSupervisor
        End Get
        Set(ByVal Value As String)
            _AOSupervisor = Value
        End Set
    End Property

    Public Property AOSales() As String
        Get
            Return _AOSales
        End Get
        Set(ByVal Value As String)
            _AOSales = Value
        End Set
    End Property

    Public Property AOFirstDate() As String
        Get
            Return _AOFirstDate
        End Get
        Set(ByVal Value As String)
            _AOFirstDate = Value
        End Set
    End Property

    Public Property EmployeeIncentiveCard() As String
        Get
            Return _EmployeeIncentiveCard
        End Get
        Set(ByVal Value As String)
            _EmployeeIncentiveCard = Value
        End Set
    End Property

    Public Property CASupervisor() As String
        Get
            Return _CASupervisor
        End Get
        Set(ByVal Value As String)
            _CASupervisor = Value
        End Set
    End Property

    Public Property OverduePenaltyCard() As String
        Get
            Return _OverduePenaltyCard
        End Get
        Set(ByVal Value As String)
            _OverduePenaltyCard = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property isActive() As Boolean
        Get
            Return _isActive
        End Get
        Set(ByVal Value As Boolean)
            _isActive = Value
        End Set
    End Property

    Public Property DestBranchId() As String
        Get
            Return _DestBranchId
        End Get
        Set(ByVal Value As String)
            _DestBranchId = Value
        End Set
    End Property
End Class
