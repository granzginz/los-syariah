
<Serializable()> _
Public Class CompanyCommisioner : Inherits Common

    Private _CompanyID As String
    Private _CommisionerID As String
    Private _CommisionerIDType As String
    Private _CommisionerIDNumber As String
    Private _CommisionerTitle As String
    Private _CommisionerName As String

    Private _listData As DataTable
    Private _TotRec As Integer

    Private _CommisionerNPWP As String

    Public Property CompanyID() As String
        Get
            Return _CompanyID
        End Get
        Set(ByVal Value As String)
            _CompanyID = Value
        End Set
    End Property

    Public Property CommisionerID() As String
        Get
            Return _CommisionerID
        End Get
        Set(ByVal Value As String)
            _CommisionerID = Value
        End Set
    End Property

    Public Property CommisionerName() As String
        Get
            Return _CommisionerName
        End Get
        Set(ByVal Value As String)
            _CommisionerName = Value
        End Set
    End Property

    Public Property CommisionerIDType() As String
        Get
            Return _CommisionerIDType
        End Get
        Set(ByVal Value As String)
            _CommisionerIDType = Value
        End Set
    End Property

    Public Property CommisionerIDNumber() As String
        Get
            Return _CommisionerIDNumber
        End Get
        Set(ByVal Value As String)
            _CommisionerIDNumber = Value
        End Set
    End Property

    Public Property CommisionerNPWP() As String
        Get
            Return _CommisionerNPWP
        End Get
        Set(ByVal Value As String)
            _CommisionerNPWP = Value
        End Set
    End Property


    Public Property CommisionerTitle() As String
        Get
            Return _CommisionerTitle
        End Get
        Set(ByVal Value As String)
            _CommisionerTitle = Value
        End Set
    End Property


    Public Property ListData() As DataTable
        Get
            Return _listData
        End Get
        Set(ByVal Value As DataTable)
            _listData = Value
        End Set
    End Property

    Public Property TotRec() As Integer
        Get
            Return _TotRec
        End Get
        Set(ByVal Value As Integer)
            _TotRec = Value
        End Set
    End Property

End Class
