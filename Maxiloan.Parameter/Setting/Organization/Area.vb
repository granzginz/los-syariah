
<Serializable()> _
Public Class Area : Inherits Common
    Private _areaid As String
    Private _areafullname As String
    Private _areainitialname As String
    Private _areamanager As String
    Private _areaaddress As String
    Private _areart As String
    Private _arearw As String
    Private _areakelurahan As String
    Private _areakecamatan As String
    Private _areacity As String
    Private _areazipcode As String
    Private _areaareaphone1 As String
    Private _areaphone1 As String
    Private _areaareaphone2 As String
    Private _areaphone2 As String
    Private _areaareafax As String
    Private _areafax As String
    Private _contactpersonname As String
    Private _contactpersonjobtitle As String
    Private _contactpersonemail As String
    Private _contactpersonhp As String
    Private _usrupd As String
    Private _dtmupd As Date

    Private _listreportarea As DataSet
    Private _listarea As DataTable
    Private _ApprovalLimit As Decimal

    Public RegionalID As String
    Public RegionalIDNew As String

    Public IDBranchStatus As String
    Public Description As String
    Public AddEdit As String
    Public Output As String

    Public Property ApprovalLimit() As Decimal
        Get
            Return (CType(_ApprovalLimit, Decimal))
        End Get
        Set(ByVal Value As Decimal)
            _ApprovalLimit = Value
        End Set
    End Property

    Public Property ListReportArea() As DataSet
        Get
            Return (CType(_listreportarea, DataSet))
        End Get
        Set(ByVal Value As DataSet)
            _listreportarea = Value
        End Set
    End Property

    Public Property ListArea() As DataTable
        Get
            Return (CType(_listarea, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listarea = Value
        End Set
    End Property

    Public Property AreaID() As String
        Get
            Return (CType(_areaid, String))
        End Get
        Set(ByVal Value As String)
            _areaid = Value
        End Set
    End Property

    Public Property AreaFullName() As String
        Get
            Return (CType(_areafullname, String))
        End Get
        Set(ByVal Value As String)
            _areafullname = Value
        End Set
    End Property

    Public Property AreaInitialName() As String
        Get
            Return (CType(_areainitialname, String))
        End Get
        Set(ByVal Value As String)
            _areainitialname = Value
        End Set
    End Property

    Public Property AreaManager() As String
        Get
            Return (CType(_areamanager, String))
        End Get
        Set(ByVal Value As String)
            _areamanager = Value
        End Set
    End Property

    Public Property AreaAddress() As String
        Get
            Return (CType(_areaaddress, String))
        End Get
        Set(ByVal Value As String)
            _areaaddress = Value
        End Set
    End Property

    Public Property AreaRT() As String
        Get
            Return (CType(_areart, String))
        End Get
        Set(ByVal Value As String)
            _areart = Value
        End Set
    End Property

    Public Property AreaRW() As String
        Get
            Return (CType(_arearw, String))
        End Get
        Set(ByVal Value As String)
            _arearw = Value
        End Set
    End Property

    Public Property AreaKelurahan() As String
        Get
            Return (CType(_areakelurahan, String))
        End Get
        Set(ByVal Value As String)
            _areakelurahan = Value
        End Set
    End Property

    Public Property AreaKecamatan() As String
        Get
            Return (CType(_areakecamatan, String))
        End Get
        Set(ByVal Value As String)
            _areakecamatan = Value
        End Set
    End Property

    Public Property AreaCity() As String
        Get
            Return (CType(_areacity, String))
        End Get
        Set(ByVal Value As String)
            _areacity = Value
        End Set
    End Property

    Public Property AreaZipCode() As String
        Get
            Return (CType(_areazipcode, String))
        End Get
        Set(ByVal Value As String)
            _areazipcode = Value
        End Set
    End Property

    Public Property AreaAreaPhone1() As String
        Get
            Return (CType(_areaareaphone1, String))
        End Get
        Set(ByVal Value As String)
            _areaareaphone1 = Value
        End Set
    End Property

    Public Property AreaPhone1() As String
        Get
            Return (CType(_areaphone1, String))
        End Get
        Set(ByVal Value As String)
            _areaphone1 = Value
        End Set
    End Property

    Public Property AreaAreaPhone2() As String
        Get
            Return (CType(_areaareaphone2, String))
        End Get
        Set(ByVal Value As String)
            _areaareaphone2 = Value
        End Set
    End Property

    Public Property AreaPhone2() As String
        Get
            Return (CType(_areaphone2, String))
        End Get
        Set(ByVal Value As String)
            _areaphone2 = Value
        End Set
    End Property

    Public Property AreaAreaFax() As String
        Get
            Return (CType(_areaareafax, String))
        End Get
        Set(ByVal Value As String)
            _areaareafax = Value
        End Set
    End Property

    Public Property AreaFax() As String
        Get
            Return (CType(_areafax, String))
        End Get
        Set(ByVal Value As String)
            _areafax = Value
        End Set
    End Property

    Public Property ContactPersonName() As String
        Get
            Return (CType(_contactpersonname, String))
        End Get
        Set(ByVal Value As String)
            _contactpersonname = Value
        End Set
    End Property

    Public Property ContactPersonJobTitle() As String
        Get
            Return (CType(_contactpersonjobtitle, String))
        End Get
        Set(ByVal Value As String)
            _contactpersonjobtitle = Value
        End Set
    End Property

    Public Property ContactPersonEmail() As String
        Get
            Return (CType(_contactpersonemail, String))
        End Get
        Set(ByVal Value As String)
            _contactpersonemail = Value
        End Set
    End Property

    Public Property ContactPersonHP() As String
        Get
            Return (CType(_contactpersonhp, String))
        End Get
        Set(ByVal Value As String)
            _contactpersonhp = Value
        End Set
    End Property

    Public Property UsrUpd() As String
        Get
            Return (CType(_usrupd, String))
        End Get
        Set(ByVal Value As String)
            _usrupd = Value
        End Set
    End Property

    Public Property DtmUpd() As Date
        Get
            Return (CType(_dtmupd, Date))
        End Get
        Set(ByVal Value As Date)
            _dtmupd = Value
        End Set
    End Property
End Class
