#Region "Revision History"
'$Header: 
'$Workfile:
'-----------------------------------------------------------
'$Log: 
'
'  
'  
'  
'  
'  
'-----------------------------------------------------------
#End Region
<Serializable()> _
Public Class Branch : Inherits Common

    Private _branchinitialname As String
    Private _branchfullname As String

    Private _companyid As String
    Private _companyname As String
    Private _areaid As String
    Private _areaname As String
    Private _year As Integer
    Private _month As Integer
    Private _assetstatus As String
    Private _branchmanager As String
    Private _STNKPICName As String
    Private _adh As String
    Private _arcontrolname As String
    Private _branchstatus As String
    Private _productivityvalue As Double
    Private _productivitycollection As Double
    Private _employeenumber As Integer
    Private _aocard As String
    Private _iscashierclosed As Char
    Private _isheadoffice As Char

    Private _listdata As DataTable
    Private _totrec As Integer
    Private _custodian As String

    Public Property Custodian() As String
        Get
            Return _custodian
        End Get
        Set(ByVal Value As String)
            _custodian = Value
        End Set
    End Property
    Public Property STNKPICName() As String
        Get
            Return _STNKPICName
        End Get
        Set(ByVal Value As String)
            _STNKPICName = Value
        End Set
    End Property
    Public Property BranchFullName() As String
        Get
            Return _branchfullname
        End Get
        Set(ByVal Value As String)
            _branchfullname = Value
        End Set
    End Property

    Public Property BranchInitialName() As String
        Get
            Return _branchinitialname
        End Get
        Set(ByVal Value As String)
            _branchinitialname = Value
        End Set
    End Property

    Public Property CompanyID() As String
        Get
            Return _companyid
        End Get
        Set(ByVal Value As String)
            _companyid = Value
        End Set
    End Property

    Public Property CompanyName() As String
        Get
            Return _companyname
        End Get
        Set(ByVal Value As String)
            _companyname = Value
        End Set
    End Property
    Public Property AssetStatus() As String
        Get
            Return _assetstatus
        End Get
        Set(ByVal Value As String)
            _assetstatus = Value
        End Set
    End Property

    Public Property AreaID() As String
        Get
            Return _areaid
        End Get
        Set(ByVal Value As String)
            _areaid = Value
        End Set
    End Property

    Public Property AreaName() As String
        Get
            Return _areaname
        End Get
        Set(ByVal Value As String)
            _areaname = Value
        End Set
    End Property

    Public Property BranchManager() As String
        Get
            Return _branchmanager
        End Get
        Set(ByVal Value As String)
            _branchmanager = Value
        End Set
    End Property

    Public Property ADH() As String
        Get
            Return _adh
        End Get
        Set(ByVal Value As String)
            _adh = Value
        End Set
    End Property

    Public Property ARControlName() As String
        Get
            Return _arcontrolname
        End Get
        Set(ByVal Value As String)
            _arcontrolname = Value
        End Set
    End Property

    Public Property BranchStatus() As String
        Get
            Return _branchstatus
        End Get
        Set(ByVal Value As String)
            _branchstatus = Value
        End Set
    End Property

    Public Property ProductivityValue() As Double
        Get
            Return _productivityvalue
        End Get
        Set(ByVal Value As Double)
            _productivityvalue = Value
        End Set
    End Property

    Public Property ProductivityCollection() As Double
        Get
            Return _productivitycollection
        End Get
        Set(ByVal Value As Double)
            _productivitycollection = Value
        End Set
    End Property

    Public Property EmployeeNumber() As Integer
        Get
            Return _employeenumber
        End Get
        Set(ByVal Value As Integer)
            _employeenumber = Value
        End Set
    End Property
    Public Property Year() As Integer
        Get
            Return _year
        End Get
        Set(ByVal Value As Integer)
            _year = Value
        End Set
    End Property
    Public Property Month() As Integer
        Get
            Return _month
        End Get
        Set(ByVal Value As Integer)
            _month = Value
        End Set
    End Property


    Public Property AOCard() As String
        Get
            Return _aocard
        End Get
        Set(ByVal Value As String)
            _aocard = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property isCashierClosed() As Char
        Get
            Return _iscashierclosed
        End Get
        Set(ByVal Value As Char)
            _iscashierclosed = Value
        End Set
    End Property


    Public Property isHeadOffice() As Char
        Get
            Return _isheadoffice
        End Get
        Set(ByVal Value As Char)
            _isheadoffice = Value
        End Set
    End Property

    Public Property TotRec() As Integer
        Get
            Return _totrec
        End Get
        Set(ByVal Value As Integer)
            _totrec = Value
        End Set
    End Property



End Class
