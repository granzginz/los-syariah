
<Serializable()> _
Public Class CompanyBOD : Inherits Common

    Private _CompanyID As String
    Private _BODID As String
    Private _BODIDType As String
    Private _BODIDNumber As String
    Private _BODTitle As String
    Private _BODName As String

    Private _listData As DataTable
    Private _TotRec As Integer

    Private _BODNPWP As String

    Public Property CompanyID() As String
        Get
            Return _CompanyID
        End Get
        Set(ByVal Value As String)
            _CompanyID = Value
        End Set
    End Property

    Public Property BODID() As String
        Get
            Return _BODID
        End Get
        Set(ByVal Value As String)
            _BODID = Value
        End Set
    End Property

    Public Property BODName() As String
        Get
            Return _BODName
        End Get
        Set(ByVal Value As String)
            _BODName = Value
        End Set
    End Property

    Public Property BODIDType() As String
        Get
            Return _BODIDType
        End Get
        Set(ByVal Value As String)
            _BODIDType = Value
        End Set
    End Property

    Public Property BODIDNumber() As String
        Get
            Return _BODIDNumber
        End Get
        Set(ByVal Value As String)
            _BODIDNumber = Value
        End Set
    End Property

    Public Property BODNPWP() As String
        Get
            Return _BODNPWP
        End Get
        Set(ByVal Value As String)
            _BODNPWP = Value
        End Set
    End Property


    Public Property BODTitle() As String
        Get
            Return _BODTitle
        End Get
        Set(ByVal Value As String)
            _BODTitle = Value
        End Set
    End Property


    Public Property ListData() As DataTable
        Get
            Return _listData
        End Get
        Set(ByVal Value As DataTable)
            _listData = Value
        End Set
    End Property

    Public Property TotRec() As Integer
        Get
            Return _TotRec
        End Get
        Set(ByVal Value As Integer)
            _TotRec = Value
        End Set
    End Property

End Class
