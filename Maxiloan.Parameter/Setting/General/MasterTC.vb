

<Serializable()> _
Public Class MasterTC : Inherits Common
    Private _TCID As String
    Private _TCName As String
    Private _IsForPersonal As Integer
    Private _IsForCompany As Integer
    Private _SequenceNo As Int16
    Private _Description As String
    Private _ListData As DataTable

    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property SequenceNo() As Int16
        Get
            Return _SequenceNo
        End Get
        Set(ByVal Value As Int16)
            _SequenceNo = Value
        End Set
    End Property

    Public Property TCID() As String
        Get
            Return _TCID
        End Get
        Set(ByVal Value As String)
            _TCID = Value
        End Set
    End Property

    Public Property TCName() As String
        Get
            Return _TCName
        End Get
        Set(ByVal Value As String)
            _TCName = Value
        End Set
    End Property

    Public Property IsForPersonal() As Integer
        Get
            Return _IsForPersonal
        End Get
        Set(ByVal Value As Integer)
            _IsForPersonal = Value
        End Set
    End Property

    Public Property IsForCompany() As Integer
        Get
            Return _IsForCompany
        End Get
        Set(ByVal Value As Integer)
            _IsForCompany = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal Value As DataTable)
            _ListData = Value
        End Set
    End Property




End Class
