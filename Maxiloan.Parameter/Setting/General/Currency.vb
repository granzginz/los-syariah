﻿<Serializable()> _
Public Class Currency : Inherits Common
    Public Property DTOList As List(Of Detail)
    Public Class Detail
        Public Property CurrencyId As String
        Public Property Description As String
        Public Property CurrencyName As String
        Public Property Rounded As Decimal
        Public Property CurrencyDate As DateTime
        Public Property InstallmentRounded As Decimal
        Public Property InsuranceRounded As Decimal
        Public Property DailyExchangeRate As Decimal
    End Class
End Class
