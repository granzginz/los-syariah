﻿<Serializable()>
Public Class MailType : Inherits Common
    Private _Description As String
    Private _MailInitial As String
    Private _DocTemplateName As String
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _MailTypeID As String
    Private _PICName As String
    Private _PICJobTitle As String
    Private _SignedName As String
    Private _SignedJobTitle As String
    Private _Phone As String
    Private _MailDocFlag As String
    Private _IsRecordChanged As String

    Public Property MailInitial() As String
        Get
            Return _MailInitial
        End Get
        Set(ByVal Value As String)
            _MailInitial = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property DocTemplateName() As String
        Get
            Return _DocTemplateName
        End Get
        Set(ByVal Value As String)
            _DocTemplateName = Value
        End Set
    End Property

    Public Property MailTypeID() As String
        Get
            Return _MailTypeID
        End Get
        Set(ByVal Value As String)
            _MailTypeID = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property PICName() As String
        Get
            Return _PICName
        End Get
        Set(ByVal Value As String)
            _PICName = Value
        End Set
    End Property

    Public Property PICJobTitle() As String
        Get
            Return _PICJobTitle
        End Get
        Set(ByVal Value As String)
            _PICJobTitle = Value
        End Set
    End Property

    Public Property SignedName() As String
        Get
            Return _SignedName
        End Get
        Set(ByVal Value As String)
            _SignedName = Value
        End Set
    End Property

    Public Property SignedJobTitle() As String
        Get
            Return _SignedJobTitle
        End Get
        Set(ByVal Value As String)
            _SignedJobTitle = Value
        End Set
    End Property

    Public Property Phone() As String
        Get
            Return _Phone
        End Get
        Set(ByVal Value As String)
            _Phone = Value
        End Set
    End Property

    Public Property MailDocFlag() As String
        Get
            Return _MailDocFlag
        End Get
        Set(ByVal Value As String)
            _MailDocFlag = Value
        End Set
    End Property

    Public Property IsRecordChanged() As String
        Get
            Return _IsRecordChanged
        End Get
        Set(ByVal Value As String)
            _IsRecordChanged = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

End Class


'MailTypeID -> sudah
'MailInitial ->
'Description -> sudah
'DocTemplateName ->
'PICName -> sudah
'PICJobTitle ->
'SignedName ->
'SignedJobTitle ->
'Phone ->
'MailDocFlag ->
'IsRecordChanged