

<Serializable()> _
Public Class MasterZipCode : Inherits Common
    Private _kelurahan As String
    Private _kecamatan As String
    Private _city As String
    Private _zipcode As String
    Private _listdata As DataTable
    Private _totalrecords As Int64
    Private _ListDataReport As DataSet
    Private _CGID As String
    Private _CollectorID As String


    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property


    Public Property CollectorID() As String
        Get
            Return _CollectorID
        End Get
        Set(ByVal Value As String)
            _CollectorID = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property ListDataReport() As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(ByVal Value As DataSet)
            _ListDataReport = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property Kelurahan() As String
        Get
            Return _kelurahan
        End Get
        Set(ByVal Value As String)
            _kelurahan = Value
        End Set
    End Property

    Public Property Kecamatan() As String
        Get
            Return _kecamatan
        End Get
        Set(ByVal Value As String)
            _kecamatan = Value
        End Set
    End Property

    Public Property City() As String
        Get
            Return _city
        End Get
        Set(ByVal Value As String)
            _city = Value
        End Set
    End Property
    Public Property ZipCode() As String
        Get
            Return _zipcode
        End Get
        Set(ByVal Value As String)
            _zipcode = Value
        End Set
    End Property


    Public Property SandiWilayah As String
    Public Property Provinsi As String
End Class
