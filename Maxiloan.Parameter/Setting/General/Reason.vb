
<Serializable()> _
Public Class Reason : Inherits Maxiloan.Parameter.Common
    Private _reasonid As String
    Private _reasontypeid As String
    Private _reasoniddescription As String
    Private _issystem As Boolean
    Public Property ReasonDescription() As String
        Get
            Return _reasoniddescription
        End Get
        Set(ByVal Value As String)
            _reasoniddescription = Value
        End Set
    End Property

    Public Property IsSystem() As Boolean
        Get
            Return _issystem
        End Get
        Set(ByVal Value As Boolean)
            _issystem = Value
        End Set
    End Property
End Class
