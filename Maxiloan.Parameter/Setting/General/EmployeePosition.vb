

<Serializable()> _
Public Class EmployeePosition : Inherits Common

    Private _EmployeePositionID As String
    Private _EmployeeDesription As String
    Private _IsSystem As String
    Private _listdata As DataTable

    Public Property listdata() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property EmployeePositionID() As String
        Get
            Return _EmployeePositionID
        End Get
        Set(ByVal Value As String)
            _EmployeePositionID = Value
        End Set
    End Property

    Public Property EmployeeDesription() As String
        Get
            Return _EmployeeDesription
        End Get
        Set(ByVal Value As String)
            _EmployeeDesription = Value
        End Set
    End Property

    Public Property IsSystem() As String
        Get
            Return _IsSystem
        End Get
        Set(ByVal Value As String)
            _IsSystem = Value
        End Set
    End Property






End Class
