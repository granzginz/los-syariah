﻿<Serializable()> _
Public Class Vendor : Inherits Common

    Public Property VendorID As String
    Public Property NamaVendor As String
    Public Property BidangUsaha As String
    Public Property NPWP As String
    Public Property TDP As String
    Public Property SIUP As String
    ' Public Property TglMulai As Date
    Public Property TglMulai As String
    Public Property Alamat As String
    Public Property RT As String
    Public Property RW As String
    Public Property Kelurahan As String
    Public Property Kecamatan As String
    Public Property Kota As String
    Public Property KodePos As String
    Public Property AreaPhone1 As String
    Public Property Phone1 As String
    Public Property AreaPhone2 As String
    Public Property Phone2 As String
    Public Property AreaFax As String
    Public Property Fax As String
    Public Property Kontak As String
    Public Property Jabatan As String
    Public Property Email As String
    Public Property NoHP As String
    Public Property BankID As String
    Public Property BankBranchID As Integer
    Public Property AccounNo As String
    Public Property AccountName As String

    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64

End Class
