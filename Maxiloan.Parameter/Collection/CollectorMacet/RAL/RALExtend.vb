

'<Serializable()> _

<Serializable()> _
Public Class RALExtend : Inherits Maxiloan.Parameter.RALPrinting
    Private _enddate As String
    Private _notes As String
    Private _onhand As Integer

    Public Property EndDate() As String
        Get
            Return _enddate
        End Get
        Set(ByVal Value As String)
            _enddate = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property

    Public Property OnHand() As Integer
        Get
            Return _onhand
        End Get
        Set(ByVal Value As Integer)
            _onhand = Value
        End Set
    End Property
End Class