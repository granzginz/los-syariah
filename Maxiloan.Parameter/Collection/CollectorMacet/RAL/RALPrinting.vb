
<Serializable()> _
Public Class RALPrinting : Inherits Maxiloan.Parameter.Common
    Private _strKey As String
    Private _CGID As String
    Private _listRAL As DataTable
    Private _collectortype As String
    Private _AgreementNo As String
    Private _RALNo As String
    Private _executorid As String
    Private _hasil As Integer
    Private _listReport As DataSet
    Private _nextBD As Date
    Private _MasaSKE As Integer
    Private _applicationid As String
    Private _IdPemberiKuasa As String

    Public Property ApplicationID() As String
        Get
            Return _applicationid
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property

    Public Property NextBD() As Date
        Get
            Return _nextBD
        End Get
        Set(ByVal Value As Date)
            _nextBD = Value
        End Set
    End Property

    Public Property ListReport() As DataSet
        Get
            Return _listReport
        End Get
        Set(ByVal Value As DataSet)
            _listReport = Value
        End Set
    End Property

    Public Property MASASKE() As Integer
        Get
            Return _MasaSKE
        End Get
        Set(ByVal Value As Integer)
            _MasaSKE = Value
        End Set
    End Property

    Public Property hasil() As Integer
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Integer)
            _hasil = Value
        End Set
    End Property
    Public Property ExecutorID() As String
        Get
            Return _executorid
        End Get
        Set(ByVal Value As String)
            _executorid = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property

    Public Property RALNo() As String
        Get
            Return _RALNo
        End Get
        Set(ByVal Value As String)
            _RALNo = Value
        End Set
    End Property

    Public Property strKey() As String
        Get
            Return _strKey
        End Get
        Set(ByVal Value As String)
            _strKey = Value
        End Set
    End Property

    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property

    Public Property ListRAL() As DataTable
        Get
            Return _listRAL
        End Get
        Set(ByVal Value As DataTable)
            _listRAL = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return _collectortype
        End Get
        Set(ByVal Value As String)
            _collectortype = Value
        End Set
    End Property

    Public Property Cases As String
    Public Property RalNotes As String

    Public Property IdPemberiKuasa() As String
        Get
            Return _IdPemberiKuasa
        End Get
        Set(ByVal Value As String)
            _IdPemberiKuasa = Value
        End Set
    End Property

End Class
