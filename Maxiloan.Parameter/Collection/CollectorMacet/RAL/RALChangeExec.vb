

'<Serializable()> _

<Serializable()> _
Public Class RALChangeExec : Inherits Maxiloan.Parameter.RALPrinting
    Private _ralno As String
    Private _ralnold As String
    Private _enddate As String
    Private _notes As String
    Private _onhand As Integer
    Private _hasil As Integer
    Private _businessdate1 As Date
    Private _collectorid As String
    Private _collectoridold As String
    Private _bapkno As String
    Private _executorid As String
    Private _branchid1 As String
    Private _applicationid As String
    Private _listreport As DataSet
    Private _nextbd As Date

    Private _answer As String
    Private _description As String
    Public Property BranchID1() As String
        Get
            Return _branchid1
        End Get
        Set(ByVal Value As String)
            _branchid1 = Value
        End Set
    End Property
    Public Property BAPKNo() As String
        Get
            Return _bapkno
        End Get
        Set(ByVal Value As String)
            _bapkno = Value
        End Set
    End Property
    Public Property CollectorIDOld() As String
        Get
            Return _collectoridold
        End Get
        Set(ByVal Value As String)
            _collectoridold = Value
        End Set
    End Property
    Public Property CollectorID() As String
        Get
            Return _collectorid
        End Get
        Set(ByVal Value As String)
            _collectorid = Value
        End Set
    End Property

    Public Property EndDate() As String
        Get
            Return _enddate
        End Get
        Set(ByVal Value As String)
            _enddate = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property

    Public Property OnHand() As Integer
        Get
            Return _onhand
        End Get
        Set(ByVal Value As Integer)
            _onhand = Value
        End Set
    End Property

    Public Property RALNOld() As String
        Get
            Return _ralnold
        End Get
        Set(ByVal Value As String)
            _ralnold = Value
        End Set
    End Property

    Public Property BusinessDate1() As Date
        Get
            Return _businessdate1
        End Get
        Set(ByVal Value As Date)
            _businessdate1 = Value
        End Set
    End Property
End Class