

<Serializable()> _
Public Class RALRelease : Inherits Maxiloan.Parameter.RALPrinting
    Private _CGID As String
    Private _listRAL As DataTable
    Private _strkey As String
    Private _collectortype As String
    Private _AgreementNo As String
    Private _RalNo As String
    Private _endDate As String
    Private _Notes As String
    Private _onHand As Integer
    Private _hasil As Integer
    Private _reasonid As String

    Public Property strReasonID() As String
        Get
            Return _reasonid
        End Get
        Set(ByVal Value As String)
            _reasonid = Value
        End Set
    End Property

    Public Property EndDate() As String
        Get
            Return _endDate
        End Get
        Set(ByVal Value As String)
            _endDate = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property

    Public Property OnHand() As Integer
        Get
            Return _onHand
        End Get
        Set(ByVal Value As Integer)
            _onHand = Value
        End Set
    End Property

End Class
