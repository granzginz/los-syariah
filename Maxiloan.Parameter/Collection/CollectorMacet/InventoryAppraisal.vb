


<Serializable()> _
Public Class InventoryAppraisal : Inherits Maxiloan.Parameter.Common

    Private _strKey As String
    Private _CGID As String
    Private _listBidder As DataTable
    Private _listRAL As DataTable
    Private _listappraisal As DataTable
    Private _collectortype As String
    Private _AssetSeqno As Integer
    Private _RepossessSeqNo As Integer
    Private _AgreementNo As String
    Private _RALNo As String
    Private _executorid As String
    Private _hasil As Integer
    Private _listReport As DataSet
    Private _approvalno As String
    Private _nextBD As Date
    Private _applicationid As String
    Private _nextInstallment As Integer
    Private _daysoverdue As Integer
    Private _receiptdates As Date
    Private _EstimationPrice As double
    Private _EstimationDate As Date
    Private _EstimationBy As String
    Private _EstimationNotes As String
    Private _approveby As String
    Private _bidderamount As Double
    Private _flag As Integer
    Private _accruedInterest As Integer
    Public Property accruedInterest() As Integer

        Get
            Return _accruedInterest
        End Get
        Set(ByVal Value As Integer)
            _accruedInterest = Value
        End Set
    End Property
    Public Property flag() As Integer

        Get
            Return _flag
        End Get
        Set(ByVal Value As Integer)
            _flag = Value
        End Set
    End Property
    Public Property approveby() As String

        Get
            Return _approveby
        End Get
        Set(ByVal Value As String)
            _approveby = Value
        End Set
    End Property
    Public Property approvalno() As String

        Get
            Return _approvalno
        End Get
        Set(ByVal Value As String)
            _approvalno = Value
        End Set
    End Property
    Public Property EstimationNotes() As String
        Get
            Return _EstimationNotes
        End Get
        Set(ByVal Value As String)
            _EstimationNotes = Value
        End Set
    End Property
    Public Property EstimationBy() As String
        Get
            Return _EstimationBy
        End Get
        Set(ByVal Value As String)
            _EstimationBy = Value
        End Set
    End Property
    Public Property EstimationDate() As Date
        Get
            Return _EstimationDate
        End Get
        Set(ByVal Value As Date)
            _EstimationDate = Value
        End Set
    End Property
    Public Property EstimationPrice() As Double
        Get
            Return _EstimationPrice
        End Get
        Set(ByVal Value As Double)
            _EstimationPrice = Value
        End Set
    End Property
    Public Property bidderamount() As Double
        Get
            Return _bidderamount
        End Get
        Set(ByVal Value As Double)
            _bidderamount = Value
        End Set
    End Property
    Public Property AssetSeqno() As Integer
        Get
            Return _AssetSeqno
        End Get
        Set(ByVal Value As Integer)
            _AssetSeqno = Value
        End Set
    End Property
    Public Property RepossessseqNo() As Integer
        Get
            Return _RepossessSeqNo
        End Get
        Set(ByVal Value As Integer)
            _RepossessSeqNo = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return _applicationid
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property

    Public Property NextBD() As Date
        Get
            Return _nextBD
        End Get
        Set(ByVal Value As Date)
            _nextBD = Value
        End Set
    End Property
    Public Property receiptdates() As Date
        Get
            Return _receiptdates
        End Get
        Set(ByVal Value As Date)
            _receiptdates = Value
        End Set
    End Property

    Public Property ListReport() As DataSet
        Get
            Return _listReport
        End Get
        Set(ByVal Value As DataSet)
            _listReport = Value
        End Set
    End Property
    Public Property daysoverdue() As Integer
        Get
            Return _daysoverdue
        End Get
        Set(ByVal Value As Integer)
            _daysoverdue = Value
        End Set
    End Property
    Public Property hasil() As Integer
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Integer)
            _hasil = Value
        End Set
    End Property
    Public Property ExecutorID() As String
        Get
            Return _executorid
        End Get
        Set(ByVal Value As String)
            _executorid = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property
    Public Property NextInstallment() As Integer
        Get
            Return _nextInstallment
        End Get
        Set(ByVal Value As Integer)
            _nextInstallment = Value
        End Set
    End Property

    Public Property RALNo() As String
        Get
            Return _RALNo
        End Get
        Set(ByVal Value As String)
            _RALNo = Value
        End Set
    End Property

    Public Property strKey() As String
        Get
            Return _strKey
        End Get
        Set(ByVal Value As String)
            _strKey = Value
        End Set
    End Property

    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property


    Public Property Listappraisal() As DataTable
        Get
            Return _listappraisal
        End Get
        Set(ByVal Value As DataTable)
            _listappraisal = Value
        End Set
    End Property

    Public Property ListRAL() As DataTable
        Get
            Return _listRAL
        End Get
        Set(ByVal Value As DataTable)
            _listRAL = Value
        End Set
    End Property
    Public Property ListBidder() As DataTable
        Get
            Return _listBidder
        End Get
        Set(ByVal Value As DataTable)
            _listBidder = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return _collectortype
        End Get
        Set(ByVal Value As String)
            _collectortype = Value
        End Set
    End Property
    Public Property FormBidder As Boolean
    Public Property NoTelepon As String


    ' Public Property BiayaPengurusanSTNKKIR As Decimal
    'Public Property BiayaPerbaikan As Decimal
    Public Property NilaiPenyesuaian As Decimal
    Public Property AssetCode As String
    Public Property ManufacturingYear As Integer
    Public Property MRP As Decimal
    Public Property HargaInternetKoran As Decimal
    Public Property NilaiDepresiasiWajar As Decimal
    Public Property PenawarTerbaik As String
    Public Property PenawarHarga As Decimal
    Public Property CaraPembayaran As String
    Public Property AccountID As String
    'Public Property RefundPremi As Integer


    Public Property MarginDealer As Decimal
    Public Property Grade As String
    Public Property GradeValue As Integer

    Public Property BiayaRepossess As Decimal
    Public Property BiayaKoordinasi As Decimal
    Public Property BiayaMobilisasi As Decimal


End Class