

Public Class CCRequest : Inherits Maxiloan.Parameter.Common
    Private _CGID As String
    Private _BranchID As String
    Private _listData As DataTable

    Private _collectortype As String
    Private _AgreementNo As String
    Private _ApplicationID As String

    Private _RepossesDate As Date
    Private _RepossesBy As String

    Private _AssetLocation As String
    Private _AssetCondition As String
    Private _Checker As String
    Private _CheckerJob As String
    Private _Checkdate As Date
    Private _hasil As Integer
    Private _AssetSeqNo As Integer
    Private _AssetTypeID As String
    Private _CustomerID As String
    Private _CollectorID As String
    Private _BussinessDate As Date
    Private _AssetDescription As String
    Private _EndpastDueDays As Integer
    Private _RepossesSeqNo As Integer

    Private _RequestBy As String
    Private _RefundAmount As Double
    Private _Approvalno As String
    Private _ApprovalStatus As String

    Private _Reason As String
    Private _ApproveBy As String
    Private _ApproveVia As String
    Private _FaxNo As String
    Private _Notes As String
    Private _listReport As DataSet
    Private _RequestNo As String
    Private _RequestDate As Date


    Public Property Approvalno() As String
        Get
            Return _Approvalno
        End Get
        Set(ByVal Value As String)
            _Approvalno = Value
        End Set
    End Property


    Public Property ApprovalStatus() As String
        Get
            Return _ApprovalStatus
        End Get
        Set(ByVal Value As String)
            _ApprovalStatus = Value
        End Set
    End Property

    Public Property RequestBy() As String
        Get
            Return _RequestBy
        End Get
        Set(ByVal Value As String)
            _RequestBy = Value
        End Set
    End Property


    Public Property RefundAmount() As Double
        Get
            Return _RefundAmount
        End Get
        Set(ByVal Value As Double)
            _RefundAmount = Value
        End Set
    End Property

    Public Property Reason() As String
        Get
            Return _Reason
        End Get
        Set(ByVal Value As String)
            _Reason = Value
        End Set
    End Property

    Public Property ApproveBy() As String
        Get
            Return _ApproveBy
        End Get
        Set(ByVal Value As String)
            _ApproveBy = Value
        End Set
    End Property

    Public Property ApproveVia() As String
        Get
            Return _ApproveVia
        End Get
        Set(ByVal Value As String)
            _ApproveVia = Value
        End Set
    End Property

    Public Property FaxNo() As String
        Get
            Return _FaxNo
        End Get
        Set(ByVal Value As String)
            _FaxNo = Value
        End Set
    End Property



    Public Property RepossesSeqNo() As Integer
        Get
            Return _RepossesSeqNo
        End Get
        Set(ByVal Value As Integer)
            _RepossesSeqNo = Value
        End Set
    End Property

    Public Property EndpastDueDays() As Integer
        Get
            Return _EndpastDueDays
        End Get
        Set(ByVal Value As Integer)
            _EndpastDueDays = Value
        End Set
    End Property

    Public Property AssetDescription() As String
        Get
            Return _AssetDescription
        End Get
        Set(ByVal Value As String)
            _AssetDescription = Value
        End Set
    End Property


    Public Property BussinessDate() As Date
        Get
            Return _BussinessDate
        End Get
        Set(ByVal Value As Date)
            _BussinessDate = Value
        End Set
    End Property

    Public Property CollectorID() As String
        Get
            Return _CollectorID
        End Get
        Set(ByVal Value As String)
            _CollectorID = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return _CustomerID
        End Get
        Set(ByVal Value As String)
            _CustomerID = Value
        End Set
    End Property

    Public Property AssetTypeID() As String
        Get
            Return _AssetTypeID
        End Get
        Set(ByVal Value As String)
            _AssetTypeID = Value
        End Set
    End Property

    Public Property AssetSeqNo() As Integer
        Get
            Return _AssetSeqNo
        End Get
        Set(ByVal Value As Integer)
            _AssetSeqNo = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property

    Public Property AssetCondition() As String
        Get
            Return _AssetCondition
        End Get
        Set(ByVal Value As String)
            _AssetCondition = Value
        End Set
    End Property

    Public Property AssetLocation() As String
        Get
            Return _AssetLocation
        End Get
        Set(ByVal Value As String)
            _AssetLocation = Value
        End Set
    End Property

    Public Property Checker() As String
        Get
            Return _Checker
        End Get
        Set(ByVal Value As String)
            _Checker = Value
        End Set
    End Property

    Public Property CheckerJob() As String
        Get
            Return _CheckerJob
        End Get
        Set(ByVal Value As String)
            _CheckerJob = Value
        End Set
    End Property

    Public Property Checkdate() As Date
        Get
            Return _Checkdate
        End Get
        Set(ByVal Value As Date)
            _Checkdate = Value
        End Set
    End Property

    Public Property RepossesDate() As Date
        Get
            Return _RepossesDate
        End Get
        Set(ByVal Value As Date)
            _RepossesDate = Value
        End Set
    End Property

    Public Property hasil() As Integer
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Integer)
            _hasil = Value
        End Set
    End Property


    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property

    Public Property RepossesBy() As String
        Get
            Return _RepossesBy
        End Get
        Set(ByVal Value As String)
            _RepossesBy = Value
        End Set
    End Property

    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return _collectortype
        End Get
        Set(ByVal Value As String)
            _collectortype = Value
        End Set
    End Property

    Public Property listData() As DataTable
        Get
            Return _listData
        End Get
        Set(ByVal Value As DataTable)
            _listData = Value
        End Set
    End Property
    Public Property ListReport() As DataSet
        Get
            Return _listReport
        End Get
        Set(ByVal Value As DataSet)
            _listReport = Value
        End Set
    End Property

    Public Property RequestNo() As String
        Get
            Return _RequestNo
        End Get
        Set(ByVal Value As String)
            _RequestNo = Value
        End Set
    End Property

    Public Property RequestDate() As Date
        Get
            Return _RequestDate
        End Get
        Set(ByVal Value As Date)
            _RequestDate = Value
        End Set
    End Property
End Class

