Public Class CollExpense : Inherits Maxiloan.Parameter.AssetRepo
    Private _ExpenseAmount As Double
    Private _ApproveBy As String
    Private _ApprovalNo As String
    Private _BankAccountID As String

    Public Property BankAccountID() As String
        Get
            Return _BankAccountID
        End Get
        Set(ByVal Value As String)
            _BankAccountID = Value
        End Set
    End Property
    Public Property ExpenseAmount() As Double
        Get
            Return _ExpenseAmount
        End Get
        Set(ByVal Value As Double)
            _ExpenseAmount = Value
        End Set
    End Property

    Public Property ApproveBy() As String
        Get
            Return _ApproveBy
        End Get
        Set(ByVal Value As String)
            _ApproveBy = Value
        End Set
    End Property
    Public Property ApprovalNo() As String
        Get
            Return _ApprovalNo
        End Get
        Set(ByVal Value As String)
            _ApprovalNo = Value
        End Set
    End Property
End Class
