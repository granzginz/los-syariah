﻿
<Serializable()> _
Public Class InsBlokir : Inherits Common    
    Public Property ApplicationId As String
    Public Property Name As String
    Public Property AgreementNo As String
    Public Property SPNo As String
    Public Property SPDate As DateTime
    Public Property AmountOverdue As Decimal
    Public Property LateCharges As Decimal
    Public Property CollectorName As String
    Public Property BlokirBayar As Boolean    
    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
End Class
