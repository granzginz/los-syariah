
<Serializable()> _
Public Class CollGroup : Inherits Common
    Private _CGID As String
    Private _CGName As String
    Private _CGHead As String
    Private _CGHeadName As String
    Private _ListData As DataTable

    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property

    Public Property CGName() As String
        Get
            Return _CGName
        End Get
        Set(ByVal Value As String)
            _CGName = Value
        End Set
    End Property

    Public Property CGHead() As String
        Get
            Return _CGHead
        End Get
        Set(ByVal Value As String)
            _CGHead = Value
        End Set
    End Property

    Public Property CGHeadName() As String
        Get
            Return _CGHeadName
        End Get
        Set(ByVal Value As String)
            _CGHeadName = Value
        End Set
    End Property



    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal Value As DataTable)
            _ListData = Value
        End Set
    End Property














End Class

