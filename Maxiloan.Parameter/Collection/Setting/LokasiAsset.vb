﻿
<Serializable()> _
Public Class LokasiAsset : Inherits Maxiloan.Parameter.Common
    Private _listLokasiAsset As DataTable
    Private _Id As String
    Private _Description As String
    Private _LokasiAssetReport As DataSet
    Private _Alamat As String
    Private _RT As String
    Private _RW As String
    Private _Kelurahan As String
    Private _Kecamatan As String
    Private _Kota As String
    Private _KodePos As String
    Private _Kontak As String
    Private _Jabatan As String
    Private _Handphone As String
    Private _Email As String
    Private _Status As String    

    Public Property listLokasiAsset() As DataTable
        Get
            Return _listLokasiAsset
        End Get
        Set(ByVal Value As DataTable)
            _listLokasiAsset = Value
        End Set
    End Property

    Public Property Id() As String
        Get
            Return _Id
        End Get
        Set(ByVal Value As String)
            _Id = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property ReportLokasiAsset() As DataSet
        Get
            Return _LokasiAssetReport
        End Get
        Set(ByVal Value As DataSet)
            _LokasiAssetReport = Value
        End Set
    End Property

    Public Property Alamat() As String
        Get
            Return _Alamat
        End Get
        Set(ByVal Value As String)
            _Alamat = Value
        End Set
    End Property

    Public Property RT() As String
        Get
            Return _RT
        End Get
        Set(ByVal Value As String)
            _RT = Value
        End Set
    End Property

    Public Property RW() As String
        Get
            Return _RW
        End Get
        Set(ByVal Value As String)
            _RW = Value
        End Set
    End Property

    Public Property Kelurahan() As String
        Get
            Return _Kelurahan
        End Get
        Set(ByVal Value As String)
            _Kelurahan = Value
        End Set
    End Property

    Public Property Kecamatan() As String
        Get
            Return _Kecamatan
        End Get
        Set(ByVal Value As String)
            _Kecamatan = Value
        End Set
    End Property

    Public Property Kota() As String
        Get
            Return _Kota
        End Get
        Set(ByVal Value As String)
            _Kota = Value
        End Set
    End Property

    Public Property KodePos() As String
        Get
            Return _KodePos
        End Get
        Set(ByVal Value As String)
            _KodePos = Value
        End Set
    End Property

    Public Property Kontak() As String
        Get
            Return _Kontak
        End Get
        Set(ByVal Value As String)
            _Kontak = Value
        End Set
    End Property

    Public Property Jabatan() As String
        Get
            Return _Jabatan
        End Get
        Set(ByVal Value As String)
            _Jabatan = Value
        End Set
    End Property

    Public Property Handphone() As String
        Get
            Return _Handphone
        End Get
        Set(ByVal Value As String)
            _Handphone = Value
        End Set
    End Property

    Public Property Email() As String
        Get
            Return _Email
        End Get
        Set(ByVal Value As String)
            _Email = Value
        End Set
    End Property

    Public Property Status() As String
        Get
            Return _Status
        End Get
        Set(ByVal Value As String)
            _Status = Value
        End Set
    End Property
End Class
