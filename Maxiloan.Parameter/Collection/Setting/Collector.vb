
<Serializable()> _
Public Class Collector : Inherits CollGroup
    Private _CollectorID As String
    Private _EmployeeID As String
    Private _EmployeeName As String
    Private _CollectorType As String
    Private _CollectorName As String
    Private _supervisor As String
    Private _active As String
    Private _notes As String
    Private _WhereCollectorType As String
    Private _DeskCollID As String
    Private _ZipCode As String
    Private _NoMOU As String
    Private _TglMOU As Date
    Private _TglExpiredMOU As Date
    Private _TglExpiredMOUNew As Date
    Private _ket As String

    Private _ExtendFrom As Date
    Private _ExtendTo As Date
    Private _OldCollectorType As String
    Private _PasswordMobile As String
    Private _ImeiMobile As String
    Private _listPemberiKuasaSKE As DataTable
    Private _Position As String

#Region "bankAccount"
    Private Property _collBankID As String
    Private Property _collBankBranch As String
    Private Property _collBankBranchID As String
    Public Property collAccountNo As String
    Public Property collAcountName As String

#End Region



    'Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
    Public Property WhereCond2 As String
    Public Property ActionAddEdit As String


    Public Property DeskCollID() As String
        Get
            Return _DeskCollID
        End Get
        Set(ByVal Value As String)
            _DeskCollID = Value
        End Set
    End Property

    Public Property ZipCode() As String
        Get
            Return _ZipCode
        End Get
        Set(ByVal Value As String)
            _ZipCode = Value
        End Set
    End Property

    Public Property CollectorID() As String
        Get
            Return _CollectorID
        End Get
        Set(ByVal Value As String)
            _CollectorID = Value
        End Set
    End Property


    Public Property CollectorName() As String
        Get
            Return _CollectorName
        End Get
        Set(ByVal Value As String)
            _CollectorName = Value
        End Set
    End Property


    Public Property EmployeeID() As String
        Get
            Return _EmployeeID
        End Get
        Set(ByVal Value As String)
            _EmployeeID = Value
        End Set
    End Property

    Public Property EmployeeName() As String
        Get
            Return _EmployeeName
        End Get
        Set(ByVal Value As String)
            _EmployeeName = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return _CollectorType
        End Get
        Set(ByVal Value As String)
            _CollectorType = Value
        End Set
    End Property

    Public Property Supervisor() As String
        Get
            Return _supervisor
        End Get
        Set(ByVal Value As String)
            _supervisor = Value
        End Set
    End Property

    Public Property Active() As String
        Get
            Return _active
        End Get
        Set(ByVal Value As String)
            _active = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property

    Public Property WhereCollectorType() As String
        Get
            Return _WhereCollectorType
        End Get
        Set(ByVal Value As String)
            _WhereCollectorType = Value
        End Set
    End Property


    Public Property NoMOU() As String
        Get
            Return _NoMOU
        End Get
        Set(ByVal Value As String)
            _NoMOU = Value
        End Set
    End Property

    Public Property TglMOU() As Date
        Get
            Return _TglMOU
        End Get
        Set(ByVal Value As Date)
            _TglMOU = Value
        End Set
    End Property

    Public Property TglExpiredMOU() As Date
        Get
            Return _TglExpiredMOU
        End Get
        Set(ByVal Value As Date)
            _TglExpiredMOU = Value
        End Set
    End Property

    Public Property TglExpiredMOUNew() As Date
        Get
            Return _TglExpiredMOUNew
        End Get
        Set(ByVal Value As Date)
            _TglExpiredMOUNew = Value
        End Set
    End Property

    Public Property ket() As String
        Get
            Return _ket
        End Get
        Set(ByVal Value As String)
            _ket = Value
        End Set
    End Property

    Public Property ExtendFrom() As Date
        Get
            Return _ExtendFrom
        End Get
        Set(ByVal Value As Date)
            _ExtendFrom = Value
        End Set
    End Property

    Public Property ExtendTo() As Date
        Get
            Return _ExtendTo
        End Get
        Set(ByVal Value As Date)
            _ExtendTo = Value
        End Set
    End Property
    Public Property collBankID As String
        Get
            Return _collBankID
        End Get
        Set(ByVal value As String)
            _collBankID = value
        End Set
    End Property
    Public Property collBankBranch As String
        Get
            Return _collBankBranch
        End Get
        Set(ByVal value As String)
            _collBankBranch = value
        End Set
    End Property

    Public Property collBankBranchID As String
        Get
            Return _collBankBranchID
        End Get
        Set(ByVal value As String)
            _collBankBranchID = value
        End Set
    End Property

    Public Property Kecamatan As String
    Public Property Kota As String
    Public Property Status As String
    Public Property MaksimalKontrak As Integer
    Public Property Kelurahan As String
    Public Property IDNumber As String
    Public Property CollectorIDA As String
    Public Property CollectorIDB As String
    Public Property CollectorA As DataTable
    Public Property CollectorB As DataTable
    Public Property CollectorStatus As String
    Public Property CollectorNPWP As String

    Public Property OldCollectorType() As String
        Get
            Return _OldCollectorType
        End Get
        Set(ByVal Value As String)
            _OldCollectorType = Value
        End Set
    End Property

    Public Property PasswordMobile() As String
        Get
            Return _PasswordMobile
        End Get
        Set(ByVal Value As String)
            _PasswordMobile = Value
        End Set
    End Property

    Public Property ImeiMobile() As String
        Get
            Return _ImeiMobile
        End Get
        Set(ByVal Value As String)
            _ImeiMobile = Value
        End Set
    End Property

    Public Property listPemberiKuasaSKE() As DataTable
        Get
            Return _listPemberiKuasaSKE
        End Get
        Set(ByVal Value As DataTable)
            _listPemberiKuasaSKE = Value
        End Set
    End Property

    Public Property Position() As String
        Get
            Return _Position
        End Get
        Set(ByVal Value As String)
            _Position = Value
        End Set
    End Property
End Class

