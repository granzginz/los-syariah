
<Serializable()> _
Public Class ActionResult : Inherits Common
    Private _actionid As String
    Private _actionname As String
    Private _resultid As String
    Private _resultname As String
    Private _resultcategory As String
    Private _ListData As DataTable

    Public Property actionid() As String
        Get
            Return _actionid
        End Get
        Set(ByVal Value As String)
            _actionid = Value
        End Set
    End Property

    Public Property actionname() As String
        Get
            Return _actionname
        End Get
        Set(ByVal Value As String)
            _actionname = Value
        End Set
    End Property

    Public Property resultid() As String
        Get
            Return _resultid
        End Get
        Set(ByVal Value As String)
            _resultid = Value
        End Set
    End Property


    Public Property resultname() As String
        Get
            Return _resultname
        End Get
        Set(ByVal Value As String)
            _resultname = Value
        End Set
    End Property

    Public Property resultcategory() As String
        Get
            Return _resultcategory
        End Get
        Set(ByVal Value As String)
            _resultcategory = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal Value As DataTable)
            _ListData = Value
        End Set
    End Property














End Class
