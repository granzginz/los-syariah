
<Serializable()> _
Public Class CasesSetting : Inherits Maxiloan.Parameter.Common
    Private _casesId As String
    Private _casesIdnew As String
    Private _description As String
    Private _listcasessetting As DataTable
    Private _listreport As DataSet


    Public Property CasesID() As String
        Get
            Return CType(_casesId, String)
        End Get
        Set(ByVal Value As String)
            _casesId = Value
        End Set
    End Property
    Public Property CasesIDnew() As String
        Get
            Return CType(_casesIdnew, String)
        End Get
        Set(ByVal Value As String)
            _casesIdnew = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return CType(_description, String)
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property

    Public Property ListCasesSetting() As DataTable
        Get
            Return (CType(_listcasessetting, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listcasessetting = Value
        End Set
    End Property

    Public Property ListReport() As DataSet
        Get
            Return (CType(_listreport, DataSet))
        End Get
        Set(ByVal Value As DataSet)
            _listreport = Value
        End Set
    End Property

    Public Property konsumen As Boolean
    Public Property unit As Boolean
    Public Property klas As String
    Public Property penyebab As String

End Class

