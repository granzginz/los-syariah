
<Serializable()> _
Public Class CollZipCode : Inherits Common
    Private _CGID As String
    Private _CGName As String
    Private _ZipCode As String
    Private _Kelurahan As String
    Private _Kecamatan As String
    Private _City As String
    Private _CollectorType As String
    Private _CollectorID As String
    Private _CollectorName As String
    Private _ListData As DataTable

    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property

    Public Property CGName() As String
        Get
            Return _CGName
        End Get
        Set(ByVal Value As String)
            _CGName = Value
        End Set
    End Property

    Public Property ZipCode() As String
        Get
            Return _ZipCode
        End Get
        Set(ByVal Value As String)
            _ZipCode = Value
        End Set
    End Property

    Public Property Kelurahan() As String
        Get
            Return _Kelurahan
        End Get
        Set(ByVal Value As String)
            _Kelurahan = Value
        End Set
    End Property

    Public Property Kecamatan() As String
        Get
            Return _Kecamatan
        End Get
        Set(ByVal Value As String)
            _Kecamatan = Value
        End Set
    End Property

    Public Property City() As String
        Get
            Return _City
        End Get
        Set(ByVal Value As String)
            _City = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return _CollectorType
        End Get
        Set(ByVal Value As String)
            _CollectorType = Value
        End Set
    End Property

    Public Property CollectorID() As String
        Get
            Return _CollectorID
        End Get
        Set(ByVal Value As String)
            _CollectorID = Value
        End Set
    End Property

    Public Property CollectorName() As String
        Get
            Return _CollectorName
        End Get
        Set(ByVal Value As String)
            _CollectorName = Value
        End Set
    End Property


    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal Value As DataTable)
            _ListData = Value
        End Set
    End Property



End Class

