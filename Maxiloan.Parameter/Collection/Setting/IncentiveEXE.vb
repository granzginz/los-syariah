
<Serializable()> _
Public Class IncentiveEXE : Inherits Common
    Private _listdata As DataTable
    Private _listreport As DataSet

    Private _assetstatus As String
    Private _action As String
    Private _minincentiveEI As Double
    Private _maxincentiveEI As Double
    Private _minincentiveEP As Double
    Private _maxincentiveEP As Double
    Public Property AssetStatus() As String
        Get
            Return _assetstatus
        End Get
        Set(ByVal Value As String)
            _assetstatus = Value
        End Set
    End Property
    Public Property Action() As String
        Get
            Return _action
        End Get
        Set(ByVal Value As String)
            _action = Value
        End Set
    End Property
    Public Property MinIncentiveEI() As Double
        Get
            Return _minincentiveEI
        End Get
        Set(ByVal Value As Double)
            _minincentiveEI = Value
        End Set
    End Property
    Public Property MaxIncentiveEI() As Double
        Get
            Return _maxincentiveEI
        End Get
        Set(ByVal Value As Double)
            _maxincentiveEI = Value
        End Set
    End Property
    Public Property MinIncentiveEP() As Double
        Get
            Return _minincentiveEP
        End Get
        Set(ByVal Value As Double)
            _minincentiveEP = Value
        End Set
    End Property
    Public Property MaxIncentiveEP() As Double
        Get
            Return _maxincentiveEP
        End Get
        Set(ByVal Value As Double)
            _maxincentiveEP = Value

        End Set
    End Property


    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property ListReport() As DataSet
        Get
            Return _listreport
        End Get
        Set(ByVal Value As DataSet)
            _listreport = Value
        End Set
    End Property


End Class
