
<Serializable()> _
Public Class IncentiveBM : Inherits Common
    Private _listdata As DataTable
    Private _combolist As DataTable

    Private _beginstatus As String
    Private _endstatus As String
    Private _percentage As Decimal
    Private _tariff As Double
    Private _active As String
    Private _seqno As Integer
    Private _beginendstatus As String
    Private _listreport As DataSet

    Public Property ListReport() As DataSet
        Get
            Return (CType(_listreport, DataSet))
        End Get
        Set(ByVal Value As DataSet)
            _listreport = Value
        End Set
    End Property
    Public Property ComboList() As DataTable
        Get
            Return _combolist
        End Get
        Set(ByVal Value As DataTable)
            _combolist = Value
        End Set
    End Property
    Public Property BeginEndStatus() As String
        Get
            Return _beginendstatus
        End Get
        Set(ByVal Value As String)
            _beginendstatus = Value

        End Set
    End Property

    Public Property Seqno() As Integer
        Get
            Return _seqno
        End Get
        Set(ByVal Value As Integer)
            _seqno = Value
        End Set
    End Property
    Public Property BeginStatus() As String
        Get
            Return _beginstatus
        End Get
        Set(ByVal Value As String)
            _beginstatus = Value
        End Set
    End Property
    Public Property EndStatus() As String
        Get
            Return _endstatus
        End Get
        Set(ByVal Value As String)
            _endstatus = Value
        End Set
    End Property
    Public Property Percentage() As Decimal
        Get
            Return _percentage
        End Get
        Set(ByVal Value As Decimal)
            _percentage = Value
        End Set
    End Property
    Public Property Tariff() As Double
        Get
            Return _tariff
        End Get
        Set(ByVal Value As Double)
            _tariff = Value
        End Set
    End Property
    Public Property Active() As String
        Get
            Return _active
        End Get
        Set(ByVal Value As String)
            _active = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
End Class
