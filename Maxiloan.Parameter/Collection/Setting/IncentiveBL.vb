
<Serializable()> _
Public Class IncentiveBL : Inherits Common
    Private _listdata As DataTable
    Private _listreport As DataSet
    Private _minpercentage As Decimal
    Private _maxpercentage As Decimal
    Private _point As Integer
    Private _tariff As Decimal

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property ListReport() As DataSet
        Get
            Return _listreport
        End Get
        Set(ByVal Value As DataSet)
            _listreport = Value
        End Set
    End Property
    
    Public Property MinPercentage() As Decimal
        Get
            Return _minpercentage
        End Get
        Set(ByVal Value As Decimal)
            _minpercentage = Value
        End Set
    End Property
    Public Property MaxPercentage() As Decimal
        Get
            Return _maxpercentage
        End Get
        Set(ByVal Value As Decimal)
            _maxpercentage = Value
        End Set
    End Property
    Public Property Point() As Integer
        Get
            Return _point
        End Get
        Set(ByVal Value As Integer)
            _point = Value
        End Set
    End Property
    Public Property Tariff() As Decimal
        Get
            Return _tariff
        End Get
        Set(ByVal Value As Decimal)
            _tariff = Value
        End Set
    End Property
End Class
