
<Serializable()> _
Public Class CollLetter : Inherits Common
    Private _listdata As DataTable
    Private _letterid As String
    Private _lettername As String
    Private _template As String
    Private _listreport As DataSet

    Public Property ListReport() As DataSet
        Get
            Return _listreport
        End Get
        Set(ByVal Value As DataSet)
            _listreport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property Template() As String
        Get
            Return _template
        End Get
        Set(ByVal Value As String)
            _template = Value
        End Set
    End Property
    Public Property LetterName() As String
        Get
            Return _lettername
        End Get
        Set(ByVal Value As String)
            _lettername = Value
        End Set
    End Property
    Public Property LetterId() As String
        Get
            Return _letterid
        End Get
        Set(ByVal Value As String)
            _letterid = Value
        End Set
    End Property


End Class
