
<Serializable()> _
Public Class CollectionList : Inherits Common
    Private _listdata As DataTable
    Private _listdataset As DataSet
    Private _TotRec As Int64
    Private _TotPage As Int64
    Private _Page As Int64
    Private _recordCount As Int64

    Public Property TotRec() As Int64
        Get
            Return _TotRec
        End Get
        Set(ByVal Value As Int64)
            _TotRec = Value
        End Set
    End Property



    Public Property TotPage() As Int64
        Get
            Return _TotPage
        End Get
        Set(ByVal Value As Int64)
            _TotPage = Value
        End Set
    End Property

    Public Property Page() As Int64
        Get
            Return _Page
        End Get
        Set(ByVal Value As Int64)
            _Page = Value
        End Set
    End Property

    Public Property RecordCount() As Int64
        Get
            Return _recordCount
        End Get
        Set(ByVal Value As Int64)
            _recordCount = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property ListDataSet() As DataSet
        Get
            Return _listdataset
        End Get
        Set(ByVal Value As DataSet)
            _listdataset = Value
        End Set
    End Property
End Class


