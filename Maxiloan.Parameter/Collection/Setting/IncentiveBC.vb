
<Serializable()> _
Public Class IncentiveBC : Inherits Common
    Private _listdata As DataTable
    Private _seqno As Integer
    Private _mindays As Integer
    Private _maxdays As Integer
    Private _percentage As Decimal
    Private _listreport As DataSet

    Public Property ListReport() As DataSet
        Get
            Return (CType(_listreport, DataSet))
        End Get
        Set(ByVal Value As DataSet)
            _listreport = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property SeqNo() As Integer
        Get
            Return _seqno
        End Get
        Set(ByVal Value As Integer)
            _seqno = Value
        End Set
    End Property
    Public Property MinDays() As Integer
        Get
            Return _mindays
        End Get
        Set(ByVal Value As Integer)
            _mindays = Value

        End Set
    End Property
    Public Property MaxDays() As Integer
        Get
            Return _maxdays
        End Get
        Set(ByVal Value As Integer)
            _maxdays = Value
        End Set
    End Property
    Public Property Percentage() As Decimal
        Get
            Return _percentage
        End Get
        Set(ByVal Value As Decimal)
            _percentage = Value
        End Set
    End Property
End Class
