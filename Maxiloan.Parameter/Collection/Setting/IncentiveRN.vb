
<Serializable()> _
Public Class IncentiveRN : Inherits Common
    Private _listdata As DataTable
    Private _maxreceipt As Integer
    Private _minreceipt As Integer
    Private _tariff As Decimal
    Private _listreport As DataSet
    Private _seqno As Integer
    Public Property SeqNo() As Integer
        Get
            Return _seqno
        End Get
        Set(ByVal Value As Integer)
            _seqno = Value
        End Set
    End Property
    Public Property ListReport() As DataSet
        Get
            Return _listreport
        End Get
        Set(ByVal Value As DataSet)
            _listreport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property MaxReceipt() As Integer
        Get
            Return _maxreceipt
        End Get
        Set(ByVal Value As Integer)
            _maxreceipt = Value
        End Set
    End Property
    Public Property MinReceipt() As Integer
        Get
            Return _minreceipt
        End Get
        Set(ByVal Value As Integer)
            _minreceipt = Value
        End Set
    End Property
    Public Property Tariff() As Decimal
        Get
            Return _tariff
        End Get
        Set(ByVal Value As Decimal)
            _tariff = Value
        End Set
    End Property

End Class
