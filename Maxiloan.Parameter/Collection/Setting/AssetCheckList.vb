

<Serializable()> _
Public Class AssetCheckList : Inherits Maxiloan.Parameter.Common
    Private _listAssetCheck As DataTable
    Private _AssetTypeID As String
    Private _CheckListID As String
    Private _Description As String
    Private _isYNQuestion As String
    Private _isQTYQuestion As String
    Private _isActive As String
    Private _listAssetReport As DataSet

    Public Property ListAssetCheckList() As DataTable
        Get
            Return _listAssetCheck
        End Get
        Set(ByVal Value As DataTable)
            _listAssetCheck = Value
        End Set
    End Property

    Public Property AssetTypeID() As String
        Get
            Return _AssetTypeID
        End Get
        Set(ByVal Value As String)
            _AssetTypeID = Value
        End Set
    End Property

    Public Property CheckListID() As String
        Get
            Return _CheckListID
        End Get
        Set(ByVal Value As String)
            _CheckListID = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property isYNQuestion() As String
        Get
            Return _isYNQuestion
        End Get
        Set(ByVal Value As String)
            _isYNQuestion = Value
        End Set
    End Property

    Public Property isQTYQuestion() As String
        Get
            Return _isQTYQuestion
        End Get
        Set(ByVal Value As String)
            _isQTYQuestion = Value
        End Set
    End Property

    Public Property ReportAssetCheck() As DataSet
        Get
            Return _listAssetReport
        End Get
        Set(ByVal Value As DataSet)
            _listAssetReport = Value
        End Set
    End Property
    Public Property IsActive() As String
        Get
            Return _isActive
        End Get
        Set(ByVal Value As String)
            _isActive = Value
        End Set
    End Property

End Class