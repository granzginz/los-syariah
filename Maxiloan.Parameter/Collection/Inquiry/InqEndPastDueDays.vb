﻿
<Serializable()>
Public Class InqEndPastDueDays : Inherits Common
    Private _ApplicationID As String
    Private _AgreementNo As String
    Private _CollectorID As String
    Private _CollectorName As String
    Private _City As String
    Private _CGID As String
    Private _ListData As DataTable

    Public Property ApplicationID As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property

    Public Property AgreementNo As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property
    Public Property CollectorID As String
        Get
            Return _CollectorID
        End Get
        Set(ByVal Value As String)
            _CollectorID = Value
        End Set
    End Property
    Public Property CollectorName As String
        Get
            Return _CollectorName
        End Get
        Set(ByVal Value As String)
            _CollectorName = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal Value As DataTable)
            _ListData = Value
        End Set
    End Property

    Public Property City() As String
        Get
            Return _City
        End Get
        Set(ByVal Value As String)
            _City = Value
        End Set
    End Property
    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property

End Class
