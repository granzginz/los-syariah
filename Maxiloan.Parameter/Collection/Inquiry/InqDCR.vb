
<Serializable()> _
Public Class InqDCR : Inherits Common
    Private _CGID As String
    Private _BranchID As String
    Private _listdata As DataTable
    Private _DateFrom As Date
    Private _DateTo As Date
    Private _CollectorID As String

    Public Property CollectorID() As String
        Get
            Return _CollectorID
        End Get
        Set(ByVal Value As String)
            _CollectorID = Value
        End Set
    End Property

    Public Property DateFrom() As Date
        Get
            Return _DateFrom
        End Get
        Set(ByVal Value As Date)
            _DateFrom = Value
        End Set
    End Property

    Public Property DateTo() As Date
        Get
            Return _DateTo
        End Get
        Set(ByVal Value As Date)
            _DateTo = Value
        End Set
    End Property


    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property

    Public Property listdata() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property



End Class
