
<Serializable()> _
Public Class InqCollectorScheme : Inherits Maxiloan.Parameter.Common
    Private _listCOllectorScheme As DataTable
    Private _criteria As String

    Public Property CollectorSchemeList() As DataTable
        Get
            Return _listCOllectorScheme
        End Get
        Set(ByVal Value As DataTable)
            _listCOllectorScheme = Value
        End Set
    End Property

    Public Property Criteria() As String
        Get
            Return _criteria
        End Get
        Set(ByVal Value As String)
            _criteria = Value
        End Set
    End Property

End Class
