

Public Class ViewCollection : Inherits Maxiloan.Parameter.Common
    Private _BranchID1 As String
    Private _listData As DataTable
    Private _strkey As String
    Private _ListReport As DataSet
    Private _AgreementNo As String
    Private _CustomerName As String
    Private _Address As String
    Private _MailingZipCode As String
    Private _CGName As String
    Private _CollectorID As String
    Private _Supervisor As String
    Private _ContractStatus As String
    Private _AssetStatus As String
    Private _CollStatus As Integer
    Private _ExRepo As Integer
    Private _FixedAssign As String
    Private _PlanDateToSKT As Date
    Private _PlanDate As Date
    Private _PlanActivity As String
    Private _EndPastDueDays As Integer
    Private _OverdueAmount As Integer
    Private _NextInstallmentNumber As Integer
    Private _NextInstallmentDate As Date
    Private _InstallmentAmount As Integer
    Private _OSBalance As Integer
    Private _OSInsurance As Integer
    Private _OSCollectionFee As Integer
    Private _OSLateCharges As Integer
    Private _RALNo As String
    Private _RALDate As Date
    Private _Executor As String
    Private _AssetDescription As String
    Private _ApplicationID As String

    Public Property BranchID1() As String
        Get
            Return _BranchID1
        End Get
        Set(ByVal Value As String)
            _BranchID1 = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property
    Public Property Executor() As String
        Get
            Return _Executor
        End Get
        Set(ByVal Value As String)
            _Executor = Value
        End Set
    End Property
    Public Property RALNo() As String
        Get
            Return _RALNo
        End Get
        Set(ByVal Value As String)
            _RALNo = Value
        End Set
    End Property
    Public Property OSInsurance() As Integer
        Get
            Return _OSInsurance
        End Get
        Set(ByVal Value As Integer)
            _OSInsurance = Value
        End Set
    End Property
    Public Property OSBalance() As Integer
        Get
            Return _OSBalance
        End Get
        Set(ByVal Value As Integer)
            _OSBalance = Value
        End Set
    End Property
    Public Property OSLateCharges() As Integer
        Get
            Return _OSLateCharges
        End Get
        Set(ByVal Value As Integer)
            _OSLateCharges = Value
        End Set
    End Property
    Public Property OSCollectionFee() As Integer
        Get
            Return _OSCollectionFee
        End Get
        Set(ByVal Value As Integer)
            _OSCollectionFee = Value
        End Set
    End Property
    Public Property RALDate() As Date
        Get
            Return _RALDate
        End Get
        Set(ByVal Value As Date)
            _RALDate = Value
        End Set
    End Property
    Public Property EndPastDueDays() As Integer
        Get
            Return _EndPastDueDays
        End Get
        Set(ByVal Value As Integer)
            _EndPastDueDays = Value
        End Set
    End Property
    Public Property AssetDescription() As String
        Get
            Return _AssetDescription
        End Get
        Set(ByVal Value As String)
            _AssetDescription = Value
        End Set
    End Property
    Public Property CollectorID() As String
        Get
            Return _CollectorID
        End Get
        Set(ByVal Value As String)
            _CollectorID = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return _CustomerName
        End Get
        Set(ByVal Value As String)
            _CustomerName = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property
    Public Property listData() As DataTable
        Get
            Return _listData
        End Get
        Set(ByVal Value As DataTable)
            _listData = Value
        End Set
    End Property
    Public Property ListReport() As DataSet
        Get
            Return _ListReport
        End Get
        Set(ByVal Value As DataSet)
            _ListReport = Value
        End Set
    End Property
    Public Property strKey() As String
        Get
            Return _strkey
        End Get
        Set(ByVal Value As String)
            _strkey = Value
        End Set
    End Property
    Public Property Address() As String
        Get
            Return _Address
        End Get
        Set(ByVal Value As String)
            _Address = Value
        End Set
    End Property
    Public Property MailingZipCode() As String
        Get
            Return _MailingZipCode
        End Get
        Set(ByVal Value As String)
            _MailingZipCode = Value
        End Set
    End Property
    Public Property CGName() As String
        Get
            Return _CGName
        End Get
        Set(ByVal Value As String)
            _CGName = Value
        End Set
    End Property
    Public Property Supervisor() As String
        Get
            Return _Supervisor
        End Get
        Set(ByVal Value As String)
            _Supervisor = Value
        End Set
    End Property
    Public Property ContractStatus() As String
        Get
            Return _ContractStatus
        End Get
        Set(ByVal Value As String)
            _ContractStatus = Value
        End Set
    End Property
    Public Property AssetStatus() As String
        Get
            Return _AssetStatus
        End Get
        Set(ByVal Value As String)
            _AssetStatus = Value
        End Set
    End Property
    Public Property FixedAssign() As String
        Get
            Return _FixedAssign
        End Get
        Set(ByVal Value As String)
            _FixedAssign = Value
        End Set
    End Property
    Public Property PlanActivity() As String
        Get
            Return _PlanActivity
        End Get
        Set(ByVal Value As String)
            _PlanActivity = Value
        End Set
    End Property
    Public Property CollStatus() As Integer
        Get
            Return _CollStatus
        End Get
        Set(ByVal Value As Integer)
            _CollStatus = Value
        End Set
    End Property
    Public Property ExRepo() As Integer
        Get
            Return _ExRepo
        End Get
        Set(ByVal Value As Integer)
            _ExRepo = Value
        End Set
    End Property
    Public Property PlanDate() As Date
        Get
            Return _PlanDate
        End Get
        Set(ByVal Value As Date)
            _PlanDate = Value
        End Set
    End Property
    Public Property PlanDateToSKT() As Date
        Get
            Return _PlanDateToSKT
        End Get
        Set(ByVal Value As Date)
            _PlanDateToSKT = Value
        End Set
    End Property
    Public Property OverdueAmount() As Integer
        Get
            Return _OverdueAmount
        End Get
        Set(ByVal Value As Integer)
            _OverdueAmount = Value
        End Set
    End Property
    Public Property NextInstallmentNumber() As Integer
        Get
            Return _NextInstallmentNumber
        End Get
        Set(ByVal Value As Integer)
            _NextInstallmentNumber = Value
        End Set
    End Property
    Public Property InstallmentAmount() As Integer
        Get
            Return _InstallmentAmount
        End Get
        Set(ByVal Value As Integer)
            _InstallmentAmount = Value
        End Set
    End Property
    Public Property NextInstallmentDate() As Date
        Get
            Return _NextInstallmentDate
        End Get
        Set(ByVal Value As Date)
            _NextInstallmentDate = Value
        End Set
    End Property
End Class

