

Public Class AssetInventory : Inherits Maxiloan.Parameter.Common
    Private _CGID As String
    Private _BranchID As String

    Private _listData As DataTable
    Private _strkey As String
    Private _collectortype As String
    Private _AgreementNo As String
    Private _ApplicationID As String
    Private _RalNo As String
    Private _RepossesDate As Date
    Private _InventoryDate As Date
    Private _Untildate As Date
    Private _Notes As String
    Private _onHand As Integer
    Private _AssetLocation As String
    Private _AssetCondition As String
    Private _Checker As String
    Private _CheckerJob As String
    Private _Checkdate As Date
    Private _hasil As Integer
    Private _AssetSeqNo As Integer
    Private _AssetTypeID As String
    Private _CustomerID As String
    Private _CollectorID As String
    Private _BussinessDate As Date
    Private _AssetDescription As String
    Private _EndpastDueDays As Integer
    Private _RepossesSeqNo As Integer

    Private _BAPKNo As String
    Private _CheckListID As String
    Private _YNQuestion As String
    Private _Quantity As String

    Private _RALDate As Date
    Private _RALEndDate As Date

    Public Property RepossesSeqNo() As Integer
        Get
            Return _RepossesSeqNo
        End Get
        Set(ByVal Value As Integer)
            _RepossesSeqNo = Value
        End Set
    End Property

    Public Property InventoryDate() As Date
        Get
            Return _InventoryDate
        End Get
        Set(ByVal Value As Date)
            _InventoryDate = Value
        End Set
    End Property

    Public Property RALDate() As Date
        Get
            Return _RALDate
        End Get
        Set(ByVal Value As Date)
            _RALDate = Value
        End Set
    End Property

    Public Property RALEndDate() As Date
        Get
            Return _RALEndDate
        End Get
        Set(ByVal Value As Date)
            _RALEndDate = Value
        End Set
    End Property

    Public Property Quantity() As String
        Get
            Return _Quantity
        End Get
        Set(ByVal Value As String)
            _Quantity = Value
        End Set
    End Property

    Public Property YNQuestion() As String
        Get
            Return _YNQuestion
        End Get
        Set(ByVal Value As String)
            _YNQuestion = Value
        End Set
    End Property

    Public Property CheckListID() As String
        Get
            Return _CheckListID
        End Get
        Set(ByVal Value As String)
            _CheckListID = Value
        End Set
    End Property


    Public Property BAPKNo() As String
        Get
            Return _BAPKNo
        End Get
        Set(ByVal Value As String)
            _BAPKNo = Value
        End Set
    End Property

    Public Property EndpastDueDays() As Integer
        Get
            Return _EndpastDueDays
        End Get
        Set(ByVal Value As Integer)
            _EndpastDueDays = Value
        End Set
    End Property

    Public Property AssetDescription() As String
        Get
            Return _AssetDescription
        End Get
        Set(ByVal Value As String)
            _AssetDescription = Value
        End Set
    End Property


    Public Property BussinessDate() As Date
        Get
            Return _BussinessDate
        End Get
        Set(ByVal Value As Date)
            _BussinessDate = Value
        End Set
    End Property

    Public Property CollectorID() As String
        Get
            Return _CollectorID
        End Get
        Set(ByVal Value As String)
            _CollectorID = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return _CustomerID
        End Get
        Set(ByVal Value As String)
            _CustomerID = Value
        End Set
    End Property

    Public Property AssetTypeID() As String
        Get
            Return _AssetTypeID
        End Get
        Set(ByVal Value As String)
            _AssetTypeID = Value
        End Set
    End Property

    Public Property AssetSeqNo() As Integer
        Get
            Return _AssetSeqNo
        End Get
        Set(ByVal Value As Integer)
            _AssetSeqNo = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property

    Public Property AssetCondition() As String
        Get
            Return _AssetCondition
        End Get
        Set(ByVal Value As String)
            _AssetCondition = Value
        End Set
    End Property

    Public Property AssetLocation() As String
        Get
            Return _AssetLocation
        End Get
        Set(ByVal Value As String)
            _AssetLocation = Value
        End Set
    End Property

    Public Property Checker() As String
        Get
            Return _Checker
        End Get
        Set(ByVal Value As String)
            _Checker = Value
        End Set
    End Property

    Public Property CheckerJob() As String
        Get
            Return _CheckerJob
        End Get
        Set(ByVal Value As String)
            _CheckerJob = Value
        End Set
    End Property

    Public Property Checkdate() As Date
        Get
            Return _Checkdate
        End Get
        Set(ByVal Value As Date)
            _Checkdate = Value
        End Set
    End Property

    Public Property RepossesDate() As Date
        Get
            Return _RepossesDate
        End Get
        Set(ByVal Value As Date)
            _RepossesDate = Value
        End Set
    End Property

    Public Property UntilDate() As Date
        Get
            Return _Untildate
        End Get
        Set(ByVal Value As Date)
            _Untildate = Value
        End Set
    End Property

    Public Property hasil() As Integer
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Integer)
            _hasil = Value
        End Set
    End Property


    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property

    Public Property OnHand() As Integer
        Get
            Return _onHand
        End Get
        Set(ByVal Value As Integer)
            _onHand = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property

    Public Property RALNo() As String
        Get
            Return _RalNo
        End Get
        Set(ByVal Value As String)
            _RalNo = Value
        End Set
    End Property

    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return _collectortype
        End Get
        Set(ByVal Value As String)
            _collectortype = Value
        End Set
    End Property

    Public Property listData() As DataTable
        Get
            Return _listData
        End Get
        Set(ByVal Value As DataTable)
            _listData = Value
        End Set
    End Property

    Public Property strKey() As String
        Get
            Return _strkey
        End Get
        Set(ByVal Value As String)
            _strkey = Value
        End Set
    End Property
End Class

