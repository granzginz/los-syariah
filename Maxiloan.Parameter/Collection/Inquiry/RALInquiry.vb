
<Serializable()> _
Public Class RALInquiry : Inherits Common
    Private _cgid As String
    Private _collectortype As String
    Private _strkey As String
    Private _listInquiry As DataTable

    Public Property ListInquiry() As DataTable
        Get
            Return _listInquiry
        End Get
        Set(ByVal Value As DataTable)
            _listInquiry = Value
        End Set
    End Property

    Public Property CGID() As String
        Get
            Return _cgid
        End Get
        Set(ByVal Value As String)
            _cgid = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return _collectortype
        End Get
        Set(ByVal Value As String)
            _collectortype = Value
        End Set
    End Property

    Public Property strKey() As String
        Get
            Return _strkey
        End Get
        Set(ByVal Value As String)
            _strkey = Value
        End Set
    End Property

End Class
