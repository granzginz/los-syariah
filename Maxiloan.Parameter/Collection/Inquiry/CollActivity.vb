

<Serializable()> _
Public Class CollActivity : Inherits Common
    Private _CGID As String
    Private _CGName As String
    Private _AgreementNo As String
    Private _ApplicationID As String
    Private _CustomerID As String
    Private _CustomerName As String
    Private _InstallmentNo As Int16
    Private _OverdueDays As Int32
    Private _CollectorID As String
    Private _CollectorName As String
    Private _CollectorType As String
    Private _ActivityID As String
    Private _ActivityDesc As String
    Private _ResultID As String
    Private _ResultDesc As String
    Private _PlanActivity As String
    Private _Plandate As Date
    Private _Activitydate As Date
    Private _PTPYDate As Date
    Private _Notes As String
    Private _ListData As DataTable
    Private _CollSeqNo As Int16
    Private _ListReport As DataSet

    Public Property CollSeqNo() As Int16
        Get
            Return _CollSeqNo
        End Get
        Set(ByVal Value As Int16)
            _CollSeqNo = Value
        End Set
    End Property

    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property

    Public Property CGName() As String
        Get
            Return _CGName
        End Get
        Set(ByVal Value As String)
            _CGName = Value
        End Set
    End Property


    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property


    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return _CustomerID
        End Get
        Set(ByVal Value As String)
            _CustomerID = Value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return _CustomerName
        End Get
        Set(ByVal Value As String)
            _CustomerName = Value
        End Set
    End Property

    Public Property InstallmentNo() As Int16
        Get
            Return _InstallmentNo
        End Get
        Set(ByVal Value As Int16)
            _InstallmentNo = Value
        End Set
    End Property


    Public Property OverdueDays() As Int32
        Get
            Return _OverdueDays
        End Get
        Set(ByVal Value As Int32)
            _OverdueDays = Value
        End Set
    End Property

    Public Property CollectorID() As String
        Get
            Return _CollectorID
        End Get
        Set(ByVal Value As String)
            _CollectorID = Value
        End Set
    End Property

    Public Property CollectorName() As String
        Get
            Return _CollectorName
        End Get
        Set(ByVal Value As String)
            _CollectorName = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return _CollectorType
        End Get
        Set(ByVal Value As String)
            _CollectorType = Value
        End Set
    End Property

    Public Property activityID() As String
        Get
            Return _ActivityID
        End Get
        Set(ByVal Value As String)
            _ActivityID = Value
        End Set
    End Property

    Public Property activityDesc() As String
        Get
            Return _ActivityDesc
        End Get
        Set(ByVal Value As String)
            _ActivityDesc = Value
        End Set
    End Property

    Public Property ResultID() As String
        Get
            Return _ResultID
        End Get
        Set(ByVal Value As String)
            _ResultID = Value
        End Set
    End Property

    Public Property ResultDesc() As String
        Get
            Return _ResultDesc
        End Get
        Set(ByVal Value As String)
            _ResultDesc = Value
        End Set
    End Property

    Public Property PlanActivity() As String
        Get
            Return _PlanActivity
        End Get
        Set(ByVal Value As String)
            _PlanActivity = Value
        End Set
    End Property

    Public Property PlanDate() As Date
        Get
            Return _Plandate
        End Get
        Set(ByVal Value As Date)
            _Plandate = Value
        End Set
    End Property

    Public Property ActivityDate() As Date
        Get
            Return _Activitydate
        End Get
        Set(ByVal Value As Date)
            _Activitydate = Value
        End Set
    End Property

    Public Property PTPYDate() As Date
        Get
            Return _PTPYDate
        End Get
        Set(ByVal Value As Date)
            _PTPYDate = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal Value As DataTable)
            _ListData = Value
        End Set
    End Property

    Public Property ListReport() As DataSet
        Get
            Return CType(_ListReport, DataSet)
        End Get
        Set(ByVal Value As DataSet)
            _ListReport = Value
        End Set
    End Property


End Class
