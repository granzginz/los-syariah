

<Serializable()> _
Public Class DeskCollReminderActivity : Inherits Maxiloan.Parameter.Common
    Private _listDeskColl As DataTable
    Private _AgreementNo As String
    Private _strKey As String
    Private _ApplicationID As String
    Private _ResultID As String
    Private _PlanDate As String
    Private _PTPYDate As String
    Private _PlanActivityID As String
    Private _ActionID As String
    Private _isRequestAssign As Boolean
    Private _Contact As String
    Private _hasil As Integer
    Private _DeskCollID As String
    Private _notes As String
    Private _listdata As New DataTable
    Public Collector As String

    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property

    Public Property DeskCollID() As String
        Get
            Return _DeskCollID
        End Get
        Set(ByVal Value As String)
            _DeskCollID = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property

    Public Property hasil() As Integer
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Integer)
            _hasil = Value
        End Set
    End Property

    Public Property ResultID() As String
        Get
            Return _ResultID
        End Get
        Set(ByVal Value As String)
            _ResultID = Value
        End Set
    End Property

    Public Property PlanDate() As String
        Get
            Return _PlanDate
        End Get
        Set(ByVal Value As String)
            _PlanDate = Value
        End Set
    End Property

    Public Property PTPYDate() As String
        Get
            Return _PTPYDate
        End Get
        Set(ByVal Value As String)
            _PTPYDate = Value
        End Set
    End Property

    Public Property PlanActivityID() As String
        Get
            Return _PlanActivityID
        End Get
        Set(ByVal Value As String)
            _PlanActivityID = Value
        End Set
    End Property

    Public Property ListDeskColl() As DataTable
        Get
            Return _listDeskColl
        End Get
        Set(ByVal Value As DataTable)
            _listDeskColl = Value
        End Set
    End Property

    Public Property ActionID() As String
        Get
            Return _ActionID
        End Get
        Set(ByVal Value As String)
            _ActionID = Value
        End Set
    End Property

    Public Property isRequestAssign() As Boolean
        Get
            Return _isRequestAssign
        End Get
        Set(ByVal Value As Boolean)
            _isRequestAssign = Value
        End Set
    End Property

    Public Property Contact() As String
        Get
            Return _Contact
        End Get
        Set(ByVal Value As String)
            _Contact = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property

    Public Property strKey() As String
        Get
            Return _strKey
        End Get
        Set(ByVal Value As String)
            _strKey = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

End Class
