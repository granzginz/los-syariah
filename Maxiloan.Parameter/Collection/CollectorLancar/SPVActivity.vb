
<Serializable()> _
Public Class SPVActivity : Inherits Common
    Private _WhereAgreementNo As String
    Private _agreementno As String
    Private _CGID As String
    Private _CollectorID As String
    Private _CollectorName As String
    Private _CollectorRequest As String
    Private _CustomerID As String
    Private _Customername As String
    Private _casesID As String
    Private _casesDesc As String
    Private _FixedStatus As String
    Private _DaysOverdue As Integer
    Private _listdata As DataTable
    Private _Action As String
    Private _PlanDateRAL As Date

    Private _CollectorType As String


    Public Property PlanDateRAL() As Date
        Get
            Return _PlanDateRAL
        End Get
        Set(ByVal Value As Date)
            _PlanDateRAL = Value
        End Set
    End Property


    Public Property Action() As String
        Get
            Return _Action
        End Get
        Set(ByVal Value As String)
            _Action = Value
        End Set
    End Property


    Public Property FixedStatus() As String
        Get
            Return _FixedStatus
        End Get
        Set(ByVal Value As String)
            _FixedStatus = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return _CollectorType
        End Get
        Set(ByVal Value As String)
            _CollectorType = Value
        End Set
    End Property


    Public Property listdata() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property WhereAgreementNo() As String
        Get
            Return _WhereAgreementNo
        End Get
        Set(ByVal Value As String)
            _WhereAgreementNo = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return _agreementno
        End Get
        Set(ByVal Value As String)
            _agreementno = Value
        End Set
    End Property

    Public Property CollectorID() As String
        Get
            Return _CollectorID
        End Get
        Set(ByVal Value As String)
            _CollectorID = Value
        End Set
    End Property

    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property

    Public Property CollectorName() As String
        Get
            Return _CollectorName
        End Get
        Set(ByVal Value As String)
            _CollectorName = Value
        End Set
    End Property

    Public Property CollectorRequest() As String
        Get
            Return _CollectorRequest
        End Get
        Set(ByVal Value As String)
            _CollectorRequest = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return _CustomerID
        End Get
        Set(ByVal Value As String)
            _CustomerID = Value
        End Set
    End Property

    Public Property casesID() As String
        Get
            Return _casesID
        End Get
        Set(ByVal Value As String)
            _casesID = Value
        End Set
    End Property

    Public Property casesDesc() As String
        Get
            Return _casesDesc
        End Get
        Set(ByVal Value As String)
            _casesDesc = Value
        End Set
    End Property

    Public Property DaysOverdue() As Integer
        Get
            Return _DaysOverdue
        End Get
        Set(ByVal Value As Integer)
            _DaysOverdue = Value
        End Set
    End Property


End Class
