
<Serializable()> _
Public Class CollLetterOnReq : Inherits Common
    Private _listdata As DataTable
    Private _listreport As DataSet
    Private _listcombo As DataTable


    Private _letterid As String
    Private _lettername As String
    Private _lettertemplate As String

    Private _agreementno As String
    Private _applicationId As String
    Private _customername As String

    Private _Asset As String
    Private _installmentNo As String
    Private _installmentamount As Double
    Private _installmentdate As String
    Private _osbalance As Double
    Private _totaltagname As Integer
    Private _texttoprint As String
    Public Property TextToPrint() As String
        Get
            Return _texttoprint
        End Get
        Set(ByVal Value As String)
            _texttoprint = Value
        End Set
    End Property

    Public Property TotalTagName() As Integer
        Get
            Return _totaltagname
        End Get
        Set(ByVal Value As Integer)
            _totaltagname = Value
        End Set
    End Property
    Public Property LetterName() As String
        Get
            Return _lettername
        End Get
        Set(ByVal Value As String)
            _lettername = Value
        End Set
    End Property
    Public Property LetterTemplate() As String
        Get
            Return _lettertemplate
        End Get
        Set(ByVal Value As String)
            _lettertemplate = Value
        End Set
    End Property
    Public Property ListCombo() As DataTable
        Get
            Return _listcombo
        End Get
        Set(ByVal Value As DataTable)
            _listcombo = Value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return _customername
        End Get
        Set(ByVal Value As String)
            _customername = Value
        End Set
    End Property
    Public Property Asset() As String
        Get
            Return _Asset
        End Get
        Set(ByVal Value As String)
            _Asset = Value
        End Set
    End Property
    Public Property InstallmentNo() As String
        Get
            Return _installmentNo
        End Get
        Set(ByVal Value As String)
            _installmentNo = Value
        End Set
    End Property
    Public Property InstallMentAmount() As Double
        Get
            Return _installmentamount
        End Get
        Set(ByVal Value As Double)
            _installmentamount = Value
        End Set
    End Property
    Public Property InstallmentDate() As String
        Get
            Return _installmentdate
        End Get
        Set(ByVal Value As String)
            _installmentdate = Value
        End Set
    End Property
    Public Property OsBalance() As Double
        Get
            Return _osbalance
        End Get
        Set(ByVal Value As Double)
            _osbalance = Value
        End Set
    End Property

    Public Property Applicationid() As String
        Get
            Return _applicationId
        End Get
        Set(ByVal Value As String)
            _applicationId = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return _agreementno
        End Get
        Set(ByVal Value As String)
            _agreementno = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property ListReport() As DataSet
        Get
            Return _listreport
        End Get
        Set(ByVal Value As DataSet)
            _listreport = Value
        End Set
    End Property
    Public Property LetterId() As String
        Get
            Return _letterid
        End Get
        Set(ByVal Value As String)
            _letterid = Value
        End Set
    End Property

End Class
