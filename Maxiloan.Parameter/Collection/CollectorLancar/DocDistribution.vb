


<Serializable()> _
Public Class DocDistribution : Inherits Maxiloan.Parameter.Common

    Private _strKey As String
    Private _CGID As String
    Private _listDistribution As DataTable
    Private _collectortype As String
    Private _AgreementNo As String
    Private _listView As DataTable
    Private _hasil As Integer
    Private _listReport As DataSet
    Private _nextBD As Date
    Private _applicationid As String
    Private _approveby As String
    Private _flag As Integer
    Private _accruedInterest As Integer
    Private _AgreementSentDate As Date
    Private _SlipSetoranDate As Date
    Private _Insurancedate As Date
    Private _AgreementReceive As String
    Private _SlipSetoranReceive As String
    Private _InsuranceReceive As String
    Private _WelcomeLetter As String
    Private _WelcomeLetterReceiveBy As String
    Private _CardInstallment As String
    Private _CardInstallmentReceiveBy As String
    Private _Courier As String

    Public Property SlipSetoranReceive() As String

        Get
            Return _SlipSetoranReceive
        End Get
        Set(ByVal Value As String)
            _SlipSetoranReceive = Value
        End Set
    End Property
    Public Property InsuranceReceive() As String

        Get
            Return _InsuranceReceive
        End Get
        Set(ByVal Value As String)
            _InsuranceReceive = Value
        End Set
    End Property
    Public Property AgreementReceive() As String

        Get
            Return _AgreementReceive
        End Get
        Set(ByVal Value As String)
            _AgreementReceive = Value
        End Set
    End Property

    Public Property AgreementSentDate() As Date
        Get
            Return _AgreementSentDate
        End Get
        Set(ByVal Value As Date)
            _AgreementSentDate = Value
        End Set
    End Property
    Public Property SlipSetoranDate() As Date
        Get
            Return _SlipSetoranDate
        End Get
        Set(ByVal Value As Date)
            _SlipSetoranDate = Value
        End Set
    End Property
    Public Property Insurancedate() As Date
        Get
            Return _Insurancedate
        End Get
        Set(ByVal Value As Date)
            _Insurancedate = Value
        End Set
    End Property

    Public Property accruedInterest() As Integer

        Get
            Return _accruedInterest
        End Get
        Set(ByVal Value As Integer)
            _accruedInterest = Value
        End Set
    End Property
    Public Property flag() As Integer

        Get
            Return _flag
        End Get
        Set(ByVal Value As Integer)
            _flag = Value
        End Set
    End Property
    Public Property approveby() As String

        Get
            Return _approveby
        End Get
        Set(ByVal Value As String)
            _approveby = Value
        End Set
    End Property



    Public Property ApplicationID() As String
        Get
            Return _applicationid
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property

    Public Property NextBD() As Date
        Get
            Return _nextBD
        End Get
        Set(ByVal Value As Date)
            _nextBD = Value
        End Set
    End Property


    Public Property ListReport() As DataSet
        Get
            Return _listReport
        End Get
        Set(ByVal Value As DataSet)
            _listReport = Value
        End Set
    End Property

    Public Property hasil() As Integer
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Integer)
            _hasil = Value
        End Set
    End Property


    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property

    Public Property strKey() As String
        Get
            Return _strKey
        End Get
        Set(ByVal Value As String)
            _strKey = Value
        End Set
    End Property

    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property

    Public Property ListView() As DataTable
        Get
            Return _listView
        End Get
        Set(ByVal Value As DataTable)
            _listView = Value
        End Set
    End Property
    Public Property ListDistribution() As DataTable
        Get
            Return _listDistribution
        End Get
        Set(ByVal Value As DataTable)
            _listDistribution = Value
        End Set
    End Property


    Public Property CollectorType() As String
        Get
            Return _collectortype
        End Get
        Set(ByVal Value As String)
            _collectortype = Value
        End Set
    End Property

    Public Property WelcomeLetter() As Date
        Get
            Return _WelcomeLetter
        End Get
        Set(ByVal Value As Date)
            _WelcomeLetter = Value
        End Set
    End Property

    Public Property WelcomeLetterReceiveBy() As String
        Get
            Return _WelcomeLetterReceiveBy
        End Get
        Set(ByVal Value As String)
            _WelcomeLetterReceiveBy = Value
        End Set
    End Property

    Public Property CardInstallment() As Date
        Get
            Return _CardInstallment
        End Get
        Set(ByVal Value As Date)
            _CardInstallment = Value
        End Set
    End Property

    Public Property CardInstallmentReceiveBy() As String
        Get
            Return _CardInstallmentReceiveBy
        End Get
        Set(ByVal Value As String)
            _CardInstallmentReceiveBy = Value
        End Set
    End Property

    Public Property Courier() As String
        Get
            Return _Courier
        End Get
        Set(ByVal Value As String)
            _Courier = Value
        End Set
    End Property
End Class