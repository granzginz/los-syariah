


<Serializable()> _
Public Class ReceiptNotesOnRequest : Inherits Maxiloan.Parameter.Common

    Private _strKey As String
    Private _CGID As String
    Private _listRAL As DataTable
    Private _collectortype As String
    Private _AgreementNo As String
    Private _RALNo As String
    Private _executorid As String
    Private _hasil As Integer
    Private _listReport As DataSet
    Private _nextBD As Date
    Private _applicationid As String
    Private _nextInstallment As Integer
    Private _daysoverdue As Integer
    Private _receiptdates As Date

    Public Property ApplicationID() As String
        Get
            Return _applicationid
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property

    Public Property NextBD() As Date
        Get
            Return _nextBD
        End Get
        Set(ByVal Value As Date)
            _nextBD = Value
        End Set
    End Property
    Public Property receiptdates() As Date
        Get
            Return _receiptdates
        End Get
        Set(ByVal Value As Date)
            _receiptdates = Value
        End Set
    End Property

    Public Property ListReport() As DataSet
        Get
            Return _listReport
        End Get
        Set(ByVal Value As DataSet)
            _listReport = Value
        End Set
    End Property
    Public Property daysoverdue() As Integer
        Get
            Return _daysoverdue
        End Get
        Set(ByVal Value As Integer)
            _daysoverdue = Value
        End Set
    End Property
    Public Property hasil() As Integer
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Integer)
            _hasil = Value
        End Set
    End Property
    Public Property ExecutorID() As String
        Get
            Return _executorid
        End Get
        Set(ByVal Value As String)
            _executorid = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property
    Public Property NextInstallment() As Integer
        Get
            Return _nextInstallment
        End Get
        Set(ByVal Value As Integer)
            _nextInstallment = Value
        End Set
    End Property

    Public Property RALNo() As String
        Get
            Return _RALNo
        End Get
        Set(ByVal Value As String)
            _RALNo = Value
        End Set
    End Property

    Public Property strKey() As String
        Get
            Return _strKey
        End Get
        Set(ByVal Value As String)
            _strKey = Value
        End Set
    End Property

    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property

    Public Property ListRAL() As DataTable
        Get
            Return _listRAL
        End Get
        Set(ByVal Value As DataTable)
            _listRAL = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return _collectortype
        End Get
        Set(ByVal Value As String)
            _collectortype = Value
        End Set
    End Property
End Class