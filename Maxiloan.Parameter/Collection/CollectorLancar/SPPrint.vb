
<Serializable()> _
Public Class SPPrint : Inherits Maxiloan.Parameter.Common
    Private _WhereAgreementNo As String
    Private _CGID As String
    Private _ListSPPrint As DataTable
    Private _ListSSPrint As DataTable
    Private _strKey As String
    Private _collectortype As String
    Private _listReport As DataSet
    Private _hasil As Integer
    Private _ListRubrikPrint As DataTable

    Public Property hasil() As Integer
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Integer)
            _hasil = Value
        End Set
    End Property

    Public Property WhereAgreementNo() As String
        Get
            Return _WhereAgreementNo
        End Get
        Set(ByVal Value As String)
            _WhereAgreementNo = Value
        End Set
    End Property
    Public Property strKey() As String
        Get
            Return _strKey
        End Get
        Set(ByVal Value As String)
            _strKey = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return _collectortype
        End Get
        Set(ByVal Value As String)
            _collectortype = Value
        End Set
    End Property
    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property

    Public Property ListSPPrint() As DataTable
        Get
            Return _ListSPPrint
        End Get
        Set(ByVal Value As DataTable)
            _ListSPPrint = Value
        End Set
    End Property
    Public Property ListSSPrint() As DataTable
        Get
            Return _ListSSPrint
        End Get
        Set(ByVal Value As DataTable)
            _ListSSPrint = Value
        End Set
    End Property

    Public Property ListReport() As DataSet
        Get
            Return _listReport
        End Get
        Set(ByVal Value As DataSet)
            _listReport = Value
        End Set
    End Property

    Public Property ListRubrikPrint() As DataTable
        Get
            Return _ListRubrikPrint
        End Get
        Set(ByVal Value As DataTable)
            _ListRubrikPrint = Value
        End Set
    End Property
    Public Property CGIDParent As String
    Public Property author1 As String
    Public Property Rubrik1 As String
    Public Property CollectorID As String
    Public Property ApplicationID As String
    Public Property produk As String
    Public Property ApplicationModule As String
End Class
