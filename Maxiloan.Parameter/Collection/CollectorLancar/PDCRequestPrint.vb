

<Serializable()> _
Public Class PDCRequestPrint : Inherits Maxiloan.Parameter.Common
    Private _strKey As String
    Private _collectortype As String
    Private _cgid As String
    Private _listPDC As DataTable
    Private _listreport As DataSet
    Private _nextinstallmentno As Integer
    Private _agreementno As String
    Private _letter As String
    Private _LetterDate As String
    Private _hasil As Integer

    Public Property hasil() As Integer
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Integer)
            _hasil = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return _agreementno
        End Get
        Set(ByVal Value As String)
            _agreementno = Value
        End Set
    End Property

    Public Property Letter() As String
        Get
            Return _letter
        End Get
        Set(ByVal Value As String)
            _letter = Value
        End Set
    End Property
    Public Property LetterDate() As String
        Get
            Return _LetterDate
        End Get
        Set(ByVal Value As String)
            _LetterDate = Value
        End Set
    End Property

    Public Property NextInstallmentNo() As Integer
        Get
            Return _nextinstallmentno
        End Get
        Set(ByVal Value As Integer)
            _nextinstallmentno = Value
        End Set
    End Property

    Public Property ListReport() As DataSet
        Get
            Return _listreport
        End Get
        Set(ByVal Value As DataSet)
            _listreport = Value
        End Set
    End Property

    Public Property ListPDC() As DataTable
        Get
            Return _listPDC
        End Get
        Set(ByVal Value As DataTable)
            _listPDC = Value
        End Set
    End Property

    Public Property CGID() As String
        Get
            Return _cgid
        End Get
        Set(ByVal Value As String)
            _cgid = Value
        End Set
    End Property

    Public Property strKey() As String
        Get
            Return _strKey
        End Get
        Set(ByVal Value As String)
            _strKey = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return _collectortype
        End Get
        Set(ByVal Value As String)
            _collectortype = Value
        End Set
    End Property

End Class
