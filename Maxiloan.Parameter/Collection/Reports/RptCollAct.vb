


<Serializable()> _
Public Class RptCollAct : Inherits Maxiloan.Parameter.Common
    Private _strKey As String
    Private oDataCollector As DataTable
    Private _listReport As DataSet
    Private _collectorType As String
    Private _CGID As String

    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property

    Public Property strKey() As String
        Get
            Return _strKey
        End Get
        Set(ByVal Value As String)
            _strKey = Value
        End Set
    End Property

    Public Property ListCollector() As DataTable
        Get
            Return oDataCollector
        End Get
        Set(ByVal Value As DataTable)
            oDataCollector = Value
        End Set
    End Property

    Public Property ListReport() As DataSet
        Get
            Return _listReport
        End Get
        Set(ByVal Value As DataSet)
            _listReport = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return _collectorType
        End Get
        Set(ByVal Value As String)
            _collectorType = Value
        End Set
    End Property
End Class
