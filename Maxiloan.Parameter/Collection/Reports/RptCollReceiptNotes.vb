

<Serializable()> _
Public Class RptCollReceiptNotes : Inherits Maxiloan.Parameter.Common
    Private _strKey As String
    Private oDataCollector As DataTable
    Private _listReport As DataTable
    Private _collectorType As String
    Private _CGID As String
    Private _ReceiptNotes As String
    Private _CollectorID As String
    Private _ReceiptPeriodeFrom As String
    Private _ReceiptPeriodeTo As String
    Private _IsPrint As Boolean
    Private _OutStandingReport As DataSet



    Public Property CollectorID() As String
        Get
            Return _CollectorID
        End Get
        Set(ByVal Value As String)
            _CollectorID = Value
        End Set
    End Property


    Public Property ReceiptNotes() As String
        Get
            Return _ReceiptNotes
        End Get
        Set(ByVal Value As String)
            _ReceiptNotes = Value
        End Set
    End Property

    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property

    Public Property strKey() As String
        Get
            Return _strKey
        End Get
        Set(ByVal Value As String)
            _strKey = Value
        End Set
    End Property

    Public Property ListCollector() As DataTable
        Get
            Return oDataCollector
        End Get
        Set(ByVal Value As DataTable)
            oDataCollector = Value
        End Set
    End Property

    Public Property ListReport() As DataTable
        Get
            Return _listReport
        End Get
        Set(ByVal Value As DataTable)
            _listReport = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return _collectorType
        End Get
        Set(ByVal Value As String)
            _collectorType = Value
        End Set
    End Property
    Public Property ReceiptPeriodeFrom() As String
        Get
            Return _ReceiptPeriodeFrom
        End Get
        Set(ByVal Value As String)
            _ReceiptPeriodeFrom = Value
        End Set
    End Property
    Public Property ReceiptPeriodeTo() As String
        Get
            Return _ReceiptPeriodeTo
        End Get
        Set(ByVal Value As String)
            _ReceiptPeriodeTo = Value
        End Set
    End Property
    Public Property IsPrint() As Boolean
        Get
            Return _IsPrint
        End Get
        Set(ByVal Value As Boolean)
            _IsPrint = Value
        End Set
    End Property
    Public Property OutStandingReport() As DataSet
        Get
            Return _OutStandingReport
        End Get
        Set(ByVal Value As DataSet)
            _OutStandingReport = Value
        End Set
    End Property
End Class
