
<Serializable()> _
Public Class ReportCreditDeliquency : Inherits Maxiloan.Parameter.Common
    Private _listdata As DataTable
    Private _cgid As String
    Private _strkey As String
    Private _collectortype As String
    Private _listreport As DataSet

    Public Property ListReport() As DataSet
        Get
            Return _listreport
        End Get
        Set(ByVal Value As DataSet)
            _listreport = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return _collectortype
        End Get
        Set(ByVal Value As String)
            _collectortype = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property CGID() As String
        Get
            Return _cgid
        End Get
        Set(ByVal Value As String)
            _cgid = Value
        End Set
    End Property

    Public Property strKey() As String
        Get
            Return _strkey
        End Get
        Set(ByVal Value As String)
            _strkey = Value
        End Set
    End Property

End Class
