
<Serializable()> _
Public Class AdminActivity : Inherits Parameter.Common
    Private _listCLActivity As DataTable
    Private _CGID As String
    Private _AgreementNo As String
    Private _strKey As String
    Private _ApplicationID As String
    Private _Contact As String
    Private _isRequest As Boolean
    Private _planDate As String
    Private _PTPYDate As String
    Private _resultID As String
    Private _activityID As String
    Private _planActivityID As String
    Private _notes As String
    Private _CollectorID As String
    Private _hasil As Integer

    Public Property Hasil() As Integer
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Integer)
            _hasil = Value
        End Set
    End Property

    Public Property CollectorID() As String
        Get
            Return _CollectorID
        End Get
        Set(ByVal Value As String)
            _CollectorID = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property

    Public Property PlanActivityID() As String
        Get
            Return _planActivityID
        End Get
        Set(ByVal Value As String)
            _planActivityID = Value
        End Set
    End Property

    Public Property ActivityID() As String
        Get
            Return _activityID
        End Get
        Set(ByVal Value As String)
            _activityID = Value
        End Set
    End Property

    Public Property ResultID() As String
        Get
            Return _resultID
        End Get
        Set(ByVal Value As String)
            _resultID = Value
        End Set
    End Property

    Public Property PTPYDate() As String
        Get
            Return _PTPYDate
        End Get
        Set(ByVal Value As String)
            _PTPYDate = Value
        End Set
    End Property

    Public Property PlanDate() As String
        Get
            Return _planDate
        End Get
        Set(ByVal Value As String)
            _planDate = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property

    Public Property Contact() As String
        Get
            Return _Contact
        End Get
        Set(ByVal Value As String)
            _Contact = Value
        End Set
    End Property

    Public Property isRequest() As Boolean
        Get
            Return _isRequest
        End Get
        Set(ByVal Value As Boolean)
            _isRequest = Value
        End Set
    End Property
    Public Property ListCLActivity() As DataTable
        Get
            Return _listCLActivity
        End Get
        Set(ByVal Value As DataTable)
            _listCLActivity = Value
        End Set
    End Property

    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property

    Public Property strKey() As String
        Get
            Return _strKey
        End Get
        Set(ByVal Value As String)
            _strKey = Value
        End Set
    End Property
End Class
