﻿
<Serializable()>
Public Class AgunanLain : Inherits Common
	Private _ID As String
	Private _ApplicationID As String
    Private _AgreementNo As String
    Private _JenisCollateral As String
    Private _Description As String
    Private _Initial As String
    Private _Status As Boolean
    Private _totalrecords As Int64
    Private _MitraID As String
    Private _MitraFullName As String
    Private _MitraShortName As String
    Private _MitraInitialName As String
    Private _listData As DataTable
    Private _TotRec As Integer

    Private _Phone1 As String
    Private _Phone2 As String
    Private _Fax As String
    Private _WebSite As String
    Private _Notes As String
    Private _Provinsi As String
    Private _JoinDate As Date
    Private _BankID As String
    Private _BranchCode As String
    Private _AccountName As String
    Private _AccountNo As String
    Private _AccountType As String
	Private _CollateralAmount As String

	Private _MFFacilityNo As String
	Private _Name As String
	Private _TglKontrak As Date
	Private _Objek As String
	Private _NoInvoice As String
	Private _TglInvoice As Date
	Private _PenerbitInvoice As String
	Private _PenerimaInvoice As String
	Private _NilaiInvoice As Double

	Private _NoRangka As String
	Private _NoMesin As String
	Private _NamaDiBPKB As String
	Private _Merk As String
	Private _Tipe As String
	Private _Warna As String
	Private _NamaDealer As String
	Private _AlamatDealer As String
	Private _TelponDealer As String
	Private _JTBPKB As Date
	Private _TglTerimaBPKB As Date
	Private _NoBPKB As String
	Private _TglBPKB As Date
	Private _NoPolisi As String
	Private _NoFaktur As String
	Private _NoNIK As String
	Private _NoFormA As String
	Private _AktaFidusia As String
	Private _SertifikatFidusia As String
	Private _TglKeluarBPKBSementara As Date
	Private _Keterangan As String
	Private _TglClose As Date

	Private _CustomerID As String
    Private _CollateralTypeID As String
    Private _CollateralDescription As String
    Private _CollateralID As String
    Private _CollateralStatus As String
    Private _Currency As String
    Private _CollateralSeqNo As Integer
    Private _PemilikCollateral As String
    Private _AlamatPemilikCollateral As String
    Private _StatusSertifikat As String
    Private _JenisSertifikat As String
    Private _ThnPenerbitanSertifikat As Date
    Private _NoSertifikat As String
    Private _NoGambarSituasi As String
    Private _Developer As String
    Private _LuasTanah As String
    Private _LuasBangunan As String
    Private _ProgresKonstruksi As String
    Private _TglUpdKonstruksi As Date
    Private _Kecamatan As String
    Private _Desa As String
    Private _Alamat As String
    Private _SandiLokasi As String
    Private _MortgageType As String
    Private _PropertyType As String
    Private _NilaiPasarDiakui As Decimal
    Private _TglPenilaian As Date
    Private _NilaiPledging As Decimal
    Private _TglupdAgunan As Date
    Private _PIBDTglPenilaian As Date
    Private _PIBDNilaiTanah As Decimal
    Private _PIBDNilaiBangunan As Decimal
    Private _PIBDNilaiTanahBangunan As Decimal
    Private _PIBFTglPenilaian As Date
    Private _PIBFNilaiTanah As Decimal
    Private _PIBFNilaiBangunan As Decimal
    Private _PIBFNilaiTanahBangunan As Decimal
    Private _PEBDTglPenilaian As Date
    Private _PEBDNilaiTanah As Decimal
    Private _PEBDNilaiBangunan As Decimal
    Private _PEBDNilaiTanahBangunan As Decimal
    Private _PEBFTglPenilaian As Date
    Private _PEBFNilaiTanah As Decimal
    Private _PEBFNilaiBangunan As Decimal
    Private _PEBFNilaiTanahBangunan As Decimal
    Private _PJNoSKMHT As String
    Private _PJTglSKMHT As Date
    Private _PJTglJatuhTempoSKMHT As Date
    Private _PJNoAPHT As String
    Private _PJTglAPHT As Date
    Private _PJNoSHT As String
    Private _PJTglIssuedSHT As Date
    Private _PJTotalNilaiHT As Decimal
    Private _PJRangkingHT As String
    Private _PJJenisPengikatan As String
    Private _PJNotaris As String
	Private _PJLokasiPenyimpananDokumen As String
	Private _PJKantorBPNWilayah As String
	Private _BgnFisikJaminan As String
	Private _BgnSuratIzin As String
	Private _BgnNoSuratIzin As String
	Private _BgnJenisSuratIzin As String
	Private _BgnPeruntukanBangunan As String
	Private _BgnNoIzinLayakHuni As String
	Private _BgnPBBTahunTerakhir As String
	Private _BgnNilaiNJOP As Decimal
	Private _BgnCetakBiru As String
	Private _BgnAktaBalikNama As String
	Private _BgnNoAkta As String
	Private _BgnTglAkta As Date
	Private _BgnNotaris As String
	Private _BgnParipasu As Integer
	Private _BgnBangunanDiasuransikan As String
	Private _PledgingPercentage As Integer
	Private _PledgingAmount As Decimal
	Private _PagingTable As DataTable
	Public Property Tabels As DataSet

	Public Property ApplicationID() As String
		Get
			Return _ApplicationID
		End Get
		Set(ByVal Value As String)
			_ApplicationID = Value
		End Set
	End Property

	Public Property AgreementNo() As String
		Get
			Return _AgreementNo
		End Get
		Set(ByVal Value As String)
			_AgreementNo = Value
		End Set
	End Property

	Public Property JenisCollateral() As String
		Get
			Return _JenisCollateral
		End Get
		Set(ByVal Value As String)
			_JenisCollateral = Value
		End Set
	End Property

	Public Property Description() As String
		Get
			Return _Description
		End Get
		Set(ByVal Value As String)
			_Description = Value
		End Set
	End Property

	Public Property Initial() As String
		Get
			Return _Initial
		End Get
		Set(ByVal Value As String)
			_Initial = Value
		End Set
	End Property

	Public Property Status() As Boolean
		Get
			Return _Status
		End Get
		Set(ByVal Value As Boolean)
			_Status = Value
		End Set
	End Property

	Public Property totalrecords() As Int64
		Get
			Return _totalrecords
		End Get
		Set(ByVal Value As Int64)
			_totalrecords = Value
		End Set
	End Property

	Public Property MitraID() As String
		Get
			Return _MitraID
		End Get
		Set(ByVal Value As String)
			_MitraID = Value
		End Set
	End Property
	Public Property MitraFullName() As String
		Get
			Return _MitraFullName
		End Get
		Set(ByVal Value As String)
			_MitraFullName = Value
		End Set
	End Property

	Public Property MitraShortName() As String
		Get
			Return _MitraShortName
		End Get
		Set(ByVal Value As String)
			_MitraShortName = Value
		End Set
	End Property

	Public Property MitraInitialName() As String
		Get
			Return _MitraInitialName
		End Get
		Set(ByVal Value As String)
			_MitraInitialName = Value
		End Set
	End Property
	Public Property Phone1() As String
		Get
			Return _Phone1
		End Get
		Set(ByVal Value As String)
			_Phone1 = Value
		End Set
	End Property
	Public Property Phone2() As String
		Get
			Return _Phone2
		End Get
		Set(ByVal Value As String)
			_Phone2 = Value
		End Set
	End Property

	Public Property Fax() As String
		Get
			Return _Fax
		End Get
		Set(ByVal Value As String)
			_Fax = Value
		End Set
	End Property
	Public Property WebSite() As String
		Get
			Return _WebSite
		End Get
		Set(ByVal Value As String)
			_WebSite = Value
		End Set
	End Property
	Public Property Notes() As String
		Get
			Return _Notes
		End Get
		Set(ByVal Value As String)
			_Notes = Value
		End Set
	End Property
	Public Property Provinsi() As String
		Get
			Return _Provinsi
		End Get
		Set(ByVal Value As String)
			_Provinsi = Value
		End Set
	End Property
	Public Property JoinDate() As Date
		Get
			Return _JoinDate
		End Get
		Set(ByVal Value As Date)
			_JoinDate = Value
		End Set
	End Property
	Public Property ListData() As DataTable
		Get
			Return _listData
		End Get
		Set(ByVal Value As DataTable)
			_listData = Value
		End Set
	End Property
	Public Property TotRec() As Integer
		Get
			Return _TotRec
		End Get
		Set(ByVal Value As Integer)
			_TotRec = Value
		End Set
	End Property
	Public Property BankID() As String
		Get
			Return _BankID
		End Get
		Set(ByVal Value As String)
			_BankID = Value
		End Set
	End Property
	Public Property BranchCode() As String
		Get
			Return _BranchCode
		End Get
		Set(ByVal Value As String)
			_BranchCode = Value
		End Set
	End Property
	Public Property AccountName() As String
		Get
			Return _AccountName
		End Get
		Set(ByVal Value As String)
			_AccountName = Value
		End Set
	End Property
	Public Property AccountNo() As String
		Get
			Return _AccountNo
		End Get
		Set(ByVal Value As String)
			_AccountNo = Value
		End Set
	End Property
	Public Property AccountType() As String
		Get
			Return _AccountType
		End Get
		Set(ByVal Value As String)
			_AccountType = Value
		End Set
	End Property

	Public Property MFFacilityNo() As String
		Get
			Return _MFFacilityNo
		End Get
		Set(value As String)
			_MFFacilityNo = value
		End Set
	End Property

	Public Property CustomerID() As String
		Get
			Return _CustomerID
		End Get
		Set(ByVal Value As String)
			_CustomerID = Value
		End Set
	End Property

	Public Property CollateralTypeID() As String
		Get
			Return _CollateralTypeID
		End Get
		Set(ByVal Value As String)
			_CollateralTypeID = Value
		End Set
	End Property

	Public Property CollateralDescription() As String
		Get
			Return _CollateralDescription
		End Get
		Set(ByVal Value As String)
			_CollateralDescription = Value
		End Set
	End Property

	Public Property CollateralID() As String
		Get
			Return _CollateralID
		End Get
		Set(ByVal Value As String)
			_CollateralID = Value
		End Set
	End Property

	Public Property CollateralStatus() As String
		Get
			Return _CollateralStatus
		End Get
		Set(ByVal Value As String)
			_CollateralStatus = Value
		End Set
	End Property

	Public Property Currency() As String
		Get
			Return _Currency
		End Get
		Set(ByVal Value As String)
			_Currency = Value
		End Set
	End Property

	Public Property CollateralSeqNo() As Integer
		Get
			Return _CollateralSeqNo
		End Get
		Set(ByVal Value As Integer)
			_CollateralSeqNo = Value
		End Set
	End Property

	Public Property PemilikCollateral() As String
		Get
			Return _PemilikCollateral
		End Get
		Set(ByVal Value As String)
			_PemilikCollateral = Value
		End Set
	End Property

	Public Property AlamatPemilikCollateral() As String
		Get
			Return _AlamatPemilikCollateral
		End Get
		Set(ByVal Value As String)
			_AlamatPemilikCollateral = Value
		End Set
	End Property

	Public Property StatusSertifikat() As String
		Get
			Return _StatusSertifikat
		End Get
		Set(ByVal Value As String)
			_StatusSertifikat = Value
		End Set
	End Property

	Public Property JenisSertifikat() As String
		Get
			Return _JenisSertifikat
		End Get
		Set(ByVal Value As String)
			_JenisSertifikat = Value
		End Set
	End Property

	Public Property ThnPenerbitanSertifikat() As Date
		Get
			Return _ThnPenerbitanSertifikat
		End Get
		Set(ByVal Value As Date)
			_ThnPenerbitanSertifikat = Value
		End Set
	End Property

	Public Property NoSertifikat() As String
		Get
			Return _NoSertifikat
		End Get
		Set(ByVal Value As String)
			_NoSertifikat = Value
		End Set
	End Property

	Public Property NoGambarSituasi() As String
		Get
			Return _NoGambarSituasi
		End Get
		Set(ByVal Value As String)
			_NoGambarSituasi = Value
		End Set
	End Property

	Public Property Developer() As String
		Get
			Return _Developer
		End Get
		Set(ByVal Value As String)
			_Developer = Value
		End Set
	End Property

	Public Property LuasTanah() As String
		Get
			Return _LuasTanah
		End Get
		Set(ByVal Value As String)
			_LuasTanah = Value
		End Set
	End Property

	Public Property LuasBangunan() As String
		Get
			Return _LuasBangunan
		End Get
		Set(ByVal Value As String)
			_LuasBangunan = Value
		End Set
	End Property

	Public Property ProgresKonstruksi() As String
		Get
			Return _ProgresKonstruksi
		End Get
		Set(ByVal Value As String)
			_ProgresKonstruksi = Value
		End Set
	End Property

	Public Property TglUpdKonstruksi() As Date
		Get
			Return _TglUpdKonstruksi
		End Get
		Set(ByVal Value As Date)
			_TglUpdKonstruksi = Value
		End Set
	End Property

	Public Property Kecamatan() As String
		Get
			Return _Kecamatan
		End Get
		Set(ByVal Value As String)
			_Kecamatan = Value
		End Set
	End Property

	Public Property Desa() As String
		Get
			Return _Desa
		End Get
		Set(ByVal Value As String)
			_Desa = Value
		End Set
	End Property

	Public Property Alamat() As String
		Get
			Return _Alamat
		End Get
		Set(ByVal Value As String)
			_Alamat = Value
		End Set
	End Property

	Public Property SandiLokasi() As String
		Get
			Return _SandiLokasi
		End Get
		Set(ByVal Value As String)
			_SandiLokasi = Value
		End Set
	End Property

	Public Property MortgageType() As String
		Get
			Return _MortgageType
		End Get
		Set(ByVal Value As String)
			_MortgageType = Value
		End Set
	End Property

	Public Property PropertyType() As String
		Get
			Return _PropertyType
		End Get
		Set(ByVal Value As String)
			_PropertyType = Value
		End Set
	End Property

	Public Property NilaiPasarDiakui() As Decimal
		Get
			Return _NilaiPasarDiakui
		End Get
		Set(ByVal Value As Decimal)
			_NilaiPasarDiakui = Value
		End Set
	End Property

	Public Property TglPenilaian() As Date
		Get
			Return _TglPenilaian
		End Get
		Set(ByVal Value As Date)
			_TglPenilaian = Value
		End Set
	End Property

	Public Property NilaiPledging() As Decimal
		Get
			Return _NilaiPledging
		End Get
		Set(ByVal Value As Decimal)
			_NilaiPledging = Value
		End Set
	End Property

	Public Property TglupdAgunan() As Date
		Get
			Return _TglupdAgunan
		End Get
		Set(ByVal Value As Date)
			_TglupdAgunan = Value
		End Set
	End Property

	Public Property PIBDTglPenilaian() As Date
		Get
			Return _PIBDTglPenilaian
		End Get
		Set(ByVal Value As Date)
			_PIBDTglPenilaian = Value
		End Set
	End Property

	Public Property PIBDNilaiTanah() As Decimal
		Get
			Return _PIBDNilaiTanah
		End Get
		Set(ByVal Value As Decimal)
			_PIBDNilaiTanah = Value
		End Set
	End Property

	Public Property PIBDNilaiBangunan() As Decimal
		Get
			Return _PIBDNilaiBangunan
		End Get
		Set(ByVal Value As Decimal)
			_PIBDNilaiBangunan = Value
		End Set
	End Property

	Public Property PIBDNilaiTanahBangunan() As Decimal
		Get
			Return _PIBDNilaiTanahBangunan
		End Get
		Set(ByVal Value As Decimal)
			_PIBDNilaiTanahBangunan = Value
		End Set
	End Property

	Public Property PIBFTglPenilaian() As Date
		Get
			Return _PIBFTglPenilaian
		End Get
		Set(ByVal Value As Date)
			_PIBFTglPenilaian = Value
		End Set
	End Property

	Public Property PIBFNilaiTanah() As Decimal
		Get
			Return _PIBFNilaiTanah
		End Get
		Set(ByVal Value As Decimal)
			_PIBFNilaiTanah = Value
		End Set
	End Property

	Public Property PIBFNilaiBangunan() As Decimal
		Get
			Return _PIBFNilaiBangunan
		End Get
		Set(ByVal Value As Decimal)
			_PIBFNilaiBangunan = Value
		End Set
	End Property

	Public Property PIBFNilaiTanahBangunan() As Decimal
		Get
			Return _PIBFNilaiTanahBangunan
		End Get
		Set(ByVal Value As Decimal)
			_PIBFNilaiTanahBangunan = Value
		End Set
	End Property

	Public Property PEBDTglPenilaian() As Date
		Get
			Return _PEBDTglPenilaian
		End Get
		Set(ByVal Value As Date)
			_PEBDTglPenilaian = Value
		End Set
	End Property

	Public Property PEBDNilaiTanah() As Decimal
		Get
			Return _PEBDNilaiTanah
		End Get
		Set(ByVal Value As Decimal)
			_PEBDNilaiTanah = Value
		End Set
	End Property

	Public Property PEBDNilaiBangunan() As Decimal
		Get
			Return _PEBDNilaiBangunan
		End Get
		Set(ByVal Value As Decimal)
			_PEBDNilaiBangunan = Value
		End Set
	End Property

	Public Property PEBDNilaiTanahBangunan() As Decimal
		Get
			Return _PEBDNilaiTanahBangunan
		End Get
		Set(ByVal Value As Decimal)
			_PEBDNilaiTanahBangunan = Value
		End Set
	End Property

	Public Property PEBFTglPenilaian() As Date
		Get
			Return _PEBFTglPenilaian
		End Get
		Set(ByVal Value As Date)
			_PEBFTglPenilaian = Value
		End Set
	End Property

	Public Property PEBFNilaiTanah() As Decimal
		Get
			Return _PEBFNilaiTanah
		End Get
		Set(ByVal Value As Decimal)
			_PEBFNilaiTanah = Value
		End Set
	End Property

	Public Property PEBFNilaiBangunan() As Decimal
		Get
			Return _PEBFNilaiBangunan
		End Get
		Set(ByVal Value As Decimal)
			_PEBFNilaiBangunan = Value
		End Set
	End Property

	Public Property PEBFNilaiTanahBangunan() As Decimal
		Get
			Return _PEBFNilaiTanahBangunan
		End Get
		Set(ByVal Value As Decimal)
			_PEBFNilaiTanahBangunan = Value
		End Set
	End Property

	Public Property PJNoSKMHT() As String
		Get
			Return _PJNoSKMHT
		End Get
		Set(ByVal Value As String)
			_PJNoSKMHT = Value
		End Set
	End Property
	Public Property PJTglSKMHT() As Date
		Get
			Return _PJTglSKMHT
		End Get
		Set(ByVal Value As Date)
			_PJTglSKMHT = Value
		End Set
	End Property

	Public Property PJTglJatuhTempoSKMHT() As Date
		Get
			Return _PJTglJatuhTempoSKMHT
		End Get
		Set(ByVal Value As Date)
			_PJTglJatuhTempoSKMHT = Value
		End Set
	End Property

	Public Property PJNoAPHT() As String
		Get
			Return _PJNoAPHT
		End Get
		Set(ByVal Value As String)
			_PJNoAPHT = Value
		End Set
	End Property

	Public Property PJTglAPHT() As Date
		Get
			Return _PJTglAPHT
		End Get
		Set(ByVal Value As Date)
			_PJTglAPHT = Value
		End Set
	End Property

	Public Property PJNoSHT() As String
		Get
			Return _PJNoSHT
		End Get
		Set(ByVal Value As String)
			_PJNoSHT = Value
		End Set
	End Property

	Public Property PJTglIssuedSHT() As Date
		Get
			Return _PJTglIssuedSHT
		End Get
		Set(ByVal Value As Date)
			_PJTglIssuedSHT = Value
		End Set
	End Property

	Public Property PJTotalNilaiHT() As Decimal
		Get
			Return _PJTotalNilaiHT
		End Get
		Set(ByVal Value As Decimal)
			_PJTotalNilaiHT = Value
		End Set
	End Property

	Public Property PJRangkingHT() As String
		Get
			Return _PJRangkingHT
		End Get
		Set(ByVal Value As String)
			_PJRangkingHT = Value
		End Set
	End Property

	Public Property PJJenisPengikatan() As String
		Get
			Return _PJJenisPengikatan
		End Get
		Set(ByVal Value As String)
			_PJJenisPengikatan = Value
		End Set
	End Property

	Public Property PJNotaris() As String
		Get
			Return _PJNotaris
		End Get
		Set(ByVal Value As String)
			_PJNotaris = Value
		End Set
	End Property

	Public Property PJLokasiPenyimpananDokumen() As String
		Get
			Return _PJLokasiPenyimpananDokumen
		End Get
		Set(ByVal Value As String)
			_PJLokasiPenyimpananDokumen = Value
		End Set
	End Property

	Public Property PJKantorBPNWilayah() As String
		Get
			Return _PJKantorBPNWilayah
		End Get
		Set(ByVal Value As String)
			_PJKantorBPNWilayah = Value
		End Set
	End Property

	Public Property BgnFisikJaminan() As String
		Get
			Return _BgnFisikJaminan
		End Get
		Set(ByVal Value As String)
			_BgnFisikJaminan = Value
		End Set
	End Property

	Public Property BgnSuratIzin() As String
		Get
			Return _BgnSuratIzin
		End Get
		Set(ByVal Value As String)
			_BgnSuratIzin = Value
		End Set
	End Property

	Public Property BgnNoSuratIzin() As String
		Get
			Return _BgnNoSuratIzin
		End Get
		Set(ByVal Value As String)
			_BgnNoSuratIzin = Value
		End Set
	End Property

	Public Property BgnJenisSuratIzin() As String
		Get
			Return _BgnJenisSuratIzin
		End Get
		Set(ByVal Value As String)
			_BgnJenisSuratIzin = Value
		End Set
	End Property

	Public Property BgnPeruntukanBangunan() As String
		Get
			Return _BgnPeruntukanBangunan
		End Get
		Set(ByVal Value As String)
			_BgnPeruntukanBangunan = Value
		End Set
	End Property

	Public Property BgnNoIzinLayakHuni() As String
		Get
			Return _BgnNoIzinLayakHuni
		End Get
		Set(ByVal Value As String)
			_BgnNoIzinLayakHuni = Value
		End Set
	End Property

	Public Property BgnPBBTahunTerakhir() As String
		Get
			Return _BgnPBBTahunTerakhir
		End Get
		Set(ByVal Value As String)
			_BgnPBBTahunTerakhir = Value
		End Set
	End Property

	Public Property BgnNilaiNJOP() As Decimal
		Get
			Return _BgnNilaiNJOP
		End Get
		Set(ByVal Value As Decimal)
			_BgnNilaiNJOP = Value
		End Set
	End Property

	Public Property BgnCetakBiru() As String
		Get
			Return _BgnCetakBiru
		End Get
		Set(ByVal Value As String)
			_BgnCetakBiru = Value
		End Set
	End Property

	Public Property BgnAktaBalikNama() As String
		Get
			Return _BgnAktaBalikNama
		End Get
		Set(ByVal Value As String)
			_BgnAktaBalikNama = Value
		End Set
	End Property

	Public Property BgnNoAkta() As String
		Get
			Return _BgnNoAkta
		End Get
		Set(ByVal Value As String)
			_BgnNoAkta = Value
		End Set
	End Property

	Public Property BgnTglAkta() As Date
		Get
			Return _BgnTglAkta
		End Get
		Set(ByVal Value As Date)
			_BgnTglAkta = Value
		End Set
	End Property

	Public Property BgnNotaris() As String
		Get
			Return _BgnNotaris
		End Get
		Set(ByVal Value As String)
			_BgnNotaris = Value
		End Set
	End Property

	Public Property BgnParipasu() As Integer
		Get
			Return _BgnParipasu
		End Get
		Set(ByVal Value As Integer)
			_BgnParipasu = Value
		End Set
	End Property

	Public Property BgnBangunanDiasuransikan() As String
		Get
			Return _BgnBangunanDiasuransikan
		End Get
		Set(ByVal Value As String)
			_BgnBangunanDiasuransikan = Value
		End Set
	End Property

	Public Property PledgingPercentage() As Integer
		Get
			Return _PledgingPercentage
		End Get
		Set(ByVal Value As Integer)
			_PledgingPercentage = Value
		End Set
	End Property

	Public Property PledgingAmount() As Decimal
		Get
			Return _PledgingAmount
		End Get
		Set(ByVal Value As Decimal)
			_PledgingAmount = Value
		End Set
	End Property

	Public Property PagingTable() As DataTable
		Get
			Return (CType(_PagingTable, DataTable))
		End Get
		Set(ByVal Value As DataTable)
			_PagingTable = Value
		End Set
	End Property

	Public Property TglKontrak As Date
		Get
			Return _TglKontrak
		End Get
		Set(value As Date)
			_TglKontrak = value
		End Set
	End Property

	Public Property Objek As String
		Get
			Return _Objek
		End Get
		Set(value As String)
			_Objek = value
		End Set
	End Property

	Public Property NoInvoice As String
		Get
			Return _NoInvoice
		End Get
		Set(value As String)
			_NoInvoice = value
		End Set
	End Property

	Public Property TglInvoice As Date
		Get
			Return _TglInvoice
		End Get
		Set(value As Date)
			_TglInvoice = value
		End Set
	End Property

	Public Property PenerbitInvoice As String
		Get
			Return _PenerbitInvoice
		End Get
		Set(value As String)
			_PenerbitInvoice = value
		End Set
	End Property

	Public Property PenerimaInvoice As String
		Get
			Return _PenerimaInvoice
		End Get
		Set(value As String)
			_PenerimaInvoice = value
		End Set
	End Property

	Public Property NilaiInvoice As Double
		Get
			Return _NilaiInvoice
		End Get
		Set(value As Double)
			_NilaiInvoice = value
		End Set
	End Property

	Public Property NoRangka As String
		Get
			Return _NoRangka
		End Get
		Set(value As String)
			_NoRangka = value
		End Set
	End Property

	Public Property NoMesin As String
		Get
			Return _NoMesin
		End Get
		Set(value As String)
			_NoMesin = value
		End Set
	End Property

	Public Property NamaDiBPKB As String
		Get
			Return _NamaDiBPKB
		End Get
		Set(value As String)
			_NamaDiBPKB = value
		End Set
	End Property

	Public Property Merk As String
		Get
			Return _Merk
		End Get
		Set(value As String)
			_Merk = value
		End Set
	End Property

	Public Property Tipe As String
		Get
			Return _Tipe
		End Get
		Set(value As String)
			_Tipe = value
		End Set
	End Property

	Public Property Warna As String
		Get
			Return _Warna
		End Get
		Set(value As String)
			_Warna = value
		End Set
	End Property

	Public Property NamaDealer As String
		Get
			Return _NamaDealer
		End Get
		Set(value As String)
			_NamaDealer = value
		End Set
	End Property

	Public Property AlamatDealer As String
		Get
			Return _AlamatDealer
		End Get
		Set(value As String)
			_AlamatDealer = value
		End Set
	End Property

	Public Property TelponDealer As String
		Get
			Return _TelponDealer
		End Get
		Set(value As String)
			_TelponDealer = value
		End Set
	End Property

	Public Property JTBPKB As Date
		Get
			Return _JTBPKB
		End Get
		Set(value As Date)
			_JTBPKB = value
		End Set
	End Property

	Public Property TglTerimaBPKB As Date
		Get
			Return _TglTerimaBPKB
		End Get
		Set(value As Date)
			_TglTerimaBPKB = value
		End Set
	End Property

	Public Property NoBPKB As String
		Get
			Return _NoBPKB
		End Get
		Set(value As String)
			_NoBPKB = value
		End Set
	End Property

	Public Property TglBPKB As Date
		Get
			Return _TglBPKB
		End Get
		Set(value As Date)
			_TglBPKB = value
		End Set
	End Property

	Public Property NoPolisi As String
		Get
			Return _NoPolisi
		End Get
		Set(value As String)
			_NoPolisi = value
		End Set
	End Property

	Public Property NoFaktur As String
		Get
			Return _NoFaktur
		End Get
		Set(value As String)
			_NoFaktur = value
		End Set
	End Property

	Public Property NoNIK As String
		Get
			Return _NoNIK
		End Get
		Set(value As String)
			_NoNIK = value
		End Set
	End Property

	Public Property NoFormA As String
		Get
			Return _NoFormA
		End Get
		Set(value As String)
			_NoFormA = value
		End Set
	End Property

	Public Property AktaFidusia As String
		Get
			Return _AktaFidusia
		End Get
		Set(value As String)
			_AktaFidusia = value
		End Set
	End Property

	Public Property SertifikatFidusia As String
		Get
			Return _SertifikatFidusia
		End Get
		Set(value As String)
			_SertifikatFidusia = value
		End Set
	End Property

	Public Property TglKeluarBPKBSementara As Date
		Get
			Return _TglKeluarBPKBSementara
		End Get
		Set(value As Date)
			_TglKeluarBPKBSementara = value
		End Set
	End Property

	Public Property Keterangan As String
		Get
			Return _Keterangan
		End Get
		Set(value As String)
			_Keterangan = value
		End Set
	End Property

	Public Property TglClose As Date
		Get
			Return _TglClose
		End Get
		Set(value As Date)
			_TglClose = value
		End Set
	End Property

	Public Property Name As String
		Get
			Return _Name
		End Get
		Set(value As String)
			_Name = value
		End Set
	End Property

	Public Property ID As String
		Get
			Return _ID
		End Get
		Set(value As String)
			_ID = value
		End Set
	End Property

	Public Property CollateralAmount As String
		Get
			Return _CollateralAmount
		End Get
		Set(value As String)
			_CollateralAmount = value
		End Set
	End Property
End Class
