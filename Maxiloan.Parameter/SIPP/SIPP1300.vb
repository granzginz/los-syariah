﻿<Serializable()>
Public Class SIPP1300 : Inherits Common
    Private _ID As Int64
    Private _Bulandata As String
    Private _RS_KS_MSK_DR_PMBYN_NVSTS_TTL As Double
    Private _RS_KS_MSK_DR_PMBYN_NVSTS_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_PMBYN_NVSTS_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_PMBYN_MDL_KRJ_TTL As Double
    Private _RS_KS_MSK_DR_PMBYN_MDL_KRJ_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_PMBYN_MDL_KRJ_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_PMBYN_MLTGN_TTL As Double
    Private _RS_KS_MSK_DR_PMBYN_MLTGN_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_PMBYN_MLTGN_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL As Double
    Private _RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_TTL As Double
    Private _RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_KGTN_BRBSS_F_TTL As Double
    Private _RS_KS_MSK_DR_KGTN_BRBSS_F_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_KGTN_BRBSS_F_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_KGTN_SW_PRS_TTL As Double
    Private _RS_KS_MSK_DR_KGTN_SW_PRS_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_KGTN_SW_PRS_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL As Double
    Private _RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL As Double
    Private _RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_TTL As Double
    Private _RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_KGTN_PRS_TTL As Double
    Private _RS_KS_MSK_DR_KGTN_PRS_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_KGTN_PRS_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_PMBYN_NVSTS_TTL As Double
    Private _RS_KS_KLR_NTK_PMBYN_NVSTS_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_PMBYN_NVSTS_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_PMBYN_MDL_KRJ_TTL As Double
    Private _RS_KS_KLR_NTK_PMBYN_MDL_KRJ_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_PMBYN_MDL_KRJ_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_PMBYN_MLTGN_TTL As Double
    Private _RS_KS_KLR_NTK_PMBYN_MLTGN_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_PMBYN_MLTGN_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL As Double
    Private _RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_TTL As Double
    Private _RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_TTL As Double
    Private _RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_TTL As Double
    Private _RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_TTL As Double
    Private _RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL As Double
    Private _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_TTL As Double
    Private _RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_KGTN_PRS_TTL As Double
    Private _RS_KS_KLR_NTK_KGTN_PRS_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_KGTN_PRS_MT_NG_SNG As Double
    Private _RS_KS_BRSH_DR_KGTN_PRS_TTL As Double
    Private _RS_KS_BRSH_DR_KGTN_PRS_NDNSN_RPH As Double
    Private _RS_KS_BRSH_DR_KGTN_PRS_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_PLPSN_NK_PRSHN_TTL As Double
    Private _RS_KS_MSK_DR_PLPSN_NK_PRSHN_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_PLPSN_NK_PRSHN_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_TTL As Double
    Private _RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL As Double
    Private _RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_DVDN_TTL As Double
    Private _RS_KS_MSK_DR_DVDN_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_DVDN_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_TTL As Double
    Private _RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_KGTN_NVSTS_LNNY_TTL As Double
    Private _RS_KS_MSK_DR_KGTN_NVSTS_LNNY_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_KGTN_NVSTS_LNNY_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_KGTN_NVSTS_TTL As Double
    Private _RS_KS_MSK_DR_KGTN_NVSTS_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_KGTN_NVSTS_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_TTL As Double
    Private _RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_TTL As Double
    Private _RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL As Double
    Private _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_TTL As Double
    Private _RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_KGTN_NVSTS_TTL As Double
    Private _RS_KS_KLR_NTK_KGTN_NVSTS_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_KGTN_NVSTS_MT_NG_SNG As Double
    Private _RS_KS_BRSH_DR_KGTN_NVSTS_TTL As Double
    Private _RS_KS_BRSH_DR_KGTN_NVSTS_NDNSN_RPH As Double
    Private _RS_KS_BRSH_DR_KGTN_NVSTS_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_TTL As Double
    Private _RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_PNDNN_LNNY_TTL As Double
    Private _RS_KS_MSK_DR_PNDNN_LNNY_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_PNDNN_LNNY_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_PNRBTN_MDL_SHM_TTL As Double
    Private _RS_KS_MSK_DR_PNRBTN_MDL_SHM_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_PNRBTN_MDL_SHM_MT_NG_SNG As Double
    Private _RS_KS_MSK_DR_KGTN_PNDNN_TTL As Double
    Private _RS_KS_MSK_DR_KGTN_PNDNN_NDNSN_RPH As Double
    Private _RS_KS_MSK_DR_KGTN_PNDNN_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_TTL As Double
    Private _RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_PNDNN_LNNY_TTL As Double
    Private _RS_KS_KLR_NTK_PNDNN_LNNY_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_PNDNN_LNNY_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_TTL As Double
    Private _RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_PMBYRN_DVDN_TTL As Double
    Private _RS_KS_KLR_NTK_PMBYRN_DVDN_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_PMBYRN_DVDN_MT_NG_SNG As Double
    Private _RS_KS_KLR_NTK_KGTN_PNDNN_TTL As Double
    Private _RS_KS_KLR_NTK_KGTN_PNDNN_NDNSN_RPH As Double
    Private _RS_KS_KLR_NTK_KGTN_PNDNN_MT_NG_SNG As Double
    Private _RS_KS_BRSH_DR_KGTN_PNDNN_TTL As Double
    Private _RS_KS_BRSH_DR_KGTN_PNDNN_NDNSN_RPH As Double
    Private _RS_KS_BRSH_DR_KGTN_PNDNN_MT_NG_SNG As Double
    Private _SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_TTL As Double
    Private _SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_NDNSN_RPH As Double
    Private _SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_MT_NG_SNG As Double
    Private _KNKN_PNRNN_BRSH_KS_DN_STR_KS_TTL As Double
    Private _KNKN_PNRNN_BRSH_KS_DN_STR_KS_NDNSN_RPH As Double
    Private _KNKN_PNRNN_BRSH_KS_DN_STR_KS_MT_NG_SNG As Double
    Private _KS_DN_STR_KS_PD_WL_PRD_TTL As Double
    Private _KS_DN_STR_KS_PD_WL_PRD_NDNSN_RPH As Double
    Private _KS_DN_STR_KS_PD_WL_PRD_MT_NG_SNG As Double
    Private _KS_DN_STR_KS_TTL As Double
    Private _KS_DN_STR_KS_NDNSN_RPH As Double
    Private _KS_DN_STR_KS_MT_NG_SNG As Double

    Private _ListData As DataTable
    Private _ListDataReport As DataSet
    Private _TotalRecords As Int64
    Private _Table As String
    Private _valueparent As String
    Private _valuemaster As String
    Private _valueelement As String

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property Bulandata As String
        Get
            Return _Bulandata
        End Get
        Set(value As String)
            _Bulandata = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PMBYN_NVSTS_TTL As Double
        Get
            Return _RS_KS_MSK_DR_PMBYN_NVSTS_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PMBYN_NVSTS_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PMBYN_NVSTS_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_PMBYN_NVSTS_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PMBYN_NVSTS_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PMBYN_NVSTS_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_PMBYN_NVSTS_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PMBYN_NVSTS_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PMBYN_MDL_KRJ_TTL As Double
        Get
            Return _RS_KS_MSK_DR_PMBYN_MDL_KRJ_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PMBYN_MDL_KRJ_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PMBYN_MDL_KRJ_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_PMBYN_MDL_KRJ_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PMBYN_MDL_KRJ_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PMBYN_MDL_KRJ_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_PMBYN_MDL_KRJ_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PMBYN_MDL_KRJ_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PMBYN_MLTGN_TTL As Double
        Get
            Return _RS_KS_MSK_DR_PMBYN_MLTGN_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PMBYN_MLTGN_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PMBYN_MLTGN_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_PMBYN_MLTGN_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PMBYN_MLTGN_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PMBYN_MLTGN_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_PMBYN_MLTGN_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PMBYN_MLTGN_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL As Double
        Get
            Return _RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_TTL As Double
        Get
            Return _RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_BRBSS_F_TTL As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_BRBSS_F_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_BRBSS_F_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_BRBSS_F_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_BRBSS_F_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_BRBSS_F_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_BRBSS_F_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_BRBSS_F_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_BRBSS_F_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_SW_PRS_TTL As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_SW_PRS_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_SW_PRS_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_SW_PRS_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_SW_PRS_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_SW_PRS_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_SW_PRS_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_SW_PRS_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_SW_PRS_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL As Double
        Get
            Return _RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_TTL As Double
        Get
            Return _RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_PRS_TTL As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_PRS_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_PRS_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_PRS_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_PRS_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_PRS_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_PRS_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_PRS_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_PRS_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYN_NVSTS_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYN_NVSTS_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYN_NVSTS_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYN_NVSTS_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYN_NVSTS_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYN_NVSTS_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYN_NVSTS_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYN_NVSTS_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYN_NVSTS_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYN_MDL_KRJ_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYN_MDL_KRJ_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYN_MDL_KRJ_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYN_MDL_KRJ_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYN_MDL_KRJ_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYN_MDL_KRJ_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYN_MDL_KRJ_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYN_MDL_KRJ_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYN_MDL_KRJ_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYN_MLTGN_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYN_MLTGN_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYN_MLTGN_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYN_MLTGN_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYN_MLTGN_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYN_MLTGN_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYN_MLTGN_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYN_MLTGN_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYN_MLTGN_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_KGTN_PRS_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_KGTN_PRS_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_KGTN_PRS_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_KGTN_PRS_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_KGTN_PRS_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_KGTN_PRS_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_KGTN_PRS_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_KGTN_PRS_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_KGTN_PRS_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_BRSH_DR_KGTN_PRS_TTL As Double
        Get
            Return _RS_KS_BRSH_DR_KGTN_PRS_TTL
        End Get
        Set(value As Double)
            _RS_KS_BRSH_DR_KGTN_PRS_TTL = value
        End Set
    End Property

    Public Property RS_KS_BRSH_DR_KGTN_PRS_NDNSN_RPH As Double
        Get
            Return _RS_KS_BRSH_DR_KGTN_PRS_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_BRSH_DR_KGTN_PRS_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_BRSH_DR_KGTN_PRS_MT_NG_SNG As Double
        Get
            Return _RS_KS_BRSH_DR_KGTN_PRS_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_BRSH_DR_KGTN_PRS_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PLPSN_NK_PRSHN_TTL As Double
        Get
            Return _RS_KS_MSK_DR_PLPSN_NK_PRSHN_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PLPSN_NK_PRSHN_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PLPSN_NK_PRSHN_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_PLPSN_NK_PRSHN_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PLPSN_NK_PRSHN_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PLPSN_NK_PRSHN_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_PLPSN_NK_PRSHN_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PLPSN_NK_PRSHN_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_TTL As Double
        Get
            Return _RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL As Double
        Get
            Return _RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_DVDN_TTL As Double
        Get
            Return _RS_KS_MSK_DR_DVDN_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_DVDN_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_DVDN_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_DVDN_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_DVDN_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_DVDN_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_DVDN_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_DVDN_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_TTL As Double
        Get
            Return _RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_NVSTS_LNNY_TTL As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_NVSTS_LNNY_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_NVSTS_LNNY_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_NVSTS_LNNY_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_NVSTS_LNNY_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_NVSTS_LNNY_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_NVSTS_LNNY_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_NVSTS_LNNY_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_NVSTS_LNNY_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_NVSTS_TTL As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_NVSTS_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_NVSTS_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_NVSTS_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_NVSTS_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_NVSTS_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_NVSTS_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_NVSTS_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_NVSTS_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_KGTN_NVSTS_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_KGTN_NVSTS_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_KGTN_NVSTS_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_KGTN_NVSTS_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_KGTN_NVSTS_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_KGTN_NVSTS_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_KGTN_NVSTS_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_KGTN_NVSTS_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_KGTN_NVSTS_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_BRSH_DR_KGTN_NVSTS_TTL As Double
        Get
            Return _RS_KS_BRSH_DR_KGTN_NVSTS_TTL
        End Get
        Set(value As Double)
            _RS_KS_BRSH_DR_KGTN_NVSTS_TTL = value
        End Set
    End Property

    Public Property RS_KS_BRSH_DR_KGTN_NVSTS_NDNSN_RPH As Double
        Get
            Return _RS_KS_BRSH_DR_KGTN_NVSTS_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_BRSH_DR_KGTN_NVSTS_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_BRSH_DR_KGTN_NVSTS_MT_NG_SNG As Double
        Get
            Return _RS_KS_BRSH_DR_KGTN_NVSTS_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_BRSH_DR_KGTN_NVSTS_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_TTL As Double
        Get
            Return _RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNDNN_LNNY_TTL As Double
        Get
            Return _RS_KS_MSK_DR_PNDNN_LNNY_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNDNN_LNNY_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNDNN_LNNY_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_PNDNN_LNNY_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNDNN_LNNY_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNDNN_LNNY_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_PNDNN_LNNY_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNDNN_LNNY_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNRBTN_MDL_SHM_TTL As Double
        Get
            Return _RS_KS_MSK_DR_PNRBTN_MDL_SHM_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNRBTN_MDL_SHM_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNRBTN_MDL_SHM_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_PNRBTN_MDL_SHM_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNRBTN_MDL_SHM_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_PNRBTN_MDL_SHM_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_PNRBTN_MDL_SHM_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_PNRBTN_MDL_SHM_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_PNDNN_TTL As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_PNDNN_TTL
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_PNDNN_TTL = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_PNDNN_NDNSN_RPH As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_PNDNN_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_PNDNN_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_MSK_DR_KGTN_PNDNN_MT_NG_SNG As Double
        Get
            Return _RS_KS_MSK_DR_KGTN_PNDNN_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_MSK_DR_KGTN_PNDNN_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PNDNN_LNNY_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_PNDNN_LNNY_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PNDNN_LNNY_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PNDNN_LNNY_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_PNDNN_LNNY_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PNDNN_LNNY_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PNDNN_LNNY_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_PNDNN_LNNY_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PNDNN_LNNY_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_DVDN_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_DVDN_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_DVDN_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_DVDN_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_DVDN_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_DVDN_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_PMBYRN_DVDN_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_PMBYRN_DVDN_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_PMBYRN_DVDN_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_KGTN_PNDNN_TTL As Double
        Get
            Return _RS_KS_KLR_NTK_KGTN_PNDNN_TTL
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_KGTN_PNDNN_TTL = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_KGTN_PNDNN_NDNSN_RPH As Double
        Get
            Return _RS_KS_KLR_NTK_KGTN_PNDNN_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_KGTN_PNDNN_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_KLR_NTK_KGTN_PNDNN_MT_NG_SNG As Double
        Get
            Return _RS_KS_KLR_NTK_KGTN_PNDNN_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_KLR_NTK_KGTN_PNDNN_MT_NG_SNG = value
        End Set
    End Property

    Public Property RS_KS_BRSH_DR_KGTN_PNDNN_TTL As Double
        Get
            Return _RS_KS_BRSH_DR_KGTN_PNDNN_TTL
        End Get
        Set(value As Double)
            _RS_KS_BRSH_DR_KGTN_PNDNN_TTL = value
        End Set
    End Property

    Public Property RS_KS_BRSH_DR_KGTN_PNDNN_NDNSN_RPH As Double
        Get
            Return _RS_KS_BRSH_DR_KGTN_PNDNN_NDNSN_RPH
        End Get
        Set(value As Double)
            _RS_KS_BRSH_DR_KGTN_PNDNN_NDNSN_RPH = value
        End Set
    End Property

    Public Property RS_KS_BRSH_DR_KGTN_PNDNN_MT_NG_SNG As Double
        Get
            Return _RS_KS_BRSH_DR_KGTN_PNDNN_MT_NG_SNG
        End Get
        Set(value As Double)
            _RS_KS_BRSH_DR_KGTN_PNDNN_MT_NG_SNG = value
        End Set
    End Property

    Public Property SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_TTL As Double
        Get
            Return _SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_TTL
        End Get
        Set(value As Double)
            _SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_TTL = value
        End Set
    End Property

    Public Property SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_NDNSN_RPH As Double
        Get
            Return _SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_NDNSN_RPH
        End Get
        Set(value As Double)
            _SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_NDNSN_RPH = value
        End Set
    End Property

    Public Property SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_MT_NG_SNG As Double
        Get
            Return _SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_MT_NG_SNG
        End Get
        Set(value As Double)
            _SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_MT_NG_SNG = value
        End Set
    End Property

    Public Property KNKN_PNRNN_BRSH_KS_DN_STR_KS_TTL As Double
        Get
            Return _KNKN_PNRNN_BRSH_KS_DN_STR_KS_TTL
        End Get
        Set(value As Double)
            _KNKN_PNRNN_BRSH_KS_DN_STR_KS_TTL = value
        End Set
    End Property

    Public Property KNKN_PNRNN_BRSH_KS_DN_STR_KS_NDNSN_RPH As Double
        Get
            Return _KNKN_PNRNN_BRSH_KS_DN_STR_KS_NDNSN_RPH
        End Get
        Set(value As Double)
            _KNKN_PNRNN_BRSH_KS_DN_STR_KS_NDNSN_RPH = value
        End Set
    End Property

    Public Property KNKN_PNRNN_BRSH_KS_DN_STR_KS_MT_NG_SNG As Double
        Get
            Return _KNKN_PNRNN_BRSH_KS_DN_STR_KS_MT_NG_SNG
        End Get
        Set(value As Double)
            _KNKN_PNRNN_BRSH_KS_DN_STR_KS_MT_NG_SNG = value
        End Set
    End Property

    Public Property KS_DN_STR_KS_PD_WL_PRD_TTL As Double
        Get
            Return _KS_DN_STR_KS_PD_WL_PRD_TTL
        End Get
        Set(value As Double)
            _KS_DN_STR_KS_PD_WL_PRD_TTL = value
        End Set
    End Property

    Public Property KS_DN_STR_KS_PD_WL_PRD_NDNSN_RPH As Double
        Get
            Return _KS_DN_STR_KS_PD_WL_PRD_NDNSN_RPH
        End Get
        Set(value As Double)
            _KS_DN_STR_KS_PD_WL_PRD_NDNSN_RPH = value
        End Set
    End Property

    Public Property KS_DN_STR_KS_PD_WL_PRD_MT_NG_SNG As Double
        Get
            Return _KS_DN_STR_KS_PD_WL_PRD_MT_NG_SNG
        End Get
        Set(value As Double)
            _KS_DN_STR_KS_PD_WL_PRD_MT_NG_SNG = value
        End Set
    End Property

    Public Property KS_DN_STR_KS_TTL As Double
        Get
            Return _KS_DN_STR_KS_TTL
        End Get
        Set(value As Double)
            _KS_DN_STR_KS_TTL = value
        End Set
    End Property

    Public Property KS_DN_STR_KS_NDNSN_RPH As Double
        Get
            Return _KS_DN_STR_KS_NDNSN_RPH
        End Get
        Set(value As Double)
            _KS_DN_STR_KS_NDNSN_RPH = value
        End Set
    End Property

    Public Property KS_DN_STR_KS_MT_NG_SNG As Double
        Get
            Return _KS_DN_STR_KS_MT_NG_SNG
        End Get
        Set(value As Double)
            _KS_DN_STR_KS_MT_NG_SNG = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property

    Public Property ListDataReport As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(value As DataSet)
            _ListDataReport = value
        End Set
    End Property

    Public Property TotalRecords As Long
        Get
            Return _TotalRecords
        End Get
        Set(value As Long)
            _TotalRecords = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property

    Public Property Valueparent As String
        Get
            Return _valueparent
        End Get
        Set(value As String)
            _valueparent = value
        End Set
    End Property

    Public Property Valuemaster As String
        Get
            Return _valuemaster
        End Get
        Set(value As String)
            _valuemaster = value
        End Set
    End Property

    Public Property Valueelement As String
        Get
            Return _valueelement
        End Get
        Set(value As String)
            _valueelement = value
        End Set
    End Property
End Class
