﻿<Serializable()>
Public Class SIPP1100 : Inherits Common

    Private _Table As String
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _BULANDATA As String
    Public Property KS_TTL As Int64
    Public Property KS_NDNSN_RPH As Int64
    Public Property KS_MT_NG_SNG As Int64
    Public Property GR_PD_BNK_DLM_NGR_TTL As Int64
    Public Property GR_PD_BNK_DLM_NGR_NDNSN_RPH As Int64
    Public Property GR_PD_BNK_DLM_NGR_MT_NG_SNG As Int64
    Public Property SMPNN_LNNY_PD_BNK_DLM_NGR_TTL As Int64
    Public Property SMPNN_LNNY_PD_BNK_DLM_NGR_NDNSN_RPH As Int64
    Public Property SMPNN_LNNY_PD_BNK_DLM_NGR_MT_NG_SNG As Int64
    Public Property SMPNN_PD_BNK_DLM_NGR_TTL As Int64
    Public Property SMPNN_PD_BNK_DLM_NGR_NDNSN_RPH As Int64
    Public Property SMPNN_PD_BNK_DLM_NGR_MT_NG_SNG As Int64
    Public Property GR_PD_BNK_LR_NGR_TTL As Int64
    Public Property GR_PD_BNK_LR_NGR_NDNSN_RPH As Int64
    Public Property GR_PD_BNK_LR_NGR_MT_NG_SNG As Int64
    Public Property SMPNN_LNNY_PD_BNK_LR_NGR_TTL As Int64
    Public Property SMPNN_LNNY_PD_BNK_LR_NGR_NDNSN_RPH As Int64
    Public Property SMPNN_LNNY_PD_BNK_LR_NGR_MT_NG_SNG As Int64
    Public Property SMPNN_PD_BNK_LR_NGR_TTL As Int64
    Public Property SMPNN_PD_BNK_LR_NGR_NDNSN_RPH As Int64
    Public Property SMPNN_PD_BNK_LR_NGR_MT_NG_SNG As Int64
    Public Property KS_DN_STR_KS_TTL As Int64
    Public Property KS_DN_STR_KS_NDNSN_RPH As Int64
    Public Property KS_DN_STR_KS_MT_NG_SNG As Int64
    Public Property ST_TGHN_DRVTF_TTL As Int64
    Public Property ST_TGHN_DRVTF_NDNSN_RPH As Int64
    Public Property ST_TGHN_DRVTF_MT_NG_SNG As Int64
    Public Property NVSTS_JNGK_PNDK_DLM_SRT_BRHRG_TTL As Int64
    Public Property NVSTS_JNGK_PNDK_DLM_SRT_BRHRG_NDNSN_RPH As Int64
    Public Property NVSTS_JNGK_PNDK_DLM_SRT_BRHRG_MT_NG_SNG As Int64
    Public Property PTNG_PMBYN_NVSTS__PKK_TTL As Int64
    Public Property PTNG_PMBYN_NVSTS__PKK_NDNSN_RPH As Int64
    Public Property PTNG_PMBYN_NVSTS__PKK_MT_NG_SNG As Int64
    Public Property CDNGN_PTNG_PMBYN_NVSTS_TTL As Int64
    Public Property CDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH As Int64
    Public Property CDNGN_PTNG_PMBYN_NVSTS_MT_NG_SNG As Int64
    Public Property PTNG_PMBYN_NVSTS__NT_TTL As Int64
    Public Property PTNG_PMBYN_NVSTS__NT_NDNSN_RPH As Int64
    Public Property PTNG_PMBYN_NVSTS__NT_MT_NG_SNG As Int64
    Public Property PTNG_PMBYN_MDL_KRJ__PKK_TTL As Int64
    Public Property PTNG_PMBYN_MDL_KRJ__PKK_NDNSN_RPH As Int64
    Public Property PTNG_PMBYN_MDL_KRJ__PKK_MT_NG_SNG As Int64
    Public Property CDNGN_PTNG_MDL_KRJ_TTL As Int64
    Public Property CDNGN_PTNG_MDL_KRJ_NDNSN_RPH As Int64
    Public Property CDNGN_PTNG_MDL_KRJ_MT_NG_SNG As Int64
    Public Property PTNG_PMBYN_MDL_KRJ__NT_TTL As Int64
    Public Property PTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH As Int64
    Public Property PTNG_PMBYN_MDL_KRJ__NT_MT_NG_SNG As Int64
    Public Property PTNG_PMBYN_MLTGN__PKK_TTL As Int64
    Public Property PTNG_PMBYN_MLTGN__PKK_NDNSN_RPH As Int64
    Public Property PTNG_PMBYN_MLTGN__PKK_MT_NG_SNG As Int64
    Public Property CDNGN_PTNG_PMBYN_MLTGN_TTL As Int64
    Public Property CDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH As Int64
    Public Property CDNGN_PTNG_PMBYN_MLTGN_MT_NG_SNG As Int64
    Public Property PTNG_PMBYN_MLTGN__NT_TTL As Int64
    Public Property PTNG_PMBYN_MLTGN__NT_NDNSN_RPH As Int64
    Public Property PTNG_PMBYN_MLTGN__NT_MT_NG_SNG As Int64
    Public Property PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_TTL As Int64
    Public Property PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH As Int64
    Public Property PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_MT_NG_SNG As Int64
    Public Property CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL As Int64
    Public Property CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH As Int64
    Public Property CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG As Int64
    Public Property PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_TTL As Int64
    Public Property PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH As Int64
    Public Property PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_MT_NG_SNG As Int64
    Public Property PTNG_PMBYN_KNVNSNL__NT_TTL As Int64
    Public Property PTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH As Int64
    Public Property PTNG_PMBYN_KNVNSNL__NT_MT_NG_SNG As Int64
    Public Property PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_TTL As Int64
    Public Property PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH As Int64
    Public Property PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG As Int64
    Public Property CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL As Int64
    Public Property CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH As Int64
    Public Property CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG As Int64
    Public Property PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_TTL As Int64
    Public Property PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH As Int64
    Public Property PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG As Int64
    Public Property PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_TTL As Int64
    Public Property PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH As Int64
    Public Property PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG As Int64
    Public Property CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL As Int64
    Public Property CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH As Int64
    Public Property CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG As Int64
    Public Property PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_TTL As Int64
    Public Property PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH As Int64
    Public Property PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG As Int64
    Public Property PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_TTL As Int64
    Public Property PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH As Int64
    Public Property PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG As Int64
    Public Property CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL As Int64
    Public Property CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH As Int64
    Public Property CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG As Int64
    Public Property PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_TTL As Int64
    Public Property PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH As Int64
    Public Property PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG As Int64
    Public Property PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_TTL As Int64
    Public Property PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH As Int64
    Public Property PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG As Int64
    Public Property PTNG_PMBYN__NT_TTL As Int64
    Public Property PTNG_PMBYN__NT_NDNSN_RPH As Int64
    Public Property PTNG_PMBYN__NT_MT_NG_SNG As Int64
    Public Property PNYRTN_MDL_PD_BNK_TTL As Int64
    Public Property PNYRTN_MDL_PD_BNK_NDNSN_RPH As Int64
    Public Property PNYRTN_MDL_PD_BNK_MT_NG_SNG As Int64
    Public Property PNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_TTL As Int64
    Public Property PNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH As Int64
    Public Property PNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG As Int64
    Public Property PNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_TTL As Int64
    Public Property PNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_NDNSN_RPH As Int64
    Public Property PNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_MT_NG_SNG As Int64
    Public Property PNYRTN_MDL_TTL As Int64
    Public Property PNYRTN_MDL_NDNSN_RPH As Int64
    Public Property PNYRTN_MDL_MT_NG_SNG As Int64
    Public Property NVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_TTL As Int64
    Public Property NVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_NDNSN_RPH As Int64
    Public Property NVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_MT_NG_SNG As Int64
    Public Property ST_YNG_DSWPRSKN_TTL As Int64
    Public Property ST_YNG_DSWPRSKN_NDNSN_RPH As Int64
    Public Property ST_YNG_DSWPRSKN_MT_NG_SNG As Int64
    Public Property KMLS_PNYSTN_ST_YNG_DSWPRSKN_TTL As Int64
    Public Property KMLS_PNYSTN_ST_YNG_DSWPRSKN_NDNSN_RPH As Int64
    Public Property KMLS_PNYSTN_ST_YNG_DSWPRSKN_MT_NG_SNG As Int64
    Public Property ST_YNG_DSWPRSKN__NT_TTL As Int64
    Public Property ST_YNG_DSWPRSKN__NT_NDNSN_RPH As Int64
    Public Property ST_YNG_DSWPRSKN__NT_MT_NG_SNG As Int64
    Public Property ST_TTP_DN_NVNTRS__TTL As Int64
    Public Property ST_TTP_DN_NVNTRS__NDNSN_RPH As Int64
    Public Property ST_TTP_DN_NVNTRS__MT_NG_SNG As Int64
    Public Property KMLS_PNYSTN_ST_TTP_DN_NVNTRS_TTL As Int64
    Public Property KMLS_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH As Int64
    Public Property KMLS_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG As Int64
    Public Property ST_TTP_DN_NVNTRS__NT_TTL As Int64
    Public Property ST_TTP_DN_NVNTRS__NT_NDNSN_RPH As Int64
    Public Property ST_TTP_DN_NVNTRS__NT_MT_NG_SNG As Int64
    Public Property ST_PJK_TNGGHN_TTL As Int64
    Public Property ST_PJK_TNGGHN_NDNSN_RPH As Int64
    Public Property ST_PJK_TNGGHN_MT_NG_SNG As Int64
    Public Property RPRP_ST_TTL As Int64
    Public Property RPRP_ST_NDNSN_RPH As Int64
    Public Property RPRP_ST_MT_NG_SNG As Int64
    Public Property ST_TTL As Int64
    Public Property ST_NDNSN_RPH As Int64
    Public Property ST_MT_NG_SNG As Int64
    Public Property LBLTS_KPD_BNK_TTL As Int64
    Public Property LBLTS_KPD_BNK_NDNSN_RPH As Int64
    Public Property LBLTS_KPD_BNK_MT_NG_SNG As Int64
    Public Property LBLTS_KPD_PRSHN_JS_KNGN_LNNY_TTL As Int64
    Public Property LBLTS_KPD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH As Int64
    Public Property LBLTS_KPD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG As Int64
    Public Property LBLTS_KPD_PRSHN_BKN_JS_KNGN_TTL As Int64
    Public Property LBLTS_KPD_PRSHN_BKN_JS_KNGN_NDNSN_RPH As Int64
    Public Property LBLTS_KPD_PRSHN_BKN_JS_KNGN_MT_NG_SNG As Int64
    Public Property LBLTS_SGR_LNNY_TTL As Int64
    Public Property LBLTS_SGR_LNNY_NDNSN_RPH As Int64
    Public Property LBLTS_SGR_LNNY_MT_NG_SNG As Int64
    Public Property LBLTS_SGR_TTL As Int64
    Public Property LBLTS_SGR_NDNSN_RPH As Int64
    Public Property LBLTS_SGR_MT_NG_SNG As Int64
    Public Property LBLTS_DRVTF_TTL As Int64
    Public Property LBLTS_DRVTF_NDNSN_RPH As Int64
    Public Property LBLTS_DRVTF_MT_NG_SNG As Int64
    Public Property TNG_PJK_TTL As Int64
    Public Property TNG_PJK_NDNSN_RPH As Int64
    Public Property TNG_PJK_MT_NG_SNG As Int64
    Public Property PNDNN_YNG_DTRM_DR_BNK_DLM_NGR_TTL As Int64
    Public Property PNDNN_YNG_DTRM_DR_BNK_DLM_NGR_NDNSN_RPH As Int64
    Public Property PNDNN_YNG_DTRM_DR_BNK_DLM_NGR_MT_NG_SNG As Int64
    Public Property PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_TTL As Int64
    Public Property PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_NDNSN_RPH As Int64
    Public Property PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_MT_NG_SNG As Int64
    Public Property PNDNN_YNG_DTRM_LNNY_DLM_NGR_TTL As Int64
    Public Property PNDNN_YNG_DTRM_LNNY_DLM_NGR_NDNSN_RPH As Int64
    Public Property PNDNN_YNG_DTRM_LNNY_DLM_NGR_MT_NG_SNG As Int64
    Public Property PNDNN_YNG_DTRM_DLM_NGR_TTL As Int64
    Public Property PNDNN_YNG_DTRM_DLM_NGR_NDNSN_RPH As Int64
    Public Property PNDNN_YNG_DTRM_DLM_NGR_MT_NG_SNG As Int64
    Public Property PNDNN_YNG_DTRM_DR_BNK_LR_NGR_TTL As Int64
    Public Property PNDNN_YNG_DTRM_DR_BNK_LR_NGR_NDNSN_RPH As Int64
    Public Property PNDNN_YNG_DTRM_DR_BNK_LR_NGR_MT_NG_SNG As Int64
    Public Property PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_TTL As Int64
    Public Property PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_NDNSN_RPH As Int64
    Public Property PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_MT_NG_SNG As Int64
    Public Property PNDNN_YNG_DTRM_LNNY_LR_NGR_TTL As Int64
    Public Property PNDNN_YNG_DTRM_LNNY_LR_NGR_NDNSN_RPH As Int64
    Public Property PNDNN_YNG_DTRM_LNNY_LR_NGR_MT_NG_SNG As Int64
    Public Property PNDNN_YNG_DTRM_LR_NGR_TTL As Int64
    Public Property PNDNN_YNG_DTRM_LR_NGR_NDNSN_RPH As Int64
    Public Property PNDNN_YNG_DTRM_LR_NGR_MT_NG_SNG As Int64
    Public Property PNDNN_YNG_DTRM_TTL As Int64
    Public Property PNDNN_YNG_DTRM_NDNSN_RPH As Int64
    Public Property PNDNN_YNG_DTRM_MT_NG_SNG As Int64
    Public Property SRT_BRHRG_YNG_DTRBTKN_TTL As Int64
    Public Property SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH As Int64
    Public Property SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG As Int64
    Public Property LBLTS_PJK_TNGGHN_TTL As Int64
    Public Property LBLTS_PJK_TNGGHN_NDNSN_RPH As Int64
    Public Property LBLTS_PJK_TNGGHN_MT_NG_SNG As Int64
    Public Property PNJMN_SBRDNS_DLM_NGR_TTL As Int64
    Public Property PNJMN_SBRDNS_DLM_NGR_NDNSN_RPH As Int64
    Public Property PNJMN_SBRDNS_DLM_NGR_MT_NG_SNG As Int64
    Public Property PNJMN_SBRDNS_LR_NGR_TTL As Int64
    Public Property PNJMN_SBRDNS_LR_NGR_NDNSN_RPH As Int64
    Public Property PNJMN_SBRDNS_LR_NGR_MT_NG_SNG As Int64
    Public Property PNJMN_SBRDNS_TTL As Int64
    Public Property PNJMN_SBRDNS_NDNSN_RPH As Int64
    Public Property PNJMN_SBRDNS_MT_NG_SNG As Int64
    Public Property RPRP_LBLTS_TTL As Int64
    Public Property RPRP_LBLTS_NDNSN_RPH As Int64
    Public Property RPRP_LBLTS_MT_NG_SNG As Int64
    Public Property MDL_DSR_TTL As Int64
    Public Property MDL_DSR_NDNSN_RPH As Int64
    Public Property MDL_DSR_MT_NG_SNG As Int64
    Public Property MDL_YNG_BLM_DSTR_TTL As Int64
    Public Property MDL_YNG_BLM_DSTR_NDNSN_RPH As Int64
    Public Property MDL_YNG_BLM_DSTR_MT_NG_SNG As Int64
    Public Property MDL_DSTR_TTL As Int64
    Public Property MDL_DSTR_NDNSN_RPH As Int64
    Public Property MDL_DSTR_MT_NG_SNG As Int64
    Public Property SMPNN_PKK_TTL As Int64
    Public Property SMPNN_PKK_NDNSN_RPH As Int64
    Public Property SMPNN_PKK_MT_NG_SNG As Int64
    Public Property SMPNN_WJB_TTL As Int64
    Public Property SMPNN_WJB_NDNSN_RPH As Int64
    Public Property SMPNN_WJB_MT_NG_SNG As Int64
    Public Property SMPNN_PKK_DN_SMPNN_WJB_TTL As Int64
    Public Property SMPNN_PKK_DN_SMPNN_WJB_NDNSN_RPH As Int64
    Public Property SMPNN_PKK_DN_SMPNN_WJB_MT_NG_SNG As Int64
    Public Property G_TTL As Int64
    Public Property G_NDNSN_RPH As Int64
    Public Property G_MT_NG_SNG As Int64
    Public Property DSG_TTL As Int64
    Public Property DSG_NDNSN_RPH As Int64
    Public Property DSG_MT_NG_SNG As Int64
    Public Property MDL_SHM_DPRLH_KMBL_TTL As Int64
    Public Property MDL_SHM_DPRLH_KMBL_NDNSN_RPH As Int64
    Public Property MDL_SHM_DPRLH_KMBL_MT_NG_SNG As Int64
    Public Property BY_MS_FK_KTS_TTL As Int64
    Public Property BY_MS_FK_KTS_NDNSN_RPH As Int64
    Public Property BY_MS_FK_KTS_MT_NG_SNG As Int64
    Public Property MDL_HBH_TTL As Int64
    Public Property MDL_HBH_NDNSN_RPH As Int64
    Public Property MDL_HBH_MT_NG_SNG As Int64
    Public Property TMBHN_MDL_DSTR_LNNY_TTL As Int64
    Public Property TMBHN_MDL_DSTR_LNNY_NDNSN_RPH As Int64
    Public Property TMBHN_MDL_DSTR_LNNY_MT_NG_SNG As Int64
    Public Property TMBHN_MDL_DSTR_TTL As Int64
    Public Property TMBHN_MDL_DSTR_NDNSN_RPH As Int64
    Public Property TMBHN_MDL_DSTR_MT_NG_SNG As Int64
    Public Property SLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_TTL As Int64
    Public Property SLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_NDNSN_RPH As Int64
    Public Property SLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_MT_NG_SNG As Int64
    Public Property MDL_TTL As Int64
    Public Property MDL_NDNSN_RPH As Int64
    Public Property MDL_MT_NG_SNG As Int64
    Public Property CDNGN_MM_TTL As Int64
    Public Property CDNGN_MM_NDNSN_RPH As Int64
    Public Property CDNGN_MM_MT_NG_SNG As Int64
    Public Property CDNGN_TJN_TTL As Int64
    Public Property CDNGN_TJN_NDNSN_RPH As Int64
    Public Property CDNGN_TJN_MT_NG_SNG As Int64
    Public Property CDNGN_MDL_TTL As Int64
    Public Property CDNGN_MDL_NDNSN_RPH As Int64
    Public Property CDNGN_MDL_MT_NG_SNG As Int64
    Public Property SLD_LB_RG_YNG_DTHN_TTL As Int64
    Public Property SLD_LB_RG_YNG_DTHN_NDNSN_RPH As Int64
    Public Property SLD_LB_RG_YNG_DTHN_MT_NG_SNG As Int64
    Public Property LB_RG_BRSH_STLH_PJK_TTL As Int64
    Public Property LB_RG_BRSH_STLH_PJK_NDNSN_RPH As Int64
    Public Property LB_RG_BRSH_STLH_PJK_MT_NG_SNG As Int64
    Public Property SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL As Int64
    Public Property SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH As Int64
    Public Property SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG As Int64
    Public Property SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL As Int64
    Public Property SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH As Int64
    Public Property SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG As Int64
    Public Property SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL As Int64
    Public Property SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH As Int64
    Public Property SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG As Int64
    Public Property SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL As Int64
    Public Property SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH As Int64
    Public Property SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG As Int64
    Public Property SLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL As Int64
    Public Property SLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH As Int64
    Public Property SLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG As Int64
    Public Property SLD_KMPNN_KTS_LNNY_TTL As Int64
    Public Property SLD_KMPNN_KTS_LNNY_NDNSN_RPH As Int64
    Public Property SLD_KMPNN_KTS_LNNY_MT_NG_SNG As Int64
    Public Property KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL As Int64
    Public Property KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH As Int64
    Public Property KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG As Int64
    Public Property KMPNN_KTS_LNNY_TTL As Int64
    Public Property KMPNN_KTS_LNNY_NDNSN_RPH As Int64
    Public Property KMPNN_KTS_LNNY_MT_NG_SNG As Int64
    Public Property LBLTS_DN_KTS_TTL As Int64
    Public Property LBLTS_DN_KTS_NDNSN_RPH As Int64
    Public Property LBLTS_DN_KTS_MT_NG_SNG As Int64


    Private _Masterid As String
    Private _Parentid As String
    Private _valuemaster As String
    Private _valueparent As String
    Private _valueelement As String

    Public Property ID As Int64
    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property
    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property

    Public Property Parentid() As String
        Get
            Return _Parentid
        End Get
        Set(ByVal Value As String)
            _Parentid = Value
        End Set
    End Property

    Public Property valuemaster() As String
        Get
            Return _valuemaster
        End Get
        Set(ByVal Value As String)
            _valuemaster = Value
        End Set
    End Property

    Public Property valueelement() As String
        Get
            Return _valueelement
        End Get
        Set(ByVal Value As String)
            _valueelement = Value
        End Set
    End Property

    Public Property valueparent() As String
        Get
            Return _valueparent
        End Get
        Set(ByVal Value As String)
            _valueparent = Value
        End Set
    End Property

    Public Property Masterid() As String
        Get
            Return _Masterid
        End Get
        Set(ByVal Value As String)
            _Masterid = Value
        End Set
    End Property



    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

End Class





