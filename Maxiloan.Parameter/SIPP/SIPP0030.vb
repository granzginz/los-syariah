﻿<Serializable()>
Public Class SIPP0030 : Inherits Common

    Private _Table As String
    Private _BULANDATA As String
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _NM_PMGNG_SHM As String
    Private _GLNGN_PHK_LWN As String
    Private _LKS_NGR_PHK_LWN As String
    Private _BNTK_HKM As String
    Private _STTS_PMGNG_SHM As String
    Private _KTS_PMGNG_SHM As Int64
    Private _NL_KPMLKN_SHM As Int64
    Private _PRSNTS_KPMLKN_SHM As String
    Private _NM_PNGRS_DRJT_KD As String
    Private _JBTN_KPNGRSN As String
    Private _KWRGNGRN As String
    Private _NM_PMGNG_SHM_DRJT_KD As String
    Private _GLNGN_KLMPK_PMGNG_SHM_DRJT_KD As String
    Private _LKS_NGR_PMGNG_SHM_DRJT_KD As String
    Private _NL_KPMLKN_SHM_DRJT_KD As Int64
    Private _valuemaster As String
    Private _valueparent As String
    Private _valueelement As String

    Public Property ID As Int64
    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property
    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property

    Public Property NM_PMGNG_SHM() As String
        Get
            Return _NM_PMGNG_SHM
        End Get
        Set(ByVal Value As String)
            _NM_PMGNG_SHM = Value
        End Set
    End Property

    Public Property valuemaster() As String
        Get
            Return _valuemaster
        End Get
        Set(ByVal Value As String)
            _valuemaster = Value
        End Set
    End Property

    Public Property valueelement() As String
        Get
            Return _valueelement
        End Get
        Set(ByVal Value As String)
            _valueelement = Value
        End Set
    End Property

    Public Property valueparent() As String
        Get
            Return _valueparent
        End Get
        Set(ByVal Value As String)
            _valueparent = Value
        End Set
    End Property

    Public Property GLNGN_PHK_LWN() As String
        Get
            Return _GLNGN_PHK_LWN
        End Get
        Set(ByVal Value As String)
            _GLNGN_PHK_LWN = Value
        End Set
    End Property

    Public Property LKS_NGR_PHK_LWN() As String
        Get
            Return _LKS_NGR_PHK_LWN
        End Get
        Set(ByVal Value As String)
            _LKS_NGR_PHK_LWN = Value
        End Set
    End Property

    Public Property BNTK_HKM() As String
        Get
            Return _BNTK_HKM
        End Get
        Set(ByVal Value As String)
            _BNTK_HKM = Value
        End Set
    End Property

    Public Property STTS_PMGNG_SHM() As String
        Get
            Return _STTS_PMGNG_SHM
        End Get
        Set(ByVal Value As String)
            _STTS_PMGNG_SHM = Value
        End Set
    End Property

    Public Property KTS_PMGNG_SHM() As Int64
        Get
            Return _KTS_PMGNG_SHM
        End Get
        Set(ByVal Value As Int64)
            _KTS_PMGNG_SHM = Value
        End Set
    End Property

    Public Property NL_KPMLKN_SHM As Int64
        Get
            Return _NL_KPMLKN_SHM
        End Get
        Set(ByVal Value As Int64)
            _NL_KPMLKN_SHM = Value
        End Set
    End Property

    Public Property PRSNTS_KPMLKN_SHM As String
        Get
            Return _PRSNTS_KPMLKN_SHM
        End Get
        Set(ByVal Value As String)
            _PRSNTS_KPMLKN_SHM = Value
        End Set
    End Property
    Public Property NM_PNGRS_DRJT_KD As String
        Get
            Return _NM_PNGRS_DRJT_KD
        End Get
        Set(ByVal Value As String)
            _NM_PNGRS_DRJT_KD = Value
        End Set
    End Property
    Public Property JBTN_KPNGRSN As String
        Get
            Return _JBTN_KPNGRSN
        End Get
        Set(ByVal Value As String)
            _JBTN_KPNGRSN = Value
        End Set
    End Property
    Public Property KWRGNGRN As String
        Get
            Return _KWRGNGRN
        End Get
        Set(ByVal Value As String)
            _KWRGNGRN = Value
        End Set
    End Property
    Public Property NM_PMGNG_SHM_DRJT_KD As String
        Get
            Return _NM_PMGNG_SHM_DRJT_KD
        End Get
        Set(ByVal Value As String)
            _NM_PMGNG_SHM_DRJT_KD = Value
        End Set
    End Property
    Public Property GLNGN_KLMPK_PMGNG_SHM_DRJT_KD As String
        Get
            Return _GLNGN_KLMPK_PMGNG_SHM_DRJT_KD
        End Get
        Set(ByVal Value As String)
            _GLNGN_KLMPK_PMGNG_SHM_DRJT_KD = Value
        End Set
    End Property
    Public Property LKS_NGR_PMGNG_SHM_DRJT_KD As String
        Get
            Return _LKS_NGR_PMGNG_SHM_DRJT_KD
        End Get
        Set(ByVal Value As String)
            _LKS_NGR_PMGNG_SHM_DRJT_KD = Value
        End Set
    End Property
    Public Property NL_KPMLKN_SHM_DRJT_KD As Int64
        Get
            Return _NL_KPMLKN_SHM_DRJT_KD
        End Get
        Set(ByVal Value As Int64)
            _NL_KPMLKN_SHM_DRJT_KD = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

End Class





