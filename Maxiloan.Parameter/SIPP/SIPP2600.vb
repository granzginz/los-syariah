﻿<Serializable()>
Public Class SIPP2600 : Inherits Common
    Private _ID As Int64
    Private _BULANDATA As String
    Private _NOSURATBERHARGA As String
    Private _JENISSURATBERHARGA As String
    Private _TGLMULAI As Date
    Private _TGJATUHTEMPO As Date
    Private _JENISSUKUBUNGA As String
    Private _TINGKATSUKUBUNGA As Int64
    Private _NILAINOMINAL As String
    Private _JENISVALUTA As String
    Private _SALDOPINJAMANASAL As String
    Private _SALDOPINJAMANRUPIAH As String
    Private _NAMAKREDITUR As String
    Private _GOLONGANKREDITUR As String
    Private _LOKASINEGARA As String

    Private _ListData As DataTable
    Private _ListDataReport As DataSet
    Private _TotalRecords As Int64
    Private _Table As String
    Private _valueparent As String
    Private _valuemaster As String
    Private _valueelement As String

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property

    Public Property NOSURATBERHARGA As String
        Get
            Return _NOSURATBERHARGA
        End Get
        Set(value As String)
            _NOSURATBERHARGA = value
        End Set
    End Property

    Public Property JENISSURATBERHARGA As String
        Get
            Return _JENISSURATBERHARGA
        End Get
        Set(value As String)
            _JENISSURATBERHARGA = value
        End Set
    End Property

    Public Property TGLMULAI As Date
        Get
            Return _TGLMULAI
        End Get
        Set(value As Date)
            _TGLMULAI = value
        End Set
    End Property

    Public Property TGJATUHTEMPO As Date
        Get
            Return _TGJATUHTEMPO
        End Get
        Set(value As Date)
            _TGJATUHTEMPO = value
        End Set
    End Property

    Public Property JENISSUKUBUNGA As String
        Get
            Return _JENISSUKUBUNGA
        End Get
        Set(value As String)
            _JENISSUKUBUNGA = value
        End Set
    End Property

    Public Property TINGKATSUKUBUNGA As Long
        Get
            Return _TINGKATSUKUBUNGA
        End Get
        Set(value As Long)
            _TINGKATSUKUBUNGA = value
        End Set
    End Property

    Public Property NILAINOMINAL As String
        Get
            Return _NILAINOMINAL
        End Get
        Set(value As String)
            _NILAINOMINAL = value
        End Set
    End Property

    Public Property JENISVALUTA As String
        Get
            Return _JENISVALUTA
        End Get
        Set(value As String)
            _JENISVALUTA = value
        End Set
    End Property

    Public Property SALDOPINJAMANASAL As String
        Get
            Return _SALDOPINJAMANASAL
        End Get
        Set(value As String)
            _SALDOPINJAMANASAL = value
        End Set
    End Property

    Public Property SALDOPINJAMANRUPIAH As String
        Get
            Return _SALDOPINJAMANRUPIAH
        End Get
        Set(value As String)
            _SALDOPINJAMANRUPIAH = value
        End Set
    End Property

    Public Property NAMAKREDITUR As String
        Get
            Return _NAMAKREDITUR
        End Get
        Set(value As String)
            _NAMAKREDITUR = value
        End Set
    End Property

    Public Property GOLONGANKREDITUR As String
        Get
            Return _GOLONGANKREDITUR
        End Get
        Set(value As String)
            _GOLONGANKREDITUR = value
        End Set
    End Property

    Public Property LOKASINEGARA As String
        Get
            Return _LOKASINEGARA
        End Get
        Set(value As String)
            _LOKASINEGARA = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property

    Public Property ListDataReport As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(value As DataSet)
            _ListDataReport = value
        End Set
    End Property

    Public Property TotalRecords As Long
        Get
            Return _TotalRecords
        End Get
        Set(value As Long)
            _TotalRecords = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property

    Public Property Valueparent As String
        Get
            Return _valueparent
        End Get
        Set(value As String)
            _valueparent = value
        End Set
    End Property

    Public Property Valuemaster As String
        Get
            Return _valuemaster
        End Get
        Set(value As String)
            _valuemaster = value
        End Set
    End Property

    Public Property Valueelement As String
        Get
            Return _valueelement
        End Get
        Set(value As String)
            _valueelement = value
        End Set
    End Property
End Class
