﻿<Serializable()>
Public Class SIPP1110 : Inherits Common
    Private _ID As Int64
    Private _BULANDATA As String
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_TTL As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_NDNSN_RPH As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_MT_NG_SNG As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_TTL As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_NDNSN_RPH As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_MT_NG_SNG As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_TTL As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_NDNSN_RPH As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_MT_NG_SNG As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_TTL As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_NDNSN_RPH As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_MT_NG_SNG As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_TTL As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_NDNSN_RPH As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_MT_NG_SNG As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_TTL As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_NDNSN_RPH As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_MT_NG_SNG As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_TTL As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_NDNSN_RPH As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_MT_NG_SNG As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_TTL As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_NDNSN_RPH As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_MT_NG_SNG As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_TTL As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_NDNSN_RPH As Double
    Private _FSLTS_PNDNN_YNG_BLM_DTRK_MT_NG_SNG As Double
    Private _FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_TTL As Double
    Private _FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_NDNSN_RPH As Double
    Private _FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_MT_NG_SNG As Double
    Private _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_TTL As Double
    Private _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_NDNSN_RPH As Double
    Private _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_MT_NG_SNG As Double
    Private _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_TTL As Double
    Private _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_NDNSN_RPH As Double
    Private _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_MT_NG_SNG As Double
    Private _PNRBTN_SRT_SNGGP_BYR_TTL As Double
    Private _PNRBTN_SRT_SNGGP_BYR_NDNSN_RPH As Double
    Private _PNRBTN_SRT_SNGGP_BYR_MT_NG_SNG As Double
    Private _PNRSN_KRDTPMBYN_CHNNLNG_TTL As Double
    Private _PNRSN_KRDTPMBYN_CHNNLNG_NDNSN_RPH As Double
    Private _PNRSN_KRDTPMBYN_CHNNLNG_MT_NG_SNG As Double
    Private _PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_TTL As Double
    Private _PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_NDNSN_RPH As Double
    Private _PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_MT_NG_SNG As Double
    Private _PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_TTL As Double
    Private _PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_NDNSN_RPH As Double
    Private _PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_MT_NG_SNG As Double
    Private _NMNL_NTRST_RT_SWP_TTL As Double
    Private _NMNL_NTRST_RT_SWP_NDNSN_RPH As Double
    Private _NMNL_NTRST_RT_SWP_MT_NG_SNG As Double
    Private _NMNL_CRRNCY_SWP_TTL As Double
    Private _NMNL_CRRNCY_SWP_NDNSN_RPH As Double
    Private _NMNL_CRRNCY_SWP_MT_NG_SNG As Double
    Private _NMNL_CRSS_CRRNCY_SWP_TTL As Double
    Private _NMNL_CRSS_CRRNCY_SWP_NDNSN_RPH As Double
    Private _NMNL_CRSS_CRRNCY_SWP_MT_NG_SNG As Double
    Private _NMNL_FRWRD_TTL As Double
    Private _NMNL_FRWRD_NDNSN_RPH As Double
    Private _NMNL_FRWRD_MT_NG_SNG As Double
    Private _NMNL_PTN_TTL As Double
    Private _NMNL_PTN_NDNSN_RPH As Double
    Private _NMNL_PTN_MT_NG_SNG As Double
    Private _NMNL_FTR_TTL As Double
    Private _NMNL_FTR_NDNSN_RPH As Double
    Private _NMNL_FTR_MT_NG_SNG As Double
    Private _NMNL_DRVTF_LNNY_TTL As Double
    Private _NMNL_DRVTF_LNNY_NDNSN_RPH As Double
    Private _NMNL_DRVTF_LNNY_MT_NG_SNG As Double
    Private _NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_TTL As Double
    Private _NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_NDNSN_RPH As Double
    Private _NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_MT_NG_SNG As Double
    Private _PTNG_PMBYN_HPS_BK_TTL As Double
    Private _PTNG_PMBYN_HPS_BK_NDNSN_RPH As Double
    Private _PTNG_PMBYN_HPS_BK_MT_NG_SNG As Double
    Private _PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_TTL As Double
    Private _PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_NDNSN_RPH As Double
    Private _PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_MT_NG_SNG As Double
    Private _PTNG_PMBYN_HPS_TGH_TTL As Double
    Private _PTNG_PMBYN_HPS_TGH_NDNSN_RPH As Double
    Private _PTNG_PMBYN_HPS_TGH_MT_NG_SNG As Double
    Private _RKNNG_DMNSTRTF_LNNY_TTL As Double
    Private _RKNNG_DMNSTRTF_LNNY_NDNSN_RPH As Double
    Private _RKNNG_DMNSTRTF_LNNY_MT_NG_SNG As Double
    Private _RKNNG_DMNSTRTF_TTL As Double
    Private _RKNNG_DMNSTRTF_NDNSN_RPH As Double
    Private _RKNNG_DMNSTRTF_MT_NG_SNG As Double

    Private _ListData As DataTable
    Private _ListDataReport As DataSet
    Private _TotalRecords As Int64
    Private _Table As String
    Private _valueparent As String
    Private _valuemaster As String
    Private _valueelement As String

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_TTL As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_TTL
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_TTL = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_NDNSN_RPH As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_NDNSN_RPH
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_NDNSN_RPH = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_MT_NG_SNG As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_MT_NG_SNG
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_MT_NG_SNG = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_TTL As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_TTL
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_TTL = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_NDNSN_RPH As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_NDNSN_RPH
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_NDNSN_RPH = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_MT_NG_SNG As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_MT_NG_SNG
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_MT_NG_SNG = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_TTL As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_TTL
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_TTL = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_NDNSN_RPH As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_NDNSN_RPH
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_NDNSN_RPH = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_MT_NG_SNG As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_MT_NG_SNG
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_MT_NG_SNG = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_TTL As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_TTL
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_TTL = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_NDNSN_RPH As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_NDNSN_RPH
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_NDNSN_RPH = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_MT_NG_SNG As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_MT_NG_SNG
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_MT_NG_SNG = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_TTL As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_TTL
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_TTL = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_NDNSN_RPH As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_NDNSN_RPH
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_NDNSN_RPH = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_MT_NG_SNG As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_MT_NG_SNG
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_MT_NG_SNG = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_TTL As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_TTL
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_TTL = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_NDNSN_RPH As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_NDNSN_RPH
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_NDNSN_RPH = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_MT_NG_SNG As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_MT_NG_SNG
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_MT_NG_SNG = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_TTL As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_TTL
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_TTL = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_NDNSN_RPH As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_NDNSN_RPH
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_NDNSN_RPH = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_MT_NG_SNG As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_MT_NG_SNG
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_MT_NG_SNG = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_TTL As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_TTL
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_TTL = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_NDNSN_RPH As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_NDNSN_RPH
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_NDNSN_RPH = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_MT_NG_SNG As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_MT_NG_SNG
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_MT_NG_SNG = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_TTL As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_TTL
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_TTL = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_NDNSN_RPH As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_NDNSN_RPH
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_NDNSN_RPH = value
        End Set
    End Property

    Public Property FSLTS_PNDNN_YNG_BLM_DTRK_MT_NG_SNG As Double
        Get
            Return _FSLTS_PNDNN_YNG_BLM_DTRK_MT_NG_SNG
        End Get
        Set(value As Double)
            _FSLTS_PNDNN_YNG_BLM_DTRK_MT_NG_SNG = value
        End Set
    End Property

    Public Property FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_TTL As Double
        Get
            Return _FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_TTL
        End Get
        Set(value As Double)
            _FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_TTL = value
        End Set
    End Property

    Public Property FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_NDNSN_RPH As Double
        Get
            Return _FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_NDNSN_RPH
        End Get
        Set(value As Double)
            _FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_NDNSN_RPH = value
        End Set
    End Property

    Public Property FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_MT_NG_SNG As Double
        Get
            Return _FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_MT_NG_SNG
        End Get
        Set(value As Double)
            _FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_MT_NG_SNG = value
        End Set
    End Property

    Public Property PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_TTL As Double
        Get
            Return _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_TTL
        End Get
        Set(value As Double)
            _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_TTL = value
        End Set
    End Property

    Public Property PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_NDNSN_RPH As Double
        Get
            Return _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_NDNSN_RPH
        End Get
        Set(value As Double)
            _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_NDNSN_RPH = value
        End Set
    End Property

    Public Property PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_MT_NG_SNG As Double
        Get
            Return _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_MT_NG_SNG
        End Get
        Set(value As Double)
            _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_MT_NG_SNG = value
        End Set
    End Property

    Public Property PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_TTL As Double
        Get
            Return _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_TTL
        End Get
        Set(value As Double)
            _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_TTL = value
        End Set
    End Property

    Public Property PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_NDNSN_RPH As Double
        Get
            Return _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_NDNSN_RPH
        End Get
        Set(value As Double)
            _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_NDNSN_RPH = value
        End Set
    End Property

    Public Property PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_MT_NG_SNG As Double
        Get
            Return _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_MT_NG_SNG
        End Get
        Set(value As Double)
            _PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_MT_NG_SNG = value
        End Set
    End Property

    Public Property PNRBTN_SRT_SNGGP_BYR_TTL As Double
        Get
            Return _PNRBTN_SRT_SNGGP_BYR_TTL
        End Get
        Set(value As Double)
            _PNRBTN_SRT_SNGGP_BYR_TTL = value
        End Set
    End Property

    Public Property PNRBTN_SRT_SNGGP_BYR_NDNSN_RPH As Double
        Get
            Return _PNRBTN_SRT_SNGGP_BYR_NDNSN_RPH
        End Get
        Set(value As Double)
            _PNRBTN_SRT_SNGGP_BYR_NDNSN_RPH = value
        End Set
    End Property

    Public Property PNRBTN_SRT_SNGGP_BYR_MT_NG_SNG As Double
        Get
            Return _PNRBTN_SRT_SNGGP_BYR_MT_NG_SNG
        End Get
        Set(value As Double)
            _PNRBTN_SRT_SNGGP_BYR_MT_NG_SNG = value
        End Set
    End Property

    Public Property PNRSN_KRDTPMBYN_CHNNLNG_TTL As Double
        Get
            Return _PNRSN_KRDTPMBYN_CHNNLNG_TTL
        End Get
        Set(value As Double)
            _PNRSN_KRDTPMBYN_CHNNLNG_TTL = value
        End Set
    End Property

    Public Property PNRSN_KRDTPMBYN_CHNNLNG_NDNSN_RPH As Double
        Get
            Return _PNRSN_KRDTPMBYN_CHNNLNG_NDNSN_RPH
        End Get
        Set(value As Double)
            _PNRSN_KRDTPMBYN_CHNNLNG_NDNSN_RPH = value
        End Set
    End Property

    Public Property PNRSN_KRDTPMBYN_CHNNLNG_MT_NG_SNG As Double
        Get
            Return _PNRSN_KRDTPMBYN_CHNNLNG_MT_NG_SNG
        End Get
        Set(value As Double)
            _PNRSN_KRDTPMBYN_CHNNLNG_MT_NG_SNG = value
        End Set
    End Property

    Public Property PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_TTL As Double
        Get
            Return _PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_TTL
        End Get
        Set(value As Double)
            _PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_TTL = value
        End Set
    End Property

    Public Property PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_NDNSN_RPH As Double
        Get
            Return _PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_NDNSN_RPH
        End Get
        Set(value As Double)
            _PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_NDNSN_RPH = value
        End Set
    End Property

    Public Property PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_MT_NG_SNG As Double
        Get
            Return _PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_MT_NG_SNG
        End Get
        Set(value As Double)
            _PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_MT_NG_SNG = value
        End Set
    End Property

    Public Property PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_TTL As Double
        Get
            Return _PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_TTL
        End Get
        Set(value As Double)
            _PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_TTL = value
        End Set
    End Property

    Public Property PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_NDNSN_RPH As Double
        Get
            Return _PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_NDNSN_RPH
        End Get
        Set(value As Double)
            _PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_NDNSN_RPH = value
        End Set
    End Property

    Public Property PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_MT_NG_SNG As Double
        Get
            Return _PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_MT_NG_SNG
        End Get
        Set(value As Double)
            _PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_MT_NG_SNG = value
        End Set
    End Property

    Public Property NMNL_NTRST_RT_SWP_TTL As Double
        Get
            Return _NMNL_NTRST_RT_SWP_TTL
        End Get
        Set(value As Double)
            _NMNL_NTRST_RT_SWP_TTL = value
        End Set
    End Property

    Public Property NMNL_NTRST_RT_SWP_NDNSN_RPH As Double
        Get
            Return _NMNL_NTRST_RT_SWP_NDNSN_RPH
        End Get
        Set(value As Double)
            _NMNL_NTRST_RT_SWP_NDNSN_RPH = value
        End Set
    End Property

    Public Property NMNL_NTRST_RT_SWP_MT_NG_SNG As Double
        Get
            Return _NMNL_NTRST_RT_SWP_MT_NG_SNG
        End Get
        Set(value As Double)
            _NMNL_NTRST_RT_SWP_MT_NG_SNG = value
        End Set
    End Property

    Public Property NMNL_CRRNCY_SWP_TTL As Double
        Get
            Return _NMNL_CRRNCY_SWP_TTL
        End Get
        Set(value As Double)
            _NMNL_CRRNCY_SWP_TTL = value
        End Set
    End Property

    Public Property NMNL_CRRNCY_SWP_NDNSN_RPH As Double
        Get
            Return _NMNL_CRRNCY_SWP_NDNSN_RPH
        End Get
        Set(value As Double)
            _NMNL_CRRNCY_SWP_NDNSN_RPH = value
        End Set
    End Property

    Public Property NMNL_CRRNCY_SWP_MT_NG_SNG As Double
        Get
            Return _NMNL_CRRNCY_SWP_MT_NG_SNG
        End Get
        Set(value As Double)
            _NMNL_CRRNCY_SWP_MT_NG_SNG = value
        End Set
    End Property

    Public Property NMNL_CRSS_CRRNCY_SWP_TTL As Double
        Get
            Return _NMNL_CRSS_CRRNCY_SWP_TTL
        End Get
        Set(value As Double)
            _NMNL_CRSS_CRRNCY_SWP_TTL = value
        End Set
    End Property

    Public Property NMNL_CRSS_CRRNCY_SWP_NDNSN_RPH As Double
        Get
            Return _NMNL_CRSS_CRRNCY_SWP_NDNSN_RPH
        End Get
        Set(value As Double)
            _NMNL_CRSS_CRRNCY_SWP_NDNSN_RPH = value
        End Set
    End Property

    Public Property NMNL_CRSS_CRRNCY_SWP_MT_NG_SNG As Double
        Get
            Return _NMNL_CRSS_CRRNCY_SWP_MT_NG_SNG
        End Get
        Set(value As Double)
            _NMNL_CRSS_CRRNCY_SWP_MT_NG_SNG = value
        End Set
    End Property

    Public Property NMNL_FRWRD_TTL As Double
        Get
            Return _NMNL_FRWRD_TTL
        End Get
        Set(value As Double)
            _NMNL_FRWRD_TTL = value
        End Set
    End Property

    Public Property NMNL_FRWRD_NDNSN_RPH As Double
        Get
            Return _NMNL_FRWRD_NDNSN_RPH
        End Get
        Set(value As Double)
            _NMNL_FRWRD_NDNSN_RPH = value
        End Set
    End Property

    Public Property NMNL_FRWRD_MT_NG_SNG As Double
        Get
            Return _NMNL_FRWRD_MT_NG_SNG
        End Get
        Set(value As Double)
            _NMNL_FRWRD_MT_NG_SNG = value
        End Set
    End Property

    Public Property NMNL_PTN_TTL As Double
        Get
            Return _NMNL_PTN_TTL
        End Get
        Set(value As Double)
            _NMNL_PTN_TTL = value
        End Set
    End Property

    Public Property NMNL_PTN_NDNSN_RPH As Double
        Get
            Return _NMNL_PTN_NDNSN_RPH
        End Get
        Set(value As Double)
            _NMNL_PTN_NDNSN_RPH = value
        End Set
    End Property

    Public Property NMNL_PTN_MT_NG_SNG As Double
        Get
            Return _NMNL_PTN_MT_NG_SNG
        End Get
        Set(value As Double)
            _NMNL_PTN_MT_NG_SNG = value
        End Set
    End Property

    Public Property NMNL_FTR_TTL As Double
        Get
            Return _NMNL_FTR_TTL
        End Get
        Set(value As Double)
            _NMNL_FTR_TTL = value
        End Set
    End Property

    Public Property NMNL_FTR_NDNSN_RPH As Double
        Get
            Return _NMNL_FTR_NDNSN_RPH
        End Get
        Set(value As Double)
            _NMNL_FTR_NDNSN_RPH = value
        End Set
    End Property

    Public Property NMNL_FTR_MT_NG_SNG As Double
        Get
            Return _NMNL_FTR_MT_NG_SNG
        End Get
        Set(value As Double)
            _NMNL_FTR_MT_NG_SNG = value
        End Set
    End Property

    Public Property NMNL_DRVTF_LNNY_TTL As Double
        Get
            Return _NMNL_DRVTF_LNNY_TTL
        End Get
        Set(value As Double)
            _NMNL_DRVTF_LNNY_TTL = value
        End Set
    End Property

    Public Property NMNL_DRVTF_LNNY_NDNSN_RPH As Double
        Get
            Return _NMNL_DRVTF_LNNY_NDNSN_RPH
        End Get
        Set(value As Double)
            _NMNL_DRVTF_LNNY_NDNSN_RPH = value
        End Set
    End Property

    Public Property NMNL_DRVTF_LNNY_MT_NG_SNG As Double
        Get
            Return _NMNL_DRVTF_LNNY_MT_NG_SNG
        End Get
        Set(value As Double)
            _NMNL_DRVTF_LNNY_MT_NG_SNG = value
        End Set
    End Property

    Public Property NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_TTL As Double
        Get
            Return _NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_TTL
        End Get
        Set(value As Double)
            _NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_TTL = value
        End Set
    End Property

    Public Property NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_NDNSN_RPH As Double
        Get
            Return _NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_NDNSN_RPH
        End Get
        Set(value As Double)
            _NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_NDNSN_RPH = value
        End Set
    End Property

    Public Property NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_MT_NG_SNG As Double
        Get
            Return _NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_MT_NG_SNG
        End Get
        Set(value As Double)
            _NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_MT_NG_SNG = value
        End Set
    End Property

    Public Property PTNG_PMBYN_HPS_BK_TTL As Double
        Get
            Return _PTNG_PMBYN_HPS_BK_TTL
        End Get
        Set(value As Double)
            _PTNG_PMBYN_HPS_BK_TTL = value
        End Set
    End Property

    Public Property PTNG_PMBYN_HPS_BK_NDNSN_RPH As Double
        Get
            Return _PTNG_PMBYN_HPS_BK_NDNSN_RPH
        End Get
        Set(value As Double)
            _PTNG_PMBYN_HPS_BK_NDNSN_RPH = value
        End Set
    End Property

    Public Property PTNG_PMBYN_HPS_BK_MT_NG_SNG As Double
        Get
            Return _PTNG_PMBYN_HPS_BK_MT_NG_SNG
        End Get
        Set(value As Double)
            _PTNG_PMBYN_HPS_BK_MT_NG_SNG = value
        End Set
    End Property

    Public Property PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_TTL As Double
        Get
            Return _PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_TTL
        End Get
        Set(value As Double)
            _PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_TTL = value
        End Set
    End Property

    Public Property PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_NDNSN_RPH As Double
        Get
            Return _PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_NDNSN_RPH
        End Get
        Set(value As Double)
            _PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_NDNSN_RPH = value
        End Set
    End Property

    Public Property PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_MT_NG_SNG As Double
        Get
            Return _PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_MT_NG_SNG
        End Get
        Set(value As Double)
            _PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_MT_NG_SNG = value
        End Set
    End Property

    Public Property PTNG_PMBYN_HPS_TGH_TTL As Double
        Get
            Return _PTNG_PMBYN_HPS_TGH_TTL
        End Get
        Set(value As Double)
            _PTNG_PMBYN_HPS_TGH_TTL = value
        End Set
    End Property

    Public Property PTNG_PMBYN_HPS_TGH_NDNSN_RPH As Double
        Get
            Return _PTNG_PMBYN_HPS_TGH_NDNSN_RPH
        End Get
        Set(value As Double)
            _PTNG_PMBYN_HPS_TGH_NDNSN_RPH = value
        End Set
    End Property

    Public Property PTNG_PMBYN_HPS_TGH_MT_NG_SNG As Double
        Get
            Return _PTNG_PMBYN_HPS_TGH_MT_NG_SNG
        End Get
        Set(value As Double)
            _PTNG_PMBYN_HPS_TGH_MT_NG_SNG = value
        End Set
    End Property

    Public Property RKNNG_DMNSTRTF_LNNY_TTL As Double
        Get
            Return _RKNNG_DMNSTRTF_LNNY_TTL
        End Get
        Set(value As Double)
            _RKNNG_DMNSTRTF_LNNY_TTL = value
        End Set
    End Property

    Public Property RKNNG_DMNSTRTF_LNNY_NDNSN_RPH As Double
        Get
            Return _RKNNG_DMNSTRTF_LNNY_NDNSN_RPH
        End Get
        Set(value As Double)
            _RKNNG_DMNSTRTF_LNNY_NDNSN_RPH = value
        End Set
    End Property

    Public Property RKNNG_DMNSTRTF_LNNY_MT_NG_SNG As Double
        Get
            Return _RKNNG_DMNSTRTF_LNNY_MT_NG_SNG
        End Get
        Set(value As Double)
            _RKNNG_DMNSTRTF_LNNY_MT_NG_SNG = value
        End Set
    End Property

    Public Property RKNNG_DMNSTRTF_TTL As Double
        Get
            Return _RKNNG_DMNSTRTF_TTL
        End Get
        Set(value As Double)
            _RKNNG_DMNSTRTF_TTL = value
        End Set
    End Property

    Public Property RKNNG_DMNSTRTF_NDNSN_RPH As Double
        Get
            Return _RKNNG_DMNSTRTF_NDNSN_RPH
        End Get
        Set(value As Double)
            _RKNNG_DMNSTRTF_NDNSN_RPH = value
        End Set
    End Property

    Public Property RKNNG_DMNSTRTF_MT_NG_SNG As Double
        Get
            Return _RKNNG_DMNSTRTF_MT_NG_SNG
        End Get
        Set(value As Double)
            _RKNNG_DMNSTRTF_MT_NG_SNG = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property

    Public Property ListDataReport As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(value As DataSet)
            _ListDataReport = value
        End Set
    End Property

    Public Property TotalRecords As Long
        Get
            Return _TotalRecords
        End Get
        Set(value As Long)
            _TotalRecords = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property

    Public Property Valueparent As String
        Get
            Return _valueparent
        End Get
        Set(value As String)
            _valueparent = value
        End Set
    End Property

    Public Property Valuemaster As String
        Get
            Return _valuemaster
        End Get
        Set(value As String)
            _valuemaster = value
        End Set
    End Property

    Public Property Valueelement As String
        Get
            Return _valueelement
        End Get
        Set(value As String)
            _valueelement = value
        End Set
    End Property
End Class
