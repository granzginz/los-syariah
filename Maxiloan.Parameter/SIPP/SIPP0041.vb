﻿<Serializable()>
Public Class SIPP0041 : Inherits Common

    Private _BULANDATA As String
    Private _Table As String
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_ As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_ As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_ As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN As Int64

    'TOTAL KESELURUHAN PEGAWAI KONTRAK PUSAT
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN As Int64
    'TOTAL KESELURUHAN PEGAWAI KONTRAK CABANG
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN As Int64
    'TOTAL KESELURUHAN PEGAWAI OUTSOURCHING CABANG
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN As Int64
    'PNDDKN LAINNYA
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN As Int64
    'PNDDKN SLTA
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN As Int64
    'PNDDKN DIPLOMA
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN As Int64
    'PNDDKN SARJANA
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN As Int64
    'PNDDKN PASCA SARJANA
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN As Int64
    'KESELURUHAN PEGAWAI TETAP SELAIN CABANG
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN As Int64
    'KESELURUHAN PEGAWAI KONTRAK SELAIN CABANG
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN As Int64
    'KESELURUHAN PEGAWAI OUTSOURCHING SELAIN CABANG
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN As Int64
    'PNDDKN LAINNYA
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN As Int64
    'PNDDKN SLTA                                                                  '
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN As Int64
    'PNDDKN DIPLOMA                                                                 
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN As Int64
    'PNDDKN SARJANA                                                                  
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN As Int64
    'PNDDKN PASCA SARJANA                                                            
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN As Int64

    Public Property JMLH_TNG_KRJ_KNTR_PST_TTL__ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_PST_SLT_TTL_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_PST_DPLM_TTL_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_PST_SRJN_TTL_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TTL_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TTL__ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SLT_TTL_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TTL_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TTL_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TTL_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL__ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TTL_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TTL_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TTL_ As Int64
    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TTL_ As Int64
    Public Property JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_ As Int64
    Public Property JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_LKLK As Int64
    Public Property JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_ As Int64
    Public Property JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_LKLK As Int64
    Public Property JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_ As Int64
    Public Property JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_LKLK As Int64
    Public Property JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_PRMPN As Int64
    Public Property JMLH_TNG_KRJ_TTL__ As Int64

    Public Property ID As Int64
    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property
    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_ = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_ = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_ = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN() As String
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN
        End Get
        Set(ByVal Value As String)
            _JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN = Value
        End Set
    End Property


    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

End Class





