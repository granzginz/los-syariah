﻿<Serializable()>
Public Class SIPP2550 : Inherits Common

    Private _Table As String
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _NMR_KNTRK As String
    Private _SFT_PNDNN As String
    Private _JNS_MT_NG As String
    Private _TNGGL_ML As Date
    Private _TNGGL_JTH_TMP As Date
    Private _JNS_SK_BNG As String
    Private _TNGKT_BNG As Decimal
    Private _PLFN_DLM_MT_NG_SL As Int64
    Private _PLFN As Int64
    Private _NL_TRCTT_DLM_MT_NG_SL As Int64
    Private _NL_TRCTT As Int64
    Private _PNDNN_YNG_DTRM_DLM_MT_NG_SL As Int64
    Private _PNDNN_YNG_DTRM As Int64
    Private _NM_KRDTR As String
    Private _GLNGN_PHK_LWN As String
    Private _STTS_KTRKTN As String
    Private _LKS_NGR_PHK_LWN As String
    Private _valuemaster As String
    Private _valueparent As String
    Private _valueelement As String
    Private _BULANDATA As String

    Public Property ID As Int64
    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property
    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property

    Public Property NMR_KNTRK() As String
        Get
            Return _NMR_KNTRK
        End Get
        Set(ByVal Value As String)
            _NMR_KNTRK = Value
        End Set
    End Property

    Public Property valuemaster() As String
        Get
            Return _valuemaster
        End Get
        Set(ByVal Value As String)
            _valuemaster = Value
        End Set
    End Property

    Public Property valueparent() As String
        Get
            Return _valueparent
        End Get
        Set(ByVal Value As String)
            _valueparent = Value
        End Set
    End Property

    Public Property valueelement() As String
        Get
            Return _valueelement
        End Get
        Set(ByVal Value As String)
            _valueelement = Value
        End Set
    End Property

    Public Property SFT_PNDNN() As String
        Get
            Return _SFT_PNDNN
        End Get
        Set(ByVal Value As String)
            _SFT_PNDNN = Value
        End Set
    End Property

    Public Property JNS_MT_NG() As String
        Get
            Return _JNS_MT_NG
        End Get
        Set(ByVal Value As String)
            _JNS_MT_NG = Value
        End Set
    End Property

    Public Property TNGGL_ML() As Date
        Get
            Return _TNGGL_ML
        End Get
        Set(ByVal Value As Date)
            _TNGGL_ML = Value
        End Set
    End Property

    Public Property TNGGL_JTH_TMP() As Date
        Get
            Return _TNGGL_JTH_TMP
        End Get
        Set(ByVal Value As Date)
            _TNGGL_JTH_TMP = Value
        End Set
    End Property

    Public Property JNS_SK_BNG() As String
        Get
            Return _JNS_SK_BNG
        End Get
        Set(ByVal Value As String)
            _JNS_SK_BNG = Value
        End Set
    End Property

    Public Property TNGKT_BNG() As Decimal
        Get
            Return _TNGKT_BNG
        End Get
        Set(ByVal Value As Decimal)
            _TNGKT_BNG = Value
        End Set
    End Property
    Public Property PLFN_DLM_MT_NG_SL() As String
        Get
            Return _PLFN_DLM_MT_NG_SL
        End Get
        Set(ByVal Value As String)
            _PLFN_DLM_MT_NG_SL = Value
        End Set
    End Property
    Public Property PLFN() As String
        Get
            Return _PLFN
        End Get
        Set(ByVal Value As String)
            _PLFN = Value
        End Set
    End Property
    Public Property NL_TRCTT_DLM_MT_NG_SL() As String
        Get
            Return _NL_TRCTT_DLM_MT_NG_SL
        End Get
        Set(ByVal Value As String)
            _NL_TRCTT_DLM_MT_NG_SL = Value
        End Set
    End Property
    Public Property NL_TRCTT() As String
        Get
            Return _NL_TRCTT
        End Get
        Set(ByVal Value As String)
            _NL_TRCTT = Value
        End Set
    End Property
    Public Property PNDNN_YNG_DTRM_DLM_MT_NG_SL() As String
        Get
            Return _PNDNN_YNG_DTRM_DLM_MT_NG_SL
        End Get
        Set(ByVal Value As String)
            _PNDNN_YNG_DTRM_DLM_MT_NG_SL = Value
        End Set
    End Property
    Public Property PNDNN_YNG_DTRM() As String
        Get
            Return _PNDNN_YNG_DTRM
        End Get
        Set(ByVal Value As String)
            _PNDNN_YNG_DTRM = Value
        End Set
    End Property
    Public Property NM_KRDTR() As String
        Get
            Return _NM_KRDTR
        End Get
        Set(ByVal Value As String)
            _NM_KRDTR = Value
        End Set
    End Property
    Public Property GLNGN_PHK_LWN() As String
        Get
            Return _GLNGN_PHK_LWN
        End Get
        Set(ByVal Value As String)
            _GLNGN_PHK_LWN = Value
        End Set
    End Property
    Public Property STTS_KTRKTN() As String
        Get
            Return _STTS_KTRKTN
        End Get
        Set(ByVal Value As String)
            _STTS_KTRKTN = Value
        End Set
    End Property
    Public Property LKS_NGR_PHK_LWN() As String
        Get
            Return _LKS_NGR_PHK_LWN
        End Get
        Set(ByVal Value As String)
            _LKS_NGR_PHK_LWN = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

End Class





