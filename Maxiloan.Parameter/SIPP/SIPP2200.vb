﻿<Serializable()>
Public Class SIPP2200 : Inherits Common

    Private _Table As String
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _NMR_SRT_BRHRG As String
    Private _JNS_SRT_BRHRG As String
    Private _TNGGL_PNRBTN As Date
    Private _TNGGL_JTH_TMP As Date
    Private _TJN_KPMLKN As String
    Private _NMR_TLPN As String
    Private _JNS_SK_BNG As String
    Private _TNGKT_SK_BNG As Decimal
    Private _JNS_VLT As String
    Private _KUALITAS As String
    Private _NL_MT_NG_SL As Int64
    Private _KVLN_RPH As Int64
    Private _NM As String
    Private _NGR As String
    Private _GLNGN As String
    Private _STTS_KTRKTN As String
    Private _Masterid As String
    Private _Parentid As String
    Private _valuemaster As String
    Private _valueparent As String
    Private _valueelement As String
    Private _BULANDATA As String



    Public Property ID As Int64
    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property
    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property

    Public Property Parentid() As String
        Get
            Return _Parentid
        End Get
        Set(ByVal Value As String)
            _Parentid = Value
        End Set
    End Property

    Public Property valuemaster() As String
        Get
            Return _valuemaster
        End Get
        Set(ByVal Value As String)
            _valuemaster = Value
        End Set
    End Property

    Public Property valueparent() As String
        Get
            Return _valueparent
        End Get
        Set(ByVal Value As String)
            _valueparent = Value
        End Set
    End Property

    Public Property Masterid() As String
        Get
            Return _Masterid
        End Get
        Set(ByVal Value As String)
            _Masterid = Value
        End Set
    End Property

    Public Property valueelement() As String
        Get
            Return _valueelement
        End Get
        Set(ByVal Value As String)
            _valueelement = Value
        End Set
    End Property

    Public Property NMR_SRT_BRHRG() As String
        Get
            Return _NMR_SRT_BRHRG
        End Get
        Set(ByVal Value As String)
            _NMR_SRT_BRHRG = Value
        End Set
    End Property

    Public Property JNS_SRT_BRHRG() As String
        Get
            Return _JNS_SRT_BRHRG
        End Get
        Set(ByVal Value As String)
            _JNS_SRT_BRHRG = Value
        End Set
    End Property

    Public Property TNGGL_PNRBTN() As Date
        Get
            Return _TNGGL_PNRBTN
        End Get
        Set(ByVal Value As Date)
            _TNGGL_PNRBTN = Value
        End Set
    End Property

    Public Property TNGGL_JTH_TMP() As Date
        Get
            Return _TNGGL_JTH_TMP
        End Get
        Set(ByVal Value As Date)
            _TNGGL_JTH_TMP = Value
        End Set
    End Property

    Public Property TJN_KPMLKN() As String
        Get
            Return _TJN_KPMLKN
        End Get
        Set(ByVal Value As String)
            _TJN_KPMLKN = Value
        End Set
    End Property

    Public Property NMR_TLPN() As String
        Get
            Return _NMR_TLPN
        End Get
        Set(ByVal Value As String)
            _NMR_TLPN = Value
        End Set
    End Property

    Public Property JNS_SK_BNG() As String
        Get
            Return _JNS_SK_BNG
        End Get
        Set(ByVal Value As String)
            _JNS_SK_BNG = Value
        End Set
    End Property

    Public Property KUALITAS() As String
        Get
            Return _KUALITAS
        End Get
        Set(ByVal Value As String)
            _KUALITAS = Value
        End Set
    End Property

    Public Property TNGKT_SK_BNG() As Decimal
        Get
            Return _TNGKT_SK_BNG
        End Get
        Set(ByVal Value As Decimal)
            _TNGKT_SK_BNG = Value
        End Set
    End Property

    Public Property JNS_VLT() As String
        Get
            Return _JNS_VLT
        End Get
        Set(ByVal Value As String)
            _JNS_VLT = Value
        End Set
    End Property

    Public Property NL_MT_NG_SL() As Int64
        Get
            Return _NL_MT_NG_SL
        End Get
        Set(ByVal Value As Int64)
            _NL_MT_NG_SL = Value
        End Set
    End Property

    Public Property KVLN_RPH() As Int64
        Get
            Return _KVLN_RPH
        End Get
        Set(ByVal Value As Int64)
            _KVLN_RPH = Value
        End Set
    End Property

    Public Property NM() As String
        Get
            Return _NM
        End Get
        Set(ByVal Value As String)
            _NM = Value
        End Set
    End Property

    Public Property NGR() As String
        Get
            Return _NGR
        End Get
        Set(ByVal Value As String)
            _NGR = Value
        End Set
    End Property

    Public Property GLNGN() As String
        Get
            Return _GLNGN
        End Get
        Set(ByVal Value As String)
            _GLNGN = Value
        End Set
    End Property

    Public Property STTS_KTRKTN() As String
        Get
            Return _STTS_KTRKTN
        End Get
        Set(ByVal Value As String)
            _STTS_KTRKTN = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

End Class





