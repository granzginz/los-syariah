﻿<Serializable()>
Public Class SIPP2100 : Inherits Common

    Private _Table As String
    Private _BULANDATA As String
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _NMR_KNTRK As String
    Private _JNS_PMBYN As String
    Private _SKM_PMBYN As String
    Private _TJN_PMBYN As String
    Private _JNS_BRNG_DN_JS As String
    Private _NL_BRNGJS_YNG_DBY As Int64
    Private _TNGGL_ML As Date
    Private _TNGGL_JTH_TMP As Date
    Private _JNS_BNGMRGNBG_HSLMBL_JS As String
    Private _NL_MRGNMBL_JS As Int64
    Private _TNGKT_BNGBG_HSL As Decimal
    Private _NL_TRCTT As Int64
    Private _SMBR_PMBYN As String
    Private _PRS_PRSHN_PD_PMBYN_BRSM As Int64
    Private _NG_MK As Int64
    Private _KLTS As String
    Private _JNS_MT_NG As String
    Private _TGHN_PMBYN_DLM_MT_NG_SL As Int64
    Private _TGHN_PMBYN As Int64
    Private _BNGMRGN_DTNGGHKN_DLM_MT_NG_SL As Int64
    Private _BNGMRGN_DTNGGHKN As Int64
    Private _PTNG_PMBYN_PKK_DLM_MT_NG_SL As Int64
    Private _PTNG_PMBYN__PKK As Int64
    Private _PRPRS_PNJMNN_KRDT_T_SRNS_KRDT As Decimal
    Private _NMR_DBTR As String
    Private _NM_DBTR As String
    Private _GRP_PHK_LWN As String
    Private _KTGR_SH_DBTR As String
    Private _GLNGN_PHK_LWN As String
    Private _STTS_KTRKTN As String
    Private _SKTR_KNM_LPNGN_SH As String
    Private _LKS_DT__PRYK As String
    Private _NMR_GNN As String
    Private _JNS_GNN As String
    Private _NL_GNNJMNN_YNG_DPT_DPRHTNGKN As String
    Private _Masterid As String
    Private _Parentid As String
    Private _valuemaster As String
    Private _valueparent As String
    Private _valueelement As String



    Public Property ID As Int64
    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property
    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property

    Public Property Parentid() As String
        Get
            Return _Parentid
        End Get
        Set(ByVal Value As String)
            _Parentid = Value
        End Set
    End Property

    Public Property valuemaster() As String
        Get
            Return _valuemaster
        End Get
        Set(ByVal Value As String)
            _valuemaster = Value
        End Set
    End Property

    Public Property valueparent() As String
        Get
            Return _valueparent
        End Get
        Set(ByVal Value As String)
            _valueparent = Value
        End Set
    End Property

    Public Property Masterid() As String
        Get
            Return _Masterid
        End Get
        Set(ByVal Value As String)
            _Masterid = Value
        End Set
    End Property

    Public Property valueelement() As String
        Get
            Return _valueelement
        End Get
        Set(ByVal Value As String)
            _valueelement = Value
        End Set
    End Property

    Public Property NMR_KNTRK() As String
        Get
            Return _NMR_KNTRK
        End Get
        Set(ByVal Value As String)
            _NMR_KNTRK = Value
        End Set
    End Property

    Public Property JNS_PMBYN() As String
        Get
            Return _JNS_PMBYN
        End Get
        Set(ByVal Value As String)
            _JNS_PMBYN = Value
        End Set
    End Property

    Public Property SKM_PMBYN() As String
        Get
            Return _SKM_PMBYN
        End Get
        Set(ByVal Value As String)
            _SKM_PMBYN = Value
        End Set
    End Property

    Public Property TJN_PMBYN() As String
        Get
            Return _TJN_PMBYN
        End Get
        Set(ByVal Value As String)
            _TJN_PMBYN = Value
        End Set
    End Property

    Public Property JNS_BRNG_DN_JS() As String
        Get
            Return _JNS_BRNG_DN_JS
        End Get
        Set(ByVal Value As String)
            _JNS_BRNG_DN_JS = Value
        End Set
    End Property

    Public Property NL_BRNGJS_YNG_DBY() As Decimal
        Get
            Return _NL_BRNGJS_YNG_DBY
        End Get
        Set(ByVal Value As Decimal)
            _NL_BRNGJS_YNG_DBY = Value
        End Set
    End Property

    Public Property TNGGL_ML() As Date
        Get
            Return _TNGGL_ML
        End Get
        Set(ByVal Value As Date)
            _TNGGL_ML = Value
        End Set
    End Property

    Public Property TNGGL_JTH_TMP() As Date
        Get
            Return _TNGGL_JTH_TMP
        End Get
        Set(ByVal Value As Date)
            _TNGGL_JTH_TMP = Value
        End Set
    End Property

    Public Property JNS_BNGMRGNBG_HSLMBL_JS() As String
        Get
            Return _JNS_BNGMRGNBG_HSLMBL_JS
        End Get
        Set(ByVal Value As String)
            _JNS_BNGMRGNBG_HSLMBL_JS = Value
        End Set
    End Property

    Public Property NL_MRGNMBL_JS() As Int64
        Get
            Return _NL_MRGNMBL_JS
        End Get
        Set(ByVal Value As Int64)
            _NL_MRGNMBL_JS = Value
        End Set
    End Property

    Public Property TNGKT_BNGBG_HSL() As Decimal
        Get
            Return _TNGKT_BNGBG_HSL
        End Get
        Set(ByVal Value As Decimal)
            _TNGKT_BNGBG_HSL = Value
        End Set
    End Property

    Public Property NL_TRCTT() As Int64
        Get
            Return _NL_TRCTT
        End Get
        Set(ByVal Value As Int64)
            _NL_TRCTT = Value
        End Set
    End Property

    Public Property SMBR_PMBYN() As String
        Get
            Return _SMBR_PMBYN
        End Get
        Set(ByVal Value As String)
            _SMBR_PMBYN = Value
        End Set
    End Property

    Public Property PRS_PRSHN_PD_PMBYN_BRSM() As Int64
        Get
            Return _PRS_PRSHN_PD_PMBYN_BRSM
        End Get
        Set(ByVal Value As Int64)
            _PRS_PRSHN_PD_PMBYN_BRSM = Value
        End Set
    End Property

    Public Property NG_MK() As Int64
        Get
            Return _NG_MK
        End Get
        Set(ByVal Value As Int64)
            _NG_MK = Value
        End Set
    End Property

    Public Property KLTS() As String
        Get
            Return _KLTS
        End Get
        Set(ByVal Value As String)
            _KLTS = Value
        End Set
    End Property

    Public Property JNS_MT_NG() As String
        Get
            Return _JNS_MT_NG
        End Get
        Set(ByVal Value As String)
            _JNS_MT_NG = Value
        End Set
    End Property

    Public Property TGHN_PMBYN_DLM_MT_NG_SL() As Int64
        Get
            Return _TGHN_PMBYN_DLM_MT_NG_SL
        End Get
        Set(ByVal Value As Int64)
            _TGHN_PMBYN_DLM_MT_NG_SL = Value
        End Set
    End Property

    Public Property TGHN_PMBYN() As Int64
        Get
            Return _TGHN_PMBYN
        End Get
        Set(ByVal Value As Int64)
            _TGHN_PMBYN = Value
        End Set
    End Property

    Public Property BNGMRGN_DTNGGHKN_DLM_MT_NG_SL() As Int64
        Get
            Return _BNGMRGN_DTNGGHKN_DLM_MT_NG_SL
        End Get
        Set(ByVal Value As Int64)
            _BNGMRGN_DTNGGHKN_DLM_MT_NG_SL = Value
        End Set
    End Property

    Public Property BNGMRGN_DTNGGHKN() As Int64
        Get
            Return _BNGMRGN_DTNGGHKN
        End Get
        Set(ByVal Value As Int64)
            _BNGMRGN_DTNGGHKN = Value
        End Set
    End Property

    Public Property PTNG_PMBYN_PKK_DLM_MT_NG_SL() As Int64
        Get
            Return _PTNG_PMBYN_PKK_DLM_MT_NG_SL
        End Get
        Set(ByVal Value As Int64)
            _PTNG_PMBYN_PKK_DLM_MT_NG_SL = Value
        End Set
    End Property

    Public Property PTNG_PMBYN__PKK() As Int64
        Get
            Return _PTNG_PMBYN__PKK
        End Get
        Set(ByVal Value As Int64)
            _PTNG_PMBYN__PKK = Value
        End Set
    End Property

    Public Property PRPRS_PNJMNN_KRDT_T_SRNS_KRDT() As Decimal
        Get
            Return _PRPRS_PNJMNN_KRDT_T_SRNS_KRDT
        End Get
        Set(ByVal Value As Decimal)
            _PRPRS_PNJMNN_KRDT_T_SRNS_KRDT = Value
        End Set
    End Property

    Public Property NMR_DBTR() As String
        Get
            Return _NMR_DBTR
        End Get
        Set(ByVal Value As String)
            _NMR_DBTR = Value
        End Set
    End Property

    Public Property NM_DBTR() As String
        Get
            Return _NM_DBTR
        End Get
        Set(ByVal Value As String)
            _NM_DBTR = Value
        End Set
    End Property

    Public Property GRP_PHK_LWN() As String
        Get
            Return _GRP_PHK_LWN
        End Get
        Set(ByVal Value As String)
            _GRP_PHK_LWN = Value
        End Set
    End Property

    Public Property KTGR_SH_DBTR() As String
        Get
            Return _KTGR_SH_DBTR
        End Get
        Set(ByVal Value As String)
            _KTGR_SH_DBTR = Value
        End Set
    End Property

    Public Property GLNGN_PHK_LWN() As String
        Get
            Return _GLNGN_PHK_LWN
        End Get
        Set(ByVal Value As String)
            _GLNGN_PHK_LWN = Value
        End Set
    End Property

    Public Property STTS_KTRKTN() As String
        Get
            Return _STTS_KTRKTN
        End Get
        Set(ByVal Value As String)
            _STTS_KTRKTN = Value
        End Set
    End Property

    Public Property SKTR_KNM_LPNGN_SH() As String
        Get
            Return _SKTR_KNM_LPNGN_SH
        End Get
        Set(ByVal Value As String)
            _SKTR_KNM_LPNGN_SH = Value
        End Set
    End Property

    Public Property LKS_DT__PRYK() As String
        Get
            Return _LKS_DT__PRYK
        End Get
        Set(ByVal Value As String)
            _LKS_DT__PRYK = Value
        End Set
    End Property

    Public Property NMR_GNN() As String
        Get
            Return _NMR_GNN
        End Get
        Set(ByVal Value As String)
            _NMR_GNN = Value
        End Set
    End Property

    Public Property JNS_GNN() As String
        Get
            Return _JNS_GNN
        End Get
        Set(ByVal Value As String)
            _JNS_GNN = Value
        End Set
    End Property

    Public Property NL_GNNJMNN_YNG_DPT_DPRHTNGKN() As Int64
        Get
            Return _NL_GNNJMNN_YNG_DPT_DPRHTNGKN
        End Get
        Set(ByVal Value As Int64)
            _NL_GNNJMNN_YNG_DPT_DPRHTNGKN = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

End Class





