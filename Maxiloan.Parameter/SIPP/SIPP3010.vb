﻿<Serializable()>
Public Class SIPP3010 : Inherits Common

    Private _Table As String
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _NMR_KNTRK As String
    Private _NMNL As Int64
    Private _NMR_KNTRK_LNDNG_NL As String
    Private _JNS As String
    Private _TNGGL_ML As Date
    Private _TNGGL_TMP As Date
    Private _NL_MT_NG_SL As Int64
    Private _KVLN_RPH As Int64
    Private _NM As String
    Private _GLNGN As String
    Private _SL_NGR As String
    Private _Masterid As String
    Private _Parentid As String
    Private _valuemaster As String
    Private _valueparent As String
    Private _valueelement As String
    Private _BULANDATA As String


    Public Property ID As Int64
    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property
    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property

    Public Property Parentid() As String
        Get
            Return _Parentid
        End Get
        Set(ByVal Value As String)
            _Parentid = Value
        End Set
    End Property

    Public Property valuemaster() As String
        Get
            Return _valuemaster
        End Get
        Set(ByVal Value As String)
            _valuemaster = Value
        End Set
    End Property

    Public Property valueparent() As String
        Get
            Return _valueparent
        End Get
        Set(ByVal Value As String)
            _valueparent = Value
        End Set
    End Property

    Public Property Masterid() As String
        Get
            Return _Masterid
        End Get
        Set(ByVal Value As String)
            _Masterid = Value
        End Set
    End Property

    Public Property valueelement() As String
        Get
            Return _valueelement
        End Get
        Set(ByVal Value As String)
            _valueelement = Value
        End Set
    End Property

    Public Property NMR_KNTRK() As String
        Get
            Return _NMR_KNTRK
        End Get
        Set(ByVal Value As String)
            _NMR_KNTRK = Value
        End Set
    End Property

    Public Property NMNL() As Int64
        Get
            Return _NMNL
        End Get
        Set(ByVal Value As Int64)
            _NMNL = Value
        End Set
    End Property

    Public Property NMR_KNTRK_LNDNG_NL() As String
        Get
            Return _NMR_KNTRK_LNDNG_NL
        End Get
        Set(ByVal Value As String)
            _NMR_KNTRK_LNDNG_NL = Value
        End Set
    End Property

    Public Property JNS() As String
        Get
            Return _JNS
        End Get
        Set(ByVal Value As String)
            _JNS = Value
        End Set
    End Property

    Public Property TNGGL_ML() As Date
        Get
            Return _TNGGL_ML
        End Get
        Set(ByVal Value As Date)
            _TNGGL_ML = Value
        End Set
    End Property

    Public Property TNGGL_TMP() As Date
        Get
            Return _TNGGL_TMP
        End Get
        Set(ByVal Value As Date)
            _TNGGL_TMP = Value
        End Set
    End Property

    Public Property NL_MT_NG_SL() As Int64
        Get
            Return _NL_MT_NG_SL
        End Get
        Set(ByVal Value As Int64)
            _NL_MT_NG_SL = Value
        End Set
    End Property

    Public Property KVLN_RPH() As Int64
        Get
            Return _KVLN_RPH
        End Get
        Set(ByVal Value As Int64)
            _KVLN_RPH = Value
        End Set
    End Property

    Public Property NM() As String
        Get
            Return _NM
        End Get
        Set(ByVal Value As String)
            _NM = Value
        End Set
    End Property

    Public Property GLNGN() As String
        Get
            Return _GLNGN
        End Get
        Set(ByVal Value As String)
            _GLNGN = Value
        End Set
    End Property

    Public Property SL_NGR() As String
        Get
            Return _SL_NGR
        End Get
        Set(ByVal Value As String)
            _SL_NGR = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

End Class





