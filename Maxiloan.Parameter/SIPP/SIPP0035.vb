﻿<Serializable()>
Public Class SIPP0035 : Inherits Common
    Private _ID As Int64
    Private _BULANDATA As String
    Private _NM_PNGRS As String
    Private _KWRGNGRN As String
    Private _JBTN_KPNGRSN As String
    Private _LKS_DT__TNG_KRJ As String
    Private _TNGGL_ML As Date
    Private _NMR_SRT_KPTSN_FT_ND_PRPR_TST As String
    Private _TNGGL_SRT_KPTSN_FT_ND_PRPR_TST As Date

    Private _ListData As DataTable
    Private _ListDataReport As DataSet
    Private _TotalRecords As Int64
    Private _Table As String
    Private _valueparent As String
    Private _valuemaster As String
    Private _valueelement As String

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property

    Public Property NM_PNGRS As String
        Get
            Return _NM_PNGRS
        End Get
        Set(value As String)
            _NM_PNGRS = value
        End Set
    End Property

    Public Property KWRGNGRN As String
        Get
            Return _KWRGNGRN
        End Get
        Set(value As String)
            _KWRGNGRN = value
        End Set
    End Property

    Public Property JBTN_KPNGRSN As String
        Get
            Return _JBTN_KPNGRSN
        End Get
        Set(value As String)
            _JBTN_KPNGRSN = value
        End Set
    End Property

    Public Property LKS_DT__TNG_KRJ As String
        Get
            Return _LKS_DT__TNG_KRJ
        End Get
        Set(value As String)
            _LKS_DT__TNG_KRJ = value
        End Set
    End Property

    Public Property TNGGL_ML As Date
        Get
            Return _TNGGL_ML
        End Get
        Set(value As Date)
            _TNGGL_ML = value
        End Set
    End Property

    Public Property NMR_SRT_KPTSN_FT_ND_PRPR_TST As String
        Get
            Return _NMR_SRT_KPTSN_FT_ND_PRPR_TST
        End Get
        Set(value As String)
            _NMR_SRT_KPTSN_FT_ND_PRPR_TST = value
        End Set
    End Property

    Public Property TNGGL_SRT_KPTSN_FT_ND_PRPR_TST As Date
        Get
            Return _TNGGL_SRT_KPTSN_FT_ND_PRPR_TST
        End Get
        Set(value As Date)
            _TNGGL_SRT_KPTSN_FT_ND_PRPR_TST = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property

    Public Property ListDataReport As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(value As DataSet)
            _ListDataReport = value
        End Set
    End Property

    Public Property TotalRecords As Long
        Get
            Return _TotalRecords
        End Get
        Set(value As Long)
            _TotalRecords = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property

    Public Property Valueparent As String
        Get
            Return _valueparent
        End Get
        Set(value As String)
            _valueparent = value
        End Set
    End Property

    Public Property Valuemaster As String
        Get
            Return _valuemaster
        End Get
        Set(value As String)
            _valuemaster = value
        End Set
    End Property

    Public Property Valueelement As String
        Get
            Return _valueelement
        End Get
        Set(value As String)
            _valueelement = value
        End Set
    End Property
End Class
