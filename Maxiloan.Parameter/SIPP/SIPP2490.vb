﻿<Serializable()>
Public Class SIPP2490 : Inherits Common
    Private _ID As Int64
    Private _BULANDATA As String
    Private _JNS_RPRP_ST_PRSHN_PMBYN As String
    Private _JNS_MT_NG As String
    Private _RPRP_ST As String

    Private _ListData As DataTable
    Private _ListDataReport As DataSet
    Private _TotalRecords As Int64
    Private _Table As String
    Private _valueparent As String
    Private _valuemaster As String
    Private _valueelement As String

    Public Property JNS_RPRP_ST_PRSHN_PMBYN As String
        Get
            Return _JNS_RPRP_ST_PRSHN_PMBYN
        End Get
        Set(value As String)
            _JNS_RPRP_ST_PRSHN_PMBYN = value
        End Set
    End Property

    Public Property JNS_MT_NG As String
        Get
            Return _JNS_MT_NG
        End Get
        Set(value As String)
            _JNS_MT_NG = value
        End Set
    End Property

    Public Property RPRP_ST As String
        Get
            Return _RPRP_ST
        End Get
        Set(value As String)
            _RPRP_ST = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property

    Public Property ListDataReport As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(value As DataSet)
            _ListDataReport = value
        End Set
    End Property

    Public Property TotalRecords As Long
        Get
            Return _TotalRecords
        End Get
        Set(value As Long)
            _TotalRecords = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property

    Public Property Valueparent As String
        Get
            Return _valueparent
        End Get
        Set(value As String)
            _valueparent = value
        End Set
    End Property

    Public Property Valuemaster As String
        Get
            Return _valuemaster
        End Get
        Set(value As String)
            _valuemaster = value
        End Set
    End Property

    Public Property Valueelement As String
        Get
            Return _valueelement
        End Get
        Set(value As String)
            _valueelement = value
        End Set
    End Property
End Class
