﻿<Serializable()>
Public Class SIPP0010 : Inherits Common

    Private _Table As String
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _NMR_ZN_SH As String
    Private _TNGGL_ZN_SH As String
    Private _JNS_PRZNN As String
    Private _KTRNGN As String
    Private _Masterid As String
    Private _Parentid As String
    Private _valuemaster As String
    Private _valueparent As String
    Private _valueelement As String
    Private _bulandata As String
    Public Property ID As Int64

    Public Property bulandata() As String
        Get
            Return _bulandata
        End Get
        Set(ByVal Value As String)
            _bulandata = Value
        End Set
    End Property

    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property

    Public Property Parentid() As String
        Get
            Return _Parentid
        End Get
        Set(ByVal Value As String)
            _Parentid = Value
        End Set
    End Property

    Public Property valuemaster() As String
        Get
            Return _valuemaster
        End Get
        Set(ByVal Value As String)
            _valuemaster = Value
        End Set
    End Property

    Public Property valueelement() As String
        Get
            Return _valueelement
        End Get
        Set(ByVal Value As String)
            _valueelement = Value
        End Set
    End Property

    Public Property valueparent() As String
        Get
            Return _valueparent
        End Get
        Set(ByVal Value As String)
            _valueparent = Value
        End Set
    End Property

    Public Property Masterid() As String
        Get
            Return _Masterid
        End Get
        Set(ByVal Value As String)
            _Masterid = Value
        End Set
    End Property

    Public Property NMR_ZN_SH() As String
        Get
            Return _NMR_ZN_SH
        End Get
        Set(ByVal Value As String)
            _NMR_ZN_SH = Value
        End Set
    End Property

    Public Property TNGGL_ZN_SH() As String
        Get
            Return _TNGGL_ZN_SH
        End Get
        Set(ByVal Value As String)
            _TNGGL_ZN_SH = Value
        End Set
    End Property

    Public Property JNS_PRZNN() As String
        Get
            Return _JNS_PRZNN
        End Get
        Set(ByVal Value As String)
            _JNS_PRZNN = Value
        End Set
    End Property

    Public Property KTRNGN() As String
        Get
            Return _KTRNGN
        End Get
        Set(ByVal Value As String)
            _KTRNGN = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

End Class





