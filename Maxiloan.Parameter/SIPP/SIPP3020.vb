﻿<Serializable()>
Public Class SIPP3020 : Inherits Common
    Private _ID As Int64
    Private _BULANDATA As String
    Private _NOKONTRAK As String
    Private _JNSKRJSMPEMB As String
    Private _TGLMULAI As Date
    Private _TGLJATUHTEMPO As Date
    Private _JENISVALUTA As String
    Private _PORSIPRSHNPEMB As String
    Private _PLAFONASAL As String
    Private _PLAFONRUPIAH As String
    Private _SOPPPBASAL As String
    Private _SOPPPBRUPIAH As String
    Private _NAMAKREDITUR As String
    Private _GOLKREDITUR As String
    Private _STATUSKETERKAITAN As String
    Private _NEGARAASAL As String

    Private _ListData As DataTable
    Private _ListDataReport As DataSet
    Private _TotalRecords As Int64
    Private _Table As String
    Private _valueparent As String
    Private _valuemaster As String
    Private _valueelement As String

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property

    Public Property NOKONTRAK As String
        Get
            Return _NOKONTRAK
        End Get
        Set(value As String)
            _NOKONTRAK = value
        End Set
    End Property

    Public Property JNSKRJSMPEMB As String
        Get
            Return _JNSKRJSMPEMB
        End Get
        Set(value As String)
            _JNSKRJSMPEMB = value
        End Set
    End Property

    Public Property TGLMULAI As Date
        Get
            Return _TGLMULAI
        End Get
        Set(value As Date)
            _TGLMULAI = value
        End Set
    End Property

    Public Property TGLJATUHTEMPO As Date
        Get
            Return _TGLJATUHTEMPO
        End Get
        Set(value As Date)
            _TGLJATUHTEMPO = value
        End Set
    End Property

    Public Property JENISVALUTA As String
        Get
            Return _JENISVALUTA
        End Get
        Set(value As String)
            _JENISVALUTA = value
        End Set
    End Property

    Public Property PORSIPRSHNPEMB As String
        Get
            Return _PORSIPRSHNPEMB
        End Get
        Set(value As String)
            _PORSIPRSHNPEMB = value
        End Set
    End Property

    Public Property PLAFONASAL As String
        Get
            Return _PLAFONASAL
        End Get
        Set(value As String)
            _PLAFONASAL = value
        End Set
    End Property

    Public Property PLAFONRUPIAH As String
        Get
            Return _PLAFONRUPIAH
        End Get
        Set(value As String)
            _PLAFONRUPIAH = value
        End Set
    End Property

    Public Property SOPPPBASAL As String
        Get
            Return _SOPPPBASAL
        End Get
        Set(value As String)
            _SOPPPBASAL = value
        End Set
    End Property

    Public Property SOPPPBRUPIAH As String
        Get
            Return _SOPPPBRUPIAH
        End Get
        Set(value As String)
            _SOPPPBRUPIAH = value
        End Set
    End Property

    Public Property NAMAKREDITUR As String
        Get
            Return _NAMAKREDITUR
        End Get
        Set(value As String)
            _NAMAKREDITUR = value
        End Set
    End Property

    Public Property GOLKREDITUR As String
        Get
            Return _GOLKREDITUR
        End Get
        Set(value As String)
            _GOLKREDITUR = value
        End Set
    End Property

    Public Property STATUSKETERKAITAN As String
        Get
            Return _STATUSKETERKAITAN
        End Get
        Set(value As String)
            _STATUSKETERKAITAN = value
        End Set
    End Property

    Public Property NEGARAASAL As String
        Get
            Return _NEGARAASAL
        End Get
        Set(value As String)
            _NEGARAASAL = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property

    Public Property ListDataReport As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(value As DataSet)
            _ListDataReport = value
        End Set
    End Property

    Public Property TotalRecords As Long
        Get
            Return _TotalRecords
        End Get
        Set(value As Long)
            _TotalRecords = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property

    Public Property Valueparent As String
        Get
            Return _valueparent
        End Get
        Set(value As String)
            _valueparent = value
        End Set
    End Property

    Public Property Valuemaster As String
        Get
            Return _valuemaster
        End Get
        Set(value As String)
            _valuemaster = value
        End Set
    End Property

    Public Property Valueelement As String
        Get
            Return _valueelement
        End Get
        Set(value As String)
            _valueelement = value
        End Set
    End Property
End Class
