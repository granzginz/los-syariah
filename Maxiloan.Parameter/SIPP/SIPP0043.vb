﻿<Serializable()>
Public Class SIPP0043 : Inherits Common
    Private _ID As Int64
    Private _BULANDATA As String
    Private _JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Int64
    Private _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Int64
    Private _JMLH_TNG_KRJ_TTL__ As Int64
    Private _JMLH_TNG_KRJ_PMSRN_TTL_ As Int64
    Private _JMLH_TNG_KRJ_PRSNL_TTL_ As Int64
    Private _JMLH_TNG_KRJ_PNGHN_TTL_ As Int64
    Private _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TTL_ As Int64
    Private _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TTL_ As Int64
    Private _JMLH_TNG_KRJ_MNJMN_RSK_TTL_ As Int64
    Private _JMLH_TNG_KRJ_DT_NTRNL_TTL_ As Int64
    Private _JMLH_TNG_KRJ_LGL_TTL_ As Int64
    Private _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TTL_ As Int64
    Private _JMLH_TNG_KRJ_FNGS_LNNY_TTL_ As Int64

    Private _ListData As DataTable
    Private _ListDataReport As DataSet
    Private _TotalRecords As Int64
    Private _Table As String
    Private _valueparent As String
    Private _valuemaster As String
    Private _valueelement As String

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS As Long
        Get
            Return _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY As Long
        Get
            Return _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_TTL__ As Long
        Get
            Return _JMLH_TNG_KRJ_TTL__
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_TTL__ = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PMSRN_TTL_ As Long
        Get
            Return _JMLH_TNG_KRJ_PMSRN_TTL_
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PMSRN_TTL_ = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PRSNL_TTL_ As Long
        Get
            Return _JMLH_TNG_KRJ_PRSNL_TTL_
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PRSNL_TTL_ = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_PNGHN_TTL_ As Long
        Get
            Return _JMLH_TNG_KRJ_PNGHN_TTL_
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_PNGHN_TTL_ = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TTL_ As Long
        Get
            Return _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TTL_
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TTL_ = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNGN_DN_KNTNS_TTL_ As Long
        Get
            Return _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TTL_
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_KNGN_DN_KNTNS_TTL_ = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_MNJMN_RSK_TTL_ As Long
        Get
            Return _JMLH_TNG_KRJ_MNJMN_RSK_TTL_
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_MNJMN_RSK_TTL_ = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_DT_NTRNL_TTL_ As Long
        Get
            Return _JMLH_TNG_KRJ_DT_NTRNL_TTL_
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_DT_NTRNL_TTL_ = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_LGL_TTL_ As Long
        Get
            Return _JMLH_TNG_KRJ_LGL_TTL_
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_LGL_TTL_ = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_TKNLG_NFRMS_T_TTL_ As Long
        Get
            Return _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TTL_
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_TKNLG_NFRMS_T_TTL_ = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_FNGS_LNNY_TTL_ As Long
        Get
            Return _JMLH_TNG_KRJ_FNGS_LNNY_TTL_
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_FNGS_LNNY_TTL_ = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property

    Public Property ListDataReport As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(value As DataSet)
            _ListDataReport = value
        End Set
    End Property

    Public Property TotalRecords As Long
        Get
            Return _TotalRecords
        End Get
        Set(value As Long)
            _TotalRecords = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property

    Public Property Valueparent As String
        Get
            Return _valueparent
        End Get
        Set(value As String)
            _valueparent = value
        End Set
    End Property

    Public Property Valuemaster As String
        Get
            Return _valuemaster
        End Get
        Set(value As String)
            _valuemaster = value
        End Set
    End Property

    Public Property Valueelement As String
        Get
            Return _valueelement
        End Get
        Set(value As String)
            _valueelement = value
        End Set
    End Property
End Class
