﻿<Serializable()>
Public Class SIPP0000 : Inherits Common
    Private _ID As Int64
    Private _BULANDATA As String
    Private _NM_LNGKP_PRSHN As String
    Private _NM_SBTN_PRSHN As String
    Private _NPWP As String
    Private _STTS_KPMLKN_PRSHN As String
    Private _BNTK_BDN_SH_PRSHN_PMBYN As String
    Private _STTS_KGTN_SH_SYRH As String
    Private _TNGGL_PNDRN As String
    Private _BDNG_SH_PRSHN_PMBYN As String
    Private _BDNG_SH_PRSHN_PMBYN_1 As String
    Private _BDNG_SH_PRSHN_PMBYN_2 As String
    Private _BDNG_SH_PRSHN_PMBYN_3 As String
    Private _BDNG_SH_PRSHN_PMBYN_4 As String
    Private _BDNG_SH_PRSHN_PMBYN_5 As String
    Private _BDNG_SH_PRSHN_PMBYN_6 As String
    Private _LMT_LNGKP As String
    Private _LKS_DT__KNTR As String
    Private _KD_PS As String
    Private _STTS_KPMLKN_GDNG As String
    Private _NMR_TLPN As String
    Private _NMR_FKSML As String
    Private _JMLH_KNTR_CBNG As Int64
    Private _JMLH_JRNGN_KNTR_SLN_KNTR_CBNG As Int64
    Private _JMLH_TNG_KRJ_KNTR_PST As Int64
    Private _JMLH_TNG_KRJ_KNTR_CBNG As Int64
    Private _JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG As Int64
    Private _NM_PTGS_PNYSN_LPRN As String
    Private _BGNDVS_PTGS_PNYSN_LPRN As String
    Private _NMR_TLPN_PTGS_PNYSN_LPRN As String
    Private _NMR_FKSML_PTGS_PNYSN_LPRN As String
    Private _NM_PNNGGNG_JWB_LPRN As String
    Private _BGNDVS_PNNGGNG_JWB_LPRN As String
    Private _NMR_TLPN_PNNGGNG_JWB_LPRN As String
    Private _NMR_FKSML_PNNGGNG_JWB_LPRN As String

    Private _ListData As DataTable
    Private _ListDataReport As DataSet
    Private _TotalRecords As Int64
    Private _Table As String
    Private _valueparent As String
    Private _valuemaster As String
    Private _valueelement As String

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property

    Public Property NM_LNGKP_PRSHN As String
        Get
            Return _NM_LNGKP_PRSHN
        End Get
        Set(value As String)
            _NM_LNGKP_PRSHN = value
        End Set
    End Property

    Public Property NM_SBTN_PRSHN As String
        Get
            Return _NM_SBTN_PRSHN
        End Get
        Set(value As String)
            _NM_SBTN_PRSHN = value
        End Set
    End Property

    Public Property NPWP As String
        Get
            Return _NPWP
        End Get
        Set(value As String)
            _NPWP = value
        End Set
    End Property

    Public Property STTS_KPMLKN_PRSHN As String
        Get
            Return _STTS_KPMLKN_PRSHN
        End Get
        Set(value As String)
            _STTS_KPMLKN_PRSHN = value
        End Set
    End Property

    Public Property BNTK_BDN_SH_PRSHN_PMBYN As String
        Get
            Return _BNTK_BDN_SH_PRSHN_PMBYN
        End Get
        Set(value As String)
            _BNTK_BDN_SH_PRSHN_PMBYN = value
        End Set
    End Property

    Public Property STTS_KGTN_SH_SYRH As String
        Get
            Return _STTS_KGTN_SH_SYRH
        End Get
        Set(value As String)
            _STTS_KGTN_SH_SYRH = value
        End Set
    End Property

    Public Property TNGGL_PNDRN As String
        Get
            Return _TNGGL_PNDRN
        End Get
        Set(value As String)
            _TNGGL_PNDRN = value
        End Set
    End Property

    Public Property BDNG_SH_PRSHN_PMBYN As String
        Get
            Return _BDNG_SH_PRSHN_PMBYN
        End Get
        Set(value As String)
            _BDNG_SH_PRSHN_PMBYN = value
        End Set
    End Property

    Public Property BDNG_SH_PRSHN_PMBYN_1 As String
        Get
            Return _BDNG_SH_PRSHN_PMBYN_1
        End Get
        Set(value As String)
            _BDNG_SH_PRSHN_PMBYN_1 = value
        End Set
    End Property

    Public Property BDNG_SH_PRSHN_PMBYN_2 As String
        Get
            Return _BDNG_SH_PRSHN_PMBYN_2
        End Get
        Set(value As String)
            _BDNG_SH_PRSHN_PMBYN_2 = value
        End Set
    End Property

    Public Property BDNG_SH_PRSHN_PMBYN_3 As String
        Get
            Return _BDNG_SH_PRSHN_PMBYN_3
        End Get
        Set(value As String)
            _BDNG_SH_PRSHN_PMBYN_3 = value
        End Set
    End Property

    Public Property BDNG_SH_PRSHN_PMBYN_4 As String
        Get
            Return _BDNG_SH_PRSHN_PMBYN_4
        End Get
        Set(value As String)
            _BDNG_SH_PRSHN_PMBYN_4 = value
        End Set
    End Property

    Public Property BDNG_SH_PRSHN_PMBYN_5 As String
        Get
            Return _BDNG_SH_PRSHN_PMBYN_5
        End Get
        Set(value As String)
            _BDNG_SH_PRSHN_PMBYN_5 = value
        End Set
    End Property

    Public Property BDNG_SH_PRSHN_PMBYN_6 As String
        Get
            Return _BDNG_SH_PRSHN_PMBYN_6
        End Get
        Set(value As String)
            _BDNG_SH_PRSHN_PMBYN_6 = value
        End Set
    End Property

    Public Property LMT_LNGKP As String
        Get
            Return _LMT_LNGKP
        End Get
        Set(value As String)
            _LMT_LNGKP = value
        End Set
    End Property

    Public Property LKS_DT__KNTR As String
        Get
            Return _LKS_DT__KNTR
        End Get
        Set(value As String)
            _LKS_DT__KNTR = value
        End Set
    End Property

    Public Property KD_PS As String
        Get
            Return _KD_PS
        End Get
        Set(value As String)
            _KD_PS = value
        End Set
    End Property

    Public Property STTS_KPMLKN_GDNG As String
        Get
            Return _STTS_KPMLKN_GDNG
        End Get
        Set(value As String)
            _STTS_KPMLKN_GDNG = value
        End Set
    End Property

    Public Property NMR_TLPN As String
        Get
            Return _NMR_TLPN
        End Get
        Set(value As String)
            _NMR_TLPN = value
        End Set
    End Property

    Public Property NMR_FKSML As String
        Get
            Return _NMR_FKSML
        End Get
        Set(value As String)
            _NMR_FKSML = value
        End Set
    End Property

    Public Property JMLH_KNTR_CBNG As Long
        Get
            Return _JMLH_KNTR_CBNG
        End Get
        Set(value As Long)
            _JMLH_KNTR_CBNG = value
        End Set
    End Property

    Public Property JMLH_JRNGN_KNTR_SLN_KNTR_CBNG As Long
        Get
            Return _JMLH_JRNGN_KNTR_SLN_KNTR_CBNG
        End Get
        Set(value As Long)
            _JMLH_JRNGN_KNTR_SLN_KNTR_CBNG = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_PST As Long
        Get
            Return _JMLH_TNG_KRJ_KNTR_PST
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_KNTR_PST = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_CBNG As Long
        Get
            Return _JMLH_TNG_KRJ_KNTR_CBNG
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_KNTR_CBNG = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG As Long
        Get
            Return _JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG = value
        End Set
    End Property

    Public Property NM_PTGS_PNYSN_LPRN As String
        Get
            Return _NM_PTGS_PNYSN_LPRN
        End Get
        Set(value As String)
            _NM_PTGS_PNYSN_LPRN = value
        End Set
    End Property

    Public Property BGNDVS_PTGS_PNYSN_LPRN As String
        Get
            Return _BGNDVS_PTGS_PNYSN_LPRN
        End Get
        Set(value As String)
            _BGNDVS_PTGS_PNYSN_LPRN = value
        End Set
    End Property

    Public Property NMR_TLPN_PTGS_PNYSN_LPRN As String
        Get
            Return _NMR_TLPN_PTGS_PNYSN_LPRN
        End Get
        Set(value As String)
            _NMR_TLPN_PTGS_PNYSN_LPRN = value
        End Set
    End Property

    Public Property NMR_FKSML_PTGS_PNYSN_LPRN As String
        Get
            Return _NMR_FKSML_PTGS_PNYSN_LPRN
        End Get
        Set(value As String)
            _NMR_FKSML_PTGS_PNYSN_LPRN = value
        End Set
    End Property

    Public Property NM_PNNGGNG_JWB_LPRN As String
        Get
            Return _NM_PNNGGNG_JWB_LPRN
        End Get
        Set(value As String)
            _NM_PNNGGNG_JWB_LPRN = value
        End Set
    End Property

    Public Property BGNDVS_PNNGGNG_JWB_LPRN As String
        Get
            Return _BGNDVS_PNNGGNG_JWB_LPRN
        End Get
        Set(value As String)
            _BGNDVS_PNNGGNG_JWB_LPRN = value
        End Set
    End Property

    Public Property NMR_TLPN_PNNGGNG_JWB_LPRN As String
        Get
            Return _NMR_TLPN_PNNGGNG_JWB_LPRN
        End Get
        Set(value As String)
            _NMR_TLPN_PNNGGNG_JWB_LPRN = value
        End Set
    End Property

    Public Property NMR_FKSML_PNNGGNG_JWB_LPRN As String
        Get
            Return _NMR_FKSML_PNNGGNG_JWB_LPRN
        End Get
        Set(value As String)
            _NMR_FKSML_PNNGGNG_JWB_LPRN = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property

    Public Property ListDataReport As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(value As DataSet)
            _ListDataReport = value
        End Set
    End Property

    Public Property TotalRecords As Long
        Get
            Return _TotalRecords
        End Get
        Set(value As Long)
            _TotalRecords = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property

    Public Property Valueparent As String
        Get
            Return _valueparent
        End Get
        Set(value As String)
            _valueparent = value
        End Set
    End Property

    Public Property Valuemaster As String
        Get
            Return _valuemaster
        End Get
        Set(value As String)
            _valuemaster = value
        End Set
    End Property

    Public Property Valueelement As String
        Get
            Return _valueelement
        End Get
        Set(value As String)
            _valueelement = value
        End Set
    End Property
End Class
