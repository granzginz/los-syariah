﻿<Serializable()>
Public Class SIPP2300 : Inherits Common

    Private _Table As String
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _NM_PERUSAHAAN As String
    Private _GOL_PERUSAHAAN As String
    Private _STATUS_KETERKAITAN As String
    Private _NEGARA As String
    Private _TGL_MULAI As Date
    Private _PRSN_BGN_PNYRTN As Decimal
    Private _JNS_MATA_UANG As String
    Private _KUALITAS As String
    Private _NILAI_PNYRTN_AWL_DLM_MUA As Int64
    Private _NILAI_AWL_PNYRTN As Int64
    Private _NILAI_AWL_PNYRTN_MDL_MUA As Int64
    Private _NILAI_PNYRTN_MODAL As Int64
    Private _Masterid As String
    Private _Parentid As String
    Private _valuemaster As String
    Private _valueparent As String
    Private _valueelement As String
    Private _BULANDATA As String


    Public Property ID As Int64
    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property
    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property
    Public Property Parentid() As String
        Get
            Return _Parentid
        End Get
        Set(ByVal Value As String)
            _Parentid = Value
        End Set
    End Property

    Public Property valuemaster() As String
        Get
            Return _valuemaster
        End Get
        Set(ByVal Value As String)
            _valuemaster = Value
        End Set
    End Property

    Public Property valueparent() As String
        Get
            Return _valueparent
        End Get
        Set(ByVal Value As String)
            _valueparent = Value
        End Set
    End Property

    Public Property Masterid() As String
        Get
            Return _Masterid
        End Get
        Set(ByVal Value As String)
            _Masterid = Value
        End Set
    End Property

    Public Property valueelement() As String
        Get
            Return _valueelement
        End Get
        Set(ByVal Value As String)
            _valueelement = Value
        End Set
    End Property

    Public Property NM_PERUSAHAAN() As String
        Get
            Return _NM_PERUSAHAAN
        End Get
        Set(ByVal Value As String)
            _NM_PERUSAHAAN = Value
        End Set
    End Property

    Public Property GOL_PERUSAHAAN() As String
        Get
            Return _GOL_PERUSAHAAN
        End Get
        Set(ByVal Value As String)
            _GOL_PERUSAHAAN = Value
        End Set
    End Property

    Public Property STATUS_KETERKAITAN() As String
        Get
            Return _STATUS_KETERKAITAN
        End Get
        Set(ByVal Value As String)
            _STATUS_KETERKAITAN = Value
        End Set
    End Property

    Public Property NEGARA() As String
        Get
            Return _NEGARA
        End Get
        Set(ByVal Value As String)
            _NEGARA = Value
        End Set
    End Property

    Public Property TGL_MULAI() As Date
        Get
            Return _TGL_MULAI
        End Get
        Set(ByVal Value As Date)
            _TGL_MULAI = Value
        End Set
    End Property

    Public Property PRSN_BGN_PNYRTN() As Decimal
        Get
            Return _PRSN_BGN_PNYRTN
        End Get
        Set(ByVal Value As Decimal)
            _PRSN_BGN_PNYRTN = Value
        End Set
    End Property

    Public Property JNS_MATA_UANG() As String
        Get
            Return _JNS_MATA_UANG
        End Get
        Set(ByVal Value As String)
            _JNS_MATA_UANG = Value
        End Set
    End Property

    Public Property KUALITAS() As String
        Get
            Return _KUALITAS
        End Get
        Set(ByVal Value As String)
            _KUALITAS = Value
        End Set
    End Property

    Public Property NILAI_PNYRTN_AWL_DLM_MUA() As String
        Get
            Return _NILAI_PNYRTN_AWL_DLM_MUA
        End Get
        Set(ByVal Value As String)
            _NILAI_PNYRTN_AWL_DLM_MUA = Value
        End Set
    End Property

    Public Property NILAI_AWL_PNYRTN() As String
        Get
            Return _NILAI_AWL_PNYRTN
        End Get
        Set(ByVal Value As String)
            _NILAI_AWL_PNYRTN = Value
        End Set
    End Property

    Public Property NILAI_AWL_PNYRTN_MDL_MUA() As String
        Get
            Return _NILAI_AWL_PNYRTN_MDL_MUA
        End Get
        Set(ByVal Value As String)
            _NILAI_AWL_PNYRTN_MDL_MUA = Value
        End Set
    End Property

    Public Property NILAI_PNYRTN_MODAL() As String
        Get
            Return _NILAI_PNYRTN_MODAL
        End Get
        Set(ByVal Value As String)
            _NILAI_PNYRTN_MODAL = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

End Class





