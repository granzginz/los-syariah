﻿<Serializable()>
Public Class SIPP0046 : Inherits Common
    Private _ID As Int64
    Private _BULANDATA As String
    Private _NAMA As String
    Private _KEWARGANEGARAAN As String
    Private _JABATAN As String
    Private _DOMISILI As String
    Private _NOSURATKEPUTUSAN As String
    Private _TGLSURATKEPUTUSAN As Date

    Private _ListData As DataTable
    Private _ListDataReport As DataSet
    Private _TotalRecords As Int64
    Private _Table As String
    Private _valueparent As String
    Private _valuemaster As String
    Private _valueelement As String

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property

    Public Property NAMA As String
        Get
            Return _NAMA
        End Get
        Set(value As String)
            _NAMA = value
        End Set
    End Property

    Public Property KEWARGANEGARAAN As String
        Get
            Return _KEWARGANEGARAAN
        End Get
        Set(value As String)
            _KEWARGANEGARAAN = value
        End Set
    End Property

    Public Property JABATAN As String
        Get
            Return _JABATAN
        End Get
        Set(value As String)
            _JABATAN = value
        End Set
    End Property

    Public Property DOMISILI As String
        Get
            Return _DOMISILI
        End Get
        Set(value As String)
            _DOMISILI = value
        End Set
    End Property

    Public Property NOSURATKEPUTUSAN As String
        Get
            Return _NOSURATKEPUTUSAN
        End Get
        Set(value As String)
            _NOSURATKEPUTUSAN = value
        End Set
    End Property

    Public Property TGLSURATKEPUTUSAN As Date
        Get
            Return _TGLSURATKEPUTUSAN
        End Get
        Set(value As Date)
            _TGLSURATKEPUTUSAN = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property

    Public Property ListDataReport As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(value As DataSet)
            _ListDataReport = value
        End Set
    End Property

    Public Property TotalRecords As Long
        Get
            Return _TotalRecords
        End Get
        Set(value As Long)
            _TotalRecords = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property

    Public Property Valueparent As String
        Get
            Return _valueparent
        End Get
        Set(value As String)
            _valueparent = value
        End Set
    End Property

    Public Property Valuemaster As String
        Get
            Return _valuemaster
        End Get
        Set(value As String)
            _valuemaster = value
        End Set
    End Property

    Public Property Valueelement As String
        Get
            Return _valueelement
        End Get
        Set(value As String)
            _valueelement = value
        End Set
    End Property
End Class
