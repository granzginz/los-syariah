﻿<Serializable()>
Public Class SIPP1200 : Inherits Common

    Private _Table As String
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _BULANDATA As String
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_TTL As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_MT_NG_SNG As Int64
    Public Property PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL As Int64
    Public Property PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH As Int64
    Public Property PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG As Int64
    Public Property PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL As Int64
    Public Property PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH As Int64
    Public Property PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG As Int64
    Public Property PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL As Int64
    Public Property PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH As Int64
    Public Property PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG As Int64
    Public Property PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_TTL As Int64
    Public Property PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH As Int64
    Public Property PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG As Int64
    Public Property PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL As Int64
    Public Property PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH As Int64
    Public Property PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG As Int64
    Public Property PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_TTL As Int64
    Public Property PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_NDNSN_RPH As Int64
    Public Property PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_MT_NG_SNG As Int64
    Public Property PNDPTN_DMNSTRS_TTL As Int64
    Public Property PNDPTN_DMNSTRS_NDNSN_RPH As Int64
    Public Property PNDPTN_DMNSTRS_MT_NG_SNG As Int64
    Public Property PNDPTN_PRVS_TTL As Int64
    Public Property PNDPTN_PRVS_NDNSN_RPH As Int64
    Public Property PNDPTN_PRVS_MT_NG_SNG As Int64
    Public Property PNDPTN_DND_TTL As Int64
    Public Property PNDPTN_DND_NDNSN_RPH As Int64
    Public Property PNDPTN_DND_MT_NG_SNG As Int64
    Public Property PNDPTN_DSKN_SRNS_TTL As Int64
    Public Property PNDPTN_DSKN_SRNS_NDNSN_RPH As Int64
    Public Property PNDPTN_DSKN_SRNS_MT_NG_SNG As Int64
    Public Property PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_TTL As Int64
    Public Property PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_NDNSN_RPH As Int64
    Public Property PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_MT_NG_SNG As Int64
    Public Property PNDPTN_PRSNL_LN_TRKT_PMBYN_TTL As Int64
    Public Property PNDPTN_PRSNL_LN_TRKT_PMBYN_NDNSN_RPH As Int64
    Public Property PNDPTN_PRSNL_LN_TRKT_PMBYN_MT_NG_SNG As Int64
    Public Property PNDPTN_DR_SW_PRS_PRTNG_LS_TTL As Int64
    Public Property PNDPTN_DR_SW_PRS_PRTNG_LS_NDNSN_RPH As Int64
    Public Property PNDPTN_DR_SW_PRS_PRTNG_LS_MT_NG_SNG As Int64
    Public Property PNDPTN_F_DR_PMSRN_PRDK_RKSDN_TTL As Int64
    Public Property PNDPTN_F_DR_PMSRN_PRDK_RKSDN_NDNSN_RPH As Int64
    Public Property PNDPTN_F_DR_PMSRN_PRDK_RKSDN_MT_NG_SNG As Int64
    Public Property PNDPTN_F_DR_PMSRN_PRDK_SRNS_TTL As Int64
    Public Property PNDPTN_F_DR_PMSRN_PRDK_SRNS_NDNSN_RPH As Int64
    Public Property PNDPTN_F_DR_PMSRN_PRDK_SRNS_MT_NG_SNG As Int64
    Public Property PNDPTN_F_DR_PMSRN_PRDK_LNNY_TTL As Int64
    Public Property PNDPTN_F_DR_PMSRN_PRDK_LNNY_NDNSN_RPH As Int64
    Public Property PNDPTN_F_DR_PMSRN_PRDK_LNNY_MT_NG_SNG As Int64
    Public Property PNDPTN_DR_KGTN_BRBSS_F_TTL As Int64
    Public Property PNDPTN_DR_KGTN_BRBSS_F_NDNSN_RPH As Int64
    Public Property PNDPTN_DR_KGTN_BRBSS_F_MT_NG_SNG As Int64
    Public Property PNDPTN_PRSNL_LNNY_LNNY_TTL As Int64
    Public Property PNDPTN_PRSNL_LNNY_LNNY_NDNSN_RPH As Int64
    Public Property PNDPTN_PRSNL_LNNY_LNNY_MT_NG_SNG As Int64
    Public Property PNDPTN_PRSNL_LNNY_TTL As Int64
    Public Property PNDPTN_PRSNL_LNNY_NDNSN_RPH As Int64
    Public Property PNDPTN_PRSNL_LNNY_MT_NG_SNG As Int64
    Public Property PNDPTN_PRSNL_TTL As Int64
    Public Property PNDPTN_PRSNL_NDNSN_RPH As Int64
    Public Property PNDPTN_PRSNL_MT_NG_SNG As Int64
    Public Property PNDPTN_BNG_NN_PRSNL_TTL As Int64
    Public Property PNDPTN_BNG_NN_PRSNL_NDNSN_RPH As Int64
    Public Property PNDPTN_BNG_NN_PRSNL_MT_NG_SNG As Int64
    Public Property PNDPTN_NN_PRSNL_LNNY_TTL As Int64
    Public Property PNDPTN_NN_PRSNL_LNNY_NDNSN_RPH As Int64
    Public Property PNDPTN_NN_PRSNL_LNNY_MT_NG_SNG As Int64
    Public Property PNDPTN_NN_PRSNL_TTL As Int64
    Public Property PNDPTN_NN_PRSNL_NDNSN_RPH As Int64
    Public Property PNDPTN_NN_PRSNL_MT_NG_SNG As Int64
    Public Property PNDPTN_TTL As Int64
    Public Property PNDPTN_NDNSN_RPH As Int64
    Public Property PNDPTN_MT_NG_SNG As Int64
    Public Property BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_TTL As Int64
    Public Property BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_NDNSN_RPH As Int64
    Public Property BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_MT_NG_SNG As Int64
    Public Property BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_TTL As Int64
    Public Property BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH As Int64
    Public Property BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG As Int64
    Public Property BBN_BNG_PRSNL_TTL As Int64
    Public Property BBN_BNG_PRSNL_NDNSN_RPH As Int64
    Public Property BBN_BNG_PRSNL_MT_NG_SNG As Int64
    Public Property BBN_PRM_TS_TRNSKS_SWP_TTL As Int64
    Public Property BBN_PRM_TS_TRNSKS_SWP_NDNSN_RPH As Int64
    Public Property BBN_PRM_TS_TRNSKS_SWP_MT_NG_SNG As Int64
    Public Property BBN_PRM_SRNS_TTL As Int64
    Public Property BBN_PRM_SRNS_NDNSN_RPH As Int64
    Public Property BBN_PRM_SRNS_MT_NG_SNG As Int64
    Public Property BBN_GJ_PH_DN_TNJNGN_TTL As Int64
    Public Property BBN_GJ_PH_DN_TNJNGN_NDNSN_RPH As Int64
    Public Property BBN_GJ_PH_DN_TNJNGN_MT_NG_SNG As Int64
    Public Property BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_TTL As Int64
    Public Property BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_NDNSN_RPH As Int64
    Public Property BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_MT_NG_SNG As Int64
    Public Property BBN_TNG_KRJ_LNNY_TTL As Int64
    Public Property BBN_TNG_KRJ_LNNY_NDNSN_RPH As Int64
    Public Property BBN_TNG_KRJ_LNNY_MT_NG_SNG As Int64
    Public Property BBN_TNG_KRJ_TTL As Int64
    Public Property BBN_TNG_KRJ_NDNSN_RPH As Int64
    Public Property BBN_TNG_KRJ_MT_NG_SNG As Int64
    Public Property BBN_NSNTF_PHK_KTG_TTL As Int64
    Public Property BBN_NSNTF_PHK_KTG_NDNSN_RPH As Int64
    Public Property BBN_NSNTF_PHK_KTG_MT_NG_SNG As Int64
    Public Property BBN_PMSRN_LNNY_TTL As Int64
    Public Property BBN_PMSRN_LNNY_NDNSN_RPH As Int64
    Public Property BBN_PMSRN_LNNY_MT_NG_SNG As Int64
    Public Property BBN_PMSRN_TTL As Int64
    Public Property BBN_PMSRN_NDNSN_RPH As Int64
    Public Property BBN_PMSRN_MT_NG_SNG As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_TTL As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_NDNSN_RPH As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_MT_NG_SNG As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_TTL As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_NDNSN_RPH As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_MT_NG_SNG As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_TTL As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_NDNSN_RPH As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_MT_NG_SNG As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_TTL As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_TTL As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_TTL As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NDNSN_RPH As Int64
    Public Property BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MT_NG_SNG As Int64
    Public Property BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_TTL As Int64
    Public Property BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_NDNSN_RPH As Int64
    Public Property BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_MT_NG_SNG As Int64
    Public Property BBN_PNYSTN_ST_TTP_DN_NVNTRS_TTL As Int64
    Public Property BBN_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH As Int64
    Public Property BBN_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG As Int64
    Public Property BBN_PNYSHNPNYSTN_TTL As Int64
    Public Property BBN_PNYSHNPNYSTN_NDNSN_RPH As Int64
    Public Property BBN_PNYSHNPNYSTN_MT_NG_SNG As Int64
    Public Property BBN_SW_TTL As Int64
    Public Property BBN_SW_NDNSN_RPH As Int64
    Public Property BBN_SW_MT_NG_SNG As Int64
    Public Property BBN_PMLHRN_DN_PRBKN_TTL As Int64
    Public Property BBN_PMLHRN_DN_PRBKN_NDNSN_RPH As Int64
    Public Property BBN_PMLHRN_DN_PRBKN_MT_NG_SNG As Int64
    Public Property BBN_DMNSTRS_DN_MM_TTL As Int64
    Public Property BBN_DMNSTRS_DN_MM_NDNSN_RPH As Int64
    Public Property BBN_DMNSTRS_DN_MM_MT_NG_SNG As Int64
    Public Property BBN_PRSNL_LNNY_TTL As Int64
    Public Property BBN_PRSNL_LNNY_NDNSN_RPH As Int64
    Public Property BBN_PRSNL_LNNY_MT_NG_SNG As Int64
    Public Property BBN_PRSNL_TTL As Int64
    Public Property BBN_PRSNL_NDNSN_RPH As Int64
    Public Property BBN_PRSNL_MT_NG_SNG As Int64
    Public Property BBN_NN_PRSNL_TTL As Int64
    Public Property BBN_NN_PRSNL_NDNSN_RPH As Int64
    Public Property BBN_NN_PRSNL_MT_NG_SNG As Int64
    Public Property BBN_TTL As Int64
    Public Property BBN_NDNSN_RPH As Int64
    Public Property BBN_MT_NG_SNG As Int64
    Public Property LB_RG_SBLM_PJK_TTL As Int64
    Public Property LB_RG_SBLM_PJK_NDNSN_RPH As Int64
    Public Property LB_RG_SBLM_PJK_MT_NG_SNG As Int64
    Public Property PJK_THN_BRJLN_TTL As Int64
    Public Property PJK_THN_BRJLN_NDNSN_RPH As Int64
    Public Property PJK_THN_BRJLN_MT_NG_SNG As Int64
    Public Property PNDPTN_BBN_PJK_TNGGHN_TTL As Int64
    Public Property PNDPTN_BBN_PJK_TNGGHN_NDNSN_RPH As Int64
    Public Property PNDPTN_BBN_PJK_TNGGHN_MT_NG_SNG As Int64
    Public Property LB_RG_BRSH_STLH_PJK_TTL As Int64
    Public Property LB_RG_BRSH_STLH_PJK_NDNSN_RPH As Int64
    Public Property LB_RG_BRSH_STLH_PJK_MT_NG_SNG As Int64
    Public Property KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL As Int64
    Public Property KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH As Int64
    Public Property KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG As Int64
    Public Property SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL As Int64
    Public Property SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH As Int64
    Public Property SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG As Int64
    Public Property KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL As Int64
    Public Property KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH As Int64
    Public Property KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG As Int64
    Public Property KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL As Int64
    Public Property KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH As Int64
    Public Property KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG As Int64
    Public Property KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL As Int64
    Public Property KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH As Int64
    Public Property KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG As Int64
    Public Property KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL As Int64
    Public Property KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH As Int64
    Public Property KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG As Int64
    Public Property LB_RG_BRSH_KMPRHNSF_THN_BRJLN_TTL As Int64
    Public Property LB_RG_BRSH_KMPRHNSF_THN_BRJLN_NDNSN_RPH As Int64
    Public Property LB_RG_BRSH_KMPRHNSF_THN_BRJLN_MT_NG_SNG As Int64
    Private _Masterid As String
    Private _Parentid As String
    Private _valuemaster As String
    Private _valueparent As String
    Private _valueelement As String

    Public Property ID As Int64
    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property
    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property

    Public Property Parentid() As String
        Get
            Return _Parentid
        End Get
        Set(ByVal Value As String)
            _Parentid = Value
        End Set
    End Property

    Public Property valuemaster() As String
        Get
            Return _valuemaster
        End Get
        Set(ByVal Value As String)
            _valuemaster = Value
        End Set
    End Property

    Public Property valueelement() As String
        Get
            Return _valueelement
        End Get
        Set(ByVal Value As String)
            _valueelement = Value
        End Set
    End Property

    Public Property valueparent() As String
        Get
            Return _valueparent
        End Get
        Set(ByVal Value As String)
            _valueparent = Value
        End Set
    End Property

    Public Property Masterid() As String
        Get
            Return _Masterid
        End Get
        Set(ByVal Value As String)
            _Masterid = Value
        End Set
    End Property



    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

End Class





