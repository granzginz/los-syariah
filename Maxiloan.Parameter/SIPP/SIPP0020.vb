﻿<Serializable()>
Public Class SIPP0020 : Inherits Common
    Private _ID As Int64
    Private _BULANDATA As String
    Private _NMR_ZN_KNTR_CBNG As String
    Private _TNGGL_ZN_KNTR_CBNG As Date
    Private _LMT_LNGKP As String
    Private _KCMTN As String
    Private _LKS_DT__KNTR As String
    Private _KD_PS As String
    Private _NMR_TLPN As String
    Private _JMLH_TNG_KRJ_KNTR_CBNG As Int64
    Private _NM_KPL_CBNG As String

    Private _ListData As DataTable
    Private _ListDataReport As DataSet
    Private _TotalRecords As Int64
    Private _Table As String
    Private _valueparent As String
    Private _valuemaster As String
    Private _valueelement As String

    Public Property ID As Long
        Get
            Return _ID
        End Get
        Set(value As Long)
            _ID = value
        End Set
    End Property

    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property

    Public Property NMR_ZN_KNTR_CBNG As String
        Get
            Return _NMR_ZN_KNTR_CBNG
        End Get
        Set(value As String)
            _NMR_ZN_KNTR_CBNG = value
        End Set
    End Property

    Public Property TNGGL_ZN_KNTR_CBNG As Date
        Get
            Return _TNGGL_ZN_KNTR_CBNG
        End Get
        Set(value As Date)
            _TNGGL_ZN_KNTR_CBNG = value
        End Set
    End Property

    Public Property LMT_LNGKP As String
        Get
            Return _LMT_LNGKP
        End Get
        Set(value As String)
            _LMT_LNGKP = value
        End Set
    End Property

    Public Property KCMTN As String
        Get
            Return _KCMTN
        End Get
        Set(value As String)
            _KCMTN = value
        End Set
    End Property

    Public Property LKS_DT__KNTR As String
        Get
            Return _LKS_DT__KNTR
        End Get
        Set(value As String)
            _LKS_DT__KNTR = value
        End Set
    End Property

    Public Property KD_PS As String
        Get
            Return _KD_PS
        End Get
        Set(value As String)
            _KD_PS = value
        End Set
    End Property

    Public Property NMR_TLPN As String
        Get
            Return _NMR_TLPN
        End Get
        Set(value As String)
            _NMR_TLPN = value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ_KNTR_CBNG As Long
        Get
            Return _JMLH_TNG_KRJ_KNTR_CBNG
        End Get
        Set(value As Long)
            _JMLH_TNG_KRJ_KNTR_CBNG = value
        End Set
    End Property

    Public Property NM_KPL_CBNG As String
        Get
            Return _NM_KPL_CBNG
        End Get
        Set(value As String)
            _NM_KPL_CBNG = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _ListData
        End Get
        Set(value As DataTable)
            _ListData = value
        End Set
    End Property

    Public Property ListDataReport As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(value As DataSet)
            _ListDataReport = value
        End Set
    End Property

    Public Property TotalRecords As Long
        Get
            Return _TotalRecords
        End Get
        Set(value As Long)
            _TotalRecords = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _Table
        End Get
        Set(value As String)
            _Table = value
        End Set
    End Property

    Public Property Valueparent As String
        Get
            Return _valueparent
        End Get
        Set(value As String)
            _valueparent = value
        End Set
    End Property

    Public Property Valuemaster As String
        Get
            Return _valuemaster
        End Get
        Set(value As String)
            _valuemaster = value
        End Set
    End Property

    Public Property Valueelement As String
        Get
            Return _valueelement
        End Get
        Set(value As String)
            _valueelement = value
        End Set
    End Property
End Class
