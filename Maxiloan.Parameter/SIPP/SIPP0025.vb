﻿<Serializable()>
Public Class SIPP0025 : Inherits Common

    Private _Table As String
    Private _BULANDATA As String
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _JNS_KNTR As String
    Private _NMR_SRT_PNCTTN As String
    Private _TNGGL_SRT_PNCTTN As Date
    Private _LMT As String
    Private _KCMTN As String
    Private _KOTA As String
    Private _NMR_TPLN As String
    Private _JMLH_TNG_KRJ As Int64
    Private _NM_PNNGGNG_JWB_KNTR As String
    Private _Masterid As String
    Private _Parentid As String
    Private _valuemaster As String
    Private _valueparent As String
    Private _valueelement As String



    Public Property ID As Int64
    Public Property BULANDATA As String
        Get
            Return _BULANDATA
        End Get
        Set(value As String)
            _BULANDATA = value
        End Set
    End Property
    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property

    Public Property Parentid() As String
        Get
            Return _Parentid
        End Get
        Set(ByVal Value As String)
            _Parentid = Value
        End Set
    End Property

    Public Property valuemaster() As String
        Get
            Return _valuemaster
        End Get
        Set(ByVal Value As String)
            _valuemaster = Value
        End Set
    End Property

    Public Property valueparent() As String
        Get
            Return _valueparent
        End Get
        Set(ByVal Value As String)
            _valueparent = Value
        End Set
    End Property

    Public Property Masterid() As String
        Get
            Return _Masterid
        End Get
        Set(ByVal Value As String)
            _Masterid = Value
        End Set
    End Property

    Public Property valueelement() As String
        Get
            Return _valueelement
        End Get
        Set(ByVal Value As String)
            _valueelement = Value
        End Set
    End Property

    Public Property JNS_KNTR() As String
        Get
            Return _JNS_KNTR
        End Get
        Set(ByVal Value As String)
            _JNS_KNTR = Value
        End Set
    End Property

    Public Property NMR_SRT_PNCTTN() As String
        Get
            Return _NMR_SRT_PNCTTN
        End Get
        Set(ByVal Value As String)
            _NMR_SRT_PNCTTN = Value
        End Set
    End Property

    Public Property TNGGL_SRT_PNCTTN() As Date
        Get
            Return _TNGGL_SRT_PNCTTN
        End Get
        Set(ByVal Value As Date)
            _TNGGL_SRT_PNCTTN = Value
        End Set
    End Property

    Public Property LMT() As String
        Get
            Return _LMT
        End Get
        Set(ByVal Value As String)
            _LMT = Value
        End Set
    End Property

    Public Property KCMTN() As String
        Get
            Return _KCMTN
        End Get
        Set(ByVal Value As String)
            _KCMTN = Value
        End Set
    End Property

    Public Property KOTA() As String
        Get
            Return _KOTA
        End Get
        Set(ByVal Value As String)
            _KOTA = Value
        End Set
    End Property

    Public Property NMR_TPLN() As String
        Get
            Return _NMR_TPLN
        End Get
        Set(ByVal Value As String)
            _NMR_TPLN = Value
        End Set
    End Property

    Public Property JMLH_TNG_KRJ() As Int64
        Get
            Return _JMLH_TNG_KRJ
        End Get
        Set(ByVal Value As Int64)
            _JMLH_TNG_KRJ = Value
        End Set
    End Property

    Public Property NM_PNNGGNG_JWB_KNTR() As String
        Get
            Return _NM_PNNGGNG_JWB_KNTR
        End Get
        Set(ByVal Value As String)
            _NM_PNNGGNG_JWB_KNTR = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

End Class





