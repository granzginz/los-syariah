﻿<Serializable()>
Public Class ExampleName : Inherits Common
    Private _idname As String
    Private _name As String
    Private _address As String
    Private _phone As String
    Private _contactperson_hp As String
    Private _usrupd As String
    Private _dtmupd As Date

    Private _listexamplename As DataTable

    Public AddEdit As String
    Public Output As String

    Public Property ListExampleName() As DataTable
        Get
            Return (CType(_listexamplename, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listexamplename = Value
        End Set
    End Property
    Public Property IDName() As String
        Get
            Return (CType(_idname, String))
        End Get
        Set(ByVal Value As String)
            _idname = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return (CType(_name, String))
        End Get
        Set(ByVal Value As String)
            _name = Value
        End Set
    End Property
    Public Property Address() As String
        Get
            Return (CType(_address, String))
        End Get
        Set(ByVal Value As String)
            _address = Value
        End Set
    End Property
    Public Property Phone() As String
        Get
            Return (CType(_phone, String))
        End Get
        Set(ByVal Value As String)
            _phone = Value
        End Set
    End Property
    Public Property COntactPerson_Hp() As String
        Get
            Return (CType(_contactperson_hp, String))
        End Get
        Set(ByVal Value As String)
            _contactperson_hp = Value
        End Set
    End Property
    Public Property UsrUpd() As String
        Get
            Return (CType(_usrupd, String))
        End Get
        Set(ByVal Value As String)
            _usrupd = Value
        End Set
    End Property
    Public Property DtmUpd() As String
        Get
            Return (CType(_dtmupd, Date))
        End Get
        Set(ByVal Value As String)
            _dtmupd = Value
        End Set
    End Property
End Class
