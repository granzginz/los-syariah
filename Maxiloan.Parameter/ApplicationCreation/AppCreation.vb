Imports System.Data
Imports System.Text

<Serializable()>
Public Class AppCreation : Inherits Common
    Public Property listdata As DataTable
    Public Property ProspectAppID As String
    Public Property ApplicationID As String
    Public Property CustomerType As String
    Public Property ProspectAppDate As String
    Public Property Name As String
    Public Property IDType As String
    Public Property IDNumber As String
    Public Property Gender As String
    Public Property BirthPlace As String
    Public Property BirthDate As String
    Public Property HomeStatus As String
    Public Property Address As String
    Public Property RT As String
    Public Property RW As String
    Public Property Kelurahan As String
    Public Property Kecamatan As String
    Public Property City As String
    Public Property ZipCode As String
    Public Property AreaPhone1 As String
    Public Property Phone1 As String
    Public Property AreaPhone2 As String
    Public Property Phone2 As String
    Public Property AreaFax As String
    Public Property Fax As String
    Public Property MobilePhone As String
    Public Property Email As String
    Public Property ProfessionID As String
    Public Property EmploymentSinceYear As Integer
    Public Property ProductID As String
    Public Property ProductOfferingID As String
    Public Property AssetCode As String
    Public Property OTRPrice As Double
    Public Property DPAmount As Double
    Public Property NTF As Double
    Public Property Tenor As Integer
    Public Property InstallmentAmount As Double
    Public Property FlatRate As Decimal
    Public Property EfektifRate As Decimal
    Public Property TotalBunga As Double
    Public Property FirstInstallment As Double
    Public Property ResaleValue As Decimal
    Public Property ResikoAngsuran As Integer
    Public Property DueDate As DateTime
    Public Property InsuranceCoyID As String
    Public Property InsuranceCoyBranchID As String
    Public Property ExistingPolicyNo As String
    Public Property IndustryTypeID As String
    Public Property CompanyStatus As String
    Public Property EstablishedSinceYear As Integer
    Public Property CreditScore As Double
    Public Property CreditScoringResult As String
    Public Property IsHold As Boolean
    Public Property UsrUpd As String
    Public Property DtmUpd As DateTime
    Public Property CustomerID As String
    Public Property DateEntryApplicationData As DateTime
    Public Property DateEntryAssetData As DateTime
    Public Property DateEntryDemografiData As DateTime
    Public Property DateEntryFinancialData As DateTime
    Public Property IsReferenced As Boolean
    Public Property PhoneReference As String
    Public Property StatusNasabah As String
    Public Property UsedNew As String
    Public Property ManufacturingYear As Integer
    Public Property AssetUsage As String
    Public Property LicensePlate As String
    Public Property DPAmountTradeIn As Double
    Public Property WayOfPayment As String
    Public Property NumOfDependence As Integer
    Public Property Education As String
    Public Property MaritalStatus As String
    Public Property StaySinceYear As Int16
    Public Property StaySinceMonth As Int16
    Public Property EmploymentSinceMonth As Int16
    Public Property MonthlyVariableIncome As Double
    Public Property MonthlyFixedIncome As Double
    Public Property LivingCostAmount As Double
    Public Property KodeIndustri As String
    Public Property KodeIndustriDetail As String
    Public Property ProfessionIDPasangan As String
    Public Property KodeIndustriPasangan As String
    Public Property KodeIndustriDetailPasangan As String
    Public Property RekeningTabungan As Boolean
    Public Property NoRekening As String
    Public Property PengalamanKredit As Boolean
    Public Property PengalamanKreditDari As String
    Public Property KepemilikanUnitLain As Boolean
    Public Property KepemilikanUnitLainTahun As Int16
    Public Property KepemilikanUnitLainMerk As String
    Public Property KepemilikanUnitLainKategori As String
    Public Property KepemilikanUnitLainPenggunaan As String
    'scoring modify ario juli 2019
    Public Property NegaraPembuat As String
    '
    Public Property TotalRecords As Integer
    Public Property JarakSurvey As Integer
    Public Property Err As String
    Public Property Mode As String
    Public Property Table As String
    Public Property StoreProcedure As String
    Public Property AOID As String
    'FICO
    Public Property Occupation As String
    Public Property OccupationSpouse As String
    Public Property NatureOfBusiness As String

    Public Property OfficeAreaPhone As String
    Public Property OfficePhone As String
    Public Property GuarantorAreaPhone As String
    Public Property GuarantorPhone As String
    Public Property AssetType As String
    Public Property AssetBrand As String
    Public Property Package As String
    Public Property NatureOfBusinessSpouse As String
    Public Property Provinsi As String
    Public Property NoRumah As String
    Public Property StreetName As String
    Public Property Country As String
    Public Property SupplierID As String
    Public Property ActivityDateStart As String
    Public Property ActivityDateEnd As String
    Public Property ActivityUser As String
    Public Property ActivitySeqNo As String
    Public Property ProspectSource As String
    Public Property BICheckingStatus As Integer
    Public Property ActivityType As String
    Public Property MotherName As String
    Public Property SIDNotes As String
    Public Property Notes As String
    Public Property OldAOID As String
    Public Property IsRelatedBNI As Boolean

    Public Property KetReference As String
    Public Property Approval As Approval
    Public Property HubunganLingkungan As String
    Public Property HomeLocation As String
    Public Property Skema As String
    Public Property HubBankLeasing As String
    Public Property Asset As String
    Public Property Sertifikat As String
End Class

<Serializable()>
Public Class ScoreComponentContent2

    Public Sub New(_ScoreSchemeID As String, _ScoreComponentID As String, _ValueContent As String, _ValueDescription As String, _ValueFrom As Decimal, _ValueTo As Decimal, _ScoreValue As Decimal, _ScoreAbsolute As Decimal, _ScoreStatus As String, _ContentSeqNo As Integer)
        ScoreSchemeID = _ScoreSchemeID
        ScoreComponentID = _ScoreComponentID
        ValueContent = _ValueContent
        ValueDescription = _ValueDescription
        ValueFrom = _ValueFrom
        ValueTo = _ValueTo
        ScoreValue = _ScoreValue
        ScoreStatus = _ScoreStatus
        ContentSeqNo = _ContentSeqNo
        ScoreAbsolute = _ScoreAbsolute
    End Sub

    Public Property ScoreSchemeID As String
    Public Property ScoreComponentID As String
    Public Property ValueContent As String
    Public Property ValueDescription As String
    Public Property ValueFrom As Decimal
    Public Property ValueTo As Decimal
    Public Property ScoreValue As Decimal
    Public Property ScoreStatus As String
    Public Property ContentSeqNo As Integer
    Public Property ScoreAbsolute As Decimal
End Class

<Serializable()>
Public Class ScoreSchemeComponent2

    Public Sub New(_Description As String, _CuttOffScore As Decimal, _PersonalApprovedScore As Decimal, _PersonalRejectScore As Decimal, _CompanyApprovedScore As Decimal, _CompanyRejectScore As Decimal, _ScoreComponentID As String, _Weight As Decimal, _SQLCmd As String, _CalculationType As String)
        CuttOffScore = _CuttOffScore
        Description = _Description
        PersonalApprovedScore = _PersonalApprovedScore
        PersonalRejectScore = _PersonalRejectScore
        CompanyApprovedScore = _CompanyApprovedScore
        CompanyRejectScore = _CompanyRejectScore
        ScoreComponentID = _ScoreComponentID
        Weight = _Weight
        SQLCmd = _SQLCmd
        CalculationType = _CalculationType
    End Sub


    Public Property Description As String
    Public Property PersonalApprovedScore As Decimal
    Public Property PersonalRejectScore As Decimal
    Public Property CompanyApprovedScore As Decimal
    Public Property CompanyRejectScore As Decimal
    Public Property ScoreComponentID As String
    Public Property Weight As Decimal
    Public Property SQLCmd As String
    Public Property CalculationType As String
    Public Property CuttOffScore As Decimal

End Class



<Serializable()>
Public Class ScoreResult2

    Public Sub New(_BranchID As String, _ProspectAppId As String, _ScoreComponentID As String, _ComponentContent As String, _Weight As Decimal, _ComponentValue As String,
                   _ScoreDescription As String)
        BranchID = _BranchID
        ProspectAppId = _ProspectAppId
        ScoreComponentID = _ScoreComponentID
        ComponentContent = _ComponentContent
        Weight = _Weight
        ComponentValue = _ComponentValue
        ScoreDescription = _ScoreDescription
        ScoreStatus = ""

    End Sub
    Public Property BranchID As String
    Public Property ProspectAppId As String
    Public Property ScoreComponentID As String
    Public Property ComponentContent As String
    Public Property ScoreValue As Decimal
    Public Property Weight As Decimal
    Public Property ComponentValue As String
    Public Property ScoreDescription As String
    Public Property ScoreStatus As String
    Public Property ContentSeqNo As Integer
    Public Property QueryResult As String
    Public Property ScoreAbsolute As Decimal

    Public ReadOnly Property ResultCalc As Decimal
        Get
            Return (Weight / 100) * ScoreValue
        End Get
    End Property

End Class


<Serializable()>
Public Class ScoreResults2
    Private _scoreResults As IList(Of Parameter.ScoreResult) = New List(Of Parameter.ScoreResult)

    Private _isFatalScore As Boolean = False
    Private _fatalStatusNote As New StringBuilder
    Private _lApproveScore As Decimal
    Private _lDblRejectScore As Decimal
    Private _lCuttOffScore As Decimal

    Public ReadOnly Property CuttOff As Decimal
        Get
            Return _lCuttOffScore
        End Get
    End Property
    Public Property ScoreSchemeID As String
    Sub New()
        _scoreResults = New List(Of Parameter.ScoreResult)

    End Sub

    Public Sub Add(item As Parameter.ScoreResult)
        _scoreResults.Add(item)
    End Sub

    Public Sub FatalStatusNote(note As String)

        If Not (_isFatalScore) Then
            _fatalStatusNote.AppendLine("Rejected With Fatal Score")
        End If

        _isFatalScore = True
        _fatalStatusNote.AppendLine(note)
    End Sub
    Public ReadOnly Property TotalScore As Decimal
        Get
            Dim result As Decimal = 0
            For Each item In _scoreResults
                result += item.ScoreValue
            Next
            Return result
        End Get
    End Property
    ReadOnly Property ResultCalc As Decimal
        Get
            Dim result As Decimal = 0
            For Each item In _scoreResults
                result += item.ResultCalc
            Next
            Return result
        End Get
    End Property

    Public ReadOnly Property ToDataTable As DataTable
        Get
            Dim dt = New DataTable()
            For Each s In New String() {"ScoreComponentID", "ComponentContent", "ScoreValue", "ScoreAbsolute", "Weight", "ComponentValue", "ScoreStatus", "ScoreDescription", "ContentSeqNo", "QueryResult"}
                dt.Columns.Add(New DataColumn(s))
            Next

            For Each item In _scoreResults
                Dim row = dt.NewRow()

                row("ScoreComponentID") = item.ScoreComponentID.Trim
                row("ComponentContent") = item.ComponentContent.Trim
                row("ScoreValue") = item.ScoreValue
                row("ScoreAbsolute") = item.ScoreAbsolute
                row("Weight") = item.Weight
                row("ComponentValue") = item.ComponentValue.Trim
                row("ScoreStatus") = item.ScoreStatus.Trim
                row("ScoreDescription") = item.ScoreDescription.Trim
                row("ContentSeqNo") = item.ContentSeqNo
                row("QueryResult") = item.QueryResult
                dt.Rows.Add(row)
            Next
            Return dt

        End Get
    End Property

    Public Sub SetRejectApproveScore(lApproveScore As Decimal, lRejectScore As Decimal, lCuttOffScore As Decimal)
        _lApproveScore = lApproveScore
        _lDblRejectScore = lRejectScore
        _lCuttOffScore = lCuttOffScore
    End Sub
    Public Function FatalCount() As Integer
        Return _scoreResults.Where(Function(x) x.ScoreStatus.Trim = "F").Count
    End Function
    Public Function WarningCount() As Integer
        Return _scoreResults.Where(Function(x) x.ScoreStatus.Trim = "W").Count
    End Function
    Public Function RejectCount() As Integer
        Return _scoreResults.Where(Function(x) x.ScoreStatus.Trim = "R").Count
    End Function
    Public Function NeutralCount() As Integer
        Return _scoreResults.Where(Function(x) x.ScoreStatus.Trim = "N").Count
    End Function
    'Public ReadOnly Property ScoringResultStatus As String
    '    Get
    '        Dim ret As String = ""
    '        If _isFatalScore Or _lDblRejectScore >= ResultCalc Then
    '            ret = "R"
    '        ElseIf _lApproveScore > ResultCalc And _lDblRejectScore < ResultCalc Then
    '            ret = "M"
    '        ElseIf _lApproveScore <= ResultCalc Then
    '            ret = "A"
    '        End If
    '        Return ret
    '    End Get
    'End Property

    Public ReadOnly Property FinalDecision As String
        Get

            If ResultCalc < _lCuttOffScore Or FatalCount() > 0 Then
                Return "MANUAL APPROVE"
            ElseIf ResultCalc > _lCuttOffScore And WarningCount() > 0 Then
                Return "AUTO APPROVE"
            ElseIf ResultCalc > _lCuttOffScore Then
                Return "AUTO APPROVE"
            Else
                Return "MANUAL APPROVE"
            End If
        End Get
    End Property


    Public ApplicationStep As String
    Public CSResult_Temp As String

End Class