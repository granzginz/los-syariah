

<Serializable()> _
    Public Class TransferAccount
    Inherits Maxiloan.Parameter.FinanceBase

#Region "Constanta"
    Private _bankAccountType As String
    Private _bankaccountid As String
    Private _bankAccountFrom As String
    Private _bankAccountTo As String
    Private _valueDate As Date
    Private _referenceNo As String
    Private _Amount As Double
    Private _Notes As String
    Private _bgNo As String
    Private _bgDate As Date
    Private _TransferType As String
    Private _TransactionIDTo As String
    Private _TransactionIDFrom As String
    Private _TransferTypeDesc As String
    Private _BranchIDTo As String
    Private _BranchIDFrom As String
    Private _Listdata As DataTable
    Private _Desc As String
    Private _transferNo As String
    Private _NDtTable As DataTable
    Private _strID As String
    Private _strError As String
    Private _noBonMerah As String
#End Region

#Region "TransferAccount"
    Public Property bankAccountType() As String
        Get
            Return (CType(_bankAccountType, String))
        End Get
        Set(ByVal Value As String)
            _bankAccountType = Value
        End Set
    End Property


    Public Property bankaccountid() As String
        Get
            Return (CType(_bankaccountid, String))
        End Get
        Set(ByVal Value As String)
            _bankaccountid = Value
        End Set
    End Property

    Public Property bankAccountFrom() As String
        Get
            Return (CType(_bankAccountFrom, String))
        End Get
        Set(ByVal Value As String)
            _bankAccountFrom = Value
        End Set
    End Property

    Public Property bankAccountTo() As String
        Get
            Return (CType(_bankAccountTo, String))
        End Get
        Set(ByVal Value As String)
            _bankAccountTo = Value
        End Set
    End Property


    Public Property valueDate() As Date
        Get
            Return (CType(_valueDate, Date))
        End Get
        Set(ByVal Value As Date)
            _valueDate = Value
        End Set
    End Property

    Public Property referenceNo() As String
        Get
            Return (CType(_referenceNo, String))
        End Get
        Set(ByVal Value As String)
            _referenceNo = Value
        End Set
    End Property

    Public Property Amount() As Double
        Get
            Return (CType(_Amount, Double))
        End Get
        Set(ByVal Value As Double)
            _Amount = Value
        End Set
    End Property


    Public Property Notes() As String
        Get
            Return (CType(_Notes, String))
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property

    Public Property bgNo() As String
        Get
            Return (CType(_bgNo, String))
        End Get
        Set(ByVal Value As String)
            _bgNo = Value
        End Set
    End Property

    Public Property bgDate() As Date
        Get
            Return (CType(_bgDate, Date))
        End Get
        Set(ByVal Value As Date)
            _bgDate = Value
        End Set
    End Property

    Public Property Listdata() As DataTable
        Get
            Return (CType(_Listdata, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _Listdata = Value
        End Set
    End Property
    Public Property Desc() As String
        Get
            Return (CType(_Desc, String))
        End Get
        Set(ByVal Value As String)
            _Desc = Value
        End Set
    End Property

    Public Property TransferNO() As String
        Get
            Return (CType(_transferNo, String))
        End Get
        Set(ByVal Value As String)
            _transferNo = Value
        End Set
    End Property

    Public Property NDtTable() As DataTable
        Get
            Return (CType(_NDtTable, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _NDtTable = Value
        End Set
    End Property

    Public Property strID() As String
        Get
            Return (CType(_strID, String))
        End Get
        Set(ByVal Value As String)
            _strID = Value
        End Set
    End Property

    Public Property strError() As String
        Get
            Return (CType(_strError, String))
        End Get
        Set(ByVal Value As String)
            _strError = Value
        End Set
    End Property

    Public Property NoBonMerah() As String
        Get
            Return (CType(_noBonMerah, String))
        End Get
        Set(ByVal Value As String)
            _noBonMerah = Value
        End Set
    End Property
#End Region

#Region "TransferHOBranch"
    Public Property TransferType() As String
        Get
            Return (CType(_TransferType, String))
        End Get
        Set(ByVal Value As String)
            _TransferType = Value
        End Set
    End Property

    Public Property TransferTypeDesc() As String
        Get
            Return (CType(_TransferTypedesc, String))
        End Get
        Set(ByVal Value As String)
            _TransferTypedesc = Value
        End Set
    End Property


    Public Property TransactionIDfrom() As String
        Get
            Return (CType(_TransactionIDFrom, String))
        End Get
        Set(ByVal Value As String)
            _TransactionIDFrom = Value
        End Set
    End Property

    Public Property TransactionIDTo() As String
        Get
            Return (CType(_TransactionIDTo, String))
        End Get
        Set(ByVal Value As String)
            _TransactionIDTo = Value
        End Set
    End Property


    Public Property BranchIDfrom() As String
        Get
            Return (CType(_BranchIDfrom, String))
        End Get
        Set(ByVal Value As String)
            _BranchIDfrom = Value
        End Set
    End Property


    Public Property BranchIDTo() As String
        Get
            Return (CType(_BranchIDTo, String))
        End Get
        Set(ByVal Value As String)
            _BranchIDTo = Value
        End Set
    End Property

    'Add 
    Public Property PaymentTypeID As String
    Public Property JenisTransfer As String
#End Region
End Class
