
<Serializable()> _
Public Class TransferFundInquiry : Inherits Common

    Private _listReport As DataSet
    Private _ListData As DataTable
    Public Property ListReport() As DataSet
        Get
            Return _listReport
        End Get
        Set(ByVal Value As DataSet)
            _listReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal Value As DataTable)
            _ListData = Value
        End Set
    End Property


End Class
