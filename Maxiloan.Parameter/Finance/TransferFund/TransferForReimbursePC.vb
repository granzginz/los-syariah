

<Serializable()> _
Public Class TransferForReimbursePC : Inherits Maxiloan.Parameter.Common
    Private _listTRFund As DataTable
    Private _requestno As String
    Private _bankaccountid As String
    Private _valuedate As String
    Private _referenceno As String
    Private _bankaccountfrom As String
    Private _branchidto As String
    'Private _amount As Integer
    Private _amount As Double
    Private _notes As String
    Private _banktype As String
    Private _bgno As String
    Private _duedate As String
    Private _hasil As Integer
    Private _companyid As String
    Private _str As String

    Public Property str() As String
        Get
            Return _str
        End Get
        Set(ByVal Value As String)
            _str = Value
        End Set
    End Property
    Public Property CompanyID() As String
        Get
            Return _companyid
        End Get
        Set(ByVal Value As String)
            _companyid = Value
        End Set
    End Property

    Public Property Hasil() As Integer
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Integer)
            _hasil = Value
        End Set
    End Property

    Public Property DueDate() As String
        Get
            Return _duedate
        End Get
        Set(ByVal Value As String)
            _duedate = Value
        End Set
    End Property

    Public Property BGNo() As String
        Get
            Return _bgno
        End Get
        Set(ByVal Value As String)
            _bgno = Value
        End Set
    End Property

    Public Property BankType() As String
        Get
            Return _banktype
        End Get
        Set(ByVal Value As String)
            _banktype = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property

    'Public Property Amount() As Integer
    '    Get
    '        Return _amount
    '    End Get
    '    Set(ByVal Value As Integer)
    '        _amount = Value
    '    End Set
    'End Property
    Public Property Amount() As Double
        Get
            Return _amount
        End Get
        Set(ByVal Value As Double)
            _amount = Value
        End Set
    End Property

    Public Property BranchIDTo() As String
        Get
            Return _branchidto
        End Get
        Set(ByVal Value As String)
            _branchidto = Value
        End Set
    End Property
    Public Property BankAccountFrom() As String
        Get
            Return _bankaccountfrom
        End Get
        Set(ByVal Value As String)
            _bankaccountfrom = Value
        End Set
    End Property

    Public Property ReferenceNo() As String
        Get
            Return _referenceno
        End Get
        Set(ByVal Value As String)
            _referenceno = Value
        End Set
    End Property

    Public Property ValueDate() As String
        Get
            Return _valuedate
        End Get
        Set(ByVal Value As String)
            _valuedate = Value
        End Set
    End Property
    Public Property RequestNo() As String
        Get
            Return _requestno
        End Get
        Set(ByVal Value As String)
            _requestno = Value
        End Set
    End Property

    Public Property ListTRFund() As DataTable
        Get
            Return _listTRFund
        End Get
        Set(ByVal Value As DataTable)
            _listTRFund = Value
        End Set
    End Property

    Public Property BankAccountID() As String
        Get
            Return _bankaccountid
        End Get
        Set(ByVal Value As String)
            _bankaccountid = Value
        End Set
    End Property
    'Add 
    Public Property PaymentTypeID As String
    Public Property JenisTransfer As String
    Public Property TransferNo As String
    Public Property Otorisasi As String
End Class
