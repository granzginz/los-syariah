
<Serializable()> _
Public Class APDisbSelec : Inherits Common
    Private _listapsele As DataTable
    Private _listapgroup As DataTable
    Private _accountpayableno As String
    Private _Status As String
    Private _BranchID As String
    Private _BusinessDate As Date
    Private _DueDate As Date
    Private _ApplicationId As String
    Private _PaymentLocation As String
    Private _PVAmount As Double
    Private _Coa As String
    Private _APType As String
    Private _APTo As String
    Private _AccountNameTo As String
    Private _AccountNoTo As String
    Private _BankNameTo As String
    Private _BankBranchTo As String
    Private _BankAccountTo As String
    Private _BankAccountID As String
    Private _PaymentTypeID As String
    Private _RequestBy As String
    Private _paymentvouncherno As String
    Private _apdetail As DataTable
    Private _amountpv As Double
    Private _strerror As String
    Private _InvoiceNo As String
    Private _listview As DataTable
    Private _InvoiceDate As Date
    Private _PODate As Date
    Private _PONo As String
    Private _AssetDesc As String
    Private _otrprice As Double
    Private _CustomerName As String
    Private _CustomerID As String
    Private _AgreementNo As String
    Private _BgNo As String
    Private _recipientname As String
    Private _paymentVoucherDate As Date
    Private _BGDueDate As Date
    Private _StatusDate As Date
    Private _ApprovalBy As String
    Private _ApprovalDate As Date
    Private _ApprovalNotes As String
    Private _paymentNotes As String
    Private _pvduedate As Date
    Private _ContractPrepaidAmount As Double
    Private _OldPaymentLocation As String

    Private _PaymentNote As String
    Private _JenisTransfer As String
    Private _DeductionAmount As Decimal
    Private _TotalAmount As Decimal
    Private _BankAccountName As String
    Private _BankBranch As String
    Private _APAmount As Decimal
    Private _APDate As Date
    Private _Name As String
    Private _NPWP As String
    Private _TDP As String
    Private _SIUP As String
    Private _Address As String
    Private _RTRW As String
    Private _Kelurahan As String
    Private _Kecamatan As String
    Private _Kota As String
    Private _KodePos As String
    Private _Tlp1 As String
    Private _Tlp2 As String
    Private _Fax As String
    Private _ContactPersonName As String
    Private _ContactPersonTitle As String
    Private _EMail As String
    Private _MobilePhone As String
 
    Private _AccountNo As String
    Private _AccountName As String
    Public Property Tlp1() As String
        Get
            Return (CType(_Tlp1, String))
        End Get
        Set(ByVal Value As String)
            _Tlp1 = Value
        End Set
    End Property
    Public Property Tlp2() As String
        Get
            Return (CType(_Tlp2, String))
        End Get
        Set(ByVal Value As String)
            _Tlp2 = Value
        End Set
    End Property
    Public Property AccountName() As String
        Get
            Return (CType(_AccountName, String))
        End Get
        Set(ByVal Value As String)
            _AccountName = Value
        End Set
    End Property
    Public Property AccountNo() As String
        Get
            Return (CType(_AccountNo, String))
        End Get
        Set(ByVal Value As String)
            _AccountNo = Value
        End Set
    End Property
    Public Property MobilePhone() As String
        Get
            Return (CType(_MobilePhone, String))
        End Get
        Set(ByVal Value As String)
            _MobilePhone = Value
        End Set
    End Property
    Public Property EMail() As String
        Get
            Return (CType(_EMail, String))
        End Get
        Set(ByVal Value As String)
            _EMail = Value
        End Set
    End Property
    Public Property ContactPersonTitle() As String
        Get
            Return (CType(_ContactPersonTitle, String))
        End Get
        Set(ByVal Value As String)
            _ContactPersonTitle = Value
        End Set
    End Property
    Public Property ContactPersonName() As String
        Get
            Return (CType(_ContactPersonName, String))
        End Get
        Set(ByVal Value As String)
            _ContactPersonName = Value
        End Set
    End Property
    Public Property Fax() As String
        Get
            Return (CType(_Fax, String))
        End Get
        Set(ByVal Value As String)
            _Fax = Value
        End Set
    End Property
    Public Property KodePos() As String
        Get
            Return (CType(_KodePos, String))
        End Get
        Set(ByVal Value As String)
            _KodePos = Value
        End Set
    End Property
    Public Property Kota() As String
        Get
            Return (CType(_Kota, String))
        End Get
        Set(ByVal Value As String)
            _Kota = Value
        End Set
    End Property

    Public Property Kecamatan() As String
        Get
            Return (CType(_Kecamatan, String))
        End Get
        Set(ByVal Value As String)
            _Kecamatan = Value
        End Set
    End Property

    Public Property Kelurahan() As String
        Get
            Return (CType(_Kelurahan, String))
        End Get
        Set(ByVal Value As String)
            _Kelurahan = Value
        End Set
    End Property

    Public Property Address() As String
        Get
            Return (CType(_Address, String))
        End Get
        Set(ByVal Value As String)
            _Address = Value
        End Set
    End Property
    Public Property RTRW() As String
        Get
            Return (CType(_RTRW, String))
        End Get
        Set(ByVal Value As String)
            _RTRW = Value
        End Set
    End Property

    Public Property TDP() As String
        Get
            Return (CType(_TDP, String))
        End Get
        Set(ByVal Value As String)
            _TDP = Value
        End Set
    End Property

    Public Property SIUP() As String
        Get
            Return (CType(_SIUP, String))
        End Get
        Set(ByVal Value As String)
            _SIUP = Value
        End Set
    End Property

    Public Property NPWP() As String
        Get
            Return (CType(_NPWP, String))
        End Get
        Set(ByVal Value As String)
            _NPWP = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return (CType(_Name, String))
        End Get
        Set(ByVal Value As String)
            _Name = Value
        End Set
    End Property

    Public Property ListAPSele() As DataTable
        Get
            Return (CType(_listapsele, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listapsele = Value
        End Set
    End Property

    Public Property ListAPGroupSelection() As DataTable
        Get
            Return (CType(_listapgroup, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listapgroup = Value
        End Set
    End Property

    Public Property AccountPayableNo() As String
        Get
            Return (CType(_accountpayableno, String))
        End Get
        Set(ByVal Value As String)
            _accountpayableno = Value
        End Set
    End Property

    Public Property Status() As String
        Get
            Return (CType(_Status, String))
        End Get
        Set(ByVal Value As String)
            _Status = Value
        End Set
    End Property

    Public Property APType() As String
        Get
            Return (CType(_APType, String))
        End Get
        Set(ByVal Value As String)
            _APType = Value
        End Set
    End Property

    Public Property DueDate() As Date
        Get
            Return (CType(_DueDate, Date))
        End Get
        Set(ByVal Value As Date)
            _DueDate = Value
        End Set
    End Property


    Public Property PaymentLocation() As String
        Get
            Return (CType(_PaymentLocation, String))
        End Get
        Set(ByVal Value As String)
            _PaymentLocation = Value
        End Set
    End Property

    Public Property APTo() As String
        Get
            Return (CType(_APTo, String))
        End Get
        Set(ByVal Value As String)
            _APTo = Value
        End Set
    End Property

    Public Property AccountNameTo() As String
        Get
            Return (CType(_AccountNameTo, String))
        End Get
        Set(ByVal Value As String)
            _AccountNameTo = Value
        End Set
    End Property

    Public Property PVAmount() As Double
        Get
            Return (CType(_PVAmount, Double))
        End Get
        Set(ByVal Value As Double)
            _PVAmount = Value
        End Set
    End Property

    Public Property BankNameTo() As String
        Get
            Return (CType(_BankNameTo, String))
        End Get
        Set(ByVal Value As String)
            _BankNameTo = Value
        End Set
    End Property

    Public Property BankBranchTo() As String
        Get
            Return (CType(_BankBranchTo, String))
        End Get
        Set(ByVal Value As String)
            _BankBranchTo = Value
        End Set
    End Property

    Public Property BankAccountTo() As String
        Get
            Return (CType(_BankAccountTo, String))
        End Get
        Set(ByVal Value As String)
            _BankAccountTo = Value
        End Set
    End Property

    Public Property BankAccountID() As String
        Get
            Return (CType(_BankAccountID, String))
        End Get
        Set(ByVal Value As String)
            _BankAccountID = Value
        End Set
    End Property

    Public Property Coa() As String
        Get
            Return (CType(_Coa, String))
        End Get
        Set(ByVal Value As String)
            _Coa = Value
        End Set
    End Property

    Public Property PaymentTypeID() As String
        Get
            Return (CType(_PaymentTypeID, String))
        End Get
        Set(ByVal Value As String)
            _PaymentTypeID = Value
        End Set
    End Property

    Public Property PaymentVoucherNo() As String
        Get
            Return CType(_paymentvouncherno, String)
        End Get
        Set(ByVal Value As String)
            _paymentvouncherno = Value
        End Set
    End Property

    Public Property AccountNoTo() As String
        Get
            Return CType(_AccountNoTo, String)
        End Get
        Set(ByVal Value As String)
            _AccountNoTo = Value
        End Set
    End Property

    Public Property APDetail() As DataTable
        Get
            Return CType(_apdetail, DataTable)
        End Get
        Set(ByVal Value As DataTable)
            _apdetail = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return CType(_ApplicationId, String)
        End Get
        Set(ByVal Value As String)
            _ApplicationId = Value
        End Set
    End Property

    Public Property StrError() As String
        Get
            Return CType(_strerror, String)
        End Get
        Set(ByVal Value As String)
            _strerror = Value
        End Set
    End Property


    Public Property InvoiceNo() As String
        Get
            Return CType(_InvoiceNo, String)
        End Get
        Set(ByVal Value As String)
            _InvoiceNo = Value
        End Set
    End Property

    Public Property listview() As DataTable
        Get
            Return (CType(_listview, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listview = Value
        End Set
    End Property

    Public Property InvoiceDate() As Date
        Get
            Return CType(_InvoiceDate, Date)
        End Get
        Set(ByVal Value As Date)
            _InvoiceDate = Value
        End Set
    End Property

    Public Property PODate() As Date
        Get
            Return CType(_PODate, Date)
        End Get
        Set(ByVal Value As Date)
            _PODate = Value
        End Set
    End Property

    Public Property AssetDesc() As String
        Get
            Return CType(_AssetDesc, String)
        End Get
        Set(ByVal Value As String)
            _AssetDesc = Value
        End Set
    End Property

    Public Property PONo() As String
        Get
            Return CType(_PONo, String)
        End Get
        Set(ByVal Value As String)
            _PONo = Value
        End Set
    End Property

    Public Property otrprice() As Double
        Get
            Return CType(_otrprice, Double)
        End Get
        Set(ByVal Value As Double)
            _otrprice = Value
        End Set
    End Property

    Public Property Customerid() As String
        Get
            Return CType(_CustomerID, String)
        End Get
        Set(ByVal Value As String)
            _CustomerID = Value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return CType(_CustomerName, String)
        End Get
        Set(ByVal Value As String)
            _CustomerName = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return CType(_AgreementNo, String)
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property

    Public Property BGNo() As String
        Get
            Return CType(_BgNo, String)
        End Get
        Set(ByVal Value As String)
            _BgNo = Value
        End Set
    End Property

    Public Property recipientname() As String
        Get
            Return CType(_recipientname, String)
        End Get
        Set(ByVal Value As String)
            _recipientname = Value
        End Set
    End Property

    Public Property PaymentVoucherDate() As Date
        Get
            Return CType(_paymentVoucherDate, Date)
        End Get
        Set(ByVal Value As Date)
            _paymentVoucherDate = Value
        End Set
    End Property

    Public Property BGDueDate() As Date
        Get
            Return CType(_BGDueDate, Date)
        End Get
        Set(ByVal Value As Date)
            _BGDueDate = Value
        End Set
    End Property

    Public Property StatusDate() As Date
        Get
            Return CType(_StatusDate, Date)
        End Get
        Set(ByVal Value As Date)
            _StatusDate = Value
        End Set
    End Property

    Public Property RequestBy() As String
        Get
            Return CType(_RequestBy, String)
        End Get
        Set(ByVal Value As String)
            _RequestBy = Value
        End Set
    End Property

    Public Property ApprovalBy() As String
        Get
            Return CType(_ApprovalBy, String)
        End Get
        Set(ByVal Value As String)
            _ApprovalBy = Value
        End Set
    End Property

    Public Property ApprovalDate() As Date
        Get
            Return CType(_ApprovalDate, Date)
        End Get
        Set(ByVal Value As Date)
            _ApprovalDate = Value
        End Set
    End Property

    Public Property ApprovalNotes() As String
        Get
            Return CType(_ApprovalNotes, String)
        End Get
        Set(ByVal Value As String)
            _ApprovalNotes = Value
        End Set
    End Property
    Public Property PaymentNotes() As String
        Get
            Return CType(_paymentNotes, String)
        End Get
        Set(ByVal Value As String)
            _paymentNotes = Value
        End Set
    End Property

    Public Property pvduedate() As Date
        Get
            Return CType(_pvduedate, Date)
        End Get
        Set(ByVal Value As Date)
            _pvduedate = Value
        End Set
    End Property

    Public Property ContractPrepaidAmount() As Double
        Get
            Return CType(_ContractPrepaidAmount, Double)
        End Get
        Set(ByVal Value As Double)
            _ContractPrepaidAmount = Value
        End Set
    End Property

    Public Property OldPaymentLocation() As String
        Get
            Return (CType(_OldPaymentLocation, String))
        End Get
        Set(ByVal Value As String)
            _OldPaymentLocation = Value
        End Set
    End Property

    Public Property PaymentNote() As String
        Get
            Return (CType(_PaymentNote, String))
        End Get
        Set(ByVal Value As String)
            _PaymentNote = Value
        End Set
    End Property

    Public Property JenisTransfer() As String
        Get
            Return (CType(_JenisTransfer, String))
        End Get
        Set(ByVal Value As String)
            _JenisTransfer = Value
        End Set
    End Property

    Public Property DeductionAmount() As Decimal
        Get
            Return (CType(_DeductionAmount, Decimal))
        End Get
        Set(ByVal Value As Decimal)
            _DeductionAmount = Value
        End Set
    End Property

    Public Property TotalAmount() As Decimal
        Get
            Return (CType(_TotalAmount, Decimal))
        End Get
        Set(ByVal Value As Decimal)
            _TotalAmount = Value
        End Set
    End Property

    Public Property BankAccountName() As String
        Get
            Return (CType(_BankAccountName, String))
        End Get
        Set(ByVal Value As String)
            _BankAccountName = Value
        End Set
    End Property

    Public Property BankBranch() As String
        Get
            Return (CType(_BankBranch, String))
        End Get
        Set(ByVal Value As String)
            _BankBranch = Value
        End Set
    End Property

    Public Property APAmount() As Decimal
        Get
            Return (CType(_APAmount, Decimal))
        End Get
        Set(ByVal Value As Decimal)
            _APAmount = Value
        End Set
    End Property
    Public Property APDate() As Date
        Get
            Return CType(_APDate, Date)
        End Get
        Set(ByVal Value As Date)
            _APDate = Value
        End Set
    End Property

    Public Property AttachedFile As String
End Class
