<Serializable()> _
Public Class AgentFeeDisbursement : Inherits Common
    Private _dataset As Dataset
    Private _datatable As Datatable
    Private _hasil As Boolean
    Private _multiApplicationID As String
    Private _errorMessage As String
    Private _referenceno As String
    Private _bankaccountid As String
    Private _wop As String
    Private _Notes As String
    Private _amount As Double
    Private _valuedate As Date
    Private _departementid As String
    Private _agreementNo As String
    Private _recepientName As String
    Public Property AgreementNo() As String
        Get
            Return _agreementNo
        End Get
        Set(ByVal Value As String)
            _agreementNo = Value
        End Set
    End Property
    Public Property RecepientName() As String
        Get
            Return _recepientName
        End Get
        Set(ByVal Value As String)
            _recepientName = Value
        End Set
    End Property
    Public Property Dataset() As Dataset
        Get
            Return _dataset
        End Get
        Set(ByVal Value As Dataset)
            _dataset = Value
        End Set
    End Property

    Public Property Datatable() As Datatable
        Get
            Return _datatable
        End Get
        Set(ByVal Value As Datatable)
            _datatable = Value
        End Set
    End Property

    Public Property Hasil() As Boolean
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Boolean)
            _hasil = Value
        End Set
    End Property

    Public Property MultiApplicationID() As String
        Get
            Return (CType(_multiApplicationID, String))
        End Get
        Set(ByVal Value As String)
            _multiApplicationID = Value
        End Set
    End Property

    Public Property ErrorMessage() As String
        Get
            Return (CType(_errorMessage, String))
        End Get
        Set(ByVal Value As String)
            _errorMessage = Value
        End Set
    End Property
    Public Property ReferenceNo() As String
        Get
            Return (CType(_referenceno, String))
        End Get
        Set(ByVal Value As String)
            _referenceno = Value
        End Set
    End Property
    Public Property BankAccountID() As String
        Get
            Return (CType(_bankaccountid, String))
        End Get
        Set(ByVal Value As String)
            _bankaccountid = Value
        End Set
    End Property
    Public Property WOP() As String
        Get
            Return (CType(_wop, String))
        End Get
        Set(ByVal Value As String)
            _wop = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return (CType(_Notes, String))
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property
    Public Property Amount() As Double
        Get
            Return (CType(_amount, Double))
        End Get
        Set(ByVal Value As Double)
            _amount = Value
        End Set
    End Property
    Public Property ValueDate() As Date
        Get
            Return (CType(_valuedate, Date))
        End Get
        Set(ByVal Value As Date)
            _valuedate = Value
        End Set
    End Property

    Public Property DepartementID() As String
        Get
            Return (CType(_departementid, String))
        End Get
        Set(ByVal Value As String)
            _departementid = Value
        End Set
    End Property
End Class
