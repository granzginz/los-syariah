<Serializable()> _
Public Class InstallmentDueDate : Inherits Maxiloan.Parameter.Common
    Private _dataSet As DataSet
    Private _StartDate As Date
    Private _EndDate As Date
    Private _CGID As String
    Private _CollectorGroupName As String
    Public Property CollectorGroupName() As String
        Get
            Return _CollectorGroupName
        End Get
        Set(ByVal Value As String)
            _CollectorGroupName = Value
        End Set
    End Property
    Public Property SelectedCGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property

    Public Property Dataset() As Dataset
        Get
            Return _dataSet
        End Get
        Set(ByVal Value As Dataset)
            _dataSet = Value
        End Set
    End Property
    Public Property StartDate() As Date
        Get
            Return _StartDate
        End Get
        Set(ByVal Value As Date)
            _StartDate = Value
        End Set
    End Property
    Public Property EndDate() As Date
        Get
            Return _EndDate
        End Get
        Set(ByVal Value As Date)
            _EndDate = Value
        End Set
    End Property
End Class

