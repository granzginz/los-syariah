
<Serializable()> _
Public Class APDisbApp : Inherits Common
    Private _listapappr As DataTable
    Private _paymenyvoucherno As String
    Private _pvstatus As String
    Private _notes As String
    Private _updatemode As String
    Private _recipientname As String
    Private _bgno As String
    Private _bgduedate As Date
    Private _bankaccountid As String
    Private _aptype As String
    Private _applicationid As String
    Private _pvamount As String
    Private _pvdate As Date
    Private _flags As String
    Private _flag As String
    Private _wop As String
    Private _referenceno As String
    Private _tahunto As String
    Private _tahunfrom As String
    Private _bulanto As String
    Private _bulanfrom As String
    Private _ListAPReport As DataSet

    Private _paymentvoucherno_DT As DataTable
    Private _paymentNote As String
    Private _approvalBy As String
    Private _approvalDate As DateTime
    Private _nilaiApproval As Decimal
    Private _requestTo As String

    Private _no As String
    Private _tanggal As DateTime
    Private _kepada As String
    Private _dari As String
    Private _perihal1 As String
    Private _perihal2 As String
    Private _cabang As String
    Private _supplier As String
    Private _namaDebitur As String
    Private _noKontrak As String
    Private _jenisKendararaan As String
    Private _noPolisi As String
    Private _gabungRefundSupplier As Boolean
    Private _namaPenerima As String
    Private _bankPenerima As String
    Private _noRekening As String
    Private _seq As Int32
    Private _transaksi As String
    Private _nilai As Decimal
    Private _TglBayar As Date
    Private _ListReport As DataSet
    Public RequestNoTemp As DataTable
    Public hasil As Integer
    Public Table As String
    Public GM1 As String
    Public GM2 As String
    Public Direksi As String
    Public EmployeeID As String
    Public SpName As String
    Public GM3 As String
    Public RequestNo As String

    Public Property ListReport() As DataSet
        Get
            Return CType(_ListReport, DataSet)
        End Get
        Set(ByVal Value As DataSet)
            _ListReport = Value
        End Set
    End Property
    Public Property NO() As String
        Get
            Return (CType(_no, String))
        End Get
        Set(ByVal Value As String)
            _no = Value
        End Set
    End Property

    Public Property Tanggal() As DateTime
        Get
            Return (CType(_tanggal, DateTime))
        End Get
        Set(ByVal Value As DateTime)
            _tanggal = Value
        End Set
    End Property

    Public Property Kepada() As String
        Get
            Return (CType(_kepada, String))
        End Get
        Set(ByVal Value As String)
            _kepada = Value
        End Set
    End Property
    Public Property Dari() As String
        Get
            Return (CType(_dari, String))
        End Get
        Set(ByVal Value As String)
            _dari = Value
        End Set
    End Property
    Public Property Perihal1() As String
        Get
            Return (CType(_perihal1, String))
        End Get
        Set(ByVal Value As String)
            _perihal1 = Value
        End Set
    End Property
    Public Property Perihal2() As String
        Get
            Return (CType(_perihal2, String))
        End Get
        Set(ByVal Value As String)
            _perihal2 = Value
        End Set
    End Property
    Public Property Cabang() As String
        Get
            Return (CType(_cabang, String))
        End Get
        Set(ByVal Value As String)
            _cabang = Value
        End Set
    End Property

    Public Property Supplier() As String
        Get
            Return (CType(_supplier, String))
        End Get
        Set(ByVal Value As String)
            _supplier = Value
        End Set
    End Property

    Public Property NamaDebitur() As String
        Get
            Return (CType(_namaDebitur, String))
        End Get
        Set(ByVal Value As String)
            _namaDebitur = Value
        End Set
    End Property
    Public Property NoKontrak() As String
        Get
            Return (CType(_noKontrak, String))
        End Get
        Set(ByVal Value As String)
            _noKontrak = Value
        End Set
    End Property
    Public Property JenisKendararaan() As String
        Get
            Return (CType(_jenisKendararaan, String))
        End Get
        Set(ByVal Value As String)
            _jenisKendararaan = Value
        End Set
    End Property
    Public Property NoPolisi() As String
        Get
            Return (CType(_noPolisi, String))
        End Get
        Set(ByVal Value As String)
            _noPolisi = Value
        End Set
    End Property
    Public Property GabungRefundSupplier() As Boolean
        Get
            Return (CType(_gabungRefundSupplier, Boolean))
        End Get
        Set(ByVal Value As Boolean)
            _gabungRefundSupplier = Value
        End Set
    End Property
    Public Property NamaPenerima() As String
        Get
            Return (CType(_namaPenerima, String))
        End Get
        Set(ByVal Value As String)
            _namaPenerima = Value
        End Set
    End Property
    Public Property BankPenerima() As String
        Get
            Return (CType(_bankPenerima, String))
        End Get
        Set(ByVal Value As String)
            _bankPenerima = Value
        End Set
    End Property
    Public Property NoRekening() As String
        Get
            Return (CType(_noRekening, String))
        End Get
        Set(ByVal Value As String)
            _noRekening = Value
        End Set
    End Property
    Public Property Seq() As Int32
        Get
            Return (CType(_seq, Int32))
        End Get
        Set(ByVal Value As Int32)
            _seq = Value
        End Set
    End Property
    Public Property Transaksi() As String
        Get
            Return (CType(_transaksi, String))
        End Get
        Set(ByVal Value As String)
            _transaksi = Value
        End Set
    End Property
    Public Property Nilai() As Decimal
        Get
            Return (CType(_nilai, Decimal))
        End Get
        Set(ByVal Value As Decimal)
            _nilai = Value
        End Set
    End Property

    Public Property RequestTo() As String
        Get
            Return (CType(_requestTo, String))
        End Get
        Set(ByVal Value As String)
            _requestTo = Value
        End Set
    End Property
    Public Property NilaiApproval() As Decimal
        Get
            Return (CType(_nilaiApproval, Decimal))
        End Get
        Set(ByVal Value As Decimal)
            _nilaiApproval = Value
        End Set
    End Property
    Public Property ListAPReport() As DataSet
        Get
            Return _ListAPReport
        End Get
        Set(ByVal Value As DataSet)
            _ListAPReport = Value
        End Set
    End Property
    Public Property ListAPAppr() As DataTable
        Get
            Return (CType(_listapappr, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listapappr = Value
        End Set
    End Property
    Public Property BulanFrom() As String
        Get
            Return (CType(_bulanfrom, String))
        End Get
        Set(ByVal Value As String)
            _bulanfrom = Value
        End Set
    End Property
    Public Property BulanTo() As String
        Get
            Return (CType(_bulanto, String))
        End Get
        Set(ByVal Value As String)
            _bulanto = Value
        End Set
    End Property
    Public Property TahunFrom() As String
        Get
            Return (CType(_tahunfrom, String))
        End Get
        Set(ByVal Value As String)
            _tahunfrom = Value
        End Set
    End Property
    Public Property TahunTo() As String
        Get
            Return (CType(_tahunto, String))
        End Get
        Set(ByVal Value As String)
            _tahunto = Value
        End Set
    End Property
    Public Property PaymentVoucherNo() As String
        Get
            Return (CType(_paymenyvoucherno, String))
        End Get
        Set(ByVal Value As String)
            _paymenyvoucherno = Value
        End Set
    End Property
    Public Property PVStatus() As String
        Get
            Return (CType(_pvstatus, String))
        End Get
        Set(ByVal Value As String)
            _pvstatus = Value
        End Set
    End Property
    Public Property PVDate() As Date
        Get
            Return (CType(_pvdate, Date))
        End Get
        Set(ByVal Value As Date)
            _pvdate = Value
        End Set
    End Property
    Public Property WOP() As String
        Get
            Return (CType(_wop, String))
        End Get
        Set(ByVal Value As String)
            _wop = Value
        End Set
    End Property
    Public Property APType() As String
        Get
            Return (CType(_aptype, String))
        End Get
        Set(ByVal Value As String)
            _aptype = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return (CType(_applicationid, String))
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property
    Public Property PVAmount() As String
        Get
            Return (CType(_pvamount, String))
        End Get
        Set(ByVal Value As String)
            _pvamount = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return (CType(_notes, String))
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property
    Public Property UpdateMode() As String
        Get
            Return (CType(_updatemode, String))
        End Get
        Set(ByVal Value As String)
            _updatemode = Value
        End Set
    End Property
    Public Property Flags() As String
        Get
            Return (CType(_flags, String))
        End Get
        Set(ByVal Value As String)
            _flags = Value
        End Set
    End Property
    Public Property Flag() As String
        Get
            Return (CType(_flag, String))
        End Get
        Set(ByVal Value As String)
            _flag = Value
        End Set
    End Property
    Public Property ReferenceNo() As String
        Get
            Return (CType(_referenceno, String))
        End Get
        Set(ByVal Value As String)
            _referenceno = Value
        End Set
    End Property

    Public Property RecipientName() As String
        Get
            Return (CType(_recipientname, String))
        End Get
        Set(ByVal Value As String)
            _recipientname = Value
        End Set
    End Property
    Public Property BGNo() As String
        Get
            Return (CType(_bgno, String))
        End Get
        Set(ByVal Value As String)
            _bgno = Value
        End Set
    End Property
    Public Property BGDueDate() As Date
        Get
            Return (CType(_bgduedate, Date))
        End Get
        Set(ByVal Value As Date)
            _bgduedate = Value
        End Set
    End Property
    Public Property BankAccountID() As String
        Get
            Return (CType(_bankaccountid, String))
        End Get
        Set(ByVal Value As String)
            _bankaccountid = Value
        End Set
    End Property

    Public Property paymentvoucherno_DT() As DataTable
        Get
            Return (CType(_paymentvoucherno_DT, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _paymentvoucherno_DT = Value
        End Set
    End Property
    Public Property PaymentNote() As String
        Get
            Return (CType(_paymentNote, String))
        End Get
        Set(ByVal Value As String)
            _paymentNote = Value
        End Set
    End Property
    Public Property approvalBy() As String
        Get
            Return (CType(_approvalBy, String))
        End Get
        Set(ByVal Value As String)
            _approvalBy = Value
        End Set
    End Property
    Public Property approvalDate() As String
        Get
            Return (CType(_approvalDate, String))
        End Get
        Set(ByVal Value As String)
            _approvalDate = Value
        End Set
    End Property
    Public Property TglBayar() As Date
        Get
            Return (CType(_TglBayar, Date))
        End Get
        Set(ByVal Value As Date)
            _TglBayar = Value
        End Set
    End Property
    Public Property ApprovalSchemaId As String

    Public Property OtorPvNoList As IList(Of String)
    Public Property WhereCond1 As String
    Public Property WhereCond2 As String
    Public Property WhereCond3 As String
    Public Property WhereCond4 As String
    Public Property WhereCond5 As String
    Public Property SortBy1 As String
    Public Property SortBy2 As String
    Public Property SortBy3 As String
    Public Property SortBy4 As String
    Public Property SortBy5 As String
    Public Property APDetail As DataTable
    Public Property isVerifikasiHO As Boolean
    Public Property PayID As String
    Public Property declineType As String
    Public Property declineReason As String
    Public Property referenceno2 As String
    Public Property ListData As DataSet
    Public Property listDataReport As DataTable
    Public Property DocumentID As String
    Public Property EmployeePosition As String
End Class

