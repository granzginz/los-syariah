
<Serializable()> _
Public Class EBankReject : Inherits Common
    Dim _TransactionRefNo As String
    Dim _JenisPembayaran As String
    Dim _Beneficiary As String
    Dim _Jumlah As Decimal
    Dim _Bank As String
    Dim _Cabang As String
    Dim _NoRekening As String
    Dim _NamaRekening As String
    Dim _NoKontrak As String
    Dim _NamaCustomer As String
    Dim _AlasanPenolakan As String

    Public Property TransactionRefNo As String
        Set(ByVal value As String)
            _TransactionRefNo = value
        End Set
        Get
            Return _TransactionRefNo
        End Get
    End Property
    Public Property JenisPembayaran As String
        Set(ByVal value As String)
            _JenisPembayaran = value
        End Set
        Get
            Return _JenisPembayaran
        End Get
    End Property
    Public Property Beneficiary As String
        Set(ByVal value As String)
            _Beneficiary = value
        End Set
        Get
            Return _Beneficiary
        End Get
    End Property
    Public Property Jumlah As Decimal
        Set(ByVal value As Decimal)
            _Jumlah = value
        End Set
        Get
            Return _Jumlah
        End Get
    End Property
    Public Property Bank As String
        Set(ByVal value As String)
            _Bank = value
        End Set
        Get
            Return _Bank
        End Get
    End Property
    Public Property Cabang As String
        Set(ByVal value As String)
            _Cabang = value
        End Set
        Get
            Return _Cabang
        End Get
    End Property
    Public Property NoRekening As String
        Set(ByVal value As String)
            _NoRekening = value
        End Set
        Get
            Return _NoRekening
        End Get
    End Property
    Public Property NamaRekening As String
        Set(ByVal value As String)
            _NamaRekening = value
        End Set
        Get
            Return _NamaRekening
        End Get
    End Property
    Public Property NoKontrak As String
        Set(ByVal value As String)
            _NoKontrak = value
        End Set
        Get
            Return _NoKontrak
        End Get
    End Property
    Public Property NamaCustomer As String
        Set(ByVal value As String)
            _NamaCustomer = value
        End Set
        Get
            Return _NamaCustomer
        End Get
    End Property
    Public Property AlasanPenolakan As String
        Set(ByVal value As String)
            _AlasanPenolakan = value
        End Set
        Get
            Return _AlasanPenolakan
        End Get
    End Property

End Class
