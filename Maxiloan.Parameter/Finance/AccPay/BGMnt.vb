
<Serializable()> _
Public Class BGMnt : Inherits Common
    Private _bankaccountid As String
    Private _bankaccountname As String
    Private _voucherno As String
    Private _status As String
    Private _statusdate As Date
    Private _bankaccounttype As String
    Private _bgno As String
    Private _bgnodummy As String
    Private _numofbg As Integer
    Private _flagstatus As Boolean
    Private _bgPrefix As String
    Private _bgnumber As String
    Private _listreport As DataSet
    Private _indexdelete As Integer
    Private _isvalidate As Boolean
    Private _txtNoBG2 As String

    Private _isGeneratedManually As Boolean
    Private _Mode As String

    Private _isgenerate As Boolean
    Private _listbgmnt As DataTable
    Private _listapsele As DataTable
    Private _bgmntcmbbankaccount As DataTable
    Public delxml As Integer

#Region "BGMaintenance"
    Public Property ListReport() As DataSet
        Get
            Return (CType(_listreport, DataSet))
        End Get
        Set(ByVal Value As DataSet)
            _listreport = Value
        End Set
    End Property

    Public Property BGPrefix() As String
        Get
            Return (CType(_bgPrefix, String))
        End Get
        Set(ByVal Value As String)
            _bgPrefix = Value
        End Set
    End Property

    Public Property BGNumber() As String
        Get
            Return (CType(_bgnumber, String))
        End Get
        Set(ByVal Value As String)
            _bgnumber = Value
        End Set
    End Property

    Public Property ListBGMnt() As DataTable
        Get
            Return (CType(_listbgmnt, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listbgmnt = Value
        End Set
    End Property

    Public Property BGMntCmbBankAccount() As DataTable
        Get
            Return (CType(_bgmntcmbbankaccount, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _bgmntcmbbankaccount = Value
        End Set
    End Property

    Public Property BGNo() As String
        Get
            Return CType(_bgno, String)
        End Get
        Set(ByVal Value As String)
            _bgno = Value
        End Set
    End Property

    Public Property BGNoDummy() As String
        Get
            Return CType(_bgnodummy, String)
        End Get
        Set(ByVal Value As String)
            _bgnodummy = Value
        End Set
    End Property

    Public Property BankAccountID() As String
        Get
            Return CType(_bankaccountid, String)
        End Get
        Set(ByVal Value As String)
            _bankaccountid = Value
        End Set
    End Property

    Public Property BankAccountName() As String
        Get
            Return CType(_bankaccountname, String)
        End Get
        Set(ByVal Value As String)
            _bankaccountname = Value
        End Set
    End Property

    Public Property VoucherNo() As String
        Get
            Return CType(_voucherno, String)
        End Get
        Set(ByVal Value As String)
            _voucherno = Value
        End Set
    End Property

    Public Property Status() As String
        Get
            Return CType(_status, String)
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property

    Public Property StatusDate() As Date
        Get
            Return CType(_statusdate, Date)
        End Get
        Set(ByVal Value As Date)
            _statusdate = Value
        End Set
    End Property

    Public Property BankAccountType() As String
        Get
            Return CType(_bankaccounttype, String)
        End Get
        Set(ByVal Value As String)
            _bankaccounttype = Value
        End Set
    End Property

    Public Property IsGenerate() As Boolean
        Get
            Return CType(_isgenerate, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _isgenerate = Value
        End Set
    End Property

    Public Property NumOfBG() As Integer
        Get
            Return CType(_numofbg, Integer)
        End Get
        Set(ByVal Value As Integer)
            _numofbg = Value
        End Set
    End Property

    Public Property IsValidate() As Boolean
        Get
            Return CType(_isvalidate, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _isvalidate = Value
        End Set
    End Property

    Public Property FlagStatus() As Boolean
        Get
            Return CType(_flagstatus, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _flagstatus = Value
        End Set
    End Property

    Public Property IndexDelete() As Integer
        Get
            Return CType(_indexdelete, Integer)
        End Get
        Set(ByVal Value As Integer)
            _indexdelete = Value
        End Set
    End Property
#End Region

#Region "AP Selection"
    Public Property ListAPSele() As DataTable
        Get
            Return (CType(_listapsele, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listapsele = Value
        End Set
    End Property
#End Region
    Public Property isGeneratedManually() As Boolean
        Get
            Return (CType(_isGeneratedManually, Boolean))
        End Get
        Set(ByVal Value As Boolean)
            _isGeneratedManually = Value
        End Set
    End Property

    Public Property Mode() As String
        Get
            Return (CType(_Mode, String))
        End Get
        Set(ByVal Value As String)
            _Mode = Value
        End Set
    End Property

    Public Property txtNoBG2() As String
        Get
            Return (CType(_txtNoBG2, String))
        End Get
        Set(ByVal Value As String)
            _txtNoBG2 = Value
        End Set
    End Property

    Public Property listBGno() As ArrayList
       



End Class
