<Serializable()> Public Class AccPayParameter : Inherits Common
    Private _dataset As DataSet
    Private _selectedApplicationIDs As String
    Private _errorMessage As String
    Private _referenceno As String
    Private _bankaccountid As String
    Private _wop As String
    Private _notes As String
    Private _amount As Double
    Private _valuedate As Date
    Private _departementid As String
    Private _agreementNo As String
    Private _recepientName As String
    Private _StartDate As Date
    Private _EndDate As Date
    Public Property StartDate() As Date
        Get
            Return _StartDate
        End Get
        Set(ByVal Value As Date)
            _StartDate = Value
        End Set
    End Property
    Public Property EndDate() As Date
        Get
            Return _EndDate
        End Get
        Set(ByVal Value As Date)
            _EndDate = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return _agreementNo
        End Get
        Set(ByVal Value As String)
            _agreementNo = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return (CType(_notes, String))
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property
    Public Property Dataset() As Dataset
        Get
            Return _dataset
        End Get
        Set(ByVal Value As Dataset)
            _dataset = Value
        End Set
    End Property
    Public Property SelectedApplicationIDs() As String
        Get
            Return _selectedApplicationIDs
        End Get
        Set(ByVal Value As String)
            _selectedApplicationIDs = Value
        End Set
    End Property
    Public Property ErrorMessage() As String
        Get
            Return _errorMessage
        End Get
        Set(ByVal Value As String)
            _errorMessage = Value
        End Set
    End Property
    Public Property ReferenceNo() As String
        Get
            Return (CType(_referenceno, String))
        End Get
        Set(ByVal Value As String)
            _referenceno = Value
        End Set
    End Property
    Public Property BankAccountID() As String
        Get
            Return (CType(_bankaccountid, String))
        End Get
        Set(ByVal Value As String)
            _bankaccountid = Value
        End Set
    End Property
    Public Property WOP() As String
        Get
            Return (CType(_wop, String))
        End Get
        Set(ByVal Value As String)
            _wop = Value
        End Set
    End Property
    Public Property Amount() As Double
        Get
            Return (CType(_amount, Double))
        End Get
        Set(ByVal Value As Double)
            _amount = Value
        End Set
    End Property
    Public Property ValueDate() As Date
        Get
            Return (CType(_valuedate, Date))
        End Get
        Set(ByVal Value As Date)
            _valuedate = Value
        End Set
    End Property
    Public Property DepartementID() As String
        Get
            Return (CType(_departementid, String))
        End Get
        Set(ByVal Value As String)
            _departementid = Value
        End Set
    End Property
    Public Property RecepientName() As String
        Get
            Return _recepientName
        End Get
        Set(ByVal Value As String)
            _recepientName = Value
        End Set
    End Property
End Class
