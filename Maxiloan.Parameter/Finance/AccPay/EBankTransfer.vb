
<Serializable()> _
Public Class EBankTransfer : Inherits Common
    Dim _TransactionRefNo As String
    Dim _ValueDate As String
    Dim _Currency As String
    Dim _Amount As Decimal
    Dim _OrderingParty As String
    Dim _OrderingPartyAddress As String
    Dim _BankAccountId As String
    Dim _BankRoutingMethod As String
    Dim _BankCode As String
    Dim _BeneficiaryBankName As String
    Dim _BeneficiaryAccNo As String
    Dim _BeneficiaryName As String
    Dim _BeneficiaryAddress1 As String
    Dim _BeneficiaryAddress2 As String
    Dim _BeneficiaryAddress3 As String
    Dim _InterBankRoutingMethod As String
    Dim _InterBankSwiftCode As String
    Dim _IntermediaryBankName As String
    Dim _PaymentDetail1 As String
    Dim _PaymentDetail2 As String
    Dim _PaymentDetail3 As String
    Dim _PaymentDetail4 As String
    Dim _BtoBInfoCitibank As String
    Dim _BtoBInfoBeneBank As String
    Dim _BtoBInfoInterBank As String
    Dim _Charges As String
    Dim _EndFile As String
    Dim _DtmUpdate As DateTime
    Dim _UserUpdate As String
    Dim _StatusFlag As String
    Dim _GenerateBy As String
    Dim _GenerateDate As DateTime
    Dim _PaidBy As String
    Dim _PaidDate As DateTime
    Dim _RejectBy As String
    Dim _RejectDate As DateTime
    Dim _DeleteBy As String
    Dim _DeleteDate As DateTime
    Dim _EditedBy As String
    Dim _EditedDate As DateTime

    Private _PVnoDT As DataTable
    Private _listapappr As DataTable

    'additional
    Dim _PaymentVoucherNo As String
    Dim _JenisTransfer As String


    Public Property ListAPAppr() As DataTable
        Get
            Return (CType(_listapappr, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listapappr = Value
        End Set
    End Property

    Public Property TransactionRefNo As String
        Set(ByVal value As String)
            _TransactionRefNo = value
        End Set
        Get
            Return _TransactionRefNo
        End Get
    End Property
    Public Property ValueDate As String
        Set(ByVal value As String)
            _ValueDate = value
        End Set
        Get
            Return _ValueDate
        End Get
    End Property
    Public Property Currency As String
        Set(ByVal value As String)
            _Currency = value
        End Set
        Get
            Return _Currency
        End Get
    End Property
    Public Property Amount As Decimal
        Set(ByVal value As Decimal)
            _Amount = value
        End Set
        Get
            Return _Amount
        End Get
    End Property
    Public Property OrderingParty As String
        Set(ByVal value As String)
            _OrderingParty = value
        End Set
        Get
            Return _OrderingParty
        End Get
    End Property
    Public Property OrderingPartyAddress As String
        Set(ByVal value As String)
            _OrderingPartyAddress = value
        End Set
        Get
            Return _OrderingPartyAddress
        End Get
    End Property
    Public Property BankAccountId As String
        Set(ByVal value As String)
            _BankAccountId = value
        End Set
        Get
            Return _BankAccountId
        End Get
    End Property
    Public Property BankRoutingMethod As String
        Set(ByVal value As String)
            _BankRoutingMethod = value
        End Set
        Get
            Return _BankRoutingMethod
        End Get
    End Property
    Public Property BankCode As String
        Set(ByVal value As String)
            _BankCode = value
        End Set
        Get
            Return _BankCode
        End Get
    End Property
    Public Property BeneficiaryBankName As String
        Set(ByVal value As String)
            _BeneficiaryBankName = value
        End Set
        Get
            Return _BeneficiaryBankName
        End Get
    End Property
    Public Property BeneficiaryAccNo As String
        Set(ByVal value As String)
            _BeneficiaryAccNo = value
        End Set
        Get
            Return _BeneficiaryAccNo
        End Get
    End Property
    Public Property BeneficiaryName As String
        Set(ByVal value As String)
            _BeneficiaryName = value
        End Set
        Get
            Return _BeneficiaryName
        End Get
    End Property
    Public Property BeneficiaryAddress1 As String
        Set(ByVal value As String)
            _BeneficiaryAddress1 = value
        End Set
        Get
            Return _BeneficiaryAddress1
        End Get
    End Property
    Public Property BeneficiaryAddress2 As String
        Set(ByVal value As String)
            _BeneficiaryAddress2 = value
        End Set
        Get
            Return _BeneficiaryAddress2
        End Get
    End Property
    Public Property BeneficiaryAddress3 As String
        Set(ByVal value As String)
            _BeneficiaryAddress3 = value
        End Set
        Get
            Return _BeneficiaryAddress3
        End Get
    End Property
    Public Property InterBankRoutingMethod As String
        Set(ByVal value As String)
            _InterBankRoutingMethod = value
        End Set
        Get
            Return _InterBankRoutingMethod
        End Get
    End Property
    Public Property InterBankSwiftCode As String
        Set(ByVal value As String)
            _InterBankSwiftCode = value
        End Set
        Get
            Return _InterBankSwiftCode
        End Get
    End Property
    Public Property IntermediaryBankName As String
        Set(ByVal value As String)
            _IntermediaryBankName = value
        End Set
        Get
            Return _IntermediaryBankName
        End Get
    End Property
    Public Property PaymentDetail1 As String
        Set(ByVal value As String)
            _PaymentDetail1 = value
        End Set
        Get
            Return _PaymentDetail1
        End Get
    End Property
    Public Property PaymentDetail2 As String
        Set(ByVal value As String)
            _PaymentDetail2 = value
        End Set
        Get
            Return _PaymentDetail2
        End Get
    End Property
    Public Property PaymentDetail3 As String
        Set(ByVal value As String)
            _PaymentDetail3 = value
        End Set
        Get
            Return _PaymentDetail3
        End Get
    End Property
    Public Property PaymentDetail4 As String
        Set(ByVal value As String)
            _PaymentDetail4 = value
        End Set
        Get
            Return _PaymentDetail4
        End Get
    End Property
    Public Property BtoBInfoCitibank As String
        Set(ByVal value As String)
            _BtoBInfoCitibank = value
        End Set
        Get
            Return _BtoBInfoCitibank
        End Get
    End Property
    Public Property BtoBInfoBeneBank As String
        Set(ByVal value As String)
            _BtoBInfoBeneBank = value
        End Set
        Get
            Return _BtoBInfoBeneBank
        End Get
    End Property
    Public Property BtoBInfoInterBank As String
        Set(ByVal value As String)
            _BtoBInfoInterBank = value
        End Set
        Get
            Return _BtoBInfoInterBank
        End Get
    End Property
    Public Property Charges As String
        Set(ByVal value As String)
            _Charges = value
        End Set
        Get
            Return _Charges
        End Get
    End Property
    Public Property EndFile As String
        Set(ByVal value As String)
            _EndFile = value
        End Set
        Get
            Return _EndFile
        End Get
    End Property
    Public Property DtmUpdate As DateTime
        Set(ByVal value As DateTime)
            _DtmUpdate = value
        End Set
        Get
            Return _DtmUpdate
        End Get
    End Property
    Public Property UserUpdate As String
        Set(ByVal value As String)
            _UserUpdate = value
        End Set
        Get
            Return _UserUpdate
        End Get
    End Property
    Public Property StatusFlag As String
        Set(ByVal value As String)
            _StatusFlag = value
        End Set
        Get
            Return _StatusFlag
        End Get
    End Property
    Public Property GenerateBy As String
        Set(ByVal value As String)
            _GenerateBy = value
        End Set
        Get
            Return _GenerateBy
        End Get
    End Property
    Public Property GenerateDate As DateTime
        Set(ByVal value As DateTime)
            _GenerateDate = value
        End Set
        Get
            Return _GenerateDate
        End Get
    End Property
    Public Property PaidBy As String
        Set(ByVal value As String)
            _PaidBy = value
        End Set
        Get
            Return _PaidBy
        End Get
    End Property
    Public Property PaidDate As DateTime
        Set(ByVal value As DateTime)
            _PaidDate = value
        End Set
        Get
            Return _PaidDate
        End Get
    End Property
    Public Property RejectBy As String
        Set(ByVal value As String)
            _RejectBy = value
        End Set
        Get
            Return _RejectBy
        End Get
    End Property
    Public Property RejectDate As DateTime
        Set(ByVal value As DateTime)
            _RejectDate = value
        End Set
        Get
            Return _RejectDate
        End Get
    End Property
    Public Property DeleteBy As String
        Set(ByVal value As String)
            _DeleteBy = value
        End Set
        Get
            Return _DeleteBy
        End Get
    End Property
    Public Property DeleteDate As DateTime
        Set(ByVal value As DateTime)
            _DeleteDate = value
        End Set
        Get
            Return _DeleteDate
        End Get
    End Property
    Public Property EditedBy As String
        Set(ByVal value As String)
            _EditedBy = value
        End Set
        Get
            Return _EditedBy
        End Get
    End Property
    Public Property EditedDate As DateTime
        Set(ByVal value As DateTime)
            _EditedDate = value
        End Set
        Get
            Return _EditedDate
        End Get
    End Property
    Public Property PVnoDT() As DataTable
        Get
            Return (CType(_PVnoDT, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _PVnoDT = Value
        End Set
    End Property
    Public Property PaymentVoucherNo As String
        Set(ByVal value As String)
            _PaymentVoucherNo = value
        End Set
        Get
            Return _PaymentVoucherNo
        End Get
    End Property
    Public Property JenisTransfer As String
        Set(ByVal value As String)
            _JenisTransfer = value
        End Set
        Get
            Return _JenisTransfer
        End Get
    End Property
    Public Property WhereCond1 As String
    Public Property WhereCond2 As String
    Public Property WhereCond3 As String
    Public Property WhereCond4 As String
    Public Property BeneficiaryBankAddress As String

    Public Property AccountPayAbleno As String
    Public Property AccountPayAbleBranchID As String
    Public Property AccountNameTo As String
    Public Property AccountNoTo As String
    Public Property BankNameTo As String
    Public Property BankBranchTo As String
    Public Property BankIdTo As String
    Public Property BankBranchID As Integer
    Public Property APType As String
End Class
