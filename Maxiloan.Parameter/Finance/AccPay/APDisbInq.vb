

<Serializable()> _
Public Class APDisbInq : Inherits Common
    Private _listapinq As DataTable
    Private _listpvinquiry As DataTable
    Private _reportapinq As DataSet
    Private _reportpvinquiry As DataSet
    Private _selectedAPNo As String

    Public Property SelectedAPNo() As String
        Get
            Return _selectedAPNo
        End Get
        Set(ByVal Value As String)
            _selectedAPNo = Value
        End Set
    End Property

    Public Property ListAPInq() As DataTable
        Get
            Return (CType(_listapinq, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listapinq = Value
        End Set
    End Property
    Public Property ReportAPInq() As DataSet
        Get
            Return (CType(_reportapinq, DataSet))
        End Get
        Set(ByVal Value As DataSet)
            _reportapinq = Value
        End Set
    End Property
    Public Property ListPVInquiry() As DataTable
        Get
            Return (CType(_listpvinquiry, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listpvinquiry = Value
        End Set
    End Property
    Public Property ReportPVInquiry() As DataSet
        Get
            Return (CType(_reportpvinquiry, DataSet))
        End Get
        Set(ByVal Value As DataSet)
            _reportpvinquiry = Value
        End Set
    End Property
End Class
