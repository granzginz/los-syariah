﻿<Serializable()> _
Public Class PEKonfirmasiEbankExec
    Inherits Maxiloan.Parameter.Common

    Public Property UsrUpd As String
    Public Property DtmUpd As Date
    Public Property PaymentVoucherNo As String

    Public Property ListData As DataTable
    Public Property RecordCount As Integer
End Class
