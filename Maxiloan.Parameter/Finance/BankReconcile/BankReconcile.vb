

<Serializable()> _
Public Class BankReconcile
    Inherits Maxiloan.Parameter.FinanceBase

#Region "Constanta"
    Private _listBankRecon As DataTable
    Private _listBankReconDet As DataTable
    Private _ReconDate As Date
    Private _GroupVoucherno As String
    Private _GroupJournalno As String
    Private _bankaccountid As String
#End Region

    Public Property listBankRecon() As DataTable
        Get
            Return (CType(_listBankRecon, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listBankRecon = Value
        End Set
    End Property

    Public Property listBankReconDet() As DataTable
        Get
            Return (CType(_listBankReconDet, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listBankReconDet = Value
        End Set
    End Property



    Public Property ReconDate() As Date
        Get
            Return (CType(_ReconDate, Date))
        End Get
        Set(ByVal Value As Date)
            _ReconDate = Value
        End Set
    End Property

    Public Property GroupVoucherno() As String
        Get
            Return (CType(_GroupVoucherno, String))
        End Get
        Set(ByVal Value As String)
            _GroupVoucherno = Value
        End Set
    End Property

    Public Property GroupJournalno() As String
        Get
            Return (CType(_GroupJournalno, String))
        End Get
        Set(ByVal Value As String)
            _GroupJournalno = Value
        End Set
    End Property

    Public Property BankAccountID() As String
        Get
            Return _bankaccountid
        End Get
        Set(ByVal Value As String)
            _bankaccountid = Value
        End Set
    End Property

End Class
