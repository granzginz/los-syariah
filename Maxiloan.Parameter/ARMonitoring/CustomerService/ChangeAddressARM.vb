
<Serializable()> _
Public Class ChangeAddressARM : Inherits Maxiloan.Parameter.Common
    Private _listAddress As DataTable
    Private _CGID As String
    Private _BranchID As String
    Private _collectortype As String
    Private _strkey As String
    Private _agreementno As String
    Private _applicationid As String
    Private _mailingaddress As String
    Private _mailingkelurahan As String
    Private _mailingkecamatan As String
    Private _mailingcity As String
    Private _mailingzipcode As String
    Private _mailingareaphone1 As String
    Private _mailingphone1 As String
    Private _mailingareaphone2 As String
    Private _mailingphone2 As String
    Private _mailingareafax As String
    Private _mailingfax As String
    Private _mailingrt As String
    Private _mailingrw As String
    Private _hasil As Integer
    Private _customerId As String
    Private _notes As String
#Region "PrivateDeclarations"
    Private _spName As String
    Private _select1 As String
    Private _select2 As String
    Private _select3 As String
    Private _ListDataReport As DataSet
#End Region

    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return _customerId
        End Get
        Set(ByVal Value As String)
            _customerId = Value
        End Set
    End Property
    Public Property hasil() As Integer
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Integer)
            _hasil = Value
        End Set
    End Property

    Public Property MailingRW() As String
        Get
            Return _mailingrw
        End Get
        Set(ByVal Value As String)
            _mailingrw = Value
        End Set
    End Property

    Public Property MailingRT() As String
        Get
            Return _mailingrt
        End Get
        Set(ByVal Value As String)
            _mailingrt = Value
        End Set
    End Property

    Public Property MailingPhone2() As String
        Get
            Return _mailingphone2
        End Get
        Set(ByVal Value As String)
            _mailingphone2 = Value
        End Set
    End Property

    Public Property MailingPhone1() As String
        Get
            Return _mailingphone1
        End Get
        Set(ByVal Value As String)
            _mailingphone1 = Value
        End Set
    End Property

    Public Property MailingAreaFax() As String
        Get
            Return _mailingareafax
        End Get
        Set(ByVal Value As String)
            _mailingareafax = Value
        End Set
    End Property

    Public Property MailingFax() As String
        Get
            Return _mailingfax
        End Get
        Set(ByVal Value As String)
            _mailingfax = Value
        End Set
    End Property


    Public Property MailingAreaPhone2() As String
        Get
            Return _mailingareaphone2
        End Get
        Set(ByVal Value As String)
            _mailingareaphone2 = Value
        End Set
    End Property

    Public Property MailingAreaPhone1() As String
        Get
            Return _mailingareaphone1
        End Get
        Set(ByVal Value As String)
            _mailingareaphone1 = Value
        End Set
    End Property

    Public Property MailingZipCode() As String
        Get
            Return _mailingzipcode
        End Get
        Set(ByVal Value As String)
            _mailingzipcode = Value
        End Set
    End Property

    Public Property MailingCity() As String
        Get
            Return _mailingcity
        End Get
        Set(ByVal Value As String)
            _mailingcity = Value
        End Set
    End Property

    Public Property MailingKecamatan() As String
        Get
            Return _mailingkecamatan
        End Get
        Set(ByVal Value As String)
            _mailingkecamatan = Value
        End Set
    End Property
    Public Property MailingKelurahan() As String
        Get
            Return _mailingkelurahan
        End Get
        Set(ByVal Value As String)
            _mailingkelurahan = Value
        End Set
    End Property

    Public Property MailingAddress() As String
        Get
            Return _mailingaddress
        End Get
        Set(ByVal Value As String)
            _mailingaddress = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return _agreementno
        End Get
        Set(ByVal Value As String)
            _agreementno = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return _applicationid
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property

    Public Property CGID() As String
        Get
            Return _CGID
        End Get
        Set(ByVal Value As String)
            _CGID = Value
        End Set
    End Property


    Public Property CollectorType() As String
        Get
            Return _collectortype
        End Get
        Set(ByVal Value As String)
            _collectortype = Value
        End Set
    End Property

    Public Property strKey() As String
        Get
            Return _strkey
        End Get
        Set(ByVal Value As String)
            _strkey = Value
        End Set
    End Property

    Public Property ListAddress() As DataTable
        Get
            Return _listAddress
        End Get
        Set(ByVal Value As DataTable)
            _listAddress = Value
        End Set
    End Property
#Region "ReportAR"
    Public Property spName() As String
        Get
            Return _spName
        End Get
        Set(ByVal Value As String)
            _spName = Value
        End Set
    End Property
    Public Property select1() As String
        Get
            Return _select1
        End Get
        Set(ByVal Value As String)
            _select1 = Value
        End Set
    End Property
    Public Property select2() As String
        Get
            Return _select2
        End Get
        Set(ByVal Value As String)
            _select2 = Value
        End Set
    End Property
    Public Property select3() As String
        Get
            Return _select3
        End Get
        Set(ByVal Value As String)
            _select3 = Value
        End Set
    End Property
    Public Property ListDataReport() As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(ByVal Value As DataSet)
            _ListDataReport = Value
        End Set
    End Property
#End Region
End Class
