
<Serializable()> _
Public Class ChangeAddressInq : Inherits Maxiloan.Parameter.Common
    Private _listaddress As DataTable
    Private _ApplicationID As String
    Private _seqno As String
    Private _strkey As String

    Public Property strkey() As String
        Get
            Return _strkey
        End Get
        Set(ByVal Value As String)
            _strkey = Value
        End Set
    End Property

    Public Property SeqNo() As String
        Get
            Return _seqno
        End Get
        Set(ByVal Value As String)
            _seqno = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property

    Public Property ListAddress() As DataTable
        Get
            Return _listaddress
        End Get
        Set(ByVal Value As DataTable)
            _listaddress = Value
        End Set
    End Property
End Class
