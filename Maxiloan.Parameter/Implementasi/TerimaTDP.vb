Imports Maxiloan.Parameter

<Serializable()>
Public Class TerimaTDP : Inherits Common
#Region "Constanta"
    Private _listTDP As DataTable
    Private _description As String
    Private _AmountRec As Double
    Private _TDPReceiveNo As String
    Private _bankaccount As String
    Private _postingdate As Date
    Private _bankAccountName As String
    Private _VoucherNo As String
    Private _TDPStatus As String
    Private _TDPStatusdesc As String
    Private _StatusDate As Date
    Private _strError As String

    Public ValueDate As Date
    Public ReferenceNo As String
    Public BankAccountID As String
    Public ListData As DataTable
    Public CustomerName As String
    Public Agreementno As String
    Public BankAccountName As String

#End Region


    Public Property listTDP() As DataTable
        Get
            Return (CType(_listTDP, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listTDP = Value
        End Set
    End Property

    Public Property description() As String
        Get
            Return (CType(_description, String))
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property

    Public Property bankaccount() As String
        Get
            Return (CType(_bankaccount, String))
        End Get
        Set(ByVal Value As String)
            _bankaccount = Value
        End Set
    End Property

    Public Property AmountRec() As Double
        Get
            Return (CType(_AmountRec, Double))
        End Get
        Set(ByVal Value As Double)
            _AmountRec = Value
        End Set
    End Property


    Public Property postingdate() As Date
        Get
            Return (CType(_postingdate, Date))
        End Get
        Set(ByVal Value As Date)
            _postingdate = Value
        End Set
    End Property

    Public Property VoucherNo() As String
        Get
            Return (CType(_VoucherNo, String))
        End Get
        Set(ByVal Value As String)
            _VoucherNo = Value
        End Set
    End Property

    Public Property TDPStatus() As String
        Get
            Return (CType(_TDPStatus, String))
        End Get
        Set(ByVal Value As String)
            _TDPStatus = Value
        End Set
    End Property

    Public Property TDPStatusdesc() As String
        Get
            Return (CType(_TDPStatusdesc, String))
        End Get
        Set(ByVal Value As String)
            _TDPStatusdesc = Value
        End Set
    End Property

    Public Property StatusDate() As Date
        Get
            Return (CType(_StatusDate, Date))
        End Get
        Set(ByVal Value As Date)
            _StatusDate = Value
        End Set
    End Property

    Public Property strError() As String
        Get
            Return (CType(_strError, String))
        End Get
        Set(ByVal Value As String)
            _strError = Value
        End Set
    End Property
    Public Property TDPReceiveNo() As String
        Get
            Return (CType(_TDPReceiveNo, String))
        End Get
        Set(ByVal Value As String)
            _TDPReceiveNo = Value
        End Set
    End Property
End Class

