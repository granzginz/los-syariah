﻿<Serializable()>
Public Class UploadAngsuran : Inherits Common

    Public UploadDate As DateTime
    Public VirtualAccountID As String
    Public SequenceNo As Integer
    Public NoKontrak As String
    Public NamaNasabah As String
    Public NoJournal As String
    Public NilaiTransaksi As Decimal
    Public TglTransaksi As Date
    Public WaktuTransaksi As String
    Public Keterangan As String
    Public BankBranchUploadAngsuran As String
    Public Post As String
    Public Pokok As Decimal
    Public Bunga As Decimal
    Public denda As Decimal
End Class
