﻿<Serializable()> _
Public Class SoftLoan : Inherits Common

    Public UploadDate As DateTime
    Public BankAccountID As String
    Public AgreementNo As String
    Public NamaPelanggan As String
    Public NilaiTransaksi As Decimal
    Public TglTransaksi As Date
    Public ApplicationID As String
End Class
