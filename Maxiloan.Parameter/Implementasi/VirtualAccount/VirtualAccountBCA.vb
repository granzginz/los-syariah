﻿
<Serializable()>
Public Class VirtualAccountBase : Inherits Common
    Public UploadDate As DateTime
    Public VirtualAccountID As String
    Public SequenceNo As Integer
    Public NoPelanggan As String
    Public NamaPelanggan As String

    Public NilaiTransaksi As Decimal
    Public denda As Decimal
    Public TglTransaksi As Date
    Public WaktuTransaksi As String

    Public Berita1 As String
    Public Berita2 As String

    Public JenisTransaksi As String
    Public Keterangan As String

    Public Journal As String
    Public BankBranchVA As String
    Public Post As String
    Public Keterangan2 As String
End Class


<Serializable()>
Public Class VirtualAccountBCA : Inherits VirtualAccountBase

    Public MataUang As String
    Public Lokasi As String
End Class



