﻿
Public Class AngsuranVirtualAccountList
    Private m_vaBankInitial As String
    Private m_list As List(Of AngsuranVirtualAccountItem)
    Private m_generatestrategy As IGenerateStrategy
    Private m_UploadDate As DateTime
    Private m_path As String
    Private m_vaBankId As String
    Private m_virtualAccountAgent As VirtualAccountAgent

    Public Sub New(_virtualAccountAgent As VirtualAccountAgent, _list As List(Of AngsuranVirtualAccountItem), _UploadDate As DateTime, path As String)

        m_virtualAccountAgent = _virtualAccountAgent
        m_list = _list
        m_UploadDate = _UploadDate
        m_path = path
        m_vaBankInitial = m_virtualAccountAgent.VaBankInitial.Trim
        m_vaBankId = m_virtualAccountAgent.VaBankID.Trim
    End Sub

    Public Sub Generate()
        Dim name As String = Me.[GetType]().[Namespace] + ".Generate" + m_vaBankInitial.ToString() + "Strategy"
        Dim factory As IGenerateStrategy = DirectCast(Activator.CreateInstance(Type.[GetType](name)), IGenerateStrategy)
        m_resultDesc = factory.Generate(m_list, m_UploadDate, m_path, m_virtualAccountAgent)
    End Sub

    Private m_resultDesc As List(Of ResultDescription)

    Public ReadOnly Property ResultDescription As List(Of ResultDescription)
        Get
            Return m_resultDesc
        End Get
    End Property

    'Public ReadOnly Property FileName
    '    Get
    '        Return String.Format("VATAGIHAN-{0}-{1:yyyyMMdd}.txt", m_vaBankInitial, m_UploadDate)
    '    End Get
    'End Property

    'Public ReadOnly Property InstallmentCount As Integer
    '    Get
    '        Return m_list.Count
    '    End Get
    'End Property
    'Public ReadOnly Property TotalInstallment As Decimal
    '    Get
    '        Return m_list.Sum(Function(e) e.installment + e.Pinalty)
    '    End Get
    'End Property

End Class
