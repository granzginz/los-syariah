﻿<Serializable()>
Public Class VirtualAccountInsAgreementLog
    Public Sub New(_id As Long, _DateOfUpload As Date, _InstallmentCount As Integer, _TotalInstallment As Decimal, _FileDirectory As String)
        Id = _id
        DateOfUpload = _DateOfUpload
        InstallmentCount = _InstallmentCount
        TotalInstallment = _TotalInstallment
        FileDirectory = _FileDirectory
    End Sub
    Public Property Id As Long
    Public Property DateOfUpload As Date
    Public Property InstallmentCount As Integer
    Public Property TotalInstallment As Decimal
    Public Property FileDirectory As String
    Public Property VaAgreementLogDetails As List(Of VirtualAccountAgreementDetailLog)
End Class


<Serializable()>
Public Class VirtualAccountAgreementDetailLog
    Public Sub New(_id As Long, _VaBankId As String, _FileName As String, _InstallmentCount As Integer, _TotalInstallment As Decimal, _vaBankName As String)
        Id = _id
        VaBankId = _VaBankId
        FileName = _FileName
        InstallmentCount = _InstallmentCount
        TotalInstallment = _TotalInstallment
        VaBankName = _vaBankName
    End Sub

    Public Property Id As Long
    Public Property ParentId As Long
    Public Property VaBankId As String
    Public Property FileName As String
    Public Property InstallmentCount As Integer
    Public Property TotalInstallment As Decimal
    Public Property VaBankName As String
End Class