﻿Imports System.Text

Public Class GenerateBRIStrategy
    Inherits GenerateStrategy

    Private Const pageCount = 9999
    Private Const header = "No|Brivano|CustCode|CustName|Type|OverwriteAddRemove|Amount|LastPeriode|Keteranga"
    Private Const detail = "{0}|{1}|{2}|{3}|K|{4}|{5}|{6:yyyyMMdd}|{7}"
    'Private Const foother = "2{0,5}{1:d8}{2:d15}"
    Protected Overrides Sub doGenerate()
        Dim eVaBinNo = m_virtualAccountAgent.VaBinNo.Trim
        Dim pgCnt = 1
        Dim listOfLists As List(Of IEnumerable(Of AngsuranVirtualAccountItem)) = New List(Of IEnumerable(Of AngsuranVirtualAccountItem))()
        For ix As Integer = 0 To m_list.Count - 1 Step pageCount
            listOfLists.Add(m_list.Skip(ix).Take(pageCount))
        Next


        For Each page In listOfLists

            builder = New StringBuilder()

            Dim resDesc = New ResultDescription(m_virtualAccountAgent.VaBankInitial.Trim, m_UploadDate, m_virtualAccountAgent.VaBankID.Trim, page.Count, page.Sum(Function(e) e.installment + e.Pinalty), pgCnt)
            m_resultDescriptions.Add(resDesc)

            Dim i = 1
            builder.AppendLine(header)
            For Each e In page
                builder.AppendLine(String.Format(detail,
                                             i,
                                             eVaBinNo.Trim,
                                             e.ContractNumber.Trim,
                                             e.NameOfCustomer.Substring(0, Math.Min(40, e.NameOfCustomer.Length)),
                                             "O",
                                             Math.Round((e.installment + e.Pinalty), 0),
                                             e.DateOverDue,
                                             String.Format("JT {0:dd/MM} Angs-{1}", e.InsDueDate, e.PeriodOfTennor)))
                i = i + 1
            Next
            m_fullPath = String.Format("{0}{1}", m_Path, resDesc.FileName)
            writeFile(builder)
            pgCnt = pgCnt + 1
        Next

    End Sub

End Class