﻿Public Class VirtualAccountAgent
    Private mVaBankID As String
    Private mVaBankName As String
    Private mVaBankInitial As String
    Private mVaBinNo As String
    Private mVaContact As String
    Private mVaJabatan As String
    Private mVaContactHP As String
    Private mVaEmail As String
    Private mVaSettleBankAccID As String
    Private mVaSettleBankAccName As String
    Private mVaTransactionFee As Decimal
    Private mVaFeeTanggungKonsumen As Decimal
    Private mVaFeeTanggungMF As Decimal
    Private mVaPPNFee As Decimal
    Private mVaPph23Fee As Decimal
    Private mCoaPpn As String
    Private mCoaPph23 As String

    Public Sub New(
                  vaBankID As String, vaBankName As String, vaBankInitial As String, vaBinNo As String, vaContact As String,
                  vaJabatan As String, vaContactHP As String, vaEmail As String, vaSettleBankAccID As String, vaSettleBankAccName As String,
                  vaTransactionFee As Decimal, vaFeeTanggungKonsumen As Decimal, vaFeeTanggungMF As Decimal, vaPPNFee As Decimal,
                  vaPph23Fee As Decimal, coaPpn As String, coaPph23 As String)
        mVaBankID = vaBankID
        mVaBankName = vaBankName
        mVaBankInitial = vaBankInitial
        mVaBinNo = vaBinNo
        mVaContact = vaContact
        mVaJabatan = vaJabatan
        mVaContactHP = vaContactHP
        mVaEmail = vaEmail
        mVaSettleBankAccID = vaSettleBankAccID
        mVaSettleBankAccName = vaSettleBankAccName
        mVaTransactionFee = vaTransactionFee
        mVaFeeTanggungKonsumen = vaFeeTanggungKonsumen
        mVaFeeTanggungMF = vaFeeTanggungMF
        mVaPPNFee = vaPPNFee
        mVaPph23Fee = vaPph23Fee
        mCoaPpn = coaPpn
        mCoaPph23 = coaPph23
    End Sub

    Public ReadOnly Property VaBankID As String
        Get
            Return mVaBankID
        End Get
    End Property

    Public ReadOnly Property VaBankName As String
        Get
            Return mVaBankName
        End Get
    End Property

    Public ReadOnly Property VaBankInitial As String
        Get
            Return mVaBankInitial
        End Get
    End Property

    Public ReadOnly Property VaBinNo As String
        Get
            Return mVaBinNo
        End Get
    End Property

    Public ReadOnly Property VaContact As String
        Get
            Return mVaContact
        End Get
    End Property

    Public ReadOnly Property VaJabatan As String
        Get
            Return mVaJabatan
        End Get
    End Property

    Public ReadOnly Property VaContactHP As String
        Get
            Return mVaContactHP
        End Get
    End Property

    Public ReadOnly Property VaEmail As String
        Get
            Return mVaEmail
        End Get
    End Property

    Public ReadOnly Property VaSettleBankAccID As String
        Get
            Return mVaSettleBankAccID
        End Get
    End Property
    Public ReadOnly Property VaSettleBankAccName As String
        Get
            Return mVaSettleBankAccName
        End Get
    End Property
    Public ReadOnly Property VaTransactionFee As Decimal
        Get
            Return mVaTransactionFee
        End Get
    End Property
    Public ReadOnly Property VaFeeTanggungKonsumen As Decimal
        Get
            Return mVaFeeTanggungKonsumen
        End Get
    End Property
    Public ReadOnly Property VaFeeTanggungMF As Decimal
        Get
            Return mVaFeeTanggungMF
        End Get
    End Property
    Public ReadOnly Property VaPPNFee As Decimal
        Get
            Return mVaPPNFee
        End Get
    End Property
    Public ReadOnly Property VaPph23Fee As Decimal
        Get
            Return mVaPph23Fee
        End Get
    End Property
    Public ReadOnly Property CoaPpn As String
        Get
            Return mCoaPpn
        End Get
    End Property
    Public ReadOnly Property CoaPph23 As String
        Get
            Return mCoaPph23
        End Get

    End Property
End Class
