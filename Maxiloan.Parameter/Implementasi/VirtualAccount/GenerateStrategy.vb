﻿
Imports System.IO
Imports System.Text

Interface IGenerateStrategy
    Function Generate(list As List(Of AngsuranVirtualAccountItem), m_UploadDate As DateTime, _fullPath As String, _virtualAccountAgent As VirtualAccountAgent) As List(Of ResultDescription)
End Interface

Public MustInherit Class GenerateStrategy
    Implements IGenerateStrategy

    Protected m_list As List(Of AngsuranVirtualAccountItem)
    Protected builder = New StringBuilder
    Property m_UploadDate As DateTime
    Protected m_fullPath As String
    Protected m_Path As String
    Protected m_virtualAccountAgent As VirtualAccountAgent
    Protected m_resultDescriptions As List(Of ResultDescription)

    Public Function Generate(list As List(Of AngsuranVirtualAccountItem), _UploadDate As DateTime, _fullPath As String, _virtualAccountAgent As VirtualAccountAgent) As List(Of ResultDescription) Implements IGenerateStrategy.Generate
        m_list = list
        m_Path = _fullPath
        m_UploadDate = _UploadDate
        m_virtualAccountAgent = _virtualAccountAgent
        m_resultDescriptions = New List(Of ResultDescription)
        doGenerate()
        Return m_resultDescriptions
    End Function

    Protected MustOverride Sub doGenerate()

    Protected Sub writeFile(builder As StringBuilder)
        If File.Exists(m_fullPath) Then
            File.Delete(m_fullPath)
        End If
        File.WriteAllText(m_fullPath, builder.ToString().TrimEnd())
    End Sub

    Protected Function shelp(str As String, len As Integer, Optional islead As Boolean = False)
        If islead Then
            str = String.Format("000000000000000{0}", str)
            Return str.Substring(str.Length - len)
        End If

        If str.Length > len Then
            Return str.Substring(0, len)
        End If

        Return str
    End Function


End Class


Public Class ResultDescription

    Private m_vaBankInitial As String
    Private m_UploadDate As DateTime
    Private m_vaBankId As String
    Private m_InstallmentCount As Integer
    Private m_TotalInstallment As Decimal
    Private m_count As String

    Public Sub New(_vaBankInitial As String, _UploadDate As DateTime, _vaBankId As String, _InstallmentCount As Integer, _TotalInstallment As Decimal, Optional _count As String = "")
        m_vaBankInitial = _vaBankInitial
        m_UploadDate = _UploadDate
        m_vaBankId = _vaBankId
        m_InstallmentCount = _InstallmentCount
        m_TotalInstallment = _TotalInstallment
        m_count = _count
    End Sub


    Public ReadOnly Property VaBankId
        Get
            Return m_vaBankId
        End Get
    End Property
    Public ReadOnly Property FileName
        Get
            Return String.Format("VATAGIHAN{0}-{1}-{2:yyyyMMdd}.txt", m_count, m_vaBankInitial, m_UploadDate)
        End Get
    End Property

    Public ReadOnly Property InstallmentCount As Integer
        Get
            Return m_InstallmentCount
        End Get
    End Property
    Public ReadOnly Property TotalInstallment As Decimal
        Get
            Return m_TotalInstallment
        End Get
    End Property
End Class
