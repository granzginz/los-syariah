﻿
Public Class AngsuranVirtualAccountItem
    Public Sub New(_ApplicationID As String, _ContractNumber As String, _NameOfCustomer As String, _DateOverDue As Date, _PeriodOfTennor As Integer, _installment As Decimal,
                   _Pinalty As Decimal, _VaBankID As String, _VaBankInitial As String, _VaBankName As String, _VaBinNo As String, _BranchId As String, _kodepersh As String, _overwriteAddRemove As String, _insDueDate As Date)
        ApplicationID = _ApplicationID
        ContractNumber = _ContractNumber
        NameOfCustomer = _NameOfCustomer
        DateOverDue = _DateOverDue
        PeriodOfTennor = _PeriodOfTennor
        installment = _installment
        Pinalty = _Pinalty
        VaBankID = _VaBankID
        VaBankInitial = _VaBankInitial
        VaBankName = _VaBankName
        VaBinNo = _VaBinNo
        BranchId = _BranchId
        kodePersh = _kodepersh
        OverwriteAddRemove = _overwriteAddRemove
        InsDueDate = _insDueDate
    End Sub
    Public Property BranchId As String
    Public Property ApplicationID As String
    Public Property ContractNumber As String
    Public Property NameOfCustomer As String
    Public Property DateOverDue As Date
    Public Property PeriodOfTennor As Integer
    Public Property installment As Decimal
    Public Property Pinalty As Decimal

    Public Property VaBankID As String
    Public Property VaBankInitial As String
    Public Property VaBankName As String
    Public Property VaBinNo As String

    Public Property kodePersh As String
    Public Property OverwriteAddRemove As String
    Public Property InsDueDate As Date
End Class


Public Class GeneratedVaToExcellLog


    'Test
    'Public Sub New(_nomor As Integer, _nama As String, _virtualaccountbank As String, _nomorvirtual As String, _nokontrak As String, _masaberlakudari As Date, _masaberlakusampai As Date)
    '    Nomor = _nomor
    '    Nama = _nama.Trim()
    '    VirtualAccountBank = _virtualaccountbank.Trim()
    '    NomorVirtual = _nomorvirtual.Trim()
    '    NoKontrak = _nokontrak.Trim()
    '    MasaBerlakuDari = _masaberlakudari
    '    MasaBerlakuSampai = _masaberlakusampai
    'End Sub

    'Public Sub New(_nomor As Integer, _nama As String, _virtualAccountBCA As String, _virtualAccountBRI As String, _nokontrak As String, _masaberlakudari As Date, _masaberlakusampai As Date)
    '    Nomor = _nomor
    '    Nama = _nama.Trim()
    '    NomorVirtualBCA = _virtualAccountBCA
    '    NomorVirtualBRI = _virtualAccountBRI
    '    NoKontrak = _nokontrak.Trim()
    '    MasaBerlakuDari = _masaberlakudari
    '    MasaBerlakuSampai = _masaberlakusampai
    'End Sub

    Public Property Nomor As Long
    Public Property Nama As String
    Public Property VirtualAccountBank As String
    Public Property NomorVirtual As String
    Public Property NoKontrak As String
    Public Property MasaBerlakuDari As Date
    Public Property MasaBerlakuSampai As Date
    Public Property NomorVirtualBCA As String
    Public Property NomorVirtualBRI As String
    Public Sub New(_NomorVA As String, _NamaVA As String, _IDVA As String, _Code1 As String, _Code2 As String, _JenisInstruksi As String)
        NomorVA = _NomorVA.Trim()
        NamaVA = _NamaVA.Trim()
        IDVA = _IDVA.Trim()
        Code1 = _Code1.Trim()
        Code2 = _Code2.Trim()
        JenisInstruksi = _JenisInstruksi.Trim()
    End Sub

    Public Property NomorVA As String
    Public Property NamaVA As String
    Public Property IDVA As String
    Public Property Code1 As String
    Public Property Code2 As String
    Public Property JenisInstruksi As String
End Class