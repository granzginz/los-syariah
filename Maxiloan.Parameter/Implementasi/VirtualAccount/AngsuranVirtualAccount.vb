﻿

<Serializable()>
Public Class AngsuranVirtualAccount
    Public Sub New(_angsuranVirtualAccountItems As IList(Of AngsuranVirtualAccountItem))
        angsuranVirtualAccountItems = _angsuranVirtualAccountItems
    End Sub

    Public Sub New(_angsuranVirtualAccountItems As IList(Of AngsuranVirtualAccountItem), _vAccountAgent As IList(Of VirtualAccountAgent))
        angsuranVirtualAccountItems = _angsuranVirtualAccountItems
        VAccountAgent = _vAccountAgent
        ''prepare()
    End Sub

    Private Property VAccountAgent As IList(Of VirtualAccountAgent)
    Public Property SettingId As String
    Public Property UploadDate As DateTime 
    Public Property TotalRecords As Long
    Public Property FileDirectory As String
    Public ReadOnly Property GenerateAngsurans As List(Of AngsuranVirtualAccountList)
        Get
            Return generateAngsuran
        End Get
    End Property
    Public ReadOnly Property InstallmentCount As Integer
        Get
            Return generateAngsuran.Sum(Function(e) e.ResultDescription.Sum(Function(x) x.InstallmentCount))
        End Get
    End Property 
    Public ReadOnly Property TotalInstallment As Decimal
        Get
            Return generateAngsuran.Sum(Function(e) e.ResultDescription.Sum(Function(x) x.TotalInstallment))
        End Get
    End Property

    Public ReadOnly Property DetailLogTable As DataTable
        Get
            Dim dt = New DataTable()
            Dim strArray As String() = New String() {"vaBankId", "fileName", "installmentcount", "totalInstallment"}
            For Each s In strArray
                dt.Columns.Add(New DataColumn(s))
            Next
            dt.Columns(2).DataType = Type.GetType("System.Int16")
            dt.Columns(3).DataType = Type.GetType("System.Decimal")
            For Each v In generateAngsuran
                For Each vnext In v.ResultDescription
                    Dim row = dt.NewRow()
                    row("vaBankId") = vnext.VaBankId
                    row("fileName") = vnext.FileName
                    row("installmentcount") = vnext.InstallmentCount
                    row("totalInstallment") = vnext.TotalInstallment
                    dt.Rows.Add(row)

                Next
            Next
            Return dt
        End Get
    End Property
     

    Private angsuranVirtualAccountItems As IList(Of AngsuranVirtualAccountItem)
    Private myDictionary As Dictionary(Of String, List(Of AngsuranVirtualAccountItem))
    Private generateAngsuran As List(Of AngsuranVirtualAccountList)

    Sub prepare()
        generateAngsuran = New List(Of AngsuranVirtualAccountList)

        For Each vaag As VirtualAccountAgent In VAccountAgent
            generateAngsuran.Add(New AngsuranVirtualAccountList(vaag, angsuranVirtualAccountItems, UploadDate, FileDirectory))
        Next

    End Sub

    Public Sub BuildUploadFiles()
        Try
            prepare()
            'generateAngsuran = New List(Of AngsuranVirtualAccountList)

            'myDictionary = angsuranVirtualAccountItems.GroupBy(Function(o) o.VaBankInitial.Trim).ToDictionary(Function(g) g.Key, Function(g) g.ToList())

            'For Each kvp As KeyValuePair(Of String, List(Of AngsuranVirtualAccountItem)) In myDictionary
            '    generateAngsuran.Add(New AngsuranVirtualAccountList(kvp.Key, kvp.Value, UploadDate, FileDirectory))
            'Next

            For Each genAng In generateAngsuran
                genAng.Generate()
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

End Class
