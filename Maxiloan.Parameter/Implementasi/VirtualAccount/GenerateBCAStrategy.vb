﻿Public Class GenerateBCAStrategy
    Inherits GenerateStrategy
    Private Const header = "0{0,5}C{1:ddMMyyyy}TAGIHAN"
    Private Const detail = "1{0,-23}{1,5}{2,-30}{3:ddMMyyyy}IDR{4:d15}{5:d15}{6:15}{7:15}{8:15}{9:15}"
    Private Const foother = "2{0,5}{1:d7}{2:d15}"
    Protected Overrides Sub doGenerate()
        createmResultDescriptions()
        Dim eVaBinNo = m_virtualAccountAgent.VaBinNo.Trim
        builder.AppendLine(String.Format(header, shelp(eVaBinNo, 5, True), m_UploadDate))
        For Each e In m_list
            Dim cnum = $"{eVaBinNo.Trim}{e.ContractNumber.Trim}"
            builder.AppendLine(String.Format(detail, shelp(cnum, 17), shelp(e.kodePersh, 5), shelp(e.NameOfCustomer, 30), e.DateOverDue,
                                             shelp(String.Format("{0}00", CType(Math.Round((e.installment + e.Pinalty), 0), Long)), 15, True),
                                             shelp(String.Format("{0}00", CType(Math.Round((e.installment + e.Pinalty), 0), Long)), 15, True), shelp("0", 15, True), shelp("0", 15, True), shelp("0", 15, True), shelp("0", 15, True)))
        Next
        Dim cnt = m_list.Count
        Dim tot = m_list.Sum(Function(e) e.installment + e.Pinalty)
        builder.appendline(String.Format(foother, shelp(eVaBinNo, 5, True), shelp(cnt, 7, True), shelp(String.Format("{0}00", CType(tot, Long)), 15, True)))
        writeFile(builder)
    End Sub

    Private Sub createmResultDescriptions()

        Dim resDesc = New ResultDescription(m_virtualAccountAgent.VaBankInitial.Trim, m_UploadDate, m_virtualAccountAgent.VaBankID.Trim, m_list.Count, m_list.Sum(Function(e) e.installment + e.Pinalty))
        m_resultDescriptions.Add(resDesc)
        m_fullPath = String.Format("{0}{1}", m_Path, resDesc.FileName)
    End Sub


End Class