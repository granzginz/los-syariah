﻿Public Class GeneratePERMATAStrategy
    Inherits GenerateStrategy 
    Private Const header = "0SEMENTARA DISAMAKAN DENGAN YANG BCA"
    Private Const detail = "1{0,5}{1,-18}{2,5}{3,-30}{4:ddMMyyyy}IDR{5:d15}{6:d15}{7:15}{8:15}{9:15}{10:15}"
    Private Const foother = "2{0,5}{1:d8}{2:d15}"
    Protected Overrides Sub doGenerate()
        Dim eVaBinNo = m_list(0).VaBinNo.Trim
        builder.AppendLine(String.Format(header, shelp(eVaBinNo, 5, True), m_UploadDate))
        For Each e In m_list
            builder.AppendLine(String.Format(detail,
                                             shelp(e.VaBinNo.Trim, 5, True),
                                             shelp(e.ContractNumber, 18),
                                             shelp(e.BranchId.Trim + "000", 5), shelp(e.NameOfCustomer, 30), e.DateOverDue,
                                             shelp(CType(Math.Round((e.installment + e.Pinalty), 0), Decimal), 15, True), shelp(CType(Math.Round((e.installment + e.Pinalty), 0), Decimal), 15, True),
            shelp("0", 15, True), shelp("0", 15, True), shelp("0", 15, True), shelp("0", 15, True)))
        Next
        Dim cnt = m_list.Count
        Dim tot = m_list.Sum(Function(e) e.installment + e.Pinalty)
        builder.AppendLine(String.Format(foother, shelp(eVaBinNo, 5, True), shelp(CType(cnt, Decimal), 8, True), shelp(CType(tot, Decimal), 15, True)))
        writeFile(builder)
    End Sub

End Class