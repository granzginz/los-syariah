﻿Imports Maxiloan.Parameter

Public Class CetakKartuPiutang : Inherits Common
    Public Property seqno As Integer
    Public Property AgreementNo As String
    Public Property AgreementUnitNo As String
    Public Property customername As String
    Public Property customerused As String
    Public Property tenor As Integer
    Public Property assetname As String
    Public Property norangka As String
    Public Property nomesin As String
    Public Property nopolisi As String
    Public Property ListData As DataTable
    Public Property UnitID
    Private _ListReport As DataSet
    Public Property ListReport() As DataSet
        Get
            Return CType(_ListReport, DataSet)
        End Get
        Set(ByVal Value As DataSet)
            _ListReport = Value
        End Set
    End Property


End Class
