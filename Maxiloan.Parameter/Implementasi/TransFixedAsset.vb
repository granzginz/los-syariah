Imports Maxiloan.Parameter

<Serializable()>
Public Class TransFixedAsset : Inherits Common
#Region "Constanta"
    Private _listTDP As DataTable
    Private _description As String
    Private _AmountRec As Double
    Private _TDPReceiveNo As String
    Private _bankaccount As String
    Private _postingdate As Date
    Private _bankAccountName As String
    Private _VoucherNo As String
    Private _TDPStatus As String
    Private _TDPStatusdesc As String
    Private _StatusDate As Date
    Private _strError As String

    Public ValueDate As Date
    Public ReferenceNo As String
    Public BankAccountID As String
	Public ListData As DataTable
	Public Listreport As DataSet
	Public CustomerName As String
    Public Agreementno As String
	Public BankAccountName As String

	Private _InvoiceNo As String
	Private _PaymentAllocationID As String
	Private _Post As String
	Private _Amount As Double
	Private _RefDesc As String
	Private _VoucherDesc As String
	Private _ReceivedFrom As String
	Private _Departement As String
    Private _AktivaId As String
    Private _ApprovalNo As String
    Private _DeleteReason As String
    Private _InvoiceDate As Date
    Private _InternalMemoNo As String
    Private _Notes As String

#End Region


    Public Property listTDP() As DataTable
		Get
			Return (CType(_listTDP, DataTable))
		End Get
		Set(ByVal Value As DataTable)
			_listTDP = Value
		End Set
	End Property

	Public Property description() As String
		Get
			Return (CType(_description, String))
		End Get
		Set(ByVal Value As String)
			_description = Value
		End Set
	End Property

	Public Property bankaccount() As String
		Get
			Return (CType(_bankaccount, String))
		End Get
		Set(ByVal Value As String)
			_bankaccount = Value
		End Set
	End Property

	Public Property AmountRec() As Double
		Get
			Return (CType(_AmountRec, Double))
		End Get
		Set(ByVal Value As Double)
			_AmountRec = Value
		End Set
	End Property


	Public Property postingdate() As Date
		Get
			Return (CType(_postingdate, Date))
		End Get
		Set(ByVal Value As Date)
			_postingdate = Value
		End Set
	End Property

	Public Property VoucherNo() As String
		Get
			Return (CType(_VoucherNo, String))
		End Get
		Set(ByVal Value As String)
			_VoucherNo = Value
		End Set
	End Property

	Public Property TDPStatus() As String
		Get
			Return (CType(_TDPStatus, String))
		End Get
		Set(ByVal Value As String)
			_TDPStatus = Value
		End Set
	End Property

	Public Property TDPStatusdesc() As String
		Get
			Return (CType(_TDPStatusdesc, String))
		End Get
		Set(ByVal Value As String)
			_TDPStatusdesc = Value
		End Set
	End Property

	Public Property StatusDate() As Date
		Get
			Return (CType(_StatusDate, Date))
		End Get
		Set(ByVal Value As Date)
			_StatusDate = Value
		End Set
	End Property

	Public Property strError() As String
		Get
			Return (CType(_strError, String))
		End Get
		Set(ByVal Value As String)
			_strError = Value
		End Set
	End Property
	Public Property TDPReceiveNo() As String
		Get
			Return (CType(_TDPReceiveNo, String))
		End Get
		Set(ByVal Value As String)
			_TDPReceiveNo = Value
		End Set
	End Property

	Public Property InvoiceNo As String
		Get
			Return _InvoiceNo
		End Get
		Set(value As String)
			_InvoiceNo = value
		End Set
	End Property

	Public Property PaymentAllocationID As String
		Get
			Return _PaymentAllocationID
		End Get
		Set(value As String)
			_PaymentAllocationID = value
		End Set
	End Property

	Public Property Post As String
		Get
			Return _Post
		End Get
		Set(value As String)
			_Post = value
		End Set
	End Property

	Public Property Amount As Double
		Get
			Return _Amount
		End Get
		Set(value As Double)
			_Amount = value
		End Set
	End Property

	Public Property RefDesc As String
		Get
			Return _RefDesc
		End Get
		Set(value As String)
			_RefDesc = value
		End Set
	End Property

	Public Property VoucherDesc As String
		Get
			Return _VoucherDesc
		End Get
		Set(value As String)
			_VoucherDesc = value
		End Set
	End Property

	Public Property ReceivedFrom As String
		Get
			Return _ReceivedFrom
		End Get
		Set(value As String)
			_ReceivedFrom = value
		End Set
	End Property

	Public Property Departement As String
		Get
			Return _Departement
		End Get
		Set(value As String)
			_Departement = value
		End Set
	End Property

    Public Property AktivaId As String
        Get
            Return _AktivaId
        End Get
        Set(value As String)
            _AktivaId = value
        End Set
    End Property
    Public Property ApprovalNo As String
        Get
            Return _ApprovalNo
        End Get
        Set(value As String)
            _ApprovalNo = value
        End Set
    End Property
    Public Property DeleteReason As String
        Get
            Return _DeleteReason
        End Get
        Set(value As String)
            _DeleteReason = value
        End Set
    End Property
    Public Property InvoiceDate As String
        Get
            Return _InvoiceDate
        End Get
        Set(value As String)
            _InvoiceDate = value
        End Set
    End Property
    Public Property InternalMemoNo As String
        Get
            Return _InternalMemoNo
        End Get
        Set(value As String)
            _InternalMemoNo = value
        End Set
    End Property
    Public Property Notes As String
        Get
            Return _Notes
        End Get
        Set(value As String)
            _Notes = value
        End Set
    End Property
End Class

