﻿<Serializable>
Public Class SimulasiPerhitunganPembiayaan : Inherits Common
    Public Property CustomerID As String
    Public Property AgreementNo As String
    Public Property ApplicationID As String
    Public Property NamaDebitur As String
    Public Property NomorKontrak As String
    Public Property JenisPembiayaanID As String
    Public Property JenisPembiayaan As String
    Public Property ProductID As String
    Public Property ObjekPembiayaan As String
    Public Property EffectiveRate As Date
    Public Property Tenor As Integer
    Public Property HargaObjekPembiayaan As Double
    Public Property UangMuka As Double
    Public Property NilaiPembiayaan As Double
    Public Property AngsuranPerBulan As Double
    Public Property BungaPembiayaan As Double
    Public Property TotalPembiayaan As String
    Public Property BiayaProvisi As Double
    Public Property BiayaAdmin As Double
    Public Property BiayaAsuransi As Double
    Public Property BiayaFidusia As Double
    Public Property RateDenda As Double
    Public Property NilaiDenda As Double
    Public Property TarifDendaText1 As String
    Public Property TarifDendaText2 As String
    Public Property TarifDendaText3 As String
    Public Property EarlyTerminationRate As Double
    Public Property EarlyTerminationPenalty As Double
    Public Property InsSeqNo As Integer
    Public Property OutstandingPokok As Double
    Public Property BranchFullName As String

    Private _ListReport As DataSet
    Public Property ListData As DataTable

    Public Property ListReport() As DataSet
        Get
            Return CType(_ListReport, DataSet)
        End Get
        Set(ByVal Value As DataSet)
            _ListReport = Value
        End Set
    End Property
End Class
