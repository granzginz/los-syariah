﻿<Serializable()> _
Public Class Implementasi : Inherits Common

    Public Property ApplicationID As String
    Public Property NilaiOSP As Decimal
    Public Property SPName As String
    Public Property BiayaTarik As Decimal
    Public Property NilaiNDT As Decimal

    Public Property StartDate As DateTime
    Public Property EndDate As DateTime
    Public Property PeriodStatus As Long
    Public Property PeriodEverClosed As Long
    Public Property UpDown As String
    Public Property ReportId As String
    Public Property GroupId As Int16
    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
    Public Property CoaId As String
    Public Property UsrUpd As String
    Public Property IsUpdateSattus As Int32

    Public Property valutadate As DateTime
    Public Property bankaccountid As String
    Public Property amount As Double
    Public Property ReffNo As String
    Public Property SourceRK As String
    Public Property Year As Object
    Public Property Month As Object
End Class
