
<Serializable()> _
Public Class News : Inherits Maxiloan.Parameter.Common
    Private _newsyear As String
    Private _newsnumber As Integer
    Private _postingdate As DateTime
    Private _authorby As String
    Private _title As String
    Private _abstract As String

    Public Property NewsYear() As String
        Get
            Return _newsyear
        End Get
        Set(ByVal Value As String)
            _newsyear = Value
        End Set
    End Property

    Public Property NewsNumber() As Integer
        Get
            Return _newsnumber
        End Get
        Set(ByVal Value As Integer)
            _newsnumber = Value
        End Set
    End Property

    Public Property PostingDate() As DateTime
        Get
            Return _postingdate
        End Get
        Set(ByVal Value As DateTime)
            _postingdate = Value
        End Set
    End Property

    Public Property AuthorBy() As String
        Get
            Return _authorby
        End Get
        Set(ByVal Value As String)
            _authorby = Value
        End Set
    End Property

    Public Property Title() As String
        Get
            Return _title
        End Get
        Set(ByVal Value As String)
            _title = Value
        End Set
    End Property

    Public Property Abstract() As String
        Get
            Return _abstract
        End Get
        Set(ByVal Value As String)
            _abstract = Value
        End Set
    End Property
    Public Property NewsDetail() As String
        Get
            Return _detailnews
        End Get
        Set(ByVal Value As String)
            _detailnews = Value
        End Set
    End Property

    Public Property ImageNews() As String
        Get
            Return _imagenews
        End Get
        Set(ByVal Value As String)
            _imagenews = Value
        End Set
    End Property
    Private _detailnews As String
    Private _imagenews As String

End Class
