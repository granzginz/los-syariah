
<Serializable()> _
Public Class GroupUser : Inherits Maxiloan.Parameter.Common
    Dim _listgroupuser As DataTable
    Dim _listmasterapplication As DataTable
    Dim _groupid As String
    Dim _applicationid As String
    Dim _groupname As String

    Public Property ListGroupUser() As DataTable
        Get
            Return (CType(_listgroupuser, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listgroupuser = Value
        End Set
    End Property

    Public Property ListMasterApplication() As DataTable
        Get
            Return (CType(_listmasterapplication, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listmasterapplication = Value
        End Set
    End Property
    Public Property GroupID() As String
        Get
            Return (CType(_groupid, String))
        End Get
        Set(ByVal Value As String)
            _groupid = Value
        End Set
    End Property

    Public Property ApplicationId() As String
        Get
            Return (CType(_applicationid, String))
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property


    Public Property GroupName() As String
        Get
            Return (CType(_groupname, String))
        End Get
        Set(ByVal Value As String)
            _groupname = Value
        End Set
    End Property
End Class
