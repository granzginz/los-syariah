
<Serializable()> _
Public Class SystemAlert : Inherits Maxiloan.Parameter.Common

    Private _alertid As String
    Private _alertmessage As String
    Private _numbersqlcmd As String
    Private _detailsqlcmd As String
    Private _keycolumnname As String
    Private _isdefaultform As Boolean
    Private _listformid As String
    Private _listformparameter As String
    Private _viewformid As String
    Private _viewformparameter As String
    Private _editformid As String
    Private _editformparameter As String
    Private _groupalertid As String
    Private _groupalertname As String
    Private _groupallalertid As String
    Private _listuseralert As DataTable
    Public Property AlertID() As String
        Get
            Return _alertid
        End Get
        Set(ByVal Value As String)
            _alertid = Value
        End Set
    End Property

    Public Property AlertMessage() As String
        Get
            Return _alertmessage
        End Get
        Set(ByVal Value As String)
            _alertmessage = Value
        End Set
    End Property

    Public Property ViewFormParameter() As String
        Get
            Return _viewformparameter
        End Get
        Set(ByVal Value As String)
            _viewformparameter = Value
        End Set
    End Property

    Public Property NumberSQLCmd() As String
        Get
            Return _numbersqlcmd
        End Get
        Set(ByVal Value As String)
            _numbersqlcmd = Value
        End Set
    End Property
    Public Property DetailSQLCmd() As String
        Get
            Return _detailsqlcmd
        End Get
        Set(ByVal Value As String)
            _detailsqlcmd = Value
        End Set
    End Property
    Public Property KeyColumnName() As String
        Get
            Return _keycolumnname
        End Get
        Set(ByVal Value As String)
            _keycolumnname = Value
        End Set
    End Property
    Public Property IsDefaultForm() As Boolean
        Get
            Return _isdefaultform
        End Get
        Set(ByVal Value As Boolean)
            _isdefaultform = Value
        End Set
    End Property
    Public Property ListFormID() As String
        Get
            Return _listformid
        End Get
        Set(ByVal Value As String)
            _listformid = Value
        End Set
    End Property
    Public Property ListFormParameter() As String
        Get
            Return _listformparameter
        End Get
        Set(ByVal Value As String)
            _listformparameter = Value
        End Set
    End Property
    Public Property ViewFormID() As String
        Get
            Return _viewformid
        End Get
        Set(ByVal Value As String)
            _viewformid = Value
        End Set
    End Property
    Public Property EditFormID() As String
        Get
            Return _editformid
        End Get
        Set(ByVal Value As String)
            _editformid = Value
        End Set
    End Property
    Public Property EditFormParameter() As String
        Get
            Return _editformparameter
        End Get
        Set(ByVal Value As String)
            _editformparameter = Value
        End Set
    End Property

    Public Property GroupAlertID() As String
        Get
            Return _groupalertid
        End Get
        Set(ByVal Value As String)
            _groupalertid = Value
        End Set
    End Property
    Public Property GroupAllAlertID() As String
        Get
            Return _groupallalertid
        End Get
        Set(ByVal Value As String)
            _groupallalertid = Value
        End Set
    End Property

    Public Property ListUserAlert() As DataTable
        Get
            Return _listuseralert
        End Get
        Set(ByVal Value As DataTable)
            _listuseralert = Value
        End Set
    End Property


    Public Property GroupAlertName() As String
        Get
            Return _groupalertname
        End Get
        Set(ByVal Value As String)
            _groupalertname = Value
        End Set
    End Property
End Class
