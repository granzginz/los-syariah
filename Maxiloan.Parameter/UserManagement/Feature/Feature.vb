
<Serializable()> _
Public Class Feature
    Inherits Maxiloan.Parameter.Common
    Dim _listmasterform As DataTable
    Dim _listfeatureuser As DataTable
    Dim _formfilename As String

    Public Property ListMasterForm() As DataTable
        Get
            Return (CType(_listmasterform, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listmasterform = Value
        End Set
    End Property

    Public Property ListFeatureUser() As DataTable
        Get
            Return (CType(_listfeatureuser, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listfeatureuser = Value
        End Set
    End Property

    Public Property FormName() As String
        Get
            Return CType(_formfilename, String)
        End Get
        Set(ByVal Value As String)
            _formfilename = Value
        End Set
    End Property
End Class
