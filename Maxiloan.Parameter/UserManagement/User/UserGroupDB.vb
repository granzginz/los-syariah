
<Serializable()> _
Public Class UserGroupDB : Inherits Maxiloan.Parameter.Common
    Dim _listuserapplication As DataTable
    Dim _listmasterapplication As DataTable
    Dim _groupdbid As String
    Dim _groupdbiditem As Int32
    Dim _applicationid As String
    Dim _groupdbidall As String
    Public Property ListUserGroupDb() As DataTable
        Get
            Return (CType(_listuserapplication, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listuserapplication = Value
        End Set
    End Property

    Public Property ApplicationId() As String
        Get
            Return (CType(_applicationid, String))
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property

    Public Property GroupDBID() As String
        Get
            Return (CType(_groupdbid, String))
        End Get
        Set(ByVal Value As String)
            _groupdbid = Value
        End Set
    End Property

    Public Property GroupDBIDItem() As Int32
        Get
            Return (CType(_groupdbiditem, Int32))
        End Get
        Set(ByVal Value As Int32)
            _groupdbiditem = Value
        End Set
    End Property
    Public Property GroubDBAll() As String
        Get
            Return (CType(_groupdbidall, String))
        End Get
        Set(ByVal Value As String)
            _groupdbidall = Value
        End Set
    End Property
End Class
