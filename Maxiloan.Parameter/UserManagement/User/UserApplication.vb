
<Serializable()> _
Public Class UserApplication : Inherits Maxiloan.Parameter.Common
    Dim _listuserapplication As DataTable
    Dim _listmasterapplication As DataTable
    Dim _order As String
    Dim _applicationid As String
    Dim _isactive As Boolean

    Public Property ListUserApplication() As DataTable
        Get
            Return (CType(_listuserapplication, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listuserapplication = Value
        End Set
    End Property

    Public Property ListMasterApplication() As DataTable
        Get
            Return (CType(_listmasterapplication, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listmasterapplication = Value
        End Set
    End Property
    Public Property Order() As String
        Get
            Return (CType(_order, String))
        End Get
        Set(ByVal Value As String)
            _order = Value
        End Set
    End Property

    Public Property ApplicationId() As String
        Get
            Return (CType(_applicationid, String))
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property


    Public Property isActive() As Boolean
        Get
            Return (CType(_isactive, Boolean))
        End Get
        Set(ByVal Value As Boolean)
            _isactive = Value
        End Set
    End Property
End Class
