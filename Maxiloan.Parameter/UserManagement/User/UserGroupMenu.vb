
<Serializable()> _
Public Class UserGroupMenu : Inherits Maxiloan.Parameter.Common
    Dim _listuserapplication As DataTable
    Dim _listmasterapplication As DataTable
    Dim _groupdbid As String
    Dim _groupdbmenu As String
    Dim _groupdbmenuitem As Int32
    Dim _applicationid As String
    Dim _groupdball As String
    Public Property ListUserGroupDb() As DataTable
        Get
            Return (CType(_listuserapplication, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listuserapplication = Value
        End Set
    End Property

    Public Property ApplicationId() As String
        Get
            Return (CType(_applicationid, String))
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property

    Public Property GroupDBID() As String
        Get
            Return (CType(_groupdbid, String))
        End Get
        Set(ByVal Value As String)
            _groupdbid = Value
        End Set
    End Property

    Public Property GroupDBMenu() As String
        Get
            Return (CType(_groupdbmenu, String))
        End Get
        Set(ByVal Value As String)
            _groupdbmenu = Value
        End Set
    End Property

    Public Property GroupDBMenuItem() As Int32
        Get
            Return (CType(_groupdbmenuitem, Int32))
        End Get
        Set(ByVal Value As Int32)
            _groupdbmenuitem = Value
        End Set
    End Property
    Public Property GroupDBAll() As String
        Get
            Return (CType(_groupdball, String))
        End Get
        Set(ByVal Value As String)
            _groupdball = Value
        End Set
    End Property
End Class
