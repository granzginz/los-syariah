
<Serializable()> _
Public Class InfoBox : Inherits Maxiloan.Parameter.Common
    Private _infoboxid As String
    Private _infoboxtitle As String
    Private _icon As String
    Private _isgeneral As Boolean
    Private _infoboxside As String
    Private _infoboxtype As String
    Private _includefilename As String
    Private _bgcolor As String
    Private _fontcolor As String
    Private _order As Integer
    Private _listuserinfobox As DataTable
    Private _groupallinfoboxid As String
    Private _groupinfoboxid As String

    Public Property GroupAllInfoBoxID() As String
        Get
            Return _groupallinfoboxid
        End Get
        Set(ByVal Value As String)
            _groupallinfoboxid = Value
        End Set
    End Property

    Public Property GroupInfoBoxID() As String
        Get
            Return _groupinfoboxid
        End Get
        Set(ByVal Value As String)
            _groupinfoboxid = Value
        End Set
    End Property

    Public Property ListUserInfoBox() As DataTable
        Get
            Return _listuserinfobox
        End Get
        Set(ByVal Value As DataTable)
            _listuserinfobox = Value
        End Set
    End Property
    Public Property InfoBoxID() As String
        Get
            Return _infoboxid
        End Get
        Set(ByVal Value As String)
            _infoboxid = Value

        End Set
    End Property

    Public Property InfoBoxTitle() As String
        Get
            Return _infoboxtitle
        End Get
        Set(ByVal Value As String)
            _infoboxtitle = Value
        End Set
    End Property

    Public Property Icon() As String
        Get
            Return _icon
        End Get
        Set(ByVal Value As String)
            _icon = Value
        End Set
    End Property

    Public Property InfoBoxSide() As String
        Get
            Return _infoboxside
        End Get
        Set(ByVal Value As String)
            _infoboxside = Value
        End Set
    End Property

    Public Property InfoBoxType() As String
        Get
            Return _infoboxtype
        End Get
        Set(ByVal Value As String)
            _infoboxtype = Value
        End Set
    End Property

    Public Property BGColor() As String
        Get
            Return _bgcolor
        End Get
        Set(ByVal Value As String)
            _bgcolor = Value
        End Set
    End Property


    Public Property FontColor() As String
        Get
            Return _fontcolor
        End Get
        Set(ByVal Value As String)
            _fontcolor = Value
        End Set
    End Property

    Public Property Order() As Integer
        Get
            Return _order
        End Get
        Set(ByVal Value As Integer)
            _order = Value
        End Set
    End Property

    Public Property IsGeneral() As Boolean
        Get
            Return _isgeneral
        End Get
        Set(ByVal Value As Boolean)
            _isgeneral = Value
        End Set
    End Property
    Public Property IncludeFileName() As String
        Get
            Return _includefilename
        End Get
        Set(ByVal Value As String)
            _includefilename = Value
        End Set
    End Property
End Class
