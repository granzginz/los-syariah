
<Serializable()> _
Public Class MasterForm : Inherits Maxiloan.Parameter.Common
    Dim _listmasterform As DataTable
    Dim _listmasterapplication As DataTable
    Dim _callbymenu As Boolean
    Dim _subsystem As String
    Dim _formid As String
    Dim _formname As String
    Dim _formfilename As String
    Dim _applicationid As String
    Dim _icon As String
    Public Property ListMasterForm() As DataTable
        Get
            Return (CType(_listmasterform, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listmasterform = Value
        End Set
    End Property

    Public Property ListMasterApplication() As DataTable
        Get
            Return (CType(_listmasterapplication, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listmasterapplication = Value
        End Set
    End Property

    Public Property CallByMenu() As Boolean
        Get
            Return (CType(_callbymenu, Boolean))
        End Get
        Set(ByVal Value As Boolean)
            _callbymenu = Value
        End Set
    End Property

    Public Property FormName() As String
        Get
            Return (CType(_formname, String))
        End Get
        Set(ByVal Value As String)
            _formname = Value
        End Set
    End Property
    Public Property FormFileName() As String
        Get
            Return (CType(_formfilename, String))
        End Get
        Set(ByVal Value As String)
            _formfilename = Value
        End Set
    End Property

    Public Property Applicationid() As String
        Get
            Return (CType(_applicationid, String))
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property

    Public Property Icon() As String
        Get
            Return (CType(_icon, String))
        End Get
        Set(ByVal Value As String)
            _icon = Value
        End Set
    End Property
End Class
