

<Serializable()> _
Public Class Personal
    Private _PersonName As String
    Private _PersonTitle As String
    Private _email As String
    Private _MobilePhone As String

    Public Property PersonName() As String
        Get
            Return _PersonName
        End Get
        Set(ByVal Value As String)
            _PersonName = Value
        End Set
    End Property
    Public Property PersonTitle() As String
        Get
            Return _PersonTitle
        End Get
        Set(ByVal Value As String)
            _PersonTitle = Value
        End Set
    End Property
    Public Property Email() As String
        Get
            Return _email
        End Get
        Set(ByVal Value As String)
            _email = Value
        End Set
    End Property
    Public Property MobilePhone() As String
        Get
            Return _MobilePhone
        End Get
        Set(ByVal Value As String)
            _MobilePhone = Value
        End Set
    End Property
    Public Property Penghasilan As Double
    Public Property Catatan As String    
End Class
