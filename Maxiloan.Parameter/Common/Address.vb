

<Serializable()> _
Public Class Address

    Private _Address As String
    Private _RT As String
    Private _RW As String
    Private _Kelurahan As String
    Private _Kecamatan As String
    Private _City As String
    Private _ZipCode As String
    Private _AreaPhone1 As String
    Private _Phone1 As String
    Private _AreaPhone2 As String
    Private _Phone2 As String
    Private _AreaFax As String
    Private _fax As String
    Private _MobilePhone As String
    Private _Email As String



    Public Property Address() As String
        Get
            Return _Address
        End Get
        Set(ByVal Value As String)
            _Address = Value
        End Set
    End Property

    Public Property RT() As String
        Get
            Return _RT
        End Get
        Set(ByVal Value As String)
            _RT = Value
        End Set
    End Property

    Public Property RW() As String
        Get
            Return _RW
        End Get
        Set(ByVal Value As String)
            _RW = Value
        End Set
    End Property

    Public Property Kelurahan() As String
        Get
            Return _Kelurahan
        End Get
        Set(ByVal Value As String)
            _Kelurahan = Value
        End Set
    End Property

    Public Property Kecamatan() As String
        Get
            Return _Kecamatan
        End Get
        Set(ByVal Value As String)
            _Kecamatan = Value
        End Set
    End Property

    Public Property City() As String
        Get
            Return _City
        End Get
        Set(ByVal Value As String)
            _City = Value
        End Set
    End Property

    Public Property ZipCode() As String
        Get
            Return _ZipCode
        End Get
        Set(ByVal Value As String)
            _ZipCode = Value
        End Set
    End Property

    Public Property AreaPhone1() As String
        Get
            Return _AreaPhone1
        End Get
        Set(ByVal Value As String)
            _AreaPhone1 = Value
        End Set
    End Property

    Public Property AreaFax() As String
        Get
            Return _AreaFax
        End Get
        Set(ByVal Value As String)
            _AreaFax = Value
        End Set
    End Property

    Public Property Phone1() As String
        Get
            Return _Phone1
        End Get
        Set(ByVal Value As String)
            _Phone1 = Value
        End Set
    End Property

    Public Property AreaPhone2() As String
        Get
            Return _AreaPhone2
        End Get
        Set(ByVal Value As String)
            _AreaPhone2 = Value
        End Set
    End Property

    Public Property Phone2() As String
        Get
            Return _Phone2
        End Get
        Set(ByVal Value As String)
            _Phone2 = Value
        End Set
    End Property

    Public Property Fax() As String
        Get
            Return _fax
        End Get
        Set(ByVal Value As String)
            _fax = Value
        End Set
    End Property

    Public Property MobilePhone() As String
        Get
            Return _MobilePhone
        End Get
        Set(ByVal value As String)
            _MobilePhone = value
        End Set
    End Property
End Class
