


Public Class MailTransaction : Inherits Maxiloan.Parameter.SPPA
    Private _ApplicationID As String
    Private _MailTypeID As String
    Private _MailTransNo As String
    Private _MailDateCreate As Date
    Private _MailUserCreate As String
    Private _MailDatePrint As Date
    Private _MailPrintedNum As Int16
    Private _MailUserPrint As String



    Public Property MailPrintedNum() As Int16
        Get
            Return _MailPrintedNum
        End Get
        Set(ByVal Value As Int16)
            _MailPrintedNum = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property


    Public Property MailDateCreate() As Date
        Get
            Return _MailDateCreate
        End Get
        Set(ByVal Value As Date)
            _MailDateCreate = Value
        End Set
    End Property

    Public Property MailDatePrint() As Date
        Get
            Return _MailDatePrint
        End Get
        Set(ByVal Value As Date)
            _MailDatePrint = Value
        End Set
    End Property

    Public Property MailTypeID() As String
        Get
            Return _MailTypeID
        End Get
        Set(ByVal Value As String)
            _MailTypeID = Value
        End Set
    End Property


    Public Property MailTransNo() As String
        Get
            Return _MailTransNo
        End Get
        Set(ByVal Value As String)
            _MailTransNo = Value
        End Set
    End Property


    Public Property MailUserCreate() As String
        Get
            Return _MailUserCreate
        End Get
        Set(ByVal Value As String)
            _MailUserCreate = Value
        End Set
    End Property


    Public Property MailUserPrint() As String
        Get
            Return _MailUserPrint
        End Get
        Set(ByVal Value As String)
            _MailUserPrint = Value
        End Set
    End Property




End Class
