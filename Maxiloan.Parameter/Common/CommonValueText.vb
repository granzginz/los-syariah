﻿<Serializable()>
Public Class CommonValueText
    Implements IComparable

    Private _value As String
    Private _text As String

    Sub New()

    End Sub

    Public Sub New(value As String, text As String)
        _value = value
        _text = text
    End Sub

    Public Property Value() As String
        Get
            Return _value
        End Get
        Set(value As String)
            _value = value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return _text
        End Get
        Set(value As String)
            _text = value
        End Set
    End Property

    Public Function CompareTo(obj As Object) As Integer Implements IComparable.CompareTo
        Return String.CompareOrdinal(Text, CType(obj, CommonValueText).Text)
    End Function

    Public Overrides Function ToString() As String
        Return String.Format("{0} {1}", _value, _text)
    End Function

End Class
