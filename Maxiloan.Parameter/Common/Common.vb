﻿<Serializable()>
Public Class Common
    Public Property TableName As String
    Public Property modeCalculateSave() As Boolean
    Public Property SubSystem() As String
    Public Property CoyID() As String
    Public Property PODateFrom() As String
    Public Property PODateTo() As String
    Public Property strConnection() As String
    Public Property LoginId() As String
    Public Property FullName() As String
    Public Property Password() As String
    Public Property DBName() As String
    Public Property ServerName() As String
    Public Property AmountTransfer() As String
    Public Property BranchName() As String
    Public Property IsAuthorize() As Boolean
    Public Property FormID() As String
    Public Property DebitAmount() As Double
    Public Property CreditAmount() As Double
    Public Property BeforeBalance() As Double
    Public Property CurrentBalance() As Double
    Public Property RequestWONo() As String
    Public Property BranchId() As String
    Public Property BusinessDate() As Date
    Public Property Search() As String
    Public Property SPType() As String
    Public Property txtSearch() As String
    Public Property AppID() As String
    Public Property FeatureID() As String
    Public Property ReasonID() As String
    Public Property JournalCode() As String
    Public Property ReasonTypeID() As String
    Public Property SesionID() As String
    Public Property CompanyID() As String
    Public Property CompanyName() As String
    Public Property AmountFrom As Double
    Public Property AmountTo As Double
    Public Property Rate As Double
    Public Property InsRateCategory As String
    Public Property BranchCity As String
    Public Property Month As String
    Public Property Year As Int32
    Public Property Tr_Nomor As String
    Public Property StatusView As String
    Public Property IsConnect() As Boolean
    Public Property MacAddress As String
    Public Property TableVoucherNO As DataTable

#Region "Paging"
    Public Property TotalRecord() As Int32
    Public Property PageSize() As Int16
    Public Property CurrentPage() As Int32
    Public Property WhereCond() As String
    Public Property WhereCond2() As String
    Public Property SortBy() As String
#End Region
End Class
