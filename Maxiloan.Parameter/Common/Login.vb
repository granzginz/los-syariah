﻿<Serializable()> _
Public Class Login : Inherits Maxiloan.Parameter.Common
    Public Property PwdExpired() As Integer
    Public Property PwdFailed() As Integer       
    Public Property PwdHistory() As Integer        
    Public Property PwdLength() As Integer        
    Public Property ListPassword() As DataTable       
    Public Property ListMultiBranch() As DataTable        
    Public Property IsActive() As Boolean        
    Public Property IsReset() As Boolean        
    Public Property IsMultiBranch() As Boolean        
    Public Property LoginApplication() As DataTable       
    Public Property IsHoBranch() As Boolean       
    Public Property Applicationid() As String       
    Public Property GroupDBID() As String       
    Public Property IsEod() As Boolean        
    Public Property IsClosed() As Boolean
    Public Property ServerNameRS As String
    Public Property DatabaseNameRS As String
    Public Property VirtualDirectoryRS As String
    Public Property ReportManagerRS As String
    Public Property UIDRS As String
    Public Property PassRS As String
    Public Property ApplicationManagerDB As String
    Public Property PassKeyPrefix As String
    Public Property EmpPos As String
    Public Property PoolSize As String
    Public Property activityid As Int16
End Class
