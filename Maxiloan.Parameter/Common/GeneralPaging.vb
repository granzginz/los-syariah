

Public Class GeneralPaging : Inherits Maxiloan.Parameter.Common

    Private _SpName As String
    Private _ApplicationID As String
    Private _ListData As DataTable
    Private _totalrecords As Int64
    Private _ListDataReport As DataSet
    Private _WhereCond1 As String
    Private _WhereCond2 As String
    Private _WhereCond3 As String
    Private _WhereCond4 As String
    Private _ARDate As String
    Private _APDate As String
    Private _SuspendDate As String
    Private _strDate As String
    Private _DateFrom As DateTime
    Private _DateTo As DateTime
    Private _ValueDate As DateTime
    Private _isBusinessDate As Boolean

    Public Property isBusinessDate() As Boolean
        Get
            Return _isBusinessDate
        End Get
        Set(ByVal Value As Boolean)
            _isBusinessDate = Value
        End Set
    End Property

    Public Property DateFrom() As DateTime
        Get
            Return _DateFrom
        End Get
        Set(ByVal Value As DateTime)
            _DateFrom = Value
        End Set
    End Property
    Public Property DateTo() As DateTime
        Get
            Return _DateTo
        End Get
        Set(ByVal Value As DateTime)
            _DateTo = Value
        End Set
    End Property
    Public Property strDate() As String
        Get
            Return _strDate
        End Get
        Set(ByVal Value As String)
            _strDate = Value
        End Set
    End Property
    Public Property SuspendDate() As String
        Get
            Return _SuspendDate
        End Get
        Set(ByVal Value As String)
            _SuspendDate = Value
        End Set
    End Property
    Public Property APDate() As String
        Get
            Return _APDate
        End Get
        Set(ByVal Value As String)
            _APDate = Value
        End Set
    End Property
    Public Property ARDate() As String
        Get
            Return _ARDate
        End Get
        Set(ByVal Value As String)
            _ARDate = Value
        End Set
    End Property

    Public Property WhereCond1() As String
        Get
            Return _WhereCond1
        End Get
        Set(ByVal Value As String)
            _WhereCond1 = Value
        End Set
    End Property

    Public Property WhereCond2() As String
        Get
            Return _WhereCond2
        End Get
        Set(ByVal Value As String)
            _WhereCond2 = Value
        End Set
    End Property

    Public Property WhereCond3() As String
        Get
            Return _WhereCond3
        End Get
        Set(ByVal Value As String)
            _WhereCond3 = Value
        End Set
    End Property

    Public Property WhereCond4() As String
        Get
            Return _WhereCond4
        End Get
        Set(ByVal Value As String)
            _WhereCond4 = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property ListDataReport() As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(ByVal Value As DataSet)
            _ListDataReport = Value
        End Set
    End Property


    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal Value As DataTable)
            _ListData = Value
        End Set
    End Property

    Public Property SpName() As String
        Get
            Return _SpName
        End Get
        Set(ByVal Value As String)
            _SpName = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property

    Public Property FundingBatchNo As String
    Public Property FundingID As String
    Public Property FasilitasNo As String
    Public Property SeqNo As String
End Class
