﻿Imports System.Text

<Serializable()>
Public Class ScoreResults
    Private _scoreResults As IList(Of Parameter.ScoreResult) = New List(Of Parameter.ScoreResult)

    Private _isFatalScore As Boolean = False
    Private _fatalStatusNote As New StringBuilder
    Private _lApproveScore As Decimal
    Private _lDblRejectScore As Decimal
    Private _lCuttOffScore As Decimal

    Public ReadOnly Property CuttOff As Decimal
        Get
            Return _lCuttOffScore
        End Get
    End Property
    Public Property ScoreSchemeID As String
    Sub New()
        _scoreResults = New List(Of Parameter.ScoreResult)

    End Sub

    Public Sub Add(item As Parameter.ScoreResult)
        _scoreResults.Add(item)
    End Sub

    Public Sub FatalStatusNote(note As String)

        If Not (_isFatalScore) Then
            _fatalStatusNote.AppendLine("Rejected With Fatal Score")
        End If

        _isFatalScore = True
        _fatalStatusNote.AppendLine(note)
    End Sub
    Public ReadOnly Property TotalScore As Decimal
        Get
            Dim result As Decimal = 0
            For Each item In _scoreResults
                result += item.ScoreValue
            Next
            Return result
        End Get
    End Property
    ReadOnly Property ResultCalc As Decimal
        Get
            Dim result As Decimal = 0
            For Each item In _scoreResults
                result += item.ResultCalc
            Next
            Return result
        End Get
    End Property

    Public ReadOnly Property ToDataTable As DataTable
        Get
            Dim dt = New DataTable()
            For Each s In New String() {"ScoreComponentID", "ComponentContent", "ScoreValue", "ScoreAbsolute", "Weight", "ComponentValue", "ScoreStatus", "ScoreDescription", "ContentSeqNo", "QueryResult"}
                dt.Columns.Add(New DataColumn(s))
            Next

            For Each item In _scoreResults
                Dim row = dt.NewRow()

                row("ScoreComponentID") = item.ScoreComponentID.Trim
                row("ComponentContent") = item.ComponentContent.Trim
                row("ScoreValue") = item.ScoreValue
                row("ScoreAbsolute") = item.ScoreAbsolute
                row("Weight") = item.Weight
                row("ComponentValue") = item.ComponentValue.Trim
                row("ScoreStatus") = item.ScoreStatus.Trim
                row("ScoreDescription") = item.ScoreDescription.Trim
                row("ContentSeqNo") = item.ContentSeqNo
                row("QueryResult") = item.QueryResult
                dt.Rows.Add(row)
            Next
            Return dt

        End Get
    End Property

    Public Sub SetRejectApproveScore(lApproveScore As Decimal, lRejectScore As Decimal, lCuttOffScore As Decimal)
        _lApproveScore = lApproveScore
        _lDblRejectScore = lRejectScore
        _lCuttOffScore = lCuttOffScore
    End Sub
    Public Function FatalCount() As Integer
        Return _scoreResults.Where(Function(x) x.ScoreStatus.Trim = "F").Count
    End Function
    Public Function WarningCount() As Integer
        Return _scoreResults.Where(Function(x) x.ScoreStatus.Trim = "W").Count
    End Function
    Public Function RejectCount() As Integer
        Return _scoreResults.Where(Function(x) x.ScoreStatus.Trim = "R").Count
    End Function
    Public Function NeutralCount() As Integer
        Return _scoreResults.Where(Function(x) x.ScoreStatus.Trim = "N").Count
    End Function
    'Public ReadOnly Property ScoringResultStatus As String
    '    Get
    '        Dim ret As String = ""
    '        If _isFatalScore Or _lDblRejectScore >= ResultCalc Then
    '            ret = "R"
    '        ElseIf _lApproveScore > ResultCalc And _lDblRejectScore < ResultCalc Then
    '            ret = "M"
    '        ElseIf _lApproveScore <= ResultCalc Then
    '            ret = "A"
    '        End If
    '        Return ret
    '    End Get
    'End Property

    Public ReadOnly Property FinalDecision As String
        Get

            If ResultCalc < _lCuttOffScore Or FatalCount() > 0 Then
                Return "MANUAL APPROVE"
            ElseIf ResultCalc > _lCuttOffScore And WarningCount() > 0 Then
                Return "AUTO APPROVE"
            ElseIf ResultCalc > _lCuttOffScore Then
                Return "AUTO APPROVE"
            Else
                Return "MANUAL APPROVE"
            End If
        End Get
    End Property


    Public ApplicationStep As String
    Public CSResult_Temp As String

End Class
