
<Serializable()> _
Public Class Sales : Inherits Common

    Private _AOSupervisor As String
    Private _AO As String
    Private _AssetStatus As String
    Private _sdate As Date
    Private _edate As Date
    Private _listReport As DataSet
    Private _ListData As DataTable
    Private _TotalODAccount As Integer
    Private _BalanceOD As Double
    Private _Bucket1 As String
    Private _Bucket2 As String
    Private _Bucket3 As String
    Private _Bucket4 As String
    Private _Bucket5 As String
    Private _Bucket6 As String
    Private _Bucket7 As String
    Private _Bucket8 As String
    Private _Bucket9 As String
    Private _Bucket10 As String
    Private _accountbucket1 As Integer
    Private _accountbucket2 As Integer
    Private _accountbucket3 As Integer
    Private _accountbucket4 As Integer
    Private _accountbucket5 As Integer
    Private _accountbucket6 As Integer
    Private _accountbucket7 As Integer
    Private _accountbucket8 As Integer
    Private _accountbucket9 As Integer
    Private _accountbucket10 As Integer
    Private _amountbucket1 As Double
    Private _amountbucket2 As Double
    Private _amountbucket3 As Double
    Private _amountbucket4 As Double
    Private _amountbucket5 As Double
    Private _amountbucket6 As Double
    Private _amountbucket7 As Double
    Private _amountbucket8 As Double
    Private _amountbucket9 As Double
    Private _amountbucket10 As Double
    Private _dataSet As DataSet
    Public Property Dataset() As DataSet
        Get
            Return _dataSet
        End Get
        Set(ByVal Value As DataSet)
            _dataSet = Value
        End Set
    End Property
    Public Property AOSupervisor() As String
        Get
            Return _AOSupervisor
        End Get
        Set(ByVal Value As String)
            _AOSupervisor = Value
        End Set
    End Property
    Public Property AO() As String
        Get
            Return _AO
        End Get
        Set(ByVal Value As String)
            _AO = Value
        End Set
    End Property

    Public Property AssetStatus() As String
        Get
            Return _AssetStatus
        End Get
        Set(ByVal Value As String)
            _AssetStatus = Value
        End Set
    End Property

    Public Property sdate() As Date
        Get
            Return _sdate
        End Get
        Set(ByVal Value As Date)
            _sdate = Value
        End Set
    End Property

    Public Property edate() As Date
        Get
            Return _edate
        End Get
        Set(ByVal Value As Date)
            _edate = Value
        End Set
    End Property
    Public Property ListReport() As DataSet
        Get
            Return _listReport
        End Get
        Set(ByVal Value As DataSet)
            _listReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal Value As DataTable)
            _ListData = Value
        End Set
    End Property
    Public Property TotalODAccount() As Integer
        Get
            Return _TotalODAccount
        End Get
        Set(ByVal Value As Integer)
            _TotalODAccount = Value
        End Set
    End Property
    Public Property BalanceOD() As Double
        Get
            Return _BalanceOD
        End Get
        Set(ByVal Value As Double)
            _BalanceOD = Value
        End Set
    End Property
    Public Property Bucket1() As String
        Get
            Return _Bucket1
        End Get
        Set(ByVal Value As String)
            _Bucket1 = Value
        End Set
    End Property
    Public Property Bucket2() As String
        Get
            Return _Bucket2
        End Get
        Set(ByVal Value As String)
            _Bucket2 = Value
        End Set
    End Property
    Public Property Bucket3() As String
        Get
            Return _Bucket3
        End Get
        Set(ByVal Value As String)
            _Bucket3 = Value
        End Set
    End Property
    Public Property Bucket4() As String
        Get
            Return _Bucket4
        End Get
        Set(ByVal Value As String)
            _Bucket4 = Value
        End Set
    End Property
    Public Property Bucket5() As String
        Get
            Return _Bucket5
        End Get
        Set(ByVal Value As String)
            _Bucket5 = Value
        End Set
    End Property
    Public Property Bucket6() As String
        Get
            Return _Bucket6
        End Get
        Set(ByVal Value As String)
            _Bucket6 = Value
        End Set
    End Property
    Public Property Bucket7() As String
        Get
            Return _Bucket7
        End Get
        Set(ByVal Value As String)
            _Bucket7 = Value
        End Set
    End Property
    Public Property Bucket8() As String
        Get
            Return _Bucket8
        End Get
        Set(ByVal Value As String)
            _Bucket8 = Value
        End Set
    End Property
    Public Property Bucket9() As String
        Get
            Return _Bucket9
        End Get
        Set(ByVal Value As String)
            _Bucket9 = Value
        End Set
    End Property
    Public Property Bucket10() As String
        Get
            Return _Bucket10
        End Get
        Set(ByVal Value As String)
            _Bucket10 = Value
        End Set
    End Property
    Public Property accountbucket1() As Integer
        Get
            Return _accountbucket1
        End Get
        Set(ByVal Value As Integer)
            _accountbucket1 = Value
        End Set
    End Property
    Public Property accountbucket2() As Integer
        Get
            Return _accountbucket2
        End Get
        Set(ByVal Value As Integer)
            _accountbucket2 = Value
        End Set
    End Property
    Public Property accountbucket3() As Integer
        Get
            Return _accountbucket3
        End Get
        Set(ByVal Value As Integer)
            _accountbucket3 = Value
        End Set
    End Property
    Public Property accountbucket4() As Integer
        Get
            Return _accountbucket4
        End Get
        Set(ByVal Value As Integer)
            _accountbucket4 = Value
        End Set
    End Property
    Public Property accountbucket5() As Integer
        Get
            Return _accountbucket5
        End Get
        Set(ByVal Value As Integer)
            _accountbucket5 = Value
        End Set
    End Property
    Public Property accountbucket6() As Integer
        Get
            Return _accountbucket6
        End Get
        Set(ByVal Value As Integer)
            _accountbucket6 = Value
        End Set
    End Property
    Public Property accountbucket7() As Integer
        Get
            Return _accountbucket7
        End Get
        Set(ByVal Value As Integer)
            _accountbucket7 = Value
        End Set
    End Property
    Public Property accountbucket8() As Integer
        Get
            Return _accountbucket8
        End Get
        Set(ByVal Value As Integer)
            _accountbucket8 = Value
        End Set
    End Property
    Public Property accountbucket9() As Integer
        Get
            Return _accountbucket9
        End Get
        Set(ByVal Value As Integer)
            _accountbucket9 = Value
        End Set
    End Property
    Public Property accountbucket10() As Integer
        Get
            Return _accountbucket10
        End Get
        Set(ByVal Value As Integer)
            _accountbucket10 = Value
        End Set
    End Property
    Public Property amountbucket1() As Double
        Get
            Return _amountbucket1
        End Get
        Set(ByVal Value As Double)
            _amountbucket1 = Value
        End Set
    End Property
    Public Property amountbucket2() As Double
        Get
            Return _amountbucket2
        End Get
        Set(ByVal Value As Double)
            _amountbucket2 = Value
        End Set
    End Property
    Public Property amountbucket3() As Double
        Get
            Return _amountbucket3
        End Get
        Set(ByVal Value As Double)
            _amountbucket3 = Value
        End Set
    End Property
    Public Property amountbucket4() As Double
        Get
            Return _amountbucket4
        End Get
        Set(ByVal Value As Double)
            _amountbucket4 = Value
        End Set
    End Property
    Public Property amountbucket5() As Double
        Get
            Return _amountbucket5
        End Get
        Set(ByVal Value As Double)
            _amountbucket5 = Value
        End Set
    End Property
    Public Property amountbucket6() As Double
        Get
            Return _amountbucket6
        End Get
        Set(ByVal Value As Double)
            _amountbucket6 = Value
        End Set
    End Property
    Public Property amountbucket7() As Double
        Get
            Return _amountbucket7
        End Get
        Set(ByVal Value As Double)
            _amountbucket7 = Value
        End Set
    End Property
    Public Property amountbucket8() As Double
        Get
            Return _amountbucket8
        End Get
        Set(ByVal Value As Double)
            _amountbucket8 = Value
        End Set
    End Property
    Public Property amountbucket9() As Double
        Get
            Return _amountbucket9
        End Get
        Set(ByVal Value As Double)
            _amountbucket9 = Value
        End Set
    End Property
    Public Property amountbucket10() As Double
        Get
            Return _amountbucket10
        End Get
        Set(ByVal Value As Double)
            _amountbucket10 = Value
        End Set
    End Property


End Class
