
<Serializable()> _
Public Class StatementOfAO : Inherits Common
    Private _year As Integer
    Private _target As Integer
    Private _ListData As DataTable
    Private _ListData1 As DataTable
    Private _cmdwhere As String
    Private _branchmanagername As String
    Private _productiviy As Integer
    Private _branchstatus As String
    Private _ao As Integer
    Private _supervisor As Integer
    Private _admin As Integer
    Private _collection As Integer
    Public Property BranchManagerName() As String
        Get
            Return _branchmanagername
        End Get
        Set(ByVal Value As String)
            _branchmanagername = Value
        End Set
    End Property
    Public Property Collection() As Integer
        Get
            Return _collection
        End Get
        Set(ByVal Value As Integer)
            _collection = Value
        End Set
    End Property
    Public Property Productivity() As Integer
        Get
            Return _productiviy
        End Get
        Set(ByVal Value As Integer)
            _productiviy = Value
        End Set
    End Property
    Public Property BranchStatus() As String
        Get
            Return _branchstatus
        End Get
        Set(ByVal Value As String)
            _branchstatus = Value
        End Set
    End Property
    Public Property AO() As Integer
        Get
            Return _ao
        End Get
        Set(ByVal Value As Integer)
            _ao = Value
        End Set
    End Property
    Public Property Supervisor() As Integer
        Get
            Return _supervisor
        End Get
        Set(ByVal Value As Integer)
            _supervisor = Value
        End Set
    End Property
    Public Property Admin() As Integer
        Get
            Return _admin
        End Get
        Set(ByVal Value As Integer)
            _admin = Value
        End Set
    End Property
    Public Property ListData1() As DataTable
        Get
            Return _ListData1
        End Get
        Set(ByVal Value As DataTable)
            _ListData1 = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal Value As DataTable)
            _ListData = Value
        End Set
    End Property
    Public Property CmdWhere() As String
        Get
            Return _cmdwhere
        End Get
        Set(ByVal Value As String)
            _cmdwhere = Value
        End Set
    End Property
    Public Property Year() As Integer
        Get
            Return _year
        End Get
        Set(ByVal Value As Integer)
            _year = Value
        End Set
    End Property
    Public Property Target() As Integer
        Get
            Return _target
        End Get
        Set(ByVal Value As Integer)
            _target = Value
        End Set
    End Property
End Class
