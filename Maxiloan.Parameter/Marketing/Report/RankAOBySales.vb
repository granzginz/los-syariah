
<Serializable()> _
Public Class RankAOBySales : Inherits Common
    Private _ListData As DataTable
    Private _cmdwhere As String
    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal Value As DataTable)
            _ListData = Value
        End Set
    End Property
    Public Property CmdWhere() As String
        Get
            Return _cmdwhere
        End Get
        Set(ByVal Value As String)
            _cmdwhere = Value
        End Set
    End Property

End Class
