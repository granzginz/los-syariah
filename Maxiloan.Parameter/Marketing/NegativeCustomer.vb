
<Serializable()> _
Public Class NegativeCustomer : Inherits Common
    Private _listNegative As DataTable
    Private _customername As String
    Private _idtype As String
    Private _idnumber As String
    Private _birthplace As String
    Private _birthdate As String
    Private _address As String
    Private _rt As String
    Private _rw As String
    Private _kelurahan As String
    Private _kecamatan As String
    Private _city As String
    Private _zipcode As String
    Private _legalareaphone1 As String
    Private _legalphone1 As String
    Private _legalareaphone2 As String
    Private _legalphone2 As String
    Private _legalareafax As String
    Private _legalfax As String
    Private _mobilephone As String
    Private _email As String
    Private _badtype As String
    Private _datasource As String
    Private _notes As String
    Private _isactive As Boolean
    Private _hasil As Integer
    Private _negativecustomerid As String
    Private _strkey As String
    Private _listReport As DataSet
    Private _params As String
    Private _Approval As Approval


    Public Property params() As String
        Get
            Return _params
        End Get
        Set(ByVal Value As String)
            _params = Value
        End Set
    End Property

    Public Property ListReport() As DataSet
        Get
            Return _listReport
        End Get
        Set(ByVal Value As DataSet)
            _listReport = Value
        End Set
    End Property

    Public Property strKey() As String
        Get
            Return _strkey
        End Get
        Set(ByVal Value As String)
            _strkey = Value
        End Set
    End Property

    Public Property NegativeCustomerID() As String
        Get
            Return _negativecustomerid
        End Get
        Set(ByVal Value As String)
            _negativecustomerid = Value
        End Set
    End Property

    Public Property Hasil() As Integer
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Integer)
            _hasil = Value
        End Set
    End Property
    Public Property IDType() As String
        Get
            Return _idtype
        End Get
        Set(ByVal Value As String)
            _idtype = Value
        End Set
    End Property

    Public Property IDNumber() As String
        Get
            Return _idnumber
        End Get
        Set(ByVal Value As String)
            _idnumber = Value
        End Set
    End Property

    Public Property BirthPlace() As String
        Get
            Return _birthplace
        End Get
        Set(ByVal Value As String)
            _birthplace = Value
        End Set
    End Property

    Public Property BirthDate() As String
        Get
            Return _birthdate
        End Get
        Set(ByVal Value As String)
            _birthdate = Value
        End Set
    End Property

    Public Property Address() As String
        Get
            Return _address
        End Get
        Set(ByVal Value As String)
            _address = Value
        End Set
    End Property

    Public Property RT() As String
        Get
            Return _rt
        End Get
        Set(ByVal Value As String)
            _rt = Value
        End Set
    End Property

    Public Property RW() As String
        Get
            Return _rw
        End Get
        Set(ByVal Value As String)
            _rw = Value
        End Set
    End Property

    Public Property Kelurahan() As String
        Get
            Return _kelurahan
        End Get
        Set(ByVal Value As String)
            _kelurahan = Value
        End Set
    End Property

    Public Property Kecamatan() As String
        Get
            Return _kecamatan
        End Get
        Set(ByVal Value As String)
            _kecamatan = Value
        End Set
    End Property

    Public Property City() As String
        Get
            Return _city
        End Get
        Set(ByVal Value As String)
            _city = Value
        End Set
    End Property

    Public Property ZipCode() As String
        Get
            Return _zipcode
        End Get
        Set(ByVal Value As String)
            _zipcode = Value
        End Set
    End Property

    Public Property LegalAreaPhone1() As String
        Get
            Return _legalareaphone1
        End Get
        Set(ByVal Value As String)
            _legalareaphone1 = Value
        End Set
    End Property
    Public Property LegalPhone1() As String
        Get
            Return _legalphone1
        End Get
        Set(ByVal Value As String)
            _legalphone1 = Value
        End Set
    End Property

    Public Property LegalAreaPhone2() As String
        Get
            Return _legalareaphone2
        End Get
        Set(ByVal Value As String)
            _legalareaphone2 = Value
        End Set
    End Property

    Public Property LegalPhone2() As String
        Get
            Return _legalphone2
        End Get
        Set(ByVal Value As String)
            _legalphone2 = Value
        End Set
    End Property

    Public Property LegalAreaFax() As String
        Get
            Return _legalareafax
        End Get
        Set(ByVal Value As String)
            _legalareafax = Value
        End Set
    End Property

    Public Property LegalFax() As String
        Get
            Return _legalfax
        End Get
        Set(ByVal Value As String)
            _legalfax = Value
        End Set
    End Property

    Public Property MobilePhone() As String
        Get
            Return _mobilephone
        End Get
        Set(ByVal Value As String)
            _mobilephone = Value
        End Set
    End Property

    Public Property Email() As String
        Get
            Return _email
        End Get
        Set(ByVal Value As String)
            _email = Value
        End Set
    End Property

    Public Property BadType() As String
        Get
            Return _badtype
        End Get
        Set(ByVal Value As String)
            _badtype = Value
        End Set
    End Property

    Public Property DataSource() As String
        Get
            Return _datasource
        End Get
        Set(ByVal Value As String)
            _datasource = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property

    Public Property IsActive() As Boolean
        Get
            Return _isactive
        End Get
        Set(ByVal Value As Boolean)
            _isactive = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return _customername
        End Get
        Set(ByVal Value As String)
            _customername = Value
        End Set
    End Property

    Public Property ListNegative() As DataTable
        Get
            Return _listNegative
        End Get
        Set(ByVal Value As DataTable)
            _listNegative = Value
        End Set
    End Property

    Public Property Approval() As Approval
        Get
            Return _Approval
        End Get
        Set(ByVal Value As Approval)
            _Approval = Value
        End Set
    End Property
End Class
