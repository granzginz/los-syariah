
<Serializable()> _
Public Class AOIndirectSales : Inherits Maxiloan.Parameter.Common
    Private _listdata As DataTable
    Private _strApplicationID As String
    Private _criteria As String
    Private _InsSequenceNo As Integer
    Private _assetstatus As String
    Private _assetseqno As Integer
    Private _year As Integer
    Private _detail As DataTable
    Private _month As Integer
    Private _employeeid As String
    Private _unit As Integer
    Private _amount As Decimal
    Public Property Detail() As DataTable
        Get
            Return _detail
        End Get
        Set(ByVal Value As DataTable)
            _detail = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property AssetSequenceNo() As Integer
        Get
            Return _assetseqno
        End Get
        Set(ByVal Value As Integer)
            _assetseqno = Value
        End Set
    End Property
    Public Property Year() As Integer
        Get
            Return _year
        End Get
        Set(ByVal Value As Integer)
            _year = Value
        End Set
    End Property
    Public Property Unit() As Integer
        Get
            Return _unit
        End Get
        Set(ByVal Value As Integer)
            _unit = Value
        End Set
    End Property
    Public Property InsSequenceNo() As Integer
        Get
            Return _InsSequenceNo
        End Get
        Set(ByVal Value As Integer)
            _InsSequenceNo = Value
        End Set
    End Property
    Public Property Criteria() As String
        Get
            Return _criteria
        End Get
        Set(ByVal Value As String)
            _criteria = Value
        End Set
    End Property
    Public Property EmployeeID() As String
        Get
            Return _employeeid
        End Get
        Set(ByVal Value As String)
            _employeeid = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return _strApplicationID
        End Get
        Set(ByVal Value As String)
            _strApplicationID = Value
        End Set
    End Property
    Public Property AssetStatus() As String
        Get
            Return _assetstatus
        End Get
        Set(ByVal Value As String)
            _assetstatus = Value
        End Set
    End Property
    Public Property Month() As Integer
        Get
            Return _month
        End Get
        Set(ByVal Value As Integer)
            _month = Value
        End Set
    End Property
    Public Property Amount() As Decimal
        Get
            Return _amount
        End Get
        Set(ByVal Value As Decimal)
            _amount = Value
        End Set
    End Property


End Class
