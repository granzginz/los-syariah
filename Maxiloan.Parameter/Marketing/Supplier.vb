
<Serializable()> _
Public Class Supplier : Inherits Common
#Region "Const"
    Private _IsActive As Boolean
    Private _listdata As DataTable
    Private _listdata2 As DataTable
    Private _totalrecords As Int64
    Private _CPEmail As String
    Private _CPHP As String
    Private _CPJobTitle As String
    Private _CPName As String
    Private _IsAutomotive As String
    Private _IsUrusBPKB As String
    Private _NPWP As String
    Private _isPerseorangan As Boolean
    Private _SupplierID As String
    Private _SIUP As String
    Private _SupplierAssetStatus As String
    Private _SupplierSaleStatus As String
    Private _SupplierBadStatus As String
    Private _Category As String
    Private _SupplierGroupID As String
    Private _SupplierInitialName As String
    Private _SupplierName As String
    Private _SupplierShortName As String
    Private _SupplierLevelStatus As String
    Private _SupplierStartDate As String
    Private _TDP As String
    Private _Output As String
    Private _WhereCond2 As String
    Private _AOID As String
    Private _NewVehicleIC As String
    Private _UsedVehicleIC As String
    Private _ROIC As String
    Private _IncentiveDistributionMethod As String
    Private _CompanyPercentage As Decimal
    Private _GMPercentage As Decimal
    Private _BMPercentage As Decimal
    Private _ADHPercentage As Decimal
    Private _SPVPercentage As Decimal
    Private _SPPercentage As Decimal
    Private _ADPercentage As Decimal
    Private _GMAmount As Decimal
    Private _BMAmount As Decimal
    Private _ADHAmount As Decimal
    Private _SPVAmount As Decimal
    Private _SPAmount As Decimal
    Private _ADAmount As Decimal
    Private _EmployeeUnitWayICPayment As String
    Private _EmployeeAFWayICPayment As String
    Private _EmployeeRAWayICPayment As String
    Private _CompanyUnitWayICPayment As String
    Private _CompanyAFWayICPayment As String
    Private _CompanyRAWayICPayment As String
    Private _ID As String
    Private _PKS As String
    Private _Name As String
    Private _Title As String
    Private _BirthPlace As String
    Private _BirthDate As String
    Private _Email As String
    Private _HP As String
    Private _IDType As String
    Private _IDNumber As String
    Private _AddEdit As String
    Private _BankBranch As String
    Private _AccountNo As String
    Private _AccountName As String
    Private _BankID As String
    Private _SignatureURL As String
    Private _JobTitle As String
    Private _SequenceNo As Int16
    Private _EmployeeName As String
    Private _EmployeePosition As String
    Private _Err As String
    Private _BudgetMonth As Integer
    Private _BudgetYear As Integer
    Private _Status As String
    Private _Unit As Integer
    Private _amount As Double
    Private _strError As String
    Private _ForecastMonth As Integer
    Private _ForecastYear As Integer
    Private _CardIDNew As Integer
    Private _CardIDUsed As Integer
    Private _CardIDRO As Integer
    Private _year As Integer
    Private _listdata3 As DataTable
    Private _NewAmountFee As Double
    Private _UsedAmountFee As Double
    Private _BankBranchId As String
    Private _TransID As String
    Private _TransIDNew As String
    'Private _BranchID As String
    Private _CMO As String
    Private _AlokasiType As String
    Private _SupplierEmployeePositionId As String
    Private _Persentase As Double
    Private _NilaiPersentase As Double
    Private _NilaiAmount As Double
    Private _RefundInterestPercent As Double
    Private _RefundInterestAmount As Double
    Private _RefundPremiPercent As Double
    Private _RefundPremiAmount As Double
    Private _RefundAdminPercent As Double
    Private _RefundAdminAmount As Double
    Private _RefundProvisiPercent As Double
    Private _RefundProvisiAmount As Double
    Private _SupervisorID As String
    Private _SupplierEmployeeID As String
    Private _Produk As String
    Private _ListNegative As DataTable
    Private _Jabatan As String
    Private _AssetTypeID As String
    Private _GetSupplierID As String
    Private _NoPKS As String
    Private _GetNoPKS As String
    Private _RefundBungaBy As String
    Private _RefundBungaMultiplier As String
    Private _RefundPremiBy As String
    Private _RefundPercentageAmount As String
    Private _RefundInterestAlokasi As String
    Private _RefundPremiAlokasi As String
    Private _RewardUnit As Double
    Private _BukBonus As Double
    Private _SKBP As Boolean
    Private _AlamatNPWP As String

#End Region
#Region "SupplierBranchRefund"
    'Public Property BranchID() As String
    '    Get
    '        Return _BranchID
    '    End Get
    '    Set(ByVal Value As String)
    '        _BranchID = Value
    '    End Set
    'End Property

    Public Property RewardUnit() As String
        Get
            Return _RewardUnit
        End Get
        Set(ByVal Value As String)
            _RewardUnit = Value
        End Set
    End Property
    Public Property BukBonus() As String
        Get
            Return _BukBonus
        End Get
        Set(ByVal Value As String)
            _BukBonus = Value
        End Set
    End Property
    Public Property RefundPremiAlokasi() As String

        Get
            Return _RefundPremiAlokasi
        End Get
        Set(ByVal Value As String)
            _RefundPremiAlokasi = Value
        End Set
    End Property
    Public Property RefundInterestAlokasi() As String

        Get
            Return _RefundInterestAlokasi
        End Get
        Set(ByVal Value As String)
            _RefundInterestAlokasi = Value
        End Set
    End Property

    Public Property RefundPercentageAmount() As String
        Get
            Return _RefundPercentageAmount
        End Get
        Set(ByVal Value As String)
            _RefundPercentageAmount = Value
        End Set
    End Property

    Public Property RefundBungaBy() As String
        Get
            Return _RefundBungaBy
        End Get
        Set(ByVal Value As String)
            _RefundBungaBy = Value
        End Set
    End Property
    Public Property RefundBungaMultiplier() As String
        Get
            Return _RefundBungaMultiplier
        End Get
        Set(ByVal Value As String)
            _RefundBungaMultiplier = Value
        End Set
    End Property
    Public Property RefundPremiBy() As String
        Get
            Return _RefundPremiBy
        End Get
        Set(ByVal Value As String)
            _RefundPremiBy = Value
        End Set
    End Property

    Public Property CMO() As String
        Get
            Return _CMO
        End Get
        Set(ByVal Value As String)
            _CMO = Value
        End Set
    End Property

    Public Property AlokasiType() As String
        Get
            Return _AlokasiType
        End Get
        Set(ByVal Value As String)
            _AlokasiType = Value
        End Set
    End Property

    Public Property SupplierEmployeePositionId() As String
        Get
            Return _SupplierEmployeePositionId
        End Get
        Set(ByVal Value As String)
            _SupplierEmployeePositionId = Value
        End Set
    End Property

    Public Property Persentase() As Double
        Get
            Return _Persentase
        End Get
        Set(ByVal Value As Double)
            _Persentase = Value
        End Set
    End Property

    Public Property RefundInterestPercent() As Integer
        Get
            Return _RefundInterestPercent
        End Get
        Set(ByVal Value As Integer)
            _RefundInterestPercent = Value
        End Set
    End Property

    Public Property RefundInterestAmount() As Decimal
        Get
            Return _RefundInterestAmount
        End Get
        Set(ByVal Value As Decimal)
            _RefundInterestAmount = Value
        End Set
    End Property

    Public Property RefundPremiPercent() As Integer
        Get
            Return _RefundPremiPercent
        End Get
        Set(ByVal Value As Integer)
            _RefundPremiPercent = Value
        End Set
    End Property

    Public Property RefundPremiAmount() As Decimal
        Get
            Return _RefundPremiAmount
        End Get
        Set(ByVal Value As Decimal)
            _RefundPremiAmount = Value
        End Set
    End Property

    Public Property RefundAdminPercent() As Integer
        Get
            Return _RefundAdminPercent
        End Get
        Set(ByVal Value As Integer)
            _RefundAdminPercent = Value
        End Set
    End Property

    Public Property RefundAdminAmount() As Decimal
        Get
            Return _RefundAdminAmount
        End Get
        Set(ByVal Value As Decimal)
            _RefundAdminAmount = Value
        End Set
    End Property

    Public Property RefundProvisiPercent() As Integer
        Get
            Return _RefundProvisiPercent
        End Get
        Set(ByVal Value As Integer)
            _RefundProvisiPercent = Value
        End Set
    End Property

    Public Property RefundProvisiAmount() As Decimal
        Get
            Return _RefundProvisiAmount
        End Get
        Set(ByVal Value As Decimal)
            _RefundProvisiAmount = Value
        End Set
    End Property

    Public Property NilaiPersentase() As Double
        Get
            Return _NilaiPersentase
        End Get
        Set(ByVal Value As Double)
            _NilaiPersentase = Value
        End Set
    End Property

    Public Property NilaiAmount() As Double
        Get
            Return _NilaiAmount
        End Get
        Set(ByVal Value As Double)
            _NilaiAmount = Value
        End Set
    End Property
#End Region
#Region "General"
    Public Property SupplierEmployeeID() As String
        Get
            Return _SupplierEmployeeID
        End Get
        Set(ByVal Value As String)
            _SupplierEmployeeID = Value
        End Set
    End Property

    Public Property SupervisorID() As String
        Get
            Return _SupervisorID
        End Get
        Set(ByVal Value As String)
            _SupervisorID = Value
        End Set
    End Property

    Public Property TransID() As String
        Get
            Return _TransID
        End Get
        Set(ByVal Value As String)
            _TransID = Value
        End Set
    End Property
    Public Property TransIDNew() As String
        Get
            Return _TransIDNew
        End Get
        Set(ByVal Value As String)
            _TransIDNew = Value
        End Set
    End Property

    Public Property IsActive() As Boolean
        Get
            Return _IsActive
        End Get
        Set(ByVal Value As Boolean)
            _IsActive = Value
        End Set
    End Property
    Public Property WhereCond2() As String
        Get
            Return _WhereCond2
        End Get
        Set(ByVal Value As String)
            _WhereCond2 = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property ListData2() As DataTable
        Get
            Return _listdata2
        End Get
        Set(ByVal Value As DataTable)
            _listdata2 = Value
        End Set
    End Property
    Public Property ListData3() As DataTable
        Get
            Return _listdata3
        End Get
        Set(ByVal Value As DataTable)
            _listdata3 = Value
        End Set
    End Property
    Public Property ListNegative() As DataTable
        Get
            Return _ListNegative
        End Get
        Set(ByVal Value As DataTable)
            _ListNegative = Value
        End Set
    End Property
    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property NewAmountFee() As Double
        Get
            Return _NewAmountFee
        End Get
        Set(ByVal Value As Double)
            _NewAmountFee = Value
        End Set
    End Property

    Public Property UsedAmountFee() As Double
        Get
            Return _UsedAmountFee
        End Get
        Set(ByVal Value As Double)
            _UsedAmountFee = Value
        End Set
    End Property

    Public Property PKSDate As Date
    Public Property NilaiAlokasi As Double
    Public Property PersenAlokasi As Double
    Public Property JumlahPlafnd As Decimal
    Public Property DepsitJaminan As Decimal
    Public Property PenerapanTVC As Boolean
    Public Property PF As Boolean
    Public Property NoKTP As String
    Public Property NoNPWP As String    

#End Region
#Region "Supplier"
    Public Property CPEmail() As String
        Get
            Return _CPEmail
        End Get
        Set(ByVal Value As String)
            _CPEmail = Value
        End Set
    End Property
    Public Property CPHP() As String
        Get
            Return _CPHP
        End Get
        Set(ByVal Value As String)
            _CPHP = Value
        End Set
    End Property
    Public Property CPJobTitle() As String
        Get
            Return _CPJobTitle
        End Get
        Set(ByVal Value As String)
            _CPJobTitle = Value
        End Set
    End Property
    Public Property CPName() As String
        Get
            Return _CPName
        End Get
        Set(ByVal Value As String)
            _CPName = Value
        End Set
    End Property
    Public Property IsAutomotive() As String
        Get
            Return _IsAutomotive
        End Get
        Set(ByVal Value As String)
            _IsAutomotive = Value
        End Set
    End Property
    Public Property IsUrusBPKB() As String
        Get
            Return _IsUrusBPKB
        End Get
        Set(ByVal Value As String)
            _IsUrusBPKB = Value
        End Set
    End Property
    Public Property NPWP() As String
        Get
            Return _NPWP
        End Get
        Set(ByVal Value As String)
            _NPWP = Value
        End Set
    End Property
    Public Property isPerseorangan() As Boolean
        Get
            Return _isPerseorangan
        End Get
        Set(value As Boolean)
            _isPerseorangan = value
        End Set
    End Property
    Public Property SupplierID() As String
        Get
            Return _SupplierID
        End Get
        Set(ByVal Value As String)
            _SupplierID = Value
        End Set
    End Property
    Public Property NoPKS() As String
        Get
            Return _NoPKS
        End Get
        Set(ByVal Value As String)
            _NoPKS = Value
        End Set
    End Property
    Public Property SIUP() As String
        Get
            Return _SIUP
        End Get
        Set(ByVal Value As String)
            _SIUP = Value
        End Set
    End Property
    Public Property Produk() As String
        Get
            Return _Produk
        End Get
        Set(ByVal Value As String)
            _Produk = Value
        End Set
    End Property
    Public Property TDP() As String
        Get
            Return _TDP
        End Get
        Set(ByVal Value As String)
            _TDP = Value
        End Set
    End Property
    Public Property SupplierAssetStatus() As String
        Get
            Return _SupplierAssetStatus
        End Get
        Set(ByVal Value As String)
            _SupplierAssetStatus = Value
        End Set
    End Property
    Public Property SupplierSaleStatus() As String
        Get
            Return _SupplierSaleStatus
        End Get
        Set(ByVal Value As String)
            _SupplierSaleStatus = Value
        End Set
    End Property
    Public Property SupplierBadStatus() As String
        Get
            Return _SupplierBadStatus
        End Get
        Set(ByVal Value As String)
            _SupplierBadStatus = Value
        End Set
    End Property
    Public Property SupplierCategory() As String
        Get
            Return _Category
        End Get
        Set(ByVal Value As String)
            _Category = Value
        End Set
    End Property
    Public Property SupplierGroupID() As String
        Get
            Return _SupplierGroupID
        End Get
        Set(ByVal Value As String)
            _SupplierGroupID = Value
        End Set
    End Property
    Public Property SupplierInitialName() As String
        Get
            Return _SupplierInitialName
        End Get
        Set(ByVal Value As String)
            _SupplierInitialName = Value
        End Set
    End Property
    Public Property SupplierName() As String
        Get
            Return _SupplierName
        End Get
        Set(ByVal Value As String)
            _SupplierName = Value
        End Set
    End Property
    Public Property SupplierShortName() As String
        Get
            Return _SupplierShortName
        End Get
        Set(ByVal Value As String)
            _SupplierShortName = Value
        End Set
    End Property
    Public Property SupplierLevelStatus() As String
        Get
            Return _SupplierLevelStatus
        End Get
        Set(ByVal Value As String)
            _SupplierLevelStatus = Value
        End Set
    End Property
    Public Property SupplierStartDate() As String
        Get
            Return _SupplierStartDate
        End Get
        Set(ByVal Value As String)
            _SupplierStartDate = Value
        End Set
    End Property
    Public Property Output() As String
        Get
            Return _Output
        End Get
        Set(ByVal Value As String)
            _Output = Value
        End Set
    End Property
    Public Property CardIDNew() As Integer
        Get
            Return _CardIDNew
        End Get
        Set(ByVal Value As Integer)
            _CardIDNew = Value
        End Set
    End Property
    Public Property CardIDUsed() As Integer
        Get
            Return _CardIDUsed
        End Get
        Set(ByVal Value As Integer)
            _CardIDUsed = Value
        End Set
    End Property
    Public Property CardIDRO() As Integer
        Get
            Return _CardIDRO
        End Get
        Set(ByVal Value As Integer)
            _CardIDRO = Value
        End Set
    End Property
    Public Property Jabatan() As String
        Get
            Return _Jabatan
        End Get
        Set(ByVal Value As String)
            _Jabatan = Value
        End Set
    End Property
    Public Property AssetTypeID() As String
        Get
            Return _AssetTypeID
        End Get
        Set(ByVal Value As String)
            _AssetTypeID = Value
        End Set
    End Property
    Public Property GetSupplierID() As String
        Get
            Return _GetSupplierID
        End Get
        Set(ByVal Value As String)
            _GetSupplierID = Value
        End Set
    End Property
    Public Property GetNoPKS() As String
        Get
            Return _GetNoPKS
        End Get
        Set(ByVal Value As String)
            _GetNoPKS = Value
        End Set
    End Property
    Public Property SKBP() As Boolean
        Get
            Return _SKBP
        End Get
        Set(value As Boolean)
            _SKBP = value
        End Set
    End Property

    Public Property AlamatNPWP() As String
        Get
            Return _AlamatNPWP
        End Get
        Set(ByVal Value As String)
            _AlamatNPWP = Value
        End Set
    End Property
#End Region
#Region "SupplierBranch"
    Public Property Year() As Integer
        Get
            Return _year
        End Get
        Set(ByVal Value As Integer)
            _year = Value
        End Set
    End Property
    Public Property AOID() As String
        Get
            Return _AOID
        End Get
        Set(ByVal Value As String)
            _AOID = Value
        End Set
    End Property
    Public Property NewVehicleIC() As String
        Get
            Return _NewVehicleIC
        End Get
        Set(ByVal Value As String)
            _NewVehicleIC = Value
        End Set
    End Property
    Public Property UsedVehicleIC() As String
        Get
            Return _UsedVehicleIC
        End Get
        Set(ByVal Value As String)
            _UsedVehicleIC = Value
        End Set
    End Property
    Public Property ROIC() As String
        Get
            Return _ROIC
        End Get
        Set(ByVal Value As String)
            _ROIC = Value
        End Set
    End Property
    Public Property IncentiveDistributionMethod() As String
        Get
            Return _IncentiveDistributionMethod
        End Get
        Set(ByVal Value As String)
            _IncentiveDistributionMethod = Value
        End Set
    End Property
    Public Property CompanyPercentage() As Decimal
        Get
            Return _CompanyPercentage
        End Get
        Set(ByVal Value As Decimal)
            _CompanyPercentage = Value
        End Set
    End Property
    Public Property GMPercentage() As Decimal
        Get
            Return _GMPercentage
        End Get
        Set(ByVal Value As Decimal)
            _GMPercentage = Value
        End Set
    End Property
    Public Property BMPercentage() As Decimal
        Get
            Return _BMPercentage
        End Get
        Set(ByVal Value As Decimal)
            _BMPercentage = Value
        End Set
    End Property
    Public Property ADHPercentage() As Decimal
        Get
            Return _ADHPercentage
        End Get
        Set(ByVal Value As Decimal)
            _ADHPercentage = Value
        End Set
    End Property
    Public Property SPVPercentage() As Decimal
        Get
            Return _SPVPercentage
        End Get
        Set(ByVal Value As Decimal)
            _SPVPercentage = Value
        End Set
    End Property
    Public Property SPPercentage() As Decimal
        Get
            Return _SPPercentage
        End Get
        Set(ByVal Value As Decimal)
            _SPPercentage = Value
        End Set
    End Property
    Public Property ADPercentage() As Decimal
        Get
            Return _ADPercentage
        End Get
        Set(ByVal Value As Decimal)
            _ADPercentage = Value
        End Set
    End Property
    Public Property GMAmount() As Decimal
        Get
            Return _GMAmount
        End Get
        Set(ByVal Value As Decimal)
            _GMAmount = Value
        End Set
    End Property
    Public Property BMAmount() As Decimal
        Get
            Return _BMAmount
        End Get
        Set(ByVal Value As Decimal)
            _BMAmount = Value
        End Set
    End Property
    Public Property ADHAmount() As Decimal
        Get
            Return _ADHAmount
        End Get
        Set(ByVal Value As Decimal)
            _ADHAmount = Value
        End Set
    End Property
    Public Property SPVAmount() As Decimal
        Get
            Return _SPVAmount
        End Get
        Set(ByVal Value As Decimal)
            _SPVAmount = Value
        End Set
    End Property
    Public Property SPAmount() As Decimal
        Get
            Return _SPAmount
        End Get
        Set(ByVal Value As Decimal)
            _SPAmount = Value
        End Set
    End Property
    Public Property ADAmount() As Decimal
        Get
            Return _ADAmount
        End Get
        Set(ByVal Value As Decimal)
            _ADAmount = Value
        End Set
    End Property
    Public Property EmployeeUnitWayICPayment() As String
        Get
            Return _EmployeeUnitWayICPayment
        End Get
        Set(ByVal Value As String)
            _EmployeeUnitWayICPayment = Value
        End Set
    End Property
    Public Property EmployeeAFWayICPayment() As String
        Get
            Return _EmployeeAFWayICPayment
        End Get
        Set(ByVal Value As String)
            _EmployeeAFWayICPayment = Value
        End Set
    End Property
    Public Property EmployeeRAWayICPayment() As String
        Get
            Return _EmployeeRAWayICPayment
        End Get
        Set(ByVal Value As String)
            _EmployeeRAWayICPayment = Value
        End Set
    End Property
    Public Property CompanyUnitWayICPayment() As String
        Get
            Return _CompanyUnitWayICPayment
        End Get
        Set(ByVal Value As String)
            _CompanyUnitWayICPayment = Value
        End Set
    End Property
    Public Property CompanyAFWayICPayment() As String
        Get
            Return _CompanyAFWayICPayment
        End Get
        Set(ByVal Value As String)
            _CompanyAFWayICPayment = Value
        End Set
    End Property
    Public Property CompanyRAWayICPayment() As String
        Get
            Return _CompanyRAWayICPayment
        End Get
        Set(ByVal Value As String)
            _CompanyRAWayICPayment = Value
        End Set
    End Property
#End Region
#Region "SupplierOwner"
    Public Property ID() As String
        Get
            Return _ID
        End Get
        Set(ByVal Value As String)
            _ID = Value
        End Set
    End Property
    Public Property PKS() As String
        Get
            Return _PKS
        End Get
        Set(ByVal Value As String)
            _PKS = Value
        End Set
    End Property
    Public Property Err() As String
        Get
            Return _Err
        End Get
        Set(ByVal Value As String)
            _Err = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return _Name
        End Get
        Set(ByVal Value As String)
            _Name = Value
        End Set
    End Property
    Public Property Title() As String
        Get
            Return _Title
        End Get
        Set(ByVal Value As String)
            _Title = Value
        End Set
    End Property
    Public Property BirthPlace() As String
        Get
            Return _BirthPlace
        End Get
        Set(ByVal Value As String)
            _BirthPlace = Value
        End Set
    End Property
    Public Property BirthDate() As String
        Get
            Return _BirthDate
        End Get
        Set(ByVal Value As String)
            _BirthDate = Value
        End Set
    End Property
    Public Property Email() As String
        Get
            Return _Email
        End Get
        Set(ByVal Value As String)
            _Email = Value
        End Set
    End Property
    Public Property HP() As String
        Get
            Return _HP
        End Get
        Set(ByVal Value As String)
            _HP = Value
        End Set
    End Property
    Public Property IDType() As String
        Get
            Return _IDType
        End Get
        Set(ByVal Value As String)
            _IDType = Value
        End Set
    End Property
    Public Property IDNumber() As String
        Get
            Return _IDNumber
        End Get
        Set(ByVal Value As String)
            _IDNumber = Value
        End Set
    End Property
    Public Property AddEdit() As String
        Get
            Return _AddEdit
        End Get
        Set(ByVal Value As String)
            _AddEdit = Value
        End Set
    End Property
#End Region
#Region "SupplierAccount"
    Public Property BankID() As String
        Get
            Return _BankID
        End Get
        Set(ByVal Value As String)
            _BankID = Value
        End Set
    End Property
    Public Property BankBranch() As String
        Get
            Return _BankBranch
        End Get
        Set(ByVal Value As String)
            _BankBranch = Value
        End Set
    End Property
    Public Property AccountNo() As String
        Get
            Return _AccountNo
        End Get
        Set(ByVal Value As String)
            _AccountNo = Value
        End Set
    End Property
    Public Property AccountName() As String
        Get
            Return _AccountName
        End Get
        Set(ByVal Value As String)
            _AccountName = Value
        End Set
    End Property
    Public Property BankBranchId() As String
        Get
            Return _BankBranchId
        End Get
        Set(ByVal Value As String)
            _BankBranchId = Value
        End Set
    End Property
    Public Property DefaultAccount As Boolean
    Public Property UntukBayar As String
    Public Property TanggalEfektif As Date
    Public Property BankCode As String
#End Region
#Region "SupplierSignature"
    Public Property SignatureURL() As String
        Get
            Return _SignatureURL
        End Get
        Set(ByVal Value As String)
            _SignatureURL = Value
        End Set
    End Property
    Public Property JobTitle() As String
        Get
            Return _JobTitle
        End Get
        Set(ByVal Value As String)
            _JobTitle = Value
        End Set
    End Property
    Public Property SequenceNo() As Int16
        Get
            Return _SequenceNo
        End Get
        Set(ByVal Value As Int16)
            _SequenceNo = Value
        End Set
    End Property
#End Region
#Region "SupplierAccount"
    Public Property EmployeeName() As String
        Get
            Return _EmployeeName
        End Get
        Set(ByVal Value As String)
            _EmployeeName = Value
        End Set
    End Property
    Public Property EmployeePosition() As String
        Get
            Return _EmployeePosition
        End Get
        Set(ByVal Value As String)
            _EmployeePosition = Value
        End Set
    End Property
#End Region
#Region "SupplierBudget"
    Public Property BudgetYear() As Integer
        Get
            Return _BudgetYear
        End Get
        Set(ByVal Value As Integer)
            _BudgetYear = Value
        End Set
    End Property

    Public Property BudgetMonth() As Integer
        Get
            Return _BudgetMonth
        End Get
        Set(ByVal Value As Integer)
            _BudgetMonth = Value
        End Set
    End Property

    Public Property Status() As String
        Get
            Return _Status
        End Get
        Set(ByVal Value As String)
            _Status = Value
        End Set
    End Property

    Public Property Unit() As Integer
        Get
            Return _Unit
        End Get
        Set(ByVal Value As Integer)
            _Unit = Value
        End Set
    End Property

    Public Property Amount() As Double
        Get
            Return _amount
        End Get
        Set(ByVal Value As Double)
            _amount = Value
        End Set
    End Property

    Public Property strError() As String
        Get
            Return _strError
        End Get
        Set(ByVal Value As String)
            _strError = Value
        End Set
    End Property
#End Region


#Region "SupplierForecast"
    Public Property ForecastYear() As Integer
        Get
            Return _ForecastYear
        End Get
        Set(ByVal Value As Integer)
            _ForecastYear = Value
        End Set
    End Property

    Public Property ForecastMonth() As Integer
        Get
            Return _ForecastMonth
        End Get
        Set(ByVal Value As Integer)
            _ForecastMonth = Value
        End Set
    End Property
#End Region


End Class
