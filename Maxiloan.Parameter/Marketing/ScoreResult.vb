﻿<Serializable()>
Public Class ScoreResult

    Public Sub New(_BranchID As String, _ProspectAppId As String, _ScoreComponentID As String, _ComponentContent As String, _Weight As Decimal, _ComponentValue As String,
                   _ScoreDescription As String)
        BranchID = _BranchID
        ProspectAppId = _ProspectAppId
        ScoreComponentID = _ScoreComponentID
        ComponentContent = _ComponentContent
        Weight = _Weight
        ComponentValue = _ComponentValue
        ScoreDescription = _ScoreDescription
        ScoreStatus = ""

    End Sub
    Public Property BranchID As String
    Public Property ProspectAppId As String
    Public Property ScoreComponentID As String
    Public Property ComponentContent As String
    Public Property ScoreValue As Decimal
    Public Property Weight As Decimal
    Public Property ComponentValue As String
    Public Property ScoreDescription As String
    Public Property ScoreStatus As String
    Public Property ContentSeqNo As Integer
    Public Property QueryResult As String
    Public Property ScoreAbsolute As Decimal

    Public ReadOnly Property ResultCalc As Decimal
        Get
            Return (Weight / 100) * ScoreValue
        End Get
    End Property

End Class
