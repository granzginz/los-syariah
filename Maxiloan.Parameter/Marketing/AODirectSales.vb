
<Serializable()> _
Public Class AODirectSales : Inherits Maxiloan.Parameter.Common
    Private _listdata As DataTable
    Private _strApplicationID As String
    Private _criteria As String
    Private _InsSequenceNo As Integer
    Private _assetstatus As String
    Private _assetseqno As Integer
    Private _year As Integer
    Private _detail As DataTable
    Private _month As Integer
    Private _employeeid As String
    Private _whereCond2 As String
    Private _unit As Integer
    Private _newAssetUnit As Integer
    Private _usedAssetUnit As Integer
    Private _amount As Decimal
    Private _newAssetAmount As Decimal
    Private _usedAssetAmount As Decimal

    Public Property Detail() As DataTable
        Get
            Return _detail
        End Get
        Set(ByVal Value As DataTable)
            _detail = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property AssetSequenceNo() As Integer
        Get
            Return _assetseqno
        End Get
        Set(ByVal Value As Integer)
            _assetseqno = Value
        End Set
    End Property
    Public Property Year() As Integer
        Get
            Return _year
        End Get
        Set(ByVal Value As Integer)
            _year = Value
        End Set
    End Property
    Public Property NewAssetUnit() As Integer
        Get
            Return _newAssetUnit
        End Get
        Set(ByVal Value As Integer)
            _newAssetUnit = Value
        End Set
    End Property
    Public Property UsedAssetUnit() As Integer
        Get
            Return _usedAssetUnit
        End Get
        Set(ByVal Value As Integer)
            _usedAssetUnit = Value
        End Set
    End Property
    Public Property Unit() As Integer
        Get
            Return _unit
        End Get
        Set(ByVal Value As Integer)
            _unit = Value
        End Set
    End Property
    Public Property InsSequenceNo() As Integer
        Get
            Return _InsSequenceNo
        End Get
        Set(ByVal Value As Integer)
            _InsSequenceNo = Value
        End Set
    End Property
    Public Property Criteria() As String
        Get
            Return _criteria
        End Get
        Set(ByVal Value As String)
            _criteria = Value
        End Set
    End Property
    Public Property EmployeeID() As String
        Get
            Return _employeeid
        End Get
        Set(ByVal Value As String)
            _employeeid = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return _strApplicationID
        End Get
        Set(ByVal Value As String)
            _strApplicationID = Value
        End Set
    End Property
    Public Property AssetStatus() As String
        Get
            Return _assetstatus
        End Get
        Set(ByVal Value As String)
            _assetstatus = Value
        End Set
    End Property
    Public Property Month() As Integer
        Get
            Return _month
        End Get
        Set(ByVal Value As Integer)
            _month = Value
        End Set
    End Property
    Public Property Amount() As Decimal
        Get
            Return _amount
        End Get
        Set(ByVal Value As Decimal)
            _amount = Value
        End Set
    End Property

    Public Property NewAssetAmount() As Decimal
        Get
            Return _newAssetAmount
        End Get
        Set(ByVal Value As Decimal)
            _newAssetAmount = Value
        End Set
    End Property

    Public Property UsedAssetAmount() As Decimal
        Get
            Return _usedAssetAmount
        End Get
        Set(ByVal Value As Decimal)
            _usedAssetAmount = Value
        End Set
    End Property

    Public Property WhereCond2() As String
        Get
            Return _whereCond2
        End Get
        Set(ByVal Value As String)
            _whereCond2 = Value
        End Set
    End Property

End Class
