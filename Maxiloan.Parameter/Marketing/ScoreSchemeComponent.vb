﻿<Serializable()>
Public Class ScoreSchemeComponent

    Public Sub New(_Description As String, _CuttOffScore As Decimal, _PersonalApprovedScore As Decimal, _PersonalRejectScore As Decimal, _CompanyApprovedScore As Decimal, _CompanyRejectScore As Decimal, _ScoreComponentID As String, _Weight As Decimal, _SQLCmd As String, _CalculationType As String)
        CuttOffScore = _CuttOffScore
        Description = _Description
        PersonalApprovedScore = _PersonalApprovedScore
        PersonalRejectScore = _PersonalRejectScore
        CompanyApprovedScore = _CompanyApprovedScore
        CompanyRejectScore = _CompanyRejectScore
        ScoreComponentID = _ScoreComponentID
        Weight = _Weight
        SQLCmd = _SQLCmd
        CalculationType = _CalculationType
    End Sub


    Public Property Description As String
    Public Property PersonalApprovedScore As Decimal
    Public Property PersonalRejectScore As Decimal
    Public Property CompanyApprovedScore As Decimal
    Public Property CompanyRejectScore As Decimal
    Public Property ScoreComponentID As String
    Public Property Weight As Decimal
    Public Property SQLCmd As String
    Public Property CalculationType As String
    Public Property CuttOffScore As Decimal

End Class
