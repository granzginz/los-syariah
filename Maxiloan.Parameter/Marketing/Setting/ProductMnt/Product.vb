

<Serializable()> _
Public Class Product : Inherits Common
    Private _InsuranceRateCardID As String
    Private _ProductId As String
    Private _ReferenceProductID As String
    Private _Description As String
    Private _IsActive As Boolean
    Private _IsActive_HO As Boolean
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _BBranchID As String
    Private _BBranchName As String
    Private _AssetTypeID As String
    Private _ScoreSchemeID As String
    Private _CreditScoreSchemeID As String
    Private _JournalSchemeID As String
    Private _ApprovalSchemeID As String
    Private _AssetUsedNew As String
    Private _FinanceType As String
    Private _EffectiveRate As Decimal
    Private _EffectiveRateBehaviour As String
    Private _EffectiveRate_HO As Decimal
    Private _EffectiveRateBehaviour_HO As String
    Private _GrossYieldRate As Decimal
    Private _GrossYieldRateBehaviour As String
    Private _GrossYieldRate_HO As Decimal
    Private _GrossYieldRateBehaviour_HO As String
    Private _MinimumTenor As Integer
    Private _MaximumTenor As Integer
    Private _MinimumTenor_HO As Integer
    Private _MaximumTenor_HO As Integer
    Private _PenaltyPercentage As Decimal
    Private _PenaltyPercentageBehaviour As String
    Private _PenaltyPercentage_HO As Decimal
    Private _PenaltyPercentageBehaviour_HO As String
    Private _InsurancePenaltyPercentage As Decimal
    Private _InsurancePenaltyPercentageBehaviour As String
    Private _InsurancePenaltyPercentage_HO As Decimal
    Private _InsurancePenaltyPercentageBehaviour_HO As String
    Private _CancellationFee As Decimal
    Private _CancellationFeeBehaviour As String
    Private _CancellationFee_HO As Decimal
    Private _CancellationFeeBehaviour_HO As String
    Private _AdminFee As Decimal
    Private _AdminFeeBehaviour As String
    Private _AdminFee_HO As Decimal
    Private _AdminFeeBehaviour_HO As String
    Private _FiduciaFee As Decimal
    Private _FiduciaFeeBehaviour As String
    Private _FiduciaFee_HO As Decimal
    Private _FiduciaFeeBehaviour_HO As String
    Private _ProvisionFee As Decimal
    Private _ProvisionFeeBehaviour As String
    Private _ProvisionFeeHO As Decimal
    Private _ProvisionFeeBehaviour_HO As String
    Private _NotaryFee As Decimal
    Private _NotaryFeeBehaviour As String
    Private _NotaryFee_HO As Decimal
    Private _NotaryFeeBehaviour_HO As String
    Private _SurveyFee As Decimal
    Private _SurveyFeeBehaviour As String
    Private _SurveyFee_HO As Decimal
    Private _SurveyFeeBehaviour_HO As String
    Private _VisitFee As Decimal
    Private _VisitFeeBehaviour As String
    Private _VisitFee_HO As Decimal
    Private _VisitFeeBehaviour_HO As String
    Private _ReschedulingFee As Decimal
    Private _ReschedulingFeeBehaviour As String
    Private _ReschedulingFee_HO As Decimal
    Private _ReschedulingFeeBehaviour_HO As String
    Private _AgreementTransferFee As Decimal
    Private _AgreementTransferFeeBehaviour As String
    Private _AgreementTransferFee_HO As Decimal
    Private _AgreementTransferFeeBehaviour_HO As String
    Private _ChangeDueDateFee As Decimal
    Private _ChangeDueDateFeeBehaviour As String
    Private _ChangeDueDateFee_HO As Decimal
    Private _ChangeDueDateFeeBehaviour_HO As String
    Private _AssetReplacementFee As Decimal
    Private _AssetReplacementFeeBehaviour As String
    Private _AssetReplacementFee_HO As Decimal
    Private _AssetReplacementFeeBehaviour_HO As String
    Private _RepossesFee As Decimal
    Private _RepossesFeeBehaviour As String
    Private _RepossesFee_HO As Decimal
    Private _RepossesFeeBehaviour_HO As String
    Private _LegalisirDocFee As Decimal
    Private _LegalisirDocFeeBehaviour As String
    Private _LegalisirDocFee_HO As Decimal
    Private _LegalisirDocFeeBehaviour_HO As String
    Private _PDCBounceFee As Decimal
    Private _PDCBounceFeeBehaviour As String
    Private _PDCBounceFee_HO As Decimal
    Private _PDCBounceFeeBehaviour_HO As String
    Private _InsAdminFee As Decimal
    Private _InsAdminFeeBehaviour As String
    Private _InsAdminFee_HO As Decimal
    Private _InsAdminFeeBehaviour_HO As String
    Private _InsStampDutyFee As Decimal
    Private _InsStampDutyFeeBehaviour As String
    Private _InsStampDutyFee_HO As Decimal
    Private _InsStampDutyFeeBehaviour_HO As String
    Private _DaysPOExpiration As Integer
    ''meisan
    Private _CreditProtection As Integer
    Private _CreditProtectionBehaviour As String

    Private _CreditProtection_HO As Integer
    Private _CreditProtectionBehaviour_HO As String

    Private _AsuransiKredit As Integer
    Private _AsuransiKreditBehaviour As String

    Private _AsuransiKredit_HO As Integer
    Private _AsuransiKreditBehaviour_HO As String

    Private _DaysPOExpirationBehaviour As String
    Private _DaysPOExpiration_HO As Integer
    Private _DaysPOExpirationBehaviour_HO As String
    Private _InstallmentToleranceAmount As Decimal
    Private _InstallmentToleranceAmountBehaviour As String
    Private _InstallmentToleranceAmount_HO As Decimal
    Private _InstallmentToleranceAmountBehaviour_HO As String
    Private _DPPercentage As Decimal
    Private _DPPercentageBehaviour As String
    Private _DPPercentage_HO As Decimal
    Private _DPPercentageBehaviour_HO As String
    Private _RejectMinimumIncome As Decimal
    Private _RejectMinimumIncomeBehaviour As String
    Private _RejectMinimumIncome_HO As Decimal
    Private _RejectMinimumIncomeBehaviour_HO As String
    Private _WarningMinimumIncome As Decimal
    Private _WarningMinimumIncomeBehaviour As String
    Private _WarningMinimumIncome_HO As Decimal
    Private _WarningMinimumIncomeBehaviour_HO As String
    Private _IsSPAutomatic As Boolean
    Private _IsSPAutomaticBehaviour As String
    Private _LengthSPProcess As Integer
    Private _IsSPAutomatic_HO As Boolean
    Private _IsSPAutomaticBehaviour_HO As String
    Private _LengthSPProcess_HO As Integer
    Private _IsSP1Automatic As Boolean
    Private _IsSP1AutomaticBehaviour As String
    Private _LengthSP1Process As Integer
    Private _IsSP1Automatic_HO As Boolean
    Private _IsSP1AutomaticBehaviour_HO As String
    Private _LengthSP1Process_HO As Integer
    Private _IsSP2Automatic As Boolean
    Private _IsSP2AutomaticBehaviour As String
    Private _LengthSP2Process As Integer
    Private _IsSP2Automatic_HO As Boolean
    Private _IsSP2AutomaticBehaviour_HO As String
    Private _LengthSP2Process_HO As Integer
    Private _LengthMainDocProcess As Integer
    Private _LengthMainDocProcessBehaviour As String
    Private _LengthMainDocProcess_HO As Integer
    Private _LengthMainDocProcessBehaviour_HO As String
    Private _LengthMainDocTaken As Integer
    Private _LengthMainDocTakenBehaviour As String
    Private _LengthMainDocTaken_HO As Integer
    Private _LengthMainDocTakenBehaviour_HO As String
    Private _GracePeriodLateCharges As Integer
    Private _GracePeriodLateChargesBehaviour As String
    Private _GracePeriodLateCharges_HO As Integer
    Private _GracePeriodLateChargesBehaviour_HO As String
    Private _PrepaymentPenaltyRate As Decimal
    Private _PrepaymentPenaltyRateBehaviour As String
    Private _PrepaymentPenaltyRate_HO As Decimal
    Private _PrepaymentPenaltyRateBehaviour_HO As String
    Private _DeskCollPhoneRemind As Integer
    Private _DeskCollPhoneRemind2 As Integer
    Private _DeskCollPhoneRemind3 As Integer
    Private _DeskCollPhoneRemindBehaviour As String
    Private _DeskCollPhoneRemindBehaviour2 As String
    Private _DeskCollPhoneRemindBehaviour3 As String
    Private _DeskCollPhoneRemind_HO As Integer
    Private _DeskCollPhoneRemindBehaviour_HO As String
    Private _DeskCollOD As Integer
    Private _DeskCollODBehaviour As String
    Private _DeskCollOD_HO As Integer
    Private _DeskCollODBehaviour_HO As String
    Private _PrevODToRemind As Integer
    Private _PrevODToRemindBehaviour As String
    Private _PrevODToRemind_HO As Integer
    Private _PrevODToRemindBehaviour_HO As String
    Private _PDCDayToRemind As Integer
    Private _PDCDayToRemindBehaviour As String
    Private _PDCDayToRemind_HO As Integer
    Private _PDCDayToRemindBehaviour_HO As String
    Private _DeskCollSMSRemind As Integer
    Private _DeskCollSMSRemind2 As Integer
    Private _DeskCollSMSRemind3 As Integer
    Private _DeskCollSMSRemindBehaviour As String
    Private _DeskCollSMSRemindBehaviour2 As String
    Private _DeskCollSMSRemindBehaviour3 As String
    Private _DeskCollSMSRemind_HO As Integer
    Private _DeskCollSMSRemindBehaviour_HO As String
    Private _DeskCollSMSRemindBehaviour_HO_2 As String
    Private _DeskCollSMSRemindBehaviour_HO_3 As String
    Private _DCR As Integer
    Private _DCRBehaviour As String
    Private _DCR_HO As Integer
    Private _DCRBehaviour_HO As String
    Private _DaysBeforeDueToRN As Integer
    Private _DaysBeforeDueToRNBehaviour As String
    Private _DaysBeforeDueToRN_HO As Integer
    Private _DaysBeforeDueToRNBehaviour_HO As String
    Private _ODToRAL As Integer
    Private _ODToRALBehaviour As String
    Private _ODToRAL_HO As Integer
    Private _ODToRALBehaviour_HO As String
    Private _RALPeriod As Integer
    Private _RALPeriodBehaviour As String
    Private _RALPeriod_HO As Integer
    Private _RALPeriodBehaviour_HO As String
    Private _MaxDaysRNPaid As Integer
    Private _MaxDaysRNPaidBehaviour As String
    Private _MaxDaysRNPaid_HO As Integer
    Private _MaxDaysRNPaidBehaviour_HO As String
    Private _MaxPTPYDays As Integer
    Private _MaxPTPYDaysBehaviour As String
    Private _MaxPTPYDays_HO As Integer
    Private _MaxPTPYDaysBehaviour_HO As String
    Private _PTPYBank As Integer
    Private _PTPYBankBehaviour As String
    Private _PTPYBank_HO As Integer
    Private _PTPYBankBehaviour_HO As String
    Private _PTPYCompany As Integer
    Private _PTPYCompanyBehaviour As String
    Private _PTPYCompany_HO As Integer
    Private _PTPYCompanyBehaviour_HO As String
    Private _PTPYSupplier As Integer
    Private _PerpanjanganSKT As Integer
    Private _PTPYSupplierBehaviour As String

    Private _PerpanjanganSKTBehaviour As String
    Private _PTPYSupplier_HO As Integer
    Private _PTPYSupplierBehaviour_HO As String
    Private _RALExtension As Integer
    Private _RALExtensionBehaviour As String
    Private _RALExtension_HO As Integer
    Private _RALExtensionBehaviour_HO As String
    Private _InventoryExpected As Integer
    Private _InventoryExpectedBehaviour As String
    Private _InventoryExpected_HO As Integer
    Private _InventoryExpectedBehaviour_HO As String
    Private _BillingCharges As Decimal
    Private _BillingChargesBehaviour As String
    Private _BillingCharges_HO As Decimal
    Private _BillingChargesBehaviour_HO As String
    Private _LimitAPCash As Decimal
    Private _LimitAPCashBehaviour As String
    Private _LimitAPCash_HO As Decimal
    Private _LimitAPCashBehaviour_HO As String
    Private _IsRecourse As Boolean
    Private _IsRecourse_HO As Boolean
    Private _TotalBranch As String
    Private _SaveProduct As String
    Private _AssetType_desc As String
    Private _ScoreSchemeMaster_desc As String
    Private _CreditScoreSchemeMaster_desc As String
    Private _JournalScheme_desc As String
    Private _ApprovalTypeScheme_desc As String
    Private _MasterTCID As String
    Private _PriorTo As String
    Private _IsMandatory As Boolean
    Private _Branch_ID As String
    Private _BranchFullName As String
    Private _Branch_IsActive As Boolean
    Private _ProductOfferingID As String
    Private _ProductOfferingDescription As String
    Private _ProductDescription As String
    Private _SupplierID As String
    Private _AssetCode As String
    Private _SupplierID_Name As String
    Private _AssetCode_Name As String
    Private _InstallmentAmount As Decimal
    Private _InstallmentAmountBehaviour As String
    Private _ResidualValue As Decimal
    Private _ResidualValueBehaviour As String
    Private _StartDate As Date
    Private _EndDate As Date
    Private _DeskCollSMSRemind_HO_2 As Integer
    Private _DeskCollSMSRemind_HO_3 As Integer


    Public Property InsuranceRateCardID() As String
        Get
            Return _InsuranceRateCardID
        End Get
        Set(ByVal Value As String)
            _InsuranceRateCardID = Value
        End Set
    End Property
    Public Property StartDate() As Date
        Get
            Return _StartDate
        End Get
        Set(ByVal Value As Date)
            _StartDate = Value
        End Set
    End Property

    Public Property EndDate() As Date
        Get
            Return _EndDate
        End Get
        Set(ByVal Value As Date)
            _EndDate = Value
        End Set
    End Property

    Public Property ResidualValue() As Decimal
        Get
            Return _ResidualValue
        End Get
        Set(ByVal Value As Decimal)
            _ResidualValue = Value
        End Set
    End Property

    Public Property ResidualValueBehaviour() As String
        Get
            Return _ResidualValueBehaviour
        End Get
        Set(ByVal Value As String)
            _ResidualValueBehaviour = Value
        End Set
    End Property


    Public Property InstallmentAmount() As Decimal
        Get
            Return _InstallmentAmount
        End Get
        Set(ByVal Value As Decimal)
            _InstallmentAmount = Value
        End Set
    End Property

    Public Property InstallmentAmountBehaviour() As String
        Get
            Return _InstallmentAmountBehaviour
        End Get
        Set(ByVal Value As String)
            _InstallmentAmountBehaviour = Value
        End Set
    End Property
    Public Property ProductDescription() As String
        Get
            Return _ProductDescription
        End Get
        Set(ByVal Value As String)
            _ProductDescription = Value
        End Set
    End Property
    Public Property ReferenceProductID() As String
        Get
            Return _ReferenceProductID
        End Get
        Set(ByVal Value As String)
            _ReferenceProductID = Value
        End Set
    End Property


    Public Property ProductOfferingDescription() As String
        Get
            Return _ProductOfferingDescription
        End Get
        Set(ByVal Value As String)
            _ProductOfferingDescription = Value
        End Set
    End Property

    Public Property AssetCode() As String
        Get
            Return _AssetCode
        End Get
        Set(ByVal Value As String)
            _AssetCode = Value
        End Set
    End Property

    Public Property SupplierID() As String
        Get
            Return _SupplierID
        End Get
        Set(ByVal Value As String)
            _SupplierID = Value
        End Set
    End Property

    Public Property AssetCode_Name() As String
        Get
            Return _AssetCode_Name
        End Get
        Set(ByVal Value As String)
            _AssetCode_Name = Value
        End Set
    End Property

    Public Property SupplierID_Name() As String
        Get
            Return _SupplierID_Name
        End Get
        Set(ByVal Value As String)
            _SupplierID_Name = Value
        End Set
    End Property


    Public Property ProductOfferingID() As String
        Get
            Return _ProductOfferingID
        End Get
        Set(ByVal Value As String)
            _ProductOfferingID = Value
        End Set
    End Property

    Public Property Branch_ID() As String
        Get
            Return _Branch_ID
        End Get
        Set(ByVal Value As String)
            _Branch_ID = Value
        End Set
    End Property


    Public Property BranchFullName() As String
        Get
            Return _BranchFullName
        End Get
        Set(ByVal Value As String)
            _BranchFullName = Value
        End Set
    End Property

    Public Property Branch_IsActive() As Boolean
        Get
            Return _Branch_IsActive
        End Get
        Set(ByVal Value As Boolean)
            _Branch_IsActive = Value
        End Set
    End Property


    Public Property MasterTCID() As String
        Get
            Return _MasterTCID
        End Get
        Set(ByVal Value As String)
            _MasterTCID = Value
        End Set
    End Property

    Public Property PriorTo() As String
        Get
            Return _PriorTo
        End Get
        Set(ByVal Value As String)
            _PriorTo = Value
        End Set
    End Property


    Public Property IsMandatory() As Boolean
        Get
            Return _IsMandatory
        End Get
        Set(ByVal Value As Boolean)
            _IsMandatory = Value
        End Set
    End Property

    Public Property ApprovalTypeScheme_desc() As String
        Get
            Return _ApprovalTypeScheme_desc
        End Get
        Set(ByVal Value As String)
            _ApprovalTypeScheme_desc = Value
        End Set
    End Property

    Public Property JournalScheme_desc() As String
        Get
            Return _JournalScheme_desc
        End Get
        Set(ByVal Value As String)
            _JournalScheme_desc = Value
        End Set
    End Property


    Public Property CreditScoreSchemeMaster_desc() As String
        Get
            Return _CreditScoreSchemeMaster_desc
        End Get
        Set(ByVal Value As String)
            _CreditScoreSchemeMaster_desc = Value
        End Set
    End Property


    Public Property ScoreSchemeMaster_desc() As String
        Get
            Return _ScoreSchemeMaster_desc
        End Get
        Set(ByVal Value As String)
            _ScoreSchemeMaster_desc = Value
        End Set
    End Property


    Public Property AssetType_desc() As String
        Get
            Return _AssetType_desc
        End Get
        Set(ByVal Value As String)
            _AssetType_desc = Value
        End Set
    End Property

    Public Property SaveProduct() As String
        Get
            Return _SaveProduct
        End Get
        Set(ByVal Value As String)
            _SaveProduct = Value
        End Set
    End Property

    Public Property TotalBranch() As String
        Get
            Return _TotalBranch
        End Get
        Set(ByVal Value As String)
            _TotalBranch = Value
        End Set
    End Property


    Public Property BBranchID() As String
        Get
            Return _BBranchID
        End Get
        Set(ByVal Value As String)
            _BBranchID = Value
        End Set
    End Property

    Public Property BBranchName() As String
        Get
            Return _BBranchName
        End Get
        Set(ByVal Value As String)
            _BBranchName = Value
        End Set
    End Property


    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property IsActive() As Boolean
        Get
            Return _IsActive
        End Get
        Set(ByVal Value As Boolean)
            _IsActive = Value
        End Set
    End Property

    Public Property IsActive_HO() As Boolean
        Get
            Return _IsActive_HO
        End Get
        Set(ByVal Value As Boolean)
            _IsActive_HO = Value
        End Set
    End Property


    Public Property ProductId() As String
        Get
            Return _ProductId
        End Get
        Set(ByVal Value As String)
            _ProductId = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property AssetTypeID() As String
        Get
            Return _AssetTypeID
        End Get
        Set(ByVal Value As String)
            _AssetTypeID = Value
        End Set
    End Property

    Public Property ScoreSchemeID() As String
        Get
            Return _ScoreSchemeID
        End Get
        Set(ByVal Value As String)
            _ScoreSchemeID = Value
        End Set
    End Property

    Public Property CreditScoreSchemeID() As String
        Get
            Return _CreditScoreSchemeID
        End Get
        Set(ByVal Value As String)
            _CreditScoreSchemeID = Value
        End Set
    End Property

    Public Property JournalSchemeID() As String
        Get
            Return _JournalSchemeID
        End Get
        Set(ByVal Value As String)
            _JournalSchemeID = Value
        End Set
    End Property

    Public Property ApprovalSchemeID() As String
        Get
            Return _ApprovalSchemeID
        End Get
        Set(ByVal Value As String)
            _ApprovalSchemeID = Value
        End Set
    End Property

    Public Property AssetUsedNew() As String
        Get
            Return _AssetUsedNew
        End Get
        Set(ByVal Value As String)
            _AssetUsedNew = Value
        End Set
    End Property

    Public Property FinanceType() As String
        Get
            Return _FinanceType
        End Get
        Set(ByVal Value As String)
            _FinanceType = Value
        End Set
    End Property

    Public Property EffectiveRate() As Decimal
        Get
            Return _EffectiveRate
        End Get
        Set(ByVal Value As Decimal)
            _EffectiveRate = Value
        End Set
    End Property

    Public Property EffectiveRateBehaviour() As String
        Get
            Return _EffectiveRateBehaviour
        End Get
        Set(ByVal Value As String)
            _EffectiveRateBehaviour = Value
        End Set
    End Property

    Public Property EffectiveRate_HO() As Decimal
        Get
            Return _EffectiveRate_HO
        End Get
        Set(ByVal Value As Decimal)
            _EffectiveRate_HO = Value
        End Set
    End Property

    Public Property EffectiveRateBehaviour_HO() As String
        Get
            Return _EffectiveRateBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _EffectiveRateBehaviour_HO = Value
        End Set
    End Property

    Public Property GrossYieldRate() As Decimal
        Get
            Return _GrossYieldRate
        End Get
        Set(ByVal Value As Decimal)
            _GrossYieldRate = Value
        End Set
    End Property

    Public Property GrossYieldRateBehaviour() As String
        Get
            Return _GrossYieldRateBehaviour
        End Get
        Set(ByVal Value As String)
            _GrossYieldRateBehaviour = Value
        End Set
    End Property
    Public Property GrossYieldRate_HO() As Decimal
        Get
            Return _GrossYieldRate_HO
        End Get
        Set(ByVal Value As Decimal)
            _GrossYieldRate_HO = Value
        End Set
    End Property

    Public Property GrossYieldRateBehaviour_HO() As String
        Get
            Return _GrossYieldRateBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _GrossYieldRateBehaviour_HO = Value
        End Set
    End Property


    Public Property MinimumTenor() As Integer
        Get
            Return _MinimumTenor
        End Get
        Set(ByVal Value As Integer)
            _MinimumTenor = Value
        End Set
    End Property

    Public Property MaximumTenor() As Integer
        Get
            Return _MaximumTenor
        End Get
        Set(ByVal Value As Integer)
            _MaximumTenor = Value
        End Set
    End Property

    Public Property MinimumTenor_HO() As Integer
        Get
            Return _MinimumTenor_HO
        End Get
        Set(ByVal Value As Integer)
            _MinimumTenor_HO = Value
        End Set
    End Property

    Public Property MaximumTenor_HO() As Integer
        Get
            Return _MaximumTenor_HO
        End Get
        Set(ByVal Value As Integer)
            _MaximumTenor_HO = Value
        End Set
    End Property

    Public Property PenaltyPercentage() As Decimal
        Get
            Return _PenaltyPercentage
        End Get
        Set(ByVal Value As Decimal)
            _PenaltyPercentage = Value
        End Set
    End Property

    Public Property PenaltyPercentageBehaviour() As String
        Get
            Return _PenaltyPercentageBehaviour
        End Get
        Set(ByVal Value As String)
            _PenaltyPercentageBehaviour = Value
        End Set
    End Property

    Public Property PenaltyPercentage_HO() As Decimal
        Get
            Return _PenaltyPercentage_HO
        End Get
        Set(ByVal Value As Decimal)
            _PenaltyPercentage_HO = Value
        End Set
    End Property

    Public Property PenaltyPercentageBehaviour_HO() As String
        Get
            Return _PenaltyPercentageBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _PenaltyPercentageBehaviour_HO = Value
        End Set
    End Property

    Public Property InsurancePenaltyPercentage() As Decimal
        Get
            Return _InsurancePenaltyPercentage
        End Get
        Set(ByVal Value As Decimal)
            _InsurancePenaltyPercentage = Value
        End Set
    End Property

    Public Property InsurancePenaltyPercentageBehaviour() As String
        Get
            Return _InsurancePenaltyPercentageBehaviour
        End Get
        Set(ByVal Value As String)
            _InsurancePenaltyPercentageBehaviour = Value
        End Set
    End Property
    Public Property InsurancePenaltyPercentage_HO() As Decimal
        Get
            Return _InsurancePenaltyPercentage_HO
        End Get
        Set(ByVal Value As Decimal)
            _InsurancePenaltyPercentage_HO = Value
        End Set
    End Property

    Public Property InsurancePenaltyPercentageBehaviour_HO() As String
        Get
            Return _InsurancePenaltyPercentageBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _InsurancePenaltyPercentageBehaviour_HO = Value
        End Set
    End Property

    Public Property CancellationFee() As Decimal
        Get
            Return _CancellationFee
        End Get
        Set(ByVal Value As Decimal)
            _CancellationFee = Value
        End Set
    End Property

    Public Property CancellationFeeBehaviour() As String
        Get
            Return _CancellationFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _CancellationFeeBehaviour = Value
        End Set
    End Property

    Public Property CancellationFee_HO() As Decimal
        Get
            Return _CancellationFee_HO
        End Get
        Set(ByVal Value As Decimal)
            _CancellationFee_HO = Value
        End Set
    End Property

    Public Property CancellationFeeBehaviour_HO() As String
        Get
            Return _CancellationFeeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _CancellationFeeBehaviour_HO = Value
        End Set
    End Property


    Public Property AdminFee() As Decimal
        Get
            Return _AdminFee
        End Get
        Set(ByVal Value As Decimal)
            _AdminFee = Value
        End Set
    End Property

    Public Property AdminFeeBehaviour() As String
        Get
            Return _AdminFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _AdminFeeBehaviour = Value
        End Set
    End Property

    Public Property AdminFee_HO() As Decimal
        Get
            Return _AdminFee_HO
        End Get
        Set(ByVal Value As Decimal)
            _AdminFee_HO = Value
        End Set
    End Property

    Public Property AdminFeeBehaviour_HO() As String
        Get
            Return _AdminFeeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _AdminFeeBehaviour_HO = Value
        End Set
    End Property

    Public Property FiduciaFee() As Decimal
        Get
            Return _FiduciaFee
        End Get
        Set(ByVal Value As Decimal)
            _FiduciaFee = Value
        End Set
    End Property

    Public Property FiduciaFeeBehaviour() As String
        Get
            Return _FiduciaFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _FiduciaFeeBehaviour = Value
        End Set
    End Property

    Public Property FiduciaFee_HO() As Decimal
        Get
            Return _FiduciaFee_HO
        End Get
        Set(ByVal Value As Decimal)
            _FiduciaFee_HO = Value
        End Set
    End Property

    Public Property FiduciaFeeBehaviour_HO() As String
        Get
            Return _FiduciaFeeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _FiduciaFeeBehaviour_HO = Value
        End Set
    End Property

    Public Property ProvisionFee() As Decimal
        Get
            Return _ProvisionFee
        End Get
        Set(ByVal Value As Decimal)
            _ProvisionFee = Value
        End Set
    End Property

    Public Property ProvisionFeeBehaviour() As String
        Get
            Return _ProvisionFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _ProvisionFeeBehaviour = Value
        End Set
    End Property

    Public Property ProvisionFeeHO() As Decimal
        Get
            Return _ProvisionFeeHO
        End Get
        Set(ByVal Value As Decimal)
            _ProvisionFeeHO = Value
        End Set
    End Property

    Public Property ProvisionFeeBehaviour_HO() As String
        Get
            Return _ProvisionFeeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _ProvisionFeeBehaviour_HO = Value
        End Set
    End Property

    Public Property NotaryFee() As Decimal
        Get
            Return _NotaryFee
        End Get
        Set(ByVal Value As Decimal)
            _NotaryFee = Value
        End Set
    End Property

    Public Property NotaryFeeBehaviour() As String
        Get
            Return _NotaryFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _NotaryFeeBehaviour = Value
        End Set
    End Property

    Public Property NotaryFee_HO() As Decimal
        Get
            Return _NotaryFee_HO
        End Get
        Set(ByVal Value As Decimal)
            _NotaryFee_HO = Value
        End Set
    End Property

    Public Property NotaryFeeBehaviour_HO() As String
        Get
            Return _NotaryFeeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _NotaryFeeBehaviour_HO = Value
        End Set
    End Property


    Public Property SurveyFee() As Decimal
        Get
            Return _SurveyFee
        End Get
        Set(ByVal Value As Decimal)
            _SurveyFee = Value
        End Set
    End Property

    Public Property SurveyFeeBehaviour() As String
        Get
            Return _SurveyFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _SurveyFeeBehaviour = Value
        End Set
    End Property

    Public Property SurveyFee_HO() As Decimal
        Get
            Return _SurveyFee_HO
        End Get
        Set(ByVal Value As Decimal)
            _SurveyFee_HO = Value
        End Set
    End Property

    Public Property SurveyFeeBehaviour_HO() As String
        Get
            Return _SurveyFeeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _SurveyFeeBehaviour_HO = Value
        End Set
    End Property

    Public Property VisitFee() As Decimal
        Get
            Return _VisitFee
        End Get
        Set(ByVal Value As Decimal)
            _VisitFee = Value
        End Set
    End Property

    Public Property VisitFeeBehaviour() As String
        Get
            Return _VisitFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _VisitFeeBehaviour = Value
        End Set
    End Property

    Public Property VisitFee_HO() As Decimal
        Get
            Return _VisitFee_HO
        End Get
        Set(ByVal Value As Decimal)
            _VisitFee_HO = Value
        End Set
    End Property

    Public Property VisitFeeBehaviour_HO() As String
        Get
            Return _VisitFeeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _VisitFeeBehaviour_HO = Value
        End Set
    End Property

    Public Property ReschedulingFee() As Decimal
        Get
            Return _ReschedulingFee
        End Get
        Set(ByVal Value As Decimal)
            _ReschedulingFee = Value
        End Set
    End Property

    Public Property ReschedulingFeeBehaviour() As String
        Get
            Return _ReschedulingFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _ReschedulingFeeBehaviour = Value
        End Set
    End Property

    Public Property ReschedulingFee_HO() As Decimal
        Get
            Return _ReschedulingFee_HO
        End Get
        Set(ByVal Value As Decimal)
            _ReschedulingFee_HO = Value
        End Set
    End Property

    Public Property ReschedulingFeeBehaviour_HO() As String
        Get
            Return _ReschedulingFeeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _ReschedulingFeeBehaviour_HO = Value
        End Set
    End Property

    Public Property AgreementTransferFee() As Decimal
        Get
            Return _AgreementTransferFee
        End Get
        Set(ByVal Value As Decimal)
            _AgreementTransferFee = Value
        End Set
    End Property

    Public Property AgreementTransferFeeBehaviour() As String
        Get
            Return _AgreementTransferFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _AgreementTransferFeeBehaviour = Value
        End Set
    End Property

    Public Property AgreementTransferFee_HO() As Decimal
        Get
            Return _AgreementTransferFee_HO
        End Get
        Set(ByVal Value As Decimal)
            _AgreementTransferFee_HO = Value
        End Set
    End Property

    Public Property AgreementTransferFeeBehaviour_HO() As String
        Get
            Return _AgreementTransferFeeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _AgreementTransferFeeBehaviour_HO = Value
        End Set
    End Property

    Public Property ChangeDueDateFee() As Decimal
        Get
            Return _ChangeDueDateFee
        End Get
        Set(ByVal Value As Decimal)
            _ChangeDueDateFee = Value
        End Set
    End Property

    Public Property ChangeDueDateFeeBehaviour() As String
        Get
            Return _ChangeDueDateFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _ChangeDueDateFeeBehaviour = Value
        End Set
    End Property

    Public Property ChangeDueDateFee_HO() As Decimal
        Get
            Return _ChangeDueDateFee_HO
        End Get
        Set(ByVal Value As Decimal)
            _ChangeDueDateFee_HO = Value
        End Set
    End Property

    Public Property ChangeDueDateFeeBehaviour_HO() As String
        Get
            Return _ChangeDueDateFeeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _ChangeDueDateFeeBehaviour_HO = Value
        End Set
    End Property

    Public Property AssetReplacementFee() As Decimal
        Get
            Return _AssetReplacementFee
        End Get
        Set(ByVal Value As Decimal)
            _AssetReplacementFee = Value
        End Set
    End Property

    Public Property AssetReplacementFeeBehaviour() As String
        Get
            Return _AssetReplacementFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _AssetReplacementFeeBehaviour = Value
        End Set
    End Property

    Public Property AssetReplacementFee_HO() As Decimal
        Get
            Return _AssetReplacementFee_HO
        End Get
        Set(ByVal Value As Decimal)
            _AssetReplacementFee_HO = Value
        End Set
    End Property

    Public Property AssetReplacementFeeBehaviour_HO() As String
        Get
            Return _AssetReplacementFeeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _AssetReplacementFeeBehaviour_HO = Value
        End Set
    End Property

    Public Property RepossesFee() As Decimal
        Get
            Return _RepossesFee
        End Get
        Set(ByVal Value As Decimal)
            _RepossesFee = Value
        End Set
    End Property

    Public Property RepossesFeeBehaviour() As String
        Get
            Return _RepossesFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _RepossesFeeBehaviour = Value
        End Set
    End Property

    Public Property RepossesFee_HO() As Decimal
        Get
            Return _RepossesFee_HO
        End Get
        Set(ByVal Value As Decimal)
            _RepossesFee_HO = Value
        End Set
    End Property

    Public Property RepossesFeeBehaviour_HO() As String
        Get
            Return _RepossesFeeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _RepossesFeeBehaviour_HO = Value
        End Set
    End Property

    Public Property LegalisirDocFee() As Decimal
        Get
            Return _LegalisirDocFee
        End Get
        Set(ByVal Value As Decimal)
            _LegalisirDocFee = Value
        End Set
    End Property

    Public Property LegalisirDocFeeBehaviour() As String
        Get
            Return _LegalisirDocFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _LegalisirDocFeeBehaviour = Value
        End Set
    End Property

    Public Property LegalisirDocFee_HO() As Decimal
        Get
            Return _LegalisirDocFee_HO
        End Get
        Set(ByVal Value As Decimal)
            _LegalisirDocFee_HO = Value
        End Set
    End Property

    Public Property LegalisirDocFeeBehaviour_HO() As String
        Get
            Return _LegalisirDocFeeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _LegalisirDocFeeBehaviour_HO = Value
        End Set
    End Property

    Public Property PDCBounceFee() As Decimal
        Get
            Return _PDCBounceFee
        End Get
        Set(ByVal Value As Decimal)
            _PDCBounceFee = Value
        End Set
    End Property

    Public Property PDCBounceFeeBehaviour() As String
        Get
            Return _PDCBounceFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _PDCBounceFeeBehaviour = Value
        End Set
    End Property

    Public Property PDCBounceFee_HO() As Decimal
        Get
            Return _PDCBounceFee_HO
        End Get
        Set(ByVal Value As Decimal)
            _PDCBounceFee_HO = Value
        End Set
    End Property

    Public Property PDCBounceFeeBehaviour_HO() As String
        Get
            Return _PDCBounceFeeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _PDCBounceFeeBehaviour_HO = Value
        End Set
    End Property

    Public Property InsAdminFee() As Decimal
        Get
            Return _InsAdminFee
        End Get
        Set(ByVal Value As Decimal)
            _InsAdminFee = Value
        End Set
    End Property

    Public Property InsAdminFeeBehaviour() As String
        Get
            Return _InsAdminFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _InsAdminFeeBehaviour = Value
        End Set
    End Property

    Public Property InsAdminFee_HO() As Decimal
        Get
            Return _InsAdminFee_HO
        End Get
        Set(ByVal Value As Decimal)
            _InsAdminFee_HO = Value
        End Set
    End Property

    Public Property InsAdminFeeBehaviour_HO() As String
        Get
            Return _InsAdminFeeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _InsAdminFeeBehaviour_HO = Value
        End Set
    End Property


    Public Property InsStampDutyFee() As Decimal
        Get
            Return _InsStampDutyFee
        End Get
        Set(ByVal Value As Decimal)
            _InsStampDutyFee = Value
        End Set
    End Property

    Public Property InsStampDutyFeeBehaviour() As String
        Get
            Return _InsStampDutyFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _InsStampDutyFeeBehaviour = Value
        End Set
    End Property

    Public Property InsStampDutyFee_HO() As Decimal
        Get
            Return _InsStampDutyFee_HO
        End Get
        Set(ByVal Value As Decimal)
            _InsStampDutyFee_HO = Value
        End Set
    End Property

    Public Property InsStampDutyFeeBehaviour_HO() As String
        Get
            Return _InsStampDutyFeeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _InsStampDutyFeeBehaviour_HO = Value
        End Set
    End Property

    Public Property DaysPOExpiration() As Integer
        Get
            Return _DaysPOExpiration
        End Get
        Set(ByVal Value As Integer)
            _DaysPOExpiration = Value
        End Set
    End Property

    ''meisan
    Public Property CreditProtection() As Integer
        Get
            Return _CreditProtection
        End Get
        Set(ByVal Value As Integer)
            _CreditProtection = Value
        End Set
    End Property

    Public Property CreditProtectionBehaviour() As String
        Get
            Return _CreditProtectionBehaviour
        End Get
        Set(ByVal Value As String)
            _CreditProtectionBehaviour = Value
        End Set
    End Property

    Public Property AsuransiKredit() As Integer
        Get
            Return _AsuransiKredit
        End Get
        Set(ByVal Value As Integer)
            _AsuransiKredit = Value
        End Set
    End Property

    Public Property AsuransiKreditBehaviour() As String
        Get
            Return _AsuransiKreditBehaviour
        End Get
        Set(ByVal Value As String)
            _AsuransiKreditBehaviour = Value
        End Set
    End Property


    Public Property CreditProtection_HO() As Integer
        Get
            Return _CreditProtection_HO
        End Get
        Set(ByVal Value As Integer)
            _CreditProtection_HO = Value
        End Set
    End Property

    Public Property CreditProtectionBehaviour_HO() As String
        Get
            Return _CreditProtectionBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _CreditProtectionBehaviour_HO = Value
        End Set
    End Property

    Public Property AsuransiKredit_HO() As Integer
        Get
            Return _AsuransiKredit_HO
        End Get
        Set(ByVal Value As Integer)
            _AsuransiKredit_HO = Value
        End Set
    End Property

    Public Property AsuransiKreditBehaviour_HO() As String
        Get
            Return _AsuransiKreditBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _AsuransiKreditBehaviour_HO = Value
        End Set
    End Property
    Public Property DaysPOExpirationBehaviour() As String
        Get
            Return _DaysPOExpirationBehaviour
        End Get
        Set(ByVal Value As String)
            _DaysPOExpirationBehaviour = Value
        End Set
    End Property

    Public Property DaysPOExpiration_HO() As Integer
        Get
            Return _DaysPOExpiration_HO
        End Get
        Set(ByVal Value As Integer)
            _DaysPOExpiration_HO = Value
        End Set
    End Property

    Public Property DaysPOExpirationBehaviour_HO() As String
        Get
            Return _DaysPOExpirationBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _DaysPOExpirationBehaviour_HO = Value
        End Set
    End Property

    Public Property InstallmentToleranceAmount() As Decimal
        Get
            Return _InstallmentToleranceAmount
        End Get
        Set(ByVal Value As Decimal)
            _InstallmentToleranceAmount = Value
        End Set
    End Property

    Public Property InstallmentToleranceAmountBehaviour() As String
        Get
            Return _InstallmentToleranceAmountBehaviour
        End Get
        Set(ByVal Value As String)
            _InstallmentToleranceAmountBehaviour = Value
        End Set
    End Property

    Public Property InstallmentToleranceAmount_HO() As Decimal
        Get
            Return _InstallmentToleranceAmount_HO
        End Get
        Set(ByVal Value As Decimal)
            _InstallmentToleranceAmount_HO = Value
        End Set
    End Property

    Public Property InstallmentToleranceAmountBehaviour_HO() As String
        Get
            Return _InstallmentToleranceAmountBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _InstallmentToleranceAmountBehaviour_HO = Value
        End Set
    End Property

    Public Property DPPercentage() As Decimal
        Get
            Return _DPPercentage
        End Get
        Set(ByVal Value As Decimal)
            _DPPercentage = Value
        End Set
    End Property

    Public Property DPPercentageBehaviour() As String
        Get
            Return _DPPercentageBehaviour
        End Get
        Set(ByVal Value As String)
            _DPPercentageBehaviour = Value
        End Set
    End Property

    Public Property DPPercentage_HO() As Decimal
        Get
            Return _DPPercentage_HO
        End Get
        Set(ByVal Value As Decimal)
            _DPPercentage_HO = Value
        End Set
    End Property

    Public Property DPPercentageBehaviour_HO() As String
        Get
            Return _DPPercentageBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _DPPercentageBehaviour_HO = Value
        End Set
    End Property

    Public Property RejectMinimumIncome() As Decimal
        Get
            Return _RejectMinimumIncome
        End Get
        Set(ByVal Value As Decimal)
            _RejectMinimumIncome = Value
        End Set
    End Property

    Public Property RejectMinimumIncomeBehaviour() As String
        Get
            Return _RejectMinimumIncomeBehaviour
        End Get
        Set(ByVal Value As String)
            _RejectMinimumIncomeBehaviour = Value
        End Set
    End Property

    Public Property RejectMinimumIncome_HO() As Decimal
        Get
            Return _RejectMinimumIncome_HO
        End Get
        Set(ByVal Value As Decimal)
            _RejectMinimumIncome_HO = Value
        End Set
    End Property

    Public Property RejectMinimumIncomeBehaviour_HO() As String
        Get
            Return _RejectMinimumIncomeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _RejectMinimumIncomeBehaviour_HO = Value
        End Set
    End Property


    Public Property WarningMinimumIncome() As Decimal
        Get
            Return _WarningMinimumIncome
        End Get
        Set(ByVal Value As Decimal)
            _WarningMinimumIncome = Value
        End Set
    End Property

    Public Property WarningMinimumIncomeBehaviour() As String
        Get
            Return _WarningMinimumIncomeBehaviour
        End Get
        Set(ByVal Value As String)
            _WarningMinimumIncomeBehaviour = Value
        End Set
    End Property

    Public Property WarningMinimumIncome_HO() As Decimal
        Get
            Return _WarningMinimumIncome_HO
        End Get
        Set(ByVal Value As Decimal)
            _WarningMinimumIncome_HO = Value
        End Set
    End Property

    Public Property WarningMinimumIncomeBehaviour_HO() As String
        Get
            Return _WarningMinimumIncomeBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _WarningMinimumIncomeBehaviour_HO = Value
        End Set
    End Property

    Public Property IsSPAutomatic() As Boolean
        Get
            Return _IsSPAutomatic
        End Get
        Set(ByVal Value As Boolean)
            _IsSPAutomatic = Value
        End Set
    End Property

    Public Property IsSPAutomaticBehaviour() As String
        Get
            Return _IsSPAutomaticBehaviour
        End Get
        Set(ByVal Value As String)
            _IsSPAutomaticBehaviour = Value
        End Set
    End Property

    Public Property LengthSPProcess() As Integer
        Get
            Return _LengthSPProcess
        End Get
        Set(ByVal Value As Integer)
            _LengthSPProcess = Value
        End Set
    End Property

    Public Property IsSPAutomatic_HO() As Boolean
        Get
            Return _IsSPAutomatic_HO
        End Get
        Set(ByVal Value As Boolean)
            _IsSPAutomatic_HO = Value
        End Set
    End Property

    Public Property IsSPAutomaticBehaviour_HO() As String
        Get
            Return _IsSPAutomaticBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _IsSPAutomaticBehaviour_HO = Value
        End Set
    End Property

    Public Property LengthSPProcess_HO() As Integer
        Get
            Return _LengthSPProcess_HO
        End Get
        Set(ByVal Value As Integer)
            _LengthSPProcess_HO = Value
        End Set
    End Property

    Public Property IsSP1Automatic() As Boolean
        Get
            Return _IsSP1Automatic
        End Get
        Set(ByVal Value As Boolean)
            _IsSP1Automatic = Value
        End Set
    End Property

    Public Property IsSP1AutomaticBehaviour() As String
        Get
            Return _IsSP1AutomaticBehaviour
        End Get
        Set(ByVal Value As String)
            _IsSP1AutomaticBehaviour = Value
        End Set
    End Property

    Public Property LengthSP1Process() As Integer
        Get
            Return _LengthSP1Process
        End Get
        Set(ByVal Value As Integer)
            _LengthSP1Process = Value
        End Set
    End Property

    Public Property IsSP1Automatic_HO() As Boolean
        Get
            Return _IsSP1Automatic_HO
        End Get
        Set(ByVal Value As Boolean)
            _IsSP1Automatic_HO = Value
        End Set
    End Property

    Public Property IsSP1AutomaticBehaviour_HO() As String
        Get
            Return _IsSP1AutomaticBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _IsSP1AutomaticBehaviour_HO = Value
        End Set
    End Property

    Public Property LengthSP1Process_HO() As Integer
        Get
            Return _LengthSP1Process_HO
        End Get
        Set(ByVal Value As Integer)
            _LengthSP1Process_HO = Value
        End Set
    End Property

    Public Property IsSP2Automatic() As Boolean
        Get
            Return _IsSP2Automatic
        End Get
        Set(ByVal Value As Boolean)
            _IsSP2Automatic = Value
        End Set
    End Property

    Public Property IsSP2AutomaticBehaviour() As String
        Get
            Return _IsSP2AutomaticBehaviour
        End Get
        Set(ByVal Value As String)
            _IsSP2AutomaticBehaviour = Value
        End Set
    End Property

    Public Property LengthSP2Process() As Integer
        Get
            Return _LengthSP2Process
        End Get
        Set(ByVal Value As Integer)
            _LengthSP2Process = Value
        End Set
    End Property

    Public Property IsSP2Automatic_HO() As Boolean
        Get
            Return _IsSP2Automatic_HO
        End Get
        Set(ByVal Value As Boolean)
            _IsSP2Automatic_HO = Value
        End Set
    End Property

    Public Property IsSP2AutomaticBehaviour_HO() As String
        Get
            Return _IsSP2AutomaticBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _IsSP2AutomaticBehaviour_HO = Value
        End Set
    End Property

    Public Property LengthSP2Process_HO() As Integer
        Get
            Return _LengthSP2Process_HO
        End Get
        Set(ByVal Value As Integer)
            _LengthSP2Process_HO = Value
        End Set
    End Property

    Public Property LengthMainDocProcess() As Integer
        Get
            Return _LengthMainDocProcess
        End Get
        Set(ByVal Value As Integer)
            _LengthMainDocProcess = Value
        End Set
    End Property

    Public Property LengthMainDocProcessBehaviour() As String
        Get
            Return _LengthMainDocProcessBehaviour
        End Get
        Set(ByVal Value As String)
            _LengthMainDocProcessBehaviour = Value
        End Set
    End Property

    Public Property LengthMainDocProcess_HO() As Integer
        Get
            Return _LengthMainDocProcess_HO
        End Get
        Set(ByVal Value As Integer)
            _LengthMainDocProcess_HO = Value
        End Set
    End Property

    Public Property LengthMainDocProcessBehaviour_HO() As String
        Get
            Return _LengthMainDocProcessBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _LengthMainDocProcessBehaviour_HO = Value
        End Set
    End Property


    Public Property LengthMainDocTaken() As Integer
        Get
            Return _LengthMainDocTaken
        End Get
        Set(ByVal Value As Integer)
            _LengthMainDocTaken = Value
        End Set
    End Property

    Public Property LengthMainDocTakenBehaviour() As String
        Get
            Return _LengthMainDocTakenBehaviour
        End Get
        Set(ByVal Value As String)
            _LengthMainDocTakenBehaviour = Value
        End Set
    End Property

    Public Property LengthMainDocTaken_HO() As Integer
        Get
            Return _LengthMainDocTaken_HO
        End Get
        Set(ByVal Value As Integer)
            _LengthMainDocTaken_HO = Value
        End Set
    End Property

    Public Property LengthMainDocTakenBehaviour_HO() As String
        Get
            Return _LengthMainDocTakenBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _LengthMainDocTakenBehaviour_HO = Value
        End Set
    End Property

    Public Property GracePeriodLateCharges() As Integer
        Get
            Return _GracePeriodLateCharges
        End Get
        Set(ByVal Value As Integer)
            _GracePeriodLateCharges = Value
        End Set
    End Property

    Public Property GracePeriodLateChargesBehaviour() As String
        Get
            Return _GracePeriodLateChargesBehaviour
        End Get
        Set(ByVal Value As String)
            _GracePeriodLateChargesBehaviour = Value
        End Set
    End Property

    Public Property GracePeriodLateCharges_HO() As Integer
        Get
            Return _GracePeriodLateCharges_HO
        End Get
        Set(ByVal Value As Integer)
            _GracePeriodLateCharges_HO = Value
        End Set
    End Property

    Public Property GracePeriodLateChargesBehaviour_HO() As String
        Get
            Return _GracePeriodLateChargesBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _GracePeriodLateChargesBehaviour_HO = Value
        End Set
    End Property

    Public Property PrepaymentPenaltyRate() As Decimal
        Get
            Return _PrepaymentPenaltyRate
        End Get
        Set(ByVal Value As Decimal)
            _PrepaymentPenaltyRate = Value
        End Set
    End Property

    Public Property PrepaymentPenaltyRateBehaviour() As String
        Get
            Return _PrepaymentPenaltyRateBehaviour
        End Get
        Set(ByVal Value As String)
            _PrepaymentPenaltyRateBehaviour = Value
        End Set
    End Property

    Public Property PrepaymentPenaltyRate_HO() As Decimal
        Get
            Return _PrepaymentPenaltyRate_HO
        End Get
        Set(ByVal Value As Decimal)
            _PrepaymentPenaltyRate_HO = Value
        End Set
    End Property

    Public Property PrepaymentPenaltyRateBehaviour_HO() As String
        Get
            Return _PrepaymentPenaltyRateBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _PrepaymentPenaltyRateBehaviour_HO = Value
        End Set
    End Property

    Public Property DeskCollPhoneRemind() As Integer
        Get
            Return _DeskCollPhoneRemind
        End Get
        Set(ByVal Value As Integer)
            _DeskCollPhoneRemind = Value
        End Set
    End Property

    Public Property DeskCollPhoneRemind2() As Integer
        Get
            Return _DeskCollPhoneRemind2
        End Get
        Set(ByVal Value As Integer)
            _DeskCollPhoneRemind2 = Value
        End Set
    End Property

    Public Property DeskCollPhoneRemind3() As Integer
        Get
            Return _DeskCollPhoneRemind3
        End Get
        Set(ByVal Value As Integer)
            _DeskCollPhoneRemind3 = Value
        End Set
    End Property

    Public Property DeskCollPhoneRemindBehaviour() As String
        Get
            Return _DeskCollPhoneRemindBehaviour
        End Get
        Set(ByVal Value As String)
            _DeskCollPhoneRemindBehaviour = Value
        End Set
    End Property

    Public Property DeskCollPhoneRemindBehaviour2() As String
        Get
            Return _DeskCollPhoneRemindBehaviour2
        End Get
        Set(ByVal Value As String)
            _DeskCollPhoneRemindBehaviour2 = Value
        End Set

    End Property


    Public Property DeskCollPhoneRemindBehaviour3() As String
        Get
            Return _DeskCollPhoneRemindBehaviour3
        End Get
        Set(ByVal Value As String)
            _DeskCollPhoneRemindBehaviour3 = Value
        End Set
    End Property

    Public Property DeskCollPhoneRemind_HO() As Integer
        Get
            Return _DeskCollPhoneRemind_HO
        End Get
        Set(ByVal Value As Integer)
            _DeskCollPhoneRemind_HO = Value
        End Set
    End Property

    Public Property DeskCollPhoneRemindBehaviour_HO() As String
        Get
            Return _DeskCollPhoneRemindBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _DeskCollPhoneRemindBehaviour_HO = Value
        End Set
    End Property

    Public Property DeskCollOD() As Integer
        Get
            Return _DeskCollOD
        End Get
        Set(ByVal Value As Integer)
            _DeskCollOD = Value
        End Set
    End Property

    Public Property DeskCollODBehaviour() As String
        Get
            Return _DeskCollODBehaviour
        End Get
        Set(ByVal Value As String)
            _DeskCollODBehaviour = Value
        End Set
    End Property

    Public Property DeskCollOD_HO() As Integer
        Get
            Return _DeskCollOD_HO
        End Get
        Set(ByVal Value As Integer)
            _DeskCollOD_HO = Value
        End Set
    End Property

    Public Property DeskCollODBehaviour_HO() As String
        Get
            Return _DeskCollODBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _DeskCollODBehaviour_HO = Value
        End Set
    End Property

    Public Property PrevODToRemind() As Integer
        Get
            Return _PrevODToRemind
        End Get
        Set(ByVal Value As Integer)
            _PrevODToRemind = Value
        End Set
    End Property

    Public Property PrevODToRemindBehaviour() As String
        Get
            Return _PrevODToRemindBehaviour
        End Get
        Set(ByVal Value As String)
            _PrevODToRemindBehaviour = Value
        End Set
    End Property

    Public Property PrevODToRemind_HO() As Integer
        Get
            Return _PrevODToRemind_HO
        End Get
        Set(ByVal Value As Integer)
            _PrevODToRemind_HO = Value
        End Set
    End Property

    Public Property PrevODToRemindBehaviour_HO() As String
        Get
            Return _PrevODToRemindBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _PrevODToRemindBehaviour_HO = Value
        End Set
    End Property


    Public Property PDCDayToRemind() As Integer
        Get
            Return _PDCDayToRemind
        End Get
        Set(ByVal Value As Integer)
            _PDCDayToRemind = Value
        End Set
    End Property

    Public Property PDCDayToRemindBehaviour() As String
        Get
            Return _PDCDayToRemindBehaviour
        End Get
        Set(ByVal Value As String)
            _PDCDayToRemindBehaviour = Value
        End Set
    End Property

    Public Property PDCDayToRemind_HO() As Integer
        Get
            Return _PDCDayToRemind_HO
        End Get
        Set(ByVal Value As Integer)
            _PDCDayToRemind_HO = Value
        End Set
    End Property

    Public Property PDCDayToRemindBehaviour_HO() As String
        Get
            Return _PDCDayToRemindBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _PDCDayToRemindBehaviour_HO = Value
        End Set
    End Property

    Public Property DeskCollSMSRemind() As Integer
        Get
            Return _DeskCollSMSRemind
        End Get
        Set(ByVal Value As Integer)
            _DeskCollSMSRemind = Value
        End Set
    End Property

    Public Property DeskCollSMSRemind2() As Integer
        Get
            Return _DeskCollSMSRemind2
        End Get
        Set(ByVal Value As Integer)
            _DeskCollSMSRemind2 = Value
        End Set
    End Property
    Public Property DeskCollSMSRemind3() As Integer
        Get
            Return _DeskCollSMSRemind3
        End Get
        Set(ByVal Value As Integer)
            _DeskCollSMSRemind3 = Value
        End Set
    End Property
    Public Property DeskCollSMSRemindBehaviour() As String
        Get
            Return _DeskCollSMSRemindBehaviour
        End Get
        Set(ByVal Value As String)
            _DeskCollSMSRemindBehaviour = Value
        End Set
    End Property
    Public Property DeskCollSMSRemindBehaviour2() As String
        Get
            Return _DeskCollSMSRemindBehaviour2
        End Get
        Set(ByVal Value As String)
            _DeskCollSMSRemindBehaviour2 = Value
        End Set
    End Property
 Public Property DeskCollSMSRemindBehaviour3() As String
        Get
            Return _DeskCollSMSRemindBehaviour3
        End Get
        Set(ByVal Value As String)
            _DeskCollSMSRemindBehaviour3 = Value
        End Set

    End Property

    Public Property DeskCollSMSRemind_HO() As Integer
        Get
            Return _DeskCollSMSRemind_HO
        End Get
        Set(ByVal Value As Integer)
            _DeskCollSMSRemind_HO = Value
        End Set
    End Property

    Public Property DeskCollSMSRemind_HO_2() As Integer
        Get
            Return _DeskCollSMSRemind_HO_2
        End Get
        Set(ByVal Value As Integer)
            _DeskCollSMSRemind_HO_2 = Value
        End Set
    End Property

    Public Property DeskCollSMSRemind_HO_3() As Integer
        Get
            Return _DeskCollSMSRemind_HO_3
        End Get
        Set(ByVal Value As Integer)
            _DeskCollSMSRemind_HO_3 = Value
        End Set
    End Property

    Public Property DeskCollSMSRemindBehaviour_HO() As String
        Get
            Return _DeskCollSMSRemindBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _DeskCollSMSRemindBehaviour_HO = Value
        End Set
    End Property

    Public Property DeskCollSMSRemindBehaviour_HO_2() As String
        Get
            Return _DeskCollSMSRemindBehaviour_HO_2
        End Get
        Set(ByVal Value As String)
            _DeskCollSMSRemindBehaviour_HO_2 = Value
        End Set
    End Property

    Public Property DeskCollSMSRemindBehaviour_HO_3() As String
        Get
            Return _DeskCollSMSRemindBehaviour_HO_3
        End Get
        Set(ByVal Value As String)
            _DeskCollSMSRemindBehaviour_HO_3 = Value
        End Set
    End Property


    Public Property DCR() As Integer
        Get
            Return _DCR
        End Get
        Set(ByVal Value As Integer)
            _DCR = Value
        End Set
    End Property

    Public Property DCRBehaviour() As String
        Get
            Return _DCRBehaviour
        End Get
        Set(ByVal Value As String)
            _DCRBehaviour = Value
        End Set
    End Property

    Public Property DCR_HO() As Integer
        Get
            Return _DCR_HO
        End Get
        Set(ByVal Value As Integer)
            _DCR_HO = Value
        End Set
    End Property

    Public Property DCRBehaviour_HO() As String
        Get
            Return _DCRBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _DCRBehaviour_HO = Value
        End Set
    End Property

    Public Property DaysBeforeDueToRN() As Integer
        Get
            Return _DaysBeforeDueToRN
        End Get
        Set(ByVal Value As Integer)
            _DaysBeforeDueToRN = Value
        End Set
    End Property

    Public Property DaysBeforeDueToRNBehaviour() As String
        Get
            Return _DaysBeforeDueToRNBehaviour
        End Get
        Set(ByVal Value As String)
            _DaysBeforeDueToRNBehaviour = Value
        End Set
    End Property

    Public Property DaysBeforeDueToRN_HO() As Integer
        Get
            Return _DaysBeforeDueToRN_HO
        End Get
        Set(ByVal Value As Integer)
            _DaysBeforeDueToRN_HO = Value
        End Set
    End Property

    Public Property DaysBeforeDueToRNBehaviour_HO() As String
        Get
            Return _DaysBeforeDueToRNBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _DaysBeforeDueToRNBehaviour_HO = Value
        End Set
    End Property

    Public Property ODToRAL() As Integer
        Get
            Return _ODToRAL
        End Get
        Set(ByVal Value As Integer)
            _ODToRAL = Value
        End Set
    End Property

    Public Property ODToRALBehaviour() As String
        Get
            Return _ODToRALBehaviour
        End Get
        Set(ByVal Value As String)
            _ODToRALBehaviour = Value
        End Set
    End Property

    Public Property ODToRAL_HO() As Integer
        Get
            Return _ODToRAL_HO
        End Get
        Set(ByVal Value As Integer)
            _ODToRAL_HO = Value
        End Set
    End Property

    Public Property ODToRALBehaviour_HO() As String
        Get
            Return _ODToRALBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _ODToRALBehaviour_HO = Value
        End Set
    End Property

    Public Property RALPeriod() As Integer
        Get
            Return _RALPeriod
        End Get
        Set(ByVal Value As Integer)
            _RALPeriod = Value
        End Set
    End Property

    Public Property RALPeriodBehaviour() As String
        Get
            Return _RALPeriodBehaviour
        End Get
        Set(ByVal Value As String)
            _RALPeriodBehaviour = Value
        End Set
    End Property

    Public Property RALPeriod_HO() As Integer
        Get
            Return _RALPeriod_HO
        End Get
        Set(ByVal Value As Integer)
            _RALPeriod_HO = Value
        End Set
    End Property

    Public Property RALPeriodBehaviour_HO() As String
        Get
            Return _RALPeriodBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _RALPeriodBehaviour_HO = Value
        End Set
    End Property

    Public Property MaxDaysRNPaid() As Integer
        Get
            Return _MaxDaysRNPaid
        End Get
        Set(ByVal Value As Integer)
            _MaxDaysRNPaid = Value
        End Set
    End Property

    Public Property MaxDaysRNPaidBehaviour() As String
        Get
            Return _MaxDaysRNPaidBehaviour
        End Get
        Set(ByVal Value As String)
            _MaxDaysRNPaidBehaviour = Value
        End Set
    End Property

    Public Property MaxDaysRNPaid_HO() As Integer
        Get
            Return _MaxDaysRNPaid_HO
        End Get
        Set(ByVal Value As Integer)
            _MaxDaysRNPaid_HO = Value
        End Set
    End Property

    Public Property MaxDaysRNPaidBehaviour_HO() As String
        Get
            Return _MaxDaysRNPaidBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _MaxDaysRNPaidBehaviour_HO = Value
        End Set
    End Property

    Public Property MaxPTPYDays() As Integer
        Get
            Return _MaxPTPYDays
        End Get
        Set(ByVal Value As Integer)
            _MaxPTPYDays = Value
        End Set
    End Property

    Public Property MaxPTPYDaysBehaviour() As String
        Get
            Return _MaxPTPYDaysBehaviour
        End Get
        Set(ByVal Value As String)
            _MaxPTPYDaysBehaviour = Value
        End Set
    End Property

    Public Property MaxPTPYDays_HO() As Integer
        Get
            Return _MaxPTPYDays_HO
        End Get
        Set(ByVal Value As Integer)
            _MaxPTPYDays_HO = Value
        End Set
    End Property

    Public Property MaxPTPYDaysBehaviour_HO() As String
        Get
            Return _MaxPTPYDaysBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _MaxPTPYDaysBehaviour_HO = Value
        End Set
    End Property

    Public Property PTPYBank() As Integer
        Get
            Return _PTPYBank
        End Get
        Set(ByVal Value As Integer)
            _PTPYBank = Value
        End Set
    End Property

    Public Property PTPYBankBehaviour() As String
        Get
            Return _PTPYBankBehaviour
        End Get
        Set(ByVal Value As String)
            _PTPYBankBehaviour = Value
        End Set
    End Property

    Public Property PTPYBank_HO() As Integer
        Get
            Return _PTPYBank_HO
        End Get
        Set(ByVal Value As Integer)
            _PTPYBank_HO = Value
        End Set
    End Property

    Public Property PTPYBankBehaviour_HO() As String
        Get
            Return _PTPYBankBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _PTPYBankBehaviour_HO = Value
        End Set
    End Property

    Public Property PTPYCompany() As Integer
        Get
            Return _PTPYCompany
        End Get
        Set(ByVal Value As Integer)
            _PTPYCompany = Value
        End Set
    End Property

    Public Property PTPYCompanyBehaviour() As String
        Get
            Return _PTPYCompanyBehaviour
        End Get
        Set(ByVal Value As String)
            _PTPYCompanyBehaviour = Value
        End Set
    End Property

    Public Property PTPYCompany_HO() As Integer
        Get
            Return _PTPYCompany_HO
        End Get
        Set(ByVal Value As Integer)
            _PTPYCompany_HO = Value
        End Set
    End Property

    Public Property PTPYCompanyBehaviour_HO() As String
        Get
            Return _PTPYCompanyBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _PTPYCompanyBehaviour_HO = Value
        End Set
    End Property


    Public Property PTPYSupplier() As Integer
        Get
            Return _PTPYSupplier
        End Get
        Set(ByVal Value As Integer)
            _PTPYSupplier = Value
        End Set
    End Property

    Public Property PerpanjanganSKT() As Integer
        Get
            Return _PerpanjanganSKT
        End Get
        Set(ByVal Value As Integer)
            _PerpanjanganSKT = Value
        End Set
    End Property

    Public Property PTPYSupplierBehaviour() As String
        Get
            Return _PTPYSupplierBehaviour
        End Get
        Set(ByVal Value As String)
            _PTPYSupplierBehaviour = Value
        End Set
    End Property

    Public Property PerpanjanganSKTBehaviour() As String
        Get
            Return _PerpanjanganSKTBehaviour
        End Get
        Set(ByVal Value As String)
            _PerpanjanganSKTBehaviour = Value
        End Set
    End Property

    Public Property PTPYSupplier_HO() As Integer
        Get
            Return _PTPYSupplier_HO
        End Get
        Set(ByVal Value As Integer)
            _PTPYSupplier_HO = Value
        End Set
    End Property

    Public Property PTPYSupplierBehaviour_HO() As String
        Get
            Return _PTPYSupplierBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _PTPYSupplierBehaviour_HO = Value
        End Set
    End Property


    Public Property RALExtension() As Integer
        Get
            Return _RALExtension
        End Get
        Set(ByVal Value As Integer)
            _RALExtension = Value
        End Set
    End Property

    Public Property RALExtensionBehaviour() As String
        Get
            Return _RALExtensionBehaviour
        End Get
        Set(ByVal Value As String)
            _RALExtensionBehaviour = Value
        End Set
    End Property

    Public Property RALExtension_HO() As Integer
        Get
            Return _RALExtension_HO
        End Get
        Set(ByVal Value As Integer)
            _RALExtension_HO = Value
        End Set
    End Property

    Public Property RALExtensionBehaviour_HO() As String
        Get
            Return _RALExtensionBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _RALExtensionBehaviour_HO = Value
        End Set
    End Property

    Public Property InventoryExpected() As Integer
        Get
            Return _InventoryExpected
        End Get
        Set(ByVal Value As Integer)
            _InventoryExpected = Value
        End Set
    End Property

    Public Property InventoryExpectedBehaviour() As String
        Get
            Return _InventoryExpectedBehaviour
        End Get
        Set(ByVal Value As String)
            _InventoryExpectedBehaviour = Value
        End Set
    End Property

    Public Property InventoryExpected_HO() As Integer
        Get
            Return _InventoryExpected_HO
        End Get
        Set(ByVal Value As Integer)
            _InventoryExpected_HO = Value
        End Set
    End Property

    Public Property InventoryExpectedBehaviour_HO() As String
        Get
            Return _InventoryExpectedBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _InventoryExpectedBehaviour_HO = Value
        End Set
    End Property

    Public Property BillingCharges() As Decimal
        Get
            Return _BillingCharges
        End Get
        Set(ByVal Value As Decimal)
            _BillingCharges = Value
        End Set
    End Property

    Public Property BillingChargesBehaviour() As String
        Get
            Return _BillingChargesBehaviour
        End Get
        Set(ByVal Value As String)
            _BillingChargesBehaviour = Value
        End Set
    End Property

    Public Property BillingCharges_HO() As Decimal
        Get
            Return _BillingCharges_HO
        End Get
        Set(ByVal Value As Decimal)
            _BillingCharges_HO = Value
        End Set
    End Property

    Public Property BillingChargesBehaviour_HO() As String
        Get
            Return _BillingChargesBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _BillingChargesBehaviour_HO = Value
        End Set
    End Property

    Public Property LimitAPCash() As Decimal
        Get
            Return _LimitAPCash
        End Get
        Set(ByVal Value As Decimal)
            _LimitAPCash = Value
        End Set
    End Property

    Public Property LimitAPCashBehaviour() As String
        Get
            Return _LimitAPCashBehaviour
        End Get
        Set(ByVal Value As String)
            _LimitAPCashBehaviour = Value
        End Set
    End Property

    Public Property LimitAPCash_HO() As Decimal
        Get
            Return _LimitAPCash_HO
        End Get
        Set(ByVal Value As Decimal)
            _LimitAPCash_HO = Value
        End Set
    End Property

    Public Property LimitAPCashBehaviour_HO() As String
        Get
            Return _LimitAPCashBehaviour_HO
        End Get
        Set(ByVal Value As String)
            _LimitAPCashBehaviour_HO = Value
        End Set
    End Property

    Public Property IsRecourse() As Boolean
        Get
            Return _IsRecourse
        End Get
        Set(ByVal Value As Boolean)
            _IsRecourse = Value
        End Set
    End Property

    Public Property IsRecourse_HO() As Boolean
        Get
            Return _IsRecourse_HO
        End Get
        Set(ByVal Value As Boolean)
            _IsRecourse_HO = Value
        End Set
    End Property

    Private _AssetCategory As String
    Private _UmurKendaraanFrom As Integer
    Private _UmurKendaraanTo As Integer
    Private _FirstInstallment As String
    Private _HargaOTRFrom As Double
    Private _HargaOTRTo As Double
    Private _Table As String

    Public Property AssetCategory() As String
        Get
            Return _AssetCategory
        End Get
        Set(ByVal Value As String)
            _AssetCategory = Value
        End Set
    End Property
    Public Property UmurKendaraanFrom() As Integer
        Get
            Return _UmurKendaraanFrom
        End Get
        Set(ByVal Value As Integer)
            _UmurKendaraanFrom = Value
        End Set
    End Property
    Public Property UmurKendaraanTo() As Integer
        Get
            Return _UmurKendaraanTo
        End Get
        Set(ByVal Value As Integer)
            _UmurKendaraanTo = Value
        End Set
    End Property
    Public Property FirstInstallment() As String
        Get
            Return _FirstInstallment
        End Get
        Set(ByVal Value As String)
            _FirstInstallment = Value
        End Set
    End Property
    Public Property HargaOTRFrom() As Double
        Get
            Return _HargaOTRFrom
        End Get
        Set(ByVal Value As Double)
            _HargaOTRFrom = Value
        End Set
    End Property
    Public Property HargaOTRTo() As Double
        Get
            Return _HargaOTRTo
        End Get
        Set(ByVal Value As Double)
            _HargaOTRTo = Value
        End Set
    End Property
    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property

    Public Property PenaltyBasedOn As String
    Public Property PenaltyBasedOnBehaviour As String
    Public Property PrepaymentPenaltyFixed As Decimal
    Public Property PrepaymentPenaltyFixedBehaviour As String
    Public Property PenaltyRatePrevious As Decimal
    Public Property PenaltyRateEffectiveDate As Date
    Public Property PrepaymentPenaltyPrevious As Decimal
    Public Property PrepaymentPenaltyEffectiveDate As Date
    Public Property PrepaymentPenaltyFixedEffectiveDate As Date
    Public Property PrepaymentPenaltyFixedPrevious As Decimal
    Public Property PrioritasPembayaran As String
    Public Property PengaturanBiaya As String


    Public Property InsuranceDiscPercentage As Decimal
    Public Property InsuranceDiscPercentageBehaviour As String
    Public Property IncomeInsRatioPercentage As Decimal
    Public Property IncomeInsRatioPercentageBehaviour As String


    Public Property PenaltyBasedOn_HO As String
    Public Property PenaltyBasedOnBehaviour_HO As String
    Public Property PrepaymentPenaltyFixed_HO As Decimal
    Public Property PrepaymentPenaltyFixedBehaviour_HO As String
    Public Property PenaltyRatePrevious_HO As Decimal
    Public Property PenaltyRateEffectiveDate_HO As Date
    Public Property PrepaymentPenaltyPrevious_HO As Decimal
    Public Property PrepaymentPenaltyEffectiveDate_HO As Date
    Public Property PrepaymentPenaltyFixedEffectiveDate_HO As Date
    Public Property PrepaymentPenaltyFixedPrevious_HO As Decimal
    Public Property PerpanjanganSKT_HO As String

    Public Property PerpanjanganSKTBehaviour_HO As String
    Public Property PrioritasPembayaran_HO As String

    Public Property PengaturanPembayaran_HO As String
    Public Property DeskCollSMSRemind2_HO As String
    Public Property DeskCollSMSRemind3_HO As String
    Public Property DeskCollSMSRemindBehaviour2_HO As String
    Public Property DeskCollSMSRemindBehaviour3_HO As String

    Public Property InsuranceDiscPercentage_HO As Decimal
    Public Property InsuranceDiscPercentageBehaviour_HO As String
    Public Property IncomeInsRatioPercentage_HO As Decimal
    Public Property IncomeInsRatioPercentageBehaviour_HO As String

    Public Property UmurKendaraanFrom_HO As Integer
    Public Property UmurKendaraanTo_HO As Integer

    Public Property BiayaPolis As Decimal
    Public Property BiayaPolis_HO As Decimal
    Public Property BiayaPolisBehaviour As String
    Public Property BiayaPolisBehaviour_HO As String


    Public Property KegiatanUsaha As String
    Public Property KegiatanUsahaDesc As String

    Public Property JenisPembiayaan As String
    Public Property JenisPembiayaanDesc As String

    Public Property AngsuranPertama As String
    Public Property SukuBungaFlat As Decimal
    Public Property SukuBungaFlatBehaviour As String
    Public Property OpsiUangMuka As String
    Public Property OpsiUangMukaAngsur As Integer
    Public Property SkemaPembiayaan As String

    Public Property AngsuranPertama_HO As String
    Public Property SukuBungaFlat_HO As Decimal
    Public Property SukuBungaFlatBehaviour_HO As String
    Public Property OpsiUangMuka_HO As String
    Public Property OpsiUangMukaAngsur_HO As Integer
    Public Property paket As String
    Public Property Akad As String


    Public Shared Function BehaviourItems() As IList(Of Parameter.CommonValueText)
        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("D", "Default"))
        def.Add(New Parameter.CommonValueText("L", "Locked"))
        def.Add(New Parameter.CommonValueText("N", "Minimum"))
        def.Add(New Parameter.CommonValueText("X", "Maximum"))

        Return def
    End Function
   

    Public Shared Function Behaviour_Value(ByVal Behaviour As String) As String
        If Behaviour = "D" Then
            Return "Default"
        ElseIf Behaviour = "L" Then
            Return "Locked"
        ElseIf Behaviour = "N" Then
            Return "Minimum"
        ElseIf Behaviour = "X" Then
            Return "Maximum"
        End If
        Return ""
    End Function

   
End Class
