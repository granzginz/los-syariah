﻿<Serializable()> _
Public Class KegiatanUsaha : Inherits CommonValueText

    Private _jnsPembiayaan As IList(Of CommonValueText)

    Public Sub New(value As String, text As String)
        MyBase.New(value, text)
        _jnsPembiayaan = New List(Of CommonValueText)
    End Sub

    Public Property JenisPembiayaan As IList(Of CommonValueText)
        Get
            Return _jnsPembiayaan
        End Get
        Set(value As IList(Of CommonValueText))
            _jnsPembiayaan = value
        End Set
    End Property


End Class
