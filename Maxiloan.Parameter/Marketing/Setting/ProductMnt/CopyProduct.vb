﻿<Serializable()> _
Public Class CopyProduct : Inherits Common
    Private _ProductId As String
    Private _ReferenceProductID As String
    Private _Description As String
    Private _BranID As String
    Public Property ProductId() As String
        Get
            Return _ProductId
        End Get
        Set(ByVal Value As String)
            _ProductId = Value
        End Set
    End Property
    Public Property ReferenceProductID() As String
        Get
            Return _ReferenceProductID
        End Get
        Set(ByVal Value As String)
            _ReferenceProductID = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property BranID() As String
        Get
            Return _BranID
        End Get
        Set(ByVal Value As String)
            _BranID = Value
        End Set
    End Property
End Class
