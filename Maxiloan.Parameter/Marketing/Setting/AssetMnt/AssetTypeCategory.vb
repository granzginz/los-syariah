
<Serializable()> _
Public Class AssetTypeCategory : Inherits Common
    Private _Id As String
    Private _Description As String
    Private _InsRate As String
    Private _CategoryID As String
    Private _listdataReport As DataSet
    Private _listdata As DataTable
    Private _totalrecords As Int64

    Public Property Id() As String
        Get
            Return _Id
        End Get
        Set(ByVal Value As String)
            _Id = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property CategoryID() As String
        Get
            Return _CategoryID
        End Get
        Set(ByVal Value As String)
            _CategoryID = Value
        End Set
    End Property

    Public Property InsRate() As String
        Get
            Return _InsRate
        End Get
        Set(ByVal Value As String)
            _InsRate = Value
        End Set
    End Property
End Class

