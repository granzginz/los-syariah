

<Serializable()> _
Public Class InsuranceAssetCategory : Inherits Common
    Private _InsRateCategoryID As String
    Private _AssetType As String
    Private _Description As String
    Private _listdata As DataTable
    Private _totalrecords As Int64
    Private _ListDataReport As DataSet

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property ListDataReport() As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(ByVal Value As DataSet)
            _ListDataReport = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property InsRateCategoryID() As String
        Get
            Return _InsRateCategoryID
        End Get
        Set(ByVal Value As String)
            _InsRateCategoryID = Value
        End Set
    End Property
    Public Property AssetType() As String
        Get
            Return _AssetType
        End Get
        Set(ByVal Value As String)
            _AssetType = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property
End Class
