
<Serializable()> _
Public Class AssetTypeOrigination : Inherits Common
    Private _Id As String
    Private _Description As String
    Private _OriginationID As String
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64

    Public Property Id() As String
        Get
            Return _Id
        End Get
        Set(ByVal Value As String)
            _Id = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property OriginationID() As String
        Get
            Return _OriginationID
        End Get
        Set(ByVal Value As String)
            _OriginationID = Value
        End Set
    End Property
End Class

