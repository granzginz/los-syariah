﻿
<Serializable()> _
Public Class AssetMasterPrice : Inherits Common
    Public Property AssetDescription As String
    Public Property EditAssetCode As String
    Public Property EditManufacturingYear As String
    Public Property AssetCode As String
    Public Property ManufacturingYear As String
    Public Property Price As Decimal
    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
End Class
