
<Serializable()> _
Public Class AssetTypeScheme : Inherits Common
    Private _Id As String
    Private _WhereCond_total As String
    Private _Description As String
    Private _AssetLevel As String
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _totalrecords_total As Int64

    Public Property Id() As String
        Get
            Return _Id
        End Get
        Set(ByVal Value As String)
            _Id = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property TotalRecords_total() As Int64
        Get
            Return _totalrecords_total
        End Get
        Set(ByVal Value As Int64)
            _totalrecords_total = Value
        End Set
    End Property

    Public Property AssetLevel() As String
        Get
            Return _AssetLevel
        End Get
        Set(ByVal Value As String)
            _AssetLevel = Value
        End Set
    End Property

    Public Property WhereCond_total() As String
        Get
            Return _WhereCond_total
        End Get
        Set(ByVal Value As String)
            _WhereCond_total = Value
        End Set
    End Property

End Class

