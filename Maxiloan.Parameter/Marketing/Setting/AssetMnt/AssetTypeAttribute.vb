
<Serializable()> _
Public Class AssetTypeAttribute : Inherits Common
    Private _Id As String
    Private _Name As String
    Private _AttributeID As String
    Private _Type As String
    Private _Length As String
    Private _Change As String
    Private _listdataReport As DataSet
    Private _listdata As DataTable
    Private _totalrecords As Int64

    Public Property Id() As String
        Get
            Return _Id
        End Get
        Set(ByVal Value As String)
            _Id = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property Name() As String
        Get
            Return _Name
        End Get
        Set(ByVal Value As String)
            _Name = Value
        End Set
    End Property

    Public Property Type() As String
        Get
            Return _Type
        End Get
        Set(ByVal Value As String)
            _Type = Value
        End Set
    End Property

    Public Property Length() As String
        Get
            Return _Length
        End Get
        Set(ByVal Value As String)
            _Length = Value
        End Set
    End Property

    Public Property Change() As String
        Get
            Return _Change
        End Get
        Set(ByVal Value As String)
            _Change = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property AttributeID() As String
        Get
            Return _AttributeID
        End Get
        Set(ByVal Value As String)
            _AttributeID = Value
        End Set
    End Property
End Class

