
<Serializable()> _
Public Class AssetTypeDocumentList : Inherits Common
    Private _Id As String
    Private _Name As String
    Private _DocumentListID As String
    Private _MainDoc As String
    Private _UsedAsset As String
    Private _Value As String
    Private _NewAsset As String
    Private _listdataReport As DataSet
    Private _listdata As DataTable
    Private _totalrecords As Int64


    Public Property Id() As String
        Get
            Return _Id
        End Get
        Set(ByVal Value As String)
            _Id = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property Value() As String
        Get
            Return _Value
        End Get
        Set(ByVal Value As String)
            _Value = Value
        End Set
    End Property

    Public Property Name() As String
        Get
            Return _Name
        End Get
        Set(ByVal Value As String)
            _Name = Value
        End Set
    End Property

    Public Property UsedAsset() As String
        Get
            Return _UsedAsset
        End Get
        Set(ByVal Value As String)
            _UsedAsset = Value
        End Set
    End Property

    Public Property NewAsset() As String
        Get
            Return _NewAsset
        End Get
        Set(ByVal Value As String)
            _NewAsset = Value
        End Set
    End Property

    Public Property MainDoc() As String
        Get
            Return _MainDoc
        End Get
        Set(ByVal Value As String)
            _MainDoc = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property DocumentListID() As String
        Get
            Return _DocumentListID
        End Get
        Set(ByVal Value As String)
            _DocumentListID = Value
        End Set
    End Property

    Public Property CustomerType As String
    Public Property Origination As String
    Public Property IsNoRequired As String
End Class

