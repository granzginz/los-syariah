﻿<Serializable()> _
Public Class PAcuanBungaIntern
    Inherits Maxiloan.Parameter.Common

    Public Property AcuanID As String
    Public Property Description As String
    Public Property UsedNew As String
    Public Property ManfcYearFrom As String
    Public Property ManfcYearTo As String
    Public Property RateType As String
    Public Property Tenor As String
    Public Property Rate As String

    Public Property JenisKredit As String
    Public Property KategoriAsset As String

    Public Property AddEdit As String
    Public Property ListData As DataTable
    Public Property RecordCount As Integer

    Public Property ArrUsage As New ArrayList
    Public Property ArrCategory As New ArrayList
    Public Property ArrList As New ArrayList
End Class

