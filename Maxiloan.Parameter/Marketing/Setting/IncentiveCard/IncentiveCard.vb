
<Serializable()> _
Public Class IncentiveCard : Inherits Common
#Region "PrivateConst"
    Private _listReport As DataSet
    Private _ListData As DataTable
    Private _CardID As Integer
    Private _SPName As String
    Private _CardName As String
    Private _ValidFrom As Date
    Private _ValidUntil As Date
    Private _MinAFIncentiveUnit As Decimal
    Private _IntervalUnit As Integer
    Private _IntervalAF As Integer
    Private _RefundPremi As Decimal
    Private _CategoryID As String
    Private _GMAmount As Decimal
    Private _BMAmount As Decimal
    Private _ADHAmount As Decimal
    Private _SPVAmount As Decimal
    Private _SPAmount As Decimal
    Private _ADAmount As Decimal
    Private _CompanyAmount As Decimal
    Private _SalesmanID As String
    Private _SalesSupervisorID As String
    Private _SupplierAdminID As String
    Private _ApplicationID As String
    Private _SupplierID As String
    Private _IncentiveRest As Decimal
    Private _SupplierEmployeePosition As String
    Private _RefundInterest As Decimal

    

#End Region
#Region "IncentiveMaintenance"
    Public Property ValidFrom() As Date
        Get
            Return _ValidFrom
        End Get
        Set(ByVal Value As Date)
            _ValidFrom = Value
        End Set
    End Property

    Public Property ValidUntil() As Date
        Get
            Return _ValidUntil
        End Get
        Set(ByVal Value As Date)
            _ValidUntil = Value
        End Set
    End Property
    Public Property ListReport() As DataSet
        Get
            Return _listReport
        End Get
        Set(ByVal Value As DataSet)
            _listReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal Value As DataTable)
            _ListData = Value
        End Set
    End Property
    Public Property CardID() As Integer
        Get
            Return _CardID
        End Get
        Set(ByVal Value As Integer)
            _CardID = Value
        End Set
    End Property
    Public Property SPName() As String
        Get
            Return _SPName
        End Get
        Set(ByVal Value As String)
            _SPName = Value
        End Set
    End Property
    Public Property CardName() As String
        Get
            Return _CardName
        End Get
        Set(ByVal Value As String)
            _CardName = Value
        End Set
    End Property
    Public Property MinAFIncentiveUnit() As Decimal
        Get
            Return _MinAFIncentiveUnit
        End Get
        Set(ByVal Value As Decimal)
            _MinAFIncentiveUnit = Value
        End Set
    End Property
    Public Property IntervalUnit() As Integer
        Get
            Return _IntervalUnit
        End Get
        Set(ByVal Value As Integer)
            _IntervalUnit = Value
        End Set
    End Property
    Public Property IntervalAF() As Integer
        Get
            Return _IntervalAF
        End Get
        Set(ByVal Value As Integer)
            _IntervalAF = Value
        End Set
    End Property
    Public Property RefundPremi() As Decimal
        Get
            Return _RefundPremi
        End Get
        Set(ByVal Value As Decimal)
            _RefundPremi = Value
        End Set
    End Property
    Public Property CategoryID() As String
        Get
            Return _CategoryID
        End Get
        Set(ByVal Value As String)
            _CategoryID = Value
        End Set
    End Property
#End Region
#Region "NewIncentiveData"
    Public Property CompanyAmount() As Decimal
        Get
            Return _CompanyAmount
        End Get
        Set(ByVal Value As Decimal)
            _CompanyAmount = Value
        End Set
    End Property
    Public Property GMAmount() As Decimal
        Get
            Return _GMAmount
        End Get
        Set(ByVal Value As Decimal)
            _GMAmount = Value
        End Set
    End Property
    Public Property BMAmount() As Decimal
        Get
            Return _BMAmount
        End Get
        Set(ByVal Value As Decimal)
            _BMAmount = Value
        End Set
    End Property
    Public Property ADHAmount() As Decimal
        Get
            Return _ADHAmount
        End Get
        Set(ByVal Value As Decimal)
            _ADHAmount = Value
        End Set
    End Property
    Public Property SPVAmount() As Decimal
        Get
            Return _SPVAmount
        End Get
        Set(ByVal Value As Decimal)
            _SPVAmount = Value
        End Set
    End Property
    Public Property SPAmount() As Decimal
        Get
            Return _SPAmount
        End Get
        Set(ByVal Value As Decimal)
            _SPAmount = Value
        End Set
    End Property
    Public Property ADAmount() As Decimal
        Get
            Return _ADAmount
        End Get
        Set(ByVal Value As Decimal)
            _ADAmount = Value
        End Set
    End Property
    Public Property SalesmanID() As String
        Get
            Return _SalesmanID
        End Get
        Set(ByVal Value As String)
            _SalesmanID = Value
        End Set
    End Property
    Public Property SalesSupervisorID() As String
        Get
            Return _SalesSupervisorID
        End Get
        Set(ByVal Value As String)
            _SalesSupervisorID = Value
        End Set
    End Property
    Public Property SupplierAdminID() As String
        Get
            Return _SupplierAdminID
        End Get
        Set(ByVal Value As String)
            _SupplierAdminID = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property
    Public Property SupplierID() As String
        Get
            Return _SupplierID
        End Get
        Set(ByVal Value As String)
            _SupplierID = Value
        End Set
    End Property
#End Region
    Public Property IncentiveRest() As Decimal
        Get
            Return _IncentiveRest
        End Get
        Set(ByVal Value As Decimal)
            _IncentiveRest = Value
        End Set
    End Property
    Public Property SupplierEmployeePosition() As String
        Get
            Return _SupplierEmployeePosition
        End Get
        Set(ByVal Value As String)
            _SupplierEmployeePosition = Value
        End Set
    End Property
    Public Property RefundInterest() As Decimal
        Get
            Return _RefundInterest
        End Get
        Set(ByVal Value As Decimal)
            _RefundInterest = Value
        End Set
    End Property
End Class
