
<Serializable()> _
Public Class Credit : Inherits Common
    Private _Id As String
    Private _Description As String
    Private _ScoringType As String
    Private _GroupComponent As String
    Private _SeqComponent As Integer
    Private _CalculationType As String
    Private _SQLCmd As String
    Private _DataSource As String
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _SPName As String

    Public Property SPName() As String
        Get
            Return _SPName
        End Get
        Set(ByVal Value As String)
            _SPName = Value
        End Set
    End Property
    Public Property Id() As String
        Get
            Return _Id
        End Get
        Set(ByVal Value As String)
            _Id = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property ScoringType() As String
        Get
            Return _ScoringType
        End Get
        Set(ByVal Value As String)
            _ScoringType = Value
        End Set
    End Property

    Public Property GroupComponent() As String
        Get
            Return _GroupComponent
        End Get
        Set(ByVal Value As String)
            _GroupComponent = Value
        End Set
    End Property

    Public Property SeqComponent() As Integer
        Get
            Return _SeqComponent
        End Get
        Set(ByVal Value As Integer)
            _SeqComponent = Value
        End Set
    End Property

    Public Property CalculationType() As String
        Get
            Return _CalculationType
        End Get
        Set(ByVal Value As String)
            _CalculationType = Value
        End Set
    End Property


    Public Property SQLCmd() As String
        Get
            Return _SQLCmd
        End Get
        Set(ByVal Value As String)
            _SQLCmd = Value
        End Set
    End Property

    Public Property DataSource() As String
        Get
            Return _DataSource
        End Get
        Set(ByVal Value As String)
            _DataSource = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

End Class
