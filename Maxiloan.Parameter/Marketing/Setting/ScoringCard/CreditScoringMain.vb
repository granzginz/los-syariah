
<Serializable()> _
Public Class CreditScoringMain : Inherits Common
    Private _CreditScoreShemeId As String
    Private _CreditScoreComponentId As String
    Private _Description As String
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Integer
    Private _PersonalApprovedScore As Double
    Private _PersonalRejectScore As Double
    ''CutOff

    Private _CutOffBefore As Double
    Private _CutOffAfter As Double

    Private _CompanyApprovedScore As Double
    Private _CompanyRejectScore As Double
    Private _ValueContent As String
    Private _ValueDescription As String
    Private _ValueFrom As Double
    Private _ValueTo As Double
    Private _ScoreValue As Integer
    Private _ScoreStatus As String
    Private _ContentSeqNo As Integer
    Private _IsChecked As Boolean
    Private _Weight As Int16
    Private _dtPersonalCustomer As DataTable
    Private _dtPersonalAsset As DataTable
    Private _dtPersonalFinancial As DataTable
    Private _dtCompanyCustomer As DataTable
    Private _dtCompanyAsset As DataTable
    Private _dtCompanyFinancial As DataTable
    Private _SPName As String
    Private _Package As String
    Private _ScoreSchemeID As String
    Private _CutOffScoreNewCustomer As Integer
    Private _CutOffScoreExistingCustomer As Integer

    Public Property SPName() As String
        Get
            Return _SPName
        End Get
        Set(ByVal Value As String)
            _SPName = Value
        End Set
    End Property
    Public Property dtPersonalCustomer() As DataTable
        Get
            Return _dtPersonalCustomer
        End Get
        Set(ByVal Value As DataTable)
            _dtPersonalCustomer = Value
        End Set
    End Property

    Public Property dtPersonalAsset() As DataTable
        Get
            Return _dtPersonalAsset
        End Get
        Set(ByVal Value As DataTable)
            _dtPersonalAsset = Value
        End Set
    End Property

    Public Property dtPersonalFinancial() As DataTable
        Get
            Return _dtPersonalFinancial
        End Get
        Set(ByVal Value As DataTable)
            _dtPersonalFinancial = Value
        End Set
    End Property

    Public Property dtCompanyCustomer() As DataTable
        Get
            Return _dtCompanyCustomer
        End Get
        Set(ByVal Value As DataTable)
            _dtCompanyCustomer = Value
        End Set
    End Property

    Public Property dtCompanyAsset() As DataTable
        Get
            Return _dtCompanyAsset
        End Get
        Set(ByVal Value As DataTable)
            _dtCompanyAsset = Value
        End Set
    End Property

    Public Property dtCompanyFinancial() As DataTable
        Get
            Return _dtCompanyFinancial
        End Get
        Set(ByVal Value As DataTable)
            _dtCompanyFinancial = Value
        End Set
    End Property

    Public Property IsChecked() As Boolean
        Get
            Return _IsChecked
        End Get
        Set(ByVal Value As Boolean)
            _IsChecked = Value
        End Set
    End Property

    Public Property Weight() As Int16
        Get
            Return _Weight
        End Get
        Set(ByVal Value As Int16)
            _Weight = Value
        End Set
    End Property

    Public Property PersonalApprovedScore() As Double
        Get
            Return _PersonalApprovedScore
        End Get
        Set(ByVal Value As Double)
            _PersonalApprovedScore = Value
        End Set
    End Property

    Public Property PersonalRejectScore() As Double
        Get
            Return _PersonalRejectScore
        End Get
        Set(ByVal Value As Double)
            _PersonalRejectScore = Value
        End Set
    End Property

    ''cutoff
    Public Property CutOffBefore() As Double
        Get
            Return _CutOffBefore
        End Get
        Set(ByVal Value As Double)
            _CutOffBefore = Value
        End Set
    End Property

    Public Property CutOffAfter() As Double
        Get
            Return _CutOffAfter
        End Get
        Set(ByVal Value As Double)
            _CutOffAfter = Value
        End Set
    End Property


    Public Property CompanyApprovedScore() As Double
        Get
            Return _CompanyApprovedScore
        End Get
        Set(ByVal Value As Double)
            _CompanyApprovedScore = Value
        End Set
    End Property

    Public Property CompanyRejectScore() As Double
        Get
            Return _CompanyRejectScore
        End Get
        Set(ByVal Value As Double)
            _CompanyRejectScore = Value
        End Set
    End Property

    Public Property CreditScoreShemeId() As String
        Get
            Return _CreditScoreShemeId
        End Get
        Set(ByVal Value As String)
            _CreditScoreShemeId = Value
        End Set
    End Property

    Public Property CreditScoreComponentId() As String
        Get
            Return _CreditScoreComponentId
        End Get
        Set(ByVal Value As String)
            _CreditScoreComponentId = Value
        End Set
    End Property


    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property


    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Integer
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Integer)
            _totalrecords = Value
        End Set
    End Property

    Public Property ValueContent() As String
        Get
            Return _ValueContent
        End Get
        Set(ByVal Value As String)
            _ValueContent = Value
        End Set
    End Property

    Public Property ValueDescription() As String
        Get
            Return _ValueDescription
        End Get
        Set(ByVal Value As String)
            _ValueDescription = Value
        End Set
    End Property

    Public Property ValueFrom() As Double
        Get
            Return CType(_ValueFrom, Double)
        End Get
        Set(ByVal Value As Double)
            _ValueFrom = Value
        End Set
    End Property

    Public Property ValueTo() As Double
        Get
            Return CType(_ValueTo, Double)
        End Get
        Set(ByVal Value As Double)
            _ValueTo = Value
        End Set
    End Property

    Public Property ScoreValue() As Integer
        Get
            Return _ScoreValue
        End Get
        Set(ByVal Value As Integer)
            _ScoreValue = Value
        End Set
    End Property

    Public Property ScoreStatus() As String
        Get
            Return _ScoreStatus
        End Get
        Set(ByVal Value As String)
            _ScoreStatus = Value
        End Set
    End Property

    Public Property ContentSeqNo() As Integer
        Get
            Return _ContentSeqNo
        End Get
        Set(ByVal Value As Integer)
            _ContentSeqNo = Value
        End Set
    End Property

    Public Property Package() As String
        Get
            Return _Package
        End Get
        Set(ByVal Value As String)
            _Package = Value
        End Set
    End Property

    Public Property ScoreSchemeID() As String
        Get
            Return _ScoreSchemeID
        End Get
        Set(ByVal Value As String)
            _ScoreSchemeID = Value
        End Set
    End Property

    Public Property CutOffScoreNewCustomer() As Integer
        Get
            Return _CutOffScoreNewCustomer
        End Get
        Set(ByVal Value As Integer)
            _CutOffScoreNewCustomer = Value
        End Set
    End Property

    Public Property CutOffScoreExistingCustomer() As Integer
        Get
            Return _CutOffScoreExistingCustomer
        End Get
        Set(ByVal Value As Integer)
            _CutOffScoreExistingCustomer = Value
        End Set
    End Property
End Class
