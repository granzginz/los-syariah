
<Serializable()> _
Public Class CreditScoreGradeMaster : Inherits Common

    Public Property CreditScoreSchemeID As String
    Public Property CustType As String
    Public Property GradeID As Integer
    Public Property GradeDescription As String
    Public Property ScoreFrom As Integer
    Public Property ScoreTo As String

    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64

End Class
