
<Serializable()> _
Public Class MktNewApp : Inherits Common
    Private _applicationId As String
    Private _prospectappid As String
    Private _listdata As DataTable
    Private _prospectappdate As Date
    Private _PersonalNPWP As String



   
    '==========COMPANY DATA======
    Private _companyname As String
    Private _industrytype As String
    Private _businessplacestatus As String
    Private _yearinbusiness As String
    Private _customertype As String

    '======PERSONAL DATA====
   
    Private _name As String
    Private _idtype As String
    Private _idnumber As String
    Private _birthplace As String
    Private _birthdate As Date
    Private _homestatus As String
    Private _gender As String

    '=========ADDRESS=====
  
    Private _address As String
    Private _rt As String
    Private _rw As String
    Private _kelurahan As String
    Private _kecamatan As String
    Private _city As String
    Private _zipcode As String
    Private _areaphone1 As String
    Private _areaphone2 As String
    Private _phone1 As String
    Private _phone2 As String
    Private _areafak As String
    Private _fakno As String

    Private _mobilephone As String
    Private _email As String
    '====WORK DATA =========    
    Private _profession As String
    Private _lengthofemployment As Integer
    '======AGREEMENT DATA ======== 
    Private _product As String
    Private _ProductOfferingId As String
    Private _assetdescription As String
    Private _otrPrice As Double
    Private _downpayment As Double
    Private _tenor As Double
    Private _flaterate As Double
    Private _firstintallment As String
    Private _installamount As Double
    Private _estimateduedate As Date
    Private _insuranceco As String
    Private _InsuranceCoBranch As String
    Private _existingpolicyno As String
    Private _AssetTypeID As String
    Public Property AssetTypeID() As String
        Get
            Return _AssetTypeID
        End Get
        Set(ByVal Value As String)
            _AssetTypeID = Value
        End Set
    End Property
    Public Property InsuranceCoBranch() As String
        Get
            Return _InsuranceCoBranch
        End Get
        Set(ByVal Value As String)
            _InsuranceCoBranch = Value
        End Set
    End Property
    Public Property ProductOfferingId() As String
        Get
            Return _ProductOfferingId
        End Get
        Set(ByVal Value As String)
            _ProductOfferingId = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property ApplicationId() As String
        Get
            Return _applicationId
        End Get
        Set(ByVal Value As String)
            _applicationId = Value
        End Set
    End Property

    Public Property ProspectAppId() As String
        Get
            Return _prospectappid
        End Get
        Set(ByVal Value As String)
            _prospectappid = Value
        End Set
    End Property

    Public Property CompanyName() As String
        Get
            Return _companyname
        End Get
        Set(ByVal Value As String)
            _companyname = Value
        End Set
    End Property
    Public Property IndustryType() As String
        Get
            Return _industrytype
        End Get
        Set(ByVal Value As String)
            _industrytype = Value
        End Set
    End Property

    Public Property BusinessPlaceStatus() As String
        Get
            Return _businessplacestatus
        End Get
        Set(ByVal Value As String)
            _businessplacestatus = Value
        End Set
    End Property
    Public Property YearInBusiness() As String
        Get
            Return _yearinbusiness
        End Get
        Set(ByVal Value As String)
            _yearinbusiness = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal Value As String)
            _name = Value
        End Set
    End Property
    Public Property IdType() As String
        Get
            Return _idtype
        End Get
        Set(ByVal Value As String)
            _idtype = Value
        End Set
    End Property
    Public Property IdNumber() As String
        Get
            Return _idnumber
        End Get
        Set(ByVal Value As String)
            _idnumber = Value
        End Set
    End Property
    Public Property BirthPlace() As String
        Get
            Return _birthplace
        End Get
        Set(ByVal Value As String)
            _birthplace = Value
        End Set
    End Property
    Public Property BirthDate() As Date
        Get
            Return _birthdate
        End Get
        Set(ByVal Value As Date)
            _birthdate = Value
        End Set
    End Property
    Public Property HomeStatus() As String
        Get
            Return _homestatus
        End Get
        Set(ByVal Value As String)
            _homestatus = Value
        End Set
    End Property

    Public Property Address() As String
        Get
            Return _address
        End Get
        Set(ByVal Value As String)
            _address = Value
        End Set
    End Property
    Public Property RT() As String
        Get
            Return _rt
        End Get
        Set(ByVal Value As String)
            _rt = Value
        End Set
    End Property
    Public Property RW() As String
        Get
            Return _rw
        End Get
        Set(ByVal Value As String)
            _rw = Value
        End Set
    End Property
    Public Property Kelurahan() As String
        Get
            Return _kelurahan
        End Get
        Set(ByVal Value As String)
            _kelurahan = Value
        End Set
    End Property
    Public Property Kecamatan() As String
        Get
            Return _kecamatan
        End Get
        Set(ByVal Value As String)
            _kecamatan = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return _city
        End Get
        Set(ByVal Value As String)
            _city = Value
        End Set
    End Property
    Public Property ZIPCode() As String
        Get
            Return _zipcode
        End Get
        Set(ByVal Value As String)
            _zipcode = Value
        End Set
    End Property
    Public Property AreaPhone1() As String
        Get
            Return _areaphone1
        End Get
        Set(ByVal Value As String)
            _areaphone1 = Value
        End Set
    End Property
    Public Property AreaPhone2() As String
        Get
            Return _areaphone2
        End Get
        Set(ByVal Value As String)
            _areaphone2 = Value
        End Set
    End Property
    Public Property Phone1() As String
        Get
            Return _phone1
        End Get
        Set(ByVal Value As String)
            _phone1 = Value
        End Set
    End Property
    Public Property Phone2() As String
        Get
            Return _phone2
        End Get
        Set(ByVal Value As String)
            _phone2 = Value
        End Set
    End Property
    Public Property AreaFak() As String
        Get
            Return _areafak
        End Get
        Set(ByVal Value As String)
            _areafak = Value
        End Set
    End Property
    Public Property FakNo() As String
        Get
            Return _fakno
        End Get
        Set(ByVal Value As String)
            _fakno = Value
        End Set
    End Property
    Public Property mobilephone() As String
        Get
            Return _mobilephone
        End Get
        Set(ByVal Value As String)
            _mobilephone = Value
        End Set
    End Property
    Public Property Email() As String
        Get
            Return _email
        End Get
        Set(ByVal Value As String)
            _email = Value
        End Set
    End Property
    Public Property Profession() As String
        Get
            Return _profession
        End Get
        Set(ByVal Value As String)
            _profession = Value
        End Set
    End Property
    Public Property LengthOfEmployment() As Integer
        Get
            Return _lengthofemployment
        End Get
        Set(ByVal Value As Integer)
            _lengthofemployment = Value
        End Set
    End Property
    Public Property Product() As String
        Get
            Return _product
        End Get
        Set(ByVal Value As String)
            _product = Value
        End Set
    End Property
    Public Property AssetDescription() As String
        Get
            Return _assetdescription
        End Get
        Set(ByVal Value As String)
            _assetdescription = Value
        End Set
    End Property

    Public Property OTRPrice() As Double
        Get
            Return _otrPrice
        End Get
        Set(ByVal Value As Double)
            _otrPrice = Value
        End Set
    End Property
    Public Property DownPayment() As Double
        Get
            Return _downpayment
        End Get
        Set(ByVal Value As Double)
            _downpayment = Value
        End Set
    End Property
    Public Property Tenor() As Double
        Get
            Return _tenor
        End Get
        Set(ByVal Value As Double)
            _tenor = Value
        End Set
    End Property
    Public Property FlateRate() As Double
        Get
            Return _flaterate
        End Get
        Set(ByVal Value As Double)
            _flaterate = Value
        End Set
    End Property
    Public Property FirstIntallment() As String
        Get
            Return _firstintallment
        End Get
        Set(ByVal Value As String)
            _firstintallment = Value
        End Set
    End Property
    Public Property InstallAmount() As Double
        Get
            Return _installamount
        End Get
        Set(ByVal Value As Double)
            _installamount = Value
        End Set
    End Property
    Public Property Estimateduedate() As Date
        Get
            Return _estimateduedate
        End Get
        Set(ByVal Value As Date)
            _estimateduedate = Value
        End Set
    End Property
    Public Property InsuranceCo() As String
        Get
            Return _insuranceco
        End Get
        Set(ByVal Value As String)
            _insuranceco = Value
        End Set
    End Property
    Public Property ExistingPolicyNo() As String
        Get
            Return _existingpolicyno
        End Get
        Set(ByVal Value As String)
            _existingpolicyno = Value
        End Set
    End Property
    Public Property Gender() As String
        Get
            Return _gender
        End Get
        Set(ByVal Value As String)
            _gender = Value
        End Set
    End Property
    Public Property CustomerType() As String
        Get
            Return _customertype
        End Get
        Set(ByVal Value As String)
            _customertype = Value
        End Set
    End Property
    Public Property ProspectAppDate() As Date
        Get
            Return _prospectappdate
        End Get
        Set(ByVal Value As Date)
            _prospectappdate = Value
        End Set
    End Property
    Public Property PersonalNPWP() As String
        Get
            Return _PersonalNPWP
        End Get
        Set(ByVal Value As String)
            _PersonalNPWP = Value
        End Set
    End Property

End Class
