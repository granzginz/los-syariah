﻿<Serializable()>
Public Class ScoreComponentContent

    Public Sub New(_ScoreSchemeID As String, _ScoreComponentID As String, _ValueContent As String, _ValueDescription As String, _ValueFrom As Decimal, _ValueTo As Decimal, _ScoreValue As Decimal, _ScoreAbsolute As Decimal, _ScoreStatus As String, _ContentSeqNo As Integer)
        ScoreSchemeID = _ScoreSchemeID
        ScoreComponentID = _ScoreComponentID
        ValueContent = _ValueContent
        ValueDescription = _ValueDescription
        ValueFrom = _ValueFrom
        ValueTo = _ValueTo
        ScoreValue = _ScoreValue
        ScoreStatus = _ScoreStatus
        ContentSeqNo = _ContentSeqNo
        ScoreAbsolute = _ScoreAbsolute
    End Sub

    Public Property ScoreSchemeID As String
    Public Property ScoreComponentID As String
    Public Property ValueContent As String
    Public Property ValueDescription As String
    Public Property ValueFrom As Decimal
    Public Property ValueTo As Decimal
    Public Property ScoreValue As Decimal
    Public Property ScoreStatus As String
    Public Property ContentSeqNo As Integer
    Public Property ScoreAbsolute As Decimal
End Class
