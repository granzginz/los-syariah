
<Serializable()> _
Public Class GroupSupplierAccount : Inherits Common

    Public Property ID As Integer
    Public Property SupplierGroupID As String
    Public Property GroupSupplierAccountBankID As String
    Public Property GroupSupplierAccountBankName As String
    Public Property GroupSupplierAccountBankBranchID As Integer
    Public Property GroupSupplierAccountBankBranch As String
    Public Property GroupSupplierAccountNo As String
    Public Property GroupSupplierAccountName As String
    Public Property DefaultAccount As Boolean
    Public Property ForPayment As String
    Public Property EffectiveDate As DateTime
    Public Property BankCity As String
    Public Property SandiCabang As String
    Public Property SandiBank As String
    Public Property UsrUpd As String
    Public Property DtmUpd As DateTime
    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64

End Class
