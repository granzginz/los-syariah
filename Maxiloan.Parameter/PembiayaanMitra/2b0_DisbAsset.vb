﻿

Imports System.Data.SqlClient

Public Class DisbAsset
    Inherits BaseDisb
    Protected Sub New() '_mfId As String, _mfBatchNo As String, _revNo As Int16, _mfAgreementNo As String, _mfcif As String)
        MyBase.New("", "", 0, "", "")
    End Sub
    Sub New(oriSplit As String())
        MyBase.New(oriSplit(0), oriSplit(1), 0, oriSplit(2), oriSplit(3))

        MfBranch = oriSplit(4)
        AssetTypeID = oriSplit(5)
        AssetCode = oriSplit(6)
        Merk = oriSplit(7)
        AssetDescription = oriSplit(8)
        AssetCategory = oriSplit(9)
        AssetCondition = oriSplit(10)
        AssetUsage = oriSplit(11)
        NoPolisi = oriSplit(12)
        ChasisNo = oriSplit(13)
        EngineNo = oriSplit(14)
        Colour = oriSplit(15)
        MfgYear = oriSplit(16)
        MfgCountry = oriSplit(17)
        NoFaktur = oriSplit(18)
        Nik = oriSplit(19)
        CollateralTypeID = oriSplit(20)
        CollateralName = oriSplit(21)
        CollaterealNo = oriSplit(22)
        CollateralCityIssued = oriSplit(23)
        CollateralIssuedDate = DateTime.ParseExact(dFmt(oriSplit(24)), "yyyyMMdd", Nothing)
        CollateralAddress = oriSplit(25)
        CollateralDati2 = oriSplit(26)
        CollateralValue = oriSplit(27)
    End Sub
    Public Shared Function Instance() As DisbAsset
        Return New DisbAsset()
    End Function
    Public Overrides Function addToList(oriSplit() As String) As AbsDisb

        Try
            Return New DisbAsset(oriSplit)
        Catch ex As Exception
            Throw New Exception($"Error parse file DisbAsset. {ex.Message}")
        End Try

    End Function
    Public ReadOnly Property MfBranch As String
    Public ReadOnly Property AssetTypeID As String
    Public ReadOnly Property AssetCode As String
    Public ReadOnly Property Merk As String
    Public ReadOnly Property AssetDescription As String
    Public ReadOnly Property AssetCategory As String
    Public ReadOnly Property AssetCondition As String
    Public ReadOnly Property AssetUsage As String
    Public ReadOnly Property NoPolisi As String
    Public ReadOnly Property ChasisNo As String
    Public ReadOnly Property EngineNo As String
    Public ReadOnly Property Colour As String
    Public ReadOnly Property MfgYear As String
    Public ReadOnly Property MfgCountry As String
    Public ReadOnly Property NoFaktur As String
    Public ReadOnly Property Nik As String
    Public ReadOnly Property CollateralTypeID As String
    Public ReadOnly Property CollateralName As String
    Public ReadOnly Property CollaterealNo As String
    Public ReadOnly Property CollateralCityIssued As String
    Public ReadOnly Property CollateralIssuedDate As DateTime
    Public ReadOnly Property CollateralAddress As String
    Public ReadOnly Property CollateralDati2 As String
    Public ReadOnly Property CollateralValue As Decimal



    Const _spName As String = "spDisb_Asset_add"
    Public Overrides Function command(cnn As SqlClient.SqlConnection) As SqlClient.SqlCommand
        Dim sqlcommand = New SqlClient.SqlCommand(_spName, cnn)
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.Parameters.Clear()
        sqlcommand.Parameters.Add(New SqlParameter("@mFID", SqlDbType.Char, 10) With {.Value = MfId})
        sqlcommand.Parameters.Add(New SqlParameter("@mFBatchNo", SqlDbType.Char, 20) With {.Value = MfBatchNo})
        sqlcommand.Parameters.Add(New SqlParameter("@mFAgreementNo", SqlDbType.Char, 20) With {.Value = MfAgreementNo})
        sqlcommand.Parameters.Add(New SqlParameter("@mFCIF", SqlDbType.Char, 20) With {.Value = MfCif})
        sqlcommand.Parameters.Add(New SqlParameter("@mFBranch", SqlDbType.Char, 3) With {.Value = MfBranch})
        sqlcommand.Parameters.Add(New SqlParameter("@assetTypeID", SqlDbType.Char, 10) With {.Value = AssetTypeID})
        sqlcommand.Parameters.Add(New SqlParameter("@assetCode", SqlDbType.Char, 20) With {.Value = AssetCode})
        sqlcommand.Parameters.Add(New SqlParameter("@merk", SqlDbType.Char, 30) With {.Value = Merk})
        sqlcommand.Parameters.Add(New SqlParameter("@assetDescription", SqlDbType.Char, 50) With {.Value = AssetDescription})
        sqlcommand.Parameters.Add(New SqlParameter("@assetCategory", SqlDbType.Char, 4) With {.Value = AssetCategory})
        sqlcommand.Parameters.Add(New SqlParameter("@assetCondition", SqlDbType.Char, 1) With {.Value = AssetCondition})
        sqlcommand.Parameters.Add(New SqlParameter("@assetUsage", SqlDbType.Char, 1) With {.Value = AssetUsage})
        sqlcommand.Parameters.Add(New SqlParameter("@noPolisi", SqlDbType.Char, 10) With {.Value = NoPolisi})
        sqlcommand.Parameters.Add(New SqlParameter("@chasisNo", SqlDbType.Char, 25) With {.Value = ChasisNo})
        sqlcommand.Parameters.Add(New SqlParameter("@engineNo", SqlDbType.Char, 15) With {.Value = EngineNo})
        sqlcommand.Parameters.Add(New SqlParameter("@colour", SqlDbType.Char, 30) With {.Value = Colour})
        sqlcommand.Parameters.Add(New SqlParameter("@mfgYear", SqlDbType.Char, 4) With {.Value = MfgYear})
        sqlcommand.Parameters.Add(New SqlParameter("@mfgCountry", SqlDbType.Char, 3) With {.Value = MfgCountry})
        sqlcommand.Parameters.Add(New SqlParameter("@noFaktur", SqlDbType.Char, 30) With {.Value = NoFaktur})
        sqlcommand.Parameters.Add(New SqlParameter("@nIK", SqlDbType.Char, 30) With {.Value = Nik})
        sqlcommand.Parameters.Add(New SqlParameter("@collateralTypeID", SqlDbType.Char, 4) With {.Value = CollateralTypeID})
        sqlcommand.Parameters.Add(New SqlParameter("@collateralName", SqlDbType.Char, 50) With {.Value = CollateralName})
        sqlcommand.Parameters.Add(New SqlParameter("@collaterealNo", SqlDbType.Char, 20) With {.Value = CollaterealNo})
        sqlcommand.Parameters.Add(New SqlParameter("@collateralCityIssued", SqlDbType.Char, 50) With {.Value = CollateralCityIssued})
        sqlcommand.Parameters.Add(New SqlParameter("@collateralIssuedDate", SqlDbType.Date) With {.Value = CollateralIssuedDate})
        sqlcommand.Parameters.Add(New SqlParameter("@collateralAddress", SqlDbType.Char, 100) With {.Value = CollateralAddress})
        sqlcommand.Parameters.Add(New SqlParameter("@collateralDati2", SqlDbType.Char, 4) With {.Value = CollateralDati2})
        sqlcommand.Parameters.Add(New SqlParameter("@collateralValue", SqlDbType.Decimal) With {.Value = CollateralValue})
        Return sqlcommand

    End Function


    Protected Overrides Sub doValidate()
        If (String.IsNullOrEmpty(MfAgreementNo)) Then build.AppendLine("DisbAsset MFAgreementNo kosong. </br>")

        If (String.IsNullOrEmpty(MfId)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " MFID kosong. </br>")
        If (String.IsNullOrEmpty(MfBatchNo)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " MFBatchNo kosong. </br>")

        If (String.IsNullOrEmpty(MfCif)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " MFCIF kosong. </br>")
        If (String.IsNullOrEmpty(MfBranch)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " MFBranch kosong. </br>")
        If (String.IsNullOrEmpty(AssetTypeID)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " AssetTypeID kosong. </br>")
        If (String.IsNullOrEmpty(AssetCode)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " AssetCode kosong. </br>")
        If (String.IsNullOrEmpty(Merk)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " Merk kosong. </br>")
        If (String.IsNullOrEmpty(AssetDescription)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " AssetDescription kosong. </br>")
        If (String.IsNullOrEmpty(AssetCategory)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " AssetCategory kosong. </br>")
        If (String.IsNullOrEmpty(AssetCondition)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " AssetCondition kosong. </br>")
        If (String.IsNullOrEmpty(AssetUsage)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " AssetUsage kosong. </br>")
        If (String.IsNullOrEmpty(NoPolisi)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " NoPolisi kosong. </br>")
        If (String.IsNullOrEmpty(ChasisNo)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " ChasisNo kosong. </br>")
        If (String.IsNullOrEmpty(EngineNo)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " EngineNo kosong. </br>")
        If (String.IsNullOrEmpty(Colour)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " Colour kosong. </br>")
        If (String.IsNullOrEmpty(MfgYear)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " MfgYear kosong. </br>")
        If (String.IsNullOrEmpty(MfgCountry)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " MfgCountry kosong. </br>")
        'If (String.IsNullOrEmpty(NoFaktur)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " NoFaktur  kosong. </br>")
        If (String.IsNullOrEmpty(CollateralTypeID)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " CollateralTypeID kosong. </br>")
        If (String.IsNullOrEmpty(CollateralName)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " CollateralName kosong. </br>")
        If (String.IsNullOrEmpty(CollaterealNo)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " CollaterealNo kosong. </br>")
        If (String.IsNullOrEmpty(CollateralCityIssued)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " CollateralCityIssued kosong. </br>")
        If (String.IsNullOrEmpty(CollateralIssuedDate)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " CollateralIssuedDate kosong. </br>")
        If (String.IsNullOrEmpty(CollateralAddress)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " CollateralAddress kosong. </br>")
        If (String.IsNullOrEmpty(CollateralDati2)) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " CollateralDati2 kosong. </br>")
        If (CollateralValue <= 0) Then build.AppendLine("FILE DISBASSET MFAgreementNo:" & MfAgreementNo & " CollateralValue kosong. </br>")

    End Sub
End Class