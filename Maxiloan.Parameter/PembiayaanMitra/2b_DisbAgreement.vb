﻿
Imports System.Data.SqlClient

Public Class DisbAgreement
    Inherits BaseDisb
    Protected Sub New()
        MyBase.New("", "", 0, "", "")
    End Sub
    Sub New(oriSplit As String())
        MyBase.New(oriSplit(0), oriSplit(1), 0, oriSplit(2), oriSplit(3))
        MfBranch = oriSplit(4)
        MfFacilityNo = oriSplit(5)
        MfBatchDate = DateTime.ParseExact(dFmt(oriSplit(6)), "yyyyMMdd", Nothing)
        AgreementDate = DateTime.ParseExact(dFmt(oriSplit(7)), "yyyyMMdd", Nothing)
        GoLiveDate = DateTime.ParseExact(dFmt(oriSplit(8)), "yyyyMMdd", Nothing)
        EffectiveDate = DateTime.ParseExact(dFmt(oriSplit(9)), "yyyyMMdd", Nothing)
        MaturityDate = DateTime.ParseExact(dFmt(oriSplit(10)), "yyyyMMdd", Nothing)
        MFFinancingType = oriSplit(11)
        MFPaymentSchme = oriSplit(12)
        TenorDebtor = oriSplit(13)
        TenorMf = oriSplit(14)
        LastPaidSeqNo = oriSplit(15)
        InstallmentScheme = oriSplit(16)
        TotalOTR = oriSplit(17)
        DownPayment = oriSplit(18)
        InsCapitalizedAmount = oriSplit(19)
        NTFDebtor = oriSplit(20)
        NTFToMF = oriSplit(21)
        BaloonValue = oriSplit(22)
        InterestType = oriSplit(23)
        EffectiveRateDebtor = oriSplit(24)
        EffectiveRateMF = oriSplit(25)
        FlatRateDebtor = oriSplit(26)
        PaymentFrequency = oriSplit(27)
        FirstPaymentType = oriSplit(28)
        InstAmountDebtor = oriSplit(29)
        InstAmountMF = oriSplit(30)
        TotalIncentif = oriSplit(31)
        TotalInterestAmount = oriSplit(32)
        AdminFee = oriSplit(33)
        InsuranceIncome = oriSplit(34)
        ProvisionFee = oriSplit(35)
        PaymentType = oriSplit(36)
        SifatKreditID = oriSplit(37)
        GolonganKredit = oriSplit(38)
        JenisPenggunaan = oriSplit(39)
        SektorEkonomi = oriSplit(40)
        Dati2LokasiProyek = oriSplit(41)
        Status = oriSplit(42)
    End Sub
    Public Overrides Function addToList(oriSplit() As String) As AbsDisb
        Try
            Return New DisbAgreement(oriSplit)
        Catch ex As Exception
            Throw New Exception($"Error parse file DisbAgreement.{vbCrLf}{ex.Message}")
        End Try
    End Function
    Public Shared Function Instance() As DisbAgreement
        Try
            Return New DisbAgreement()
        Catch ex As Exception
            Throw New Exception($"Error parse file DisbAgreement. {ex.Message}")
        End Try
    End Function

    Public ReadOnly Property MfBranch As String
    Public ReadOnly Property MfFacilityNo As String


    Public ReadOnly Property MfBatchDate As DateTime
    Public ReadOnly Property AgreementDate As DateTime
    Public ReadOnly Property GoLiveDate As DateTime
    Public ReadOnly Property EffectiveDate As DateTime
    Public ReadOnly Property MaturityDate As DateTime


    Public ReadOnly Property MFFinancingType As String
    Public ReadOnly Property MFPaymentSchme As String
    Public ReadOnly Property TenorDebtor As Decimal
    Public ReadOnly Property TenorMf As Decimal
    Public ReadOnly Property InstallmentScheme As String
    Public ReadOnly Property TotalOTR As Decimal
    Public ReadOnly Property DownPayment As Decimal
    Public ReadOnly Property InsCapitalizedAmount As Decimal
    Public ReadOnly Property NTFDebtor As Decimal
    Public ReadOnly Property NTFToMF As Decimal
    Public ReadOnly Property BaloonValue As Decimal
    Public ReadOnly Property InterestType As String
    Public ReadOnly Property EffectiveRateDebtor As Decimal
    Public ReadOnly Property EffectiveRateMF As Decimal
    Public ReadOnly Property FlatRateDebtor As Decimal
    Public ReadOnly Property PaymentFrequency As String
    Public ReadOnly Property FirstPaymentType As String
    Public ReadOnly Property InstAmountDebtor As Decimal
    Public ReadOnly Property InstAmountMF As Decimal
    Public ReadOnly Property TotalIncentif As Decimal
    Public ReadOnly Property TotalInterestAmount As Decimal
    Public ReadOnly Property AdminFee As Decimal
    Public ReadOnly Property InsuranceIncome As Decimal
    Public ReadOnly Property ProvisionFee As Decimal
    Public ReadOnly Property PaymentType As String
    Public ReadOnly Property SifatKreditID As String
    Public ReadOnly Property GolonganKredit As String
    Public ReadOnly Property JenisPenggunaan As String
    Public ReadOnly Property SektorEkonomi As String
    Public ReadOnly Property Dati2LokasiProyek As String
    Public ReadOnly Property Status As String

    Public ReadOnly Property LastPaidSeqNo As Decimal

    Private _insurance As DisbInsurance
    Private _asset As DisbAsset


    Const _spName As String = "spDisb_Agreement_add"



    Public Overrides Function command(cnn As SqlClient.SqlConnection) As SqlClient.SqlCommand
        Dim sqlcommand = New SqlClient.SqlCommand(_spName, cnn)

        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.Parameters.Clear()
        sqlcommand.Parameters.Add(New SqlParameter("@mFID", SqlDbType.Char, 10) With {.Value = MfId})
        sqlcommand.Parameters.Add(New SqlParameter("@mFBatchNo", SqlDbType.Char, 20) With {.Value = MfBatchNo})
        sqlcommand.Parameters.Add(New SqlParameter("@mFAgreementNo", SqlDbType.Char, 20) With {.Value = MfAgreementNo})
        sqlcommand.Parameters.Add(New SqlParameter("@mFCIF", SqlDbType.Char, 20) With {.Value = MfCif})
        sqlcommand.Parameters.Add(New SqlParameter("@mFBranch", SqlDbType.Char, 3) With {.Value = MfBranch})
        sqlcommand.Parameters.Add(New SqlParameter("@mFFacilityNo", SqlDbType.Char, 20) With {.Value = MfFacilityNo})
        sqlcommand.Parameters.Add(New SqlParameter("@mFBatchDate", SqlDbType.Date) With {.Value = MfBatchDate})
        sqlcommand.Parameters.Add(New SqlParameter("@agreementDate", SqlDbType.Date) With {.Value = AgreementDate})
        sqlcommand.Parameters.Add(New SqlParameter("@goLiveDate", SqlDbType.Date) With {.Value = GoLiveDate})
        sqlcommand.Parameters.Add(New SqlParameter("@effectiveDate", SqlDbType.Date) With {.Value = EffectiveDate})
        sqlcommand.Parameters.Add(New SqlParameter("@maturityDate", SqlDbType.Date) With {.Value = MaturityDate})
        sqlcommand.Parameters.Add(New SqlParameter("@mFFinancingType", SqlDbType.Char, 1) With {.Value = MFFinancingType})
        sqlcommand.Parameters.Add(New SqlParameter("@mFPaymentSchme", SqlDbType.Char, 2) With {.Value = MFPaymentSchme})
        sqlcommand.Parameters.Add(New SqlParameter("@tenorDebtor", SqlDbType.SmallInt) With {.Value = TenorDebtor})
        sqlcommand.Parameters.Add(New SqlParameter("@tenorMF", SqlDbType.SmallInt) With {.Value = TenorMf})
        sqlcommand.Parameters.Add(New SqlParameter("@installmentScheme", SqlDbType.Char, 2) With {.Value = InstallmentScheme})
        sqlcommand.Parameters.Add(New SqlParameter("@totalOTR", SqlDbType.Decimal) With {.Value = TotalOTR})
        sqlcommand.Parameters.Add(New SqlParameter("@downPayment", SqlDbType.Decimal) With {.Value = DownPayment})
        sqlcommand.Parameters.Add(New SqlParameter("@insCapitalizedAmount", SqlDbType.Decimal) With {.Value = InsCapitalizedAmount})
        sqlcommand.Parameters.Add(New SqlParameter("@nTFDebtor", SqlDbType.Decimal) With {.Value = NTFDebtor})
        sqlcommand.Parameters.Add(New SqlParameter("@nTFToMF", SqlDbType.Decimal) With {.Value = NTFToMF})
        sqlcommand.Parameters.Add(New SqlParameter("@baloonValue", SqlDbType.Decimal) With {.Value = BaloonValue})
        sqlcommand.Parameters.Add(New SqlParameter("@interestType", SqlDbType.Char, 1) With {.Value = InterestType})
        sqlcommand.Parameters.Add(New SqlParameter("@effectiveRateDebtor", SqlDbType.Decimal) With {.Value = EffectiveRateDebtor})
        sqlcommand.Parameters.Add(New SqlParameter("@effectiveRateMF", SqlDbType.Decimal) With {.Value = EffectiveRateMF})
        sqlcommand.Parameters.Add(New SqlParameter("@flatRateDebtor", SqlDbType.Decimal) With {.Value = FlatRateDebtor})
        sqlcommand.Parameters.Add(New SqlParameter("@firstPaymentType", SqlDbType.Char, 2) With {.Value = FirstPaymentType})
        sqlcommand.Parameters.Add(New SqlParameter("@paymentFrequency", SqlDbType.Char, 2) With {.Value = PaymentFrequency})
        sqlcommand.Parameters.Add(New SqlParameter("@instAmountDebtor", SqlDbType.Decimal) With {.Value = InstAmountDebtor})
        sqlcommand.Parameters.Add(New SqlParameter("@instAmountMF", SqlDbType.Decimal) With {.Value = InstAmountMF})
        sqlcommand.Parameters.Add(New SqlParameter("@totalIncentif", SqlDbType.Decimal) With {.Value = TotalIncentif})
        sqlcommand.Parameters.Add(New SqlParameter("@totalInterestAmount", SqlDbType.Decimal) With {.Value = TotalInterestAmount})
        sqlcommand.Parameters.Add(New SqlParameter("@adminFee", SqlDbType.Decimal) With {.Value = AdminFee})
        sqlcommand.Parameters.Add(New SqlParameter("@insuranceIncome", SqlDbType.Decimal) With {.Value = InsuranceIncome})
        sqlcommand.Parameters.Add(New SqlParameter("@provisionFee", SqlDbType.Decimal) With {.Value = ProvisionFee})
        sqlcommand.Parameters.Add(New SqlParameter("@paymentType", SqlDbType.Char, 2) With {.Value = PaymentType})
        sqlcommand.Parameters.Add(New SqlParameter("@sifatKreditID", SqlDbType.Char, 2) With {.Value = SifatKreditID})
        sqlcommand.Parameters.Add(New SqlParameter("@golonganKredit", SqlDbType.Char, 2) With {.Value = GolonganKredit})
        sqlcommand.Parameters.Add(New SqlParameter("@jenisPenggunaan", SqlDbType.Char, 2) With {.Value = JenisPenggunaan})
        sqlcommand.Parameters.Add(New SqlParameter("@sektorEkonomi", SqlDbType.Char, 5) With {.Value = SektorEkonomi})
        sqlcommand.Parameters.Add(New SqlParameter("@dati2LokasiProyek", SqlDbType.Char, 4) With {.Value = Dati2LokasiProyek})
        sqlcommand.Parameters.Add(New SqlParameter("@status", SqlDbType.Char, 1) With {.Value = Status})

        sqlcommand.Parameters.Add(New SqlParameter("@lastPaidSeqNo", SqlDbType.SmallInt) With {.Value = LastPaidSeqNo})
        Return sqlcommand

    End Function

    Protected Overrides Sub doValidate()
        If (String.IsNullOrEmpty(MfAgreementNo)) Then build.AppendLine("DisbAgreement MFAgreementNo kosong. </br>")

        If (String.IsNullOrEmpty(MfId)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " MFID kosong. </br>")
        If (String.IsNullOrEmpty(MfBatchNo)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " MFBatchNo kosong. </br>")

        If (String.IsNullOrEmpty(MfCif)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " MFCIF kosong. </br>")
        If (String.IsNullOrEmpty(MfBranch)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " MFBranch kosong. </br>")
        If (String.IsNullOrEmpty(MfFacilityNo)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " MFFacilityNo kosong. </br>")
        If (String.IsNullOrEmpty(MfBatchDate)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " MFBatchDate kosong. </br>")
        If (String.IsNullOrEmpty(AgreementDate)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " AgreementDate kosong. </br>")
        If (String.IsNullOrEmpty(GoLiveDate)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " GoLiveDate kosong. </br>")
        If (String.IsNullOrEmpty(EffectiveDate)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " EffectiveDate kosong. </br>")
        If (String.IsNullOrEmpty(MaturityDate)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " MaturityDate kosong. </br>")
        If (String.IsNullOrEmpty(MFFinancingType)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " MFFinancingType kosong. </br>")
        If (String.IsNullOrEmpty(MFPaymentSchme)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " MFPaymentSchme kosong. </br>")
        If (TenorDebtor <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " TenorDebtor kosong. </br>")
        If (TenorMf <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " TenorMF kosong. </br>")
        If (InstallmentScheme <> "RF") Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " InstallmentScheme harus diisi RF.")
        If (TotalOTR <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " TotalOTR kosong. </br>")
        If (DownPayment <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " DownPayment kosong. </br>")
        'If (InsCapitalizedAmount <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MFAgreementNo & " InsCapitalizedAmount kosong. </br>")
        If (NTFDebtor <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " NTFDebtor kosong. </br>")
        If (NTFToMF <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " NTFToMF kosong. </br>")
        'If (BaloonValue <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MFAgreementNo & " BaloonValue kosong. </br>")
        If (String.IsNullOrEmpty(InterestType)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " InterestType kosong. </br>")
        If (EffectiveRateDebtor <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " EffectiveRateDebtor kosong. </br>")
        If (EffectiveRateMF <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " EffectiveRateMF kosong. </br>")
        If (FlatRateDebtor <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " FlatRateDebtor kosong. </br>")
        If (String.IsNullOrEmpty(PaymentFrequency)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " PaymentFrequency kosong. </br>")
        If (String.IsNullOrEmpty(FirstPaymentType)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " FirstPaymentType kosong. </br>")
        If (InstAmountDebtor <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " InstAmountDebtor kosong. </br>")
        If (InstAmountMF <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " InstAmountMF kosong. </br>")
        If (TotalIncentif <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " TotalIncentif kosong. </br>")
        If (TotalInterestAmount <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " TotalInterestAmount kosong. </br>")
        If (AdminFee <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " AdminFee kosong. </br>")
        If (InsuranceIncome <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " InsuranceIncome kosong. </br>")
        If (ProvisionFee <= 0) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " ProvisionFee kosong. </br>")
        If (String.IsNullOrEmpty(PaymentType)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " PaymentType kosong. </br>")
        If (String.IsNullOrEmpty(SifatKreditID)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " SifatKreditID  kosong. </br>")
        If (String.IsNullOrEmpty(JenisPenggunaan)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " JenisPenggunaan kosong. </br>")
        If (String.IsNullOrEmpty(SektorEkonomi)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " SektorEkonomi kosong. </br>")
        If (String.IsNullOrEmpty(Dati2LokasiProyek)) Then build.AppendLine("FILE DISBAGREEMENT MFAgreementNo:" & MfAgreementNo & " Dati2LokasiProyek kosong. </br>")



    End Sub

End Class