﻿Imports System.Data.SqlClient

Public Class DocCollateralHeader
    Inherits AbsDisb

    Protected Sub New()
        MyBase.New("", "", 0)
    End Sub
    Sub New(oriSplit As String())
        MyBase.New(oriSplit(0), "", 0)
        ReferenceNo = oriSplit(1)
        MultiFinanceName = oriSplit(2)
        SubmittedDate = DateTime.ParseExact(dFmt(oriSplit(3)), "yyyyMMdd", Nothing)
        TransactionType = oriSplit(4)
        MfFacilityNo = oriSplit(5)
        FacilityType = oriSplit(6)
        TotalDoc = oriSplit(7)

        Status = oriSplit(8)
    End Sub

    Public Shared Function Instance() As DocCollateralHeader
        Return New DocCollateralHeader()
    End Function
    Private _disbDebtor As IEnumerable(Of DocCollateralHeader)

    Public Overrides Function addToList(oriSplit() As String) As AbsDisb
        Try
            Return New DocCollateralHeader(oriSplit)
        Catch ex As Exception
            Throw New Exception("Error parse file DisbHeader.")
        End Try
    End Function
    Public ReadOnly Property ReferenceNo As String
    Public ReadOnly Property MultiFinanceName As String
    Public ReadOnly Property SubmittedDate As DateTime
    Public ReadOnly Property TransactionType As String
    Public ReadOnly Property MfFacilityNo As String
    Public ReadOnly Property FacilityType As String
    Public ReadOnly Property TotalDoc As Decimal

    Public ReadOnly Property Status As String

    Protected Overrides Sub doValidate()
        If (String.IsNullOrEmpty(MfId)) Then build.AppendLine("DocCollateralHeader MfId kosong.")
        If (String.IsNullOrEmpty(ReferenceNo)) Then build.AppendLine("DocCollateralHeader ReferenceNo kosong.")
        If (String.IsNullOrEmpty(MultiFinanceName)) Then build.AppendLine("DocCollateralHeader MultiFinanceName kosong.")
        If (String.IsNullOrEmpty(TransactionType)) Then build.AppendLine("DocCollateralHeader TransactionType kosong.")
        If (String.IsNullOrEmpty(MfFacilityNo)) Then build.AppendLine("DocCollateralHeader MfFacilityNo kosong.")
        If (String.IsNullOrEmpty(FacilityType)) Then build.AppendLine("DocCollateralHeader FacilityType kosong.")
        If (String.IsNullOrEmpty(Status)) Then build.AppendLine("DocCollateralHeader Status kosong.")
    End Sub
    Const _spName As String = "sp_Disb_DocCollateralHeader_add"
    Public Overrides Function command(cnn As SqlConnection) As SqlCommand
        Dim sqlcommand = New SqlCommand(_spName, cnn)
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.Parameters.Clear()
        sqlcommand.Parameters.Add(New SqlParameter("@mfId", SqlDbType.Char, 10) With {.Value = MfId})
        sqlcommand.Parameters.Add(New SqlParameter("@ReferenceNo", SqlDbType.Char, 20) With {.Value = ReferenceNo})
        sqlcommand.Parameters.Add(New SqlParameter("@multiFinanceName", SqlDbType.Char, 50) With {.Value = MultiFinanceName})
        sqlcommand.Parameters.Add(New SqlParameter("@SubmittedDate", SqlDbType.DateTime) With {.Value = SubmittedDate})
        sqlcommand.Parameters.Add(New SqlParameter("@transactionType", SqlDbType.Char, 1) With {.Value = TransactionType})
        sqlcommand.Parameters.Add(New SqlParameter("@mfFacilityNo", SqlDbType.Char, 20) With {.Value = MfFacilityNo})
        sqlcommand.Parameters.Add(New SqlParameter("@facilityType", SqlDbType.Char, 2) With {.Value = FacilityType})
        sqlcommand.Parameters.Add(New SqlParameter("@TotalDoc", SqlDbType.Decimal) With {.Value = TotalDoc})
        sqlcommand.Parameters.Add(New SqlParameter("@status", SqlDbType.Char, 3) With {.Value = Status})
        Return sqlcommand

    End Function
End Class
