﻿Imports System.IO

Public Class PaserManager

    Public Sub New()
        DisbParamList = New Dictionary(Of String, IList(Of AbsDisb))
    End Sub

    Public ReadOnly Property DisbParamList As Dictionary(Of String, IList(Of AbsDisb))

    Public Function NewparseToList(reader As Dictionary(Of String, StreamReader))

        For Each pair In reader
            Dim n = pair.Key.Split("_")(1).Trim.ToUpper()
            Select Case n
                Case "DISBHEADER"
                    DisbParamList.Add(n, DisbHeader.Instance().DoParseCsv(pair.Value))
                Case "DISBDEBTOR"
                    DisbParamList.Add(n, DisbDebtor.Instance().DoParseCsv(pair.Value))
                Case "DISBDEBTORMGT"
                    DisbParamList.Add(n, DisbDebtorMgt.Instance().DoParseCsv(pair.Value))
                Case "DISBDEBTORGUARANTOR"
                    DisbParamList.Add(n, DisbGuarantor.Instance().DoParseCsv(pair.Value))
                Case "DISBAGREEMENT"
                    DisbParamList.Add(n, DisbAgreement.Instance().DoParseCsv(pair.Value))
                Case "DISBASSET"
                    DisbParamList.Add(n, DisbAsset.Instance().DoParseCsv(pair.Value))
                Case "DISBINSURANCE"
                    DisbParamList.Add(n, DisbInsurance.Instance().DoParseCsv(pair.Value))
                Case "DISBINSURANCEDETAIL"
                    DisbParamList.Add(n, DisbInsuranceDetail.Instance().DoParseCsv(pair.Value))
                Case "PAYMENTHISTORY"
                    DisbParamList.Add(n, DisbPaymentHistory.Instance().DoParseCsv(pair.Value))
            End Select
        Next
        Return Me
    End Function

    Public Function NewparseBPKBAngsToList(reader As Dictionary(Of String, StreamReader))
        For Each pair In reader
            Dim n = pair.Key.Split("_")
            Select Case n(0)
                Case "COLLATERAL"
                    Select Case n(1)
                        Case "H"
                            DisbParamList.Add(pair.Key, DocCollateralHeader.Instance().DoParseCsv(pair.Value))
                        Case "D"
                            DisbParamList.Add(pair.Key, DocCollateralSubmitted.Instance().DoParseCsv(pair.Value))
                    End Select
                Case "PAYMENT"
                    Select Case n(1)
                        Case "H"
                            DisbParamList.Add(pair.Key, PaymentHeader.Instance().DoParseCsv(pair.Value))
                        Case "D"
                            DisbParamList.Add(pair.Key, PaymentAgreement.Instance().DoParseCsv(pair.Value))
                    End Select
            End Select
        Next
        Return Me
    End Function
End Class
