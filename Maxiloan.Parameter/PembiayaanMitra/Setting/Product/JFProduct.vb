﻿<Serializable()>
Public Class JFProduct : Inherits Common
    Private _ProductCode As String
    Private _InitialName As String
    Private _ProductName As String
    'Private _LoginID As String
    Private _listData As DataTable
    Private _TotRec As Integer

    Public Property ProductCode() As String
        Get
            Return _ProductCode
        End Get
        Set(ByVal Value As String)
            _ProductCode = Value
        End Set
    End Property

    Public Property InitialName() As String
        Get
            Return _InitialName
        End Get
        Set(ByVal Value As String)
            _InitialName = Value
        End Set
    End Property

    Public Property ProductName() As String
        Get
            Return _ProductName
        End Get
        Set(ByVal Value As String)
            _ProductName = Value
        End Set
    End Property
    'Public Overloads Property LoginID() As String
    '    Get
    '        Return _LoginID
    '    End Get
    '    Set(ByVal Value As String)
    '        _LoginID = Value
    '    End Set
    'End Property
    Public Property ListData() As DataTable
        Get
            Return _listData
        End Get
        Set(ByVal Value As DataTable)
            _listData = Value
        End Set
    End Property

    Public Property TotRec() As Integer
        Get
            Return _TotRec
        End Get
        Set(ByVal Value As Integer)
            _TotRec = Value
        End Set
    End Property

    Public Property AssetTypeCode() As String
    Public Property DisburseApprovalCode() As String
    Public Property JournalSchemeCode() As String
    Public Property CriteriaRacCode() As String
    Public Property ProvisionFee() As Decimal
    Public Property AdminFee() As Decimal
    Public Property ServiceFee() As Decimal
    Public Property CollateralManagementFee() As Decimal
    Public Property PrepaymentPenaltyFixed() As Decimal
    Public Property PrepaymentPenaltyRate() As Decimal
    Public Property LateChargesRate() As Decimal
    Public Property LateChargesGracePeriod() As Int16
    Public Property PaymentPriority() As String
    Public Property AccruedInterestCalculationType() As String
    Public Property CreatedBy() As String
    Public Property CreatedDate() As DateTime
    Public Property ChangedBy() As String
    Public Property ChangedDate() As DateTime

    Public Property SpName As String
        Get
            Return _spName
        End Get
        Set(value As String)
            _spName = value
        End Set
    End Property

    Private _spName As String




End Class
