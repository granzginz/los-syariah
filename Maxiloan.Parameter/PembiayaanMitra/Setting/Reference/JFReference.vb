﻿
<Serializable()>
Public Class JFReference : Inherits Common
    Private _GroupID As String
    Private _ReferenceKey As String
    Private _ReferenceDescription As String
    Private _LoginID As String
    Private _listData As DataTable
    Private _TotRec As Integer

    Public Property GroupID() As String
        Get
            Return _GroupID
        End Get
        Set(ByVal Value As String)
            _GroupID = Value
        End Set
    End Property

    Public Property ReferenceKey() As String
        Get
            Return _ReferenceKey
        End Get
        Set(ByVal Value As String)
            _ReferenceKey = Value
        End Set
    End Property

    Public Property ReferenceDescription() As String
        Get
            Return _ReferenceDescription
        End Get
        Set(ByVal Value As String)
            _ReferenceDescription = Value
        End Set
    End Property
    Public Overloads Property LoginID() As String
        Get
            Return _LoginID
        End Get
        Set(ByVal Value As String)
            _LoginID = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listData
        End Get
        Set(ByVal Value As DataTable)
            _listData = Value
        End Set
    End Property

    Public Property TotRec() As Integer
        Get
            Return _TotRec
        End Get
        Set(ByVal Value As Integer)
            _TotRec = Value
        End Set
    End Property

End Class
