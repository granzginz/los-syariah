﻿
<Serializable()>
Public Class MitraMultifinance : Inherits Common
    Private _MitraID As String
    Private _MitraFullName As String
    Private _MitraShortName As String
    Private _MitraInitialName As String
    Private _listData As DataTable
    Private _TotRec As Integer

    Private _Phone1 As String
    Private _Phone2 As String
    Private _Fax As String
    Private _WebSite As String
    Private _Notes As String
    Private _Provinsi As String
    Private _JoinDate As Date
    Private _BankID As String
    Private _BranchCode As String
    Private _AccountName As String
    Private _AccountNo As String
    Private _AccountType As String

    Private _MFFacilityNo As String



    Public Property MitraID() As String
        Get
            Return _MitraID
        End Get
        Set(ByVal Value As String)
            _MitraID = Value
        End Set
    End Property
    Public Property MitraFullName() As String
        Get
            Return _MitraFullName
        End Get
        Set(ByVal Value As String)
            _MitraFullName = Value
        End Set
    End Property

    Public Property MitraShortName() As String
        Get
            Return _MitraShortName
        End Get
        Set(ByVal Value As String)
            _MitraShortName = Value
        End Set
    End Property

    Public Property MitraInitialName() As String
        Get
            Return _MitraInitialName
        End Get
        Set(ByVal Value As String)
            _MitraInitialName = Value
        End Set
    End Property
    Public Property Phone1() As String
        Get
            Return _Phone1
        End Get
        Set(ByVal Value As String)
            _Phone1 = Value
        End Set
    End Property
    Public Property Phone2() As String
        Get
            Return _Phone2
        End Get
        Set(ByVal Value As String)
            _Phone2 = Value
        End Set
    End Property

    Public Property Fax() As String
        Get
            Return _Fax
        End Get
        Set(ByVal Value As String)
            _Fax = Value
        End Set
    End Property
    Public Property WebSite() As String
        Get
            Return _WebSite
        End Get
        Set(ByVal Value As String)
            _WebSite = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property
    Public Property Provinsi() As String
        Get
            Return _Provinsi
        End Get
        Set(ByVal Value As String)
            _Provinsi = Value
        End Set
    End Property
    Public Property JoinDate() As Date
        Get
            Return _JoinDate
        End Get
        Set(ByVal Value As Date)
            _JoinDate = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listData
        End Get
        Set(ByVal Value As DataTable)
            _listData = Value
        End Set
    End Property
    Public Property TotRec() As Integer
        Get
            Return _TotRec
        End Get
        Set(ByVal Value As Integer)
            _TotRec = Value
        End Set
    End Property
    Public Property BankID() As String
        Get
            Return _BankID
        End Get
        Set(ByVal Value As String)
            _BankID = Value
        End Set
    End Property
    Public Property BranchCode() As String
        Get
            Return _BranchCode
        End Get
        Set(ByVal Value As String)
            _BranchCode = Value
        End Set
    End Property
    Public Property AccountName() As String
        Get
            Return _AccountName
        End Get
        Set(ByVal Value As String)
            _AccountName = Value
        End Set
    End Property
    Public Property AccountNo() As String
        Get
            Return _AccountNo
        End Get
        Set(ByVal Value As String)
            _AccountNo = Value
        End Set
    End Property
    Public Property AccountType() As String
        Get
            Return _AccountType
        End Get
        Set(ByVal Value As String)
            _AccountType = Value
        End Set
    End Property

    Public Property MFFacilityNo() As String
        Get
            Return _MFFacilityNo
        End Get
        Set(value As String)
            _MFFacilityNo = value
        End Set
    End Property
End Class
