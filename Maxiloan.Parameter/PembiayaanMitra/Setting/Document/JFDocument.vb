﻿Public Class JFDocument : Inherits Common
    Private _ProductCode As String
    Private _DocumentCode As String
    Private _DocumentName As String
    Private _IsPersonal As Boolean
    Private _IsCompany As Boolean
    Private _IsMandatory As Boolean
    Private _PersonalGender As String
    Private _listData As DataTable
    Private _TotRec As Integer

    Public Property ProductCode As String
        Get
            Return _ProductCode
        End Get
        Set(value As String)
            _ProductCode = value
        End Set
    End Property

    Public Property DocumentCode As String
        Get
            Return _DocumentCode
        End Get
        Set(value As String)
            _DocumentCode = value
        End Set
    End Property

    Public Property DocumentName As String
        Get
            Return _DocumentName
        End Get
        Set(value As String)
            _DocumentName = value
        End Set
    End Property

    Public Property IsPersonal As Boolean
        Get
            Return _IsPersonal
        End Get
        Set(value As Boolean)
            _IsPersonal = value
        End Set
    End Property

    Public Property IsCompany As Boolean
        Get
            Return _IsCompany
        End Get
        Set(value As Boolean)
            _IsCompany = value
        End Set
    End Property

    Public Property IsMandatory As Boolean
        Get
            Return _IsMandatory
        End Get
        Set(value As Boolean)
            _IsMandatory = value
        End Set
    End Property

    Public Property PersonalGender As String
        Get
            Return _PersonalGender
        End Get
        Set(value As String)
            _PersonalGender = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _listData
        End Get
        Set(value As DataTable)
            _listData = value
        End Set
    End Property

    Public Property TotRec As Integer
        Get
            Return _TotRec
        End Get
        Set(value As Integer)
            _TotRec = value
        End Set
    End Property

    Public Property CreatedBy() As String
    Public Property CreatedDate() As DateTime
    Public Property ChangedBy() As String
    Public Property ChangedDate() As DateTime
    Public Property spName
        Get
            Return _spName
        End Get
        Set(value)
            _spName = value
        End Set
    End Property
    Private Property _spName() As String
End Class
