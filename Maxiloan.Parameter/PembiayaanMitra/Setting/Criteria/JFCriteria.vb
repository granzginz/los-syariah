﻿Public Class JFCriteria : Inherits Common
    Private _ProductCode As String
    Private _ComponentId As String
    Private _Active As Boolean
    Private _Opertaor As String
    Private _CriteriaValue As String
    Private _listData As DataTable
    Private _TotRec As Integer

    Public Property ProductCode As String
        Get
            Return _ProductCode
        End Get
        Set(value As String)
            _ProductCode = value
        End Set
    End Property

    Public Property ComponentId As String
        Get
            Return _ComponentId
        End Get
        Set(value As String)
            _ComponentId = value
        End Set
    End Property

    Public Property Active As Boolean
        Get
            Return _Active
        End Get
        Set(value As Boolean)
            _Active = value
        End Set
    End Property

    Public Property Opertaor As String
        Get
            Return _Opertaor
        End Get
        Set(value As String)
            _Opertaor = value
        End Set
    End Property

    Public Property CriteriaValue As String
        Get
            Return _CriteriaValue
        End Get
        Set(value As String)
            _CriteriaValue = value
        End Set
    End Property

    Public Property ListData As DataTable
        Get
            Return _listData
        End Get
        Set(value As DataTable)
            _listData = value
        End Set
    End Property

    Public Property TotRec As Integer
        Get
            Return _TotRec
        End Get
        Set(value As Integer)
            _TotRec = value
        End Set
    End Property

    Public Property CreatedBy() As String
    Public Property CreatedDate() As DateTime
    Public Property ChangedBy() As String
    Public Property ChangedDate() As DateTime
    Public Property spName
        Get
            Return _spName
        End Get
        Set(value)
            _spName = value
        End Set
    End Property
    Private Property _spName() As String

End Class
