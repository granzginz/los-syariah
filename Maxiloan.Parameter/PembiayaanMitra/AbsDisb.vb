﻿


Imports System.Data.SqlClient
Imports System.Text

Public MustInherit Class AbsDisb
    Protected dFmt As Func(Of String, String) = Function(s) s.Replace("/", "").Replace("-", "")
    Protected Shared delim = "|"
    Protected build As StringBuilder = New StringBuilder()
    Protected Function oriSplit(originalLine As String) As String()
        Dim delim = "|"
        Return Split(originalLine, delim, -1, False)
    End Function

    Protected Function OriSplitChr(originalLine As String) As String()
        Dim delim = "|"
        Return Split(originalLine, delim, -1, False)
    End Function

    Public Function DoParseCsv(reader As IO.StreamReader) As IList(Of AbsDisb)
        Dim ret = New List(Of AbsDisb)
        While Not reader.EndOfStream
            Try
                Dim data = reader.ReadLine()
                If (String.IsNullOrEmpty(data)) Then Continue While
                ret.Add(addToList(oriSplit(data)))
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End While
        Return ret
    End Function

    Protected Sub New(_mfId As String, _mfBatchNo As String, _revNo As Int16)
        MfId = _mfId
        MfBatchNo = _mfBatchNo
        RevNo = _revNo
    End Sub

    Public ReadOnly Property MfId As String
    Public ReadOnly Property MfBatchNo As String
    Public ReadOnly Property RevNo As Int16

    Overridable Function addToList(oriSplit As String()) As AbsDisb
        Return Nothing
    End Function

    MustOverride Function command(cnn As SqlConnection) As SqlCommand



    Protected MustOverride Sub doValidate()

    Public ReadOnly Property ValidateString() As StringBuilder
        Get
            doValidate()
            Return build
        End Get
    End Property
End Class


Public MustInherit Class BaseDisb
    Inherits AbsDisb
    Protected Sub New(_mfId As String, _mfBatchNo As String, _revNo As Int16, _mfAgreementNo As String, _mfcif As String)
        MyBase.New(_mfId, _mfBatchNo, _revNo)
        MfAgreementNo = _mfAgreementNo
        MfCif = _mfcif
    End Sub
    Public ReadOnly Property MfAgreementNo As String
    Public ReadOnly Property MfCif As String

End Class