﻿Public Class AgreementToValidate : Inherits Common
    'Agreement
    Public Property MFID As String
    Public Property MFBatchNo As String
    Public Property MFFacilityNo As String
    Public Property MFAgreementNo As String
    Public Property AgreementDate As DateTime
    Public Property EffectiveDate As DateTime
    Public Property MaturityDate As DateTime
    Public Property TotalOTR As Decimal
    Public Property NTFToMF As Decimal
    Public Property TenorDebtor As Integer
    Public Property DownPayment As Decimal
    Public Property TenorMF As Integer

    'Debitur
    Public Property BirthDate As DateTime
    Public Property MaritalStatus As String
    Public Property PersonalCustType As String
    Public Property DebtorType As String
    Public Property NPWP As String
    Public Property NoTDP As String
    Public Property NoSIUP As String

    'Asset
    Public Property MfgCountry As String
    Public Property Merk As String
    Public Property AssetDescription As String
    Public Property AssetCondition As String
    Public Property AssetConditionFacility As String
    Public Property DrawdownStartDate As DateTime
    Public Property DrawdownEndDate As DateTime
    Public Property AssetUsage As String
    Public Property MfgYear As Int32

    'Facility
    Public Property MaxTenorFacility As String
    Public Property MfgCountryFacility As String
    Public Property AssetBrandFacility As String
    Public Property MaxAssetAgeFacility As String
    Public Property MinDownPaymentFacility As String
End Class
