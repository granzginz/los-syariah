﻿Imports System.Data.SqlClient

Public Class DocCollateralSubmitted
    Inherits BaseDisb
    Protected Sub New() '_mfId As String, _mfBatchNo As String, _revNo As Int16, _mfcif As String)
        MyBase.New("", "", 0, "", "")
    End Sub

    Public Shared Function Instance() As DocCollateralSubmitted
        Return New DocCollateralSubmitted()
    End Function

    Sub New(oriSplit As String())
        MyBase.New(oriSplit(0), "", 0, oriSplit(2), "")
        ReferenceNo = oriSplit(1)
        DocTypeID = oriSplit(3)
        DebtorName = oriSplit(4)
        DocNo = oriSplit(5)
        OwnerName = oriSplit(6)
        CollateralAddress = oriSplit(7)
        CollateralKelurahan = oriSplit(8)
        CollateralKecamatan = oriSplit(9)
        CollateralCity = oriSplit(10)
        DocIssuedDate = DateTime.ParseExact(dFmt(oriSplit(11)), "yyyyMMdd", Nothing)
    End Sub


    'MFID 
    Public ReadOnly Property ReferenceNo As String
    Public ReadOnly Property DocTypeID As String
    Public ReadOnly Property DebtorName As String
    Public ReadOnly Property DocNo As String
    Public ReadOnly Property OwnerName As String
    Public ReadOnly Property CollateralAddress As String
    Public ReadOnly Property CollateralKelurahan As String
    Public ReadOnly Property CollateralKecamatan As String
    Public ReadOnly Property CollateralCity As String
    Public ReadOnly Property DocIssuedDate As DateTime

    Protected Overrides Sub doValidate()
        If (String.IsNullOrEmpty(MfId)) Then build.AppendLine("DocCollateralSubmitted MfId kosong.")
        If (String.IsNullOrEmpty(ReferenceNo)) Then build.AppendLine("DocCollateralSubmitted ReferenceNo kosong.")
        If (String.IsNullOrEmpty(DebtorName)) Then build.AppendLine("DocCollateralSubmitted MultiFinanceName kosong.")

        If (String.IsNullOrEmpty(DocTypeID)) Then build.AppendLine("DocCollateralSubmitted DocTypeID kosong.")

        If (String.IsNullOrEmpty(DocNo)) Then build.AppendLine("DocCollateralSubmitted DocNo kosong.")
        If (String.IsNullOrEmpty(OwnerName)) Then build.AppendLine("DocCollateralSubmitted OwnerName kosong.")
        If (String.IsNullOrEmpty(CollateralAddress)) Then build.AppendLine("DocCollateralSubmitted CollateralAddress kosong.")
        If (String.IsNullOrEmpty(CollateralKelurahan)) Then build.AppendLine("DocCollateralSubmitted CollateralKelurahan kosong.")
        If (String.IsNullOrEmpty(CollateralKecamatan)) Then build.AppendLine("DocCollateralSubmitted CollateralKecamatan kosong.")
        If (String.IsNullOrEmpty(CollateralCity)) Then build.AppendLine("DocCollateralSubmitted CollateralCity kosong.")

    End Sub


    Public Overrides Function addToList(oriSplit() As String) As AbsDisb
        Try
            Return New DocCollateralSubmitted(oriSplit)
        Catch ex As Exception
            Throw New Exception("Error parse file DisbHeader.")
        End Try
    End Function


    Const _spName As String = "sp_Disb_DocCollateralSubmitted_add"
    Public Overrides Function command(cnn As SqlConnection) As SqlCommand
        Dim sqlcommand = New SqlCommand(_spName, cnn)
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.Parameters.Clear()
        sqlcommand.Parameters.Add(New SqlParameter("@mFID", SqlDbType.Char, 10) With {.Value = MfId})
        sqlcommand.Parameters.Add(New SqlParameter("@referenceNo", SqlDbType.Char, 20) With {.Value = ReferenceNo})
        sqlcommand.Parameters.Add(New SqlParameter("@mFAgreementNo", SqlDbType.Char, 20) With {.Value = MfAgreementNo})
        sqlcommand.Parameters.Add(New SqlParameter("@DocTypeID", SqlDbType.Char, 10) With {.Value = DocTypeID})
        sqlcommand.Parameters.Add(New SqlParameter("@DebtorName", SqlDbType.Char, 50) With {.Value = DebtorName})
        sqlcommand.Parameters.Add(New SqlParameter("@DocNo", SqlDbType.Char, 20) With {.Value = DocNo})
        sqlcommand.Parameters.Add(New SqlParameter("@OwnerName", SqlDbType.Char, 50) With {.Value = OwnerName})
        sqlcommand.Parameters.Add(New SqlParameter("@CollateralAddress", SqlDbType.Char, 100) With {.Value = CollateralAddress})
        sqlcommand.Parameters.Add(New SqlParameter("@CollateralKelurahan", SqlDbType.Char, 30) With {.Value = CollateralKelurahan})
        sqlcommand.Parameters.Add(New SqlParameter("@CollateralKecamatan", SqlDbType.Char, 30) With {.Value = CollateralKecamatan})
        sqlcommand.Parameters.Add(New SqlParameter("@CollateralCity", SqlDbType.Char, 30) With {.Value = CollateralCity})
        sqlcommand.Parameters.Add(New SqlParameter("@DocIssuedDate ", SqlDbType.Date) With {.Value = DocIssuedDate})
        Return sqlcommand
    End Function

End Class
