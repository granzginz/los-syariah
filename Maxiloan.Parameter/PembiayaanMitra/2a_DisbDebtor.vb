﻿

Imports System.Data.SqlClient

Public Class DisbDebtor
    Inherits BaseDisb
    Protected Sub New() '_mfId As String, _mfBatchNo As String, _revNo As Int16, _mfcif As String)
        MyBase.New("", "", 0, "", "")
    End Sub

    Public Shared Function Instance() As DisbDebtor
        Return New DisbDebtor()
    End Function
    Sub New(oriSplit As String())
        MyBase.New(oriSplit(0), oriSplit(1), 0, "", oriSplit(2))

        MfBranchID = oriSplit(3)
        DebtorName = oriSplit(4)
        DebtorType = oriSplit(5)
        PersonalCustType = oriSplit(6)
        BirthPlace = oriSplit(7)
        BirthDate = DateTime.ParseExact(dFmt(oriSplit(8)), "yyyyMMdd", Nothing)
        Gender = oriSplit(9)
        MotherName = oriSplit(10)
        ReligionID = oriSplit(11)
        MaritalStatus = oriSplit(12)
        DependenceNumber = oriSplit(13)
        KTPNo = oriSplit(14)
        PasporNo = oriSplit(15)
        NPWP = oriSplit(16)
        Nationality = oriSplit(17)
        EducationID = oriSplit(18)
        DebtorNotes = oriSplit(19)
        CompanyTypeID = oriSplit(20)
        PlaceofAktaIssued = oriSplit(21)
        AktaNo = oriSplit(22)
        TxtAktaIssuedDate = oriSplit(23)
        LastAktaNo = oriSplit(24)
        LastAktaDate = oriSplit(25)
        NoTDP = oriSplit(26)
        NOSIUP = oriSplit(27)
        DebtorAddress = oriSplit(28)
        DebtorDati2 = oriSplit(29)
        DebtorZipCode = oriSplit(30)
        DebtorKelurahan = oriSplit(31)
        DebtorKecamatan = oriSplit(32)
        HomeStatusID = oriSplit(33)
        DebtorAreaPhone = oriSplit(34)
        DebtorPhone = oriSplit(35)
        DebtorMobilePhone1 = oriSplit(36)
        DebtorMobilePhone2 = oriSplit(37)
        GolonganDebiturID = oriSplit(38)
        DebtorJobID = oriSplit(39)
        CompanyName = oriSplit(40)
        LengthWorking = oriSplit(41)
        MonthlyFixedIncome = oriSplit(42)
        AdditionalIncome = oriSplit(43)
        LivingCost = oriSplit(44)
        OtherInstallment = oriSplit(45)
        BidangUsahaID = oriSplit(46)
        TotalEmployee = oriSplit(47)
        BusinessCategory = oriSplit(48)
        GroupIDDebitur = oriSplit(49)
        GroupDebiturName = oriSplit(50)
        RelationshipWithBank = oriSplit(51)
        RatingDebtor = oriSplit(52)
        RatingInstitution = oriSplit(53)
        GoPublic = oriSplit(54)
    End Sub
    Public Overrides Function addToList(oriSplit() As String) As AbsDisb
        Try
            Return New DisbDebtor(oriSplit)
        Catch ex As Exception
            Throw New Exception($"Error parse file DisbDebtor. {ex.Message}")
        End Try


    End Function

    Public ReadOnly Property MfBranchID As String
    Public ReadOnly Property DebtorName As String
    Public ReadOnly Property DebtorType As String
    Public ReadOnly Property BirthPlace As String
    Public ReadOnly Property BirthDate As DateTime
    Public ReadOnly Property Gender As String
    Public ReadOnly Property MaritalStatus As String
    Public ReadOnly Property MotherName As String
    Public ReadOnly Property ReligionID As String
    Public ReadOnly Property DependenceNumber As Decimal
    Public ReadOnly Property KTPNo As String
    Public ReadOnly Property PasporNo As String
    Public ReadOnly Property NPWP As String
    Public ReadOnly Property Nationality As String
    Public ReadOnly Property EducationID As String
    Public ReadOnly Property DebtorNotes As String
    Public ReadOnly Property CompanyTypeID As String
    Public ReadOnly Property PlaceofAktaIssued As String
    Public ReadOnly Property AktaNo As String
    Public ReadOnly Property TxtAktaIssuedDate As String
    Protected Property AktaIssuedDate As DateTime

    Public ReadOnly Property LastAktaNo As String
    Public ReadOnly Property LastAktaDate As String
    Public ReadOnly Property DebtorAddress As String
    Public ReadOnly Property DebtorDati2 As String
    Public ReadOnly Property DebtorZipCode As Decimal
    Public ReadOnly Property DebtorKelurahan As String
    Public ReadOnly Property DebtorKecamatan As String
    Public ReadOnly Property HomeStatusID As String
    Public ReadOnly Property DebtorAreaPhone As String
    Public ReadOnly Property DebtorPhone As String
    Public ReadOnly Property DebtorMobilePhone1 As String
    Public ReadOnly Property DebtorMobilePhone2 As String
    Public ReadOnly Property GolonganDebiturID As String
    Public ReadOnly Property DebtorJobID As String
    Public ReadOnly Property CompanyName As String
    Public ReadOnly Property LengthWorking As Decimal
    Public ReadOnly Property MonthlyFixedIncome As Decimal
    Public ReadOnly Property AdditionalIncome As Decimal
    Public ReadOnly Property LivingCost As Decimal
    Public ReadOnly Property OtherInstallment As Decimal
    Public ReadOnly Property BidangUsahaID As String
    Public ReadOnly Property TotalEmployee As Decimal
    Public ReadOnly Property BusinessCategory As String
    Public ReadOnly Property GroupIDDebitur As String
    Public ReadOnly Property GroupDebiturName As String
    Public ReadOnly Property RelationshipWithBank As String
    Public ReadOnly Property RatingDebtor As String
    Public ReadOnly Property RatingInstitution As String
    Public ReadOnly Property GoPublic As String

    Public ReadOnly Property PersonalCustType As String
    Public ReadOnly Property NoTDP As String
    Public ReadOnly Property NOSIUP As String



    Private _disbDebtorMgt As DisbDebtorMgt
    Private _disbAgreement As DisbAgreement

    Const _spName As String = "sp_Disb_Debtor_add"
    Public Overrides Function command(cnn As SqlClient.SqlConnection) As SqlClient.SqlCommand
        Dim sqlcommand = New SqlClient.SqlCommand(_spName, cnn)
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.Parameters.Clear()


        sqlcommand.Parameters.Add(New SqlParameter("@mfId", SqlDbType.Char, 10) With {.Value = MfId})
        sqlcommand.Parameters.Add(New SqlParameter("@mfBatchNo", SqlDbType.Char, 20) With {.Value = MfBatchNo})
        sqlcommand.Parameters.Add(New SqlParameter("@mFCif", SqlDbType.Char, 20) With {.Value = MfCif})
        sqlcommand.Parameters.Add(New SqlParameter("@mfBranchId", SqlDbType.Char, 10) With {.Value = MfBranchID})
        sqlcommand.Parameters.Add(New SqlParameter("@debtorName", SqlDbType.Char, 10) With {.Value = DebtorName})
        sqlcommand.Parameters.Add(New SqlParameter("@debtorType", SqlDbType.Char, 10) With {.Value = DebtorType})
        sqlcommand.Parameters.Add(New SqlParameter("@birthPlace", SqlDbType.Char, 10) With {.Value = BirthPlace})
        sqlcommand.Parameters.Add(New SqlParameter("@birthDate", SqlDbType.Date) With {.Value = BirthDate})
        sqlcommand.Parameters.Add(New SqlParameter("@gender", SqlDbType.Char, 100) With {.Value = Gender})
        sqlcommand.Parameters.Add(New SqlParameter("@maritalStatus", SqlDbType.Char, 100) With {.Value = MaritalStatus})
        sqlcommand.Parameters.Add(New SqlParameter("@motherName", SqlDbType.Char, 1) With {.Value = MotherName})
        sqlcommand.Parameters.Add(New SqlParameter("@religionID", SqlDbType.Char, 50) With {.Value = ReligionID})
        sqlcommand.Parameters.Add(New SqlParameter("@dependenceNumber", SqlDbType.Decimal) With {.Value = DependenceNumber})
        sqlcommand.Parameters.Add(New SqlParameter("@kTPNo", SqlDbType.Char, 30) With {.Value = KTPNo})
        sqlcommand.Parameters.Add(New SqlParameter("@pasporNo", SqlDbType.Char, 30) With {.Value = PasporNo})
        sqlcommand.Parameters.Add(New SqlParameter("@nPWP", SqlDbType.Char, 20) With {.Value = NPWP})
        sqlcommand.Parameters.Add(New SqlParameter("@nationality", SqlDbType.Char, 1) With {.Value = Nationality})
        sqlcommand.Parameters.Add(New SqlParameter("@educationID", SqlDbType.Char, 4) With {.Value = EducationID})
        sqlcommand.Parameters.Add(New SqlParameter("@debtorNotes", SqlDbType.Char, 50) With {.Value = DebtorNotes})
        sqlcommand.Parameters.Add(New SqlParameter("@companyTypeID", SqlDbType.Char, 4) With {.Value = CompanyTypeID})
        sqlcommand.Parameters.Add(New SqlParameter("@placeofAktaIssued", SqlDbType.Char, 50) With {.Value = PlaceofAktaIssued})
        sqlcommand.Parameters.Add(New SqlParameter("@aktaNo", SqlDbType.Char, 30) With {.Value = AktaNo})
        sqlcommand.Parameters.Add(New SqlParameter("@aktaIssuedDate", SqlDbType.Date) With {.Value = AktaIssuedDate})
        sqlcommand.Parameters.Add(New SqlParameter("@lastAktaNo", SqlDbType.Char, 30) With {.Value = LastAktaNo})
        sqlcommand.Parameters.Add(New SqlParameter("@lastAktaDate", SqlDbType.Char, 30) With {.Value = LastAktaDate})
        sqlcommand.Parameters.Add(New SqlParameter("@debtorAddress", SqlDbType.Char, 100) With {.Value = DebtorAddress})
        sqlcommand.Parameters.Add(New SqlParameter("@debtorDati2", SqlDbType.Char, 4) With {.Value = DebtorDati2})
        sqlcommand.Parameters.Add(New SqlParameter("@debtorZipCode", SqlDbType.Char, 5) With {.Value = DebtorZipCode})
        sqlcommand.Parameters.Add(New SqlParameter("@debtorKelurahan", SqlDbType.Char, 50) With {.Value = DebtorKelurahan})
        sqlcommand.Parameters.Add(New SqlParameter("@debtorKecamatan", SqlDbType.Char, 50) With {.Value = DebtorKecamatan})
        sqlcommand.Parameters.Add(New SqlParameter("@homeStatusID", SqlDbType.Char, 1) With {.Value = HomeStatusID})
        sqlcommand.Parameters.Add(New SqlParameter("@debtorAreaPhone", SqlDbType.Char, 4) With {.Value = DebtorAreaPhone})
        sqlcommand.Parameters.Add(New SqlParameter("@debtorPhone", SqlDbType.Char, 8) With {.Value = DebtorPhone})
        sqlcommand.Parameters.Add(New SqlParameter("@debtorMobilePhone1", SqlDbType.Char, 15) With {.Value = DebtorMobilePhone1})
        sqlcommand.Parameters.Add(New SqlParameter("@debtorMobilePhone2", SqlDbType.Char, 15) With {.Value = DebtorMobilePhone2})
        sqlcommand.Parameters.Add(New SqlParameter("@golonganDebiturID", SqlDbType.Char, 3) With {.Value = GolonganDebiturID})
        sqlcommand.Parameters.Add(New SqlParameter("@debtorJobID", SqlDbType.Char, 3) With {.Value = DebtorJobID})
        sqlcommand.Parameters.Add(New SqlParameter("@companyName", SqlDbType.Char, 50) With {.Value = CompanyName})
        sqlcommand.Parameters.Add(New SqlParameter("@lengthWorking", SqlDbType.Int) With {.Value = LengthWorking})
        sqlcommand.Parameters.Add(New SqlParameter("@monthlyFixedIncome", SqlDbType.Decimal) With {.Value = MonthlyFixedIncome})
        sqlcommand.Parameters.Add(New SqlParameter("@additionalIncome", SqlDbType.Decimal) With {.Value = AdditionalIncome})
        sqlcommand.Parameters.Add(New SqlParameter("@livingCost", SqlDbType.Decimal) With {.Value = LivingCost})
        sqlcommand.Parameters.Add(New SqlParameter("@otherInstallment", SqlDbType.Decimal) With {.Value = OtherInstallment})
        sqlcommand.Parameters.Add(New SqlParameter("@bidangUsahaID", SqlDbType.Char, 5) With {.Value = BidangUsahaID})
        sqlcommand.Parameters.Add(New SqlParameter("@totalEmployee", SqlDbType.Int) With {.Value = TotalEmployee})
        sqlcommand.Parameters.Add(New SqlParameter("@businessCategory", SqlDbType.Char, 1) With {.Value = BusinessCategory})
        sqlcommand.Parameters.Add(New SqlParameter("@groupIDDebitur", SqlDbType.Char, 6) With {.Value = GroupIDDebitur})
        sqlcommand.Parameters.Add(New SqlParameter("@groupDebiturName", SqlDbType.Char, 20) With {.Value = GroupDebiturName})
        sqlcommand.Parameters.Add(New SqlParameter("@relationshipWithBank", SqlDbType.Char, 4) With {.Value = RelationshipWithBank})
        sqlcommand.Parameters.Add(New SqlParameter("@ratingDebtor", SqlDbType.Char, 5) With {.Value = RatingDebtor})
        sqlcommand.Parameters.Add(New SqlParameter("@ratingInstitution", SqlDbType.Char, 50) With {.Value = RatingInstitution})
        sqlcommand.Parameters.Add(New SqlParameter("@goPublic", SqlDbType.Char, 1) With {.Value = GoPublic})

        sqlcommand.Parameters.Add(New SqlParameter("@personalCustType", SqlDbType.Char, 2) With {.Value = PersonalCustType})
        sqlcommand.Parameters.Add(New SqlParameter("@noTDP", SqlDbType.Char, 30) With {.Value = NoTDP})
        sqlcommand.Parameters.Add(New SqlParameter("@nOSIUP", SqlDbType.Char, 30) With {.Value = NOSIUP})
        Return sqlcommand

    End Function



    Protected Overrides Sub doValidate()
        If (String.IsNullOrEmpty(MfCif)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " MFCIF kosong. </br>")

        If (String.IsNullOrEmpty(MfId)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " MFID kosong. </br>")
        If (String.IsNullOrEmpty(MfBatchNo)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " MFBatchNo   kosong. </br>")
        If (String.IsNullOrEmpty(MfBranchID)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " MFBranchID kosong. </br>")
        If (String.IsNullOrEmpty(Gender)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " Gender kosong. </br>")
        If (String.IsNullOrEmpty(DebtorName)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " DebtorName kosong. </br>")
        If (String.IsNullOrEmpty(DebtorType)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " DebtorType kosong. </br>")
        If (String.IsNullOrEmpty(BirthPlace)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " BirthPlace kosong. </br>")
        If (String.IsNullOrEmpty(MotherName)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " MotherName kosong. </br>")
        If (String.IsNullOrEmpty(ReligionID)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " ReligionID  kosong. </br>")
        If (String.IsNullOrEmpty(MaritalStatus)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " MaritalStatus	 kosong. </br>")
        'If (DependenceNumber <= 0) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & mfcif & " DependenceNumber  kosong. </br>")
        If (String.IsNullOrEmpty(KTPNo)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " KTPNo kosong. </br>")
        If (String.IsNullOrEmpty(Nationality)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " Nationality kosong. </br>")
        If (String.IsNullOrEmpty(EducationID)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " EducationID kosong. </br>")
        'If (String.IsNullOrEmpty(PlaceofAktaIssued)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & mfcif & " PlaceofAktaIssued	 kosong. </br>")
        'If (String.IsNullOrEmpty(AktaNo)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & mfcif & " AktaNo kosong. </br>")
        If (String.IsNullOrEmpty(DebtorAddress)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " DebtorAddress	 kosong. </br>")
        If (String.IsNullOrEmpty(DebtorDati2)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " DebtorDati2 kosong. </br>")
        If (String.IsNullOrEmpty(DebtorZipCode)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " DebtorZipCode	kosong. </br>")
        If (String.IsNullOrEmpty(DebtorKelurahan)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " DebtorKelurahan kosong. </br>")
        If (String.IsNullOrEmpty(DebtorKecamatan)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " DebtorKecamatan kosong. </br>")
        If (String.IsNullOrEmpty(HomeStatusID)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " HomeStatusID  kosong. </br>")
        If (String.IsNullOrEmpty(GolonganDebiturID)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " GolonganDebiturID	 kosong. </br>")
        If (String.IsNullOrEmpty(DebtorJobID)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " DebtorJobID kosong. </br>")
        'If (String.IsNullOrEmpty(CompanyName)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & mfcif & " CompanyName	 kosong. </br>")

        If (MonthlyFixedIncome + AdditionalIncome <= 0 And DebtorType = "P") Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " MonthlyFixedIncome + AdditionalIncome kosong. </br>")

        'If (LivingCost <= 0) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " LivingCost kosong. </br>")
        If (String.IsNullOrEmpty(BidangUsahaID)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " BidangUsahaID	kosong. </br>")
        'If (TotalEmployee <= 0) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & mfcif & " TotalEmployee	kosong. </br>")
        If (String.IsNullOrEmpty(BusinessCategory)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " BusinessCategory kosong. </br>")
        If (String.IsNullOrEmpty(PersonalCustType)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " PersonalCustType  kosong. </br>")
        If (DebtorType = "C") Then
            If (String.IsNullOrEmpty(LastAktaDate)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " LastAktaDate  harus di isi. </br>")
            If (String.IsNullOrEmpty(NoTDP)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " NoTDP Wajib diisi jika debitur merupakan Badan Usaha. </br>")
            If (String.IsNullOrEmpty(NOSIUP)) Then build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " NOSIUP Wajib diisi jika debitur merupakan Badan Usaha. </br>")
        End If

        If (Not String.IsNullOrEmpty(TxtAktaIssuedDate.Trim)) Then
            Try
                AktaIssuedDate = DateTime.ParseExact(TxtAktaIssuedDate.Trim, "yyyyMMdd", Nothing)
            Catch ex As Exception
                build.AppendLine($"File DISBDEBTOR MFCIF No: " & MfCif & " AktaIssuedDate " & ex.Message & ". </br>")
            End Try
        Else
            If (DebtorType = "C") Then
                build.AppendLine("File DISBDEBTOR MFCIF No: " & MfCif & " AktaIssuedDate  harus di isi. </br>")
            Else
                AktaIssuedDate = New Date(1900, 1, 1)
            End If
        End If
    End Sub
End Class
