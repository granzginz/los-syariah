﻿
Imports System.Data.SqlClient
Imports System.Text

Public Class DisbHeader
    Inherits AbsDisb


    Protected Sub New()
        MyBase.New("", "", 0)
    End Sub

    Sub New(oriSplit As String())
        MyBase.New(oriSplit(0), oriSplit(1), 0)
        MultiFinanceName = oriSplit(2)
        RequestDateTime = DateTime.ParseExact(dFmt(oriSplit(3)), "yyyyMMdd", Nothing)
        TransactionType = oriSplit(4)
        MfFacilityNo = oriSplit(5)
        FacilityType = oriSplit(6)
        TotalAccount = oriSplit(7)
        TotalAmount = oriSplit(8)
        Status = oriSplit(9)
    End Sub

    Public Shared Function Instance() As DisbHeader
        Return New DisbHeader()
    End Function
    Public ReadOnly Property MultiFinanceName As String
    Public ReadOnly Property RequestDateTime As DateTime
    Public ReadOnly Property TransactionType As String
    Public ReadOnly Property MfFacilityNo As String
    Public ReadOnly Property FacilityType As String
    Public ReadOnly Property TotalAccount As Decimal
    Public ReadOnly Property TotalAmount As Decimal
    Public ReadOnly Property Status As String

    Private _disbDebtor As IEnumerable(Of DisbDebtor)

    Public Overrides Function addToList(oriSplit() As String) As AbsDisb
        Try
            Return New DisbHeader(oriSplit)
        Catch ex As Exception
            Throw New Exception("Error parse file DisbHeader.")
        End Try
    End Function


    Const _spName As String = "spDisb_Header_add"
    Public Overrides Function command(cnn As SqlConnection) As SqlCommand
        Dim sqlcommand = New SqlCommand(_spName, cnn)
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.Parameters.Clear()
        sqlcommand.Parameters.Add(New SqlParameter("@mfId", SqlDbType.Char, 10) With {.Value = MfId})
        sqlcommand.Parameters.Add(New SqlParameter("@mfBatchNo", SqlDbType.Char, 20) With {.Value = MfBatchNo})
        sqlcommand.Parameters.Add(New SqlParameter("@multiFinanceName", SqlDbType.Char, 50) With {.Value = MultiFinanceName})
        sqlcommand.Parameters.Add(New SqlParameter("@requestDateTime", SqlDbType.DateTime) With {.Value = RequestDateTime})
        sqlcommand.Parameters.Add(New SqlParameter("@transactionType", SqlDbType.Char, 1) With {.Value = TransactionType})
        sqlcommand.Parameters.Add(New SqlParameter("@mfFacilityNo", SqlDbType.Char, 20) With {.Value = MfFacilityNo})
        sqlcommand.Parameters.Add(New SqlParameter("@facilityType", SqlDbType.Char, 2) With {.Value = FacilityType})
        sqlcommand.Parameters.Add(New SqlParameter("@totalAccount", SqlDbType.Decimal) With {.Value = TotalAccount})
        sqlcommand.Parameters.Add(New SqlParameter("@totalAmount", SqlDbType.Decimal) With {.Value = TotalAmount})
        sqlcommand.Parameters.Add(New SqlParameter("@status", SqlDbType.Char, 3) With {.Value = Status})
        Return sqlcommand

    End Function


    Protected Overrides Sub doValidate()

        If (String.IsNullOrEmpty(MfId)) Then build.AppendLine("DisbHeader MfId kosong. </br>")
        If (String.IsNullOrEmpty(MfBatchNo)) Then build.AppendLine("DisbHeader MfBatchNo kosong. </br>")
        If (String.IsNullOrEmpty(MultiFinanceName)) Then build.AppendLine("DisbHeader MultiFinanceName kosong. </br>")

        If (String.IsNullOrEmpty(TransactionType)) Then build.AppendLine("DisbHeader TransactionType kosong. </br>")
        If (String.IsNullOrEmpty(MfFacilityNo)) Then build.AppendLine("DisbHeader MfFacilityNo kosong. </br>")
        If (String.IsNullOrEmpty(FacilityType)) Then build.AppendLine("DisbHeader FacilityType kosong. </br>")

        'If (String.IsNullOrEmpty(TotalAccount)) Then build.AppendLine("DisbHeader MfId kosong. </br>")
        'If (String.IsNullOrEmpty(TotalAmount)) Then build.AppendLine("DisbHeader MfId kosong. </br>")
        If (String.IsNullOrEmpty(Status)) Then build.AppendLine("DisbHeader Status kosong. </br>")
        If Status <> "REQ" Then build.AppendLine("DisbHeader Status harus diisi REQ. </br>")


    End Sub
End Class









