﻿

Imports System.Data.SqlClient

Public Class DisbInsuranceDetail
    Inherits BaseDisb
    Protected Sub New()
        MyBase.New("", "", 0, "", "")
    End Sub
    Sub New(oriSplit As String())
        MyBase.New(oriSplit(0), oriSplit(1), 0, oriSplit(2), "")

        Year = oriSplit(3)
        InsType = oriSplit(4)
        InsAmount = oriSplit(5)
        InsRate = oriSplit(6)
        InsTenor = oriSplit(7)
        InsStartDate = DateTime.ParseExact(dFmt(oriSplit(8)), "yyyyMMdd", Nothing)
        InsEndDate = DateTime.ParseExact(dFmt(oriSplit(9)), "yyyyMMdd", Nothing)
        PolisNo = oriSplit(10)
        PolisDate = DateTime.ParseExact(dFmt(oriSplit(11)), "yyyyMMdd", Nothing)
        PremiGross = oriSplit(12)
        PremiNet = oriSplit(13)
    End Sub

    Public Shared Function Instance() As DisbInsuranceDetail
        Return New DisbInsuranceDetail()
    End Function

    Public Overrides Function addToList(oriSplit() As String) As AbsDisb
        Try
            Return New DisbInsuranceDetail(oriSplit)
        Catch ex As Exception
            Throw New Exception($"Error parse file DisbInsuranceDetail. {ex.Message}")
        End Try
    End Function
    Public ReadOnly Property Year As Int16
    Public ReadOnly Property InsType As String
    Public ReadOnly Property InsAmount As Decimal
    Public ReadOnly Property InsRate As Decimal
    Public ReadOnly Property InsTenor As Int16
    Public ReadOnly Property InsStartDate As DateTime
    Public ReadOnly Property InsEndDate As DateTime
    Public ReadOnly Property PolisNo As String
    Public ReadOnly Property PolisDate As DateTime

    Public ReadOnly Property PremiGross As Decimal
    Public ReadOnly Property PremiNet As Decimal


    Const _spName As String = "spDisb_InsuranceDetail_add"
    Public Overrides Function command(cnn As SqlClient.SqlConnection) As SqlClient.SqlCommand
        Dim sqlcommand = New SqlClient.SqlCommand(_spName, cnn)
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.Parameters.Clear()

        sqlcommand.Parameters.Add(New SqlParameter("@mFID", SqlDbType.Char, 10) With {.Value = MfId})
        sqlcommand.Parameters.Add(New SqlParameter("@mFBatchNo", SqlDbType.Char, 20) With {.Value = MfBatchNo})
        sqlcommand.Parameters.Add(New SqlParameter("@mFAgreementNo", SqlDbType.Char, 20) With {.Value = MfAgreementNo})
        sqlcommand.Parameters.Add(New SqlParameter("@year", SqlDbType.Int) With {.Value = Year})
        sqlcommand.Parameters.Add(New SqlParameter("@insType", SqlDbType.Char, 10) With {.Value = InsType})
        sqlcommand.Parameters.Add(New SqlParameter("@insAmount", SqlDbType.Decimal) With {.Value = InsAmount})
        sqlcommand.Parameters.Add(New SqlParameter("@insRate", SqlDbType.Decimal) With {.Value = InsRate})
        sqlcommand.Parameters.Add(New SqlParameter("@insTenor", SqlDbType.Int) With {.Value = InsTenor})
        sqlcommand.Parameters.Add(New SqlParameter("@insStartDate", SqlDbType.Date) With {.Value = InsStartDate})
        sqlcommand.Parameters.Add(New SqlParameter("@insEndDate", SqlDbType.Date) With {.Value = InsEndDate})
        sqlcommand.Parameters.Add(New SqlParameter("@polisNo", SqlDbType.Char, 50) With {.Value = PolisNo})
        sqlcommand.Parameters.Add(New SqlParameter("@polisDate", SqlDbType.Date) With {.Value = PolisDate})
        sqlcommand.Parameters.Add(New SqlParameter("@premiGross", SqlDbType.Decimal) With {.Value = PremiGross})
        sqlcommand.Parameters.Add(New SqlParameter("@premiNet", SqlDbType.Decimal) With {.Value = PremiNet})
        Return sqlcommand

    End Function


    Protected Overrides Sub doValidate()
        If (String.IsNullOrEmpty(MfAgreementNo)) Then build.AppendLine("DisbInsuranceDetail MFAgreementNo kosong. </br>")

        If (String.IsNullOrEmpty(MfId)) Then build.AppendLine("FILE DISBINSURANCEDETAIL MFAgreementNo:" & MfAgreementNo & " MFID kosong. </br>")
        If (String.IsNullOrEmpty(MfBatchNo)) Then build.AppendLine("FILE DISBINSURANCEDETAIL MFAgreementNo:" & MfAgreementNo & " MFBatchNo kosong. </br>")

        If (String.IsNullOrEmpty(Year)) Then build.AppendLine("FILE DISBINSURANCEDETAIL MFAgreementNo:" & MfAgreementNo & " Year kosong. </br>")
        If (String.IsNullOrEmpty(InsType)) Then build.AppendLine("FILE DISBINSURANCEDETAIL MFAgreementNo:" & MfAgreementNo & " InsType kosong. </br>")
        If (InsAmount <= 0) Then build.AppendLine("FILE DISBINSURANCEDETAIL MFAgreementNo:" & MfAgreementNo & " InsAmount kosong. </br>")
        If (InsRate <= 0) Then build.AppendLine("FILE DISBINSURANCEDETAIL MFAgreementNo:" & MfAgreementNo & " InsRate kosong. </br>")
        If (InsTenor <= 0) Then build.AppendLine("FILE DISBINSURANCEDETAIL MFAgreementNo:" & MfAgreementNo & " InsTenor kosong. </br>")
        If (String.IsNullOrEmpty(InsStartDate)) Then build.AppendLine("FILE DISBINSURANCEDETAIL MFAgreementNo:" & MfAgreementNo & " InsStartDate kosong. </br>")
        If (String.IsNullOrEmpty(InsEndDate)) Then build.AppendLine("FILE DISBINSURANCEDETAIL MFAgreementNo:" & MfAgreementNo & " InsEndDate kosong. </br>")
        If (String.IsNullOrEmpty(PolisNo)) Then build.AppendLine("FILE DISBINSURANCEDETAIL MFAgreementNo:" & MfAgreementNo & " PolisNo kosong. </br>")
        If (String.IsNullOrEmpty(PolisDate)) Then build.AppendLine("FILE DISBINSURANCEDETAIL MFAgreementNo:" & MfAgreementNo & " PolisDate kosong. </br>")
        If (PremiGross <= 0) Then build.AppendLine("FILE DISBINSURANCEDETAIL MFAgreementNo:" & MfAgreementNo & " PremiGross kosong. </br>")
        If (PremiNet <= 0) Then build.AppendLine("FILE DISBINSURANCEDETAIL MFAgreementNo:" & MfAgreementNo & " PremiNet kosong. </br>")
    End Sub
End Class
