﻿Imports System.Data.SqlClient

Public Class PaymentAgreement
    Inherits BaseDisb
    Protected Sub New() '_mfId As String, _mfBatchNo As String, _revNo As Int16, _mfcif As String)
        MyBase.New("", "", 0, "", "")
    End Sub

    Public Shared Function Instance() As PaymentAgreement
        Return New PaymentAgreement()
    End Function

    Sub New(oriSplit As String())
        MyBase.New(oriSplit(0), oriSplit(5), 0, oriSplit(2), "")
        ReferenceNo = oriSplit(1)
        DebtorName = oriSplit(3)
        MFFacilityNo = oriSplit(4)
        InsSeqNo = oriSplit(6)
        DueDate = DateTime.ParseExact(dFmt(oriSplit(7)), "yyyyMMdd", Nothing)
        InstallmentAmount = oriSplit(8)
        PrincipalAmount = oriSplit(9)
        InterestAmount = oriSplit(10)
        LateChargesAmount = oriSplit(11)
        TotalAmount = oriSplit(12)
    End Sub
    'MFID
    Public ReadOnly Property ReferenceNo As String
    'MFAgreementNo 
    Public ReadOnly Property DebtorName As String
    Public ReadOnly Property MFFacilityNo As String
    'Public ReadOnly Property MFBatchNo As String
    Public ReadOnly Property InsSeqNo As Decimal
    Public ReadOnly Property DueDate As DateTime
    Public ReadOnly Property InstallmentAmount As Decimal
    Public ReadOnly Property PrincipalAmount As Decimal
    Public ReadOnly Property InterestAmount As Decimal
    Public ReadOnly Property LateChargesAmount As Decimal
    Public ReadOnly Property TotalAmount As Decimal

    Protected Overrides Sub doValidate()

        If (String.IsNullOrEmpty(MfId)) Then build.AppendLine("PaymentAgreement MfId kosong.")
        If (String.IsNullOrEmpty(ReferenceNo)) Then build.AppendLine("PaymentAgreement ReferenceNo kosong.")
        If (String.IsNullOrEmpty(DebtorName)) Then build.AppendLine("PaymentAgreement MultiFinanceName kosong.")

        If (String.IsNullOrEmpty(MFFacilityNo)) Then build.AppendLine("PaymentAgreement MFFacilityNo kosong.")
        If (String.IsNullOrEmpty(MfBatchNo)) Then build.AppendLine("PaymentAgreement MfBatchNo kosong.")
        If (InsSeqNo <= 0) Then build.AppendLine("PaymentAgreement InsSeqNo tidak valid.")
        If (InstallmentAmount <= 0) Then build.AppendLine("PaymentAgreement InstallmentAmount tidak valid.")
        If (PrincipalAmount <= 0) Then build.AppendLine("PaymentAgreement PrincipalAmount tidak valid.")
        If (InterestAmount <= 0) Then build.AppendLine("PaymentAgreement InterestAmount tidak valid.")
        If (LateChargesAmount < 0) Then build.AppendLine("PaymentAgreement LateChargesAmount tidak valid.")
        If (TotalAmount <= 0) Then build.AppendLine("PaymentAgreement TotalAmount tidak valid.")
    End Sub


    Public Overrides Function addToList(oriSplit() As String) As AbsDisb
        Try
            Return New PaymentAgreement(oriSplit)
        Catch ex As Exception
            Throw New Exception("Error parse file DisbHeader.")
        End Try
    End Function


    Const _spName As String = "sp_Disb_PaymentAgreement_add"
    Public Overrides Function command(cnn As SqlConnection) As SqlCommand
        Dim sqlcommand = New SqlCommand(_spName, cnn)
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.Parameters.Clear()
        sqlcommand.Parameters.Add(New SqlParameter("mFID", SqlDbType.Char, 10) With {.Value = MfId})
        sqlcommand.Parameters.Add(New SqlParameter("referenceNo", SqlDbType.Char, 20) With {.Value = ReferenceNo})
        sqlcommand.Parameters.Add(New SqlParameter("mFAgreementNo", SqlDbType.Char, 20) With {.Value = MfAgreementNo})
        sqlcommand.Parameters.Add(New SqlParameter("debtorName", SqlDbType.Char, 50) With {.Value = DebtorName})
        sqlcommand.Parameters.Add(New SqlParameter("mFFacilityNo", SqlDbType.Char, 20) With {.Value = MFFacilityNo})
        sqlcommand.Parameters.Add(New SqlParameter("mFBatchNo", SqlDbType.Char, 20) With {.Value = MfBatchNo})
        sqlcommand.Parameters.Add(New SqlParameter("insSeqNo", SqlDbType.Decimal) With {.Value = InsSeqNo})
        sqlcommand.Parameters.Add(New SqlParameter("dueDate", SqlDbType.Date) With {.Value = DueDate})
        sqlcommand.Parameters.Add(New SqlParameter("installmentAmount", SqlDbType.Decimal) With {.Value = InstallmentAmount})
        sqlcommand.Parameters.Add(New SqlParameter("principalAmount", SqlDbType.Decimal) With {.Value = PrincipalAmount})
        sqlcommand.Parameters.Add(New SqlParameter("interestAmount", SqlDbType.Decimal) With {.Value = InterestAmount})
        sqlcommand.Parameters.Add(New SqlParameter("lateChargesAmount", SqlDbType.Decimal) With {.Value = LateChargesAmount})
        sqlcommand.Parameters.Add(New SqlParameter("totalAmount", SqlDbType.Decimal) With {.Value = TotalAmount})
        Return sqlcommand

    End Function
End Class
