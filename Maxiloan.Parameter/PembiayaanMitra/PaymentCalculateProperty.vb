﻿<Serializable> Public Class PaymentCalculateProperty : Inherits Common
    Public Property PRINCIPALAMOUNT() As Decimal
    Public Property TENOR() As Int16
    Public Property RATE() As Decimal
    Public Property INTERESTAMOUNT() As Decimal
    Public Property OSPRINCIPAL() As Decimal
    Public Property OSINTEREST() As Decimal
    Public Property PAIDSEQNO() As Decimal
    Public Property STATUS() As String
    Public Property MFAGREEMENTNO As String
    Public Property TANGGALEFEKTIF As DateTime
    Public Property MFFACILITYNO As String
    Public Property MFBATCHNO As String
    Public Property MFID As String
    Public Property SISAPOKOK As Decimal
    Public Property BUNGABERJALAN As Decimal
    Public Property DENDAPERUSAHAAN As Decimal
    Public Property totalpelunasan As Decimal
    Public Property DisbList As DataTable
    Public Property RecipientName As String
    Public Property ReferenceNo As String
    Public Property BankAccountID As String
    Public Property Notes As String
    Public Property DateValue As Date
    Public Property disburselist As DataTable
    Public Property ListData As DataTable
End Class
