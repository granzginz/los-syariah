﻿Public Class DisbRejection
    Inherits BaseDisb
    Protected Sub New(_mfId As String, _mfAgreementNo As String)
        MyBase.New(_mfId, "", 0, _mfAgreementNo, "")
    End Sub
    Public ReadOnly Property DebtorName As String
    Public ReadOnly Property NTFToBank As String
    Public ReadOnly Property RejectionID As String
    Public ReadOnly Property RejectionCategory As String
    Public ReadOnly Property RejectionDescription As String

    Const _spName As String = ""
    Public Overrides Function command(cnn As SqlClient.SqlConnection) As SqlClient.SqlCommand
        Dim sqlcommand = New SqlClient.SqlCommand(_spName, cnn)
        sqlcommand.Parameters.Clear()
        Return sqlcommand

    End Function
    Protected Overrides Sub doValidate()

    End Sub
End Class
