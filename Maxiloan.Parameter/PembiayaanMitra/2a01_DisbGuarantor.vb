﻿
Imports System.Data.SqlClient

Public Class DisbGuarantor
    Inherits BaseDisb
    Protected Sub New()
        MyBase.New("", "", 0, "", "")
    End Sub
    Sub New(oriSplit As String())
        MyBase.New(oriSplit(0), oriSplit(1), 0, oriSplit(2), "")

        GuarantorName = oriSplit(3)
        GuarantorType = oriSplit(4)
        MaritalStatus = oriSplit(5)
        KTPNo = oriSplit(6)
        NPWPNo = oriSplit(7)

        GuarantorAddress = oriSplit(8)
        GuarantorDati2 = oriSplit(9)
        GuarantorKelurahan = oriSplit(10)
        GuarantorKecamatan = oriSplit(11)
        GuarantorZipCode = oriSplit(12)
        BirthPlace = oriSplit(13)
        BirthDate = DateTime.ParseExact(dFmt(oriSplit(14)), "yyyyMMdd", Nothing)
        GuarantorAreaPhone = oriSplit(15)
        GuarantorPhone = oriSplit(16)
        GuarantorMobilePhone1 = oriSplit(17)
        GolonganPenjamin = oriSplit(18)
        BagianDijamin = oriSplit(19)
        PrimeBank = oriSplit(20)
    End Sub
    Public Shared Function Instance() As DisbGuarantor
        Return New DisbGuarantor()
    End Function
    Public Overrides Function addToList(oriSplit() As String) As AbsDisb
        Try
            Return New DisbGuarantor(oriSplit)
        Catch ex As Exception
            Throw New Exception($"Error parse file DisbGuarantor. {ex.Message}")
        End Try
    End Function

    Public ReadOnly Property GuarantorName As String
    Public ReadOnly Property GuarantorType As String
    Public ReadOnly Property MaritalStatus As String
    Public ReadOnly Property KTPNo As String
    Public ReadOnly Property NPWPNo As String
    Public ReadOnly Property GuarantorAddress As String
    Public ReadOnly Property GuarantorDati2 As String
    Public ReadOnly Property GuarantorKelurahan As String
    Public ReadOnly Property GuarantorKecamatan As String
    Public ReadOnly Property GuarantorZipCode As String
    Public ReadOnly Property BirthPlace As String
    Public ReadOnly Property BirthDate As DateTime
    Public ReadOnly Property GuarantorAreaPhone As String
    Public ReadOnly Property GuarantorPhone As String
    Public ReadOnly Property GuarantorMobilePhone1 As String
    Public ReadOnly Property GolonganPenjamin As String
    Public ReadOnly Property BagianDijamin As Decimal
    Public ReadOnly Property PrimeBank As String



    Const _spName As String = "spDisb_DebtorGuarantor_add"
    Public Overrides Function command(cnn As SqlClient.SqlConnection) As SqlClient.SqlCommand
        Dim sqlcommand = New SqlClient.SqlCommand(_spName, cnn)
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.Parameters.Clear()

        sqlcommand.Parameters.Add(New SqlParameter("@mFID", SqlDbType.Char, 10) With {.Value = MfId})
        sqlcommand.Parameters.Add(New SqlParameter("@mFBatchNo", SqlDbType.Char, 20) With {.Value = MfBatchNo})
        sqlcommand.Parameters.Add(New SqlParameter("@mFAgreementNo", SqlDbType.Char, 20) With {.Value = MfAgreementNo})
        sqlcommand.Parameters.Add(New SqlParameter("@guarantorName", SqlDbType.Char, 50) With {.Value = GuarantorName})
        sqlcommand.Parameters.Add(New SqlParameter("@guarantorType", SqlDbType.Char, 1) With {.Value = GuarantorType})
        sqlcommand.Parameters.Add(New SqlParameter("@maritalStatus", SqlDbType.Char, 1) With {.Value = MaritalStatus})
        sqlcommand.Parameters.Add(New SqlParameter("@kTPNo", SqlDbType.Char, 30) With {.Value = KTPNo})
        sqlcommand.Parameters.Add(New SqlParameter("@nPWPNo", SqlDbType.Char, 20) With {.Value = NPWPNo})
        sqlcommand.Parameters.Add(New SqlParameter("@guarantorAddress", SqlDbType.Char, 100) With {.Value = GuarantorAddress})
        sqlcommand.Parameters.Add(New SqlParameter("@guarantorDati2", SqlDbType.Char, 4) With {.Value = GuarantorDati2})
        sqlcommand.Parameters.Add(New SqlParameter("@guarantorKelurahan", SqlDbType.Char, 50) With {.Value = GuarantorKelurahan})
        sqlcommand.Parameters.Add(New SqlParameter("@guarantorKecamatan", SqlDbType.Char, 50) With {.Value = GuarantorKecamatan})
        sqlcommand.Parameters.Add(New SqlParameter("@guarantorZipCode", SqlDbType.Char, 4) With {.Value = GuarantorZipCode})
        sqlcommand.Parameters.Add(New SqlParameter("@birthPlace", SqlDbType.Char, 50) With {.Value = BirthPlace})
        sqlcommand.Parameters.Add(New SqlParameter("@birthDate", SqlDbType.Date) With {.Value = BirthDate})
        sqlcommand.Parameters.Add(New SqlParameter("@guarantorAreaPhone", SqlDbType.Char, 4) With {.Value = GuarantorAreaPhone})
        sqlcommand.Parameters.Add(New SqlParameter("@guarantorPhone", SqlDbType.Char, 8) With {.Value = GuarantorPhone})
        sqlcommand.Parameters.Add(New SqlParameter("@guarantorMobilePhone1", SqlDbType.Char, 15) With {.Value = GuarantorMobilePhone1})
        sqlcommand.Parameters.Add(New SqlParameter("@golonganPenjamin", SqlDbType.Char, 3) With {.Value = GolonganPenjamin})
        sqlcommand.Parameters.Add(New SqlParameter("@bagianDijamin", SqlDbType.Decimal) With {.Value = BagianDijamin})
        sqlcommand.Parameters.Add(New SqlParameter("@primeBank", SqlDbType.Char, 1) With {.Value = PrimeBank})

        Return sqlcommand

    End Function

    Protected Overrides Sub doValidate()
        If (String.IsNullOrEmpty(MfAgreementNo)) Then build.AppendLine("DisbGuarantor MFAgreementNo kosong. </br>")
        If (String.IsNullOrEmpty(MfId)) Then build.AppendLine("FILE DISBGUARANTOR  MFAgreementNo:" & MfAgreementNo & " MFID kosong. </br>")
        If (String.IsNullOrEmpty(MfBatchNo)) Then build.AppendLine("FILE DISBGUARANTOR  MFAgreementNo:" & MfAgreementNo & " MFBatchNo kosong. </br>")

        If (String.IsNullOrEmpty(GuarantorName)) Then build.AppendLine("FILE DISBGUARANTOR  MFAgreementNo:" & MfAgreementNo & " GuarantorName  kosong. </br>")
        If (String.IsNullOrEmpty(MaritalStatus)) Then build.AppendLine("FILE DISBGUARANTOR  MFAgreementNo:" & MfAgreementNo & " MaritalStatus  kosong. </br>")
        If (String.IsNullOrEmpty(GuarantorAddress)) Then build.AppendLine("FILE DISBGUARANTOR  MFAgreementNo:" & MfAgreementNo & " GuarantorAddress kosong. </br>")
        If (String.IsNullOrEmpty(GuarantorDati2)) Then build.AppendLine("FILE DISBGUARANTOR  MFAgreementNo:" & MfAgreementNo & " GuarantorDati2 kosong. </br>")
        If (String.IsNullOrEmpty(GuarantorKelurahan)) Then build.AppendLine("FILE DISBGUARANTOR  MFAgreementNo:" & MfAgreementNo & " GuarantorKelurahan kosong. </br>")
        If (String.IsNullOrEmpty(GuarantorKecamatan)) Then build.AppendLine("FILE DISBGUARANTOR  MFAgreementNo:" & MfAgreementNo & " GuarantorKecamatan kosong. </br>")
        If (String.IsNullOrEmpty(BirthPlace)) Then build.AppendLine("FILE DISBGUARANTOR  MFAgreementNo:" & MfAgreementNo & " BirthPlace kosong. </br>")
        ' If (String.IsNullOrEmpty(BirthDate)) Then build.AppendLine("FILE DISBGUARANTOR  MFAgreementNo:" & MFAgreementNo & " BirthDate  kosong. </br>")
        If (String.IsNullOrEmpty(GolonganPenjamin)) Then build.AppendLine("FILE DISBGUARANTOR  MFAgreementNo:" & MfAgreementNo & " GolonganPenjamin kosong. </br>")
        If (BagianDijamin <= 0) Then build.AppendLine("FILE DISBGUARANTOR  MFAgreementNo:" & MfAgreementNo & " BagianDijamin kosong. </br>")
        If (String.IsNullOrEmpty(PrimeBank)) Then build.AppendLine("FILE DISBGUARANTOR  MFAgreementNo:" & MfAgreementNo & " PrimeBank kosong. </br>")
    End Sub
End Class
