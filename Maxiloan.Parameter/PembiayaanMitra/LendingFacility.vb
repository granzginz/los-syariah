﻿Public Class LendingFacility : Inherits Common
    Public Property MFCode As String
    Public Property MFFacilityNo As String
    Public Property MFFacilityName As String
    Public Property JenisFacility As String
    Public Property Plafond As Decimal
    Public Property OSPlafond As Decimal
    Public Property SifatFacility As String
    Public Property LenderPortion As Decimal
    Public Property AssetCondition As String
    Public Property DrawdownStartDate As DateTime
    Public Property DrawdownEndDate As DateTime
    Public Property LateCharges As Decimal
    Public Property Penalty As Decimal
    Public Property FacilityDate As DateTime
    Public Property MinDrawDownAmount As Decimal
    Public Property UserUpdate As String
    Public Property DateUpdate As DateTime
End Class
