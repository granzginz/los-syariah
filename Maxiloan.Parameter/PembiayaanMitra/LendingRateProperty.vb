﻿Imports System.Data

Public Class LendingRateProperty : Inherits Common

    Private _mFCode1 As String
    Private _mFFacilityNo1 As String
    Private _tenorFrom1 As Int16
    Private _tenorTo1 As Int16
    Private _usedRate1 As Decimal
    Private _newRate1 As Decimal
    Private _userUpdate1 As String
    Private _dateUpdate1 As String

    Public Property MFCode As String
        Get
            Return _mFCode1
        End Get
        Set(value As String)
            _mFCode1 = value
        End Set
    End Property

    Public Property MFFacilityNo As String
        Get
            Return _mFFacilityNo1
        End Get
        Set(value As String)
            _mFFacilityNo1 = value
        End Set
    End Property

    Public Property TenorFrom As Short
        Get
            Return _tenorFrom1
        End Get
        Set(value As Short)
            _tenorFrom1 = value
        End Set
    End Property

    Public Property TenorTo As Short
        Get
            Return _tenorTo1
        End Get
        Set(value As Short)
            _tenorTo1 = value
        End Set
    End Property

    Public Property UsedRate As Decimal
        Get
            Return _usedRate1
        End Get
        Set(value As Decimal)
            _usedRate1 = value
        End Set
    End Property

    Public Property NewRate As Decimal
        Get
            Return _newRate1
        End Get
        Set(value As Decimal)
            _newRate1 = value
        End Set
    End Property

    Public Property UserUpdate As String
        Get
            Return _userUpdate1
        End Get
        Set(value As String)
            _userUpdate1 = value
        End Set
    End Property

    Public Property DateUpdate As String
        Get
            Return _dateUpdate1
        End Get
        Set(value As String)
            _dateUpdate1 = value
        End Set
    End Property

    Public Property ListData() As DataTable
End Class
