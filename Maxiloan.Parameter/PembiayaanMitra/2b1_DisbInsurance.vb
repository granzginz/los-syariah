﻿
Imports System.Data.SqlClient

Public Class DisbInsurance
    Inherits BaseDisb
    Protected Sub New()
        MyBase.New("", "", 0, "", "")
    End Sub
    Sub New(oriSplit As String())
        MyBase.New(oriSplit(0), oriSplit(1), 0, oriSplit(2), "")
        InsuranceId = oriSplit(3)
        InsuranceBranch = oriSplit(4)
        InsuranceName = oriSplit(5)
        InsAddress = oriSplit(6)
        InsKelurahan = oriSplit(7)
        InsKecamatan = oriSplit(8)
        InsCity = oriSplit(9)
        InsZipCode = oriSplit(10)
        InsArea = oriSplit(11)
        InsCoverType = oriSplit(12)
        InsAmount = oriSplit(13)
        InsTenor = oriSplit(14)
        InsStartDate = DateTime.ParseExact(dFmt(oriSplit(15)), "yyyyMMdd", Nothing)
        InsEndDate = DateTime.ParseExact(dFmt(oriSplit(16)), "yyyyMMdd", Nothing)
        InsType = oriSplit(17)
        PolisNo = oriSplit(18)
        PolisDate = DateTime.ParseExact(dFmt(oriSplit(19)), "yyyyMMdd", Nothing)
        PremiGross = oriSplit(20)
        PremiNet = oriSplit(21)
    End Sub
    Public Shared Function Instance() As DisbInsurance
        Return New DisbInsurance()
    End Function
    Public Overrides Function addToList(oriSplit() As String) As AbsDisb

        Try
            Return New DisbInsurance(oriSplit)
        Catch ex As Exception
            Throw New Exception($"Error parse file DisbInsurance. {ex.Message}")
        End Try
    End Function

    Public ReadOnly Property InsuranceId As String
    Public ReadOnly Property InsuranceBranch As String
    Public ReadOnly Property InsuranceName As String
    Public ReadOnly Property InsAddress As String
    Public ReadOnly Property InsKelurahan As String
    Public ReadOnly Property InsKecamatan As String
    Public ReadOnly Property InsCity As String
    Public ReadOnly Property InsZipCode As Decimal
    Public ReadOnly Property InsArea As String
    Public ReadOnly Property InsCoverType As String
    Public ReadOnly Property InsAmount As Decimal
    Public ReadOnly Property InsTenor As Decimal
    Public ReadOnly Property InsStartDate As DateTime
    Public ReadOnly Property InsEndDate As DateTime
    Public ReadOnly Property InsType As String
    Public ReadOnly Property PolisNo As String
    Public ReadOnly Property PolisDate As DateTime
    Public ReadOnly Property PremiGross As Decimal
    Public ReadOnly Property PremiNet As Decimal

    Private _insuranceDetails As IEnumerable(Of DisbInsuranceDetail)


    Const _spName As String = "spDisb_Insurance_add"
    Public Overrides Function command(cnn As SqlClient.SqlConnection) As SqlClient.SqlCommand
        Dim sqlcommand = New SqlClient.SqlCommand(_spName, cnn)
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.Parameters.Clear()
        sqlcommand.Parameters.Add(New SqlParameter("@mFID", SqlDbType.Char, 10) With {.Value = MfId})
        sqlcommand.Parameters.Add(New SqlParameter("@mFBatchNo", SqlDbType.Char, 20) With {.Value = MfBatchNo})
        sqlcommand.Parameters.Add(New SqlParameter("@mFAgreementNo", SqlDbType.Char, 20) With {.Value = MfAgreementNo})
        sqlcommand.Parameters.Add(New SqlParameter("@insuranceID", SqlDbType.Char, 2) With {.Value = InsuranceId})
        sqlcommand.Parameters.Add(New SqlParameter("@insuranceBranch", SqlDbType.Char, 30) With {.Value = InsuranceBranch})
        sqlcommand.Parameters.Add(New SqlParameter("@insuranceName", SqlDbType.Char, 50) With {.Value = InsuranceName})
        sqlcommand.Parameters.Add(New SqlParameter("@insAddress", SqlDbType.Char, 50) With {.Value = InsAddress})
        sqlcommand.Parameters.Add(New SqlParameter("@insKelurahan", SqlDbType.Char, 30) With {.Value = InsKelurahan})
        sqlcommand.Parameters.Add(New SqlParameter("@insKecamatan", SqlDbType.Char, 30) With {.Value = InsKecamatan})
        sqlcommand.Parameters.Add(New SqlParameter("@insCity", SqlDbType.Char, 30) With {.Value = InsCity})
        sqlcommand.Parameters.Add(New SqlParameter("@insZipCode", SqlDbType.Char, 5) With {.Value = InsZipCode})
        sqlcommand.Parameters.Add(New SqlParameter("@insArea", SqlDbType.Char, 1) With {.Value = InsArea})
        sqlcommand.Parameters.Add(New SqlParameter("@insCoverType", SqlDbType.Char, 1) With {.Value = InsCoverType})
        sqlcommand.Parameters.Add(New SqlParameter("@insAmount", SqlDbType.Decimal) With {.Value = InsAmount})
        sqlcommand.Parameters.Add(New SqlParameter("@insTenor", SqlDbType.Int) With {.Value = InsTenor})
        sqlcommand.Parameters.Add(New SqlParameter("@insStartDate", SqlDbType.Date) With {.Value = InsStartDate})
        sqlcommand.Parameters.Add(New SqlParameter("@insEndDate", SqlDbType.Date) With {.Value = InsEndDate})
        sqlcommand.Parameters.Add(New SqlParameter("@insType", SqlDbType.Char, 10) With {.Value = InsType})
        sqlcommand.Parameters.Add(New SqlParameter("@polisNo", SqlDbType.Char, 50) With {.Value = PolisNo})
        sqlcommand.Parameters.Add(New SqlParameter("@polisDate", SqlDbType.Date) With {.Value = PolisDate})
        sqlcommand.Parameters.Add(New SqlParameter("@premiGross", SqlDbType.Decimal) With {.Value = PremiGross})
        sqlcommand.Parameters.Add(New SqlParameter("@premiNet", SqlDbType.Decimal) With {.Value = PremiNet})
        Return sqlcommand

    End Function

    Protected Overrides Sub doValidate()
        If (String.IsNullOrEmpty(MfAgreementNo)) Then build.AppendLine("DisbInsurance MFAgreementNo kosong. </br>")

        If (String.IsNullOrEmpty(MfId)) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " MFID kosong. </br>")
        If (String.IsNullOrEmpty(MfBatchNo)) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " MFBatchNo kosong. </br>")

        If (String.IsNullOrEmpty(InsuranceId)) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " InsuranceID kosong. </br>")
        If (String.IsNullOrEmpty(InsuranceBranch)) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " InsuranceBranch kosong. </br>")
        If (String.IsNullOrEmpty(InsuranceName)) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " InsuranceName kosong. </br>")
        If (String.IsNullOrEmpty(InsAddress)) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " InsAddress kosong. </br>")
        If (String.IsNullOrEmpty(InsKelurahan)) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " InsKelurahan kosong. </br>")
        If (String.IsNullOrEmpty(InsKecamatan)) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " InsKecamatan kosong. </br>")
        If (String.IsNullOrEmpty(InsCity)) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " InsCity kosong. </br>")
        If (String.IsNullOrEmpty(InsZipCode)) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " InsZipCode kosong. </br>")
        If (String.IsNullOrEmpty(InsArea)) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " InsArea kosong. </br>")
        If (String.IsNullOrEmpty(InsCoverType)) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " InsCoverType kosong. </br>")
        If (InsAmount <= 0) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " InsAmount kosong. </br>")
        If (InsTenor <= 0) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " InsTenor kosong. </br>")
        If (String.IsNullOrEmpty(InsStartDate)) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " InsStartDate kosong. </br>")
        If (String.IsNullOrEmpty(InsEndDate)) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " InsEndDate kosong. </br>")
        If (String.IsNullOrEmpty(InsType)) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " InsType kosong. </br>")
        If (String.IsNullOrEmpty(PolisNo)) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " PolisNo kosong. </br>")
        If (String.IsNullOrEmpty(PolisDate)) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " PolisDate kosong. </br>")
        If (PremiGross <= 0) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " PremiGross kosong. </br>")
        If (PremiNet <= 0) Then build.AppendLine("FILE DISBINSURANCE MFAgreementNo:" & MfAgreementNo & " PremiNet kosong. </br>")
    End Sub
End Class
