﻿Imports System.Data.SqlClient

Public Class DisbDebtorMgt
    Inherits BaseDisb
    Protected Sub New() '_mfId As String, _mfBatchNo As String, _revNo As Int16, _mfcif As String)
        MyBase.New("", "", 0, "", "")
    End Sub
    Public Shared Function Instance() As DisbDebtorMgt
        Return New DisbDebtorMgt()
    End Function
    Sub New(oriSplit As String())
        MyBase.New(oriSplit(0), oriSplit(1), 0, "", oriSplit(2))
        IdPengurus = oriSplit(3)
        MfBranchID = oriSplit(4)
        MgtName = oriSplit(5)
        BirthPlace = oriSplit(6)
        BirthDate = DateTime.ParseExact(dFmt(oriSplit(7)), "yyyyMMdd", Nothing)
        MaritalStatus = oriSplit(8)
        KTPNo = oriSplit(9)
        NPWPNo = oriSplit(10)
        MgtAddress = oriSplit(11)
        MgtDati2 = oriSplit(12)
        MgtKelurahan = oriSplit(13)
        MgtKecamatan = oriSplit(14)
        MgtZipCode = oriSplit(15)
        OwnerType = oriSplit(16)
        SharePortion = oriSplit(17)
        JobTitleID = oriSplit(18)
    End Sub
    Public Overrides Function addToList(oriSplit() As String) As AbsDisb
        Try
            Return New DisbDebtorMgt(oriSplit)
        Catch ex As Exception
            Throw New Exception($"Error parse file DisbDebtorMgt. {ex.Message}")
        End Try

    End Function
    Public ReadOnly Property IdPengurus As String
    Public ReadOnly Property MfBranchID As String
    Public ReadOnly Property MgtName As String
    Public ReadOnly Property BirthPlace As String
    Public ReadOnly Property BirthDate As DateTime
    Public ReadOnly Property MaritalStatus As String
    Public ReadOnly Property KTPNo As String
    Public ReadOnly Property NPWPNo As String
    Public ReadOnly Property MgtAddress As String
    Public ReadOnly Property MgtDati2 As String
    Public ReadOnly Property MgtKelurahan As String
    Public ReadOnly Property MgtKecamatan As String
    Public ReadOnly Property MgtZipCode As String
    Public ReadOnly Property OwnerType As String
    Public ReadOnly Property SharePortion As Decimal
    Public ReadOnly Property JobTitleID As String



    Private _guarantor As DisbGuarantor



    Const _spName As String = "spDisb_Debtormgt_add"
    Public Overrides Function command(cnn As SqlClient.SqlConnection) As SqlClient.SqlCommand
        Dim sqlcommand = New SqlClient.SqlCommand(_spName, cnn)
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.Parameters.Clear()

        sqlcommand.Parameters.Add(New SqlParameter("@mFID", SqlDbType.Char, 10) With {.Value = MfId})
        sqlcommand.Parameters.Add(New SqlParameter("@mFBatchNo", SqlDbType.Char, 20) With {.Value = MfBatchNo})
        sqlcommand.Parameters.Add(New SqlParameter("@mFCIF", SqlDbType.Char, 20) With {.Value = MfCif})
        sqlcommand.Parameters.Add(New SqlParameter("@iDPengurus", SqlDbType.Char, 2) With {.Value = IdPengurus})
        sqlcommand.Parameters.Add(New SqlParameter("@mFBranchID", SqlDbType.Char, 3) With {.Value = MfBranchID})
        sqlcommand.Parameters.Add(New SqlParameter("@mgtName", SqlDbType.Char, 100) With {.Value = MgtName})
        sqlcommand.Parameters.Add(New SqlParameter("@birthPlace", SqlDbType.Char, 50) With {.Value = BirthPlace})
        sqlcommand.Parameters.Add(New SqlParameter("@birthDate", SqlDbType.Date) With {.Value = BirthDate})
        sqlcommand.Parameters.Add(New SqlParameter("@maritalStatus", SqlDbType.Char, 1) With {.Value = MaritalStatus})
        sqlcommand.Parameters.Add(New SqlParameter("@kTPNo", SqlDbType.Char, 30) With {.Value = KTPNo})
        sqlcommand.Parameters.Add(New SqlParameter("@nPWPNo", SqlDbType.Char, 20) With {.Value = NPWPNo})
        sqlcommand.Parameters.Add(New SqlParameter("@mgtAddress", SqlDbType.Char, 100) With {.Value = MgtAddress})
        sqlcommand.Parameters.Add(New SqlParameter("@mgtDati2", SqlDbType.Char, 4) With {.Value = MgtDati2})
        sqlcommand.Parameters.Add(New SqlParameter("@mgtKelurahan", SqlDbType.Char, 50) With {.Value = MgtKelurahan})
        sqlcommand.Parameters.Add(New SqlParameter("@mgtKecamatan", SqlDbType.Char, 50) With {.Value = MgtKecamatan})
        sqlcommand.Parameters.Add(New SqlParameter("@mgtZipCode", SqlDbType.Char, 4) With {.Value = MgtZipCode})
        sqlcommand.Parameters.Add(New SqlParameter("@ownerType", SqlDbType.Char, 1) With {.Value = OwnerType})
        sqlcommand.Parameters.Add(New SqlParameter("@sharePortion", SqlDbType.Decimal) With {.Value = SharePortion})
        sqlcommand.Parameters.Add(New SqlParameter("@jobTitleID", SqlDbType.Char, 2) With {.Value = JobTitleID})
        Return sqlcommand

    End Function
    Protected Overrides Sub doValidate()

        If (String.IsNullOrEmpty(MfCif)) Then build.AppendLine("DisDebtorMgt MFCIF kosong. </br>")

        If (String.IsNullOrEmpty(MfId)) Then build.AppendLine("FILE DISBDEBTORMGT  MFCIF No:" & MfCif & "MFID kosong. </br>")
        If (String.IsNullOrEmpty(MfBatchNo)) Then build.AppendLine("FILE DISBDEBTORMGT  MFCIF No:" & MfCif & "MFBatchNo kosong. </br>")

        If (String.IsNullOrEmpty(IdPengurus)) Then build.AppendLine("FILE DISBDEBTORMGT  MFCIF No:" & MfCif & " IDPengurus kosong. </br>")
        If (String.IsNullOrEmpty(MfBranchID)) Then build.AppendLine("FILE DISBDEBTORMGT  MFCIF No:" & MfCif & " MFBranchID kosong. </br>")
        If (String.IsNullOrEmpty(MgtName)) Then build.AppendLine("FILE DISBDEBTORMGT  MFCIF No:" & MfCif & " MgtName kosong. </br>")
        'If (String.IsNullOrEmpty(BirthPlace		)) Then build.AppendLine("FILE DISBDEBTORMGT  MFCIF No:" & mfcif & " BirthPlace kosong. </br>") 
        If (String.IsNullOrEmpty(BirthDate)) Then build.AppendLine("FILE DISBDEBTORMGT  MFCIF No:" & MfCif & " BirthDate kosong. </br>")
        'If (String.IsNullOrEmpty(MaritalStatus)) Then build.AppendLine("FILE DISBDEBTORMGT  MFCIF No:" & mfcif & " MaritalStatus kosong. </br>")
        If (String.IsNullOrEmpty(MgtAddress)) Then build.AppendLine("FILE DISBDEBTORMGT  MFCIF No:" & MfCif & " MgtAddress kosong. </br>")
        If (String.IsNullOrEmpty(MgtDati2)) Then build.AppendLine("FILE DISBDEBTORMGT  MFCIF No:" & MfCif & " MgtDati2 kosong. </br>")
        'If (String.IsNullOrEmpty(MgtKelurahan)) Then build.AppendLine("FILE DISBDEBTORMGT  MFCIF No:" & mfcif & "MgtKelurahan kosong. </br>")
        'If (String.IsNullOrEmpty(MgtKecamatan)) Then build.AppendLine("FILE DISBDEBTORMGT  MFCIF No:" & mfcif & "MgtKecamatan kosong. </br>")
        If (SharePortion <= 0) Then build.AppendLine("FILE DISBDEBTORMGT  MFCIF No:" & MfCif & "SharePortion kosong. </br>")
        If (String.IsNullOrEmpty(JobTitleID)) Then build.AppendLine("FILE DISBDEBTORMGT  MFCIF No:" & MfCif & " JobTitleID kosong. </br>")
    End Sub

End Class
