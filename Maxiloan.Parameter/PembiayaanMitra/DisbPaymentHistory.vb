﻿Imports System.Data.SqlClient

Public Class DisbPaymentHistory
    Inherits BaseDisb

    Public Sub New()
        MyBase.New("", "", 0, "", "")
    End Sub
    Sub New(oriSplit As String())
        MyBase.New(oriSplit(0), oriSplit(1), 0, oriSplit(2), "")
        SeqNo = oriSplit(3)
        DueDate = oriSplit(4)
        PaidDate = oriSplit(5)
        PaidAmount = oriSplit(6)
        PrincipalAmount = oriSplit(7)
        InterestAmount = oriSplit(8)
    End Sub
    Public ReadOnly Property SeqNo As Decimal
    Public ReadOnly Property DueDate As DateTime
    Public ReadOnly Property PaidDate As DateTime
    Public ReadOnly Property PaidAmount As Decimal
    Public ReadOnly Property PrincipalAmount As Decimal
    Public ReadOnly Property InterestAmount As Decimal

    Public Overrides Function addToList(oriSplit() As String) As AbsDisb
        Try
            Return New DisbAgreement(oriSplit)
        Catch ex As Exception
            Throw New Exception($"Error parse file DisbPaymentHistory.{vbCrLf}{ex.Message}")
        End Try
    End Function
    Public Shared Function Instance() As DisbPaymentHistory
        Try
            Return New DisbPaymentHistory()
        Catch ex As Exception
            Throw New Exception($"Error parse file DisbPaymentHistory. {ex.Message}")
        End Try
    End Function

    Protected Overrides Sub doValidate()
        If (String.IsNullOrEmpty(MfAgreementNo)) Then build.AppendLine("FILE DISBPAYMENTHISTORY MFAgreementNo kosong. </br>")

        If (String.IsNullOrEmpty(MfId)) Then build.AppendLine("FILE DISBPAYMENTHISTORY MFAgreementNo:" & MfAgreementNo & " MFID kosong. </br>")
        If (String.IsNullOrEmpty(MfBatchNo)) Then build.AppendLine("FILE DISBPAYMENTHISTORY MFAgreementNo:" & MfAgreementNo & " MFBatchNo kosong. </br>")
    End Sub
    Const _spName As String = "spDisb_DisbPaymentHistoryt_add"
    Public Overrides Function command(cnn As SqlConnection) As SqlCommand
        Dim sqlcommand = New SqlCommand(_spName, cnn)

        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.Parameters.Clear()
        sqlcommand.Parameters.Add(New SqlParameter("@mFID", SqlDbType.Char, 10) With {.Value = MfId})
        sqlcommand.Parameters.Add(New SqlParameter("@mFBatchNo", SqlDbType.Char, 20) With {.Value = MfBatchNo})
        sqlcommand.Parameters.Add(New SqlParameter("@mFAgreementNo", SqlDbType.Char, 20) With {.Value = MfAgreementNo})
        sqlcommand.Parameters.Add(New SqlParameter("@SeqNo", SqlDbType.SmallInt) With {.Value = SeqNo})
        sqlcommand.Parameters.Add(New SqlParameter("@DueDate", SqlDbType.Date) With {.Value = DueDate})
        sqlcommand.Parameters.Add(New SqlParameter("@PaidDate", SqlDbType.Date) With {.Value = PaidDate})
        sqlcommand.Parameters.Add(New SqlParameter("@PaidAmount", SqlDbType.Decimal) With {.Value = PaidAmount})
        sqlcommand.Parameters.Add(New SqlParameter("@PrincipalAmount", SqlDbType.Decimal) With {.Value = PrincipalAmount})
        sqlcommand.Parameters.Add(New SqlParameter("@InterestAmount", SqlDbType.Decimal) With {.Value = InterestAmount})
        Return sqlcommand
    End Function
End Class
