
<Serializable()> _
Public Class FundingContractBatch : Inherits FundingContract

    'new parameter
    Property CaraBayar As String
    Private _Filter As String
    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
    'FundingBatch
    Private _FundingBatchNo As String
    Private _FundingBatchNoUsed As String
    Private _BatchDate As Date
    Private _PrincipalAmtToFunCoy As Decimal
    Private _InterestRate As Decimal
    Private _AdminAmount As Decimal
    Private _ExchangeRate As Decimal
    Private _Tenor As Integer
    Private _InstallmentPeriod As String
    Private _InstallmentScheme As String
    Private _ProposeDate As Date
    Private _AragingDate As Date
    Private _RealizedDate As Date
    Private _InstallmentDueDate As Date
    Private _AccProposedNum As Integer
    Private _AccRealizedNum As Integer
    Private _OSAmtToFunCoy As Decimal
    Private _FirstInstallment As String




    'FundingBatchInstallment
    Private _InsSecNo As Integer
    Private _PrincipalAmount As Decimal
    Private _InterestAmount As Decimal
    Private _PrincipalPaidAmount As Decimal
    Private _InterestPaidAmount As Decimal
    Private _OSPrincipalAmount As Decimal
    Private _OSInterestAmount As Decimal
    Private _PenaltyPaidAmount As Decimal
    Private _DueDate As Date


    'Others
    Private _referenceno As String
    Private _referenceno2 As String
    Private _referenceno3 As String
    Private _ListDrowDownBPKB As DataTable
    Private _SpName As String
    Private _OtherFeeAmount As Decimal
    'FundingPaymentOut
    Private _ValueDate As Date
    Private _BankAccountID As String
    Private _BankAccountID2 As String
    Private _BankAccountID3 As String
    Private _ApplicationID As String
    Private _CompanyID As String

    Private _AccruedInterest As Decimal
    Private _PrepaymentAmount As Decimal
    Private _BranchIDApplication As String
    Private _InterestForThisMonth As Decimal
    Private _IsBatchPrepayment As Boolean
    Private _AmtDrawDown1 As Decimal
    Private _AmtDrawDown2 As Decimal
    Private _AmtDrawDown3 As Decimal
    Private _PPHPaid As Decimal
    Private _InstallmentAmont As Decimal

    Private _GoLiveDate As Date
    Private _NetDP As Decimal
    Private _payment_asof As Date
    Private _isFundingNTF As Integer
    Private _perhitunganBunga As String
    Private _errlabel As String
    Private _scheme As String

    Private _listData2 As DataTable
    Private _IsPrincipalPaidAmount As Boolean
    Private _IsInterestPaidAmount As Boolean
    Private _Notes As String

#Region "AsString"

    Public Property Filter() As String
        Get
            Return _Filter
        End Get
        Set(ByVal Value As String)
            _Filter = Value
        End Set
    End Property

    Public Property perhitunganBunga() As String
        Get
            Return _perhitunganBunga
        End Get
        Set(ByVal Value As String)
            _perhitunganBunga = Value
        End Set
    End Property

    Public Property isFundingNTF() As Integer
        Get
            Return _isFundingNTF
        End Get
        Set(ByVal Value As Integer)
            _isFundingNTF = Value
        End Set
    End Property

    Public Property FirstInstallment() As String
        Get
            Return _FirstInstallment
        End Get
        Set(ByVal Value As String)
            _FirstInstallment = Value
        End Set
    End Property

    Public Property FundingBatchNo() As String
        Get
            Return _FundingBatchNo
        End Get
        Set(ByVal Value As String)
            _FundingBatchNo = Value
        End Set
    End Property
    Public Property FundingBatchNoUsed() As String
        Get
            Return _FundingBatchNoUsed
        End Get
        Set(ByVal Value As String)
            _FundingBatchNoUsed = Value
        End Set
    End Property
    Public Property BranchIDApplication() As String
        Get
            Return _BranchIDApplication
        End Get
        Set(ByVal Value As String)
            _BranchIDApplication = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property
    Public Property CompanyID() As String
        Get
            Return _CompanyID
        End Get
        Set(ByVal Value As String)
            _CompanyID = Value
        End Set
    End Property
    Public Property InstallmentPeriod() As String
        Get
            Return _InstallmentPeriod
        End Get
        Set(ByVal Value As String)
            _InstallmentPeriod = Value
        End Set
    End Property

    Public Property InstallmentScheme() As String
        Get
            Return _InstallmentScheme
        End Get
        Set(ByVal Value As String)
            _InstallmentScheme = Value
        End Set
    End Property

    Public Property ReferenceNo() As String
        Get
            Return _referenceno
        End Get
        Set(ByVal Value As String)
            _referenceno = Value
        End Set
    End Property
    Public Property ReferenceNo2() As String
        Get
            Return _referenceno2
        End Get
        Set(ByVal Value As String)
            _referenceno2 = Value
        End Set
    End Property
    Public Property ReferenceNo3() As String
        Get
            Return _referenceno3
        End Get
        Set(ByVal Value As String)
            _referenceno3 = Value
        End Set
    End Property
    Public Property SpName() As String
        Get
            Return _SpName
        End Get
        Set(ByVal Value As String)
            _SpName = Value
        End Set
    End Property
    Public Property BankAccountID() As String
        Get
            Return _BankAccountID
        End Get
        Set(ByVal Value As String)
            _BankAccountID = Value
        End Set
    End Property
    Public Property BankAccountID2() As String
        Get
            Return _BankAccountID2
        End Get
        Set(ByVal Value As String)
            _BankAccountID2 = Value
        End Set
    End Property
    Public Property BankAccountID3() As String
        Get
            Return _BankAccountID3
        End Get
        Set(ByVal Value As String)
            _BankAccountID3 = Value
        End Set
    End Property
    Public Property ErrLabel() As String
        Get
            Return _errlabel
        End Get
        Set(ByVal Value As String)
            _errlabel = Value
        End Set
    End Property
    Public Property Scheme() As String
        Get
            Return _scheme
        End Get
        Set(ByVal Value As String)
            _scheme = Value
        End Set
    End Property

#End Region

#Region "AsInteger"
    Public Property AccProposedNum() As Integer
        Get
            Return _AccProposedNum
        End Get
        Set(ByVal Value As Integer)
            _AccProposedNum = Value
        End Set
    End Property

    Public Property AccRealizedNum() As Integer
        Get
            Return _AccRealizedNum
        End Get
        Set(ByVal Value As Integer)
            _AccRealizedNum = Value
        End Set
    End Property

    Public Property InsSecNo() As Integer
        Get
            Return _InsSecNo
        End Get
        Set(ByVal Value As Integer)
            _InsSecNo = Value
        End Set
    End Property

    Public Property Tenor() As Integer
        Get
            Return _Tenor
        End Get
        Set(ByVal Value As Integer)
            _Tenor = Value
        End Set
    End Property

    Public Property GoLiveDate() As Date
        Get
            Return _GoLiveDate
        End Get
        Set(ByVal Value As Date)
            _GoLiveDate = Value
        End Set
    End Property

    Public Property NetDP() As Decimal
        Get
            Return _NetDP
        End Get
        Set(ByVal Value As Decimal)
            _NetDP = Value
        End Set
    End Property

#End Region

#Region "AsDate"
    Public Property BatchDate() As Date
        Get
            Return _BatchDate
        End Get
        Set(ByVal Value As Date)
            _BatchDate = Value
        End Set
    End Property

    Public Property ProposeDate() As Date
        Get
            Return _ProposeDate
        End Get
        Set(ByVal Value As Date)
            _ProposeDate = Value
        End Set
    End Property

    Public Property AragingDate() As Date
        Get
            Return _AragingDate
        End Get
        Set(ByVal Value As Date)
            _AragingDate = Value
        End Set
    End Property

    Public Property RealizedDate() As Date
        Get
            Return _RealizedDate
        End Get
        Set(ByVal Value As Date)
            _RealizedDate = Value
        End Set
    End Property

    Public Property InstallmentDueDate() As Date
        Get
            Return _InstallmentDueDate
        End Get
        Set(ByVal Value As Date)
            _InstallmentDueDate = Value
        End Set
    End Property

    Public Property DueDate() As Date
        Get
            Return _DueDate
        End Get
        Set(ByVal Value As Date)
            _DueDate = Value
        End Set
    End Property
    Public Property ValueDate() As Date
        Get
            Return _ValueDate
        End Get
        Set(ByVal Value As Date)
            _ValueDate = Value
        End Set
    End Property
#End Region

#Region "AsDecimal"
    Public Property OSAmtToFunCoy() As Decimal
        Get
            Return _OSAmtToFunCoy
        End Get
        Set(ByVal Value As Decimal)
            _OSAmtToFunCoy = Value
        End Set
    End Property
    Public Property PenaltyPaidAmount() As Decimal
        Get
            Return _PenaltyPaidAmount
        End Get
        Set(ByVal Value As Decimal)
            _PenaltyPaidAmount = Value
        End Set
    End Property
    Public Property PrincipalAmtToFunCoy() As Decimal
        Get
            Return _PrincipalAmtToFunCoy
        End Get
        Set(ByVal Value As Decimal)
            _PrincipalAmtToFunCoy = Value
        End Set
    End Property
    Public Property AccruedInterest() As Decimal
        Get
            Return _AccruedInterest
        End Get
        Set(ByVal Value As Decimal)
            _AccruedInterest = Value
        End Set
    End Property
    Public Property PrepaymentAmount() As Decimal
        Get
            Return _PrepaymentAmount
        End Get
        Set(ByVal Value As Decimal)
            _PrepaymentAmount = Value
        End Set
    End Property


    Public Property InterestRate() As Decimal
        Get
            Return _InterestRate
        End Get
        Set(ByVal Value As Decimal)
            _InterestRate = Value
        End Set
    End Property

    Public Property AdminAmount() As Decimal
        Get
            Return _AdminAmount
        End Get
        Set(ByVal Value As Decimal)
            _AdminAmount = Value
        End Set
    End Property

    Public Property ExchangeRate() As Decimal
        Get
            Return _ExchangeRate
        End Get
        Set(ByVal Value As Decimal)
            _ExchangeRate = Value
        End Set
    End Property

    Public Property PrincipalAmount() As Decimal
        Get
            Return _PrincipalAmount
        End Get
        Set(ByVal Value As Decimal)
            _PrincipalAmount = Value
        End Set
    End Property

    Public Property InterestAmount() As Decimal
        Get
            Return _InterestAmount
        End Get
        Set(ByVal Value As Decimal)
            _InterestAmount = Value
        End Set
    End Property

    Public Property PrincipalPaidAmount() As Decimal
        Get
            Return _PrincipalPaidAmount
        End Get
        Set(ByVal Value As Decimal)
            _PrincipalPaidAmount = Value
        End Set
    End Property

    Public Property InterestPaidAmount() As Decimal
        Get
            Return _InterestPaidAmount
        End Get
        Set(ByVal Value As Decimal)
            _InterestPaidAmount = Value
        End Set
    End Property

    Public Property OSPrincipalAmount() As Decimal
        Get
            Return _OSPrincipalAmount
        End Get
        Set(ByVal Value As Decimal)
            _OSPrincipalAmount = Value
        End Set
    End Property

    Public Property OSInterestAmount() As Decimal
        Get
            Return _OSInterestAmount
        End Get
        Set(ByVal Value As Decimal)
            _OSInterestAmount = Value
        End Set
    End Property

    Public Property OtherFeeAmount() As Decimal
        Get
            Return _OtherFeeAmount
        End Get
        Set(ByVal Value As Decimal)
            _OtherFeeAmount = Value
        End Set
    End Property
    Public Property InterestForThisMonth() As Decimal
        Get
            Return _InterestForThisMonth
        End Get
        Set(ByVal Value As Decimal)
            _InterestForThisMonth = Value
        End Set
    End Property
    Public Property AmtDrawDown1() As Decimal
        Get
            Return _AmtDrawDown1
        End Get
        Set(ByVal Value As Decimal)
            _AmtDrawDown1 = Value
        End Set
    End Property
    Public Property AmtDrawDown2() As Decimal
        Get
            Return _AmtDrawDown2
        End Get
        Set(ByVal Value As Decimal)
            _AmtDrawDown2 = Value
        End Set
    End Property
    Public Property AmtDrawDown3() As Decimal
        Get
            Return _AmtDrawDown3
        End Get
        Set(ByVal Value As Decimal)
            _AmtDrawDown3 = Value
        End Set
    End Property
    Public Property PPHPaid() As Decimal
        Get
            Return _PPHPaid
        End Get
        Set(ByVal Value As Decimal)
            _PPHPaid = Value
        End Set
    End Property
    Public Property InstallmentAmount() As Decimal
        Get
            Return _InstallmentAmont
        End Get
        Set(value As Decimal)
            _InstallmentAmont = value
        End Set
    End Property
#End Region

#Region "As DataTable"
    Public Property ListDrowDownBPKB() As DataTable
        Get
            Return ListDrowDownBPKB
        End Get
        Set(ByVal Value As DataTable)
            _ListDrowDownBPKB = Value
        End Set
    End Property
    Public Property ListData2() As DataTable
        Get
            Return _listData2
        End Get
        Set(value As DataTable)
            _listData2 = value
        End Set
    End Property
#End Region
    Public Property IsBatchPrepayment() As Boolean
        Get
            Return _IsBatchPrepayment
        End Get
        Set(ByVal Value As Boolean)
            _IsBatchPrepayment = Value
        End Set
    End Property

    Public Property PaymentOutAsOfDate() As Date
        Get
            Return _payment_asof
        End Get
        Set(ByVal Value As Date)
            _payment_asof = Value
        End Set
    End Property
    Public Property ListReport As DataSet
    Public Property hasil As Integer
    Public Property ListConfLetter As DataTable
    Public Property AgreementNoTbl As DataTable
    Public Property TotalPrepaymentAmount As Decimal
    Public Property EffectiveDate As Date
    Public Property Catatan As String
    Public Property SP As String
    Public Property IsPrincipalPaidAmount() As Boolean
        Get
            Return _IsPrincipalPaidAmount
        End Get
        Set(ByVal Value As Boolean)
            _IsPrincipalPaidAmount = Value
        End Set
    End Property
    Public Property IsInterestPaidAmount() As Boolean
        Get
            Return _IsInterestPaidAmount
        End Get
        Set(ByVal Value As Boolean)
            _IsInterestPaidAmount = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property

    Public Property IsExcel As Boolean

End Class


