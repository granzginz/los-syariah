﻿<Serializable()> _
Public Class FundingContractCriteria : Inherits Maxiloan.Parameter.Common
    Public Property FundingCoyID As String
    Public Property FundingContractNo As String
    Public Property CriteriaID As String    
    Public Property BankID As String
    Public Property Query As String
    Public Property CriteriaValue1 As String
    Public Property CriteriaValue2 As String
    Public Property ChekPilih As Boolean
    Public Property CriteriaValueID As String
    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
End Class
