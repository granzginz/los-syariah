
<Serializable()> _
Public Class FundingContractBranch : Inherits Common
    Private _BankId As String
    Private _FundingCoyID As String
    Private _FundingContractNo As String
    Private _PlafondAmount As Decimal
    Private _BookAmount As Decimal
    Private _OSAmount As Decimal
    Private _AcqStatus As String
    Private _listData As DataTable
    Private _TotRec As Integer

    Public Property PlafondAmount() As Decimal
        Get
            Return _PlafondAmount
        End Get
        Set(ByVal Value As Decimal)
            _PlafondAmount = Value
        End Set
    End Property

    Public Property BookAmount() As Decimal
        Get
            Return _BookAmount
        End Get
        Set(ByVal Value As Decimal)
            _BookAmount = Value
        End Set
    End Property

    Public Property OSAmount() As Decimal
        Get
            Return _OSAmount
        End Get
        Set(ByVal Value As Decimal)
            _OSAmount = Value
        End Set
    End Property

    Public Property AcqStatus() As String
        Get
            Return _AcqStatus
        End Get
        Set(ByVal Value As String)
            _AcqStatus = Value
        End Set
    End Property
    Public Property FundingContractNo() As String
        Get
            Return _FundingContractNo
        End Get
        Set(ByVal Value As String)
            _FundingContractNo = Value
        End Set
    End Property

    Public Property BankId() As String
        Get
            Return _BankId
        End Get
        Set(ByVal Value As String)
            _BankId = Value
        End Set
    End Property

    Public Property FundingCoyID() As String
        Get
            Return _FundingCoyID
        End Get
        Set(ByVal Value As String)
            _FundingCoyID = Value
        End Set
    End Property


    Public Property ListData() As DataTable
        Get
            Return _listData
        End Get
        Set(ByVal Value As DataTable)
            _listData = Value
        End Set
    End Property

    Public Property TotRec() As Integer
        Get
            Return _TotRec
        End Get
        Set(ByVal Value As Integer)
            _TotRec = Value
        End Set
    End Property

End Class

