
<Serializable()> _
Public Class FundingContract : Inherits Common

    Private _BankId As String
    'Private _FundingCoyId As String
    'Private _FundingContractNo As String
    Public Property _FundingCoyId As String
    Public Property _FundingContractNo As String
    Private _ContractName As String
    Private _FlagCS As String
    Private _CurrencyID As String
    Private _PlafondAmount As Decimal
    Private _ContractDate As Date
    Private _PeriodFrom As Date
    Private _PeriodTo As Date
    Private _FacilityType As String
    Private _FinalMaturityDate As Date
    Private _EvaluationDate As Date
    Private _RateToFundingCoy As Decimal
    Private _InterestType As String
    Private _InterestNotes As String
    Private _LCPerDay As Decimal
    Private _LCGracePeriod As Integer
    Private _FlagCoBranding As Boolean
    Private _FundingCoyPortion As Double
    Private _PrepaymentPenalty As Decimal
    Private _PaymentScheme As String
    Private _RecourseType As String
    Private _AcqActiveStatus As Boolean
    Private _NegCovDate As Date
    Private _CashHoldBack As Decimal
    Private _ContractStatus As String
    Private _FloatingStart As Date
    Private _FacilityKind As String
    Private _SecurityCoveragePercentage As Decimal
    Private _SecurityCoverageType As String
    Private _SecurityType As String
    Private _BalanceSheetStatus As String
    Private _ProvisionFeeAmount As Decimal
    Private _AdminFeePerAccount As Decimal
    Private _AdminFeeFacility As Decimal
    Private _AdminFeePerDrawDown As Decimal
    Private _CommitmentFee As Decimal
    Private _MaximumDateForDD As Decimal
    Private _PrepaymentType As String
    Private _AssetDocLocation As String
    Private _CommitmentStatus As Boolean
    Private _NegativeCov1N1 As Integer
    Private _NegativeCov1N2 As Integer
    Private _NegativeCov3N1 As Integer

    Private _FundingDocId As Integer
    Private _DocumentNote As String

    Private _listData As DataTable
    Private _TotRec As Integer
    Private _mirroring As Boolean
    Private _persenPokokHutang As Decimal
    Private _notarisName As String

    Private _COAFunding As String
    Private _COAIntExp As String
    Private _COAPrepaid As String

    Private _Pinalty As Double
    Private _PeriodeBulan As Integer
    Public Property _AgreementNo As String
    Public Property _InsSeqno As String
    Public Property _FundingBatchNo As String
    Public Property PeriodeBulan As Integer
        Get
            Return _PeriodeBulan
        End Get
        Set(value As Integer)
            _PeriodeBulan = value
        End Set
    End Property


    Public Property Pinalty() As Double
        Get
            Return _Pinalty
        End Get
        Set(value As Double)
            _Pinalty = value
        End Set
    End Property

    Public Property COAFunding() As String
        Get
            Return _COAFunding
        End Get
        Set(ByVal Value As String)
            _COAFunding = Value
        End Set
    End Property
    Public Property COAIntExp() As String
        Get
            Return _COAIntExp
        End Get
        Set(ByVal Value As String)
            _COAIntExp = Value
        End Set
    End Property
    Public Property COAPrepaid() As String
        Get
            Return _COAPrepaid
        End Get
        Set(ByVal Value As String)
            _COAPrepaid = Value
        End Set
    End Property

    Public Property NotarisName() As String
        Get
            Return _notarisName
        End Get
        Set(ByVal Value As String)
            _notarisName = Value
        End Set
    End Property

    Public Property persenPokokHutang() As Decimal
        Get
            Return _persenPokokHutang
        End Get
        Set(ByVal Value As Decimal)
            _persenPokokHutang = Value
        End Set
    End Property

    Public Property Mirroring() As Boolean
        Get
            Return _mirroring
        End Get
        Set(ByVal Value As Boolean)
            _mirroring = Value
        End Set
    End Property
    Public Property TotRec() As Integer
        Get
            Return _TotRec
        End Get
        Set(ByVal Value As Integer)
            _TotRec = Value
        End Set
    End Property

#Region "ContractDoc"
    Public Property FundingDocId() As Integer
        Get
            Return _FundingDocId
        End Get
        Set(ByVal Value As Integer)
            _FundingDocId = Value
        End Set
    End Property

    Public Property DocumentNote() As String
        Get
            Return _DocumentNote
        End Get
        Set(ByVal Value As String)
            _DocumentNote = Value
        End Set
    End Property
#End Region

#Region "AsDataTable"
    Public Property ListData() As DataTable
        Get
            Return _listData
        End Get
        Set(ByVal Value As DataTable)
            _listData = Value
        End Set
    End Property

#End Region

#Region "AsString"

    Public Property BankId() As String
        Get
            Return _BankId
        End Get
        Set(ByVal Value As String)
            _BankId = Value
        End Set
    End Property

    Public Property FundingCoyId() As String
        Get
            Return _FundingCoyId
        End Get
        Set(ByVal Value As String)
            _FundingCoyId = Value
        End Set
    End Property


    Public Property FundingContractNo() As String
        Get
            Return _FundingContractNo
        End Get
        Set(ByVal Value As String)
            _FundingContractNo = Value
        End Set
    End Property


    Public Property ContractName() As String
        Get
            Return _ContractName
        End Get
        Set(ByVal Value As String)
            _ContractName = Value
        End Set
    End Property


    Public Property FlagCS() As String
        Get
            Return _FlagCS
        End Get
        Set(ByVal Value As String)
            _FlagCS = Value
        End Set
    End Property

    Public Property CurrencyID() As String
        Get
            Return _CurrencyID
        End Get
        Set(ByVal Value As String)
            _CurrencyID = Value
        End Set
    End Property

    Public Property FacilityType() As String
        Get
            Return _FacilityType
        End Get
        Set(ByVal Value As String)
            _FacilityType = Value
        End Set
    End Property

    Public Property InterestType() As String
        Get
            Return _InterestType
        End Get
        Set(ByVal Value As String)
            _InterestType = Value
        End Set
    End Property

    Public Property InterestNotes() As String
        Get
            Return _InterestNotes
        End Get
        Set(ByVal Value As String)
            _InterestNotes = Value
        End Set
    End Property

    Public Property PaymentScheme() As String
        Get
            Return _PaymentScheme
        End Get
        Set(ByVal Value As String)
            _PaymentScheme = Value
        End Set
    End Property

    Public Property RecourseType() As String
        Get
            Return _RecourseType
        End Get
        Set(ByVal Value As String)
            _RecourseType = Value
        End Set
    End Property

    Public Property ContractStatus() As String
        Get
            Return _ContractStatus
        End Get
        Set(ByVal Value As String)
            _ContractStatus = Value
        End Set
    End Property

    Public Property FacilityKind() As String
        Get
            Return _FacilityKind
        End Get
        Set(ByVal Value As String)
            _FacilityKind = Value
        End Set
    End Property

    Public Property BalanceSheetStatus() As String
        Get
            Return _BalanceSheetStatus
        End Get
        Set(ByVal Value As String)
            _BalanceSheetStatus = Value
        End Set
    End Property

    Public Property SecurityCoverageType() As String
        Get
            Return _SecurityCoverageType
        End Get
        Set(ByVal Value As String)
            _SecurityCoverageType = Value
        End Set
    End Property

    Public Property SecurityType() As String
        Get
            Return _SecurityType
        End Get
        Set(ByVal Value As String)
            _SecurityType = Value
        End Set
    End Property

    Public Property PrepaymentType() As String
        Get
            Return _PrepaymentType
        End Get
        Set(ByVal Value As String)
            _PrepaymentType = Value
        End Set
    End Property

    Public Property AssetDocLocation() As String
        Get
            Return _AssetDocLocation
        End Get
        Set(ByVal Value As String)
            _AssetDocLocation = Value
        End Set
    End Property
#End Region

#Region "AsDate"

    Public Property ContractDate() As Date
        Get
            Return _ContractDate
        End Get
        Set(ByVal Value As Date)
            _ContractDate = Value
        End Set
    End Property

    Public Property PeriodFrom() As Date
        Get
            Return _PeriodFrom
        End Get
        Set(ByVal Value As Date)
            _PeriodFrom = Value
        End Set
    End Property

    Public Property PeriodTo() As Date
        Get
            Return _PeriodTo
        End Get
        Set(ByVal Value As Date)
            _PeriodTo = Value
        End Set
    End Property

    Public Property FinalMaturityDate() As Date
        Get
            Return _FinalMaturityDate
        End Get
        Set(ByVal Value As Date)
            _FinalMaturityDate = Value
        End Set
    End Property

    Public Property EvaluationDate() As Date
        Get
            Return _EvaluationDate
        End Get
        Set(ByVal Value As Date)
            _EvaluationDate = Value
        End Set
    End Property

    Public Property NegCovDate() As Date
        Get
            Return _NegCovDate
        End Get
        Set(ByVal Value As Date)
            _NegCovDate = Value
        End Set
    End Property

    Public Property FloatingStart() As Date
        Get
            Return _FloatingStart
        End Get
        Set(ByVal Value As Date)
            _FloatingStart = Value
        End Set
    End Property

#End Region

#Region "AsDecimal"

    Public Property RateToFundingCoy() As Decimal
        Get
            Return _RateToFundingCoy
        End Get
        Set(ByVal Value As Decimal)
            _RateToFundingCoy = Value
        End Set
    End Property

    Public Property SecurityCoveragePercentage() As Decimal
        Get
            Return _SecurityCoveragePercentage
        End Get
        Set(ByVal Value As Decimal)
            _SecurityCoveragePercentage = Value
        End Set
    End Property

    Public Property ProvisionFeeAmount() As Decimal
        Get
            Return _ProvisionFeeAmount
        End Get
        Set(ByVal Value As Decimal)
            _ProvisionFeeAmount = Value
        End Set
    End Property

    Public Property AdminFeePerAccount() As Decimal
        Get
            Return _AdminFeePerAccount
        End Get
        Set(ByVal Value As Decimal)
            _AdminFeePerAccount = Value
        End Set
    End Property

    Public Property AdminFeeFacility() As Decimal
        Get
            Return _AdminFeeFacility
        End Get
        Set(ByVal Value As Decimal)
            _AdminFeeFacility = Value
        End Set
    End Property

    Public Property AdminFeePerDrawDown() As Decimal
        Get
            Return _AdminFeePerDrawDown
        End Get
        Set(ByVal Value As Decimal)
            _AdminFeePerDrawDown = Value
        End Set
    End Property

    Public Property CommitmentFee() As Decimal
        Get
            Return _CommitmentFee
        End Get
        Set(ByVal Value As Decimal)
            _CommitmentFee = Value
        End Set
    End Property

    Public Property PrepaymentPenalty() As Decimal
        Get
            Return _PrepaymentPenalty
        End Get
        Set(ByVal Value As Decimal)
            _PrepaymentPenalty = Value
        End Set
    End Property

    Public Property CashHoldBack() As Decimal
        Get
            Return _CashHoldBack
        End Get
        Set(ByVal Value As Decimal)
            _CashHoldBack = Value
        End Set
    End Property

    Public Property PlafondAmount() As Decimal
        Get
            Return _PlafondAmount
        End Get
        Set(ByVal Value As Decimal)
            _PlafondAmount = Value
        End Set
    End Property

    Public Property LCPerDay() As Decimal
        Get
            Return _LCPerDay
        End Get
        Set(ByVal Value As Decimal)
            _LCPerDay = Value
        End Set
    End Property



#End Region

#Region "AsInteger"

    Public Property MaximumDateForDD() As Decimal
        Get
            Return _MaximumDateForDD
        End Get
        Set(ByVal Value As Decimal)
            _MaximumDateForDD = Value
        End Set
    End Property

    Public Property LCGracePeriod() As Integer
        Get
            Return _LCGracePeriod
        End Get
        Set(ByVal Value As Integer)
            _LCGracePeriod = Value
        End Set
    End Property

    Public Property FundingCoyPortion() As Double
        Get
            Return _FundingCoyPortion
        End Get
        Set(ByVal Value As Double)
            _FundingCoyPortion = Value
        End Set
    End Property
    Public Property NegativeCov1N1() As Integer
        Get
            Return _NegativeCov1N1
        End Get
        Set(ByVal Value As Integer)
            _NegativeCov1N1 = Value
        End Set
    End Property
    Public Property NegativeCov1N2() As Integer
        Get
            Return _NegativeCov1N2
        End Get
        Set(ByVal Value As Integer)
            _NegativeCov1N2 = Value
        End Set
    End Property
    Public Property NegativeCov3N1() As Integer
        Get
            Return _NegativeCov3N1
        End Get
        Set(ByVal Value As Integer)
            _NegativeCov3N1 = Value
        End Set
    End Property


#End Region

#Region "AsBoolean"

    Public Property AcqActiveStatus() As Boolean
        Get
            Return _AcqActiveStatus
        End Get
        Set(ByVal Value As Boolean)
            _AcqActiveStatus = Value
        End Set
    End Property

    Public Property FlagCoBranding() As Boolean
        Get
            Return _FlagCoBranding
        End Get
        Set(ByVal Value As Boolean)
            _FlagCoBranding = Value
        End Set
    End Property

    Public Property CommitmentStatus() As Boolean
        Get
            Return _CommitmentStatus
        End Get
        Set(ByVal Value As Boolean)
            _CommitmentStatus = Value
        End Set
    End Property

#End Region


    Public Property TglJatuhTempoSpesifik As Date
    Public Property JumlahHaridlmBulan As String
    Public Property MinimumPencairan As Decimal
    Public Property CaraBayar As String
    Public Property BatasAngsuran As Integer
    Public Property Req As String
    Public Property CoaBeban As String
    Public Property CoaAdmin As String

    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property
    Public Property InsSeqno() As String
        Get
            Return _InsSeqno
        End Get
        Set(ByVal Value As String)
            _InsSeqno = Value
        End Set
    End Property
    Public Property FundingBatchNo() As String
        Get
            Return _FundingBatchNo
        End Get
        Set(ByVal Value As String)
            _FundingBatchNo = Value
        End Set
    End Property
End Class

