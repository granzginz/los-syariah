﻿<Serializable()> _
Public Class FundingCriteria : Inherits Maxiloan.Parameter.Common
    Public Property CriteriaID As String
    Public Property CriteriaIDEdit As String
    Public Property CriteriaDescription As String
    Public Property CriteriaOption As String
    Public Property Query As String
    Public Property Notes As String
    Public Property Req As String
    Public Property FundingCoyId As String
    Public Property Label As String
    Public Property FundingContractNo As String
    Public Property FundingBatchNo As String
    Public Property BankID As String
    Public Property SQLString As String

    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
End Class
