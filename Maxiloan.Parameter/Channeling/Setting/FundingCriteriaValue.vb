﻿<Serializable()> _
Public Class FundingCriteriaValue : Inherits Maxiloan.Parameter.Common
    Public Property CriteriaID As String
    Public Property CriteriaValueID As String
    Public Property CriteriaValueIDEdit As String
    Public Property CriteriaDescription As String
    Public Property CriteriaValue As String
    Public Property SQLData As String

    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
End Class
