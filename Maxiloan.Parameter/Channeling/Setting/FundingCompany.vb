
<Serializable()> _
Public Class FundingCompany : Inherits Common
    Private _BankId As String
    Private _FundingCoyID As String
    Private _FundingContractNo As String
    Private _FundingBatchNo As String
    Private _FundingCoyName As String
    Private _FundingCoBankBranchAccount As String
    Private _FundingCoBankAccountName As String
    Private _InterestCalculationOption As Integer
    Private _listData As DataTable
    Private _listFundingCompany As DataSet
    Private _TotRec As Integer
    Private _COAFunding As String
    Private _COAIntExp As String
    Private _COAPrepaid As String

    Public Property COAFunding() As String
        Get
            Return _COAFunding
        End Get
        Set(ByVal Value As String)
            _COAFunding = Value
        End Set
    End Property
    Public Property COAIntExp() As String
        Get
            Return _COAIntExp
        End Get
        Set(ByVal Value As String)
            _COAIntExp = Value
        End Set
    End Property
    Public Property COAPrepaid() As String
        Get
            Return _COAPrepaid
        End Get
        Set(ByVal Value As String)
            _COAPrepaid = Value
        End Set
    End Property

    Public Property InterestCalculationOption() As Integer
        Get
            Return _InterestCalculationOption
        End Get
        Set(ByVal Value As Integer)
            _InterestCalculationOption = Value
        End Set
    End Property

    Public Property FundingCoBankAccountName() As String
        Get
            Return _FundingCoBankAccountName
        End Get
        Set(ByVal Value As String)
            _FundingCoBankAccountName = Value
        End Set
    End Property

    Public Property FundingCoBankBranchAccount() As String
        Get
            Return _FundingCoBankBranchAccount
        End Get
        Set(ByVal Value As String)
            _FundingCoBankBranchAccount = Value
        End Set
    End Property

    Public Property BankId() As String
        Get
            Return _BankId
        End Get
        Set(ByVal Value As String)
            _BankId = Value
        End Set
    End Property

    Public Property FundingCoyID() As String
        Get
            Return _FundingCoyID
        End Get
        Set(ByVal Value As String)
            _FundingCoyID = Value
        End Set
    End Property
    Public Property ListFundingCompany() As DataSet
        Get
            Return _ListFundingCompany
        End Get
        Set(ByVal Value As DataSet)
            _ListFundingCompany = Value
        End Set
    End Property
    Public Property FundingBatchNo() As String
        Get
            Return _FundingBatchNo
        End Get
        Set(ByVal Value As String)
            _FundingBatchNo = Value
        End Set
    End Property
    Public Property FundingContractNo() As String
        Get
            Return _FundingContractNo
        End Get
        Set(ByVal Value As String)
            _FundingContractNo = Value
        End Set
    End Property

    Public Property FundingCoyName() As String
        Get
            Return _FundingCoyName
        End Get
        Set(ByVal Value As String)
            _FundingCoyName = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listData
        End Get
        Set(ByVal Value As DataTable)
            _listData = Value
        End Set
    End Property
    Public Property TotRec() As Integer
        Get
            Return _TotRec
        End Get
        Set(ByVal Value As Integer)
            _TotRec = Value
        End Set
    End Property
    Public Property Up As String
    Public Property PrintedBy As String
    Public Property debetDate As DateTime
    Public Property Status As String
    Public Property NoReff As String

End Class

