﻿<Serializable()> _
Public Class FundingContractRate : Inherits Maxiloan.Parameter.Common    
    Public Property BankID As String
    Public Property FundingCoyID As String
    Public Property FundingContractNo As String
    Public Property FUndingBatchNo As String
    Public Property TenorFrom As Integer
    Public Property TenorTo As Integer
    Public Property FundingRate As Decimal

    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
End Class
