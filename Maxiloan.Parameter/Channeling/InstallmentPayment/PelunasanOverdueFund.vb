﻿<Serializable()>
Public Class PelunasanOverdueFundResult
    Public Property PelunasanOverdueFundList As IList(Of Parameter.PelunasanOverdueFund)
    Public Property TotalRecord As Integer

End Class


<Serializable()>
Public Class PelunasanOverdueFund
    Private _ApplicationID As String
    Private _AgreementNo As String
    Private _FundingCoyID As String
    Private _FundingContractID As String
    Private _FundingBatchID As String
    Private _CustomerID As String
    Private _CustomerName As String
    Private _SupplierID As String
    Private _SupplierName As String
    Private _InsSeqNo As Integer
    Private _InstallmentAmount As Double
    Private _PrincipalAmount As Double
    Private _InterestAmount As Double
    Private _RefundInterest As Double
    Private _Tenor As Integer
    Private _GoLiveDate As Date
    Private _DueDate As Date
    Private _Pelunasan As Date



    Public Sub New()
    End Sub

    Public Property ApplicationID As String
        Get
            Return _ApplicationID
        End Get
        Set(value As String)
            _ApplicationID = value
        End Set
    End Property
    Public Property AgreementNo As String
        Get
            Return _AgreementNo
        End Get
        Set(value As String)
            _AgreementNo = value
        End Set
    End Property
    Public Property FundingCoyID As String
        Get
            Return (_FundingCoyID)
        End Get
        Set(value As String)
            _FundingCoyID = value
        End Set
    End Property
    Public Property FundingContractID As String
        Get
            Return _FundingContractID
        End Get
        Set(value As String)
            _FundingContractID = value
        End Set
    End Property
    Public Property FundingBatchID As String
        Get
            Return _FundingBatchID
        End Get
        Set(value As String)
            _FundingBatchID = value
        End Set
    End Property
    Public Property CustomerID As String
        Get
            Return (_CustomerID)
        End Get
        Set(value As String)
            _CustomerID = value
        End Set
    End Property
    Public Property CustomerName As String
        Get
            Return (_CustomerName)
        End Get
        Set(value As String)
            _CustomerName = value
        End Set
    End Property
    Public Property SupplierID As String
        Get
            Return _SupplierID
        End Get
        Set(value As String)
            _SupplierID = value
        End Set
    End Property
    Public Property SupplierName As String
        Get
            Return _SupplierName
        End Get
        Set(value As String)
            _SupplierName = value
        End Set
    End Property
    Public Property InsSeqNo As Integer
        Get
            Return _InsSeqNo
        End Get
        Set(value As Integer)
            _InsSeqNo = value
        End Set
    End Property
    Public Property InstallmentAmount As Double
        Get
            Return _InstallmentAmount
        End Get
        Set(value As Double)
            _InstallmentAmount = value
        End Set
    End Property
    Public Property PrincipalAmount As Double
        Get
            Return _PrincipalAmount
        End Get
        Set(value As Double)
            _PrincipalAmount = value
        End Set
    End Property
    Public Property InterestAmount As Double
        Get
            Return _InstallmentAmount
        End Get
        Set(value As Double)
            _InstallmentAmount = value
        End Set
    End Property
    Public Property RefundInterest As Double
        Get
            Return _RefundInterest
        End Get
        Set(value As Double)
            _RefundInterest = value
        End Set
    End Property
    Public Property Tenor As Integer
        Get
            Return _Tenor
        End Get
        Set(value As Integer)
            _Tenor = value
        End Set
    End Property
    Public Property GoLiveDate As Date
        Get
            Return _GoLiveDate
        End Get
        Set(value As Date)
            _GoLiveDate = value
        End Set
    End Property
    Public Property GoLiveDateStr As String
        Set(value As String)
            _GoLiveDate = DateTime.ParseExact(value, "dd/MM/yyyy", Globalization.CultureInfo.InvariantCulture)
        End Set
        Get
            Return ""
        End Get
    End Property
    Public Property DueDateStr As String
        Set(value As String)
            _DueDate = DateTime.ParseExact(value, "dd/MM/yyyy", Globalization.CultureInfo.InvariantCulture)
        End Set
        Get
            Return ""
        End Get
    End Property
    Public Property DueDate As Date
        Get
            Return _DueDate
        End Get
        Set(value As Date)
            _DueDate = value
        End Set
    End Property
    Public Property Pelunasan As Date
        Get
            Return _Pelunasan
        End Get
        Set(value As Date)
            _Pelunasan = value
        End Set
    End Property
End Class
