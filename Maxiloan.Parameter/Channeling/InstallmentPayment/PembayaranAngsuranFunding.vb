<Serializable()> Public Class PembayaranAngsuranFunding : Inherits Common
    Private _datasetPembayaranAngsuran As DataSet
    Private _fundingCOyID As String
    Private _fundingContractNo As String
    Private _installmentDueDate As DateTime
    Private _jumlahKontrak As Integer
    Private _jumlahAngsuran As Double
    Private _jumlahBunga As Double
    Private _jumlahPokok As Double
    Private _refundBunga As Double
    Private _referenceNoBonHijau As String
    Private _listbonhijau As DataSet
    Private _installmentDueDateto As DateTime
    Private _listdatavendor As DataTable

    Public Property ListDataVendor() As DataTable
        Get
            Return _listdatavendor
        End Get
        Set(ByVal Value As DataTable)
            _listdatavendor = Value
        End Set
    End Property

    Public Property ListBonHijau() As DataSet
        Get
            Return _listbonhijau
        End Get
        Set(ByVal Value As DataSet)
            _listbonhijau = Value
        End Set
    End Property

    Public Property ReferenceNoBonHijau() As String
        Get
            Return _referenceNoBonHijau
        End Get
        Set(ByVal Value As String)
            _referenceNoBonHijau = Value
        End Set
    End Property

    Public Property JumlahKontrak() As Integer
        Get
            Return _jumlahKontrak
        End Get
        Set(ByVal Value As Integer)
            _jumlahKontrak = Value
        End Set
    End Property

    Public Property JumlahPokok() As Double
        Get
            Return _jumlahPokok
        End Get
        Set(ByVal Value As Double)
            _jumlahPokok = Value
        End Set
    End Property

    Public Property JumlahAngsuran() As Double
        Get
            Return _jumlahAngsuran
        End Get
        Set(ByVal Value As Double)
            _jumlahAngsuran = Value
        End Set
    End Property

    Public Property JumlahBunga() As Double
        Get
            Return _jumlahBunga
        End Get
        Set(ByVal Value As Double)
            _jumlahBunga = Value
        End Set
    End Property

    Public Property ListPembayaranAngsuran() As DataSet
        Get
            Return _datasetPembayaranAngsuran
        End Get
        Set(ByVal Value As DataSet)
            _datasetPembayaranAngsuran = Value
        End Set
    End Property

    Public Property FundingCoyID() As String
        Get
            Return _fundingCOyID
        End Get
        Set(ByVal Value As String)
            _fundingCOyID = Value
        End Set
    End Property

    Public Property FundingContractNo() As String
        Get
            Return _fundingContractNo
        End Get
        Set(ByVal Value As String)
            _fundingContractNo = Value
        End Set
    End Property

    Public Property InstallmentDueDate() As Date
        Get
            Return _installmentDueDate
        End Get
        Set(ByVal Value As Date)
            _installmentDueDate = Value
        End Set
    End Property

    Public Property RefundBunga() As Double
        Get
            Return _refundBunga
        End Get
        Set(ByVal Value As Double)
            _refundBunga = Value
        End Set
    End Property

    Public Property InstallmentDueDateTo() As Date
        Get
            Return _installmentDueDateto
        End Get
        Set(ByVal Value As Date)
            _installmentDueDateto = Value
        End Set
    End Property

    Public Property ListData As DataSet
    Public Property FundingBatchNo As String
    Public Property FundingBankID As String
    Public Property valuedate As Date
    Public Property referenceno As String
    Public Property bankaccountid As String
    Public Property notes As String
End Class


<Serializable()> Public Class DisbursePembayaranAngsuranFunding : Inherits PembayaranAngsuranFunding
    Private _principalpaidamount As Double
    Private _interestpaidamount As Double
    Private _penaltypaidamount As Double
    Private _valuedate As Date
    Private _referenceno As String
    Private _bankaccountid As String
    Private _companyid As String
    Private _pphpaid As Double
    Private _principalmustpaid As Double
    Private _interestmustpaid As Double
    Private _selectedapplicationids As String

    Public Property PrincipalPaidAmount() As Double
        Get
            Return _principalpaidamount
        End Get
        Set(ByVal Value As Double)
            _principalpaidamount = Value
        End Set
    End Property

    Public Property InterestPaidAmount() As Double
        Get
            Return _interestpaidamount
        End Get
        Set(ByVal Value As Double)
            _interestpaidamount = Value
        End Set
    End Property

    Public Property PenaltyPaidAmount() As Double
        Get
            Return _penaltypaidamount
        End Get
        Set(ByVal Value As Double)
            _penaltypaidamount = Value
        End Set
    End Property

    Public Property ValueDate() As Date
        Get
            Return _valuedate
        End Get
        Set(ByVal Value As Date)
            _valuedate = Value
        End Set
    End Property
    Public Property ReferenceNo() As String
        Get
            Return _referenceno
        End Get
        Set(ByVal Value As String)
            _referenceno = Value
        End Set
    End Property

    Public Property BankAccountId() As String
        Get
            Return _bankaccountid
        End Get
        Set(ByVal Value As String)
            _bankaccountid = Value
        End Set
    End Property

    'Public Property CompanyId() As String
    '    Get
    '        Return _companyid
    '    End Get
    '    Set(ByVal Value As String)
    '        _companyid = Value
    '    End Set
    'End Property

    Public Property PPHPaid() As Double
        Get
            Return _pphpaid
        End Get
        Set(ByVal Value As Double)
            _pphpaid = Value
        End Set
    End Property

    Public Property SelectedApplicationIDs() As String
        Get
            Return _selectedapplicationids
        End Get
        Set(ByVal Value As String)
            _selectedapplicationids = Value
        End Set
    End Property



End Class


