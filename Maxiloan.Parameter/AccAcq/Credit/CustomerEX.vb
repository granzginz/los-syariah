﻿<Serializable()> _
Public Class CustomerEX : Inherits Parameter.Customer
    Public Property SPTTahunan As Boolean
    Public Property TinggalSejakBulan As Integer
    Public Property KeteranganUsaha As String
    Public Property KategoryPers As String
    Public Property KondisiKantor As String
    Public Property KaryawanSejakBulan As Integer
    Public Property SaldoAwal As Decimal
    Public Property SaldoAkhir As Decimal
    Public Property JumlahHariTransaksi As Int16
    'Public Property JumlahKendaraan As Int16
    'Public Property Garasi As Boolean
    'Public Property PertamaKredit As String
    'Public Property OrderKe As Int16
    Public Property TotalLamaKerja As Integer
End Class
