﻿<Serializable()> _
Public Class CustomerGroup : Inherits Maxiloan.Parameter.Common
    Public Property CustomerGroupID As String
    Public Property CustomerGroupNm As String
    Public Property Plafond As Decimal
    Public Property Catatan As String
    Public Property Oustanding As Decimal
    Public Property Kontrak As Char
    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
End Class
