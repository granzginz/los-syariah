﻿<Serializable()> _
Public Class CustomerCases : Inherits Maxiloan.Parameter.Common
    Public Property CustomerID As String
    Public Property CaseID As Integer
    Public Property CustomerIDEdit As String
    Public Property CaseIDEdit As Integer
    Public Property ReferenceNo As String
    Public Property ReferenceDate As Date
    Public Property Notes As String

    Public Property Kota As String
    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
End Class
