
<Serializable()> _
Public Class PPK : Inherits Maxiloan.Parameter.Common

    Private _listPPK As DataTable
    Private _listdata As DataTable
    Private _hasil As Integer
    Private _listAgreement As DataSet
    Private _WhereCond2 As String
    Private _CustomerType As String
    Private _ApplicationID As String
    Private _BDate As String
    Private _ListReport As DataSet
    Private _ListConfLetter As DataTable
    Private _ListBonHijau As DataTable
    Private _ListBonHijauReport As DataSet
    Private _ListTandaTerimaDokumen As DataTable
    Private _ListTandaTerimaDokumenReport As DataSet
    Private _ListKelengkapanData As DataTable
    Private _ListKelengkapanDataReport As DataSet
    Private _ListListingSuratKontrak As DataTable
    Private _ListListingSuratKontrakReport As DataSet
    Private _GroupInvoiceNo As String
    Private _GroupSupplierID As String
    Private _MultiApplicationID As String
    Private _MultiAgreementNo As String
    Private _Branch_ID As String
    Private _AccountNo As String
    Private _ListReportOM As DataSet
    Private _Month As String
    Private _Year As String
    Private _ListPPKreditor As DataTable
    Private _ListWLetter As DataTable
    Private _ListSKEPO As DataTable

    Public Property ListReportOM() As DataSet
        Get
            Return CType(_ListReportOM, DataSet)
        End Get
        Set(ByVal Value As DataSet)
            _ListReportOM = Value
        End Set
    End Property
    Public Property Month() As String
        Get
            Return (CType(_Month, String))
        End Get
        Set(ByVal Value As String)
            _Month = Value
        End Set
    End Property
    Public Property Year() As String
        Get
            Return (CType(_Year, String))
        End Get
        Set(ByVal Value As String)
            _Year = Value
        End Set
    End Property

    Public Property BDate() As String
        Get
            Return (CType(_BDate, String))
        End Get
        Set(ByVal Value As String)
            _BDate = Value
        End Set
    End Property

    Public Property Branch_ID() As String
        Get
            Return (CType(_Branch_ID, String))
        End Get
        Set(ByVal Value As String)
            _Branch_ID = Value
        End Set
    End Property

    Public Property AccountNo() As String
        Get
            Return (CType(_AccountNo, String))
        End Get
        Set(ByVal Value As String)
            _AccountNo = Value
        End Set
    End Property

    Public Property MultiApplicationID() As String
        Get
            Return (CType(_MultiApplicationID, String))
        End Get
        Set(ByVal Value As String)
            _MultiApplicationID = Value
        End Set
    End Property

    Public Property MultiAgreementNo() As String
        Get
            Return (CType(_MultiAgreementNo, String))
        End Get
        Set(ByVal Value As String)
            _MultiAgreementNo = Value
        End Set
    End Property

    Public Property GroupInvoiceNo() As String
        Get
            Return (CType(_GroupInvoiceNo, String))
        End Get
        Set(ByVal Value As String)
            _GroupInvoiceNo = Value
        End Set
    End Property
    Public Property GroupSupplierID() As String
        Get
            Return (CType(_GroupSupplierID, String))
        End Get
        Set(ByVal Value As String)
            _GroupSupplierID = Value
        End Set
    End Property

    Public Property CustomerType() As String
        Get
            Return (CType(_CustomerType, String))
        End Get
        Set(ByVal Value As String)
            _CustomerType = Value
        End Set
    End Property

    Public Property ListAgreement() As DataSet
        Get
            Return _listAgreement
        End Get
        Set(ByVal Value As DataSet)
            _listAgreement = Value
        End Set
    End Property

    Public Property ListConfLetter() As DataTable
        Get
            Return _ListConfLetter
        End Get
        Set(ByVal Value As DataTable)
            _ListConfLetter = Value
        End Set
    End Property
    Public Property ListTandaTerimaDokumen() As DataTable
        Get
            Return _ListTandaTerimaDokumen
        End Get
        Set(ByVal Value As DataTable)
            _ListTandaTerimaDokumen = Value
        End Set
    End Property
    Public Property ListKelengkapanData() As DataTable
        Get
            Return _ListKelengkapanData
        End Get
        Set(ByVal Value As DataTable)
            _ListKelengkapanData = Value
        End Set
    End Property
    Public Property ListListingSuratKontrak() As DataTable
        Get
            Return _ListListingSuratKontrak
        End Get
        Set(ByVal Value As DataTable)
            _ListListingSuratKontrak = Value
        End Set
    End Property
    Public Property ListBonHijau() As DataTable
        Get
            Return _ListBonHijau
        End Get
        Set(ByVal Value As DataTable)
            _ListBonHijau = Value
        End Set
    End Property

    Public Property hasil() As Integer
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Integer)
            _hasil = Value
        End Set
    End Property

    Public Property ListPPK() As DataTable
        Get
            Return _listPPK
        End Get
        Set(ByVal Value As DataTable)
            _listPPK = Value
        End Set
    End Property
    Public Property ListReport() As DataSet
        Get
            Return CType(_ListReport, DataSet)
        End Get
        Set(ByVal Value As DataSet)
            _ListReport = Value
        End Set
    End Property
    Public Property listdata() As DataTable
        Get
            Return CType(_listdata, DataTable)
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property ListBonHijauReport() As DataSet
        Get
            Return CType(_ListBonHijauReport, DataSet)
        End Get
        Set(ByVal Value As DataSet)
            _ListBonHijauReport = Value
        End Set
    End Property
    Public Property ListTandaTerimaDokumenReport() As DataSet
        Get
            Return CType(_ListTandaTerimaDokumenReport, DataSet)
        End Get
        Set(ByVal Value As DataSet)
            _ListTandaTerimaDokumenReport = Value
        End Set
    End Property
    Public Property ListKelengkapanDataReport() As DataSet
        Get
            Return CType(_ListKelengkapanDataReport, DataSet)
        End Get
        Set(ByVal Value As DataSet)
            _ListKelengkapanDataReport = Value
        End Set
    End Property
    Public Property ListListingSuratKontrakReport() As DataSet
        Get
            Return CType(_ListListingSuratKontrakReport, DataSet)
        End Get
        Set(ByVal Value As DataSet)
            _ListListingSuratKontrakReport = Value
        End Set
    End Property
    Public Property WhereCond2() As String
        Get
            Return (CType(_WhereCond2, String))
        End Get
        Set(ByVal Value As String)
            _WhereCond2 = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return (CType(_ApplicationID, String))
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property

    Public Property TanggalKontrak As Date
    Public Property Status As String
    Public Property BankAccountType As String
    Public Property BankPurpose As String

    Public Property ListPPKreditor() As DataTable
        Get
            Return _ListPPKreditor
        End Get
        Set(ByVal Value As DataTable)
            _ListPPKreditor = Value
        End Set
    End Property

    Public Property ListWLetter() As DataTable
        Get
            Return _ListWLetter
        End Get
        Set(ByVal Value As DataTable)
            _ListWLetter = Value
        End Set
    End Property

    Public Property ListSKEPO() As DataTable
        Get
            Return _ListSKEPO
        End Get
        Set(ByVal Value As DataTable)
            _ListSKEPO = Value
        End Set
    End Property
End Class
