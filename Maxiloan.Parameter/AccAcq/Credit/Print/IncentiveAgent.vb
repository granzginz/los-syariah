<Serializable()> _
Public Class IncentiveAgent
    Private _dsIncentiveAgent As DataSet
    Private _dtIncentiveAgent As DataTable
    Private _hasil As Boolean    
    Private _errorMessage As String

    Public Property dsIncentiveAgent() As DataSet
        Get
            Return _dsIncentiveAgent
        End Get
        Set(ByVal Value As DataSet)
            _dsIncentiveAgent = Value
        End Set
    End Property
    Public Property dtIncentiveAgent() As DataTable
        Get
            Return _dtIncentiveAgent
        End Get
        Set(ByVal Value As DataTable)
            _dtIncentiveAgent = Value
        End Set
    End Property

    Public Property Hasil() As Boolean
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Boolean)
            _hasil = Value
        End Set
    End Property

    Public Property ErrorMessage() As String
        Get
            Return (CType(_ErrorMessage, String))
        End Get
        Set(ByVal Value As String)
            _ErrorMessage = Value
        End Set
    End Property

End Class
