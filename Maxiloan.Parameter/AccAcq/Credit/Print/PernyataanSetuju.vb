<Serializable()> _
Public Class PernyataanSetuju : Inherits Maxiloan.Parameter.Common
    Private _listPernyataanSetuju As DataSet
    Private _dtPernyataanSetuju As DataTable
    Private _hasil As Boolean
    Private _multiApplicationID As String
    Private _errorMessage As String

    Public Property listPernyataanSetuju() As DataSet
        Get
            Return _listPernyataanSetuju
        End Get
        Set(ByVal Value As DataSet)
            _listPernyataanSetuju = Value
        End Set
    End Property

    Public Property dtPernyataanSetuju() As DataTable
        Get
            Return _dtPernyataanSetuju
        End Get
        Set(ByVal Value As DataTable)
            _dtPernyataanSetuju = Value
        End Set
    End Property

    Public Property Hasil() As Boolean
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Boolean)
            _hasil = Value
        End Set
    End Property

    Public Property MultiApplicationID() As String
        Get
            Return (CType(_multiApplicationID, String))
        End Get
        Set(ByVal Value As String)
            _multiApplicationID = Value
        End Set
    End Property

    Public Property ErrorMessage() As String
        Get
            Return (CType(_ErrorMessage, String))
        End Get
        Set(ByVal Value As String)
            _ErrorMessage = Value
        End Set
    End Property

End Class
