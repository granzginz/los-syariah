<Serializable()> _
Public Class AgentFeeReceipt : Inherits Maxiloan.Parameter.Common
    Private _dataset As Dataset
    Private _datatable As DataTable
    Private _hasil As Boolean
    Private _multiApplicationID As String
    Private _errorMessage As String


    Public Property Dataset() As DataSet
        Get
            Return _dataset
        End Get
        Set(ByVal Value As DataSet)
            _dataset = Value
        End Set
    End Property

    Public Property Datatable() As DataTable
        Get
            Return _datatable
        End Get
        Set(ByVal Value As DataTable)
            _datatable = Value
        End Set
    End Property

    Public Property Hasil() As Boolean
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Boolean)
            _hasil = Value
        End Set
    End Property

    Public Property MultiApplicationID() As String
        Get
            Return (CType(_multiApplicationID, String))
        End Get
        Set(ByVal Value As String)
            _multiApplicationID = Value
        End Set
    End Property

    Public Property ErrorMessage() As String
        Get
            Return (CType(_errorMessage, String))
        End Get
        Set(ByVal Value As String)
            _errorMessage = Value
        End Set
    End Property
End Class
