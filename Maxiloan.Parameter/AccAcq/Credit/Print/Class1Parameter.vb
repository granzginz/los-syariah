<Serializable()> _
Public Class Class1Parameter : Inherits Maxiloan.Parameter.Common
    Private _listAdminFeeAllocation As DataSet

    Public Property listAdminFeeAllocation() As DataSet
        Get
            Return _listAdminFeeAllocation
        End Get
        Set(ByVal Value As DataSet)
            _listAdminFeeAllocation = Value
        End Set
    End Property
End Class
