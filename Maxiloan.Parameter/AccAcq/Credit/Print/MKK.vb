<Serializable()> _
Public Class MKK : Inherits Maxiloan.Parameter.Common
    Private _ds1 As DataSet
    Private _ds2 As DataSet
    Private _ds3 As DataSet
    
    Public Property ds1() As DataSet
        Get
            Return _ds1
        End Get
        Set(ByVal Value As DataSet)
            _ds1 = Value
        End Set
    End Property

    Public Property ds2() As DataSet
        Get
            Return _ds2
        End Get
        Set(ByVal Value As DataSet)
            _ds2 = Value
        End Set
    End Property

    Public Property ds3() As DataSet
        Get
            Return _ds3
        End Get
        Set(ByVal Value As DataSet)
            _ds3 = Value
        End Set
    End Property

    Private _custid As String

    Public Property CustomerID() As String
        Get
            Return _custid
        End Get
        Set(ByVal Value As String)
            _custid = Value
        End Set
    End Property

    Private _apprno As String

    Public Property ApprovalNo() As String
        Get
            Return _ApprNo
        End Get
        Set(ByVal Value As String)
            _ApprNo = Value
        End Set
    End Property
End Class
