

<Serializable()> _
Public Class Customer : Inherits Maxiloan.Parameter.Common
    Private _Name As String
    Private _Err As String
    Private _Type As String
    Private _IDType As String
    Private _IDNumber As String
    Private _Gender As String
    Private _BirthPlace As String
    Private _BirthDate As String
    Private _listdatareport As DataSet
    Private _SearchBy As String
    Private _listdata As New DataTable
    Private _totalrecords As Int64
    Private _Grid As String
    Private _Table As String
    Private _Additionalcollateralamount As String
    Private _AverageBalance As String
    Private _AverageCreditTransaction As String
    Private _AverageDebitTransaction As String
    Private _AvgBalanceAccount As String
    Private _AdditionalCollateralType As String
    Private _BankAccountType As String
    Private _BankBranch As String
    Private _BankID As String
    Private _CompanyJobTitle As String
    Private _CompanyName As String
    Private _CreditCardID As String
    Private _CreditCardType As String
    Private _CustomerType As String
    Private _Deposito As String
    Private _Education As String
    Private _HomeLocation As String
    Private _HomePrice As String
    Private _HomeStatus As String
    Private _IndustryTypeID As String
    Private _IsApplycarLoanBefore As String
    Private _JobTypeID As String
    Private _LivingCostAmount As String
    Private _AccountNo As String
    Private _EmploymentSinceYear As String
    Private _JobPos As String
    Private _MaritalStatus As String
    Private _MonthlyVariableIncome As String
    Private _Nationality As String
    Private _NoKK As String
    Private _Notes As String
    Private _NumOfDependence As String
    Private _OtherBusinessJobTitle As String
    Private _OtherBusinessName As String
    Private _OtherBusinessSinceYear As String
    Private _OtherBusinessType As String
    Private _PersonalCustomerType As String
    Private _PersonalNPWP As String
    Private _NumOfCreditCard As String
    Private _OtherLoanInstallment As String
    Private _ProffesionID As String
    Private _Reference As String
    Private _Religion As String
    Private _RentFinishDate As String
    Private _SpouseIncome As String
    Private _StaysinceYear As String
    Private _WNACountry As String
    Private _ProfessionID As String
    Private _MonthlyFixedIncome As String
    Private _OtherBusinessIndustryTypeID As String
    Private _CustomerGroupID As String
    Private _AccountName As String
    Private _MobilePhone As String
    Private _Email As String
    Private _CompanyType As String
    Private _NumEmployees As String
    Private _YearEstablished As String
    Private _Ratio As String
    Private _ROI As String
    Private _DER As String
    Private _BusPlaceStatus As String
    Private _BusPlaceSinceYear As String
    Private _WarehouseStatus As String
    Private _CustomerID As String
    Private _ListReport As DataSet
    Private _ProspectAppID As String
    Private _Gelar As String
    Private _SumberPembiayaan As String
    Private _RataRataPenghasilanPerTahun As String
    Private _KodeSumberPenghasilan As String
    Public Property Gelar() As String
        Get
            Return _Gelar
        End Get
        Set(ByVal Value As String)
            _Gelar = Value
        End Set
    End Property
    Public Property Err() As String
        Get
            Return _Err
        End Get
        Set(ByVal Value As String)
            _Err = Value
        End Set
    End Property
    Public Property CustomerID() As String
        Get
            Return _CustomerID
        End Get
        Set(ByVal Value As String)
            _CustomerID = Value
        End Set
    End Property
    Public Property Ratio() As String
        Get
            Return _Ratio
        End Get
        Set(ByVal Value As String)
            _Ratio = Value
        End Set
    End Property
    Public Property ROI() As String
        Get
            Return _ROI
        End Get
        Set(ByVal Value As String)
            _ROI = Value
        End Set
    End Property
    Public Property DER() As String
        Get
            Return _DER
        End Get
        Set(ByVal Value As String)
            _DER = Value
        End Set
    End Property
    Public Property BusPlaceStatus() As String
        Get
            Return _BusPlaceStatus
        End Get
        Set(ByVal Value As String)
            _BusPlaceStatus = Value
        End Set
    End Property
    Public Property BusPlaceSinceYear() As String
        Get
            Return _BusPlaceSinceYear
        End Get
        Set(ByVal Value As String)
            _BusPlaceSinceYear = Value
        End Set
    End Property
    Public Property WarehouseStatus() As String
        Get
            Return _WarehouseStatus
        End Get
        Set(ByVal Value As String)
            _WarehouseStatus = Value
        End Set
    End Property
    Public Property YearEstablished() As String
        Get
            Return _YearEstablished
        End Get
        Set(ByVal Value As String)
            _YearEstablished = Value
        End Set
    End Property
    Public Property NumEmployees() As String
        Get
            Return _NumEmployees
        End Get
        Set(ByVal Value As String)
            _NumEmployees = Value
        End Set
    End Property
    Public Property MobilePhone() As String
        Get
            Return _MobilePhone
        End Get
        Set(ByVal Value As String)
            _MobilePhone = Value
        End Set
    End Property
    Public Property CompanyType() As String
        Get
            Return _CompanyType
        End Get
        Set(ByVal Value As String)
            _CompanyType = Value
        End Set
    End Property
    Public Property Email() As String
        Get
            Return _Email
        End Get
        Set(ByVal Value As String)
            _Email = Value
        End Set
    End Property
    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property
    Public Property Grid() As String
        Get
            Return _Grid
        End Get
        Set(ByVal Value As String)
            _Grid = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return _Name
        End Get
        Set(ByVal Value As String)
            _Name = Value
        End Set
    End Property
    Public Property Type() As String
        Get
            Return _Type
        End Get
        Set(ByVal Value As String)
            _Type = Value
        End Set
    End Property
    Public Property IDType() As String
        Get
            Return _Type
        End Get
        Set(ByVal Value As String)
            _Type = Value
        End Set
    End Property
    Public Property IDNumber() As String
        Get
            Return _IDNumber
        End Get
        Set(ByVal Value As String)
            _IDNumber = Value
        End Set
    End Property
    Public Property Gender() As String
        Get
            Return _Gender
        End Get
        Set(ByVal Value As String)
            _Gender = Value
        End Set
    End Property
    Public Property BirthPlace() As String
        Get
            Return _BirthPlace
        End Get
        Set(ByVal Value As String)
            _BirthPlace = Value
        End Set
    End Property
    Public Property BirthDate() As String
        Get
            Return _BirthDate
        End Get
        Set(ByVal Value As String)
            _BirthDate = Value
        End Set
    End Property
    Public Property listdatareport() As DataSet
        Get
            Return _listdatareport
        End Get
        Set(ByVal Value As DataSet)
            _listdatareport = Value
        End Set
    End Property
    Public Property SearchBy() As String
        Get
            Return _SearchBy
        End Get
        Set(ByVal Value As String)
            _SearchBy = Value
        End Set
    End Property
    Public Property listdata() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property totalrecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property PersonalCustomerType() As String
        Get
            Return _PersonalCustomerType
        End Get
        Set(ByVal Value As String)
            _PersonalCustomerType = Value
        End Set
    End Property
    Public Property CustomerType() As String
        Get
            Return _CustomerType
        End Get
        Set(ByVal Value As String)
            _CustomerType = Value
        End Set
    End Property
    Public Property CustomerGroupID() As String
        Get
            Return _CustomerGroupID
        End Get
        Set(ByVal Value As String)
            _CustomerGroupID = Value
        End Set
    End Property
    Public Property Religion() As String
        Get
            Return _Religion
        End Get
        Set(ByVal Value As String)
            _Religion = Value
        End Set
    End Property
    Public Property MaritalStatus() As String
        Get
            Return _MaritalStatus
        End Get
        Set(ByVal Value As String)
            _MaritalStatus = Value
        End Set
    End Property
    Public Property NumOfDependence() As String
        Get
            Return _NumOfDependence
        End Get
        Set(ByVal Value As String)
            _NumOfDependence = Value
        End Set
    End Property
    Public Property PersonalNPWP() As String
        Get
            Return _PersonalNPWP
        End Get
        Set(ByVal Value As String)
            _PersonalNPWP = Value
        End Set
    End Property
    Public Property NoKK() As String
        Get
            Return _NoKK
        End Get
        Set(ByVal Value As String)
            _NoKK = Value
        End Set
    End Property
    Public Property Education() As String
        Get
            Return _Education
        End Get
        Set(ByVal Value As String)
            _Education = Value
        End Set
    End Property
    Public Property ProfessionID() As String
        Get
            Return _ProfessionID
        End Get
        Set(ByVal Value As String)
            _ProfessionID = Value
        End Set
    End Property
    Public Property Nationality() As String
        Get
            Return _Nationality
        End Get
        Set(ByVal Value As String)
            _Nationality = Value
        End Set
    End Property
    Public Property WNACountry() As String
        Get
            Return _WNACountry
        End Get
        Set(ByVal Value As String)
            _WNACountry = Value
        End Set
    End Property
    Public Property HomeStatus() As String
        Get
            Return _HomeStatus
        End Get
        Set(ByVal Value As String)
            _HomeStatus = Value
        End Set
    End Property
    Public Property RentFinishDate() As String
        Get
            Return _RentFinishDate
        End Get
        Set(ByVal Value As String)
            _RentFinishDate = Value
        End Set
    End Property
    Public Property HomeLocation() As String
        Get
            Return _HomeLocation
        End Get
        Set(ByVal Value As String)
            _HomeLocation = Value
        End Set
    End Property
    Public Property HomePrice() As String
        Get
            Return _HomePrice
        End Get
        Set(ByVal Value As String)
            _HomePrice = Value
        End Set
    End Property
    Public Property StaySinceYear() As String
        Get
            Return _StaysinceYear
        End Get
        Set(ByVal Value As String)
            _StaysinceYear = Value
        End Set
    End Property
    Public Property ListReport() As DataSet
        Get
            Return CType(_ListReport, DataSet)
        End Get
        Set(ByVal Value As DataSet)
            _ListReport = Value
        End Set
    End Property
    Public Property ProspectAppID() As String

        Get
            Return _ProspectAppID
        End Get
        Set(ByVal Value As String)
            _ProspectAppID = Value
        End Set
    End Property
    Public Property SumberPembiayaan As String
        Get
            Return _SumberPembiayaan
        End Get
        Set(ByVal Value As String)
            _SumberPembiayaan = Value
        End Set
    End Property
    Public Property RataRataPenghasilanPerTahun As String
        Get
            Return _RataRataPenghasilanPerTahun
        End Get
        Set(ByVal Value As String)
            _RataRataPenghasilanPerTahun = Value
        End Set
    End Property
    Public Property KodeSumberPenghasilan As String
        Get
            Return _KodeSumberPenghasilan
        End Get
        Set(ByVal Value As String)
            _KodeSumberPenghasilan = Value
        End Set
    End Property

#Region "Job Data"
    Public Property JobTypeID() As String
        Get
            Return _JobTypeID
        End Get
        Set(ByVal Value As String)
            _JobTypeID = Value
        End Set
    End Property
    Public Property JobPos() As String
        Get
            Return _JobPos
        End Get
        Set(ByVal Value As String)
            _JobPos = Value
        End Set
    End Property
    Public Property CompanyName() As String
        Get
            Return _CompanyName
        End Get
        Set(ByVal Value As String)
            _CompanyName = Value
        End Set
    End Property
    Public Property IndustryTypeID() As String
        Get
            Return _IndustryTypeID
        End Get
        Set(ByVal Value As String)
            _IndustryTypeID = Value
        End Set
    End Property
    Public Property CompanyJobTitle() As String
        Get
            Return _CompanyJobTitle
        End Get
        Set(ByVal Value As String)
            _CompanyJobTitle = Value
        End Set
    End Property
    Public Property EmploymentSinceYear() As String
        Get
            Return _EmploymentSinceYear
        End Get
        Set(ByVal Value As String)
            _EmploymentSinceYear = Value
        End Set
    End Property
#End Region
#Region "Professional"
    Public Property MonthlyFixedIncome() As String
        Get
            Return _MonthlyFixedIncome
        End Get
        Set(ByVal Value As String)
            _MonthlyFixedIncome = Value
        End Set
    End Property
    Public Property MonthlyVariableIncome() As String
        Get
            Return _MonthlyVariableIncome
        End Get
        Set(ByVal Value As String)
            _MonthlyVariableIncome = Value
        End Set
    End Property
#End Region
#Region "Entrepreneur"
    Public Property OtherBusinessName() As String
        Get
            Return _OtherBusinessName
        End Get
        Set(ByVal Value As String)
            _OtherBusinessName = Value
        End Set
    End Property
    Public Property OtherBusinessType() As String
        Get
            Return _OtherBusinessType
        End Get
        Set(ByVal Value As String)
            _OtherBusinessType = Value
        End Set
    End Property
    Public Property OtherBusinessIndustryTypeID() As String
        Get
            Return _OtherBusinessIndustryTypeID
        End Get
        Set(ByVal Value As String)
            _OtherBusinessIndustryTypeID = Value
        End Set
    End Property
    Public Property OtherBusinessJobTitle() As String
        Get
            Return _OtherBusinessJobTitle
        End Get
        Set(ByVal Value As String)
            _OtherBusinessJobTitle = Value
        End Set
    End Property
    Public Property OtherBusinessSinceYear() As String
        Get
            Return _OtherBusinessSinceYear
        End Get
        Set(ByVal Value As String)
            _OtherBusinessSinceYear = Value
        End Set
    End Property
#End Region
#Region "Financial Data"
    Public Property SpouseIncome() As String
        Get
            Return _SpouseIncome
        End Get
        Set(ByVal Value As String)
            _SpouseIncome = Value
        End Set
    End Property
    Public Property AvgBalanceAccount() As String
        Get
            Return _AvgBalanceAccount
        End Get
        Set(ByVal Value As String)
            _AvgBalanceAccount = Value
        End Set
    End Property
    Public Property BankAccountType() As String
        Get
            Return _BankAccountType
        End Get
        Set(ByVal Value As String)
            _BankAccountType = Value
        End Set
    End Property
    Public Property AverageDebitTransaction() As String
        Get
            Return _AverageDebitTransaction
        End Get
        Set(ByVal Value As String)
            _AverageDebitTransaction = Value
        End Set
    End Property
    Public Property AverageCreditTransaction() As String
        Get
            Return _AverageCreditTransaction
        End Get
        Set(ByVal Value As String)
            _AverageCreditTransaction = Value
        End Set
    End Property
    Public Property AverageBalance() As String
        Get
            Return _AverageBalance
        End Get
        Set(ByVal Value As String)
            _AverageBalance = Value
        End Set
    End Property
    Public Property Deposito() As String
        Get
            Return _Deposito
        End Get
        Set(ByVal Value As String)
            _Deposito = Value
        End Set
    End Property
    Public Property LivingCostAmount() As String
        Get
            Return _LivingCostAmount
        End Get
        Set(ByVal Value As String)
            _LivingCostAmount = Value
        End Set
    End Property
    Public Property OtherLoanInstallment() As String
        Get
            Return _OtherLoanInstallment
        End Get
        Set(ByVal Value As String)
            _OtherLoanInstallment = Value
        End Set
    End Property
    Public Property AdditionalCollateralType() As String
        Get
            Return _AdditionalCollateralType
        End Get
        Set(ByVal Value As String)
            _AdditionalCollateralType = Value
        End Set
    End Property
    Public Property AdditionalCollateralAmount() As String
        Get
            Return _Additionalcollateralamount
        End Get
        Set(ByVal Value As String)
            _Additionalcollateralamount = Value
        End Set
    End Property
    Public Property CreditCardID() As String
        Get
            Return _CreditCardID
        End Get
        Set(ByVal Value As String)
            _CreditCardID = Value
        End Set
    End Property
    Public Property CreditCardType() As String
        Get
            Return _CreditCardType
        End Get
        Set(ByVal Value As String)
            _CreditCardType = Value
        End Set
    End Property
    Public Property NumOfCreditCard() As String
        Get
            Return _NumOfCreditCard
        End Get
        Set(ByVal Value As String)
            _NumOfCreditCard = Value
        End Set
    End Property
#End Region
#Region "Bank Account"
    Public Property BankID() As String
        Get
            Return _BankID
        End Get
        Set(ByVal Value As String)
            _BankID = Value
        End Set
    End Property
    Public Property BankBranch() As String
        Get
            Return _BankBranch
        End Get
        Set(ByVal Value As String)
            _BankBranch = Value
        End Set
    End Property
    Public Property AccountNo() As String
        Get
            Return _AccountNo
        End Get
        Set(ByVal Value As String)
            _AccountNo = Value
        End Set
    End Property
    Public Property AccountName() As String
        Get
            Return _AccountName
        End Get
        Set(ByVal Value As String)
            _AccountName = Value
        End Set
    End Property
#End Region
#Region "Other Data"
    Public Property Reference() As String
        Get
            Return _Reference
        End Get
        Set(ByVal Value As String)
            _Reference = Value
        End Set
    End Property
    Public Property IsApplyCarLoanBefore() As String
        Get
            Return _IsApplycarLoanBefore
        End Get
        Set(ByVal Value As String)
            _IsApplycarLoanBefore = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property
#End Region

    Public Property JumlahKendaraan As Integer
    Public Property Garasi As Boolean
    Public Property PertamaKredit As String
    Public Property OrderKe As Integer
    Public Property IDExpiredDate As Date
    Public Property OmsetBulanan As Double
    Public Property BiayaBulanan As Double

    Public Property GelarBelakang As String
    Public Property KondisiRumah As String
    Public Property MotherName As String
    Public Property TglLahir As Date
    Public Property NamaSuamiIstri As String
    Public Property ResikoUsaha As Integer
    Public Property ResikoLS As Integer
    Public Property BankBranchId As Integer
    Public Property MobilePhone1 As String
    Public Property IsOLS As Boolean
    Public Property IsBUMN As Boolean

    Public Property KegiatanUsaha As String
    Public Property JenisPembiayaan As String


    Public Property JobsKantorOperasiSejak As Integer
    Public Property JobsKantorNPWP As String

    Public Property PenghasilanTetapPasangan As Double
    Public Property PenghasilanTambahanPasangan As Double
    Public Property PenghasilanTetapPenjamin As Double
    Public Property PenghasilanTambahanPenjamin As Double
    Public Property AngsuranLainnya As Double

    Public Property NatureOfBusinessId As String
    Public Property OccupationID As String
    Public Property IsRelatedBNI As Boolean
    Public Property IsEmployee As Boolean
    Public Property OPCSI As IList(Of CustomerSI)
    Public Property IndustryRisk As String
End Class
