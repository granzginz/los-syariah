﻿<Serializable()> _
Public Class CompanyCustomerDPEx : Inherits Parameter.Common    
    Public Property KeteranganUsaha As String
    Public Property StatusUsaha As String
    Public Property KategoriPerusahaan As String
    Public Property KondisiKantor As String
    Public Property KondisiLingkungan As String
    Public Property NamaWebsite As String
    Public Property JenisPembiayaan As String
End Class
