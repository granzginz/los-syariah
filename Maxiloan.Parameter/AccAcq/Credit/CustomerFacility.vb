

<Serializable()>
Public Class CustomerFacility : Inherits Maxiloan.Parameter.Common
    Private _NoFasilitas As String
    Private _NamaFasilitas As String
    Private _CustomerID As String
    Private _FacilityStartDate As String
    Private _FacilityMaturityDate As String
    Private _DrawDownStartDate As String
    Private _DrawDownMaturityDate As String
    Private _FasilitasAmount As Double
    Private _DrawDownAmount As Double
    Private _AvailableAmount As Double
    Private _FasilitasType As String
    Private _EffectiveRate As Decimal
    Private _FlatRate As Decimal
    Private _Retensi As Decimal
    Private _Tenor As Int16
    Private _AdminFee As Double
    Private _CommitmentFee As Double
    Private _ProvisionFee As Decimal
    Private _NotaryFee As Double
    Private _HandlingFee As Decimal
    Private _RateAsuransiKredit As Decimal
    Private _BiayaPolis As Double
    Private _FasilitasBalance As Double
    Private _CustomerName As String
    Private _ListData As DataTable
    Private _Status As String
    Private _ApprovalBy As String
    Private _ApprovalDate As String
    Private _MinimumPencairan As Double
    Private _ApprovalNo As String
    Private _ApprovalSchemeID As String
    Private _ApprovalResult As String
    Private _notes As String
    Private _IsReRequest As Boolean
    Private _IsFinal As Boolean
    Private _UserApproval As String
    Private _NextPersonApproval As String
    Private _Where As String
    Private _SecurityCode As String
    Private _UserSecurityCode As String
    Private _IsEverRejected As Boolean
    Private _JenisPembiayaan As String
    Private _KegiatanUsaha As String
    Private _InstallmentScheme As String
    Private _LateChargeRate As Decimal
    Private _NoAddendum As String
    Private _PaidAmount As Double
    Private _totalrecords As Int64
    Private _IDFasilitas As String
    Private _FacilityId As String
    Private _NoFasilitasAddendum As String

#Region "Property"
    Public Property NoFasilitas() As String
        Get
            Return _NoFasilitas
        End Get
        Set(ByVal Value As String)
            _NoFasilitas = Value
        End Set
    End Property

    Public Property IDFasilitas() As String
        Get
            Return _IDFasilitas
        End Get
        Set(ByVal Value As String)
            _IDFasilitas = Value
        End Set
    End Property

    Public Property NamaFasilitas() As String
        Get
            Return _NamaFasilitas
        End Get
        Set(ByVal Value As String)
            _NamaFasilitas = Value
        End Set
    End Property


    Public Property CustomerID() As String
        Get
            Return _CustomerID
        End Get
        Set(ByVal Value As String)
            _CustomerID = Value
        End Set
    End Property

    Public Property FacilityStartDate() As String
        Get
            Return _FacilityStartDate
        End Get
        Set(ByVal Value As String)
            _FacilityStartDate = Value
        End Set
    End Property

    Public Property FacilityMaturityDate() As String
        Get
            Return _FacilityMaturityDate
        End Get
        Set(ByVal Value As String)
            _FacilityMaturityDate = Value
        End Set
    End Property

    Public Property DrawDownStartDate() As String
        Get
            Return _DrawDownStartDate
        End Get
        Set(ByVal Value As String)
            _DrawDownStartDate = Value
        End Set
    End Property
    Public Property DrawDownMaturityDate() As String
        Get
            Return _DrawDownMaturityDate
        End Get
        Set(ByVal Value As String)
            _DrawDownMaturityDate = Value
        End Set
    End Property
    Public Property FasilitasAmount() As Double
        Get
            Return _FasilitasAmount
        End Get
        Set(ByVal Value As Double)
            _FasilitasAmount = Value
        End Set
    End Property
    Public Property DrawDownAmount() As Double
        Get
            Return _DrawDownAmount
        End Get
        Set(ByVal Value As Double)
            _DrawDownAmount = Value
        End Set
    End Property
    Public Property AvailableAmount() As Double
        Get
            Return _AvailableAmount
        End Get
        Set(ByVal Value As Double)
            _AvailableAmount = Value
        End Set
    End Property
    Public Property FasilitasType() As String
        Get
            Return _FasilitasType
        End Get
        Set(ByVal Value As String)
            _FasilitasType = Value
        End Set
    End Property

    Public Property EffectiveRate() As Decimal
        Get
            Return _EffectiveRate
        End Get
        Set(ByVal Value As Decimal)
            _EffectiveRate = Value
        End Set
    End Property

    Public Property FlatRate() As Decimal
        Get
            Return _FlatRate
        End Get
        Set(ByVal Value As Decimal)
            _FlatRate = Value
        End Set
    End Property

    Public Property Tenor() As Int16
        Get
            Return _Tenor
        End Get
        Set(ByVal Value As Int16)
            _Tenor = Value
        End Set
    End Property

    Public Property AdminFee() As Double
        Get
            Return _AdminFee
        End Get
        Set(ByVal Value As Double)
            _AdminFee = Value
        End Set
    End Property

    Public Property CommitmentFee() As Double
        Get
            Return _CommitmentFee
        End Get
        Set(ByVal Value As Double)
            _CommitmentFee = Value
        End Set
    End Property

    Public Property ProvisionFee() As Decimal
        Get
            Return _ProvisionFee
        End Get
        Set(ByVal Value As Decimal)
            _ProvisionFee = Value
        End Set
    End Property
    Public Property NotaryFee() As String
        Get
            Return _NotaryFee
        End Get
        Set(ByVal Value As String)
            _NotaryFee = Value
        End Set
    End Property
    Public Property HandlingFee() As Decimal
        Get
            Return _HandlingFee
        End Get
        Set(ByVal Value As Decimal)
            _HandlingFee = Value
        End Set
    End Property
    Public Property RateAsuransiKredit() As Decimal
        Get
            Return _RateAsuransiKredit
        End Get
        Set(ByVal Value As Decimal)
            _RateAsuransiKredit = Value
        End Set
    End Property
    Public Property BiayaPolis() As Double
        Get
            Return _BiayaPolis
        End Get
        Set(ByVal Value As Double)
            _BiayaPolis = Value
        End Set
    End Property

    Public Property FasilitasBalance() As Double
        Get
            Return _FasilitasBalance
        End Get
        Set(ByVal Value As Double)
            _FasilitasBalance = Value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return _CustomerName
        End Get
        Set(ByVal Value As String)
            _CustomerName = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal Value As DataTable)
            _ListData = Value
        End Set
    End Property

    Public Property Status() As String
        Get
            Return _Status
        End Get
        Set(ByVal Value As String)
            _Status = Value
        End Set
    End Property

    Public Property ApprovalBy() As String
        Get
            Return _ApprovalBy
        End Get
        Set(ByVal Value As String)
            _ApprovalBy = Value
        End Set
    End Property

    Public Property ApprovalDate() As String
        Get
            Return _ApprovalDate
        End Get
        Set(ByVal Value As String)
            _ApprovalDate = Value
        End Set
    End Property

    Public Property MinimumPencairan() As Double
        Get
            Return _MinimumPencairan
        End Get
        Set(ByVal Value As Double)
            _MinimumPencairan = Value
        End Set
    End Property

    Public Property ApprovalNo() As String
        Get
            Return _ApprovalNo
        End Get
        Set(ByVal Value As String)
            _ApprovalNo = Value
        End Set
    End Property

    Public Property ApprovalSchemeID() As String
        Get
            Return _ApprovalSchemeID
        End Get
        Set(ByVal Value As String)
            _ApprovalSchemeID = Value
        End Set
    End Property

    Public Property ApprovalResult() As String
        Get
            Return _ApprovalResult
        End Get
        Set(ByVal Value As String)
            _ApprovalResult = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property

    Public Property SecurityCode() As String
        Get
            Return _SecurityCode
        End Get
        Set(ByVal Value As String)
            _SecurityCode = Value
        End Set
    End Property

    Public Property IsReRequest() As Boolean
        Get
            Return _IsReRequest
        End Get
        Set(ByVal Value As Boolean)
            _IsReRequest = Value
        End Set
    End Property

    Public Property IsFinal() As Boolean
        Get
            Return _IsFinal
        End Get
        Set(ByVal Value As Boolean)
            _IsFinal = Value
        End Set
    End Property

    Public Property UserApproval() As String
        Get
            Return _UserApproval
        End Get
        Set(ByVal Value As String)
            _UserApproval = Value
        End Set
    End Property

    Public Property NextPersonApproval() As String
        Get
            Return _NextPersonApproval
        End Get
        Set(ByVal Value As String)
            _NextPersonApproval = Value
        End Set
    End Property

    Public Property UserSecurityCode() As String
        Get
            Return _UserSecurityCode
        End Get
        Set(ByVal Value As String)
            _UserSecurityCode = Value
        End Set
    End Property

    Public Property IsEverRejected() As Boolean
        Get
            Return _IsEverRejected
        End Get
        Set(ByVal Value As Boolean)
            _IsEverRejected = Value
        End Set
    End Property

    Public Property JenisPembiayaan() As String
        Get
            Return _JenisPembiayaan
        End Get
        Set(ByVal Value As String)
            _JenisPembiayaan = Value
        End Set
    End Property

    Public Property InstallmentScheme() As String
        Get
            Return _InstallmentScheme
        End Get
        Set(ByVal Value As String)
            _InstallmentScheme = Value
        End Set
    End Property

    Public Property KegiatanUsaha() As String
        Get
            Return _KegiatanUsaha
        End Get
        Set(ByVal Value As String)
            _KegiatanUsaha = Value
        End Set
    End Property

    Public Property Retensi() As Decimal
        Get
            Return _Retensi
        End Get
        Set(ByVal Value As Decimal)
            _Retensi = Value
        End Set
    End Property

    Public Property LateChargeRate() As Decimal
        Get
            Return _LateChargeRate
        End Get
        Set(ByVal Value As Decimal)
            _LateChargeRate = Value
        End Set
    End Property
#End Region

    Public Property LateChargePersen() As Decimal
    Public Property ApplicationModule() As String
    Public Property MPPA() As String
    Public Property MPPB() As String
    Public Property MPPC() As String
    Public Property MPPD() As String
    Public Property MPPE() As String

    Public Property NoAddendum() As String
        Get
            Return _NoAddendum
        End Get
        Set(ByVal Value As String)
            _NoAddendum = Value
        End Set
    End Property

    Public Property PaidAmount() As Double
        Get
            Return _PaidAmount
        End Get
        Set(ByVal Value As Double)
            _PaidAmount = Value
        End Set
    End Property
    Public Property totalrecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property FacilityId() As String
        Get
            Return _FacilityId
        End Get
        Set(ByVal Value As String)
            _FacilityId = Value
        End Set
    End Property

    Public Property NoFasilitasAddendum() As String
        Get
            Return _NoFasilitasAddendum
        End Get
        Set(ByVal Value As String)
            _NoFasilitasAddendum = Value
        End Set
    End Property
End Class
