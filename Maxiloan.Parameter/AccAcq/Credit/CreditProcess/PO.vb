
<Serializable()> _
Public Class PO : Inherits Common
    Private _ApplicationID As String
    Private _ProductOfferingID As String
    Private _ProductID As String
    Private _AgreementNo As String
    Private _CustomerName As String
    Private _SupplierName As String
    Private _SupplierAddress As String
    Private _SupplierRTRW As String
    Private _SupplierKecamatan As String
    Private _SupplierKelurahan As String
    Private _SupplierZipCode As String
    Private _SupplierPhone As String
    Private _SupplierCity As String
    Private _EmployeeName As String
    Private _ApprovalDate As Date
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _ApplicationStep As String
    Private _CreditScoringDate As Date
    Private _CreditScore As Decimal
    Private _CreditScoringResult As String
    Private _SupplierID As String
    Private _CustomerID As String
    Private _CustomerAddress As String
    Private _CustomerKecamatan As String
    Private _CustomerKelurahan As String
    Private _CustomerCity As String
    Private _CustomerZipCode As String
    Private _ID As String
    Private _Description As String
    Private _TotalOTR As Decimal
    Private _DownPayment As Decimal
    Private _InstallmentAmount As Decimal
    Private _AdminFee As Decimal
    Private _FiduciaFee As Decimal
    Private _ProvisionFee As Decimal
    Private _NotaryFee As Decimal
    Private _SurveyFee As Decimal
    Private _BBNFee As Decimal
    Private _OtherFee As Decimal
    Private _InsAssetReceivedinAdv As Decimal
    Private _EffectiveRate As Decimal
    Private _SupplierRate As Decimal
    Private _DiffRateAmount As Decimal
    Private _PONumber As String
    Private _BankName As String
    Private _BankBranch As String
    Private _AccountNo As String
    Private _AccountName As String
    Private _PODate As Date
    Private _POTotal As Decimal
    Private _Notes As String
    Private _PODescription As String
    Private _POAmount As Decimal
    Private _APRefund As Decimal
    Private _SupplierBankID As String
    Private _SupplierBankBranch As String
    Private _SupplierAccountName As String
    Private _SupplierAccountNo As String
    Private _POExtendCounter As Integer
    Private _IsExpired As Boolean
    Private _POExpiredDate As Date
    Private _Account As String
    Private _SuppCompanyIncentiveAmount As Decimal
    Private _RefundInterest As Decimal
    Private _SupplierAccountID As Integer

    Public Property RefundInterest() As Decimal
        Get
            Return _RefundInterest
        End Get
        Set(ByVal Value As Decimal)
            _RefundInterest = Value
        End Set
    End Property

    Public Property Account() As String
        Get
            Return _Account
        End Get
        Set(ByVal Value As String)
            _Account = Value
        End Set
    End Property

    Public Property POExtendCounter() As Integer
        Get
            Return _POExtendCounter
        End Get
        Set(ByVal Value As Integer)
            _POExtendCounter = Value
        End Set
    End Property

    Public Property IsExpired() As Boolean
        Get
            Return _IsExpired
        End Get
        Set(ByVal Value As Boolean)
            _IsExpired = Value
        End Set
    End Property

    Public Property POExpiredDate() As Date
        Get
            Return _POExpiredDate
        End Get
        Set(ByVal Value As Date)
            _POExpiredDate = Value
        End Set
    End Property

    Public Property ProductOfferingID() As String
        Get
            Return _ProductOfferingID
        End Get
        Set(ByVal Value As String)
            _ProductOfferingID = Value
        End Set
    End Property

    Public Property ProductID() As String
        Get
            Return _ProductID
        End Get
        Set(ByVal Value As String)
            _ProductID = Value
        End Set
    End Property

    Public Property SupplierBankID() As String
        Get
            Return _SupplierBankID
        End Get
        Set(ByVal Value As String)
            _SupplierBankID = Value
        End Set
    End Property

    Public Property SupplierBankBranch() As String
        Get
            Return _SupplierBankBranch
        End Get
        Set(ByVal Value As String)
            _SupplierBankBranch = Value
        End Set
    End Property

    Public Property SupplierAccountName() As String
        Get
            Return _SupplierAccountName
        End Get
        Set(ByVal Value As String)
            _SupplierAccountName = Value
        End Set
    End Property

    Public Property SupplierAccountNo() As String
        Get
            Return _SupplierAccountNo
        End Get
        Set(ByVal Value As String)
            _SupplierAccountNo = Value
        End Set
    End Property

    Public Property SupplierAccountID() As Integer
        Get
            Return _SupplierAccountID
        End Get
        Set(ByVal Value As Integer)
            _SupplierAccountID = Value
        End Set
    End Property

    Public Property APRefund() As Decimal
        Get
            Return _APRefund
        End Get
        Set(ByVal Value As Decimal)
            _APRefund = Value
        End Set
    End Property

    Public Property POAmount() As Decimal
        Get
            Return _POAmount
        End Get
        Set(ByVal Value As Decimal)
            _POAmount = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property

    Public Property PODescription() As String
        Get
            Return _PODescription
        End Get
        Set(ByVal Value As String)
            _PODescription = Value
        End Set
    End Property

    Public Property POTotal() As Decimal
        Get
            Return _POTotal
        End Get
        Set(ByVal Value As Decimal)
            _POTotal = Value
        End Set
    End Property

    Public Property PODate() As Date
        Get
            Return _PODate
        End Get
        Set(ByVal Value As Date)
            _PODate = Value
        End Set
    End Property

    Public Property AccountNo() As String
        Get
            Return _AccountNo
        End Get
        Set(ByVal Value As String)
            _AccountNo = Value
        End Set
    End Property

    Public Property AccountName() As String
        Get
            Return _AccountName
        End Get
        Set(ByVal Value As String)
            _AccountName = Value
        End Set
    End Property

    Public Property BankBranch() As String
        Get
            Return _BankBranch
        End Get
        Set(ByVal Value As String)
            _BankBranch = Value
        End Set
    End Property

    Public Property BankName() As String
        Get
            Return _BankName
        End Get
        Set(ByVal Value As String)
            _BankName = Value
        End Set
    End Property

    Public Property PONumber() As String
        Get
            Return _PONumber
        End Get
        Set(ByVal Value As String)
            _PONumber = Value
        End Set
    End Property

    Public Property ID() As String
        Get
            Return _ID
        End Get
        Set(ByVal Value As String)
            _ID = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property SupplierRate() As Decimal
        Get
            Return _SupplierRate
        End Get
        Set(ByVal Value As Decimal)
            _SupplierRate = Value
        End Set
    End Property

    Public Property DiffRateAmount() As Decimal
        Get
            Return _DiffRateAmount
        End Get
        Set(ByVal Value As Decimal)
            _DiffRateAmount = Value
        End Set
    End Property

    Public Property BBNFee() As Decimal
        Get
            Return _BBNFee
        End Get
        Set(ByVal Value As Decimal)
            _BBNFee = Value
        End Set
    End Property

    Public Property OtherFee() As Decimal
        Get
            Return _OtherFee
        End Get
        Set(ByVal Value As Decimal)
            _OtherFee = Value
        End Set
    End Property

    Public Property InsAssetReceivedinAdv() As Decimal
        Get
            Return _InsAssetReceivedinAdv
        End Get
        Set(ByVal Value As Decimal)
            _InsAssetReceivedinAdv = Value
        End Set
    End Property

    Public Property EffectiveRate() As Decimal
        Get
            Return _EffectiveRate
        End Get
        Set(ByVal Value As Decimal)
            _EffectiveRate = Value
        End Set
    End Property

    Public Property FiduciaFee() As Decimal
        Get
            Return _FiduciaFee
        End Get
        Set(ByVal Value As Decimal)
            _FiduciaFee = Value
        End Set
    End Property

    Public Property ProvisionFee() As Decimal
        Get
            Return _ProvisionFee
        End Get
        Set(ByVal Value As Decimal)
            _ProvisionFee = Value
        End Set
    End Property

    Public Property NotaryFee() As Decimal
        Get
            Return _NotaryFee
        End Get
        Set(ByVal Value As Decimal)
            _NotaryFee = Value
        End Set
    End Property

    Public Property SurveyFee() As Decimal
        Get
            Return _SurveyFee
        End Get
        Set(ByVal Value As Decimal)
            _SurveyFee = Value
        End Set
    End Property

    Public Property TotalOTR() As Decimal
        Get
            Return _TotalOTR
        End Get
        Set(ByVal Value As Decimal)
            _TotalOTR = Value
        End Set
    End Property

    Public Property DownPayment() As Decimal
        Get
            Return _DownPayment
        End Get
        Set(ByVal Value As Decimal)
            _DownPayment = Value
        End Set
    End Property

    Public Property InstallmentAmount() As Decimal
        Get
            Return _InstallmentAmount
        End Get
        Set(ByVal Value As Decimal)
            _InstallmentAmount = Value
        End Set
    End Property

    Public Property AdminFee() As Decimal
        Get
            Return _AdminFee
        End Get
        Set(ByVal Value As Decimal)
            _AdminFee = Value
        End Set
    End Property

    Public Property CustomerCity() As String
        Get
            Return _CustomerCity
        End Get
        Set(ByVal Value As String)
            _CustomerCity = Value
        End Set
    End Property

    Public Property SupplierCity() As String
        Get
            Return _SupplierCity
        End Get
        Set(ByVal Value As String)
            _SupplierCity = Value
        End Set
    End Property

    Public Property SupplierAddress() As String
        Get
            Return _SupplierAddress
        End Get
        Set(ByVal Value As String)
            _SupplierAddress = Value
        End Set
    End Property

    Public Property SupplierRTRW() As String
        Get
            Return _SupplierRTRW
        End Get
        Set(ByVal Value As String)
            _SupplierRTRW = Value
        End Set
    End Property

    Public Property SupplierKecamatan() As String
        Get
            Return _SupplierKecamatan
        End Get
        Set(ByVal Value As String)
            _SupplierKecamatan = Value
        End Set
    End Property

    Public Property SupplierKelurahan() As String
        Get
            Return _SupplierKelurahan
        End Get
        Set(ByVal Value As String)
            _SupplierKelurahan = Value
        End Set
    End Property

    Public Property SupplierZipCode() As String
        Get
            Return _SupplierZipCode
        End Get
        Set(ByVal Value As String)
            _SupplierZipCode = Value
        End Set
    End Property

    Public Property CustomerAddress() As String
        Get
            Return _CustomerAddress
        End Get
        Set(ByVal Value As String)
            _CustomerAddress = Value
        End Set
    End Property

    Public Property CustomerKecamatan() As String
        Get
            Return _CustomerKecamatan
        End Get
        Set(ByVal Value As String)
            _CustomerKecamatan = Value
        End Set
    End Property

    Public Property CustomerKelurahan() As String
        Get
            Return _CustomerKelurahan
        End Get
        Set(ByVal Value As String)
            _CustomerKelurahan = Value
        End Set
    End Property

    Public Property CustomerZipCode() As String
        Get
            Return _CustomerZipCode
        End Get
        Set(ByVal Value As String)
            _CustomerZipCode = Value
        End Set
    End Property

    Public Property SupplierID() As String
        Get
            Return _SupplierID
        End Get
        Set(ByVal Value As String)
            _SupplierID = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return _CustomerID
        End Get
        Set(ByVal Value As String)
            _CustomerID = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property

    Public Property SupplierPhone() As String
        Get
            Return _SupplierPhone
        End Get
        Set(ByVal Value As String)
            _SupplierPhone = Value
        End Set
    End Property

    Public Property CreditScoringDate() As Date
        Get
            Return _CreditScoringDate
        End Get
        Set(ByVal Value As Date)
            _CreditScoringDate = Value
        End Set
    End Property

    Public Property ApplicationStep() As String
        Get
            Return _ApplicationStep
        End Get
        Set(ByVal Value As String)
            _ApplicationStep = Value
        End Set
    End Property

    Public Property CreditScoringResult() As String
        Get
            Return _CreditScoringResult
        End Get
        Set(ByVal Value As String)
            _CreditScoringResult = Value
        End Set
    End Property

    Public Property CreditScore() As Decimal
        Get
            Return _CreditScore
        End Get
        Set(ByVal Value As Decimal)
            _CreditScore = Value
        End Set
    End Property

    Public Property ApprovalDate() As Date
        Get
            Return _ApprovalDate
        End Get
        Set(ByVal Value As Date)
            _ApprovalDate = Value
        End Set
    End Property

    Public Property SupplierName() As String
        Get
            Return _SupplierName
        End Get
        Set(ByVal Value As String)
            _SupplierName = Value
        End Set
    End Property

    Public Property EmployeeName() As String
        Get
            Return _EmployeeName
        End Get
        Set(ByVal Value As String)
            _EmployeeName = Value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return _CustomerName
        End Get
        Set(ByVal Value As String)
            _CustomerName = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property SuppCompanyIncentiveAmount() As Decimal
        Get
            Return _SuppCompanyIncentiveAmount
        End Get
        Set(ByVal Value As Decimal)
            _SuppCompanyIncentiveAmount = Value
        End Set
    End Property

    Public Property HargaKaroseri As Double
    Public Property SupplierIDKaroseri As String
    Public Property SplitPembayaran As Boolean
    Public Property NotesKaroseri As String
    Public Property DefaultBankAccount As Boolean
    Public Property UsedNew As String
    Public Property PengurangPO As String
    Public Property DescriptionPengurangPO As String
    Public Property IDKurs As String
    Public Property NilaiKurs As String
    Public Property SyaratPO As DataTable
End Class






