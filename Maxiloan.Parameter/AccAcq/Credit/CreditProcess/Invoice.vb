
<Serializable()> _
Public Class Invoice : Inherits Common
    Private _SupplierID As String
    Private _SupplierName As String
    Private _CustomerName As String
    Private _CustomerID As String
    Private _ContactPersonName As String
    Private _Phone As String
    Private _ApplicationID As String
    Private _ApplicationStep As String
    Private _Address As String
    Private _InvoiceNo As String
    Private _InvoiceDate As Date
    Private _InvoiceAmount As Double
    Private _AgreementNo As String
    Private _DeliveryOrderDate As Date
    Private _APAmount As Double
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _ListATPM As DataTable
    Private _listagreement As DataTable
    Private _VoucherNo As String
    Public Property VoucherNO() As String
        Get
            Return _VoucherNo
        End Get
        Set(value As String)
            _VoucherNo = value
        End Set
    End Property
    Public Property ListAgreement() As DataTable
        Get
            Return (CType(_listagreement, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listagreement = Value
        End Set
    End Property
    Public Property ListATPM() As DataTable
        Get
            Return (CType(_ListATPM, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _ListATPM = Value
        End Set
    End Property
    Public Property ApplicationStep() As String
        Get
            Return _ApplicationStep
        End Get
        Set(ByVal Value As String)
            _ApplicationStep = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return _CustomerID
        End Get
        Set(ByVal Value As String)
            _CustomerID = Value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return _CustomerName
        End Get
        Set(ByVal Value As String)
            _CustomerName = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property

    Public Property APAmount() As Double
        Get
            Return _APAmount
        End Get
        Set(ByVal Value As Double)
            _APAmount = Value
        End Set
    End Property

    Public Property InvoiceNo() As String
        Get
            Return _InvoiceNo
        End Get
        Set(ByVal Value As String)
            _InvoiceNo = Value
        End Set
    End Property
    Public Property InvoiceDate() As Date
        Get
            Return _InvoiceDate
        End Get
        Set(ByVal Value As Date)
            _InvoiceDate = Value
        End Set
    End Property

    Public Property DeliveryOrderDate() As Date
        Get
            Return _DeliveryOrderDate
        End Get
        Set(ByVal Value As Date)
            _DeliveryOrderDate = Value
        End Set
    End Property

    Public Property InvoiceAmount() As Double
        Get
            Return _InvoiceAmount
        End Get
        Set(ByVal Value As Double)
            _InvoiceAmount = Value
        End Set
    End Property

    Public Property SupplierID() As String
        Get
            Return _SupplierID
        End Get
        Set(ByVal Value As String)
            _SupplierID = Value
        End Set
    End Property

    Public Property SupplierName() As String
        Get
            Return _SupplierName
        End Get
        Set(ByVal Value As String)
            _SupplierName = Value
        End Set
    End Property

    Public Property ContactPersonName() As String
        Get
            Return _ContactPersonName
        End Get
        Set(ByVal Value As String)
            _ContactPersonName = Value
        End Set
    End Property

    Public Property Phone() As String
        Get
            Return _Phone
        End Get
        Set(ByVal Value As String)
            _Phone = Value
        End Set
    End Property

    Public Property Address() As String
        Get
            Return _Address
        End Get
        Set(ByVal Value As String)
            _Address = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property APDueDate As Date
#Region "INVOICEATPM"
    Private _SupplierATPM As String
    Private _InvoiceNoATPM As String
    Private _TotSubsidiBungaATPM As Double
    Private _TotSubsidiAngsuranATPM As Double
    Private _TotSubsidiUangmukaATPM As Double
    Private _TotPPN As Double
    Private _TotPPh23 As Double
    Private _TotalInvAmount As Double
    Private _TotalAmount As Double
    Public Property SupplierATPM() As String
        Get
            Return _SupplierATPM
        End Get
        Set(ByVal Value As String)
            _SupplierATPM = Value
        End Set
    End Property
    Public Property InvoiceNoATPM() As String
        Get
            Return _InvoiceNoATPM
        End Get
        Set(ByVal Value As String)
            _InvoiceNoATPM = Value
        End Set
    End Property
    Public Property TotSubsidiBungaATPM() As Double
        Get
            Return _TotSubsidiBungaATPM
        End Get
        Set(ByVal Value As Double)
            _TotSubsidiBungaATPM = Value
        End Set
    End Property
    Public Property TotSubsidiAngsuranATPM() As Double
        Get
            Return _TotSubsidiAngsuranATPM
        End Get
        Set(ByVal Value As Double)
            _TotSubsidiAngsuranATPM = Value
        End Set
    End Property
    Public Property TotSubsidiUangmukaATPM() As Double
        Get
            Return _TotSubsidiUangmukaATPM
        End Get
        Set(ByVal Value As Double)
            _TotSubsidiUangmukaATPM = Value
        End Set
    End Property
    Public Property TotPPN() As Double
        Get
            Return _TotPPN
        End Get
        Set(ByVal Value As Double)
            _TotPPN = Value
        End Set
    End Property
    Public Property TotPPh23() As Double
        Get
            Return _TotPPh23
        End Get
        Set(ByVal Value As Double)
            _TotPPh23 = Value
        End Set
    End Property
    Public Property TotalInvAmount() As Double
        Get
            Return _TotalInvAmount
        End Get
        Set(ByVal Value As Double)
            _TotalInvAmount = Value
        End Set
    End Property

    Public Property TotalAmount() As Double
        Get
            Return _TotalAmount
        End Get
        Set(ByVal Value As Double)
            _TotalAmount = Value
        End Set
    End Property

    Public Property WhereCond2 As String

#End Region
End Class


