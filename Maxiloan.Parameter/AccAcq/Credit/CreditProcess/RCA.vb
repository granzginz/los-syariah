
<Serializable()> _
Public Class RCA : Inherits Common
    Private _listdata As DataTable
    Private _totalrecords As Int64
    Private _WhereCond2 As String
    Private _Notes As String
    Private _RefundAmount As Double
    Private _RequestBy As String
    Private _ApprovalNo As String
    Private _Err As String
    Private _schemeid As String
    Private _AgreementNo As String
    Private _FundingBatchNo As String
    Private _FundingContractNo As String
    Private _FundingCoyId As String


    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property

    Public Property SchemeID() As String
        Get
            Return _schemeid
        End Get
        Set(ByVal Value As String)
            _schemeid = Value
        End Set
    End Property

    Public Property Err() As String
        Get
            Return _Err
        End Get
        Set(ByVal Value As String)
            _Err = Value
        End Set
    End Property

    Public Property ApprovalNo() As String
        Get
            Return _ApprovalNo
        End Get
        Set(ByVal Value As String)
            _ApprovalNo = Value
        End Set
    End Property
    Public Property RequestBy() As String
        Get
            Return _RequestBy
        End Get
        Set(ByVal Value As String)
            _RequestBy = Value
        End Set
    End Property
    Public Property RefundAmount() As Double
        Get
            Return _RefundAmount
        End Get
        Set(ByVal Value As Double)
            _RefundAmount = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property
    Public Property WhereCond2() As String
        Get
            Return _WhereCond2
        End Get
        Set(ByVal Value As String)
            _WhereCond2 = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property FundingBatchNo() As String
        Get
            Return _FundingBatchNo
        End Get
        Set(ByVal Value As String)
            _FundingBatchNo = Value
        End Set
    End Property
    Public Property FundingContractNo() As String
        Get
            Return _FundingContractNo
        End Get
        Set(ByVal Value As String)
            _FundingContractNo = Value
        End Set
    End Property
    Public Property FundingCoyId() As String
        Get
            Return _FundingCoyId
        End Get
        Set(ByVal Value As String)
            _FundingCoyId = Value
        End Set
    End Property

    Private _Argumentasi As String
    Public Property Argumentasi() As String
        Get
            Return _Argumentasi
        End Get
        Set(ByVal Value As String)
            _Argumentasi = Value
        End Set
    End Property
End Class
