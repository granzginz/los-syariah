﻿<Serializable()> _
Public Class KonfirmasiDealer : Inherits Common
    Public Property ApplicationID As String
    Public Property TotalRecords() As Int64
    Public Property dsKonfirmasiDealer() As DataTable
    Public Property TanggalKonfirmasi As Date
    Public Property TanggalKontrak As Date
    Public Property TanggalEfektif As Date
    Public Property SerialNo1 As String
    Public Property SerialNo2 As String
    Public Property Warna As String
    Public Property TahunKendaraan As String
    Public Property NamaSTNK As String
    Public Property AgreementNo As String
    Public Property Name As String
    Public Property Sts As String
    Public Property FirstInstallment As String
    Public Property stsFilter As String
End Class
