
<Serializable()> _
Public Class _DO : Inherits Common
    Private _ApplicationID As String
    Private _AttributeID As String
    Private _LicencePlate As String
    Private _AgreementNo As String
    Private _CustomerName As String
    Private _SupplierName As String
    Private _ContactPersonName As String
    Private _EmployeeName As String
    Private _PurchaseOrderDate As Date
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _ApplicationStep As String
    Private _CreditScoringDate As Date
    Private _CreditScore As Decimal
    Private _CreditScoringResult As String
    Private _SupplierID As String
    Private _CustomerID As String
    Private _Description As String
    Private _SerialNo1 As String
    Private _SerialNo2 As String
    Private _SerialNo1Label As String
    Private _SerialNo2Label As String
    Private _ManufacturingYear As Integer
    Private _OldOwnerAsset As String
    Private _OldOwnerAddress As String
    Private _OldOwnerRT As String
    Private _OldOwnerRW As String
    Private _OldOwnerKelurahan As String
    Private _OldOwnerKecamatan As String
    Private _OldOwnerCity As String
    Private _OldOwnerZipCode As String
    Private _TaxDate As Date
    Private _Notes As String
    Private _DeliveryDate As Date
    Private _AttributeContent As String
    Private _UsedNew As String
    Private _AssetTypeID As String
    Private _Input As String
    Private _AssetDocID As String
    Private _LicensePlate As String
    Private _Warna As String
    Private _NoPol As String
    Private _PromiseDate As Date

    Public Property Warna() As String
        Get
            Return _Warna
        End Get
        Set(ByVal Value As String)
            _Warna = Value
        End Set
    End Property
    Public Property Nopol() As String
        Get
            Return _NoPol
        End Get
        Set(ByVal Value As String)
            _NoPol = Value
        End Set
    End Property

    Public Property AssetDocID() As String
        Get
            Return _AssetDocID
        End Get
        Set(ByVal Value As String)
            _AssetDocID = Value
        End Set
    End Property

    Public Property Input() As String
        Get
            Return _Input
        End Get
        Set(ByVal Value As String)
            _Input = Value
        End Set
    End Property

    Public Property AssetTypeID() As String
        Get
            Return _AssetTypeID
        End Get
        Set(ByVal Value As String)
            _AssetTypeID = Value
        End Set
    End Property

    Public Property UsedNew() As String
        Get
            Return _UsedNew
        End Get
        Set(ByVal Value As String)
            _UsedNew = Value
        End Set
    End Property

    Public Property AttributeContent() As String
        Get
            Return _AttributeContent
        End Get
        Set(ByVal Value As String)
            _AttributeContent = Value
        End Set
    End Property


    Public Property AttributeID() As String
        Get
            Return _AttributeID
        End Get
        Set(ByVal Value As String)
            _AttributeID = Value
        End Set
    End Property

    Public Property LicensePlate() As String
        Get
            Return _LicensePlate
        End Get
        Set(ByVal Value As String)
            _LicensePlate = Value
        End Set
    End Property

    Public Property DeliveryDate() As Date
        Get
            Return _DeliveryDate
        End Get
        Set(ByVal Value As Date)
            _DeliveryDate = Value
        End Set
    End Property


    Public Property TaxDate() As Date
        Get
            Return _TaxDate
        End Get
        Set(ByVal Value As Date)
            _TaxDate = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property


    Public Property OldOwnerCity() As String
        Get
            Return _OldOwnerCity
        End Get
        Set(ByVal Value As String)
            _OldOwnerCity = Value
        End Set
    End Property

    Public Property OldOwnerZipCode() As String
        Get
            Return _OldOwnerZipCode
        End Get
        Set(ByVal Value As String)
            _OldOwnerZipCode = Value
        End Set
    End Property

    Public Property OldOwnerKelurahan() As String
        Get
            Return _OldOwnerKelurahan
        End Get
        Set(ByVal Value As String)
            _OldOwnerKelurahan = Value
        End Set
    End Property

    Public Property OldOwnerKecamatan() As String
        Get
            Return _OldOwnerKecamatan
        End Get
        Set(ByVal Value As String)
            _OldOwnerKecamatan = Value
        End Set
    End Property


    Public Property OldOwnerRT() As String
        Get
            Return _OldOwnerRT
        End Get
        Set(ByVal Value As String)
            _OldOwnerRT = Value
        End Set
    End Property

    Public Property OldOwnerRW() As String
        Get
            Return _OldOwnerRW
        End Get
        Set(ByVal Value As String)
            _OldOwnerRW = Value
        End Set
    End Property

    Public Property OldOwnerAddress() As String
        Get
            Return _OldOwnerAddress
        End Get
        Set(ByVal Value As String)
            _OldOwnerAddress = Value
        End Set
    End Property

    Public Property OldOwnerAsset() As String
        Get
            Return _OldOwnerAsset
        End Get
        Set(ByVal Value As String)
            _OldOwnerAsset = Value
        End Set
    End Property

    Public Property ManufacturingYear() As Integer
        Get
            Return _ManufacturingYear
        End Get
        Set(ByVal Value As Integer)
            _ManufacturingYear = Value
        End Set
    End Property

    Public Property SerialNo2() As String
        Get
            Return _SerialNo2
        End Get
        Set(ByVal Value As String)
            _SerialNo2 = Value
        End Set
    End Property

    Public Property SerialNo1() As String
        Get
            Return _SerialNo1
        End Get
        Set(ByVal Value As String)
            _SerialNo1 = Value
        End Set
    End Property

    Public Property SerialNo2Label() As String
        Get
            Return _SerialNo2Label
        End Get
        Set(ByVal Value As String)
            _SerialNo2Label = Value
        End Set
    End Property

    Public Property SerialNo1Label() As String
        Get
            Return _SerialNo1Label
        End Get
        Set(ByVal Value As String)
            _SerialNo1Label = Value
        End Set
    End Property

    Public Property PurchaseOrderDate() As Date
        Get
            Return _PurchaseOrderDate
        End Get
        Set(ByVal Value As Date)
            _PurchaseOrderDate = Value
        End Set
    End Property

    Public Property ContactPersonName() As String
        Get
            Return _ContactPersonName
        End Get
        Set(ByVal Value As String)
            _ContactPersonName = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property SupplierID() As String
        Get
            Return _SupplierID
        End Get
        Set(ByVal Value As String)
            _SupplierID = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return _CustomerID
        End Get
        Set(ByVal Value As String)
            _CustomerID = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property

    Public Property CreditScoringDate() As Date
        Get
            Return _CreditScoringDate
        End Get
        Set(ByVal Value As Date)
            _CreditScoringDate = Value
        End Set
    End Property

    Public Property ApplicationStep() As String
        Get
            Return _ApplicationStep
        End Get
        Set(ByVal Value As String)
            _ApplicationStep = Value
        End Set
    End Property

    Public Property CreditScoringResult() As String
        Get
            Return _CreditScoringResult
        End Get
        Set(ByVal Value As String)
            _CreditScoringResult = Value
        End Set
    End Property

    Public Property CreditScore() As Decimal
        Get
            Return _CreditScore
        End Get
        Set(ByVal Value As Decimal)
            _CreditScore = Value
        End Set
    End Property

    Public Property SupplierName() As String
        Get
            Return _SupplierName
        End Get
        Set(ByVal Value As String)
            _SupplierName = Value
        End Set
    End Property

    Public Property EmployeeName() As String
        Get
            Return _EmployeeName
        End Get
        Set(ByVal Value As String)
            _EmployeeName = Value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return _CustomerName
        End Get
        Set(ByVal Value As String)
            _CustomerName = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property


    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property PromiseDate() As Date
        Get
            Return _PromiseDate
        End Get
        Set(ByVal Value As Date)
            _PromiseDate = Value
        End Set
    End Property

    'Add
    Public Property AlasanReturn As String
    Public Property AssetOrigination As String
    Public Property CustomerType As String
    Public Property TipeAplikasi As String
    Public Property PPN As Double
    Public Property BBN As Double

End Class

