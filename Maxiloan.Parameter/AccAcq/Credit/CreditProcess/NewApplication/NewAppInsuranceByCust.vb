
<Serializable()> _
Public Class NewAppInsuranceByCust
    Inherits Maxiloan.Parameter.NewAppInsuranceByCompany

    Private _InsuranceCompanyName As String
    Private _SumInsured As Decimal
    Private _PolicyNo As String
    Private _ExpiredDate As Date
    Private _InsNotes As String

    Public Property InsNotes() As String
        Get
            Return _InsNotes
        End Get
        Set(ByVal Value As String)
            _InsNotes = Value
        End Set
    End Property

    Public Property ExpiredDate() As Date
        Get
            Return _ExpiredDate
        End Get
        Set(ByVal Value As Date)
            _ExpiredDate = Value
        End Set
    End Property


    Public Property PolicyNo() As String
        Get
            Return _PolicyNo
        End Get
        Set(ByVal Value As String)
            _PolicyNo = Value
        End Set
    End Property

    Public Property InsuranceCompanyName() As String
        Get
            Return _InsuranceCompanyName
        End Get
        Set(ByVal Value As String)
            _InsuranceCompanyName = Value
        End Set
    End Property

    Public Property SumInsured() As Decimal
        Get
            Return _SumInsured
        End Get
        Set(ByVal Value As Decimal)
            _SumInsured = Value
        End Set
    End Property


End Class
