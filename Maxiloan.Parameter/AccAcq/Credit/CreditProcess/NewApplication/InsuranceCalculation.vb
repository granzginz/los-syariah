
<Serializable()> _
Public Class InsuranceCalculation
    Inherits Maxiloan.Parameter.Common
    Private _ApplicationTypeID As String
    Private _ApplicationID As String
    Private _CustomerID As String
    Private _YearNum As Int16
    Private _CoverageType As String
    Private _InsuranceType As String
    Private _UsageID As String
    Private _NewUsed As String
    Private _BolSRCC As Boolean
    Private _TPL As Double
    Private _BolFlood As Boolean
    Private _BolRiot As Boolean
    Private _BolPA As Boolean
    Private _PaidByCustStatus As String
    Private _AmountCoverage As Double
    Private _InsLength As Int16
    Private _DateMonthYearManufacturingYear As DateTime
    Private _MaskAssBranchID As String
    Private _InsuranceComBranchID As String
    Private _AdminFeeToCust As Double
    Private _MeteraiFeeToCust As Double
    Private _DiscToCustAmount As Double
    Private _PaidAmountByCust As Double
    Private _PremiumBaseForRefundSupp As Double
    Private _AccNotes As String
    Private _InsNotes As String
    Private _IsPageSourceCompanyCustomer As Boolean
    Private _AdditionalCapitalized As Double
    Private _AdditionalInsurancePremium As Double
    Private _PaidAmtByCust As Double
    Private _DiscountToCust As Double
    Private _NumRows As Integer
    Private _BolSRCCDtg As String
    Private _BolFloodDtg As String
    Private _YearNumDtg As String
    Private _YearNumRateDtg As String
    Private _CoverageTypeDtg As String
    Private _TPLAmountToCustDtg As String
    Private _ListData As DataTable
    Private _BolRiotDtg As String
    Private _BolPADtg As String
    Private _AssetSeqNo As Int16
    Private _YearNumRate As Int16

    Public Property AdditionalCapitalized() As Double
        Get
            Return _AdditionalCapitalized
        End Get
        Set(ByVal Value As Double)
            _AdditionalCapitalized = Value
        End Set
    End Property

    Public Property AdditionalInsurancePremium() As Double
        Get
            Return _AdditionalInsurancePremium
        End Get
        Set(ByVal Value As Double)
            _AdditionalInsurancePremium = Value
        End Set
    End Property


    Public Property CustomerID() As String
        Get
            Return _CustomerID
        End Get
        Set(ByVal Value As String)
            _CustomerID = Value
        End Set
    End Property

    Public Property IsPageSourceCompanyCustomer() As Boolean
        Get
            Return _IsPageSourceCompanyCustomer
        End Get
        Set(ByVal Value As Boolean)
            _IsPageSourceCompanyCustomer = Value
        End Set
    End Property
    Public Property InsNotes() As String
        Get
            Return _InsNotes
        End Get
        Set(ByVal Value As String)
            _InsNotes = Value
        End Set
    End Property

    Public Property AccNotes() As String
        Get
            Return _AccNotes
        End Get
        Set(ByVal Value As String)
            _AccNotes = Value
        End Set
    End Property

    Public Property PremiumBaseForRefundSupp() As Double
        Get
            Return _PremiumBaseForRefundSupp
        End Get
        Set(ByVal Value As Double)
            _PremiumBaseForRefundSupp = Value
        End Set
    End Property


    Public Property PaidAmountByCust() As Double
        Get
            Return _PaidAmountByCust
        End Get
        Set(ByVal Value As Double)
            _PaidAmountByCust = Value
        End Set
    End Property

    Public Property DiscToCustAmount() As Double
        Get
            Return _DiscToCustAmount
        End Get
        Set(ByVal Value As Double)
            _DiscToCustAmount = Value
        End Set
    End Property



    Public Property MeteraiFeeToCust() As Double
        Get
            Return _MeteraiFeeToCust
        End Get
        Set(ByVal Value As Double)
            _MeteraiFeeToCust = Value
        End Set
    End Property



    Public Property AdminFeeToCust() As Double
        Get
            Return _AdminFeeToCust
        End Get
        Set(ByVal Value As Double)
            _AdminFeeToCust = Value
        End Set
    End Property

    Public Property InsuranceComBranchID() As String
        Get
            Return _InsuranceComBranchID
        End Get
        Set(ByVal Value As String)
            _InsuranceComBranchID = Value
        End Set
    End Property


    Public Property DateMonthYearManufacturingYear() As DateTime
        Get
            Return _DateMonthYearManufacturingYear
        End Get
        Set(ByVal Value As DateTime)
            _DateMonthYearManufacturingYear = Value
        End Set
    End Property




    Public Property ApplicationTypeID() As String
        Get
            Return _ApplicationTypeID
        End Get
        Set(ByVal Value As String)
            _ApplicationTypeID = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property

    Public Property YearNum() As Int16
        Get
            Return _YearNum
        End Get
        Set(ByVal Value As Int16)
            _YearNum = Value
        End Set
    End Property

    Public Property YearNumRate() As Int16
        Get
            Return _YearNumRate
        End Get
        Set(ByVal Value As Int16)
            _YearNumRate = Value
        End Set
    End Property

    Public Property CoverageType() As String
        Get
            Return _CoverageType
        End Get
        Set(ByVal Value As String)
            _CoverageType = Value
        End Set
    End Property

    Public Property InsuranceType() As String
        Get
            Return _InsuranceType
        End Get
        Set(ByVal Value As String)
            _InsuranceType = Value
        End Set
    End Property

    Public Property UsageID() As String
        Get
            Return _UsageID
        End Get
        Set(ByVal Value As String)
            _UsageID = Value
        End Set
    End Property

    Public Property NewUsed() As String
        Get
            Return _NewUsed
        End Get
        Set(ByVal Value As String)
            _NewUsed = Value
        End Set
    End Property

    Public Property BolSRCC() As Boolean
        Get
            Return _BolSRCC
        End Get
        Set(ByVal Value As Boolean)
            _BolSRCC = Value
        End Set
    End Property

    Public Property TPL() As Double
        Get
            Return _TPL
        End Get
        Set(ByVal Value As Double)
            _TPL = Value
        End Set
    End Property

    Public Property BolFlood() As Boolean
        Get
            Return _BolFlood
        End Get
        Set(ByVal Value As Boolean)
            _BolFlood = Value
        End Set
    End Property

    Public Property BolRiot() As Boolean
        Get
            Return _BolRiot
        End Get
        Set(ByVal Value As Boolean)
            _BolRiot = Value
        End Set
    End Property

    Public Property BolPA() As Boolean
        Get
            Return _BolPA
        End Get
        Set(ByVal Value As Boolean)
            _BolPA = Value
        End Set
    End Property



    Public Property PaidByCustStatus() As String
        Get
            Return _PaidByCustStatus
        End Get
        Set(ByVal Value As String)
            _PaidByCustStatus = Value
        End Set
    End Property

    Public Property AmountCoverage() As Double
        Get
            Return _AmountCoverage
        End Get
        Set(ByVal Value As Double)
            _AmountCoverage = Value
        End Set
    End Property

    Public Property InsLength() As Int16
        Get
            Return _InsLength
        End Get
        Set(ByVal Value As Int16)
            _InsLength = Value
        End Set
    End Property

    Public Property PaidAmtByCust() As Double
        Get
            Return _PaidAmtByCust
        End Get
        Set(ByVal Value As Double)
            _PaidAmtByCust = Value
        End Set
    End Property
    Public Property DiscountToCust() As Double
        Get
            Return _DiscountToCust
        End Get
        Set(ByVal Value As Double)
            _DiscountToCust = Value
        End Set
    End Property

    Public Property NumRows() As Integer
        Get
            Return _NumRows
        End Get
        Set(ByVal Value As Integer)
            _NumRows = Value
        End Set
    End Property

    Public Property BolSRCCDtg() As String
        Get
            Return _BolSRCCDtg
        End Get
        Set(ByVal Value As String)
            _BolSRCCDtg = Value
        End Set
    End Property

    Public Property BolFloodDtg() As String
        Get
            Return _BolFloodDtg
        End Get
        Set(ByVal Value As String)
            _BolFloodDtg = Value
        End Set
    End Property

    Public Property YearNumDtg() As String
        Get
            Return _YearNumDtg
        End Get
        Set(ByVal Value As String)
            _YearNumDtg = Value
        End Set
    End Property

    Public Property YearNumRateDtg() As String
        Get
            Return _YearNumRateDtg
        End Get
        Set(ByVal Value As String)
            _YearNumRateDtg = Value
        End Set
    End Property

    Public Property CoverageTypeDtg() As String
        Get
            Return _CoverageTypeDtg
        End Get
        Set(ByVal Value As String)
            _CoverageTypeDtg = Value
        End Set
    End Property

    Public Property TPLAmountToCustDtg() As String
        Get
            Return _TPLAmountToCustDtg
        End Get
        Set(ByVal Value As String)
            _TPLAmountToCustDtg = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal Value As DataTable)
            _ListData = Value
        End Set
    End Property
    Public Property BolRiotDtg() As String
        Get
            Return _BolRiotDtg
        End Get
        Set(ByVal Value As String)
            _BolRiotDtg = Value
        End Set
    End Property
    Public Property BolPADtg() As String
        Get
            Return _BolPADtg
        End Get
        Set(ByVal Value As String)
            _BolPADtg = Value
        End Set
    End Property
    Public Property MaskAssBranchID() As String
        Get
            Return _MaskAssBranchID
        End Get
        Set(ByVal Value As String)
            _MaskAssBranchID = Value
        End Set
    End Property
    Public Property AssetSeqNo() As Int16
        Get
            Return _AssetSeqNo
        End Get
        Set(ByVal Value As Int16)
            _AssetSeqNo = Value
        End Set
    End Property

    Public Property bolPADriver As Boolean
    Public Property bolTPL As Boolean
    Public Property InsuranceRate As Decimal
    Public Property MainPremiumToCust As Decimal
    Public Property MasterRateCardID As String

End Class
