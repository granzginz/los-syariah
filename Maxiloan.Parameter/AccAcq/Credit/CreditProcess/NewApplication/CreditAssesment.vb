﻿<Serializable()> _
Public Class CreditAssesment : Inherits Parameter.Application
    Public Property SurveyorID As String
    Public Property TeleponRumah As String
    Public Property TeleponKantor As String
    Public Property NoHp As String
    Public Property TeleponEC As String
    Public Property TeleponPenjamin As String
    Public Property TanggalTelepon As Date
    Public Property CreditAnalyst As String
    Public Property SurveyorNotes As String
    Public Property SaldoRataRata As Double
    Public Property SaldoAwal As Double
    Public Property SaldoAkhir As Double
    Public Property JumlahPemasukan As Double
    Public Property JumlahPengeluaran As Double
    Public Property JumlahHariTransaksi As Integer
    Public Property NotesBank As String
    Public Property JenisRekening As String
    Public Property RequestBy As String
    Public Property SchemeID As String
    Public Property RefundAmount As String
    Public Property ApprovalNo As String
    Public Property TeleponRumahArea As String
    Public Property TeleponKantorArea As String
    Public Property TeleponECArea As String
    Public Property TeleponPenjaminArea As String
    Public Property Argumentasi As String
    Public Property part As String
End Class
