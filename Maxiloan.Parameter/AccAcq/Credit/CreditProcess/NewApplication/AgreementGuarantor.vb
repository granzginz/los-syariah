﻿<Serializable()> _
Public Class AgreementGuarantor : Inherits Maxiloan.Parameter.Common

    Public Property ApplicationID As String
    Public Property GuarantorName As String
    Public Property GuarantorJobTitle As String
    Public Property GuarantorAddress As String
    Public Property GuarantorRT As String
    Public Property GuarantorRW As String
    Public Property GuarantorKelurahan As String
    Public Property GuarantorKecamatan As String
    Public Property GuarantorCity As String
    Public Property GuarantorZipCode As String
    Public Property GuarantorAreaPhone1 As String
    Public Property GuarantorPhone1 As String
    Public Property GuarantorAreaPhone2 As String
    Public Property GuarantorPhone2 As String
    Public Property GuarantorAreaFax As String
    Public Property GuarantorFax As String
    Public Property GuarantorMobilePhone As String
    Public Property GuarantorEmail As String
    Public Property GuarantorNotes As String
    Public Property GuarantorPenghasilan As Decimal

    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
End Class
