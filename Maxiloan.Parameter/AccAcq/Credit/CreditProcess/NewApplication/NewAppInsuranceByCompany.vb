
<Serializable()> _
Public Class NewAppInsuranceByCompany
    Inherits Maxiloan.Parameter.NewAppInsurance

    Private _InsAdminFee As Double
    Private _InsAdminFeeBehaviour As String
    Private _InsStampDutyFee As Double
    Private _InsStampDutyFeeBehaviour As String
    Private _TotalOTR As Double
    Private _AssetUsageID As String
    Private _AssetUsageDescr As String
    Private _Tenor As Double
    Private _Tenor2 As Double
    Private _AssetMasterDescr As String
    Private _InsuranceAssetDescr As String
    Private _AssetUsageNewUsed As String
    Private _MinimumTenor As Int16
    Private _MaximumTenor As Int16
    Private _PBMaximumTenor As Int16
    Private _PBMinimumTenor As Int16
    Private _PMaximumTenor As Int16
    Private _PMinimumTenor As Int16
    Private _MaskAssID As String
    Private _InsuranceComBranchID As String
    Private _InsuranceComBranchName As String
    Private _YearInsurance As Int16
    Private _CoverageType As String
    Private _Premium As Double
    Private _SRCC As Boolean
    Private _SRCCAmount As Double
    Private _TPL As Double
    Private _TPLAmount As Double
    Private _Flood As Boolean
    Private _FloodAmount As Double
    Private _Total As Double
    Private _PaidByCust As String
    Private _JmlGrid As Int16
    Private _JmlGrid2 As Int16
    Private _InsuranceType As String
    Private _ManufacturingYear As String
    Private _ApplicationTypeDescr As String
    Private _refundtosupplier As Double
    Private _PerluasanPA As Boolean
    Private _Table As String
    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property

    Public Property RefundToSupplier() As Double
        Get
            Return _refundtosupplier
        End Get
        Set(ByVal Value As Double)
            _refundtosupplier = Value
        End Set
    End Property

    Public Property ApplicationTypeDescr() As String
        Get
            Return _ApplicationTypeDescr
        End Get
        Set(ByVal Value As String)
            _ApplicationTypeDescr = Value
        End Set
    End Property

    Public Property ManufacturingYear() As String
        Get
            Return _ManufacturingYear
        End Get
        Set(ByVal Value As String)
            _ManufacturingYear = Value
        End Set
    End Property

    Public Property JmlGrid() As Int16
        Get
            Return _JmlGrid
        End Get
        Set(ByVal Value As Int16)
            _JmlGrid = Value
        End Set
    End Property
    Public Property JmlGrid2() As Int16
        Get
            Return _JmlGrid2
        End Get
        Set(ByVal Value As Int16)
            _JmlGrid2 = Value
        End Set
    End Property


    Public Property YearInsurance() As Int16
        Get
            Return _YearInsurance
        End Get
        Set(ByVal Value As Int16)
            _YearInsurance = Value
        End Set
    End Property

    Public Property CoverageType() As String
        Get
            Return _CoverageType
        End Get
        Set(ByVal Value As String)
            _CoverageType = Value
        End Set
    End Property

    Public Property Premium() As Double
        Get
            Return _Premium
        End Get
        Set(ByVal Value As Double)
            _Premium = Value
        End Set
    End Property

    Public Property SRCC() As Boolean
        Get
            Return _SRCC
        End Get
        Set(ByVal Value As Boolean)
            _SRCC = Value
        End Set
    End Property

    Public Property SRCCAmount() As Double
        Get
            Return _SRCCAmount
        End Get
        Set(ByVal Value As Double)
            _SRCCAmount = Value
        End Set
    End Property

    Public Property TPL() As Double
        Get
            Return _TPL
        End Get
        Set(ByVal Value As Double)
            _TPL = Value
        End Set
    End Property


    Public Property TPLAmount() As Double
        Get
            Return _TPLAmount
        End Get
        Set(ByVal Value As Double)
            _TPLAmount = Value
        End Set
    End Property



    Public Property Flood() As Boolean
        Get
            Return _Flood
        End Get
        Set(ByVal Value As Boolean)
            _Flood = Value
        End Set
    End Property

    Public Property FloodAmount() As Double
        Get
            Return _FloodAmount
        End Get
        Set(ByVal Value As Double)
            _FloodAmount = Value
        End Set
    End Property

    Public Property Total() As Double
        Get
            Return _Total
        End Get
        Set(ByVal Value As Double)
            _Total = Value
        End Set
    End Property

    Public Property PaidByCust() As String
        Get
            Return _PaidByCust
        End Get
        Set(ByVal Value As String)
            _PaidByCust = Value
        End Set
    End Property

    Public Property InsuranceComBranchName() As String
        Get
            Return _InsuranceComBranchName
        End Get
        Set(ByVal Value As String)
            _InsuranceComBranchName = Value
        End Set
    End Property

    Public Property MaskAssID() As String
        Get
            Return _MaskAssID
        End Get
        Set(ByVal Value As String)
            _MaskAssID = Value
        End Set
    End Property

    Public Property InsuranceComBranchID() As String
        Get
            Return _InsuranceComBranchID
        End Get
        Set(ByVal Value As String)
            _InsuranceComBranchID = Value
        End Set
    End Property
    Public Property PBMaximumTenor() As Int16
        Get
            Return _PBMaximumTenor
        End Get
        Set(ByVal Value As Int16)
            _PBMaximumTenor = Value
        End Set
    End Property
    Public Property PBMinimumTenor() As Int16
        Get
            Return _PBMinimumTenor
        End Get
        Set(ByVal Value As Int16)
            _PBMinimumTenor = Value
        End Set
    End Property
    Public Property PMaximumTenor() As Int16
        Get
            Return _PMaximumTenor
        End Get
        Set(ByVal Value As Int16)
            _PMaximumTenor = Value
        End Set
    End Property

    Public Property PMinimumTenor() As Int16
        Get
            Return _PMinimumTenor
        End Get
        Set(ByVal Value As Int16)
            _PMinimumTenor = Value
        End Set
    End Property
    Public Property MaximumTenor() As Int16
        Get
            Return _MaximumTenor
        End Get
        Set(ByVal Value As Int16)
            _MaximumTenor = Value
        End Set
    End Property
    Public Property MinimumTenor() As Int16
        Get
            Return _MinimumTenor
        End Get
        Set(ByVal Value As Int16)
            _MinimumTenor = Value
        End Set
    End Property
    Public Property InsAdminFee() As Double
        Get
            Return _InsAdminFee
        End Get
        Set(ByVal Value As Double)
            _InsAdminFee = Value
        End Set
    End Property

    Public Property InsAdminFeeBehaviour() As String
        Get
            Return _InsAdminFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _InsAdminFeeBehaviour = Value
        End Set
    End Property

    Public Property InsStampDutyFee() As Double
        Get
            Return _InsStampDutyFee
        End Get
        Set(ByVal Value As Double)
            _InsStampDutyFee = Value
        End Set
    End Property

    Public Property InsStampDutyFeeBehaviour() As String
        Get
            Return _InsStampDutyFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _InsStampDutyFeeBehaviour = Value
        End Set
    End Property
    Public Property TotalOTR() As Double
        Get
            Return _TotalOTR
        End Get
        Set(ByVal Value As Double)
            _TotalOTR = Value
        End Set
    End Property


    Public Property AssetUsageID() As String
        Get
            Return _AssetUsageID
        End Get
        Set(ByVal Value As String)
            _AssetUsageID = Value
        End Set
    End Property


    Public Property AssetUsageDescr() As String
        Get
            Return _AssetUsageDescr
        End Get
        Set(ByVal Value As String)
            _AssetUsageDescr = Value
        End Set
    End Property
    Public Property Tenor() As Double
        Get
            Return _Tenor
        End Get
        Set(ByVal Value As Double)
            _Tenor = Value
        End Set
    End Property
    Public Property Tenor2() As Double
        Get
            Return _Tenor2
        End Get
        Set(ByVal Value As Double)
            _Tenor2 = Value
        End Set
    End Property

    'Public Property Tenor() As Int32
    '    Get
    '        Return _Tenor
    '    End Get
    '    Set(ByVal Value As Int32)
    '        _Tenor = Value
    '    End Set
    'End Property

    'Public Property Tenor2() As Int32
    '    Get
    '        Return _Tenor2
    '    End Get
    '    Set(ByVal Value As Int32)
    '        _Tenor2 = Value
    '    End Set
    'End Property

    Public Property AssetMasterDescr() As String
        Get
            Return _AssetMasterDescr
        End Get
        Set(ByVal Value As String)
            _AssetMasterDescr = Value
        End Set
    End Property


    Public Property InsuranceAssetDescr() As String
        Get
            Return _InsuranceAssetDescr
        End Get
        Set(ByVal Value As String)
            _InsuranceAssetDescr = Value
        End Set
    End Property


    Public Property AssetUsageNewUsed() As String
        Get
            Return _AssetUsageNewUsed
        End Get
        Set(ByVal Value As String)
            _AssetUsageNewUsed = Value
        End Set
    End Property


    Public Property InsuranceType() As String
        Get
            Return _InsuranceType
        End Get
        Set(ByVal Value As String)
            _InsuranceType = Value
        End Set
    End Property
    Public Property PerluasanPA() As Boolean
        Get
            Return _PerluasanPA
        End Get
        Set(value As Boolean)
            _PerluasanPA = value
        End Set
    End Property

    Public Property NilaiAksesoris As String
    Public Property JenisAksesoris As String
    Public Property InsNotes As String
    Public Property sellingRate As Decimal
    Public Property BiayaPolis As Double
    Public Property IsBMSave As Boolean
    Public Property DescWil As String
    Public Property BiayaMaterai As Double

    Public Property ProductID As String
    Public Property MaskAssIDCP As String
    Public Property InsuranceComBranchIDCP As String
    Public Property InsuranceComBranchNameCP As String
    Public Property MaskAssIDAJK As String
    Public Property InsuranceComBranchIDAJK As String
    Public Property InsuranceComBranchNameAJK As String
    Public Property RateCP As Double
    Public Property RateAJK As Double
    Public Property CreditProtection As Boolean
    Public Property JaminanKredit As Boolean
    Public Property BiayaPolisCP As Double
    Public Property BiayaPolisAJK As Double
    Public Property RateCPInsco As Double
    Public Property RateAJKInsco As Double
    Public Property Rate As Int32
    Public Property InsRateCategory As String
End Class
