
<Serializable()> _
Public Class NewAppInsurance
    Inherits Maxiloan.Parameter.Common

    Private _listdata As DataTable
    Private _totalrecords As Int64
    Private _ApplicationID As String
    Private _CustomerID As String
    Private _CustomerName As String
    Private _ProductOfferingID As String
    Private _ProductOfferingDescr As String
    Private _AOID As String
    Private _AOName As String
    Private _InsAssetInsuredBy As String
    Private _InsAssetInsuredByName As String
    Private _InsAssetPaidBy As String
    Private _InsAssetPaidByName As String
    Private _Page As String
    Private _InterestType As String
    Private _InstallmentScheme As String
    Private _SupplierGroupID As String
    Private _SpName As String
    Private _NumberInstallment As Double
    Private _NumberAgreementAsset As Double
    Private _AssetID As String
    Private _DateEntryApplicationData As Date
    Private _DateEntryAssetData As Date
    Private _DateEntryInsuranceData As Date
    Private _DateEntryFinancialData As Date
    Private _DateEntryIncentiveData As Date
    Public Property DateEntryIncentiveData() As Date
        Get
            Return _DateEntryIncentiveData
        End Get
        Set(ByVal Value As Date)
            _DateEntryIncentiveData = Value
        End Set
    End Property
    Public Property DateEntryApplicationData() As Date
        Get
            Return _DateEntryApplicationData
        End Get
        Set(ByVal Value As Date)
            _DateEntryApplicationData = Value
        End Set
    End Property

    Public Property DateEntryAssetData() As Date
        Get
            Return _DateEntryAssetData
        End Get
        Set(ByVal Value As Date)
            _DateEntryAssetData = Value
        End Set
    End Property
    Public Property DateEntryInsuranceData() As Date
        Get
            Return _DateEntryInsuranceData
        End Get
        Set(ByVal Value As Date)
            _DateEntryInsuranceData = Value
        End Set
    End Property
    Public Property DateEntryFinancialData() As Date
        Get
            Return _DateEntryFinancialData
        End Get
        Set(ByVal Value As Date)
            _DateEntryFinancialData = Value
        End Set
    End Property



    Public Property AssetID() As String
        Get
            Return _AssetID
        End Get
        Set(ByVal Value As String)
            _AssetID = Value
        End Set
    End Property
    Public Property NumberAgreementAsset() As Double
        Get
            Return _NumberAgreementAsset
        End Get
        Set(ByVal Value As Double)
            _NumberAgreementAsset = Value
        End Set
    End Property
    Public Property NumberInstallment() As Double
        Get
            Return _NumberInstallment
        End Get
        Set(ByVal Value As Double)
            _NumberInstallment = Value
        End Set
    End Property
    Public Property SupplierGroupID() As String
        Get
            Return _SupplierGroupID
        End Get
        Set(ByVal Value As String)
            _SupplierGroupID = Value
        End Set
    End Property
    Public Property SpName() As String
        Get
            Return _SpName
        End Get
        Set(ByVal Value As String)
            _SpName = Value
        End Set
    End Property
    Public Property InterestType() As String
        Get
            Return _InterestType
        End Get
        Set(ByVal Value As String)
            _InterestType = Value
        End Set
    End Property
    Public Property InstallmentScheme() As String
        Get
            Return _InstallmentScheme
        End Get
        Set(ByVal Value As String)
            _InstallmentScheme = Value
        End Set
    End Property
    Public Property Page() As String
        Get
            Return _Page
        End Get
        Set(ByVal Value As String)
            _Page = Value
        End Set
    End Property
    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property
    Public Property CustomerID() As String
        Get
            Return _CustomerID
        End Get
        Set(ByVal Value As String)
            _CustomerID = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return _CustomerName
        End Get
        Set(ByVal Value As String)
            _CustomerName = Value
        End Set
    End Property
    Public Property ProductOfferingID() As String
        Get
            Return _ProductOfferingID
        End Get
        Set(ByVal Value As String)
            _ProductOfferingID = Value
        End Set
    End Property
    Public Property ProductOfferingDescr() As String
        Get
            Return _ProductOfferingDescr
        End Get
        Set(ByVal Value As String)
            _ProductOfferingDescr = Value
        End Set
    End Property
    Public Property AOID() As String
        Get
            Return _AOID
        End Get
        Set(ByVal Value As String)
            _AOID = Value
        End Set
    End Property
    Public Property AOName() As String
        Get
            Return _AOName
        End Get
        Set(ByVal Value As String)
            _AOName = Value
        End Set
    End Property
    Public Property InsAssetInsuredBy() As String
        Get
            Return _InsAssetInsuredBy
        End Get
        Set(ByVal Value As String)
            _InsAssetInsuredBy = Value
        End Set
    End Property
    Public Property InsAssetInsuredByName() As String
        Get
            Return _InsAssetInsuredByName
        End Get
        Set(ByVal Value As String)
            _InsAssetInsuredByName = Value
        End Set
    End Property
    Public Property InsAssetPaidBy() As String
        Get
            Return _InsAssetPaidBy
        End Get
        Set(ByVal Value As String)
            _InsAssetPaidBy = Value
        End Set
    End Property
    Public Property InsAssetPaidByName() As String
        Get
            Return _InsAssetPaidByName
        End Get
        Set(ByVal Value As String)
            _InsAssetPaidByName = Value
        End Set
    End Property
End Class
