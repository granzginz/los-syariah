

<Serializable()> _
Public Class AssetData : Inherits Common
    Private _listdata As DataTable
    Private _totalrecords As Int64
    Private _Where As String
    Private _Table As String
    Private _AssetID As String
    Private _AssetDocID As String
    Private _SupplierID As String
    Private _SupplierName As String
    Private _Serial1 As String
    Private _Serial2 As String
    Private _Output As String
    Private _Input As String
    Private _AOID As String
    Private _AssetCode As String
    Private _AssetUsage As String
    Private _CAID As String
    Private _DP As Decimal
    Private _OTR As Decimal
    Private _UsedNew As String
    Private _ManufacturingYear As Integer
    Private _OldOwnerAsset As String
    Private _TaxDate As String
    Private _InsuredBy As String
    Private _PaidBy As String
    Private _Notes As String
    Private _SalesmanID As String
    Private _SurveyorID As String
    Private _SalesSupervisorID As String
    Private _SupplierAdminID As String
    Private _IsIncentiveSupplier As String
    Private _DateEntryAssetData As Date
    Private _CustomerID As String
    Private _DataAttribute As DataTable
    Private _DataAssetdoc As DataTable
    Private _ProducOfferingID As String
    Private _ProducID As String
    Private _SpName As String
    Private _Message As String
    Private _Flag As String
    Private _applicationID As String
    Private _NewAmountFee As Double
    Private _UsedAmountFee As Double
    Public Property NewAmountFee() As Double
        Get
            Return _NewAmountFee
        End Get
        Set(ByVal Value As Double)
            _NewAmountFee = Value
        End Set
    End Property
    Public Property UsedAmountFee() As Double
        Get
            Return _UsedAmountFee
        End Get
        Set(ByVal Value As Double)
            _UsedAmountFee = Value
        End Set
    End Property

    Public Property Flag() As String
        Get
            Return _Flag
        End Get
        Set(ByVal Value As String)
            _Flag = Value
        End Set
    End Property
    Public Property Message() As String
        Get
            Return _Message
        End Get
        Set(ByVal Value As String)
            _Message = Value
        End Set
    End Property
    Public Property SpName() As String
        Get
            Return _SpName
        End Get
        Set(ByVal Value As String)
            _SpName = Value
        End Set
    End Property
    Public Property DataAttribute() As DataTable
        Get
            Return _DataAttribute
        End Get
        Set(ByVal Value As DataTable)
            _DataAttribute = Value
        End Set
    End Property
    Public Property DataAssetdoc() As DataTable
        Get
            Return _DataAssetdoc
        End Get
        Set(ByVal Value As DataTable)
            _DataAssetdoc = Value
        End Set
    End Property
    Public Property CustomerID() As String
        Get
            Return _CustomerID
        End Get
        Set(ByVal Value As String)
            _CustomerID = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property Where() As String
        Get
            Return _Where
        End Get
        Set(ByVal Value As String)
            _Where = Value
        End Set
    End Property
    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property
    Public Property AssetID() As String
        Get
            Return _AssetID
        End Get
        Set(ByVal Value As String)
            _AssetID = Value
        End Set
    End Property
    Public Property AssetDocID() As String
        Get
            Return _AssetDocID
        End Get
        Set(ByVal Value As String)
            _AssetDocID = Value
        End Set
    End Property
    Public Property SupplierID() As String
        Get
            Return _SupplierID
        End Get
        Set(ByVal Value As String)
            _SupplierID = Value
        End Set
    End Property

    Public Property SupplierName() As String
        Get
            Return _SupplierName
        End Get
        Set(ByVal Value As String)
            _SupplierName = Value
        End Set
    End Property
    Public Property Serial1() As String
        Get
            Return _Serial1
        End Get
        Set(ByVal Value As String)
            _Serial1 = Value
        End Set
    End Property
    Public Property Serial2() As String
        Get
            Return _Serial2
        End Get
        Set(ByVal Value As String)
            _Serial2 = Value
        End Set
    End Property
    Public Property Output() As String
        Get
            Return _Output
        End Get
        Set(ByVal Value As String)
            _Output = Value
        End Set
    End Property
    Public Property Input() As String
        Get
            Return _Input
        End Get
        Set(ByVal Value As String)
            _Input = Value
        End Set
    End Property
    Public Property AOID() As String
        Get
            Return _AOID
        End Get
        Set(ByVal Value As String)
            _AOID = Value
        End Set
    End Property
    Public Property AssetCode() As String
        Get
            Return _AssetCode
        End Get
        Set(ByVal Value As String)
            _AssetCode = Value
        End Set
    End Property
    Public Property AssetUsage() As String
        Get
            Return _AssetUsage
        End Get
        Set(ByVal Value As String)
            _AssetUsage = Value
        End Set
    End Property
    Public Property CAID() As String
        Get
            Return _CAID
        End Get
        Set(ByVal Value As String)
            _CAID = Value
        End Set
    End Property
    Public Property DP() As Decimal
        Get
            Return _DP
        End Get
        Set(ByVal Value As Decimal)
            _DP = Value
        End Set
    End Property
    Public Property OTR() As Decimal
        Get
            Return _OTR
        End Get
        Set(ByVal Value As Decimal)
            _OTR = Value
        End Set
    End Property
    Public Property UsedNew() As String
        Get
            Return _UsedNew
        End Get
        Set(ByVal Value As String)
            _UsedNew = Value
        End Set
    End Property
    Public Property ManufacturingYear() As Integer
        Get
            Return _ManufacturingYear
        End Get
        Set(ByVal Value As Integer)
            _ManufacturingYear = Value
        End Set
    End Property
    Public Property OldOwnerAsset() As String
        Get
            Return _OldOwnerAsset
        End Get
        Set(ByVal Value As String)
            _OldOwnerAsset = Value
        End Set
    End Property
    Public Property TaxDate() As String
        Get
            Return _TaxDate
        End Get
        Set(ByVal Value As String)
            _TaxDate = Value
        End Set
    End Property
    Public Property InsuredBy() As String
        Get
            Return _InsuredBy
        End Get
        Set(ByVal Value As String)
            _InsuredBy = Value
        End Set
    End Property
    Public Property PaidBy() As String
        Get
            Return _PaidBy
        End Get
        Set(ByVal Value As String)
            _PaidBy = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property
    Public Property SalesmanID() As String
        Get
            Return _SalesmanID
        End Get
        Set(ByVal Value As String)
            _SalesmanID = Value
        End Set
    End Property
    Public Property SurveyorID() As String
        Get
            Return _SurveyorID
        End Get
        Set(ByVal Value As String)
            _SurveyorID = Value
        End Set
    End Property
    Public Property SalesSupervisorID() As String
        Get
            Return _SalesSupervisorID
        End Get
        Set(ByVal Value As String)
            _SalesSupervisorID = Value
        End Set
    End Property
    Public Property SupplierAdminID() As String
        Get
            Return _SupplierAdminID
        End Get
        Set(ByVal Value As String)
            _SupplierAdminID = Value
        End Set
    End Property
    Public Property IsIncentiveSupplier() As String
        Get
            Return _IsIncentiveSupplier
        End Get
        Set(ByVal Value As String)
            _IsIncentiveSupplier = Value
        End Set
    End Property
    Public Property DateEntryAssetData() As Date
        Get
            Return _DateEntryAssetData
        End Get
        Set(ByVal Value As Date)
            _DateEntryAssetData = Value
        End Set
    End Property

    Public Property ProductOfferingID() As String
        Get
            Return _ProducOfferingID
        End Get
        Set(ByVal Value As String)
            _ProducOfferingID = Value
        End Set
    End Property
    Public Property ProductID() As String
        Get
            Return _ProducID
        End Get
        Set(ByVal Value As String)
            _ProducID = Value
        End Set
    End Property
    Public Property ApplicationId() As String
        Get
            Return _applicationID
        End Get
        Set(ByVal Value As String)
            _applicationID = Value
        End Set
    End Property

    Private _Pemakai As String
    Public Property Pemakai() As String
        Get
            Return _Pemakai
        End Get
        Set(ByVal Value As String)
            _Pemakai = Value
        End Set
    End Property
    Private _Lokasi As String
    Public Property Lokasi() As String
        Get
            Return _Lokasi
        End Get
        Set(ByVal Value As String)
            _Lokasi = Value
        End Set
    End Property
    Private _HargaLaku As Double
    Public Property HargaLaku() As Double
        Get
            Return _HargaLaku
        End Get
        Set(ByVal Value As Double)
            _HargaLaku = Value
        End Set
    End Property
    Private _SR1 As String
    Public Property SR1() As String
        Get
            Return _SR1
        End Get
        Set(ByVal Value As String)
            _SR1 = Value
        End Set
    End Property
    Private _SR2 As String
    Public Property SR2() As String
        Get
            Return _SR2
        End Get
        Set(ByVal Value As String)
            _SR2 = Value
        End Set
    End Property
    Private _HargaSR1 As Double
    Public Property HargaSR1() As Double
        Get
            Return _HargaSR1
        End Get
        Set(ByVal Value As Double)
            _HargaSR1 = Value
        End Set
    End Property
    Private _HargaSR2 As Double
    Public Property HargaSR2() As Double
        Get
            Return _HargaSR2
        End Get
        Set(ByVal Value As Double)
            _HargaSR2 = Value
        End Set
    End Property

    Public Property SplitPembayaran As Boolean
    Public Property UangMukaBayarDi As String
    Public Property PencairanKe As String
    Public Property SupplierIDKaroseri As String
    Public Property HargaKaroseri As Double
    Public Property PHJMB As Double
    Public Property StatusKendaraan As String
    Public Property NamaBPKBSama As Boolean

    Public Property isKoreksi As Boolean
    Public Property alasanKoreksi As String
    Public Property BPKBPengganti As Boolean
    Public Property AlasanSTNKExpired As String
    Public Property OwnerAssetCompany As Boolean
    Public Property CustomerType As String
    Public Property Origination As String
    Public Property KondisiAsset As String
    Public Property GradeCode As String
    Public Property GradeValue As Double
    Public Property IsIzinTrayek As Boolean

    Public Property TrayekAtasNama As String
    Public Property Jurusan As String
    Public Property BukuKeur As String

    Public Property PPN As Double
    Public Property BBN As Double
    Public Property RVEstimateOL As Double
    Public Property RVInterestOL As Double
    Public Property DPKaroseriAmount As Double

End Class
