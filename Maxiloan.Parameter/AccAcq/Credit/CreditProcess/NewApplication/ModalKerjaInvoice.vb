﻿<Serializable()>
Public Class ModalKerjaInvoice : Inherits Maxiloan.Parameter.Common
    Public Property InvoiceSeqNo As Integer
    Public Property ApplicationID As String
    Public Property InvoiceNo As String
    Public Property InvoiceDate As DateTime
    Public Property InvoiceDueDate As DateTime
    Public Property InvoiceAmount As Decimal
    Public Property Retensi As Decimal
    Public Property RetensiAmount As Decimal
    Public Property InterestAmount As Decimal
    Public Property TotalPembiayaan As Decimal
    Public Property JangkaWaktu As Integer
    Public Property InvoiceTo As String
    Public Property AccountNameTo As String
    Public Property AccountNoTo As String
    Public Property BankNameTo As String
    Public Property BankBranchTo As String

    Public Property DtmUpd As String
    Public Property UsrUpd As String

    Public Property ApprovalSchemeID As String
    Public Property Listdata As DataTable
    Public Property Totalrecords As Int64
    Public Property ApprovedBy As String
    Public Property ApprovedDate As String
    Public Property RequestedBy As String
    Public Property RequestedDate As String
    Public Property PaidStatus As String
    Public Property Status As String
    Public Property ApprovalNo As String
    Public Property ApprovalResult As String
    Public Property notes As String
    Public Property IsReRequest As Boolean
    Public Property IsFinal As Boolean
    Public Property UserApproval As String
    Public Property NextPersonApproval As String
    Public Property Where As String
    Public Property SecurityCode As String
    Public Property UserSecurityCode As String
    Public Property IsEverRejected As Boolean
	Public Property AgreementNo As String

	Public Property Interestpaid As Double
	Public Property DrawDownDate As Date
	Public Property EffectiveRate As Decimal
	Public Property LastInterestPaymentDate As Date
	Public Property DueDateInvoice As Date
	Public Property InterestAmountInvoice As Decimal

	Public Property OutstandingPrincipal As Double
    Public Property LcInterest As Double
    Public Property InsSeqno As Integer

    Public Property DueDate As Date
    Public Property InvoiceNoBatch As String
    Public Property PiutangDibiayai As Double
    Public Property RateDenda As Double
    Private _valuedate As Date

    Public Property TotalAlokasi As String

    Public Property ValueDate() As Date
        Get
            Return (CType(_valuedate, Date))
        End Get
        Set(ByVal Value As Date)
            _valuedate = Value
        End Set
    End Property
End Class
