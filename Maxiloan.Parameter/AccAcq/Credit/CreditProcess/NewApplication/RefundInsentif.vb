﻿<Serializable()> _
Public Class RefundInsentif : Inherits Parameter.Common
    Public Property SupplierID As String
    Public Property ApplicationID As String
    Public Property ListData As DataSet
    Public Property ListDataTable As DataTable
    Public Property RefundPremi As Double
    Public Property RefundInterest As Double
    Public Property DistribusiNilaiInsentif As DataTable
    Public Property NilaiInsentif As Decimal
    Public Property IsInsentifPercent As Boolean
    Public Property SupplierEmployeePosition As String
    Public Property SPName As String
    Public Property SupplierEmployeeID As String
    Public Property EmployeeName As String
    Public Property pph As String
    Public Property tarifPajak As Decimal
    Public Property nilaiPajak As Decimal
    Public Property insentifNet As Decimal
    Public Property nilaiAlokasi As Decimal
    Public Property nilaiAlokasiPersen As Decimal
    Public Property persentase As Decimal
    Public Property TransID As String
    Public Property PenggunaanID As String
    Public Property isInternal As String
    Public Property TransIDDescription As String
    Public Property IsDiscountPPH As String
End Class
