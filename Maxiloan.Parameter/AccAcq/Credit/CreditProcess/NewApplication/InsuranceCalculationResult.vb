
<Serializable()> _
Public Class InsuranceCalculationResult
    Inherits Maxiloan.Parameter.InsuranceCalculation    
    Private _TotalSRCPremiumToCust As Double
    Private _TotalTPLPremiumToCust As Double
    Private _TotalFloodPremiumToCust As Double
    Private _TotalRiotPremiumToCust As Double
    Private _TotalPAPremiumToCust As Double
    Private _TotalLoadingFeeToCust As Double
    Private _TotalStdPremium As Double
    Private _TotalPremiumToCustBeforeDiscount As Double
    Private _TotalPremiumToCustAfterDiscount As Double
    Private _PremiumBaseRefundShowroom As Double
    Private _MainPremiumToCust As Double
    Private _AmountToCust As Double
    Private _PremiumToCust As Double
    Private _FloodPremiumToCust As Double
    Private _PremiumToCustAmount As Double
    Private _ListData As DataTable
    Private _InterestType As String
    Private _InstallmentScheme As String
    Private _refundtosupplier As Double
    Private _TotalPaidAmountByCust As Double
    Private _TotalDiscountToCust As Double
    Private _AdminFee As Double
    Private _PerluasanPA As Boolean


    Public Property RefundToSupplier() As Double
        Get
            Return _refundtosupplier
        End Get
        Set(ByVal Value As Double)
            _refundtosupplier = Value
        End Set
    End Property

    Public Property InterestType() As String
        Get
            Return _InterestType
        End Get
        Set(ByVal Value As String)
            _InterestType = Value
        End Set
    End Property

    Public Property InstallmentScheme() As String
        Get
            Return _InstallmentScheme
        End Get
        Set(ByVal Value As String)
            _InstallmentScheme = Value
        End Set
    End Property

    Public Property PremiumToCustAmount() As Double
        Get
            Return _PremiumToCustAmount
        End Get
        Set(ByVal Value As Double)
            _PremiumToCustAmount = Value
        End Set
    End Property


    Public Property FloodPremiumToCust() As Double
        Get
            Return _FloodPremiumToCust
        End Get
        Set(ByVal Value As Double)
            _FloodPremiumToCust = Value
        End Set
    End Property

    Public Property PremiumToCust() As Double
        Get
            Return _PremiumToCust
        End Get
        Set(ByVal Value As Double)
            _PremiumToCust = Value
        End Set
    End Property

    Public Property AmountToCust() As Double
        Get
            Return _AmountToCust
        End Get
        Set(ByVal Value As Double)
            _AmountToCust = Value
        End Set
    End Property

    Public Property MainPremiumToCust() As Double
        Get
            Return _MainPremiumToCust
        End Get
        Set(ByVal Value As Double)
            _MainPremiumToCust = Value
        End Set
    End Property

    Public Property TotalSRCPremiumToCust() As Double
        Get
            Return _TotalSRCPremiumToCust
        End Get
        Set(ByVal Value As Double)
            _TotalSRCPremiumToCust = Value
        End Set
    End Property

    Public Property TotalTPLPremiumToCust() As Double
        Get
            Return _TotalTPLPremiumToCust
        End Get
        Set(ByVal Value As Double)
            _TotalTPLPremiumToCust = Value
        End Set
    End Property

    Public Property TotalFloodPremiumToCust() As Double
        Get
            Return _TotalFloodPremiumToCust
        End Get
        Set(ByVal Value As Double)
            _TotalFloodPremiumToCust = Value
        End Set
    End Property

    Public Property TotalRiotPremiumToCust() As Double
        Get
            Return _TotalRiotPremiumToCust
        End Get
        Set(ByVal Value As Double)
            _TotalRiotPremiumToCust = Value
        End Set
    End Property

    Public Property TotalPAPremiumToCust() As Double
        Get
            Return _TotalPAPremiumToCust
        End Get
        Set(ByVal Value As Double)
            _TotalPAPremiumToCust = Value
        End Set
    End Property

    Public Property TotalLoadingFeeToCust() As Double
        Get
            Return _TotalLoadingFeeToCust
        End Get
        Set(ByVal Value As Double)
            _TotalLoadingFeeToCust = Value
        End Set
    End Property

    Public Property TotalStdPremium() As Double
        Get
            Return _TotalStdPremium
        End Get
        Set(ByVal Value As Double)
            _TotalStdPremium = Value
        End Set
    End Property

    Public Property TotalPremiumToCustBeforeDiscount() As Double
        Get
            Return _TotalPremiumToCustBeforeDiscount
        End Get
        Set(ByVal Value As Double)
            _TotalPremiumToCustBeforeDiscount = Value
        End Set
    End Property

    Public Property TotalPremiumToCustAfterDiscount() As Double
        Get
            Return _TotalPremiumToCustAfterDiscount
        End Get
        Set(ByVal Value As Double)
            _TotalPremiumToCustAfterDiscount = Value
        End Set
    End Property

    Public Property PremiumBaseRefundShowroom() As Double
        Get
            Return _PremiumBaseRefundShowroom
        End Get
        Set(ByVal Value As Double)
            _PremiumBaseRefundShowroom = Value
        End Set
    End Property

    Public Property TotalPaidAmountByCust() As Double
        Get
            Return _TotalPaidAmountByCust
        End Get
        Set(ByVal Value As Double)
            _TotalPaidAmountByCust = Value
        End Set
    End Property

    Public Property TotalDiscountToCust() As Double
        Get
            Return _TotalDiscountToCust
        End Get
        Set(ByVal Value As Double)
            _TotalDiscountToCust = Value
        End Set
    End Property

    Public Property AdminFee() As Double
        Get
            Return _AdminFee
        End Get
        Set(ByVal Value As Double)
            _AdminFee = Value
        End Set
    End Property
    Public Property PerluasanPA() As Boolean
        Get
            Return _PerluasanPA
        End Get
        Set(value As Boolean)
            _PerluasanPA = value
        End Set
    End Property
    Public Property JenisAksesoris As String
    Public Property NilaiAksesoris As Double
    Public Property isKoreksi As Boolean
    Public Property alasanKoreksi As String
    Public Property sellingRate As Decimal
    Public Property BiayaPolis As Double
    Public Property DataTablePaid As DataTable
    Public Property BiayaMaterai As Double

    Public Property PremiCP As Double
    Public Property BiayaPolisCP As Double
    Public Property RateCP As Double
    Public Property InsComBranchIDCP As String
    Public Property CreditProtection As Boolean

    Public Property PremiAJK As Double
    Public Property BiayaPolisAJK As Double
    Public Property RateAJK As Double
    Public Property InsComBranchIDAJK As String
    Public Property JaminanKredit As Boolean

End Class
