

<Serializable()>
Public Class Application : Inherits Common
    Private _listdata As DataTable
    Private _totalrecords As Int64
    Private _Type As String
    Private _AgreementNo As String
    Private _AgreementDate As String
    Private _DefaultStatus As String
    Private _ContractStatus As String
    Private _ApplicationID As String
    Private _CustomerId As String
    Private _ProductID As String
    Private _ProductOffID As String
    Private _RefNotes As String
    Private _Notes As String
    Private _IsRecourse As String
    Private _IsNST As String
    Private _NumOfAssetUnit As Int16
    Private _InterestType As String
    Private _InstallmentScheme As String
    Private _GuarantorID As String
    Private _SpouseID As String
    Private _WayOfPayment As String
    Private _SurveyDate As String
    Private _ApplicationSource As String
    Private _PercentagePenalty As String
    Private _AdminFee As String
    Private _AdditionalAdminFee As String
    Private _FiduciaFee As String
    Private _StatusFiduciaFee As String
    Private _ApplicationStep As String
    Private _ProvisionFee As String
    Private _NotaryFee As String
    Private _SurveyFee As String
    Private _BBNFee As String
    Private _OtherFee As String
    Private _CrossDefaultApplicationId As String
    Private _FinanceType As String
    Private _Error As String
    Private _DataCD As DataTable
    Private _DataTC As DataTable
    Private _DataTC2 As DataTable
    Private _WhereCond2 As String
    Private _AddEdit As String
    Private _EffectiveDate As Date
    Private _AgrDate As Date
    Private _SpName As String
    Private _StepUpStepDownType As String
    Private _FundingBatchNo As String
    Private _FundingContractNo As String
    Private _FundingCoyId As String
    Private _FundingPledgeStatus As String
    Private _ProspectAppID As String
    Private _AssetCode As String
    Private _OTR As Double
    Private _DP As Double
    Private _InstallmentAmount As Double
    Private _Tenor As Integer
    Private _AssetDesc As String
    Private _ExistingPolicy As String
    Private _FirstInstallment As String
    Private _Name As String
    Private _Address As String
    Private _Rt As String
    Private _Rw As String
    Private _Kelurahan As String
    Private _Kecamatan As String
    Private _City As String
    Private _ZipCode As String
    Private _AreaPhone1 As String
    Private _Phone1 As String
    Private _CustomerType As String
    Private _IDType As String
    Private _IDNumber As String
    Private _Gender As String
    Private _BirthPlace As String
    Private _BirthDate As DateTime
    Private _HomeStatus As String
    Private _MobilePhone As String
    Private _Email As String
    Private _ProfessionID As String
    Private _IndustryTypeID As String
    Private _IsAdminFeeCredit As Boolean
    Private _isRO As Boolean
    Private _Education As String
    Private _StaySinceYear As Integer
    Private _StaySinceMonth As Integer
    Private _NumOfDependence As Integer
    Private _KodeIndustri As String
    Private _KodeIndustriDetail As String
    Private _NatureOfBusinessId As String
    Private _OccupationId As String
    Private _MaritalStatus As String
    Private _EmploymentSinceYear As Integer
    Private _EmploymentSinceMonth As Integer
    Private _ActivityDateStart As String
    Private _ActivityDateEnd As String
    Private _ActivityUser As String
    Private _ActivitySeqNo As String
    Private _ActivityType As String
    Private _NamaIndustri As String
    Private _LicensePlate As String
    Private _HandlingFee As String
    Private _UnitBisnis As String
    Private _ReferalFee As String
    Private _Lokasi As String
    Private _PorsiInstitusi As String
    Private _NPP As String
    Private _PorsiPemberiReferal As String
    Private _NPWP As String
    Private _UsrUpdt As String
    Private _DtmUpdt As Date
    Private _isAdmCapitalized As Boolean

    Public Property isUpAdmCapitalized() As Boolean
    Public Property isAdmCapitalized() As Boolean
        Get
            Return _isAdmCapitalized
        End Get
        Set(value As Boolean)
            _isAdmCapitalized = value
        End Set
    End Property
    Public Property isRO() As Boolean
        Get
            Return _isRO
        End Get
        Set(ByVal Value As Boolean)
            _isRO = Value
        End Set
    End Property

    Public Property Name() As String
        Get
            Return _Name
        End Get
        Set(ByVal Value As String)
            _Name = Value
        End Set
    End Property
    Public Property Address() As String
        Get
            Return _Address
        End Get
        Set(ByVal Value As String)
            _Address = Value
        End Set
    End Property
    Public Property Rt() As String
        Get
            Return _Rt
        End Get
        Set(ByVal Value As String)
            _Rt = Value
        End Set
    End Property
    Public Property Rw() As String
        Get
            Return _Rw
        End Get
        Set(ByVal Value As String)
            _Rw = Value
        End Set
    End Property
    Public Property Kelurahan() As String
        Get
            Return _Kelurahan
        End Get
        Set(ByVal Value As String)
            _Kelurahan = Value
        End Set
    End Property
    Public Property Kecamatan() As String
        Get
            Return _Kecamatan
        End Get
        Set(ByVal Value As String)
            _Kecamatan = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return _City
        End Get
        Set(ByVal Value As String)
            _City = Value
        End Set
    End Property
    Public Property ZipCode() As String
        Get
            Return _ZipCode
        End Get
        Set(ByVal Value As String)
            _ZipCode = Value
        End Set
    End Property
    Public Property AreaPhone1() As String
        Get
            Return _AreaPhone1
        End Get
        Set(ByVal Value As String)
            _AreaPhone1 = Value
        End Set
    End Property
    Public Property Phone1() As String
        Get
            Return _Phone1
        End Get
        Set(ByVal Value As String)
            _Phone1 = Value
        End Set
    End Property
    Public Property CustomerType() As String
        Get
            Return _CustomerType
        End Get
        Set(ByVal Value As String)
            _CustomerType = Value
        End Set
    End Property
    Public Property IDType() As String
        Get
            Return _IDType
        End Get
        Set(ByVal Value As String)
            _IDType = Value
        End Set
    End Property
    Public Property IDNumber() As String
        Get
            Return _IDNumber
        End Get
        Set(ByVal Value As String)
            _IDNumber = Value
        End Set
    End Property
    Public Property Gender() As String
        Get
            Return _Gender
        End Get
        Set(ByVal Value As String)
            _Gender = Value
        End Set
    End Property
    Public Property BirthPlace() As String
        Get
            Return _BirthPlace
        End Get
        Set(ByVal Value As String)
            _BirthPlace = Value
        End Set
    End Property
    Public Property BirthDate() As Date
        Get
            Return _BirthDate
        End Get
        Set(ByVal Value As Date)
            _BirthDate = Value
        End Set
    End Property
    Public Property HomeStatus() As String
        Get
            Return _HomeStatus
        End Get
        Set(ByVal Value As String)
            _HomeStatus = Value
        End Set
    End Property
    Public Property MobilePhone() As String
        Get
            Return _MobilePhone
        End Get
        Set(ByVal Value As String)
            _MobilePhone = Value
        End Set
    End Property
    Public Property Email() As String
        Get
            Return _Email
        End Get
        Set(ByVal Value As String)
            _Email = Value
        End Set
    End Property
    Public Property ProfessionID() As String
        Get
            Return _ProfessionID
        End Get
        Set(ByVal Value As String)
            _ProfessionID = Value
        End Set
    End Property
    Public Property IndustryTypeID() As String
        Get
            Return _IndustryTypeID
        End Get
        Set(ByVal Value As String)
            _IndustryTypeID = Value
        End Set
    End Property
    Public Property AssetDesc() As String
        Get
            Return _AssetDesc
        End Get
        Set(ByVal Value As String)
            _AssetDesc = Value
        End Set
    End Property
    Public Property Tenor() As Integer
        Get
            Return _Tenor
        End Get
        Set(ByVal Value As Integer)
            _Tenor = Value
        End Set
    End Property
    Public Property InstallmentAmount() As Double
        Get
            Return _InstallmentAmount
        End Get
        Set(ByVal Value As Double)
            _InstallmentAmount = Value
        End Set
    End Property
    Public Property DP() As Double
        Get
            Return _DP
        End Get
        Set(ByVal Value As Double)
            _DP = Value
        End Set
    End Property
    Public Property OTR() As Double
        Get
            Return _OTR
        End Get
        Set(ByVal Value As Double)
            _OTR = Value
        End Set
    End Property
    Public Property ExistingPolicy() As String
        Get
            Return _ExistingPolicy
        End Get
        Set(ByVal Value As String)
            _ExistingPolicy = Value
        End Set
    End Property
    Public Property FirstInstallment() As String
        Get
            Return _FirstInstallment
        End Get
        Set(ByVal Value As String)
            _FirstInstallment = Value
        End Set
    End Property
    Public Property AssetCode() As String
        Get
            Return _AssetCode
        End Get
        Set(ByVal Value As String)
            _AssetCode = Value
        End Set
    End Property
    Public Property ProspectAppID() As String
        Get
            Return _ProspectAppID
        End Get
        Set(ByVal Value As String)
            _ProspectAppID = Value
        End Set
    End Property
    Public Property StepUpStepDownType() As String
        Get
            Return _StepUpStepDownType
        End Get
        Set(ByVal Value As String)
            _StepUpStepDownType = Value
        End Set
    End Property
    Public Property SpName() As String
        Get
            Return _SpName
        End Get
        Set(ByVal Value As String)
            _SpName = Value
        End Set
    End Property
    Public Property AgrDate() As Date
        Get
            Return _AgrDate
        End Get
        Set(ByVal Value As Date)
            _AgrDate = Value
        End Set
    End Property
    Public Property EffectiveDate() As Date
        Get
            Return _EffectiveDate
        End Get
        Set(ByVal Value As Date)
            _EffectiveDate = Value
        End Set
    End Property
    Public Property AddEdit() As String
        Get
            Return _AddEdit
        End Get
        Set(ByVal Value As String)
            _AddEdit = Value
        End Set
    End Property
    Public Property WhereCond2() As String
        Get
            Return _WhereCond2
        End Get
        Set(ByVal Value As String)
            _WhereCond2 = Value
        End Set
    End Property
    Public Property DataCD() As DataTable
        Get
            Return _DataCD
        End Get
        Set(ByVal Value As DataTable)
            _DataCD = Value
        End Set
    End Property
    Public Property DataTC() As DataTable
        Get
            Return _DataTC
        End Get
        Set(ByVal Value As DataTable)
            _DataTC = Value
        End Set
    End Property
    Public Property DataTC2() As DataTable
        Get
            Return _DataTC2
        End Get
        Set(ByVal Value As DataTable)
            _DataTC2 = Value
        End Set
    End Property
    Public Property Err() As String
        Get
            Return _Error
        End Get
        Set(ByVal Value As String)
            _Error = Value
        End Set
    End Property
    Public Property CustomerId() As String
        Get
            Return _CustomerId
        End Get
        Set(ByVal Value As String)
            _CustomerId = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property ProductID() As String
        Get
            Return _ProductID
        End Get
        Set(ByVal Value As String)
            _ProductID = Value
        End Set
    End Property
    Public Property Type() As String
        Get
            Return _Type
        End Get
        Set(ByVal Value As String)
            _Type = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return _AgreementNo
        End Get
        Set(ByVal Value As String)
            _AgreementNo = Value
        End Set
    End Property
    Public Property AgreementDate() As String
        Get
            Return _AgreementDate
        End Get
        Set(ByVal Value As String)
            _AgreementDate = Value
        End Set
    End Property
    Public Property DefaultStatus() As String
        Get
            Return _DefaultStatus
        End Get
        Set(ByVal Value As String)
            _DefaultStatus = Value
        End Set
    End Property
    Public Property ContractStatus() As String
        Get
            Return _ContractStatus
        End Get
        Set(ByVal Value As String)
            _ContractStatus = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property
    Public Property RefNotes() As String
        Get
            Return _RefNotes
        End Get
        Set(ByVal Value As String)
            _RefNotes = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property
    Public Property ProductOffID() As String
        Get
            Return _ProductOffID
        End Get
        Set(ByVal Value As String)
            _ProductOffID = Value
        End Set
    End Property
    Private _ProductOffIDDesc As String
    Public Property ProductOffIDDesc() As String
        Get
            Return _ProductOffIDDesc
        End Get
        Set(ByVal Value As String)
            _ProductOffIDDesc = Value
        End Set
    End Property

    Public Property IsRecourse() As String
        Get
            Return _IsRecourse
        End Get
        Set(ByVal Value As String)
            _IsRecourse = Value
        End Set
    End Property
    Public Property IsNST() As String
        Get
            Return _IsNST
        End Get
        Set(ByVal Value As String)
            _IsNST = Value
        End Set
    End Property
    Public Property NumOfAssetUnit() As Int16
        Get
            Return _NumOfAssetUnit
        End Get
        Set(ByVal Value As Int16)
            _NumOfAssetUnit = Value
        End Set
    End Property
    Public Property InterestType() As String
        Get
            Return _InterestType
        End Get
        Set(ByVal Value As String)
            _InterestType = Value
        End Set
    End Property
    Public Property InstallmentScheme() As String
        Get
            Return _InstallmentScheme
        End Get
        Set(ByVal Value As String)
            _InstallmentScheme = Value
        End Set
    End Property
    Public Property GuarantorID() As String
        Get
            Return _GuarantorID
        End Get
        Set(ByVal Value As String)
            _GuarantorID = Value
        End Set
    End Property
    Public Property SpouseID() As String
        Get
            Return _SpouseID
        End Get
        Set(ByVal Value As String)
            _SpouseID = Value
        End Set
    End Property
    Public Property WayOfPayment() As String
        Get
            Return _WayOfPayment
        End Get
        Set(ByVal Value As String)
            _WayOfPayment = Value
        End Set
    End Property
    Public Property SurveyDate() As String
        Get
            Return _SurveyDate
        End Get
        Set(ByVal Value As String)
            _SurveyDate = Value
        End Set
    End Property
    Public Property ApplicationSource() As String
        Get
            Return _ApplicationSource
        End Get
        Set(ByVal Value As String)
            _ApplicationSource = Value
        End Set
    End Property
    Public Property PercentagePenalty() As String
        Get
            Return _PercentagePenalty
        End Get
        Set(ByVal Value As String)
            _PercentagePenalty = Value
        End Set
    End Property
    Public Property AdminFee() As String
        Get
            Return _AdminFee
        End Get
        Set(ByVal Value As String)
            _AdminFee = Value
        End Set
    End Property
    Public Property AdditionalAdminFee() As String
        Get
            Return _AdditionalAdminFee
        End Get
        Set(ByVal Value As String)
            _AdditionalAdminFee = Value
        End Set
    End Property

    Public Property FiduciaFee() As String
        Get
            Return _FiduciaFee
        End Get
        Set(ByVal Value As String)
            _FiduciaFee = Value
        End Set
    End Property


    Public Property StatusFiduciaFee() As String
        Get
            Return _StatusFiduciaFee
        End Get
        Set(ByVal Value As String)
            _StatusFiduciaFee = Value
        End Set
    End Property

    Public Property ApplicationStep() As String
        Get
            Return _ApplicationStep
        End Get
        Set(ByVal Value As String)
            _ApplicationStep = Value
        End Set
    End Property
    Public Property ProvisionFee() As String
        Get
            Return _ProvisionFee
        End Get
        Set(ByVal Value As String)
            _ProvisionFee = Value
        End Set
    End Property
    Public Property NotaryFee() As String
        Get
            Return _NotaryFee
        End Get
        Set(ByVal Value As String)
            _NotaryFee = Value
        End Set
    End Property
    Public Property SurveyFee() As String
        Get
            Return _SurveyFee
        End Get
        Set(ByVal Value As String)
            _SurveyFee = Value
        End Set
    End Property
    Public Property BBNFee() As String
        Get
            Return _BBNFee
        End Get
        Set(ByVal Value As String)
            _BBNFee = Value
        End Set
    End Property
    Public Property OtherFee() As String
        Get
            Return _OtherFee
        End Get
        Set(ByVal Value As String)
            _OtherFee = Value
        End Set
    End Property
    Public Property CrossDefaultApplicationId() As String
        Get
            Return _CrossDefaultApplicationId
        End Get
        Set(ByVal Value As String)
            _CrossDefaultApplicationId = Value
        End Set
    End Property
    Public Property FinanceType() As String
        Get
            Return _FinanceType
        End Get
        Set(ByVal Value As String)
            _FinanceType = Value
        End Set
    End Property
    Public Property FundingBatchNo() As String
        Get
            Return _FundingBatchNo
        End Get
        Set(ByVal Value As String)
            _FundingBatchNo = Value
        End Set
    End Property
    Public Property FundingContractNo() As String
        Get
            Return _FundingContractNo
        End Get
        Set(ByVal Value As String)
            _FundingContractNo = Value
        End Set
    End Property
    Public Property FundingCoyId() As String
        Get
            Return _FundingCoyId
        End Get
        Set(ByVal Value As String)
            _FundingCoyId = Value
        End Set
    End Property
    Public Property FundingPledgeStatus() As String
        Get
            Return _FundingPledgeStatus
        End Get
        Set(ByVal Value As String)
            _FundingPledgeStatus = Value
        End Set
    End Property

    Public Property IsAdminFeeCredit() As Boolean
        Get
            Return _IsAdminFeeCredit
        End Get
        Set(ByVal Value As Boolean)
            _IsAdminFeeCredit = Value
        End Set
    End Property

    Private _IsProvisiCredit As Boolean

    Public Property IsProvisiCredit() As Boolean
        Get
            Return _IsProvisiCredit
        End Get
        Set(ByVal Value As Boolean)
            _IsProvisiCredit = Value
        End Set
    End Property

    Public Property Education() As String
        Get
            Return _Education
        End Get
        Set(ByVal Value As String)
            _Education = Value
        End Set
    End Property

    Public Property StaySinceYear() As Integer
        Get
            Return _StaySinceYear
        End Get
        Set(ByVal Value As Integer)
            _StaySinceYear = Value
        End Set
    End Property

    Public Property StaySinceMonth() As Integer
        Get
            Return _StaySinceMonth
        End Get
        Set(ByVal Value As Integer)
            _StaySinceMonth = Value
        End Set
    End Property

    Public Property NumOfDependence() As Integer
        Get
            Return _NumOfDependence
        End Get
        Set(ByVal Value As Integer)
            _NumOfDependence = Value
        End Set
    End Property

    Public Property KodeIndustri() As String
        Get
            Return _KodeIndustri
        End Get
        Set(ByVal Value As String)
            _KodeIndustri = Value
        End Set
    End Property

    Public Property KodeIndustriDetail() As String
        Get
            Return _KodeIndustriDetail
        End Get
        Set(ByVal Value As String)
            _KodeIndustriDetail = Value
        End Set
    End Property

    Public Property NatureOfBusinessId() As String
        Get
            Return _NatureOfBusinessId
        End Get
        Set(ByVal Value As String)
            _NatureOfBusinessId = Value
        End Set
    End Property

    Public Property OccupationId() As String
        Get
            Return _OccupationId
        End Get
        Set(ByVal Value As String)
            _OccupationId = Value
        End Set
    End Property

    Public Property MaritalStatus() As String
        Get
            Return _MaritalStatus
        End Get
        Set(ByVal Value As String)
            _MaritalStatus = Value
        End Set
    End Property

    Public Property EmploymentSinceYear() As Integer
        Get
            Return _EmploymentSinceYear
        End Get
        Set(ByVal Value As Integer)
            _EmploymentSinceYear = Value
        End Set
    End Property

    Public Property EmploymentSinceMonth() As Integer
        Get
            Return _EmploymentSinceMonth
        End Get
        Set(ByVal Value As Integer)
            _EmploymentSinceMonth = Value
        End Set
    End Property

    Public Property ActivityDateStart() As String
        Get
            Return _ActivityDateStart
        End Get
        Set(ByVal Value As String)
            _ActivityDateStart = Value
        End Set
    End Property

    Public Property ActivityDateEnd() As String
        Get
            Return _ActivityDateEnd
        End Get
        Set(ByVal Value As String)
            _ActivityDateEnd = Value
        End Set
    End Property

    Public Property ActivityUser() As String
        Get
            Return _ActivityUser
        End Get
        Set(ByVal Value As String)
            _ActivityUser = Value
        End Set
    End Property

    Public Property ActivitySeqNo() As String
        Get
            Return _ActivitySeqNo
        End Get
        Set(ByVal Value As String)
            _ActivitySeqNo = Value
        End Set
    End Property

    Public Property ActivityType() As String
        Get
            Return _ActivityType
        End Get
        Set(ByVal Value As String)
            _ActivityType = Value
        End Set
    End Property

    Public Property NamaIndustri() As String
        Get
            Return _NamaIndustri
        End Get
        Set(ByVal Value As String)
            _NamaIndustri = Value
        End Set
    End Property

    Public Property LicensePlate() As String
        Get
            Return _LicensePlate
        End Get
        Set(ByVal Value As String)
            _LicensePlate = Value
        End Set
    End Property
    Public Property HandlingFee() As String
        Get
            Return _HandlingFee
        End Get
        Set(ByVal Value As String)
            _HandlingFee = Value
        End Set
    End Property
    Public Property UnitBisnis() As String
        Get
            Return _UnitBisnis
        End Get
        Set(ByVal Value As String)
            _UnitBisnis = Value
        End Set
    End Property
    Public Property ReferalFee() As String
        Get
            Return _ReferalFee
        End Get
        Set(ByVal Value As String)
            _ReferalFee = Value
        End Set
    End Property
    Public Property Lokasi() As String
        Get
            Return _Lokasi
        End Get
        Set(ByVal Value As String)
            _Lokasi = Value
        End Set
    End Property
    Public Property PorsiInstitusi() As String
        Get
            Return _PorsiInstitusi
        End Get
        Set(ByVal Value As String)
            _PorsiInstitusi = Value
        End Set
    End Property
    Public Property NPP() As String
        Get
            Return _NPP
        End Get
        Set(ByVal Value As String)
            _NPP = Value
        End Set
    End Property
    Public Property PorsiPemberiReferal() As String
        Get
            Return _PorsiPemberiReferal
        End Get
        Set(ByVal Value As String)
            _PorsiPemberiReferal = Value
        End Set
    End Property
    Public Property NPWP() As String
        Get
            Return _NPWP
        End Get
        Set(ByVal Value As String)
            _NPWP = Value
        End Set
    End Property
    Public Property UsrUpd() As String
        Get
            Return _UsrUpdt
        End Get
        Set(ByVal Value As String)
            _UsrUpdt = Value
        End Set
    End Property
    Public Property DtmUpd() As Date
        Get
            Return _DtmUpdt
        End Get
        Set(ByVal Value As Date)
            _DtmUpdt = Value
        End Set
    End Property

    Public Property TglKonfirmasiDealer As Date
    Public Property TglRealisasiTtdKontrak As Date
    Public Property TglJanjiTtdKontrak As Date
    Public Property StatusTVCDesc As String
    Public Property BiayaPeriksaBPKB As Double
    Public Property KodeDATI As String
    Public Property JenisPembiayaan As String
    Public Property PolaTransaksi As String
    Public Property AdminFeeGross As Double
    Public Property Refinancing As Boolean
    Public Property isKoreksi As Boolean
    Public Property alasanKoreksi As String
    Public Property IsFleet As Boolean
    Public Property UseNew As String
    Public Property NoApplicationData As String
    Public Property BiayaPolis As String
    Public Property NoPJJ As String
    Public Property JumlahAplikasi As Integer
    Public Property NamaPenjamin As String
    Public Property PersonalCustomerType As String
    Public Property RefinancingApplicationID As String
    Public Property KegiatanUsaha As String
    Public Property LiniBisnis As String
    Public Property TipeLoan As String
    'Public Property IsCOP As Boolean
    Public Property IsCOP As String
    Public Property HakOpsi As Boolean
    Public Property ApplicationModule As String
    Public Property FactoringPiutangDibiayai As Decimal
    Public Property FactoringRetensi As Decimal
    Public Property FactoringInterest As Decimal
    Public Property FactoringPPN As Decimal
    Public Property FactoringDiscountCharges As Decimal

    Public Property SupplierID As String
    Public Property FactoringPencairanDate As String

    Public Property FactoringPolisAmount As Decimal
    Public Property FactoringPolis As Decimal
    Public Property FactoringPPNAmount As Decimal
    Public Property FactoringPaymentMethod As String
    Public Property FactoringAsuransi As Decimal

    Public Property MaturityDate As String
    Public Property FacilityType As String

    Public Property FacilityNo As String

    Public Property FactoringInsuranceComID As String

    Public Property EffectiveRate As Decimal
    Public Property AOID As String
    Public Property IndustryRisk As String
    Public Property RatioAngsuran As String
    Public Property UsiaPemohon As String
    Public Property KondisiLingkungan As String

    Public Property cbanuitas As Boolean
    Public Property InvoiceSeqNo As Integer
    Public Property Akad As String
End Class
