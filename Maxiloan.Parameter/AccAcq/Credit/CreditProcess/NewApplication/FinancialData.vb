<Serializable()>
Public Class FinancialData : Inherits Maxiloan.Parameter.AccMntBase


    'modify kpr

    Private _PerusahaanAppraisal As String
    Private _Appraisal As String
    Private _Appriser As String
    Private _TangggalSurvey As Date
    Private _jam As String
    Private _LuastanahApplikasi As Integer
    Private _PanjangXLebarTanahApplikasi As String
    Private _LuasTanahAppraisal As Integer
    Private _PanjangXLebarTanah As String
    Private _HargaPerMTanah As String
    Private _NilaiPasarTanah As Decimal
    Private _LikuidasiTanah As Integer
    Private _NilaiLikuidasiTanah As Decimal
    Private _NamaContact As String
    Private _HubunganContact As String
    Private _NoTelp As String
    Private _NoHP As String
    Private _LuasBangunanApplikasi As Integer
    Private _PanjangXLebarBangunanApplikasi As String
    Private _LuasBangunan As Integer
    Private _PanjangXLebarBangunan As String
    Private _Lantai As Integer
    Private _HargaPermBangunan As Decimal
    Private _NilaiLikuidasiBangunan As Decimal

    Public Property NilaiPasarTtl As Decimal
    Public Property NilaiPermTtl As Decimal
    Public Property NilaiLikuidasiTtl As Decimal
    Public Property LuasMTtl As Decimal
    Public Property SumberPerbandingan As String
    Public Property HargaInternetTtl As Decimal
    Public Property SumbardataInternet As String
    Public Property Keterangan As String
    Public Property TotalSurvey As Decimal


    Public Property HargaPermBangunan() As Decimal
        Get
            Return _HargaPermBangunan
        End Get
        Set(ByVal Value As Decimal)
            _HargaPermBangunan = Value
        End Set

    End Property
    Public Property NilaiLikuidasiBangunan() As Decimal
        Get
            Return _NilaiLikuidasiBangunan
        End Get
        Set(ByVal Value As Decimal)
            _NilaiLikuidasiBangunan = Value
        End Set
    End Property
    Public Property Lantai() As Integer
        Get
            Return _Lantai
        End Get
        Set(ByVal Value As Integer)
            _Lantai = Value
        End Set
    End Property
    Public Property PanjangXLebarBangunan() As String
        Get
            Return _PanjangXLebarBangunan
        End Get
        Set(ByVal Value As String)
            _PanjangXLebarBangunan = Value
        End Set
    End Property
    Public Property LuasBangunan() As Integer
        Get
            Return _LuasBangunan
        End Get
        Set(ByVal Value As Integer)
            _LuasBangunan = Value
        End Set
    End Property
    Public Property PanjangXLebarBangunanApplikasi() As String
        Get
            Return _PanjangXLebarBangunanApplikasi
        End Get
        Set(ByVal Value As String)
            _PanjangXLebarBangunanApplikasi = Value
        End Set
    End Property
    Public Property LuasBangunanApplikasi() As String
        Get
            Return _LuasBangunanApplikasi
        End Get
        Set(ByVal Value As String)
            _LuasBangunanApplikasi = Value
        End Set
    End Property
    Public Property NoHP() As String
        Get
            Return _NoHP
        End Get
        Set(ByVal Value As String)
            _NoHP = Value
        End Set
    End Property
    Public Property HubunganContact() As String
        Get
            Return _HubunganContact
        End Get
        Set(ByVal Value As String)
            _HubunganContact = Value
        End Set
    End Property
    Public Property NoTelp() As String
        Get
            Return _NoTelp
        End Get
        Set(ByVal Value As String)
            _NoTelp = Value
        End Set
    End Property
    Public Property NilaiLikuidasiTanah() As Decimal
        Get
            Return _NilaiLikuidasiTanah
        End Get
        Set(ByVal Value As Decimal)
            _NilaiLikuidasiTanah = Value
        End Set
    End Property
    Public Property NamaContact() As String
        Get
            Return _NamaContact
        End Get
        Set(ByVal Value As String)
            _NamaContact = Value
        End Set
    End Property

    Public Property PerusahaanAppraisal() As String
        Get
            Return _PerusahaanAppraisal
        End Get
        Set(ByVal Value As String)
            _PerusahaanAppraisal = Value
        End Set
    End Property
    Public Property Appraisal() As String
        Get
            Return _Appraisal
        End Get
        Set(ByVal Value As String)
            _Appraisal = Value
        End Set
    End Property
    Public Property Appriser() As String
        Get
            Return _Appriser
        End Get
        Set(ByVal Value As String)
            _Appriser = Value
        End Set
    End Property
    Public Property TangggalSurvey() As Date
        Get
            Return _TangggalSurvey
        End Get
        Set(ByVal Value As Date)
            _TangggalSurvey = Value
        End Set
    End Property
    Public Property jam() As String
        Get
            Return _jam
        End Get
        Set(ByVal Value As String)
            _jam = Value
        End Set
    End Property
    Public Property LuastanahApplikasi() As Integer
        Get
            Return _LuastanahApplikasi
        End Get
        Set(ByVal Value As Integer)
            _LuastanahApplikasi = Value
        End Set
    End Property
    Public Property PanjangXLebarTanahApplikasi() As String
        Get
            Return _PanjangXLebarTanahApplikasi
        End Get
        Set(ByVal Value As String)
            _PanjangXLebarTanahApplikasi = Value
        End Set
    End Property
    Public Property LuasTanahAppraisal() As Integer
        Get
            Return _LuasTanahAppraisal
        End Get
        Set(ByVal Value As Integer)
            _LuasTanahAppraisal = Value
        End Set
    End Property
    Public Property PanjangXLebarTanah() As String
        Get
            Return _PanjangXLebarTanah
        End Get
        Set(ByVal Value As String)
            _PanjangXLebarTanah = Value
        End Set
    End Property
    Public Property HargaPerMTanah() As Decimal
        Get
            Return _HargaPerMTanah
        End Get
        Set(ByVal Value As Decimal)
            _HargaPerMTanah = Value
        End Set
    End Property
    Public Property NilaiPasarTanah() As Decimal
        Get
            Return _NilaiPasarTanah
        End Get
        Set(ByVal Value As Decimal)
            _NilaiPasarTanah = Value
        End Set
    End Property
    Public Property LikuidasiTanah() As Integer
        Get
            Return _LikuidasiTanah
        End Get
        Set(ByVal Value As Integer)
            _LikuidasiTanah = Value
        End Set
    End Property

    Private _listdata As DataTable
    Private _totalrecords As Int64
    Private _FlatRate As Double
    Private _InterestTotal As Double
    Private _MydataSet As DataSet
    Private _EffectiveRate As Double
    Private _FirstInstallment As String
    Private _FloatingPeriod As String
    Private _GracePeriod As Integer
    Private _GracePeriodType As String
    Private _InstallmentAmount As Double
    Private _NTF As Double
    Private _NumOfInstallment As Integer
    Private _IsSave As Integer
    Private _Tenor As Integer
    Private _CummulativeStart As Integer
    Private _SupplierRate As Double
    Private _PaymentFrequency As String
    Private _BusDate As DateTime
    Private _DiffRate As Double
    Private _GrossYield As Double
    Private _OutstandingPrincipal As Double
    Private _OutstandingInterest As Double
    Private _Output As String
    Private _Data1 As DataTable
    Private _data2 As DataTable
    Private _data3 As DataTable
    Private _SpName As String
    Private _Principal As Double
    Private _InstallmentScheme As String
    Private _effectivedate As DateTime
    Private _administrationfee As Double
    Private _seqno As Integer
    Private _notes As String
    Private _outstandingtenor As Integer
    Private _partialprepaymentamount As Double
    Private _contractprepaidamount As Double
    Private _outstandingprincipalnew As Double
    Private _outstandinginterestnew As Double
    Private _outstandingprincipalold As Double
    Private _outstandinginterestold As Double
    Private _status As String
    Private _tr_nomor As String
    Private _collectionexpense As Double
    Private _guarantorID As String
    Private _interestyype As String
    Private _duedate As DateTime
    Private _maxseqNo As Integer
    Private _newnuminst As Integer
    Private _LCInstallmentAmountDisc As Double
    Private _LCInsuranceAmountDisc As Double
    Private _InstallCollectionFeeDisc As Double
    Private _InsurCollectionFeeDisc As Double
    Private _PDCBounceFeeDisc As Double
    Private _STNKFeeDisc As Double
    Private _InsuranceClaimExpenseDisc As Double
    Private _RepossesFeeDisc As Double
    Private _TotalDiscount As Double
    Private _TotalAmountToBePaid As Double
    Private _TotalOSAR As Double
    Private _NewPrincipalAmount As Double
    Private _FloatingNextReviewDate As DateTime
    Private _EffRate As Integer
    Private _EffectiveRateBehaviour As String
    Private _Flag As String
    Private _StepUpStepDownType As String
    Private _agentFee As Double
    Private _ProvisionFee As Double
    Private _SubsidiBungaDealer As Double
    Private _SubsidiAngsuran As Double
    Private _strApplicationID As String
    Private _PphAccrued As Double
    Private _Table As String
    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property
    Public Property SubsidiAngsuran() As Double
        Get
            Return _SubsidiAngsuran
        End Get
        Set(ByVal Value As Double)
            _SubsidiAngsuran = Value
        End Set
    End Property

    Public Property SubsidiBungaDealer() As Double
        Get
            Return _SubsidiBungaDealer
        End Get
        Set(ByVal Value As Double)
            _SubsidiBungaDealer = Value
        End Set
    End Property


    Public Property ProvisionFee() As Double
        Get
            Return _ProvisionFee
        End Get
        Set(ByVal Value As Double)
            _ProvisionFee = Value
        End Set
    End Property

    Public Property AgentFee() As Double
        Get
            Return _agentFee
        End Get
        Set(ByVal Value As Double)
            _agentFee = Value
        End Set
    End Property
    Public Property StepUpStepDownType() As String
        Get
            Return (CType(_StepUpStepDownType, String))
        End Get
        Set(ByVal Value As String)
            _StepUpStepDownType = Value
        End Set
    End Property
    Public Property Flag() As String
        Get
            Return _Flag
        End Get
        Set(ByVal Value As String)
            _Flag = Value
        End Set
    End Property
    Public Property EffRate() As Integer
        Get
            Return _EffRate
        End Get
        Set(ByVal Value As Integer)
            _EffRate = Value
        End Set
    End Property
    Public Property FloatingNextReviewDate() As DateTime
        Get
            Return _FloatingNextReviewDate
        End Get
        Set(ByVal Value As DateTime)
            _FloatingNextReviewDate = Value
        End Set
    End Property

    Public Property NewPrincipalAmount() As Double
        Get
            Return _NewPrincipalAmount
        End Get
        Set(ByVal Value As Double)
            _NewPrincipalAmount = Value
        End Set
    End Property
    Public Property InterestType() As String
        Get
            Return _interestyype
        End Get
        Set(ByVal Value As String)
            _interestyype = Value
        End Set
    End Property
    Public Property EffectiveRateBehaviour() As String
        Get
            Return _EffectiveRateBehaviour
        End Get
        Set(ByVal Value As String)
            _EffectiveRateBehaviour = Value
        End Set
    End Property
    Public Property GuarantorID() As String
        Get
            Return _guarantorID
        End Get
        Set(ByVal Value As String)
            _guarantorID = Value
        End Set
    End Property
    Public Property CollectionExpense() As Double
        Get
            Return _collectionexpense
        End Get
        Set(ByVal Value As Double)
            _collectionexpense = Value
        End Set
    End Property
    Public Property TR_Nomor() As String
        Get
            Return _tr_nomor
        End Get
        Set(ByVal Value As String)
            _tr_nomor = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property

    Public Property OutstandingInterestOld() As Double
        Get
            Return _outstandinginterestold
        End Get
        Set(ByVal Value As Double)
            _outstandinginterestold = Value
        End Set
    End Property

    Public Property OutstandingPrincipalOld() As Double
        Get
            Return _outstandingprincipalold
        End Get
        Set(ByVal Value As Double)
            _outstandingprincipalold = Value
        End Set
    End Property

    Public Property OutstandingInterestNew() As Double
        Get
            Return _outstandinginterestnew
        End Get
        Set(ByVal Value As Double)
            _outstandinginterestnew = Value
        End Set
    End Property


    Public Property LCInstallmentAmountDisc() As Double
        Get
            Return _LCInstallmentAmountDisc
        End Get
        Set(ByVal Value As Double)
            _LCInstallmentAmountDisc = Value
        End Set
    End Property
    Public Property LCInsuranceAmountDisc() As Double
        Get
            Return _LCInsuranceAmountDisc
        End Get
        Set(ByVal Value As Double)
            _LCInsuranceAmountDisc = Value
        End Set
    End Property
    Public Property InstallCollectionFeeDisc() As Double
        Get
            Return _InstallCollectionFeeDisc
        End Get
        Set(ByVal Value As Double)
            _InstallCollectionFeeDisc = Value
        End Set
    End Property
    Public Property InsurCollectionFeeDisc() As Double
        Get
            Return _InsurCollectionFeeDisc
        End Get
        Set(ByVal Value As Double)
            _InsurCollectionFeeDisc = Value
        End Set
    End Property
    Public Property PDCBounceFeeDisc() As Double
        Get
            Return _PDCBounceFeeDisc
        End Get
        Set(ByVal Value As Double)
            _PDCBounceFeeDisc = Value
        End Set
    End Property
    Public Property STNKFeeDisc() As Double
        Get
            Return _STNKFeeDisc
        End Get
        Set(ByVal Value As Double)
            _STNKFeeDisc = Value
        End Set
    End Property
    Public Property InsuranceClaimExpenseDisc() As Double
        Get
            Return _InsuranceClaimExpenseDisc
        End Get
        Set(ByVal Value As Double)
            _InsuranceClaimExpenseDisc = Value
        End Set
    End Property
    Public Property RepossesFeeDisc() As Double
        Get
            Return _RepossesFeeDisc
        End Get
        Set(ByVal Value As Double)
            _RepossesFeeDisc = Value
        End Set
    End Property
    Public Property TotalDiscount() As Double
        Get
            Return _TotalDiscount
        End Get
        Set(ByVal Value As Double)
            _TotalDiscount = Value
        End Set
    End Property
    Public Property TotalAmountToBePaid() As Double
        Get
            Return _TotalAmountToBePaid
        End Get
        Set(ByVal Value As Double)
            _TotalAmountToBePaid = Value
        End Set
    End Property
    Public Property TotalOSAR() As Double
        Get
            Return _TotalOSAR
        End Get
        Set(ByVal Value As Double)
            _TotalOSAR = Value
        End Set
    End Property

    Public Property OutstandingPrincipalNew() As Double
        Get
            Return _outstandingprincipalnew
        End Get
        Set(ByVal Value As Double)
            _outstandingprincipalnew = Value
        End Set
    End Property
    Public Property ContractPrepaidAmount() As Double
        Get
            Return _contractprepaidamount
        End Get
        Set(ByVal Value As Double)
            _contractprepaidamount = Value
        End Set
    End Property

    Public Property PartialPrepaymentAmount() As Double
        Get
            Return _partialprepaymentamount
        End Get
        Set(ByVal Value As Double)
            _partialprepaymentamount = Value
        End Set
    End Property
    Public Property OutstandingTenor() As Integer
        Get
            Return _outstandingtenor
        End Get
        Set(ByVal Value As Integer)
            _outstandingtenor = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property
    Public Property NewNumInst() As Integer
        Get
            Return (CType(_newnuminst, Integer))
        End Get
        Set(ByVal Value As Integer)
            _newnuminst = Value
        End Set
    End Property
    Public Property MaxSeqNo() As Integer
        Get
            Return (CType(_maxseqNo, Integer))
        End Get
        Set(ByVal Value As Integer)
            _maxseqNo = Value
        End Set
    End Property
    Public Property SeqNo() As Integer
        Get
            Return (CType(_seqno, Integer))
        End Get
        Set(ByVal Value As Integer)
            _seqno = Value
        End Set
    End Property
    Public Property AdministrationFee() As Double
        Get
            Return _administrationfee
        End Get
        Set(ByVal Value As Double)
            _administrationfee = Value
        End Set
    End Property
    Public Property EffectiveDate() As DateTime
        Get
            Return _effectivedate
        End Get
        Set(ByVal Value As DateTime)
            _effectivedate = Value
        End Set
    End Property
    Public Property DueDate() As DateTime
        Get
            Return _duedate
        End Get
        Set(ByVal Value As DateTime)
            _duedate = Value
        End Set
    End Property
    Public Property InstallmentScheme() As String
        Get
            Return _InstallmentScheme
        End Get
        Set(ByVal Value As String)
            _InstallmentScheme = Value
        End Set
    End Property

    Public Property FloatingPeriod() As String
        Get
            Return _FloatingPeriod
        End Get
        Set(ByVal Value As String)
            _FloatingPeriod = Value
        End Set
    End Property
    Public Property CummulativeStart() As Integer
        Get
            Return _CummulativeStart
        End Get
        Set(ByVal Value As Integer)
            _CummulativeStart = Value
        End Set
    End Property
    Public Property Tenor() As Integer
        Get
            Return (CType(_Tenor, Integer))
        End Get
        Set(ByVal Value As Integer)
            _Tenor = Value
        End Set
    End Property
    Public Property SpName() As String
        Get
            Return _SpName
        End Get
        Set(ByVal Value As String)
            _SpName = Value
        End Set
    End Property
    Public Property Principal() As Double
        Get
            Return _Principal
        End Get
        Set(ByVal Value As Double)
            _Principal = Value
        End Set
    End Property
    Public Property Data1() As DataTable
        Get
            Return _Data1
        End Get
        Set(ByVal Value As DataTable)
            _Data1 = Value
        End Set
    End Property
    Public Property data2() As DataTable
        Get
            Return _data2
        End Get
        Set(ByVal Value As DataTable)
            _data2 = Value
        End Set
    End Property
    Public Property data3() As DataTable
        Get
            Return _data3
        End Get
        Set(ByVal Value As DataTable)
            _data3 = Value
        End Set
    End Property
    Public Property Output() As String
        Get
            Return _Output
        End Get
        Set(ByVal Value As String)
            _Output = Value
        End Set
    End Property

    Public Property MydataSet() As DataSet
        Get
            Return _MydataSet
        End Get
        Set(ByVal Value As DataSet)
            _MydataSet = Value
        End Set
    End Property
    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property FlatRate() As Double
        Get
            Return _FlatRate
        End Get
        Set(ByVal Value As Double)
            _FlatRate = Value
        End Set
    End Property
    Public Property InterestTotal() As Double
        Get
            Return _InterestTotal
        End Get
        Set(ByVal Value As Double)
            _InterestTotal = Value
        End Set
    End Property
    Public Property EffectiveRate() As Double
        Get
            Return _EffectiveRate
        End Get
        Set(ByVal Value As Double)
            _EffectiveRate = Value
        End Set
    End Property
    Public Property FirstInstallment() As String
        Get
            Return _FirstInstallment
        End Get
        Set(ByVal Value As String)
            _FirstInstallment = Value
        End Set
    End Property
    Public Property GracePeriod() As Integer
        Get
            Return _GracePeriod
        End Get
        Set(ByVal Value As Integer)
            _GracePeriod = Value
        End Set
    End Property
    Public Property IsSave() As Integer
        Get
            Return _IsSave
        End Get
        Set(ByVal Value As Integer)
            _IsSave = Value
        End Set
    End Property
    Public Property GracePeriodType() As String
        Get
            Return _GracePeriodType
        End Get
        Set(ByVal Value As String)
            _GracePeriodType = Value
        End Set
    End Property
    Public Property NTF() As Double
        Get
            Return _NTF
        End Get
        Set(ByVal Value As Double)
            _NTF = Value
        End Set
    End Property
    Public Property NumOfInstallment() As Integer
        Get
            Return _NumOfInstallment
        End Get
        Set(ByVal Value As Integer)
            _NumOfInstallment = Value
        End Set
    End Property
    Public Property SupplierRate() As Double
        Get
            Return _SupplierRate
        End Get
        Set(ByVal Value As Double)
            _SupplierRate = Value
        End Set
    End Property
    Public Property PaymentFrequency() As String
        Get
            Return _PaymentFrequency
        End Get
        Set(ByVal Value As String)
            _PaymentFrequency = Value
        End Set
    End Property
    Public Property BusDate() As DateTime
        Get
            Return _BusDate
        End Get
        Set(ByVal Value As DateTime)
            _BusDate = Value
        End Set
    End Property
    Public Property DiffRate() As Double
        Get
            Return _DiffRate
        End Get
        Set(ByVal Value As Double)
            _DiffRate = Value
        End Set
    End Property
    Public Property GrossYield() As Double
        Get
            Return _GrossYield
        End Get
        Set(ByVal Value As Double)
            _GrossYield = Value
        End Set
    End Property

    'Public Property ApplicationID() As String
    '    Get
    '        Return _strApplicationID
    '    End Get
    '    Set(ByVal Value As String)
    '        _strApplicationID = Value
    '    End Set
    'End Property

    Public Property PolaAngsuran As String
    Public Property TidakAngsur As Double
    Public Property PotongDanaCair As Boolean
    Public Property AngsuranBayarDealer As Boolean
    Public Property TotalOTRSupplier As Double
    Public Property DPSupplier As Double
    Public Property NTFSupplier As Double
    Public Property TenorSupplier As Integer
    Public Property FlatRateSupplier As Decimal
    Public Property EffectiveRateSupplier As Decimal
    Public Property TotalBunga As Double
    Public Property TotalBungaSupplier As Double
    Public Property NilaiKontrak As Double
    Public Property NilaiKontrakSupplier As Double
    Public Property InstallmentAmountSupplier As Double
    Public Property AngsuranTidakSamaSupplier As Double

    Public Property InstallmentUnpaid As Double
    Public Property DownPayment As Double
    Public Property DPKaroseriAmount As Double
    Public Property DPKaroseriAmountMF As Double
    Public Property AlokasiInsentifRefundBunga As Double
    Public Property AlokasiInsentifRefundBungaPercent As Double
    Public Property TitipanRefundBunga As Double
    Public Property TitipanRefundBungaPercent As Double
    Public Property AlokasiInsentifPremi As Double
    Public Property AlokasiInsentifPremiPercent As Double
    Public Property AlokasiProgresifPremi As Double
    Public Property AlokasiProgresifPremiPercent As Double
    Public Property SubsidiBungaPremi As Double
    Public Property SubsidiBungaPremiPercent As Double
    Public Property PendapatanPremi As Double
    Public Property PendapatanPremiPercent As Double
    Public Property ProvisiPercent As Double
    Public Property AlokasiInsentifProvisi As Double
    Public Property AlokasiInsentifProvisiPercent As Double
    Public Property SubsidiBungaProvisi As Double
    Public Property SubsidiBungaProvisiPercent As Double
    Public Property TitipanProvisi As Double
    Public Property TitipanProvisiPercent As Double
    Public Property AlokasiInsentifBiayaLain As Double
    Public Property AlokasiInsentifBiayaLainPercent As Double
    Public Property SubsidiBungaBiayaLain As Double
    Public Property SubsidiBungaBiayaLainPercent As Double
    Public Property TitipanBiayaLain As Double
    Public Property TitipanBiayaLainPercent As Double
    Public Property TitipanRewardDealer As Double
    Public Property TitipanRewardDealerPercent As Double
    Public Property BungaNettEff As Decimal
    Public Property BungaNettFlat As Decimal

    Public Property RefundBungaPercent As Decimal
    Public Property RefundBungaAmount As Double
    Public Property RefundLainPercent As Decimal
    Public Property RefundLainAmount As Double
    Public Property RefundAdminPercent As Decimal
    Public Property RefundAdminAmount As Double
    Public Property RefundPremiPercent As Decimal
    Public Property RefundPremiAmount As Double


    Public Property RefundProvisiPercent As Decimal
    Public Property RefundProvisiAmount As Double
    Public Property GabungRefundSupplier As Boolean

    Public Property SubsidiBungaATPM As Double
    Public Property SubsidiAngsuranATPM As Double
    Public Property SubsidiUangMuka As Double
    Public Property SubsidiUangMukaATPM As Double
    Public Property DealerDiscount As Double
    Public Property BiayaMaintenance As Double

    Public Property Term As Integer
    Public Property RateIRR As Double
    Public Property ExcludeAmount As Double
    Public Property RefundBiayaLain As Double
    Public Property RefundBiayaLainPercent As Decimal

    Public Property SupplierID As String
    Public Property SupplierName As String

    Public Property APHTFee As Decimal
    Public Property BPHTBFee As Decimal
    Public Property AppraisalTab As String
    Public Property AppraisalFee As Decimal
    Public Property AdminFee As Double
    Public Property NotarisFee As Decimal
    Public Property SubsidiBunga As Double
    Public Property IncomePremi As Double
    Public Property OtherFee As Double
    Public Property IncentiveDealer As Double
    Public Property InsuranceRefund As Double
    Public Property ProvisionRefund As Double
    Public Property OtherRefund As Double

    Public Property AdminFeeOL As Double
    Public Property MaintenanceFeeOL As Double
    Public Property STNKFeeOL As Double
    Public Property ProvisiOL As Double
    Public Property InsuranceMonthlyOL As Double
    Public Property BasicLease As Double

    Public Property UppingBungaRate As Decimal
    Public Property UppingBungaFlatRate As Decimal
    Public Property UppingBungaAmount As Double

    Public Property SubsidiRisk As Double
    Public Property SubsidiRiskATPM As Double
    Public Property RefundCreditProtectionPercent As Decimal
    Public Property RefundCreditProtectionAmount As Double
    Public Property RefundJaminanKreditPercent As Decimal
    Public Property RefundJaminanKreditAmount As Double
    Public Property RefundUppingBungaPercent As Decimal
    Public Property RefundUppingBungaAmount As Double
    Public Property SubsidiDPUppingBungaPercent As Decimal
    Public Property SubsidiDPUppingBungaAmount As Double

    Public Property DownPaymentMf As Double
    Public Property AdminFeeMf As Double
    Public Property FiduciaFeeMf As Double
    Public Property OtherFeeMf As Double
    Public Property ProvisionFeeMf As Double

    Public Property AsuransiTunaiMF As Double
    Public Property AngsuranPertamaMf As Double
    Public Property GracePeriodAngsuran As Integer

    Public Property AccruedInterestDisc As Double
    Public Property InstallmentDueDisc As Double
    Public Property OutstandingInterestDisc As Double
    Public Property OutstandingPrincipalDisc As Double

    Public Property PerjanjianNo As String
    Public Property PPh As Double
    Public Property Penalty As Double
    Public Property TerminationPenalty As Double

    Public Property InterestTypeDesc As String
    Public Property ProductDesc As String
    Public Property InstallmentSchemeDesc As String
    Public Property ProductOfferingDesc As String
    Public Property FinanceTypeDesc As String
    Public Property GuarantorName As String
    Public Property ReschedulingNo As Integer
    Public Property TotalPrePaymentAmountResch As Double


    Public Property PphAccrued As Double
        Get
            Return _PphAccrued
        End Get
        Set(value As Double)
            _PphAccrued = value
        End Set
    End Property

    Public Property PphDenda As Double
        Get
            Return _PphDenda
        End Get
        Set(value As Double)
            _PphDenda = value
        End Set
    End Property


    ''Parameter KPR
    Private _NTFKPR As Decimal
    Private _TenorAkhirKPR As Integer
    Private _TenorAwalKPR As Integer
    Private _RateKPR As Double
    Private _TotalTenorKPR As Integer
    Private _PphDenda As Double

    Public Property NTFKPR() As Decimal
        Get
            Return _NTFKPR
        End Get
        Set(ByVal Value As Decimal)
            _NTFKPR = Value
        End Set
    End Property
    Public Property TenorAwalKPR() As Integer
        Get
            Return _TenorAwalKPR
        End Get
        Set(ByVal Value As Integer)
            _TenorAwalKPR = Value
        End Set
    End Property
    Public Property TenorAkhirKPR() As Integer
        Get
            Return _TenorAkhirKPR
        End Get
        Set(ByVal Value As Integer)
            _TenorAkhirKPR = Value
        End Set
    End Property
    Public Property RateKPR() As Double
        Get
            Return _RateKPR
        End Get
        Set(ByVal Value As Double)
            _RateKPR = Value
        End Set
    End Property
    Public Property TotalTenorKPR() As Integer
        Get
            Return _TotalTenorKPR
        End Get
        Set(ByVal Value As Integer)
            _TotalTenorKPR = Value
        End Set
    End Property

    Public Property Ratetype As String
    Public Property TermSeqNo As Object
    Public Property SelectedAppraisal As Object
    Public Property StatusView As Object
    'end


End Class
