
<Serializable()> _
Public Class CreditScoring : Inherits Common
    Private _ApplicationID As String
    Private _CustomerName As String
    Private _SupplierName As String
    Private _EmployeeName As String
    Private _NewApplicationDate As Date
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _ApplicationStep As String
    Private _CreditScoringDate As Date
    Private _CreditScore As Decimal
    Private _CreditScoringResult As String
    Private _ID As String
    Private _Value As String
    Private _ScoreValue As Integer
    Private _Weight As Integer
    Private _Description As String
    Private _CSResult_Temp As String

    Public Property CSResult_Temp() As String
        Get
            Return _CSResult_Temp
        End Get
        Set(ByVal Value As String)
            _CSResult_Temp = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property Weight() As Integer
        Get
            Return _Weight
        End Get
        Set(ByVal Value As Integer)
            _Weight = Value
        End Set
    End Property

    Public Property ScoreValue() As Integer
        Get
            Return _ScoreValue
        End Get
        Set(ByVal Value As Integer)
            _ScoreValue = Value
        End Set
    End Property

    Public Property ID() As String
        Get
            Return _ID
        End Get
        Set(ByVal Value As String)
            _ID = Value
        End Set
    End Property

    Public Property Value() As String
        Get
            Return _Value
        End Get
        Set(ByVal Value As String)
            _Value = Value
        End Set
    End Property

    Public Property CreditScoringDate() As Date
        Get
            Return _CreditScoringDate
        End Get
        Set(ByVal Value As Date)
            _CreditScoringDate = Value
        End Set
    End Property

    Public Property ApplicationStep() As String
        Get
            Return _ApplicationStep
        End Get
        Set(ByVal Value As String)
            _ApplicationStep = Value
        End Set
    End Property

    Public Property CreditScoringResult() As String
        Get
            Return _CreditScoringResult
        End Get
        Set(ByVal Value As String)
            _CreditScoringResult = Value
        End Set
    End Property

    Public Property CreditScore() As Decimal
        Get
            Return _CreditScore
        End Get
        Set(ByVal Value As Decimal)
            _CreditScore = Value
        End Set
    End Property


    Public Property NewApplicationDate() As Date
        Get
            Return _NewApplicationDate
        End Get
        Set(ByVal Value As Date)
            _NewApplicationDate = Value
        End Set
    End Property

    Public Property SupplierName() As String
        Get
            Return _SupplierName
        End Get
        Set(ByVal Value As String)
            _SupplierName = Value
        End Set
    End Property

    Public Property EmployeeName() As String
        Get
            Return _EmployeeName
        End Get
        Set(ByVal Value As String)
            _EmployeeName = Value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return _CustomerName
        End Get
        Set(ByVal Value As String)
            _CustomerName = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property


    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property Approval As Approval
End Class



