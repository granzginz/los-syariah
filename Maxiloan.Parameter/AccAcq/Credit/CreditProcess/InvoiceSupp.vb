﻿
<Serializable()> _
Public Class InvoiceSupp
    Sub New()

        InvoiceSuppDeductions = New List(Of InvoiceSuppDeduction)
    End Sub
    Public Property BranchID As String

    Public Property SupplierID As String
    Public Property SupplierName As String

    Public Property InvoiceNo As String
    Public Property InvoiceDate As Date

    Public Property InvAmountOriginal As Decimal

    Public ReadOnly Property InvAmountDeduction As Decimal
        Get
            Return InvoiceSuppDeductions.Sum(Function(x) x.DeductionAmount)
        End Get
    End Property

    Public ReadOnly Property InvoiceAmount As Decimal
        Get
            Return InvAmountOriginal - InvAmountDeduction
        End Get
    End Property


    Public Property ApplicationID As String
    Public Property AgreementNo As String
    Public Property CustomerID As String
    Public Property CustomerName As String
    Public Property APAmount As Decimal
    Public Property APType As String

    Public Property SuppBankId As String
    Public Property InvoiceFile As String
    Public Property InvoiceFileBase64String As String
    Public Property IsGabungRefundSupplier As Boolean
    Public Property Refound As Decimal

    Public Property NilPelunasan As Double
    Public Property FirstInstallment As Double
    Public Property FirstInstallmentDate As Date
    Public Property InvoiceSuppDeductions As IList(Of InvoiceSuppDeduction) 
    Public Property APDueDate As Date


    Public Function ToSuppDeductionDataTable() As DataTable
        Dim dt = New DataTable()

        dt.Columns.Add("DeductionID", GetType(String))
        dt.Columns.Add("DeductionAmount", GetType(Decimal))
        For Each v In InvoiceSuppDeductions 
            Dim row = dt.NewRow()  
            row("DeductionID") = v.DeductionID
            row("DeductionAmount") = v.DeductionAmount
            dt.Rows.Add(row)
        Next 
        Return dt 
    End Function
End Class


<Serializable()> _
Public Class InvoiceSuppDeduction
    Public Property Seq As Integer
    Public Property DeductionID As String
    Public Property DeductionDescription As String
    Public Property DeductionAmount As Decimal
    Public Property ApplicationIDTo As String

End Class



