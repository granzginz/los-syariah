﻿<Serializable()> _
Public Class CreditScoring_calculate : Inherits Common
    Public Property ApplicationId As String
    Public Property CustomerType As String
    Public Property DT As DataTable
    Public Property CreditScoreSchemeID As String
    Public Property CreditScoreComponentID As String
    Public Property CSResult_Temp As String
    Public Property CreditScoreResult As String
    Public Property CreditScore As Decimal
    Public Property lblGrade As String
    Public Property ReturnResult As String
    Public Property ScoreResults As Parameter.ScoreResults

    Public Property ProductID As String
    Public Property ProductOfferingID As String
End Class
