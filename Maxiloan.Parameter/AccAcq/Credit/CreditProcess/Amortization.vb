
<Serializable()> _
Public Class Amortization
    Private _numinstallment As Integer
    Private _installmentamount As Double
    Private _ntf As Double
    Private _runrate As Double
    Private _payfrequently As Integer
    Private _graceperiod As Integer
    Public Enum AmortizationType As Integer
        Advance = 1
        Arrear = 2
        GracePeriod_RollOver = 3
        GracePeriod_InterestOnly = 4
    End Enum
    Public Property NumberInstallment() As Integer
        Get
            Return _numinstallment
        End Get
        Set(ByVal Value As Integer)
            _numinstallment = Value
        End Set
    End Property
    Public Property InstallmentAmount() As Double
        Get
            Return _installmentamount
        End Get
        Set(ByVal Value As Double)
            _installmentamount = Value
        End Set
    End Property

    Public Property NTF() As Double
        Get
            Return _ntf
        End Get
        Set(ByVal Value As Double)
            _ntf = Value
        End Set
    End Property

    Public Property PaymentFrequently() As Integer
        Get
            Return _payfrequently
        End Get
        Set(ByVal Value As Integer)
            _payfrequently = Value
        End Set
    End Property

    Public Property RunRate() As Double
        Get
            Return _runrate
        End Get
        Set(ByVal Value As Double)
            _runrate = Value
        End Set
    End Property

    Public Property GracePeriod() As Integer
        Get
            Return _graceperiod
        End Get
        Set(ByVal Value As Integer)
            _graceperiod = Value
        End Set
    End Property
End Class
