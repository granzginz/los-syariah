﻿
<Serializable()> _
Public Class TolakanNonCust : Inherits Common
    Public Property IDTolakan As String
    Public Property Description As String

    Public Property ID As Integer
    Public Property Nama As String
    Public Property NoKTP As String
    Public Property StatusPerkawinan As String
    Public Property Pekerjaan As String
    Public Property AlasanPenolakan As String
    Public Property DetailPenolakan As String
    Public Property Keterangan As String

    Public Property BirthPlace As String
    Public Property BirthDate As String
    Public Property SupplierID As String
    Public Property AssetCode As String
    Public Property jumlah As Integer
    Public Property Output As String

    Public Property listdata As DataTable
    Public Property totalrecords As Int64
    Public Property Where As String
    Public Property table As String
    Public Property AddEdit As String
    Public Property ListReportTolakanNonCust As DataSet

End Class
