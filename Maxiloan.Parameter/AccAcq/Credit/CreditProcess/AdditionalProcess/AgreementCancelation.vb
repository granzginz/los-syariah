
Public Class AgreementCancellation
    Inherits Common
    Private _cancelationdate As Date
    Private _cancelationreasionID As String
    Private _notesofcancelation As String
    Private _applicationid As String
    Public Property ApplicationID() As String
        Get
            Return _applicationid
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property
    Public Property Cancellationdate() As Date
        Get
            Return _cancelationdate
        End Get
        Set(ByVal Value As Date)
            _cancelationdate = Value
        End Set
    End Property

    Public Property CancellationReasonID() As String
        Get
            Return _cancelationreasionID
        End Get
        Set(ByVal Value As String)
            _cancelationreasionID = Value
        End Set
    End Property

    Public Property NotesOfCancellation() As String
        Get
            Return _notesofcancelation
        End Get
        Set(ByVal Value As String)
            _notesofcancelation = Value
        End Set
    End Property

    Public Property isCopyApplication As Boolean
End Class
