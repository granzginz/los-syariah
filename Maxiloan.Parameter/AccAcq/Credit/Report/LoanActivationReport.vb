<Serializable()> _
Public Class LoanActivationReport : Inherits Maxiloan.Parameter.Common
    Private _dataSet As Dataset
    Private _SelectedDate As String
    Public Property Dataset() As DataSet
        Get
            Return _dataSet
        End Get
        Set(ByVal Value As DataSet)
            _dataSet = Value
        End Set
    End Property

    Public Property SelectedDate() As String
        Get
            Return _SelectedDate
        End Get
        Set(ByVal Value As String)
            _SelectedDate = Value
        End Set
    End Property
End Class
