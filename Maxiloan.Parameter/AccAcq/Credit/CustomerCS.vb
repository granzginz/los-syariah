﻿<Serializable()>
Public Class PinjamanLain
    
    Public Property No As Integer
    Public Property CustID As String = String.Empty
    Public Property SeqNo As Integer
    ' Public Property Name As String = String.Empty
    Public Property Name As String
    Public Property JenisPinjaman As String
    Public Property JumlahPinjaman As Double
    Public Property Tenor As Integer
    Public Property SisaPokok As Double
    Public Property Angsuran As Double
    Public Shared Function ToDataTable(data As IList(Of PinjamanLain)) As DataTable
        Dim dt = New DataTable()

        'dt.Columns.Add("No", GetType(Integer))
        dt.Columns.Add("CustID", GetType(String))
        dt.Columns.Add("SeqNo", GetType(Integer))
        dt.Columns.Add("Name", GetType(String))
        dt.Columns.Add("JenisPinjaman", GetType(String))
        dt.Columns.Add("JumlahPinjaman", GetType(Double))
        dt.Columns.Add("Tenor", GetType(Integer))
        dt.Columns.Add("SisaPokok", GetType(Double))
        dt.Columns.Add("Angsuran", GetType(Double))

        For Each v In data
            Dim row = dt.NewRow()

            'row("No") = v.No
            row("CustID") = v.CustID
            row("SeqNo") = v.SeqNo
            row("Name") = v.Name
            row("JenisPinjaman") = v.JenisPinjaman
            row("JumlahPinjaman") = v.JumlahPinjaman
            row("Tenor") = v.Tenor
            row("SisaPokok") = v.SisaPokok
            row("Angsuran") = v.Angsuran
            dt.Rows.Add(row)
        Next
        Return dt
    End Function
End Class