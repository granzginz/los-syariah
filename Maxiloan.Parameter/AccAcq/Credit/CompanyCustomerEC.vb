﻿<Serializable()> _
Public Class CompanyCustomerEC
    Public Property CustomerID As String
    Public Property Name As String
    Public Property HP As String
    Public Property Email As String
    Public Property Relation As String
    Public Property Alamat As Parameter.Address

    Public Sub BuildProperties(oRow As DataRow)
        CustomerID = oRow("CustomerID").ToString.Trim
        Name = oRow("ECtName").ToString.Trim
        HP = oRow("ECtHP").ToString.Trim
        Email = oRow("ECtEmail").ToString.Trim
        Relation = oRow("ECtRelation").ToString.Trim

        Alamat = New Parameter.Address
        With Alamat
            .Address = oRow("ECtAddress").ToString.Trim
            .RT = oRow("ECtRT").ToString.Trim
            .RW = oRow("ECtRW").ToString.Trim
            .Kelurahan = oRow("ECtKelurahan").ToString.Trim
            .Kecamatan = oRow("ECtKecamatan").ToString.Trim
            .City = oRow("ECtCity").ToString.Trim
            .ZipCode = oRow("ECtZipCode").ToString.Trim
            .AreaPhone1 = oRow("ECtAreaPhone1").ToString.Trim
            .Phone1 = oRow("ECtPhone1").ToString.Trim
            .AreaPhone2 = oRow("ECtAreaPhone2").ToString.Trim
            .Phone2 = oRow("ECtPhone2").ToString.Trim
            .AreaFax = oRow("ECtAreaFax").ToString.Trim
            .Fax = oRow("ECtFax").ToString.Trim
        End With

    End Sub
End Class

















