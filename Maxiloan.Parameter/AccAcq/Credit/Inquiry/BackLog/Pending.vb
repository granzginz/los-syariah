
<Serializable()> _
Public Class Pending : Inherits Common
    Private _listdata As DataTable
    Private _totalrecords As Int64
    Private _WhereCond2 As String
    Private _Table As String

    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property
    Public Property listdata() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property WhereCond2() As String
        Get
            Return _WhereCond2
        End Get
        Set(ByVal Value As String)
            _WhereCond2 = Value
        End Set
    End Property
End Class
