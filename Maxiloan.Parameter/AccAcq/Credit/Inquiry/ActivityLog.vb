

<Serializable()> _
Public Class ActivityLog : Inherits Maxiloan.Parameter.Common
    Private _listActivity As DataTable
    Private _listReport As DataSet
    Public Property DateTo As Date
    Public Property DateFrom As Date
    Public Property status As String

    Public Property ListActivity() As DataTable
        Get
            Return _listActivity
        End Get
        Set(ByVal Value As DataTable)
            _listActivity = Value
        End Set
    End Property
    Public Property ListReport() As DataSet
        Get
            Return _listReport
        End Get
        Set(ByVal Value As DataSet)
            _listReport = Value
        End Set
    End Property
End Class
