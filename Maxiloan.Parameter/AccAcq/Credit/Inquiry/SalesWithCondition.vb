

<Serializable()> _
Public Class SalesWithCondition : Inherits Maxiloan.Parameter.Common
    Private _listSales As DataTable

    Public Property ListSales() As DataTable
        Get
            Return _listSales
        End Get
        Set(ByVal Value As DataTable)
            _listSales = Value
        End Set
    End Property
End Class
