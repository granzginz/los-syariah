﻿<Serializable()> _
Public Class CustomerGroupPIC : Inherits Maxiloan.Parameter.Common
    Public Property ID As String
    Public Property NamaPIC As String
    Public Property Alamat As String
    Public Property RT As String
    Public Property RW As String
    Public Property Kelurahan As String
    Public Property Kecamatan As String
    Public Property Kota As String
    Public Property KodePos As String
    Public Property AreaPhone1 As String
    Public Property Phone1 As String
    Public Property AreaPhone2 As String
    Public Property Phone2 As String
    Public Property AreaFax As String
    Public Property Fax As String
    Public Property NoHP As String
    Public Property CustomerGroupID As String
    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
End Class
