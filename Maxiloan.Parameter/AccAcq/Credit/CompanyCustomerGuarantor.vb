﻿<Serializable()>
Public Class CompanyCustomerGuarantor
    Public Property Id As String
    Public Property CustomerId As String
    Public Property NamaPenjamin As String
    Public Property JabatanId As String
    Public Property JabatanDesc As String
    Public Property TempatLahir As String
    Public Property TglLahir As Date

    Public Property JenisDokumenId As String
    Public Property JenisDokumenDesc As String

    Public Property NoDokumen As String
    Public Property MasaBerlakuKtp As Date

    Public Property Seq As String
    Public Property AlamatKTP As Parameter.Address

    Public Shared Function ToDataTable(data As IList(Of CompanyCustomerGuarantor)) As DataTable
        Dim dt = New DataTable()

        dt.Columns.Add("ID", GetType(Long))
        dt.Columns.Add("CustomerID", GetType(String))
        dt.Columns.Add("NamaGurantor", GetType(String))
        dt.Columns.Add("JabatanID", GetType(String))
        dt.Columns.Add("TempatLahir", GetType(String))
        dt.Columns.Add("TglLahir", GetType(DateTime))
        dt.Columns.Add("JenisDokumen", GetType(String))
        dt.Columns.Add("NoDokument", GetType(String))
        dt.Columns.Add("MasaBerlakuKTP", GetType(DateTime))
        dt.Columns.Add("GurantorAddress", GetType(String))
        dt.Columns.Add("GurantorRT", GetType(String))
        dt.Columns.Add("GurantorRW", GetType(String))
        dt.Columns.Add("GurantorKelurahan", GetType(String))
        dt.Columns.Add("GurantorKecamatan", GetType(String))
        dt.Columns.Add("GurantorCity", GetType(String))
        dt.Columns.Add("GurantorZipCode", GetType(String))
        dt.Columns.Add("GurantorAreaPhone1", GetType(String))
        dt.Columns.Add("GurantorPhone1", GetType(String))
        dt.Columns.Add("GurantorAreaPhone2", GetType(String))
        dt.Columns.Add("GurantorPhone2", GetType(String))
        dt.Columns.Add("GurantorAreaFax", GetType(String))
        dt.Columns.Add("GurantorFax", GetType(String))
        dt.Columns.Add("GurantorMoblilePhone", GetType(String))


        For Each v In data


            Dim row = dt.NewRow()


            row("ID") = v.Id
            row("CustomerID") = v.CustomerId
            row("NamaGurantor") = v.NamaPenjamin
            row("JabatanID") = v.JabatanId
            row("TempatLahir") = v.TempatLahir
            row("TglLahir") = v.TglLahir
            row("JenisDokumen") = v.JenisDokumenId
            row("NoDokument") = v.NoDokumen
            row("MasaBerlakuKTP") = v.MasaBerlakuKtp
            row("GurantorAddress") = v.AlamatKTP.Address
            row("GurantorRT") = v.AlamatKTP.RT
            row("GurantorRW") = v.AlamatKTP.RT
            row("GurantorKelurahan") = v.AlamatKTP.Kelurahan
            row("GurantorKecamatan") = v.AlamatKTP.Kecamatan
            row("GurantorCity") = v.AlamatKTP.City
            row("GurantorZipCode") = v.AlamatKTP.ZipCode
            row("GurantorAreaPhone1") = v.AlamatKTP.AreaPhone1
            row("GurantorPhone1") = v.AlamatKTP.Phone1
            row("GurantorAreaPhone2") = v.AlamatKTP.AreaPhone2
            row("GurantorPhone2") = v.AlamatKTP.Phone2
            row("GurantorAreaFax") = v.AlamatKTP.AreaFax
            row("GurantorFax") = v.AlamatKTP.Fax
            row("GurantorMoblilePhone") = v.AlamatKTP.MobilePhone

            dt.Rows.Add(row)
        Next

        Return dt



    End Function

    Public Shared Function BuildProperties(reader As IDataReader) As CompanyCustomerGuarantor

        Dim _seq = reader.GetOrdinal("Seq")
        Dim _CustomerID = reader.GetOrdinal("CustomerID")
        Dim _NamaGurantor = reader.GetOrdinal("NamaGurantor")
        Dim _JabatanID = reader.GetOrdinal("JabatanID")
        Dim _EmployeePosition = reader.GetOrdinal("EmployeePosition")
        Dim _TempatLahir = reader.GetOrdinal("TempatLahir")
        Dim _TglLahir = reader.GetOrdinal("TglLahir")
        Dim _JenisDokumen = reader.GetOrdinal("JenisDokumen")
        Dim _JenisDokumenDescription = reader.GetOrdinal("JenisDokumenDescription")
        Dim _NoDokument = reader.GetOrdinal("NoDokument")
        Dim _MasaBerlakuKTP = reader.GetOrdinal("MasaBerlakuKTP")
        Dim _GurantorAddress = reader.GetOrdinal("GurantorAddress")
        Dim _GurantorRT = reader.GetOrdinal("GurantorRT")
        Dim _GurantorRW = reader.GetOrdinal("GurantorRW")
        Dim _GurantorKelurahan = reader.GetOrdinal("GurantorKelurahan")
        Dim _GurantorKecamatan = reader.GetOrdinal("GurantorKecamatan")
        Dim _GurantorCity = reader.GetOrdinal("GurantorCity")
        Dim _GurantorZipCode = reader.GetOrdinal("GurantorZipCode")
        Dim _GurantorAreaPhone1 = reader.GetOrdinal("GurantorAreaPhone1")
        Dim _GurantorPhone1 = reader.GetOrdinal("GurantorPhone1")
        Dim _GurantorAreaPhone2 = reader.GetOrdinal("GurantorAreaPhone2")
        Dim _GurantorPhone2 = reader.GetOrdinal("GurantorPhone2")
        Dim _GurantorAreaFax = reader.GetOrdinal("GurantorAreaFax")
        Dim _GurantorFax = reader.GetOrdinal("GurantorFax")
        Dim _GurantorMoblilePhone = reader.GetOrdinal("GurantorMoblilePhone")

        Dim result = New CompanyCustomerGuarantor With
                     {
                         .Id = IIf(reader.IsDBNull(_seq), "0", reader.GetInt64(_seq).ToString()),
                         .CustomerId = IIf(reader.IsDBNull(_CustomerID), String.Empty, reader.GetString(_CustomerID)),
                         .NamaPenjamin = IIf(reader.IsDBNull(_NamaGurantor), String.Empty, reader.GetString(_NamaGurantor)),
                         .JabatanId = IIf(reader.IsDBNull(_JabatanID), String.Empty, reader.GetString(_JabatanID)),
                         .JabatanDesc = IIf(reader.IsDBNull(_EmployeePosition), String.Empty, reader.GetString(_EmployeePosition)),
                         .TempatLahir = IIf(reader.IsDBNull(_TempatLahir), String.Empty, reader.GetString(_TempatLahir)),
                         .TglLahir = IIf(reader.IsDBNull(_TglLahir), String.Empty, reader.GetDateTime(_TglLahir).ToString),
                         .JenisDokumenId = IIf(reader.IsDBNull(_JenisDokumen), String.Empty, reader.GetString(_JenisDokumen)),
                         .JenisDokumenDesc = IIf(reader.IsDBNull(_JenisDokumenDescription), String.Empty, reader.GetString(_JenisDokumenDescription)),
                         .NoDokumen = IIf(reader.IsDBNull(_NoDokument), String.Empty, reader.GetString(_NoDokument)),
                         .MasaBerlakuKtp = IIf(reader.IsDBNull(_MasaBerlakuKTP), String.Empty, reader.GetDateTime(_MasaBerlakuKTP).ToString()),
                         .AlamatKTP = New Parameter.Address With {
                             .Address = IIf(reader.IsDBNull(_GurantorAddress), String.Empty, reader.GetString(_GurantorAddress)),
                             .RT = IIf(reader.IsDBNull(_GurantorRT), String.Empty, reader.GetString(_GurantorRT)),
                             .RW = IIf(reader.IsDBNull(_GurantorRW), String.Empty, reader.GetString(_GurantorRW)),
                             .Kelurahan = IIf(reader.IsDBNull(_GurantorKelurahan), String.Empty, reader.GetString(_GurantorKelurahan)),
                             .Kecamatan = IIf(reader.IsDBNull(_GurantorKecamatan), String.Empty, reader.GetString(_GurantorKecamatan)),
                             .City = IIf(reader.IsDBNull(_GurantorCity), String.Empty, reader.GetString(_GurantorCity)),
                             .ZipCode = IIf(reader.IsDBNull(_GurantorZipCode), String.Empty, reader.GetString(_GurantorZipCode)),
                             .AreaPhone1 = IIf(reader.IsDBNull(_GurantorAreaPhone1), String.Empty, reader.GetString(_GurantorAreaPhone1)),
                             .Phone1 = IIf(reader.IsDBNull(_GurantorPhone1), String.Empty, reader.GetString(_GurantorPhone1)),
                             .AreaPhone2 = IIf(reader.IsDBNull(_GurantorAreaPhone2), String.Empty, reader.GetString(_GurantorAreaPhone2)),
                             .Phone2 = IIf(reader.IsDBNull(_GurantorPhone2), String.Empty, reader.GetString(_GurantorPhone2)),
                             .AreaFax = IIf(reader.IsDBNull(_GurantorAreaFax), String.Empty, reader.GetString(_GurantorAreaFax)),
                             .Fax = IIf(reader.IsDBNull(_GurantorFax), String.Empty, reader.GetString(_GurantorFax)),
                             .MobilePhone = IIf(reader.IsDBNull(_GurantorMoblilePhone), String.Empty, reader.GetString(_GurantorMoblilePhone))
                         }
                     }

        Return result
    End Function

End Class
