﻿<Serializable()> _
Public Class CustomerSI : Inherits Parameter.Customer    
    ' Public Property NamaSuamiIstri As String
    Public Property Seq As Integer
    Public Property JenisPekerjaanID As String
    Public Property NamaPers As String
    Public Property JenisIndustriID As String
    Public Property KeteranganUsaha As String    
    Public Property KaryawanSejak As Short
    Public Property KaryawanSejakTahun As String
    Public Property PenghasilanTetap As Double
    Public Property PenghasilanTambahan As Double


    Public Property TempatLahirSI As String
    Public Property TglLahirSI As DateTime
    Public Property NoDokumentSI As String
    Public Property MasaBerlakuKTPSI As DateTime 
    Public Property BeroperasiSejakSI As Short
    Public Property MasaKerjaSI As Short
    Public Property UsahaLainnyaSI As String
    Public Property JenisDokumenSI As String

    Public Property AlamatKTP As Parameter.Address


    Public Property PCSIAddress As Parameter.Address
End Class
