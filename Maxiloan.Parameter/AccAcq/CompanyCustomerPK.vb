﻿<Serializable()>
Public Class CompanyCustomerPK : Inherits Maxiloan.Parameter.Common
	Private _SeqNo As String
	Private _CustomerId As String
	Private _TglLaporanKeuangan As DateTime
	Private _AssetIDR As Double
	Private _AssetLancar As Double
	Private _KasSetaraKasAsetLancar As Double
	Private _PiutangUsahAsetLancar As Double
	Private _InvestasiLainnyaAsetLancar As Double
	Private _AsetLancarLainnya As Double
	Private _AsetTidakLancar As Double
	Private _PiutangUsahaAsetTidakLancar As Double
	Private _InvestasiLainnyaAsetTidakLancar As Double
	Private _AsetTidakLancarLainnya As Double
	Private _Liabilitas As Double
	Private _LiabilitasJangkaPendek As Double
	Private _PinjamanJangkaPendek As Double
	Private _UtangUsahaJangkaPendek As Double
	Private _LiabilitasJangkaPendekLainnya As Double
	Private _LiabilitasJangkaPanjang As Double
	Private _PinjamanJangkaPanjang As Double
	Private _UtangUsahaJangkaPanjang As Double
	Private _LiabilitasJangkaPanjangLainnya As Double
	Private _Ekuitas As Double
	Private _PendapatanUsahaOpr As Double
	Private _BebanPokokPendapatanOpr As Double
	Private _LabaRugiBruto As Double
	Private _PLLNonOpr As Double
	Private _BebanLainLainNonOpr As Double
	Private _LabaRugiSebelumPajak As Double
	Private _LabaRugiTahunBerjalan As Double

	Private _listdata As New DataTable
	Private _CustomerType As String

	Public Err As String

	Public Property CustomerId As String
		Get
			Return _CustomerId
		End Get
		Set(value As String)
			_CustomerId = value
		End Set
	End Property

	Public Property TglLaporanKeuangan As Date
		Get
			Return _TglLaporanKeuangan
		End Get
		Set(value As Date)
			_TglLaporanKeuangan = value
		End Set
	End Property

	Public Property AssetIDR As Double
		Get
			Return _AssetIDR
		End Get
		Set(value As Double)
			_AssetIDR = value
		End Set
	End Property

	Public Property AssetLancar As Double
		Get
			Return _AssetLancar
		End Get
		Set(value As Double)
			_AssetLancar = value
		End Set
	End Property

	Public Property KasSetaraKasAsetLancar As Double
		Get
			Return _KasSetaraKasAsetLancar
		End Get
		Set(value As Double)
			_KasSetaraKasAsetLancar = value
		End Set
	End Property

	Public Property PiutangUsahAsetLancar As Double
		Get
			Return _PiutangUsahAsetLancar
		End Get
		Set(value As Double)
			_PiutangUsahAsetLancar = value
		End Set
	End Property

	Public Property InvestasiLainnyaAsetLancar As Double
		Get
			Return _InvestasiLainnyaAsetLancar
		End Get
		Set(value As Double)
			_InvestasiLainnyaAsetLancar = value
		End Set
	End Property

	Public Property AsetLancarLainnya As Double
		Get
			Return _AsetLancarLainnya
		End Get
		Set(value As Double)
			_AsetLancarLainnya = value
		End Set
	End Property

	Public Property AsetTidakLancar As Double
		Get
			Return _AsetTidakLancar
		End Get
		Set(value As Double)
			_AsetTidakLancar = value
		End Set
	End Property

	Public Property PiutangUsahaAsetTidakLancar As Double
		Get
			Return _PiutangUsahaAsetTidakLancar
		End Get
		Set(value As Double)
			_PiutangUsahaAsetTidakLancar = value
		End Set
	End Property

	Public Property InvestasiLainnyaAsetTidakLancar As Double
		Get
			Return _InvestasiLainnyaAsetTidakLancar
		End Get
		Set(value As Double)
			_InvestasiLainnyaAsetTidakLancar = value
		End Set
	End Property

	Public Property AsetTidakLancarLainnya As Double
		Get
			Return _AsetTidakLancarLainnya
		End Get
		Set(value As Double)
			_AsetTidakLancarLainnya = value
		End Set
	End Property

	Public Property Liabilitas As Double
		Get
			Return _Liabilitas
		End Get
		Set(value As Double)
			_Liabilitas = value
		End Set
	End Property

	Public Property LiabilitasJangkaPendek As Double
		Get
			Return _LiabilitasJangkaPendek
		End Get
		Set(value As Double)
			_LiabilitasJangkaPendek = value
		End Set
	End Property

	Public Property PinjamanJangkaPendek As Double
		Get
			Return _PinjamanJangkaPendek
		End Get
		Set(value As Double)
			_PinjamanJangkaPendek = value
		End Set
	End Property

	Public Property UtangUsahaJangkaPendek As Double
		Get
			Return _UtangUsahaJangkaPendek
		End Get
		Set(value As Double)
			_UtangUsahaJangkaPendek = value
		End Set
	End Property

	Public Property LiabilitasJangkaPendekLainnya As Double
		Get
			Return _LiabilitasJangkaPendekLainnya
		End Get
		Set(value As Double)
			_LiabilitasJangkaPendekLainnya = value
		End Set
	End Property

	Public Property LiabilitasJangkaPanjang As Double
		Get
			Return _LiabilitasJangkaPanjang
		End Get
		Set(value As Double)
			_LiabilitasJangkaPanjang = value
		End Set
	End Property

	Public Property PinjamanJangkaPanjang As Double
		Get
			Return _PinjamanJangkaPanjang
		End Get
		Set(value As Double)
			_PinjamanJangkaPanjang = value
		End Set
	End Property

	Public Property UtangUsahaJangkaPanjang As Double
		Get
			Return _UtangUsahaJangkaPanjang
		End Get
		Set(value As Double)
			_UtangUsahaJangkaPanjang = value
		End Set
	End Property

	Public Property LiabilitasJangkaPanjangLainnya As Double
		Get
			Return _LiabilitasJangkaPanjangLainnya
		End Get
		Set(value As Double)
			_LiabilitasJangkaPanjangLainnya = value
		End Set
	End Property

	Public Property Ekuitas As Double
		Get
			Return _Ekuitas
		End Get
		Set(value As Double)
			_Ekuitas = value
		End Set
	End Property

	Public Property PendapatanUsahaOpr As Double
		Get
			Return _PendapatanUsahaOpr
		End Get
		Set(value As Double)
			_PendapatanUsahaOpr = value
		End Set
	End Property

	Public Property BebanPokokPendapatanOpr As Double
		Get
			Return _BebanPokokPendapatanOpr
		End Get
		Set(value As Double)
			_BebanPokokPendapatanOpr = value
		End Set
	End Property

	Public Property LabaRugiBruto As Double
		Get
			Return _LabaRugiBruto
		End Get
		Set(value As Double)
			_LabaRugiBruto = value
		End Set
	End Property

	Public Property PLLNonOpr As Double
		Get
			Return _PLLNonOpr
		End Get
		Set(value As Double)
			_PLLNonOpr = value
		End Set
	End Property

	Public Property BebanLainLainNonOpr As Double
		Get
			Return _BebanLainLainNonOpr
		End Get
		Set(value As Double)
			_BebanLainLainNonOpr = value
		End Set
	End Property

	Public Property LabaRugiSebelumPajak As Double
		Get
			Return _LabaRugiSebelumPajak
		End Get
		Set(value As Double)
			_LabaRugiSebelumPajak = value
		End Set
	End Property

	Public Property LabaRugiTahunBerjalan As Double
		Get
			Return _LabaRugiTahunBerjalan
		End Get
		Set(value As Double)
			_LabaRugiTahunBerjalan = value
		End Set
	End Property

	Public Property Listdata As DataTable
		Get
			Return _listdata
		End Get
		Set(value As DataTable)
			_listdata = value
		End Set
	End Property

	Public Property CustomerType As String
		Get
			Return _CustomerType
		End Get
		Set(value As String)
			_CustomerType = value
		End Set
	End Property

	Public Property SeqNo As String
		Get
			Return _SeqNo
		End Get
		Set(value As String)
			_SeqNo = value
		End Set
	End Property
End Class
