
<Serializable()> _
Public Class StopAccrued : Inherits Maxiloan.Parameter.FullPrepay
    Private _pastduedays As Integer
    Private _lastpayment As Date
    Private _requestnona As String
    Private _natype As String
    Private _approvalstatus As String

    Public Property PastDueDays() As Integer
        Get
            Return _pastduedays
        End Get
        Set(ByVal Value As Integer)
            _pastduedays = Value
        End Set
    End Property

    Public Property LastPayment() As Date
        Get
            Return _lastpayment
        End Get
        Set(ByVal Value As Date)
            _lastpayment = Value
        End Set
    End Property

    Public Property RequestNoNa() As String
        Get
            Return _requestnona
        End Get
        Set(ByVal Value As String)
            _requestnona = Value
        End Set
    End Property

    Public Property NAType() As String
        Get
            Return _natype
        End Get
        Set(ByVal Value As String)
            _natype = Value
        End Set
    End Property

    Public Property ApprovalStatus() As String
        Get
            Return _approvalstatus
        End Get
        Set(ByVal Value As String)
            _approvalstatus = Value
        End Set
    End Property
End Class
