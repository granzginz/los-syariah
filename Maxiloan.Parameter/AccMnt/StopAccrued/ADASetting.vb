
<Serializable()> _
Public Class ADASetting : Inherits Maxiloan.Parameter.FullPrepay

#Region "Constanta"

    Private _Bucket1From As Integer
    Private _Bucket2From As Integer
    Private _Bucket3From As Integer
    Private _Bucket4From As Integer
    Private _Bucket5From As Integer
    Private _Bucket6From As Integer
    Private _Bucket7From As Integer
    Private _Bucket8From As Integer
    Private _Bucket9From As Integer
    Private _Bucket10From As Integer

    Private _Bucket1To As Integer
    Private _Bucket2To As Integer
    Private _Bucket3To As Integer
    Private _Bucket4To As Integer
    Private _Bucket5To As Integer
    Private _Bucket6To As Integer
    Private _Bucket7To As Integer
    Private _Bucket8To As Integer
    Private _Bucket9To As Integer
    Private _Bucket10To As Integer

    Private _Bucket1Rate As Decimal
    Private _Bucket2Rate As Decimal
    Private _Bucket3Rate As Decimal
    Private _Bucket4Rate As Decimal
    Private _Bucket5Rate As Decimal
    Private _Bucket6Rate As Decimal
    Private _Bucket7Rate As Decimal
    Private _Bucket8Rate As Decimal
    Private _Bucket9Rate As Decimal
    Private _Bucket10Rate As Decimal

    Private _SPName As String

#End Region

    Public Property SPName() As String
        Get
            Return _SPName
        End Get
        Set(ByVal Value As String)
            _SPName = Value
        End Set
    End Property


#Region "Property BucketFrom"

    Public Property Bucket1From() As Integer
        Get
            Return _Bucket1From
        End Get
        Set(ByVal Value As Integer)
            _Bucket1From = Value
        End Set
    End Property
    Public Property Bucket2From() As Integer
        Get
            Return _Bucket2From
        End Get
        Set(ByVal Value As Integer)
            _Bucket2From = Value
        End Set
    End Property
    Public Property Bucket3From() As Integer
        Get
            Return _Bucket3From
        End Get
        Set(ByVal Value As Integer)
            _Bucket3From = Value
        End Set
    End Property
    Public Property Bucket4From() As Integer
        Get
            Return _Bucket4From
        End Get
        Set(ByVal Value As Integer)
            _Bucket4From = Value
        End Set
    End Property
    Public Property Bucket5From() As Integer
        Get
            Return _Bucket5From
        End Get
        Set(ByVal Value As Integer)
            _Bucket5From = Value
        End Set
    End Property
    Public Property Bucket6From() As Integer
        Get
            Return _Bucket6From
        End Get
        Set(ByVal Value As Integer)
            _Bucket6From = Value
        End Set
    End Property


    Public Property Bucket7From() As Integer
        Get
            Return _Bucket7From
        End Get
        Set(ByVal Value As Integer)
            _Bucket7From = Value
        End Set
    End Property



    Public Property Bucket8From() As Integer
        Get
            Return _Bucket8From
        End Get
        Set(ByVal Value As Integer)
            _Bucket8From = Value
        End Set
    End Property
    Public Property Bucket9From() As Integer
        Get
            Return _Bucket9From
        End Get
        Set(ByVal Value As Integer)
            _Bucket9From = Value
        End Set
    End Property
    Public Property Bucket10From() As Integer
        Get
            Return _Bucket10From
        End Get
        Set(ByVal Value As Integer)
            _Bucket10From = Value
        End Set
    End Property


#End Region

#Region "Property BucketTO"

    Public Property Bucket1To() As Integer
        Get
            Return _Bucket1To
        End Get
        Set(ByVal Value As Integer)
            _Bucket1To = Value
        End Set
    End Property
    Public Property Bucket2To() As Integer
        Get
            Return _Bucket2To
        End Get
        Set(ByVal Value As Integer)
            _Bucket2To = Value
        End Set
    End Property
    Public Property Bucket3To() As Integer
        Get
            Return _Bucket3To
        End Get
        Set(ByVal Value As Integer)
            _Bucket3To = Value
        End Set
    End Property
    Public Property Bucket4To() As Integer
        Get
            Return _Bucket4To
        End Get
        Set(ByVal Value As Integer)
            _Bucket4To = Value
        End Set
    End Property
    Public Property Bucket5To() As Integer
        Get
            Return _Bucket5To
        End Get
        Set(ByVal Value As Integer)
            _Bucket5To = Value
        End Set
    End Property
    Public Property Bucket6To() As Integer
        Get
            Return _Bucket6To
        End Get
        Set(ByVal Value As Integer)
            _Bucket6To = Value
        End Set
    End Property


    Public Property Bucket7To() As Integer
        Get
            Return _Bucket7To
        End Get
        Set(ByVal Value As Integer)
            _Bucket7To = Value
        End Set
    End Property
    Public Property Bucket8To() As Integer
        Get
            Return _Bucket8To
        End Get
        Set(ByVal Value As Integer)
            _Bucket8To = Value
        End Set
    End Property
    Public Property Bucket9To() As Integer
        Get
            Return _Bucket9To
        End Get
        Set(ByVal Value As Integer)
            _Bucket9To = Value
        End Set
    End Property
    Public Property Bucket10To() As Integer
        Get
            Return _Bucket10To
        End Get
        Set(ByVal Value As Integer)
            _Bucket10To = Value
        End Set
    End Property
#End Region

#Region " BucketRate "

    Public Property Bucket1Rate() As Decimal
        Get
            Return _Bucket1Rate
        End Get
        Set(ByVal Value As Decimal)
            _Bucket1Rate = Value
        End Set
    End Property
    Public Property Bucket2Rate() As Decimal
        Get
            Return _Bucket2Rate
        End Get
        Set(ByVal Value As Decimal)
            _Bucket2Rate = Value
        End Set
    End Property
    Public Property Bucket3Rate() As Decimal
        Get
            Return _Bucket3Rate
        End Get
        Set(ByVal Value As Decimal)
            _Bucket3Rate = Value
        End Set
    End Property
    Public Property Bucket4Rate() As Decimal
        Get
            Return _Bucket4Rate
        End Get
        Set(ByVal Value As Decimal)
            _Bucket4Rate = Value
        End Set
    End Property
    Public Property Bucket5Rate() As Decimal
        Get
            Return _Bucket5Rate
        End Get
        Set(ByVal Value As Decimal)
            _Bucket5Rate = Value
        End Set
    End Property
    Public Property Bucket6Rate() As Decimal
        Get
            Return _Bucket6Rate
        End Get
        Set(ByVal Value As Decimal)
            _Bucket6Rate = Value
        End Set
    End Property

    Public Property Bucket7Rate() As Decimal
        Get
            Return _Bucket7Rate
        End Get
        Set(ByVal Value As Decimal)
            _Bucket7Rate = Value
        End Set
    End Property
    Public Property Bucket8Rate() As Decimal
        Get
            Return _Bucket8Rate
        End Get
        Set(ByVal Value As Decimal)
            _Bucket8Rate = Value
        End Set
    End Property
    Public Property Bucket9Rate() As Decimal
        Get
            Return _Bucket9Rate
        End Get
        Set(ByVal Value As Decimal)
            _Bucket9Rate = Value
        End Set
    End Property
    Public Property Bucket10Rate() As Decimal
        Get
            Return _Bucket10Rate
        End Get
        Set(ByVal Value As Decimal)
            _Bucket10Rate = Value
        End Set
    End Property

    Dim _AidaRate As Decimal
    Public Property Ayda1Rate() As Decimal
        Get
            Return _AidaRate
        End Get
        Set(ByVal Value As Decimal)
            _AidaRate = Value
        End Set
    End Property
    Dim _Aida2Rate As Decimal
    Public Property Ayda2Rate() As Decimal
        Get
            Return _Aida2Rate
        End Get
        Set(ByVal Value As Decimal)
            _Aida2Rate = Value
        End Set
    End Property
#End Region

End Class
