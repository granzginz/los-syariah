

<Serializable()> _
Public Class WriteOff : Inherits Maxiloan.Parameter.FullPrepay

    Private _SpName As String
    Private _ApplicationID As String
    Private _ListData As DataTable
    Private _totalrecords As Int64
    Private _ListDataReport As DataSet
    Private _NADate As Date
    Private _NAAMount As Decimal
    Private _pastduedays As Integer
    Private _lastpayment As Date
    Private _totalamount As Double
    Private _requestby As String
    Private _approvalstatus As String
    Private _PotensiTagih As String

    Public Property ApprovalStatus() As String
        Get
            Return _approvalstatus
        End Get
        Set(ByVal Value As String)
            _approvalstatus = Value
        End Set
    End Property
    Public Property RequestBy() As String
        Get
            Return _requestby
        End Get
        Set(ByVal Value As String)
            _requestby = Value
        End Set
    End Property
    Public Property TotalAmount() As Double
        Get
            Return _totalamount
        End Get
        Set(ByVal Value As Double)
            _totalamount = Value
        End Set
    End Property
    Public Property PastDueDays() As Integer
        Get
            Return _pastduedays
        End Get
        Set(ByVal Value As Integer)
            _pastduedays = Value
        End Set
    End Property
    Public Property LastPayment() As Date
        Get
            Return _lastpayment
        End Get
        Set(ByVal Value As Date)
            _lastpayment = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property ListDataReport() As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(ByVal Value As DataSet)
            _ListDataReport = Value
        End Set
    End Property


    Public Property SpName() As String
        Get
            Return _SpName
        End Get
        Set(ByVal Value As String)
            _SpName = Value
        End Set
    End Property

    Public Property PotensiTagih() As String
        Get
            Return _PotensiTagih
        End Get
        Set(ByVal Value As String)
            _PotensiTagih = Value
        End Set
    End Property
End Class
