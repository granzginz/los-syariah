
<Serializable()> _
Public Class AssetReplacement : Inherits Maxiloan.Parameter.AccMntBase
    Private _SpName As String
    Private _serialno1 As String
    Private _serialno2 As String
    Private _totalrecords As Int64
    Private _AssetID As String
    Private _AssetTypeID As String
    Private _ProductID As String
    Private _SupplierID As String
    Private _Serial1 As String
    Private _Serial2 As String
    Private _listdata As DataTable
    Private _Where As String
    Private _Table As String
    Private _AssetDocID As String
    Private _SupplierName As String
    Private _Output As String
    Private _Input As String
    Private _AOID As String
    Private _AssetCode As String
    Private _AssetUsage As String
    Private _CAID As String
    Private _DP As Decimal
    Private _OTR As Decimal
    Private _UsedNew As String
    Private _ManufacturingYear As Integer
    Private _OldOwnerAsset As String
    Private _TaxDate As Date
    Private _InsuredBy As String
    Private _PaidBy As String
    Private _Notes As String
    Private _SalesmanID As String
    Private _SurveyorID As String
    Private _SalesSupervisorID As String
    Private _SupplierAdminID As String
    Private _IsIncentiveSupplier As String
    Private _DateEntryAssetData As Date
    Private _CustomerID As String
    Private _DataAttribute As DataTable
    Private _DataAssetdoc As DataTable
    Private _ProducOfferingID As String
    Private _ProducID As String
    Private _AssetReqNo As Integer
    Private _assetseqNo As Integer
    Private _requestdate As DateTime
    Private _OwnerAsset As String
    Private _AssetReplacementPaid As Integer
    Private _issendbyfax As Integer
    Private _issendbyemail As Integer
    Private _issendbysms As Integer
    Private _approvalorderid As String
    Private _approvaldate As DateTime
    Private _approvalstatus As String
    Private _voucherNo As String
    Private _approvalby As String
    Private _InsuranceStatus As String
    Private _approvalrecomendation As String
    Private _ManufacturingMonth As Integer
    Private _AssetReplacementFeePaid As Double
    Private _AssetReplacementFee As Double
    Private _description As String
    Private _insurancecoy As String
    Private _lks As String
    Private _license As String
    Private _claimdate As Date
    Private _claimamount As Double
    Private _usage As String
    Private _bulan As String
    Private _year As Integer
    Private _name As String
    Private _kelurahan As String
    Private _kecamatan As String
    Private _city As String
    Private _zipcode As String
    Private _rt As String
    Private _rw As String
    Private _areaphone1 As String
    Private _phone1 As String
    Private _areaphone2 As String
    Private _phone2 As String
    Private _areafax As String
    Private _fax As String
    Private _ListAttribute As DataTable
    Private _AdmFee As Double
    Private _ucNotes As String
    Private _AssetCodeID As String
    Private _ismaindoc As Integer
    Private _isdocexist As Integer
    Private _documentno As String
    Private _attributeID As String
    Private _attributecontent As String
    Private _listreplacement As DataSet

    Public Property ListReplacement() As DataSet
        Get
            Return (CType(_listreplacement, DataSet))
        End Get
        Set(ByVal Value As DataSet)
            _listreplacement = Value
        End Set
    End Property
    Public Property ListAttribute() As DataTable
        Get
            Return _ListAttribute
        End Get
        Set(ByVal Value As DataTable)
            _ListAttribute = Value
        End Set
    End Property
    Public Property SerialNo1() As String
        Get
            Return _serialno1
        End Get
        Set(ByVal Value As String)
            _serialno1 = Value
        End Set
    End Property
    Public Property SerialNo2() As String
        Get
            Return _serialno2
        End Get
        Set(ByVal Value As String)
            _serialno2 = Value
        End Set
    End Property
    Public Property Rt() As String
        Get
            Return _rt
        End Get
        Set(ByVal Value As String)
            _rt = Value
        End Set
    End Property
    Public Property Rw() As String
        Get
            Return _rw
        End Get
        Set(ByVal Value As String)
            _rw = Value
        End Set
    End Property
    Public Property AreaPhone1() As String
        Get
            Return _areaphone1
        End Get
        Set(ByVal Value As String)
            _areaphone1 = Value
        End Set
    End Property
    Public Property Phone1() As String
        Get
            Return _phone1
        End Get
        Set(ByVal Value As String)
            _phone1 = Value
        End Set
    End Property
    Public Property AreaPhone2() As String
        Get
            Return _areaphone2
        End Get
        Set(ByVal Value As String)
            _areaphone2 = Value
        End Set
    End Property
    Public Property Phone2() As String
        Get
            Return _phone2
        End Get
        Set(ByVal Value As String)
            _phone2 = Value
        End Set
    End Property
    Public Property AreaFax() As String
        Get
            Return _areafax
        End Get
        Set(ByVal Value As String)
            _areafax = Value
        End Set
    End Property
    Public Property Fax() As String
        Get
            Return _fax
        End Get
        Set(ByVal Value As String)
            _fax = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal Value As String)
            _name = Value
        End Set
    End Property
    Public Property Kelurahan() As String
        Get
            Return _kelurahan
        End Get
        Set(ByVal Value As String)
            _kelurahan = Value
        End Set
    End Property
    Public Property Kecamatan() As String
        Get
            Return _kecamatan
        End Get
        Set(ByVal Value As String)
            _kecamatan = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return _city
        End Get
        Set(ByVal Value As String)
            _city = Value
        End Set
    End Property
    Public Property ZipCode() As String
        Get
            Return _zipcode
        End Get
        Set(ByVal Value As String)
            _zipcode = Value
        End Set
    End Property
    Public Property Year() As Integer
        Get
            Return _year
        End Get
        Set(ByVal Value As Integer)
            _year = Value
        End Set
    End Property
    Public Property Bulan() As String
        Get
            Return _bulan
        End Get
        Set(ByVal Value As String)
            _bulan = Value
        End Set
    End Property
    Public Property Usage() As String
        Get
            Return _usage
        End Get
        Set(ByVal Value As String)
            _usage = Value
        End Set
    End Property
    Public Property AdmFee() As Double
        Get
            Return (CType(_AdmFee, Double))
        End Get
        Set(ByVal Value As Double)
            _AdmFee = Value
        End Set
    End Property
    Public Property ClaimAmount() As Double
        Get
            Return (CType(_claimamount, Double))
        End Get
        Set(ByVal Value As Double)
            _claimamount = Value
        End Set
    End Property
    Public Property ClaimDate() As Date
        Get
            Return _claimdate
        End Get
        Set(ByVal Value As Date)
            _claimdate = Value
        End Set
    End Property
    Public Property License() As String
        Get
            Return _license
        End Get
        Set(ByVal Value As String)
            _license = Value
        End Set
    End Property
    Public Property LKS() As String
        Get
            Return _lks
        End Get
        Set(ByVal Value As String)
            _lks = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property insurancecoy() As String
        Get
            Return _insurancecoy
        End Get
        Set(ByVal Value As String)
            _insurancecoy = Value
        End Set
    End Property
    Public Property ucNotes() As String
        Get
            Return _ucNotes
        End Get
        Set(ByVal Value As String)
            _ucNotes = Value
        End Set
    End Property
    Public Property approvalby() As String
        Get
            Return _approvalby
        End Get
        Set(ByVal Value As String)
            _approvalby = Value
        End Set
    End Property
    Public Property InsuranceStatus() As String
        Get
            Return _InsuranceStatus
        End Get
        Set(ByVal Value As String)
            _InsuranceStatus = Value
        End Set
    End Property
    Public Property approvalrecomendation() As String
        Get
            Return _approvalrecomendation
        End Get
        Set(ByVal Value As String)
            _approvalrecomendation = Value
        End Set
    End Property
    Public Property ManufacturingMonth() As Integer
        Get
            Return _ManufacturingMonth
        End Get
        Set(ByVal Value As Integer)
            _ManufacturingMonth = Value
        End Set
    End Property
    Public Property AssetReplacementFee() As Double
        Get
            Return _AssetReplacementFee
        End Get
        Set(ByVal Value As Double)
            _AssetReplacementFee = Value
        End Set
    End Property
    Public Property AssetReplacementFeePaid() As Double
        Get
            Return _AssetReplacementFeePaid
        End Get
        Set(ByVal Value As Double)
            _AssetReplacementFeePaid = Value
        End Set
    End Property
    Public Property IsSendByFax() As Integer
        Get
            Return _issendbyfax
        End Get
        Set(ByVal Value As Integer)
            _issendbyfax = Value
        End Set
    End Property
    Public Property IsSendByEmail() As Integer
        Get
            Return _issendbyemail
        End Get
        Set(ByVal Value As Integer)
            _issendbyemail = Value
        End Set
    End Property
    Public Property issendbysms() As Integer
        Get
            Return _issendbysms
        End Get
        Set(ByVal Value As Integer)
            _issendbysms = Value
        End Set
    End Property

    Public Property approvalorderid() As String
        Get
            Return _approvalorderid
        End Get
        Set(ByVal Value As String)
            _approvalorderid = Value
        End Set
    End Property
    Public Property approvaldate() As DateTime
        Get
            Return _approvaldate
        End Get
        Set(ByVal Value As DateTime)
            _approvaldate = Value
        End Set
    End Property
    Public Property approvalstatus() As String
        Get
            Return _approvalstatus
        End Get
        Set(ByVal Value As String)
            _approvalstatus = Value
        End Set
    End Property
    Public Property voucherNo() As String
        Get
            Return _voucherNo
        End Get
        Set(ByVal Value As String)
            _voucherNo = Value
        End Set
    End Property
    Public Property OwnerAsset() As String
        Get
            Return _OwnerAsset
        End Get
        Set(ByVal Value As String)
            _OwnerAsset = Value
        End Set
    End Property

    Public Property AssetReqNo() As Integer
        Get
            Return _AssetReqNo
        End Get
        Set(ByVal Value As Integer)
            _AssetReqNo = Value
        End Set
    End Property

    Public Property assetseqNo() As Integer
        Get
            Return _assetseqNo
        End Get
        Set(ByVal Value As Integer)
            _assetseqNo = Value
        End Set
    End Property
    Public Property requestdate() As DateTime
        Get
            Return _requestdate
        End Get
        Set(ByVal Value As DateTime)
            _requestdate = Value
        End Set
    End Property
    Public Property DataAttribute() As DataTable
        Get
            Return _DataAttribute
        End Get
        Set(ByVal Value As DataTable)
            _DataAttribute = Value
        End Set
    End Property
    Public Property DataAssetdoc() As DataTable
        Get
            Return _DataAssetdoc
        End Get
        Set(ByVal Value As DataTable)
            _DataAssetdoc = Value
        End Set
    End Property
    Public Property Where() As String
        Get
            Return _Where
        End Get
        Set(ByVal Value As String)
            _Where = Value
        End Set
    End Property
    Public Property Table() As String
        Get
            Return _Table
        End Get
        Set(ByVal Value As String)
            _Table = Value
        End Set
    End Property

    Public Property SupplierName() As String
        Get
            Return _SupplierName
        End Get
        Set(ByVal Value As String)
            _SupplierName = Value
        End Set
    End Property
    Public Property Output() As String
        Get
            Return _Output
        End Get
        Set(ByVal Value As String)
            _Output = Value
        End Set
    End Property
    Public Property Input() As String
        Get
            Return _Input
        End Get
        Set(ByVal Value As String)
            _Input = Value
        End Set
    End Property
    Public Property AOID() As String
        Get
            Return _AOID
        End Get
        Set(ByVal Value As String)
            _AOID = Value
        End Set
    End Property
    Public Property AssetCodeID() As String
        Get
            Return _AssetCodeID
        End Get
        Set(ByVal Value As String)
            _AssetCodeID = Value
        End Set
    End Property
    Public Property AssetCode() As String
        Get
            Return _AssetCode
        End Get
        Set(ByVal Value As String)
            _AssetCode = Value
        End Set
    End Property
    Public Property AssetUsage() As String
        Get
            Return _AssetUsage
        End Get
        Set(ByVal Value As String)
            _AssetUsage = Value
        End Set
    End Property
    Public Property CAID() As String
        Get
            Return _CAID
        End Get
        Set(ByVal Value As String)
            _CAID = Value
        End Set
    End Property
    Public Property DP() As Decimal
        Get
            Return _DP
        End Get
        Set(ByVal Value As Decimal)
            _DP = Value
        End Set
    End Property
    Public Property OTR() As Decimal
        Get
            Return _OTR
        End Get
        Set(ByVal Value As Decimal)
            _OTR = Value
        End Set
    End Property
    Public Property UsedNew() As String
        Get
            Return _UsedNew
        End Get
        Set(ByVal Value As String)
            _UsedNew = Value
        End Set
    End Property
    Public Property ManufacturingYear() As Integer
        Get
            Return _ManufacturingYear
        End Get
        Set(ByVal Value As Integer)
            _ManufacturingYear = Value
        End Set
    End Property
    Public Property OldOwnerAsset() As String
        Get
            Return _OldOwnerAsset
        End Get
        Set(ByVal Value As String)
            _OldOwnerAsset = Value
        End Set
    End Property
    Public Property TaxDate() As Date
        Get
            Return _TaxDate
        End Get
        Set(ByVal Value As Date)
            _TaxDate = Value
        End Set
    End Property
    Public Property InsuredBy() As String
        Get
            Return _InsuredBy
        End Get
        Set(ByVal Value As String)
            _InsuredBy = Value
        End Set
    End Property
    Public Property PaidBy() As String
        Get
            Return _PaidBy
        End Get
        Set(ByVal Value As String)
            _PaidBy = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property
    Public Property SalesmanID() As String
        Get
            Return _SalesmanID
        End Get
        Set(ByVal Value As String)
            _SalesmanID = Value
        End Set
    End Property
    Public Property SurveyorID() As String
        Get
            Return _SurveyorID
        End Get
        Set(ByVal Value As String)
            _SurveyorID = Value
        End Set
    End Property
    Public Property SalesSupervisorID() As String
        Get
            Return _SalesSupervisorID
        End Get
        Set(ByVal Value As String)
            _SalesSupervisorID = Value
        End Set
    End Property
    Public Property SupplierAdminID() As String
        Get
            Return _SupplierAdminID
        End Get
        Set(ByVal Value As String)
            _SupplierAdminID = Value
        End Set
    End Property
    Public Property IsIncentiveSupplier() As String
        Get
            Return _IsIncentiveSupplier
        End Get
        Set(ByVal Value As String)
            _IsIncentiveSupplier = Value
        End Set
    End Property
    Public Property DateEntryAssetData() As Date
        Get
            Return _DateEntryAssetData
        End Get
        Set(ByVal Value As Date)
            _DateEntryAssetData = Value
        End Set
    End Property

    Public Property ProductOfferingID() As String
        Get
            Return _ProducOfferingID
        End Get
        Set(ByVal Value As String)
            _ProducOfferingID = Value
        End Set
    End Property
    Public Property IsMainDoc() As Integer
        Get
            Return _ismaindoc
        End Get
        Set(ByVal Value As Integer)
            _ismaindoc = Value
        End Set
    End Property
    Public Property IsDocExist() As Integer
        Get
            Return _isdocexist
        End Get
        Set(ByVal Value As Integer)
            _isdocexist = Value
        End Set
    End Property
    Public Property AssetDocID() As String
        Get
            Return _AssetDocID
        End Get
        Set(ByVal Value As String)
            _AssetDocID = Value
        End Set
    End Property
    Public Property DocumentNo() As String
        Get
            Return _documentno
        End Get
        Set(ByVal Value As String)
            _documentno = Value
        End Set
    End Property

    Public Property AttributeID() As String
        Get
            Return _attributeID
        End Get
        Set(ByVal Value As String)
            _attributeID = Value
        End Set
    End Property
    Public Property AttributeContent() As String
        Get
            Return _attributecontent
        End Get
        Set(ByVal Value As String)
            _attributecontent = Value
        End Set
    End Property
    Public Property AssetTypeID() As String
        Get
            Return _AssetTypeID
        End Get
        Set(ByVal Value As String)
            _AssetTypeID = Value
        End Set
    End Property
    Public Property ProductID() As String
        Get
            Return _ProductID
        End Get
        Set(ByVal Value As String)
            _ProductID = Value
        End Set
    End Property
    Public Property SupplierID() As String
        Get
            Return _SupplierID
        End Get
        Set(ByVal Value As String)
            _SupplierID = Value
        End Set
    End Property
    Public Property Serial1() As String
        Get
            Return _Serial1
        End Get
        Set(ByVal Value As String)
            _Serial1 = Value
        End Set
    End Property
    Public Property Serial2() As String
        Get
            Return _Serial2
        End Get
        Set(ByVal Value As String)
            _Serial2 = Value
        End Set
    End Property
    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property SpName() As String
        Get
            Return _SpName
        End Get
        Set(ByVal Value As String)
            _SpName = Value
        End Set
    End Property
    Public Property AssetID() As String
        Get
            Return _AssetID
        End Get
        Set(ByVal Value As String)
            _AssetID = Value
        End Set
    End Property
End Class
