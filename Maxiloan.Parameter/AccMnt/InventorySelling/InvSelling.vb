
<Serializable()> _
Public Class InvSelling : Inherits Maxiloan.Parameter.AccMntBase
    Private _listdata As DataTable
    Private _viewdata As DataTable
    Private _page As String
    Private _totalrecords As Int64
    'Private _branchid As String  
    '=============viewdata===
    Private _assetdescription As String
    Private _assettype As String
    Private _chassisno As String
    Private _engineno As String
    Private _licenseplate As String
    Private _taxdate As Date
    Private _sellingdate As Date
    Private _sellingamount As Double
    Private _buyer As String
    Private _sellingnotes As String
    Private _wop As String
    Private _notes As String
    Private _TitiapanPembeli As Double


    Public Property TitiapnPembeli As Double
        Get
            Return _TitiapanPembeli
        End Get
        Set(value As Double)
            _TitiapanPembeli = value
        End Set
    End Property

    Public Property WOP() As String
        Get
            Return _wop
        End Get
        Set(ByVal Value As String)
            _wop = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property

    Public Property AssetDescription() As String
        Get
            Return _assetdescription
        End Get
        Set(ByVal Value As String)
            _assetdescription = Value
        End Set
    End Property
    Public Property AssetType() As String
        Get
            Return _assettype
        End Get
        Set(ByVal Value As String)
            _assettype = Value
        End Set
    End Property
    Public Property ChassisNo() As String
        Get
            Return _chassisno
        End Get
        Set(ByVal Value As String)
            _chassisno = Value
        End Set
    End Property
    Public Property EngineNo() As String
        Get
            Return _engineno
        End Get
        Set(ByVal Value As String)
            _engineno = Value
        End Set
    End Property
    Public Property LicensePlate() As String
        Get
            Return _licenseplate
        End Get
        Set(ByVal Value As String)
            _licenseplate = Value
        End Set
    End Property
    Public Property TaxDate() As Date
        Get
            Return _taxdate
        End Get
        Set(ByVal Value As Date)
            _taxdate = Value
        End Set
    End Property
    Public Property SellingDate() As Date
        Get
            Return _sellingdate
        End Get
        Set(ByVal Value As Date)
            _sellingdate = Value

        End Set
    End Property
    Public Property SellingAmount() As Double
        Get
            Return _sellingamount
        End Get
        Set(ByVal Value As Double)
            _sellingamount = Value

        End Set
    End Property
    Public Property Buyer() As String
        Get
            Return _buyer
        End Get
        Set(ByVal Value As String)
            _buyer = Value
        End Set
    End Property
    Public Property SellingNotes() As String
        Get
            Return _sellingnotes
        End Get
        Set(ByVal Value As String)
            _sellingnotes = Value
        End Set
    End Property
     

    Public Property ViewData() As DataTable
        Get
            Return _viewdata
        End Get
        Set(ByVal Value As DataTable)
            _viewdata = Value
        End Set
    End Property
    Public Property Page() As String
        Get
            Return _page
        End Get
        Set(ByVal Value As String)
            _page = Value
        End Set
    End Property
    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    '=========================view===============

    Public Property RepoId As Int64
    Public Property Approval As String
End Class