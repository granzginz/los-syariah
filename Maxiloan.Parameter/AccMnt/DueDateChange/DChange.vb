
<Serializable()> _
Public Class DChange : Inherits Maxiloan.Parameter.AccMntBase
#Region "Constanta Change Due Date"
    Private _PaymentFrequency As String
    Private _ChangeDueDateFee As Double
    Private _ChangeDueDateBehaviour As String
    Private _EffectiveRate As Decimal
    Private _TotAmountToBePaid As Double
    Private _EffectiveDate As Date
    Private _ChangeNotes As String
    Private _TotInfo As Double
    Private _AdminFee As Double
    Private _InterestAmount As Double
    Private _DueDateMax As Date
    Private _RequestDate As Date
    Private _Reasondesc As String
    Private _ApprovedBy As String
    Private _statusdesc As String
    Private _SeqNo As Integer
    Private _flagDel As String
#End Region

#Region "Constanta Agreement Transfer"
    Private _ProductId As String
    Private _AgreementTransferBehav As String
    Private _CustType As String
    Private _IsCrossDefault As String
    Private _GuarantorName As String
    Private _GuarantorID As String
    Private _Data1 As DataTable
    Private _Data2 As DataTable
    Private _OldCustId As String
    Private _AgreementTransferFee As Double
    Private _ATTo As String
    Private _ATName As String
    Private _NGuarantorName As String
    Private _NGuarantorID As String
    Private _GuarantorJobTitle As String
    Private _GuarantorAddress As String
    Private _GuarantorRT As String
    Private _GuarantorRW As String
    Private _GuarantorKelurahan As String
    Private _GuarantorKecamatan As String
    Private _GuarantorCity As String
    Private _GuarantorZipCode As String
    Private _GuarantorAreaPhone1 As String
    Private _GuarantorPhone1 As String
    Private _GuarantorAreaPhone2 As String
    Private _GuarantorPhone2 As String
    Private _GuarantorAreaFax As String
    Private _GuarantorFax As String
    Private _GuarantorMobilePhone As String
    Private _GuarantorEmail As String
    Private _GuarantorNotes As String
    Private _GuarantorPenghasilan As String
    
#End Region

#Region "Constanta Rescheduling"
    Private _InterestType As String
    Private _InterestTypeDesc As String
    Private _InstallmentScheme As String
    Private _InstallmentSchemeDesc As String
    Private _FinanceType As String
    Private _FinanceTypeDesc As String
    Private _ReschedulingNo As Integer
    Private _ProductDesc As String
    Private _ProductOfferingDesc As String
    Private _NumOfInstallment As Integer
    Private _ReschedulingFee As Double
    Private _ReschedulingFeeBehaviour As String
    Private _InvoiceNo As String
    Private _InvoiceSeqNo As Int64

#End Region

#Region "Agreement Transfer"
    Public Property NGuarantorID() As String
        Get
            Return _NGuarantorID
        End Get
        Set(ByVal Value As String)
            _NGuarantorID = Value
        End Set
    End Property
    Public Property NGuarantorName() As String
        Get
            Return _NGuarantorName
        End Get
        Set(ByVal Value As String)
            _NGuarantorName = Value
        End Set
    End Property

    Public Property GuarantorJobTitle() As String
        Get
            Return _GuarantorJobTitle
        End Get
        Set(ByVal Value As String)
            _GuarantorJobTitle = Value
        End Set
    End Property
    Public Property GuarantorAddress() As String
        Get
            Return _GuarantorAddress
        End Get
        Set(ByVal Value As String)
            _GuarantorAddress = Value
        End Set
    End Property
    Public Property GuarantorRT() As String
        Get
            Return _GuarantorRT
        End Get
        Set(ByVal Value As String)
            _GuarantorRT = Value
        End Set
    End Property
    Public Property GuarantorRW() As String
        Get
            Return _GuarantorRW
        End Get
        Set(ByVal Value As String)
            _GuarantorRW = Value
        End Set
    End Property
    Public Property GuarantorKelurahan() As String
        Get
            Return _GuarantorKelurahan
        End Get
        Set(ByVal Value As String)
            _GuarantorKelurahan = Value
        End Set
    End Property

    Public Property GuarantorKecamatan() As String
        Get
            Return _GuarantorKecamatan
        End Get
        Set(ByVal Value As String)
            _GuarantorKecamatan = Value
        End Set
    End Property
    Public Property GuarantorCity() As String
        Get
            Return _GuarantorCity
        End Get
        Set(ByVal Value As String)
            _GuarantorCity = Value
        End Set
    End Property

    Public Property GuarantorZipCode() As String
        Get
            Return _GuarantorZipCode
        End Get
        Set(ByVal Value As String)
            _GuarantorZipCode = Value
        End Set
    End Property

    Public Property GuarantorAreaPhone1() As String
        Get
            Return _GuarantorAreaPhone1
        End Get
        Set(ByVal Value As String)
            _GuarantorAreaPhone1 = Value
        End Set
    End Property
    Public Property GuarantorPhone1() As String
        Get
            Return _GuarantorPhone1
        End Get
        Set(ByVal Value As String)
            _GuarantorPhone1 = Value
        End Set
    End Property
    Public Property GuarantorAreaPhone2() As String
        Get
            Return _GuarantorAreaPhone2
        End Get
        Set(ByVal Value As String)
            _GuarantorAreaPhone2 = Value
        End Set
    End Property
    Public Property GuarantorPhone2() As String
        Get
            Return _GuarantorPhone2
        End Get
        Set(ByVal Value As String)
            _GuarantorPhone2 = Value
        End Set
    End Property
    Public Property GuarantorAreaFax() As String
        Get
            Return _GuarantorAreaFax
        End Get
        Set(ByVal Value As String)
            _GuarantorAreaFax = Value
        End Set
    End Property
    Public Property GuarantorFax() As String
        Get
            Return _GuarantorFax
        End Get
        Set(ByVal Value As String)
            _GuarantorFax = Value
        End Set
    End Property
    Public Property GuarantorMobilePhone() As String
        Get
            Return _GuarantorMobilePhone
        End Get
        Set(ByVal Value As String)
            _GuarantorMobilePhone = Value
        End Set
    End Property
    Public Property GuarantorEmail() As String
        Get
            Return _GuarantorEmail
        End Get
        Set(ByVal Value As String)
            _GuarantorEmail = Value
        End Set
    End Property
    Public Property GuarantorNotes() As String
        Get
            Return _GuarantorNotes
        End Get
        Set(ByVal Value As String)
            _GuarantorNotes = Value
        End Set
    End Property
    Public Property GuarantorPenghasilan() As String
        Get
            Return _GuarantorPenghasilan
        End Get
        Set(ByVal Value As String)
            _GuarantorPenghasilan = Value
        End Set
    End Property


    Public Property ATName() As String
        Get
            Return _ATName
        End Get
        Set(ByVal Value As String)
            _ATName = Value
        End Set
    End Property

    Public Property ATTo() As String
        Get
            Return _ATTo
        End Get
        Set(ByVal Value As String)
            _ATTo = Value
        End Set
    End Property

    Public Property AgreementTransferFee() As Double
        Get
            Return _AgreementTransferFee
        End Get
        Set(ByVal Value As Double)
            _AgreementTransferFee = Value
        End Set
    End Property

    Public Property OldCustId() As String
        Get
            Return _OldCustId
        End Get
        Set(ByVal Value As String)
            _OldCustId = Value
        End Set
    End Property

    Public Property Data1() As DataTable
        Get
            Return _Data1
        End Get
        Set(ByVal Value As DataTable)
            _Data1 = Value
        End Set
    End Property

    Public Property Data2() As DataTable
        Get
            Return _Data2
        End Get
        Set(ByVal Value As DataTable)
            _Data2 = Value
        End Set
    End Property

    Public Property GuarantorID() As String
        Get
            Return _GuarantorID
        End Get
        Set(ByVal Value As String)
            _GuarantorID = Value
        End Set
    End Property
    Public Property GuarantorName() As String
        Get
            Return _GuarantorName
        End Get
        Set(ByVal Value As String)
            _GuarantorName = Value
        End Set
    End Property


    Public Property IsCrossDefault() As String
        Get
            Return _IsCrossDefault
        End Get
        Set(ByVal Value As String)
            _IsCrossDefault = Value
        End Set
    End Property
    Public Property CustType() As String
        Get
            Return _CustType
        End Get
        Set(ByVal Value As String)
            _CustType = Value
        End Set
    End Property

    Public Property AgreementTransferBehav() As String
        Get
            Return _AgreementTransferBehav
        End Get
        Set(ByVal Value As String)
            _AgreementTransferBehav = Value
        End Set
    End Property

    Public Property ProductId() As String
        Get
            Return _ProductId
        End Get
        Set(ByVal Value As String)
            _ProductId = Value
        End Set
    End Property

#End Region
#Region "Change Due Date"
    Public Property flagDel() As String
        Get
            Return _flagDel
        End Get
        Set(ByVal Value As String)
            _flagDel = Value
        End Set
    End Property
    Public Property SeqNo() As Integer
        Get
            Return _SeqNo
        End Get
        Set(ByVal Value As Integer)
            _SeqNo = Value
        End Set
    End Property

    Public Property statusdesc() As String
        Get
            Return _statusdesc
        End Get
        Set(ByVal Value As String)
            _statusdesc = Value
        End Set
    End Property

    Public Property ApprovedBy() As String
        Get
            Return _ApprovedBy
        End Get
        Set(ByVal Value As String)
            _ApprovedBy = Value
        End Set
    End Property

    Public Property RequestDate() As Date
        Get
            Return _RequestDate
        End Get
        Set(ByVal Value As Date)
            _RequestDate = Value
        End Set
    End Property


    Public Property DueDateMax() As Date
        Get
            Return _DueDateMax
        End Get
        Set(ByVal Value As Date)
            _DueDateMax = Value
        End Set
    End Property

    Public Property InterestAmount() As Double
        Get
            Return _InterestAmount
        End Get
        Set(ByVal Value As Double)
            _InterestAmount = Value
        End Set
    End Property

    Public Property TotInfo() As Double
        Get
            Return _TotInfo
        End Get
        Set(ByVal Value As Double)
            _TotInfo = Value
        End Set
    End Property
    Public Property AdminFee() As Double
        Get
            Return _AdminFee
        End Get
        Set(ByVal Value As Double)
            _AdminFee = Value
        End Set
    End Property

    Public Property ChangeNotes() As String
        Get
            Return _ChangeNotes
        End Get
        Set(ByVal Value As String)
            _ChangeNotes = Value
        End Set
    End Property

    Public Property EffectiveDate() As Date
        Get
            Return _EffectiveDate
        End Get
        Set(ByVal Value As Date)
            _EffectiveDate = Value
        End Set
    End Property

    Public Property TotAmountToBePaid() As Double
        Get
            Return _TotAmountToBePaid
        End Get
        Set(ByVal Value As Double)
            _TotAmountToBePaid = Value
        End Set
    End Property

    Public Property PaymentFrequency() As String
        Get
            Return _PaymentFrequency
        End Get
        Set(ByVal Value As String)
            _PaymentFrequency = Value
        End Set
    End Property

    Public Property ChangeDueDateFee() As Double
        Get
            Return _ChangeDueDateFee
        End Get
        Set(ByVal Value As Double)
            _ChangeDueDateFee = Value
        End Set
    End Property
    Public Property ChangeDueDateBehaviour() As String
        Get
            Return _ChangeDueDateBehaviour
        End Get
        Set(ByVal Value As String)
            _ChangeDueDateBehaviour = Value
        End Set
    End Property

    Public Property EffectiveRate() As Decimal
        Get
            Return _EffectiveRate
        End Get
        Set(ByVal Value As Decimal)
            _EffectiveRate = Value
        End Set
    End Property
#End Region

#Region "Rescheduling"
    Public Property NumOfInstallment() As Integer
        Get
            Return _NumOfInstallment
        End Get
        Set(ByVal Value As Integer)
            _NumOfInstallment = Value
        End Set
    End Property

    Public Property ProductOfferingDesc() As String
        Get
            Return _ProductOfferingDesc
        End Get
        Set(ByVal Value As String)
            _ProductOfferingDesc = Value
        End Set
    End Property
    Public Property ReschedulingFeeBehaviour() As String
        Get
            Return _ReschedulingFeeBehaviour
        End Get
        Set(ByVal Value As String)
            _ReschedulingFeeBehaviour = Value
        End Set
    End Property
    Public Property ProductDesc() As String
        Get
            Return _ProductDesc
        End Get
        Set(ByVal Value As String)
            _ProductDesc = Value
        End Set
    End Property

    Public Property ReschedulingFee() As Double
        Get
            Return _ReschedulingFee
        End Get
        Set(ByVal Value As Double)
            _ReschedulingFee = Value
        End Set
    End Property
    Public Property ReschedulingNo() As Integer
        Get
            Return _ReschedulingNo
        End Get
        Set(ByVal Value As Integer)
            _ReschedulingNo = Value
        End Set
    End Property

    Public Property FinanceType() As String
        Get
            Return _FinanceType
        End Get
        Set(ByVal Value As String)
            _FinanceType = Value
        End Set
    End Property

    Public Property FinanceTypeDesc() As String
        Get
            Return _FinanceTypeDesc
        End Get
        Set(ByVal Value As String)
            _FinanceTypeDesc = Value
        End Set
    End Property

    Public Property InstallmentScheme() As String
        Get
            Return _InstallmentScheme
        End Get
        Set(ByVal Value As String)
            _InstallmentScheme = Value
        End Set
    End Property

    Public Property InstallmentSchemeDesc() As String
        Get
            Return _InstallmentSchemeDesc
        End Get
        Set(ByVal Value As String)
            _InstallmentSchemeDesc = Value
        End Set
    End Property

    Public Property InterestType() As String
        Get
            Return _InterestType
        End Get
        Set(ByVal Value As String)
            _InterestType = Value
        End Set
    End Property

    Public Property InterestTypeDesc() As String
        Get
            Return _InterestTypeDesc
        End Get
        Set(ByVal Value As String)
            _InterestTypeDesc = Value
        End Set
    End Property

    Public Property InvoiceNo() As String
        Get
            Return _InvoiceNo
        End Get
        Set(ByVal Value As String)
            _InvoiceNo = Value
        End Set
    End Property

    Public Property InvoiceSeqNo() As Int64
        Get
            Return _InvoiceSeqNo
        End Get
        Set(ByVal Value As Int64)
            _InvoiceSeqNo = Value
        End Set
    End Property

#End Region
    Public Property NextDueDate As Date
    Public Property FacilityId As String
    Public Property OldDueDate As Date
    Public Property DueDateAwal As Date

End Class

