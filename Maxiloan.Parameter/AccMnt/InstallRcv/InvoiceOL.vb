

<Serializable()> _
Public Class InvoiceOL : Inherits Maxiloan.Parameter.Common

    Public Property ApplicationID As String
    Public Property InvoiceNo As String
    Public Property InvoiceDate As Date
    Public Property InvoiceDueDate As Date
    Public Property Attn As String
    Public Property SeqNo As Integer
    Public Property Description As String
    Public Property BasicLease As Decimal
    Public Property VATPercentage As Decimal
    Public Property VATAmount As Decimal
    Public Property WHTPercentage As Decimal
    Public Property WHTAmount As Decimal
    Public Property BillAmount As Decimal
    Public Property PaidAmount As Decimal
    Public Property PaidStatus As String
    Public Property strError As String
    Public Property BankAccountID As String
    Public Property PaymentDate As Date
    Public Property ListData As DataTable
    Public Property TotalRecords As Int64
    Public Property BayarDenda As String

End Class

