﻿<Serializable()>
Public Class InstallRcv3rdTempTable
    Public Property BranchId As String
    Public Property ApplicationId As String
    Public Property LogID As String
    Public Property PaymentAllocationID As String
    Public Property Post As String
    Public Property Amount As Double
    Public Property RefDesc As String
    Public Property DepartementID As String
    Public Property VoucherDesc As String
     
End Class

<Serializable()>
Public Class InstallRcv3rdUpladedLog
    Private _Branchid As String
    Private _ApplicationID As String
    Private _InstallmentAmount As Double
    Private _Customerid As String
    Private _InsntNo As Integer
    Private _InstDate As Date
    Private _customerName As String
    Private _logId As String
    '' setting payementGateWay
    Private _PaymentGateway3rd As PaymentGateway3rd

    '' temp table yang yang akan di generate
    '' (journalnya)
    Private _InstallRcv3rdTempTable As IList(Of InstallRcv3rdTempTable)

    Public Sub New(mBranchid As String, mApplicationID As String, mInstallmentAmount As Double, mCustomerid As String, mInsntNo As Integer, mInstDate As Date, mCustomerName As String, mLogId As String)
        _Branchid = mBranchid
        _ApplicationID = mApplicationID
        _InstallmentAmount = mInstallmentAmount
        _Customerid = mCustomerid
        _InsntNo = mInsntNo
        _InstDate = mInstDate
        _customerName = mCustomerName
        _logId = mLogId
    End Sub
    Function Build(appId As String, post As String, amount As Double) As InstallRcv3rdTempTable
        Return New InstallRcv3rdTempTable With {
            .PaymentAllocationID = appId,
            .Post = post,
            .Amount = amount,
            .RefDesc = String.Format("{0} - {1}", _ApplicationID, _customerName),
            .VoucherDesc = "-",
            .DepartementID = "",
            .LogID = _logId,
            .BranchId = _Branchid,
            .ApplicationId = _ApplicationID
        }
    End Function

    Function getBuildTempTablePos(pg As PaymentGateway3rd) As IList(Of InstallRcv3rdTempTable)

        '_InstallRcv3rdTempTable.Add(Build(pg.ARID, "D", _InstallmentAmount + pg.TanggungMF + pg.Pph23FeeAmount))
        '_InstallRcv3rdTempTable.Add(Build("PPNIN", "D", (pg.FeeTrans * (pg.PpnAtasFee / 100))))
        '_InstallRcv3rdTempTable.Add(Build("PLL", "C", (pg.FeeTrans * (pg.PpnAtasFee / 100))))
        '_InstallRcv3rdTempTable.Add(Build("RTB", "C", _InstallmentAmount)) 
        '_InstallRcv3rdTempTable.Add(Build(pg.APID, "C", pg.TanggungMF))
        '_InstallRcv3rdTempTable.Add(Build("APPPH23", "C", pg.Pph23FeeAmount))

        _InstallRcv3rdTempTable.Add(Build(pg.ARID, "D", _InstallmentAmount + pg.TanggungKonsumen - pg.FeeTrans - (pg.FeeTrans * (pg.PpnAtasFee / 100)) + pg.Pph23FeeAmount))
        _InstallRcv3rdTempTable.Add(Build(pg.ADMINFEEID, "D", pg.FeeTrans))
        _InstallRcv3rdTempTable.Add(Build(pg.ADMINFEEID, "C", pg.TanggungKonsumen))
        _InstallRcv3rdTempTable.Add(Build("PPNIN", "D", (pg.FeeTrans * (pg.PpnAtasFee / 100))))
        _InstallRcv3rdTempTable.Add(Build(pg.ADMINFEEID, "D", (pg.FeeSwitching + (pg.FeeSwitching * (pg.PpnAtasFeeSwitching / 100)))))
        _InstallRcv3rdTempTable.Add(Build("RTB", "C", _InstallmentAmount))
        _InstallRcv3rdTempTable.Add(Build(pg.APID, "C", (pg.FeeSwitching + (pg.FeeSwitching * (pg.PpnAtasFeeSwitching / 100)))))
        _InstallRcv3rdTempTable.Add(Build("APPPH23", "C", pg.Pph23FeeAmount))

        Return _InstallRcv3rdTempTable
    End Function

    Function getBuildTempTableFinnet(pg As PaymentGateway3rd) As IList(Of InstallRcv3rdTempTable)

        _InstallRcv3rdTempTable.Add(Build(pg.ARID, "D", _InstallmentAmount - (pg.TanggungMF + pg.PpnFeeAmount) + pg.Pph23FeeAmount))
        _InstallRcv3rdTempTable.Add(Build(pg.ADMINFEEID, "D", pg.TanggungMF))
        _InstallRcv3rdTempTable.Add(Build("PPNIN", "D", pg.PpnFeeAmount))
        _InstallRcv3rdTempTable.Add(Build("RTB", "C", _InstallmentAmount))
        _InstallRcv3rdTempTable.Add(Build("APPPH23", "C", pg.Pph23FeeAmount))

        Return _InstallRcv3rdTempTable
    End Function

    Public Function GetBuildTempTable(pg As PaymentGateway3rd) As IList(Of InstallRcv3rdTempTable)
        If (_InstallRcv3rdTempTable Is Nothing) Then
            _InstallRcv3rdTempTable = New List(Of InstallRcv3rdTempTable)
        End If 
        _InstallRcv3rdTempTable.Clear()

        If (pg.ID = "POS") Then
            Return getBuildTempTablePos(pg)
        ElseIf (pg.ID = "FINNET") Then
            Return getBuildTempTableFinnet(pg)
        End If

        ' pembayaran indomaret
        _InstallRcv3rdTempTable.Add(Build(pg.ARID, "D", _InstallmentAmount - ((pg.FeeTrans * (pg.PpnAtasFee / 100)) + pg.FeeTrans)))
        _InstallRcv3rdTempTable.Add(Build(pg.ARID, "D", pg.Pph23FeeAmount))
        _InstallRcv3rdTempTable.Add(Build(pg.ADMINFEEID, "D", pg.FeeTrans))
        _InstallRcv3rdTempTable.Add(Build("PPNIN", "D", (pg.FeeTrans * (pg.PpnAtasFee / 100))))
        _InstallRcv3rdTempTable.Add(Build("RTB", "C", _InstallmentAmount - pg.TanggungKonsumen))
        _InstallRcv3rdTempTable.Add(Build("APPPH23", "C", pg.Pph23FeeAmount))
        _InstallRcv3rdTempTable.Add(Build(pg.ADMINFEEID, "C", pg.TanggungKonsumen))

        '_InstallRcv3rdTempTable.Add(Build(pg.ARID, "D", _InstallmentAmount - (pg.TanggungMF + pg.PpnFeeAmount) + pg.Pph23FeeAmount))
        '_InstallRcv3rdTempTable.Add(Build("PPNIN", "D", pg.PpnFeeAmount))
        '_InstallRcv3rdTempTable.Add(Build(pg.ADMINFEEID, "D", pg.TanggungMF))
        '_InstallRcv3rdTempTable.Add(Build("RTB", "C", _InstallmentAmount))  
        '_InstallRcv3rdTempTable.Add(Build("APPPH23", "C", pg.Pph23FeeAmount))

        Return _InstallRcv3rdTempTable
    End Function

End Class

<Serializable()>
Public Class InstallRcv3rdTempTableHost
    Private _PaymentGateway3rd As PaymentGateway3rd
    Private _InstallRcv3rdUpladedLogs As IList(Of InstallRcv3rdUpladedLog)

    Public Sub New(pg As PaymentGateway3rd, upladedLogs As IList(Of InstallRcv3rdUpladedLog))
        _PaymentGateway3rd = pg
        _InstallRcv3rdUpladedLogs = upladedLogs
    End Sub

    Public Sub SetPaymentGateway(pg As PaymentGateway3rd)
        _PaymentGateway3rd = pg
    End Sub


    Public Function ToTableTempTable() As DataTable
        Dim dt As DataTable = New DataTable()
        Dim row As DataRow
        Dim strArray As String() = New String() {"PaymentAllocationID", "Post", "Amount", "RefDesc", "DepartementID", "VoucherDesc", "LogId", "BranchId", "Applicationid"}
        For Each s In strArray
            dt.Columns.Add(New DataColumn(s))
        Next

        For Each item In _InstallRcv3rdUpladedLogs
            For Each ttbl In item.GetBuildTempTable(_PaymentGateway3rd)
                row = dt.NewRow()
                row("PaymentAllocationID") = ttbl.PaymentAllocationID
                row("Post") = ttbl.Post
                row("Amount") = ttbl.Amount
                row("RefDesc") = ttbl.RefDesc
                row("DepartementID") = ttbl.DepartementID
                row("VoucherDesc") = ttbl.VoucherDesc
                row("LogId") = ttbl.LogID
                row("BranchId") = ttbl.BranchId
                row("Applicationid") = ttbl.ApplicationId
                dt.Rows.Add(row)
            Next
        Next 
        Return dt 
    End Function

End Class