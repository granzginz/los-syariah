﻿
Imports System.Linq
Imports System.IO
Imports System.Text
Imports System.Text.RegularExpressions

<Serializable()>
Public Class AgreementVirtualAcc

    Private _pgID As String
    Private _bIN As String
    Private _agreementNo As String
    Private _vaNo As String 
    Private _virtualAccountRows As IList(Of AgreementVirtualAcc)
    Private _name As String 
    Public Property AgreementNo As String
        Get
            Return _agreementNo
        End Get
        Set(value As String)
            _agreementNo = value
        End Set
    End Property
    Public Property Nama As String
        Get
            Return _name
        End Get
        Set(value As String)
            _name = value
        End Set
    End Property
    Public Property VirtualAccount As String
        Get
            Return _vaNo
        End Get
        Set(value As String)
            _vaNo = value
        End Set
    End Property


    Public Property VirtualAccountRows As IList(Of AgreementVirtualAcc)
        Get
            Return _virtualAccountRows
        End Get
        Set(value As IList(Of AgreementVirtualAcc))
            _virtualAccountRows = value
        End Set
    End Property

    Public Sub New()
        _virtualAccountRows = New List(Of AgreementVirtualAcc)
    End Sub

    Public Sub New(pgID As String, bIN As String, agreementNo As String, vaNo As String, name As String)
        _pgID = pgID
        _bIN = bIN
        _agreementNo = agreementNo
        _vaNo = vaNo
        _name = name
    End Sub


    Public Shared Function DoParse(reader As System.IO.StreamReader, viaType As EnViaType, _pgID As String) As AgreementVirtualAcc
        Dim result = New AgreementVirtualAcc
        Dim provider As Globalization.CultureInfo = Globalization.CultureInfo.InvariantCulture
        While Not reader.EndOfStream
            Dim originalLine = reader.ReadLine()
            Dim delim = "|"c
            Dim oriSplit() As String = originalLine.Split(delim)
            Try
                Dim bin = IIf(viaType = EnViaType.Finnet, Left(oriSplit(2), 4), "")
                Dim jv = New AgreementVirtualAcc(_pgID, bin, oriSplit(0), oriSplit(2), oriSplit(1))
                result.VirtualAccountRows.Add(jv)
            Catch
            End Try
        End While
        Return result
    End Function




    Public Function ToTable() As DataTable
        Dim dt = New DataTable()
        For Each s As String In New String() {
            "Col_000",
            "Col_001",
            "Col_002",
            "Col_003",
            "Col_004",
            "Col_005",
            "Col_006",
            "Col_007",
            "Col_008",
            "Col_009",
            "Col_010",
            "Col_011",
            "Col_012"
 }
            dt.Columns.Add(New DataColumn(s))
        Next
        For Each v In VirtualAccountRows
            Dim row = dt.NewRow()

            row("Col_000") = v._vaNo
            row("Col_001") = v._pgID
            row("Col_002") = v._bIN
            row("Col_003") = v._agreementNo
            row("Col_004") = ""
            row("Col_005") = ""
            row("Col_006") = ""
            row("Col_007") = ""
            row("Col_008") = ""
            row("Col_009") = ""
            row("Col_010") = ""
            row("Col_011") = ""
            row("Col_012") = ""

            dt.Rows.Add(row)
        Next
        Return dt
    End Function
End Class
