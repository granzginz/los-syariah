

<Serializable()> _
Public Class InstallRcv : Inherits Maxiloan.Parameter.AccMntBase

#Region "Variable : WOP "
    Private _currencyrate As Double
    Private _wop As String
    Private _bankaccountid As String
#End Region


    Private _notes As String
    Private _strError As String

#Region "Variable : Credit Card"
    Private _cardname As String
    Private _cardid As String
    Private _cardnumber As String
    Private _authorizenumber As String

    Private _chargeratetocust As Double
    Private _chargeamttocust As Double
    Private _paidamtincludecard As Boolean
#End Region

    Private _processtype As String
    Private _hasil As Integer
    Private _MultiApplicationID As String
    Private _MultiAgreementNo As String
    Private _AdvanceInstallment As Decimal
    Private _AdvanceInstallmentDate As Date
    Private _VoucherNo As String

#Region "PaymentComponent"
    Private _paymentamount As Double
    Private _lcinstallmentpayment As Double
    Private _lcinsurancepayment As Double
    Private _installmentduepayment As Double
    Private _insuranceduepayment As Double
    Private _prepaidpayment As Double
    Private _otherincomepayment As Double
#End Region

#Region "Property: PaymentComponent"
    Public Property PaymentAmount() As Double
        Get
            Return (CType(_paymentamount, Double))
        End Get
        Set(ByVal Value As Double)
            _paymentamount = Value
        End Set
    End Property

    Public Property LCInstallmentPayment() As Double
        Get
            Return (CType(_lcinstallmentpayment, Double))
        End Get
        Set(ByVal Value As Double)
            _lcinstallmentpayment = Value
        End Set
    End Property

    Public Property LCInsurancePayment() As Double
        Get
            Return (CType(_lcinsurancepayment, Double))
        End Get
        Set(ByVal Value As Double)
            _lcinsurancepayment = Value
        End Set
    End Property

    Public Property InstallmentDuePayment() As Double
        Get
            Return (CType(_installmentduepayment, Double))
        End Get
        Set(ByVal Value As Double)
            _installmentduepayment = Value
        End Set
    End Property

    Public Property InsuranceDuePayment() As Double
        Get
            Return (CType(_insuranceduepayment, Double))
        End Get
        Set(ByVal Value As Double)
            _insuranceduepayment = Value
        End Set
    End Property

    Public Property PrepaidPayment() As Double
        Get
            Return (CType(_prepaidpayment, Double))
        End Get
        Set(ByVal Value As Double)
            _prepaidpayment = Value
        End Set
    End Property

    Public Property OtherIncomePayment() As Double
        Get
            Return (CType(_otherincomepayment, Double))
        End Get
        Set(ByVal Value As Double)
            _otherincomepayment = Value
        End Set
    End Property
#End Region

#Region "Credit Card Properties"

    Public Property CardName() As String
        Get
            Return (CType(_cardname, String))
        End Get
        Set(ByVal Value As String)
            _cardname = Value
        End Set
    End Property

    Public Property CardID() As String
        Get
            Return (CType(_cardid, String))
        End Get
        Set(ByVal Value As String)
            _cardid = Value
        End Set
    End Property

    Public Property CardNumber() As String
        Get
            Return (CType(_cardnumber, String))
        End Get
        Set(ByVal Value As String)
            _cardnumber = Value
        End Set
    End Property

    Public Property AuthorizeNumber() As String
        Get
            Return (CType(_authorizenumber, String))
        End Get
        Set(ByVal Value As String)
            _authorizenumber = Value
        End Set
    End Property

    Public Property ChargeRateToCust() As Double
        Get
            Return (CType(_chargeratetocust, Double))
        End Get
        Set(ByVal Value As Double)
            _chargeratetocust = Value
        End Set
    End Property

    Public Property ChargeAmtToCust() As Double
        Get
            Return (CType(_chargeamttocust, Double))
        End Get
        Set(ByVal Value As Double)
            _chargeamttocust = Value
        End Set
    End Property

    Public Property PaidAmtIncludeCard() As Boolean
        Get
            Return (CType(_paidamtincludecard, Boolean))
        End Get
        Set(ByVal Value As Boolean)
            _paidamtincludecard = Value
        End Set
    End Property

#End Region

#Region "Currency Properties"
    Public Property CurrencyRate() As Double
        Get
            Return (CType(_currencyrate, Double))
        End Get
        Set(ByVal Value As Double)
            _currencyrate = Value
        End Set
    End Property
#End Region

#Region "WOP"
    Public Property WOP() As String
        Get
            Return (CType(_wop, String))
        End Get
        Set(ByVal Value As String)
            _wop = Value
        End Set
    End Property
#End Region
    Public Property VoucherNO() As String
        Get
            Return _VoucherNo
        End Get
        Set(value As String)
            _VoucherNo = value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return (CType(_notes, String))
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property
    Public Property strError() As String
        Get
            Return (CType(_strError, String))
        End Get
        Set(ByVal Value As String)
            _strError = Value
        End Set
    End Property

    Public Property Hasil() As Integer
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Integer)
            _hasil = Value
        End Set
    End Property


    Public Property MultiApplicationID() As String
        Get
            Return (CType(_MultiApplicationID, String))
        End Get
        Set(ByVal Value As String)
            _MultiApplicationID = Value
        End Set
    End Property

    Public Property MultiAgreementNo() As String
        Get
            Return (CType(_MultiAgreementNo, String))
        End Get
        Set(ByVal Value As String)
            _MultiAgreementNo = Value
        End Set
    End Property
    Public Property AdvanceInstallment() As Decimal
        Get
            Return (CType(_AdvanceInstallment, Decimal))
        End Get
        Set(ByVal Value As Decimal)
            _AdvanceInstallment = Value
        End Set
    End Property
    Public Property AdvanceInstallmentDate() As Date
        Get
            Return (CType(_AdvanceInstallmentDate, Date))
        End Get
        Set(ByVal Value As Date)
            _AdvanceInstallmentDate = Value
        End Set
    End Property
    Public Property HistorySeqNo As Integer
    Public Property ReferenceNo1 As String
    Public Property SPName() As String
    Public Property NoKwitansi As String
    Public Property IsReversedOnBusinessDate As Boolean
    Public Property ReferenceNoLog As String
    Public Property ApplicationIDLog As String
    Public Property InsSeqNo As Integer
    Public Property Status As String
    Public Property Keterangan As String

    Public Property PaidInterest() As Decimal
    Public Property PaidPrincipal() As Decimal
    Public Property InvoiceNo() String
    Public Property PaidRetensi() Decimal
    Public Property DendaPaid() As Decimal
    Public Property Amount() As Decimal
    Public Property Departemen As String
    Public Property Description As String
    Public Property BayarPPh As Decimal
    Public Property Denda As Decimal
    Public Property TableVoucherNO As Object
End Class

