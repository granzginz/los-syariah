﻿Imports System.Linq
Imports System.IO
Imports System.Text
Imports System.Text.RegularExpressions



<Serializable()>
Public Class InstallRcv3rd
    Private Const currencyCode As String = "360"
    Private Const PermataBankBINCode = "XXX"
    Private Const PermataBankInstitutionCode = "999999"
    Private Const PermataBankDepartCode = "0000"
    Private Const maxInstallment As Integer = 3

    Private _viaType As EnViaType
    Private _installRcv3rdRows As IList(Of InstallRcv3rdCols)
    Public Property UploadDate As DateTime

    Public Property DataSetResult As DataSet
    Public Property TotalRecords As Long


    Public Property FileDirectory As String

    Public Property PaymentGateway3rd As IList(Of PaymentGateway3rd)

    Private m_LastSeq As Integer
    Public Property LastSeq As Integer
        Get
            If (m_LastSeq > 999) Then
                Return 1
            End If
            Return m_LastSeq
        End Get
        Set(value As Integer)
            m_LastSeq = value
        End Set
    End Property

    Sub New()
        _installRcv3rdRows = New List(Of InstallRcv3rdCols)
    End Sub

    Public Property ViaType As EnViaType
        Get
            Return _viaType
        End Get
        Set(value As EnViaType)
            _viaType = value
        End Set
    End Property

    Public Property InstallRcv3rdRows As IList(Of InstallRcv3rdCols)
        Get
            Return _installRcv3rdRows
        End Get
        Set(value As IList(Of InstallRcv3rdCols))
            _installRcv3rdRows = value
        End Set
    End Property

    Public ReadOnly Property TotalInstallment As Double
        Get
            Return _installRcv3rdRows.Sum(Function(x) x.Amount)
        End Get
    End Property
    Public Shared Function DoParse(reader As System.IO.StreamReader, viaType As EnViaType, mdate As DateTime) As InstallRcv3rd
        Dim result = New InstallRcv3rd
        Dim format = "yyyyMMdd"

        result.ViaType = viaType
        result.UploadDate = mdate

        Dim provider As Globalization.CultureInfo = Globalization.CultureInfo.InvariantCulture
        While Not reader.EndOfStream

            Dim originalLine = reader.ReadLine()
            Dim delim = IIf(viaType = EnViaType.Finnet, ";", "|")
            Dim oriSplit = split(originalLine, delim, Chr(34), False)

            If ((viaType = EnViaType.Indomaret Or viaType = EnViaType.POS Or viaType = EnViaType.BCA)) Then
                If (CInt(oriSplit(7) = 0)) Then Continue While
            End If

            Try
                Dim jv = New InstallRcv3rdCols(oriSplit, viaType)
                result.InstallRcv3rdRows.Add(jv)
            Catch

            End Try
        End While
        Return result
    End Function

    Public Function ToTable() As DataTable
        Dim dt = New DataTable()
        For Each s As String In New String() {
            "Col_000",
            "Col_001",
            "Col_002",
            "Col_003",
            "Col_004",
            "Col_005",
            "Col_006",
            "Col_007",
            "Col_008",
            "Col_009",
            "Col_010",
            "Col_011",
            "Col_012"
 }
            dt.Columns.Add(New DataColumn(s))
        Next
        For Each v In InstallRcv3rdRows
            Dim row = dt.NewRow()

            row("Col_000") = v.Col_0
            row("Col_001") = v.Col_1
            row("Col_002") = v.Col_2
            row("Col_003") = v.Col_3
            row("Col_004") = v.Col_4
            row("Col_005") = v.Col_5
            row("Col_006") = v.Col_6
            row("Col_007") = v.Col_7
            row("Col_008") = v.Col_8
            row("Col_009") = v.Col_9
            row("Col_010") = v.Col_10
            row("Col_011") = v.Col_11
            row("Col_012") = v.Col_12

            dt.Rows.Add(row)
        Next
        Return dt
    End Function


    Private Shared Function split(expression As String, delimiter As String, qualifier As String, ignoreCase As Boolean) As String()
        Dim statement = String.Format("{0}(?=(?:[^{1}]*{1}[^{1}]*{1})*(?![^{1}]*{1}))", Regex.Escape(delimiter), Regex.Escape(qualifier))
        Dim options = RegexOptions.Compiled Or RegexOptions.Multiline
        If (ignoreCase) Then options = options Or RegexOptions.IgnoreCase
        Return New Regex(statement, options).Split(expression)
    End Function



    Public ReadOnly Property PosFileName As String
        Get
            Return String.Format("2800-{0:yyyyMMdd}-{1:d3}.bdt", UploadDate, LastSeq)
        End Get
    End Property

    Public ReadOnly Property FinnetFileName As String
        Get

            Return String.Format("801-{0:yyyyMMdd}-{1:d3}.txt", UploadDate, LastSeq)
        End Get
    End Property

    Public ReadOnly Property BCAFileName As String
        Get

            Return String.Format("014-{0:yyyyMMdd}-{1:d3}.bdt", UploadDate, LastSeq)
        End Get
    End Property

    Public ReadOnly Property IndomaretFileName As String
        Get
            Return String.Format("R{0:yyyyMMdd}-{1:hhmmss}-BILLING-AF-INDOMARET.txt", UploadDate, UploadDate)
        End Get
    End Property

    Public ReadOnly Property PermataFileName As String
        Get
            Return String.Format("FGVASHR@{0:3}{1:MMddyyyy}-{2:hhmmss}.txt", PermataBankBINCode, UploadDate, UploadDate)
        End Get
    End Property

    Public Sub BuildUploadFiles()
        Try
            buildPOSFile()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Private Sub writeFile(path As String, builder As StringBuilder)
        If File.Exists(path) Then
            File.Delete(path)
        End If
        File.WriteAllText(path, builder.ToString())
    End Sub


    Private Function getFee(key As String) As Decimal
        Dim fee = PaymentGateway3rd.Where(Function(x) x.ID = key).SingleOrDefault()
        If Not (fee Is Nothing) Then
            Return fee.TanggungKonsumen
        End If
        Return 7000
    End Function

    Private Const posAndFinnetFormat = "{0,12}|{1,-40}|{2}|{3:yyyyMMdd}|{4}|{5}|{6}|{7}|{8}|{9}|{10:d9}"
    Private Const indomaretFormat = "{0,12}{1,-60}{2}{3:d2}{4:dd/MM/yyyy}"
    Private Const permataFormatHeader = "{0,1}{1,6}{2:d4}{3,30}{4,4}{5,30}"
    'Private Const permataFormatDetail = "{0,1}{1,6}{2:d4}{3,4}{4,30}{5,16}{6,16}{7,3}{8:d15}{9,30}{10:d2}"
    'Private Const permataFormatDetail = "{0,1}{1,6}{2:d4}{3,4}{4,30}{5,16}{6,16}{7,3}{8:d15}{9,30}{10,2}"
    Private Const permataFormatDetail = "{0,1}{1,6}{2:d4}{3,4}{4,30}{5,16}{6,16}{7,3}{8,15}{9,30}{10,2}"

    Private Sub buildPOSFile()
        Try
            Dim builderPos = New StringBuilder
            Dim builderFinn = New StringBuilder
            Dim builderIndomaret = New StringBuilder
            Dim builderPermataHeader = New StringBuilder
            Dim builderPermataDetail = New StringBuilder
            Dim builderBCA = New StringBuilder

            Dim adminfeePos = getFee("POS").ToString("###")
            Dim adminfeeFinn = getFee("FINNET").ToString("###")
            Dim adminfeeIndomaret = getFee("INDO").ToString("###")
            Dim adminfeePermata = getFee("PERMATA").ToString("###")
            Dim adminfeeBCA = getFee("BCA").ToString("###")

            Dim PermataCounter As Integer = 0
            Dim PermataHeaderDescription As String

            For Each e In _installRcv3rdRows
                PermataHeaderDescription = String.Format("BAYAR ANGSURAN BULAN {0} {1}", e.TransactionDate.Month.ToString, e.TransactionDate.Year)

                builderPos.AppendLine(String.Format(posAndFinnetFormat,
                                                 sHlp(e.AgreementNo, 12), sHlp(e.NameOfCustomer, 40), e.PoliceNumber, e.DateOverDue, e.Tenor, adminfeePos,
                                                 Math.Round((e.Amount + e.Pinalty)), Math.Round((e.Amount + e.Pinalty)), Math.Round((e.Amount * maxInstallment)), currencyCode,
                                                 sHlp(CType(e.Pinalty, Decimal), 9, True)))

                builderFinn.AppendLine(String.Format(posAndFinnetFormat,
                                                 sHlp(e.AgreementNo, 12), sHlp(e.NameOfCustomer, 40), e.PoliceNumber, e.DateOverDue, e.Tenor, adminfeeFinn,
                                                 Math.Round((e.Amount + e.Pinalty)), Math.Round((e.Amount + e.Pinalty)), Math.Round((e.Amount * maxInstallment)), currencyCode,
                                                 sHlp(CType(e.Pinalty, Decimal), 9, True)))

                builderIndomaret.AppendLine(String.Format(indomaretFormat,
                                                 sHlp(e.AgreementNo, 12), sHlp(e.NameOfCustomer, 60), Math.Round((e.Amount + e.Pinalty), 0), e.Tenor, e.DateOverDue))

                builderBCA.AppendLine(String.Format(posAndFinnetFormat,
                                                 sHlp(e.AgreementNo, 12), sHlp(e.NameOfCustomer, 40), e.PoliceNumber, e.DateOverDue, e.Tenor, adminfeePos,
                                                 Math.Round((e.Amount + e.Pinalty)), Math.Round((e.Amount + e.Pinalty)), Math.Round((e.Amount * maxInstallment)), currencyCode,
                                                 sHlp(CType(e.Pinalty, Decimal), 9, True)))

                PermataCounter += 1
                builderPermataHeader.AppendLine(String.Format(permataFormatHeader,
                                                              "D", PermataBankInstitutionCode, PermataBankDepartCode, "",
                                                              sHlp(PermataCounter.ToString(), 4), sHlp(PermataHeaderDescription, 30)))
                builderPermataHeader.AppendLine(String.Format(permataFormatHeader,
                                                              "A", PermataBankInstitutionCode, PermataBankDepartCode, "",
                                                              sHlp(PermataCounter.ToString(), 4), sHlp(PermataHeaderDescription, 30)))

                'builderPermataDetail.AppendLine(String.Format(permataFormatDetail,
                '                                              "D", PermataBankInstitutionCode, PermataBankDepartCode,
                '                                              sHlp(PermataCounter.ToString(), 4), sHlp(e.NameOfCustomer, 30),
                '                                              sHlp(e.AgreementNo, 16), sHlp(e.PoliceNumber, 16), currencyCode,
                '                                              Math.Round((e.Amount + e.Pinalty)), sHlp(PermataHeaderDescription, 30), "01"))
                'builderPermataDetail.AppendLine(String.Format(permataFormatDetail,
                '                                              "A", PermataBankInstitutionCode, PermataBankDepartCode,
                '                                              sHlp(PermataCounter.ToString(), 4), sHlp(e.NameOfCustomer, 30),
                '                                              sHlp(e.AgreementNo, 16), sHlp(e.PoliceNumber, 16), currencyCode,
                '                                              Math.Round((e.Amount + e.Pinalty)), sHlp(PermataHeaderDescription, 30), "01"))
                builderPermataDetail.AppendLine(String.Format(permataFormatDetail,
                                                                          "D", PermataBankInstitutionCode, PermataBankDepartCode,
                                                                          sHlp(PermataCounter.ToString(), 4), sHlp(e.NameOfCustomer, 30),
                                                                          sHlp(e.AgreementNo, 16), sHlp(e.PoliceNumber, 16), currencyCode,
                                                                          Math.Round(e.Amount + e.Pinalty), sHlp(PermataHeaderDescription, 30), "01"))
                builderPermataDetail.AppendLine(String.Format(permataFormatDetail,
                                                              "A", PermataBankInstitutionCode, PermataBankDepartCode,
                                                              sHlp(PermataCounter.ToString(), 4), sHlp(e.NameOfCustomer, 30),
                                                              sHlp(e.AgreementNo, 16), sHlp(e.PoliceNumber, 16), currencyCode,
                                                              Math.Round(e.Amount + e.Pinalty), sHlp(PermataHeaderDescription, 30), "01"))

            Next
            writeFile(String.Format("{0}{1}", FileDirectory, PosFileName), builderPos)

            'writeFile(String.Format("{0}{1}", FileDirectory, FinnetFileName), builderFinn)

            writeFile(String.Format("{0}{1}", FileDirectory, IndomaretFileName), builderIndomaret)

            writeFile(String.Format("{0}{1}", FileDirectory, BCAFileName), builderBCA)

            writeFile(String.Format("{0}{1}", FileDirectory, PermataFileName.Replace("@", "1")), builderPermataHeader)

            writeFile(String.Format("{0}{1}", FileDirectory, PermataFileName.Replace("@", "2")), builderPermataDetail)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub


    Function sHlp(str As String, len As Integer, Optional islead As Boolean = False)
        If islead Then
            str = String.Format("00000000{0}", str)

            Return str.Substring(str.Length - len)
        End If

        If str.Length > len Then
            Return str.Substring(0, len)
        End If

        Return str
    End Function
End Class 
' INDOMARET
' agrNo  - - Marchant           -  date                  amount 
'   0    1 2 3             4          5       6     7  8 9         10      11     
'165796|01|0|G028T9WU|157878502557|20150830|220610|360|0|2589983|2589983|2589983

'POS
'     agrNo                                             date                                             amount
'0      1      2     3                 4        5         6             7    8       9           10         11         12
'0|0000151375|22| 144083752194967286|5111|921784153329|20150829|030112150028|360|     7000|     4986427|000004979427|000004986427

'FINNET
'date                          amount                             agrNo
'0                         1      2               3                 4     5
'20150830105933;0195759983463;6756250    ;Artha Asia Finance002;00091521;Not Reversal

<Serializable()>
Public Class InstallRcv3rdCols

    Private mAgreementNo As String
    Private mMarchandId As String
    Private mTransactionDate As DateTime
    Private mAmount As Double



    Private mNameOfCustomer As String
    Private mPoliceNumber As String
    Private mDateOverDue As Date
    Private mTenor As Integer
    Private mAdminFee As Decimal
    Private mPinalty As Double

    Public Property Col_0 As String
    Public Property Col_1 As String
    Public Property Col_2 As String
    Public Property Col_3 As String
    Public Property Col_4 As String
    Public Property Col_5 As String
    Public Property Col_6 As String
    Public Property Col_7 As String
    Public Property Col_8 As String
    Public Property Col_9 As String
    Public Property Col_10 As String
    Public Property Col_11 As String
    Public Property Col_12 As String

    Public Sub New(agNo As String, mMarcid As String, mTrDate As Date, amount As Double)
        mAgreementNo = agNo
        mMarchandId = mMarcid
        mTransactionDate = mTrDate
        mAmount = amount
    End Sub
    Private format = "yyyyMMdd"
    Private provider As Globalization.CultureInfo = Globalization.CultureInfo.InvariantCulture

    Public Sub New(oriSplit As String(), viaType As EnViaType)

        Col_0 = ""
        Col_1 = ""
        Col_2 = ""
        Col_3 = ""
        Col_4 = ""
        Col_5 = ""
        Col_6 = ""
        Col_7 = ""
        Col_8 = ""
        Col_9 = ""
        Col_10 = ""
        Col_11 = ""
        Col_12 = ""

        Col_0 = oriSplit(0)
        Col_1 = oriSplit(1)
        Col_2 = oriSplit(2)
        Col_3 = oriSplit(3)
        Col_4 = oriSplit(4)
        Col_5 = oriSplit(5)

        Dim MyChar() As Char = {"0"}

        If (viaType = EnViaType.Finnet) Then
            Col_4 = Col_4.TrimStart(MyChar)
        End If

        If (viaType = EnViaType.Permata) Then
            Col_6 = oriSplit(6)
            Col_7 = oriSplit(7)
            Col_8 = oriSplit(8)
        End If

        If (viaType = EnViaType.Indomaret Or viaType = EnViaType.POS Or viaType = EnViaType.BCA) Then
            Col_6 = oriSplit(6)
            Col_7 = oriSplit(7)
            Col_8 = oriSplit(8)
            Col_9 = oriSplit(9)
            Col_10 = oriSplit(10)
            Col_11 = oriSplit(11)
            If (viaType = EnViaType.POS Or viaType = EnViaType.BCA) Then
                Col_1 = Col_1.TrimStart(MyChar).Trim
                Col_7 = Col_7.TrimStart(MyChar).Trim
                Col_11 = Col_11.TrimStart(MyChar).Trim
                Col_12 = oriSplit(12).TrimStart(MyChar).Trim
            End If
        End If
    End Sub

    Public Sub New(_AgreementNo As String, _NameOfCustomer As String, _PoliceNumber As String, _DateOverDue As Date, _Tenor As Integer, _installment As Double, _Pinalty As Double)
        mAgreementNo = _AgreementNo
        mNameOfCustomer = _NameOfCustomer
        mPoliceNumber = _PoliceNumber
        mDateOverDue = _DateOverDue
        mTenor = _Tenor
        mAmount = _installment
        mPinalty = _Pinalty
    End Sub
    Public ReadOnly Property AgreementNo As String
        Get
            Return mAgreementNo
        End Get
    End Property
    Public ReadOnly Property MarchandId As String
        Get
            Return mMarchandId
        End Get
    End Property
    Public ReadOnly Property TransactionDate As DateTime
        Get
            Return mTransactionDate
        End Get
    End Property
    Public ReadOnly Property Amount As Double
        Get
            Return mAmount
        End Get
    End Property

    Public ReadOnly Property NameOfCustomer As String
        Get
            Return mNameOfCustomer
        End Get
    End Property
    Public ReadOnly Property PoliceNumber As String
        Get
            Return mPoliceNumber
        End Get
    End Property
    Public ReadOnly Property DateOverDue As Date
        Get
            Return mDateOverDue
        End Get
    End Property
    Public ReadOnly Property Tenor As Integer
        Get
            Return mTenor
        End Get
    End Property

    Public ReadOnly Property Pinalty As Double
        Get
            Return IIf(mPinalty < 0, 0, mPinalty)
        End Get
    End Property

End Class

<Serializable()>
Public Enum EnViaType
    Indomaret
    POS
    Finnet
    Permata
    BCA
End Enum

<Serializable()>
Public Class PaymentGateway3rd

    Private pgID As String
    Private pgName As String
    Private pgSettelmentBankAccount As String
    Private pgFeeTrans As Decimal
    Private pgTanggugKonsumen As Decimal
    Private pgTanggungMF As Decimal
    Private pgPpnAtasFee As Decimal
    Private pgPph23AtasFee As Decimal
    Private pgFeeSwitching As Decimal
    Private pgPpnAtasFeeSwitching As Decimal
    Private pgPph23AtasFeeSwitching As Decimal
    Public Sub New()

    End Sub
    Public Sub New(
        _pgID As String,
        _pgName As String,
        _pgSettelmentBankAccount As String,
        _pgFeeTrans As Decimal,
        _pgTanggugKonsumen As Decimal,
        _pgPpnAtasFee As Decimal,
        _pgPph23AtasFee As Decimal,
        _pgFeeSwitching As Decimal,
        _pgPpnAtasFeeSwitching As Decimal,
        _pgPph23AtasFeeSwitching As Decimal)

        pgID = _pgID
        pgName = _pgName
        pgSettelmentBankAccount = _pgSettelmentBankAccount
        pgFeeTrans = _pgFeeTrans
        pgTanggugKonsumen = _pgTanggugKonsumen
        pgPpnAtasFee = _pgPpnAtasFee
        pgPph23AtasFee = _pgPph23AtasFee
        pgFeeSwitching = _pgFeeSwitching
        pgPpnAtasFeeSwitching = _pgPpnAtasFeeSwitching
        pgPph23AtasFeeSwitching = _pgPph23AtasFeeSwitching
    End Sub


    Public Sub New(
        _pgID As String,
        _pgName As String,
        _pgSettelmentBankAccount As String,
        _pgFeeTrans As Decimal,
        _pgTanggugKonsumen As Decimal,
        _pgPpnAtasFee As Decimal,
        _pgPph23AtasFee As Decimal,
        _pgFeeSwitching As Decimal,
        _pgPpnAtasFeeSwitching As Decimal,
        _pgPph23AtasFeeSwitching As Decimal,
         _pgTanggungMF As Decimal,
        _pgARID As String,
        _pgARPPH23ID As String,
        _pgADMINFEEID As String,
        _pgAPID As String
        )

        pgID = _pgID
        pgName = _pgName
        pgSettelmentBankAccount = _pgSettelmentBankAccount
        pgFeeTrans = _pgFeeTrans
        pgTanggugKonsumen = _pgTanggugKonsumen
        pgPpnAtasFee = _pgPpnAtasFee
        pgPph23AtasFee = _pgPph23AtasFee
        pgFeeSwitching = _pgFeeSwitching
        pgPpnAtasFeeSwitching = _pgPpnAtasFeeSwitching
        pgPph23AtasFeeSwitching = _pgPph23AtasFeeSwitching

        pgTanggungMF = _pgTanggungMF
        _ARID = _pgARID
        _ARPPH23ID = _pgARPPH23ID
        _ADMINFEEID = _pgADMINFEEID
        _APID = _pgAPID
    End Sub

    Public ReadOnly Property ID As String
        Get
            Return pgID
        End Get
    End Property
    Public ReadOnly Property Name As String
        Get
            Return pgName
        End Get
    End Property
    Public ReadOnly Property SettlementBankAccount As String
        Get
            Return pgSettelmentBankAccount
        End Get
    End Property
    Public ReadOnly Property FeeTrans As Decimal
        Get
            Return pgFeeTrans
        End Get
    End Property
    Public ReadOnly Property TanggungKonsumen As Decimal
        Get
            Return pgTanggugKonsumen
        End Get
    End Property

    Public ReadOnly Property TanggungMF As Decimal
        Get
            Return pgTanggungMF
        End Get
    End Property
    Public ReadOnly Property PpnFeeAmount As Decimal
        Get
            Return pgFeeTrans * (pgPpnAtasFee / 100)
        End Get
    End Property

    Public ReadOnly Property Pph23FeeAmount As Decimal
        Get
            Return pgFeeTrans * (pgPph23AtasFee / 100)
        End Get
    End Property
    Public ReadOnly Property PpnAtasFee As Decimal
        Get
            Return pgPpnAtasFee
        End Get
    End Property
    Public ReadOnly Property Pph23AtasFee As Decimal
        Get
            Return pgPph23AtasFee
        End Get
    End Property
    Public ReadOnly Property FeeSwitching As Decimal
        Get
            Return pgFeeSwitching
        End Get
    End Property
    Public ReadOnly Property PpnAtasFeeSwitching As Decimal
        Get
            Return pgPpnAtasFeeSwitching
        End Get
    End Property
    Public ReadOnly Property Pph23AtasFeeSwitching As Decimal
        Get
            Return pgPph23AtasFeeSwitching
        End Get
    End Property

    Private _ARID As String
    Private _ARPPH23ID As String
    Private _ADMINFEEID As String
    Private _APID As String

    Public ReadOnly Property ARID As String
        Get
            Return _ARID
        End Get
    End Property
    Public ReadOnly Property ARPPH23ID As String
        Get
            Return _ARPPH23ID
        End Get
    End Property
    Public ReadOnly Property ADMINFEEID As String
        Get
            Return _ADMINFEEID
        End Get
    End Property
    Public ReadOnly Property APID As String
        Get
            Return _APID
        End Get
    End Property

End Class