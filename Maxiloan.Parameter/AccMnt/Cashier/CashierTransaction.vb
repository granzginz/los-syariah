
<Serializable()> _
Public Class CashierTransaction : Inherits Maxiloan.Parameter.Common
    Private _isHeadCashierClose As Boolean
    Private _HeadCashierBalance As Double
    Private _listcashierhistory As DataTable
    Private _openingamount As Double
    Private _closingamount As Double
    Private _openingsequence As Integer
    Private _cashdate As DateTime
    Private _listheadcashierhistory As DataTable
    Private _numberofpdc As Integer
    Private _pdcamount As Double
    Private _totalaptodisburse As Integer
    Private _totalpdctoclear As Integer
    Private _listdata As DataTable
    Private _dataset As DataSet

    Public Property DataSet() As DataSet
        Get
            Return (CType(_dataset, DataSet))
        End Get
        Set(ByVal Value As DataSet)
            _dataset = Value
        End Set
    End Property

    Public Property listdata() As DataTable
        Get
            Return (CType(_listdata, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property ListHeadCashierHistory() As DataTable
        Get
            Return (CType(_listheadcashierhistory, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listheadcashierhistory = Value
        End Set
    End Property

    Public Property ListCashierHistory() As DataTable
        Get
            Return (CType(_listcashierhistory, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listcashierhistory = Value
        End Set
    End Property

    Public Property ListCashierClose() As DataTable
        Get
            Return (CType(_listcashierhistory, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listcashierhistory = Value
        End Set
    End Property

    Public Property IsHeadCashierClose() As Boolean
        Get
            Return (CType(_isHeadCashierClose, Boolean))
        End Get
        Set(ByVal Value As Boolean)
            _isHeadCashierClose = Value
        End Set
    End Property

    Public Property HeadCashierBalance() As Double
        Get
            Return (CType(_HeadCashierBalance, Double))
        End Get
        Set(ByVal Value As Double)
            _HeadCashierBalance = Value
        End Set
    End Property

    Public Property OpeningAmount() As Double
        Get
            Return (CType(_openingamount, Double))
        End Get
        Set(ByVal Value As Double)
            _openingamount = Value
        End Set
    End Property

    Public Property ClosingAmount() As Double
        Get
            Return (CType(_closingamount, Double))
        End Get
        Set(ByVal Value As Double)
            _closingamount = Value
        End Set
    End Property

    Public Property OpeningSequence() As Integer
        Get
            Return (CType(_openingsequence, Integer))
        End Get
        Set(ByVal Value As Integer)
            _openingsequence = Value
        End Set
    End Property


    Public Property NumberOfPDC() As Integer
        Get
            Return (CType(_numberofpdc, Integer))
        End Get
        Set(ByVal Value As Integer)
            _numberofpdc = Value
        End Set
    End Property

    Public Property PDCAmount() As Double
        Get
            Return (CType(_pdcamount, Double))
        End Get
        Set(ByVal Value As Double)
            _pdcamount = Value
        End Set
    End Property

    Public Property TotalPDCToClear() As Integer
        Get
            Return _totalpdctoclear
        End Get
        Set(ByVal Value As Integer)
            _totalpdctoclear = Value
        End Set
    End Property

    Public Property TotalAPToDisburse() As Integer
        Get
            Return _totalaptodisburse
        End Get
        Set(ByVal Value As Integer)
            _totalaptodisburse = Value
        End Set
    End Property

End Class
