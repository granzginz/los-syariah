
<Serializable()> _
Public Class FullPrepay : Inherits Maxiloan.Parameter.AccMntBase
    Private _PrepaymentType As String
    Private _ECI As Double
    Private _ArUndue As Double
    Private _UCI As Double
    Private _TerminationPenalty As Double
    Private _InsurancePaidBy As String
    Private _crosscolateral As Boolean
    Private _crossdefault As Boolean
    Private _PrepaymentAmount As Double
    Private _prepaymentnotes As String
    Private _insuranceterminationflag As String
    Private _prepaymentno As String
    Private _userapproval As String
    Private _userrequest As String
    Private _requestdate As Date
    Private _prepaymentstatus As String
    Private _statusdate As Date
    Private _notes As String
    Private _waiveno As String
    Private _naamount As Double
    Private _totalDiscount As Double
    Private _Angsuran As Double
    Private _Denda As Double
    Private _BiayaSTNK As Double
    Private _BiayaTarik As Double
    Private _BiayaTolakanPDC As Double
    Private _BiayaTagihAngsuran As Double
    Private _PaidDate As Date

    Public Property StatusDate() As Date
        Get
            Return _statusdate
        End Get
        Set(ByVal Value As Date)
            _statusdate = Value
        End Set
    End Property
    Public Property PrepaymentStatus() As String
        Get
            Return _prepaymentstatus
        End Get
        Set(ByVal Value As String)
            _prepaymentstatus = Value
        End Set
    End Property

    Public Property UserApproval() As String
        Get
            Return _userapproval
        End Get
        Set(ByVal Value As String)
            _userapproval = Value
        End Set
    End Property

    Public Property UserRequest() As String
        Get
            Return _userrequest
        End Get
        Set(ByVal Value As String)
            _userrequest = Value
        End Set
    End Property

    Public Property RequestDate() As Date
        Get
            Return _requestdate
        End Get
        Set(ByVal Value As Date)
            _requestdate = Value
        End Set
    End Property
    Public Property NAAmount() As Double
        Get
            Return (CType(_naamount, Double))
        End Get
        Set(ByVal Value As Double)
            _naamount = Value
        End Set
    End Property
    Public Property PrepaymentNo() As String
        Get
            Return _prepaymentno
        End Get
        Set(ByVal Value As String)
            _prepaymentno = Value
        End Set
    End Property

    Public Property totalDiscount() As Double
        Get
            Return _totalDiscount
        End Get
        Set(ByVal Value As Double)
            _totalDiscount = Value
        End Set
    End Property
    Public Property PrepaymentAmount() As Double
        Get
            Return _PrepaymentAmount
        End Get
        Set(ByVal Value As Double)
            _PrepaymentAmount = Value
        End Set
    End Property
    Public Property PrepaymentType() As String
        Get
            Return _PrepaymentType
        End Get
        Set(ByVal Value As String)
            _PrepaymentType = Value
        End Set
    End Property

    Public Property ECI() As Double
        Get
            Return _ECI
        End Get
        Set(ByVal Value As Double)
            _ECI = Value
        End Set
    End Property

    Public Property UCI() As Double
        Get
            Return _UCI
        End Get
        Set(ByVal Value As Double)
            _UCI = Value
        End Set
    End Property

    Public Property TerminationPenalty() As Double
        Get
            Return _TerminationPenalty
        End Get
        Set(ByVal Value As Double)
            _TerminationPenalty = Value
        End Set
    End Property

    Public Property InsurancePaidBy() As String
        Get
            Return _InsurancePaidBy
        End Get
        Set(ByVal Value As String)
            _InsurancePaidBy = Value
        End Set
    End Property

    Public Property PrepaymentNotes() As String
        Get
            Return _prepaymentnotes
        End Get
        Set(ByVal Value As String)
            _prepaymentnotes = Value
        End Set
    End Property


    Public Property CrossColateral() As Boolean
        Get
            Return _crosscolateral
        End Get
        Set(ByVal Value As Boolean)
            _crosscolateral = Value
        End Set
    End Property

    Public Property CrossDefault() As Boolean
        Get
            Return _crossdefault
        End Get
        Set(ByVal Value As Boolean)
            _crossdefault = Value
        End Set
    End Property

    Public Property InsuranceTerminationFlag() As String
        Get
            Return _insuranceterminationflag
        End Get
        Set(ByVal Value As String)
            _insuranceterminationflag = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property

    Public Property WaiveNo() As String
        Get
            Return _waiveno
        End Get
        Set(ByVal Value As String)
            _waiveno = Value
        End Set
    End Property

    Public Property Angsuran() As Double
        Get
            Return (CType(_Angsuran, Double))
        End Get
        Set(ByVal Value As Double)
            _Angsuran = Value
        End Set
    End Property
    Public Property Denda() As Double
        Get
            Return (CType(_Denda, Double))
        End Get
        Set(ByVal Value As Double)
            _Denda = Value
        End Set
    End Property
    Public Property BiayaSTNK() As Double
        Get
            Return (CType(_BiayaSTNK, Double))
        End Get
        Set(ByVal Value As Double)
            _BiayaSTNK = Value
        End Set
    End Property
    Public Property BiayaTarik() As Double
        Get
            Return (CType(_BiayaTarik, Double))
        End Get
        Set(ByVal Value As Double)
            _BiayaTarik = Value
        End Set
    End Property
    Public Property BiayaTolakanPDC() As Double
        Get
            Return (CType(_BiayaTolakanPDC, Double))
        End Get
        Set(ByVal Value As Double)
            _BiayaTolakanPDC = Value
        End Set
    End Property
    Public Property BiayaTagihAngsuran() As Double
        Get
            Return (CType(_BiayaTagihAngsuran, Double))
        End Get
        Set(ByVal Value As Double)
            _BiayaTagihAngsuran = Value
        End Set
    End Property
    Public Property PaidDate() As Date
        Get
            Return _PaidDate
        End Get
        Set(ByVal Value As Date)
            _PaidDate = Value
        End Set
    End Property
End Class
