
<Serializable()>
Public Class RatePphMaster : Inherits Maxiloan.Parameter.AccMntBase
    Private _level As Int16
    Private _from As Int64
    Private _to As Int64
    Private _rate_Npwp As Int16
    Private _rate_Non_Npwp As Int16
    Private _listTarifPph As DataTable

    Public Property ListRatePphMaster() As DataTable
        Get
            Return (CType(_listTarifPph, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listTarifPph = Value
        End Set
    End Property

    Public Property Level() As Int16
        Get
            Return CType(_level, Int16)
        End Get
        Set(ByVal Value As Int16)
            _level = Value
        End Set
    End Property

    Public Property AmntFrom() As Decimal
        Get
            Return CType(_from, Decimal)
        End Get
        Set(ByVal Value As Decimal)
            _from = Value
        End Set
    End Property

    Public Property AmntTo() As Decimal
        Get
            Return CType(_to, Decimal)
        End Get
        Set(ByVal Value As Decimal)
            _to = Value
        End Set
    End Property

    Public Property RateNpwp() As Int16
        Get
            Return CType(_rate_Npwp, Int16)
        End Get
        Set(ByVal Value As Int16)
            _rate_Npwp = Value
        End Set
    End Property

    Public Property RateNonNpwp() As Int16
        Get
            Return CType(_rate_Non_Npwp, Int16)
        End Get
        Set(ByVal Value As Int16)
            _rate_Non_Npwp = Value
        End Set
    End Property
End Class
