
<Serializable()> _
Public Class BankMaster : Inherits Maxiloan.Parameter.AccMntBase
    Private _bankId As String
    Private _bankname As String
    Private _status As Boolean
    Private _listbank As Boolean
    Private _listbankmaster As DataTable

    Public Property ListBankMaster() As DataTable
        Get
            Return (CType(_listbankmaster, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listbankmaster = Value
        End Set
    End Property

    Public Property BankName() As String
        Get
            Return CType(_bankname, String)
        End Get
        Set(ByVal Value As String)
            _bankname = Value
        End Set
    End Property

    Public Property Status() As Boolean
        Get
            Return CType(_status, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _status = Value
        End Set
    End Property

    Public Property ShortName As String
    Public Property SandiKliring As String
End Class
