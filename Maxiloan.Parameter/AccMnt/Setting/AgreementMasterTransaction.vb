﻿<Serializable()> _
Public Class AgreementMasterTransaction : Inherits Maxiloan.Parameter.Common

    Public Property TransID As String
    Public Property TransIDEdit As String
    Public Property TransDesc As String
    Public Property Isactive As Boolean
    Public Property PaymentAllocationIDSupplier As String
    Public Property NoUrutTrans As Integer
    Public Property BungaNet As String

    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
    Public Property isDistribuTable As Boolean

    Public Property defaultValueTagihanKeSupplier As String
    Public Property isReadOnly As Boolean
    Public Property GroupTransID As String
    Public Property SupplierEmployeePosition As String
End Class
