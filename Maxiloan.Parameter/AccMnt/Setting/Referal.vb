﻿<Serializable()>
Public Class Referal : Inherits Common

    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _NPP As String
    Private _UnitBisnisID As String
    Private _Lokasi As String
    Private _Nama As String
    Private _NPWP As String
    Private _BranchID As String
    Private _BankID As String
    Private _BankBranch As String
    Private _AccountNo As String
    Private _AccountName As String
    Private _BankBranchId As String
    Public Property UnitBisnisID() As String
        Get
            Return _UnitBisnisID
        End Get
        Set(ByVal Value As String)
            _UnitBisnisID = Value
        End Set
    End Property

    Public Overloads Property BranchID() As String
        Get
            Return _BranchID
        End Get
        Set(ByVal Value As String)
            _BranchID = Value
        End Set
    End Property

    Public Property NPP() As String
        Get
            Return _NPP
        End Get
        Set(ByVal Value As String)
            _NPP = Value
        End Set
    End Property

    Public Property Lokasi() As String
        Get
            Return _Lokasi
        End Get
        Set(ByVal Value As String)
            _Lokasi = Value
        End Set
    End Property

    Public Property NPWP() As String
        Get
            Return _NPWP
        End Get
        Set(ByVal Value As String)
            _NPWP = Value
        End Set
    End Property

    Public Property Nama() As String
        Get
            Return _Nama
        End Get
        Set(ByVal Value As String)
            _Nama = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property
    Public Property BankID() As String
        Get
            Return _BankID
        End Get
        Set(ByVal Value As String)
            _BankID = Value
        End Set
    End Property
    Public Property BankBranch() As String
        Get
            Return _BankBranch
        End Get
        Set(ByVal Value As String)
            _BankBranch = Value
        End Set
    End Property
    Public Property AccountNo() As String
        Get
            Return _AccountNo
        End Get
        Set(ByVal Value As String)
            _AccountNo = Value
        End Set
    End Property
    Public Property AccountName() As String
        Get
            Return _AccountName
        End Get
        Set(ByVal Value As String)
            _AccountName = Value
        End Set
    End Property
    Public Property BankBranchId() As String
        Get
            Return _BankBranchId
        End Get
        Set(ByVal Value As String)
            _BankBranchId = Value
        End Set
    End Property
End Class



