﻿<Serializable()>
Public Class BisnisUnit : Inherits Common

    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _totalrecords As Int64
    Private _UnitBisnisID As Int16
    Private _Nama As String


    Public Property UnitBisnisID() As Int16
        Get
            Return _UnitBisnisID
        End Get
        Set(ByVal Value As Int16)
            _UnitBisnisID = Value
        End Set
    End Property

    Public Property Nama() As String
        Get
            Return _Nama
        End Get
        Set(ByVal Value As String)
            _Nama = Value
        End Set
    End Property

    Public Property listdataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

End Class



