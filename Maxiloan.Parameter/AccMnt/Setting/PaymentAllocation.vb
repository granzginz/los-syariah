<Serializable()> _
Public Class PaymentAllocation : Inherits Common
    Private strPaymentAllocationId As String
    Private strDescription As String
    Private strCOA, strBranch, strPrgId, strPrintedBy As String
    Private _isSystem As Boolean
    Private _isScheme As Boolean
    Private _isAgreement As Boolean
    Private _IsPaymentReceive As Boolean
    Private _isPettyCash As Boolean
    Private _isHeadOffice As Boolean
    Private _isActive As Boolean
    Private _listdata As DataTable
    Private _listreport As DataSet
    Private _isPaymentRequest As Boolean

    Public Property ListPaymentAllocation() As DataTable
        Get
            Return CType(_listdata, DataTable)
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property ListReportPaymentAllocation() As DataSet
        Get
            Return CType(_listreport, DataSet)
        End Get
        Set(ByVal Value As DataSet)
            _listreport = Value
        End Set
    End Property

    Public Property PaymentAllocationId() As String
        Get
            Return CType(strPaymentAllocationId, String)
        End Get
        Set(ByVal Value As String)
            strPaymentAllocationId = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return CType(strDescription, String)
        End Get
        Set(ByVal Value As String)
            strDescription = Value
        End Set
    End Property

    Public Property COA() As String
        Get
            Return CType(strCOA, String)
        End Get
        Set(ByVal Value As String)
            strCOA = Value
        End Set
    End Property

    Public Property IsSystem() As Boolean
        Get
            Return CType(_isSystem, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _isSystem = Value
        End Set
    End Property

    Public Property IsScheme() As Boolean
        Get
            Return CType(_isScheme, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _isScheme = Value
        End Set
    End Property

    Public Property IsAgreement() As Boolean
        Get
            Return CType(_isAgreement, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _isAgreement = Value
        End Set
    End Property
    Public Property IsPaymentReceive() As Boolean
        Get
            Return CType(_IsPaymentReceive, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _IsPaymentReceive = Value
        End Set
    End Property

    Public Property IsPettyCash() As Boolean
        Get
            Return CType(_isPettyCash, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _isPettyCash = Value
        End Set
    End Property

    Public Property IsHeadOffice() As Boolean
        Get
            Return CType(_isHeadOffice, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _isHeadOffice = Value
        End Set
    End Property

    Public Property Status() As Boolean
        Get
            Return CType(_isActive, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _isActive = Value
        End Set
    End Property

    Public Property isPaymentRequest() As Boolean
        Get
            Return CType(_isPaymentRequest, Boolean)
        End Get
        Set(ByVal value As Boolean)
            _isPaymentRequest = value
        End Set
    End Property


End Class
