
<Serializable()> _
Public Class JournalScheme : Inherits Common
    Private _Id As String
    Private _Description As String
    Private _Status As String
    Private _listdatareport As DataSet
    Private _PaymentAllocationID As String
    Private _COA As String
    Private _AddEdit As String
    Private _SearchBy As String
    Private _listdata As New DataTable
    Private _totalrecords As Int64
    Public Property AddEdit() As String
        Get
            Return _AddEdit
        End Get
        Set(ByVal Value As String)
            _AddEdit = Value
        End Set
    End Property

    Public Property ListDataReport() As DataSet
        Get
            Return _listdatareport
        End Get
        Set(ByVal Value As DataSet)
            _listdatareport = Value
        End Set
    End Property

    Public Property Id() As String
        Get
            Return _Id
        End Get
        Set(ByVal Value As String)
            _Id = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property Status() As String
        Get
            Return _Status
        End Get
        Set(ByVal Value As String)
            _Status = Value
        End Set
    End Property
    Public Property PaymentAllocationID() As String
        Get
            Return _PaymentAllocationID
        End Get
        Set(ByVal Value As String)
            _PaymentAllocationID = Value
        End Set
    End Property
    Public Property COA() As String
        Get
            Return _COA
        End Get
        Set(ByVal Value As String)
            _COA = Value
        End Set
    End Property
    Public Property SearchBy() As String
        Get
            Return _SearchBy
        End Get
        Set(ByVal Value As String)
            _SearchBy = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

End Class
