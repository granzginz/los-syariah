
<Serializable()> _
Public Class BankAccount : Inherits Maxiloan.Parameter.AccMntBase

    Private _bankaccountname As String
    Private _accountname As String
    Private _accountno As String
    Private _bankbranch As String
    Private _bankName As String

    Private _bankstatus As Boolean
    Private _isgenerate As Boolean
    Private _listbankaccount As DataTable

    Private _bankbranchid As String

    Public Property ListBankAccount() As DataTable
        Get
            Return (CType(_listbankaccount, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listbankaccount = Value
        End Set
    End Property

    Public Property AccountName() As String
        Get
            Return CType(_accountname, String)
        End Get
        Set(ByVal Value As String)
            _accountname = Value
        End Set
    End Property

    Public Property AccountNo() As String
        Get
            Return CType(_accountno, String)
        End Get
        Set(ByVal Value As String)
            _accountno = Value
        End Set
    End Property

    Public Property BankBranch() As String
        Get
            Return CType(_bankbranch, String)
        End Get
        Set(ByVal Value As String)
            _bankbranch = Value
        End Set
    End Property

    Public Property Status() As Boolean
        Get
            Return CType(_bankstatus, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _bankstatus = Value
        End Set
    End Property

    Public Property IsGenerate() As Boolean
        Get
            Return CType(_isgenerate, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _isgenerate = Value
        End Set
    End Property

    Public Property BankName() As String
        Get
            Return CType(_bankName, String)
        End Get
        Set(ByVal Value As String)
            _bankName = Value
        End Set
    End Property

    Public Property BankBranchId() As String
        Get
            Return CType(_bankbranchid, String)
        End Get
        Set(ByVal Value As String)
            _bankbranchid = Value
        End Set
    End Property

    'Add
    Public Property TarifSKN As Decimal
    Public Property TarifRTGS As Decimal
    Public Property BankAccountDefault As Boolean
    Public Property SweepOtomatis As Boolean
End Class
