﻿<Serializable()> _
Public Class BankBranchMaster : Inherits Maxiloan.Parameter.Common    
    Public Property BankBranchid As Integer
    Public Property BankCode As String
    Public Property BankBranchName As String
    Public Property BankID As String
    Public Property IsActive As Boolean
    Public Property City As String
    Public Property Address As String
    Public Property BankName As String
    Public Property ShortName As String
    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
End Class
