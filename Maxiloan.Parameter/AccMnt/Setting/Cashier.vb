

<Serializable()> _
Public Class Cashier : Inherits Maxiloan.Parameter.AccMntBase
    Private _cashierid As String
    Private _cashiername As String
    Private _cashiertitle As String
    Private _cashierstatus As Boolean
    Private _listcashier As DataTable

    Public Property ListCashier() As DataTable
        Get
            Return CType(_listcashier, DataTable)
        End Get
        Set(ByVal Value As DataTable)
            _listcashier = Value
        End Set
    End Property


    Public Property CashierID() As String
        Get
            Return CType(_cashierid, String)
        End Get
        Set(ByVal Value As String)
            _cashierid = Value
        End Set
    End Property

    Public Property CashierName() As String
        Get
            Return CType(_cashiername, String)
        End Get
        Set(ByVal Value As String)
            _cashiername = Value
        End Set
    End Property

    Public Property CashierStatus() As Boolean
        Get
            Return CType(_cashierstatus, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _cashierstatus = Value
        End Set
    End Property

    Public Property CashierTitle() As String
        Get
            Return CType(_cashiertitle, String)
        End Get
        Set(ByVal Value As String)
            _cashiertitle = Value
        End Set
    End Property
End Class
