
<Serializable()> _
Public Class AgreementList
    Inherits Maxiloan.Parameter.AccMntBase

    Private _listagreement As DataTable
    Private _hasil As Integer
    Private _spname As String    

    Public Property SPName() As String
        Get
            Return _spname
        End Get
        Set(ByVal Value As String)
            _spname = Value
        End Set
    End Property


    Public Property Hasil() As Integer
        Get
            Return _hasil
        End Get
        Set(ByVal Value As Integer)
            _hasil = Value
        End Set
    End Property

    Public Property ListAgreement() As DataTable
        Get
            Return (CType(_listagreement, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listagreement = Value
        End Set
    End Property

    Public Property InsSeqNo As String
    Public Property VoucherNo As String
    Public Property PostingDate As Date
    Public Property InvoiceNo() As String
    Public Property InvoiceSeqNo() As Integer
    Public Property Amount() As Double
    Public Property ModuleApplication As String
End Class
