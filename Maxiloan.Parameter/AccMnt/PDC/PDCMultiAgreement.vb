
Public Class PDCMultiAgreement : Inherits PDCReceive
    Private _agreementlist As DataTable
    Private _filename As String
    Private _tablepdc As DataTable
    Private _oldtablepdc As DataTable
    Private _totalpdcgroupamount As Double

    Public Property TotalPDCGroupAmount() As Double
        Get
            Return _totalpdcgroupamount
        End Get
        Set(ByVal Value As Double)
            _totalpdcgroupamount = Value
        End Set
    End Property
    Public Property AgreementList() As DataTable
        Get
            Return _agreementlist
        End Get
        Set(ByVal Value As DataTable)
            _agreementlist = Value
        End Set
    End Property

    Public Property FileName() As String
        Get
            Return _filename
        End Get
        Set(ByVal Value As String)
            _filename = Value
        End Set
    End Property

    Public Property TablePDC() As DataTable
        Get
            Return _tablepdc
        End Get
        Set(ByVal Value As DataTable)
            _tablepdc = Value
        End Set
    End Property

    Public Property OldTablePDC() As DataTable
        Get
            Return _oldtablepdc
        End Get
        Set(ByVal Value As DataTable)
            _oldtablepdc = Value
        End Set
    End Property
End Class
