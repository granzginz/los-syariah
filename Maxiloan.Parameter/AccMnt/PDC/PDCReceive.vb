

<Serializable()> _
Public Class PDCReceive
    Inherits Maxiloan.Parameter.AccMntBase
#Region "Constanta"
    Private _listPDC As DataTable
    Private _listPDCD As DataTable
    Private _listPDCData As DataTable
    Private _listPDCPA As DataTable
    Private _listPDCHistory As DataTable
    Private _listPDCStatus As DataTable
    Private _listPDCClear As DataTable
    Private _listPDCSave As DataTable
    Private _branchpdc As String
    Private _branchlocation As String

    Private _InstallmentAmount As Double
    Private _PaymentFrequency As Integer
    Private _GrandTotPDCAmount As Double
    Private _groupreceiptno As String
    Private _ReceiveFrom As String

    Private _GiroNo As String
    Private _PDCAmount As Double
    Private _BankPDC As String
    Private _BankPDCName As String
    Private _NumPDC As Integer
    Private _PDCDueDate As Date
    Private _PDCValueDate As Date
    Private _IsInkaso As Boolean
    Private _IsCumm As Boolean
    Private _PDCType As String
    Private _IsValidPDC As Boolean
    Private _holidaydate As Hashtable
    Private _description As String
    Private _listgirono As String
    Private _prefixpdc As String
    Private _IsExists As Boolean
    Private _hpsxml As String
    Private _flagdelete As String
    Private _CDtgrid As Integer
    Private _IndexDelete As String
    Private _pdcReceiptNo As String

    Private _referencesno As String

    Private _PaymentAllocationName As String
    Private _TransDescription As String
    Private _TransAmount As Double
    Private _SelectAll As Double

    Private _GroupPDCNo As String
    Private _Bounce As Integer
    Private _BankaccountidDep As String
    Private _BranchLocationName As String
    Private _BranchInitialName As String
    Private _PDCStatus As String
    Private _BankClearing As String
    Private _BankClearingName As String
    Private _PDCStatusDate As Date
    Private _HoldUntildate As Date
    Private _RecDate As Date
    Private _Notes As String
    Private _ReceivedBy As String
    Private _LastPrintRecDate As Date
    Private _NumOfPrint As Double
    Private _StatusDate As Date
    Private _paramReport As String
    Private _GroupVoucherno As String
    Private _GroupJournalno As String
    Private _ivalueDate As Date
    Private _strError As String
    Private _GiroSeqNo As Integer
#End Region

#Region "PDC Receive"
    Public Property BranchPDC() As String
        Get
            Return _branchpdc
        End Get
        Set(ByVal Value As String)
            _branchpdc = Value
        End Set
    End Property

    Public Property ListPDC() As DataTable
        Get
            Return (CType(_listPDC, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listPDC = Value
        End Set
    End Property
    Public Property ListPDCSave() As DataTable
        Get
            Return (CType(_listPDCSave, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listPDCSave = Value
        End Set
    End Property


    Public Property PaymentFrequency() As Integer
        Get
            Return CType(_PaymentFrequency, Integer)
        End Get
        Set(ByVal Value As Integer)
            _PaymentFrequency = Value
        End Set
    End Property

    Public Property GrandTotPDCAmount() As Double
        Get
            Return CType(_GrandTotPDCAmount, Double)
        End Get
        Set(ByVal Value As Double)
            _GrandTotPDCAmount = Value
        End Set
    End Property

    Public Property GiroNo() As String
        Get
            Return CType(_GiroNo, String)
        End Get
        Set(ByVal Value As String)
            _GiroNo = Value
        End Set
    End Property
    Public Property GiroSeqNo() As Integer
        Get
            Return CType(_GiroSeqNo, Integer)
        End Get
        Set(ByVal Value As Integer)
            _GiroSeqNo = Value
        End Set
    End Property
    Public Property PDCAmount() As Double
        Get
            Return CType(_PDCAmount, Double)
        End Get
        Set(ByVal Value As Double)
            _PDCAmount = Value
        End Set
    End Property

    Public Property BankPDC() As String
        Get
            Return CType(_BankPDC, String)
        End Get
        Set(ByVal Value As String)
            _BankPDC = Value
        End Set
    End Property

    Public Property BankPDCName() As String
        Get
            Return CType(_BankPDCName, String)
        End Get
        Set(ByVal Value As String)
            _BankPDCName = Value
        End Set
    End Property

    Public Property NumPDC() As Integer
        Get
            Return CType(_NumPDC, Integer)
        End Get
        Set(ByVal Value As Integer)
            _NumPDC = Value
        End Set
    End Property

    Public Property PDCDueDate() As Date
        Get
            Return CType(_PDCDueDate, Date)
        End Get
        Set(ByVal Value As Date)
            _PDCDueDate = Value
        End Set
    End Property
    Public Property PDCValueDate() As Date
        Get
            Return CType(_PDCValueDate, Date)
        End Get
        Set(ByVal Value As Date)
            _PDCValueDate = Value
        End Set
    End Property

    Public Property IsInkaso() As Boolean
        Get
            Return CType(_IsInkaso, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _IsInkaso = Value
        End Set
    End Property

    Public Property IsCumm() As Boolean
        Get
            Return CType(_IsCumm, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _IsCumm = Value
        End Set
    End Property

    Public Property PDCType() As String
        Get
            Return CType(_PDCType, String)
        End Get
        Set(ByVal Value As String)
            _PDCType = Value
        End Set
    End Property

    Public Property IsValidPDC() As Boolean
        Get
            Return CType(_IsValidPDC, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _IsValidPDC = Value
        End Set
    End Property

    Public Property IsExists() As Boolean
        Get
            Return CType(_IsExists, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _IsExists = Value
        End Set
    End Property

    Public Property HolidayDate() As Hashtable
        Get
            Return CType(_holidaydate, Hashtable)
        End Get
        Set(ByVal Value As Hashtable)
            _holidaydate = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return CType(_description, String)
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property

    Public Property ListGiroNo() As String
        Get
            Return CType(_listgirono, String)
        End Get
        Set(ByVal Value As String)
            _listgirono = Value
        End Set
    End Property

    Public Property PrefixPDC() As String
        Get
            Return CType(_prefixpdc, String)
        End Get
        Set(ByVal Value As String)
            _prefixpdc = Value
        End Set
    End Property

    Public Property HpsXml() As String
        Get
            Return CType(_hpsxml, String)
        End Get
        Set(ByVal Value As String)
            _hpsxml = Value
        End Set
    End Property

    Public Property FlagDelete() As String
        Get
            Return CType(_flagdelete, String)
        End Get
        Set(ByVal Value As String)
            _flagdelete = Value
        End Set
    End Property

    Public Property CDtGrid() As Integer
        Get
            Return CType(_CDtgrid, Integer)
        End Get
        Set(ByVal Value As Integer)
            _CDtgrid = Value
        End Set
    End Property
    Public Property IndexDelete() As String
        Get
            Return CType(_IndexDelete, String)
        End Get
        Set(ByVal Value As String)
            _IndexDelete = Value
        End Set
    End Property


    Public Property PDCReceiptNo() As String
        Get
            Return CType(_pdcReceiptNo, String)
        End Get
        Set(ByVal Value As String)
            _pdcReceiptNo = Value
        End Set
    End Property
#End Region

#Region "Tambahan Untuk Multi PDC"


    Public Property PaymentAllocationName() As String
        Get
            Return CType(_PaymentAllocationName, String)
        End Get
        Set(ByVal Value As String)
            _PaymentAllocationName = Value
        End Set
    End Property

    Public Property TransDescription() As String
        Get
            Return CType(_TransDescription, String)
        End Get
        Set(ByVal Value As String)
            _TransDescription = Value
        End Set
    End Property

    Public Property TransAmount() As Double
        Get
            Return CType(_TransAmount, Double)
        End Get
        Set(ByVal Value As Double)
            _TransAmount = Value
        End Set
    End Property
#End Region

#Region "PDCDeposit"
    Public Property SelectAll() As Double
        Get
            Return CType(_SelectAll, Double)
        End Get
        Set(ByVal Value As Double)
            _SelectAll = Value
        End Set
    End Property

    Public Property BranchLocation() As String
        Get
            Return CType(_branchlocation, String)
        End Get
        Set(ByVal Value As String)
            _branchlocation = Value
        End Set
    End Property

    Public Property ListPDCD() As DataTable
        Get
            Return (CType(_listPDCD, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listPDCD = Value
        End Set
    End Property

    Public Property ListPDCData() As DataTable
        Get
            Return (CType(_listPDCData, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listPDCData = Value
        End Set
    End Property

    Public Property GroupPDCNo() As String
        Get
            Return (CType(_GroupPDCNo, String))
        End Get
        Set(ByVal Value As String)
            _GroupPDCNo = Value
        End Set
    End Property

    Public Property Bounce() As Integer
        Get
            Return (CType(_Bounce, Integer))
        End Get
        Set(ByVal Value As Integer)
            _Bounce = Value
        End Set
    End Property

    Public Property BankaccountIDDep() As String
        Get
            Return (CType(_BankaccountidDep, String))
        End Get
        Set(ByVal Value As String)
            _BankaccountidDep = Value
        End Set
    End Property
    Public Property strError() As String
        Get
            Return (CType(_strError, String))
        End Get
        Set(ByVal Value As String)
            _strError = Value
        End Set
    End Property
    Public Property GroupReceiptNo() As String
        Get
            Return (CType(_groupreceiptno, String))
        End Get
        Set(ByVal Value As String)
            _groupreceiptno = Value
        End Set
    End Property

#End Region

#Region "PDCChangeStatus"
    Public Property BranchLocationName() As String
        Get
            Return (CType(_BranchLocationName, String))
        End Get
        Set(ByVal Value As String)
            _BranchLocationName = Value
        End Set
    End Property

    Public Property BranchInitialName() As String
        Get
            Return (CType(_BranchInitialName, String))
        End Get
        Set(ByVal Value As String)
            _BranchInitialName = Value
        End Set
    End Property

    Public Property PDCStatus() As String
        Get
            Return (CType(_PDCStatus, String))
        End Get
        Set(ByVal Value As String)
            _PDCStatus = Value
        End Set
    End Property

    Public Property BankClearing() As String
        Get
            Return (CType(_BankClearing, String))
        End Get
        Set(ByVal Value As String)
            _BankClearing = Value
        End Set
    End Property

    Public Property BankClearingName() As String
        Get
            Return (CType(_BankClearingName, String))
        End Get
        Set(ByVal Value As String)
            _BankClearingName = Value
        End Set
    End Property

    Public Property PDCStatusDate() As Date
        Get
            Return (CType(_PDCStatusDate, Date))
        End Get
        Set(ByVal Value As Date)
            _PDCStatusDate = Value
        End Set
    End Property

    Public Property HoldUntildate() As Date
        Get
            Return (CType(_HoldUntildate, Date))
        End Get
        Set(ByVal Value As Date)
            _HoldUntildate = Value
        End Set
    End Property

    Public Property RecDate() As Date
        Get
            Return (CType(_RecDate, Date))
        End Get
        Set(ByVal Value As Date)
            _RecDate = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return (CType(_Notes, String))
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property



#End Region

#Region "PDCReceiptInquiry"
    Public Property ReceivedBy() As String
        Get
            Return (CType(_ReceivedBy, String))
        End Get
        Set(ByVal Value As String)
            _ReceivedBy = Value
        End Set
    End Property



    Public Property LastPrintRecDate() As Date
        Get
            Return (CType(_LastPrintRecDate, Date))
        End Get
        Set(ByVal Value As Date)
            _LastPrintRecDate = Value
        End Set
    End Property

    Public Property NumOfPrint() As Double
        Get
            Return (CType(_NumOfPrint, Double))
        End Get
        Set(ByVal Value As Double)
            _NumOfPrint = Value
        End Set
    End Property

    Public Property StatusDate() As Date
        Get
            Return (CType(_StatusDate, Date))
        End Get
        Set(ByVal Value As Date)
            _StatusDate = Value
        End Set
    End Property

    Public Property listPDCPA() As DataTable
        Get
            Return (CType(_listPDCPA, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listPDCPA = Value
        End Set
    End Property

    Public Property listPDCHistory() As DataTable
        Get
            Return (CType(_listPDCHistory, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listPDCHistory = Value
        End Set
    End Property

    Public Property paramReport() As String
        Get
            Return (CType(_paramReport, String))
        End Get
        Set(ByVal Value As String)
            _paramReport = Value
        End Set
    End Property
#End Region

#Region "PDCInqStatus"
    Public Property listPDCStatus() As DataTable
        Get
            Return (CType(_listPDCStatus, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listPDCStatus = Value
        End Set
    End Property
#End Region

#Region "PDCClearReconcile"
    Public Property GroupVoucherno() As String
        Get
            Return (CType(_GroupVoucherno, String))
        End Get
        Set(ByVal Value As String)
            _GroupVoucherno = Value
        End Set
    End Property

    Public Property GroupJournalno() As String
        Get
            Return (CType(_GroupJournalno, String))
        End Get
        Set(ByVal Value As String)
            _GroupJournalno = Value
        End Set
    End Property

    Public Property listPDCClear() As DataTable
        Get
            Return (CType(_listPDCClear, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listPDCClear = Value
        End Set
    End Property

    Public Property ivalueDate() As Date
        Get
            Return (CType(_ivalueDate, Date))
        End Get
        Set(ByVal Value As Date)
            _ivalueDate = Value
        End Set
    End Property
#End Region


    Public Property PDCSource As IList(Of PDCParam)
    Public Shared Function ToDataTable(businessDate As DateTime, PDCStatus As String, FisikStatus As String, data As IList(Of PDCParam)) As DataTable
        Dim dt = New DataTable()
        Dim seq = 1

        dt.Columns.Add("GiroNo", GetType(String))
        dt.Columns.Add("GiroSeq", GetType(Integer))
        dt.Columns.Add("Recdate", GetType(DateTime))
        dt.Columns.Add("bankID", GetType(String))
        dt.Columns.Add("PDCAmount", GetType(Decimal))
        dt.Columns.Add("PDCDueDate", GetType(DateTime))
        dt.Columns.Add("PDCType", GetType(String))
        dt.Columns.Add("IsCumm", GetType(Boolean))
        dt.Columns.Add("IsInkaso", GetType(Boolean))
        dt.Columns.Add("PDCStatus", GetType(String))
        dt.Columns.Add("FisikStatus", GetType(String))
        dt.Columns.Add("StatusDate", GetType(DateTime))
        dt.Columns.Add("PaymentAllocationID", GetType(String))
        dt.Columns.Add("Description", GetType(String))
        For Each v In data
            Dim row = dt.NewRow()

            row("GiroNo") = v.PDCNo
            row("GiroSeq") = seq
            row("Recdate") = businessDate
            row("bankID") = v.BankID
            row("PDCAmount") = v.PDCAmount
            row("PDCDueDate") = v.DueDate
            row("PDCType") = v.Type
            row("IsCumm") = v.IsCummulative
            row("IsInkaso") = v.IsInkaso
            row("PDCStatus") = PDCStatus
            row("FisikStatus") = FisikStatus
            row("StatusDate") = businessDate
            row("PaymentAllocationID") = v.PaymentAllocationID
            row("Description") = v.Description
            dt.Rows.Add(row)
            seq += 1
        Next
        Return dt
    End Function


End Class
