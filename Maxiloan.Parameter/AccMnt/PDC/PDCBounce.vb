
<Serializable()> _
Public Class PDCBounce : Inherits Maxiloan.Parameter.AccMntBase
    Private _listpdcbounce As DataTable
    Private _cleardate As Date
    Private _girono As String
    Private _pdcreceiptno As String
    Private _bankname As String
    Private _pdcduedate As Date
    Private _totalpdcamount As Double
    Private _pdctype As String
    Private _iscummulative As Boolean
    Private _isinkaso As Boolean
    Private _bankaccountname As String
    Private _notes As String
    Public Property ListPDCBounce() As DataTable
        Get
            Return _listpdcbounce
        End Get
        Set(ByVal Value As DataTable)
            _listpdcbounce = Value
        End Set
    End Property

    Public Property ClearDate() As Date
        Get
            Return _cleardate
        End Get
        Set(ByVal Value As Date)
            _cleardate = Value
        End Set
    End Property

    Public Property GiroNo() As String
        Get
            Return _girono
        End Get
        Set(ByVal Value As String)
            _girono = Value
        End Set
    End Property

    Public Property PDCReceiptNo() As String
        Get
            Return _pdcreceiptno
        End Get
        Set(ByVal Value As String)
            _pdcreceiptno = Value
        End Set
    End Property

    Public Property BankName() As String
        Get
            Return _bankname
        End Get
        Set(ByVal Value As String)
            _bankname = Value
        End Set
    End Property


    Public Property PDCDueDate() As Date
        Get
            Return _pdcduedate
        End Get
        Set(ByVal Value As Date)
            _pdcduedate = Value
        End Set
    End Property

    Public Property TotalPDCAmount() As Double
        Get
            Return _totalpdcamount
        End Get
        Set(ByVal Value As Double)
            _totalpdcamount = Value
        End Set
    End Property

    Public Property PDCType() As String
        Get
            Return _pdctype
        End Get
        Set(ByVal Value As String)
            _pdctype = Value
        End Set
    End Property

    Public Property IsCummulative() As Boolean
        Get
            Return _iscummulative
        End Get
        Set(ByVal Value As Boolean)
            _iscummulative = Value
        End Set
    End Property

    Public Property IsInkaso() As Boolean
        Get
            Return _isinkaso
        End Get
        Set(ByVal Value As Boolean)
            _isinkaso = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property
End Class
