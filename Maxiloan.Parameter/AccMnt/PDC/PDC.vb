﻿
<Serializable()>
Public Class PDCParam
    Public Property seq As Integer
    Public Property Holiday As String
    Public Property BankPDC As String
    Public Property BankID As String
    Public Property PDCNo As String
    Public Property PDCAmount As Decimal
    Public Property DueDate As DateTime
    Public Property IsInkaso As Boolean
    Public Property IsCummulative As Boolean
    Public Property Type As String
    Public Property Description As String
    Public Property PaymentAllocationID As String
        Get
            Return _paymentAllocationID
        End Get
        Set(value As String)
            _paymentAllocationID = value
        End Set
    End Property
    Private _paymentAllocationID As String = ""
End Class
