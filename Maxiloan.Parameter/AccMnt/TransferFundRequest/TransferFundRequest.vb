
<Serializable()>
Public Class TransferFundRequest : Inherits Common
    Private _listdata As DataTable
    Private _listdataHistory As DataTable
    Private _listdataHistoryApproval As DataTable
    Private _totalrecords As Int64
    Private _PaymentAllocationID As String
    Private _PaymentAllDescription As String
    Private _BankAccount As String
    Private _Departement As String
    Private _DepartementId As String
    Private _Amount As Double
    Private _Description As String
    Private _TotalAmount As Double
    Private _IsValid As Boolean
    Private _hpsxml As String
    Private _flagdelete As String
    Private _IndexDelete As String
    Private _RequestNo As String
    Private _NumPR As Integer
    Private _PRDataTable As DataTable
    Private _ID As String
    Private _BankAccountLain As String
    '**** REQEST INQ VIEW ADDITIONAL FIELDS
    Private _RequestDate As Date
    Private _TransferRefVoucherNo As String
    Private _TransferReferenceNo As String
    Private _TransferDate As Date
    Private _TransferAmount As Double
    Private _Status As String
    Private _RequestBy As String
    Private _StatusDate As Date
    Private _PRDataSet As DataSet
    Private _NoMemo As String
    Private _NoRekening As String
    Private _StatusBank As String
    Private _NamaRekening As String
    Private _NamaBank As String
    Private _OtherBankAccount As String
    Private _BankAccountId As String
    Private _isPotong As Boolean

    Public Property PaymentStatus As String
    Public Property notes As String
    Public Property PayReqStatus As String
    Public Property IsReconcile As Boolean
    Public Property BankId As String
    Public Property SandiBank As String
    Public Property _Error As String
    Public Property Num As Int64
    Public Property NumApprove As Int64

    Public Property isPotong() As Boolean
        Get
            Return _isPotong
        End Get
        Set(value As Boolean)
            _isPotong = value
        End Set
    End Property

    Public Property BankAccountLain() As String
        Get
            Return _BankAccountLain
        End Get
        Set(value As String)
            _BankAccountLain = value
        End Set
    End Property
    Public Property NoMemo() As String
        Get
            Return _NoMemo
        End Get
        Set(value As String)
            _NoMemo = value
        End Set
    End Property

    Public Property NoRekening() As String
        Get
            Return _NoRekening
        End Get
        Set(value As String)
            _NoRekening = value
        End Set
    End Property
    Public Property StatusBank() As String
        Get
            Return _StatusBank
        End Get
        Set(value As String)
            _StatusBank = value
        End Set
    End Property
    Public Property NamaRekening() As String
        Get
            Return _NamaRekening
        End Get
        Set(value As String)
            _NamaRekening = value
        End Set
    End Property
    Public Property NamaBank() As String
        Get
            Return _NamaBank
        End Get
        Set(value As String)
            _NamaBank = value
        End Set
    End Property

    Public Property RequestNo() As String
        Get
            Return CType(_RequestNo, String)
        End Get
        Set(ByVal Value As String)
            _RequestNo = Value
        End Set
    End Property

    Public Property ID() As String
        Get
            Return CType(_ID, String)
        End Get
        Set(ByVal Value As String)
            _ID = Value
        End Set
    End Property

    Public Property PRDataTable() As DataTable
        Get
            Return _PRDataTable
        End Get
        Set(ByVal Value As DataTable)
            _PRDataTable = Value
        End Set
    End Property

    Public Property NumPR() As Integer
        Get
            Return CType(_NumPR, Integer)
        End Get
        Set(ByVal Value As Integer)
            _NumPR = Value
        End Set
    End Property

    Public Property IndexDelete() As String
        Get
            Return CType(_IndexDelete, String)
        End Get
        Set(ByVal Value As String)
            _IndexDelete = Value
        End Set
    End Property

    Public Property flagdelete() As String
        Get
            Return CType(_flagdelete, String)
        End Get
        Set(ByVal Value As String)
            _flagdelete = Value
        End Set
    End Property

    Public Property hpsxml() As String
        Get
            Return CType(_hpsxml, String)
        End Get
        Set(ByVal Value As String)
            _hpsxml = Value
        End Set
    End Property

    Public Property IsValid() As Boolean
        Get
            Return CType(_IsValid, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            _IsValid = Value
        End Set
    End Property

    Public Property Amount() As Double
        Get
            Return _Amount
        End Get
        Set(ByVal Value As Double)
            _Amount = Value
        End Set
    End Property

    Public Property TotalAmount() As Double
        Get
            Return _TotalAmount
        End Get
        Set(ByVal Value As Double)
            _TotalAmount = Value
        End Set
    End Property

    Public Property PaymentAllocationID() As String
        Get
            Return _PaymentAllocationID
        End Get
        Set(ByVal Value As String)
            _PaymentAllocationID = Value
        End Set
    End Property

    Public Property PaymentAllDescription() As String
        Get
            Return _PaymentAllDescription
        End Get
        Set(ByVal Value As String)
            _PaymentAllDescription = Value
        End Set
    End Property

    Public Property BankAccount() As String
        Get
            Return _BankAccount
        End Get
        Set(ByVal Value As String)
            _BankAccount = Value
        End Set
    End Property
    Public Property BankAccountId() As String
        Get
            Return _BankAccountId
        End Get
        Set(ByVal Value As String)
            _BankAccountId = Value
        End Set
    End Property
    Public Property OtherBankAccount() As String
        Get
            Return _OtherBankAccount
        End Get
        Set(ByVal Value As String)
            _OtherBankAccount = Value
        End Set
    End Property

    Public Property DepartementId() As String
        Get
            Return _DepartementId
        End Get
        Set(ByVal Value As String)
            _DepartementId = Value
        End Set
    End Property
    Public Property Departement() As String
        Get
            Return _Departement
        End Get
        Set(ByVal Value As String)
            _Departement = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property ListDataHistory() As DataTable
        Get
            Return _listdataHistory
        End Get
        Set(ByVal Value As DataTable)
            _listdataHistory = Value
        End Set
    End Property

    Public Property TotalRecords() As Int64
        Get
            Return _totalrecords
        End Get
        Set(ByVal Value As Int64)
            _totalrecords = Value
        End Set
    End Property

    Public Property RequestDate() As Date
        Get
            Return _RequestDate
        End Get
        Set(ByVal Value As Date)
            _RequestDate = Value
        End Set
    End Property
    Public Property TransferRefVoucherNo() As String
        Get
            Return _TransferRefVoucherNo
        End Get
        Set(ByVal Value As String)
            _TransferRefVoucherNo = Value
        End Set
    End Property
    Public Property TransferReferenceNo() As String
        Get
            Return _TransferReferenceNo
        End Get
        Set(ByVal Value As String)
            _TransferReferenceNo = Value
        End Set
    End Property
    Public Property TransferDate() As Date
        Get
            Return _TransferDate
        End Get
        Set(ByVal Value As Date)
            _TransferDate = Value
        End Set
    End Property
    Public Property TransferAmount() As Double
        Get
            Return _TransferAmount
        End Get
        Set(ByVal Value As Double)
            _TransferAmount = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _Status
        End Get
        Set(ByVal Value As String)
            _Status = Value
        End Set
    End Property
    Public Property RequestBy() As String
        Get
            Return _RequestBy
        End Get
        Set(ByVal Value As String)
            _RequestBy = Value
        End Set
    End Property
    Public Property StatusDate() As Date
        Get
            Return _StatusDate
        End Get
        Set(ByVal Value As Date)
            _StatusDate = Value
        End Set
    End Property
    Public Property PRDataSet() As DataSet
        Get
            Return _PRDataSet
        End Get
        Set(ByVal Value As DataSet)
            _PRDataSet = Value
        End Set
    End Property
    Public Property Err() As String
        Get
            Return _Error
        End Get
        Set(ByVal Value As String)
            _Error = Value
        End Set
    End Property
    Public Property Approval As Parameter.Approval
    Public Property TransactionName As String

    Public Property ListDataHistoryApproval() As DataTable
        Get
            Return _listdataHistoryApproval
        End Get
        Set(ByVal Value As DataTable)
            _listdataHistoryApproval = Value
        End Set
    End Property
End Class


