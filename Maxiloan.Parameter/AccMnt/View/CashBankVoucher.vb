

Public Class CashBankVoucher : Inherits Maxiloan.Parameter.Common
    Private _voucherno As String
    Private _listcashbankvoucher As DataTable

    Public Property VoucherNo() As String
        Get
            Return (CType(_voucherno, String))
        End Get
        Set(ByVal Value As String)
            _voucherno = Value
        End Set
    End Property
    Public Property ListCashBankVoucher() As DataTable
        Get
            Return (CType(_listcashbankvoucher, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listcashbankvoucher = Value
        End Set
    End Property
End Class
