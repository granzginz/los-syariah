
<Serializable()> _
Public Class PaymentHistory : Inherits Maxiloan.Parameter.AccMntBase
    Private _listpaymenthistoryheader As DataTable
    Private _listpaymenthistorydetail As DataTable
    Private _historysequenceNo As Integer
    Private _status As String
    Private _statusdate As String
    Private _transactiontype As String
    Private _wop As String
    Private _ReceiptNo As String
    Private _PrintBy As String
    Private _NumOfPrint As Integer
    Private _LastPrintDate As String
    Private _girono As String
    Private _bankname As String
    Private _InvoiceNo As String
    Private _InvoiceSeqNo As Integer

    Public Property ListPaymentHistoryHeader() As DataTable
        Get
            Return CType(_listpaymenthistoryheader, DataTable)
        End Get
        Set(ByVal Value As DataTable)
            _listpaymenthistoryheader = Value
        End Set
    End Property

    Public Property ListPaymentHistoryDetail() As DataTable
        Get
            Return CType(_listpaymenthistorydetail, DataTable)
        End Get
        Set(ByVal Value As DataTable)
            _listpaymenthistorydetail = Value
        End Set
    End Property

    Public Property HistorySequenceNo() As Integer
        Get
            Return CType(_historysequenceNo, Integer)
        End Get
        Set(ByVal Value As Integer)
            _historysequenceNo = Value
        End Set
    End Property

    Public Property Status() As String
        Get
            Return CType(_status, String)
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property

    Public Property StatusDate() As String
        Get
            Return CType(_statusdate, String)
        End Get
        Set(ByVal Value As String)
            _statusdate = Value
        End Set
    End Property


    Public Property TransactionType() As String
        Get
            Return CType(_transactiontype, String)
        End Get
        Set(ByVal Value As String)
            _transactiontype = Value
        End Set
    End Property

    Public Property PaymentTypeID() As String
        Get
            Return (CType(_wop, String))
        End Get
        Set(ByVal Value As String)
            _wop = Value
        End Set
    End Property

    Public Property ReceiptNo() As String
        Get
            Return (CType(_ReceiptNo, String))
        End Get
        Set(ByVal Value As String)
            _ReceiptNo = Value
        End Set
    End Property

    Public Property PrintBy() As String
        Get
            Return (CType(_PrintBy, String))
        End Get
        Set(ByVal Value As String)
            _PrintBy = Value
        End Set
    End Property

    Public Property NumOfPrint() As Integer
        Get
            Return (CType(_NumOfPrint, Integer))
        End Get
        Set(ByVal Value As Integer)
            _NumOfPrint = Value
        End Set
    End Property

    Public Property LastPrintDate() As String
        Get
            Return (CType(_LastPrintDate, String))
        End Get
        Set(ByVal Value As String)
            _LastPrintDate = Value
        End Set
    End Property

    Public Property BankName() As String
        Get
            Return _bankname
        End Get
        Set(ByVal Value As String)
            _bankname = Value
        End Set
    End Property
    Public Property GiroNo() As String
        Get
            Return _girono
        End Get
        Set(ByVal Value As String)
            _girono = Value
        End Set
    End Property

    Public Property InvoiceNo() As String
        Get
            Return _InvoiceNo
        End Get
        Set(ByVal Value As String)
            _InvoiceNo = Value
        End Set
    End Property

    Public Property InvoiceSeqNo() As Integer
        Get
            Return (CType(_InvoiceSeqNo, Integer))
        End Get
        Set(ByVal Value As Integer)
            _InvoiceSeqNo = Value
        End Set
    End Property

    Private _ReferenceNoOut As String
    Public Property ReferenceNoOut() As String
        Get
            Return _ReferenceNoOut
        End Get
        Set(ByVal value As String)
            _ReferenceNoOut = value
        End Set
    End Property

    Private _ReasonReversal As String
    Public Property ReasonReversal() As String
        Get
            Return _ReasonReversal
        End Get
        Set(ByVal value As String)
            _ReasonReversal = value
        End Set
    End Property

    Public Property Err As String

End Class
