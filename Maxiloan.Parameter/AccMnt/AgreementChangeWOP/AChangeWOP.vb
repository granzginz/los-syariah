
<Serializable()> _
Public Class AChangeWOP : Inherits Maxiloan.Parameter.AccMntBase
    Private _WOP As String
    Private _WOPDesc As String
    Private _AdminFee As Double
    Private _WOPToBe As String
    Private _Notes As String

    Public Property WOP() As String
        Get
            Return _WOP
        End Get
        Set(ByVal Value As String)
            _WOP = Value
        End Set
    End Property


    Public Property WOPDesc() As String
        Get
            Return _WOPDesc
        End Get
        Set(ByVal Value As String)
            _WOPDesc = Value
        End Set
    End Property

    Public Property AdminFee() As Double
        Get
            Return _AdminFee
        End Get
        Set(ByVal Value As Double)
            _AdminFee = Value
        End Set
    End Property

    Public Property WOPToBe() As String
        Get
            Return _WOPToBe
        End Get
        Set(ByVal Value As String)
            _WOPToBe = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property
End Class
