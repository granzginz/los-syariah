
<Serializable()>
Public Class Drawdown
    Inherits Maxiloan.Parameter.Common

    Public Property listData As DataTable
    Public Property ApplicationID As String
    Public Property DrawdownNo As String
    Public Property InsSeqNo As Int16
    Public Property DrawdownDate As String
    Public Property InstallmentAmount As Decimal
    Public Property DrawdownAmount As Decimal
    Public Property OutstandingPrincipal As Decimal
    Public Property InstallmentAmountUnused As Decimal
    Public Property InterestPeriodFrom As String
    Public Property InterestPeriodTo As String
    Public Property InterestPeriod As Int16
    Public Property InterestAmount As Decimal
    Public Property InterestAmountPaid As Decimal
    Public Property LCAmount As Decimal
    Public Property LCAmountPaid As Decimal
    Public Property InterestAmountTotal As Decimal
    Public Property PaymentNotes As String
    Public Property ApprovedBy As String
    Public Property ApprovedDate As String
    Public Property RequestedBy As String
    Public Property RequestedDate As String
    Public Property PaidStatus As String
    Public Property Status As String
    Public Property ApprovalNo As String
    Public Property PaidDate As String
    Public Property PaidAmount As Decimal
    Public Property InterestType As String
    Public Property PaidPrincipal As Decimal
    Public Property InstallmentAmountTotal As Decimal
    Public Property BiayaProvisi As Decimal
    Public Property ApprovalSchemeID As String

    Public Property ApprovalResult As String
    Public Property notes As String
    Public Property IsReRequest As Boolean
    Public Property IsFinal As Boolean
    Public Property UserApproval As String
    Public Property NextPersonApproval As String
    Public Property Where As String
    Public Property SecurityCode As String
    Public Property UserSecurityCode As String
    Public Property IsEverRejected As Boolean


End Class
