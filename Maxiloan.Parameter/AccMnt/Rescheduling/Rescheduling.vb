
<Serializable()> _
Public Class Rescheduling : Inherits Maxiloan.Parameter.AccMntBase
    Private _partialpay As Double
    Private _adminfee As Double
    Private _totadvamt As Double
    Private _totdiscamt As Double
    Private _effectiverate As Double
    Private _listcrossdefault As DataTable
    Private _listterm As DataTable
    Private _listcondition As DataTable
    Private _reasonname As String
    Private _notes As String
    Private _rate As Double
    Private _installmentscheme As String
    Private _tenor As Integer
    Private _newprincipleamount As Double
    Private _paymentfreq As String
    Private _flat As String
    Private _payfreq As String
    Private _lblCumm As String
    Private _Cummulative As Integer
    Private _lblNumOfStep As String
    Private _NoStep As Integer
    Private _MydataSet As DataSet
	Private _NewTenor As Integer
	Private _PphAccrued As Double
    Private _PphDenda As Double
    Private _PPh As Double
    Private _PerjanjianNo As String
    Private _Penalty As Double
    Private _PenaltyAmount As Double

    Public Property NewTenor() As Integer
        Get
            Return (CType(_NewTenor, Integer))
        End Get
        Set(ByVal Value As Integer)
            _NewTenor = Value
        End Set
    End Property
    Public Property MydataSet() As DataSet
        Get
            Return _MydataSet
        End Get
        Set(ByVal Value As DataSet)
            _MydataSet = Value
        End Set
    End Property
    Public Property Cummulative() As Integer
        Get
            Return (CType(_Cummulative, Integer))
        End Get
        Set(ByVal Value As Integer)
            _Cummulative = Value
        End Set
    End Property

    Public Property lblNumOfStep() As String
        Get
            Return (CType(_lblNumOfStep, String))
        End Get
        Set(ByVal Value As String)
            _lblNumOfStep = Value
        End Set
    End Property
    Public Property NoStep() As Integer
        Get
            Return (CType(_NoStep, Integer))
        End Get
        Set(ByVal Value As Integer)
            _NoStep = Value
        End Set
    End Property

    Public Property lblCumm() As String
        Get
            Return (CType(_lblCumm, String))
        End Get
        Set(ByVal Value As String)
            _lblCumm = Value
        End Set
    End Property

    Public Property flat() As String
        Get
            Return (CType(_flat, String))
        End Get
        Set(ByVal Value As String)
            _flat = Value
        End Set
    End Property
    Public Property NewPrincipleAmount() As Double
        Get
            Return (CType(_newprincipleamount, Double))
        End Get
        Set(ByVal Value As Double)
            _newprincipleamount = Value
        End Set
    End Property
    Public Property PaymentFreq() As String
        Get
            Return (CType(_paymentfreq, String))
        End Get
        Set(ByVal Value As String)
            _paymentfreq = Value
        End Set
    End Property
    Public Property PayFreq() As String
        Get
            Return (CType(_payfreq, String))
        End Get
        Set(ByVal Value As String)
            _payfreq = Value
        End Set
    End Property
    Public Property ListCrossDefault() As DataTable
        Get
            Return (CType(_listcrossdefault, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listcrossdefault = Value
        End Set
    End Property
    Public Property ListTerm() As DataTable
        Get
            Return (CType(_listterm, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listterm = Value
        End Set
    End Property
    Public Property ListCondition() As DataTable
        Get
            Return (CType(_listcondition, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listcondition = Value
        End Set
    End Property
    Public Property EffectiveRate() As Double
        Get
            Return (CType(_effectiverate, Double))
        End Get
        Set(ByVal Value As Double)
            _effectiverate = Value
        End Set
    End Property
	Public Property TotAdvAmt() As Double
		Get
			Return (CType(_totadvamt, Double))
		End Get
		Set(ByVal Value As Double)
			_totadvamt = Value
		End Set
	End Property
	Public Property TotDiscamt() As Double
		Get
			Return (CType(_totdiscamt, Double))
		End Get
		Set(ByVal Value As Double)
			_totdiscamt = Value
		End Set
	End Property
	Public Property PartialPay() As Double
		Get
			Return (CType(_partialpay, Double))
		End Get
		Set(ByVal Value As Double)
			_partialpay = Value
		End Set
	End Property
	Public Property AdminFee() As Double
		Get
			Return (CType(_adminfee, Double))
		End Get
		Set(ByVal Value As Double)
			_adminfee = Value
		End Set
	End Property
	Public Property InstallmentScheme() As String
		Get
			Return (CType(_installmentscheme, String))
		End Get
		Set(ByVal Value As String)
			_installmentscheme = Value
		End Set
	End Property
	Public Property ReasonName() As String
		Get
			Return (CType(_reasonname, String))
		End Get
		Set(ByVal Value As String)
			_reasonname = Value
		End Set
	End Property
	Public Property Rate() As Double
		Get
			Return (CType(_rate, Double))
		End Get
		Set(ByVal Value As Double)
			_rate = Value
		End Set
	End Property
	Public Property Notes() As String
		Get
			Return (CType(_notes, String))
		End Get
		Set(ByVal Value As String)
			_notes = Value
		End Set
	End Property
	Public Property Tenor() As Integer
		Get
			Return (CType(_tenor, Integer))
		End Get
		Set(ByVal Value As Integer)
			_tenor = Value
		End Set
	End Property

	Public Property PphAccrued As Double
		Get
			Return _PphAccrued
		End Get
		Set(value As Double)
			_PphAccrued = value
		End Set
	End Property

    Public Property PphDenda As Double
        Get
            Return _PphDenda
        End Get
        Set(value As Double)
            _PphDenda = value
        End Set
    End Property

    Public Property PPh As Double
        Get
            Return _PPh
        End Get
        Set(value As Double)
            _PPh = value
        End Set
    End Property
    Public Property PerjanjianNo As String
        Get
            Return _PerjanjianNo
        End Get
        Set(value As String)
            _PerjanjianNo = value
        End Set
    End Property

    Public Property Penalty As Double
        Get
            Return _Penalty
        End Get
        Set(value As Double)
            _Penalty = value
        End Set
    End Property
    Public Property PenaltyAmount As Double
        Get
            Return _PenaltyAmount
        End Get
        Set(value As Double)
            _PenaltyAmount = value
        End Set
    End Property

End Class
