

<Serializable()> _
Public Class Refund
    Inherits Maxiloan.Parameter.AccMntBase

#Region "Constanta"
    Private _listRefund As DataTable
    Private _RefundNo As String
    Private _RefundAmount As Double
    Private _OSRefundAmount As Double
    Private _RequestDate As Date
    Private _StatusRefund As String
    Private _StatusRefundDesc As String
    Private _paramReport As String
    Private _bankName As String
    Private _BankBranch As String
    Private _bankAccountNo As String
    Private _BankAccountName As String
    Private _ReasonDesc As String
    Private _Notes As String
    Private _RequestBy As String
    Private _ContractPrepaidAmount As Double
    Private _StatusDate As Date
    Private _PrepaidRefundable As Double
    Private _ApprovedVia As String
    Private _ApprovedDesc As String
    Private _ApprovedSMS As String
    Private _ApprovedEmail As String
    Private _ApprovedFaxNo As String
    Private _approvalno As String
    Private _ApprovedBy As String
    Private _RefundTo As String
    Private _RefundToName As String
    Private _TotalRefund As Double
    Private _AccountPayableNo As String
    Private _RefundStatus As String
    Private _RequestNo As String
    Private _ReferenceNo As String
#End Region

    Public Property Reference() As String
        Get
            Return (CType(_ReferenceNo, String))
        End Get
        Set(ByVal Value As String)
            _ReferenceNo = Value
        End Set
    End Property

    Public Property RequestNo() As String
        Get
            Return (CType(_RequestNo, String))
        End Get
        Set(ByVal Value As String)
            _RequestNo = Value
        End Set
    End Property

    Public Property RefundStatus() As String
        Get
            Return (CType(_RefundStatus, String))
        End Get
        Set(ByVal Value As String)
            _RefundStatus = Value
        End Set
    End Property

    Public Property AccountPayableNo() As String
        Get
            Return (CType(_AccountPayableNo, String))
        End Get
        Set(ByVal Value As String)
            _AccountPayableNo = Value
        End Set
    End Property

    Public Property TotalRefund() As Double
        Get
            Return (CType(_TotalRefund, Double))
        End Get
        Set(ByVal Value As Double)
            _TotalRefund = Value
        End Set
    End Property

    Public Property RefundToName() As String
        Get
            Return (CType(_RefundToName, String))
        End Get
        Set(ByVal Value As String)
            _RefundToName = Value
        End Set
    End Property

    Public Property RefundTo() As String
        Get
            Return (CType(_RefundTo, String))
        End Get
        Set(ByVal Value As String)
            _RefundTo = Value
        End Set
    End Property
    Public Property ApprovedBy() As String
        Get
            Return (CType(_ApprovedBy, String))
        End Get
        Set(ByVal Value As String)
            _ApprovedBy = Value
        End Set
    End Property

    Public Property listRefund() As DataTable
        Get
            Return (CType(_listRefund, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listRefund = Value
        End Set
    End Property

    Public Property RefundNo() As String
        Get
            Return (CType(_RefundNo, String))
        End Get
        Set(ByVal Value As String)
            _RefundNo = Value
        End Set
    End Property

    Public Property RefundAmount() As Double
        Get
            Return (CType(_RefundAmount, Double))
        End Get
        Set(ByVal Value As Double)
            _RefundAmount = Value
        End Set
    End Property

    Public Property OSRefundAmount() As Double
        Get
            Return (CType(_OSRefundAmount, Double))
        End Get
        Set(ByVal Value As Double)
            _OSRefundAmount = Value
        End Set
    End Property

    Public Property RequestDate() As Date
        Get
            Return (CType(_RequestDate, Date))
        End Get
        Set(ByVal Value As Date)
            _RequestDate = Value
        End Set
    End Property

    Public Property StatusRefund() As String
        Get
            Return (CType(_StatusRefund, String))
        End Get
        Set(ByVal Value As String)
            _StatusRefund = Value
        End Set
    End Property

    Public Property StatusRefundDesc() As String
        Get
            Return (CType(_StatusRefundDesc, String))
        End Get
        Set(ByVal Value As String)
            _StatusRefundDesc = Value
        End Set
    End Property

    Public Property paramReport() As String
        Get
            Return (CType(_paramReport, String))
        End Get
        Set(ByVal Value As String)
            _paramReport = Value
        End Set
    End Property

    Public Property bankName() As String
        Get
            Return (CType(_bankName, String))
        End Get
        Set(ByVal Value As String)
            _bankName = Value
        End Set
    End Property

    Public Property BankBranchID() As Integer
    Public Property BankBranchName() As String     

    Public Property bankAccountNo() As String
        Get
            Return (CType(_bankAccountNo, String))
        End Get
        Set(ByVal Value As String)
            _bankAccountNo = Value
        End Set
    End Property

    Public Property ReasonDesc() As String
        Get
            Return (CType(_ReasonDesc, String))
        End Get
        Set(ByVal Value As String)
            _ReasonDesc = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return (CType(_Notes, String))
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property

    Public Property Requestby() As String
        Get
            Return (CType(_RequestBy, String))
        End Get
        Set(ByVal Value As String)
            _RequestBy = Value
        End Set
    End Property
    Public Property contractprepaidamount() As Double
        Get
            Return (CType(_ContractPrepaidAmount, Double))
        End Get
        Set(ByVal Value As Double)
            _ContractPrepaidAmount = Value
        End Set
    End Property

    Public Property StatusDate() As Date
        Get
            Return (CType(_StatusDate, Date))
        End Get
        Set(ByVal Value As Date)
            _StatusDate = Value
        End Set
    End Property

    Public Property PrepaidRefundable() As Double
        Get
            Return (CType(_PrepaidRefundable, Double))
        End Get
        Set(ByVal Value As Double)
            _PrepaidRefundable = Value
        End Set
    End Property
    Public Property ApprovedSMS() As String
        Get
            Return (CType(_ApprovedSMS, String))
        End Get
        Set(ByVal Value As String)
            _ApprovedSMS = Value
        End Set
    End Property

    Public Property ApprovedEmail() As String
        Get
            Return (CType(_ApprovedEmail, String))
        End Get
        Set(ByVal Value As String)
            _ApprovedEmail = Value
        End Set
    End Property

    Public Property ApprovedFaxNo() As String
        Get
            Return (CType(_ApprovedFaxNo, String))
        End Get
        Set(ByVal Value As String)
            _ApprovedFaxNo = Value
        End Set
    End Property

    Public Property ApprovedDesc() As String
        Get
            Return (CType(_ApprovedDesc, String))
        End Get
        Set(ByVal Value As String)
            _ApprovedDesc = Value
        End Set
    End Property

    Public Property approvalno() As String
        Get
            Return (CType(_approvalno, String))
        End Get
        Set(ByVal Value As String)
            _approvalno = Value
        End Set
    End Property
End Class
