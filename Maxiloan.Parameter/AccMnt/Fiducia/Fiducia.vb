
<Serializable()> _
Public Class Fiducia : Inherits Maxiloan.Parameter.AccMntBase

    Private _NotaryID As String
    Private _NotaryName As String
    Private _seqNo As String
    Private _RegisterDate As DateTime
    Private _OfferingDate As DateTime
    Private _RegisterNotes As String
    Private _FiduciaFee As Double
    Private _RegisterBy As String
    Private _AktaNo As String
    Private _AktaDate As DateTime
    Private _CertificateNo As String
    Private _CertificateDate As DateTime
    Private _InvoiceNo As String
    Private _InvoiceDate As DateTime
    Private _DueDate As DateTime
    Private _InvoiceNotes As String
    Private _OTRFrom As Double
    Private _OTRUntil As Double
    Private _NoSKMenkeh As String
    Private _TglSKMenkeh As DateTime
    Private _NotaryAddress As String
    Private _NotaryRT As String
    Private _NotaryRW As String
    Private _NotaryKelurahan As String
    Private _NotaryKecamatan As String
    Private _NotaryCity As String
    Private _NotaryZipCode As String
    Private _NotaryAreaPhone1 As String
    Private _NotaryPhone1 As String
    Private _NotaryAreaPhone2 As String
    Private _NotaryPhone2 As String
    Private _NotaryAreaFax As String
    Private _NotaryFax As String
    Private _EMail As String
    Private _MobilePhone As String
    Private _ContactPersonName As String
    Private _ContactPersonTitle As String
    Private _NotaryBankID As String
    Private _NotaryBankBranch As String
    Private _NotaryBankBranchID As String
    Private _NotaryAccountName As String
    Private _NotaryAccountNo As String
    Private _NotaryBankName As String
    Private _NotaryBankBranchCity As String
    Private _IsActive As Boolean
    Private _ModeStatus As String
    Private _NotaryNPWP As String
    Private _ReceiveDate As DateTime


    Public Property ModeStatus() As String
        Get
            Return _ModeStatus
        End Get
        Set(ByVal Value As String)
            _ModeStatus = Value
        End Set
    End Property


    Public Property IsActive() As Boolean
        Get
            Return _IsActive
        End Get
        Set(ByVal Value As Boolean)
            _IsActive = Value
        End Set
    End Property

    Public Property NotaryAccountNo() As String
        Get
            Return _NotaryAccountNo
        End Get
        Set(ByVal Value As String)
            _NotaryAccountNo = Value
        End Set
    End Property
    Public Property NotaryBankBranchCity() As String
        Get
            Return _NotaryBankBranchCity
        End Get
        Set(ByVal Value As String)
            _NotaryBankBranchCity = Value
        End Set
    End Property
    Public Property NotaryBankName() As String
        Get
            Return _NotaryBankName
        End Get
        Set(ByVal Value As String)
            _NotaryBankName = Value
        End Set
    End Property

    Public Property NotaryAccountName() As String
        Get
            Return _NotaryAccountName
        End Get
        Set(ByVal Value As String)
            _NotaryAccountName = Value
        End Set
    End Property

    Public Property NotaryBankBranch() As String
        Get
            Return _NotaryBankBranch
        End Get
        Set(ByVal Value As String)
            _NotaryBankBranch = Value
        End Set
    End Property
    Public Property NotaryBankBranchID() As String
        Get
            Return _NotaryBankBranchID
        End Get
        Set(ByVal Value As String)
            _NotaryBankBranchID = Value
        End Set
    End Property

    Public Property NotaryBankID() As String
        Get
            Return _NotaryBankID
        End Get
        Set(ByVal Value As String)
            _NotaryBankID = Value
        End Set
    End Property

    Public Property ContactPersonTitle() As String
        Get
            Return _ContactPersonTitle
        End Get
        Set(ByVal Value As String)
            _ContactPersonTitle = Value
        End Set
    End Property


    Public Property ContactPersonName() As String
        Get
            Return _ContactPersonName
        End Get
        Set(ByVal Value As String)
            _ContactPersonName = Value
        End Set
    End Property


    Public Property MobilePhone() As String
        Get
            Return _MobilePhone
        End Get
        Set(ByVal Value As String)
            _MobilePhone = Value
        End Set
    End Property


    Public Property EMail() As String
        Get
            Return _EMail
        End Get
        Set(ByVal Value As String)
            _EMail = Value
        End Set
    End Property


    Public Property NotaryFax() As String
        Get
            Return _NotaryFax
        End Get
        Set(ByVal Value As String)
            _NotaryFax = Value
        End Set
    End Property

    Public Property NotaryAreaFax() As String
        Get
            Return _NotaryAreaFax
        End Get
        Set(ByVal Value As String)
            _NotaryAreaFax = Value
        End Set
    End Property


    Public Property NotaryPhone2() As String
        Get
            Return _NotaryPhone2
        End Get
        Set(ByVal Value As String)
            _NotaryPhone2 = Value
        End Set
    End Property


    Public Property NotaryAreaPhone2() As String
        Get
            Return _NotaryAreaPhone2
        End Get
        Set(ByVal Value As String)
            _NotaryAreaPhone2 = Value
        End Set
    End Property


    Public Property NotaryPhone1() As String
        Get
            Return _NotaryPhone1
        End Get
        Set(ByVal Value As String)
            _NotaryPhone1 = Value
        End Set
    End Property

    Public Property NotaryAreaPhone1() As String
        Get
            Return _NotaryAreaPhone1
        End Get
        Set(ByVal Value As String)
            _NotaryAreaPhone1 = Value
        End Set
    End Property

    Public Property NotaryZipCode() As String
        Get
            Return _NotaryZipCode
        End Get
        Set(ByVal Value As String)
            _NotaryZipCode = Value
        End Set
    End Property
    Public Property NotaryCity() As String
        Get
            Return _NotaryCity
        End Get
        Set(ByVal Value As String)
            _NotaryCity = Value
        End Set
    End Property


    Public Property NotaryKecamatan() As String
        Get
            Return _NotaryKecamatan
        End Get
        Set(ByVal Value As String)
            _NotaryKecamatan = Value
        End Set
    End Property

    Public Property NotaryKelurahan() As String
        Get
            Return _NotaryKelurahan
        End Get
        Set(ByVal Value As String)
            _NotaryKelurahan = Value
        End Set
    End Property



    Public Property NotaryRW() As String
        Get
            Return _NotaryRW
        End Get
        Set(ByVal Value As String)
            _NotaryRW = Value
        End Set
    End Property

    Public Property NotaryRT() As String
        Get
            Return _NotaryRT
        End Get
        Set(ByVal Value As String)
            _NotaryRT = Value
        End Set
    End Property

    Public Property NotaryAddress() As String
        Get
            Return _NotaryAddress
        End Get
        Set(ByVal Value As String)
            _NotaryAddress = Value
        End Set
    End Property


    Public Property TglSKMenkeh() As DateTime
        Get
            Return _TglSKMenkeh
        End Get
        Set(ByVal Value As DateTime)
            _TglSKMenkeh = Value
        End Set
    End Property

    Public Property NoSKMenkeh() As String
        Get
            Return _NoSKMenkeh
        End Get
        Set(ByVal Value As String)
            _NoSKMenkeh = Value
        End Set
    End Property

    Public Property NotaryID() As String
        Get
            Return _NotaryID
        End Get
        Set(ByVal Value As String)
            _NotaryID = Value
        End Set
    End Property


    Public Property NotaryName() As String
        Get
            Return _NotaryName
        End Get
        Set(ByVal Value As String)
            _NotaryName = Value
        End Set
    End Property

    Public Property SeqNo() As String
        Get
            Return _seqNo
        End Get
        Set(ByVal Value As String)
            _seqNo = Value
        End Set
    End Property

    Public Property DueDate() As DateTime
        Get
            Return _DueDate
        End Get
        Set(ByVal Value As DateTime)
            _DueDate = Value
        End Set
    End Property

    Public Property RegisterDate() As DateTime
        Get
            Return _RegisterDate
        End Get
        Set(ByVal Value As DateTime)
            _RegisterDate = Value
        End Set
    End Property


    Public Property OfferingDate() As DateTime
        Get
            Return _OfferingDate
        End Get
        Set(ByVal Value As DateTime)
            _OfferingDate = Value
        End Set
    End Property

    Public Property RegisterNotes() As String
        Get
            Return _RegisterNotes
        End Get
        Set(ByVal Value As String)
            _RegisterNotes = Value
        End Set
    End Property

    Public Property FiduciaFee() As Double
        Get
            Return _FiduciaFee
        End Get
        Set(ByVal Value As Double)
            _FiduciaFee = Value
        End Set
    End Property

    Public Property RegisterBy() As String
        Get
            Return _RegisterBy
        End Get
        Set(ByVal Value As String)
            _RegisterBy = Value
        End Set
    End Property

    Public Property AktaNo() As String
        Get
            Return _AktaNo
        End Get
        Set(ByVal Value As String)
            _AktaNo = Value
        End Set
    End Property

    Public Property AktaDate() As DateTime
        Get
            Return _AktaDate
        End Get
        Set(ByVal Value As DateTime)
            _AktaDate = Value
        End Set
    End Property

    Public Property CertificateNo() As String
        Get
            Return _CertificateNo
        End Get
        Set(ByVal Value As String)
            _CertificateNo = Value
        End Set
    End Property

    Public Property CertificateDate() As DateTime
        Get
            Return _CertificateDate
        End Get
        Set(ByVal Value As DateTime)
            _CertificateDate = Value
        End Set
    End Property

    Public Property InvoiceNo() As String
        Get
            Return _InvoiceNo
        End Get
        Set(ByVal Value As String)
            _InvoiceNo = Value
        End Set
    End Property

    Public Property InvoiceDate() As DateTime
        Get
            Return _InvoiceDate
        End Get
        Set(ByVal Value As DateTime)
            _InvoiceDate = Value
        End Set
    End Property
    Public Property InvoiceNotes() As String
        Get
            Return _InvoiceNotes
        End Get
        Set(ByVal Value As String)
            _InvoiceNotes = Value
        End Set
    End Property
    Public Property OTRFrom() As Double
        Get
            Return _OTRFrom
        End Get
        Set(ByVal Value As Double)
            _OTRFrom = Value
        End Set
    End Property

    Public Property OTRUntil() As Double
        Get
            Return _OTRUntil
        End Get
        Set(ByVal Value As Double)
            _OTRUntil = Value
        End Set
    End Property

    Public Property NotaryNPWP() As String
        Get
            Return _NotaryNPWP
        End Get
        Set(ByVal Value As String)
            _NotaryNPWP = Value
        End Set
    End Property

    Public Property ReceiveDate() As DateTime
        Get
            Return _ReceiveDate
        End Get
        Set(ByVal Value As DateTime)
            _ReceiveDate = Value
        End Set
    End Property
End Class
