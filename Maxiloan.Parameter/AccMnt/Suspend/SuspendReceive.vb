
<Serializable()> _
Public Class SuspendReceive
    Inherits Maxiloan.Parameter.AccMntBase
#Region "Constanta"
    Private _listSuspend As DataTable
    Private _description As String
    Private _AmountRec As Double
    Private _SuspendNo As String
    Private _bankaccount As String
    Private _postingdate As Date
    Private _bankAccountName As String
    Private _VoucherNo As String
    Private _SuspendStatus As String
    Private _SuspendStatusdesc As String
    Private _StatusDate As Date
    Private _strError As String
   
#End Region


    Public Property listSuspend() As DataTable
        Get
            Return (CType(_listSuspend, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listSuspend = Value
        End Set
    End Property

    Public Property description() As String
        Get
            Return (CType(_description, String))
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property

    Public Property bankaccount() As String
        Get
            Return (CType(_bankaccount, String))
        End Get
        Set(ByVal Value As String)
            _bankaccount = Value
        End Set
    End Property

    Public Property AmountRec() As Double
        Get
            Return (CType(_AmountRec, Double))
        End Get
        Set(ByVal Value As Double)
            _AmountRec = Value
        End Set
    End Property


    Public Property postingdate() As Date
        Get
            Return (CType(_postingdate, Date))
        End Get
        Set(ByVal Value As Date)
            _postingdate = Value
        End Set
    End Property

    Public Property VoucherNo() As String
        Get
            Return (CType(_VoucherNo, String))
        End Get
        Set(ByVal Value As String)
            _VoucherNo = Value
        End Set
    End Property

    Public Property SuspendStatus() As String
        Get
            Return (CType(_SuspendStatus, String))
        End Get
        Set(ByVal Value As String)
            _SuspendStatus = Value
        End Set
    End Property

    Public Property SuspendStatusdesc() As String
        Get
            Return (CType(_SuspendStatusdesc, String))
        End Get
        Set(ByVal Value As String)
            _SuspendStatusdesc = Value
        End Set
    End Property

    Public Property StatusDate() As Date
        Get
            Return (CType(_StatusDate, Date))
        End Get
        Set(ByVal Value As Date)
            _StatusDate = Value
        End Set
    End Property

    Public Property strError() As String
        Get
            Return (CType(_strError, String))
        End Get
        Set(ByVal Value As String)
            _strError = Value
        End Set
    End Property

    Public txtReferenceNo As String
    Public Nomor As Integer
End Class

