﻿<Serializable()> _
Public Class SuspendAllocationGeneral : Inherits Maxiloan.Parameter.Common    
    Public Property COA As Integer
    Public Property Departemen As String
    Public Property Amount As Decimal
    Public Property Description As String
    Public Property SuspendNo As String
    Public Property PaymentAllocationID As String
    Public Property Listdata As DataTable
    Public Property ListdataReport As DataSet
    Public Property Totalrecords As Int64
    Public Property IsBlock As Boolean
End Class
