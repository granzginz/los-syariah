
<Serializable()> _
Public Class SuspendAllocate : Inherits Maxiloan.Parameter.AccMntBase
    Private _suspendstatus As String
    Public Property SuspendStatus() As String
        Get
            Return (CType(_suspendstatus, String))
        End Get
        Set(ByVal Value As String)
            _suspendstatus = Value
        End Set
    End Property
End Class
