
<Serializable()> _
    Public Class AccMntBase : Inherits Maxiloan.Parameter.Common

#Region "Constanta"
    Private _nextinstallmentnumber As Integer
    Private _productid As String
    Private _agreementno As String
    Private _applicationid As String
    Private _fundingcoyname As String
    Private _fundingpledgestatus As String
    Private _residuvalue As Double
    Private _nextinstallmentdate As Date
    Private _NextInstallmentDueDate As Date
    Private _listdata As DataTable
    Private _SuspendNo As String
    Private _PPHNo As String
    Private _prepaidholdstatus As String
    Private _defaultstatus As String
    Private _contractstatus As String
    Private _requestto As String
    Private _reasondescription As String
    Private _applicationstep As String
    Private _address As String
    Private _departementid As String
    Private _bankaccountname As String
    Private _departmentname As String
    Private _branchidx As String
    Private _naamount As Double
    Private _nadate As Date
    Private _paymentTypeName As String
    Private _reversalDate As Date
    Private _titipanAngsuranDesc As String

    Private _ToleransiBayarKurang As String
    Private _ToleransiBayarLebih As String
    Private _ContractPrepaidAmount As Double
#Region "Customer Info"
    Private _customertype As String
    Private _customername As String
    Private _customerid As String
#End Region

#Region "Variable : Received From "
    Private _referenceno As String
    Private _receivedfrom As String
#End Region

#Region "Installment Receive"
    Private _maximumInstallment As Double
    Private _lcinstallment As Double
    Private _lcinstallmentwaived As Double
    Private _LCInsuranceBal As Double
    Private _TotalNilai As Double

    Private _titipanAngsuran As Double
    Private _installmentdue As Double
    Private _installmentwaived As Double

    Private _installmentcollfee As Double
    Private _installmentcollfeewaived As Double

    Private _lcinstallmentdesc As String
    Private _installmentduedesc As String
    Private _installmentcollfeedesc As String

#End Region

#Region "Insurance Receive"
    Private _insuranceduewaived As Double
    Private _lcinsurancewaived As Double
    Private _insurancecollfeewaived As Double

    Private _insurancedue As Double
    Private _lcinsurance As Double
    Private _insurancecollfee As Double

    Private _insuranceduedesc As String
    Private _lcinsurancedesc As String
    Private _insurancecollfeedesc As String
#End Region

#Region "Others Receive"
    Private _pdcbouncefeewaived As Double
    Private _insuranceclaimexpensewaived As Double
    Private _stnkrenewalfeewaived As Double
    Private _repossessionfeewaived As Double

    Private _pdcbouncefee As Double
    Private _insuranceclaimexpense As Double
    Private _stnkrenewalfee As Double
    Private _repossessionfee As Double
    Private _prepaid As Double
    Private _pll As Double

    Private _plldes As String
    Private _prepaiddesc As String
    Private _pdcbouncefeedesc As String
    Private _insuranceclaimexpensedesc As String
    Private _stnkrenewalfeedesc As String
    Private _repossessionfeedesc As String
    Private _terminationpenaltywaived As Double
#End Region

    Private _paymenthierarchy As String
    Private _productoffering As String
    Private _amountreceive As Double
    Private _numofinstallment As Int16
    Private _installmentamount As Double
	Private _amounttobepaid As Double
	Private _PrincipalAmountDue As Double
	Private _InterestAmountDue As Double

	Private _outstandingprincipal As Double
    Private _outstandinginterest As Double
    Private _accruedinterest As Double
    Private _faktorpengurang As Double
    Private _valuedate As Date

    Private _tenor As Int16

    Private _bankaccountid As String
    Private _bankaccounttype As String
    Private _bankpurpose As String
    Private _bankId As String
    Private _coa As String
    Private _listreport As DataSet

    Private _endingbalance As Double
    Private _isCashierOpen As Boolean

    Private _branchagereement As String
    Private _insurancepaidby As String
    Private _PaymentAllocationID As String
    Private _isHaveOtor As Boolean
    Private _PrepaidHoldStatusDesc As String
    Private _NoFasilitas As String
#End Region

    Public Property PrepaidHoldStatusDesc() As String
        Get
            Return (CType(_PrepaidHoldStatusDesc, String))
        End Get
        Set(value As String)
            _PrepaidHoldStatusDesc = value
        End Set
    End Property
    Public Property InsSeqNo As String
#Region "BANK"
    Public Property BankAccountID() As String
        Get
            Return (CType(_bankaccountid, String))
        End Get
        Set(ByVal Value As String)
            _bankaccountid = Value
        End Set
    End Property

    Public Property Coa() As String
        Get
            Return (CType(_coa, String))
        End Get
        Set(ByVal Value As String)
            _coa = Value
        End Set
    End Property

    Public Property BankID() As String
        Get
            Return (CType(_bankId, String))
        End Get
        Set(ByVal Value As String)
            _bankId = Value
        End Set
    End Property

    Public Property BankPurpose() As String
        Get
            Return CType(_bankpurpose, String)
        End Get
        Set(ByVal Value As String)
            _bankpurpose = Value
        End Set
    End Property

    Public Property BankAccountType() As String
        Get
            Return CType(_bankaccounttype, String)
        End Get
        Set(ByVal Value As String)
            _bankaccounttype = Value
        End Set
    End Property

    Public Property EndingBalance() As Double
        Get
            Return CType(_endingbalance, Double)
        End Get
        Set(ByVal Value As Double)
            _endingbalance = Value
        End Set
    End Property

#End Region

    Public Property BankAccountName() As String
        Get
            Return _bankaccountname
        End Get
        Set(ByVal Value As String)
            _bankaccountname = Value
        End Set
    End Property

    Public Property ApplicationStep() As String
        Get
            Return _applicationstep
        End Get
        Set(ByVal Value As String)
            _applicationstep = Value
        End Set
    End Property

    Public Property Address() As String
        Get
            Return _address
        End Get
        Set(ByVal Value As String)
            _address = Value
        End Set
    End Property

    Public Property ReasonDescription() As String
        Get
            Return (CType(_reasondescription, String))
        End Get
        Set(ByVal Value As String)
            _reasondescription = Value
        End Set
    End Property

    Public Property RequestTo() As String
        Get
            Return (CType(_requestto, String))
        End Get
        Set(ByVal Value As String)
            _requestto = Value
        End Set
    End Property
    Public Property PrepaidHoldStatus() As String
        Get
            Return (CType(_prepaidholdstatus, String))
        End Get
        Set(ByVal Value As String)
            _prepaidholdstatus = Value
        End Set
    End Property
    Public Property AmountToBePaid() As Double
        Get
            Return (CType(_amounttobepaid, Double))
        End Get
        Set(ByVal Value As Double)
            _amounttobepaid = Value
        End Set
    End Property
    Public Property ContractPrepaidAmount() As Double
        Get
            Return (CType(_ContractPrepaidAmount, Double))
        End Get
        Set(value As Double)
            _ContractPrepaidAmount = value
        End Set
    End Property
    Public Property AmountReceive() As Double
        Get
            Return (CType(_amountreceive, Double))
        End Get
        Set(ByVal Value As Double)
            _amountreceive = Value
        End Set
    End Property

    Public Property ListReport() As DataSet
        Get
            Return (CType(_listreport, DataSet))
        End Get
        Set(ByVal Value As DataSet)
            _listreport = Value
        End Set
    End Property

    Public Property IsCashierOpen() As Boolean
        Get
            Return (CType(_isCashierOpen, Boolean))
        End Get
        Set(ByVal Value As Boolean)
            _isCashierOpen = Value
        End Set
    End Property

    Public Property ReversalDate() As Date
        Get
            Return (CType(_reversalDate, Date))
        End Get
        Set(ByVal Value As Date)
            _reversalDate = Value
        End Set
    End Property

    Public Property ValueDate() As Date
        Get
            Return (CType(_valuedate, Date))
        End Get
        Set(ByVal Value As Date)
            _valuedate = Value
        End Set
    End Property

    Public Property DefaultStatus() As String
        Get
            Return (CType(_defaultstatus, String))
        End Get
        Set(ByVal Value As String)
            _defaultstatus = Value
        End Set
    End Property

    Public Property ContractStatus() As String
        Get
            Return (CType(_contractstatus, String))
        End Get
        Set(ByVal Value As String)
            _contractstatus = Value
        End Set
    End Property

#Region "ReceivedFrom"
    Public Property ReferenceNo() As String
        Get
            Return (CType(_referenceno, String))
        End Get
        Set(ByVal Value As String)
            _referenceno = Value
        End Set
    End Property

    Public Property ReceivedFrom() As String
        Get
            Return (CType(_receivedfrom, String))
        End Get
        Set(ByVal Value As String)
            _receivedfrom = Value
        End Set
    End Property
#End Region

#Region "Agreement"
    Public Property Agreementno() As String
        Get
            Return (CType(_agreementno, String))
        End Get
        Set(ByVal Value As String)
            _agreementno = Value
        End Set
    End Property

    Public Property FundingCoyName() As String
        Get
            Return (CType(_fundingcoyname, String))
        End Get
        Set(ByVal Value As String)
            _fundingcoyname = Value
        End Set
    End Property
    Public Property FundingPledgeStatus() As String
        Get
            Return (CType(_fundingpledgestatus, String))
        End Get
        Set(ByVal Value As String)
            _fundingpledgestatus = Value
        End Set
    End Property
    Public Property ResiduValue() As Double
        Get
            Return (CType(_residuvalue, Double))
        End Get
        Set(ByVal Value As Double)
            _residuvalue = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return (CType(_applicationid, String))
        End Get
        Set(ByVal Value As String)
            _applicationid = Value
        End Set
    End Property

	Public Property installmentamount() As Double
		Get
			Return (CType(_installmentamount, Double))
		End Get
		Set(ByVal Value As Double)
			_installmentamount = Value
		End Set
	End Property

	Public Property PrincipalAmountDue() As Double
		Get
			Return (CType(_PrincipalAmountDue, Double))
		End Get
		Set(ByVal Value As Double)
			_PrincipalAmountDue = Value
		End Set
	End Property

	Public Property InterestAmountDue() As Double
		Get
			Return (CType(_InterestAmountDue, Double))
		End Get
		Set(ByVal Value As Double)
			_InterestAmountDue = Value
		End Set
	End Property

	Public Property PaymentTypeName() As String
        Get
            Return (CType(_paymentTypeName, String))
        End Get
        Set(ByVal Value As String)
            _PaymentTypeName = Value
        End Set
    End Property

    Public Property NADate() As Date
        Get
            Return (CType(_nadate, Date))
        End Get
        Set(ByVal Value As Date)
            _nadate = Value
        End Set
    End Property
    'Public Property NAAmount() As Double
    '    Get
    '        Return (CType(_naamount, Double))
    '    End Get
    '    Set(ByVal Value As Double)
    '        _naamount = Value
    '    End Set
    'End Property
    Public Property NextInstallmentDate() As Date
        Get
            Return (CType(_nextinstallmentdate, Date))
        End Get
        Set(ByVal Value As Date)
            _nextinstallmentdate = Value
        End Set
    End Property

    Public Property NextInstallmentDueDate() As Date
        Get
            Return CType(_NextInstallmentDueDate, Date)
        End Get
        Set(ByVal Value As Date)
            _NextInstallmentDueDate = Value
        End Set
    End Property

#End Region

    Public ReadOnly Property AmountReceiveInsurance() As Double
        Get
            Return IIf(ContractStatus <> "OSD", InstallmentAmount, InstallmentDue)
        End Get
    End Property
#Region "Branch Properties"
    Public Property BranchAgreement() As String
        Get
            Return (CType(_branchagereement, String))
        End Get
        Set(ByVal Value As String)
            _branchagereement = Value
        End Set
    End Property
#End Region

#Region "CustomerInfo"
    Public Property CustomerName() As String
        Get
            Return (CType(_customername, String))
        End Get
        Set(ByVal Value As String)
            _customername = Value
        End Set
    End Property

    Public Property CustomerType() As String
        Get
            Return (CType(_customertype, String))
        End Get
        Set(ByVal Value As String)
            _customertype = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return (CType(_customerid, String))
        End Get
        Set(ByVal Value As String)
            _customerid = Value
        End Set
    End Property
#End Region

#Region "Installment Receive"

    Public Property MaximumInstallment() As Double
        Get
            Return (CType(_maximumInstallment, Double))
        End Get
        Set(ByVal Value As Double)
            _maximumInstallment = Value
        End Set
    End Property

    Public Property InstallmentWaived() As Double
        Get
            Return (CType(_installmentwaived, Double))
        End Get
        Set(ByVal Value As Double)
            _installmentwaived = Value
        End Set
    End Property

    Public Property InstallmentDue() As Double
        Get
            Return (CType(_installmentdue, Double))
        End Get
        Set(ByVal Value As Double)
            _installmentdue = Value
        End Set
    End Property

    Public Property LcInstallmentWaived() As Double
        Get
            Return (CType(_lcinstallmentwaived, Double))
        End Get
        Set(ByVal Value As Double)
            _lcinstallmentwaived = Value
        End Set
    End Property

    Public Property LCInsuranceBal() As Double
        Get
            Return (CType(_LCInsuranceBal, Double))
        End Get
        Set(ByVal Value As Double)
            _LCInsuranceBal = Value
        End Set
    End Property

    Public Property TotalNilai() As Double
        Get
            Return (CType(_TotalNilai, Double))
        End Get
        Set(ByVal Value As Double)
            _TotalNilai = Value
        End Set
    End Property

    Public Property LcInstallment() As Double
        Get
            Return (CType(_lcinstallment, Double))
        End Get
        Set(ByVal Value As Double)
            _lcinstallment = Value
        End Set
    End Property

    Public Property InstallmentCollFeeWaived() As Double
        Get
            Return (CType(_installmentcollfeewaived, Double))
        End Get
        Set(ByVal Value As Double)
            _installmentcollfeewaived = Value
        End Set
    End Property

    Public Property InstallmentCollFee() As Double
        Get
            Return (CType(_installmentcollfee, Double))
        End Get
        Set(ByVal Value As Double)
            _installmentcollfee = Value
        End Set
    End Property
    Public Property TitipanAngsuran() As Double
        Get
            Return (CType(_titipanAngsuran, Double))
        End Get
        Set(ByVal Value As Double)
            _titipanAngsuran = Value
        End Set
    End Property
    Public Property TitipanAngsuranDesc() As String
        Get
            Return (CType(_titipanAngsuranDesc, String))
        End Get
        Set(ByVal Value As String)
            _titipanAngsuranDesc = Value
        End Set
    End Property


    Public Property InstallmentDueDesc() As String
        Get
            Return (CType(_installmentduedesc, String))
        End Get
        Set(ByVal Value As String)
            _installmentduedesc = Value
        End Set
    End Property

    Public Property LcInstallmentDesc() As String
        Get
            Return (CType(_lcinstallmentdesc, String))
        End Get
        Set(ByVal Value As String)
            _lcinstallmentdesc = Value
        End Set
    End Property

    Public Property InstallmentCollFeeDesc() As String
        Get
            Return (CType(_installmentcollfeedesc, String))
        End Get
        Set(ByVal Value As String)
            _installmentcollfeedesc = Value
        End Set
    End Property

    Public Property ToleransiBayarKurang() As String
        Get
            Return CType(_ToleransiBayarKurang, String)
        End Get
        Set(value As String)
            _ToleransiBayarKurang = value
        End Set
    End Property
    Public Property ToleransiBayarLebih() As String
        Get
            Return CType(_ToleransiBayarLebih, String)
        End Get
        Set(value As String)
            _ToleransiBayarLebih = value
        End Set
    End Property
#End Region

#Region "Insurance Receive"
    Public Property InsuranceDueWaived() As Double
        Get
            Return (CType(_insuranceduewaived, Double))
        End Get
        Set(ByVal Value As Double)
            _insuranceduewaived = Value
        End Set
    End Property


    Public Property InsuranceDue() As Double
        Get
            Return (CType(_insurancedue, Double))
        End Get
        Set(ByVal Value As Double)
            _insurancedue = Value
        End Set
    End Property

    Public Property LcInsuranceWaived() As Double
        Get
            Return (CType(_lcinsurancewaived, Double))
        End Get
        Set(ByVal Value As Double)
            _lcinsurancewaived = Value
        End Set
    End Property

    Public Property LcInsurance() As Double
        Get
            Return (CType(_lcinsurance, Double))
        End Get
        Set(ByVal Value As Double)
            _lcinsurance = Value
        End Set
    End Property

    Public Property InsuranceCollFeeWaived() As Double
        Get
            Return (CType(_insurancecollfeewaived, Double))
        End Get
        Set(ByVal Value As Double)
            _insurancecollfeewaived = Value
        End Set
    End Property

    Public Property InsuranceCollFee() As Double
        Get
            Return (CType(_insurancecollfee, Double))
        End Get
        Set(ByVal Value As Double)
            _insurancecollfee = Value
        End Set
    End Property

    Public Property InsuranceDueDesc() As String
        Get
            Return (CType(_insuranceduedesc, String))
        End Get
        Set(ByVal Value As String)
            _insuranceduedesc = Value
        End Set
    End Property

    Public Property LcInsuranceDesc() As String
        Get
            Return (CType(_lcinsurancedesc, String))
        End Get
        Set(ByVal Value As String)
            _lcinsurancedesc = Value
        End Set
    End Property
    Public Property InsuranceCollFeeDesc() As String
        Get
            Return (CType(_insurancecollfeedesc, String))
        End Get
        Set(ByVal Value As String)
            _insurancecollfeedesc = Value
        End Set
    End Property
#End Region

#Region "Others Receive"
    Public Property PDCBounceFeeWaived() As Double
        Get
            Return (CType(_pdcbouncefeewaived, Double))
        End Get
        Set(ByVal Value As Double)
            _pdcbouncefeewaived = Value
        End Set
    End Property

    Public Property PDCBounceFee() As Double
        Get
            Return (CType(_pdcbouncefee, Double))
        End Get
        Set(ByVal Value As Double)
            _pdcbouncefee = Value
        End Set
    End Property

    Public Property InsuranceClaimExpenseWaived() As Double
        Get
            Return (CType(_insuranceclaimexpensewaived, Double))
        End Get
        Set(ByVal Value As Double)
            _insuranceclaimexpensewaived = Value
        End Set
    End Property

    Public Property InsuranceClaimExpense() As Double
        Get
            Return (CType(_insuranceclaimexpense, Double))
        End Get
        Set(ByVal Value As Double)
            _insuranceclaimexpense = Value
        End Set
    End Property

    Public Property RepossessionFeeWaived() As Double
        Get
            Return (CType(_repossessionfeewaived, Double))
        End Get
        Set(ByVal Value As Double)
            _repossessionfeewaived = Value
        End Set
    End Property

    Public Property RepossessionFee() As Double
        Get
            Return (CType(_repossessionfee, Double))
        End Get
        Set(ByVal Value As Double)
            _repossessionfee = Value
        End Set
    End Property

    Public Property STNKRenewalFeeWaived() As Double
        Get
            Return (CType(_stnkrenewalfeewaived, Double))
        End Get
        Set(ByVal Value As Double)
            _stnkrenewalfeewaived = Value
        End Set
    End Property

    Public Property STNKRenewalFee() As Double
        Get
            Return (CType(_stnkrenewalfee, Double))
        End Get
        Set(ByVal Value As Double)
            _stnkrenewalfee = Value
        End Set
    End Property

    Public Property Prepaid() As Double
        Get
            Return (CType(_prepaid, Double))
        End Get
        Set(ByVal Value As Double)
            _prepaid = Value
        End Set
    End Property
    Public Property PLL() As Double
        Get
            Return (CType(_pll, Double))
        End Get
        Set(value As Double)
            _pll = value
        End Set
    End Property
    Public Property PLLDesc() As String
        Get
            Return (CType(_plldes, String))
        End Get
        Set(value As String)
            _plldes = value
        End Set
    End Property

    Public Property PDCBounceFeeDesc() As String
        Get
            Return (CType(_pdcbouncefeedesc, String))
        End Get
        Set(ByVal Value As String)
            _pdcbouncefeedesc = Value
        End Set
    End Property

    Public Property InsuranceClaimExpenseDesc() As String
        Get
            Return (CType(_insuranceclaimexpensedesc, String))
        End Get
        Set(ByVal Value As String)
            _insuranceclaimexpensedesc = Value
        End Set
    End Property

    Public Property STNKRenewalFeeDesc() As String
        Get
            Return (CType(_stnkrenewalfeedesc, String))
        End Get
        Set(ByVal Value As String)
            _stnkrenewalfeedesc = Value
        End Set
    End Property

    Public Property RepossessionFeeDesc() As String
        Get
            Return (CType(_repossessionfeedesc, String))
        End Get
        Set(ByVal Value As String)
            _repossessionfeedesc = Value
        End Set
    End Property

    Public Property PrepaidDesc() As String
        Get
            Return (CType(_prepaiddesc, String))
        End Get
        Set(ByVal Value As String)
            _prepaiddesc = Value
        End Set
    End Property

    Public Property TerminationPenaltyWaived() As Double
        Get
            Return (CType(_terminationpenaltywaived, Double))
        End Get
        Set(ByVal Value As Double)
            _terminationpenaltywaived = Value
        End Set
    End Property
    Public Function GetTotalInstall() As Double
        Return InstallmentDue + LcInstallment
    End Function
    Public Function GetTotalOSOverDue() As Double
        Return InstallmentCollFee + InsuranceDue + InsuranceCollFee + LcInsurance + PDCBounceFee + STNKRenewalFee + InsuranceClaimExpense + RepossessionFee + LcInstallment + InstallmentDue
    End Function
#End Region

#Region "Outstanding Principal and Interest Properties"
    Public Property OutstandingPrincipal() As Double
        Get
            Return (CType(_outstandingprincipal, Double))
        End Get
        Set(ByVal Value As Double)
            _outstandingprincipal = Value
        End Set
    End Property

    Public Property OutStandingInterest() As Double
        Get
            Return (CType(_outstandinginterest, Double))
        End Get
        Set(ByVal Value As Double)
            _outstandinginterest = Value
        End Set
    End Property

    Public Property AccruedInterest() As Double
        Get
            Return (CType(_accruedinterest, Double))
        End Get
        Set(ByVal Value As Double)
            _accruedinterest = Value
        End Set
    End Property

    Public Property FaktorPengurang() As Double
        Get
            Return (CType(_faktorpengurang, Double))
        End Get
        Set(ByVal Value As Double)
            _faktorpengurang = Value
        End Set
    End Property
#End Region

    '    Public Property PaymentHierarchy() As String
    '        Get
    '            Return (CType(_paymenthierarchy, String))
    '        End Get
    '        Set(ByVal Value As String)
    '            _paymenthierarchy = Value
    '        End Set
    '    End Property

    '    Public Property ProductOffering() As String
    '        Get
    '            Return (CType(_productoffering, String))
    '        End Get
    '        Set(ByVal Value As String)
    '            _productoffering = Value
    '        End Set
    '    End Property

    '    Public Property NumOfInstallment() As Int16
    '        Get
    '            Return (CType(_numofinstallment, Int16))
    '        End Get
    '        Set(ByVal Value As Int16)
    '            _numofinstallment = Value
    '        End Set
    '    End Property


    '    Public Property Tenor() As Int16
    '        Get
    '            Return (CType(_tenor, Int16))
    '        End Get
    '        Set(ByVal Value As Int16)
    '            _tenor = Value
    '        End Set
    '    End Property

    Public Property PaymentAllocationID() As String
        Get
            Return CType(_PaymentAllocationID, String)
        End Get
        Set(ByVal Value As String)
            _PaymentAllocationID = Value
        End Set
    End Property

    Public Property NextInstallmentNumber() As Integer
        Get
            Return (CType(_nextinstallmentnumber, Integer))
        End Get
        Set(ByVal Value As Integer)
            _nextinstallmentnumber = Value
        End Set
    End Property

    '    Public Property Productid() As String
    '        Get
    '            Return (CType(_productid, String))
    '        End Get
    '        Set(ByVal Value As String)
    '            _productid = Value
    '        End Set
    '    End Property
    Public Property ListData() As DataTable
        Get
            Return (CType(_listdata, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property

    Public Property SuspendNo() As String
        Get
            Return (CType(_SuspendNo, String))
        End Get
        Set(ByVal Value As String)
            _SuspendNo = Value
        End Set
    End Property
    Public Property PPHNo() As String
        Get
            Return (CType(_PPHNo, String))
        End Get
        Set(ByVal Value As String)
            _PPHNo = Value
        End Set
    End Property

    Public Property DepartementID() As String
        Get
            Return (CType(_departementid, String))
        End Get
        Set(ByVal Value As String)
            _departementid = Value
        End Set
    End Property

    Public Property DepartmentName() As String
        Get
            Return (CType(_departmentname, String))
        End Get
        Set(ByVal Value As String)
            _departmentname = Value
        End Set
    End Property


    Public Property BranchIDX() As String
        Get
            Return _branchidx
        End Get
        Set(ByVal Value As String)
            _branchidx = Value
        End Set
    End Property

#Region "Exposure"
    Private _maxoutstandingbalance As Double
    Private _outstandingbalancear As Double
    Private _numofassetrepossed As Integer
    Private _numofassetinventoried As Integer
    Private _numofrescheduling As Integer
    Private _numofagreementtransfer As Integer
    Private _numofassetreplacement As Integer
    Private _numofbouncecheque As Integer
    Private _bucket1principle As String
    Private _bucket2principle As String
    Private _bucket3principle As String
    Private _bucket4principle As String
    Private _bucket5principle As String
    Private _bucket6principle As String
    Private _bucket7principle As String
    Private _bucket8principle As String
    Private _bucket9principle As String
    Private _bucket10principle As String
    Private _bucket1_gross As String
    Private _bucket2_gross As String
    Private _bucket3_gross As String
    Private _bucket4_gross As String
    Private _bucket5_gross As String
    Private _bucket6_gross As String
    Private _bucket7_gross As String
    Private _bucket8_gross As String
    Private _bucket9_gross As String
    Private _bucket10_gross As String
	Private _totalallbucket As Decimal
	Private _maxoverduedays As Integer

    Private _maxoverdueamount As Double
    Private _numofactiveagreement As Integer
    Private _numofinprocessagreement As Integer
    Private _numofassetinfinancing As Integer
    Private _numofwrittenoffagreement As Integer
    Private _numofrejectedagreement As Integer
    Private _numofnonaccrualagreement As Integer
    Private _numofcancelledagreement As Integer

    Public Property NumOfCancelledAgreement() As Integer
        Get
            Return (CType(_numofcancelledagreement, Integer))
        End Get
        Set(ByVal Value As Integer)
            _numofcancelledagreement = Value
        End Set
    End Property


    Public Property NumOfNonAccrualAgreement() As Integer
        Get
            Return (CType(_numofnonaccrualagreement, Integer))
        End Get
        Set(ByVal Value As Integer)
            _numofnonaccrualagreement = Value
        End Set
    End Property

    Public Property NumOfRejectedAgreement() As Integer
        Get
            Return (CType(_numofrejectedagreement, Integer))
        End Get
        Set(ByVal Value As Integer)
            _numofrejectedagreement = Value
        End Set
    End Property


    Public Property NumOfWrittenOffAgreement() As Integer
        Get
            Return (CType(_numofwrittenoffagreement, Integer))
        End Get
        Set(ByVal Value As Integer)
            _numofwrittenoffagreement = Value
        End Set
    End Property


    Public Property NumOfAssetInFinancing() As Integer
        Get
            Return (CType(_numofassetinfinancing, Integer))
        End Get
        Set(ByVal Value As Integer)
            _numofassetinfinancing = Value
        End Set
    End Property

    Public Property NumofInProcessAgreement() As Integer
        Get
            Return (CType(_numofinprocessagreement, Integer))
        End Get
        Set(ByVal Value As Integer)
            _numofinprocessagreement = Value
        End Set
    End Property

    Public Property NumofActiveAgreement() As Integer
        Get
            Return (CType(_numofactiveagreement, Integer))
        End Get
        Set(ByVal Value As Integer)
            _numofactiveagreement = Value
        End Set
    End Property

    Public Property MaxOverDueAmount() As Double
        Get
            Return (CType(_maxoverdueamount, Double))
        End Get
        Set(ByVal Value As Double)
            _maxoverdueamount = Value
        End Set
    End Property
    Public Property MaxOverdueDays() As Integer
        Get
            Return (CType(_maxoverduedays, Integer))
        End Get
        Set(ByVal Value As Integer)
            _maxoverduedays = Value
        End Set
    End Property

    Public Property MaxOutstandingBalance() As Double
        Get
            Return (CType(_maxoutstandingbalance, Double))
        End Get
        Set(ByVal Value As Double)
            _maxoutstandingbalance = Value
        End Set
    End Property

    Public Property OutstandingBalanceAR() As Double
        Get
            Return (CType(_outstandingbalancear, Double))
        End Get
        Set(ByVal Value As Double)
            _outstandingbalancear = Value
        End Set
    End Property

    Public Property NumOfAssetRepossed() As Integer
        Get
            Return (CType(_numofassetrepossed, Integer))
        End Get
        Set(ByVal Value As Integer)
            _numofassetrepossed = Value
        End Set
    End Property

    Public Property NumOfAssetInventoried() As Integer
        Get
            Return (CType(_numofassetinventoried, Integer))
        End Get
        Set(ByVal Value As Integer)
            _numofassetinventoried = Value
        End Set
    End Property

    Public Property NumOfRescheduling() As Integer
        Get
            Return (CType(_numofrescheduling, Integer))
        End Get
        Set(ByVal Value As Integer)
            _numofrescheduling = Value
        End Set
    End Property

    Public Property NumOfAgreementTransfer() As Integer
        Get
            Return (CType(_numofagreementtransfer, Integer))
        End Get
        Set(ByVal Value As Integer)
            _numofagreementtransfer = Value
        End Set
    End Property

    Public Property NumOfAssetReplacement() As Integer
        Get
            Return (CType(_numofassetreplacement, Integer))
        End Get
        Set(ByVal Value As Integer)
            _numofassetreplacement = Value
        End Set
    End Property

    Public Property NumOfBounceCheque() As Integer
        Get
            Return (CType(_numofbouncecheque, Integer))
        End Get
        Set(ByVal Value As Integer)
            _numofbouncecheque = Value
        End Set
    End Property

    Public Property Bucket1Principle() As String
        Get
            Return (CType(_bucket1principle, String))
        End Get
        Set(ByVal Value As String)
            _bucket1principle = Value
        End Set
    End Property

    Public Property Bucket2Principle() As String
        Get
            Return (CType(_bucket2principle, String))
        End Get
        Set(ByVal Value As String)
            _bucket2principle = Value
        End Set
    End Property

    Public Property Bucket3Principle() As String
        Get
            Return (CType(_bucket3principle, String))
        End Get
        Set(ByVal Value As String)
            _bucket3principle = Value
        End Set
    End Property

    Public Property Bucket4Principle() As String
        Get
            Return (CType(_bucket4principle, String))
        End Get
        Set(ByVal Value As String)
            _bucket4principle = Value
        End Set
    End Property

    Public Property Bucket5Principle() As String
        Get
            Return (CType(_bucket5principle, String))
        End Get
        Set(ByVal Value As String)
            _bucket5principle = Value
        End Set
    End Property

    Public Property Bucket6Principle() As String
        Get
            Return (CType(_bucket6principle, String))
        End Get
        Set(ByVal Value As String)
            _bucket6principle = Value
        End Set
    End Property
    Public Property Bucket7Principle() As String
        Get
            Return (CType(_bucket7principle, String))
        End Get
        Set(ByVal Value As String)
            _bucket7principle = Value
        End Set
    End Property
    Public Property Bucket8Principle() As String
        Get
            Return (CType(_bucket8principle, String))
        End Get
        Set(ByVal Value As String)
            _bucket8principle = Value
        End Set
    End Property
    Public Property Bucket9Principle() As String
        Get
            Return (CType(_bucket9principle, String))
        End Get
        Set(ByVal Value As String)
            _bucket9principle = Value
        End Set
    End Property
    Public Property Bucket10Principle() As String
        Get
            Return (CType(_bucket10principle, String))
        End Get
        Set(ByVal Value As String)
            _bucket10principle = Value
        End Set
    End Property
    Public Property Bucket1_gross() As String
        Get
            Return (CType(_bucket1_gross, String))
        End Get
        Set(ByVal Value As String)
            _bucket1_gross = Value
        End Set
    End Property
    Public Property Bucket2_gross() As String
        Get
            Return (CType(_bucket2_gross, String))
        End Get
        Set(ByVal Value As String)
            _bucket2_gross = Value
        End Set
    End Property
    Public Property Bucket3_gross() As String
        Get
            Return (CType(_bucket3_gross, String))
        End Get
        Set(ByVal Value As String)
            _bucket3_gross = Value
        End Set
    End Property
    Public Property Bucket4_gross() As String
        Get
            Return (CType(_bucket4_gross, String))
        End Get
        Set(ByVal Value As String)
            _bucket4_gross = Value
        End Set
    End Property
    Public Property Bucket5_gross() As String
        Get
            Return (CType(_bucket5_gross, String))
        End Get
        Set(ByVal Value As String)
            _bucket5_gross = Value
        End Set
    End Property
    Public Property Bucket6_gross() As String
        Get
            Return (CType(_bucket6_gross, String))
        End Get
        Set(ByVal Value As String)
            _bucket6_gross = Value
        End Set
    End Property
    Public Property Bucket7_gross() As String
        Get
            Return (CType(_bucket7_gross, String))
        End Get
        Set(ByVal Value As String)
            _bucket7_gross = Value
        End Set
    End Property
    Public Property Bucket8_gross() As String
        Get
            Return (CType(_bucket8_gross, String))
        End Get
        Set(ByVal Value As String)
            _bucket8_gross = Value
        End Set
    End Property
    Public Property Bucket9_gross() As String
        Get
            Return (CType(_bucket9_gross, String))
        End Get
        Set(ByVal Value As String)
            _bucket9_gross = Value
        End Set
    End Property
    Public Property Bucket10_gross() As String
        Get
            Return (CType(_bucket10_gross, String))
        End Get
        Set(ByVal Value As String)
            _bucket10_gross = Value
        End Set
    End Property

	'Public Property TotalAllBucket() As Integer
	'    Get
	'        Return (CType(_totalallbucket, Integer))
	'    End Get
	'    Set(ByVal Value As Integer)
	'        _totalallbucket = Value
	'    End Set
	'End Property

	Public Property TotalAllBucket() As Decimal
		Get
			Return (CType(_totalallbucket, Decimal))
		End Get
		Set(ByVal Value As Decimal)
			_totalallbucket = Value
		End Set
	End Property
#End Region


	Private _BayarDi As String
    Private _CollectorID As String
    Private _PrioritasPembayaran As String
    Private _BlokirBayar As String
    Private _Penyetor As String

    Public Property BayarDi() As String
        Get
            Return (CType(_BayarDi, String))
        End Get
        Set(ByVal Value As String)
            _BayarDi = Value
        End Set
    End Property

    Public Property CollectorID() As String
        Get
            Return (CType(_CollectorID, String))
        End Get
        Set(ByVal Value As String)
            _CollectorID = Value
        End Set
    End Property

    Public Property PrioritasPembayaran() As String
        Get
            Return (CType(_PrioritasPembayaran, String))
        End Get
        Set(ByVal Value As String)
            _PrioritasPembayaran = Value
        End Set
    End Property

    Public Property BlokirBayar() As Boolean
        Get
            Return (CType(_BlokirBayar, Boolean))
        End Get
        Set(ByVal Value As Boolean)
            _BlokirBayar = Value
        End Set
    End Property

    Public Property Penyetor() As String
        Get
            Return (CType(_Penyetor, String))
        End Get
        Set(ByVal Value As String)
            _Penyetor = Value
        End Set
    End Property


    Public Property ToAmountAllocate As Double
    Public Property ToAmountInstFee As Double
    Public Property ToAmountOther As Double

    Public Function ValidateAlocate() As Boolean
        Return ToAmountAllocate - (ToAmountInstFee + ToAmountOther) = 0
    End Function

    Public Function GetInstDueByPrioritas(ByVal busdate As DateTime, Optional ByVal dtShcedule As DataTable = Nothing) As Double
        ' P - PRINCIPLE
        ' I - INSURANCE
        ' L - LATE CHARGES
        ' Modified By Dede 10 Nov 2015  - Recalculate InstallmentDue berdasarkan prioritas pembayaran
        'Dim isOverDue As Boolean
        ''Hitung ulang dari installment schedule
        'If dtShcedule IsNot Nothing Then
        '    Dim newAllocation As Decimal = 0
        '    Dim sisaAllocation As Decimal = ToAmountInstFee
        '    If dtShcedule.Rows.Count > 0 Then
        '        For Each drow As DataRow In dtShcedule.Rows
        '            If CDate(drow("DueDate")) <= busdate Then
        '                isOverDue = True
        '            Else
        '                isOverDue = False
        '            End If
        '            If sisaAllocation > 0 Then
        '                If (PrioritasPembayaran.Trim.ToUpper() = "LIP" Or PrioritasPembayaran.Trim.ToUpper() = "LPI") Then
        '                    ' Late charge di alokasikan dahulu
        '                    sisaAllocation = sisaAllocation - CType(drow.Item("LCAmount"), Decimal)
        '                    If CType(drow.Item("InstallmentAmount"), Decimal) - CType(drow.Item("PaidAmount"), Decimal) < sisaAllocation Then
        '                        newAllocation = newAllocation + CType(drow.Item("InstallmentAmount"), Decimal) - CType(drow.Item("PaidAmount"), Decimal)
        '                    ElseIf sisaAllocation > 0 Then
        '                        newAllocation = newAllocation + sisaAllocation
        '                    End If
        '                    sisaAllocation = sisaAllocation - (CType(drow.Item("InstallmentAmount"), Decimal) - CType(drow.Item("PaidAmount"), Decimal))
        '                Else
        '                    If CType(drow.Item("InstallmentAmount"), Decimal) - CType(drow.Item("PaidAmount"), Decimal) < sisaAllocation Then
        '                        newAllocation = newAllocation + CType(drow.Item("InstallmentAmount"), Decimal) - CType(drow.Item("PaidAmount"), Decimal)
        '                    ElseIf sisaAllocation > 0 Then
        '                        newAllocation = newAllocation + sisaAllocation
        '                    End If
        '                    sisaAllocation = sisaAllocation - (CType(drow.Item("InstallmentAmount"), Decimal) - CType(drow.Item("PaidAmount"), Decimal))
        '                End If
        '                If (isOverDue = False And LcInstallment > 0 And sisaAllocation > 0) Then
        '                    sisaAllocation = sisaAllocation - LcInstallment
        '                    Exit For
        '                End If
        '            End If
        '        Next
        '        If sisaAllocation > 0 Then
        '            newAllocation = newAllocation + sisaAllocation
        '        End If
        '        Return newAllocation
        '    End If
        'End If

        ' Default sebelum penambahan hitung ulang dari installment schedule
        If Not (PrioritasPembayaran.Trim.ToUpper() = "LPI" Or PrioritasPembayaran.Trim.ToUpper() = "LIP") Then
            If (InstallmentDue >= ToAmountInstFee) Then
                Return ToAmountInstFee
            Else
                If ((ToAmountInstFee - InstallmentDue) <= LcInstallment) Then
                    Return InstallmentDue
                End If
                Return ToAmountInstFee - LcInstallment
            End If
        End If

        Return IIf(ToAmountInstFee - LcInstallment > 0, ToAmountInstFee - LcInstallment, 0)

    End Function

    Public Function GetLCInstByPrioritas(ByVal busdate As DateTime, Optional ByVal dtShcedule As DataTable = Nothing) As Double
        ' Modified By Dede 10 Nov 2015  - Recalculate InstallmentDue berdasarkan prioritas pembayaran
        'Dim isOverDue As Boolean
        '' Hitung ulang dari installment schedule
        'If dtShcedule IsNot Nothing Then
        '    Dim newAllocation As Decimal = 0
        '    Dim sisaAllocation As Decimal = ToAmountInstFee
        '    If dtShcedule.Rows.Count > 0 Then
        '        For Each drow As DataRow In dtShcedule.Rows
        '            If CDate(drow("DueDate")) <= busdate Then
        '                isOverDue = True
        '            Else
        '                isOverDue = False
        '            End If
        '            If sisaAllocation > 0 Then
        '                If (PrioritasPembayaran.Trim.ToUpper() = "LIP" Or PrioritasPembayaran.Trim.ToUpper() = "LPI") Then
        '                    ' Late charge di alokasikan dahulu
        '                    If CType(drow.Item("LCAmount"), Decimal) < sisaAllocation Then
        '                        newAllocation = newAllocation + CType(drow.Item("LCAmount"), Decimal)
        '                    Else
        '                        newAllocation = newAllocation + sisaAllocation
        '                    End If
        '                    sisaAllocation = sisaAllocation - CType(drow.Item("LCAmount"), Decimal) - (CType(drow.Item("InstallmentAmount"), Decimal) - CType(drow.Item("PaidAmount"), Decimal))
        '                Else
        '                    If (isOverDue = False And LcInstallment > 0 And CType(drow.Item("InstallmentAmount"), Decimal) - CType(drow.Item("PaidAmount"), Decimal) <= 0) Then
        '                        If LcInstallment < sisaAllocation Then
        '                            newAllocation = newAllocation + LcInstallment
        '                        Else
        '                            newAllocation = newAllocation + sisaAllocation
        '                        End If
        '                        sisaAllocation = sisaAllocation - LcInstallment
        '                    End If
        '                    sisaAllocation = sisaAllocation - CType(drow.Item("InstallmentAmount"), Decimal) - CType(drow.Item("PaidAmount"), Decimal)
        '                    If CType(drow.Item("LCAmount"), Decimal) < sisaAllocation Then
        '                        newAllocation = newAllocation + CType(drow.Item("LCAmount"), Decimal)
        '                    ElseIf sisaAllocation > 0 Then
        '                        newAllocation = newAllocation + sisaAllocation
        '                    End If
        '                    sisaAllocation = sisaAllocation - CType(drow.Item("LCAmount"), Decimal)
        '                End If
        '            End If
        '        Next
        '        Return newAllocation
        '    End If
        'End If

        ' Default sebelum penambahan hitung ulang dari installment schedule
        If (PrioritasPembayaran.Trim.ToUpper() = "LPI" Or PrioritasPembayaran.Trim.ToUpper() = "LIP") Then
            Return IIf(LcInstallment >= ToAmountInstFee, ToAmountInstFee, LcInstallment)
        End If

        If (InstallmentDue >= ToAmountInstFee) Then Return 0

        Dim sisa = ToAmountInstFee - InstallmentDue
        If (sisa <= 0) Then Return 0
        If (sisa <= LcInstallment) Then Return sisa
        Return LcInstallment

    End Function

    Public Property IsHaveOtor() As Boolean
        Get
            Return (CType(_isHaveOtor, Boolean))
        End Get
        Set(ByVal Value As Boolean)
            _isHaveOtor = Value
        End Set
    End Property
    Public Property chkPotong As String
    Public Property isAkhirBayar As Boolean
    Public UploadDate As Date
    Public TglTransaksi As Date
    Public WaktuTransaksi As DateTime
    Public SequenceNo As Integer
    Public DueDate As DateTime
    Public PrincipalAmount As Double
    Public InterestAmount As Double
    Public Property InvoiceNo As String
    Public InvoiceSeqNo As Integer
    Public RequestNo As String
    Public ApplicationModule As String

    Public Property NoFasilitas() As String
        Get
            Return CType(_NoFasilitas, String)
        End Get
        Set(ByVal Value As String)
            _NoFasilitas = Value
        End Set
    End Property
End Class
