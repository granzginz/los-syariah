
<Serializable()> _
Public Class BatchVoucher : Inherits Maxiloan.Parameter.AccMntBase
    Private _batchno As String
    Private _batchlist As DataTable
    Private _batchdescription As String
    Private _wayofpayment As String
    Private _notes As String
    Private _status As String
    Private _statusdate As String
    Private _postedcashierid As String
    Private _postedcashierdate As String
    Private _openingsequenceno As Integer
    Private _bgduedate As String
    Private _bgno As String
    Private _processid As String
    Private _receivedisburseflag As String
    Private _batchdate As Date
    Private _wop As String
    Private _batchsequenceno As Integer
    Private _tablebatch As DataTable
    Private _ExcelFilePath As String

    Public Property ExcelFilePath() As String
        Get
            Return _ExcelFilePath
        End Get
        Set(ByVal Value As String)
            _ExcelFilePath = Value
        End Set
    End Property

    Public Property TableBatch() As DataTable
        Get
            Return _tablebatch
        End Get
        Set(ByVal Value As DataTable)
            _tablebatch = Value
        End Set
    End Property

    Public Property BatchSequenceNo() As Integer
        Get
            Return _batchsequenceno
        End Get
        Set(ByVal Value As Integer)
            _batchsequenceno = Value
        End Set
    End Property
    Public Property BGDueDate() As String
        Get
            Return _bgduedate
        End Get
        Set(ByVal Value As String)
            _bgduedate = Value
        End Set
    End Property

    Public Property BGNo() As String
        Get
            Return _bgno
        End Get
        Set(ByVal Value As String)
            _bgno = Value
        End Set
    End Property

    Public Property ProcessID() As String
        Get
            Return _processid
        End Get
        Set(ByVal Value As String)
            _processid = Value
        End Set
    End Property

    Public Property ReceiveDisburseFlag() As String
        Get
            Return _receivedisburseflag
        End Get
        Set(ByVal Value As String)
            _receivedisburseflag = Value
        End Set
    End Property

    Public Property BatchNo() As String
        Get
            Return _batchno
        End Get
        Set(ByVal Value As String)
            _batchno = Value
        End Set
    End Property

    Public Property BatchList() As DataTable
        Get
            Return _batchlist
        End Get
        Set(ByVal Value As DataTable)
            _batchlist = Value
        End Set
    End Property

    Public Property BatchDescription() As String
        Get
            Return _batchdescription
        End Get
        Set(ByVal Value As String)
            _batchdescription = Value
        End Set
    End Property

    Public Property WayOfPayment() As String
        Get
            Return _wayofpayment
        End Get
        Set(ByVal Value As String)
            _wayofpayment = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property

    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property

    Public Property StatusDate() As String
        Get
            Return _statusdate
        End Get
        Set(ByVal Value As String)
            _statusdate = Value
        End Set
    End Property

    Public Property PostedCashierID() As String
        Get
            Return _postedcashierid
        End Get
        Set(ByVal Value As String)
            _postedcashierid = Value
        End Set
    End Property

    Public Property PostedDate() As String
        Get
            Return _postedcashierdate
        End Get
        Set(ByVal Value As String)
            _postedcashierdate = Value
        End Set
    End Property

    Public Property OpeningSequenceNo() As Integer
        Get
            Return _openingsequenceno
        End Get
        Set(ByVal Value As Integer)
            _openingsequenceno = Value
        End Set
    End Property

    Public Property BatchDate() As DateTime
        Get
            Return _batchdate
        End Get
        Set(ByVal Value As DateTime)
            _batchdate = Value
        End Set
    End Property
    Public Property WayOfPaymentName() As String
        Get
            Return _wop
        End Get
        Set(ByVal Value As String)
            _wop = Value
        End Set
    End Property
End Class
