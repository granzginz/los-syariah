
<Serializable()> _
Public Class PaymentReversal : Inherits Maxiloan.Parameter.AccMntBase
    Private _listreversalpayment As DataTable
    Private _historysequenceno As Integer
    Private _branchreceivedid As String
    Private _reasonReversal As String
    Private _InvoiceSeqNo As String
    Private _InvoiceNo As String

    Public Property ListReversalPayment() As DataTable
        Get
            Return CType(_listreversalpayment, DataTable)
        End Get
        Set(ByVal Value As DataTable)
            _listreversalpayment = Value
        End Set
    End Property
    Public Property HistorySequenceNo() As Integer
        Get
            Return CType(_historysequenceno, Integer)
        End Get
        Set(ByVal Value As Integer)
            _historysequenceno = Value
        End Set
    End Property
    Public Property ReasonReversal() As String
        Get
            Return CType(_reasonReversal, String)
        End Get
        Set(ByVal Value As String)
            _reasonReversal = Value
        End Set
    End Property

    Public Property BranchReceivedID() As String
        Get
            Return CType(_branchreceivedid, String)
        End Get
        Set(ByVal Value As String)
            _branchreceivedid = Value
        End Set
    End Property

    Public Property InvoiceSeqNo() As Integer
        Get
            Return CType(_InvoiceSeqNo, Integer)
        End Get
        Set(ByVal Value As Integer)
            _InvoiceSeqNo = Value
        End Set
    End Property

    Public Property InvoiceNo() As String
        Get
            Return CType(_InvoiceNo, String)
        End Get
        Set(ByVal Value As String)
            _InvoiceNo = Value
        End Set
    End Property
End Class
