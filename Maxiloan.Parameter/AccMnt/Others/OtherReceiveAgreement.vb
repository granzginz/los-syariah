
<Serializable()> _
Public Class OtherReceiveAgreement
    Inherits Maxiloan.Parameter.OtherReceive
    Private _isvalidtrans As Boolean
    Private _listreceive As DataTable
    Private _filename As String
    Private _erasexml As String
    Private _wop As String
    Private _notes As String
    Private _desctrans As String
    Private _amounttrans As String
    Public Property FileName() As String
        Get
            Return _filename
        End Get
        Set(ByVal Value As String)
            _filename = Value
        End Set
    End Property

    Public Property EraseXML() As String
        Get
            Return _erasexml
        End Get
        Set(ByVal Value As String)
            _erasexml = Value
        End Set
    End Property

    Public Property DescTrans() As String
        Get
            Return (CType(_desctrans, String))
        End Get
        Set(ByVal Value As String)
            _desctrans = Value
        End Set
    End Property
End Class
