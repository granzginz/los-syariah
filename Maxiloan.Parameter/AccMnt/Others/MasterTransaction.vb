
<Serializable()> _
Public Class MasterTransaction : Inherits MasterTransactionBase
    Public Property DtTable As DataTable
    Public Property List As List(Of MasterTransactionBase)
End Class

<Serializable()> _
Public Class MasterTransactionBase : Inherits Common    
    Public Property PaymentAllocationID As String
    Public Property Description As String
    Public Property COA As String
    Public Property Jumlah As String
End Class

