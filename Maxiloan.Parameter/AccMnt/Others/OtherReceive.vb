
<Serializable()> _
Public Class OtherReceive
    Inherits Maxiloan.Parameter.AccMntBase

#Region "Constanta"
    Private _listReceive As DataTable
    Private _PaymentAllocationdesc As String
    Private _WOP As String
    Private _BankAccountname As String
    Private _Notes As String
    Private _Desc As String
    Private _AmountTrans As Double
    Private _IsValidTrans As Boolean
    Private _flagdelete As String
    Private _hpsxml As String
    Private _IndexDelete As String
    Private _NumTrans As Double
    Private _strId As String
    Private _strdesc As String
    Private _strAmount As String
    Private _BGNo As String
    Private _BGDueDate As Date
#End Region


    Public Property listReceive() As DataTable
        Get
            Return (CType(_listReceive, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _listReceive = Value
        End Set
    End Property


    Public Property PaymentAllocationdesc() As String
        Get
            Return (CType(_PaymentAllocationdesc, String))
        End Get
        Set(ByVal Value As String)
            _PaymentAllocationdesc = Value
        End Set
    End Property

    Public Property WOP() As String
        Get
            Return (CType(_WOP, String))
        End Get
        Set(ByVal Value As String)
            _WOP = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return (CType(_Notes, String))
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property
    Public Property Desc() As String
        Get
            Return (CType(_Desc, String))
        End Get
        Set(ByVal Value As String)
            _Desc = Value
        End Set
    End Property

    Public Property AmountTrans() As Double
        Get
            Return (CType(_AmountTrans, Double))
        End Get
        Set(ByVal Value As Double)
            _AmountTrans = Value
        End Set
    End Property


    Public Property IsValidTrans() As Boolean
        Get
            Return (CType(_IsValidTrans, Boolean))
        End Get
        Set(ByVal Value As Boolean)
            _IsValidTrans = Value
        End Set
    End Property

    Public Property hpsxml() As String
        Get
            Return (CType(_hpsxml, String))
        End Get
        Set(ByVal Value As String)
            _hpsxml = Value
        End Set
    End Property
    Public Property flagdelete() As String
        Get
            Return (CType(_flagdelete, String))
        End Get
        Set(ByVal Value As String)
            _flagdelete = Value
        End Set
    End Property
    Public Property IndexDelete() As String
        Get
            Return (CType(_IndexDelete, String))
        End Get
        Set(ByVal Value As String)
            _IndexDelete = Value
        End Set
    End Property

    Public Property NumTrans() As Double
        Get
            Return (CType(_NumTrans, Double))
        End Get
        Set(ByVal Value As Double)
            _NumTrans = Value
        End Set
    End Property

    Public Property strId() As String
        Get
            Return (CType(_strId, String))
        End Get
        Set(ByVal Value As String)
            _strId = Value
        End Set
    End Property
    Public Property strDesc() As String
        Get
            Return (CType(_strdesc, String))
        End Get
        Set(ByVal Value As String)
            _strdesc = Value
        End Set
    End Property

    Public Property strAmount() As String
        Get
            Return (CType(_strAmount, String))
        End Get
        Set(ByVal Value As String)
            _strAmount = Value
        End Set
    End Property

    Public Property BGNo() As String
        Get
            Return (CType(_BGNo, String))
        End Get
        Set(ByVal Value As String)
            _BGNo = Value
        End Set
    End Property
    Public Property BGDueDate() As Date
        Get
            Return (CType(_BGDueDate, Date))
        End Get
        Set(ByVal Value As Date)
            _BGDueDate = Value
        End Set
    End Property

    Public Property JenisTransfer As String
    Public Property BeneficiaryBankID As String
    Public Property BeneficiaryBankBranchID As String
    Public Property BeneficiaryBankAccountNo As String
    Public Property BeneficiaryBankAccountName As String
    Public Property ErrorMessage As String
    Public Property strchkPotong As String
    Public Property strIsPotong As String
End Class
