
<Serializable()> _
Public Class ArAnalysis : Inherits Maxiloan.Parameter.AccMntBase
    Private _periode As String
    Private _ListDataReport As DataSet
    Private _SpName As String

    Public Property Periode() As String
        Get
            Return _periode
        End Get
        Set(ByVal Value As String)
            _periode = Value
        End Set
    End Property
    Public Property ListDataReport() As DataSet
        Get
            Return _ListDataReport
        End Get
        Set(ByVal Value As DataSet)
            _ListDataReport = Value
        End Set
    End Property
    Public Property SpName() As String
        Get
            Return _SpName
        End Get
        Set(ByVal Value As String)
            _SpName = Value
        End Set
    End Property
End Class
