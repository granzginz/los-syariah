﻿<Serializable()> _
    Public Class AdvanceDetail
    Public Property BranchId As String
    Public Property AdvanceNo As String
    Public Property SequenceNo As Integer
    Public Property PaymentAllocationId As String
    Public Property PaymentAllocationName As String
    Public Property Description As String
    Public Property Amount As Decimal
    Public Property Id As String
    Public Property Seq As String

   

        Public Shared Function ToDataTable(data As IList(Of AdvanceDetail)) As DataTable
            Dim dt = New DataTable()

            dt.Columns.Add("ID", GetType(Long))
            dt.Columns.Add("BranchId", GetType(String))
            dt.Columns.Add("AdvanceNo", GetType(String))
        dt.Columns.Add("Seq", GetType(Integer))
            dt.Columns.Add("PaymentAllocationId", GetType(String))
            dt.Columns.Add("Description", GetType(String))
            dt.Columns.Add("Amount", GetType(Decimal))

            For Each v In data


                Dim row = dt.NewRow()

                row("ID") = v.Id
            row("BranchId") = v.BranchId
                row("AdvanceNo") = v.AdvanceNo
            row("Seq") = CType(v.Seq, Int32)
                row("PaymentAllocationId") = v.PaymentAllocationID
                row("Description") = v.Description
                row("Amount") = v.Amount
                dt.Rows.Add(row)
            Next

            Return dt



        End Function

   

    End Class
