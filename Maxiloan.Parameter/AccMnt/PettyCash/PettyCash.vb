
<Serializable()> _
Public Class PettyCash : Inherits Maxiloan.Parameter.Common
#Region "Member Variables"
    '*** PC = PettyCash
    Private _PettyCashNo As String
    Private _PettyCashNoPDC As String
    Private _SequenceNO As String
    Private _DepartementId As String
    Private _DepartementName As String
    Private _EmployeeId As String
    Private _EmployeeName As String
    Private _Description As String
    Private _DescriptionDetail As String
    Private _PCAmount As Double
    Private _PCDate As Date
    Private _Status As String
    Private _PCDStatus As String
    Private _WOP As String
    '*** PettyCashInquiryView additional fields
    Private _PCVoucherNo As String
    Private _BankAccountName As String
    Private _TransactionName As String
    Private _PCDetailDescription As String
    Private _PCDetailAmount As Double
    Private _PCReversalVoucherNo As String
    Private _CashierNameTransaction As String
    Private _CashierNameReversal As String
    Private _PCStatusDate As Date

    Private _PagingTable As DataTable
    Private _ReportDataSet As DataSet
    Private _TotalAmount As Double
    Private _BankAccountID As String
    Private _ListData As DataTable
    Private _ListPC As DataTable
    Private _Amount As Double
    Private _ValueDate As Date
    Private _FlagDelete As String
    Private _HpsXml As String
    Private _isValidPC As Boolean
    Private _NumPC As Integer
    Private _IndexDelete As String
    Private _TransactionID As String

    Private _BGNo As String
    Private _BGDueDate As DateTime
    Private _PaymentAllocationID As String

    Private _strID As String
    Private _strDesc As String
    Private _strAmount As String
    Private _BankAccountType As String
    Private _NDtTable As DataTable
    Private _IsReimburse As Boolean
    Private _ApprovalSchemeID As String
    Private _RequestNo As String
    Private _IsReRequest As Boolean
    Private _IsFinal As Boolean
    Private _UserApproval As String
    Private _NextPersonApproval As String
    Private _Where As String
    Private _ApprovalResult As String
    Private _ApprovalDate As String
    Private _SecurityCode As String
    Private _UserSecurityCode As String
    Private _IsEverRejected As Boolean

#End Region

    Public Property PaymentAllocationID() As String
        Get
            Return _PaymentAllocationID
        End Get
        Set(ByVal Value As String)
            _PaymentAllocationID = Value
        End Set
    End Property

    Public Property BGNo() As String
        Get
            Return _BGNo
        End Get
        Set(ByVal Value As String)
            _BGNo = Value
        End Set
    End Property

    Public Property BGDueDate() As DateTime
        Get
            Return _BGDueDate
        End Get
        Set(ByVal Value As DateTime)
            _BGDueDate = Value
        End Set
    End Property

    Public Property IndexDelete() As String
        Get
            Return _IndexDelete
        End Get
        Set(ByVal Value As String)
            _IndexDelete = Value
        End Set
    End Property
    Public Property TransactionID() As String
        Get
            Return _TransactionID
        End Get
        Set(ByVal Value As String)
            _TransactionID = Value
        End Set
    End Property

    Public Property NumPC() As Integer
        Get
            Return _NumPC
        End Get
        Set(ByVal Value As Integer)
            _NumPC = Value
        End Set
    End Property
    Public Property isValidPC() As Boolean
        Get
            Return _isValidPC
        End Get
        Set(ByVal Value As Boolean)
            _isValidPC = Value
        End Set
    End Property


    Public Property HpsXml() As String
        Get
            Return _HpsXml
        End Get
        Set(ByVal Value As String)
            _HpsXml = Value
        End Set
    End Property

    Public Property Amount() As Double
        Get
            Return _Amount
        End Get
        Set(ByVal Value As Double)
            _Amount = Value
        End Set
    End Property

    Public Property ValueDate() As Date
        Get
            Return _ValueDate
        End Get
        Set(ByVal Value As Date)
            _ValueDate = Value
        End Set
    End Property


    Public Property FlagDelete() As String
        Get
            Return _FlagDelete
        End Get
        Set(ByVal Value As String)
            _FlagDelete = Value
        End Set
    End Property

    Public Property ListPC() As DataTable
        Get
            Return _ListPC
        End Get
        Set(ByVal Value As DataTable)
            _ListPC = Value
        End Set
    End Property

    Public Property PettyCashNo() As String
        Get
            Return _PettyCashNo
        End Get
        Set(ByVal Value As String)
            _PettyCashNo = Value
        End Set
    End Property

    Public Property PettyCashNoPDC() As String
        Get
            Return _PettyCashNoPDC
        End Get
        Set(ByVal Value As String)
            _PettyCashNoPDC = Value
        End Set
    End Property
    Public Property DepartementId() As String
        Get
            Return _DepartementId
        End Get
        Set(ByVal Value As String)
            _DepartementId = Value
        End Set
    End Property
    Public Property DepartementName() As String
        Get
            Return _DepartementName
        End Get
        Set(ByVal Value As String)
            _DepartementName = Value
        End Set
    End Property
    Public Property EmployeeId() As String
        Get
            Return _EmployeeId
        End Get
        Set(ByVal Value As String)
            _EmployeeId = Value
        End Set
    End Property
    Public Property EmployeeName() As String
        Get
            Return _EmployeeName
        End Get
        Set(ByVal Value As String)
            _EmployeeName = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property

    Public Property DescriptionDetail() As String
        Get
            Return _DescriptionDetail
        End Get
        Set(ByVal Value As String)
            _DescriptionDetail = Value
        End Set
    End Property


    Public Property PCAmount() As Double
        Get
            Return _PCAmount
        End Get
        Set(ByVal Value As Double)
            _PCAmount = Value
        End Set
    End Property
    Public Property PCDate() As Date
        Get
            Return _PCDate
        End Get
        Set(ByVal Value As Date)
            _PCDate = Value
        End Set
    End Property
    Public Property PCStatus() As String
        Get
            Return _Status
        End Get
        Set(ByVal Value As String)
            _Status = Value
        End Set
    End Property
    Public Property PCDStatus() As String
        Get
            Return _PCDStatus
        End Get
        Set(ByVal Value As String)
            _PCDStatus = Value
        End Set
    End Property
    Public Property ReportDataSet() As DataSet
        Get
            Return (CType(_ReportDataSet, DataSet))
        End Get
        Set(ByVal Value As DataSet)
            _ReportDataSet = Value
        End Set
    End Property
    Public Property PagingTable() As DataTable
        Get
            Return (CType(_PagingTable, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            _PagingTable = Value
        End Set
    End Property
    Public Property PCVoucherNo() As String
        Get
            Return _PCVoucherNo
        End Get
        Set(ByVal Value As String)
            _PCVoucherNo = Value
        End Set
    End Property
    Public Property BankAccountName() As String
        Get
            Return _BankAccountName
        End Get
        Set(ByVal Value As String)
            _BankAccountName = Value
        End Set
    End Property
    Public Property TransactionName() As String
        Get
            Return _TransactionName
        End Get
        Set(ByVal Value As String)
            _TransactionName = Value
        End Set
    End Property
    Public Property PCDetailDescription() As String
        Get
            Return _PCDetailDescription
        End Get
        Set(ByVal Value As String)
            _PCDetailDescription = Value
        End Set
    End Property
    Public Property PCDetailAmount() As Double
        Get
            Return _PCDetailAmount
        End Get
        Set(ByVal Value As Double)
            _PCDetailAmount = Value
        End Set
    End Property
    Public Property PCReversalVoucherNo() As String
        Get
            Return _PCReversalVoucherNo
        End Get
        Set(ByVal Value As String)
            _PCReversalVoucherNo = Value
        End Set
    End Property
    Public Property CashierNameTransaction() As String
        Get
            Return _CashierNameTransaction
        End Get
        Set(ByVal Value As String)
            _CashierNameTransaction = Value
        End Set
    End Property
    Public Property CashierNameReversal() As String
        Get
            Return _CashierNameReversal
        End Get
        Set(ByVal Value As String)
            _CashierNameReversal = Value
        End Set
    End Property
    Public Property PCStatusDate() As Date
        Get
            Return _PCStatusDate
        End Get
        Set(ByVal Value As Date)
            _PCStatusDate = Value
        End Set
    End Property
    Public Property BankAccountID() As String
        Get
            Return _BankAccountID
        End Get
        Set(ByVal Value As String)
            _BankAccountID = Value
        End Set
    End Property
    Public Property TotalAmount() As Double
        Get
            Return _TotalAmount
        End Get
        Set(ByVal Value As Double)
            _TotalAmount = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal Value As DataTable)
            _ListData = Value
        End Set
    End Property

    Public Property WOP() As String
        Get
            Return _WOP
        End Get
        Set(ByVal Value As String)
            _WOP = Value
        End Set
    End Property

    Public Property strID() As String
        Get
            Return _strID
        End Get
        Set(ByVal Value As String)
            _strID = Value
        End Set
    End Property

    Public Property strDesc() As String
        Get
            Return _strDesc
        End Get
        Set(ByVal Value As String)
            _strDesc = Value
        End Set
    End Property

    Public Property strAmount() As String
        Get
            Return _strAmount
        End Get
        Set(ByVal Value As String)
            _strAmount = Value
        End Set
    End Property


    Public Property BankAccountType() As String
        Get
            Return _BankAccountType
        End Get
        Set(ByVal Value As String)
            _BankAccountType = Value
        End Set
    End Property

    Public Property SequenceNo() As String
        Get
            Return _SequenceNO
        End Get
        Set(ByVal Value As String)
            _SequenceNO = Value
        End Set
    End Property

    Public Property NDtTable() As DataTable
        Get
            Return _NDtTable
        End Get
        Set(ByVal Value As DataTable)
            _NDtTable = Value
        End Set
    End Property
    Public Property IsReimburse() As Boolean
        Get
            Return _IsReimburse
        End Get
        Set(ByVal Value As Boolean)
            _IsReimburse = Value
        End Set
    End Property
    Public Property ApprovalBy As String
    Public Property ApprovalFrom As String
    Public Property notes As String

    Public Property ApprovalSchemeID() As String
        Get
            Return _ApprovalSchemeID
        End Get
        Set(ByVal Value As String)
            _ApprovalSchemeID = Value
        End Set
    End Property

    Public Property RequestNo() As String
        Get
            Return _RequestNo
        End Get
        Set(ByVal Value As String)
            _RequestNo = Value
        End Set
    End Property

    Public Property IsReRequest() As Boolean
        Get
            Return _IsReRequest
        End Get
        Set(ByVal Value As Boolean)
            _IsReRequest = Value
        End Set
    End Property

    Public Property IsFinal() As Boolean
        Get
            Return _IsFinal
        End Get
        Set(ByVal Value As Boolean)
            _IsFinal = Value
        End Set
    End Property

    Public Property UserApproval() As String
        Get
            Return _UserApproval
        End Get
        Set(ByVal Value As String)
            _UserApproval = Value
        End Set
    End Property

    Public Property NextPersonApproval() As String
        Get
            Return _NextPersonApproval
        End Get
        Set(ByVal Value As String)
            _NextPersonApproval = Value
        End Set
    End Property

    Public Property Where() As String
        Get
            Return _Where
        End Get
        Set(ByVal Value As String)
            _Where = Value
        End Set
    End Property

    Public Property ApprovalResult() As String
        Get
            Return _ApprovalResult
        End Get
        Set(ByVal Value As String)
            _ApprovalResult = Value
        End Set
    End Property

    Public Property ApprovalDate() As Date
        Get
            Return _ApprovalDate
        End Get
        Set(ByVal Value As Date)
            _ApprovalDate = Value
        End Set
    End Property

    Public Property SecurityCode() As String
        Get
            Return _SecurityCode
        End Get
        Set(ByVal Value As String)
            _SecurityCode = Value
        End Set
    End Property

    Public Property UserSecurityCode() As String
        Get
            Return _UserSecurityCode
        End Get
        Set(ByVal Value As String)
            _UserSecurityCode = Value
        End Set
    End Property

    Public Property IsEverRejected() As Boolean
        Get
            Return _IsEverRejected
        End Get
        Set(ByVal Value As Boolean)
            _IsEverRejected = Value
        End Set
    End Property
    Public Property PettyCashStatus As String
    Public Property RejectBy As String
    Public Property RejectDate As DateTime
End Class