

<Serializable()> _
Public Class AdvanceRequest : Inherits Maxiloan.Parameter.AccMntBase
    Private m_EmployeeID As String
    Private m_BusinessDate As Date
    Private m_BankAccountID As String
    Private m_BankAccountName As String
    Private m_BankAccountType As String
    Private m_Amount As String
    Private m_BilyetGiroNo As String
    Private m_BilyetGiroDueDate As Date
    Private m_Description As String
    Private m_ParamReport As String
    '*** AdvanceInquiryView additional fields
    Private m_AdvanceNo As String
    Private m_EmployeeName As String
    Private m_VoucherNoAdvance As String
    Private m_AdvanceDate As Date
    Private m_ReturnToAccount As String
    Private m_CashierReturn As String
    Private m_VoucherNoReturn As String
    Private m_AdvanceStatus As String
    Private m_CashierAdvance As String
    Private m_StatusDate As Date

    Public Const FLD_NM_VOUCHER_NO_RTN As String = "VoucherNoRtn"
    Public Const FLD_NM_ADVANCE_STATUS As String = "StatusAdvance"

    Private m_PettyCashAdvanceList As DataTable
    Private m_PettyCashAdvanceDataSet As DataSet

    Public Property PettyCashAdvanceDataSet() As DataSet
        Get
            Return (CType(m_PettyCashAdvanceDataSet, DataSet))
        End Get
        Set(ByVal Value As DataSet)
            m_PettyCashAdvanceDataSet = Value
        End Set
    End Property
    Public Property AmountString() As String
        Get
            Return m_Amount
        End Get
        Set(ByVal Value As String)
            m_Amount = Value
        End Set
    End Property
    Public Property EmployeeId() As String
        Get
            Return m_EmployeeID
        End Get
        Set(ByVal Value As String)
            m_EmployeeID = Value
        End Set
    End Property
    Public Property AdvanceDate() As Date
        Get
            Return m_BusinessDate
        End Get
        Set(ByVal Value As Date)
            m_BusinessDate = Value
        End Set
    End Property

    Public Property BilyetGiroNo() As String
        Get
            Return m_BilyetGiroNo
        End Get
        Set(ByVal Value As String)
            m_BilyetGiroNo = Value
        End Set
    End Property
    Public Property BilyetGiroDueDate() As Date
        Get
            Return m_BilyetGiroDueDate
        End Get
        Set(ByVal Value As Date)
            m_BilyetGiroDueDate = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return m_Description
        End Get
        Set(ByVal Value As String)
            m_Description = Value
        End Set
    End Property
    Public Property PettyCashAdvanceList() As DataTable
        Get
            Return (CType(m_PettyCashAdvanceList, DataTable))
        End Get
        Set(ByVal Value As DataTable)
            m_PettyCashAdvanceList = Value
        End Set
    End Property

    Public Property ParamReport() As String
        Get
            Return m_ParamReport
        End Get
        Set(ByVal Value As String)
            m_ParamReport = Value
        End Set
    End Property

    '*** AdvanceInquiryView additional properties
    Public Property AdvanceNo() As String
        Get
            Return m_AdvanceNo
        End Get
        Set(ByVal Value As String)
            m_AdvanceNo = Value
        End Set
    End Property
    Public Property EmployeeName() As String
        Get
            Return m_EmployeeName
        End Get
        Set(ByVal Value As String)
            m_EmployeeName = Value
        End Set
    End Property
    Public Property VoucherNoAdvance() As String
        Get
            Return m_VoucherNoAdvance
        End Get
        Set(ByVal Value As String)
            m_VoucherNoAdvance = Value
        End Set
    End Property
    Public Property ReturnToAccount() As String
        Get
            Return m_ReturnToAccount
        End Get
        Set(ByVal Value As String)
            m_ReturnToAccount = Value
        End Set
    End Property
    Public Property CashierReturn() As String
        Get
            Return m_CashierReturn
        End Get
        Set(ByVal Value As String)
            m_CashierReturn = Value
        End Set
    End Property
    Public Property VoucherNoReturn() As String
        Get
            Return m_VoucherNoReturn
        End Get
        Set(ByVal Value As String)
            m_VoucherNoReturn = Value
        End Set
    End Property
    Public Property AdvanceStatus() As String
        Get
            Return m_AdvanceStatus
        End Get
        Set(ByVal Value As String)
            m_AdvanceStatus = Value
        End Set
    End Property
    Public Property CashierAdvance() As String
        Get
            Return m_CashierAdvance
        End Get
        Set(ByVal Value As String)
            m_CashierAdvance = Value
        End Set
    End Property
    Public Property StatusDate() As Date
        Get
            Return m_StatusDate
        End Get
        Set(ByVal Value As Date)
            m_StatusDate = Value
        End Set
    End Property
    Public Property Keterangan As String
    Public Property Amount As Double
    Public Property TransactionName As String
    Public Property TransactionID As String
End Class
