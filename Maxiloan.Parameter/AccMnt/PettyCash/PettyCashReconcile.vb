
<Serializable()> _
Public Class PettyCashReconcile : Inherits PettyCash

    Private _RequestNo As String
    Private _ReconcileMode As String


    Public Property ReconcileMode() As String
        Get
            Return _ReconcileMode
        End Get
        Set(ByVal Value As String)
            _ReconcileMode = Value
        End Set
    End Property



    Public Property RequestNo() As String
        Get
            Return _RequestNo
        End Get
        Set(ByVal Value As String)
            _RequestNo = Value
        End Set
    End Property


End Class
