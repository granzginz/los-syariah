
<Serializable()> _
Public Class InsClaimReceive : Inherits Maxiloan.Parameter.AccMntBase
    Private _insurancecoy As String
    Private _claimtype As String
    Private _claimdate As DateTime
    Private _eventdate As String
    Private _claimamount As Double
    Private _reportedby As String
    Private _reportdate As String
    Private _notes As String
    Private _claimdocno As String
    Private _insurancepaidby As String
    Private _description As String
    Private _wop As String
    Private _companyid As String
    Private _InsSequenceNo As String
    Private _AssetSeqNo As String
    Private _InsClaimSeqNo As String
    Public Property InsSequenceNo() As String
        Get
            Return _InsSequenceNo
        End Get
        Set(ByVal Value As String)
            _InsSequenceNo = Value
        End Set
    End Property

    Public Property AssetSeqNo() As String
        Get
            Return _AssetSeqNo
        End Get
        Set(ByVal Value As String)
            _AssetSeqNo = Value
        End Set
    End Property
    Public Property InsClaimSeqNo() As String
        Get
            Return _InsClaimSeqNo
        End Get
        Set(ByVal Value As String)
            _InsClaimSeqNo = Value
        End Set
    End Property

    Public Property CompanyID() As String
        Get
            Return _companyid
        End Get
        Set(ByVal Value As String)
            _companyid = Value
        End Set
    End Property
    Public Property WOP() As String
        Get
            Return _wop
        End Get
        Set(ByVal Value As String)
            _wop = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal Value As String)
            _description = Value
        End Set
    End Property
    Public Property InsurancePaidBy() As String
        Get
            Return _insurancepaidby
        End Get
        Set(ByVal Value As String)
            _insurancepaidby = Value
        End Set
    End Property
    Public Property ClaimDocNo() As String
        Get
            Return _claimdocno
        End Get
        Set(ByVal Value As String)
            _claimdocno = Value
        End Set
    End Property
    Public Property InsuranceCoy() As String
        Get
            Return _insurancecoy
        End Get
        Set(ByVal Value As String)
            _insurancecoy = Value
        End Set
    End Property
    Public Property ClaimType() As String
        Get
            Return _claimtype
        End Get
        Set(ByVal Value As String)
            _claimtype = Value
        End Set
    End Property
    Public Property ClaimDate() As DateTime
        Get
            Return _claimdate
        End Get
        Set(ByVal Value As DateTime)
            _claimdate = Value
        End Set
    End Property
    Public Property EventDate() As String
        Get
            Return _eventdate
        End Get
        Set(ByVal Value As String)
            _eventdate = Value
        End Set
    End Property
    Public Property ClaimAmount() As Double
        Get
            Return _claimamount
        End Get
        Set(ByVal Value As Double)
            _claimamount = Value
        End Set
    End Property
    Public Property ReportedBy() As String
        Get
            Return _reportedby
        End Get
        Set(ByVal Value As String)
            _reportedby = Value
        End Set
    End Property

    Public Property ReportDate() As String
        Get
            Return _reportdate
        End Get
        Set(ByVal Value As String)
            _reportdate = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal Value As String)
            _notes = Value
        End Set
    End Property
End Class
