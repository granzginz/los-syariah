<Serializable()> _
Public Class DocumentChangeLocation : Inherits Common
#Region "Variables"
    Private _OnHandDocumentList As DataTable
    Private _DocumentChangeLocationLog As DataTable
    Private _DocumentRackList As DataTable
    Private _DocumentFillingList As DataTable
    Private _AssetDocumentReport As DataTable
    Private _ActorBranchID As String
    Private _DocumentRackID As String
    Private _DocumentRackDesc As String
    Private _DocumentFillingID As String
    Private _DocumentFillingDesc As String
    Private _TotalPages As Int64
    Private _ChangedDocumentLocation As DataTable
    Private _StatusDateFrom As String
    Private _StatusDateTo As String
    Private _AssetTypeID As String
#End Region
#Region "Properties"
    Public Property OnHandDocumentList() As DataTable
        Get
            Return _OnHandDocumentList
        End Get
        Set(ByVal Value As DataTable)
            _OnHandDocumentList = Value
        End Set
    End Property
    Public Property ChangedDocumentLocation() As DataTable
        Get
            Return _ChangedDocumentLocation
        End Get
        Set(ByVal Value As DataTable)
            _ChangedDocumentLocation = Value
        End Set
    End Property
    Public Property DocumentChangeLocationLog() As DataTable
        Get
            Return _DocumentChangeLocationLog
        End Get
        Set(ByVal Value As DataTable)
            _DocumentChangeLocationLog = Value
        End Set
    End Property
    Public Property DocumentRackList() As DataTable
        Get
            Return _DocumentRackList
        End Get
        Set(ByVal Value As DataTable)
            _DocumentRackList = Value
        End Set
    End Property
    Public Property AssetDocumentReport() As DataTable
        Get
            Return _AssetDocumentReport
        End Get
        Set(ByVal Value As DataTable)
            _AssetDocumentReport = Value
        End Set
    End Property
    Public Property DocumentFillingList() As DataTable
        Get
            Return _DocumentFillingList
        End Get
        Set(ByVal Value As DataTable)
            _DocumentFillingList = Value
        End Set
    End Property
    Public Property ActorBranchID() As String
        Get
            Return _ActorBranchID
        End Get
        Set(ByVal Value As String)
            _ActorBranchID = Value
        End Set
    End Property
    Public Property DocumentRackID() As String
        Get
            Return _DocumentRackID
        End Get
        Set(ByVal Value As String)
            _DocumentRackID = Value
        End Set
    End Property
    Public Property DocumentRackDescription() As String
        Get
            Return _DocumentRackDesc
        End Get
        Set(ByVal Value As String)
            _DocumentRackDesc = Value
        End Set
    End Property
    Public Property DocumentFillingID() As String
        Get
            Return _DocumentFillingID
        End Get
        Set(ByVal Value As String)
            _DocumentFillingID = Value
        End Set
    End Property
    Public Property DocumentFillingDescription() As String
        Get
            Return _DocumentFillingDesc
        End Get
        Set(ByVal Value As String)
            _DocumentFillingDesc = Value
        End Set
    End Property
    Public Property TotalPages() As Int64
        Get
            Return _TotalPages
        End Get
        Set(ByVal Value As Int64)
            _TotalPages = Value
        End Set
    End Property
    Public Property StatusDateFrom() As String
        Get
            Return _StatusDateFrom
        End Get
        Set(ByVal Value As String)
            _StatusDateFrom = Value
        End Set
    End Property
    Public Property StatusDateTo() As String
        Get
            Return _StatusDateTo
        End Get
        Set(ByVal Value As String)
            _StatusDateTo = Value
        End Set
    End Property
    Public Property AssetTypeID() As String
        Get
            Return _AssetTypeID
        End Get
        Set(ByVal Value As String)
            _AssetTypeID = Value
        End Set
    End Property
#End Region
End Class
