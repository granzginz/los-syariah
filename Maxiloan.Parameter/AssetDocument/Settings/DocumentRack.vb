<Serializable()> _
Public Class DocumentRack : Inherits Common
    Private _RackID As String
    Private _Description As String
    Private _IsActive As Boolean
    Private _listdata As DataTable
    Private _listdataReport As DataSet
    Private _TotalPage As Int64
    Private _ErrorMessage As String
    Private _UpdateAction As Integer
    Private _BranchDescription As String
    Private _BranchListData As DataTable
    Private _FilingID As String = ""

#Region "Properties"
    Public Property FilingID() As String
        Get
            Return _FilingID
        End Get
        Set(ByVal Value As String)
            _FilingID = Value
        End Set
    End Property
    Public Property BranchListData() As DataTable
        Get
            Return _BranchListData
        End Get
        Set(ByVal Value As DataTable)
            _BranchListData = Value
        End Set
    End Property
    Public Property RackID() As String
        Get
            Return _RackID
        End Get
        Set(ByVal Value As String)
            _RackID = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal Value As String)
            _Description = Value
        End Set
    End Property
    Public Property IsActive() As Boolean
        Get
            Return _IsActive
        End Get
        Set(ByVal Value As Boolean)
            _IsActive = Value
        End Set
    End Property
    Public Property ListData() As DataTable
        Get
            Return _listdata
        End Get
        Set(ByVal Value As DataTable)
            _listdata = Value
        End Set
    End Property
    Public Property ListDataReport() As DataSet
        Get
            Return _listdataReport
        End Get
        Set(ByVal Value As DataSet)
            _listdataReport = Value
        End Set
    End Property
    Public Property TotalPage() As Int64
        Get
            Return _TotalPage
        End Get
        Set(ByVal Value As Int64)
            _TotalPage = Value
        End Set
    End Property
    Public Property ErrorMessage() As String
        Get
            Return _ErrorMessage
        End Get
        Set(ByVal Value As String)
            _ErrorMessage = Value
        End Set
    End Property
    Public Property UpdateAction() As Integer
        Get
            Return _UpdateAction
        End Get
        Set(ByVal Value As Integer)
            _UpdateAction = Value
        End Set
    End Property
    Public Property BranchDescription() As String
        Get
            Return _BranchDescription
        End Get
        Set(ByVal Value As String)
            _BranchDescription = Value
        End Set
    End Property
#End Region
#Region "Subs"
    Public Sub New()
        Me.IsActive = True
        Me.Description = ""
        Me.ErrorMessage = "OK"
        Me.BranchId = ""
        Me.RackID = ""
        Me.TotalPage = 0
        Me.TotalRecord = 0
        Me.PageSize = 10
        Me.BranchDescription = ""
    End Sub
#End Region
End Class
