<Serializable()> _
Public Class DocumentCheckOut : Inherits Common
#Region "Variables"
    Private _ActorBranchID As String
    Private _SenderRecipientType As String
    Private _SenderRecipientID As String
    Private _TransactionNo As String
    Private _TransactionDate As DateTime
    Private _SenderRecipientName As String
    Private _CourierName As String
    Private _ExpectedReturnedDateH As DateTime
    Private _Notes As String
    Private _IsFinalRelease As Boolean
    Private _IsPosted As Boolean
    Private _PostedDate As DateTime
    Private _AssetDocMoveSeq As Int64
    'Required Paramater for Update
    Private _DocumentCheckOutHeaderFile As DataTable
    Private _DocumentCheckOutDetailFile As DataTable
    Private _DocumentOnHandForCheckOut As DataTable
    Private _DocumentCheckOutRptFile As DataSet
    Private _DocumentRecipientList As DataTable
    Private _TotalPages As Int64
    Private _LastActionResult As Int64 = 1
    Private _LastActionResultMessage As String = ""
    Private _ActionTakenParam As String
    'Flag When a Person is Authorize to relase document for those contract with it's status marked as 'RRD'
    Private _IsFinalReleaseAllowed As Boolean
    'Detal Table Field
    Private _ApplicationBranchOwner As String
    Private _ApplicationID As String
    Private _CustomerName As String
    Private _AssetSeqNo As Int16
    Private _AssetTypeID As String
    Private _AssetDesc As String
    Private _AssetDocID As String
    Private _DocumentType As String
    Private _DocumentNo As String
    Private _DocumentDate As DateTime
    Private _ExpectedReturnedDateD As DateTime

#End Region
#Region "Properties"
    Public Property DocumentCheckOutHeaderFile() As DataTable
        Get
            Return _DocumentCheckOutHeaderFile
        End Get
        Set(ByVal Value As DataTable)
            _DocumentCheckOutHeaderFile = Value
        End Set
    End Property
    Public Property DocumentCheckOutDetailFile() As DataTable
        Get
            Return _DocumentCheckOutDetailFile
        End Get
        Set(ByVal Value As DataTable)
            _DocumentCheckOutDetailFile = Value
        End Set
    End Property
    Public Property DocumentCheckOutRptFile() As DataSet
        Get
            Return _DocumentCheckOutRptFile
        End Get
        Set(ByVal Value As DataSet)
            _DocumentCheckOutRptFile = Value
        End Set
    End Property
    Public Property DocumentOnHandForCheckOut() As DataTable
        Get
            Return _DocumentOnHandForCheckOut
        End Get
        Set(ByVal Value As DataTable)
            _DocumentOnHandForCheckOut = Value
        End Set
    End Property
    Public Property TotalPages() As Int64
        Get
            Return _TotalPages
        End Get
        Set(ByVal Value As Int64)
            _TotalPages = Value
        End Set
    End Property
    Public Property DocumentRecipientList() As DataTable
        Get
            Return _DocumentRecipientList
        End Get
        Set(ByVal Value As DataTable)
            _DocumentRecipientList = Value
        End Set
    End Property
    'This is Field Data Store Temporary
    Public Property ActorBranchID() As String
        Get
            Return _ActorBranchID
        End Get
        Set(ByVal Value As String)
            _ActorBranchID = Value
        End Set
    End Property
    Public Property SenderRecipientType() As String
        Get
            Return _SenderRecipientType
        End Get
        Set(ByVal Value As String)
            _SenderRecipientType = Value
        End Set
    End Property
    Public Property AssetDocMoveSeq() As Int64
        Get
            Return _AssetDocMoveSeq
        End Get
        Set(ByVal Value As Int64)
            _AssetDocMoveSeq = Value
        End Set
    End Property
    Public Property SenderRecipientID() As String
        Get
            Return _SenderRecipientID
        End Get
        Set(ByVal Value As String)
            _SenderRecipientID = Value
        End Set
    End Property
    Public Property SenderRecipientName() As String
        Get
            Return _SenderRecipientName
        End Get
        Set(ByVal Value As String)
            _SenderRecipientName = Value
        End Set
    End Property
    Public Property TransactionNo() As String
        Get
            Return _TransactionNo
        End Get
        Set(ByVal Value As String)
            _TransactionNo = Value
        End Set
    End Property
    Public Property TransactionDate() As DateTime
        Get
            Return _TransactionDate
        End Get
        Set(ByVal Value As DateTime)
            _TransactionDate = Value
        End Set
    End Property
    Public Property CourierName() As String
        Get
            Return _CourierName
        End Get
        Set(ByVal Value As String)
            _CourierName = Value
        End Set
    End Property
    Public Property ExpectedReturnedDateH() As DateTime
        Get
            Return _ExpectedReturnedDateH
        End Get
        Set(ByVal Value As DateTime)
            _ExpectedReturnedDateH = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property
    Public Property IsPosted() As Boolean
        Get
            Return _IsPosted
        End Get
        Set(ByVal Value As Boolean)
            _IsPosted = Value
        End Set
    End Property
    Public Property PostedDate() As DateTime
        Get
            Return _PostedDate
        End Get
        Set(ByVal Value As DateTime)
            _PostedDate = Value
        End Set
    End Property
    Public Property IsFinalRelease() As Boolean
        Get
            Return _IsFinalRelease
        End Get
        Set(ByVal Value As Boolean)
            _IsFinalRelease = Value
        End Set
    End Property
    Public Property ApplicationBranchOwner() As String
        Get
            Return _ApplicationBranchOwner
        End Get
        Set(ByVal Value As String)
            _ApplicationBranchOwner = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return _CustomerName
        End Get
        Set(ByVal Value As String)
            _CustomerName = Value
        End Set
    End Property
    Public Property AssetSeqNo() As Int16
        Get
            Return _AssetSeqNo
        End Get
        Set(ByVal Value As Int16)
            _AssetSeqNo = Value
        End Set
    End Property
    Public Property AssetTypeID() As String
        Get
            Return _AssetTypeID
        End Get
        Set(ByVal Value As String)
            _AssetTypeID = Value
        End Set
    End Property
    Public Property AssetDesc() As String
        Get
            Return _AssetDesc
        End Get
        Set(ByVal Value As String)
            _AssetDesc = Value
        End Set
    End Property
    Public Property AssetDocID() As String
        Get
            Return _AssetDocID
        End Get
        Set(ByVal Value As String)
            _AssetDocID = Value
        End Set
    End Property
    Public Property DocumentType() As String
        Get
            Return _DocumentType
        End Get
        Set(ByVal Value As String)
            _DocumentType = Value
        End Set
    End Property
    Public Property DocumentNo() As String
        Get
            Return _DocumentNo
        End Get
        Set(ByVal Value As String)
            _DocumentNo = Value
        End Set
    End Property
    Public Property DocumentDate() As DateTime
        Get
            Return _DocumentDate
        End Get
        Set(ByVal Value As DateTime)
            _DocumentDate = Value
        End Set
    End Property
    Public Property LastActionResult() As Int64
        Get
            Return _LastActionResult
        End Get
        Set(ByVal Value As Int64)
            _LastActionResult = Value
        End Set
    End Property
    Public Property LastActionResultMessage() As String
        Get
            Return _LastActionResultMessage
        End Get
        Set(ByVal Value As String)
            _LastActionResultMessage = Value
        End Set
    End Property
    Public Property ActionTakenParam() As String
        Get
            Return _ActionTakenParam
        End Get
        Set(ByVal Value As String)
            _ActionTakenParam = Value
        End Set
    End Property
    Public Property IsFinalReleaseAllowed() As Boolean
        Get
            Return _IsFinalReleaseAllowed
        End Get
        Set(ByVal Value As Boolean)
            _IsFinalReleaseAllowed = Value
        End Set
    End Property
    Public Property ExpectedReturnedDateD() As DateTime
        Get
            Return _ExpectedReturnedDateD
        End Get
        Set(ByVal Value As DateTime)
            _ExpectedReturnedDateD = Value
        End Set
    End Property
#End Region

End Class
