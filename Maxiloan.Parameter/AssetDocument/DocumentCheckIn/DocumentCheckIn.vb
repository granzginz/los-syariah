<Serializable()> _
Public Class DocumentCheckIn : Inherits Common
#Region "Variables"
    Private _ActorBranchID As String
    Private _AssetDocMoveSeq As Int64
    Private _SenderRecipientType As String
    Private _SenderRecipientID As String
    Private _TransactionNo As String
    Private _TransactionDate As DateTime
    Private _SenderRecipientName As String
    Private _SenderRecipientRefNo As String
    Private _CourierName As String
    Private _AssetDocRack As String
    Private _AssetDocFilling As String
    Private _Notes As String
    Private _IsPosted As Boolean
    Private _PostedDate As DateTime
    'Detail Table
    Private _ApplicationBranchOwner As String
    Private _ApplicationID As String
    Private _AssetSeqNo As Integer
    Private _AssetTypeID As String
    Private _AssetDocID As String
    Private _DocumentNo As String
    Private _DocumentDate As DateTime
    'Required Paramater for Update
    Private _AssetValidatorType As String
    Private _AssetValidatorData As String
    Private _AssetValidatorTypeDesc As String
    Private _DocumentCheckInHeaderFile As DataTable
    Private _DocumentCheckInDetailFile As DataTable
    Private _OutstandingDocumentList As DataTable
    Private _DocumentCheckInRptFile As DataSet
    Private _DocumentSenderList As DataTable
    Private _TotalPages As Int64
    Private _LastActionResult As Int64 = 1
    Private _LastActionResultMessage As String = ""
    Private _ActionTakenParam As String
    Private _AssetDocRackList As DataTable
    Private _AssetDocFillingList As DataTable
    Private _CustomerName As String
    Private _AssetDesc As String
    Private _DocDesc As String
    Private _SerialNoData() As String
    Private _SerialNoDesc() As String
#End Region
#Region "Properties"
    Public Property DocumentCheckInHeaderFile() As DataTable
        Get
            Return _DocumentCheckInHeaderFile
        End Get
        Set(ByVal Value As DataTable)
            _DocumentCheckInHeaderFile = Value
        End Set
    End Property
    Public Property DocumentCheckInDetailFile() As DataTable
        Get
            Return _DocumentCheckInDetailFile
        End Get
        Set(ByVal Value As DataTable)
            _DocumentCheckInDetailFile = Value
        End Set
    End Property
    Public Property DocumentCheckInRptFile() As DataSet
        Get
            Return _DocumentCheckInRptFile
        End Get
        Set(ByVal Value As DataSet)
            _DocumentCheckInRptFile = Value
        End Set
    End Property
    Public Property OutstandingDocumentList() As DataTable
        Get
            Return _OutstandingDocumentList
        End Get
        Set(ByVal Value As DataTable)
            _OutstandingDocumentList = Value
        End Set
    End Property
    Public Property TotalPages() As Int64
        Get
            Return _TotalPages
        End Get
        Set(ByVal Value As Int64)
            _TotalPages = Value
        End Set
    End Property
    Public Property DocumentSenderList() As DataTable
        Get
            Return _DocumentSenderList
        End Get
        Set(ByVal Value As DataTable)
            _DocumentSenderList = Value
        End Set
    End Property
    'This is Field Data Store Temporary
    Public Property ActorBranchID() As String
        Get
            Return _ActorBranchID
        End Get
        Set(ByVal Value As String)
            _ActorBranchID = Value
        End Set
    End Property
    Public Property AssetDocMoveSeq() As Int64
        Get
            Return _AssetDocMoveSeq
        End Get
        Set(ByVal Value As Int64)
            _AssetDocMoveSeq = Value
        End Set
    End Property
    Public Property SenderRecipientType() As String
        Get
            Return _SenderRecipientType
        End Get
        Set(ByVal Value As String)
            _SenderRecipientType = Value
        End Set
    End Property
    Public Property SenderRecipientID() As String
        Get
            Return _SenderRecipientID
        End Get
        Set(ByVal Value As String)
            _SenderRecipientID = Value
        End Set
    End Property
    Public Property SenderRecipientName() As String
        Get
            Return _SenderRecipientName
        End Get
        Set(ByVal Value As String)
            _SenderRecipientName = Value
        End Set
    End Property
    Public Property TransactionNo() As String
        Get
            Return _TransactionNo
        End Get
        Set(ByVal Value As String)
            _TransactionNo = Value
        End Set
    End Property
    Public Property TransactionDate() As DateTime
        Get
            Return _TransactionDate
        End Get
        Set(ByVal Value As DateTime)
            _TransactionDate = Value
        End Set
    End Property
    Public Property SenderRecipientRefNo() As String
        Get
            Return _SenderRecipientRefNo
        End Get
        Set(ByVal Value As String)
            _SenderRecipientRefNo = Value
        End Set
    End Property
    Public Property CourierName() As String
        Get
            Return _CourierName
        End Get
        Set(ByVal Value As String)
            _CourierName = Value
        End Set
    End Property
    Public Property AssetDocRack() As String
        Get
            Return _AssetDocRack
        End Get
        Set(ByVal Value As String)
            _AssetDocRack = Value
        End Set
    End Property
    Public Property AssetDocFilling() As String
        Get
            Return _AssetDocFilling
        End Get
        Set(ByVal Value As String)
            _AssetDocFilling = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return _Notes
        End Get
        Set(ByVal Value As String)
            _Notes = Value
        End Set
    End Property
    Public Property IsPosted() As Boolean
        Get
            Return _IsPosted
        End Get
        Set(ByVal Value As Boolean)
            _IsPosted = Value
        End Set
    End Property
    Public Property PostedDate() As DateTime
        Get
            Return _PostedDate
        End Get
        Set(ByVal Value As DateTime)
            _PostedDate = Value
        End Set
    End Property
    Public Property ApplicationBranchOwner() As String
        Get
            Return _ApplicationBranchOwner
        End Get
        Set(ByVal Value As String)
            _ApplicationBranchOwner = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return _ApplicationID
        End Get
        Set(ByVal Value As String)
            _ApplicationID = Value
        End Set
    End Property
    Public Property AssetSeqNo() As Integer
        Get
            Return _AssetSeqNo
        End Get
        Set(ByVal Value As Integer)
            _AssetSeqNo = Value
        End Set
    End Property
    Public Property AssetTypeID() As String
        Get
            Return _AssetTypeID
        End Get
        Set(ByVal Value As String)
            _AssetTypeID = Value
        End Set
    End Property
    Public Property AssetDocID() As String
        Get
            Return _AssetDocID
        End Get
        Set(ByVal Value As String)
            _AssetDocID = Value
        End Set
    End Property
    Public Property DocumentNo() As String
        Get
            Return _DocumentNo
        End Get
        Set(ByVal Value As String)
            _DocumentNo = Value
        End Set
    End Property
    Public Property DocumentDate() As DateTime
        Get
            Return _DocumentDate
        End Get
        Set(ByVal Value As DateTime)
            _DocumentDate = Value
        End Set
    End Property
    Public Property AssetValidatorType() As String
        Get
            Return _AssetValidatorType
        End Get
        Set(ByVal Value As String)
            _AssetValidatorType = Value
        End Set
    End Property
    Public Property AssetValidatorData() As String
        Get
            Return _AssetValidatorData
        End Get
        Set(ByVal Value As String)
            _AssetValidatorData = Value
        End Set
    End Property
    Public Property AssetValidatorTypeDesc() As String
        Get
            Return _AssetValidatorTypeDesc
        End Get
        Set(ByVal Value As String)
            _AssetValidatorTypeDesc = Value
        End Set
    End Property
    Public Property LastActionResult() As Int64
        Get
            Return _LastActionResult
        End Get
        Set(ByVal Value As Int64)
            _LastActionResult = Value
        End Set
    End Property
    Public Property LastActionResultMessage() As String
        Get
            Return _LastActionResultMessage
        End Get
        Set(ByVal Value As String)
            _LastActionResultMessage = Value
        End Set
    End Property
    Public Property ActionTakenParam() As String
        Get
            Return _ActionTakenParam
        End Get
        Set(ByVal Value As String)
            _ActionTakenParam = Value
        End Set
    End Property
    Public Property AssetDocRackList() As DataTable
        Get
            Return _AssetDocRackList
        End Get
        Set(ByVal Value As DataTable)
            _AssetDocRackList = Value
        End Set
    End Property
    Public Property AssetDocFillingList() As DataTable
        Get
            Return _AssetDocFillingList
        End Get
        Set(ByVal Value As DataTable)
            _AssetDocFillingList = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return _CustomerName
        End Get
        Set(ByVal Value As String)
            _CustomerName = Value
        End Set
    End Property
    Public Property AssetDesc() As String
        Get
            Return _AssetDesc
        End Get
        Set(ByVal Value As String)
            _AssetDesc = Value
        End Set
    End Property
    Public Property DocDesc() As String
        Get
            Return _DocDesc
        End Get
        Set(ByVal Value As String)
            _DocDesc = Value
        End Set
    End Property
    Public Property SerialNoData() As String()
        Get
            Return _SerialNoData
        End Get
        Set(ByVal Value As String())
            _SerialNoData = Value
        End Set
    End Property
    Public Property SerialNoDesc() As String()
        Get
            Return _SerialNoDesc
        End Get
        Set(ByVal Value As String())
            _SerialNoDesc = Value
        End Set
    End Property
#End Region

End Class
