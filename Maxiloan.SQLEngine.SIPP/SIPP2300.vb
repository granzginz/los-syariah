﻿

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class SIPP2300 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP2300List"
    Private Const SaveAdd As String = "spSIPP2300SaveAdd"
    Private Const SaveEdit As String = "spSIPP2300SaveEdit"
    Private Const LIST_EDIT As String = "spSIPP2300EditList"
    Private Const spFillCbo As String = "spGetCboSIPP"
    Private Const Delete As String = "spSIPP2300Delete"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"

#End Region

    Public Function GetSIPP2300(ByVal oCustomClass As Parameter.SIPP2300) As Parameter.SIPP2300
        Dim oReturnValue As New Parameter.SIPP2300
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0010.GetSIPP0010")
        End Try
    End Function

    Public Function SIPP2300Delete(ByVal oCustomClass As Parameter.SIPP2300) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.Id
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, Delete, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

    Public Function SIPP2300SaveEdit(ocustomClass As Parameter.SIPP2300) As Parameter.SIPP2300
        Dim params(13) As SqlParameter
        Try
            params(0) = New SqlParameter("GOL_PERUSAHAAN", SqlDbType.VarChar, 100)
            params(0).Value = ocustomClass.GOL_PERUSAHAAN
        params(1) = New SqlParameter("STATUS_KETERKAITAN", SqlDbType.VarChar, 100)
        params(1).Value = ocustomClass.STATUS_KETERKAITAN
        params(2) = New SqlParameter("NEGARA", SqlDbType.VarChar, 100)
        params(2).Value = ocustomClass.NEGARA
        params(3) = New SqlParameter("JNS_MATA_UANG", SqlDbType.VarChar, 100)
        params(3).Value = ocustomClass.JNS_MATA_UANG
        params(4) = New SqlParameter("KUALITAS", SqlDbType.VarChar, 100)
        params(4).Value = ocustomClass.KUALITAS
        params(5) = New SqlParameter("NM_PERUSAHAAN", SqlDbType.VarChar, 100)
        params(5).Value = ocustomClass.NM_PERUSAHAAN
        params(6) = New SqlParameter("TGL_MULAI", SqlDbType.VarChar, 100)
        params(6).Value = ocustomClass.TGL_MULAI
        params(7) = New SqlParameter("PRSN_BGN_PNYRTN", SqlDbType.VarChar, 100)
        params(7).Value = ocustomClass.PRSN_BGN_PNYRTN
        params(8) = New SqlParameter("NILAI_PNYRTN_AWL_DLM_MUA", SqlDbType.VarChar, 100)
        params(8).Value = ocustomClass.NILAI_PNYRTN_AWL_DLM_MUA
        params(9) = New SqlParameter("NILAI_AWL_PNYRTN", SqlDbType.VarChar, 100)
        params(9).Value = ocustomClass.NILAI_AWL_PNYRTN
        params(10) = New SqlParameter("NILAI_AWL_PNYRTN_MDL_MUA", SqlDbType.VarChar, 100)
        params(10).Value = ocustomClass.NILAI_AWL_PNYRTN_MDL_MUA
        params(11) = New SqlParameter("NILAI_PNYRTN_MODAL", SqlDbType.VarChar, 100)
        params(11).Value = ocustomClass.NILAI_PNYRTN_MODAL
        params(12) = New SqlParameter("@ID", SqlDbType.Int)
        params(12).Value = ocustomClass.ID
        params(13) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(13).Direction = ParameterDirection.Output


        SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SaveEdit, params)
        Catch ex As Exception
        Throw New Exception("Error On DataAccess.SIPP.SIPP2300.SIPP2300SaveEdit")
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP2300) As Parameter.SIPP2300
        Dim oReturnValue As New Parameter.SIPP2300
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2300.GetCboBulandataSIPP")
        End Try
    End Function
    Public Function SIPP2300SaveAdd(ocustomClass As Parameter.SIPP2300) As Parameter.SIPP2300
        Dim params(12) As SqlParameter
        Try
            params(0) = New SqlParameter("GOL_PERUSAHAAN", SqlDbType.VarChar, 100)
            params(0).Value = ocustomClass.GOL_PERUSAHAAN
            params(1) = New SqlParameter("STATUS_KETERKAITAN", SqlDbType.VarChar, 100)
            params(1).Value = ocustomClass.STATUS_KETERKAITAN
            params(2) = New SqlParameter("NEGARA", SqlDbType.VarChar, 100)
            params(2).Value = ocustomClass.NEGARA
            params(3) = New SqlParameter("JNS_MATA_UANG", SqlDbType.VarChar, 100)
            params(3).Value = ocustomClass.JNS_MATA_UANG
            params(4) = New SqlParameter("KUALITAS", SqlDbType.VarChar, 100)
            params(4).Value = ocustomClass.KUALITAS
            params(5) = New SqlParameter("NM_PERUSAHAAN", SqlDbType.VarChar, 100)
            params(5).Value = ocustomClass.NM_PERUSAHAAN
            params(6) = New SqlParameter("TGL_MULAI", SqlDbType.VarChar, 100)
            params(6).Value = ocustomClass.TGL_MULAI
            params(7) = New SqlParameter("PRSN_BGN_PNYRTN", SqlDbType.VarChar, 100)
            params(7).Value = ocustomClass.PRSN_BGN_PNYRTN
            params(8) = New SqlParameter("NILAI_PNYRTN_AWL_DLM_MUA", SqlDbType.VarChar, 100)
            params(8).Value = ocustomClass.NILAI_PNYRTN_AWL_DLM_MUA
            params(9) = New SqlParameter("NILAI_AWL_PNYRTN", SqlDbType.VarChar, 100)
            params(9).Value = ocustomClass.NILAI_AWL_PNYRTN
            params(10) = New SqlParameter("NILAI_AWL_PNYRTN_MDL_MUA", SqlDbType.VarChar, 100)
            params(10).Value = ocustomClass.NILAI_AWL_PNYRTN_MDL_MUA
            params(11) = New SqlParameter("NILAI_PNYRTN_MODAL", SqlDbType.VarChar, 100)
            params(11).Value = ocustomClass.NILAI_PNYRTN_MODAL
            params(12) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 100)
            params(12).Value = ocustomClass.BULANDATA

            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, SaveAdd, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception

        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSIPP2300Edit(ByVal ocustomClass As Parameter.SIPP2300) As Parameter.SIPP2300
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP2300


        params(0) = New SqlParameter("ID", SqlDbType.VarChar, 100)
        params(0).Value = ocustomClass.Id

        Try
            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2300.GetSIPP2300Edit")
        End Try
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP2300) As Parameter.SIPP2300
        Dim oReturnValue As New Parameter.SIPP2300
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@valueparent", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.valueparent
        params(1) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.valuemaster
        params(2) = New SqlParameter("@valueelement", SqlDbType.VarChar, 20)
        params(2).Value = oCustomClass.valueelement

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.D01.GetCbo")
        End Try
    End Function

    'Public Function Generate(ByVal oCustomClass As Parameter.D01) As Parameter.D01
    '    Dim oReturnValue As New Parameter.D01

    '    'Dim params(0) As SqlParameter
    '    'params(0) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
    '    'params(0).Value = oCustomClass.CIF
    '    Try
    '        oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, Generate_TEXT).Tables(0)

    '        Return oReturnValue
    '    Catch exp As Exception
    '        Throw New Exception("Error On DataAccess.SLIK.D01.Generate")
    '    End Try
    'End Function

End Class


