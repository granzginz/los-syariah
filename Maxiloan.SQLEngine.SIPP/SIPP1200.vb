﻿

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class SIPP1200 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP1200List"
    Private Const SaveAdd As String = "spSIPP1200SaveAdd"
    Private Const SaveEdit As String = "spSIPP1200SaveEdit"
    Private Const LIST_EDIT As String = "spSIPP1200EditList"
    Private Const spFillCbo As String = "spGetCboSIPP"
    Private Const Delete As String = "spSIPP1200Delete"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"

#End Region

    Public Function GetSIPP1200List(ByVal oCustomClass As Parameter.SIPP1200) As Parameter.SIPP1200
        Dim oReturnValue As New Parameter.SIPP1200
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP1200.GetSIPP1200")
        End Try
    End Function

    Public Function GetSIPP1200Edit(ocustomClass As Parameter.SIPP1200) As Parameter.SIPP1200
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP1200


        params(0) = New SqlParameter("ID", SqlDbType.VarChar, 100)
        params(0).Value = ocustomClass.ID

        Try
            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.D01.GetD01Edit")
        End Try
    End Function

    Public Function SIPP1200SaveEdit(ocustomClass As Parameter.SIPP1200) As Parameter.SIPP1200
        Dim params(250) As SqlParameter
        Try
            params(0) = New SqlParameter("@ID", SqlDbType.Int)
            params(0).Value = ocustomClass.ID
            params(1) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_TTL", SqlDbType.Int)
            params(1).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_TTL
            params(2) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_NDNSN_RPH", SqlDbType.Int)
            params(2).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_NDNSN_RPH
            params(3) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_MT_NG_SNG", SqlDbType.Int)
            params(3).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_MT_NG_SNG
            params(4) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_TTL", SqlDbType.Int)
            params(4).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_TTL
            params(5) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH", SqlDbType.Int)
            params(5).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH
            params(6) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG", SqlDbType.Int)
            params(6).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG
            params(7) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_TTL", SqlDbType.Int)
            params(7).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_TTL
            params(8) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_NDNSN_RPH", SqlDbType.Int)
            params(8).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_NDNSN_RPH
            params(9) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_MT_NG_SNG", SqlDbType.Int)
            params(9).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_MT_NG_SNG
            params(10) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL", SqlDbType.Int)
            params(10).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL
            params(11) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH", SqlDbType.Int)
            params(11).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH
            params(12) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG", SqlDbType.Int)
            params(12).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG
            params(13) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_TTL", SqlDbType.Int)
            params(13).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_TTL
            params(14) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_NDNSN_RPH", SqlDbType.Int)
            params(14).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_NDNSN_RPH
            params(15) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_MT_NG_SNG", SqlDbType.Int)
            params(15).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_MT_NG_SNG
            params(16) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_TTL", SqlDbType.Int)
            params(16).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_TTL
            params(17) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_NDNSN_RPH", SqlDbType.Int)
            params(17).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_NDNSN_RPH
            params(18) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_MT_NG_SNG", SqlDbType.Int)
            params(18).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_MT_NG_SNG
            params(19) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_TTL", SqlDbType.Int)
            params(19).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_TTL
            params(20) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH", SqlDbType.Int)
            params(20).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH
            params(21) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG", SqlDbType.Int)
            params(21).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG
            params(22) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_TTL", SqlDbType.Int)
            params(22).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_TTL
            params(23) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_NDNSN_RPH", SqlDbType.Int)
            params(23).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_NDNSN_RPH
            params(24) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_MT_NG_SNG", SqlDbType.Int)
            params(24).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_MT_NG_SNG
            params(25) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_TTL", SqlDbType.Int)
            params(25).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_TTL
            params(26) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH", SqlDbType.Int)
            params(26).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH
            params(27) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG", SqlDbType.Int)
            params(27).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG
            params(28) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_TTL", SqlDbType.Int)
            params(28).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_TTL
            params(29) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_NDNSN_RPH", SqlDbType.Int)
            params(29).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_NDNSN_RPH
            params(30) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_MT_NG_SNG", SqlDbType.Int)
            params(30).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_MT_NG_SNG
            params(31) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_TTL", SqlDbType.Int)
            params(31).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_TTL
            params(32) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_NDNSN_RPH", SqlDbType.Int)
            params(32).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_NDNSN_RPH
            params(33) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_MT_NG_SNG", SqlDbType.Int)
            params(33).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_MT_NG_SNG
            params(34) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_TTL", SqlDbType.Int)
            params(34).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_TTL
            params(35) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_NDNSN_RPH", SqlDbType.Int)
            params(35).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_NDNSN_RPH
            params(36) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_MT_NG_SNG", SqlDbType.Int)
            params(36).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_MT_NG_SNG
            params(37) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_TTL", SqlDbType.Int)
            params(37).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_TTL
            params(38) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH", SqlDbType.Int)
            params(38).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH
            params(39) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG", SqlDbType.Int)
            params(39).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG
            params(40) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_TTL", SqlDbType.Int)
            params(40).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_TTL
            params(41) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_NDNSN_RPH", SqlDbType.Int)
            params(41).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_NDNSN_RPH
            params(42) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_MT_NG_SNG", SqlDbType.Int)
            params(42).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_MT_NG_SNG
            params(43) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_TTL", SqlDbType.Int)
            params(43).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_TTL
            params(44) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_NDNSN_RPH", SqlDbType.Int)
            params(44).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_NDNSN_RPH
            params(45) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_MT_NG_SNG", SqlDbType.Int)
            params(45).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_MT_NG_SNG
            params(46) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL", SqlDbType.Int)
            params(46).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL
            params(47) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH", SqlDbType.Int)
            params(47).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH
            params(48) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG", SqlDbType.Int)
            params(48).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG
            params(49) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_TTL", SqlDbType.Int)
            params(49).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_TTL
            params(50) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH", SqlDbType.Int)
            params(50).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH
            params(51) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG", SqlDbType.Int)
            params(51).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG
            params(52) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_TTL", SqlDbType.Int)
            params(52).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_TTL
            params(53) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_NDNSN_RPH", SqlDbType.Int)
            params(53).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_NDNSN_RPH
            params(54) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_MT_NG_SNG", SqlDbType.Int)
            params(54).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_MT_NG_SNG
            params(55) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL", SqlDbType.Int)
            params(55).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL
            params(56) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH", SqlDbType.Int)
            params(56).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH
            params(57) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG", SqlDbType.Int)
            params(57).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG
            params(58) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_TTL", SqlDbType.Int)
            params(58).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_TTL
            params(59) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_NDNSN_RPH", SqlDbType.Int)
            params(59).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_NDNSN_RPH
            params(60) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_MT_NG_SNG", SqlDbType.Int)
            params(60).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_MT_NG_SNG
            params(61) = New SqlParameter("@PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL", SqlDbType.Int)
            params(61).Value = ocustomClass.PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL
            params(62) = New SqlParameter("@PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH", SqlDbType.Int)
            params(62).Value = ocustomClass.PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH
            params(63) = New SqlParameter("@PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG", SqlDbType.Int)
            params(63).Value = ocustomClass.PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG
            params(64) = New SqlParameter("@PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL", SqlDbType.Int)
            params(64).Value = ocustomClass.PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL
            params(65) = New SqlParameter("@PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH", SqlDbType.Int)
            params(65).Value = ocustomClass.PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH
            params(66) = New SqlParameter("@PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG", SqlDbType.Int)
            params(66).Value = ocustomClass.PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG
            params(67) = New SqlParameter("@PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL", SqlDbType.Int)
            params(67).Value = ocustomClass.PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL
            params(68) = New SqlParameter("@PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH", SqlDbType.Int)
            params(68).Value = ocustomClass.PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH
            params(69) = New SqlParameter("@PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG", SqlDbType.Int)
            params(69).Value = ocustomClass.PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG
            params(70) = New SqlParameter("@PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_TTL", SqlDbType.Int)
            params(70).Value = ocustomClass.PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_TTL
            params(71) = New SqlParameter("@PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH", SqlDbType.Int)
            params(71).Value = ocustomClass.PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH
            params(72) = New SqlParameter("@PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG", SqlDbType.Int)
            params(72).Value = ocustomClass.PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG
            params(73) = New SqlParameter("@PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL", SqlDbType.Int)
            params(73).Value = ocustomClass.PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL
            params(74) = New SqlParameter("@PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH", SqlDbType.Int)
            params(74).Value = ocustomClass.PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH
            params(75) = New SqlParameter("@PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG", SqlDbType.Int)
            params(75).Value = ocustomClass.PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG
            params(76) = New SqlParameter("@PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_TTL", SqlDbType.Int)
            params(76).Value = ocustomClass.PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_TTL
            params(77) = New SqlParameter("@PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_NDNSN_RPH", SqlDbType.Int)
            params(77).Value = ocustomClass.PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_NDNSN_RPH
            params(78) = New SqlParameter("@PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_MT_NG_SNG", SqlDbType.Int)
            params(78).Value = ocustomClass.PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_MT_NG_SNG
            params(79) = New SqlParameter("@PNDPTN_DMNSTRS_TTL", SqlDbType.Int)
            params(79).Value = ocustomClass.PNDPTN_DMNSTRS_TTL
            params(80) = New SqlParameter("@PNDPTN_DMNSTRS_NDNSN_RPH", SqlDbType.Int)
            params(80).Value = ocustomClass.PNDPTN_DMNSTRS_NDNSN_RPH
            params(81) = New SqlParameter("@PNDPTN_DMNSTRS_MT_NG_SNG", SqlDbType.Int)
            params(81).Value = ocustomClass.PNDPTN_DMNSTRS_MT_NG_SNG
            params(82) = New SqlParameter("@PNDPTN_PRVS_TTL", SqlDbType.Int)
            params(82).Value = ocustomClass.PNDPTN_PRVS_TTL
            params(83) = New SqlParameter("@PNDPTN_PRVS_NDNSN_RPH", SqlDbType.Int)
            params(83).Value = ocustomClass.PNDPTN_PRVS_NDNSN_RPH
            params(84) = New SqlParameter("@PNDPTN_PRVS_MT_NG_SNG", SqlDbType.Int)
            params(84).Value = ocustomClass.PNDPTN_PRVS_MT_NG_SNG
            params(85) = New SqlParameter("@PNDPTN_DND_TTL", SqlDbType.Int)
            params(85).Value = ocustomClass.PNDPTN_DND_TTL
            params(86) = New SqlParameter("@PNDPTN_DND_NDNSN_RPH", SqlDbType.Int)
            params(86).Value = ocustomClass.PNDPTN_DND_NDNSN_RPH
            params(87) = New SqlParameter("@PNDPTN_DND_MT_NG_SNG", SqlDbType.Int)
            params(87).Value = ocustomClass.PNDPTN_DND_MT_NG_SNG
            params(88) = New SqlParameter("@PNDPTN_DSKN_SRNS_TTL", SqlDbType.Int)
            params(88).Value = ocustomClass.PNDPTN_DSKN_SRNS_TTL
            params(89) = New SqlParameter("@PNDPTN_DSKN_SRNS_NDNSN_RPH", SqlDbType.Int)
            params(89).Value = ocustomClass.PNDPTN_DSKN_SRNS_NDNSN_RPH
            params(90) = New SqlParameter("@PNDPTN_DSKN_SRNS_MT_NG_SNG", SqlDbType.Int)
            params(90).Value = ocustomClass.PNDPTN_DSKN_SRNS_MT_NG_SNG
            params(91) = New SqlParameter("@PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_TTL", SqlDbType.Int)
            params(91).Value = ocustomClass.PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_TTL
            params(92) = New SqlParameter("@PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_NDNSN_RPH", SqlDbType.Int)
            params(92).Value = ocustomClass.PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_NDNSN_RPH
            params(93) = New SqlParameter("@PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_MT_NG_SNG", SqlDbType.Int)
            params(93).Value = ocustomClass.PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_MT_NG_SNG
            params(94) = New SqlParameter("@PNDPTN_PRSNL_LN_TRKT_PMBYN_TTL", SqlDbType.Int)
            params(94).Value = ocustomClass.PNDPTN_PRSNL_LN_TRKT_PMBYN_TTL
            params(95) = New SqlParameter("@PNDPTN_PRSNL_LN_TRKT_PMBYN_NDNSN_RPH", SqlDbType.Int)
            params(95).Value = ocustomClass.PNDPTN_PRSNL_LN_TRKT_PMBYN_NDNSN_RPH
            params(96) = New SqlParameter("@PNDPTN_PRSNL_LN_TRKT_PMBYN_MT_NG_SNG", SqlDbType.Int)
            params(96).Value = ocustomClass.PNDPTN_PRSNL_LN_TRKT_PMBYN_MT_NG_SNG
            params(97) = New SqlParameter("@PNDPTN_DR_SW_PRS_PRTNG_LS_TTL", SqlDbType.Int)
            params(97).Value = ocustomClass.PNDPTN_DR_SW_PRS_PRTNG_LS_TTL
            params(98) = New SqlParameter("@PNDPTN_DR_SW_PRS_PRTNG_LS_NDNSN_RPH", SqlDbType.Int)
            params(98).Value = ocustomClass.PNDPTN_DR_SW_PRS_PRTNG_LS_NDNSN_RPH
            params(99) = New SqlParameter("@PNDPTN_DR_SW_PRS_PRTNG_LS_MT_NG_SNG", SqlDbType.Int)
            params(99).Value = ocustomClass.PNDPTN_DR_SW_PRS_PRTNG_LS_MT_NG_SNG
            params(100) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_RKSDN_TTL", SqlDbType.Int)
            params(100).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_RKSDN_TTL
            params(101) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_RKSDN_NDNSN_RPH", SqlDbType.Int)
            params(101).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_RKSDN_NDNSN_RPH
            params(102) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_RKSDN_MT_NG_SNG", SqlDbType.Int)
            params(102).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_RKSDN_MT_NG_SNG
            params(103) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_SRNS_TTL", SqlDbType.Int)
            params(103).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_SRNS_TTL
            params(104) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_SRNS_NDNSN_RPH", SqlDbType.Int)
            params(104).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_SRNS_NDNSN_RPH
            params(105) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_SRNS_MT_NG_SNG", SqlDbType.Int)
            params(105).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_SRNS_MT_NG_SNG
            params(106) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_LNNY_TTL", SqlDbType.Int)
            params(106).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_LNNY_TTL
            params(107) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_LNNY_NDNSN_RPH", SqlDbType.Int)
            params(107).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_LNNY_NDNSN_RPH
            params(108) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_LNNY_MT_NG_SNG", SqlDbType.Int)
            params(108).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_LNNY_MT_NG_SNG
            params(109) = New SqlParameter("@PNDPTN_DR_KGTN_BRBSS_F_TTL", SqlDbType.Int)
            params(109).Value = ocustomClass.PNDPTN_DR_KGTN_BRBSS_F_TTL
            params(110) = New SqlParameter("@PNDPTN_DR_KGTN_BRBSS_F_NDNSN_RPH", SqlDbType.Int)
            params(110).Value = ocustomClass.PNDPTN_DR_KGTN_BRBSS_F_NDNSN_RPH
            params(111) = New SqlParameter("@PNDPTN_DR_KGTN_BRBSS_F_MT_NG_SNG", SqlDbType.Int)
            params(111).Value = ocustomClass.PNDPTN_DR_KGTN_BRBSS_F_MT_NG_SNG
            params(112) = New SqlParameter("@PNDPTN_PRSNL_LNNY_LNNY_TTL", SqlDbType.Int)
            params(112).Value = ocustomClass.PNDPTN_PRSNL_LNNY_LNNY_TTL
            params(113) = New SqlParameter("@PNDPTN_PRSNL_LNNY_LNNY_NDNSN_RPH", SqlDbType.Int)
            params(113).Value = ocustomClass.PNDPTN_PRSNL_LNNY_LNNY_NDNSN_RPH
            params(114) = New SqlParameter("@PNDPTN_PRSNL_LNNY_LNNY_MT_NG_SNG", SqlDbType.Int)
            params(114).Value = ocustomClass.PNDPTN_PRSNL_LNNY_LNNY_MT_NG_SNG
            params(115) = New SqlParameter("@PNDPTN_PRSNL_LNNY_TTL", SqlDbType.Int)
            params(115).Value = ocustomClass.PNDPTN_PRSNL_LNNY_TTL
            params(116) = New SqlParameter("@PNDPTN_PRSNL_LNNY_NDNSN_RPH", SqlDbType.Int)
            params(116).Value = ocustomClass.PNDPTN_PRSNL_LNNY_NDNSN_RPH
            params(117) = New SqlParameter("@PNDPTN_PRSNL_LNNY_MT_NG_SNG", SqlDbType.Int)
            params(117).Value = ocustomClass.PNDPTN_PRSNL_LNNY_MT_NG_SNG
            params(118) = New SqlParameter("@PNDPTN_PRSNL_TTL", SqlDbType.Int)
            params(118).Value = ocustomClass.PNDPTN_PRSNL_TTL
            params(119) = New SqlParameter("@PNDPTN_PRSNL_NDNSN_RPH", SqlDbType.Int)
            params(119).Value = ocustomClass.PNDPTN_PRSNL_NDNSN_RPH
            params(120) = New SqlParameter("@PNDPTN_PRSNL_MT_NG_SNG", SqlDbType.Int)
            params(120).Value = ocustomClass.PNDPTN_PRSNL_MT_NG_SNG
            params(121) = New SqlParameter("@PNDPTN_BNG_NN_PRSNL_TTL", SqlDbType.Int)
            params(121).Value = ocustomClass.PNDPTN_BNG_NN_PRSNL_TTL
            params(122) = New SqlParameter("@PNDPTN_BNG_NN_PRSNL_NDNSN_RPH", SqlDbType.Int)
            params(122).Value = ocustomClass.PNDPTN_BNG_NN_PRSNL_NDNSN_RPH
            params(123) = New SqlParameter("@PNDPTN_BNG_NN_PRSNL_MT_NG_SNG", SqlDbType.Int)
            params(123).Value = ocustomClass.PNDPTN_BNG_NN_PRSNL_MT_NG_SNG
            params(124) = New SqlParameter("@PNDPTN_NN_PRSNL_LNNY_TTL", SqlDbType.Int)
            params(124).Value = ocustomClass.PNDPTN_NN_PRSNL_LNNY_TTL
            params(125) = New SqlParameter("@PNDPTN_NN_PRSNL_LNNY_NDNSN_RPH", SqlDbType.Int)
            params(125).Value = ocustomClass.PNDPTN_NN_PRSNL_LNNY_NDNSN_RPH
            params(126) = New SqlParameter("@PNDPTN_NN_PRSNL_LNNY_MT_NG_SNG", SqlDbType.Int)
            params(126).Value = ocustomClass.PNDPTN_NN_PRSNL_LNNY_MT_NG_SNG
            params(127) = New SqlParameter("@PNDPTN_NN_PRSNL_TTL", SqlDbType.Int)
            params(127).Value = ocustomClass.PNDPTN_NN_PRSNL_TTL
            params(128) = New SqlParameter("@PNDPTN_NN_PRSNL_NDNSN_RPH", SqlDbType.Int)
            params(128).Value = ocustomClass.PNDPTN_NN_PRSNL_NDNSN_RPH
            params(129) = New SqlParameter("@PNDPTN_NN_PRSNL_MT_NG_SNG", SqlDbType.Int)
            params(129).Value = ocustomClass.PNDPTN_NN_PRSNL_MT_NG_SNG
            params(130) = New SqlParameter("@PNDPTN_TTL", SqlDbType.Int)
            params(130).Value = ocustomClass.PNDPTN_TTL
            params(131) = New SqlParameter("@PNDPTN_NDNSN_RPH", SqlDbType.Int)
            params(131).Value = ocustomClass.PNDPTN_NDNSN_RPH
            params(132) = New SqlParameter("@PNDPTN_MT_NG_SNG", SqlDbType.Int)
            params(132).Value = ocustomClass.PNDPTN_MT_NG_SNG
            params(133) = New SqlParameter("@BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_TTL", SqlDbType.Int)
            params(133).Value = ocustomClass.BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_TTL
            params(134) = New SqlParameter("@BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_NDNSN_RPH", SqlDbType.Int)
            params(134).Value = ocustomClass.BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_NDNSN_RPH
            params(135) = New SqlParameter("@BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_MT_NG_SNG", SqlDbType.Int)
            params(135).Value = ocustomClass.BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_MT_NG_SNG
            params(136) = New SqlParameter("@BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_TTL", SqlDbType.Int)
            params(136).Value = ocustomClass.BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_TTL
            params(137) = New SqlParameter("@BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH", SqlDbType.Int)
            params(137).Value = ocustomClass.BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH
            params(138) = New SqlParameter("@BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG", SqlDbType.Int)
            params(138).Value = ocustomClass.BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG
            params(139) = New SqlParameter("@BBN_BNG_PRSNL_TTL", SqlDbType.Int)
            params(139).Value = ocustomClass.BBN_BNG_PRSNL_TTL
            params(140) = New SqlParameter("@BBN_BNG_PRSNL_NDNSN_RPH", SqlDbType.Int)
            params(140).Value = ocustomClass.BBN_BNG_PRSNL_NDNSN_RPH
            params(141) = New SqlParameter("@BBN_BNG_PRSNL_MT_NG_SNG", SqlDbType.Int)
            params(141).Value = ocustomClass.BBN_BNG_PRSNL_MT_NG_SNG
            params(142) = New SqlParameter("@BBN_PRM_TS_TRNSKS_SWP_TTL", SqlDbType.Int)
            params(142).Value = ocustomClass.BBN_PRM_TS_TRNSKS_SWP_TTL
            params(143) = New SqlParameter("@BBN_PRM_TS_TRNSKS_SWP_NDNSN_RPH", SqlDbType.Int)
            params(143).Value = ocustomClass.BBN_PRM_TS_TRNSKS_SWP_NDNSN_RPH
            params(144) = New SqlParameter("@BBN_PRM_TS_TRNSKS_SWP_MT_NG_SNG", SqlDbType.Int)
            params(144).Value = ocustomClass.BBN_PRM_TS_TRNSKS_SWP_MT_NG_SNG
            params(145) = New SqlParameter("@BBN_PRM_SRNS_TTL", SqlDbType.Int)
            params(145).Value = ocustomClass.BBN_PRM_SRNS_TTL
            params(146) = New SqlParameter("@BBN_PRM_SRNS_NDNSN_RPH", SqlDbType.Int)
            params(146).Value = ocustomClass.BBN_PRM_SRNS_NDNSN_RPH
            params(147) = New SqlParameter("@BBN_PRM_SRNS_MT_NG_SNG", SqlDbType.Int)
            params(147).Value = ocustomClass.BBN_PRM_SRNS_MT_NG_SNG
            params(148) = New SqlParameter("@BBN_GJ_PH_DN_TNJNGN_TTL", SqlDbType.Int)
            params(148).Value = ocustomClass.BBN_GJ_PH_DN_TNJNGN_TTL
            params(149) = New SqlParameter("@BBN_GJ_PH_DN_TNJNGN_NDNSN_RPH", SqlDbType.Int)
            params(149).Value = ocustomClass.BBN_GJ_PH_DN_TNJNGN_NDNSN_RPH
            params(150) = New SqlParameter("@BBN_GJ_PH_DN_TNJNGN_MT_NG_SNG", SqlDbType.Int)
            params(150).Value = ocustomClass.BBN_GJ_PH_DN_TNJNGN_MT_NG_SNG
            params(151) = New SqlParameter("@BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_TTL", SqlDbType.Int)
            params(151).Value = ocustomClass.BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_TTL
            params(152) = New SqlParameter("@BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_NDNSN_RPH", SqlDbType.Int)
            params(152).Value = ocustomClass.BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_NDNSN_RPH
            params(153) = New SqlParameter("@BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_MT_NG_SNG", SqlDbType.Int)
            params(153).Value = ocustomClass.BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_MT_NG_SNG
            params(154) = New SqlParameter("@BBN_TNG_KRJ_LNNY_TTL", SqlDbType.Int)
            params(154).Value = ocustomClass.BBN_TNG_KRJ_LNNY_TTL
            params(155) = New SqlParameter("@BBN_TNG_KRJ_LNNY_NDNSN_RPH", SqlDbType.Int)
            params(155).Value = ocustomClass.BBN_TNG_KRJ_LNNY_NDNSN_RPH
            params(156) = New SqlParameter("@BBN_TNG_KRJ_LNNY_MT_NG_SNG", SqlDbType.Int)
            params(156).Value = ocustomClass.BBN_TNG_KRJ_LNNY_MT_NG_SNG
            params(157) = New SqlParameter("@BBN_TNG_KRJ_TTL", SqlDbType.Int)
            params(157).Value = ocustomClass.BBN_TNG_KRJ_TTL
            params(158) = New SqlParameter("@BBN_TNG_KRJ_NDNSN_RPH", SqlDbType.Int)
            params(158).Value = ocustomClass.BBN_TNG_KRJ_NDNSN_RPH
            params(159) = New SqlParameter("@BBN_TNG_KRJ_MT_NG_SNG", SqlDbType.Int)
            params(159).Value = ocustomClass.BBN_TNG_KRJ_MT_NG_SNG
            params(160) = New SqlParameter("@BBN_NSNTF_PHK_KTG_TTL", SqlDbType.Int)
            params(160).Value = ocustomClass.BBN_NSNTF_PHK_KTG_TTL
            params(161) = New SqlParameter("@BBN_NSNTF_PHK_KTG_NDNSN_RPH", SqlDbType.Int)
            params(161).Value = ocustomClass.BBN_NSNTF_PHK_KTG_NDNSN_RPH
            params(162) = New SqlParameter("@BBN_NSNTF_PHK_KTG_MT_NG_SNG", SqlDbType.Int)
            params(162).Value = ocustomClass.BBN_NSNTF_PHK_KTG_MT_NG_SNG
            params(163) = New SqlParameter("@BBN_PMSRN_LNNY_TTL", SqlDbType.Int)
            params(163).Value = ocustomClass.BBN_PMSRN_LNNY_TTL
            params(164) = New SqlParameter("@BBN_PMSRN_LNNY_NDNSN_RPH", SqlDbType.Int)
            params(164).Value = ocustomClass.BBN_PMSRN_LNNY_NDNSN_RPH
            params(165) = New SqlParameter("@BBN_PMSRN_LNNY_MT_NG_SNG", SqlDbType.Int)
            params(165).Value = ocustomClass.BBN_PMSRN_LNNY_MT_NG_SNG
            params(166) = New SqlParameter("@BBN_PMSRN_TTL", SqlDbType.Int)
            params(166).Value = ocustomClass.BBN_PMSRN_TTL
            params(167) = New SqlParameter("@BBN_PMSRN_NDNSN_RPH", SqlDbType.Int)
            params(167).Value = ocustomClass.BBN_PMSRN_NDNSN_RPH
            params(168) = New SqlParameter("@BBN_PMSRN_MT_NG_SNG", SqlDbType.Int)
            params(168).Value = ocustomClass.BBN_PMSRN_MT_NG_SNG
            params(169) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_TTL", SqlDbType.Int)
            params(169).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_TTL
            params(170) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_NDNSN_RPH", SqlDbType.Int)
            params(170).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_NDNSN_RPH
            params(171) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_MT_NG_SNG", SqlDbType.Int)
            params(171).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_MT_NG_SNG
            params(172) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_TTL", SqlDbType.Int)
            params(172).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_TTL
            params(173) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_NDNSN_RPH", SqlDbType.Int)
            params(173).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_NDNSN_RPH
            params(174) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_MT_NG_SNG", SqlDbType.Int)
            params(174).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_MT_NG_SNG
            params(175) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_TTL", SqlDbType.Int)
            params(175).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_TTL
            params(176) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_NDNSN_RPH", SqlDbType.Int)
            params(176).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_NDNSN_RPH
            params(177) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_MT_NG_SNG", SqlDbType.Int)
            params(177).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_MT_NG_SNG
            params(178) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_TTL", SqlDbType.Int)
            params(178).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_TTL
            params(179) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH", SqlDbType.Int)
            params(179).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH
            params(180) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG", SqlDbType.Int)
            params(180).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG
            params(181) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_TTL", SqlDbType.Int)
            params(181).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_TTL
            params(182) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH", SqlDbType.Int)
            params(182).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH
            params(183) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG", SqlDbType.Int)
            params(183).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG
            params(184) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_TTL", SqlDbType.Int)
            params(184).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_TTL
            params(185) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NDNSN_RPH", SqlDbType.Int)
            params(185).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NDNSN_RPH
            params(186) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MT_NG_SNG", SqlDbType.Int)
            params(186).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MT_NG_SNG
            params(187) = New SqlParameter("@BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_TTL", SqlDbType.Int)
            params(187).Value = ocustomClass.BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_TTL
            params(188) = New SqlParameter("@BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_NDNSN_RPH", SqlDbType.Int)
            params(188).Value = ocustomClass.BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_NDNSN_RPH
            params(189) = New SqlParameter("@BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_MT_NG_SNG", SqlDbType.Int)
            params(189).Value = ocustomClass.BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_MT_NG_SNG
            params(190) = New SqlParameter("@BBN_PNYSTN_ST_TTP_DN_NVNTRS_TTL", SqlDbType.Int)
            params(190).Value = ocustomClass.BBN_PNYSTN_ST_TTP_DN_NVNTRS_TTL
            params(191) = New SqlParameter("@BBN_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH", SqlDbType.Int)
            params(191).Value = ocustomClass.BBN_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH
            params(192) = New SqlParameter("@BBN_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG", SqlDbType.Int)
            params(192).Value = ocustomClass.BBN_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG
            params(193) = New SqlParameter("@BBN_PNYSHNPNYSTN_TTL", SqlDbType.Int)
            params(193).Value = ocustomClass.BBN_PNYSHNPNYSTN_TTL
            params(194) = New SqlParameter("@BBN_PNYSHNPNYSTN_NDNSN_RPH", SqlDbType.Int)
            params(194).Value = ocustomClass.BBN_PNYSHNPNYSTN_NDNSN_RPH
            params(195) = New SqlParameter("@BBN_PNYSHNPNYSTN_MT_NG_SNG", SqlDbType.Int)
            params(195).Value = ocustomClass.BBN_PNYSHNPNYSTN_MT_NG_SNG
            params(196) = New SqlParameter("@BBN_SW_TTL", SqlDbType.Int)
            params(196).Value = ocustomClass.BBN_SW_TTL
            params(197) = New SqlParameter("@BBN_SW_NDNSN_RPH", SqlDbType.Int)
            params(197).Value = ocustomClass.BBN_SW_NDNSN_RPH
            params(198) = New SqlParameter("@BBN_SW_MT_NG_SNG", SqlDbType.Int)
            params(198).Value = ocustomClass.BBN_SW_MT_NG_SNG
            params(199) = New SqlParameter("@BBN_PMLHRN_DN_PRBKN_TTL", SqlDbType.Int)
            params(199).Value = ocustomClass.BBN_PMLHRN_DN_PRBKN_TTL
            params(200) = New SqlParameter("@BBN_PMLHRN_DN_PRBKN_NDNSN_RPH", SqlDbType.Int)
            params(200).Value = ocustomClass.BBN_PMLHRN_DN_PRBKN_NDNSN_RPH
            params(201) = New SqlParameter("@BBN_PMLHRN_DN_PRBKN_MT_NG_SNG", SqlDbType.Int)
            params(201).Value = ocustomClass.BBN_PMLHRN_DN_PRBKN_MT_NG_SNG
            params(202) = New SqlParameter("@BBN_DMNSTRS_DN_MM_TTL", SqlDbType.Int)
            params(202).Value = ocustomClass.BBN_DMNSTRS_DN_MM_TTL
            params(203) = New SqlParameter("@BBN_DMNSTRS_DN_MM_NDNSN_RPH", SqlDbType.Int)
            params(203).Value = ocustomClass.BBN_DMNSTRS_DN_MM_NDNSN_RPH
            params(204) = New SqlParameter("@BBN_DMNSTRS_DN_MM_MT_NG_SNG", SqlDbType.Int)
            params(204).Value = ocustomClass.BBN_DMNSTRS_DN_MM_MT_NG_SNG
            params(205) = New SqlParameter("@BBN_PRSNL_LNNY_TTL", SqlDbType.Int)
            params(205).Value = ocustomClass.BBN_PRSNL_LNNY_TTL
            params(206) = New SqlParameter("@BBN_PRSNL_LNNY_NDNSN_RPH", SqlDbType.Int)
            params(206).Value = ocustomClass.BBN_PRSNL_LNNY_NDNSN_RPH
            params(207) = New SqlParameter("@BBN_PRSNL_LNNY_MT_NG_SNG", SqlDbType.Int)
            params(207).Value = ocustomClass.BBN_PRSNL_LNNY_MT_NG_SNG
            params(208) = New SqlParameter("@BBN_PRSNL_TTL", SqlDbType.Int)
            params(208).Value = ocustomClass.BBN_PRSNL_TTL
            params(209) = New SqlParameter("@BBN_PRSNL_NDNSN_RPH", SqlDbType.Int)
            params(209).Value = ocustomClass.BBN_PRSNL_NDNSN_RPH
            params(210) = New SqlParameter("@BBN_PRSNL_MT_NG_SNG", SqlDbType.Int)
            params(210).Value = ocustomClass.BBN_PRSNL_MT_NG_SNG
            params(211) = New SqlParameter("@BBN_NN_PRSNL_TTL", SqlDbType.Int)
            params(211).Value = ocustomClass.BBN_NN_PRSNL_TTL
            params(212) = New SqlParameter("@BBN_NN_PRSNL_NDNSN_RPH", SqlDbType.Int)
            params(212).Value = ocustomClass.BBN_NN_PRSNL_NDNSN_RPH
            params(213) = New SqlParameter("@BBN_NN_PRSNL_MT_NG_SNG", SqlDbType.Int)
            params(213).Value = ocustomClass.BBN_NN_PRSNL_MT_NG_SNG
            params(214) = New SqlParameter("@BBN_TTL", SqlDbType.Int)
            params(214).Value = ocustomClass.BBN_TTL
            params(215) = New SqlParameter("@BBN_NDNSN_RPH", SqlDbType.Int)
            params(215).Value = ocustomClass.BBN_NDNSN_RPH
            params(216) = New SqlParameter("@BBN_MT_NG_SNG", SqlDbType.Int)
            params(216).Value = ocustomClass.BBN_MT_NG_SNG
            params(217) = New SqlParameter("@LB_RG_SBLM_PJK_TTL", SqlDbType.Int)
            params(217).Value = ocustomClass.LB_RG_SBLM_PJK_TTL
            params(218) = New SqlParameter("@LB_RG_SBLM_PJK_NDNSN_RPH", SqlDbType.Int)
            params(218).Value = ocustomClass.LB_RG_SBLM_PJK_NDNSN_RPH
            params(219) = New SqlParameter("@LB_RG_SBLM_PJK_MT_NG_SNG", SqlDbType.Int)
            params(219).Value = ocustomClass.LB_RG_SBLM_PJK_MT_NG_SNG
            params(220) = New SqlParameter("@PJK_THN_BRJLN_TTL", SqlDbType.Int)
            params(220).Value = ocustomClass.PJK_THN_BRJLN_TTL
            params(221) = New SqlParameter("@PJK_THN_BRJLN_NDNSN_RPH", SqlDbType.Int)
            params(221).Value = ocustomClass.PJK_THN_BRJLN_NDNSN_RPH
            params(222) = New SqlParameter("@PJK_THN_BRJLN_MT_NG_SNG", SqlDbType.Int)
            params(222).Value = ocustomClass.PJK_THN_BRJLN_MT_NG_SNG
            params(223) = New SqlParameter("@PNDPTN_BBN_PJK_TNGGHN_TTL", SqlDbType.Int)
            params(223).Value = ocustomClass.PNDPTN_BBN_PJK_TNGGHN_TTL
            params(224) = New SqlParameter("@PNDPTN_BBN_PJK_TNGGHN_NDNSN_RPH", SqlDbType.Int)
            params(224).Value = ocustomClass.PNDPTN_BBN_PJK_TNGGHN_NDNSN_RPH
            params(225) = New SqlParameter("@PNDPTN_BBN_PJK_TNGGHN_MT_NG_SNG", SqlDbType.Int)
            params(225).Value = ocustomClass.PNDPTN_BBN_PJK_TNGGHN_MT_NG_SNG
            params(226) = New SqlParameter("@LB_RG_BRSH_STLH_PJK_TTL", SqlDbType.Int)
            params(226).Value = ocustomClass.LB_RG_BRSH_STLH_PJK_TTL
            params(227) = New SqlParameter("@LB_RG_BRSH_STLH_PJK_NDNSN_RPH", SqlDbType.Int)
            params(227).Value = ocustomClass.LB_RG_BRSH_STLH_PJK_NDNSN_RPH
            params(228) = New SqlParameter("@LB_RG_BRSH_STLH_PJK_MT_NG_SNG", SqlDbType.Int)
            params(228).Value = ocustomClass.LB_RG_BRSH_STLH_PJK_MT_NG_SNG
            params(229) = New SqlParameter("@KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL", SqlDbType.Int)
            params(229).Value = ocustomClass.KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL
            params(230) = New SqlParameter("@KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH", SqlDbType.Int)
            params(230).Value = ocustomClass.KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH
            params(231) = New SqlParameter("@KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG", SqlDbType.Int)
            params(231).Value = ocustomClass.KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG
            params(232) = New SqlParameter("@SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL", SqlDbType.Int)
            params(232).Value = ocustomClass.SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL
            params(233) = New SqlParameter("@SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH", SqlDbType.Int)
            params(233).Value = ocustomClass.SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH
            params(234) = New SqlParameter("@SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG", SqlDbType.Int)
            params(234).Value = ocustomClass.SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG
            params(235) = New SqlParameter("@KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL", SqlDbType.Int)
            params(235).Value = ocustomClass.KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL
            params(236) = New SqlParameter("@KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH", SqlDbType.Int)
            params(236).Value = ocustomClass.KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH
            params(237) = New SqlParameter("@KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG", SqlDbType.Int)
            params(237).Value = ocustomClass.KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG
            params(238) = New SqlParameter("@KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL", SqlDbType.Int)
            params(238).Value = ocustomClass.KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL
            params(239) = New SqlParameter("@KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH", SqlDbType.Int)
            params(239).Value = ocustomClass.KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH
            params(240) = New SqlParameter("@KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG", SqlDbType.Int)
            params(240).Value = ocustomClass.KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG
            params(241) = New SqlParameter("@KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL", SqlDbType.Int)
            params(241).Value = ocustomClass.KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL
            params(242) = New SqlParameter("@KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH", SqlDbType.Int)
            params(242).Value = ocustomClass.KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH
            params(243) = New SqlParameter("@KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG", SqlDbType.Int)
            params(243).Value = ocustomClass.KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG
            params(244) = New SqlParameter("@KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL", SqlDbType.Int)
            params(244).Value = ocustomClass.KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL
            params(245) = New SqlParameter("@KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH", SqlDbType.Int)
            params(245).Value = ocustomClass.KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH
            params(246) = New SqlParameter("@KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG", SqlDbType.Int)
            params(246).Value = ocustomClass.KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG
            params(247) = New SqlParameter("@LB_RG_BRSH_KMPRHNSF_THN_BRJLN_TTL", SqlDbType.Int)
            params(247).Value = ocustomClass.LB_RG_BRSH_KMPRHNSF_THN_BRJLN_TTL
            params(248) = New SqlParameter("@LB_RG_BRSH_KMPRHNSF_THN_BRJLN_NDNSN_RPH", SqlDbType.Int)
            params(248).Value = ocustomClass.LB_RG_BRSH_KMPRHNSF_THN_BRJLN_NDNSN_RPH
            params(249) = New SqlParameter("@LB_RG_BRSH_KMPRHNSF_THN_BRJLN_MT_NG_SNG", SqlDbType.Int)
            params(249).Value = ocustomClass.LB_RG_BRSH_KMPRHNSF_THN_BRJLN_MT_NG_SNG
            params(250) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(250).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SaveEdit, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP1200.SIPP1200SaveEdit")
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function SIPP1200SaveAdd(ocustomClass As Parameter.SIPP1200) As Parameter.SIPP1200
        Dim params(249) As SqlParameter
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar)
            params(0).Value = ocustomClass.BULANDATA
            params(1) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_TTL", SqlDbType.Int)
            params(1).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_TTL
            params(2) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_NDNSN_RPH", SqlDbType.Int)
            params(2).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_NDNSN_RPH
            params(3) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_MT_NG_SNG", SqlDbType.Int)
            params(3).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_MT_NG_SNG
            params(4) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_TTL", SqlDbType.Int)
            params(4).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_TTL
            params(5) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH", SqlDbType.Int)
            params(5).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH
            params(6) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG", SqlDbType.Int)
            params(6).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG
            params(7) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_TTL", SqlDbType.Int)
            params(7).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_TTL
            params(8) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_NDNSN_RPH", SqlDbType.Int)
            params(8).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_NDNSN_RPH
            params(9) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_MT_NG_SNG", SqlDbType.Int)
            params(9).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_MT_NG_SNG
            params(10) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL", SqlDbType.Int)
            params(10).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL
            params(11) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH", SqlDbType.Int)
            params(11).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH
            params(12) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG", SqlDbType.Int)
            params(12).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG
            params(13) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_TTL", SqlDbType.Int)
            params(13).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_TTL
            params(14) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_NDNSN_RPH", SqlDbType.Int)
            params(14).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_NDNSN_RPH
            params(15) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_MT_NG_SNG", SqlDbType.Int)
            params(15).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_MT_NG_SNG
            params(16) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_TTL", SqlDbType.Int)
            params(16).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_TTL
            params(17) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_NDNSN_RPH", SqlDbType.Int)
            params(17).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_NDNSN_RPH
            params(18) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_MT_NG_SNG", SqlDbType.Int)
            params(18).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_MT_NG_SNG
            params(19) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_TTL", SqlDbType.Int)
            params(19).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_TTL
            params(20) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH", SqlDbType.Int)
            params(20).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH
            params(21) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG", SqlDbType.Int)
            params(21).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG
            params(22) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_TTL", SqlDbType.Int)
            params(22).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_TTL
            params(23) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_NDNSN_RPH", SqlDbType.Int)
            params(23).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_NDNSN_RPH
            params(24) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_MT_NG_SNG", SqlDbType.Int)
            params(24).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_MT_NG_SNG
            params(25) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_TTL", SqlDbType.Int)
            params(25).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_TTL
            params(26) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH", SqlDbType.Int)
            params(26).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH
            params(27) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG", SqlDbType.Int)
            params(27).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG
            params(28) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_TTL", SqlDbType.Int)
            params(28).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_TTL
            params(29) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_NDNSN_RPH", SqlDbType.Int)
            params(29).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_NDNSN_RPH
            params(30) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_MT_NG_SNG", SqlDbType.Int)
            params(30).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_MT_NG_SNG
            params(31) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_TTL", SqlDbType.Int)
            params(31).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_TTL
            params(32) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_NDNSN_RPH", SqlDbType.Int)
            params(32).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_NDNSN_RPH
            params(33) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_MT_NG_SNG", SqlDbType.Int)
            params(33).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_MT_NG_SNG
            params(34) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_TTL", SqlDbType.Int)
            params(34).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_TTL
            params(35) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_NDNSN_RPH", SqlDbType.Int)
            params(35).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_NDNSN_RPH
            params(36) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_MT_NG_SNG", SqlDbType.Int)
            params(36).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_MT_NG_SNG
            params(37) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_TTL", SqlDbType.Int)
            params(37).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_TTL
            params(38) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH", SqlDbType.Int)
            params(38).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH
            params(39) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG", SqlDbType.Int)
            params(39).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG
            params(40) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_TTL", SqlDbType.Int)
            params(40).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_TTL
            params(41) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_NDNSN_RPH", SqlDbType.Int)
            params(41).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_NDNSN_RPH
            params(42) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_MT_NG_SNG", SqlDbType.Int)
            params(42).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_MT_NG_SNG
            params(43) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_TTL", SqlDbType.Int)
            params(43).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_TTL
            params(44) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_NDNSN_RPH", SqlDbType.Int)
            params(44).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_NDNSN_RPH
            params(45) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_MT_NG_SNG", SqlDbType.Int)
            params(45).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_MT_NG_SNG
            params(46) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL", SqlDbType.Int)
            params(46).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL
            params(47) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH", SqlDbType.Int)
            params(47).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH
            params(48) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG", SqlDbType.Int)
            params(48).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG
            params(49) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_TTL", SqlDbType.Int)
            params(49).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_TTL
            params(50) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH", SqlDbType.Int)
            params(50).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH
            params(51) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG", SqlDbType.Int)
            params(51).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG
            params(52) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_TTL", SqlDbType.Int)
            params(52).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_TTL
            params(53) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_NDNSN_RPH", SqlDbType.Int)
            params(53).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_NDNSN_RPH
            params(54) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_MT_NG_SNG", SqlDbType.Int)
            params(54).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_MT_NG_SNG
            params(55) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL", SqlDbType.Int)
            params(55).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL
            params(56) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH", SqlDbType.Int)
            params(56).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH
            params(57) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG", SqlDbType.Int)
            params(57).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG
            params(58) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_TTL", SqlDbType.Int)
            params(58).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_TTL
            params(59) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_NDNSN_RPH", SqlDbType.Int)
            params(59).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_NDNSN_RPH
            params(60) = New SqlParameter("@PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_MT_NG_SNG", SqlDbType.Int)
            params(60).Value = ocustomClass.PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_MT_NG_SNG
            params(61) = New SqlParameter("@PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL", SqlDbType.Int)
            params(61).Value = ocustomClass.PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL
            params(62) = New SqlParameter("@PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH", SqlDbType.Int)
            params(62).Value = ocustomClass.PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH
            params(63) = New SqlParameter("@PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG", SqlDbType.Int)
            params(63).Value = ocustomClass.PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG
            params(64) = New SqlParameter("@PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL", SqlDbType.Int)
            params(64).Value = ocustomClass.PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL
            params(65) = New SqlParameter("@PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH", SqlDbType.Int)
            params(65).Value = ocustomClass.PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH
            params(66) = New SqlParameter("@PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG", SqlDbType.Int)
            params(66).Value = ocustomClass.PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG
            params(67) = New SqlParameter("@PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL", SqlDbType.Int)
            params(67).Value = ocustomClass.PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL
            params(68) = New SqlParameter("@PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH", SqlDbType.Int)
            params(68).Value = ocustomClass.PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH
            params(69) = New SqlParameter("@PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG", SqlDbType.Int)
            params(69).Value = ocustomClass.PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG
            params(70) = New SqlParameter("@PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_TTL", SqlDbType.Int)
            params(70).Value = ocustomClass.PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_TTL
            params(71) = New SqlParameter("@PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH", SqlDbType.Int)
            params(71).Value = ocustomClass.PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH
            params(72) = New SqlParameter("@PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG", SqlDbType.Int)
            params(72).Value = ocustomClass.PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG
            params(73) = New SqlParameter("@PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL", SqlDbType.Int)
            params(73).Value = ocustomClass.PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL
            params(74) = New SqlParameter("@PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH", SqlDbType.Int)
            params(74).Value = ocustomClass.PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH
            params(75) = New SqlParameter("@PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG", SqlDbType.Int)
            params(75).Value = ocustomClass.PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG
            params(76) = New SqlParameter("@PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_TTL", SqlDbType.Int)
            params(76).Value = ocustomClass.PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_TTL
            params(77) = New SqlParameter("@PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_NDNSN_RPH", SqlDbType.Int)
            params(77).Value = ocustomClass.PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_NDNSN_RPH
            params(78) = New SqlParameter("@PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_MT_NG_SNG", SqlDbType.Int)
            params(78).Value = ocustomClass.PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_MT_NG_SNG
            params(79) = New SqlParameter("@PNDPTN_DMNSTRS_TTL", SqlDbType.Int)
            params(79).Value = ocustomClass.PNDPTN_DMNSTRS_TTL
            params(80) = New SqlParameter("@PNDPTN_DMNSTRS_NDNSN_RPH", SqlDbType.Int)
            params(80).Value = ocustomClass.PNDPTN_DMNSTRS_NDNSN_RPH
            params(81) = New SqlParameter("@PNDPTN_DMNSTRS_MT_NG_SNG", SqlDbType.Int)
            params(81).Value = ocustomClass.PNDPTN_DMNSTRS_MT_NG_SNG
            params(82) = New SqlParameter("@PNDPTN_PRVS_TTL", SqlDbType.Int)
            params(82).Value = ocustomClass.PNDPTN_PRVS_TTL
            params(83) = New SqlParameter("@PNDPTN_PRVS_NDNSN_RPH", SqlDbType.Int)
            params(83).Value = ocustomClass.PNDPTN_PRVS_NDNSN_RPH
            params(84) = New SqlParameter("@PNDPTN_PRVS_MT_NG_SNG", SqlDbType.Int)
            params(84).Value = ocustomClass.PNDPTN_PRVS_MT_NG_SNG
            params(85) = New SqlParameter("@PNDPTN_DND_TTL", SqlDbType.Int)
            params(85).Value = ocustomClass.PNDPTN_DND_TTL
            params(86) = New SqlParameter("@PNDPTN_DND_NDNSN_RPH", SqlDbType.Int)
            params(86).Value = ocustomClass.PNDPTN_DND_NDNSN_RPH
            params(87) = New SqlParameter("@PNDPTN_DND_MT_NG_SNG", SqlDbType.Int)
            params(87).Value = ocustomClass.PNDPTN_DND_MT_NG_SNG
            params(88) = New SqlParameter("@PNDPTN_DSKN_SRNS_TTL", SqlDbType.Int)
            params(88).Value = ocustomClass.PNDPTN_DSKN_SRNS_TTL
            params(89) = New SqlParameter("@PNDPTN_DSKN_SRNS_NDNSN_RPH", SqlDbType.Int)
            params(89).Value = ocustomClass.PNDPTN_DSKN_SRNS_NDNSN_RPH
            params(90) = New SqlParameter("@PNDPTN_DSKN_SRNS_MT_NG_SNG", SqlDbType.Int)
            params(90).Value = ocustomClass.PNDPTN_DSKN_SRNS_MT_NG_SNG
            params(91) = New SqlParameter("@PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_TTL", SqlDbType.Int)
            params(91).Value = ocustomClass.PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_TTL
            params(92) = New SqlParameter("@PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_NDNSN_RPH", SqlDbType.Int)
            params(92).Value = ocustomClass.PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_NDNSN_RPH
            params(93) = New SqlParameter("@PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_MT_NG_SNG", SqlDbType.Int)
            params(93).Value = ocustomClass.PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_MT_NG_SNG
            params(94) = New SqlParameter("@PNDPTN_PRSNL_LN_TRKT_PMBYN_TTL", SqlDbType.Int)
            params(94).Value = ocustomClass.PNDPTN_PRSNL_LN_TRKT_PMBYN_TTL
            params(95) = New SqlParameter("@PNDPTN_PRSNL_LN_TRKT_PMBYN_NDNSN_RPH", SqlDbType.Int)
            params(95).Value = ocustomClass.PNDPTN_PRSNL_LN_TRKT_PMBYN_NDNSN_RPH
            params(96) = New SqlParameter("@PNDPTN_PRSNL_LN_TRKT_PMBYN_MT_NG_SNG", SqlDbType.Int)
            params(96).Value = ocustomClass.PNDPTN_PRSNL_LN_TRKT_PMBYN_MT_NG_SNG
            params(97) = New SqlParameter("@PNDPTN_DR_SW_PRS_PRTNG_LS_TTL", SqlDbType.Int)
            params(97).Value = ocustomClass.PNDPTN_DR_SW_PRS_PRTNG_LS_TTL
            params(98) = New SqlParameter("@PNDPTN_DR_SW_PRS_PRTNG_LS_NDNSN_RPH", SqlDbType.Int)
            params(98).Value = ocustomClass.PNDPTN_DR_SW_PRS_PRTNG_LS_NDNSN_RPH
            params(99) = New SqlParameter("@PNDPTN_DR_SW_PRS_PRTNG_LS_MT_NG_SNG", SqlDbType.Int)
            params(99).Value = ocustomClass.PNDPTN_DR_SW_PRS_PRTNG_LS_MT_NG_SNG
            params(100) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_RKSDN_TTL", SqlDbType.Int)
            params(100).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_RKSDN_TTL
            params(101) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_RKSDN_NDNSN_RPH", SqlDbType.Int)
            params(101).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_RKSDN_NDNSN_RPH
            params(102) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_RKSDN_MT_NG_SNG", SqlDbType.Int)
            params(102).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_RKSDN_MT_NG_SNG
            params(103) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_SRNS_TTL", SqlDbType.Int)
            params(103).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_SRNS_TTL
            params(104) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_SRNS_NDNSN_RPH", SqlDbType.Int)
            params(104).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_SRNS_NDNSN_RPH
            params(105) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_SRNS_MT_NG_SNG", SqlDbType.Int)
            params(105).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_SRNS_MT_NG_SNG
            params(106) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_LNNY_TTL", SqlDbType.Int)
            params(106).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_LNNY_TTL
            params(107) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_LNNY_NDNSN_RPH", SqlDbType.Int)
            params(107).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_LNNY_NDNSN_RPH
            params(108) = New SqlParameter("@PNDPTN_F_DR_PMSRN_PRDK_LNNY_MT_NG_SNG", SqlDbType.Int)
            params(108).Value = ocustomClass.PNDPTN_F_DR_PMSRN_PRDK_LNNY_MT_NG_SNG
            params(109) = New SqlParameter("@PNDPTN_DR_KGTN_BRBSS_F_TTL", SqlDbType.Int)
            params(109).Value = ocustomClass.PNDPTN_DR_KGTN_BRBSS_F_TTL
            params(110) = New SqlParameter("@PNDPTN_DR_KGTN_BRBSS_F_NDNSN_RPH", SqlDbType.Int)
            params(110).Value = ocustomClass.PNDPTN_DR_KGTN_BRBSS_F_NDNSN_RPH
            params(111) = New SqlParameter("@PNDPTN_DR_KGTN_BRBSS_F_MT_NG_SNG", SqlDbType.Int)
            params(111).Value = ocustomClass.PNDPTN_DR_KGTN_BRBSS_F_MT_NG_SNG
            params(112) = New SqlParameter("@PNDPTN_PRSNL_LNNY_LNNY_TTL", SqlDbType.Int)
            params(112).Value = ocustomClass.PNDPTN_PRSNL_LNNY_LNNY_TTL
            params(113) = New SqlParameter("@PNDPTN_PRSNL_LNNY_LNNY_NDNSN_RPH", SqlDbType.Int)
            params(113).Value = ocustomClass.PNDPTN_PRSNL_LNNY_LNNY_NDNSN_RPH
            params(114) = New SqlParameter("@PNDPTN_PRSNL_LNNY_LNNY_MT_NG_SNG", SqlDbType.Int)
            params(114).Value = ocustomClass.PNDPTN_PRSNL_LNNY_LNNY_MT_NG_SNG
            params(115) = New SqlParameter("@PNDPTN_PRSNL_LNNY_TTL", SqlDbType.Int)
            params(115).Value = ocustomClass.PNDPTN_PRSNL_LNNY_TTL
            params(116) = New SqlParameter("@PNDPTN_PRSNL_LNNY_NDNSN_RPH", SqlDbType.Int)
            params(116).Value = ocustomClass.PNDPTN_PRSNL_LNNY_NDNSN_RPH
            params(117) = New SqlParameter("@PNDPTN_PRSNL_LNNY_MT_NG_SNG", SqlDbType.Int)
            params(117).Value = ocustomClass.PNDPTN_PRSNL_LNNY_MT_NG_SNG
            params(118) = New SqlParameter("@PNDPTN_PRSNL_TTL", SqlDbType.Int)
            params(118).Value = ocustomClass.PNDPTN_PRSNL_TTL
            params(119) = New SqlParameter("@PNDPTN_PRSNL_NDNSN_RPH", SqlDbType.Int)
            params(119).Value = ocustomClass.PNDPTN_PRSNL_NDNSN_RPH
            params(120) = New SqlParameter("@PNDPTN_PRSNL_MT_NG_SNG", SqlDbType.Int)
            params(120).Value = ocustomClass.PNDPTN_PRSNL_MT_NG_SNG
            params(121) = New SqlParameter("@PNDPTN_BNG_NN_PRSNL_TTL", SqlDbType.Int)
            params(121).Value = ocustomClass.PNDPTN_BNG_NN_PRSNL_TTL
            params(122) = New SqlParameter("@PNDPTN_BNG_NN_PRSNL_NDNSN_RPH", SqlDbType.Int)
            params(122).Value = ocustomClass.PNDPTN_BNG_NN_PRSNL_NDNSN_RPH
            params(123) = New SqlParameter("@PNDPTN_BNG_NN_PRSNL_MT_NG_SNG", SqlDbType.Int)
            params(123).Value = ocustomClass.PNDPTN_BNG_NN_PRSNL_MT_NG_SNG
            params(124) = New SqlParameter("@PNDPTN_NN_PRSNL_LNNY_TTL", SqlDbType.Int)
            params(124).Value = ocustomClass.PNDPTN_NN_PRSNL_LNNY_TTL
            params(125) = New SqlParameter("@PNDPTN_NN_PRSNL_LNNY_NDNSN_RPH", SqlDbType.Int)
            params(125).Value = ocustomClass.PNDPTN_NN_PRSNL_LNNY_NDNSN_RPH
            params(126) = New SqlParameter("@PNDPTN_NN_PRSNL_LNNY_MT_NG_SNG", SqlDbType.Int)
            params(126).Value = ocustomClass.PNDPTN_NN_PRSNL_LNNY_MT_NG_SNG
            params(127) = New SqlParameter("@PNDPTN_NN_PRSNL_TTL", SqlDbType.Int)
            params(127).Value = ocustomClass.PNDPTN_NN_PRSNL_TTL
            params(128) = New SqlParameter("@PNDPTN_NN_PRSNL_NDNSN_RPH", SqlDbType.Int)
            params(128).Value = ocustomClass.PNDPTN_NN_PRSNL_NDNSN_RPH
            params(129) = New SqlParameter("@PNDPTN_NN_PRSNL_MT_NG_SNG", SqlDbType.Int)
            params(129).Value = ocustomClass.PNDPTN_NN_PRSNL_MT_NG_SNG
            params(130) = New SqlParameter("@PNDPTN_TTL", SqlDbType.Int)
            params(130).Value = ocustomClass.PNDPTN_TTL
            params(131) = New SqlParameter("@PNDPTN_NDNSN_RPH", SqlDbType.Int)
            params(131).Value = ocustomClass.PNDPTN_NDNSN_RPH
            params(132) = New SqlParameter("@PNDPTN_MT_NG_SNG", SqlDbType.Int)
            params(132).Value = ocustomClass.PNDPTN_MT_NG_SNG
            params(133) = New SqlParameter("@BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_TTL", SqlDbType.Int)
            params(133).Value = ocustomClass.BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_TTL
            params(134) = New SqlParameter("@BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_NDNSN_RPH", SqlDbType.Int)
            params(134).Value = ocustomClass.BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_NDNSN_RPH
            params(135) = New SqlParameter("@BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_MT_NG_SNG", SqlDbType.Int)
            params(135).Value = ocustomClass.BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_MT_NG_SNG
            params(136) = New SqlParameter("@BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_TTL", SqlDbType.Int)
            params(136).Value = ocustomClass.BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_TTL
            params(137) = New SqlParameter("@BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH", SqlDbType.Int)
            params(137).Value = ocustomClass.BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH
            params(138) = New SqlParameter("@BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG", SqlDbType.Int)
            params(138).Value = ocustomClass.BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG
            params(139) = New SqlParameter("@BBN_BNG_PRSNL_TTL", SqlDbType.Int)
            params(139).Value = ocustomClass.BBN_BNG_PRSNL_TTL
            params(140) = New SqlParameter("@BBN_BNG_PRSNL_NDNSN_RPH", SqlDbType.Int)
            params(140).Value = ocustomClass.BBN_BNG_PRSNL_NDNSN_RPH
            params(141) = New SqlParameter("@BBN_BNG_PRSNL_MT_NG_SNG", SqlDbType.Int)
            params(141).Value = ocustomClass.BBN_BNG_PRSNL_MT_NG_SNG
            params(142) = New SqlParameter("@BBN_PRM_TS_TRNSKS_SWP_TTL", SqlDbType.Int)
            params(142).Value = ocustomClass.BBN_PRM_TS_TRNSKS_SWP_TTL
            params(143) = New SqlParameter("@BBN_PRM_TS_TRNSKS_SWP_NDNSN_RPH", SqlDbType.Int)
            params(143).Value = ocustomClass.BBN_PRM_TS_TRNSKS_SWP_NDNSN_RPH
            params(144) = New SqlParameter("@BBN_PRM_TS_TRNSKS_SWP_MT_NG_SNG", SqlDbType.Int)
            params(144).Value = ocustomClass.BBN_PRM_TS_TRNSKS_SWP_MT_NG_SNG
            params(145) = New SqlParameter("@BBN_PRM_SRNS_TTL", SqlDbType.Int)
            params(145).Value = ocustomClass.BBN_PRM_SRNS_TTL
            params(146) = New SqlParameter("@BBN_PRM_SRNS_NDNSN_RPH", SqlDbType.Int)
            params(146).Value = ocustomClass.BBN_PRM_SRNS_NDNSN_RPH
            params(147) = New SqlParameter("@BBN_PRM_SRNS_MT_NG_SNG", SqlDbType.Int)
            params(147).Value = ocustomClass.BBN_PRM_SRNS_MT_NG_SNG
            params(148) = New SqlParameter("@BBN_GJ_PH_DN_TNJNGN_TTL", SqlDbType.Int)
            params(148).Value = ocustomClass.BBN_GJ_PH_DN_TNJNGN_TTL
            params(149) = New SqlParameter("@BBN_GJ_PH_DN_TNJNGN_NDNSN_RPH", SqlDbType.Int)
            params(149).Value = ocustomClass.BBN_GJ_PH_DN_TNJNGN_NDNSN_RPH
            params(150) = New SqlParameter("@BBN_GJ_PH_DN_TNJNGN_MT_NG_SNG", SqlDbType.Int)
            params(150).Value = ocustomClass.BBN_GJ_PH_DN_TNJNGN_MT_NG_SNG
            params(151) = New SqlParameter("@BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_TTL", SqlDbType.Int)
            params(151).Value = ocustomClass.BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_TTL
            params(152) = New SqlParameter("@BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_NDNSN_RPH", SqlDbType.Int)
            params(152).Value = ocustomClass.BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_NDNSN_RPH
            params(153) = New SqlParameter("@BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_MT_NG_SNG", SqlDbType.Int)
            params(153).Value = ocustomClass.BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_MT_NG_SNG
            params(154) = New SqlParameter("@BBN_TNG_KRJ_LNNY_TTL", SqlDbType.Int)
            params(154).Value = ocustomClass.BBN_TNG_KRJ_LNNY_TTL
            params(155) = New SqlParameter("@BBN_TNG_KRJ_LNNY_NDNSN_RPH", SqlDbType.Int)
            params(155).Value = ocustomClass.BBN_TNG_KRJ_LNNY_NDNSN_RPH
            params(156) = New SqlParameter("@BBN_TNG_KRJ_LNNY_MT_NG_SNG", SqlDbType.Int)
            params(156).Value = ocustomClass.BBN_TNG_KRJ_LNNY_MT_NG_SNG
            params(157) = New SqlParameter("@BBN_TNG_KRJ_TTL", SqlDbType.Int)
            params(157).Value = ocustomClass.BBN_TNG_KRJ_TTL
            params(158) = New SqlParameter("@BBN_TNG_KRJ_NDNSN_RPH", SqlDbType.Int)
            params(158).Value = ocustomClass.BBN_TNG_KRJ_NDNSN_RPH
            params(159) = New SqlParameter("@BBN_TNG_KRJ_MT_NG_SNG", SqlDbType.Int)
            params(159).Value = ocustomClass.BBN_TNG_KRJ_MT_NG_SNG
            params(160) = New SqlParameter("@BBN_NSNTF_PHK_KTG_TTL", SqlDbType.Int)
            params(160).Value = ocustomClass.BBN_NSNTF_PHK_KTG_TTL
            params(161) = New SqlParameter("@BBN_NSNTF_PHK_KTG_NDNSN_RPH", SqlDbType.Int)
            params(161).Value = ocustomClass.BBN_NSNTF_PHK_KTG_NDNSN_RPH
            params(162) = New SqlParameter("@BBN_NSNTF_PHK_KTG_MT_NG_SNG", SqlDbType.Int)
            params(162).Value = ocustomClass.BBN_NSNTF_PHK_KTG_MT_NG_SNG
            params(163) = New SqlParameter("@BBN_PMSRN_LNNY_TTL", SqlDbType.Int)
            params(163).Value = ocustomClass.BBN_PMSRN_LNNY_TTL
            params(164) = New SqlParameter("@BBN_PMSRN_LNNY_NDNSN_RPH", SqlDbType.Int)
            params(164).Value = ocustomClass.BBN_PMSRN_LNNY_NDNSN_RPH
            params(165) = New SqlParameter("@BBN_PMSRN_LNNY_MT_NG_SNG", SqlDbType.Int)
            params(165).Value = ocustomClass.BBN_PMSRN_LNNY_MT_NG_SNG
            params(166) = New SqlParameter("@BBN_PMSRN_TTL", SqlDbType.Int)
            params(166).Value = ocustomClass.BBN_PMSRN_TTL
            params(167) = New SqlParameter("@BBN_PMSRN_NDNSN_RPH", SqlDbType.Int)
            params(167).Value = ocustomClass.BBN_PMSRN_NDNSN_RPH
            params(168) = New SqlParameter("@BBN_PMSRN_MT_NG_SNG", SqlDbType.Int)
            params(168).Value = ocustomClass.BBN_PMSRN_MT_NG_SNG
            params(169) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_TTL", SqlDbType.Int)
            params(169).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_TTL
            params(170) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_NDNSN_RPH", SqlDbType.Int)
            params(170).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_NDNSN_RPH
            params(171) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_MT_NG_SNG", SqlDbType.Int)
            params(171).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_MT_NG_SNG
            params(172) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_TTL", SqlDbType.Int)
            params(172).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_TTL
            params(173) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_NDNSN_RPH", SqlDbType.Int)
            params(173).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_NDNSN_RPH
            params(174) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_MT_NG_SNG", SqlDbType.Int)
            params(174).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_MT_NG_SNG
            params(175) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_TTL", SqlDbType.Int)
            params(175).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_TTL
            params(176) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_NDNSN_RPH", SqlDbType.Int)
            params(176).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_NDNSN_RPH
            params(177) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_MT_NG_SNG", SqlDbType.Int)
            params(177).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_MT_NG_SNG
            params(178) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_TTL", SqlDbType.Int)
            params(178).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_TTL
            params(179) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH", SqlDbType.Int)
            params(179).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH
            params(180) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG", SqlDbType.Int)
            params(180).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG
            params(181) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_TTL", SqlDbType.Int)
            params(181).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_TTL
            params(182) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH", SqlDbType.Int)
            params(182).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH
            params(183) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG", SqlDbType.Int)
            params(183).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG
            params(184) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_TTL", SqlDbType.Int)
            params(184).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_TTL
            params(185) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NDNSN_RPH", SqlDbType.Int)
            params(185).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NDNSN_RPH
            params(186) = New SqlParameter("@BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MT_NG_SNG", SqlDbType.Int)
            params(186).Value = ocustomClass.BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MT_NG_SNG
            params(187) = New SqlParameter("@BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_TTL", SqlDbType.Int)
            params(187).Value = ocustomClass.BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_TTL
            params(188) = New SqlParameter("@BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_NDNSN_RPH", SqlDbType.Int)
            params(188).Value = ocustomClass.BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_NDNSN_RPH
            params(189) = New SqlParameter("@BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_MT_NG_SNG", SqlDbType.Int)
            params(189).Value = ocustomClass.BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_MT_NG_SNG
            params(190) = New SqlParameter("@BBN_PNYSTN_ST_TTP_DN_NVNTRS_TTL", SqlDbType.Int)
            params(190).Value = ocustomClass.BBN_PNYSTN_ST_TTP_DN_NVNTRS_TTL
            params(191) = New SqlParameter("@BBN_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH", SqlDbType.Int)
            params(191).Value = ocustomClass.BBN_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH
            params(192) = New SqlParameter("@BBN_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG", SqlDbType.Int)
            params(192).Value = ocustomClass.BBN_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG
            params(193) = New SqlParameter("@BBN_PNYSHNPNYSTN_TTL", SqlDbType.Int)
            params(193).Value = ocustomClass.BBN_PNYSHNPNYSTN_TTL
            params(194) = New SqlParameter("@BBN_PNYSHNPNYSTN_NDNSN_RPH", SqlDbType.Int)
            params(194).Value = ocustomClass.BBN_PNYSHNPNYSTN_NDNSN_RPH
            params(195) = New SqlParameter("@BBN_PNYSHNPNYSTN_MT_NG_SNG", SqlDbType.Int)
            params(195).Value = ocustomClass.BBN_PNYSHNPNYSTN_MT_NG_SNG
            params(196) = New SqlParameter("@BBN_SW_TTL", SqlDbType.Int)
            params(196).Value = ocustomClass.BBN_SW_TTL
            params(197) = New SqlParameter("@BBN_SW_NDNSN_RPH", SqlDbType.Int)
            params(197).Value = ocustomClass.BBN_SW_NDNSN_RPH
            params(198) = New SqlParameter("@BBN_SW_MT_NG_SNG", SqlDbType.Int)
            params(198).Value = ocustomClass.BBN_SW_MT_NG_SNG
            params(199) = New SqlParameter("@BBN_PMLHRN_DN_PRBKN_TTL", SqlDbType.Int)
            params(199).Value = ocustomClass.BBN_PMLHRN_DN_PRBKN_TTL
            params(200) = New SqlParameter("@BBN_PMLHRN_DN_PRBKN_NDNSN_RPH", SqlDbType.Int)
            params(200).Value = ocustomClass.BBN_PMLHRN_DN_PRBKN_NDNSN_RPH
            params(201) = New SqlParameter("@BBN_PMLHRN_DN_PRBKN_MT_NG_SNG", SqlDbType.Int)
            params(201).Value = ocustomClass.BBN_PMLHRN_DN_PRBKN_MT_NG_SNG
            params(202) = New SqlParameter("@BBN_DMNSTRS_DN_MM_TTL", SqlDbType.Int)
            params(202).Value = ocustomClass.BBN_DMNSTRS_DN_MM_TTL
            params(203) = New SqlParameter("@BBN_DMNSTRS_DN_MM_NDNSN_RPH", SqlDbType.Int)
            params(203).Value = ocustomClass.BBN_DMNSTRS_DN_MM_NDNSN_RPH
            params(204) = New SqlParameter("@BBN_DMNSTRS_DN_MM_MT_NG_SNG", SqlDbType.Int)
            params(204).Value = ocustomClass.BBN_DMNSTRS_DN_MM_MT_NG_SNG
            params(205) = New SqlParameter("@BBN_PRSNL_LNNY_TTL", SqlDbType.Int)
            params(205).Value = ocustomClass.BBN_PRSNL_LNNY_TTL
            params(206) = New SqlParameter("@BBN_PRSNL_LNNY_NDNSN_RPH", SqlDbType.Int)
            params(206).Value = ocustomClass.BBN_PRSNL_LNNY_NDNSN_RPH
            params(207) = New SqlParameter("@BBN_PRSNL_LNNY_MT_NG_SNG", SqlDbType.Int)
            params(207).Value = ocustomClass.BBN_PRSNL_LNNY_MT_NG_SNG
            params(208) = New SqlParameter("@BBN_PRSNL_TTL", SqlDbType.Int)
            params(208).Value = ocustomClass.BBN_PRSNL_TTL
            params(209) = New SqlParameter("@BBN_PRSNL_NDNSN_RPH", SqlDbType.Int)
            params(209).Value = ocustomClass.BBN_PRSNL_NDNSN_RPH
            params(210) = New SqlParameter("@BBN_PRSNL_MT_NG_SNG", SqlDbType.Int)
            params(210).Value = ocustomClass.BBN_PRSNL_MT_NG_SNG
            params(211) = New SqlParameter("@BBN_NN_PRSNL_TTL", SqlDbType.Int)
            params(211).Value = ocustomClass.BBN_NN_PRSNL_TTL
            params(212) = New SqlParameter("@BBN_NN_PRSNL_NDNSN_RPH", SqlDbType.Int)
            params(212).Value = ocustomClass.BBN_NN_PRSNL_NDNSN_RPH
            params(213) = New SqlParameter("@BBN_NN_PRSNL_MT_NG_SNG", SqlDbType.Int)
            params(213).Value = ocustomClass.BBN_NN_PRSNL_MT_NG_SNG
            params(214) = New SqlParameter("@BBN_TTL", SqlDbType.Int)
            params(214).Value = ocustomClass.BBN_TTL
            params(215) = New SqlParameter("@BBN_NDNSN_RPH", SqlDbType.Int)
            params(215).Value = ocustomClass.BBN_NDNSN_RPH
            params(216) = New SqlParameter("@BBN_MT_NG_SNG", SqlDbType.Int)
            params(216).Value = ocustomClass.BBN_MT_NG_SNG
            params(217) = New SqlParameter("@LB_RG_SBLM_PJK_TTL", SqlDbType.Int)
            params(217).Value = ocustomClass.LB_RG_SBLM_PJK_TTL
            params(218) = New SqlParameter("@LB_RG_SBLM_PJK_NDNSN_RPH", SqlDbType.Int)
            params(218).Value = ocustomClass.LB_RG_SBLM_PJK_NDNSN_RPH
            params(219) = New SqlParameter("@LB_RG_SBLM_PJK_MT_NG_SNG", SqlDbType.Int)
            params(219).Value = ocustomClass.LB_RG_SBLM_PJK_MT_NG_SNG
            params(220) = New SqlParameter("@PJK_THN_BRJLN_TTL", SqlDbType.Int)
            params(220).Value = ocustomClass.PJK_THN_BRJLN_TTL
            params(221) = New SqlParameter("@PJK_THN_BRJLN_NDNSN_RPH", SqlDbType.Int)
            params(221).Value = ocustomClass.PJK_THN_BRJLN_NDNSN_RPH
            params(222) = New SqlParameter("@PJK_THN_BRJLN_MT_NG_SNG", SqlDbType.Int)
            params(222).Value = ocustomClass.PJK_THN_BRJLN_MT_NG_SNG
            params(223) = New SqlParameter("@PNDPTN_BBN_PJK_TNGGHN_TTL", SqlDbType.Int)
            params(223).Value = ocustomClass.PNDPTN_BBN_PJK_TNGGHN_TTL
            params(224) = New SqlParameter("@PNDPTN_BBN_PJK_TNGGHN_NDNSN_RPH", SqlDbType.Int)
            params(224).Value = ocustomClass.PNDPTN_BBN_PJK_TNGGHN_NDNSN_RPH
            params(225) = New SqlParameter("@PNDPTN_BBN_PJK_TNGGHN_MT_NG_SNG", SqlDbType.Int)
            params(225).Value = ocustomClass.PNDPTN_BBN_PJK_TNGGHN_MT_NG_SNG
            params(226) = New SqlParameter("@LB_RG_BRSH_STLH_PJK_TTL", SqlDbType.Int)
            params(226).Value = ocustomClass.LB_RG_BRSH_STLH_PJK_TTL
            params(227) = New SqlParameter("@LB_RG_BRSH_STLH_PJK_NDNSN_RPH", SqlDbType.Int)
            params(227).Value = ocustomClass.LB_RG_BRSH_STLH_PJK_NDNSN_RPH
            params(228) = New SqlParameter("@LB_RG_BRSH_STLH_PJK_MT_NG_SNG", SqlDbType.Int)
            params(228).Value = ocustomClass.LB_RG_BRSH_STLH_PJK_MT_NG_SNG
            params(229) = New SqlParameter("@KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL", SqlDbType.Int)
            params(229).Value = ocustomClass.KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL
            params(230) = New SqlParameter("@KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH", SqlDbType.Int)
            params(230).Value = ocustomClass.KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH
            params(231) = New SqlParameter("@KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG", SqlDbType.Int)
            params(231).Value = ocustomClass.KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG
            params(232) = New SqlParameter("@SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL", SqlDbType.Int)
            params(232).Value = ocustomClass.SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL
            params(233) = New SqlParameter("@SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH", SqlDbType.Int)
            params(233).Value = ocustomClass.SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH
            params(234) = New SqlParameter("@SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG", SqlDbType.Int)
            params(234).Value = ocustomClass.SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG
            params(235) = New SqlParameter("@KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL", SqlDbType.Int)
            params(235).Value = ocustomClass.KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL
            params(236) = New SqlParameter("@KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH", SqlDbType.Int)
            params(236).Value = ocustomClass.KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH
            params(237) = New SqlParameter("@KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG", SqlDbType.Int)
            params(237).Value = ocustomClass.KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG
            params(238) = New SqlParameter("@KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL", SqlDbType.Int)
            params(238).Value = ocustomClass.KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL
            params(239) = New SqlParameter("@KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH", SqlDbType.Int)
            params(239).Value = ocustomClass.KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH
            params(240) = New SqlParameter("@KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG", SqlDbType.Int)
            params(240).Value = ocustomClass.KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG
            params(241) = New SqlParameter("@KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL", SqlDbType.Int)
            params(241).Value = ocustomClass.KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL
            params(242) = New SqlParameter("@KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH", SqlDbType.Int)
            params(242).Value = ocustomClass.KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH
            params(243) = New SqlParameter("@KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG", SqlDbType.Int)
            params(243).Value = ocustomClass.KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG
            params(244) = New SqlParameter("@KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL", SqlDbType.Int)
            params(244).Value = ocustomClass.KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL
            params(245) = New SqlParameter("@KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH", SqlDbType.Int)
            params(245).Value = ocustomClass.KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH
            params(246) = New SqlParameter("@KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG", SqlDbType.Int)
            params(246).Value = ocustomClass.KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG
            params(247) = New SqlParameter("@LB_RG_BRSH_KMPRHNSF_THN_BRJLN_TTL", SqlDbType.Int)
            params(247).Value = ocustomClass.LB_RG_BRSH_KMPRHNSF_THN_BRJLN_TTL
            params(248) = New SqlParameter("@LB_RG_BRSH_KMPRHNSF_THN_BRJLN_NDNSN_RPH", SqlDbType.Int)
            params(248).Value = ocustomClass.LB_RG_BRSH_KMPRHNSF_THN_BRJLN_NDNSN_RPH
            params(249) = New SqlParameter("@LB_RG_BRSH_KMPRHNSF_THN_BRJLN_MT_NG_SNG", SqlDbType.Int)
            params(249).Value = ocustomClass.LB_RG_BRSH_KMPRHNSF_THN_BRJLN_MT_NG_SNG

            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, SaveAdd, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception

        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSIPP1200(ByVal ocustomClass As Parameter.SIPP1200) As Parameter.SIPP1200
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP1200


        params(0) = New SqlParameter("ID", SqlDbType.VarChar, 100)
        params(0).Value = ocustomClass.ID

        Try
            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.D01.GetD01Edit")
        End Try
    End Function

    'Public Function D01Save(ByVal ocustomClass As Parameter.D01) As Parameter.D01
    '    Dim params(38) As SqlParameter
    '    Dim oReturnValue As New Parameter.D01

    '    params(38) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
    '    params(38).Value = ocustomClass.CIF
    '    params(37) = New SqlParameter("JenisIdentitas", SqlDbType.VarChar, 100)
    '    params(37).Value = ocustomClass.JenisIdentitas
    '    params(36) = New SqlParameter("NIK", SqlDbType.VarChar, 100)
    '    params(36).Value = ocustomClass.NIK
    '    params(35) = New SqlParameter("NamaIdentitas", SqlDbType.VarChar, 100)
    '    params(35).Value = ocustomClass.NamaIdentitas
    '    params(34) = New SqlParameter("NamaLengkap", SqlDbType.VarChar, 100)
    '    params(34).Value = ocustomClass.NamaLengkap
    '    params(33) = New SqlParameter("Gelar", SqlDbType.VarChar, 100)
    '    params(33).Value = ocustomClass.Gelar
    '    params(32) = New SqlParameter("JenisKelamin", SqlDbType.VarChar, 100)
    '    params(32).Value = ocustomClass.JenisKelamin
    '    params(31) = New SqlParameter("TempatLahir", SqlDbType.VarChar, 100)
    '    params(31).Value = ocustomClass.TempatLahir
    '    params(30) = New SqlParameter("TanggalLahir", SqlDbType.VarChar, 100)
    '    params(30).Value = ocustomClass.TanggalLahir
    '    params(29) = New SqlParameter("NPWP", SqlDbType.VarChar, 100)
    '    params(29).Value = ocustomClass.NPWP
    '    params(28) = New SqlParameter("Alamat", SqlDbType.VarChar, 100)
    '    params(28).Value = ocustomClass.Alamat
    '    params(27) = New SqlParameter("Kelurahan", SqlDbType.VarChar, 100)
    '    params(27).Value = ocustomClass.Kelurahan
    '    params(26) = New SqlParameter("Kecamatan", SqlDbType.VarChar, 100)
    '    params(26).Value = ocustomClass.Kecamatan
    '    params(25) = New SqlParameter("City", SqlDbType.VarChar, 100)
    '    params(25).Value = ocustomClass.Kota
    '    params(24) = New SqlParameter("ZipCode", SqlDbType.VarChar, 100)
    '    params(24).Value = ocustomClass.KodePos
    '    params(23) = New SqlParameter("Telp", SqlDbType.VarChar, 100)
    '    params(23).Value = ocustomClass.Telp
    '    params(22) = New SqlParameter("Hp", SqlDbType.VarChar, 100)
    '    params(22).Value = ocustomClass.Hp
    '    params(21) = New SqlParameter("Email", SqlDbType.VarChar, 100)
    '    params(21).Value = ocustomClass.Email
    '    params(20) = New SqlParameter("KodeNegara", SqlDbType.VarChar, 100)
    '    params(20).Value = ocustomClass.KodeNegara
    '    params(19) = New SqlParameter("KodePekerjaan", SqlDbType.VarChar, 100)
    '    params(19).Value = ocustomClass.KodePekerjaan
    '    params(18) = New SqlParameter("TempatBekerja", SqlDbType.VarChar, 255)
    '    params(18).Value = ocustomClass.TempatBekerja
    '    params(17) = New SqlParameter("KodeBidangUsaha", SqlDbType.VarChar, 100)
    '    params(17).Value = ocustomClass.KodeBidangUsaha
    '    params(16) = New SqlParameter("AlamatTempatBekerja", SqlDbType.VarChar, 100)
    '    params(16).Value = ocustomClass.AlamatTempatBekerja
    '    params(15) = New SqlParameter("PenghasilanPertahun", SqlDbType.VarChar, 100)
    '    params(15).Value = ocustomClass.PenghasilanPertahun
    '    params(14) = New SqlParameter("KodeSumberPenghasilan", SqlDbType.VarChar, 100)
    '    params(14).Value = ocustomClass.KodeSumberPenghasilan
    '    params(13) = New SqlParameter("JumlahTanggungan", SqlDbType.VarChar, 100)
    '    params(13).Value = ocustomClass.JumlahTanggungan
    '    params(12) = New SqlParameter("KodeHubunganLJK", SqlDbType.VarChar, 100)
    '    params(12).Value = ocustomClass.KodeHubunganLJK
    '    params(11) = New SqlParameter("KodeGolonganDebitur", SqlDbType.VarChar, 100)
    '    params(11).Value = ocustomClass.KodeGolonganDebitur
    '    params(10) = New SqlParameter("StatusPerkawinanDebitur", SqlDbType.VarChar, 100)
    '    params(10).Value = ocustomClass.StatusPerkawinanDebitur
    '    params(9) = New SqlParameter("NikPaspor", SqlDbType.VarChar, 100)
    '    params(9).Value = ocustomClass.NikPaspor
    '    params(8) = New SqlParameter("NamaPasangan", SqlDbType.VarChar, 100)
    '    params(8).Value = ocustomClass.NamaPasangan
    '    params(7) = New SqlParameter("TanggalLahirPasangan", SqlDbType.VarChar, 100)
    '    params(7).Value = ocustomClass.TanggalLahirPasangan
    '    params(6) = New SqlParameter("PerjanjianPisahHarta", SqlDbType.VarChar, 100)
    '    params(6).Value = ocustomClass.PerjanjianPisahHarta
    '    params(5) = New SqlParameter("MelanggarBMPK", SqlDbType.VarChar, 100)
    '    params(5).Value = ocustomClass.MelanggarBMPK
    '    params(4) = New SqlParameter("MelampauiBMPK", SqlDbType.VarChar, 100)
    '    params(4).Value = ocustomClass.MelampauiBMPK
    '    params(3) = New SqlParameter("NamaGadisIbuKandung", SqlDbType.VarChar, 100)
    '    params(3).Value = ocustomClass.NamaGadisIbuKandung
    '    params(2) = New SqlParameter("KodeKantorCabang", SqlDbType.VarChar, 100)
    '    params(2).Value = ocustomClass.KodeKantorCabang
    '    params(1) = New SqlParameter("OperasiData", SqlDbType.VarChar, 100)
    '    params(1).Value = ocustomClass.OperasiData
    '    'params(0) = New SqlParameter("AB", SqlDbType.VarChar, 255)
    '    'params(0).Value = ocustomClass.AB
    '    params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
    '    params(0).Direction = ParameterDirection.Output
    '    Try
    '        SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SAVE, params)
    '    Catch ex As Exception
    '        Throw New Exception("Error On DataAccess.SLIK.D01.D01Save")
    '    End Try
    'End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP1200) As Parameter.SIPP1200
        Dim oReturnValue As New Parameter.SIPP1200
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@valueparent", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.valueparent
        params(1) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.valuemaster
        params(2) = New SqlParameter("@valueelement", SqlDbType.VarChar, 20)
        params(2).Value = oCustomClass.valueelement

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP1200.GetCbo")
        End Try
    End Function
    Public Function SIPP1200Delete(ByVal oCustomClass As Parameter.SIPP1200) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, Delete, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP1200) As Parameter.SIPP1200
        Dim oReturnValue As New Parameter.SIPP1200
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP1200.GetCboBulandataSIPP")
        End Try
    End Function

End Class


