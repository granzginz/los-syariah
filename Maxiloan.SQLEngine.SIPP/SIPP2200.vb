﻿

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class SIPP2200 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP2200List"
    Private Const SaveAdd As String = "spSIPP2200SaveAdd"
    Private Const SaveEdit As String = "spSIPP2200SaveEdit"
    Private Const LIST_EDIT As String = "spSIPP2200EditList"
    Private Const spFillCbo As String = "spGetCboSIPP"
    Private Const Delete As String = "spSIPP2200Delete"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"

#End Region

    Public Function GetSIPP2200(ByVal oCustomClass As Parameter.SIPP2200) As Parameter.SIPP2200
        Dim oReturnValue As New Parameter.SIPP2200
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0010.GetSIPP0010")
        End Try
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP2200) As Parameter.SIPP2200
        Dim oReturnValue As New Parameter.SIPP2200
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2200.GetCboBulandataSIPP")
        End Try
    End Function
    Public Function SIPP2200Delete(ByVal oCustomClass As Parameter.SIPP2200) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.Id
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, Delete, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

    Public Function SIPP2200SaveEdit(ocustomClass As Parameter.SIPP2200) As Parameter.SIPP2200
        Dim params(16) As SqlParameter

        params(0) = New SqlParameter("@NMR_SRT_BRHRG", SqlDbType.VarChar, 25)
            params(0).Value = ocustomClass.NMR_SRT_BRHRG
            params(1) = New SqlParameter("@JNS_SRT_BRHRG", SqlDbType.VarChar, 100)
            params(1).Value = ocustomClass.JNS_SRT_BRHRG
            params(2) = New SqlParameter("@TNGGL_PNRBTN", SqlDbType.Date)
            params(2).Value = ocustomClass.TNGGL_PNRBTN
            params(3) = New SqlParameter("@TNGGL_JTH_TMP", SqlDbType.Date)
            params(3).Value = ocustomClass.TNGGL_JTH_TMP
            params(4) = New SqlParameter("@TJN_KPMLKN", SqlDbType.VarChar, 100)
            params(4).Value = ocustomClass.TJN_KPMLKN
            params(5) = New SqlParameter("@JNS_SK_BNG", SqlDbType.VarChar, 100)
            params(5).Value = ocustomClass.JNS_SK_BNG
            params(6) = New SqlParameter("@TNGKT_SK_BNG", SqlDbType.Decimal)
            params(6).Value = ocustomClass.TNGKT_SK_BNG
            params(7) = New SqlParameter("@JNS_VLT", SqlDbType.VarChar, 100)
            params(7).Value = ocustomClass.JNS_VLT
            params(8) = New SqlParameter("@KUALITAS", SqlDbType.VarChar, 100)
            params(8).Value = ocustomClass.KUALITAS
            params(9) = New SqlParameter("@NL_MT_NG_SL", SqlDbType.Int)
            params(9).Value = ocustomClass.NL_MT_NG_SL
            params(10) = New SqlParameter("@KVLN_RPH", SqlDbType.Int)
            params(10).Value = ocustomClass.KVLN_RPH
            params(11) = New SqlParameter("@NM", SqlDbType.VarChar, 50)
            params(11).Value = ocustomClass.NM
            params(12) = New SqlParameter("@NGR", SqlDbType.VarChar, 100)
            params(12).Value = ocustomClass.NGR
            params(13) = New SqlParameter("@GLNGN", SqlDbType.VarChar, 100)
            params(13).Value = ocustomClass.GLNGN
            params(14) = New SqlParameter("@STTS_KTRKTN", SqlDbType.VarChar, 100)
            params(14).Value = ocustomClass.STTS_KTRKTN
            params(15) = New SqlParameter("@ID", SqlDbType.Int)
            params(15).Value = ocustomClass.ID
            params(16) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(16).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SaveEdit, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2200.SIPP2200SaveEdit")
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function SIPP2200SaveAdd(ocustomClass As Parameter.SIPP2200) As Parameter.SIPP2200
        Dim params(15) As SqlParameter

        Try
            params(0) = New SqlParameter("@NMR_SRT_BRHRG", SqlDbType.VarChar, 25)
            params(0).Value = ocustomClass.NMR_SRT_BRHRG
            params(1) = New SqlParameter("@JNS_SRT_BRHRG", SqlDbType.VarChar, 100)
            params(1).Value = ocustomClass.JNS_SRT_BRHRG
            params(2) = New SqlParameter("@TNGGL_PNRBTN", SqlDbType.Date)
            params(2).Value = ocustomClass.TNGGL_PNRBTN
            params(3) = New SqlParameter("@TNGGL_JTH_TMP", SqlDbType.Date)
            params(3).Value = ocustomClass.TNGGL_JTH_TMP
            params(4) = New SqlParameter("@TJN_KPMLKN", SqlDbType.VarChar, 100)
            params(4).Value = ocustomClass.TJN_KPMLKN
            params(5) = New SqlParameter("@JNS_SK_BNG", SqlDbType.VarChar, 100)
            params(5).Value = ocustomClass.JNS_SK_BNG
            params(6) = New SqlParameter("@TNGKT_SK_BNG", SqlDbType.Decimal)
            params(6).Value = ocustomClass.TNGKT_SK_BNG
            params(7) = New SqlParameter("@JNS_VLT", SqlDbType.VarChar, 100)
            params(7).Value = ocustomClass.JNS_VLT
            params(8) = New SqlParameter("@KUALITAS", SqlDbType.VarChar, 100)
            params(8).Value = ocustomClass.KUALITAS
            params(9) = New SqlParameter("@NL_MT_NG_SL", SqlDbType.Int)
            params(9).Value = ocustomClass.NL_MT_NG_SL
            params(10) = New SqlParameter("@KVLN_RPH", SqlDbType.Int)
            params(10).Value = ocustomClass.KVLN_RPH
            params(11) = New SqlParameter("@NM", SqlDbType.VarChar, 50)
            params(11).Value = ocustomClass.NM
            params(12) = New SqlParameter("@NGR", SqlDbType.VarChar, 100)
            params(12).Value = ocustomClass.NGR
            params(13) = New SqlParameter("@GLNGN", SqlDbType.VarChar, 100)
            params(13).Value = ocustomClass.GLNGN
            params(14) = New SqlParameter("@STTS_KTRKTN", SqlDbType.VarChar, 100)
            params(14).Value = ocustomClass.STTS_KTRKTN
            params(15) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 100)
            params(15).Value = ocustomClass.BULANDATA

            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SaveAdd, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2200.GetSIPP2200SaveAdd")
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSIPP2200Edit(ByVal ocustomClass As Parameter.SIPP2200) As Parameter.SIPP2200
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP2200


        params(0) = New SqlParameter("ID", SqlDbType.VarChar, 100)
        params(0).Value = ocustomClass.Id

        Try
            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2200.GetSIPP2200Edit")
        End Try
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP2200) As Parameter.SIPP2200
        Dim oReturnValue As New Parameter.SIPP2200
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@valueparent", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.valueparent
        params(1) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.valuemaster
        params(2) = New SqlParameter("@valueelement", SqlDbType.VarChar, 20)
        params(2).Value = oCustomClass.valueelement

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.D01.GetCbo")
        End Try
    End Function

    'Public Function Generate(ByVal oCustomClass As Parameter.D01) As Parameter.D01
    '    Dim oReturnValue As New Parameter.D01

    '    'Dim params(0) As SqlParameter
    '    'params(0) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
    '    'params(0).Value = oCustomClass.CIF
    '    Try
    '        oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, Generate_TEXT).Tables(0)

    '        Return oReturnValue
    '    Catch exp As Exception
    '        Throw New Exception("Error On DataAccess.SLIK.D01.Generate")
    '    End Try
    'End Function

End Class


