﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region
Public Class SIPP1110 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP1110List"
    Private Const SAVE As String = "spSIPP1110Save"
    Private Const LIST_EDIT As String = "spSIPP1110EditList"
    Private Const ADD As String = "spSIPP1110Add"
    Private Const FILL_CBO As String = "spGetCboSIPP"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
    Private Const DELETE As String = "spSIPP1110Delete"
#End Region

    Public Function GetSIPP1110(ByVal oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110
        Dim oReturnValue As New Parameter.SIPP1110
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP1110.GetSIPP1110")
        End Try
    End Function

    Public Function GetSelectSIPP1110(ByVal oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@ID", SqlDbType.Int)
            params(0).Value = oCustomClass.ID
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP1110Edit(ByVal oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP1110

        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP1110.SIPP1110Edit")
        End Try
    End Function

    Public Function SIPP1110Save(ByVal oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110
        Dim params(88) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP1110

        params(88) = New SqlParameter("ID", SqlDbType.Int)
        params(88).Value = oCustomClass.ID
        params(87) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_TTL", SqlDbType.BigInt)
        params(87).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_TTL
        params(86) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_NDNSN_RPH", SqlDbType.BigInt)
        params(86).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_NDNSN_RPH
        params(85) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_MT_NG_SNG", SqlDbType.BigInt)
        params(85).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_MT_NG_SNG
        params(84) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_TTL", SqlDbType.BigInt)
        params(84).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_TTL
        params(83) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_NDNSN_RPH", SqlDbType.BigInt)
        params(83).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_NDNSN_RPH
        params(82) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_MT_NG_SNG", SqlDbType.BigInt)
        params(82).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_MT_NG_SNG
        params(81) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_TTL", SqlDbType.BigInt)
        params(81).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_TTL
        params(80) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(80).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_NDNSN_RPH
        params(79) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(79).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_MT_NG_SNG
        params(78) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_TTL", SqlDbType.BigInt)
        params(78).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_TTL
        params(77) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_NDNSN_RPH", SqlDbType.BigInt)
        params(77).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_NDNSN_RPH
        params(76) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_MT_NG_SNG", SqlDbType.BigInt)
        params(76).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_MT_NG_SNG
        params(75) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_TTL", SqlDbType.BigInt)
        params(75).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_TTL
        params(74) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_NDNSN_RPH", SqlDbType.BigInt)
        params(74).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_NDNSN_RPH
        params(73) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_MT_NG_SNG", SqlDbType.BigInt)
        params(73).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_MT_NG_SNG
        params(72) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_TTL", SqlDbType.BigInt)
        params(72).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_TTL
        params(71) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_NDNSN_RPH", SqlDbType.BigInt)
        params(71).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_NDNSN_RPH
        params(70) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_MT_NG_SNG", SqlDbType.BigInt)
        params(70).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_MT_NG_SNG
        params(69) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_TTL", SqlDbType.BigInt)
        params(69).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_TTL
        params(68) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(68).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_NDNSN_RPH
        params(67) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(67).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_MT_NG_SNG
        params(66) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_TTL", SqlDbType.BigInt)
        params(66).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_TTL
        params(65) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_NDNSN_RPH", SqlDbType.BigInt)
        params(65).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_NDNSN_RPH
        params(64) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_MT_NG_SNG", SqlDbType.BigInt)
        params(64).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_MT_NG_SNG
        params(63) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_TTL", SqlDbType.BigInt)
        params(63).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_TTL
        params(62) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_NDNSN_RPH", SqlDbType.BigInt)
        params(62).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_NDNSN_RPH
        params(61) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_MT_NG_SNG", SqlDbType.BigInt)
        params(61).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_MT_NG_SNG
        params(60) = New SqlParameter("@FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_TTL", SqlDbType.BigInt)
        params(60).Value = oCustomClass.FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_TTL
        params(59) = New SqlParameter("@FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_NDNSN_RPH", SqlDbType.BigInt)
        params(59).Value = oCustomClass.FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_NDNSN_RPH
        params(58) = New SqlParameter("@FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_MT_NG_SNG", SqlDbType.BigInt)
        params(58).Value = oCustomClass.FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_MT_NG_SNG
        params(57) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_TTL", SqlDbType.BigInt)
        params(57).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_TTL
        params(56) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_NDNSN_RPH", SqlDbType.BigInt)
        params(56).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_NDNSN_RPH
        params(55) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_MT_NG_SNG", SqlDbType.BigInt)
        params(55).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_MT_NG_SNG
        params(54) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_TTL", SqlDbType.BigInt)
        params(54).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_TTL
        params(53) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_NDNSN_RPH", SqlDbType.BigInt)
        params(53).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_NDNSN_RPH
        params(52) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_MT_NG_SNG", SqlDbType.BigInt)
        params(52).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_MT_NG_SNG
        params(51) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_TTL", SqlDbType.BigInt)
        params(51).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_TTL
        params(50) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_NDNSN_RPH", SqlDbType.BigInt)
        params(50).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_NDNSN_RPH
        params(49) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_MT_NG_SNG", SqlDbType.BigInt)
        params(49).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_MT_NG_SNG
        params(48) = New SqlParameter("@PNRSN_KRDTPMBYN_CHNNLNG_TTL", SqlDbType.BigInt)
        params(48).Value = oCustomClass.PNRSN_KRDTPMBYN_CHNNLNG_TTL
        params(47) = New SqlParameter("@PNRSN_KRDTPMBYN_CHNNLNG_NDNSN_RPH", SqlDbType.BigInt)
        params(47).Value = oCustomClass.PNRSN_KRDTPMBYN_CHNNLNG_NDNSN_RPH
        params(46) = New SqlParameter("@PNRSN_KRDTPMBYN_CHNNLNG_MT_NG_SNG", SqlDbType.BigInt)
        params(46).Value = oCustomClass.PNRSN_KRDTPMBYN_CHNNLNG_MT_NG_SNG
        params(45) = New SqlParameter("@PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_TTL", SqlDbType.BigInt)
        params(45).Value = oCustomClass.PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_TTL
        params(44) = New SqlParameter("@PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_NDNSN_RPH", SqlDbType.BigInt)
        params(44).Value = oCustomClass.PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_NDNSN_RPH
        params(43) = New SqlParameter("@PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_MT_NG_SNG", SqlDbType.BigInt)
        params(43).Value = oCustomClass.PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_MT_NG_SNG
        params(42) = New SqlParameter("@PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_TTL", SqlDbType.BigInt)
        params(42).Value = oCustomClass.PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_TTL
        params(41) = New SqlParameter("@PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_NDNSN_RPH", SqlDbType.BigInt)
        params(41).Value = oCustomClass.PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_NDNSN_RPH
        params(40) = New SqlParameter("@PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_MT_NG_SNG", SqlDbType.BigInt)
        params(40).Value = oCustomClass.PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_MT_NG_SNG
        params(39) = New SqlParameter("@NMNL_NTRST_RT_SWP_TTL", SqlDbType.BigInt)
        params(39).Value = oCustomClass.NMNL_NTRST_RT_SWP_TTL
        params(38) = New SqlParameter("@NMNL_NTRST_RT_SWP_NDNSN_RPH", SqlDbType.BigInt)
        params(38).Value = oCustomClass.NMNL_NTRST_RT_SWP_NDNSN_RPH
        params(37) = New SqlParameter("@NMNL_NTRST_RT_SWP_MT_NG_SNG", SqlDbType.BigInt)
        params(37).Value = oCustomClass.NMNL_NTRST_RT_SWP_MT_NG_SNG
        params(36) = New SqlParameter("@NMNL_CRRNCY_SWP_TTL", SqlDbType.BigInt)
        params(36).Value = oCustomClass.NMNL_CRRNCY_SWP_TTL
        params(35) = New SqlParameter("@NMNL_CRRNCY_SWP_NDNSN_RPH", SqlDbType.BigInt)
        params(35).Value = oCustomClass.NMNL_CRRNCY_SWP_NDNSN_RPH
        params(34) = New SqlParameter("@NMNL_CRRNCY_SWP_MT_NG_SNG", SqlDbType.BigInt)
        params(34).Value = oCustomClass.NMNL_CRRNCY_SWP_MT_NG_SNG
        params(33) = New SqlParameter("@NMNL_CRSS_CRRNCY_SWP_TTL", SqlDbType.BigInt)
        params(33).Value = oCustomClass.NMNL_CRSS_CRRNCY_SWP_TTL
        params(32) = New SqlParameter("@NMNL_CRSS_CRRNCY_SWP_NDNSN_RPH", SqlDbType.BigInt)
        params(32).Value = oCustomClass.NMNL_CRSS_CRRNCY_SWP_NDNSN_RPH
        params(31) = New SqlParameter("@NMNL_CRSS_CRRNCY_SWP_MT_NG_SNG", SqlDbType.BigInt)
        params(31).Value = oCustomClass.NMNL_CRSS_CRRNCY_SWP_MT_NG_SNG
        params(30) = New SqlParameter("@NMNL_FRWRD_TTL", SqlDbType.BigInt)
        params(30).Value = oCustomClass.NMNL_FRWRD_TTL
        params(29) = New SqlParameter("@NMNL_FRWRD_NDNSN_RPH", SqlDbType.BigInt)
        params(29).Value = oCustomClass.NMNL_FRWRD_NDNSN_RPH
        params(28) = New SqlParameter("@NMNL_FRWRD_MT_NG_SNG", SqlDbType.BigInt)
        params(28).Value = oCustomClass.NMNL_FRWRD_MT_NG_SNG
        params(27) = New SqlParameter("@NMNL_PTN_TTL", SqlDbType.BigInt)
        params(27).Value = oCustomClass.NMNL_PTN_TTL
        params(26) = New SqlParameter("@NMNL_PTN_NDNSN_RPH", SqlDbType.BigInt)
        params(26).Value = oCustomClass.NMNL_PTN_NDNSN_RPH
        params(25) = New SqlParameter("@NMNL_PTN_MT_NG_SNG", SqlDbType.BigInt)
        params(25).Value = oCustomClass.NMNL_PTN_MT_NG_SNG
        params(24) = New SqlParameter("@NMNL_FTR_TTL", SqlDbType.BigInt)
        params(24).Value = oCustomClass.NMNL_FTR_TTL
        params(23) = New SqlParameter("@NMNL_FTR_NDNSN_RPH", SqlDbType.BigInt)
        params(23).Value = oCustomClass.NMNL_FTR_NDNSN_RPH
        params(22) = New SqlParameter("@NMNL_FTR_MT_NG_SNG", SqlDbType.BigInt)
        params(22).Value = oCustomClass.NMNL_FTR_MT_NG_SNG
        params(21) = New SqlParameter("@NMNL_DRVTF_LNNY_TTL", SqlDbType.BigInt)
        params(21).Value = oCustomClass.NMNL_DRVTF_LNNY_TTL
        params(20) = New SqlParameter("@NMNL_DRVTF_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(20).Value = oCustomClass.NMNL_DRVTF_LNNY_NDNSN_RPH
        params(19) = New SqlParameter("@NMNL_DRVTF_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(19).Value = oCustomClass.NMNL_DRVTF_LNNY_MT_NG_SNG
        params(18) = New SqlParameter("@NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_TTL", SqlDbType.BigInt)
        params(18).Value = oCustomClass.NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_TTL
        params(17) = New SqlParameter("@NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_NDNSN_RPH", SqlDbType.BigInt)
        params(17).Value = oCustomClass.NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_NDNSN_RPH
        params(16) = New SqlParameter("@NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_MT_NG_SNG", SqlDbType.BigInt)
        params(16).Value = oCustomClass.NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_MT_NG_SNG
        params(15) = New SqlParameter("@PTNG_PMBYN_HPS_BK_TTL", SqlDbType.BigInt)
        params(15).Value = oCustomClass.PTNG_PMBYN_HPS_BK_TTL
        params(14) = New SqlParameter("@PTNG_PMBYN_HPS_BK_NDNSN_RPH", SqlDbType.BigInt)
        params(14).Value = oCustomClass.PTNG_PMBYN_HPS_BK_NDNSN_RPH
        params(13) = New SqlParameter("@PTNG_PMBYN_HPS_BK_MT_NG_SNG", SqlDbType.BigInt)
        params(13).Value = oCustomClass.PTNG_PMBYN_HPS_BK_MT_NG_SNG
        params(12) = New SqlParameter("@PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_TTL", SqlDbType.BigInt)
        params(12).Value = oCustomClass.PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_TTL
        params(11) = New SqlParameter("@PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_NDNSN_RPH", SqlDbType.BigInt)
        params(11).Value = oCustomClass.PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_NDNSN_RPH
        params(10) = New SqlParameter("@PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_MT_NG_SNG", SqlDbType.BigInt)
        params(10).Value = oCustomClass.PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_MT_NG_SNG
        params(9) = New SqlParameter("@PTNG_PMBYN_HPS_TGH_TTL", SqlDbType.BigInt)
        params(9).Value = oCustomClass.PTNG_PMBYN_HPS_TGH_TTL
        params(8) = New SqlParameter("@PTNG_PMBYN_HPS_TGH_NDNSN_RPH", SqlDbType.BigInt)
        params(8).Value = oCustomClass.PTNG_PMBYN_HPS_TGH_NDNSN_RPH
        params(7) = New SqlParameter("@PTNG_PMBYN_HPS_TGH_MT_NG_SNG", SqlDbType.BigInt)
        params(7).Value = oCustomClass.PTNG_PMBYN_HPS_TGH_MT_NG_SNG
        params(6) = New SqlParameter("@RKNNG_DMNSTRTF_LNNY_TTL", SqlDbType.BigInt)
        params(6).Value = oCustomClass.RKNNG_DMNSTRTF_LNNY_TTL
        params(5) = New SqlParameter("@RKNNG_DMNSTRTF_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(5).Value = oCustomClass.RKNNG_DMNSTRTF_LNNY_NDNSN_RPH
        params(4) = New SqlParameter("@RKNNG_DMNSTRTF_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(4).Value = oCustomClass.RKNNG_DMNSTRTF_LNNY_MT_NG_SNG
        params(3) = New SqlParameter("@RKNNG_DMNSTRTF_TTL", SqlDbType.BigInt)
        params(3).Value = oCustomClass.RKNNG_DMNSTRTF_TTL
        params(2) = New SqlParameter("@RKNNG_DMNSTRTF_NDNSN_RPH", SqlDbType.BigInt)
        params(2).Value = oCustomClass.RKNNG_DMNSTRTF_NDNSN_RPH
        params(1) = New SqlParameter("@RKNNG_DMNSTRTF_MT_NG_SNG", SqlDbType.BigInt)
        params(1).Value = oCustomClass.RKNNG_DMNSTRTF_MT_NG_SNG
        params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP1110.SIPP1110Save")
        End Try
    End Function

    Public Function SIPP1110Add(ByVal oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110
        Dim params(87) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP1110

        params(0) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_TTL", SqlDbType.BigInt)
        params(0).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_TTL
        params(1) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_NDNSN_RPH", SqlDbType.BigInt)
        params(1).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_NDNSN_RPH
        params(2) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_MT_NG_SNG", SqlDbType.BigInt)
        params(2).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_MT_NG_SNG
        params(3) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_TTL", SqlDbType.BigInt)
        params(3).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_TTL
        params(4) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_NDNSN_RPH", SqlDbType.BigInt)
        params(4).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_NDNSN_RPH
        params(5) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_MT_NG_SNG", SqlDbType.BigInt)
        params(5).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_MT_NG_SNG
        params(6) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_TTL", SqlDbType.BigInt)
        params(6).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_TTL
        params(7) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(7).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_NDNSN_RPH
        params(8) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(8).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_MT_NG_SNG
        params(9) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_TTL", SqlDbType.BigInt)
        params(9).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_TTL
        params(10) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_NDNSN_RPH", SqlDbType.BigInt)
        params(10).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_NDNSN_RPH
        params(11) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_MT_NG_SNG", SqlDbType.BigInt)
        params(11).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_MT_NG_SNG
        params(12) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_TTL", SqlDbType.BigInt)
        params(12).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_TTL
        params(13) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_NDNSN_RPH", SqlDbType.BigInt)
        params(13).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_NDNSN_RPH
        params(14) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_MT_NG_SNG", SqlDbType.BigInt)
        params(14).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_MT_NG_SNG
        params(15) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_TTL", SqlDbType.BigInt)
        params(15).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_TTL
        params(16) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_NDNSN_RPH", SqlDbType.BigInt)
        params(16).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_NDNSN_RPH
        params(17) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_MT_NG_SNG", SqlDbType.BigInt)
        params(17).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_MT_NG_SNG
        params(18) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_TTL", SqlDbType.BigInt)
        params(18).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_TTL
        params(19) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(19).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_NDNSN_RPH
        params(20) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(20).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_MT_NG_SNG
        params(21) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_TTL", SqlDbType.BigInt)
        params(21).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_TTL
        params(22) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_NDNSN_RPH", SqlDbType.BigInt)
        params(22).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_NDNSN_RPH
        params(23) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_MT_NG_SNG", SqlDbType.BigInt)
        params(23).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_MT_NG_SNG
        params(24) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_TTL", SqlDbType.BigInt)
        params(24).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_TTL
        params(25) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_NDNSN_RPH", SqlDbType.BigInt)
        params(25).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_NDNSN_RPH
        params(26) = New SqlParameter("@FSLTS_PNDNN_YNG_BLM_DTRK_MT_NG_SNG", SqlDbType.BigInt)
        params(26).Value = oCustomClass.FSLTS_PNDNN_YNG_BLM_DTRK_MT_NG_SNG
        params(27) = New SqlParameter("@FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_TTL", SqlDbType.BigInt)
        params(27).Value = oCustomClass.FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_TTL
        params(28) = New SqlParameter("@FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_NDNSN_RPH", SqlDbType.BigInt)
        params(28).Value = oCustomClass.FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_NDNSN_RPH
        params(29) = New SqlParameter("@FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_MT_NG_SNG", SqlDbType.BigInt)
        params(29).Value = oCustomClass.FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_MT_NG_SNG
        params(30) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_TTL", SqlDbType.BigInt)
        params(30).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_TTL
        params(31) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_NDNSN_RPH", SqlDbType.BigInt)
        params(31).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_NDNSN_RPH
        params(32) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_MT_NG_SNG", SqlDbType.BigInt)
        params(32).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_MT_NG_SNG
        params(33) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_TTL", SqlDbType.BigInt)
        params(33).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_TTL
        params(34) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_NDNSN_RPH", SqlDbType.BigInt)
        params(34).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_NDNSN_RPH
        params(35) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_MT_NG_SNG", SqlDbType.BigInt)
        params(35).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_MT_NG_SNG
        params(36) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_TTL", SqlDbType.BigInt)
        params(36).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_TTL
        params(37) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_NDNSN_RPH", SqlDbType.BigInt)
        params(37).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_NDNSN_RPH
        params(38) = New SqlParameter("@PNRBTN_SRT_SNGGP_BYR_MT_NG_SNG", SqlDbType.BigInt)
        params(38).Value = oCustomClass.PNRBTN_SRT_SNGGP_BYR_MT_NG_SNG
        params(39) = New SqlParameter("@PNRSN_KRDTPMBYN_CHNNLNG_TTL", SqlDbType.BigInt)
        params(39).Value = oCustomClass.PNRSN_KRDTPMBYN_CHNNLNG_TTL
        params(40) = New SqlParameter("@PNRSN_KRDTPMBYN_CHNNLNG_NDNSN_RPH", SqlDbType.BigInt)
        params(40).Value = oCustomClass.PNRSN_KRDTPMBYN_CHNNLNG_NDNSN_RPH
        params(41) = New SqlParameter("@PNRSN_KRDTPMBYN_CHNNLNG_MT_NG_SNG", SqlDbType.BigInt)
        params(41).Value = oCustomClass.PNRSN_KRDTPMBYN_CHNNLNG_MT_NG_SNG
        params(42) = New SqlParameter("@PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_TTL", SqlDbType.BigInt)
        params(42).Value = oCustomClass.PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_TTL
        params(43) = New SqlParameter("@PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_NDNSN_RPH", SqlDbType.BigInt)
        params(43).Value = oCustomClass.PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_NDNSN_RPH
        params(44) = New SqlParameter("@PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_MT_NG_SNG", SqlDbType.BigInt)
        params(44).Value = oCustomClass.PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_MT_NG_SNG
        params(45) = New SqlParameter("@PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_TTL", SqlDbType.BigInt)
        params(45).Value = oCustomClass.PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_TTL
        params(46) = New SqlParameter("@PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_NDNSN_RPH", SqlDbType.BigInt)
        params(46).Value = oCustomClass.PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_NDNSN_RPH
        params(47) = New SqlParameter("@PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_MT_NG_SNG", SqlDbType.BigInt)
        params(47).Value = oCustomClass.PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_MT_NG_SNG
        params(48) = New SqlParameter("@NMNL_NTRST_RT_SWP_TTL", SqlDbType.BigInt)
        params(48).Value = oCustomClass.NMNL_NTRST_RT_SWP_TTL
        params(49) = New SqlParameter("@NMNL_NTRST_RT_SWP_NDNSN_RPH", SqlDbType.BigInt)
        params(49).Value = oCustomClass.NMNL_NTRST_RT_SWP_NDNSN_RPH
        params(50) = New SqlParameter("@NMNL_NTRST_RT_SWP_MT_NG_SNG", SqlDbType.BigInt)
        params(50).Value = oCustomClass.NMNL_NTRST_RT_SWP_MT_NG_SNG
        params(51) = New SqlParameter("@NMNL_CRRNCY_SWP_TTL", SqlDbType.BigInt)
        params(51).Value = oCustomClass.NMNL_CRRNCY_SWP_TTL
        params(52) = New SqlParameter("@NMNL_CRRNCY_SWP_NDNSN_RPH", SqlDbType.BigInt)
        params(52).Value = oCustomClass.NMNL_CRRNCY_SWP_NDNSN_RPH
        params(53) = New SqlParameter("@NMNL_CRRNCY_SWP_MT_NG_SNG", SqlDbType.BigInt)
        params(53).Value = oCustomClass.NMNL_CRRNCY_SWP_MT_NG_SNG
        params(54) = New SqlParameter("@NMNL_CRSS_CRRNCY_SWP_TTL", SqlDbType.BigInt)
        params(54).Value = oCustomClass.NMNL_CRSS_CRRNCY_SWP_TTL
        params(55) = New SqlParameter("@NMNL_CRSS_CRRNCY_SWP_NDNSN_RPH", SqlDbType.BigInt)
        params(55).Value = oCustomClass.NMNL_CRSS_CRRNCY_SWP_NDNSN_RPH
        params(56) = New SqlParameter("@NMNL_CRSS_CRRNCY_SWP_MT_NG_SNG", SqlDbType.BigInt)
        params(56).Value = oCustomClass.NMNL_CRSS_CRRNCY_SWP_MT_NG_SNG
        params(57) = New SqlParameter("@NMNL_FRWRD_TTL", SqlDbType.BigInt)
        params(57).Value = oCustomClass.NMNL_FRWRD_TTL
        params(58) = New SqlParameter("@NMNL_FRWRD_NDNSN_RPH", SqlDbType.BigInt)
        params(58).Value = oCustomClass.NMNL_FRWRD_NDNSN_RPH
        params(59) = New SqlParameter("@NMNL_FRWRD_MT_NG_SNG", SqlDbType.BigInt)
        params(59).Value = oCustomClass.NMNL_FRWRD_MT_NG_SNG
        params(60) = New SqlParameter("@NMNL_PTN_TTL", SqlDbType.BigInt)
        params(60).Value = oCustomClass.NMNL_PTN_TTL
        params(61) = New SqlParameter("@NMNL_PTN_NDNSN_RPH", SqlDbType.BigInt)
        params(61).Value = oCustomClass.NMNL_PTN_NDNSN_RPH
        params(62) = New SqlParameter("@NMNL_PTN_MT_NG_SNG", SqlDbType.BigInt)
        params(62).Value = oCustomClass.NMNL_PTN_MT_NG_SNG
        params(63) = New SqlParameter("@NMNL_FTR_TTL", SqlDbType.BigInt)
        params(63).Value = oCustomClass.NMNL_FTR_TTL
        params(64) = New SqlParameter("@NMNL_FTR_NDNSN_RPH", SqlDbType.BigInt)
        params(64).Value = oCustomClass.NMNL_FTR_NDNSN_RPH
        params(65) = New SqlParameter("@NMNL_FTR_MT_NG_SNG", SqlDbType.BigInt)
        params(65).Value = oCustomClass.NMNL_FTR_MT_NG_SNG
        params(66) = New SqlParameter("@NMNL_DRVTF_LNNY_TTL", SqlDbType.BigInt)
        params(66).Value = oCustomClass.NMNL_DRVTF_LNNY_TTL
        params(67) = New SqlParameter("@NMNL_DRVTF_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(67).Value = oCustomClass.NMNL_DRVTF_LNNY_NDNSN_RPH
        params(68) = New SqlParameter("@NMNL_DRVTF_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(68).Value = oCustomClass.NMNL_DRVTF_LNNY_MT_NG_SNG
        params(69) = New SqlParameter("@NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_TTL", SqlDbType.BigInt)
        params(69).Value = oCustomClass.NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_TTL
        params(70) = New SqlParameter("@NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_NDNSN_RPH", SqlDbType.BigInt)
        params(70).Value = oCustomClass.NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_NDNSN_RPH
        params(71) = New SqlParameter("@NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_MT_NG_SNG", SqlDbType.BigInt)
        params(71).Value = oCustomClass.NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_MT_NG_SNG
        params(72) = New SqlParameter("@PTNG_PMBYN_HPS_BK_TTL", SqlDbType.BigInt)
        params(72).Value = oCustomClass.PTNG_PMBYN_HPS_BK_TTL
        params(73) = New SqlParameter("@PTNG_PMBYN_HPS_BK_NDNSN_RPH", SqlDbType.BigInt)
        params(73).Value = oCustomClass.PTNG_PMBYN_HPS_BK_NDNSN_RPH
        params(74) = New SqlParameter("@PTNG_PMBYN_HPS_BK_MT_NG_SNG", SqlDbType.BigInt)
        params(74).Value = oCustomClass.PTNG_PMBYN_HPS_BK_MT_NG_SNG
        params(75) = New SqlParameter("@PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_TTL", SqlDbType.BigInt)
        params(75).Value = oCustomClass.PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_TTL
        params(76) = New SqlParameter("@PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_NDNSN_RPH", SqlDbType.BigInt)
        params(76).Value = oCustomClass.PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_NDNSN_RPH
        params(77) = New SqlParameter("@PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_MT_NG_SNG", SqlDbType.BigInt)
        params(77).Value = oCustomClass.PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_MT_NG_SNG
        params(78) = New SqlParameter("@PTNG_PMBYN_HPS_TGH_TTL", SqlDbType.BigInt)
        params(78).Value = oCustomClass.PTNG_PMBYN_HPS_TGH_TTL
        params(79) = New SqlParameter("@PTNG_PMBYN_HPS_TGH_NDNSN_RPH", SqlDbType.BigInt)
        params(79).Value = oCustomClass.PTNG_PMBYN_HPS_TGH_NDNSN_RPH
        params(80) = New SqlParameter("@PTNG_PMBYN_HPS_TGH_MT_NG_SNG", SqlDbType.BigInt)
        params(80).Value = oCustomClass.PTNG_PMBYN_HPS_TGH_MT_NG_SNG
        params(81) = New SqlParameter("@RKNNG_DMNSTRTF_LNNY_TTL", SqlDbType.BigInt)
        params(81).Value = oCustomClass.RKNNG_DMNSTRTF_LNNY_TTL
        params(82) = New SqlParameter("@RKNNG_DMNSTRTF_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(82).Value = oCustomClass.RKNNG_DMNSTRTF_LNNY_NDNSN_RPH
        params(83) = New SqlParameter("@RKNNG_DMNSTRTF_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(83).Value = oCustomClass.RKNNG_DMNSTRTF_LNNY_MT_NG_SNG
        params(84) = New SqlParameter("@RKNNG_DMNSTRTF_TTL", SqlDbType.BigInt)
        params(84).Value = oCustomClass.RKNNG_DMNSTRTF_TTL
        params(85) = New SqlParameter("@RKNNG_DMNSTRTF_NDNSN_RPH", SqlDbType.BigInt)
        params(85).Value = oCustomClass.RKNNG_DMNSTRTF_NDNSN_RPH
        params(86) = New SqlParameter("@RKNNG_DMNSTRTF_MT_NG_SNG", SqlDbType.BigInt)
        params(86).Value = oCustomClass.RKNNG_DMNSTRTF_MT_NG_SNG
        params(87) = New SqlParameter("@BULANDATA", SqlDbType.BigInt)
        params(87).Value = oCustomClass.BULANDATA

        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, ADD, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP1110.SIPP1110Add")
        End Try

    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110
        Dim oReturnValue As New Parameter.SIPP1110
        Dim params(2) As SqlParameter

        params(0) = New SqlParameter("@valueelement", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Valueelement
        params(1) = New SqlParameter("@valueparent", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.Valueparent
        params(2) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 50)
        params(2).Value = oCustomClass.Valuemaster
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2490.GetCbo")
        End Try
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110
        Dim oReturnValue As New Parameter.SIPP1110
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP1110.GetCboBulandataSIPP")
        End Try
    End Function

    Public Function SIPP1110Delete(ByVal oCustomClass As Parameter.SIPP1110) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

End Class

