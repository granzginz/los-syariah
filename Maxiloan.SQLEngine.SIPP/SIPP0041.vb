﻿

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class SIPP0041 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const"
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP0041EditList"
    Private Const SaveAdd As String = "spSIPP0041SaveAdd"
    Private Const SaveEdit As String = "spSIPP0041SaveEdit"
    Private Const LIST As String = "spSIPP0041List"
    Private Const spFillCbo As String = "spGetCboSIPP"
    Private Const Delete As String = "spSIPP0041Delete"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"

#End Region

    Public Function GetSIPP0041(ByVal oCustomClass As Parameter.SIPP0041) As Parameter.SIPP0041
        Dim oReturnValue As New Parameter.SIPP0041
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0041.GetSIPP0041")
        End Try
    End Function

    Public Function SIPP0041SaveEdit(ocustomClass As Parameter.SIPP0041) As Parameter.SIPP0041
        Dim params(191) As SqlParameter
        Try
            params(0) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_", SqlDbType.Int)
            params(0).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_
            params(1) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(1).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK
            params(2) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(2).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN
            params(3) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_", SqlDbType.Int)
            params(3).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_
            params(4) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(4).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK
            params(5) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(5).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN
            params(6) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_", SqlDbType.Int)
            params(6).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_
            params(7) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(7).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK
            params(8) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(8).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN
            params(9) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(9).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL
            params(10) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(10).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK
            params(11) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(11).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN
            params(12) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(12).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL
            params(13) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(13).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK
            params(14) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(14).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN
            params(15) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(15).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL
            params(16) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(16).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK
            params(17) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(17).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN
            params(18) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(18).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL
            params(19) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(19).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK
            params(20) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(20).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN
            params(21) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(21).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL
            params(22) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(22).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK
            params(23) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(23).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN
            params(24) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(24).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL
            params(25) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(25).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK
            params(26) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(26).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN
            params(27) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(27).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL
            params(28) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(28).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK
            params(29) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(29).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN
            params(30) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(30).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL
            params(31) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(31).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK
            params(32) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(32).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN
            params(33) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(33).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL
            params(34) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(34).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK
            params(35) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(35).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN
            params(36) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(36).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL
            params(37) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(37).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK
            params(38) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(38).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN
            params(39) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(39).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL
            params(40) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(40).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK
            params(41) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(41).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN
            params(42) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(42).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL
            params(43) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(43).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK
            params(44) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(44).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN
            params(45) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(45).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL
            params(46) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(46).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK
            params(47) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(47).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN
            params(48) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(48).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL
            params(49) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(49).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK
            params(50) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(50).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN
            params(51) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(51).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL
            params(52) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(52).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK
            params(53) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(53).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN

            params(54) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_", SqlDbType.Int)
            params(54).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_
            params(55) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(55).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK
            params(56) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(56).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN
            params(57) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_", SqlDbType.Int)
            params(57).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_
            params(58) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(58).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK
            params(59) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(59).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN
            params(60) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_", SqlDbType.Int)
            params(60).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_
            params(61) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(61).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK
            params(62) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(62).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN
            params(63) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(63).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL
            params(64) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(64).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK
            params(65) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(65).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN
            params(66) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(66).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL
            params(67) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(67).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK
            params(68) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(68).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN
            params(69) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(69).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL
            params(70) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(70).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK
            params(71) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(71).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN
            params(72) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(72).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL
            params(73) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(73).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK
            params(74) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(74).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN
            params(75) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(75).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL
            params(76) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(76).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK
            params(77) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(77).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN
            params(78) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(78).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL
            params(79) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(79).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK
            params(80) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(80).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN
            params(81) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(81).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL
            params(82) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(82).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK
            params(83) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(83).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN
            params(84) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(84).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL
            params(85) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(85).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK
            params(86) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(86).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN
            params(87) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(87).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL
            params(88) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(88).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK
            params(89) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(89).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN
            params(90) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(90).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL
            params(91) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(91).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK
            params(92) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(92).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN
            params(93) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(93).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL
            params(94) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(94).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK
            params(95) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(95).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN
            params(96) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(96).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL
            params(97) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(97).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK
            params(98) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(98).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN
            params(99) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(99).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL
            params(100) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(100).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK
            params(101) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(101).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN
            params(102) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(102).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL
            params(103) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(103).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK
            params(104) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(104).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN
            params(105) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(105).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL
            params(106) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(106).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK
            params(107) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(107).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN
            params(108) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_", SqlDbType.Int)
            params(108).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_
            params(109) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(109).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK
            params(110) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(110).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN
            params(111) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_", SqlDbType.Int)
            params(111).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_
            params(112) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(112).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK
            params(113) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(113).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN
            params(114) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_", SqlDbType.Int)
            params(114).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_
            params(115) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(115).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK
            params(116) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(116).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN
            params(117) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(117).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL
            params(118) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(118).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK
            params(119) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(119).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN
            params(120) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(120).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL
            params(121) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(121).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK
            params(122) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(122).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN
            params(123) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(123).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL
            params(124) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(124).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK
            params(125) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(125).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN
            params(126) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(126).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL
            params(127) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(127).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK
            params(128) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(128).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN
            params(129) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(129).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL
            params(130) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(130).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK
            params(131) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(131).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN
            params(132) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(132).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL
            params(133) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(133).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK
            params(134) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(134).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN
            params(135) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(135).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL
            params(136) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(136).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK
            params(137) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(137).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN
            params(138) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(138).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL
            params(139) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(139).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK
            params(140) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(140).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN
            params(141) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(141).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL
            params(142) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(142).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK
            params(143) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(143).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN
            params(144) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(144).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL
            params(145) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(145).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK
            params(146) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(146).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN
            params(147) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(147).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL
            params(148) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(148).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK
            params(149) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(149).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN
            params(150) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(150).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL
            params(151) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(151).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK
            params(152) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(152).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN
            params(153) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(153).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL
            params(154) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(154).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK
            params(155) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(155).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN
            params(156) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(156).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL
            params(157) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(157).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK
            params(158) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(158).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN
            params(159) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(159).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL
            params(160) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(160).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK
            params(161) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(161).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN
            params(162) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL__", SqlDbType.Int)
            params(162).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL__
            params(163) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_", SqlDbType.Int)
            params(163).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_
            params(164) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TTL_", SqlDbType.Int)
            params(164).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TTL_
            params(165) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TTL_", SqlDbType.Int)
            params(165).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TTL_
            params(166) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TTL_", SqlDbType.Int)
            params(166).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TTL_
            params(167) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TTL_", SqlDbType.Int)
            params(167).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TTL_
            params(168) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL__", SqlDbType.Int)
            params(168).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL__
            params(169) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_", SqlDbType.Int)
            params(169).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_
            params(170) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TTL_", SqlDbType.Int)
            params(170).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TTL_
            params(171) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TTL_", SqlDbType.Int)
            params(171).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TTL_
            params(172) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TTL_", SqlDbType.Int)
            params(172).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TTL_
            params(173) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TTL_", SqlDbType.Int)
            params(173).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TTL_
            params(174) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL__", SqlDbType.Int)
            params(174).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL__
            params(175) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_", SqlDbType.Int)
            params(175).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_
            params(176) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TTL_", SqlDbType.Int)
            params(176).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TTL_
            params(177) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TTL_", SqlDbType.Int)
            params(177).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TTL_
            params(178) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TTL_", SqlDbType.Int)
            params(178).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TTL_
            params(179) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TTL_", SqlDbType.Int)
            params(179).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TTL_
            params(180) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_", SqlDbType.Int)
            params(180).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_
            params(181) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(181).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_LKLK
            params(182) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(182).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_PRMPN
            params(183) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_", SqlDbType.Int)
            params(183).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_
            params(184) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(184).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_LKLK
            params(185) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(185).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_PRMPN
            params(186) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_", SqlDbType.Int)
            params(186).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_
            params(187) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(187).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_LKLK
            params(188) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(188).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_PRMPN
            params(189) = New SqlParameter("@JMLH_TNG_KRJ_TTL__", SqlDbType.Int)
            params(189).Value = ocustomClass.JMLH_TNG_KRJ_TTL__

            params(190) = New SqlParameter("@ID", SqlDbType.Int)
            params(190).Value = ocustomClass.ID
            params(191) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(191).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SaveEdit, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0041.SIPP0041SaveEdit")
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function SIPP0041SaveAdd(ocustomClass As Parameter.SIPP0041) As Parameter.SIPP0041
        Dim params(190) As SqlParameter

        params(0) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_", SqlDbType.Int)
            params(0).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_
            params(1) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(1).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK
            params(2) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(2).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN
            params(3) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_", SqlDbType.Int)
            params(3).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_
            params(4) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(4).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK
            params(5) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(5).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN
            params(6) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_", SqlDbType.Int)
            params(6).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_
            params(7) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(7).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK
            params(8) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(8).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN
            params(9) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(9).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL
            params(10) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(10).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK
            params(11) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(11).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN
            params(12) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(12).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL
            params(13) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(13).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK
            params(14) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(14).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN
            params(15) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(15).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL
            params(16) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(16).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK
            params(17) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(17).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN
            params(18) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(18).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL
            params(19) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(19).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK
            params(20) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(20).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN
            params(21) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(21).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL
            params(22) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(22).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK
            params(23) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(23).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN
            params(24) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(24).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL
            params(25) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(25).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK
            params(26) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(26).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN
            params(27) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(27).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL
            params(28) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(28).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK
            params(29) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(29).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN
            params(30) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(30).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL
            params(31) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(31).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK
            params(32) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(32).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN
            params(33) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(33).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL
            params(34) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(34).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK
            params(35) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(35).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN
            params(36) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(36).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL
            params(37) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(37).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK
            params(38) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(38).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN
            params(39) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(39).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL
            params(40) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(40).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK
            params(41) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(41).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN
            params(42) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(42).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL
            params(43) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(43).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK
            params(44) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(44).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN
            params(45) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(45).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL
            params(46) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(46).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK
            params(47) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(47).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN
            params(48) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(48).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL
            params(49) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(49).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK
            params(50) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(50).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN
            params(51) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(51).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL
            params(52) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(52).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK
            params(53) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(53).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN

            params(54) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_", SqlDbType.Int)
            params(54).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_
            params(55) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(55).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK
            params(56) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(56).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN
            params(57) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_", SqlDbType.Int)
            params(57).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_
            params(58) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(58).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK
            params(59) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(59).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN
            params(60) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_", SqlDbType.Int)
            params(60).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_
            params(61) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(61).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK
            params(62) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(62).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN
            params(63) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(63).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL
            params(64) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(64).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK
            params(65) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(65).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN
            params(66) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(66).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL
            params(67) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(67).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK
            params(68) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(68).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN
            params(69) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(69).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL
            params(70) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(70).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK
            params(71) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(71).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN
            params(72) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(72).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL
            params(73) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(73).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK
            params(74) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(74).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN
            params(75) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(75).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL
            params(76) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(76).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK
            params(77) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(77).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN
            params(78) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(78).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL
            params(79) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(79).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK
            params(80) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(80).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN
            params(81) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(81).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL
            params(82) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(82).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK
            params(83) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(83).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN
            params(84) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(84).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL
            params(85) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(85).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK
            params(86) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(86).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN
            params(87) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(87).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL
            params(88) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(88).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK
            params(89) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(89).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN
            params(90) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(90).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL
            params(91) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(91).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK
            params(92) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(92).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN
            params(93) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(93).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL
            params(94) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(94).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK
            params(95) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(95).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN
            params(96) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(96).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL
            params(97) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(97).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK
            params(98) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(98).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN
            params(99) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(99).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL
            params(100) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(100).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK
            params(101) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(101).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN
            params(102) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(102).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL
            params(103) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(103).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK
            params(104) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(104).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN
            params(105) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(105).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL
            params(106) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(106).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK
            params(107) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(107).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN
            params(108) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_", SqlDbType.Int)
            params(108).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_
            params(109) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(109).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK
            params(110) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(110).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN
            params(111) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_", SqlDbType.Int)
            params(111).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_
            params(112) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(112).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK
            params(113) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(113).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN
            params(114) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_", SqlDbType.Int)
            params(114).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_
            params(115) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(115).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK
            params(116) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(116).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN
            params(117) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(117).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL
            params(118) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(118).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK
            params(119) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(119).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN
            params(120) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(120).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL
            params(121) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(121).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK
            params(122) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(122).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN
            params(123) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(123).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL
            params(124) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(124).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK
            params(125) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(125).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN
            params(126) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(126).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL
            params(127) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(127).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK
            params(128) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(128).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN
            params(129) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(129).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL
            params(130) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(130).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK
            params(131) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(131).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN
            params(132) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(132).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL
            params(133) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(133).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK
            params(134) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(134).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN
            params(135) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(135).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL
            params(136) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(136).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK
            params(137) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(137).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN
            params(138) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(138).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL
            params(139) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(139).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK
            params(140) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(140).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN
            params(141) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(141).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL
            params(142) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(142).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK
            params(143) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(143).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN
            params(144) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(144).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL
            params(145) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(145).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK
            params(146) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(146).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN
            params(147) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(147).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL
            params(148) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(148).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK
            params(149) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(149).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN
            params(150) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(150).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL
            params(151) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(151).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK
            params(152) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(152).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN
            params(153) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL", SqlDbType.Int)
            params(153).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL
            params(154) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(154).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK
            params(155) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(155).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN
            params(156) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL", SqlDbType.Int)
            params(156).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL
            params(157) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(157).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK
            params(158) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(158).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN
            params(159) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL", SqlDbType.Int)
            params(159).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL
            params(160) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(160).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK
            params(161) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(161).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN
            params(162) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TTL__", SqlDbType.Int)
            params(162).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TTL__
            params(163) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_", SqlDbType.Int)
            params(163).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_
            params(164) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SLT_TTL_", SqlDbType.Int)
            params(164).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SLT_TTL_
            params(165) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_DPLM_TTL_", SqlDbType.Int)
            params(165).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_DPLM_TTL_
            params(166) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_SRJN_TTL_", SqlDbType.Int)
            params(166).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_SRJN_TTL_
            params(167) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TTL_", SqlDbType.Int)
            params(167).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TTL_
            params(168) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TTL__", SqlDbType.Int)
            params(168).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TTL__
            params(169) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_", SqlDbType.Int)
            params(169).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_
            params(170) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SLT_TTL_", SqlDbType.Int)
            params(170).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SLT_TTL_
            params(171) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TTL_", SqlDbType.Int)
            params(171).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TTL_
            params(172) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TTL_", SqlDbType.Int)
            params(172).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TTL_
            params(173) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TTL_", SqlDbType.Int)
            params(173).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TTL_
            params(174) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL__", SqlDbType.Int)
            params(174).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL__
            params(175) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_", SqlDbType.Int)
            params(175).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_
            params(176) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TTL_", SqlDbType.Int)
            params(176).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TTL_
            params(177) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TTL_", SqlDbType.Int)
            params(177).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TTL_
            params(178) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TTL_", SqlDbType.Int)
            params(178).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TTL_
            params(179) = New SqlParameter("@JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TTL_", SqlDbType.Int)
            params(179).Value = ocustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TTL_
            params(180) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_", SqlDbType.Int)
            params(180).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_
            params(181) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_LKLK", SqlDbType.Int)
            params(181).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_LKLK
            params(182) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_PRMPN", SqlDbType.Int)
            params(182).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_PRMPN
            params(183) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_", SqlDbType.Int)
            params(183).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_
            params(184) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_LKLK", SqlDbType.Int)
            params(184).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_LKLK
            params(185) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_PRMPN", SqlDbType.Int)
            params(185).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_PRMPN
            params(186) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_", SqlDbType.Int)
            params(186).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_
            params(187) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_LKLK", SqlDbType.Int)
            params(187).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_LKLK
            params(188) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_PRMPN", SqlDbType.Int)
            params(188).Value = ocustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_PRMPN
            params(189) = New SqlParameter("@JMLH_TNG_KRJ_TTL__", SqlDbType.Int)
        params(189).Value = ocustomClass.JMLH_TNG_KRJ_TTL__
        params(190) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 100)
        params(190).Value = ocustomClass.BULANDATA
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SaveAdd, params)
            Return ocustomClass
        Catch ex As Exception

        End Try
        'Throw New NotImplementedException()
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0041) As Parameter.SIPP0041
        Dim oReturnValue As New Parameter.SIPP0041
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0041.GetCboBulandataSIPP")
        End Try
    End Function
    Public Function GetSIPP0041EditList(ByVal ocustomClass As Parameter.SIPP0041) As Parameter.SIPP0041
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0041


        params(0) = New SqlParameter("ID", SqlDbType.Int)
        params(0).Value = ocustomClass.ID

        Try
            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.D01.GetD01Edit")
        End Try
    End Function



    'Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP0041) As Parameter.SIPP0041
    '    Dim oReturnValue As New Parameter.SIPP0041
    '    Dim params(2) As SqlParameter
    '    params(0) = New SqlParameter("@valueparent", SqlDbType.VarChar, 20)
    '    params(0).Value = oCustomClass.valueparent
    '    params(1) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 20)
    '    params(1).Value = oCustomClass.valuemaster
    '    params(2) = New SqlParameter("@valueelement", SqlDbType.VarChar, 20)
    '    params(2).Value = oCustomClass.valueelement

    '    Try
    '        oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo, params).Tables(0)
    '        Return oReturnValue
    '    Catch exp As Exception
    '        Throw New Exception("Error On DataAccess.SIPP.SIPP0041.GetCbo")
    '    End Try
    'End Function
    Public Function SIPP0041Delete(ByVal oCustomClass As Parameter.SIPP0041) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, Delete, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

    'Public Function Generate(ByVal oCustomClass As Parameter.D01) As Parameter.D01
    '    Dim oReturnValue As New Parameter.D01

    '    'Dim params(0) As SqlParameter
    '    'params(0) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
    '    'params(0).Value = oCustomClass.CIF
    '    Try
    '        oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, Generate_TEXT).Tables(0)

    '        Return oReturnValue
    '    Catch exp As Exception
    '        Throw New Exception("Error On DataAccess.SLIK.D01.Generate")
    '    End Try
    'End Function

End Class


