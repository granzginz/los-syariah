﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region
Public Class SIPP0035 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP0035List"
    Private Const SAVE As String = "spSIPP0035Save"
    Private Const LIST_EDIT As String = "spSIPP0035EditList"
    Private Const ADD As String = "spSIPP0035Add"
    Private Const FILL_CBO As String = "spGetCboSIPP"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
    Private Const DELETE As String = "spSIPP0035Delete"
#End Region
    Public Function GetSIPP0035(ByVal oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035
        Dim oReturnValue As New Parameter.SIPP0035
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0035.GetSIPP0035")
        End Try
    End Function

    Public Function GetSelectSIPP0035(ByVal oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@ID", SqlDbType.Int)
            params(0).Value = oCustomClass.ID
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0035Edit(ByVal oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0035

        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0035.SIPP0035Edit")
        End Try
    End Function

    Public Function SIPP0035Save(ByVal oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035
        Dim params(8) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0035

        params(8) = New SqlParameter("@ID", SqlDbType.Int)
        params(8).Value = oCustomClass.ID
        params(7) = New SqlParameter("@NAMA", SqlDbType.VarChar, 50)
        params(7).Value = oCustomClass.NM_PNGRS
        params(6) = New SqlParameter("@KEWARGANEGARAAN", SqlDbType.VarChar, 100)
        params(6).Value = oCustomClass.KWRGNGRN
        params(5) = New SqlParameter("@JBTN", SqlDbType.VarChar, 50)
        params(5).Value = oCustomClass.JBTN_KPNGRSN
        params(4) = New SqlParameter("@DATI", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.LKS_DT__TNG_KRJ
        params(3) = New SqlParameter("@TGLMULAI", SqlDbType.Date)
        params(3).Value = oCustomClass.TNGGL_ML
        params(2) = New SqlParameter("@NOSERTIFIKAT", SqlDbType.VarChar, 25)
        params(2).Value = oCustomClass.NMR_SRT_KPTSN_FT_ND_PRPR_TST
        params(1) = New SqlParameter("@TGLSERTIFIKAT", SqlDbType.Date)
        params(1).Value = oCustomClass.TNGGL_SRT_KPTSN_FT_ND_PRPR_TST
        params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0035.SIPP0035Save")
        End Try
    End Function

    Public Function SIPP0035Add(ByVal oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035
        Dim params(7) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0035

        params(0) = New SqlParameter("@NAMA", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.NM_PNGRS
        params(1) = New SqlParameter("@KEWARGANEGARAAN", SqlDbType.VarChar, 100)
        params(1).Value = oCustomClass.KWRGNGRN
        params(2) = New SqlParameter("@JBTN", SqlDbType.VarChar, 50)
        params(2).Value = oCustomClass.JBTN_KPNGRSN
        params(3) = New SqlParameter("@DATI", SqlDbType.VarChar, 100)
        params(3).Value = oCustomClass.LKS_DT__TNG_KRJ
        params(4) = New SqlParameter("@TGLMULAI", SqlDbType.Date)
        params(4).Value = oCustomClass.TNGGL_ML
        params(5) = New SqlParameter("@NOSERTIFIKAT", SqlDbType.VarChar, 25)
        params(5).Value = oCustomClass.NMR_SRT_KPTSN_FT_ND_PRPR_TST
        params(6) = New SqlParameter("@TGLSERTIFIKAT", SqlDbType.Date)
        params(6).Value = oCustomClass.TNGGL_SRT_KPTSN_FT_ND_PRPR_TST
        params(7) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 50)
        params(7).Value = oCustomClass.BULANDATA
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, ADD, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0035.SIPP0035Add")
        End Try

    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035
        Dim oReturnValue As New Parameter.SIPP0035
        Dim params(2) As SqlParameter

        params(0) = New SqlParameter("@valueelement", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Valueelement
        params(1) = New SqlParameter("@valueparent", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.Valueparent
        params(2) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 50)
        params(2).Value = oCustomClass.Valuemaster
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2490.GetCbo")
        End Try
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035
        Dim oReturnValue As New Parameter.SIPP0035
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0035.GetCboBulandataSIPP")
        End Try
    End Function

    Public Function SIPP0035Delete(ByVal oCustomClass As Parameter.SIPP0035) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function
End Class
