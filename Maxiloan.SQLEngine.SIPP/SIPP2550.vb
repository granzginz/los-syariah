﻿

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class SIPP2550 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP2550List"
    Private Const SaveAdd As String = "spSIPP2550SaveAdd"
    Private Const SaveEdit As String = "spSIPP2550SaveEdit"
    Private Const LIST_EDIT As String = "spSIPP2550EditList"
    Private Const spFillCbo As String = "spGetCboSIPP"
    Private Const Delete As String = "spSIPP2550Delete"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"

#End Region

    Public Function GetSIPP2550(ByVal oCustomClass As Parameter.SIPP2550) As Parameter.SIPP2550
        Dim oReturnValue As New Parameter.SIPP2550
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2550.GetSIPP2550")
        End Try
    End Function

    Public Function SIPP2550Delete(ByVal oCustomClass As Parameter.SIPP2550) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.Id
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, Delete, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

    Public Function SIPP2550SaveEdit(ocustomClass As Parameter.SIPP2550) As Parameter.SIPP2550
        Dim params(18) As SqlParameter
        Try
            params(0) = New SqlParameter("@NMR_KNTRK", SqlDbType.VarChar, 50)
            params(0).Value = ocustomClass.NMR_KNTRK
            params(1) = New SqlParameter("@SFT_PNDNN", SqlDbType.VarChar, 100)
            params(1).Value = ocustomClass.SFT_PNDNN
            params(2) = New SqlParameter("@JNS_MT_NG", SqlDbType.VarChar, 100)
            params(2).Value = ocustomClass.JNS_MT_NG
            params(3) = New SqlParameter("@TNGGL_ML", SqlDbType.Date)
            params(3).Value = ocustomClass.TNGGL_ML
            params(4) = New SqlParameter("@TNGGL_JTH_TMP", SqlDbType.Date)
            params(4).Value = ocustomClass.TNGGL_JTH_TMP
            params(5) = New SqlParameter("@JNS_SK_BNG", SqlDbType.VarChar, 100)
            params(5).Value = ocustomClass.JNS_SK_BNG
            params(6) = New SqlParameter("@TNGKT_BNG", SqlDbType.VarChar, 100)
            params(6).Value = ocustomClass.TNGKT_BNG
            params(7) = New SqlParameter("@PLFN_DLM_MT_NG_SL", SqlDbType.Int)
            params(7).Value = ocustomClass.PLFN_DLM_MT_NG_SL
            params(8) = New SqlParameter("@PLFN", SqlDbType.Int)
            params(8).Value = ocustomClass.PLFN
            params(9) = New SqlParameter("@NL_TRCTT_DLM_MT_NG_SL", SqlDbType.Int)
            params(9).Value = ocustomClass.NL_TRCTT_DLM_MT_NG_SL
            params(10) = New SqlParameter("@NL_TRCTT", SqlDbType.Int)
            params(10).Value = ocustomClass.NL_TRCTT
            params(11) = New SqlParameter("@PNDNN_YNG_DTRM_DLM_MT_NG_SL", SqlDbType.Int)
            params(11).Value = ocustomClass.PNDNN_YNG_DTRM_DLM_MT_NG_SL
            params(12) = New SqlParameter("@PNDNN_YNG_DTRM", SqlDbType.Int)
            params(12).Value = ocustomClass.PNDNN_YNG_DTRM
            params(13) = New SqlParameter("@NM_KRDTR", SqlDbType.VarChar, 50)
            params(13).Value = ocustomClass.NM_KRDTR
            params(14) = New SqlParameter("@GLNGN_PHK_LWN", SqlDbType.VarChar, 100)
            params(14).Value = ocustomClass.GLNGN_PHK_LWN
            params(15) = New SqlParameter("@STTS_KTRKTN", SqlDbType.VarChar, 100)
            params(15).Value = ocustomClass.STTS_KTRKTN
            params(16) = New SqlParameter("@LKS_NGR_PHK_LWN", SqlDbType.VarChar, 100)
            params(16).Value = ocustomClass.LKS_NGR_PHK_LWN
            params(17) = New SqlParameter("@ID", SqlDbType.Int)
            params(17).Value = ocustomClass.ID
            params(18) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(18).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SaveEdit, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2550.SIPP2550SaveEdit")
        End Try
        'Throw New NotImplementedException()
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP2550) As Parameter.SIPP2550
        Dim oReturnValue As New Parameter.SIPP2550
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2550.GetCboBulandataSIPP")
        End Try
    End Function
    Public Function SIPP2550SaveAdd(ocustomClass As Parameter.SIPP2550) As Parameter.SIPP2550
        Dim params(17) As SqlParameter
        Try
            params(0) = New SqlParameter("@NMR_KNTRK", SqlDbType.VarChar, 50)
            params(0).Value = ocustomClass.NMR_KNTRK
            params(1) = New SqlParameter("@SFT_PNDNN", SqlDbType.VarChar, 100)
            params(1).Value = ocustomClass.SFT_PNDNN
            params(2) = New SqlParameter("@JNS_MT_NG", SqlDbType.VarChar, 100)
            params(2).Value = ocustomClass.JNS_MT_NG
            params(3) = New SqlParameter("@TNGGL_ML", SqlDbType.Date)
            params(3).Value = ocustomClass.TNGGL_ML
            params(4) = New SqlParameter("@TNGGL_JTH_TMP", SqlDbType.Date)
            params(4).Value = ocustomClass.TNGGL_JTH_TMP
            params(5) = New SqlParameter("@JNS_SK_BNG", SqlDbType.VarChar, 100)
            params(5).Value = ocustomClass.JNS_SK_BNG
            params(6) = New SqlParameter("@TNGKT_BNG", SqlDbType.VarChar, 100)
            params(6).Value = ocustomClass.TNGKT_BNG
            params(7) = New SqlParameter("@PLFN_DLM_MT_NG_SL", SqlDbType.VarChar, 100)
            params(7).Value = ocustomClass.PLFN_DLM_MT_NG_SL
            params(8) = New SqlParameter("@PLFN", SqlDbType.VarChar, 100)
            params(8).Value = ocustomClass.PLFN
            params(9) = New SqlParameter("@NL_TRCTT_DLM_MT_NG_SL", SqlDbType.VarChar, 100)
            params(9).Value = ocustomClass.NL_TRCTT_DLM_MT_NG_SL
            params(10) = New SqlParameter("@NL_TRCTT", SqlDbType.VarChar, 100)
            params(10).Value = ocustomClass.NL_TRCTT
            params(11) = New SqlParameter("@PNDNN_YNG_DTRM_DLM_MT_NG_SL", SqlDbType.VarChar, 100)
            params(11).Value = ocustomClass.PNDNN_YNG_DTRM_DLM_MT_NG_SL
            params(12) = New SqlParameter("@PNDNN_YNG_DTRM", SqlDbType.VarChar, 100)
            params(12).Value = ocustomClass.PNDNN_YNG_DTRM
            params(13) = New SqlParameter("@NM_KRDTR", SqlDbType.VarChar, 50)
            params(13).Value = ocustomClass.NM_KRDTR
            params(14) = New SqlParameter("@GLNGN_PHK_LWN", SqlDbType.VarChar, 100)
            params(14).Value = ocustomClass.GLNGN_PHK_LWN
            params(15) = New SqlParameter("@STTS_KTRKTN", SqlDbType.VarChar, 100)
            params(15).Value = ocustomClass.STTS_KTRKTN
            params(16) = New SqlParameter("@LKS_NGR_PHK_LWN", SqlDbType.VarChar, 100)
            params(16).Value = ocustomClass.LKS_NGR_PHK_LWN
            params(17) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 100)
            params(17).Value = ocustomClass.BULANDATA

            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SaveAdd, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2550.SIPP2550SaveAdd")
        End Try
        'Throw New NotImplementedException()
    End Function


    Public Function GetSIPP2550Edit(ByVal ocustomClass As Parameter.SIPP2550) As Parameter.SIPP2550
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP2550


        params(0) = New SqlParameter("ID", SqlDbType.VarChar, 100)
        params(0).Value = ocustomClass.Id

        Try
            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2550.GetSIPP2550Edit")
        End Try
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP2550) As Parameter.SIPP2550
        Dim oReturnValue As New Parameter.SIPP2550
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@valueparent", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.valueparent
        params(1) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.valuemaster
        params(2) = New SqlParameter("@valueelement", SqlDbType.VarChar, 20)
        params(2).Value = oCustomClass.valueelement

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.D01.GetCbo")
        End Try
    End Function


End Class


