﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region
Public Class SIPP2600 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP2600List"
    Private Const SAVE As String = "spSIPP2600Save"
    Private Const LIST_EDIT As String = "spSIPP2600EditList"
    Private Const ADD As String = "spSIPP2600Add"
    Private Const FILL_CBO As String = "spGetCboSIPP"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
    Private Const DELETE As String = "spSIPP2600Delete"
#End Region

    Public Function GetSIPP2600(ByVal oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600
        Dim oReturnValue As New Parameter.SIPP2600
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2600.GetSIPP2600")
        End Try
    End Function

    Public Function GetSelectSIPP2600(ByVal oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@ID", SqlDbType.Int)
            params(0).Value = oCustomClass.ID
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2600Edit(ByVal oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP2600

        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2600.SIPP2600Edit")
        End Try
    End Function

    Public Function SIPP2600Save(ByVal oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600
        Dim params(14) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP2600

        params(14) = New SqlParameter("ID", SqlDbType.Int)
        params(14).Value = oCustomClass.ID
        params(13) = New SqlParameter("@NOSURATBERHARGA", SqlDbType.VarChar, 25)
        params(13).Value = oCustomClass.NOSURATBERHARGA
        params(12) = New SqlParameter("@JENISSURATBERHARGA", SqlDbType.VarChar, 100)
        params(12).Value = oCustomClass.JENISSURATBERHARGA
        params(11) = New SqlParameter("@TGLMULAI", SqlDbType.Date)
        params(11).Value = oCustomClass.TGLMULAI
        params(10) = New SqlParameter("@TGJATUHTEMPO", SqlDbType.Date)
        params(10).Value = oCustomClass.TGJATUHTEMPO
        params(9) = New SqlParameter("@JENISSUKUBUNGA", SqlDbType.VarChar, 100)
        params(9).Value = oCustomClass.JENISSUKUBUNGA
        params(8) = New SqlParameter("@TINGKATSUKUBUNGA", SqlDbType.Int)
        params(8).Value = oCustomClass.TINGKATSUKUBUNGA
        params(7) = New SqlParameter("@NILAINOMINAL", SqlDbType.Money)
        params(7).Value = oCustomClass.NILAINOMINAL
        params(6) = New SqlParameter("@JENISVALUTA", SqlDbType.VarChar, 100)
        params(6).Value = oCustomClass.JENISVALUTA
        params(5) = New SqlParameter("@SALDOPINJAMANASAL", SqlDbType.Money)
        params(5).Value = oCustomClass.SALDOPINJAMANASAL
        params(4) = New SqlParameter("@SALDOPINJAMANRUPIAH ", SqlDbType.Money)
        params(4).Value = oCustomClass.SALDOPINJAMANRUPIAH
        params(3) = New SqlParameter("@NAMAKREDITUR", SqlDbType.VarChar, 50)
        params(3).Value = oCustomClass.NAMAKREDITUR
        params(2) = New SqlParameter("@GOLONGANKREDITUR", SqlDbType.VarChar, 100)
        params(2).Value = oCustomClass.GOLONGANKREDITUR
        params(1) = New SqlParameter("@LOKASINEGARA", SqlDbType.VarChar, 100)
        params(1).Value = oCustomClass.LOKASINEGARA
        params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2600.SIPP2600Save")
        End Try
    End Function

    Public Function SIPP2600Add(ByVal oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600
        Dim params(13) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP2600

        params(0) = New SqlParameter("@NOSURATBERHARGA", SqlDbType.VarChar, 25)
        params(0).Value = oCustomClass.NOSURATBERHARGA
        params(1) = New SqlParameter("@JENISSURATBERHARGA", SqlDbType.VarChar, 100)
        params(1).Value = oCustomClass.JENISSURATBERHARGA
        params(2) = New SqlParameter("@TGLMULAI", SqlDbType.Date)
        params(2).Value = oCustomClass.TGLMULAI
        params(3) = New SqlParameter("@TGJATUHTEMPO", SqlDbType.Date)
        params(3).Value = oCustomClass.TGJATUHTEMPO
        params(4) = New SqlParameter("@JENISSUKUBUNGA", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.JENISSUKUBUNGA
        params(5) = New SqlParameter("@TINGKATSUKUBUNGA", SqlDbType.Int)
        params(5).Value = oCustomClass.TINGKATSUKUBUNGA
        params(6) = New SqlParameter("@NILAINOMINAL", SqlDbType.Money)
        params(6).Value = oCustomClass.NILAINOMINAL
        params(7) = New SqlParameter("@JENISVALUTA", SqlDbType.VarChar, 100)
        params(7).Value = oCustomClass.JENISVALUTA
        params(8) = New SqlParameter("@SALDOPINJAMANASAL", SqlDbType.Money)
        params(8).Value = oCustomClass.SALDOPINJAMANASAL
        params(9) = New SqlParameter("@SALDOPINJAMANRUPIAH ", SqlDbType.Money)
        params(9).Value = oCustomClass.SALDOPINJAMANRUPIAH
        params(10) = New SqlParameter("@NAMAKREDITUR", SqlDbType.VarChar, 50)
        params(10).Value = oCustomClass.NAMAKREDITUR
        params(11) = New SqlParameter("@GOLONGANKREDITUR", SqlDbType.VarChar, 100)
        params(11).Value = oCustomClass.GOLONGANKREDITUR
        params(12) = New SqlParameter("@LOKASINEGARA", SqlDbType.VarChar, 100)
        params(12).Value = oCustomClass.LOKASINEGARA
        params(13) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 100)
        params(13).Value = oCustomClass.BULANDATA

        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, ADD, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2600.SIPP2600Add")
        End Try

    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600
        Dim oReturnValue As New Parameter.SIPP2600
        Dim params(2) As SqlParameter

        params(0) = New SqlParameter("@valueelement", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Valueelement
        params(1) = New SqlParameter("@valueparent", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.Valueparent
        params(2) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 50)
        params(2).Value = oCustomClass.Valuemaster
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2490.GetCbo")
        End Try
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600
        Dim oReturnValue As New Parameter.SIPP2600
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2600.GetCboBulandataSIPP")
        End Try
    End Function

    Public Function SIPP2600Delete(ByVal oCustomClass As Parameter.SIPP2600) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

End Class


