﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region
Public Class SIPP0046 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP0046List"
    Private Const SAVE As String = "spSIPP0046Save"
    Private Const LIST_EDIT As String = "spSIPP0046EditList"
    Private Const ADD As String = "spSIPP0046Add"
    Private Const FILL_CBO As String = "spGetCboSIPP"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
    Private Const DELETE As String = "spSIPP0046Delete"
#End Region

    Public Function GetSIPP0046(ByVal oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046
        Dim oReturnValue As New Parameter.SIPP0046
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0046.GetSIPP0046")
        End Try
    End Function

    Public Function GetSelectSIPP0046(ByVal oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@ID", SqlDbType.Int)
            params(0).Value = oCustomClass.ID
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0046Edit(ByVal oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0046

        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0046.SIPP0046Edit")
        End Try
    End Function

    Public Function SIPP0046Save(ByVal oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046
        Dim params(7) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0046

        params(7) = New SqlParameter("ID", SqlDbType.Int)
        params(7).Value = oCustomClass.ID
        params(6) = New SqlParameter("NAMA", SqlDbType.VarChar, 50)
        params(6).Value = oCustomClass.NAMA
        params(5) = New SqlParameter("KEWARGANEGARAAN", SqlDbType.VarChar, 100)
        params(5).Value = oCustomClass.KEWARGANEGARAAN
        params(4) = New SqlParameter("JABATAN", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.JABATAN
        params(3) = New SqlParameter("DOMISILI", SqlDbType.VarChar, 100)
        params(3).Value = oCustomClass.DOMISILI
        params(2) = New SqlParameter("NOSURATKEPUTUSAN", SqlDbType.VarChar, 25)
        params(2).Value = oCustomClass.NOSURATKEPUTUSAN
        params(1) = New SqlParameter("TGLSURATKEPUTUSAN", SqlDbType.Date)
        params(1).Value = oCustomClass.TGLSURATKEPUTUSAN
        params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0046.SIPP0046Save")
        End Try
    End Function

    Public Function SIPP0046Add(ByVal oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046
        Dim params(6) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0046

        params(0) = New SqlParameter("@NAMA", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.NAMA
        params(1) = New SqlParameter("@KEWARGANEGARAAN", SqlDbType.VarChar, 100)
        params(1).Value = oCustomClass.KEWARGANEGARAAN
        params(2) = New SqlParameter("@JABATAN", SqlDbType.VarChar, 100)
        params(2).Value = oCustomClass.JABATAN
        params(3) = New SqlParameter("@DOMISILI", SqlDbType.VarChar, 100)
        params(3).Value = oCustomClass.DOMISILI
        params(4) = New SqlParameter("@NOSURATKEPUTUSAN", SqlDbType.VarChar, 25)
        params(4).Value = oCustomClass.NOSURATKEPUTUSAN
        params(5) = New SqlParameter("@TGLSURATKEPUTUSAN", SqlDbType.Date)
        params(5).Value = oCustomClass.TGLSURATKEPUTUSAN
        params(6) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 8)
        params(6).Value = oCustomClass.BULANDATA

        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, ADD, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0046.SIPP0046Add")
        End Try

    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046
        Dim oReturnValue As New Parameter.SIPP0046
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0046.GetCboBulandataSIPP")
        End Try
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046
        Dim oReturnValue As New Parameter.SIPP0046
        Dim params(2) As SqlParameter

        params(0) = New SqlParameter("@valueelement", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Valueelement
        params(1) = New SqlParameter("@valueparent", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.Valueparent
        params(2) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 50)
        params(2).Value = oCustomClass.Valuemaster
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2490.GetCbo")
        End Try
    End Function

    Public Function SIPP0046Delete(ByVal oCustomClass As Parameter.SIPP0046) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

End Class

