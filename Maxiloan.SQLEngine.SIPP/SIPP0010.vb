﻿

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class SIPP0010 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP0010List"
    Private Const SaveAdd As String = "spSIPP0010SaveAdd"
    Private Const SaveEdit As String = "spSIPP0010SaveEdit"
    Private Const LIST_EDIT As String = "spSIPP0010EditList"
    Private Const spFillCbo As String = "spGetCboSIPP"
    Private Const Delete As String = "spSIPP0010Delete"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
    'Private Const Generate_TEXT As String = "spGenerateD01"

#End Region

    Public Function GetSIPP0010(ByVal oCustomClass As Parameter.SIPP0010) As Parameter.SIPP0010
        Dim oReturnValue As New Parameter.SIPP0010
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0010.GetSIPP0010")
        End Try
    End Function

    Public Function SIPP0010SaveEdit(ocustomClass As Parameter.SIPP0010) As Parameter.SIPP0010
        Dim params(5) As SqlParameter
        Try
            params(0) = New SqlParameter("@KTRNGN", SqlDbType.VarChar, 100)
            params(0).Value = ocustomClass.KTRNGN
            params(1) = New SqlParameter("@NMR_ZN_SH", SqlDbType.VarChar, 100)
            params(1).Value = ocustomClass.NMR_ZN_SH
            params(2) = New SqlParameter("@JNS_PRZNN", SqlDbType.VarChar, 100)
            params(2).Value = ocustomClass.JNS_PRZNN
            params(3) = New SqlParameter("@TNGGL_ZN_SH", SqlDbType.Date)
            params(3).Value = ocustomClass.TNGGL_ZN_SH
            params(4) = New SqlParameter("@ID", SqlDbType.Int)
            params(4).Value = ocustomClass.ID
            params(5) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(5).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SaveEdit, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0010.SIPP0010SaveEdit")
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function SIPP0010SaveAdd(ocustomClass As Parameter.SIPP0010) As Parameter.SIPP0010
        Dim params(4) As SqlParameter
        Try
            params(0) = New SqlParameter("@KTRNGN", SqlDbType.VarChar, 100)
            params(0).Value = ocustomClass.KTRNGN
            params(1) = New SqlParameter("@NMR_ZN_SH", SqlDbType.VarChar, 100)
            params(1).Value = ocustomClass.NMR_ZN_SH
            params(2) = New SqlParameter("@JNS_PRZNN", SqlDbType.VarChar, 100)
            params(2).Value = ocustomClass.JNS_PRZNN
            params(3) = New SqlParameter("@TNGGL_ZN_SH", SqlDbType.Date)
            params(3).Value = ocustomClass.TNGGL_ZN_SH
            params(4) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 100)
            params(4).Value = ocustomClass.bulandata

            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, SaveAdd, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception

        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSIPP0010Edit(ByVal ocustomClass As Parameter.SIPP0010) As Parameter.SIPP0010
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0010


        params(0) = New SqlParameter("ID", SqlDbType.VarChar, 100)
        params(0).Value = ocustomClass.ID

        Try
            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SLIK.D01.GetD01Edit")
        End Try
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP0010) As Parameter.SIPP0010
        Dim oReturnValue As New Parameter.SIPP0010
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@valueparent", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.valueparent
        params(1) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.valuemaster
        params(2) = New SqlParameter("@valueelement", SqlDbType.VarChar, 20)
        params(2).Value = oCustomClass.valueelement

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0010.GetCbo")
        End Try
    End Function
    Public Function SIPP0010Delete(ByVal oCustomClass As Parameter.SIPP0010) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, Delete, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function


    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0010) As Parameter.SIPP0010
        Dim oReturnValue As New Parameter.SIPP0010
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0010.GetCboBulandataSIPP")
        End Try
    End Function

End Class


