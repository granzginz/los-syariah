﻿

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class SIPP0030 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP0030List"
    Private Const SaveAdd As String = "spSIPP0030SaveAdd"
    Private Const SaveEdit As String = "spSIPP0030SaveEdit"
    Private Const LIST_EDIT As String = "spSIPP0030EditList"
    Private Const spFillCbo As String = "spGetCboSIPP"
    Private Const Delete As String = "spSIPP0030Delete"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"

#End Region

    Public Function GetSIPP0030(ByVal oCustomClass As Parameter.SIPP0030) As Parameter.SIPP0030
        Dim oReturnValue As New Parameter.SIPP0030
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0030.GetSIPP0030")
        End Try
    End Function

    Public Function SIPP0030SaveEdit(ocustomClass As Parameter.SIPP0030) As Parameter.SIPP0030
        Dim params(16) As SqlParameter
        Try
            params(0) = New SqlParameter("NM_PMGNG_SHM", SqlDbType.VarChar, 100)
            params(0).Value = ocustomClass.NM_PMGNG_SHM
            params(1) = New SqlParameter("GLNGN_PHK_LWN", SqlDbType.VarChar, 100)
            params(1).Value = ocustomClass.GLNGN_PHK_LWN
            params(2) = New SqlParameter("LKS_NGR_PHK_LWN", SqlDbType.VarChar, 100)
            params(2).Value = ocustomClass.LKS_NGR_PHK_LWN
            params(3) = New SqlParameter("BNTK_HKM", SqlDbType.VarChar, 100)
            params(3).Value = ocustomClass.BNTK_HKM
            params(4) = New SqlParameter("STTS_PMGNG_SHM", SqlDbType.VarChar, 100)
            params(4).Value = ocustomClass.STTS_PMGNG_SHM
            params(5) = New SqlParameter("KTS_PMGNG_SHM", SqlDbType.BigInt)
            params(5).Value = ocustomClass.KTS_PMGNG_SHM
            params(6) = New SqlParameter("NL_KPMLKN_SHM", SqlDbType.BigInt)
            params(6).Value = ocustomClass.NL_KPMLKN_SHM
            params(7) = New SqlParameter("PRSNTS_KPMLKN_SHM", SqlDbType.VarChar, 100)
            params(7).Value = ocustomClass.PRSNTS_KPMLKN_SHM
            params(8) = New SqlParameter("NM_PNGRS_DRJT_KD", SqlDbType.VarChar, 100)
            params(8).Value = ocustomClass.NM_PNGRS_DRJT_KD
            params(9) = New SqlParameter("JBTN_KPNGRSN", SqlDbType.VarChar, 100)
            params(9).Value = ocustomClass.JBTN_KPNGRSN
            params(10) = New SqlParameter("KWRGNGRN", SqlDbType.VarChar, 100)
            params(10).Value = ocustomClass.KWRGNGRN
            params(11) = New SqlParameter("NM_PMGNG_SHM_DRJT_KD", SqlDbType.VarChar, 100)
            params(11).Value = ocustomClass.NM_PMGNG_SHM_DRJT_KD
            params(12) = New SqlParameter("GLNGN_KLMPK_PMGNG_SHM_DRJT_KD", SqlDbType.VarChar, 100)
            params(12).Value = ocustomClass.GLNGN_KLMPK_PMGNG_SHM_DRJT_KD
            params(13) = New SqlParameter("LKS_NGR_PMGNG_SHM_DRJT_KD", SqlDbType.VarChar, 100)
            params(13).Value = ocustomClass.LKS_NGR_PMGNG_SHM_DRJT_KD
            params(14) = New SqlParameter("NL_KPMLKN_SHM_DRJT_KD", SqlDbType.BigInt)
            params(14).Value = ocustomClass.NL_KPMLKN_SHM_DRJT_KD
            params(15) = New SqlParameter("@ID", SqlDbType.Int)
            params(15).Value = ocustomClass.ID
            params(16) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(16).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SaveEdit, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0030.SIPP0030SaveEdit")
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function SIPP0030SaveAdd(ocustomClass As Parameter.SIPP0030) As Parameter.SIPP0030
        Dim params(15) As SqlParameter

        params(0) = New SqlParameter("NM_PMGNG_SHM", SqlDbType.VarChar, 50)
        params(0).Value = ocustomClass.NM_PMGNG_SHM
        params(1) = New SqlParameter("GLNGN_PHK_LWN", SqlDbType.VarChar, 100)
        params(1).Value = ocustomClass.GLNGN_PHK_LWN
        params(2) = New SqlParameter("LKS_NGR_PHK_LWN", SqlDbType.VarChar, 100)
        params(2).Value = ocustomClass.LKS_NGR_PHK_LWN
        params(3) = New SqlParameter("BNTK_HKM", SqlDbType.VarChar, 100)
        params(3).Value = ocustomClass.BNTK_HKM
        params(4) = New SqlParameter("STTS_PMGNG_SHM", SqlDbType.VarChar, 100)
        params(4).Value = ocustomClass.STTS_PMGNG_SHM
        params(5) = New SqlParameter("KTS_PMGNG_SHM", SqlDbType.BigInt)
        params(5).Value = ocustomClass.KTS_PMGNG_SHM
        params(6) = New SqlParameter("NL_KPMLKN_SHM", SqlDbType.BigInt)
        params(6).Value = ocustomClass.NL_KPMLKN_SHM
        params(7) = New SqlParameter("PRSNTS_KPMLKN_SHM", SqlDbType.VarChar, 20)
        params(7).Value = ocustomClass.PRSNTS_KPMLKN_SHM
        params(8) = New SqlParameter("NM_PNGRS_DRJT_KD", SqlDbType.VarChar, 100)
        params(8).Value = ocustomClass.NM_PNGRS_DRJT_KD
        params(9) = New SqlParameter("JBTN_KPNGRSN", SqlDbType.VarChar, 100)
        params(9).Value = ocustomClass.JBTN_KPNGRSN
        params(10) = New SqlParameter("KWRGNGRN", SqlDbType.VarChar, 100)
        params(10).Value = ocustomClass.KWRGNGRN
        params(11) = New SqlParameter("NM_PMGNG_SHM_DRJT_KD", SqlDbType.VarChar, 100)
        params(11).Value = ocustomClass.NM_PMGNG_SHM_DRJT_KD
        params(12) = New SqlParameter("GLNGN_KLMPK_PMGNG_SHM_DRJT_KD", SqlDbType.VarChar, 100)
        params(12).Value = ocustomClass.GLNGN_KLMPK_PMGNG_SHM_DRJT_KD
        params(13) = New SqlParameter("LKS_NGR_PMGNG_SHM_DRJT_KD", SqlDbType.VarChar, 100)
        params(13).Value = ocustomClass.LKS_NGR_PMGNG_SHM_DRJT_KD
        params(14) = New SqlParameter("NL_KPMLKN_SHM_DRJT_KD", SqlDbType.BigInt)
        params(14).Value = ocustomClass.NL_KPMLKN_SHM_DRJT_KD
        params(15) = New SqlParameter("BULANDATA", SqlDbType.VarChar, 100)
        params(15).Value = ocustomClass.BULANDATA

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SaveAdd, params)
            Return ocustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0030.SIPP0030SaveAdd")
        End Try
        'Throw New NotImplementedException()
    End Function


    Public Function GetSIPP0030Edit(ByVal ocustomClass As Parameter.SIPP0030) As Parameter.SIPP0030
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0030


        params(0) = New SqlParameter("ID", SqlDbType.VarChar, 100)
        params(0).Value = ocustomClass.ID

        Try
            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0030.GetSIPP0030Edit")
        End Try
    End Function


    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP0030) As Parameter.SIPP0030
        Dim oReturnValue As New Parameter.SIPP0030
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@valueparent", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.valueparent
        params(1) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.valuemaster
        params(2) = New SqlParameter("@valueelement", SqlDbType.VarChar, 20)
        params(2).Value = oCustomClass.valueelement

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.D01.GetCbo")
        End Try
    End Function

    Public Function SIPP0030Delete(ByVal oCustomClass As Parameter.SIPP0030) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, Delete, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0030) As Parameter.SIPP0030
        Dim oReturnValue As New Parameter.SIPP0030
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0030.GetCboBulandataSIPP")
        End Try
    End Function

End Class


