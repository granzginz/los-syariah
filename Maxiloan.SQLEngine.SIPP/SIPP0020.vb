﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region
Public Class SIPP0020 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP0020List"
    Private Const SAVE As String = "spSIPP0020Save"
    Private Const LIST_EDIT As String = "spSIPP0020EditList"
    Private Const ADD As String = "spSIPP0020Add"
    Private Const FILL_CBO As String = "spGetCboSIPP"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
    Private Const DELETE As String = "spSIPP0020Delete"
#End Region
    Public Function GetSIPP0020(ByVal oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020
        Dim oReturnValue As New Parameter.SIPP0020
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0020.GetSIPP0020")
        End Try
    End Function

    Public Function GetSelectSIPP0020(ByVal oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@ID", SqlDbType.Int)
            params(0).Value = oCustomClass.ID
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0020Edit(ByVal oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0020

        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0020.SIPP0020Edit")
        End Try
    End Function

    Public Function SIPP0020Save(ByVal oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020
        Dim params(10) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0020

        params(10) = New SqlParameter("@ID", SqlDbType.Int)
        params(10).Value = oCustomClass.ID
        params(9) = New SqlParameter("@NOIZIN", SqlDbType.VarChar, 50)
        params(9).Value = oCustomClass.NMR_ZN_KNTR_CBNG
        params(8) = New SqlParameter("@TGLIZIN", SqlDbType.Date)
        params(8).Value = oCustomClass.TNGGL_ZN_KNTR_CBNG
        params(7) = New SqlParameter("@ALAMAT", SqlDbType.VarChar, 100)
        params(7).Value = oCustomClass.LMT_LNGKP
        params(6) = New SqlParameter("@KECAMATAN", SqlDbType.VarChar, 50)
        params(6).Value = oCustomClass.KCMTN
        params(5) = New SqlParameter("@DATI", SqlDbType.VarChar, 100)
        params(5).Value = oCustomClass.LKS_DT__KNTR
        params(4) = New SqlParameter("@KODEPOS", SqlDbType.VarChar, 5)
        params(4).Value = oCustomClass.KD_PS
        params(3) = New SqlParameter("@NOTELPON", SqlDbType.VarChar, 15)
        params(3).Value = oCustomClass.NMR_TLPN
        params(2) = New SqlParameter("@JMLTNGKRJCBG", SqlDbType.Int)
        params(2).Value = oCustomClass.JMLH_TNG_KRJ_KNTR_CBNG
        params(1) = New SqlParameter("@NAMAKEPALACABANG", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.NM_KPL_CBNG
        params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0020.SIPP0020Save")
        End Try
    End Function

    Public Function SIPP0020Add(ByVal oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020
        Dim params(9) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0020

        params(0) = New SqlParameter("@NOIZIN", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.NMR_ZN_KNTR_CBNG
        params(1) = New SqlParameter("@TGLIZIN", SqlDbType.Date)
        params(1).Value = oCustomClass.TNGGL_ZN_KNTR_CBNG
        params(2) = New SqlParameter("@ALAMAT", SqlDbType.VarChar, 100)
        params(2).Value = oCustomClass.LMT_LNGKP
        params(3) = New SqlParameter("@KECAMATAN", SqlDbType.VarChar, 50)
        params(3).Value = oCustomClass.KCMTN
        params(4) = New SqlParameter("@DATI", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.LKS_DT__KNTR
        params(5) = New SqlParameter("@KODEPOS", SqlDbType.VarChar, 5)
        params(5).Value = oCustomClass.KD_PS
        params(6) = New SqlParameter("@NOTELPON", SqlDbType.VarChar, 15)
        params(6).Value = oCustomClass.NMR_TLPN
        params(7) = New SqlParameter("@JMLTNGKRJCBG", SqlDbType.Int)
        params(7).Value = oCustomClass.JMLH_TNG_KRJ_KNTR_CBNG
        params(8) = New SqlParameter("@NAMAKEPALACABANG", SqlDbType.VarChar, 50)
        params(8).Value = oCustomClass.NM_KPL_CBNG
        params(9) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 50)
        params(9).Value = oCustomClass.BULANDATA
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, ADD, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0020.SIPP0020Add")
        End Try

    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020
        Dim oReturnValue As New Parameter.SIPP0020
        Dim params(2) As SqlParameter

        params(0) = New SqlParameter("@valueelement", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Valueelement
        params(1) = New SqlParameter("@valueparent", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.Valueparent
        params(2) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 50)
        params(2).Value = oCustomClass.Valuemaster
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2490.GetCbo")
        End Try
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020
        Dim oReturnValue As New Parameter.SIPP0020
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0020.GetCboBulandataSIPP")
        End Try
    End Function

    Public Function SIPP0020Delete(ByVal oCustomClass As Parameter.SIPP0020) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

End Class
