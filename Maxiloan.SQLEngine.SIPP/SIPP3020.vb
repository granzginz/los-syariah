﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region
Public Class SIPP3020 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP3020List"
    Private Const SAVE As String = "spSIPP3020Save"
    Private Const LIST_EDIT As String = "spSIPP3020EditList"
    Private Const ADD As String = "spSIPP3020Add"
    Private Const FILL_CBO As String = "spGetCboSIPP"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
    Private Const DELETE As String = "spSIPP3020Delete"
#End Region

    Public Function GetSIPP3020(ByVal oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020
        Dim oReturnValue As New Parameter.SIPP3020
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP3020.GetSIPP3020")
        End Try
    End Function

    Public Function GetSelectSIPP3020(ByVal oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@ID", SqlDbType.Int)
            params(0).Value = oCustomClass.ID
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP3020Edit(ByVal oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP3020

        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP3020.SIPP3020Edit")
        End Try
    End Function

    Public Function SIPP3020Save(ByVal oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020
        Dim params(15) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP3020

        params(15) = New SqlParameter("ID", SqlDbType.Int)
        params(15).Value = oCustomClass.ID
        params(14) = New SqlParameter("@NOKONTRAK", SqlDbType.VarChar, 25)
        params(14).Value = oCustomClass.NOKONTRAK
        params(13) = New SqlParameter("@JNSKRJSMPEMB", SqlDbType.VarChar, 100)
        params(13).Value = oCustomClass.JNSKRJSMPEMB
        params(12) = New SqlParameter("@TGLMULAI", SqlDbType.Date)
        params(12).Value = oCustomClass.TGLMULAI
        params(11) = New SqlParameter("@TGLJATUHTEMPO", SqlDbType.Date)
        params(11).Value = oCustomClass.TGLJATUHTEMPO
        params(10) = New SqlParameter("@JENISVALUTA", SqlDbType.VarChar, 100)
        params(10).Value = oCustomClass.JENISVALUTA
        params(9) = New SqlParameter("@PORSIPRSHNPEMB", SqlDbType.Decimal)
        params(9).Value = oCustomClass.PORSIPRSHNPEMB
        params(8) = New SqlParameter("@PLAFONASAL", SqlDbType.VarChar, 100)
        params(8).Value = oCustomClass.PLAFONASAL
        params(7) = New SqlParameter("@PLAFONRUPIAH", SqlDbType.VarChar, 100)
        params(7).Value = oCustomClass.PLAFONRUPIAH
        params(6) = New SqlParameter("@SOPPPBASAL", SqlDbType.VarChar, 100)
        params(6).Value = oCustomClass.SOPPPBASAL
        params(5) = New SqlParameter("@SOPPPBRUPIAH", SqlDbType.VarChar, 100)
        params(5).Value = oCustomClass.SOPPPBRUPIAH
        params(4) = New SqlParameter("@NAMAKREDITUR", SqlDbType.VarChar, 50)
        params(4).Value = oCustomClass.NAMAKREDITUR
        params(3) = New SqlParameter("@GOLKREDITUR", SqlDbType.VarChar, 100)
        params(3).Value = oCustomClass.GOLKREDITUR
        params(2) = New SqlParameter("@STATUSKETERKAITAN", SqlDbType.VarChar, 100)
        params(2).Value = oCustomClass.STATUSKETERKAITAN
        params(1) = New SqlParameter("@NEGARAASAL", SqlDbType.VarChar, 100)
        params(1).Value = oCustomClass.NEGARAASAL
        params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP3020.SIPP3020Save")
        End Try
    End Function

    Public Function SIPP3020Add(ByVal oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020
        Dim params(14) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP3020

        params(0) = New SqlParameter("@NOKONTRAK", SqlDbType.VarChar, 25)
        params(0).Value = oCustomClass.NOKONTRAK
        params(1) = New SqlParameter("@JNSKRJSMPEMB", SqlDbType.VarChar, 100)
        params(1).Value = oCustomClass.JNSKRJSMPEMB
        params(2) = New SqlParameter("@TGLMULAI", SqlDbType.Date)
        params(2).Value = oCustomClass.TGLMULAI
        params(3) = New SqlParameter("@TGLJATUHTEMPO", SqlDbType.Date)
        params(3).Value = oCustomClass.TGLJATUHTEMPO
        params(4) = New SqlParameter("@JENISVALUTA", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.JENISVALUTA
        params(5) = New SqlParameter("@PORSIPRSHNPEMB", SqlDbType.Decimal)
        params(5).Value = oCustomClass.PORSIPRSHNPEMB
        params(6) = New SqlParameter("@PLAFONASAL", SqlDbType.VarChar, 100)
        params(6).Value = oCustomClass.PLAFONASAL
        params(7) = New SqlParameter("@PLAFONRUPIAH", SqlDbType.VarChar, 100)
        params(7).Value = oCustomClass.PLAFONRUPIAH
        params(8) = New SqlParameter("@SOPPPBASAL", SqlDbType.VarChar, 100)
        params(8).Value = oCustomClass.SOPPPBASAL
        params(9) = New SqlParameter("@SOPPPBRUPIAH", SqlDbType.VarChar, 100)
        params(9).Value = oCustomClass.SOPPPBRUPIAH
        params(10) = New SqlParameter("@NAMAKREDITUR", SqlDbType.VarChar, 50)
        params(10).Value = oCustomClass.NAMAKREDITUR
        params(11) = New SqlParameter("@GOLKREDITUR", SqlDbType.VarChar, 100)
        params(11).Value = oCustomClass.GOLKREDITUR
        params(12) = New SqlParameter("@STATUSKETERKAITAN", SqlDbType.VarChar, 100)
        params(12).Value = oCustomClass.STATUSKETERKAITAN
        params(13) = New SqlParameter("@NEGARAASAL", SqlDbType.VarChar, 100)
        params(13).Value = oCustomClass.NEGARAASAL
        params(14) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 100)
        params(14).Value = oCustomClass.BULANDATA

        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, ADD, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP3020.SIPP3020Add")
        End Try

    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020
        Dim oReturnValue As New Parameter.SIPP3020
        Dim params(2) As SqlParameter

        params(0) = New SqlParameter("@valueelement", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Valueelement
        params(1) = New SqlParameter("@valueparent", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.Valueparent
        params(2) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 50)
        params(2).Value = oCustomClass.Valuemaster
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2490.GetCbo")
        End Try
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020
        Dim oReturnValue As New Parameter.SIPP3020
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP3020.GetCboBulandataSIPP")
        End Try
    End Function

    Public Function SIPP3020Delete(ByVal oCustomClass As Parameter.SIPP3020) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

End Class

