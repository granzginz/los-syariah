﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region
Public Class SIPP0043 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP0043List"
    Private Const SAVE As String = "spSIPP0043Save"
    Private Const LIST_EDIT As String = "spSIPP0043EditList"
    Private Const ADD As String = "spSIPP0043Add"
    Private Const FILL_CBO As String = "spGetCboSIPP"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
    Private Const DELETE As String = "spSIPP0043Delete"
#End Region

    Public Function GetSIPP0043(ByVal oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043
        Dim oReturnValue As New Parameter.SIPP0043
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0043.GetSIPP0043")
        End Try
    End Function

    Public Function GetSelectSIPP0043(ByVal oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@ID", SqlDbType.Int)
            params(0).Value = oCustomClass.ID
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0043Edit(ByVal oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0043

        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0043.SIPP0043Edit")
        End Try
    End Function

    Public Function SIPP0043Save(ByVal oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043
        Dim params(78) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0043

        params(78) = New SqlParameter("ID", SqlDbType.Int)
        params(78).Value = oCustomClass.ID
        params(77) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(77).Value = oCustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(76) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(76).Value = oCustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(75) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(75).Value = oCustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(74) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(74).Value = oCustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(73) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(73).Value = oCustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(72) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(72).Value = oCustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(71) = New SqlParameter("@JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(71).Value = oCustomClass.JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(70) = New SqlParameter("@JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(70).Value = oCustomClass.JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(69) = New SqlParameter("@JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(69).Value = oCustomClass.JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(68) = New SqlParameter("@JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(68).Value = oCustomClass.JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(67) = New SqlParameter("@JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(67).Value = oCustomClass.JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(66) = New SqlParameter("@JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(66).Value = oCustomClass.JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(65) = New SqlParameter("@JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(65).Value = oCustomClass.JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(64) = New SqlParameter("@JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(64).Value = oCustomClass.JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(63) = New SqlParameter("@JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(63).Value = oCustomClass.JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(62) = New SqlParameter("@JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(62).Value = oCustomClass.JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(61) = New SqlParameter("@JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(61).Value = oCustomClass.JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(60) = New SqlParameter("@JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(60).Value = oCustomClass.JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(59) = New SqlParameter("@JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(59).Value = oCustomClass.JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(58) = New SqlParameter("@JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(58).Value = oCustomClass.JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(57) = New SqlParameter("@JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(57).Value = oCustomClass.JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(56) = New SqlParameter("@JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(56).Value = oCustomClass.JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(55) = New SqlParameter("@JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(55).Value = oCustomClass.JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(54) = New SqlParameter("@JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(54).Value = oCustomClass.JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(53) = New SqlParameter("@JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(53).Value = oCustomClass.JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(52) = New SqlParameter("@JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(52).Value = oCustomClass.JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(51) = New SqlParameter("@JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(51).Value = oCustomClass.JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(50) = New SqlParameter("@JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(50).Value = oCustomClass.JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(49) = New SqlParameter("@JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(49).Value = oCustomClass.JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(48) = New SqlParameter("@JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(48).Value = oCustomClass.JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(47) = New SqlParameter("@JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(47).Value = oCustomClass.JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(46) = New SqlParameter("@JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(46).Value = oCustomClass.JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(45) = New SqlParameter("@JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(45).Value = oCustomClass.JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(44) = New SqlParameter("@JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(44).Value = oCustomClass.JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(43) = New SqlParameter("@JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(43).Value = oCustomClass.JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(42) = New SqlParameter("@JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(42).Value = oCustomClass.JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(41) = New SqlParameter("@JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(41).Value = oCustomClass.JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(40) = New SqlParameter("@JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(40).Value = oCustomClass.JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(39) = New SqlParameter("@JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(39).Value = oCustomClass.JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(38) = New SqlParameter("@JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(38).Value = oCustomClass.JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(37) = New SqlParameter("@JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(37).Value = oCustomClass.JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(36) = New SqlParameter("@JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(36).Value = oCustomClass.JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(35) = New SqlParameter("@JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(35).Value = oCustomClass.JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(34) = New SqlParameter("@JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(34).Value = oCustomClass.JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(33) = New SqlParameter("@JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(33).Value = oCustomClass.JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(32) = New SqlParameter("@JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(32).Value = oCustomClass.JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(31) = New SqlParameter("@JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(31).Value = oCustomClass.JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(30) = New SqlParameter("@JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(30).Value = oCustomClass.JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(29) = New SqlParameter("@JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(29).Value = oCustomClass.JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(28) = New SqlParameter("@JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(28).Value = oCustomClass.JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(27) = New SqlParameter("@JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(27).Value = oCustomClass.JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(26) = New SqlParameter("@JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(26).Value = oCustomClass.JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(25) = New SqlParameter("@JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(25).Value = oCustomClass.JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(24) = New SqlParameter("@JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(24).Value = oCustomClass.JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(23) = New SqlParameter("@JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(23).Value = oCustomClass.JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(22) = New SqlParameter("@JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(22).Value = oCustomClass.JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(21) = New SqlParameter("@JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(21).Value = oCustomClass.JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(20) = New SqlParameter("@JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(20).Value = oCustomClass.JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(19) = New SqlParameter("@JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(19).Value = oCustomClass.JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(18) = New SqlParameter("@JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(18).Value = oCustomClass.JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(17) = New SqlParameter("@JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(17).Value = oCustomClass.JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(16) = New SqlParameter("@JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(16).Value = oCustomClass.JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(15) = New SqlParameter("@JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(15).Value = oCustomClass.JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(14) = New SqlParameter("@JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(14).Value = oCustomClass.JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(13) = New SqlParameter("@JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(13).Value = oCustomClass.JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(12) = New SqlParameter("@JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(12).Value = oCustomClass.JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(11) = New SqlParameter("@JMLH_TNG_KRJ_TTL__", SqlDbType.Int)
        params(11).Value = oCustomClass.JMLH_TNG_KRJ_TTL__
        params(10) = New SqlParameter("@JMLH_TNG_KRJ_PMSRN_TTL_", SqlDbType.Int)
        params(10).Value = oCustomClass.JMLH_TNG_KRJ_PMSRN_TTL_
        params(9) = New SqlParameter("@JMLH_TNG_KRJ_PRSNL_TTL_", SqlDbType.Int)
        params(9).Value = oCustomClass.JMLH_TNG_KRJ_PRSNL_TTL_
        params(8) = New SqlParameter("@JMLH_TNG_KRJ_PNGHN_TTL_", SqlDbType.Int)
        params(8).Value = oCustomClass.JMLH_TNG_KRJ_PNGHN_TTL_
        params(7) = New SqlParameter("@JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TTL_", SqlDbType.Int)
        params(7).Value = oCustomClass.JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TTL_
        params(6) = New SqlParameter("@JMLH_TNG_KRJ_KNGN_DN_KNTNS_TTL_", SqlDbType.Int)
        params(6).Value = oCustomClass.JMLH_TNG_KRJ_KNGN_DN_KNTNS_TTL_
        params(5) = New SqlParameter("@JMLH_TNG_KRJ_MNJMN_RSK_TTL_", SqlDbType.Int)
        params(5).Value = oCustomClass.JMLH_TNG_KRJ_MNJMN_RSK_TTL_
        params(4) = New SqlParameter("@JMLH_TNG_KRJ_DT_NTRNL_TTL_", SqlDbType.Int)
        params(4).Value = oCustomClass.JMLH_TNG_KRJ_DT_NTRNL_TTL_
        params(3) = New SqlParameter("@JMLH_TNG_KRJ_LGL_TTL_", SqlDbType.Int)
        params(3).Value = oCustomClass.JMLH_TNG_KRJ_LGL_TTL_
        params(2) = New SqlParameter("@JMLH_TNG_KRJ_TKNLG_NFRMS_T_TTL_", SqlDbType.Int)
        params(2).Value = oCustomClass.JMLH_TNG_KRJ_TKNLG_NFRMS_T_TTL_
        params(1) = New SqlParameter("@JMLH_TNG_KRJ_FNGS_LNNY_TTL_", SqlDbType.Int)
        params(1).Value = oCustomClass.JMLH_TNG_KRJ_FNGS_LNNY_TTL_
        params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0043.SIPP0043Save")
        End Try
    End Function

    Public Function SIPP0043Add(ByVal oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043
        Dim params(77) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0043

        params(0) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(0).Value = oCustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(1) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(1).Value = oCustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(2) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(2).Value = oCustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(3) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(3).Value = oCustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(4) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(4).Value = oCustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(5) = New SqlParameter("@JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(5).Value = oCustomClass.JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(6) = New SqlParameter("@JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(6).Value = oCustomClass.JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(7) = New SqlParameter("@JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(7).Value = oCustomClass.JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(8) = New SqlParameter("@JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(8).Value = oCustomClass.JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(9) = New SqlParameter("@JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(9).Value = oCustomClass.JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(10) = New SqlParameter("@JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(10).Value = oCustomClass.JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(11) = New SqlParameter("@JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(11).Value = oCustomClass.JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(12) = New SqlParameter("@JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(12).Value = oCustomClass.JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(13) = New SqlParameter("@JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(13).Value = oCustomClass.JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(14) = New SqlParameter("@JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(14).Value = oCustomClass.JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(15) = New SqlParameter("@JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(15).Value = oCustomClass.JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(16) = New SqlParameter("@JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(16).Value = oCustomClass.JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(17) = New SqlParameter("@JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(17).Value = oCustomClass.JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(18) = New SqlParameter("@JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(18).Value = oCustomClass.JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(19) = New SqlParameter("@JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(19).Value = oCustomClass.JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(20) = New SqlParameter("@JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(20).Value = oCustomClass.JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(21) = New SqlParameter("@JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(21).Value = oCustomClass.JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(22) = New SqlParameter("@JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(22).Value = oCustomClass.JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(23) = New SqlParameter("@JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(23).Value = oCustomClass.JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(24) = New SqlParameter("@JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(24).Value = oCustomClass.JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(25) = New SqlParameter("@JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(25).Value = oCustomClass.JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(26) = New SqlParameter("@JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(26).Value = oCustomClass.JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(27) = New SqlParameter("@JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(27).Value = oCustomClass.JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(28) = New SqlParameter("@JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(28).Value = oCustomClass.JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(29) = New SqlParameter("@JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(29).Value = oCustomClass.JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(30) = New SqlParameter("@JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(30).Value = oCustomClass.JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(31) = New SqlParameter("@JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(31).Value = oCustomClass.JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(32) = New SqlParameter("@JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(32).Value = oCustomClass.JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(33) = New SqlParameter("@JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(33).Value = oCustomClass.JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(34) = New SqlParameter("@JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(34).Value = oCustomClass.JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(35) = New SqlParameter("@JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(35).Value = oCustomClass.JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(36) = New SqlParameter("@JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(36).Value = oCustomClass.JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(37) = New SqlParameter("@JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(37).Value = oCustomClass.JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(38) = New SqlParameter("@JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(38).Value = oCustomClass.JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(39) = New SqlParameter("@JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(39).Value = oCustomClass.JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(40) = New SqlParameter("@JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(40).Value = oCustomClass.JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(41) = New SqlParameter("@JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(41).Value = oCustomClass.JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(42) = New SqlParameter("@JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(42).Value = oCustomClass.JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(43) = New SqlParameter("@JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(43).Value = oCustomClass.JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(44) = New SqlParameter("@JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(44).Value = oCustomClass.JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(45) = New SqlParameter("@JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(45).Value = oCustomClass.JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(46) = New SqlParameter("@JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(46).Value = oCustomClass.JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(47) = New SqlParameter("@JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(47).Value = oCustomClass.JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(48) = New SqlParameter("@JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(48).Value = oCustomClass.JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(49) = New SqlParameter("@JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(49).Value = oCustomClass.JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(50) = New SqlParameter("@JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(50).Value = oCustomClass.JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(51) = New SqlParameter("@JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(51).Value = oCustomClass.JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(52) = New SqlParameter("@JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(52).Value = oCustomClass.JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(53) = New SqlParameter("@JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(53).Value = oCustomClass.JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(54) = New SqlParameter("@JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(54).Value = oCustomClass.JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(55) = New SqlParameter("@JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(55).Value = oCustomClass.JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(56) = New SqlParameter("@JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(56).Value = oCustomClass.JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(57) = New SqlParameter("@JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(57).Value = oCustomClass.JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(58) = New SqlParameter("@JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(58).Value = oCustomClass.JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(59) = New SqlParameter("@JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(59).Value = oCustomClass.JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(60) = New SqlParameter("@JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(60).Value = oCustomClass.JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(61) = New SqlParameter("@JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(61).Value = oCustomClass.JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(62) = New SqlParameter("@JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(62).Value = oCustomClass.JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(63) = New SqlParameter("@JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(63).Value = oCustomClass.JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(64) = New SqlParameter("@JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS", SqlDbType.Int)
        params(64).Value = oCustomClass.JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS
        params(65) = New SqlParameter("@JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY", SqlDbType.Int)
        params(65).Value = oCustomClass.JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY
        params(66) = New SqlParameter("@JMLH_TNG_KRJ_TTL__", SqlDbType.Int)
        params(66).Value = oCustomClass.JMLH_TNG_KRJ_TTL__
        params(67) = New SqlParameter("@JMLH_TNG_KRJ_PMSRN_TTL_", SqlDbType.Int)
        params(67).Value = oCustomClass.JMLH_TNG_KRJ_PMSRN_TTL_
        params(68) = New SqlParameter("@JMLH_TNG_KRJ_PRSNL_TTL_", SqlDbType.Int)
        params(68).Value = oCustomClass.JMLH_TNG_KRJ_PRSNL_TTL_
        params(69) = New SqlParameter("@JMLH_TNG_KRJ_PNGHN_TTL_", SqlDbType.Int)
        params(69).Value = oCustomClass.JMLH_TNG_KRJ_PNGHN_TTL_
        params(70) = New SqlParameter("@JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TTL_", SqlDbType.Int)
        params(70).Value = oCustomClass.JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TTL_
        params(71) = New SqlParameter("@JMLH_TNG_KRJ_KNGN_DN_KNTNS_TTL_", SqlDbType.Int)
        params(71).Value = oCustomClass.JMLH_TNG_KRJ_KNGN_DN_KNTNS_TTL_
        params(72) = New SqlParameter("@JMLH_TNG_KRJ_MNJMN_RSK_TTL_", SqlDbType.Int)
        params(72).Value = oCustomClass.JMLH_TNG_KRJ_MNJMN_RSK_TTL_
        params(73) = New SqlParameter("@JMLH_TNG_KRJ_DT_NTRNL_TTL_", SqlDbType.Int)
        params(73).Value = oCustomClass.JMLH_TNG_KRJ_DT_NTRNL_TTL_
        params(74) = New SqlParameter("@JMLH_TNG_KRJ_LGL_TTL_", SqlDbType.Int)
        params(74).Value = oCustomClass.JMLH_TNG_KRJ_LGL_TTL_
        params(75) = New SqlParameter("@JMLH_TNG_KRJ_TKNLG_NFRMS_T_TTL_", SqlDbType.Int)
        params(75).Value = oCustomClass.JMLH_TNG_KRJ_TKNLG_NFRMS_T_TTL_
        params(76) = New SqlParameter("@JMLH_TNG_KRJ_FNGS_LNNY_TTL_", SqlDbType.Int)
        params(76).Value = oCustomClass.JMLH_TNG_KRJ_FNGS_LNNY_TTL_
        params(77) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 8)
        params(77).Value = oCustomClass.BULANDATA
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, ADD, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0043.SIPP0043Add")
        End Try

    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043
        Dim oReturnValue As New Parameter.SIPP0043
        Dim params(2) As SqlParameter

        params(0) = New SqlParameter("@valueelement", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Valueelement
        params(1) = New SqlParameter("@valueparent", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.Valueparent
        params(2) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 50)
        params(2).Value = oCustomClass.Valuemaster
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2490.GetCbo")
        End Try
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043
        Dim oReturnValue As New Parameter.SIPP0043
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0043.GetCboBulandataSIPP")
        End Try
    End Function

    Public Function SIPP0043Delete(ByVal oCustomClass As Parameter.SIPP0043) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function
End Class
