﻿

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class SIPP0025 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP0025List"
    Private Const SaveAdd As String = "spSIPP0025SaveAdd"
    Private Const SaveEdit As String = "spSIPP0025SaveEdit"
    Private Const LIST_EDIT As String = "spSIPP0025EditList"
    Private Const spFillCbo As String = "spGetCboSIPP"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
    Private Const Delete As String = "spSIPP0025Delete"

#End Region
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0025) As Parameter.SIPP0025
        Dim oReturnValue As New Parameter.SIPP0025
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0025.GetCboBulandataSIPP")
        End Try
    End Function
    Public Function GetSIPP0025(ByVal oCustomClass As Parameter.SIPP0025) As Parameter.SIPP0025
        Dim oReturnValue As New Parameter.SIPP0025
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0010.GetSIPP0010")
        End Try
    End Function

    Public Function SIPP0025Delete(ByVal oCustomClass As Parameter.SIPP0025) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.Id
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, Delete, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

    Public Function SIPP0025SaveEdit(ocustomClass As Parameter.SIPP0025) As Parameter.SIPP0025
        Dim params(10) As SqlParameter
        Try
            params(0) = New SqlParameter("NMR_SRT_PNCTTN", SqlDbType.VarChar, 25)
            params(0).Value = ocustomClass.NMR_SRT_PNCTTN
            params(1) = New SqlParameter("LMT", SqlDbType.VarChar, 50)
            params(1).Value = ocustomClass.LMT
            params(2) = New SqlParameter("KCMTN", SqlDbType.VarChar, 50)
            params(2).Value = ocustomClass.KCMTN
            params(3) = New SqlParameter("NMR_TPLN", SqlDbType.VarChar, 15)
            params(3).Value = ocustomClass.NMR_TPLN
            params(4) = New SqlParameter("JMLH_TNG_KRJ", SqlDbType.Int)
            params(4).Value = ocustomClass.JMLH_TNG_KRJ
            params(5) = New SqlParameter("NM_PNNGGNG_JWB_KNTR", SqlDbType.VarChar, 50)
            params(5).Value = ocustomClass.NM_PNNGGNG_JWB_KNTR
            params(6) = New SqlParameter("JNS_KNTR", SqlDbType.VarChar, 100)
            params(6).Value = ocustomClass.JNS_KNTR
            params(7) = New SqlParameter("KOTA", SqlDbType.VarChar, 100)
            params(7).Value = ocustomClass.KOTA
            params(8) = New SqlParameter("TNGGL_SRT_PNCTTN", SqlDbType.Date)
            params(8).Value = ocustomClass.TNGGL_SRT_PNCTTN
            params(9) = New SqlParameter("@ID", SqlDbType.Int)
            params(9).Value = ocustomClass.ID
            params(10) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(10).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SaveEdit, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0025.SIPP0025SaveEdit")
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function SIPP0025SaveAdd(ocustomClass As Parameter.SIPP0025) As Parameter.SIPP0025
        Dim params(9) As SqlParameter
        Try
            params(0) = New SqlParameter("NMR_SRT_PNCTTN", SqlDbType.VarChar, 25)
            params(0).Value = ocustomClass.NMR_SRT_PNCTTN
            params(1) = New SqlParameter("LMT", SqlDbType.VarChar, 50)
            params(1).Value = ocustomClass.LMT
            params(2) = New SqlParameter("KCMTN", SqlDbType.VarChar, 50)
            params(2).Value = ocustomClass.KCMTN
            params(3) = New SqlParameter("NMR_TPLN", SqlDbType.VarChar, 15)
            params(3).Value = ocustomClass.NMR_TPLN
            params(4) = New SqlParameter("JMLH_TNG_KRJ", SqlDbType.Int)
            params(4).Value = ocustomClass.JMLH_TNG_KRJ
            params(5) = New SqlParameter("NM_PNNGGNG_JWB_KNTR", SqlDbType.VarChar, 50)
            params(5).Value = ocustomClass.NM_PNNGGNG_JWB_KNTR
            params(6) = New SqlParameter("JNS_KNTR", SqlDbType.VarChar, 100)
            params(6).Value = ocustomClass.JNS_KNTR
            params(7) = New SqlParameter("KOTA", SqlDbType.VarChar, 100)
            params(7).Value = ocustomClass.KOTA
            params(8) = New SqlParameter("TNGGL_SRT_PNCTTN", SqlDbType.Date)
            params(8).Value = ocustomClass.TNGGL_SRT_PNCTTN
            params(9) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 100)
            params(9).Value = ocustomClass.BULANDATA

            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SaveAdd, params)

        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2300.SIPP2300SaveAdd")
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSIPP0025Edit(ByVal ocustomClass As Parameter.SIPP0025) As Parameter.SIPP0025
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0025


        params(0) = New SqlParameter("ID", SqlDbType.VarChar, 100)
        params(0).Value = ocustomClass.Id

        Try
            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0025.GetSIPP0025Edit")
        End Try
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP0025) As Parameter.SIPP0025
        Dim oReturnValue As New Parameter.SIPP0025
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@valueparent", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.valueparent
        params(1) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.valuemaster
        params(2) = New SqlParameter("@valueelement", SqlDbType.VarChar, 20)
        params(2).Value = oCustomClass.valueelement

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.D01.GetCbo")
        End Try
    End Function

    'Public Function Generate(ByVal oCustomClass As Parameter.D01) As Parameter.D01
    '    Dim oReturnValue As New Parameter.D01

    '    'Dim params(0) As SqlParameter
    '    'params(0) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
    '    'params(0).Value = oCustomClass.CIF
    '    Try
    '        oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, Generate_TEXT).Tables(0)

    '        Return oReturnValue
    '    Catch exp As Exception
    '        Throw New Exception("Error On DataAccess.SLIK.D01.Generate")
    '    End Try
    'End Function

End Class


