﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region
Public Class SIPP0000 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP0000List"
    Private Const SAVE As String = "spSIPP0000Save"
    Private Const LIST_EDIT As String = "spSIPP0000EditList"
    Private Const ADD As String = "spSIPP0000Add"
    Private Const FILL_CBO As String = "spGetCboSIPP"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
    Private Const DELETE As String = "spSIPP0000Delete"
#End Region

    Public Function GetSIPP0000(ByVal oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000
        Dim oReturnValue As New Parameter.SIPP0000
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0000.GetSIPP0000")
        End Try
    End Function

    Public Function GetSelectSIPP0000(ByVal oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@ID", SqlDbType.Int)
            params(0).Value = oCustomClass.ID
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP0000Edit(ByVal oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0000

        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0000.SIPP0000Edit")
        End Try
    End Function

    Public Function SIPP0000Save(ByVal oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000
        Dim params(34) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0000

        params(34) = New SqlParameter("ID", SqlDbType.Int)
        params(34).Value = oCustomClass.ID
        params(33) = New SqlParameter("@NAMALENGKAP", SqlDbType.VarChar, 50)
        params(33).Value = oCustomClass.NM_LNGKP_PRSHN
        params(32) = New SqlParameter("@NAMASEBUTAN", SqlDbType.VarChar, 50)
        params(32).Value = oCustomClass.NM_SBTN_PRSHN
        params(31) = New SqlParameter("@NPWP", SqlDbType.VarChar, 100)
        params(31).Value = oCustomClass.NPWP
        params(30) = New SqlParameter("@STTSKEPEMILIKAN", SqlDbType.VarChar, 100)
        params(30).Value = oCustomClass.STTS_KPMLKN_PRSHN
        params(29) = New SqlParameter("@BENTUKBADANUSAHA", SqlDbType.VarChar, 100)
        params(29).Value = oCustomClass.BNTK_BDN_SH_PRSHN_PMBYN
        params(28) = New SqlParameter("@STATUSKEGIATANSYARIAH", SqlDbType.VarChar, 100)
        params(28).Value = oCustomClass.STTS_KGTN_SH_SYRH
        params(27) = New SqlParameter("@TANGGALPENDIRIAN", SqlDbType.Date)
        params(27).Value = oCustomClass.TNGGL_PNDRN
        params(26) = New SqlParameter("@BIDANGUSAHA", SqlDbType.VarChar, 100)
        params(26).Value = oCustomClass.BDNG_SH_PRSHN_PMBYN
        params(25) = New SqlParameter("@BIDANGUSAHA1", SqlDbType.VarChar, 100)
        params(25).Value = oCustomClass.BDNG_SH_PRSHN_PMBYN_1
        params(24) = New SqlParameter("@BIDANGUSAHA2", SqlDbType.VarChar, 100)
        params(24).Value = oCustomClass.BDNG_SH_PRSHN_PMBYN_2
        params(23) = New SqlParameter("@BIDANGUSAHA3", SqlDbType.VarChar, 100)
        params(23).Value = oCustomClass.BDNG_SH_PRSHN_PMBYN_3
        params(22) = New SqlParameter("@BIDANGUSAHA4", SqlDbType.VarChar, 100)
        params(22).Value = oCustomClass.BDNG_SH_PRSHN_PMBYN_4
        params(21) = New SqlParameter("@BIDANGUSAHA5", SqlDbType.VarChar, 100)
        params(21).Value = oCustomClass.BDNG_SH_PRSHN_PMBYN_5
        params(20) = New SqlParameter("@BIDANGUSAHA6", SqlDbType.VarChar, 100)
        params(20).Value = oCustomClass.BDNG_SH_PRSHN_PMBYN_6
        params(19) = New SqlParameter("@ALAMATLENGKAP", SqlDbType.VarChar, 100)
        params(19).Value = oCustomClass.LMT_LNGKP
        params(18) = New SqlParameter("@DATI", SqlDbType.VarChar, 100)
        params(18).Value = oCustomClass.LKS_DT__KNTR
        params(17) = New SqlParameter("@KODEPOS", SqlDbType.VarChar, 5)
        params(17).Value = oCustomClass.KD_PS
        params(16) = New SqlParameter("@STTSKEPEMILIKANGEDUNG", SqlDbType.VarChar, 100)
        params(16).Value = oCustomClass.STTS_KPMLKN_GDNG
        params(15) = New SqlParameter("@NOTELPON", SqlDbType.VarChar, 100)
        params(15).Value = oCustomClass.NMR_TLPN
        params(14) = New SqlParameter("@NOFAX", SqlDbType.VarChar, 100)
        params(14).Value = oCustomClass.NMR_FKSML
        params(13) = New SqlParameter("@JMLKANTORCBG", SqlDbType.Int)
        params(13).Value = oCustomClass.JMLH_KNTR_CBNG
        params(12) = New SqlParameter("@JMLKANTORSLNCBG", SqlDbType.Int)
        params(12).Value = oCustomClass.JMLH_JRNGN_KNTR_SLN_KNTR_CBNG
        params(11) = New SqlParameter("@JMLTNGKRJPUSAT", SqlDbType.Int)
        params(11).Value = oCustomClass.JMLH_TNG_KRJ_KNTR_PST
        params(10) = New SqlParameter("@JMLTNGKRJCBG", SqlDbType.Int)
        params(10).Value = oCustomClass.JMLH_TNG_KRJ_KNTR_CBNG
        params(9) = New SqlParameter("@JMLTNGKRJSLNCBG", SqlDbType.Int)
        params(9).Value = oCustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG
        params(8) = New SqlParameter("@NAMAPTGSPENYUSUNLAP", SqlDbType.VarChar, 100)
        params(8).Value = oCustomClass.NM_PTGS_PNYSN_LPRN
        params(7) = New SqlParameter("@BAGIANPTGSPENYUSUNLAP", SqlDbType.VarChar, 100)
        params(7).Value = oCustomClass.BGNDVS_PTGS_PNYSN_LPRN
        params(6) = New SqlParameter("@NOTELPPTGSPENYUSUNLAP", SqlDbType.VarChar, 100)
        params(6).Value = oCustomClass.NMR_TLPN_PTGS_PNYSN_LPRN
        params(5) = New SqlParameter("@NOFAXPTGSPENYUSUNLAP", SqlDbType.VarChar, 100)
        params(5).Value = oCustomClass.NMR_FKSML_PTGS_PNYSN_LPRN
        params(4) = New SqlParameter("@NAMAPENANGGUNGJAWABLAP", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.NM_PNNGGNG_JWB_LPRN
        params(3) = New SqlParameter("@BAGIANPENANGGUNGJAWABLAP", SqlDbType.VarChar, 100)
        params(3).Value = oCustomClass.BGNDVS_PNNGGNG_JWB_LPRN
        params(2) = New SqlParameter("@NOTELPPENANGGUNGJAWABLAP", SqlDbType.VarChar, 100)
        params(2).Value = oCustomClass.NMR_TLPN_PNNGGNG_JWB_LPRN
        params(1) = New SqlParameter("@NOFAXPENANGGUNGJAWABLAP", SqlDbType.VarChar, 100)
        params(1).Value = oCustomClass.NMR_FKSML_PNNGGNG_JWB_LPRN
        params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0000.SIPP0000Save")
        End Try
    End Function

    Public Function SIPP0000Add(ByVal oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000
        Dim params(33) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP0000

        params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 100)
        params(0).Value = oCustomClass.BULANDATA
        params(1) = New SqlParameter("@NAMALENGKAP", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.NM_LNGKP_PRSHN
        params(2) = New SqlParameter("@NAMASEBUTAN", SqlDbType.VarChar, 50)
        params(2).Value = oCustomClass.NM_SBTN_PRSHN
        params(3) = New SqlParameter("@NPWP", SqlDbType.VarChar, 100)
        params(3).Value = oCustomClass.NPWP
        params(4) = New SqlParameter("@STTSKEPEMILIKAN", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.STTS_KPMLKN_PRSHN
        params(5) = New SqlParameter("@BENTUKBADANUSAHA", SqlDbType.VarChar, 100)
        params(5).Value = oCustomClass.BNTK_BDN_SH_PRSHN_PMBYN
        params(6) = New SqlParameter("@STATUSKEGIATANSYARIAH", SqlDbType.VarChar, 100)
        params(6).Value = oCustomClass.STTS_KGTN_SH_SYRH
        params(7) = New SqlParameter("@TANGGALPENDIRIAN", SqlDbType.Date)
        params(7).Value = oCustomClass.TNGGL_PNDRN
        params(8) = New SqlParameter("@BIDANGUSAHA", SqlDbType.VarChar, 100)
        params(8).Value = oCustomClass.BDNG_SH_PRSHN_PMBYN
        params(9) = New SqlParameter("@BIDANGUSAHA1", SqlDbType.VarChar, 100)
        params(9).Value = oCustomClass.BDNG_SH_PRSHN_PMBYN_1
        params(10) = New SqlParameter("@BIDANGUSAHA2", SqlDbType.VarChar, 100)
        params(10).Value = oCustomClass.BDNG_SH_PRSHN_PMBYN_2
        params(11) = New SqlParameter("@BIDANGUSAHA3", SqlDbType.VarChar, 100)
        params(11).Value = oCustomClass.BDNG_SH_PRSHN_PMBYN_3
        params(12) = New SqlParameter("@BIDANGUSAHA4", SqlDbType.VarChar, 100)
        params(12).Value = oCustomClass.BDNG_SH_PRSHN_PMBYN_4
        params(13) = New SqlParameter("@BIDANGUSAHA5", SqlDbType.VarChar, 100)
        params(13).Value = oCustomClass.BDNG_SH_PRSHN_PMBYN_5
        params(14) = New SqlParameter("@BIDANGUSAHA6", SqlDbType.VarChar, 100)
        params(14).Value = oCustomClass.BDNG_SH_PRSHN_PMBYN_6
        params(15) = New SqlParameter("@ALAMATLENGKAP", SqlDbType.VarChar, 100)
        params(15).Value = oCustomClass.LMT_LNGKP
        params(16) = New SqlParameter("@DATI", SqlDbType.VarChar, 100)
        params(16).Value = oCustomClass.LKS_DT__KNTR
        params(17) = New SqlParameter("@KODEPOS", SqlDbType.VarChar, 5)
        params(17).Value = oCustomClass.KD_PS
        params(18) = New SqlParameter("@STTSKEPEMILIKANGEDUNG", SqlDbType.VarChar, 100)
        params(18).Value = oCustomClass.STTS_KPMLKN_GDNG
        params(19) = New SqlParameter("@NOTELPON", SqlDbType.VarChar, 100)
        params(19).Value = oCustomClass.NMR_TLPN
        params(20) = New SqlParameter("@NOFAX", SqlDbType.VarChar, 100)
        params(20).Value = oCustomClass.NMR_FKSML
        params(21) = New SqlParameter("@JMLKANTORCBG", SqlDbType.Int)
        params(21).Value = oCustomClass.JMLH_KNTR_CBNG
        params(22) = New SqlParameter("@JMLKANTORSLNCBG", SqlDbType.Int)
        params(22).Value = oCustomClass.JMLH_JRNGN_KNTR_SLN_KNTR_CBNG
        params(23) = New SqlParameter("@JMLTNGKRJPUSAT", SqlDbType.Int)
        params(23).Value = oCustomClass.JMLH_TNG_KRJ_KNTR_PST
        params(24) = New SqlParameter("@JMLTNGKRJCBG", SqlDbType.Int)
        params(24).Value = oCustomClass.JMLH_TNG_KRJ_KNTR_CBNG
        params(25) = New SqlParameter("@JMLTNGKRJSLNCBG", SqlDbType.Int)
        params(25).Value = oCustomClass.JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG
        params(26) = New SqlParameter("@NAMAPTGSPENYUSUNLAP", SqlDbType.VarChar, 100)
        params(26).Value = oCustomClass.NM_PTGS_PNYSN_LPRN
        params(27) = New SqlParameter("@BAGIANPTGSPENYUSUNLAP", SqlDbType.VarChar, 100)
        params(27).Value = oCustomClass.BGNDVS_PTGS_PNYSN_LPRN
        params(28) = New SqlParameter("@NOTELPPTGSPENYUSUNLAP", SqlDbType.VarChar, 100)
        params(28).Value = oCustomClass.NMR_TLPN_PTGS_PNYSN_LPRN
        params(29) = New SqlParameter("@NOFAXPTGSPENYUSUNLAP", SqlDbType.VarChar, 100)
        params(29).Value = oCustomClass.NMR_FKSML_PTGS_PNYSN_LPRN
        params(30) = New SqlParameter("@NAMAPENANGGUNGJAWABLAP", SqlDbType.VarChar, 100)
        params(30).Value = oCustomClass.NM_PNNGGNG_JWB_LPRN
        params(31) = New SqlParameter("@BAGIANPENANGGUNGJAWABLAP", SqlDbType.VarChar, 100)
        params(31).Value = oCustomClass.BGNDVS_PNNGGNG_JWB_LPRN
        params(32) = New SqlParameter("@NOTELPPENANGGUNGJAWABLAP", SqlDbType.VarChar, 100)
        params(32).Value = oCustomClass.NMR_TLPN_PNNGGNG_JWB_LPRN
        params(33) = New SqlParameter("@NOFAXPENANGGUNGJAWABLAP", SqlDbType.VarChar, 100)
        params(33).Value = oCustomClass.NMR_FKSML_PNNGGNG_JWB_LPRN
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, ADD, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0000.SIPP0000Add")
        End Try

    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000
        Dim oReturnValue As New Parameter.SIPP0000
        Dim params(2) As SqlParameter

        params(0) = New SqlParameter("@valueelement", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Valueelement
        params(1) = New SqlParameter("@valueparent", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.Valueparent
        params(2) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 50)
        params(2).Value = oCustomClass.Valuemaster
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2490.GetCbo")
        End Try
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000
        Dim oReturnValue As New Parameter.SIPP0000
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0000.GetCboBulandataSIPP")
        End Try
    End Function

    Public Function SIPP0000Delete(ByVal oCustomClass As Parameter.SIPP0000) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

End Class
