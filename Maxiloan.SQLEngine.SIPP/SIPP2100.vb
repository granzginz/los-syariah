﻿

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class SIPP2100 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP2100List"
    Private Const SaveAdd As String = "spSIPP2100SaveAdd"
    Private Const SaveEdit As String = "spSIPP2100SaveEdit"
    Private Const LIST_EDIT As String = "spSIPP2100EditList"
    Private Const spFillCbo As String = "spGetCboSIPP"
    Private Const Delete As String = "spSIPP2100Delete"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"

#End Region


    Public Function GetSIPP2100(ByVal oCustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
        Dim oReturnValue As New Parameter.SIPP2100
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0010.GetSIPP0010")
        End Try
    End Function


    Public Function GetSelectSIPP2100(ByVal oCustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@ID", SqlDbType.Int)
            params(0).Value = oCustomClass.ID
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2100Delete(ByVal oCustomClass As Parameter.SIPP2100) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.Id
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, Delete, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

    Public Function SIPP2100SaveEdit(ocustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
        Dim params(36) As SqlParameter

        params(0) = New SqlParameter("@NMR_KNTRK", SqlDbType.VarChar, 50)
            params(0).Value = ocustomClass.NMR_KNTRK
            params(1) = New SqlParameter("@JNS_PMBYN", SqlDbType.VarChar, 50)
            params(1).Value = ocustomClass.JNS_PMBYN
            params(2) = New SqlParameter("@SKM_PMBYN", SqlDbType.VarChar, 50)
            params(2).Value = ocustomClass.SKM_PMBYN
            params(3) = New SqlParameter("@TJN_PMBYN", SqlDbType.VarChar, 50)
            params(3).Value = ocustomClass.TJN_PMBYN
            params(4) = New SqlParameter("@JNS_BRNG_DN_JS", SqlDbType.VarChar, 50)
            params(4).Value = ocustomClass.JNS_BRNG_DN_JS
            params(5) = New SqlParameter("@NL_BRNGJS_YNG_DBY", SqlDbType.VarChar, 50)
            params(5).Value = ocustomClass.NL_BRNGJS_YNG_DBY
            params(6) = New SqlParameter("@TNGGL_ML", SqlDbType.VarChar, 50)
            params(6).Value = ocustomClass.TNGGL_ML
            params(7) = New SqlParameter("@TNGGL_JTH_TMP", SqlDbType.VarChar, 50)
            params(7).Value = ocustomClass.TNGGL_JTH_TMP
            params(8) = New SqlParameter("@JNS_BNGMRGNBG_HSLMBL_JS", SqlDbType.VarChar, 50)
            params(8).Value = ocustomClass.JNS_BNGMRGNBG_HSLMBL_JS
            params(9) = New SqlParameter("@NL_MRGNMBL_JS", SqlDbType.VarChar, 50)
            params(9).Value = ocustomClass.NL_MRGNMBL_JS
            params(10) = New SqlParameter("@TNGKT_BNGBG_HSL", SqlDbType.VarChar, 50)
            params(10).Value = ocustomClass.TNGKT_BNGBG_HSL
            params(11) = New SqlParameter("@NL_TRCTT", SqlDbType.VarChar, 50)
            params(11).Value = ocustomClass.NL_TRCTT
            params(12) = New SqlParameter("@SMBR_PMBYN", SqlDbType.VarChar, 50)
            params(12).Value = ocustomClass.SMBR_PMBYN
            params(13) = New SqlParameter("@PRS_PRSHN_PD_PMBYN_BRSM", SqlDbType.VarChar, 50)
            params(13).Value = ocustomClass.PRS_PRSHN_PD_PMBYN_BRSM
            params(14) = New SqlParameter("@NG_MK", SqlDbType.VarChar, 50)
            params(14).Value = ocustomClass.NG_MK
            params(15) = New SqlParameter("@KLTS", SqlDbType.VarChar, 50)
            params(15).Value = ocustomClass.KLTS
            params(16) = New SqlParameter("@JNS_MT_NG", SqlDbType.VarChar, 50)
            params(16).Value = ocustomClass.JNS_MT_NG
            params(17) = New SqlParameter("@TGHN_PMBYN_DLM_MT_NG_SL", SqlDbType.VarChar, 50)
            params(17).Value = ocustomClass.TGHN_PMBYN_DLM_MT_NG_SL
            params(18) = New SqlParameter("@TGHN_PMBYN", SqlDbType.VarChar, 50)
            params(18).Value = ocustomClass.TGHN_PMBYN
            params(19) = New SqlParameter("@BNGMRGN_DTNGGHKN_DLM_MT_NG_SL", SqlDbType.VarChar, 50)
            params(19).Value = ocustomClass.BNGMRGN_DTNGGHKN_DLM_MT_NG_SL
            params(20) = New SqlParameter("@BNGMRGN_DTNGGHKN", SqlDbType.VarChar, 50)
            params(20).Value = ocustomClass.BNGMRGN_DTNGGHKN
            params(21) = New SqlParameter("@PTNG_PMBYN_PKK_DLM_MT_NG_SL", SqlDbType.VarChar, 50)
            params(21).Value = ocustomClass.PTNG_PMBYN_PKK_DLM_MT_NG_SL
            params(22) = New SqlParameter("@PTNG_PMBYN__PKK", SqlDbType.VarChar, 50)
            params(22).Value = ocustomClass.PTNG_PMBYN__PKK
            params(23) = New SqlParameter("@PRPRS_PNJMNN_KRDT_T_SRNS_KRDT", SqlDbType.VarChar, 50)
            params(23).Value = ocustomClass.PRPRS_PNJMNN_KRDT_T_SRNS_KRDT
            params(24) = New SqlParameter("@NMR_DBTR", SqlDbType.VarChar, 50)
            params(24).Value = ocustomClass.NMR_DBTR
            params(25) = New SqlParameter("@NM_DBTR", SqlDbType.VarChar, 50)
            params(25).Value = ocustomClass.NM_DBTR
            params(26) = New SqlParameter("@GRP_PHK_LWN", SqlDbType.VarChar, 50)
            params(26).Value = ocustomClass.GRP_PHK_LWN
            params(27) = New SqlParameter("@KTGR_SH_DBTR", SqlDbType.VarChar, 50)
            params(27).Value = ocustomClass.KTGR_SH_DBTR
            params(28) = New SqlParameter("@GLNGN_PHK_LWN", SqlDbType.VarChar, 50)
            params(28).Value = ocustomClass.GLNGN_PHK_LWN
            params(29) = New SqlParameter("@STTS_KTRKTN", SqlDbType.VarChar, 50)
            params(29).Value = ocustomClass.STTS_KTRKTN
            params(30) = New SqlParameter("@SKTR_KNM_LPNGN_SH", SqlDbType.VarChar, 50)
            params(30).Value = ocustomClass.SKTR_KNM_LPNGN_SH
            params(31) = New SqlParameter("@LKS_DT__PRYK", SqlDbType.VarChar, 50)
            params(31).Value = ocustomClass.LKS_DT__PRYK
            params(32) = New SqlParameter("@NMR_GNN", SqlDbType.VarChar, 50)
            params(32).Value = ocustomClass.NMR_GNN
            params(33) = New SqlParameter("@JNS_GNN", SqlDbType.VarChar, 50)
            params(33).Value = ocustomClass.JNS_GNN
            params(34) = New SqlParameter("@NL_GNNJMNN_YNG_DPT_DPRHTNGKN", SqlDbType.VarChar, 50)
            params(34).Value = ocustomClass.NL_GNNJMNN_YNG_DPT_DPRHTNGKN
            params(35) = New SqlParameter("@ID", SqlDbType.Int)
            params(35).Value = ocustomClass.ID
            params(36) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(36).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SaveEdit, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2100.SIPP2100SaveEdit")
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function SIPP2100SaveAdd(ocustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
        Dim params(35) As SqlParameter

        params(0) = New SqlParameter("@NMR_KNTRK", SqlDbType.VarChar, 50)
            params(0).Value = ocustomClass.NMR_KNTRK
            params(1) = New SqlParameter("@JNS_PMBYN", SqlDbType.VarChar, 50)
            params(1).Value = ocustomClass.JNS_PMBYN
            params(2) = New SqlParameter("@SKM_PMBYN", SqlDbType.VarChar, 50)
            params(2).Value = ocustomClass.SKM_PMBYN
            params(3) = New SqlParameter("@TJN_PMBYN", SqlDbType.VarChar, 50)
            params(3).Value = ocustomClass.TJN_PMBYN
            params(4) = New SqlParameter("@JNS_BRNG_DN_JS", SqlDbType.VarChar, 50)
            params(4).Value = ocustomClass.JNS_BRNG_DN_JS
            params(5) = New SqlParameter("@NL_BRNGJS_YNG_DBY", SqlDbType.VarChar, 50)
            params(5).Value = ocustomClass.NL_BRNGJS_YNG_DBY
            params(6) = New SqlParameter("@TNGGL_ML", SqlDbType.VarChar, 50)
            params(6).Value = ocustomClass.TNGGL_ML
            params(7) = New SqlParameter("@TNGGL_JTH_TMP", SqlDbType.VarChar, 50)
            params(7).Value = ocustomClass.TNGGL_JTH_TMP
            params(8) = New SqlParameter("@JNS_BNGMRGNBG_HSLMBL_JS", SqlDbType.VarChar, 50)
            params(8).Value = ocustomClass.JNS_BNGMRGNBG_HSLMBL_JS
            params(9) = New SqlParameter("@NL_MRGNMBL_JS", SqlDbType.VarChar, 50)
            params(9).Value = ocustomClass.NL_MRGNMBL_JS
            params(10) = New SqlParameter("@TNGKT_BNGBG_HSL", SqlDbType.VarChar, 50)
            params(10).Value = ocustomClass.TNGKT_BNGBG_HSL
            params(11) = New SqlParameter("@NL_TRCTT", SqlDbType.VarChar, 50)
            params(11).Value = ocustomClass.NL_TRCTT
            params(12) = New SqlParameter("@SMBR_PMBYN", SqlDbType.VarChar, 50)
            params(12).Value = ocustomClass.SMBR_PMBYN
            params(13) = New SqlParameter("@PRS_PRSHN_PD_PMBYN_BRSM", SqlDbType.VarChar, 50)
            params(13).Value = ocustomClass.PRS_PRSHN_PD_PMBYN_BRSM
            params(14) = New SqlParameter("@NG_MK", SqlDbType.VarChar, 50)
            params(14).Value = ocustomClass.NG_MK
            params(15) = New SqlParameter("@KLTS", SqlDbType.VarChar, 50)
            params(15).Value = ocustomClass.KLTS
            params(16) = New SqlParameter("@JNS_MT_NG", SqlDbType.VarChar, 50)
            params(16).Value = ocustomClass.JNS_MT_NG
            params(17) = New SqlParameter("@TGHN_PMBYN_DLM_MT_NG_SL", SqlDbType.VarChar, 50)
            params(17).Value = ocustomClass.TGHN_PMBYN_DLM_MT_NG_SL
            params(18) = New SqlParameter("@TGHN_PMBYN", SqlDbType.VarChar, 50)
            params(18).Value = ocustomClass.TGHN_PMBYN
            params(19) = New SqlParameter("@BNGMRGN_DTNGGHKN_DLM_MT_NG_SL", SqlDbType.VarChar, 50)
            params(19).Value = ocustomClass.BNGMRGN_DTNGGHKN_DLM_MT_NG_SL
            params(20) = New SqlParameter("@BNGMRGN_DTNGGHKN", SqlDbType.VarChar, 50)
            params(20).Value = ocustomClass.BNGMRGN_DTNGGHKN
            params(21) = New SqlParameter("@PTNG_PMBYN_PKK_DLM_MT_NG_SL", SqlDbType.VarChar, 50)
            params(21).Value = ocustomClass.PTNG_PMBYN_PKK_DLM_MT_NG_SL
            params(22) = New SqlParameter("@PTNG_PMBYN__PKK", SqlDbType.VarChar, 50)
            params(22).Value = ocustomClass.PTNG_PMBYN__PKK
            params(23) = New SqlParameter("@PRPRS_PNJMNN_KRDT_T_SRNS_KRDT", SqlDbType.VarChar, 50)
            params(23).Value = ocustomClass.PRPRS_PNJMNN_KRDT_T_SRNS_KRDT
            params(24) = New SqlParameter("@NMR_DBTR", SqlDbType.VarChar, 50)
            params(24).Value = ocustomClass.NMR_DBTR
            params(25) = New SqlParameter("@NM_DBTR", SqlDbType.VarChar, 50)
            params(25).Value = ocustomClass.NM_DBTR
            params(26) = New SqlParameter("@GRP_PHK_LWN", SqlDbType.VarChar, 50)
            params(26).Value = ocustomClass.GRP_PHK_LWN
            params(27) = New SqlParameter("@KTGR_SH_DBTR", SqlDbType.VarChar, 50)
            params(27).Value = ocustomClass.KTGR_SH_DBTR
            params(28) = New SqlParameter("@GLNGN_PHK_LWN", SqlDbType.VarChar, 50)
            params(28).Value = ocustomClass.GLNGN_PHK_LWN
            params(29) = New SqlParameter("@STTS_KTRKTN", SqlDbType.VarChar, 50)
            params(29).Value = ocustomClass.STTS_KTRKTN
            params(30) = New SqlParameter("@SKTR_KNM_LPNGN_SH", SqlDbType.VarChar, 50)
            params(30).Value = ocustomClass.SKTR_KNM_LPNGN_SH
            params(31) = New SqlParameter("@LKS_DT__PRYK", SqlDbType.VarChar, 50)
            params(31).Value = ocustomClass.LKS_DT__PRYK
            params(32) = New SqlParameter("@NMR_GNN", SqlDbType.VarChar, 50)
            params(32).Value = ocustomClass.NMR_GNN
            params(33) = New SqlParameter("@JNS_GNN", SqlDbType.VarChar, 50)
            params(33).Value = ocustomClass.JNS_GNN
            params(34) = New SqlParameter("@NL_GNNJMNN_YNG_DPT_DPRHTNGKN", SqlDbType.VarChar, 50)
        params(34).Value = ocustomClass.NL_GNNJMNN_YNG_DPT_DPRHTNGKN
        params(35) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 100)
        params(35).Value = ocustomClass.BULANDATA

        Try
            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, SaveAdd, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception

        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSIPP2100Edit(ByVal ocustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP2100


        params(0) = New SqlParameter("ID", SqlDbType.VarChar, 100)
        params(0).Value = ocustomClass.Id

        Try
            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2100.GetSIPP2100Edit")
        End Try
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
        Dim oReturnValue As New Parameter.SIPP2100
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@valueparent", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.valueparent
        params(1) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.valuemaster
        params(2) = New SqlParameter("@valueelement", SqlDbType.VarChar, 20)
        params(2).Value = oCustomClass.valueelement

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.D01.GetCbo")
        End Try
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
        Dim oReturnValue As New Parameter.SIPP2100
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2100.GetCboBulandataSIPP")
        End Try
    End Function
    'Public Function Generate(ByVal oCustomClass As Parameter.D01) As Parameter.D01
    '    Dim oReturnValue As New Parameter.D01

    '    'Dim params(0) As SqlParameter
    '    'params(0) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
    '    'params(0).Value = oCustomClass.CIF
    '    Try
    '        oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, Generate_TEXT).Tables(0)

    '        Return oReturnValue
    '    Catch exp As Exception
    '        Throw New Exception("Error On DataAccess.SLIK.D01.Generate")
    '    End Try
    'End Function

End Class


