﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region
Public Class SIPP1300 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP1300List"
    Private Const SAVE As String = "spSIPP1300Save"
    Private Const LIST_EDIT As String = "spSIPP1300EditList"
    Private Const ADD As String = "spSIPP1300Add"
    Private Const FILL_CBO As String = "spGetCboSIPP"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
    Private Const DELETE As String = "spSIPP1300Delete"
#End Region

    Public Function GetSIPP1300(ByVal oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300
        Dim oReturnValue As New Parameter.SIPP1300
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP1300.GetSIPP1300")
        End Try
    End Function

    Public Function GetSelectSIPP1300(ByVal oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@ID", SqlDbType.Int)
            params(0).Value = oCustomClass.ID
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP1300Edit(ByVal oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP1300

        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
        oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP1300.SIPP1300Edit")
        End Try
    End Function

    Public Function SIPP1300Save(ByVal oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300
        Dim params(151) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP1300

        params(151) = New SqlParameter("ID", SqlDbType.Int)
        params(151).Value = oCustomClass.ID
        params(150) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_NVSTS_TTL", SqlDbType.BigInt)
        params(150).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_NVSTS_TTL
        params(149) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_NVSTS_NDNSN_RPH", SqlDbType.BigInt)
        params(149).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_NVSTS_NDNSN_RPH
        params(148) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_NVSTS_MT_NG_SNG", SqlDbType.BigInt)
        params(148).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_NVSTS_MT_NG_SNG
        params(147) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_MDL_KRJ_TTL", SqlDbType.BigInt)
        params(147).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_MDL_KRJ_TTL
        params(146) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_MDL_KRJ_NDNSN_RPH", SqlDbType.BigInt)
        params(146).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_MDL_KRJ_NDNSN_RPH
        params(145) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_MDL_KRJ_MT_NG_SNG", SqlDbType.BigInt)
        params(145).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_MDL_KRJ_MT_NG_SNG
        params(144) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_MLTGN_TTL", SqlDbType.BigInt)
        params(144).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_MLTGN_TTL
        params(143) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_MLTGN_NDNSN_RPH", SqlDbType.BigInt)
        params(143).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_MLTGN_NDNSN_RPH
        params(142) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_MLTGN_MT_NG_SNG", SqlDbType.BigInt)
        params(142).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_MLTGN_MT_NG_SNG
        params(141) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL", SqlDbType.BigInt)
        params(141).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL
        params(140) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH", SqlDbType.BigInt)
        params(140).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH
        params(139) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG", SqlDbType.BigInt)
        params(139).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG
        params(138) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_TTL", SqlDbType.BigInt)
        params(138).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_TTL
        params(137) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH", SqlDbType.BigInt)
        params(137).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH
        params(136) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG", SqlDbType.BigInt)
        params(136).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG
        params(135) = New SqlParameter("@RS_KS_MSK_DR_KGTN_BRBSS_F_TTL", SqlDbType.BigInt)
        params(135).Value = oCustomClass.RS_KS_MSK_DR_KGTN_BRBSS_F_TTL
        params(134) = New SqlParameter("@RS_KS_MSK_DR_KGTN_BRBSS_F_NDNSN_RPH", SqlDbType.BigInt)
        params(134).Value = oCustomClass.RS_KS_MSK_DR_KGTN_BRBSS_F_NDNSN_RPH
        params(133) = New SqlParameter("@RS_KS_MSK_DR_KGTN_BRBSS_F_MT_NG_SNG", SqlDbType.BigInt)
        params(133).Value = oCustomClass.RS_KS_MSK_DR_KGTN_BRBSS_F_MT_NG_SNG
        params(132) = New SqlParameter("@RS_KS_MSK_DR_KGTN_SW_PRS_TTL", SqlDbType.BigInt)
        params(132).Value = oCustomClass.RS_KS_MSK_DR_KGTN_SW_PRS_TTL
        params(131) = New SqlParameter("@RS_KS_MSK_DR_KGTN_SW_PRS_NDNSN_RPH", SqlDbType.BigInt)
        params(131).Value = oCustomClass.RS_KS_MSK_DR_KGTN_SW_PRS_NDNSN_RPH
        params(130) = New SqlParameter("@RS_KS_MSK_DR_KGTN_SW_PRS_MT_NG_SNG", SqlDbType.BigInt)
        params(130).Value = oCustomClass.RS_KS_MSK_DR_KGTN_SW_PRS_MT_NG_SNG
        params(129) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL", SqlDbType.BigInt)
        params(129).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL
        params(128) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH", SqlDbType.BigInt)
        params(128).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH
        params(127) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG", SqlDbType.BigInt)
        params(127).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG
        params(126) = New SqlParameter("@RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL", SqlDbType.BigInt)
        params(126).Value = oCustomClass.RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL
        params(125) = New SqlParameter("@RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH", SqlDbType.BigInt)
        params(125).Value = oCustomClass.RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH
        params(124) = New SqlParameter("@RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG", SqlDbType.BigInt)
        params(124).Value = oCustomClass.RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG
        params(123) = New SqlParameter("@RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_TTL", SqlDbType.BigInt)
        params(123).Value = oCustomClass.RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_TTL
        params(122) = New SqlParameter("@RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(122).Value = oCustomClass.RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_NDNSN_RPH
        params(121) = New SqlParameter("@RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(121).Value = oCustomClass.RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_MT_NG_SNG
        params(120) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PRS_TTL", SqlDbType.BigInt)
        params(120).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PRS_TTL
        params(119) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PRS_NDNSN_RPH", SqlDbType.BigInt)
        params(119).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PRS_NDNSN_RPH
        params(118) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PRS_MT_NG_SNG", SqlDbType.BigInt)
        params(118).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PRS_MT_NG_SNG
        params(117) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_NVSTS_TTL", SqlDbType.BigInt)
        params(117).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_NVSTS_TTL
        params(116) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_NVSTS_NDNSN_RPH", SqlDbType.BigInt)
        params(116).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_NVSTS_NDNSN_RPH
        params(115) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_NVSTS_MT_NG_SNG", SqlDbType.BigInt)
        params(115).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_NVSTS_MT_NG_SNG
        params(114) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_MDL_KRJ_TTL", SqlDbType.BigInt)
        params(114).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_MDL_KRJ_TTL
        params(113) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_MDL_KRJ_NDNSN_RPH", SqlDbType.BigInt)
        params(113).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_MDL_KRJ_NDNSN_RPH
        params(112) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_MDL_KRJ_MT_NG_SNG", SqlDbType.BigInt)
        params(112).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_MDL_KRJ_MT_NG_SNG
        params(111) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_MLTGN_TTL", SqlDbType.BigInt)
        params(111).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_MLTGN_TTL
        params(110) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_MLTGN_NDNSN_RPH", SqlDbType.BigInt)
        params(110).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_MLTGN_NDNSN_RPH
        params(109) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_MLTGN_MT_NG_SNG", SqlDbType.BigInt)
        params(109).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_MLTGN_MT_NG_SNG
        params(108) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL", SqlDbType.BigInt)
        params(108).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL
        params(107) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH", SqlDbType.BigInt)
        params(107).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH
        params(106) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG", SqlDbType.BigInt)
        params(106).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG
        params(105) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_TTL", SqlDbType.BigInt)
        params(105).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_TTL
        params(104) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH", SqlDbType.BigInt)
        params(104).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH
        params(103) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG", SqlDbType.BigInt)
        params(103).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG
        params(102) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_TTL", SqlDbType.BigInt)
        params(102).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_TTL
        params(101) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_NDNSN_RPH", SqlDbType.BigInt)
        params(101).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_NDNSN_RPH
        params(100) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_MT_NG_SNG", SqlDbType.BigInt)
        params(100).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_MT_NG_SNG
        params(99) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_TTL", SqlDbType.BigInt)
        params(99).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_TTL
        params(98) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_NDNSN_RPH", SqlDbType.BigInt)
        params(98).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_NDNSN_RPH
        params(97) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_MT_NG_SNG", SqlDbType.BigInt)
        params(97).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_MT_NG_SNG
        params(96) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_TTL", SqlDbType.BigInt)
        params(96).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_TTL
        params(95) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_NDNSN_RPH", SqlDbType.BigInt)
        params(95).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_NDNSN_RPH
        params(94) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_MT_NG_SNG", SqlDbType.BigInt)
        params(94).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_MT_NG_SNG
        params(93) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL", SqlDbType.BigInt)
        params(93).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL
        params(92) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH", SqlDbType.BigInt)
        params(92).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH
        params(91) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG", SqlDbType.BigInt)
        params(91).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG
        params(90) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_TTL", SqlDbType.BigInt)
        params(90).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_TTL
        params(89) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(89).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_NDNSN_RPH
        params(88) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(88).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_MT_NG_SNG
        params(87) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_PRS_TTL", SqlDbType.BigInt)
        params(87).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_PRS_TTL
        params(86) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_PRS_NDNSN_RPH", SqlDbType.BigInt)
        params(86).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_PRS_NDNSN_RPH
        params(85) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_PRS_MT_NG_SNG", SqlDbType.BigInt)
        params(85).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_PRS_MT_NG_SNG
        params(84) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_PRS_TTL", SqlDbType.BigInt)
        params(84).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_PRS_TTL
        params(83) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_PRS_NDNSN_RPH", SqlDbType.BigInt)
        params(83).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_PRS_NDNSN_RPH
        params(82) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_PRS_MT_NG_SNG", SqlDbType.BigInt)
        params(82).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_PRS_MT_NG_SNG
        params(81) = New SqlParameter("@RS_KS_MSK_DR_PLPSN_NK_PRSHN_TTL", SqlDbType.BigInt)
        params(81).Value = oCustomClass.RS_KS_MSK_DR_PLPSN_NK_PRSHN_TTL
        params(80) = New SqlParameter("@RS_KS_MSK_DR_PLPSN_NK_PRSHN_NDNSN_RPH", SqlDbType.BigInt)
        params(80).Value = oCustomClass.RS_KS_MSK_DR_PLPSN_NK_PRSHN_NDNSN_RPH
        params(79) = New SqlParameter("@RS_KS_MSK_DR_PLPSN_NK_PRSHN_MT_NG_SNG", SqlDbType.BigInt)
        params(79).Value = oCustomClass.RS_KS_MSK_DR_PLPSN_NK_PRSHN_MT_NG_SNG
        params(78) = New SqlParameter("@RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_TTL", SqlDbType.BigInt)
        params(78).Value = oCustomClass.RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_TTL
        params(77) = New SqlParameter("@RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH", SqlDbType.BigInt)
        params(77).Value = oCustomClass.RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH
        params(76) = New SqlParameter("@RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG", SqlDbType.BigInt)
        params(76).Value = oCustomClass.RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG
        params(75) = New SqlParameter("@RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL", SqlDbType.BigInt)
        params(75).Value = oCustomClass.RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL
        params(74) = New SqlParameter("@RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH", SqlDbType.BigInt)
        params(74).Value = oCustomClass.RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH
        params(73) = New SqlParameter("@RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG", SqlDbType.BigInt)
        params(73).Value = oCustomClass.RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG
        params(72) = New SqlParameter("@RS_KS_MSK_DR_DVDN_TTL", SqlDbType.BigInt)
        params(72).Value = oCustomClass.RS_KS_MSK_DR_DVDN_TTL
        params(71) = New SqlParameter("@RS_KS_MSK_DR_DVDN_NDNSN_RPH", SqlDbType.BigInt)
        params(71).Value = oCustomClass.RS_KS_MSK_DR_DVDN_NDNSN_RPH
        params(70) = New SqlParameter("@RS_KS_MSK_DR_DVDN_MT_NG_SNG", SqlDbType.BigInt)
        params(70).Value = oCustomClass.RS_KS_MSK_DR_DVDN_MT_NG_SNG
        params(69) = New SqlParameter("@RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_TTL", SqlDbType.BigInt)
        params(69).Value = oCustomClass.RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_TTL
        params(68) = New SqlParameter("@RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_NDNSN_RPH", SqlDbType.BigInt)
        params(68).Value = oCustomClass.RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_NDNSN_RPH
        params(67) = New SqlParameter("@RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_MT_NG_SNG", SqlDbType.BigInt)
        params(67).Value = oCustomClass.RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_MT_NG_SNG
        params(66) = New SqlParameter("@RS_KS_MSK_DR_KGTN_NVSTS_LNNY_TTL", SqlDbType.BigInt)
        params(66).Value = oCustomClass.RS_KS_MSK_DR_KGTN_NVSTS_LNNY_TTL
        params(65) = New SqlParameter("@RS_KS_MSK_DR_KGTN_NVSTS_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(65).Value = oCustomClass.RS_KS_MSK_DR_KGTN_NVSTS_LNNY_NDNSN_RPH
        params(64) = New SqlParameter("@RS_KS_MSK_DR_KGTN_NVSTS_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(64).Value = oCustomClass.RS_KS_MSK_DR_KGTN_NVSTS_LNNY_MT_NG_SNG
        params(63) = New SqlParameter("@RS_KS_MSK_DR_KGTN_NVSTS_TTL", SqlDbType.BigInt)
        params(63).Value = oCustomClass.RS_KS_MSK_DR_KGTN_NVSTS_TTL
        params(62) = New SqlParameter("@RS_KS_MSK_DR_KGTN_NVSTS_NDNSN_RPH", SqlDbType.BigInt)
        params(62).Value = oCustomClass.RS_KS_MSK_DR_KGTN_NVSTS_NDNSN_RPH
        params(61) = New SqlParameter("@RS_KS_MSK_DR_KGTN_NVSTS_MT_NG_SNG", SqlDbType.BigInt)
        params(61).Value = oCustomClass.RS_KS_MSK_DR_KGTN_NVSTS_MT_NG_SNG
        params(60) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_TTL", SqlDbType.BigInt)
        params(60).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_TTL
        params(59) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_NDNSN_RPH", SqlDbType.BigInt)
        params(59).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_NDNSN_RPH
        params(58) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_MT_NG_SNG", SqlDbType.BigInt)
        params(58).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_MT_NG_SNG
        params(57) = New SqlParameter("@RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_TTL", SqlDbType.BigInt)
        params(57).Value = oCustomClass.RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_TTL
        params(56) = New SqlParameter("@RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH", SqlDbType.BigInt)
        params(56).Value = oCustomClass.RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH
        params(55) = New SqlParameter("@RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG", SqlDbType.BigInt)
        params(55).Value = oCustomClass.RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG
        params(54) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL", SqlDbType.BigInt)
        params(54).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL
        params(53) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH", SqlDbType.BigInt)
        params(53).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH
        params(52) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG", SqlDbType.BigInt)
        params(52).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG
        params(51) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_TTL", SqlDbType.BigInt)
        params(51).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_TTL
        params(50) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(50).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_NDNSN_RPH
        params(49) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(49).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_MT_NG_SNG
        params(48) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_NVSTS_TTL", SqlDbType.BigInt)
        params(48).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_NVSTS_TTL
        params(47) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_NVSTS_NDNSN_RPH", SqlDbType.BigInt)
        params(47).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_NVSTS_NDNSN_RPH
        params(46) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_NVSTS_MT_NG_SNG", SqlDbType.BigInt)
        params(46).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_NVSTS_MT_NG_SNG
        params(45) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_NVSTS_TTL", SqlDbType.BigInt)
        params(45).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_NVSTS_TTL
        params(44) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_NVSTS_NDNSN_RPH", SqlDbType.BigInt)
        params(44).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_NVSTS_NDNSN_RPH
        params(43) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_NVSTS_MT_NG_SNG", SqlDbType.BigInt)
        params(43).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_NVSTS_MT_NG_SNG
        params(42) = New SqlParameter("@RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_TTL", SqlDbType.BigInt)
        params(42).Value = oCustomClass.RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_TTL
        params(41) = New SqlParameter("@RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_NDNSN_RPH", SqlDbType.BigInt)
        params(41).Value = oCustomClass.RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_NDNSN_RPH
        params(40) = New SqlParameter("@RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_MT_NG_SNG", SqlDbType.BigInt)
        params(40).Value = oCustomClass.RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_MT_NG_SNG
        params(39) = New SqlParameter("@RS_KS_MSK_DR_PNDNN_LNNY_TTL", SqlDbType.BigInt)
        params(39).Value = oCustomClass.RS_KS_MSK_DR_PNDNN_LNNY_TTL
        params(38) = New SqlParameter("@RS_KS_MSK_DR_PNDNN_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(38).Value = oCustomClass.RS_KS_MSK_DR_PNDNN_LNNY_NDNSN_RPH
        params(37) = New SqlParameter("@RS_KS_MSK_DR_PNDNN_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(37).Value = oCustomClass.RS_KS_MSK_DR_PNDNN_LNNY_MT_NG_SNG
        params(36) = New SqlParameter("@RS_KS_MSK_DR_PNRBTN_MDL_SHM_TTL", SqlDbType.BigInt)
        params(36).Value = oCustomClass.RS_KS_MSK_DR_PNRBTN_MDL_SHM_TTL
        params(35) = New SqlParameter("@RS_KS_MSK_DR_PNRBTN_MDL_SHM_NDNSN_RPH", SqlDbType.BigInt)
        params(35).Value = oCustomClass.RS_KS_MSK_DR_PNRBTN_MDL_SHM_NDNSN_RPH
        params(34) = New SqlParameter("@RS_KS_MSK_DR_PNRBTN_MDL_SHM_MT_NG_SNG", SqlDbType.BigInt)
        params(34).Value = oCustomClass.RS_KS_MSK_DR_PNRBTN_MDL_SHM_MT_NG_SNG
        params(33) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PNDNN_TTL", SqlDbType.BigInt)
        params(33).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PNDNN_TTL
        params(32) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PNDNN_NDNSN_RPH", SqlDbType.BigInt)
        params(32).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PNDNN_NDNSN_RPH
        params(31) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PNDNN_MT_NG_SNG", SqlDbType.BigInt)
        params(31).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PNDNN_MT_NG_SNG
        params(30) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_TTL", SqlDbType.BigInt)
        params(30).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_TTL
        params(29) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH", SqlDbType.BigInt)
        params(29).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH
        params(28) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG", SqlDbType.BigInt)
        params(28).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG
        params(27) = New SqlParameter("@RS_KS_KLR_NTK_PNDNN_LNNY_TTL", SqlDbType.BigInt)
        params(27).Value = oCustomClass.RS_KS_KLR_NTK_PNDNN_LNNY_TTL
        params(26) = New SqlParameter("@RS_KS_KLR_NTK_PNDNN_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(26).Value = oCustomClass.RS_KS_KLR_NTK_PNDNN_LNNY_NDNSN_RPH
        params(25) = New SqlParameter("@RS_KS_KLR_NTK_PNDNN_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(25).Value = oCustomClass.RS_KS_KLR_NTK_PNDNN_LNNY_MT_NG_SNG
        params(24) = New SqlParameter("@RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_TTL", SqlDbType.BigInt)
        params(24).Value = oCustomClass.RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_TTL
        params(23) = New SqlParameter("@RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_NDNSN_RPH", SqlDbType.BigInt)
        params(23).Value = oCustomClass.RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_NDNSN_RPH
        params(22) = New SqlParameter("@RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_MT_NG_SNG", SqlDbType.BigInt)
        params(22).Value = oCustomClass.RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_MT_NG_SNG
        params(21) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_DVDN_TTL", SqlDbType.BigInt)
        params(21).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_DVDN_TTL
        params(20) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_DVDN_NDNSN_RPH", SqlDbType.BigInt)
        params(20).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_DVDN_NDNSN_RPH
        params(19) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_DVDN_MT_NG_SNG", SqlDbType.BigInt)
        params(19).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_DVDN_MT_NG_SNG
        params(18) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_PNDNN_TTL", SqlDbType.BigInt)
        params(18).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_PNDNN_TTL
        params(17) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_PNDNN_NDNSN_RPH", SqlDbType.BigInt)
        params(17).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_PNDNN_NDNSN_RPH
        params(16) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_PNDNN_MT_NG_SNG", SqlDbType.BigInt)
        params(16).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_PNDNN_MT_NG_SNG
        params(15) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_PNDNN_TTL", SqlDbType.BigInt)
        params(15).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_PNDNN_TTL
        params(14) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_PNDNN_NDNSN_RPH", SqlDbType.BigInt)
        params(14).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_PNDNN_NDNSN_RPH
        params(13) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_PNDNN_MT_NG_SNG", SqlDbType.BigInt)
        params(13).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_PNDNN_MT_NG_SNG
        params(12) = New SqlParameter("@SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_TTL", SqlDbType.BigInt)
        params(12).Value = oCustomClass.SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_TTL
        params(11) = New SqlParameter("@SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_NDNSN_RPH", SqlDbType.BigInt)
        params(11).Value = oCustomClass.SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_NDNSN_RPH
        params(10) = New SqlParameter("@SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_MT_NG_SNG", SqlDbType.BigInt)
        params(10).Value = oCustomClass.SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_MT_NG_SNG
        params(9) = New SqlParameter("@KNKN_PNRNN_BRSH_KS_DN_STR_KS_TTL", SqlDbType.BigInt)
        params(9).Value = oCustomClass.KNKN_PNRNN_BRSH_KS_DN_STR_KS_TTL
        params(8) = New SqlParameter("@KNKN_PNRNN_BRSH_KS_DN_STR_KS_NDNSN_RPH", SqlDbType.BigInt)
        params(8).Value = oCustomClass.KNKN_PNRNN_BRSH_KS_DN_STR_KS_NDNSN_RPH
        params(7) = New SqlParameter("@KNKN_PNRNN_BRSH_KS_DN_STR_KS_MT_NG_SNG", SqlDbType.BigInt)
        params(7).Value = oCustomClass.KNKN_PNRNN_BRSH_KS_DN_STR_KS_MT_NG_SNG
        params(6) = New SqlParameter("@KS_DN_STR_KS_PD_WL_PRD_TTL", SqlDbType.BigInt)
        params(6).Value = oCustomClass.KS_DN_STR_KS_PD_WL_PRD_TTL
        params(5) = New SqlParameter("@KS_DN_STR_KS_PD_WL_PRD_NDNSN_RPH", SqlDbType.BigInt)
        params(5).Value = oCustomClass.KS_DN_STR_KS_PD_WL_PRD_NDNSN_RPH
        params(4) = New SqlParameter("@KS_DN_STR_KS_PD_WL_PRD_MT_NG_SNG", SqlDbType.BigInt)
        params(4).Value = oCustomClass.KS_DN_STR_KS_PD_WL_PRD_MT_NG_SNG
        params(3) = New SqlParameter("@KS_DN_STR_KS_TTL", SqlDbType.BigInt)
        params(3).Value = oCustomClass.KS_DN_STR_KS_TTL
        params(2) = New SqlParameter("@KS_DN_STR_KS_NDNSN_RPH", SqlDbType.BigInt)
        params(2).Value = oCustomClass.KS_DN_STR_KS_NDNSN_RPH
        params(1) = New SqlParameter("@KS_DN_STR_KS_MT_NG_SNG", SqlDbType.BigInt)
        params(1).Value = oCustomClass.KS_DN_STR_KS_MT_NG_SNG
        params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP1300.SIPP1300Save")
        End Try
    End Function

    Public Function SIPP1300Add(ByVal oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300
        Dim params(150) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP1300

        params(0) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_NVSTS_TTL", SqlDbType.BigInt)
        params(0).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_NVSTS_TTL
        params(1) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_NVSTS_NDNSN_RPH", SqlDbType.BigInt)
        params(1).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_NVSTS_NDNSN_RPH
        params(2) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_NVSTS_MT_NG_SNG", SqlDbType.BigInt)
        params(2).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_NVSTS_MT_NG_SNG
        params(3) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_MDL_KRJ_TTL", SqlDbType.BigInt)
        params(3).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_MDL_KRJ_TTL
        params(4) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_MDL_KRJ_NDNSN_RPH", SqlDbType.BigInt)
        params(4).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_MDL_KRJ_NDNSN_RPH
        params(5) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_MDL_KRJ_MT_NG_SNG", SqlDbType.BigInt)
        params(5).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_MDL_KRJ_MT_NG_SNG
        params(6) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_MLTGN_TTL", SqlDbType.BigInt)
        params(6).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_MLTGN_TTL
        params(7) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_MLTGN_NDNSN_RPH", SqlDbType.BigInt)
        params(7).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_MLTGN_NDNSN_RPH
        params(8) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_MLTGN_MT_NG_SNG", SqlDbType.BigInt)
        params(8).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_MLTGN_MT_NG_SNG
        params(9) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL", SqlDbType.BigInt)
        params(9).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL
        params(10) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH", SqlDbType.BigInt)
        params(10).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH
        params(11) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG", SqlDbType.BigInt)
        params(11).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG
        params(12) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_TTL", SqlDbType.BigInt)
        params(12).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_TTL
        params(13) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH", SqlDbType.BigInt)
        params(13).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH
        params(14) = New SqlParameter("@RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG", SqlDbType.BigInt)
        params(14).Value = oCustomClass.RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG
        params(15) = New SqlParameter("@RS_KS_MSK_DR_KGTN_BRBSS_F_TTL", SqlDbType.BigInt)
        params(15).Value = oCustomClass.RS_KS_MSK_DR_KGTN_BRBSS_F_TTL
        params(16) = New SqlParameter("@RS_KS_MSK_DR_KGTN_BRBSS_F_NDNSN_RPH", SqlDbType.BigInt)
        params(16).Value = oCustomClass.RS_KS_MSK_DR_KGTN_BRBSS_F_NDNSN_RPH
        params(17) = New SqlParameter("@RS_KS_MSK_DR_KGTN_BRBSS_F_MT_NG_SNG", SqlDbType.BigInt)
        params(17).Value = oCustomClass.RS_KS_MSK_DR_KGTN_BRBSS_F_MT_NG_SNG
        params(18) = New SqlParameter("@RS_KS_MSK_DR_KGTN_SW_PRS_TTL", SqlDbType.BigInt)
        params(18).Value = oCustomClass.RS_KS_MSK_DR_KGTN_SW_PRS_TTL
        params(19) = New SqlParameter("@RS_KS_MSK_DR_KGTN_SW_PRS_NDNSN_RPH", SqlDbType.BigInt)
        params(19).Value = oCustomClass.RS_KS_MSK_DR_KGTN_SW_PRS_NDNSN_RPH
        params(20) = New SqlParameter("@RS_KS_MSK_DR_KGTN_SW_PRS_MT_NG_SNG", SqlDbType.BigInt)
        params(20).Value = oCustomClass.RS_KS_MSK_DR_KGTN_SW_PRS_MT_NG_SNG
        params(21) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL", SqlDbType.BigInt)
        params(21).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL
        params(22) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH", SqlDbType.BigInt)
        params(22).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH
        params(23) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG", SqlDbType.BigInt)
        params(23).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG
        params(24) = New SqlParameter("@RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL", SqlDbType.BigInt)
        params(24).Value = oCustomClass.RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL
        params(25) = New SqlParameter("@RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH", SqlDbType.BigInt)
        params(25).Value = oCustomClass.RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH
        params(26) = New SqlParameter("@RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG", SqlDbType.BigInt)
        params(26).Value = oCustomClass.RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG
        params(27) = New SqlParameter("@RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_TTL", SqlDbType.BigInt)
        params(27).Value = oCustomClass.RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_TTL
        params(28) = New SqlParameter("@RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(28).Value = oCustomClass.RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_NDNSN_RPH
        params(29) = New SqlParameter("@RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(29).Value = oCustomClass.RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_MT_NG_SNG
        params(30) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PRS_TTL", SqlDbType.BigInt)
        params(30).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PRS_TTL
        params(31) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PRS_NDNSN_RPH", SqlDbType.BigInt)
        params(31).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PRS_NDNSN_RPH
        params(32) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PRS_MT_NG_SNG", SqlDbType.BigInt)
        params(32).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PRS_MT_NG_SNG
        params(33) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_NVSTS_TTL", SqlDbType.BigInt)
        params(33).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_NVSTS_TTL
        params(34) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_NVSTS_NDNSN_RPH", SqlDbType.BigInt)
        params(34).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_NVSTS_NDNSN_RPH
        params(35) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_NVSTS_MT_NG_SNG", SqlDbType.BigInt)
        params(35).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_NVSTS_MT_NG_SNG
        params(36) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_MDL_KRJ_TTL", SqlDbType.BigInt)
        params(36).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_MDL_KRJ_TTL
        params(37) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_MDL_KRJ_NDNSN_RPH", SqlDbType.BigInt)
        params(37).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_MDL_KRJ_NDNSN_RPH
        params(38) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_MDL_KRJ_MT_NG_SNG", SqlDbType.BigInt)
        params(38).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_MDL_KRJ_MT_NG_SNG
        params(39) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_MLTGN_TTL", SqlDbType.BigInt)
        params(39).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_MLTGN_TTL
        params(40) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_MLTGN_NDNSN_RPH", SqlDbType.BigInt)
        params(40).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_MLTGN_NDNSN_RPH
        params(41) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_MLTGN_MT_NG_SNG", SqlDbType.BigInt)
        params(41).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_MLTGN_MT_NG_SNG
        params(42) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL", SqlDbType.BigInt)
        params(42).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL
        params(43) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH", SqlDbType.BigInt)
        params(43).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH
        params(44) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG", SqlDbType.BigInt)
        params(44).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG
        params(45) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_TTL", SqlDbType.BigInt)
        params(45).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_TTL
        params(46) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH", SqlDbType.BigInt)
        params(46).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH
        params(47) = New SqlParameter("@RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG", SqlDbType.BigInt)
        params(47).Value = oCustomClass.RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG
        params(48) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_TTL", SqlDbType.BigInt)
        params(48).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_TTL
        params(49) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_NDNSN_RPH", SqlDbType.BigInt)
        params(49).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_NDNSN_RPH
        params(50) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_MT_NG_SNG", SqlDbType.BigInt)
        params(50).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_MT_NG_SNG
        params(51) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_TTL", SqlDbType.BigInt)
        params(51).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_TTL
        params(52) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_NDNSN_RPH", SqlDbType.BigInt)
        params(52).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_NDNSN_RPH
        params(53) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_MT_NG_SNG", SqlDbType.BigInt)
        params(53).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_MT_NG_SNG
        params(54) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_TTL", SqlDbType.BigInt)
        params(54).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_TTL
        params(55) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_NDNSN_RPH", SqlDbType.BigInt)
        params(55).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_NDNSN_RPH
        params(56) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_MT_NG_SNG", SqlDbType.BigInt)
        params(56).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_MT_NG_SNG
        params(57) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL", SqlDbType.BigInt)
        params(57).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL
        params(58) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH", SqlDbType.BigInt)
        params(58).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH
        params(59) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG", SqlDbType.BigInt)
        params(59).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG
        params(60) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_TTL", SqlDbType.BigInt)
        params(60).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_TTL
        params(61) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(61).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_NDNSN_RPH
        params(62) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(62).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_MT_NG_SNG
        params(63) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_PRS_TTL", SqlDbType.BigInt)
        params(63).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_PRS_TTL
        params(64) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_PRS_NDNSN_RPH", SqlDbType.BigInt)
        params(64).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_PRS_NDNSN_RPH
        params(65) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_PRS_MT_NG_SNG", SqlDbType.BigInt)
        params(65).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_PRS_MT_NG_SNG
        params(66) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_PRS_TTL", SqlDbType.BigInt)
        params(66).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_PRS_TTL
        params(67) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_PRS_NDNSN_RPH", SqlDbType.BigInt)
        params(67).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_PRS_NDNSN_RPH
        params(68) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_PRS_MT_NG_SNG", SqlDbType.BigInt)
        params(68).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_PRS_MT_NG_SNG
        params(69) = New SqlParameter("@RS_KS_MSK_DR_PLPSN_NK_PRSHN_TTL", SqlDbType.BigInt)
        params(69).Value = oCustomClass.RS_KS_MSK_DR_PLPSN_NK_PRSHN_TTL
        params(70) = New SqlParameter("@RS_KS_MSK_DR_PLPSN_NK_PRSHN_NDNSN_RPH", SqlDbType.BigInt)
        params(70).Value = oCustomClass.RS_KS_MSK_DR_PLPSN_NK_PRSHN_NDNSN_RPH
        params(71) = New SqlParameter("@RS_KS_MSK_DR_PLPSN_NK_PRSHN_MT_NG_SNG", SqlDbType.BigInt)
        params(71).Value = oCustomClass.RS_KS_MSK_DR_PLPSN_NK_PRSHN_MT_NG_SNG
        params(72) = New SqlParameter("@RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_TTL", SqlDbType.BigInt)
        params(72).Value = oCustomClass.RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_TTL
        params(73) = New SqlParameter("@RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH", SqlDbType.BigInt)
        params(73).Value = oCustomClass.RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH
        params(74) = New SqlParameter("@RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG", SqlDbType.BigInt)
        params(74).Value = oCustomClass.RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG
        params(75) = New SqlParameter("@RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL", SqlDbType.BigInt)
        params(75).Value = oCustomClass.RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL
        params(76) = New SqlParameter("@RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH", SqlDbType.BigInt)
        params(76).Value = oCustomClass.RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH
        params(77) = New SqlParameter("@RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG", SqlDbType.BigInt)
        params(77).Value = oCustomClass.RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG
        params(78) = New SqlParameter("@RS_KS_MSK_DR_DVDN_TTL", SqlDbType.BigInt)
        params(78).Value = oCustomClass.RS_KS_MSK_DR_DVDN_TTL
        params(79) = New SqlParameter("@RS_KS_MSK_DR_DVDN_NDNSN_RPH", SqlDbType.BigInt)
        params(79).Value = oCustomClass.RS_KS_MSK_DR_DVDN_NDNSN_RPH
        params(80) = New SqlParameter("@RS_KS_MSK_DR_DVDN_MT_NG_SNG", SqlDbType.BigInt)
        params(80).Value = oCustomClass.RS_KS_MSK_DR_DVDN_MT_NG_SNG
        params(81) = New SqlParameter("@RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_TTL", SqlDbType.BigInt)
        params(81).Value = oCustomClass.RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_TTL
        params(82) = New SqlParameter("@RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_NDNSN_RPH", SqlDbType.BigInt)
        params(82).Value = oCustomClass.RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_NDNSN_RPH
        params(83) = New SqlParameter("@RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_MT_NG_SNG", SqlDbType.BigInt)
        params(83).Value = oCustomClass.RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_MT_NG_SNG
        params(84) = New SqlParameter("@RS_KS_MSK_DR_KGTN_NVSTS_LNNY_TTL", SqlDbType.BigInt)
        params(84).Value = oCustomClass.RS_KS_MSK_DR_KGTN_NVSTS_LNNY_TTL
        params(85) = New SqlParameter("@RS_KS_MSK_DR_KGTN_NVSTS_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(85).Value = oCustomClass.RS_KS_MSK_DR_KGTN_NVSTS_LNNY_NDNSN_RPH
        params(86) = New SqlParameter("@RS_KS_MSK_DR_KGTN_NVSTS_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(86).Value = oCustomClass.RS_KS_MSK_DR_KGTN_NVSTS_LNNY_MT_NG_SNG
        params(87) = New SqlParameter("@RS_KS_MSK_DR_KGTN_NVSTS_TTL", SqlDbType.BigInt)
        params(87).Value = oCustomClass.RS_KS_MSK_DR_KGTN_NVSTS_TTL
        params(88) = New SqlParameter("@RS_KS_MSK_DR_KGTN_NVSTS_NDNSN_RPH", SqlDbType.BigInt)
        params(88).Value = oCustomClass.RS_KS_MSK_DR_KGTN_NVSTS_NDNSN_RPH
        params(89) = New SqlParameter("@RS_KS_MSK_DR_KGTN_NVSTS_MT_NG_SNG", SqlDbType.BigInt)
        params(89).Value = oCustomClass.RS_KS_MSK_DR_KGTN_NVSTS_MT_NG_SNG
        params(90) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_TTL", SqlDbType.BigInt)
        params(90).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_TTL
        params(91) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_NDNSN_RPH", SqlDbType.BigInt)
        params(91).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_NDNSN_RPH
        params(92) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_MT_NG_SNG", SqlDbType.BigInt)
        params(92).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_MT_NG_SNG
        params(93) = New SqlParameter("@RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_TTL", SqlDbType.BigInt)
        params(93).Value = oCustomClass.RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_TTL
        params(94) = New SqlParameter("@RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH", SqlDbType.BigInt)
        params(94).Value = oCustomClass.RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH
        params(95) = New SqlParameter("@RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG", SqlDbType.BigInt)
        params(95).Value = oCustomClass.RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG
        params(96) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL", SqlDbType.BigInt)
        params(96).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL
        params(97) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH", SqlDbType.BigInt)
        params(97).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH
        params(98) = New SqlParameter("@RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG", SqlDbType.BigInt)
        params(98).Value = oCustomClass.RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG
        params(99) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_TTL", SqlDbType.BigInt)
        params(99).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_TTL
        params(100) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(100).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_NDNSN_RPH
        params(101) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(101).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_MT_NG_SNG
        params(102) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_NVSTS_TTL", SqlDbType.BigInt)
        params(102).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_NVSTS_TTL
        params(103) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_NVSTS_NDNSN_RPH", SqlDbType.BigInt)
        params(103).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_NVSTS_NDNSN_RPH
        params(104) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_NVSTS_MT_NG_SNG", SqlDbType.BigInt)
        params(104).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_NVSTS_MT_NG_SNG
        params(105) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_NVSTS_TTL", SqlDbType.BigInt)
        params(105).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_NVSTS_TTL
        params(106) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_NVSTS_NDNSN_RPH", SqlDbType.BigInt)
        params(106).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_NVSTS_NDNSN_RPH
        params(107) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_NVSTS_MT_NG_SNG", SqlDbType.BigInt)
        params(107).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_NVSTS_MT_NG_SNG
        params(108) = New SqlParameter("@RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_TTL", SqlDbType.BigInt)
        params(108).Value = oCustomClass.RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_TTL
        params(109) = New SqlParameter("@RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_NDNSN_RPH", SqlDbType.BigInt)
        params(109).Value = oCustomClass.RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_NDNSN_RPH
        params(110) = New SqlParameter("@RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_MT_NG_SNG", SqlDbType.BigInt)
        params(110).Value = oCustomClass.RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_MT_NG_SNG
        params(111) = New SqlParameter("@RS_KS_MSK_DR_PNDNN_LNNY_TTL", SqlDbType.BigInt)
        params(111).Value = oCustomClass.RS_KS_MSK_DR_PNDNN_LNNY_TTL
        params(112) = New SqlParameter("@RS_KS_MSK_DR_PNDNN_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(112).Value = oCustomClass.RS_KS_MSK_DR_PNDNN_LNNY_NDNSN_RPH
        params(113) = New SqlParameter("@RS_KS_MSK_DR_PNDNN_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(113).Value = oCustomClass.RS_KS_MSK_DR_PNDNN_LNNY_MT_NG_SNG
        params(114) = New SqlParameter("@RS_KS_MSK_DR_PNRBTN_MDL_SHM_TTL", SqlDbType.BigInt)
        params(114).Value = oCustomClass.RS_KS_MSK_DR_PNRBTN_MDL_SHM_TTL
        params(115) = New SqlParameter("@RS_KS_MSK_DR_PNRBTN_MDL_SHM_NDNSN_RPH", SqlDbType.BigInt)
        params(115).Value = oCustomClass.RS_KS_MSK_DR_PNRBTN_MDL_SHM_NDNSN_RPH
        params(116) = New SqlParameter("@RS_KS_MSK_DR_PNRBTN_MDL_SHM_MT_NG_SNG", SqlDbType.BigInt)
        params(116).Value = oCustomClass.RS_KS_MSK_DR_PNRBTN_MDL_SHM_MT_NG_SNG
        params(117) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PNDNN_TTL", SqlDbType.BigInt)
        params(117).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PNDNN_TTL
        params(118) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PNDNN_NDNSN_RPH", SqlDbType.BigInt)
        params(118).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PNDNN_NDNSN_RPH
        params(119) = New SqlParameter("@RS_KS_MSK_DR_KGTN_PNDNN_MT_NG_SNG", SqlDbType.BigInt)
        params(119).Value = oCustomClass.RS_KS_MSK_DR_KGTN_PNDNN_MT_NG_SNG
        params(120) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_TTL", SqlDbType.BigInt)
        params(120).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_TTL
        params(121) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH", SqlDbType.BigInt)
        params(121).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH
        params(122) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG", SqlDbType.BigInt)
        params(122).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG
        params(123) = New SqlParameter("@RS_KS_KLR_NTK_PNDNN_LNNY_TTL", SqlDbType.BigInt)
        params(123).Value = oCustomClass.RS_KS_KLR_NTK_PNDNN_LNNY_TTL
        params(124) = New SqlParameter("@RS_KS_KLR_NTK_PNDNN_LNNY_NDNSN_RPH", SqlDbType.BigInt)
        params(124).Value = oCustomClass.RS_KS_KLR_NTK_PNDNN_LNNY_NDNSN_RPH
        params(125) = New SqlParameter("@RS_KS_KLR_NTK_PNDNN_LNNY_MT_NG_SNG", SqlDbType.BigInt)
        params(125).Value = oCustomClass.RS_KS_KLR_NTK_PNDNN_LNNY_MT_NG_SNG
        params(126) = New SqlParameter("@RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_TTL", SqlDbType.BigInt)
        params(126).Value = oCustomClass.RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_TTL
        params(127) = New SqlParameter("@RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_NDNSN_RPH", SqlDbType.BigInt)
        params(127).Value = oCustomClass.RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_NDNSN_RPH
        params(128) = New SqlParameter("@RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_MT_NG_SNG", SqlDbType.BigInt)
        params(128).Value = oCustomClass.RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_MT_NG_SNG
        params(129) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_DVDN_TTL", SqlDbType.BigInt)
        params(129).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_DVDN_TTL
        params(130) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_DVDN_NDNSN_RPH", SqlDbType.BigInt)
        params(130).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_DVDN_NDNSN_RPH
        params(131) = New SqlParameter("@RS_KS_KLR_NTK_PMBYRN_DVDN_MT_NG_SNG", SqlDbType.BigInt)
        params(131).Value = oCustomClass.RS_KS_KLR_NTK_PMBYRN_DVDN_MT_NG_SNG
        params(132) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_PNDNN_TTL", SqlDbType.BigInt)
        params(132).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_PNDNN_TTL
        params(133) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_PNDNN_NDNSN_RPH", SqlDbType.BigInt)
        params(133).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_PNDNN_NDNSN_RPH
        params(134) = New SqlParameter("@RS_KS_KLR_NTK_KGTN_PNDNN_MT_NG_SNG", SqlDbType.BigInt)
        params(134).Value = oCustomClass.RS_KS_KLR_NTK_KGTN_PNDNN_MT_NG_SNG
        params(135) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_PNDNN_TTL", SqlDbType.BigInt)
        params(135).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_PNDNN_TTL
        params(136) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_PNDNN_NDNSN_RPH", SqlDbType.BigInt)
        params(136).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_PNDNN_NDNSN_RPH
        params(137) = New SqlParameter("@RS_KS_BRSH_DR_KGTN_PNDNN_MT_NG_SNG", SqlDbType.BigInt)
        params(137).Value = oCustomClass.RS_KS_BRSH_DR_KGTN_PNDNN_MT_NG_SNG
        params(138) = New SqlParameter("@SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_TTL", SqlDbType.BigInt)
        params(138).Value = oCustomClass.SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_TTL
        params(139) = New SqlParameter("@SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_NDNSN_RPH", SqlDbType.BigInt)
        params(139).Value = oCustomClass.SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_NDNSN_RPH
        params(140) = New SqlParameter("@SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_MT_NG_SNG", SqlDbType.BigInt)
        params(140).Value = oCustomClass.SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_MT_NG_SNG
        params(141) = New SqlParameter("@KNKN_PNRNN_BRSH_KS_DN_STR_KS_TTL", SqlDbType.BigInt)
        params(141).Value = oCustomClass.KNKN_PNRNN_BRSH_KS_DN_STR_KS_TTL
        params(142) = New SqlParameter("@KNKN_PNRNN_BRSH_KS_DN_STR_KS_NDNSN_RPH", SqlDbType.BigInt)
        params(142).Value = oCustomClass.KNKN_PNRNN_BRSH_KS_DN_STR_KS_NDNSN_RPH
        params(143) = New SqlParameter("@KNKN_PNRNN_BRSH_KS_DN_STR_KS_MT_NG_SNG", SqlDbType.BigInt)
        params(143).Value = oCustomClass.KNKN_PNRNN_BRSH_KS_DN_STR_KS_MT_NG_SNG
        params(144) = New SqlParameter("@KS_DN_STR_KS_PD_WL_PRD_TTL", SqlDbType.BigInt)
        params(144).Value = oCustomClass.KS_DN_STR_KS_PD_WL_PRD_TTL
        params(145) = New SqlParameter("@KS_DN_STR_KS_PD_WL_PRD_NDNSN_RPH", SqlDbType.BigInt)
        params(145).Value = oCustomClass.KS_DN_STR_KS_PD_WL_PRD_NDNSN_RPH
        params(146) = New SqlParameter("@KS_DN_STR_KS_PD_WL_PRD_MT_NG_SNG", SqlDbType.BigInt)
        params(146).Value = oCustomClass.KS_DN_STR_KS_PD_WL_PRD_MT_NG_SNG
        params(147) = New SqlParameter("@KS_DN_STR_KS_TTL", SqlDbType.BigInt)
        params(147).Value = oCustomClass.KS_DN_STR_KS_TTL
        params(148) = New SqlParameter("@KS_DN_STR_KS_NDNSN_RPH", SqlDbType.BigInt)
        params(148).Value = oCustomClass.KS_DN_STR_KS_NDNSN_RPH
        params(149) = New SqlParameter("@KS_DN_STR_KS_MT_NG_SNG", SqlDbType.BigInt)
        params(149).Value = oCustomClass.KS_DN_STR_KS_MT_NG_SNG
        params(150) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 8)
		params(150).Value = oCustomClass.Bulandata
		Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, ADD, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP1300.SIPP1300Add")
        End Try

    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300
        Dim oReturnValue As New Parameter.SIPP1300
        Dim params(2) As SqlParameter

        params(0) = New SqlParameter("@valueelement", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Valueelement
        params(1) = New SqlParameter("@valueparent", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.Valueparent
        params(2) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 50)
        params(2).Value = oCustomClass.Valuemaster
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2490.GetCbo")
        End Try
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300
        Dim oReturnValue As New Parameter.SIPP1300
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP1300.GetCboBulandataSIPP")
        End Try
    End Function

    Public Function SIPP1300Delete(ByVal oCustomClass As Parameter.SIPP1300) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

End Class

