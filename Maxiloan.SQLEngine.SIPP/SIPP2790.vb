﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class SIPP2790 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP2790List"
    Private Const SAVE As String = "spSIPP2790Save"
    Private Const LIST_EDIT As String = "spSIPP2790EditList"
    Private Const ADD As String = "spSIPP2790Add"
    Private Const FILL_CBO As String = "spGetCboSIPP"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"
    Private Const DELETE As String = "spSIPP2790Delete"
#End Region

    Public Function GetSIPP2790(ByVal oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790
        Dim oReturnValue As New Parameter.SIPP2790
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2790.GetSIPP2790")
        End Try
    End Function

    Public Function GetSelectSIPP2790(ByVal oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@ID", SqlDbType.Int)
            params(0).Value = oCustomClass.ID
            params(1) = New SqlParameter("@error", SqlDbType.VarChar, 1000)
            params(1).Value = ""

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function SIPP2790Edit(ByVal oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP2790

        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2790.SIPP2790Edit")
        End Try
    End Function

    Public Function SIPP2790Save(ByVal oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790
        Dim params(4) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP2790

        params(4) = New SqlParameter("ID", SqlDbType.VarChar, 100)
        params(4).Value = oCustomClass.ID
        params(3) = New SqlParameter("Jenis", SqlDbType.VarChar, 100)
        params(3).Value = oCustomClass.JNS_RPRP_LBLTS_PRSHN_PMBYN
        params(2) = New SqlParameter("JenisValuta", SqlDbType.VarChar, 100)
        params(2).Value = oCustomClass.JNS_MT_NG
        params(1) = New SqlParameter("Nominal", SqlDbType.VarChar, 100)
        params(1).Value = oCustomClass.RPRP_LBLTS

        params(0) = New SqlParameter("Err", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SAVE, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2790.SIPP2790Save")
        End Try
    End Function

    Public Function SIPP2790Add(ByVal oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790
        Dim params(2) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP2790

        params(0) = New SqlParameter("@Jenis", SqlDbType.VarChar, 100)
        params(0).Value = oCustomClass.JNS_RPRP_LBLTS_PRSHN_PMBYN
        params(1) = New SqlParameter("@JenisValuta", SqlDbType.VarChar, 100)
        params(1).Value = oCustomClass.JNS_MT_NG
        params(2) = New SqlParameter("@Nominal", SqlDbType.Decimal)
        params(2).Value = oCustomClass.RPRP_LBLTS

        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, ADD, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2790.SIPP2790Add")
        End Try

    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790
        Dim oReturnValue As New Parameter.SIPP2790
        Dim params(2) As SqlParameter


        params(0) = New SqlParameter("@valueelement", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Valueelement
        params(1) = New SqlParameter("@valueparent", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.Valueparent
        params(2) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 50)
        params(2).Value = oCustomClass.Valuemaster
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2790.GetCbo")
        End Try
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790
        Dim oReturnValue As New Parameter.SIPP2790
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP2790.GetCboBulandataSIPP")
        End Try
    End Function

    Public Function SIPP2790Delete(ByVal oCustomClass As Parameter.SIPP2790) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function
End Class
