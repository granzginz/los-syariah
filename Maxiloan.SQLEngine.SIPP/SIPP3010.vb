﻿

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class SIPP3010 : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spSIPP3010List"
    Private Const SaveAdd As String = "spSIPP3010SaveAdd"
    Private Const SaveEdit As String = "spSIPP3010SaveEdit"
    Private Const LIST_EDIT As String = "spSIPP3010EditList"
    Private Const spFillCbo As String = "spGetCboSIPP"
    Private Const Delete As String = "spSIPP3010Delete"
    Private Const FILL_CBO_BULANDATA As String = "spGetCboBulandataSIPP"


#End Region

    Public Function GetSIPP3010(ByVal oCustomClass As Parameter.SIPP3010) As Parameter.SIPP3010
        Dim oReturnValue As New Parameter.SIPP3010
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP0010.GetSIPP0010")
        End Try
    End Function

    Public Function SIPP3010Delete(ByVal oCustomClass As Parameter.SIPP3010) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.Id
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, Delete, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP3010) As Parameter.SIPP3010
        Dim oReturnValue As New Parameter.SIPP3010
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, FILL_CBO_BULANDATA, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP3010.GetCboBulandataSIPP")
        End Try
    End Function
    Public Function SIPP3010SaveEdit(ocustomClass As Parameter.SIPP3010) As Parameter.SIPP3010
        Dim params(12) As SqlParameter

        Try
            params(0) = New SqlParameter("@NMR_KNTRK", SqlDbType.VarChar, 25)
            params(0).Value = ocustomClass.NMR_KNTRK
            params(1) = New SqlParameter("@NMNL", SqlDbType.Int)
            params(1).Value = ocustomClass.NMNL
            params(2) = New SqlParameter("@NMR_KNTRK_LNDNG_NL", SqlDbType.VarChar, 25)
            params(2).Value = ocustomClass.NMR_KNTRK_LNDNG_NL
            params(3) = New SqlParameter("@JNS", SqlDbType.VarChar, 100)
            params(3).Value = ocustomClass.JNS
            params(4) = New SqlParameter("@TNGGL_ML", SqlDbType.Date)
            params(4).Value = ocustomClass.TNGGL_ML
            params(5) = New SqlParameter("@TNGGL_TMP", SqlDbType.Date)
            params(5).Value = ocustomClass.TNGGL_TMP
            params(6) = New SqlParameter("@NL_MT_NG_SL", SqlDbType.Int)
            params(6).Value = ocustomClass.NL_MT_NG_SL
            params(7) = New SqlParameter("@KVLN_RPH", SqlDbType.Int)
            params(7).Value = ocustomClass.KVLN_RPH
            params(8) = New SqlParameter("@NM", SqlDbType.VarChar, 50)
            params(8).Value = ocustomClass.NM
            params(9) = New SqlParameter("@GLNGN", SqlDbType.VarChar, 100)
            params(9).Value = ocustomClass.GLNGN
            params(10) = New SqlParameter("@SL_NGR", SqlDbType.VarChar, 100)
            params(10).Value = ocustomClass.SL_NGR
            params(11) = New SqlParameter("@ID", SqlDbType.Int)
            params(11).Value = ocustomClass.ID
            params(12) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(12).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SaveEdit, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP3010.SIPP3010SaveEdit")
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function SIPP3010SaveAdd(ocustomClass As Parameter.SIPP3010) As Parameter.SIPP3010
        Dim params(11) As SqlParameter

        Try

            params(0) = New SqlParameter("@NMR_KNTRK", SqlDbType.VarChar, 25)
            params(0).Value = ocustomClass.NMR_KNTRK
            params(1) = New SqlParameter("@NMNL", SqlDbType.BigInt)
            params(1).Value = ocustomClass.NMNL
            params(2) = New SqlParameter("@NMR_KNTRK_LNDNG_NL", SqlDbType.VarChar, 25)
            params(2).Value = ocustomClass.NMR_KNTRK_LNDNG_NL
            params(3) = New SqlParameter("@JNS", SqlDbType.VarChar, 100)
            params(3).Value = ocustomClass.JNS
            params(4) = New SqlParameter("@TNGGL_ML", SqlDbType.Date)
            params(4).Value = ocustomClass.TNGGL_ML
            params(5) = New SqlParameter("@TNGGL_TMP", SqlDbType.Date)
            params(5).Value = ocustomClass.TNGGL_TMP
            params(6) = New SqlParameter("@NL_MT_NG_SL", SqlDbType.BigInt)
            params(6).Value = ocustomClass.NL_MT_NG_SL
            params(7) = New SqlParameter("@KVLN_RPH", SqlDbType.BigInt)
            params(7).Value = ocustomClass.KVLN_RPH
            params(8) = New SqlParameter("@NM", SqlDbType.VarChar, 50)
            params(8).Value = ocustomClass.NM
            params(9) = New SqlParameter("@GLNGN", SqlDbType.VarChar, 100)
            params(9).Value = ocustomClass.GLNGN
            params(10) = New SqlParameter("@SL_NGR", SqlDbType.VarChar, 100)
            params(10).Value = ocustomClass.SL_NGR
            params(11) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 100)
            params(11).Value = ocustomClass.BULANDATA

            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SaveAdd, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP3010.SIPP3010SaveAdd")
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSIPP3010Edit(ByVal ocustomClass As Parameter.SIPP3010) As Parameter.SIPP3010
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.SIPP3010


        params(0) = New SqlParameter("ID", SqlDbType.VarChar, 100)
        params(0).Value = ocustomClass.Id

        Try
            ocustomClass.ListData = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_EDIT, params).Tables(0)
            Return ocustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.SIPP.SIPP3010.GetSIPP3010Edit")
        End Try
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP3010) As Parameter.SIPP3010
        Dim oReturnValue As New Parameter.SIPP3010
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@valueparent", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.valueparent
        params(1) = New SqlParameter("@valuemaster", SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.valuemaster
        params(2) = New SqlParameter("@valueelement", SqlDbType.VarChar, 20)
        params(2).Value = oCustomClass.valueelement

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SLIK.D01.GetCbo")
        End Try
    End Function

    'Public Function Generate(ByVal oCustomClass As Parameter.D01) As Parameter.D01
    '    Dim oReturnValue As New Parameter.D01

    '    'Dim params(0) As SqlParameter
    '    'params(0) = New SqlParameter("CIF", SqlDbType.VarChar, 100)
    '    'params(0).Value = oCustomClass.CIF
    '    Try
    '        oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, Generate_TEXT).Tables(0)

    '        Return oReturnValue
    '    Catch exp As Exception
    '        Throw New Exception("Error On DataAccess.SLIK.D01.Generate")
    '    End Try
    'End Function

End Class


