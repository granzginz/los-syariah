
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class PaymentRequestController
    Public Function GetTableTransaction(ByVal oCustomClass As Parameter.PaymentRequest, ByVal strFile As String) As Parameter.PaymentRequest
        Dim oGetTable As IPaymentRequest
        oGetTable = ComponentFactory.CreatePaymentRequest()
        Return oGetTable.GetTableTransaction(oCustomClass, strFile)
    End Function

    Public Function Get_RequestNo(ByVal customClass As Parameter.PaymentRequest) As String
        Dim BO As IPaymentRequest
        BO = ComponentFactory.CreatePaymentRequest
        Return BO.Get_RequestNo(customClass)
    End Function

    Public Function SavePR(ByVal customClass As Parameter.PaymentRequest) As String
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        Return PRBO.SavePR(customClass)
    End Function
    Public Function CekPR(ByVal customClass As Parameter.PaymentRequest) As String
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        Return PRBO.CekPR(customClass)
    End Function

    Public Function GetPaymentRequestPaging(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        Return PRBO.GetPaymentRequestPaging(customClass)
    End Function
    Public Function GetPaymentRequestPagingFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        Return PRBO.GetPaymentRequestPagingFA(customClass)
    End Function
    Public Function GetPaymentReceivePaging(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        Return PRBO.GetPaymentReceivePaging(customClass)
    End Function
    Public Function GetPenjualanPaging(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        Return PRBO.GetPenjualanPagingFA(customClass)
    End Function
    Public Function GetPenghapusanPaging(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        Return PRBO.GetPenghapusanPagingFA(customClass)
    End Function
    Public Function GetPaymentRequestHeaderAndDetail(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        Return PRBO.GetPaymentRequestHeaderAndDetail(customClass)
    End Function
    Public Function GetPaymentRequestHeaderAndDetailFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        Return PRBO.GetPaymentRequestHeaderAndDetailFA(customClass)
    End Function
    Public Function GetPaymentReceiveHeaderAndDetailFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        Return PRBO.GetPaymentReceiveHeaderAndDetailFA(customClass)
    End Function
    Public Function GetPenjualanHeaderAndDetailFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        Return PRBO.GetPenjualanHeaderAndDetailFA(customClass)
    End Function
    Public Function GetPenghapusanHeaderAndDetailFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        Return PRBO.GetPenghapusanHeaderAndDetailFA(customClass)
    End Function

    Public Function GetPaymentRequestDataset(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        Return PRBO.GetPaymentRequestDataset(customClass)
    End Function

    Public Function saveApprovalPaymentRequest(ByVal customClass As Parameter.PaymentRequest, ByVal strApproval As String) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        PRBO.saveApprovalPaymentRequest(customClass, strApproval)
    End Function
    Public Function saveApprovalPaymentRequestFA(ByVal customClass As Parameter.PaymentRequest, ByVal strApproval As String) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        PRBO.saveApprovalPaymentRequestFA(customClass, strApproval)
    End Function
    Public Function saveApprovalPaymentReceiveFA(ByVal customClass As Parameter.PaymentRequest, ByVal strApproval As String) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        PRBO.saveApprovalPaymentReceiveFA(customClass, strApproval)
    End Function
    Public Function saveApprovalPenjualanFA(ByVal customClass As Parameter.PaymentRequest, ByVal strApproval As String) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        PRBO.saveApprovalPenjualanFA(customClass, strApproval)
    End Function
    Public Function saveApprovalPenghapusanFA(ByVal customClass As Parameter.PaymentRequest, ByVal strApproval As String) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        PRBO.saveApprovalPenghapusanFA(customClass, strApproval)
    End Function

    Public Function GetPaymentRequestFormKuning(ByVal customClass As IList(Of String), cnn As String) As DataSet
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        Return PRBO.GetPaymentRequestFormKuning(customClass, cnn)
    End Function

    Public Function PayReqStatusReject(ByVal customClass As Parameter.PaymentRequest, ByVal strApproval As String) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        PRBO.PayReqStatusReject(customClass, strApproval)
    End Function
    Public Function PRCOAUpdate(ByVal oCustomClass As Parameter.PaymentRequest) As String
        Dim oPC As IPaymentRequest
        oPC = ComponentFactory.CreatePaymentRequest
        Return oPC.PRCOAUpdate(oCustomClass)
    End Function
    Public Function GetPaymentVoucherByRequestNoPrint(ByVal ocustom As Parameter.PaymentRequest) As Parameter.PaymentRequest
        Dim oPC As IPaymentRequest
        oPC = ComponentFactory.CreatePaymentRequest
        Return oPC.GetPaymentVoucherByRequestNoPrint(ocustom)
    End Function
    Public Function GetBankAccountOther(ByVal Customclass As Parameter.PaymentRequest) As Parameter.PaymentRequest
        Dim oPC As IPaymentRequest
        oPC = ComponentFactory.CreatePaymentRequest()
        Return oPC.GetBankAccountOther(Customclass)
    End Function

    Public Function UpdatePR(ByVal customClass As Parameter.PaymentRequest) As String
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        Return PRBO.UpdatePR(customClass)
    End Function

    Public Function GetFAPaymentRequestDataSet(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
        Dim PRBO As IPaymentRequest
        PRBO = ComponentFactory.CreatePaymentRequest
        Return PRBO.GetFAPaymentRequestDataSet(customClass)
    End Function

End Class
