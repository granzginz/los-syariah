

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class CheckCashierController
    Public Function CheckCashier(ByVal strConnection As String, ByVal LoginID As String, _
                                 ByVal BranchID As String, ByVal BusinessDate As Date) As Boolean
        Dim oCheckCashier As ICheckCashier
        oCheckCashier = ComponentFactory.CreateCheckCashier()
        Return oCheckCashier.CheckCashier(strConnection, LoginID, BranchID, BusinessDate)
    End Function
End Class
