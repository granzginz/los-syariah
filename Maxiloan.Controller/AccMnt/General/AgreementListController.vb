

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AgreementListController
    Public Function AgreementList(ByVal oCustomClass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oAgreementList As IAgreementList
        oAgreementList = ComponentFactory.CreateAgreementList()
        Return oAgreementList.ListAgreement(oCustomClass)
    End Function
    Public Function AgreementListView(ByVal oCustomClass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oAgreementList As IAgreementList
        oAgreementList = ComponentFactory.CreateAgreementList()
        Return oAgreementList.ListAgreementView(oCustomClass)
    End Function
    Public Function AgreementListFactoring(ByVal oCustomClass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oAgreementList As IAgreementList
        oAgreementList = ComponentFactory.CreateAgreementList()
        Return oAgreementList.ListAgreementFactoring(oCustomClass)
    End Function
    Public Function AgreementListFactoringView(ByVal oCustomClass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oAgreementList As IAgreementList
        oAgreementList = ComponentFactory.CreateAgreementList()
        Return oAgreementList.ListAgreementFactoringView(oCustomClass)
    End Function
    Public Function AgreementListRCVPPH(ByVal oCustomClass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oAgreementList As IAgreementList
        oAgreementList = ComponentFactory.CreateAgreementList()
        Return oAgreementList.ListRCVPPH(oCustomClass)
    End Function
    Public Function AgreementListModalKerja(ByVal oCustomClass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oAgreementList As IAgreementList
        oAgreementList = ComponentFactory.CreateAgreementList()
        Return oAgreementList.ListAgreementModalKerja(oCustomClass)
    End Function
    Public Function AgreementListModalKerjaView(ByVal oCustomClass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oAgreementList As IAgreementList
        oAgreementList = ComponentFactory.CreateAgreementList()
        Return oAgreementList.ListAgreementModalKerjaView(oCustomClass)
    End Function
    Public Function AgreementListLinkAge(ByVal oCustomClass As Parameter.AgreementList) As Parameter.AgreementList
		Dim oAgreementList As IAgreementList
		oAgreementList = ComponentFactory.CreateAgreementList()
		Return oAgreementList.ListAgreementLinkAge(oCustomClass)
	End Function
	Public Function AgreementListReport(ByVal oCustomClass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oAgreementList As IAgreementList
        oAgreementList = ComponentFactory.CreateAgreementList()
        Return oAgreementList.ReportListAgreement(oCustomClass)
    End Function

    Public Function PrepayRequestList(ByVal oCustomClass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oAgreementList As IAgreementList
        oAgreementList = ComponentFactory.CreateAgreementList()
        Return oAgreementList.PrepayRequestList(oCustomClass)
    End Function
    Public Function AgreementIdentifikasiPembayaran(ByVal customclass As Parameter.AgreementList, ByVal dtTable As DataTable) As Parameter.AgreementList
        Dim oAgreementList As IAgreementList
        oAgreementList = ComponentFactory.CreateAgreementList()
        Return oAgreementList.AgreementIdentifikasiPembayaran(customclass, dtTable)
    End Function

    Public Function AgreementListDrawdown(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oAgreementList As IAgreementList
        oAgreementList = ComponentFactory.CreateAgreementList()
        Return oAgreementList.AgreementListDrawdown(customclass)
    End Function

    Public Function AgreementListDisburseFactoring(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oAgreementList As IAgreementList
        oAgreementList = ComponentFactory.CreateAgreementList()
        Return oAgreementList.AgreementListDisburseFactoring(customclass)
    End Function

    'penambahan reversal fact & mdkj
    Public Function AgreementListFACT(ByVal oCustomClass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oAgreementList As IAgreementList
        oAgreementList = ComponentFactory.CreateAgreementList()
        Return oAgreementList.ListAgreementFACT(oCustomClass)
    End Function
    Public Function AgreementListMDKJ(ByVal oCustomClass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oAgreementList As IAgreementList
        oAgreementList = ComponentFactory.CreateAgreementList()
        Return oAgreementList.ListAgreementMDKJ(oCustomClass)
    End Function

    'penambahan alokasi prepaid
    Public Function AgreementListPrepaid(ByVal oCustomClass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oAgreementList As IAgreementList
        oAgreementList = ComponentFactory.CreateAgreementList()
        Return oAgreementList.AgreementListPrepaid(oCustomClass)
    End Function

    Public Function PrepaidAllocation(ByVal oCustomClass2 As Parameter.InstallRcv) As Parameter.InstallRcv
        Dim oAgreementList As IAgreementList
        oAgreementList = ComponentFactory.CreateAgreementList()
        Return oAgreementList.PrepaidAllocation(oCustomClass2)
    End Function
    Public Function ListAgreementnew(ByVal oCustomClass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oAgreementList As IAgreementList
        oAgreementList = ComponentFactory.CreateAgreementList()
        Return oAgreementList.ListAgreementnew(oCustomClass)
    End Function
End Class
