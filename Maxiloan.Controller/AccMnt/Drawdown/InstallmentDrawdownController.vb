
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Parameter

Public Class InstallmentDrawdownController


    Public Function DrawdownApprovalList(oCustomClass As Parameter.Drawdown) As Parameter.Drawdown
        Dim DataBO As IInstallmentDrawdown
        DataBO = ComponentFactory.CreateInstallmentDrawdown()
        Return DataBO.DrawdownApprovalList(oCustomClass)
    End Function

    Public Function GetCboUserAprPCAll(oCustomClass As Parameter.Drawdown) As Parameter.Drawdown
        Dim DataBO As IInstallmentDrawdown
        DataBO = ComponentFactory.CreateInstallmentDrawdown()
        Return DataBO.GetCboUserAprPCAll(oCustomClass)
    End Function

    Public Function GetCboUserApproval(oCustomClass As Parameter.Drawdown) As Parameter.Drawdown
        Dim DataBO As IInstallmentDrawdown
        DataBO = ComponentFactory.CreateInstallmentDrawdown()
        Return DataBO.GetCboUserApproval(oCustomClass)
    End Function

    Public Function ApproveSaveToNextPerson(oCustomClass As Parameter.Drawdown) As String
        Dim DataBO As IInstallmentDrawdown
        DataBO = ComponentFactory.CreateInstallmentDrawdown()
        Return DataBO.ApproveSaveToNextPerson(oCustomClass)
    End Function

    Public Function ApproveSave(oCustomClass As Parameter.Drawdown) As String
        Dim DataBO As IInstallmentDrawdown
        DataBO = ComponentFactory.CreateInstallmentDrawdown()
        Return DataBO.ApproveSave(oCustomClass)
    End Function

    Public Function ApproveisFinal(oCustomClass As Parameter.Drawdown) As Parameter.Drawdown
        Dim DataBO As IInstallmentDrawdown
        DataBO = ComponentFactory.CreateInstallmentDrawdown()
        Return DataBO.ApproveisFinal(oCustomClass)
    End Function

    Public Function ApproveIsValidApproval(oCustomClass As Parameter.Drawdown) As Parameter.Drawdown
        Dim DataBO As IInstallmentDrawdown
        DataBO = ComponentFactory.CreateInstallmentDrawdown()
        Return DataBO.ApproveIsValidApproval(oCustomClass)
    End Function

End Class
