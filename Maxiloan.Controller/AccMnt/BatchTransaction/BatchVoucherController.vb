
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class BatchVoucherController
#Region "Batch Voucher"
    Public Function BatchVoucherPaging(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
        Dim oBatchVoucher As IBatchVoucher
        oBatchVoucher = ComponentFactory.CreateBatchVoucher()
        Return oBatchVoucher.BatchVoucherPaging(oCustomClass)
    End Function

    Public Sub BatchVoucherAdd(ByVal oCustomClass As Parameter.BatchVoucher)
        Dim oBatchVoucher As IBatchVoucher
        oBatchVoucher = ComponentFactory.CreateBatchVoucher()
        oBatchVoucher.BatchVoucherAdd(oCustomClass)
    End Sub
    Public Sub BatchVoucherEdit(ByVal oCustomClass As Parameter.BatchVoucher)
        Dim oBatchVoucher As IBatchVoucher
        oBatchVoucher = ComponentFactory.CreateBatchVoucher()
        oBatchVoucher.BatchVoucherEdit(oCustomClass)
    End Sub
    Public Sub BatchVoucherDelete(ByVal oCustomClass As Parameter.BatchVoucher)
        Dim oBatchVoucher As IBatchVoucher
        oBatchVoucher = ComponentFactory.CreateBatchVoucher()
        oBatchVoucher.BatchVoucherDelete(oCustomClass)
    End Sub
    Public Function BatchVoucherView(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
        Dim oBatchVoucher As IBatchVoucher
        oBatchVoucher = ComponentFactory.CreateBatchVoucher()
        Return oBatchVoucher.BatchVoucherView(oCustomClass)
    End Function
    Public Sub BatchVoucherPosted(ByVal ocustomclass As Parameter.BatchVoucher)
        Dim oBatchVoucher As IBatchVoucher
        oBatchVoucher = ComponentFactory.CreateBatchVoucher()
        oBatchVoucher.BatchVoucherPosted(ocustomclass)
    End Sub
#End Region

#Region "Cash Bank Batch Voucher"
    Public Function CashBankBatchVoucherPaging(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
        Dim oBatchVoucher As IBatchVoucher
        oBatchVoucher = ComponentFactory.CreateBatchVoucher()
        Return oBatchVoucher.CashBankBatchVoucherPaging(oCustomClass)
    End Function

    Public Sub CashBankBatchVoucherAdd(ByVal oCustomClass As Parameter.BatchVoucher)
        Dim oBatchVoucher As IBatchVoucher
        oBatchVoucher = ComponentFactory.CreateBatchVoucher()
        oBatchVoucher.CashBankBatchVoucherAdd(oCustomClass)
    End Sub

    Public Sub CashBankBatchVoucherEdit(ByVal oCustomClass As Parameter.BatchVoucher)
        Dim oBatchVoucher As IBatchVoucher
        oBatchVoucher = ComponentFactory.CreateBatchVoucher()
        oBatchVoucher.CashBankBatchVoucherEdit(oCustomClass)
    End Sub
    Public Sub CashBankBatchVoucherDelete(ByVal oCustomClass As Parameter.BatchVoucher)
        Dim oBatchVoucher As IBatchVoucher
        oBatchVoucher = ComponentFactory.CreateBatchVoucher()
        oBatchVoucher.CashBankBatchVoucherDelete(oCustomClass)
    End Sub

    Public Function CashBankBatchVoucherView(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
        Dim oBatchVoucher As IBatchVoucher
        oBatchVoucher = ComponentFactory.CreateBatchVoucher()
        Return oBatchVoucher.CashBankBatchVoucherView(oCustomClass)
    End Function
#End Region

    Public Function getTransactionBatchUpload(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
        Dim oBatchVoucher As IBatchVoucher
        oBatchVoucher = ComponentFactory.CreateBatchVoucher()
        Return oBatchVoucher.getTransactionBatchUpload(oCustomClass)
    End Function

    Public Sub saveSingleBatch(ByVal oCustomClass As Parameter.BatchVoucher)
        Dim oBatchVoucher As IBatchVoucher
        oBatchVoucher = ComponentFactory.CreateBatchVoucher()
        oBatchVoucher.saveSingleBatch(oCustomClass)
    End Sub
    Public Sub DeleteUploadedBatch(ByVal oCustomClass As Parameter.BatchVoucher)
        Dim oBatchVoucher As IBatchVoucher
        oBatchVoucher = ComponentFactory.CreateBatchVoucher()
        oBatchVoucher.DeleteUploadedBatch(oCustomClass)
    End Sub

    Public Function getCompleteBatch(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
        Dim oBatchVoucher As IBatchVoucher
        oBatchVoucher = ComponentFactory.CreateBatchVoucher()
        Return oBatchVoucher.getCompleteBatch(oCustomClass)
    End Function

End Class
