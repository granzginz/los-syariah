
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class ARAnalysisReportController

    Public Function ArAnalysisReport(ByVal oCustomClass As Parameter.ArAnalysis) As DataSet
        Dim oArAnalysisReport As IArAnalysis
        oArAnalysisReport = ComponentFactory.CreateArAnalysisReport()
        Return oArAnalysisReport.ArAnalysisReport(oCustomClass)
    End Function
    Public Function TerminationReport(ByVal oCustomClass As Parameter.ArAnalysis) As Parameter.ArAnalysis
        Dim BO As IArAnalysis
        BO = ComponentFactory.CreateArAnalysisReport()
        Return BO.TerminationReport(oCustomClass)
    End Function

End Class
