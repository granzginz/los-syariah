
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class DChangeController
#Region "Request"
    Public Function GetListAmor(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim GAmor As IDChange
        GAmor = ComponentFactory.CreateDChange
        Return GAmor.GetListAmor(oCustomClass)
    End Function

    Public Function GetList(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim GList As IDChange
        GList = ComponentFactory.CreateDChange
        Return GList.GetList(oCustomClass)
    End Function

    Public Sub DueDateChangeRequest(ByVal oCustomClass As Parameter.DChange)
        Dim DChange As IDChange
        DChange = ComponentFactory.CreateDChange
        DChange.DueDateChangeRequest(oCustomClass)
    End Sub

    Public Function GetMaxDate(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim DMax As IDChange
        DMax = ComponentFactory.CreateDChange
        Return DMax.GetMaxDate(oCustomClass)
    End Function

#End Region

#Region "Execute"
    Public Function GetListExec(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim GList As IDChange
        GList = ComponentFactory.CreateDChange
        Return GList.GetListExec(oCustomClass)
    End Function

    Public Sub DueDateChangeExec(ByVal oCustomClass As Parameter.DChange)
        Dim DChangeE As IDChange
        DChangeE = ComponentFactory.CreateDChange
        DChangeE.DueDateChangeExec(oCustomClass)
    End Sub

#End Region

#Region "Request Modal Kerja"
	Public Function GetListAmorModalKerja(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
		Dim GAmor As IDChange
		GAmor = ComponentFactory.CreateDChange
		Return GAmor.GetListAmorModalKerja(oCustomClass)
	End Function

	Public Function GetListAmorModalKerjaFromTable(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
		Dim GAmor As IDChange
		GAmor = ComponentFactory.CreateDChange
		Return GAmor.GetListAmorModalKerjaFromTable(oCustomClass)
	End Function

	Public Function CreateAmorModalKerja(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
		Dim GAmor As IDChange
		GAmor = ComponentFactory.CreateDChange
		Return GAmor.CreateAmorModalKerja(oCustomClass)
	End Function

	Public Function GetListModalKerja(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
		Dim GList As IDChange
		GList = ComponentFactory.CreateDChange
		Return GList.GetListModalKerja(oCustomClass)
	End Function

	Public Sub MDKJDueDateChangeRequest(ByVal oCustomClass As Parameter.DChange)
		Dim DChange As IDChange
		DChange = ComponentFactory.CreateDChange
		DChange.MDKJDueDateChangeRequest(oCustomClass)
	End Sub

	'Public Function GetMaxDate(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
	'	Dim DMax As IDChange
	'	DMax = ComponentFactory.CreateDChange
	'	Return DMax.GetMaxDate(oCustomClass)
	'End Function

	Public Function GetListExecMDKJ(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
		Dim GList As IDChange
		GList = ComponentFactory.CreateDChange
		Return GList.GetListExecMDKJ(oCustomClass)
	End Function

    Public Sub DueDateChangeExecMDKJ(ByVal oCustomClass As Parameter.DChange)
        Dim DChangeE As IDChange
        DChangeE = ComponentFactory.CreateDChange
        DChangeE.DueDateChangeExecMDKJ(oCustomClass)
    End Sub

    Public Function GetListAmorMDKJ(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim GAmor As IDChange
        GAmor = ComponentFactory.CreateDChange
        Return GAmor.GetListAmorMDKJ(oCustomClass)
    End Function

    Public Function GetListAmorFACT(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim GAmor As IDChange
        GAmor = ComponentFactory.CreateDChange
        Return GAmor.GetListAmorFACT(oCustomClass)
    End Function
#End Region

#Region "Factoring"
    Public Function GetListAmorFactoring(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
		Dim GAmor As IDChange
		GAmor = ComponentFactory.CreateDChange
		Return GAmor.GetListAmorFactoring(oCustomClass)
	End Function

	Public Function GetListInvoiceFactoring(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
		Dim GAmor As IDChange
		GAmor = ComponentFactory.CreateDChange
		Return GAmor.GetListInvoiceFactoring(oCustomClass)
	End Function
	Public Function GetListAmorFactoringFromTable(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
		Dim GAmor As IDChange
		GAmor = ComponentFactory.CreateDChange
		Return GAmor.GetListAmorFactoringFromTable(oCustomClass)
	End Function

	Public Function CreateAmorFactoring(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
		Dim GAmor As IDChange
		GAmor = ComponentFactory.CreateDChange
		Return GAmor.CreateAmorFactoring(oCustomClass)
	End Function

	Public Function GetListFactoring(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
		Dim GList As IDChange
		GList = ComponentFactory.CreateDChange
		Return GList.GetListFactoring(oCustomClass)
	End Function

	Public Sub DueDateChangeRequestFactoring(ByVal oCustomClass As Parameter.DChange)
		Dim DChange As IDChange
		DChange = ComponentFactory.CreateDChange
		DChange.DueDateChangeRequestFactoring(oCustomClass)
	End Sub

    'Public Function GetMaxDate(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
    '	Dim DMax As IDChange
    '	DMax = ComponentFactory.CreateDChange
    '	Return DMax.GetMaxDate(oCustomClass)
    'End Function

    Public Function GetListExecFactoring(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim GList As IDChange
        GList = ComponentFactory.CreateDChange
        Return GList.GetListExecFactoring(oCustomClass)
    End Function
    Public Function GetListExecFactoringApproval(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim GList As IDChange
        GList = ComponentFactory.CreateDChange
        Return GList.GetListExecFactoringApproval(oCustomClass)
    End Function

    Public Sub DueDateChangeExecFactoring(ByVal oCustomClass As Parameter.DChange)
        Dim DChangeE As IDChange
        DChangeE = ComponentFactory.CreateDChange
        DChangeE.DueDateChangeExecFactoring(oCustomClass)
    End Sub
    Public Function GetListFactoringView(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim GList As IDChange
        GList = ComponentFactory.CreateDChange
        Return GList.GetListFactoringView(oCustomClass)
    End Function
    Public Function GetListFactoringViewApproval(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim GList As IDChange
        GList = ComponentFactory.CreateDChange
        Return GList.GetListFactoringViewApproval(oCustomClass)
    End Function
#End Region

    Public Function GetListFactAndMU(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim GList As IDChange
        GList = ComponentFactory.CreateDChange
        Return GList.GetListFactAndMU(oCustomClass)
    End Function

    Public Function GetListReschedulingApproval(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim GList As IDChange
        GList = ComponentFactory.CreateDChange
        Return GList.GetListReschedulingApproval(oCustomClass)
    End Function

    Public Function GetListInvoiceFactoringView(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim GAmor As IDChange
        GAmor = ComponentFactory.CreateDChange
        Return GAmor.GetListInvoiceFactoringView(oCustomClass)
    End Function
    Public Function GetListAmorFactoringView(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim GAmor As IDChange
        GAmor = ComponentFactory.CreateDChange
        Return GAmor.GetListAmorFactoringView(oCustomClass)
    End Function

End Class
