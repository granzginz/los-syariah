

#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class AChangeWOPController
    Public Function GetListWOP(ByVal oCustomClass As Parameter.AChangeWOP) As Parameter.AChangeWOP
        Dim oListW As IAChangeWOP
        oListW = ComponentFactory.CreateAgreementChangeWOP
        Return oListW.GetListWOP(oCustomClass)
    End Function

    Public Sub WOPChange(ByVal oCustomClass As Parameter.AChangeWOP)
        Dim WOP As IAChangeWOP
        WOP = ComponentFactory.CreateAgreementChangeWOP
        WOP.WOPChange(oCustomClass)
    End Sub
End Class
