﻿Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class InstallRcv3rdController


    Public Function DoUpload3rdFile(cnn As String, rows As Parameter.InstallRcv3rd, sesId As String) As String
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.DoUpload3rdFile(cnn, rows, sesId)
    End Function

    Public Function GetUploadFilePage(cnn As String, currentPage As Integer, pageSize As Integer, sesId As String, nvType As Parameter.EnViaType) As Parameter.InstallRcv3rd
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.GetUploadFilePage(cnn, currentPage, pageSize, sesId, nvType)
    End Function

    Public Function GetUploadFilePage(cnn As String, currentPage As Integer, pageSize As Integer, trDate As DateTime, nvType As Parameter.EnViaType) As Parameter.InstallRcv3rd
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.GetUploadFilePage(cnn, currentPage, pageSize, trDate, nvType)
    End Function

    Public Function PostingInstallmentUpload(cnn As String, compid As String, branchid As String, valuedate As DateTime, bsnDate As DateTime, login As String, vtype As Parameter.EnViaType) As String
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.PostingInstallmentUpload(cnn, compid, branchid, valuedate, bsnDate, login, vtype)
    End Function
     
    Public Function GeneratePaymentGatewayFile(cnn As String, uploadDate As Date, dir As String) As Boolean
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.GeneratePaymentGatewayFile(cnn, uploadDate, dir)
    End Function

    Public Function GetPaymentGatewayFile(cnn As String, processDate As DateTime) As DataTable
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.GetPaymentGatewayFile(cnn, processDate)
    End Function
    Function DoPostingInstallmentUpload(cnn As String, branchho As String, processDate As DateTime, businessDate As DateTime, companyId As String, vtype As Parameter.EnViaType, listKey As IList(Of String)) As String
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.DoPostingInstallmentUpload(cnn, branchho, processDate, businessDate, companyId, vtype, listKey)
    End Function


    Public Function DoUploadAgreementVirtualAccount(cnn As String, rows As Parameter.AgreementVirtualAcc) As String
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.DoUploadAgreementVirtualAccount(cnn, rows)
    End Function
End Class
