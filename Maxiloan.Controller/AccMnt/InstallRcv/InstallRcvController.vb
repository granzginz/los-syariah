
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InstallRcvController
    Private oCustomClass As Parameter.InstallRcv

    Public Sub New(Optional ByVal pCustomClass As Parameter.InstallRcv = Nothing)
        oCustomClass = pCustomClass
    End Sub

    Public Function IsLastPayment(ByVal oCustomClass As Parameter.InstallRcv) As Boolean
        Dim oMaxBackDate As IInstallRcv
        oMaxBackDate = ComponentFactory.CreateInstallmentReceive
        Return oMaxBackDate.IsLastPayment(oCustomClass)
    End Function

    Public Function InstallmentReceive() As Integer
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.InstallmentReceive(oCustomClass)
    End Function

    Public Function InstallmentReceiveAdvIns() As Integer
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.InstallmentReceiveAdvIns(oCustomClass)
    End Function

    Public Function IsMaxBackDate(ByVal strConnection As String, ByVal ValueDate As Date, ByVal BusinessDate As Date) As Boolean
        Dim oMaxBackDate As IInstallRcv
        oMaxBackDate = ComponentFactory.CreateInstallmentReceive
        Return oMaxBackDate.IsMaxBackDate(strConnection, ValueDate, BusinessDate)
    End Function

    Public Function IsValidLastPayment(ByVal strConnection As String, ByVal ApplicationID As String, _
                                        ByVal ValueDate As Date) As Boolean
        Dim oIsValidLastPayment As IInstallRcv
        oIsValidLastPayment = ComponentFactory.CreateInstallmentReceive
        Return oIsValidLastPayment.IsValidLastPayment(strConnection, ApplicationID, ValueDate)
    End Function

    Public Overloads Function PrintTTU(ByVal strConnection As String, ByVal ApplicationID As String, ByVal branchID As String, _
                                    ByVal BusinessDate As Date, ByVal HistorySequenceNo As Integer, ByVal LoginID As String) As DataSet
        Dim oPrintTTU As IInstallRcv
        oPrintTTU = ComponentFactory.CreateInstallmentReceive
        Return oPrintTTU.PrintTTU(strConnection, ApplicationID, branchID, BusinessDate, HistorySequenceNo, LoginID)

    End Function

    Public Overloads Function PrintTTU(ByVal strConnection As String, ByVal ApplicationID As String, ByVal branchID As String, _
                                    ByVal BusinessDate As Date, ByVal HistorySequenceNo As Integer, ByVal LoginID As String, ByVal InsSeqNo As Integer) As DataSet
        Dim oPrintTTU As IInstallRcv
        oPrintTTU = ComponentFactory.CreateInstallmentReceive
        Return oPrintTTU.PrintTTU(strConnection, ApplicationID, branchID, BusinessDate, HistorySequenceNo, LoginID, InsSeqNo)

    End Function

    Public Overloads Function PrintTTU(ByVal strConnection As String, ByVal ApplicationID As String, ByVal branchID As String, _
                                    ByVal BusinessDate As Date, ByVal LoginID As String) As DataSet
        Dim oPrintTTU As IInstallRcv
        oPrintTTU = ComponentFactory.CreateInstallmentReceive
        Return oPrintTTU.PrintTTU(strConnection, ApplicationID, branchID, BusinessDate, LoginID)
    End Function

    Public Overloads Function PrintInstallmentReceive(ByVal strConnection As String, ByVal ApplicationID As String, ByVal branchID As String, _
                                  ByVal BusinessDate As Date, ByVal LoginID As String) As DataSet
        Dim oPrintTTU As IInstallRcv
        oPrintTTU = ComponentFactory.CreateInstallmentReceive
        Return oPrintTTU.PrintInstallmentReceive(strConnection, ApplicationID, branchID, BusinessDate, LoginID)
    End Function

    Public Function ListKwitansiInstallment(ByVal oCustomClass As Parameter.InstallRcv) As DataSet
        Dim oPrintTTU As IInstallRcv
        oPrintTTU = ComponentFactory.CreateInstallmentReceive
        Return oPrintTTU.ListKwitansiInstallment(oCustomClass)
    End Function

    Public Function SavePrintKwitansiInstallment(ByVal oCustomClass As Parameter.InstallRcv) As Parameter.InstallRcv
        Dim oPrintTTU As IInstallRcv
        oPrintTTU = ComponentFactory.CreateInstallmentReceive
        Return oPrintTTU.SavePrintKwitansiInstallment(oCustomClass)
    End Function

    Public Function ReportKwitansiInstallment(ByVal oCustomClass As Parameter.InstallRcv) As DataSet
        Dim oPrintTTU As IInstallRcv
        oPrintTTU = ComponentFactory.CreateInstallmentReceive
        Return oPrintTTU.ReportKwitansiInstallment(oCustomClass)
    End Function

    Public Function ReportKwitansiInstallment2(ByVal oCustomClass As Parameter.InstallRcv) As DataSet
        Dim oPrintTTU As IInstallRcv
        oPrintTTU = ComponentFactory.CreateInstallmentReceive
        Return oPrintTTU.ReportKwitansiInstallment2(oCustomClass)
    End Function
    Public Function ReportKwitansiInstallmentWithVoucherNo(ByVal customclass As Parameter.InstallRcv) As DataSet
        Dim oPrintTTU As IInstallRcv
        oPrintTTU = ComponentFactory.CreateInstallmentReceive
        Return oPrintTTU.ReportKwitansiInstallmentWithVoucherNo(customclass)
    End Function

    Public Function InstallRcvInstallmentSchedulePaging(ocustomclass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim oPrintTTU As IInstallRcv
        oPrintTTU = ComponentFactory.CreateInstallmentReceive
        Return oPrintTTU.InstallRcvInstallmentSchedulePaging(ocustomclass)
    End Function

    Public Function AdvanceInstallmet(ByVal CustomCLass As Parameter.InstallRcv) As Parameter.InstallRcv
        Dim oPrintTTU As IInstallRcv
        oPrintTTU = ComponentFactory.CreateInstallmentReceive
        Return oPrintTTU.AdvanceInstallmet(CustomCLass)
    End Function
    Public Function AlokasiAdvanceInstallmet(ByVal CustomClass As Parameter.InstallRcv) As String
        Dim oPrintTTU As IInstallRcv
        oPrintTTU = ComponentFactory.CreateInstallmentReceive
        Return oPrintTTU.AlokasiAdvanceInstallmet(CustomClass)
    End Function

    Public Function GetSP(ByVal customclass As Parameter.InstallRcv) As Parameter.InstallRcv
        Dim oPrintTTU As IInstallRcv
        oPrintTTU = ComponentFactory.CreateInstallmentReceive
        Return oPrintTTU.GetSP(customclass)
    End Function

    Public Function ReportKwitansiInstallmentAdvIns(ByVal oCustomClass As Parameter.InstallRcv) As DataSet
        Dim oPrintTTU As IInstallRcv
        oPrintTTU = ComponentFactory.CreateInstallmentReceive
        Return oPrintTTU.ReportKwitansiInstallmentAdvIns(oCustomClass)

    End Function

    Public Function InstallRCVKorAngsSave() As Integer
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.InstallRCVKorAngsSave(oCustomClass)
    End Function

    Public Function InstallRCVTemp() As Integer
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.InstallRCVTemp(oCustomClass)
    End Function

    Public Function TransaksiPending(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.TransaksiPending(customClass)
    End Function
    Public Function DetailTransaction(ByVal customClass As Parameter.InstallRcv) As DataTable
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.DetailTransaction(customClass)
    End Function

    Public Function InstallRCVBATemp() As Integer
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.InstallRCVBATemp(oCustomClass)
    End Function

    Public Function InstallRCVBAFactoring() As Integer
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.InstallRCVBAFactoring(oCustomClass)
    End Function
    Public Function InstallRCVPPH() As Integer
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.InstallRCVPPH(oCustomClass)
    End Function

    Public Sub SaveOtorisasiPPH(ByVal oCustomClass As Parameter.InstallRcv)
        Dim oSaveOtorisasiPPH As IInstallRcv
        oSaveOtorisasiPPH = ComponentFactory.CreateInstallmentReceive
        oSaveOtorisasiPPH.SaveOtorisasiPPH(oCustomClass)
    End Sub
    Public Sub RejectOtorisasiPPH(ByVal oCustomClass As Parameter.InstallRcv)
        Dim oRejectOtorisasiPPH As IInstallRcv
        oRejectOtorisasiPPH = ComponentFactory.CreateInstallmentReceive
        oRejectOtorisasiPPH.RejectOtorisasiPPH(oCustomClass)
    End Sub

    Public Function InstallRCVBAModalKerja() As Integer
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.InstallRCVBAModalKerja(oCustomClass)
    End Function

    Public Function GetGeneralPaging(ByVal CustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.GetGeneralPaging(CustomClass)
    End Function
    Public Sub SaveOtorisasi(ByVal oCustomClass As Parameter.InstallRcv)
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        oInstallmentReceive.SaveOtorisasi(oCustomClass)
    End Sub
    Public Sub RejectOtorisasi(ByVal oCustomClass As Parameter.InstallRcv)
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        oInstallmentReceive.RejectOtorisasi(oCustomClass)
    End Sub
    Public Function InstallRCVCollOtor() As Integer
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.InstallRCVCollOtor(oCustomClass)
    End Function

    Public Sub EditOtorisasi(ByVal oCustomClass As Parameter.InstallRcv)
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        oInstallmentReceive.EditOtorisasi(oCustomClass)
    End Sub

    Public Function InstallmentReceiveMobile() As Integer
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.InstallmentReceiveMobile(oCustomClass)
    End Function

    Public Function GetComboDepartemen(ByVal ocustomClass As Parameter.InstallRcv) As DataTable
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive()
        Return oInstallmentReceive.GetComboDepartemen(ocustomClass)
    End Function

    Public Function GetComboCabang(ByVal ocustomClass As Parameter.InstallRcv) As DataTable
        Dim oInstallmentReceive = ComponentFactory.CreateInstallmentReceive()
        Return oInstallmentReceive.GetComboCabang(ocustomClass)
    End Function

    Public Sub AlokasiPembNonARSaveEdit(ByVal ocustomClass As Parameter.InstallRcv)
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive()
        oInstallmentReceive.AlokasiPembNonARSaveEdit(ocustomClass)
    End Sub

    Public Function InstallKoreksiModalKerja() As Integer
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.InstallKoreksiModalKerja(oCustomClass)
    End Function

    Public Function InstallKoreksiFactoring() As Integer
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive
        Return oInstallmentReceive.InstallKoreksiFactoring(oCustomClass)
    End Function

    Public Sub AlokasiPembTolakanBGSaveEdit(ByVal ocustomClass As Parameter.InstallRcv)
        Dim oInstallmentReceive As IInstallRcv
        oInstallmentReceive = ComponentFactory.CreateInstallmentReceive()
        oInstallmentReceive.AlokasiPembTolakanBGSaveEdit(ocustomClass)
    End Sub
End Class
