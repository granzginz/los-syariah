
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InvoiceOLController

    Public Function CetakInvoiceOLPaging(ByVal customClass As Parameter.InvoiceOL) As Parameter.InvoiceOL
        Dim InvoiceOL As IInvoiceOL
        InvoiceOL = ComponentFactory.CreateInvoiceOL
        Return InvoiceOL.CetakInvoiceOLPaging(customClass)
    End Function

    Public Function CetakInvoiceOLDetail(ByVal customClass As Parameter.InvoiceOL) As DataTable
        Dim InvoiceOL As IInvoiceOL
        InvoiceOL = ComponentFactory.CreateInvoiceOL
        Return InvoiceOL.CetakInvoiceOLDetail(customClass)
    End Function

    Public Function CetakInvoiceOLSave(ByVal oCustomClass As Parameter.InvoiceOL) As String
        Dim InvoiceOL As IInvoiceOL
        InvoiceOL = ComponentFactory.CreateInvoiceOL
        Return InvoiceOL.CetakInvoiceOLSave(oCustomClass)
    End Function

    Public Function ReportKwitansi(ByVal customClass As Parameter.InvoiceOL) As DataTable
        Dim InvoiceOL As IInvoiceOL
        InvoiceOL = ComponentFactory.CreateInvoiceOL
        Return InvoiceOL.ReportKwitansi(customClass)
    End Function

    Public Function InvoiceOLPaging(ByVal customClass As Parameter.InvoiceOL) As Parameter.InvoiceOL
        Dim InvoiceOL As IInvoiceOL
        InvoiceOL = ComponentFactory.CreateInvoiceOL
        Return InvoiceOL.InvoiceOLPaging(customClass)
    End Function

    Public Function InvoiceOLDetail(ByVal customClass As Parameter.InvoiceOL) As DataTable
        Dim InvoiceOL As IInvoiceOL
        InvoiceOL = ComponentFactory.CreateInvoiceOL
        Return InvoiceOL.InvoiceOLDetail(customClass)
    End Function

    Public Function InvoiceOLExecute(ByVal oCustomClass As Parameter.InvoiceOL) As String
        Dim InvoiceOL As IInvoiceOL
        InvoiceOL = ComponentFactory.CreateInvoiceOL
        Return InvoiceOL.InvoiceOLExecute(oCustomClass)
    End Function

    Function GetInstallRCVPrint(ByVal customClass As Parameter.InvoiceOL) As DataTable
        Dim InvoiceOL As IInvoiceOL
        InvoiceOL = ComponentFactory.CreateInvoiceOL
        Return InvoiceOL.GetInstallRCVPrint(customClass)
    End Function

End Class
