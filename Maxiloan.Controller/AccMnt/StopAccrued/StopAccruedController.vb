

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class StopAccruedController
    Public Sub StopAccruedRequest(ByVal oCustomClass As Parameter.StopAccrued)
        Dim oStopAccruedRequest As IStopAccrued
        oStopAccruedRequest = ComponentFactory.CreateStopAccrued
        oStopAccruedRequest.StopAccruedRequest(oCustomClass)
    End Sub

    Public Function StopAccruedGetPastDueDays(ByVal oCustomClass As Parameter.StopAccrued) As Parameter.StopAccrued
        Dim oStopAccruedRequest As IStopAccrued
        oStopAccruedRequest = ComponentFactory.CreateStopAccrued
        Return oStopAccruedRequest.StopAccruedGetPastDueDays(oCustomClass)
    End Function

    Public Function StopAccruedView(ByVal oCustomClass As Parameter.StopAccrued) As Parameter.StopAccrued
        Dim oStopAccruedRequest As IStopAccrued
        oStopAccruedRequest = ComponentFactory.CreateStopAccrued
        Return oStopAccruedRequest.StopAccruedView(oCustomClass)
    End Function

    Public Function StopAccruedInquiry(ByVal oCustomClass As Parameter.StopAccrued) As Parameter.StopAccrued
        Dim oStopAccruedRequest As IStopAccrued
        oStopAccruedRequest = ComponentFactory.CreateStopAccrued
        Return oStopAccruedRequest.StopAccruedInquiry(oCustomClass)
    End Function
    Public Sub StopAccruedReversal(ByVal oCustomClass As Parameter.StopAccrued)
        Dim oStopAccruedRequest As IStopAccrued
        oStopAccruedRequest = ComponentFactory.CreateStopAccrued
        oStopAccruedRequest.StopAccruedReversal(oCustomClass)
    End Sub
    Public Function StopAccruedReversalList(ByVal oCustomClass As Parameter.StopAccrued) As Parameter.StopAccrued
        Dim oStopAccruedRequest As IStopAccrued
        oStopAccruedRequest = ComponentFactory.CreateStopAccrued
        Return oStopAccruedRequest.StopAccruedReversalList(oCustomClass)
    End Function
End Class
