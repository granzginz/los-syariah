
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class PDCEditMultiAgreementController
    Public Sub SaveEditPDCMultiAgreement(ByVal oCustomClass As Parameter.PDCMultiAgreement)
        Dim oPDCReceiveList As IPDCMultiAgreement
        oPDCReceiveList = ComponentFactory.CreatePDCMultiAgreement()
        oPDCReceiveList.SaveEditPDCMultiAgreement(oCustomClass)
    End Sub
    Public Function GetEditPDCMultiAgreement(ByVal oCustomClass As Parameter.PDCMultiAgreement) As Parameter.PDCMultiAgreement
        Dim oPDCReceiveList As IPDCMultiAgreement
        oPDCReceiveList = ComponentFactory.CreatePDCMultiAgreement()
        Return oPDCReceiveList.GetEditPDCMultiAgreement(oCustomClass)
    End Function
End Class