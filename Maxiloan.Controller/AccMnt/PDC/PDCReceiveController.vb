


#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class PDCReceiveController



    Public Function PDCReceiveList(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCReceiveList As IPDCReceive
        oPDCReceiveList = ComponentFactory.CreatePDCReceive()
        Return oPDCReceiveList.ListPDCReceive(oCustomClass)
    End Function

    Public Function GetHoliday(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oGetHoliday As IPDCReceive
        oGetHoliday = ComponentFactory.CreatePDCReceive()
        Return oGetHoliday.GetHoliday(oCustomClass)
    End Function

    Public Function GetTablePDC(ByVal oCustomClass As Parameter.PDCReceive, ByVal strFile As String) As Parameter.PDCReceive
        Dim oGetTablePDC As IPDCReceive
        oGetTablePDC = ComponentFactory.CreatePDCReceive()
        Return oGetTablePDC.GetTablePDC(oCustomClass, strFile)
    End Function

    Public Function SavePDCReceive(ByVal oCustomClass As Parameter.PDCReceive, ByVal strFile As String) As String
        Dim oSavePDCReceive As IPDCReceive
        oSavePDCReceive = ComponentFactory.CreatePDCReceive()
        Return oSavePDCReceive.SavePDCReceive(oCustomClass, strFile)
    End Function

    Public Function SavePDCReceiveBp(ByVal strConnection As String, ByVal oCustomClass As Parameter.PDCReceive) As String
        Dim oSavePDCReceive As IPDCReceive
        oSavePDCReceive = ComponentFactory.CreatePDCReceive()
        Return oSavePDCReceive.SavePDCReceiveBp(strConnection, oCustomClass)
    End Function


    Public Function PrintReceiptNo(ByVal strConnection As String, ByVal PDCReceiptNo As String, ByVal BranchID As String) As DataSet
        Dim oGetTablePDCEdit As IPDCReceive
        oGetTablePDCEdit = ComponentFactory.CreatePDCReceive()
        Return oGetTablePDCEdit.PrintReceiptNo(strConnection, PDCREceiptNo, BranchID)
    End Function

End Class
