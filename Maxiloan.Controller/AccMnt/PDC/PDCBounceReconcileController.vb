
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class PDCBounceReconcileController

    Public Function PDCBounceList(ByVal oCustomClass As Parameter.PDCBounce) As Parameter.PDCBounce
        Dim oPDCBounceList As IPDCBounce
        oPDCBounceList = ComponentFactory.CreatePDCBounce()
        Return oPDCBounceList.PDCBounceList(oCustomClass)
    End Function


    Public Sub ProcesPDCBounce(ByVal oCustomClass As Parameter.PDCBounce)
        Dim oProcesPDCBounce As IPDCBounce
        oProcesPDCBounce = ComponentFactory.CreatePDCBounce()
        oProcesPDCBounce.ProcesPDCBounce(oCustomClass)
    End Sub

    Public Function PDCBounceView(ByVal oCustomClass As Parameter.PDCBounce) As Parameter.PDCBounce
        Dim oProcesPDCBounce As IPDCBounce
        oProcesPDCBounce = ComponentFactory.CreatePDCBounce()
        Return oProcesPDCBounce.PDCBounceView(oCustomClass)
    End Function

    Public Function PrintPDCBounce(ByVal oCustomClass As Parameter.PDCBounce) As DataSet
        Dim oProcesPDCBounce As IPDCBounce
        oProcesPDCBounce = ComponentFactory.CreatePDCBounce()
        Return oProcesPDCBounce.PrintPDCBounce(oCustomClass)
    End Function
End Class
