

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class PDCClearReconcileController
    Public Function PDCClearReconList(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCClearReconList As IPDCClearRecon
        oPDCClearReconList = ComponentFactory.CreatePDCClearReconcile
        Return oPDCClearReconList.ListPDCClearRecon(oCustomClass)
    End Function

    Public Function PDCClearReconCList(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCClearReconCList As IPDCClearRecon
        oPDCClearReconCList = ComponentFactory.CreatePDCClearReconcile
        Return oPDCClearReconCList.ListPDCClearCRecon(oCustomClass)
    End Function


    Public Function SavePDCClearRecon(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oSavePDCClearRecon As IPDCClearRecon
        oSavePDCClearRecon = ComponentFactory.CreatePDCClearReconcile
        Return oSavePDCClearRecon.SavePDCClearRecon(oCustomClass)
    End Function

    Public Sub SavePDCClearCRecon(ByVal oCustomClass As Parameter.PDCReceive)
        Dim oSavePDCClearCRecon As IPDCClearRecon
        oSavePDCClearCRecon = ComponentFactory.CreatePDCClearReconcile
        oSavePDCClearCRecon.SavePDCClearCRecon(oCustomClass)
    End Sub
End Class
