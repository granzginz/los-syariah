

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class PDCDepositController

    Public Function PDCDepositList(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCDepositList As IPDCDeposit
        oPDCDepositList = ComponentFactory.CreatePDCDeposit
        Return oPDCDepositList.ListPDCDeposit(oCustomClass)
    End Function

    Public Function SavePDCDeposit(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oSavePDCDeposit As IPDCDeposit
        oSavePDCDeposit = ComponentFactory.CreatePDCDeposit
        Return oSavePDCDeposit.SavePDCDeposit(oCustomClass)
    End Function
    Public Function CheckDupPDCStatus(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oCheckDupPDCStatus As IPDCDeposit
        oCheckDupPDCStatus = ComponentFactory.CreatePDCDeposit
        Return oCheckDupPDCStatus.CheckDupPDCStatus(oCustomClass)
    End Function
End Class
