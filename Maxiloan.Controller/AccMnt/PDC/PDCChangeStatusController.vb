

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class PDCChangeStatusController

    Public Function PDCStatusList(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCStatusList As IPDCStatus
        oPDCStatusList = ComponentFactory.CreatePDCChangeStatus()
        Return oPDCStatusList.ListPDCStatus(oCustomClass)
    End Function

    Public Function PDCStatusListDet(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCStatusListDet As IPDCStatus
        oPDCStatusListDet = ComponentFactory.CreatePDCChangeStatus()
        Return oPDCStatusListDet.ListPDCStatusDet(oCustomClass)
    End Function

    Public Sub PDCSaveStatusDet(ByVal oCustomClass As Parameter.PDCReceive)
        Dim oPDCSaveStatusDet As IPDCStatus
        oPDCSaveStatusDet = ComponentFactory.CreatePDCChangeStatus()
        oPDCSaveStatusDet.SavePDCStatusDet(oCustomClass)
    End Sub
End Class
