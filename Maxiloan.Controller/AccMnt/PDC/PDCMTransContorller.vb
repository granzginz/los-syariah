
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class PDCMTransContorller
    Public Function PDCMTransList(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCReceiveList As IPDCMTrans
        oPDCReceiveList = ComponentFactory.CreatePDCMTrans()
        Return oPDCReceiveList.ListPDCMTrans(oCustomClass)
    End Function
    Public Function GetTablePDC(ByVal oCustomClass As Parameter.PDCReceive, ByVal strFile As String) As Parameter.PDCReceive
        Dim oGetTablePDC As IPDCMTrans
        oGetTablePDC = ComponentFactory.CreatePDCMTrans()
        Return oGetTablePDC.GetTablePDCTrans(oCustomClass, strFile)
    End Function

    Public Function SavePDCTrans(ByVal oCustomClass As Parameter.PDCReceive, ByVal strFile As String) As String
        Dim oSavePDCTrans As IPDCMTrans
        oSavePDCTrans = ComponentFactory.CreatePDCMTrans
        Return oSavePDCTrans.SavePDCTrans(oCustomClass, strFile)
    End Function
    Public Function SavePDCEdit(ByVal oCustomClass As Parameter.PDCReceive, ByVal strFile As String) As String
        Dim oSavePDCEdit As IPDCMTrans
        oSavePDCEdit = ComponentFactory.CreatePDCMTrans
        Return oSavePDCEdit.SavePDCEdit(oCustomClass, strFile)
    End Function
    Public Function GetLastPDCDetail(ByVal oCustomClass As Parameter.PDCReceive, ByVal strFile As String) As Parameter.PDCReceive
        Dim oGetLastPDCDetail As IPDCMTrans
        oGetLastPDCDetail = ComponentFactory.CreatePDCMTrans()
        Return oGetLastPDCDetail.GetLastPDCDetail(oCustomClass, strFile)
    End Function
    Public Function GetTablePDCEdit(ByVal oCustomClass As Parameter.PDCReceive, ByVal strFile As String) As Parameter.PDCReceive
        Dim oGetTablePDCEdit As IPDCMTrans
        oGetTablePDCEdit = ComponentFactory.CreatePDCMTrans()
        Return oGetTablePDCEdit.GetTablePDCEdit(oCustomClass, strFile)
    End Function
    Public Function PrintReceiptNo(ByVal strConnection As String, ByVal PDCReceiptNo As String, ByVal BranchID As String) As DataSet
        Dim oGetTablePDCEdit As IPDCMTrans
        oGetTablePDCEdit = ComponentFactory.CreatePDCMTrans()
        Return oGetTablePDCEdit.PrintReceiptNo(strConnection, PDCReceiptNo, BranchID)
    End Function
    Public Function GetPDCEditListMultiAgrementController(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCEditListMultiAgrement As IPDCMTrans
        oPDCEditListMultiAgrement = ComponentFactory.CreatePDCMTrans()
        Return oPDCEditListMultiAgrement.GetPDCEditListMultiAgrementController(oCustomClass)
    End Function
    Public Function SavePDCReceiveBp(ByVal strConnection As String, ByVal oCustomClass As Parameter.PDCReceive) As String
        Dim oSavePDCReceive As IPDCMTrans
        oSavePDCReceive = ComponentFactory.CreatePDCMTrans()
        Return oSavePDCReceive.SavePDCReceiveBp(strConnection, oCustomClass)
    End Function
End Class
