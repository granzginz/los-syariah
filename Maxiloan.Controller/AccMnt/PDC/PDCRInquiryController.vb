

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class PDCRInquiryController
#Region "PDCRInquiry"
    Public Function PDCRInquiryList(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCRInquiry As IPDCRInquiry
        oPDCRInquiry = ComponentFactory.CreatePDCRInquiry
        Return oPDCRInquiry.ListPDCRInquiry(oCustomClass)
    End Function
    Public Function PDCRInquiryListV(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCRInquiryV As IPDCRInquiry
        oPDCRInquiryV = ComponentFactory.CreatePDCRInquiry
        Return oPDCRInquiryV.ListPDCRInquiryV(oCustomClass)
    End Function
    Public Function PDCRInquiryListDet(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCRInquiryListDet As IPDCRInquiry
        oPDCRInquiryListDet = ComponentFactory.CreatePDCRInquiry
        Return oPDCRInquiryListDet.ListPDCRInquiryDet(oCustomClass)
    End Function

    Public Function PDCInqReport(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCInq As IPDCRInquiry
        oPDCInq = ComponentFactory.CreatePDCRInquiry
        Return oPDCInq.ListPDCInqReport(oCustomClass)
    End Function
#End Region

#Region "PDCRInquiryDetail"
    Public Function PDCInquiryListDetail(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCInquiryListDetail As IPDCRInquiry
        oPDCInquiryListDetail = ComponentFactory.CreatePDCRInquiry
        Return oPDCInquiryListDetail.ListPDCInquiryListDetail(oCustomClass)
    End Function

    Public Function PDCInquiryDetail(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCInquiryDetail As IPDCRInquiry
        oPDCInquiryDetail = ComponentFactory.CreatePDCRInquiry
        Return oPDCInquiryDetail.ListPDCInquiryDetail(oCustomClass)
    End Function
    Public Function PDCInquiryDetailwithGiroSeqNo(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCInquiryDetail As IPDCRInquiry
        oPDCInquiryDetail = ComponentFactory.CreatePDCRInquiry
        Return oPDCInquiryDetail.PDCInquiryDetailwithGiroSeqNo(oCustomClass)
    End Function
#End Region

#Region "PDCInquiryStatus"
    Public Function PDCInquiryStatus(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCInquiryStatus As IPDCRInquiry
        oPDCInquiryStatus = ComponentFactory.CreatePDCRInquiry
        Return oPDCInquiryStatus.ListPDCInquiryStatus(oCustomClass)
    End Function

    Public Function PDCInqStatusReport(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCInqStatus As IPDCRInquiry
        oPDCInqStatus = ComponentFactory.CreatePDCRInquiry
        Return oPDCInqStatus.ListPDCInqStatusReport(oCustomClass)
    End Function

#End Region
#Region "PDCInquiryIncomplete"
    Public Function InqPDCIncomplete(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim oPDCInqIncomplete As IPDCRInquiry
        oPDCInqIncomplete = ComponentFactory.CreatePDCRInquiry
        Return oPDCInqIncomplete.InqPDCIncomplete(oCustomClass)
    End Function
#End Region



End Class
