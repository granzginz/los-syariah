
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class PDCMultiAgreementController
    Public Function ListAgreementCustomer(ByVal oCustomClass As Parameter.PDCMultiAgreement) As Parameter.PDCMultiAgreement
        Dim oPDCReceiveList As IPDCMultiAgreement
        oPDCReceiveList = ComponentFactory.CreatePDCMultiAgreement()
        Return oPDCReceiveList.ListAgreementCustomer(oCustomClass)
    End Function

    Public Function AgreementInformation(ByVal ocustomclass As Parameter.PDCMultiAgreement) As DataTable
        Dim oPDCReceiveList As IPDCMultiAgreement
        oPDCReceiveList = ComponentFactory.CreatePDCMultiAgreement()
        Return oPDCReceiveList.AgreementInformation(ocustomclass)
    End Function

    Public Sub SavePDCMultiAgreement(ByVal oCustomClass As Parameter.PDCMultiAgreement)
        Dim oPDCReceiveList As IPDCMultiAgreement
        oPDCReceiveList = ComponentFactory.CreatePDCMultiAgreement()
        oPDCReceiveList.SavePDCMultiAgreement(oCustomClass)
    End Sub
End Class
