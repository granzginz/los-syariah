
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InventorySellingController
    Public Function GetInvSellingReceive(ByVal customClass As Parameter.InvSelling) As Parameter.InvSelling
        Dim oInvSellingInterface As IInvSelling
        oInvSellingInterface = ComponentFactory.CreateInventorySelling
        Return oInvSellingInterface.GetInvSellingReceive(customClass)
    End Function
    Public Function GetInvSellingReceiveApproval(ByVal customClass As Parameter.InvSelling) As Parameter.InvSelling
        Dim oInvSellingInterface As IInvSelling
        oInvSellingInterface = ComponentFactory.CreateInventorySelling
        Return oInvSellingInterface.GetInvSellingReceiveApproval(customClass)
    End Function
    Function InvSellingReceiveViewRepo(ByVal customclass As Parameter.InvSelling) As Parameter.InvSelling
        Dim oInvSellingInterface As IInvSelling
        oInvSellingInterface = ComponentFactory.CreateInventorySelling
        Return oInvSellingInterface.InvSellingReceiveViewRepo(customclass)
    End Function

    Public Function InvSellingReceiveView(ByVal customclass As Parameter.InvSelling) As Parameter.InvSelling
        Dim oInvSellingInterface As IInvSelling
        oInvSellingInterface = ComponentFactory.CreateInventorySelling
        Return oInvSellingInterface.InvSellingReceiveView(customclass)
    End Function
    Public Function SavingInventorySellingReceive(ByVal oCustomClass As Parameter.InvSelling) As String
        Dim oInvSellingInterface As IInvSelling
        oInvSellingInterface = ComponentFactory.CreateInventorySelling
        Return oInvSellingInterface.SavingInventorySellingReceive(oCustomClass)
    End Function

    Public Function SavingInventorySellingReceiveApproval(ByVal oCustomClass As Parameter.InvSelling) As String
        Dim oInvSellingInterface As IInvSelling
        oInvSellingInterface = ComponentFactory.CreateInventorySelling
        Return oInvSellingInterface.SavingInventorySellingReceiveApproval(oCustomClass)
    End Function

    Sub SavingInventorySellingExec(ByVal oCustomClass As Parameter.InvSelling)
        Dim oInvSellingInterface As IInvSelling
        oInvSellingInterface = ComponentFactory.CreateInventorySelling
        oInvSellingInterface.SavingInventorySellingExec(oCustomClass)
    End Sub
End Class
