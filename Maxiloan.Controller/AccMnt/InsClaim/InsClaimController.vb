
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class InsClaimController
#Region "Insurance Claim Info"
    Public Function InsClaiminfo(ByVal oCustomClass As Parameter.InsClaimReceive) As Parameter.InsClaimReceive
        Dim InsuranceInfo As IInsClaimReceive

        InsuranceInfo = ComponentFactory.CreateInsuranceClaim()
        

        Return InsuranceInfo.InsClaimInfo(oCustomClass)
    End Function
    Public Function SaveInsClaimReceive(ByVal ocustomClass As Parameter.InsClaimReceive) As Parameter.InsClaimReceive
        Dim InsuranceInfo As IInsClaimReceive
        InsuranceInfo = ComponentFactory.CreateInsuranceClaim
        InsuranceInfo.SaveInsClaimReceive(ocustomClass)
    End Function

#End Region

End Class
