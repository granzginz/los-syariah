
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class TransferFundRequestController
    Public Function GetTableTransaction(ByVal oCustomClass As Parameter.TransferFundRequest, ByVal strFile As String) As Parameter.TransferFundRequest
        Dim oGetTable As ITransferFundRequest
        oGetTable = ComponentFactory.CreateTransferFundRequest()
        Return oGetTable.GetTableTransaction(oCustomClass, strFile)
    End Function

    Public Function Get_RequestNo(ByVal customClass As Parameter.TransferFundRequest) As String
        Dim BO As ITransferFundRequest
        BO = ComponentFactory.CreateTransferFundRequest
        Return BO.Get_RequestNo(customClass)
    End Function

    Public Function SavePR(ByVal customClass As Parameter.TransferFundRequest) As String
        Dim PRBO As ITransferFundRequest
        PRBO = ComponentFactory.CreateTransferFundRequest
        Return PRBO.SavePR(customClass)
    End Function
    Public Function CekPR(ByVal customClass As Parameter.TransferFundRequest) As String
        Dim PRBO As ITransferFundRequest
        PRBO = ComponentFactory.CreateTransferFundRequest
        Return PRBO.CekPR(customClass)
    End Function

    Public Function GetTransferFundRequestPaging(ByVal customClass As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
        Dim PRBO As ITransferFundRequest
        PRBO = ComponentFactory.CreateTransferFundRequest
        Return PRBO.GetTransferFundRequestPaging(customClass)
    End Function
    Public Function GetTransferFundRequestHeaderAndDetail(ByVal customClass As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
        Dim PRBO As ITransferFundRequest
        PRBO = ComponentFactory.CreateTransferFundRequest
        Return PRBO.GetTransferFundRequestHeaderAndDetail(customClass)
    End Function
    Public Function GetTransferFundRequestDataset(ByVal customClass As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
        Dim PRBO As ITransferFundRequest
        PRBO = ComponentFactory.CreateTransferFundRequest
        Return PRBO.GetTransferFundRequestDataset(customClass)
    End Function

    Public Function saveApprovalTransferFundRequest(ByVal customClass As Parameter.TransferFundRequest, ByVal strApproval As String) As Parameter.TransferFundRequest
        Dim PRBO As ITransferFundRequest
        PRBO = ComponentFactory.CreateTransferFundRequest
        PRBO.saveApprovalTransferFundRequest(customClass, strApproval)
    End Function

    Public Function GetTransferFundRequestFormKuning(ByVal customClass As IList(Of String), cnn As String) As DataSet
        Dim PRBO As ITransferFundRequest
        PRBO = ComponentFactory.CreateTransferFundRequest
        Return PRBO.GetTransferFundRequestFormKuning(customClass, cnn)
    End Function

    Public Function PayReqStatusReject(ByVal customClass As Parameter.TransferFundRequest, ByVal strApproval As String) As Parameter.TransferFundRequest
        Dim PRBO As ITransferFundRequest
        PRBO = ComponentFactory.CreateTransferFundRequest
        PRBO.PayReqStatusReject(customClass, strApproval)
    End Function
    Public Function PRCOAUpdate(ByVal oCustomClass As Parameter.TransferFundRequest) As String
        Dim oPC As ITransferFundRequest
        oPC = ComponentFactory.CreateTransferFundRequest
        Return oPC.PRCOAUpdate(oCustomClass)
    End Function
    Public Function GetPaymentVoucherByRequestNoPrint(ByVal ocustom As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
        Dim oPC As ITransferFundRequest
        oPC = ComponentFactory.CreateTransferFundRequest
        Return oPC.GetPaymentVoucherByRequestNoPrint(ocustom)
    End Function
    Public Function GetBankAccountOther(ByVal Customclass As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
        Dim oPC As ITransferFundRequest
        oPC = ComponentFactory.CreateTransferFundRequest()
        Return oPC.GetBankAccountOther(Customclass)
    End Function

    Public Function UpdatePR(ByVal customClass As Parameter.TransferFundRequest) As String
        Dim PRBO As ITransferFundRequest
        PRBO = ComponentFactory.CreateTransferFundRequest
        Return PRBO.UpdatePR(customClass)
    End Function

End Class
