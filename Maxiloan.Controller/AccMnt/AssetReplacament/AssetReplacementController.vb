
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class AssetReplacementController
    Public Function AssetReplacementPaging(ByVal oCustomClass As Parameter.AssetReplacement) As Parameter.AssetReplacement
        Dim BO As IAssetReplacement
        BO = ComponentFactory.CreateAssetReplacement
        Return BO.AssetReplacementPaging(oCustomClass)
    End Function
    Public Function GetAssetRepSerial(ByVal oCustomClass As Parameter.AssetReplacement) As Parameter.AssetReplacement
        Dim BO As IAssetReplacement
        BO = ComponentFactory.CreateAssetReplacement
        Return BO.GetAssetRepSerial(oCustomClass)
    End Function

    Public Sub AssetReplacementSaveAdd(ByVal oCustomClass As Parameter.AssetReplacement, _
     ByVal oAddress As Parameter.Address, ByVal oData1 As DataTable, _
     ByVal oData2 As DataTable)
        Dim AssetDataBO As IAssetReplacement
        AssetDataBO = ComponentFactory.CreateAssetReplacement
        AssetDataBO.AssetReplacementSaveAdd(oCustomClass, oAddress, oData1, oData2)
    End Sub
    Public Function AssetRepApproval(ByVal oCustomClass As Parameter.AssetReplacement) As Parameter.AssetReplacement
        Dim BO As IAssetReplacement
        BO = ComponentFactory.CreateAssetReplacement
        Return BO.AssetRepApproval(oCustomClass)
    End Function
    Public Function AssetInquiryReport(ByVal customclass As Parameter.AssetReplacement) As Parameter.AssetReplacement
        Dim BO As IAssetReplacement
        BO = ComponentFactory.CreateAssetReplacement
        Return BO.AssetInquiryReport(customclass)
    End Function
    Public Function ViewAssetReplInquiry(ByVal oCustomClass As Parameter.AssetReplacement) As Parameter.AssetReplacement
        Dim BO As IAssetReplacement
        BO = ComponentFactory.CreateAssetReplacement
        Return BO.ViewAssetReplInquiry(oCustomClass)

    End Function
    Public Sub SaveAssetReplacementCancel(ByVal customClass As Parameter.AssetReplacement)
        Dim AssetDataBO As IAssetReplacement
        AssetDataBO = ComponentFactory.CreateAssetReplacement
        AssetDataBO.SaveAssetReplacementCancel(CustomClass)
    End Sub
    Public Sub SaveAssetReplacementExecute(ByVal customClass As Parameter.AssetReplacement)
        Dim AssetDataBO As IAssetReplacement
        AssetDataBO = ComponentFactory.CreateAssetReplacement
        AssetDataBO.SaveAssetReplacementExecute(CustomClass)
    End Sub
End Class
