
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class ReschedulingController
    Public Function SaveReschedulingData(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable, _
            ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.FinancialData
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        Return BO.SaveReschedulingData(oCustomClass, oData1, oData2, oData3)
    End Function
    Public Function GetMinDueDate(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        Return BO.GetMinDueDate(oCustomClass)
    End Function
    Public Function ViewRescheduling(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        Return BO.ViewRescheduling(oCustomClass)
    End Function
    Public Function ViewReschedulingApproval(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        Return BO.ViewReschedulingApproval(oCustomClass)
    End Function
    Public Sub ReschedulingCancel(ByVal ocustomClass As Parameter.FinancialData)
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        BO.ReschedulingCancel(ocustomClass)
    End Sub
    Public Sub ReschedulingExecute(ByVal ocustomClass As Parameter.FinancialData)
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        BO.ReschedulingExecute(ocustomClass)
    End Sub
    Public Function ReschedulingPrintTrial(ByVal oCustomClass As Parameter.FinancialData) As System.Data.DataSet
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        Return BO.ReschedulingPrintTrial(oCustomClass)
    End Function
    Public Function ReschedulingProduct(ByVal strConnection As String, ByVal BranchId As String) As DataTable
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        Return BO.ReschedulingProduct(strConnection, BranchId)
    End Function
    Public Function SaveReschedulingIRR(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable, _
            ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.FinancialData
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        Return BO.SaveReschedulingIRR(oCustomClass, oData1, oData2, oData3)
    End Function
    Public Function ViewReschedulingIRR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        Return BO.ViewReschedulingIRR(oCustomClass)
    End Function
    Public Function SaveReschedulingStepUpStepDown(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable, _
               ByVal oData2 As DataTable, ByVal oData3 As DataTable, ByVal oData4 As DataTable) As Parameter.FinancialData
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        Return BO.SaveReschedulingStepUpStepDown(oCustomClass, oData1, oData2, oData3, oData4)
    End Function
    Public Function SaveReschedulingEP(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable, _
            ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.FinancialData
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        Return BO.SaveReschedulingEP(oCustomClass, oData1, oData2, oData3)
    End Function
    Public Function GetEffectiveDate(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        Return BO.GetEffectiveDate(oCustomClass)
    End Function
    Public Function SaveReschedulingStepUpStepDownFactAndMDKJ(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable,
               ByVal oData2 As DataTable, ByVal oData3 As DataTable, ByVal oData4 As DataTable) As Parameter.FinancialData
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        Return BO.SaveReschedulingStepUpStepDownFactAndMDKJ(oCustomClass, oData1, oData2, oData3, oData4)
    End Function
    Public Function ViewRestrcutFactAndMU(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        Return BO.ViewRestrcutFactAndMU(oCustomClass)
    End Function
    Public Function ViewApprovalRestrcutFactAndMU(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        Return BO.ViewApprovalRestrcutFactAndMU(oCustomClass)
    End Function
    Public Sub RestructFactAndMUExecute(ByVal ocustomClass As Parameter.FinancialData)
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        BO.RestructFactAndMUExecute(ocustomClass)
    End Sub
    Public Function GetMinDueDateFactAndMDKJ(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        Return BO.GetMinDueDateFactAndMDKJ(oCustomClass)
    End Function
    Public Sub ReschedulingCancelFactAndMDKJ(ByVal ocustomClass As Parameter.FinancialData)
        Dim BO As IRescheduling
        BO = ComponentFactory.CreateRescheduling
        BO.ReschedulingCancelFactAndMDKJ(ocustomClass)
    End Sub
End Class
