﻿
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class ReferalController
    Public Function GetReferal(ByVal ocustomClass As Parameter.Referal) As Parameter.Referal
        Dim ReferalBO As IReferal
        ReferalBO = ComponentFactory.CreateReferal()
        Return ReferalBO.GetReferal(ocustomClass)
    End Function
    Public Function GetReferalReport(ByVal ocustomClass As Parameter.Referal) As Parameter.Referal
        Dim ReferalBO As IReferal
        ReferalBO = ComponentFactory.CreateReferal()
        Return ReferalBO.GetReferalReport(ocustomClass)
    End Function
    Public Function Getsector(ByVal ocustomClass As Parameter.Referal) As Parameter.Referal
        Dim ReferalBO As IReferal
        ReferalBO = ComponentFactory.CreateReferal()
        Return ReferalBO.GetSector(ocustomClass)
    End Function
    Public Function GetReferalEdit(ByVal ocustomClass As Parameter.Referal) As Parameter.Referal
        Dim ReferalBO As IReferal
        ReferalBO = ComponentFactory.CreateReferal()
        Return ReferalBO.GetReferalEdit(ocustomClass)
    End Function

    Public Function ReferalAdd(ByVal ocustomClass As Parameter.Referal) As String
        Dim ReferalBO As IReferal
        ReferalBO = ComponentFactory.CreateReferal()
        Return ReferalBO.ReferalSaveAdd(ocustomClass)
    End Function

    Public Sub ReferalUpdate(ByVal ocustomClass As Parameter.Referal)
        Dim ReferalBO As IReferal
        ReferalBO = ComponentFactory.CreateReferal()
        ReferalBO.ReferalSaveEdit(ocustomClass)
    End Sub

    Public Function ReferalDelete(ByVal ocustomClass As Parameter.Referal) As String
        Dim ReferalBO As IReferal
        ReferalBO = ComponentFactory.CreateReferal()
        Return ReferalBO.ReferalDelete(ocustomClass)
    End Function

End Class

