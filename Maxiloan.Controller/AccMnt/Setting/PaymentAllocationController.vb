Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class PaymentAllocationController
    Public Function GetPaymentAllocation(ByVal oCustomClass As Parameter.PaymentAllocation) As Parameter.PaymentAllocation
        Dim PaymentAllocationBO As IPaymentAllocation
        PaymentAllocationBO = ComponentFactory.CreatePaymentAllocation()
        Return PaymentAllocationBO.GetPaymentAllocation(oCustomClass)
    End Function

    Public Function GetPaymentAllocationID(ByVal strConenction As String, ByVal id As String) As Parameter.PaymentAllocation
        Dim PaymentAllocationBO As IPaymentAllocation
        PaymentAllocationBO = ComponentFactory.CreatePaymentAllocation()
        Return PaymentAllocationBO.GetPaymentAllocationID(strConenction, id)
    End Function

    Public Sub PaymentAllocationEdit(ByVal customClass As Parameter.PaymentAllocation)
        Dim PaymentAllocationBO As IPaymentAllocation
        PaymentAllocationBO = ComponentFactory.CreatePaymentAllocation()
        PaymentAllocationBO.PaymentAllocationEdt(customClass)
    End Sub

    Public Sub PaymentAllocationAdd(ByVal customClass As Parameter.PaymentAllocation)
        Dim PaymentAllocationBO As IPaymentAllocation
        PaymentAllocationBO = ComponentFactory.CreatePaymentAllocation()
        PaymentAllocationBO.PaymentAllocationAdd(customClass)
    End Sub

    Public Sub PaymentAllocationDelete(ByVal customClass As Parameter.PaymentAllocation)
        Dim PaymentAllocationBO As IPaymentAllocation
        PaymentAllocationBO = ComponentFactory.CreatePaymentAllocation()
        PaymentAllocationBO.PaymentAllocationDelete(customClass)
    End Sub

    Public Function GetPaymentAllocationReport(ByVal oCustomClass As Parameter.PaymentAllocation) As Parameter.PaymentAllocation
        Dim PaymentAllocationBO As IPaymentAllocation
        PaymentAllocationBO = ComponentFactory.CreatePaymentAllocation()
        Return PaymentAllocationBO.GetPaymentAllocationReport(oCustomClass)
    End Function
End Class
