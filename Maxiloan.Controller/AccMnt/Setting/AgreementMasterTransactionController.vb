
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class AgreementMasterTransactionController
    Public Function GetAgreementMasterTransaction(ByVal ocustomClass As Parameter.AgreementMasterTransaction) As Parameter.AgreementMasterTransaction
        Dim AgreementMasterTransactionBO As IAgreementMasterTransaction
        AgreementMasterTransactionBO = ComponentFactory.CreateAgreementMasterTransaction()
        Return AgreementMasterTransactionBO.GetAgreementMasterTransaction(ocustomClass)
    End Function
    Public Function GetAgreementMasterTransactionList(ByVal ocustomClass As Parameter.AgreementMasterTransaction) As Parameter.AgreementMasterTransaction
        Dim AgreementMasterTransactionBO As IAgreementMasterTransaction
        AgreementMasterTransactionBO = ComponentFactory.CreateAgreementMasterTransaction()
        Return AgreementMasterTransactionBO.GetAgreementMasterTransactionList(ocustomClass)
    End Function

    Public Function AgreementMasterTransactionSaveAdd(ByVal ocustomClass As Parameter.AgreementMasterTransaction) As String
        Dim AgreementMasterTransactionBO As IAgreementMasterTransaction
        AgreementMasterTransactionBO = ComponentFactory.CreateAgreementMasterTransaction()
        Return AgreementMasterTransactionBO.AgreementMasterTransactionSaveAdd(ocustomClass)
    End Function

    Public Sub AgreementMasterTransactionSaveEdit(ByVal ocustomClass As Parameter.AgreementMasterTransaction)
        Dim AgreementMasterTransactionBO As IAgreementMasterTransaction
        AgreementMasterTransactionBO = ComponentFactory.CreateAgreementMasterTransaction()
        AgreementMasterTransactionBO.AgreementMasterTransactionSaveEdit(ocustomClass)
    End Sub

    Public Function AgreementMasterTransactionDelete(ByVal ocustomClass As Parameter.AgreementMasterTransaction) As String
        Dim AgreementMasterTransactionBO As IAgreementMasterTransaction
        AgreementMasterTransactionBO = ComponentFactory.CreateAgreementMasterTransaction()
        Return AgreementMasterTransactionBO.AgreementMasterTransactionDelete(ocustomClass)
    End Function

End Class
