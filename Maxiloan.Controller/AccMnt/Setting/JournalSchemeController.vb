
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class JournalSchemeController

    Public Function GetJournalScheme(ByVal oCustomClass As Parameter.JournalScheme) As Parameter.JournalScheme
        Dim JournalSchemeBO As IJournalScheme
        JournalSchemeBO = ComponentFactory.CreateJournalScheme()
        Return JournalSchemeBO.GetJournalScheme(oCustomClass)
    End Function

    Public Function GetJournalSchemeAddDtl(ByVal oCustomClass As Parameter.JournalScheme) As Parameter.JournalScheme
        Dim JournalSchemeBO As IJournalScheme
        JournalSchemeBO = ComponentFactory.CreateJournalScheme()
        Return JournalSchemeBO.GetJournalSchemeAddDtl(oCustomClass)
    End Function

    Public Function GetJournalSchemeReport(ByVal oCustomClass As Parameter.JournalScheme) As Parameter.JournalScheme
        Dim JournalSchemeBO As IJournalScheme
        JournalSchemeBO = ComponentFactory.CreateJournalScheme()
        Return JournalSchemeBO.GetJournalSchemeReport(oCustomClass)
    End Function

    Public Function GetJournalSchemeCopyFrom(ByVal oCustomClass As Parameter.JournalScheme) As Parameter.JournalScheme
        Dim JournalSchemeBO As IJournalScheme
        JournalSchemeBO = ComponentFactory.CreateJournalScheme()
        Return JournalSchemeBO.GetJournalSchemeCopyFrom(oCustomClass)
    End Function

    Public Function GetJournalSchemeEditHdr(ByVal oCustomClass As Parameter.JournalScheme) As Parameter.JournalScheme
        Dim JournalSchemeBO As IJournalScheme
        JournalSchemeBO = ComponentFactory.CreateJournalScheme()
        Return JournalSchemeBO.GetJournalSchemeEditHdr(oCustomClass)
    End Function

    Public Function GetJournalSchemeEditDtl(ByVal oCustomClass As Parameter.JournalScheme) As Parameter.JournalScheme
        Dim JournalSchemeBO As IJournalScheme
        JournalSchemeBO = ComponentFactory.CreateJournalScheme()
        Return JournalSchemeBO.GetJournalSchemeEditDtl(oCustomClass)
    End Function

    Public Function JournalSchemeSaveAdd(ByVal oCustomClass As Parameter.JournalScheme) As String
        Dim JournalSchemeBO As IJournalScheme
        JournalSchemeBO = ComponentFactory.CreateJournalScheme()
        Return JournalSchemeBO.JournalSchemeSaveAdd(oCustomClass)
    End Function

    Public Sub JournalSchemeSaveEdit(ByVal customClass As Parameter.JournalScheme)
        Dim JournalSchemeBO As IJournalScheme
        JournalSchemeBO = ComponentFactory.CreateJournalScheme()
        JournalSchemeBO.JournalSchemeSaveEdit(customClass)
    End Sub

    Public Sub JournalSchemeDelete(ByVal customClass As Parameter.JournalScheme)
        Dim JournalSchemeBO As IJournalScheme
        JournalSchemeBO = ComponentFactory.CreateJournalScheme()
        JournalSchemeBO.JournalSchemeDelete(customClass)
    End Sub

End Class
