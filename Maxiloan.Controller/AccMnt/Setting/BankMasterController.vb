

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region

Public Class BankController
#Region "Bank Master"
    Public Function BankMasterList(ByVal oCustomClass As Parameter.BankMaster) As Parameter.BankMaster
        Dim oBankMasterList As IBank
        oBankMasterList = ComponentFactory.CreateBank()
        Return oBankMasterList.ListBankMaster(oCustomClass)
    End Function

    Public Function BankMasterReport(ByVal oCustomClass As Parameter.BankMaster) As Parameter.BankMaster
        Dim oBankMasterList As IBank
        oBankMasterList = ComponentFactory.CreateBank()
        Return oBankMasterList.ReportBankMaster(oCustomClass)
    End Function

    Public Function BankMasterAdd(ByVal oCustomClass As Parameter.BankMaster) As String
        Dim oBankMasterList As IBank
        oBankMasterList = ComponentFactory.CreateBank()
        Return oBankMasterList.AddBankMaster(oCustomClass)
    End Function

    Public Function BankMasterUpdate(ByVal oCustomClass As Parameter.BankMaster) As String
        Dim oBankMasterList As IBank
        oBankMasterList = ComponentFactory.CreateBank()
        Return oBankMasterList.UpdateBankMaster(oCustomClass)
    End Function

    Public Function BankMasterDelete(ByVal oCustomClass As Parameter.BankMaster) As String
        Dim oBankMasterList As IBank
        oBankMasterList = ComponentFactory.CreateBank()
        Return oBankMasterList.DeleteBankMaster(oCustomClass)
    End Function
#End Region

#Region "BANK ACCOUNT"
    Public Function BankAccountList(ByVal oCustomClass As Parameter.BankAccount) As Parameter.BankAccount
        Dim oBankAccountList As IBank
        oBankAccountList = ComponentFactory.CreateBank()
        Return oBankAccountList.ListBankAccount(oCustomClass)
    End Function

    Public Function BankAccountReport(ByVal oCustomClass As Parameter.BankAccount) As Parameter.BankAccount
        Dim oBankAccountList As IBank
        oBankAccountList = ComponentFactory.CreateBank()
        Return oBankAccountList.ReportBankAccount(oCustomClass)
    End Function

    Public Function BankAccountAdd(ByVal customclass As Parameter.BankAccount, _
                                    ByVal oClassAddress As Parameter.Address, _
                                    ByVal oClassPersonal As Parameter.Personal) As String
        Dim oBankAccountAdd As IBank
        oBankAccountAdd = ComponentFactory.CreateBank()
        Return oBankAccountAdd.AddBankAccount(customclass, oClassAddress, oClassPersonal)
    End Function

    Public Function BankAccountUpdate(ByVal customclass As Parameter.BankAccount, _
                                    ByVal oClassAddress As Parameter.Address, _
                                    ByVal oClassPersonal As Parameter.Personal) As String
        Dim oBankAccountUpdate As IBank
        oBankAccountUpdate = ComponentFactory.CreateBank()
        Return oBankAccountUpdate.UpdateBankAccount(customclass, oClassAddress, oClassPersonal)
    End Function

    Public Function BankAccountUpdateH(ByVal customclass As Parameter.BankAccount) As String
        Dim oBankAccountUpdate As IBank
        oBankAccountUpdate = ComponentFactory.CreateBank()
        Return oBankAccountUpdate.UpdateBankAccountH(customclass)
    End Function

    Public Function BankAccountDelete(ByVal oCustomClass As Parameter.BankAccount) As String
        Dim oBankAccountDelete As IBank
        oBankAccountDelete = ComponentFactory.CreateBank()
        Return oBankAccountDelete.DeleteBankAccount(oCustomClass)
    End Function

    Public Function BankAccountInformasi(ByVal strConnection As String, ByVal strBankAccountID As String, ByVal strBranchid As String) As DataTable
        Dim oBankAccountDelete As IBank
        oBankAccountDelete = ComponentFactory.CreateBank()
        Return oBankAccountDelete.BankAccountInformasi(strConnection, strBankAccountID, strBranchid)
    End Function

    Public Function BankAccountListByType(ByVal oCustom As Parameter.BankAccount) As DataTable
        Dim oBankAccountDelete As IBank
        oBankAccountDelete = ComponentFactory.CreateBank()
        Return oBankAccountDelete.BankAccountListByType(oCustom)
    End Function
#End Region

End Class
