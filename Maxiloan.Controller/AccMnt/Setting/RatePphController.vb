

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region

Public Class RatePphController
#Region "Bank Master"
    Public Function RatePphMasterList(ByVal oCustomClass As Parameter.RatePphMaster) As Parameter.RatePphMaster
        Dim oRatePphMasterList As IRatePph
        oRatePphMasterList = ComponentFactory.CreateTarifPph()
        Return oRatePphMasterList.ListRatePphMaster(oCustomClass)
    End Function

    Public Function RatePphMasterAdd(ByVal oCustomClass As Parameter.RatePphMaster) As String
        Dim oRatePphMasterList As IRatePph
        oRatePphMasterList = ComponentFactory.CreateTarifPph()
        Return oRatePphMasterList.AddRatePphMaster(oCustomClass)
    End Function

    Public Function RatePphMasterUpdate(ByVal oCustomClass As Parameter.RatePphMaster) As String
        Dim oRatePphMasterList As IRatePph
        oRatePphMasterList = ComponentFactory.CreateTarifPph()
        Return oRatePphMasterList.UpdateRatePphMaster(oCustomClass)
    End Function

    Public Function RatePphMasterDelete(ByVal oCustomClass As Parameter.RatePphMaster) As String
        Dim oRatePphMasterList As IRatePph
        oRatePphMasterList = ComponentFactory.CreateTarifPph()
        Return oRatePphMasterList.DeleteRatePphMaster(oCustomClass)
    End Function
#End Region

End Class
