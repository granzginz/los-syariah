
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class CashierController
    Public Function ListCashier(ByVal oCustomClass As Parameter.Cashier) As Parameter.Cashier
        Dim CashierBO As ICashier
        CashierBO = ComponentFactory.CreateCashier()
        Return CashierBO.ListCashier(oCustomClass)
    End Function

    Public Sub CashierAdd(ByVal oCustomClass As Parameter.Cashier)
        Dim CashierAddBO As ICashier
        CashierAddBO = ComponentFactory.CreateCashier()
        CashierAddBO.CashierAdd(oCustomClass)
    End Sub

    Public Sub CashierEdit(ByVal oCustomClass As Parameter.Cashier)
        Dim CashierEditBO As ICashier
        CashierEditBO = ComponentFactory.CreateCashier()
        CashierEditBO.CashierEdit(oCustomClass)
    End Sub

    Public Sub CashierDelete(ByVal oCustomClass As Parameter.Cashier)
        Dim CashierUpdateBO As ICashier
        CashierUpdateBO = ComponentFactory.CreateCashier()
        CashierUpdateBO.CashierDelete(oCustomClass)
    End Sub

    Public Function CashierReport(ByVal oCustomClass As Parameter.Cashier) As Parameter.Cashier
        Dim CashierReportBO As ICashier
        CashierReportBO = ComponentFactory.CreateCashier()
        Return CashierReportBO.CashierReport(oCustomClass)
    End Function
End Class
