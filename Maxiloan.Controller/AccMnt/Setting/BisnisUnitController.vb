﻿
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class BisnisUnitController
    Public Function GetBisnisUnit(ByVal ocustomClass As Parameter.BisnisUnit) As Parameter.BisnisUnit
        Dim BisnisUnitBO As IBisnisUnit
        BisnisUnitBO = ComponentFactory.CreateBisnisUnit()
        Return BisnisUnitBO.GetBisnisUnit(ocustomClass)
    End Function
    Public Function GetBisnisUnitReport(ByVal ocustomClass As Parameter.BisnisUnit) As Parameter.BisnisUnit
        Dim BisnisUnitBO As IBisnisUnit
        BisnisUnitBO = ComponentFactory.CreateBisnisUnit()
        Return BisnisUnitBO.GetBisnisUnitReport(ocustomClass)
    End Function
    Public Function Getsector(ByVal ocustomClass As Parameter.BisnisUnit) As Parameter.BisnisUnit
        Dim BisnisUnitBO As IBisnisUnit
        BisnisUnitBO = ComponentFactory.CreateBisnisUnit()
        Return BisnisUnitBO.GetSector(ocustomClass)
    End Function
    Public Function GetBisnisUnitEdit(ByVal ocustomClass As Parameter.BisnisUnit) As Parameter.BisnisUnit
        Dim BisnisUnitBO As IBisnisUnit
        BisnisUnitBO = ComponentFactory.CreateBisnisUnit()
        Return BisnisUnitBO.GetBisnisUnitEdit(ocustomClass)
    End Function

    Public Function BisnisUnitAdd(ByVal ocustomClass As Parameter.BisnisUnit) As String
        Dim BisnisUnitBO As IBisnisUnit
        BisnisUnitBO = ComponentFactory.CreateBisnisUnit()
        Return BisnisUnitBO.BisnisUnitSaveAdd(ocustomClass)
    End Function

    Public Sub BisnisUnitUpdate(ByVal ocustomClass As Parameter.BisnisUnit)
        Dim BisnisUnitBO As IBisnisUnit
        BisnisUnitBO = ComponentFactory.CreateBisnisUnit()
        BisnisUnitBO.BisnisUnitSaveEdit(ocustomClass)
    End Sub

    Public Function BisnisUnitDelete(ByVal ocustomClass As Parameter.BisnisUnit) As String
        Dim BisnisUnitBO As IBisnisUnit
        BisnisUnitBO = ComponentFactory.CreateBisnisUnit()
        Return BisnisUnitBO.BisnisUnitDelete(ocustomClass)
    End Function

End Class

