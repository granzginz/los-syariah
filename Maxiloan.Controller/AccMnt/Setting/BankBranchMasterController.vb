
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class BankBranchMasterController
    Public Function GetBankBranchMaster(ByVal ocustomClass As Parameter.BankBranchMaster) As Parameter.BankBranchMaster
        Dim BankBranchMasterBO As IBankBranchMaster
        BankBranchMasterBO = ComponentFactory.CreateBankBranchMaster()
        Return BankBranchMasterBO.GetBankBranchMaster(ocustomClass)
    End Function
    Public Function GetBankBranchMasterReport(ByVal ocustomClass As Parameter.BankBranchMaster) As Parameter.BankBranchMaster
        Dim BankBranchMasterBO As IBankBranchMaster
        BankBranchMasterBO = ComponentFactory.CreateBankBranchMaster()
        Return BankBranchMasterBO.GetBankBranchMasterReport(ocustomClass)
    End Function
    Public Function GetBankBranchMasterList(ByVal ocustomClass As Parameter.BankBranchMaster) As Parameter.BankBranchMaster
        Dim BankBranchMasterBO As IBankBranchMaster
        BankBranchMasterBO = ComponentFactory.CreateBankBranchMaster()
        Return BankBranchMasterBO.GetBankBranchMasterList(ocustomClass)
    End Function

    Public Function BankBranchMasterSaveAdd(ByVal ocustomClass As Parameter.BankBranchMaster) As String
        Dim BankBranchMasterBO As IBankBranchMaster
        BankBranchMasterBO = ComponentFactory.CreateBankBranchMaster()
        Return BankBranchMasterBO.BankBranchMasterSaveAdd(ocustomClass)
    End Function

    Public Sub BankBranchMasterSaveEdit(ByVal ocustomClass As Parameter.BankBranchMaster)
        Dim BankBranchMasterBO As IBankBranchMaster
        BankBranchMasterBO = ComponentFactory.CreateBankBranchMaster()
        BankBranchMasterBO.BankBranchMasterSaveEdit(ocustomClass)
    End Sub

    Public Function BankBranchMasterDelete(ByVal ocustomClass As Parameter.BankBranchMaster) As String
        Dim BankBranchMasterBO As IBankBranchMaster
        BankBranchMasterBO = ComponentFactory.CreateBankBranchMaster()
        Return BankBranchMasterBO.BankBranchMasterDelete(ocustomClass)
    End Function

End Class
