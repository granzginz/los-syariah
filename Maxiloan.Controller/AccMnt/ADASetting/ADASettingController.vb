

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class ADASettingController
    Public Function UpdateADASetting(ByVal oCustomClass As Parameter.ADASetting) As Boolean
        Dim BO As IADASetting
        BO = ComponentFactory.CreateADASetting()
        Return BO.UpdateADASetting(oCustomClass)
    End Function
End Class
