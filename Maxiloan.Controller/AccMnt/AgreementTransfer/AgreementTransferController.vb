

#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class AgreementTransferController
    Public Function AssetDocPaging(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim oDList As IAgreementTransfer
        oDList = ComponentFactory.CreateAgreementTransfer
        Return oDList.AssetDocPaging(oCustomClass)
    End Function
    Public Function GetCust(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim oCust As IAgreementTransfer
        oCust = ComponentFactory.CreateAgreementTransfer
        Return oCust.GetCust(oCustomClass)
    End Function

    Public Sub AgreementTransferReq(ByVal oCustomClass As Parameter.DChange)
        Dim ATransR As IAgreementTransfer
        ATransR = ComponentFactory.CreateAgreementTransfer
        ATransR.AgreementTransferReq(oCustomClass)
    End Sub

    Public Function GetListExecAT(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim oListE As IAgreementTransfer
        oListE = ComponentFactory.CreateAgreementTransfer
        Return oListE.GetListExecAT(oCustomClass)
    End Function

    Public Function GetATTC(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim oListE As IAgreementTransfer
        oListE = ComponentFactory.CreateAgreementTransfer
        Return oListE.GetATTC(oCustomClass)
    End Function

    Public Function GetATTC2(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
        Dim oListE As IAgreementTransfer
        oListE = ComponentFactory.CreateAgreementTransfer
        Return oListE.GetATTC2(oCustomClass)
    End Function

    Public Sub ATExec(ByVal oCustomClass As Parameter.DChange)
        Dim ATransR As IAgreementTransfer
        ATransR = ComponentFactory.CreateAgreementTransfer
        ATransR.ATExec(oCustomClass)
    End Sub
    Function PrintEndorsAgreementTransfer(ByVal customClass As Parameter.DChange) As String
        Dim ATransR As IAgreementTransfer
        ATransR = ComponentFactory.CreateAgreementTransfer
        Return ATransR.PrintEndorsAgreementTransfer(customClass)
    End Function
End Class
