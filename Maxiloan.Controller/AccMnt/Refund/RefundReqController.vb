

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class RefundReqController

    Public Function RefundReqList(ByVal oCustomClass As Parameter.Refund) As Parameter.Refund
        Dim oRefundReqList As IRefundReq
        oRefundReqList = ComponentFactory.CreateRefundRequest
        Return oRefundReqList.ListRefundReq(oCustomClass)
    End Function

    Public Function SaveRefundReq(ByVal oCustomClass As Parameter.Refund) As Parameter.Refund
        Dim oRefundReq As IRefundReq
        oRefundReq = ComponentFactory.CreateRefundRequest
        Return oRefundReq.SaveRefundReq(oCustomClass)
    End Function

    Public Function SaveRefundReqH(ByVal oCustomClass As Parameter.Refund) As Parameter.Refund
        Dim oRefundReqH As IRefundReq
        oRefundReqH = ComponentFactory.CreateRefundRequest
        Return oRefundReqH.SaveRefundReqH(oCustomClass)
    End Function

    Public Function RefundReqListCust(ByVal oCustomClass As Parameter.Refund) As Parameter.Refund
        Dim oRefundReqH As IRefundReq
        oRefundReqH = ComponentFactory.CreateRefundRequest
        Return oRefundReqH.ListRefundReqCust(oCustomClass)
    End Function


End Class
