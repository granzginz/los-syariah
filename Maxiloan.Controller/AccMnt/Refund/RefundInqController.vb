
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class RefundInqController
    Public Function RefundInqListReport(ByVal oCustomClass As Parameter.Refund) As Parameter.Refund
        Dim oRefundInqList As IRefundInq
        oRefundInqList = ComponentFactory.CreateRefundInquiry
        Return oRefundInqList.ListRefundReport(oCustomClass)
    End Function
    Public Function ViewRefund(ByVal oCustomClass As Parameter.Refund) As Parameter.Refund
        Dim oViewRefund As IRefundInq
        oViewRefund = ComponentFactory.CreateRefundInquiry
        Return oViewRefund.ViewRefund(oCustomClass)
    End Function

    Public Function ViewRefundApproval(ByVal oCustomClass As Parameter.Refund) As Parameter.Refund
        Dim oViewRefund As IRefundInq
        oViewRefund = ComponentFactory.CreateRefundInquiry
        Return oViewRefund.ViewRefundApproval(oCustomClass)
    End Function
End Class
