

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class RefundReqAdvController
    '============================= Request ====================================
#Region "RefundReqListAdv"
    Public Function RefundReqListAdv(ByVal oCustomClass As Parameter.Refund) As Parameter.Refund
        Dim oRefundReqListAdv As IRefundReqAdv
        oRefundReqListAdv = ComponentFactory.CreateRefundRequestAdvance
        Return oRefundReqListAdv.ListRefundReqAdv(oCustomClass)
    End Function
#End Region
#Region "SelectedRefundReqAdv"
    Public Function SelectedRefundReqAdv(ByVal oCustomClass As Parameter.Refund) As Parameter.Refund
        Dim oSelectedRefundReqAdv As IRefundReqAdv
        oSelectedRefundReqAdv = ComponentFactory.CreateRefundRequestAdvance
        Return oSelectedRefundReqAdv.SelectedRefundReqAdv(oCustomClass)
    End Function
#End Region
#Region "GetProduct"
    Public Function GetProduct(ByVal oCustomClass As Parameter.Refund) As Parameter.Refund
        Dim oSelectedRefundReqAdv As IRefundReqAdv
        oSelectedRefundReqAdv = ComponentFactory.CreateRefundRequestAdvance
        Return oSelectedRefundReqAdv.GetProduct(oCustomClass)
    End Function
#End Region
#Region "SaveRefundReqAdv"
    Public Function SaveRefundReqAdv(ByVal oCustomClass As Parameter.Refund, ByVal oData1 As DataTable) As Parameter.Refund
        Dim oSelectedRefundReqAdv As IRefundReqAdv
        oSelectedRefundReqAdv = ComponentFactory.CreateRefundRequestAdvance
        Return oSelectedRefundReqAdv.SaveRefundReqAdv(oCustomClass, oData1)
    End Function
#End Region

    '============================= Inquiry ====================================
#Region "RefundInqListAdv"
    Public Function RefundInqListAdv(ByVal oCustomClass As Parameter.Refund) As Parameter.Refund
        Dim oRefundReqListAdv As IRefundReqAdv
        oRefundReqListAdv = ComponentFactory.CreateRefundRequestAdvance
        Return oRefundReqListAdv.ListRefundInqAdv(oCustomClass)
    End Function
#End Region
#Region "RefundInqListAdvByOSRefund"
    Public Function RefundInqListAdvByOSRefund(ByVal oCustomClass As Parameter.Refund) As Parameter.Refund
        Dim oRefundReqListAdv As IRefundReqAdv
        oRefundReqListAdv = ComponentFactory.CreateRefundRequestAdvance
        Return oRefundReqListAdv.ListRefundInqAdvByOSRefund(oCustomClass)
    End Function
#End Region
#Region "ListingReportByRefund"
    Public Function ListingReportByRefund(ByVal oCustomClass As Parameter.Refund) As Parameter.Refund
        Dim oRefundReqListAdv As IRefundReqAdv
        oRefundReqListAdv = ComponentFactory.CreateRefundRequestAdvance
        Return oRefundReqListAdv.ListingReportByRefund(oCustomClass)
    End Function
#End Region
#Region "ListingReportByOSRefund"
    Public Function ListingReportByOSRefund(ByVal oCustomClass As Parameter.Refund) As Parameter.Refund
        Dim oRefundReqListAdv As IRefundReqAdv
        oRefundReqListAdv = ComponentFactory.CreateRefundRequestAdvance
        Return oRefundReqListAdv.ListingReportByOSRefund(oCustomClass)
    End Function
#End Region
#Region "ViewRefundAdvance1"
    Public Function ViewRefundAdvance1(ByVal oCustomClass As Parameter.Refund) As Parameter.Refund
        Dim oRefundReqListAdv As IRefundReqAdv
        oRefundReqListAdv = ComponentFactory.CreateRefundRequestAdvance
        Return oRefundReqListAdv.ViewRefundAdvance1(oCustomClass)
    End Function
#End Region
#Region "ViewRefundAdvance2"
    Public Function ViewRefundAdvance2(ByVal oCustomClass As Parameter.Refund) As Parameter.Refund
        Dim oRefundReqListAdv As IRefundReqAdv
        oRefundReqListAdv = ComponentFactory.CreateRefundRequestAdvance
        Return oRefundReqListAdv.ViewRefundAdvance2(oCustomClass)
    End Function
#End Region
#Region "ViewRefundDetail"
    Public Function ViewRefundDetail(ByVal oCustomClass As Parameter.Refund) As Parameter.Refund
        Dim oRefundReqListAdv As IRefundReqAdv
        oRefundReqListAdv = ComponentFactory.CreateRefundRequestAdvance
        Return oRefundReqListAdv.ViewRefundDetail(oCustomClass)
    End Function
#End Region
End Class
