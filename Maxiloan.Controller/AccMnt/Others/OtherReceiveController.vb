

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class OtherReceiveController


    Public Function GetTable(ByVal oCustomClass As Parameter.OtherReceive, ByVal strFile As String) As Parameter.OtherReceive
        Dim oGetTable As IOtherReceive
        oGetTable = ComponentFactory.CreateOtherReceive
        Return oGetTable.GetTableTrans(oCustomClass, strFile)
    End Function

    Public Function SaveTrans(ByVal oCustomClass As Parameter.OtherReceive, ByVal strFile As String) As Parameter.OtherReceive
        Dim oSaveTrans As IOtherReceive
        oSaveTrans = ComponentFactory.CreateOtherReceive
        Return oSaveTrans.SaveTrans(oCustomClass, strFile)
    End Function
End Class
