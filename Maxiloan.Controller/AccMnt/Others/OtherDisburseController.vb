

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class OtherDisburseController
    Public Function GetTable(ByVal oCustomClass As Parameter.OtherReceive, ByVal strFile As String) As Parameter.OtherReceive
        Dim oGetTable As IOtherDisburse
        oGetTable = ComponentFactory.CreateOtherDisburse
        Return oGetTable.GetTableTrans(oCustomClass, strFile)
    End Function

    Public Function SaveTrans(ByVal oCustomClass As Parameter.OtherReceive, ByVal strFile As String) As String
        Dim oSaveTrans As IOtherDisburse
        oSaveTrans = ComponentFactory.CreateOtherDisburse
        Return oSaveTrans.SaveTrans(oCustomClass, strFile)
    End Function

    Public Function OtherDisburseAdd(ByVal customclass As Parameter.OtherReceive, ByVal strFile As String) As Parameter.OtherReceive
        Dim oInterface As IOtherDisburse
        oInterface = ComponentFactory.CreateOtherDisburse
        Return oInterface.OtherDisburseAdd(customclass, strFile)
    End Function

End Class
