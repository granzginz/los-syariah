
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class OtherDisburseAgreementRelatedController

    Public Sub OtherDisburseAgreementRelatedSaved(ByVal oCustomClass As Parameter.OtherDisburseAgreement)
        Dim oSaveTrans As IOtherDisburseAgreementRelated
        oSaveTrans = ComponentFactory.CreateOtherDisburseAgreementRelated()
        oSaveTrans.OtherDisburseAgreementRelatedSaved(oCustomClass)
    End Sub

    Public Function GetTableOtherDisburseAgreementRelated(ByVal oCustomClass As Parameter.OtherDisburseAgreement, ByVal strFile As String) As Parameter.OtherDisburseAgreement
        Dim oGetTable As IOtherDisburseAgreementRelated
        oGetTable = ComponentFactory.CreateOtherDisburseAgreementRelated
        Return oGetTable.GetTableOtherDisburseAgreementRelated(oCustomClass, strFile)
    End Function

End Class
