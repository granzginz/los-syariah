

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class TransferFundReconcileController
    Public Function TransferList(ByVal oCustomClass As Parameter.TransferAccount) As Parameter.TransferAccount
        Dim oListT As ITransferFundReconcile
        oListT = ComponentFactory.CreateTransferFundReconcile
        Return oListT.ListTransfer(oCustomClass)
    End Function

    Public Function TransferSave(ByVal oCustomClass As Parameter.TransferAccount) As Parameter.TransferAccount
        Dim oSaveT As ITransferFundReconcile
        oSaveT = ComponentFactory.CreateTransferFundReconcile
        Return oSaveT.SaveTransfer(oCustomClass)
    End Function
End Class
