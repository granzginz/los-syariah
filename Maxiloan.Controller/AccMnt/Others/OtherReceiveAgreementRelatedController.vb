
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class OtherReceiveAgreementRelatedController
    Public Sub OtherReceiveAgreementRelatedSaved(ByVal oCustomClass As Parameter.OtherReceiveAgreement)
        Dim oSaveTrans As IOtherReceiveAgreementRelated
        oSaveTrans = ComponentFactory.CreateOtherReceiveAgreementRelated()
        oSaveTrans.OtherReceiveAgreementRelatedSaved(oCustomClass)
    End Sub

    Public Function GetTableOtherReceiveAgreementRelated(ByVal oCustomClass As Parameter.OtherReceiveAgreement, ByVal strFile As String) As Parameter.OtherReceiveAgreement
        Dim oGetTable As IOtherReceiveAgreementRelated
        oGetTable = ComponentFactory.CreateOtherReceiveAgreementRelated
        Return oGetTable.GetTableOtherReceiveAgreementRelated(oCustomClass, strFile)
    End Function
End Class