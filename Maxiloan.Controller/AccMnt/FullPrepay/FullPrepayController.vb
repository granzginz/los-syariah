

#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class FullPrepayController

#Region "Full Prepay Request"
    Public Sub FullPrepayRequest(ByVal oCustomClass As Parameter.FullPrepay)
        Dim PaymentInfoBO As IFullPrepay
        PaymentInfoBO = ComponentFactory.CreatePrepayment
        PaymentInfoBO.FullPrepayRequest(oCustomClass)
    End Sub
    Public Sub FullPrepayRequestOL(ByVal oCustomClass As Parameter.FullPrepay)
        Dim PaymentInfoBO As IFullPrepay
        PaymentInfoBO = ComponentFactory.CreatePrepayment
        PaymentInfoBO.FullPrepayRequestOL(oCustomClass)
    End Sub
#End Region

#Region "Full Prepay Cancel"
    Public Sub FullPrepayCancel(ByVal oCustomClass As Parameter.FullPrepay)
        Dim FullPrepayCancelationBo As IFullPrepay
        FullPrepayCancelationBo = ComponentFactory.CreatePrepayment
        FullPrepayCancelationBo.FullPrepayCancel(oCustomClass)
    End Sub
#End Region

#Region "Full Prepay Execution"
    Public Sub FullPrepayExecution(ByVal oCustomClass As Parameter.FullPrepay)
        Dim PaymentInfoBO As IFullPrepay
        PaymentInfoBO = ComponentFactory.CreatePrepayment
        PaymentInfoBO.FullPrepayExecution(oCustomClass)
    End Sub
    Public Sub FullPrepayExecutionOL(ByVal oCustomClass As Parameter.FullPrepay)
        Dim PaymentInfoBO As IFullPrepay
        PaymentInfoBO = ComponentFactory.CreatePrepayment
        PaymentInfoBO.FullPrepayExecutionOL(oCustomClass)
    End Sub
#End Region

#Region "Full Prepay Execution"
    Public Function FullPrepayInfo(ByVal oCustomClass As Parameter.FullPrepay) As Parameter.FullPrepay
        Dim PaymentInfoBO As IFullPrepay
        PaymentInfoBO = ComponentFactory.CreatePrepayment
        Return PaymentInfoBO.FullPrepayInfo(oCustomClass)
    End Function
#End Region
#Region "Prepayment Inquiry"
    Public Function InqPrepayment(ByVal ocustomclass As Parameter.FullPrepay) As Parameter.FullPrepay
        Dim Prepayment As IFullPrepay
        Prepayment = ComponentFactory.CreatePrepayment
        Return Prepayment.InqPrepayment(ocustomclass)
    End Function
#End Region

#Region "Full Prepay Print Trial "
    Public Function PrintTrial(ByVal ocustomclass As Parameter.FullPrepay) As DataSet
        Dim Prepayment As IFullPrepay
        Prepayment = ComponentFactory.CreatePrepayment
        Return Prepayment.PrintTrial(ocustomclass)
    End Function
    Public Function PrintTrialOL(ByVal ocustomclass As Parameter.FullPrepay) As DataSet
        Dim Prepayment As IFullPrepay
        Prepayment = ComponentFactory.CreatePrepayment
        Return Prepayment.PrintTrialOL(ocustomclass)
    End Function
#End Region
End Class
