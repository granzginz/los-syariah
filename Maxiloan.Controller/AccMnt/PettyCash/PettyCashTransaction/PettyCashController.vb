
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class PettyCashController

#Region "Constants"
    Private STR_ERR_MSG_UNABLE_TO_CREATE_COMPONENT As String = "Unable to create COPettyCash component."
#End Region
#Region "GetARecord"


    Public Function GetARecord(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim PettyCashBO As IPettyCash
        Try
            PettyCashBO = ComponentFactory.CreatePettyCash()
            Return PettyCashBO.GetARecord(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region
#Region "GetPagingTable"

    Public Function GetPagingTable(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim PettyCashBO As IPettyCash
        Try
            PettyCashBO = ComponentFactory.CreatePettyCash()
            Return PettyCashBO.GetPagingTable(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region

#Region "GetARecordAndDetailTable"


    Public Function GetARecordAndDetailTable(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim PettyCashBO As IPettyCash
        Try
            PettyCashBO = ComponentFactory.CreatePettyCash()
            Return PettyCashBO.GetARecordAndDetailTable(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

#Region "GetARecordAndDetailTableACC"


    Public Function GetARecordAndDetailTableACC(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim PettyCashBO As IPettyCash
        Try
            PettyCashBO = ComponentFactory.CreatePettyCash()
            Return PettyCashBO.GetARecordAndDetailTableACC(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region
#Region "GetDetailTable"

    Public Function GetDetailTable(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim PettyCashBO As IPettyCash
        Try
            PettyCashBO = ComponentFactory.CreatePettyCash()
            Return PettyCashBO.GetDetailTable(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region
#Region "GetReportDataSet"


    Public Function GetReportDataSet(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim PettyCashBO As IPettyCash
        Try
            PettyCashBO = ComponentFactory.CreatePettyCash()
            Return PettyCashBO.GetReportDataSet(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
#Region "GetTablePC"


    Public Function GetTablePC(ByVal oET As Parameter.PettyCash, ByVal strfile As String) As Parameter.PettyCash

        Dim oPC As IPettyCashTrans
        oPC = ComponentFactory.CreatePettyCashTrans()
        Return oPC.GetTablePC(oET, strfile)

        'Try
        '    Dim oPC As IPettyCashTrans
        '    Return oPC.GetTablePC(oET, file)
        'Catch ex As Exception
        '    Throw ex
        'End Try
    End Function

#End Region
#Region "PCTransSave"

    Public Sub PCTransSave(ByVal oET As Parameter.PettyCash, ByVal strfile As String)
        Dim oPC As IPettyCashTrans
        oPC = ComponentFactory.CreatePettyCashTrans()
        oPC.SavePCTrans(oET, strfile)

        'Try
        '    Dim oPC As IPettyCashTrans
        '    oPC.SavePCTrans(oET, strfile)
        'Catch ex As Exception
        '    Throw ex
        'End Try
    End Sub

#End Region
#Region "PCReimburseSave"
    Public Function PCReimburseSave(ByVal oCustomClass As Parameter.PettyCash) As Parameter.PettyCash
        Dim oPC As IPettyCashTrans
        oPC = ComponentFactory.CreatePettyCashTrans
        Return oPC.SavePCReimburse(oCustomClass)
    End Function

    Public Function PCreimburseStatusUpdate(ByVal oCustomClass As Parameter.PettyCash) As String
        Dim oPC As IPettyCashTrans
        oPC = ComponentFactory.CreatePettyCashTrans
        Return oPC.PCreimburseStatusUpdate(oCustomClass)
    End Function
    Public Function PCreimburseStatusUpdateACC(ByVal oCustomClass As Parameter.PettyCash) As String
        Dim oPC As IPettyCashTrans
        oPC = ComponentFactory.CreatePettyCashTrans
        Return oPC.PCreimburseStatusUpdateACC(oCustomClass)
    End Function
    Public Function PCreimburseStatusUpdateACCReject(ByVal oCustomClass As Parameter.PettyCash) As String
        Dim oPC As IPettyCashTrans
        oPC = ComponentFactory.CreatePettyCashTrans
        Return oPC.PCreimburseStatusUpdateACCReject(oCustomClass)
    End Function
#End Region
#Region "PCReimburseReconcile "
    Public Function PCReimburseReconcile(ByVal customClass As Parameter.PettyCashReconcile) As Parameter.PettyCashReconcile
        Dim BO As IPettyCashTrans
        BO = ComponentFactory.CreatePettyCashTrans()
        Return BO.PCReimburseReconcile(customClass)
    End Function
    Public Function GetPettyCashFormKuning(ByVal customClass As IList(Of String), cnn As String) As DataSet
        Dim BO As IPettyCashTrans
        BO = ComponentFactory.CreatePettyCashTrans()
        Return BO.GetPettyCashFormKuning(customClass, cnn)
    End Function
#End Region
#Region "InqPCReimburse"
    Public Function InqPCReimburse(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim PCReimburse As IPettyCash
        PCReimburse = ComponentFactory.CreatePettyCash()
        Return PCReimburse.InqPCReimburse(ocustomclass)
    End Function
    Public Function InqPCReimburseACC(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim PCReimburse As IPettyCash
        PCReimburse = ComponentFactory.CreatePettyCash()
        Return PCReimburse.InqPCReimburseACC(ocustomclass)
    End Function
#End Region
#Region "ViewPCReimburse"
    Public Function getViewPCReimburseLabel(ByVal oCustomClass As Parameter.PettyCash) As Parameter.PettyCash
        Dim PCReimburseLabel As IPettyCash
        PCReimburseLabel = ComponentFactory.CreatePettyCash()
        Return PCReimburseLabel.getViewPCReimburseLabel(oCustomClass)
    End Function
    Public Function GetViewPCReimburseGrid(ByVal oCustomClass As Parameter.PettyCash) As Parameter.PettyCash
        Dim PCReimburseGrid As IPettyCash
        PCReimburseGrid = ComponentFactory.CreatePettyCash()
        Return PCReimburseGrid.GetViewPCReimburseGrid(oCustomClass)
    End Function
    Public Function GetViewPCReimburseGridACC(ByVal oCustomClass As Parameter.PettyCash) As Parameter.PettyCash
        Dim PCReimburseGrid As IPettyCash
        PCReimburseGrid = ComponentFactory.CreatePettyCash()
        Return PCReimburseGrid.GetViewPCReimburseGridACC(oCustomClass)
    End Function
#End Region
    Public Sub SavePettyCashReversal(ByVal oET As Parameter.PettyCash)
        Dim PettyCashBO As IPettyCash
        Try
            PettyCashBO = ComponentFactory.CreatePettyCash()
            PettyCashBO.SavePettyCashReversal(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub EditCOAPettyCash(ByVal oET As Parameter.PettyCash)
        Dim PettyCashBO As IPettyCash
        Try
            PettyCashBO = ComponentFactory.CreatePettyCash()
            PettyCashBO.EditCOAPettyCash(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "Print"
    Public Function GetPettyCashVoucher(ByVal oCustomClass As Parameter.PettyCash) As DataSet
        Dim PettyCashBO As IPettyCash
        PettyCashBO = ComponentFactory.CreatePettyCash()
        Return PettyCashBO.GetPettyCashVoucher(oCustomClass)
    End Function



#End Region

#Region "Approval"
    Public Function AprPCReimburse(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim PCReimburse As IPettyCash
        PCReimburse = ComponentFactory.CreatePettyCash()
        Return PCReimburse.AprPCReimburse(ocustomclass)
    End Function

    Public Function AprPCReimburseisFinal(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim PCReimburse As IPettyCash
        PCReimburse = ComponentFactory.CreatePettyCash()
        Return PCReimburse.AprPCReimburseisFinal(ocustomclass)
    End Function

    Public Function AprPCReimburseIsValidApproval(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim PCReimburse As IPettyCash
        PCReimburse = ComponentFactory.CreatePettyCash()
        Return PCReimburse.AprPCReimburseIsValidApproval(ocustomclass)
    End Function

    Public Function GetCboUserAprPC(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim PCReimburse As IPettyCash
        PCReimburse = ComponentFactory.CreatePettyCash()
        Return PCReimburse.GetCboUserAprPC(ocustomclass)
    End Function

    Public Function PCAprReimburseSave(ByVal ocustomclass As Parameter.PettyCash) As String
        Dim PCReimburse As IPettyCash
        PCReimburse = ComponentFactory.CreatePettyCash()
        Return PCReimburse.PCAprReimburseSave(ocustomclass)
    End Function

    Public Function PCAprReimburseSaveToNextPerson(ByVal ocustomclass As Parameter.PettyCash) As String
        Dim PCReimburse As IPettyCash
        PCReimburse = ComponentFactory.CreatePettyCash()
        Return PCReimburse.PCAprReimburseSaveToNextPerson(ocustomclass)
    End Function

    Public Function GetCboUserAprPCAll(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash
        Dim PCReimburse As IPettyCash
        PCReimburse = ComponentFactory.CreatePettyCash()
        Return PCReimburse.GetCboUserAprPCAll(ocustomclass)
    End Function

    Public Function GetARecordAndDetailTableACCWithSeq(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim PettyCashBO As IPettyCash
        Try
            PettyCashBO = ComponentFactory.CreatePettyCash()
            Return PettyCashBO.GetARecordAndDetailTableACCWithSeq(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetARecordAndDetailTableWithSeq(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
        Dim PettyCashBO As IPettyCash
        Try
            PettyCashBO = ComponentFactory.CreatePettyCash()
            Return PettyCashBO.GetARecordAndDetailTableWithSeq(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetViewPCReimburseGridHistoryReject(ByVal oCustomClass As Parameter.PettyCash) As Parameter.PettyCash
        Dim PCReimburseGrid As IPettyCash
        PCReimburseGrid = ComponentFactory.CreatePettyCash()
        Return PCReimburseGrid.GetViewPCReimburseGridHistoryReject(oCustomClass)
    End Function

    Public Function PCreimburseStatusReject(ByVal oCustomClass As Parameter.PettyCash) As String
        Dim oPC As IPettyCashTrans
        oPC = ComponentFactory.CreatePettyCashTrans
        Return oPC.PCreimburseStatusReject(oCustomClass)
    End Function
    Public Function PCreimburseCOAUpdate(ByVal oCustomClass As Parameter.PettyCash) As String
        Dim oPC As IPettyCashTrans
        oPC = ComponentFactory.CreatePettyCashTrans
        Return oPC.PCreimburseCOAUpdate(oCustomClass)
    End Function
#End Region
End Class


