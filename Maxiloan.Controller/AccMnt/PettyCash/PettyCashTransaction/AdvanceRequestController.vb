
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess


Public Class AdvanceRequestController
#Region "PrivateConst"
    Private STR_ERR_MSG_UNABLE_TO_CREATE_COMPONENT As String = "Unable to create COAdvanceRequest component."
#End Region
    Public Sub SaveAdvanceTransaction(ByVal oAdvanceRequest As Parameter.AdvanceRequest)
        Dim oIAdvReq As IAdvanceRequest

        Try
            oIAdvReq = ComponentFactory.CreateAdvanceRequest()
            If oIAdvReq Is Nothing Then
                Throw New Exception(STR_ERR_MSG_UNABLE_TO_CREATE_COMPONENT)
            End If

            oIAdvReq.SaveAdvanceTransaction(oAdvanceRequest)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Function GetPettyCashAdvanceList(ByVal oETAdvReq As AdvanceRequest) As AdvanceRequest
        Dim oIAdvReq As IAdvanceRequest

        Try
            oIAdvReq = ComponentFactory.CreateAdvanceRequest()
            If oIAdvReq Is Nothing Then
                Throw New Exception(STR_ERR_MSG_UNABLE_TO_CREATE_COMPONENT)
            End If

            Return oIAdvReq.GetPettyCashAdvanceList(oETAdvReq)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetAnAdvanceTransactionRecord(ByVal oETAdvReq As AdvanceRequest) As AdvanceRequest
        Dim oIAdvReq As IAdvanceRequest

        Try
            oIAdvReq = ComponentFactory.CreateAdvanceRequest
            If oIAdvReq Is Nothing Then
                Throw New Exception(STR_ERR_MSG_UNABLE_TO_CREATE_COMPONENT)
            End If

            Return oIAdvReq.GetAnAdvanceTransactionRecord(oETAdvReq)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPettyCashAdvanceRpt(ByVal oETAdvReq As AdvanceRequest) As AdvanceRequest
        Dim oIAdvReq As IAdvanceRequest

        Try
            oIAdvReq = ComponentFactory.CreateAdvanceRequest
            If oIAdvReq Is Nothing Then
                Throw New Exception(STR_ERR_MSG_UNABLE_TO_CREATE_COMPONENT)
            End If

            Return oIAdvReq.GetPettyCashAdvanceRpt(oETAdvReq)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub SaveAdvanceReturnTransaction(ByVal oAdvanceRequest As Parameter.AdvanceRequest)
        Dim oIAdvReq As IAdvanceRequest

        Try
            oIAdvReq = ComponentFactory.CreateAdvanceRequest
            If oIAdvReq Is Nothing Then
                Throw New Exception(STR_ERR_MSG_UNABLE_TO_CREATE_COMPONENT)
            End If

            oIAdvReq.SaveAdvanceReturnTransaction(oAdvanceRequest)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Function CreateAdvanceTransactionDetail(cnn As String, advanceNo As String, atDetail As IList(Of Parameter.AdvanceDetail)) As String
        Return CType(ComponentFactory.CreateAdvanceRequest(), IAdvanceRequest).CreateAdvanceTransactionDetail(cnn, advanceNo, atDetail)
    End Function
    'Public Function GetTablePC(ByVal oET As Parameter.AdvanceRequest, ByVal strfile As String) As Parameter.AdvanceRequest
    '    Dim oIAdvReq As IAdvanceRequest
    '    oIAdvReq = ComponentFactory.CreateAdvanceRequest()
    '    Return oIAdvReq.GetTableAR(oET, strfile)
    'End Function
End Class
