
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class LCWaivedController
    Public Sub SaveLCWaived(ByVal customclass As Parameter.FullPrepay)
        Dim oLCWaivedInterface As ILCWaived
        oLCWaivedInterface = ComponentFactory.CreateLCWaived()
        oLCWaivedInterface.SaveLCWaived(customclass)
    End Sub

    Public Function ListLCWaived(ByVal customclass As Parameter.FullPrepay) As Parameter.FullPrepay
        Dim oLCWaivedInterface As ILCWaived
        oLCWaivedInterface = ComponentFactory.CreateLCWaived()
        Return oLCWaivedInterface.ListLCWaived(customclass)
    End Function

    Public Function ViewLCWaived(ByVal customclass As Parameter.FullPrepay) As Parameter.FullPrepay
        Dim oLCWaivedInterface As ILCWaived
        oLCWaivedInterface = ComponentFactory.CreateLCWaived()
        Return oLCWaivedInterface.ViewLCWaived(customclass)
    End Function

End Class
