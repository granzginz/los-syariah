
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region

Public Class PaymentReversalController
    Public Function PaymentReversalList(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal
        Dim oPaymentReversalList As IPaymentReversal
        oPaymentReversalList = ComponentFactory.CreatePaymentReversal
        Return oPaymentReversalList.PaymentReversalList(oCustomClass)
    End Function

    Public Function PaymentReversalListOtor(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal
        Dim oPaymentReversalList As IPaymentReversal
        oPaymentReversalList = ComponentFactory.CreatePaymentReversal
        Return oPaymentReversalList.PaymentReversalListOtor(oCustomClass)
    End Function

    Public Sub ProcessPaymentReversal(ByVal oCustomClass As Parameter.PaymentReversal)
        Dim oProcessPaymentReversal As IPaymentReversal
        oProcessPaymentReversal = ComponentFactory.CreatePaymentReversal
        oProcessPaymentReversal.ProcessPaymentReversal(oCustomClass)
    End Sub

    Public Sub ProcessPaymentReversalOtor(ByVal oCustomClass As Parameter.PaymentReversal)
        Dim oProcessPaymentReversal As IPaymentReversal
        oProcessPaymentReversal = ComponentFactory.CreatePaymentReversal
        oProcessPaymentReversal.ProcessPaymentReversalOtor(oCustomClass)
    End Sub

    Public Sub ProcessReversalOtorFactoring(ByVal oCustomClass As Parameter.PaymentReversal)
        Dim oProcessPaymentReversal As IPaymentReversal
        oProcessPaymentReversal = ComponentFactory.CreatePaymentReversal
        oProcessPaymentReversal.ProcessReversalOtorFactoring(oCustomClass)
    End Sub

    Public Sub ProcessReversalOtorModalKerja(ByVal oCustomClass As Parameter.PaymentReversal)
        Dim oProcessPaymentReversal As IPaymentReversal
        oProcessPaymentReversal = ComponentFactory.CreatePaymentReversal
        oProcessPaymentReversal.ProcessReversalOtorModalKerja(oCustomClass)
    End Sub


    Public Function ReversalLogList(oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal
        Dim oReversalLog As IPaymentReversal
        oReversalLog = ComponentFactory.CreatePaymentReversal
        Return oReversalLog.ReversalLogList(oCustomClass)
    End Function

    Public Sub ProcessPaymentReversalOL(ByVal oCustomClass As Parameter.PaymentReversal)
        Dim oProcessPaymentReversal As IPaymentReversal
        oProcessPaymentReversal = ComponentFactory.CreatePaymentReversal
        oProcessPaymentReversal.ProcessPaymentReversalOL(oCustomClass)
    End Sub

	Public Function PaymentReversalListOL(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal
		Dim oPaymentReversalList As IPaymentReversal
		oPaymentReversalList = ComponentFactory.CreatePaymentReversal
		Return oPaymentReversalList.PaymentReversalListOL(oCustomClass)
	End Function

	Public Function PaymentReversalListFactoring(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal
		Dim oPaymentReversalList As IPaymentReversal
		oPaymentReversalList = ComponentFactory.CreatePaymentReversal
		Return oPaymentReversalList.PaymentReversalListFactoring(oCustomClass)
	End Function

	Public Sub ProcessPaymentReversalFactoring(ByVal oCustomClass As Parameter.PaymentReversal)
		Dim oProcessPaymentReversal As IPaymentReversal
		oProcessPaymentReversal = ComponentFactory.CreatePaymentReversal
		oProcessPaymentReversal.ProcessPaymentReversalFactoring(oCustomClass)
	End Sub

	Public Function PaymentReversalListModalKerja(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal
		Dim oPaymentReversalList As IPaymentReversal
		oPaymentReversalList = ComponentFactory.CreatePaymentReversal
		Return oPaymentReversalList.PaymentReversalListModalKerja(oCustomClass)
	End Function

    Public Sub ProcessPaymentReversalModalKerja(ByVal oCustomClass As Parameter.PaymentReversal)
        Dim oProcessPaymentReversal As IPaymentReversal
        oProcessPaymentReversal = ComponentFactory.CreatePaymentReversal
        oProcessPaymentReversal.ProcessPaymentReversalModalKerja(oCustomClass)
    End Sub
    Public Sub ProcessReversalTolakanBGOtor(ByVal oCustomClass As Parameter.PaymentReversal)
        Dim oProcessPaymentReversal As IPaymentReversal
        oProcessPaymentReversal = ComponentFactory.CreatePaymentReversal
        oProcessPaymentReversal.ProcessReversalTolakanBGOtor(oCustomClass)
    End Sub

End Class
