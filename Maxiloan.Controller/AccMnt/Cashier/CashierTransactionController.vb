
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class CashierTransactionController
    Public Function ListCashierHistory(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction
        Dim CashierBO As ICashierTransaction
        CashierBO = ComponentFactory.CreateCashierTransaction()
        Return CashierBO.ListCashierHistory(oCustomClass)
    End Function

    Public Sub CashierOpen(ByVal oCustomClass As Parameter.CashierTransaction)
        Dim CashierBO As ICashierTransaction
        CashierBO = ComponentFactory.CreateCashierTransaction()
        CashierBO.CashierOpen(oCustomClass)
    End Sub

    Public Function CashierCloseList(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction
        Dim CashierBO As ICashierTransaction
        CashierBO = ComponentFactory.CreateCashierTransaction()
        Return CashierBO.CashierCloseList(oCustomClass)
    End Function

    Public Sub CashierClose(ByVal oCustomClass As Parameter.CashierTransaction)
        Dim CashierBO As ICashierTransaction
        CashierBO = ComponentFactory.CreateCashierTransaction()
        CashierBO.CashierClose(oCustomClass)
    End Sub

    Public Function CHCloseList(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction
        Dim CashierBO As ICashierTransaction
        CashierBO = ComponentFactory.CreateCashierTransaction()
        Return CashierBO.CHCloseList(oCustomClass)
    End Function

    Public Sub HeadCashierClose(ByVal oCustomClass As Parameter.CashierTransaction)
        Dim CashierBO As ICashierTransaction
        CashierBO = ComponentFactory.CreateCashierTransaction()
        CashierBO.HeadCashierClose(oCustomClass)
    End Sub

    Public Function CheckHeadCashier(ByVal oCustomClass As Parameter.CashierTransaction) As Boolean
        Dim CashierBO As ICashierTransaction
        CashierBO = ComponentFactory.CreateCashierTransaction()
        Return CashierBO.CheckHeadCashier(oCustomClass)
    End Function

    Public Function CheckAllCashierClose(ByVal oCustomClass As Parameter.CashierTransaction) As Boolean
        Dim CashierBO As ICashierTransaction
        CashierBO = ComponentFactory.CreateCashierTransaction()
        Return CashierBO.CheckAllCashierClose(oCustomClass)
    End Function

    Public Function GetCashier(ByVal oCustomClass As Parameter.CashierTransaction) As DataTable
        Dim CA As ICashierTransaction
        CA = ComponentFactory.CreateCashierTransaction()
        Return CA.GetCashier(oCustomClass)
    End Function

    Public Sub HeadCashierBatchProcess(ByVal ocustomclass As Parameter.CashierTransaction)
        Dim CashierBO As ICashierTransaction
        CashierBO = ComponentFactory.CreateCashierTransaction()
        CashierBO.HeadCashierBatchProcess(ocustomclass)
    End Sub

    Public Function GLInterfaceHeader(ByVal ocustomclass As Parameter.CashierTransaction) As DataTable
        Dim CashierBO As ICashierTransaction
        CashierBO = ComponentFactory.CreateCashierTransaction()
        Return CashierBO.GLInterfaceHeader(ocustomclass)
    End Function

    Public Function GLInterfaceDetail(ByVal ocustomclass As Parameter.CashierTransaction) As DataTable
        Dim CashierBO As ICashierTransaction
        CashierBO = ComponentFactory.CreateCashierTransaction()
        Return CashierBO.GLInterfaceDetail(ocustomclass)
    End Function
    Public Function FileAllocationGLInterface(ByVal strConnection As String) As String
        Dim CashierBO As ICashierTransaction
        CashierBO = ComponentFactory.CreateCashierTransaction()
        Return CashierBO.FileAllocationGLInterface(strConnection)
    End Function


    Public Function GetTransaction(ByVal oCustomClass As Parameter.CashierTransaction) As DataTable
        Dim Trans As ICashierTransaction
        Trans = ComponentFactory.CreateCashierTransaction()
        Return Trans.GetTransaction(oCustomClass)
    End Function

    Public Function GetOutProc(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction
        Dim OutP As ICashierTransaction
        OutP = ComponentFactory.CreateCashierTransaction()
        Return OutP.GetOutProc(oCustomClass)
    End Function

    Public Function GetListOutProc(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction
        Dim ListOutP As ICashierTransaction
        ListOutP = ComponentFactory.CreateCashierTransaction()
        Return ListOutP.GetListOutProc(oCustomClass)
    End Function

    Public Function getCashBankBalance(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction
        Dim oInterface As ICashierTransaction
        oInterface = ComponentFactory.CreateCashierTransaction()
        Return oInterface.getCashBankBalance(oCustomClass)
    End Function

    Public Function getCashBankBalanceHistory(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction
        Dim oInterface As ICashierTransaction
        oInterface = ComponentFactory.CreateCashierTransaction()
        Return oInterface.getCashBankBalanceHistory(oCustomClass)
    End Function



End Class
