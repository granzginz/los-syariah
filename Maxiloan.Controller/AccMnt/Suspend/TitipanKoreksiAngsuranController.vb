

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class TitipanKoreksiAngsuranController
    Public Sub TitipanSplit(oCustomClass As Parameter.SuspendReceive)
        Dim oTitipanSplit As ISuspendAllocation
        oTitipanSplit = ComponentFactory.CreateSuspendAllocation
        oTitipanSplit.TitipanSplit(oCustomClass)
    End Sub
End Class
