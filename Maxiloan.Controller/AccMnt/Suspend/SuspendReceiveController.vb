
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SuspendReceiveController

    Public Sub SaveSuspendReceive(ByVal oCustomClass As Parameter.SuspendReceive)
        Dim oSaveSuspendReceive As ISuspendReceive
        oSaveSuspendReceive = ComponentFactory.CreateSuspendReceive
        oSaveSuspendReceive.SaveSuspendReceive(oCustomClass)
    End Sub
    Public Sub SaveSuspendTransactionImplementasi(ByVal oCustomClass As Parameter.SuspendReceive)
        Dim oSaveSuspendReceive As ISuspendReceive
        oSaveSuspendReceive = ComponentFactory.CreateSuspendReceive
        oSaveSuspendReceive.SaveSuspendTransactionImplementasi(oCustomClass)
    End Sub

    Public Sub SaveSuspendOtorisasi(ByVal oCustomClass As Parameter.SuspendReceive)
        Dim oSaveSuspendReceive As ISuspendReceive
        oSaveSuspendReceive = ComponentFactory.CreateSuspendReceive
        oSaveSuspendReceive.SaveSuspendOtorisasi(oCustomClass)
    End Sub

    Public Sub SaveReversalOtorisasi(ByVal oCustomClass As Parameter.SuspendReceive)
        Dim oSaveReversalOtorisasi As ISuspendReceive
        oSaveReversalOtorisasi = ComponentFactory.CreateSuspendReceive
        oSaveReversalOtorisasi.SaveReversalOtorisasi(oCustomClass)
    End Sub
End Class
