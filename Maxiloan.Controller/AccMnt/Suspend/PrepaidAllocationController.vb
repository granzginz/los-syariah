﻿#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class PrepaidAllocationController
    Public Function PrepaidAllocation(ByVal oCustomClass2 As Parameter.InstallRcv) As Parameter.InstallRcv
        Dim oPrepaidAllocation As IPrepaidAllocation
        oPrepaidAllocation = ComponentFactory.CreatePrepaidAllocation()
        Return oPrepaidAllocation.PrepaidAllocation(oCustomClass2)
    End Function
End Class
