

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SuspendReversalController
    Public Function SuspendReversalList(ByVal oCustomClass As Parameter.SuspendReceive) As Parameter.SuspendReceive
        Dim oSuspendReversalList As ISuspendReversal
        oSuspendReversalList = ComponentFactory.CreateSuspendReversal
        Return oSuspendReversalList.SuspendReversalList(oCustomClass)
    End Function

    Public Function SuspendReverse(ByVal oCustomClass As Parameter.SuspendReceive) As Parameter.SuspendReceive
        Dim oSuspendReverse As ISuspendReversal
        oSuspendReverse = ComponentFactory.CreateSuspendReversal
        Return oSuspendReverse.SuspendReverse(oCustomClass)
    End Function

    Public Sub SaveSuspendReverse(ByVal oCustomClass As Parameter.SuspendReceive)
        Dim oSaveSuspendReverse As ISuspendReversal
        oSaveSuspendReverse = ComponentFactory.CreateSuspendReversal
        oSaveSuspendReverse.SaveSuspendReverse(oCustomClass)
    End Sub

    Public Function SplitTitipanList(ByVal oCustomClass As Parameter.SuspendReceive) As Parameter.SuspendReceive
        Dim oSplitTitipanList As ISuspendReversal
        oSplitTitipanList = ComponentFactory.CreateSuspendReversal
        Return oSplitTitipanList.SplitTitipanList(oCustomClass)
    End Function
End Class
