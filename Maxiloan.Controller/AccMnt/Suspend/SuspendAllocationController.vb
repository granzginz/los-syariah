

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SuspendAllocationController
    Public Sub SuspendAllocation(ByVal oCustomClass As Parameter.InstallRcv)
        Dim oSuspendAllocation As ISuspendAllocation
        oSuspendAllocation = ComponentFactory.CreateSuspendAllocation
        oSuspendAllocation.SuspendAllocationSave(oCustomClass)
    End Sub
    Public Function SuspendDetail(oCustom As Parameter.SuspendReceive) As Parameter.SuspendReceive
        Dim oSuspendAllocatin As ISuspendAllocation
        oSuspendAllocatin = ComponentFactory.CreateSuspendAllocation
        Return oSuspendAllocatin.SuspendDetail(oCustom)
    End Function
    Public Sub SuspendSplit(oCustomClass As Parameter.SuspendReceive)
        Dim oSuspendAllocation As ISuspendAllocation
        oSuspendAllocation = ComponentFactory.CreateSuspendAllocation
        oSuspendAllocation.SuspendSplit(oCustomClass)
    End Sub
    'penambahan alokasi suspend fact & mdkj
    Public Sub SuspendAllocationFACT(ByVal oCustomClass As Parameter.InstallRcv)
        Dim oSuspendAllocation As ISuspendAllocation
        oSuspendAllocation = ComponentFactory.CreateSuspendAllocation
        oSuspendAllocation.SuspendAllocationSaveFACT(oCustomClass)
    End Sub
    Public Sub SuspendAllocationMDKJ(ByVal oCustomClass As Parameter.InstallRcv)
        Dim oSuspendAllocation As ISuspendAllocation
        oSuspendAllocation = ComponentFactory.CreateSuspendAllocation
        oSuspendAllocation.SuspendAllocationSaveMDKJ(oCustomClass)
    End Sub

    Public Function TitipanDetail(oCustom As Parameter.SuspendReceive) As Parameter.SuspendReceive
        Dim oTitipanDetail As ISuspendAllocation
        oTitipanDetail = ComponentFactory.CreateSuspendAllocation
        Return oTitipanDetail.TitipanDetail(oCustom)
    End Function
    Public Sub TitipanSplit(oCustomClass As Parameter.SuspendReceive)
        Dim oTitipanSplit As ISuspendAllocation
        oTitipanSplit = ComponentFactory.CreateSuspendAllocation
        oTitipanSplit.TitipanSplit(oCustomClass)
    End Sub
End Class
