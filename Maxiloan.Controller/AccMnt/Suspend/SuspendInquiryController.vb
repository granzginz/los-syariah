

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SuspendInquiryController
    Public Function SuspendInqList(ByVal oCustomClass As Parameter.SuspendReceive) As Parameter.SuspendReceive
        Dim oSuspendInqList As ISuspendInquiry
        oSuspendInqList = ComponentFactory.CreateSuspendInquiry
        Return oSuspendInqList.SuspendInquiryList(oCustomClass)
    End Function

    Public Function SuspendInqReport(ByVal oCustomClass As Parameter.SuspendReceive) As Parameter.SuspendReceive
        Dim oSuspendInq As ISuspendInquiry
        oSuspendInq = ComponentFactory.CreateSuspendInquiry
        Return oSuspendInq.SuspendInqReport(oCustomClass)
    End Function
    Public Function GetBankAccountAll(ByVal oCustomClass As Parameter.SuspendReceive) As DataTable
        Dim oSuspendBankAccount As ISuspendInquiry
        oSuspendBankAccount = ComponentFactory.CreateSuspendInquiry
        Return oSuspendBankAccount.GetBankAccountAll(oCustomClass)
    End Function
End Class
