
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class SuspendAllocationGeneralController
    Public Function GetSuspendAllocationGeneral(ByVal ocustomClass As Parameter.SuspendAllocationGeneral) As Parameter.SuspendAllocationGeneral
        Dim SuspendAllocationGeneralBO As ISuspendAllocationGeneral
        SuspendAllocationGeneralBO = ComponentFactory.CreateSuspendAllocationGeneral()
        Return SuspendAllocationGeneralBO.GetSuspendAllocationGeneral(ocustomClass)
    End Function
    Public Function GetSuspendAllocationGeneralList(ByVal ocustomClass As Parameter.SuspendAllocationGeneral) As Parameter.SuspendAllocationGeneral
        Dim SuspendAllocationGeneralBO As ISuspendAllocationGeneral
        SuspendAllocationGeneralBO = ComponentFactory.CreateSuspendAllocationGeneral()
        Return SuspendAllocationGeneralBO.GetSuspendAllocationGeneralList(ocustomClass)
    End Function

    Public Sub SuspendAllocationGeneralSaveEdit(ByVal ocustomClass As Parameter.SuspendAllocationGeneral)
        Dim SuspendAllocationGeneralBO As ISuspendAllocationGeneral
        SuspendAllocationGeneralBO = ComponentFactory.CreateSuspendAllocationGeneral()
        SuspendAllocationGeneralBO.SuspendAllocationGeneralSaveEdit(ocustomClass)
    End Sub
    Public Function GetComboDepartemen(ByVal ocustomClass As Parameter.SuspendAllocationGeneral) As DataTable
        Dim SuspendAllocationGeneralBO As ISuspendAllocationGeneral
        SuspendAllocationGeneralBO = ComponentFactory.CreateSuspendAllocationGeneral()
        Return SuspendAllocationGeneralBO.GetComboDepartemen(ocustomClass)
    End Function
    Public Function GetComboCabang(ByVal ocustomClass As Parameter.SuspendAllocationGeneral) As DataTable
        Dim SuspendAllocationGeneralBO = ComponentFactory.CreateSuspendAllocationGeneral()
        Return SuspendAllocationGeneralBO.GetComboCabang(ocustomClass)
    End Function

    Public Function GetIsBlock(ByVal oCustomClass As Parameter.SuspendAllocationGeneral) As Parameter.SuspendAllocationGeneral
        Dim SuspendAllocationGeneralBO As ISuspendAllocationGeneral
        SuspendAllocationGeneralBO = ComponentFactory.CreateSuspendAllocationGeneral()
        Return SuspendAllocationGeneralBO.GetIsBlock(oCustomClass)
    End Function

    Public Function GetSuspendIsBlockList(ByVal ocustomClass As Parameter.SuspendAllocationGeneral) As Parameter.SuspendAllocationGeneral
        Dim SuspendAllocationGeneralBO As ISuspendAllocationGeneral
        SuspendAllocationGeneralBO = ComponentFactory.CreateSuspendAllocationGeneral()
        Return SuspendAllocationGeneralBO.GetSuspendIsBlockList(ocustomClass)
    End Function
    Public Sub ReleaseBlokirSuspend(ByVal ocustomClass As Parameter.SuspendAllocationGeneral)
        Dim SuspendAllocationGeneralBO As ISuspendAllocationGeneral
        SuspendAllocationGeneralBO = ComponentFactory.CreateSuspendAllocationGeneral()
        SuspendAllocationGeneralBO.ReleaseBlokirSuspend(ocustomClass)
    End Sub
End Class
