
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class PaymentHistoryController
    Public Function ListPaymentHistory(ByVal oCustomClass As Parameter.PaymentHistory) As Parameter.PaymentHistory
        Dim oPaymentHistory As IPaymentHistory
        oPaymentHistory = ComponentFactory.CreatePaymentHistoryList
        Return oPaymentHistory.ListPaymentHistory(oCustomClass)
    End Function

    Public Function ListPaymentHistoryDetailOtor(ByVal oCustomClass As Parameter.PaymentHistory) As Parameter.PaymentHistory
        Dim oPaymentHistory As IPaymentHistory
        oPaymentHistory = ComponentFactory.CreatePaymentHistoryList
        Return oPaymentHistory.ListPaymentHistoryDetailOtor(oCustomClass)
    End Function

    Public Function ListPaymentHistoryDetail(ByVal oCustomClass As Parameter.PaymentHistory) As Parameter.PaymentHistory
        Dim oPaymentHistory As IPaymentHistory
        oPaymentHistory = ComponentFactory.CreatePaymentHistoryList
        Return oPaymentHistory.ListPaymentHistoryDetail(oCustomClass)
    End Function

    Public Function ReversalTransaksiLainnyaSave(ByVal m_oCustomClass As Parameter.PaymentHistory) As Parameter.PaymentHistory
        Dim oPaymentHistory As IPaymentHistory
        oPaymentHistory = ComponentFactory.CreatePaymentHistoryList
        Return oPaymentHistory.ReversalTransaksiLainnyaSave(m_oCustomClass)
    End Function

    Public Function ListPaymentHistoryDetailFactoring(ByVal oCustomClass As Parameter.PaymentHistory) As Parameter.PaymentHistory
		Dim oPaymentHistory As IPaymentHistory
		oPaymentHistory = ComponentFactory.CreatePaymentHistoryList
		Return oPaymentHistory.ListPaymentHistoryDetailFactoring(oCustomClass)
	End Function

	Public Function ListPaymentHistoryDetailModalKerja(ByVal oCustomClass As Parameter.PaymentHistory) As Parameter.PaymentHistory
		Dim oPaymentHistory As IPaymentHistory
		oPaymentHistory = ComponentFactory.CreatePaymentHistoryList
		Return oPaymentHistory.ListPaymentHistoryDetailModalKerja(oCustomClass)
	End Function

    Public Function ReversalPembayaranTDPSave(ByVal m_oCustomClass As Parameter.PaymentHistory) As Parameter.PaymentHistory
        Dim oPaymentHistory As IPaymentHistory
        oPaymentHistory = ComponentFactory.CreatePaymentHistoryList
        Return oPaymentHistory.ReversalPembayaranTDPSave(m_oCustomClass)
    End Function
End Class
