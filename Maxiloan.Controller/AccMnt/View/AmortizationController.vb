

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AmortizationController
    Public Function ListAmortization(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim oListAmortization As Iamortization
        oListAmortization = ComponentFactory.CreateAmortization
        Return oListAmortization.ListAmortization(oCustomClass)
    End Function
End Class
