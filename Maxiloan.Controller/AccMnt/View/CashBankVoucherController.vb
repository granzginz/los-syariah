
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class CashBankVoucherController
    Public Function ListCashBankVoucher(ByVal oCustomClass As Parameter.CashBankVoucher) As Parameter.CashBankVoucher
        Dim oCashBankVoucher As ICashBankVoucher
        oCashBankVoucher = ComponentFactory.CreateCashBankVoucher
        Return oCashBankVoucher.ListCashBankVoucher(oCustomClass)
    End Function
End Class
