
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class FloatingController
    Public Function ReschedulingProduct(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFloating
        BO = ComponentFactory.CreateFloating
        Return BO.ReschedulingProduct(oCustomClass)
    End Function
    Public Function LastAdjustment(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFloating
        BO = ComponentFactory.CreateFloating
        Return BO.LastAdjustment(oCustomClass)
    End Function
    Public Function SaveFloatingData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFloating
        BO = ComponentFactory.CreateFloating
        Return BO.SaveFloatingData(oCustomClass)
    End Function
    Public Function GetFloatingData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFloating
        BO = ComponentFactory.CreateFloating
        Return BO.GetFloatingData(oCustomClass)
    End Function
    Public Function FloatingPrintTrial(ByVal oCustomClass As Parameter.FinancialData) As System.Data.DataSet
        Dim BO As IFloating
        BO = ComponentFactory.CreateFloating
        Return BO.FloatingPrintTrial(oCustomClass)
    End Function
End Class
