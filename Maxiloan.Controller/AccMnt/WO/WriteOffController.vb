
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class WriteOffController
    Public Function GetGeneralPagingWO(ByVal oCustomClass As Parameter.WriteOff) As Parameter.WriteOff
        Dim BO As IWriteOff
        BO = ComponentFactory.CreateWriteOff
        Return BO.GetGeneralPagingWO(oCustomClass)
    End Function
    Public Function WriteOffGetSAAmount(ByVal oCustomClass As Parameter.WriteOff) As Parameter.WriteOff
        Dim BO As IWriteOff
        BO = ComponentFactory.CreateWriteOff
        Return BO.WriteOffGetSAAmount(oCustomClass)
    End Function
    Public Sub WORequest(ByVal oCustomClass As Parameter.WriteOff)
        Dim BO As IWriteOff
        BO = ComponentFactory.CreateWriteOff
        BO.WORequest(oCustomClass)
    End Sub
    Public Function WriteOffInquiry(ByVal oCustomClass As Parameter.WriteOff) As Parameter.WriteOff
        Dim BO As IWriteOff
        BO = ComponentFactory.CreateWriteOff
        Return BO.WriteOffInquiry(oCustomClass)
    End Function
    Public Function WOInquiryReport(ByVal oCustomClass As Parameter.WriteOff) As Parameter.WriteOff
        Dim BO As IWriteOff
        BO = ComponentFactory.CreateWriteOff
        Return BO.WOInquiryReport(oCustomClass)
    End Function
    Public Function WriteOffView(ByVal oCustomClass As Parameter.WriteOff) As Parameter.WriteOff
        Dim BO As IWriteOff
        BO = ComponentFactory.CreateWriteOff
        Return BO.WriteOffView(oCustomClass)
    End Function
    Public Sub WriteOffEditPotensiTagih(ByVal oCustomClass As Parameter.WriteOff)
        Dim BO As IWriteOff
        BO = ComponentFactory.CreateWriteOff
        BO.WriteOffEditPotensiTagih(oCustomClass)
    End Sub
End Class
