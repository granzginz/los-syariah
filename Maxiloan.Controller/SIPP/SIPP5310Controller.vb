﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Parameter
#End Region

Public Class SIPP5310Controller
    Public Function GetSIPP5310List(ByVal ocustomClass As Parameter.SIPP5310) As Parameter.SIPP5310
        Dim SIPP5310BO As ISIPP5310
        SIPP5310BO = ComponentFactory.CreateSIPP5310()
        Return SIPP5310BO.GetSIPP5310List(ocustomClass)
    End Function

    Public Function SIPP5310SaveAdd(ByVal ocustomClass As Parameter.SIPP5310)
        Dim SIPP5310BO As ISIPP5310
        SIPP5310BO = ComponentFactory.CreateSIPP5310()
        Return SIPP5310BO.SIPP5310SaveAdd(ocustomClass)
    End Function
    Public Function GetSIPP5310Edit(ByVal ocustomClass As Parameter.SIPP5310) As Parameter.SIPP5310
        Dim SIPP5310BO As ISIPP5310
        SIPP5310BO = ComponentFactory.CreateSIPP5310()
        Return SIPP5310BO.GetSIPP5310Edit(ocustomClass)
    End Function
    Public Function SIPP5310Delete(ByVal ocustomClass As Parameter.SIPP5310) As String
        Dim SIPP5310BO As ISIPP5310
        SIPP5310BO = ComponentFactory.CreateSIPP5310()
        Return SIPP5310BO.SIPP5310Delete(ocustomClass)
    End Function
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP5310) As Parameter.SIPP5310
        Dim SIPP5310BO As ISIPP5310
        SIPP5310BO = ComponentFactory.CreateSIPP5310()
        Return SIPP5310BO.GetCbo(ocustomClass)
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP5310) As Parameter.SIPP5310
        Dim SIPP5310BO As ISIPP5310
        SIPP5310BO = ComponentFactory.CreateSIPP5310()
        Return SIPP5310BO.GetCboBulandataSIPP(oCustomClass)
    End Function

    Public Function SIPP5310EditSave(ocustomClass As SIPP5310) As SIPP5310
        Dim SIPP5310BO As ISIPP5310
        SIPP5310BO = ComponentFactory.CreateSIPP5310()
        SIPP5310BO.SIPP5310SaveEdit(ocustomClass)
    End Function
End Class
