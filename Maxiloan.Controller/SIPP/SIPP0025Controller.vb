﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP0025Controller
    Public Function GetSIPP0025(ByVal ocustomClass As Parameter.SIPP0025) As Parameter.SIPP0025
        Dim SIPP0025BO As ISIPP0025
        SIPP0025BO = ComponentFactory.CreateSIPP0025()
        Return SIPP0025BO.GetSIPP0025(ocustomClass)
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0025) As Parameter.SIPP0025
        Dim SIPP0025BO As ISIPP0025
        SIPP0025BO = ComponentFactory.CreateSIPP0025()
        Return SIPP0025BO.GetCboBulandataSIPP(oCustomClass)
    End Function
    Public Function SIPP0025Add(ByVal ocustomClass As Parameter.SIPP0025)
        Dim SIPP0025BO As ISIPP0025
        SIPP0025BO = ComponentFactory.CreateSIPP0025()
        Return SIPP0025BO.SIPP0025SaveAdd(ocustomClass)
    End Function
    Public Sub SIPP0025Update(ByVal ocustomClass As Parameter.SIPP0025)
        Dim SIPP0025BO As ISIPP0025
        SIPP0025BO = ComponentFactory.CreateSIPP0025()
        SIPP0025BO.SIPP0025SaveEdit(ocustomClass)
    End Sub

    Public Function GetSIPP0025Edit(ByVal ocustomClass As Parameter.SIPP0025) As Parameter.SIPP0025
        Dim SIPP0025BO As ISIPP0025
        SIPP0025BO = ComponentFactory.CreateSIPP0025()
        Return SIPP0025BO.GetSIPP0025Edit(ocustomClass)
    End Function
    Public Function SIPP0025Delete(ByVal ocustomClass As Parameter.SIPP0025) As String
        Dim SIPP0025BO As ISIPP0025
        SIPP0025BO = ComponentFactory.CreateSIPP0025()
        Return SIPP0025BO.SIPP0025Delete(ocustomClass)
    End Function
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP0025) As Parameter.SIPP0025
        Dim SIPP0025BO As ISIPP0025
        SIPP0025BO = ComponentFactory.CreateSIPP0025()
        Return SIPP0025BO.GetCbo(ocustomClass)
    End Function

End Class
