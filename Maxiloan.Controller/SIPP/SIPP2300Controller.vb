﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP2300Controller
    Public Function GetSIPP2300(ByVal ocustomClass As Parameter.SIPP2300) As Parameter.SIPP2300
        Dim SIPP2300BO As ISIPP2300
        SIPP2300BO = ComponentFactory.CreateSIPP2300()
        Return SIPP2300BO.GetSIPP2300(ocustomClass)
    End Function

    Public Function SIPP2300Add(ByVal ocustomClass As Parameter.SIPP2300)
        Dim SIPP2300BO As ISIPP2300
        SIPP2300BO = ComponentFactory.CreateSIPP2300()
        Return SIPP2300BO.SIPP2300SaveAdd(ocustomClass)
    End Function
    Public Sub SIPP2300Update(ByVal ocustomClass As Parameter.SIPP2300)
        Dim SIPP2300BO As ISIPP2300
        SIPP2300BO = ComponentFactory.CreateSIPP2300()
        SIPP2300BO.SIPP2300SaveEdit(ocustomClass)
    End Sub

    Public Function GetSIPP2300Edit(ByVal ocustomClass As Parameter.SIPP2300) As Parameter.SIPP2300
        Dim SIPP2300BO As ISIPP2300
        SIPP2300BO = ComponentFactory.CreateSIPP2300()
        Return SIPP2300BO.GetSIPP2300Edit(ocustomClass)
    End Function
    Public Function SIPP2300Delete(ByVal ocustomClass As Parameter.SIPP2300) As String
        Dim SIPP2300BO As ISIPP2300
        SIPP2300BO = ComponentFactory.CreateSIPP2300()
        Return SIPP2300BO.SIPP2300Delete(ocustomClass)
    End Function
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP2300) As Parameter.SIPP2300
        Dim SIPP2300BO As ISIPP2300
        SIPP2300BO = ComponentFactory.CreateSIPP2300()
        Return SIPP2300BO.GetCbo(ocustomClass)
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP2300) As Parameter.SIPP2300
        Dim SIPP2300BO As ISIPP2300
        SIPP2300BO = ComponentFactory.CreateSIPP2300()
        Return SIPP2300BO.GetCboBulandataSIPP(oCustomClass)
    End Function
End Class
