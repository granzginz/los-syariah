﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP0010Controller
    Public Function GetSIPP0010(ByVal ocustomClass As Parameter.SIPP0010) As Parameter.SIPP0010
        Dim SIPP0010BO As ISIPP0010
        SIPP0010BO = ComponentFactory.CreateSIPP0010()
        Return SIPP0010BO.GetSIPP0010(ocustomClass)
    End Function

    Public Function SIPP0010Add(ByVal ocustomClass As Parameter.SIPP0010)
        Dim SIPP0010BO As ISIPP0010
        SIPP0010BO = ComponentFactory.CreateSIPP0010()
        Return SIPP0010BO.SIPP0010SaveAdd(ocustomClass)
    End Function
    Public Sub SIPP0010Update(ByVal ocustomClass As Parameter.SIPP0010)
        Dim SIPP0010BO As ISIPP0010
        SIPP0010BO = ComponentFactory.CreateSIPP0010()
        SIPP0010BO.SIPP0010SaveEdit(ocustomClass)
    End Sub

    Public Function GetSIPP0010Edit(ByVal ocustomClass As Parameter.SIPP0010) As Parameter.SIPP0010
        Dim SIPP0010BO As ISIPP0010
        SIPP0010BO = ComponentFactory.CreateSIPP0010()
        Return SIPP0010BO.GetSIPP0010Edit(ocustomClass)
    End Function
    Public Function SIPP0010Delete(ByVal ocustomClass As Parameter.SIPP0010) As String
        Dim SIPP0010BO As ISIPP0010
        SIPP0010BO = ComponentFactory.CreateSIPP0010()
        Return SIPP0010BO.SIPP0010Delete(ocustomClass)
    End Function
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP0010) As Parameter.SIPP0010
        Dim SIPP0010BO As ISIPP0010
        SIPP0010BO = ComponentFactory.CreateSIPP0010()
        Return SIPP0010BO.GetCbo(ocustomClass)
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0010) As Parameter.SIPP0010
        Dim SIPP0010BO As ISIPP0010
        SIPP0010BO = ComponentFactory.CreateSIPP0010()
        Return SIPP0010BO.GetCboBulandataSIPP(oCustomClass)
    End Function

End Class
