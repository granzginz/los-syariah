﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP3020Controller
    Public Function GetSIPP3020(ByVal oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020
        Dim SIPP3020BO As ISIPP3020
        SIPP3020BO = ComponentFactory.CreateSIPP3020()
        Return SIPP3020BO.GetSIPP3020(oCustomClass)
    End Function

    Public Function GetSelectSIPP3020(ByVal oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020
        Dim SIPP3020BO As ISIPP3020
        SIPP3020BO = ComponentFactory.CreateSIPP3020()
        Return SIPP3020BO.GetSIPP3020(oCustomClass)
    End Function

    Public Function SIPP3020Edit(ByVal oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020
        Dim SIPP3020BO As ISIPP3020
        SIPP3020BO = ComponentFactory.CreateSIPP3020()
        Return SIPP3020BO.SIPP3020Edit(oCustomClass)
    End Function

    Public Function SIPP3020Save(ByVal oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020
        Dim SIPP3020BO As ISIPP3020
        SIPP3020BO = ComponentFactory.CreateSIPP3020()
        Return SIPP3020BO.SIPP3020Save(oCustomClass)
    End Function

    Public Function SIPP3020Add(ByVal oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020
        Dim SIPP3020BO As ISIPP3020
        SIPP3020BO = ComponentFactory.CreateSIPP3020()
        Return SIPP3020BO.SIPP3020Add(oCustomClass)
    End Function
    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020
        Dim SIPP3020BO As ISIPP3020
        SIPP3020BO = ComponentFactory.CreateSIPP3020()
        Return SIPP3020BO.GetCbo(oCustomClass)
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP3020) As Parameter.SIPP3020
        Dim SIPP3020BO As ISIPP3020
        SIPP3020BO = ComponentFactory.CreateSIPP3020()
        Return SIPP3020BO.GetCboBulandataSIPP(oCustomClass)
    End Function

    Public Function SIPP3020Delete(ByVal oCustomClass As Parameter.SIPP3020) As String
        Dim SIPP3020BO As ISIPP3020
        SIPP3020BO = ComponentFactory.CreateSIPP3020()
        Return SIPP3020BO.SIPP3020Delete(oCustomClass)
    End Function
End Class

