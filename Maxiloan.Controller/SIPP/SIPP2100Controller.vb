﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP2100Controller
    Public Function GetSIPP2100(ByVal ocustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
        Dim SIPP2100BO As ISIPP2100
        SIPP2100BO = ComponentFactory.CreateSIPP2100()
        Return SIPP2100BO.GetSIPP2100(ocustomClass)
    End Function

    Public Function GetSelectSIPP2100(ByVal oCustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
        Dim SIPP2100BO As ISIPP2100
        SIPP2100BO = ComponentFactory.CreateSIPP2100()
        Return SIPP2100BO.GetSIPP2100(oCustomClass)
    End Function

    Public Function SIPP2100Add(ByVal ocustomClass As Parameter.SIPP2100)
        Dim SIPP2100BO As ISIPP2100
        SIPP2100BO = ComponentFactory.CreateSIPP2100()
        Return SIPP2100BO.SIPP2100SaveAdd(ocustomClass)
    End Function
    Public Sub SIPP2100Update(ByVal ocustomClass As Parameter.SIPP2100)
        Dim SIPP2100BO As ISIPP2100
        SIPP2100BO = ComponentFactory.CreateSIPP2100()
        SIPP2100BO.SIPP2100SaveEdit(ocustomClass)
    End Sub

    Public Function GetSIPP2100Edit(ByVal ocustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
        Dim SIPP2100BO As ISIPP2100
        SIPP2100BO = ComponentFactory.CreateSIPP2100()
        Return SIPP2100BO.GetSIPP2100Edit(ocustomClass)
    End Function
    Public Function SIPP2100Delete(ByVal ocustomClass As Parameter.SIPP2100) As String
        Dim SIPP2100BO As ISIPP2100
        SIPP2100BO = ComponentFactory.CreateSIPP2100()
        Return SIPP2100BO.SIPP2100Delete(ocustomClass)
    End Function
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
        Dim SIPP2100BO As ISIPP2100
        SIPP2100BO = ComponentFactory.CreateSIPP2100()
        Return SIPP2100BO.GetCbo(ocustomClass)
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
        Dim SIPP2100BO As ISIPP2100
        SIPP2100BO = ComponentFactory.CreateSIPP2100()
        Return SIPP2100BO.GetCboBulandataSIPP(oCustomClass)
    End Function
End Class
