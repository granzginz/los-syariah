﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class SIPP0043Controller
    Public Function GetSIPP0043(ByVal oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043
        Dim SIPP0043BO As ISIPP0043
        SIPP0043BO = ComponentFactory.CreateSIPP0043()
        Return SIPP0043BO.GetSIPP0043(oCustomClass)
    End Function

    Public Function GetSelectSIPP0043(ByVal oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043
        Dim SIPP0043BO As ISIPP0043
        SIPP0043BO = ComponentFactory.CreateSIPP0043()
        Return SIPP0043BO.GetSIPP0043(oCustomClass)
    End Function

    Public Function SIPP0043Edit(ByVal oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043
        Dim SIPP0043BO As ISIPP0043
        SIPP0043BO = ComponentFactory.CreateSIPP0043()
        Return SIPP0043BO.SIPP0043Edit(oCustomClass)
    End Function

    Public Function SIPP0043Save(ByVal oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043
        Dim SIPP0043BO As ISIPP0043
        SIPP0043BO = ComponentFactory.CreateSIPP0043()
        Return SIPP0043BO.SIPP0043Save(oCustomClass)
    End Function

    Public Function SIPP0043Add(ByVal oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043
        Dim SIPP0043BO As ISIPP0043
        SIPP0043BO = ComponentFactory.CreateSIPP0043()
        Return SIPP0043BO.SIPP0043Add(oCustomClass)
    End Function
    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043
        Dim SIPP0043BO As ISIPP0043
        SIPP0043BO = ComponentFactory.CreateSIPP0043()
        Return SIPP0043BO.GetCbo(oCustomClass)
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0043) As Parameter.SIPP0043
        Dim SIPP0043BO As ISIPP0043
        SIPP0043BO = ComponentFactory.CreateSIPP0043()
        Return SIPP0043BO.GetCboBulandataSIPP(oCustomClass)
    End Function

    Public Function SIPP0043Delete(ByVal oCustomClass As Parameter.SIPP0043) As String
        Dim SIPP0043BO As ISIPP0043
        SIPP0043BO = ComponentFactory.CreateSIPP0043()
        Return SIPP0043BO.SIPP0043Delete(oCustomClass)
    End Function
End Class
