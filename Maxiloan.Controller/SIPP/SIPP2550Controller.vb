﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP2550Controller
    Public Function GetSIPP2550(ByVal ocustomClass As Parameter.SIPP2550) As Parameter.SIPP2550
        Dim SIPP2550BO As ISIPP2550
        SIPP2550BO = ComponentFactory.CreateSIPP2550()
        Return SIPP2550BO.GetSIPP2550(ocustomClass)
    End Function

    Public Function SIPP2550Add(ByVal ocustomClass As Parameter.SIPP2550)
        Dim SIPP2550BO As ISIPP2550
        SIPP2550BO = ComponentFactory.CreateSIPP2550()
        Return SIPP2550BO.SIPP2550SaveAdd(ocustomClass)
    End Function
    Public Sub SIPP2550Update(ByVal ocustomClass As Parameter.SIPP2550)
        Dim SIPP2550BO As ISIPP2550
        SIPP2550BO = ComponentFactory.CreateSIPP2550()
        SIPP2550BO.SIPP2550SaveEdit(ocustomClass)
    End Sub

    Public Function GetSIPP2550Edit(ByVal ocustomClass As Parameter.SIPP2550) As Parameter.SIPP2550
        Dim SIPP2550BO As ISIPP2550
        SIPP2550BO = ComponentFactory.CreateSIPP2550()
        Return SIPP2550BO.GetSIPP2550Edit(ocustomClass)
    End Function
    Public Function SIPP2550Delete(ByVal ocustomClass As Parameter.SIPP2550) As String
        Dim SIPP2550BO As ISIPP2550
        SIPP2550BO = ComponentFactory.CreateSIPP2550()
        Return SIPP2550BO.SIPP2550Delete(ocustomClass)
    End Function
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP2550) As Parameter.SIPP2550
        Dim SIPP2550BO As ISIPP2550
        SIPP2550BO = ComponentFactory.CreateSIPP2550()
        Return SIPP2550BO.GetCbo(ocustomClass)
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP2550) As Parameter.SIPP2550
        Dim SIPP2550BO As ISIPP2550
        SIPP2550BO = ComponentFactory.CreateSIPP2550()
        Return SIPP2550BO.GetCboBulandataSIPP(oCustomClass)
    End Function
End Class
