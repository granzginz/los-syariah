﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP0035Controller
    Public Function GetSIPP0035(ByVal oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035
        Dim SIPP0035BO As ISIPP0035
        SIPP0035BO = ComponentFactory.CreateSIPP0035()
        Return SIPP0035BO.GetSIPP0035(oCustomClass)
    End Function

    Public Function GetSelectSIPP0035(ByVal oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035
        Dim SIPP0035BO As ISIPP0035
        SIPP0035BO = ComponentFactory.CreateSIPP0035()
        Return SIPP0035BO.GetSIPP0035(oCustomClass)
    End Function

    Public Function SIPP0035Edit(ByVal oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035
        Dim SIPP0035BO As ISIPP0035
        SIPP0035BO = ComponentFactory.CreateSIPP0035()
        Return SIPP0035BO.SIPP0035Edit(oCustomClass)
    End Function

    Public Function SIPP0035Save(ByVal oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035
        Dim SIPP0035BO As ISIPP0035
        SIPP0035BO = ComponentFactory.CreateSIPP0035()
        Return SIPP0035BO.SIPP0035Save(oCustomClass)
    End Function

    Public Function SIPP0035Add(ByVal oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035
        Dim SIPP0035BO As ISIPP0035
        SIPP0035BO = ComponentFactory.CreateSIPP0035()
        Return SIPP0035BO.SIPP0035Add(oCustomClass)
    End Function
    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035
        Dim SIPP0035BO As ISIPP0035
        SIPP0035BO = ComponentFactory.CreateSIPP0035()
        Return SIPP0035BO.GetCbo(oCustomClass)
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0035) As Parameter.SIPP0035
        Dim SIPP0035BO As ISIPP0035
        SIPP0035BO = ComponentFactory.CreateSIPP0035()
        Return SIPP0035BO.GetCboBulandataSIPP(oCustomClass)
    End Function

    Public Function SIPP0035Delete(ByVal oCustomClass As Parameter.SIPP0035) As String
        Dim SIPP0035BO As ISIPP0035
        SIPP0035BO = ComponentFactory.CreateSIPP0035()
        Return SIPP0035BO.SIPP0035Delete(oCustomClass)
    End Function
End Class

