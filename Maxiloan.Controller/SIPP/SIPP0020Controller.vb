﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP0020Controller
    Public Function GetSIPP0020(ByVal oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020
        Dim SIPP0020BO As ISIPP0020
        SIPP0020BO = ComponentFactory.CreateSIPP0020()
        Return SIPP0020BO.GetSIPP0020(oCustomClass)
    End Function

    Public Function GetSelectSIPP0020(ByVal oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020
        Dim SIPP0020BO As ISIPP0020
        SIPP0020BO = ComponentFactory.CreateSIPP0020()
        Return SIPP0020BO.GetSIPP0020(oCustomClass)
    End Function

    Public Function SIPP0020Edit(ByVal oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020
        Dim SIPP0020BO As ISIPP0020
        SIPP0020BO = ComponentFactory.CreateSIPP0020()
        Return SIPP0020BO.SIPP0020Edit(oCustomClass)
    End Function

    Public Function SIPP0020Save(ByVal oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020
        Dim SIPP0020BO As ISIPP0020
        SIPP0020BO = ComponentFactory.CreateSIPP0020()
        Return SIPP0020BO.SIPP0020Save(oCustomClass)
    End Function

    Public Function SIPP0020Add(ByVal oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020
        Dim SIPP0020BO As ISIPP0020
        SIPP0020BO = ComponentFactory.CreateSIPP0020()
        Return SIPP0020BO.SIPP0020Add(oCustomClass)
    End Function
    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020
        Dim SIPP0020BO As ISIPP0020
        SIPP0020BO = ComponentFactory.CreateSIPP0020()
        Return SIPP0020BO.GetCbo(oCustomClass)
    End Function
    Public Function GetCboBUlanDataSIPP(ByVal oCustomClass As Parameter.SIPP0020) As Parameter.SIPP0020
        Dim SIPP0020BO As ISIPP0020
        SIPP0020BO = ComponentFactory.CreateSIPP0020()
        Return SIPP0020BO.GetCboBUlanDataSIPP(oCustomClass)
    End Function

    Public Function SIPP0020Delete(ByVal oCustomClass As Parameter.SIPP0020) As String
        Dim SIPP0020BO As ISIPP0020
        SIPP0020BO = ComponentFactory.CreateSIPP0020()
        Return SIPP0020BO.SIPP0020Delete(oCustomClass)
    End Function
End Class
