﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP0030Controller
    Public Function GetSIPP0030(ByVal ocustomClass As Parameter.SIPP0030) As Parameter.SIPP0030
        Dim SIPP0030BO As ISIPP0030
        SIPP0030BO = ComponentFactory.CreateSIPP0030()
        Return SIPP0030BO.GetSIPP0030(ocustomClass)
    End Function

    Public Function SIPP0030Add(ByVal ocustomClass As Parameter.SIPP0030)
        Dim SIPP0030BO As ISIPP0030
        SIPP0030BO = ComponentFactory.CreateSIPP0030()
        Return SIPP0030BO.SIPP0030SaveAdd(ocustomClass)
    End Function
    Public Sub SIPP0030Update(ByVal ocustomClass As Parameter.SIPP0030)
        Dim SIPP0030BO As ISIPP0030
        SIPP0030BO = ComponentFactory.CreateSIPP0030()
        SIPP0030BO.SIPP0030SaveEdit(ocustomClass)
    End Sub
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0030) As Parameter.SIPP0030
        Dim SIPP0030BO As ISIPP0030
        SIPP0030BO = ComponentFactory.CreateSIPP0030()
        Return SIPP0030BO.GetCboBulandataSIPP(oCustomClass)
    End Function
    Public Function GetSIPP0030Edit(ByVal ocustomClass As Parameter.SIPP0030) As Parameter.SIPP0030
        Dim SIPP0030BO As ISIPP0030
        SIPP0030BO = ComponentFactory.CreateSIPP0030()
        Return SIPP0030BO.GetSIPP0030Edit(ocustomClass)
    End Function
    Public Function SIPP0030Delete(ByVal ocustomClass As Parameter.SIPP0030) As String
        Dim SIPP0030BO As ISIPP0030
        SIPP0030BO = ComponentFactory.CreateSIPP0030()
        Return SIPP0030BO.SIPP0030Delete(ocustomClass)
    End Function
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP0030) As Parameter.SIPP0030
        Dim SIPP0030BO As ISIPP0030
        SIPP0030BO = ComponentFactory.CreateSIPP0030()
        Return SIPP0030BO.GetCbo(ocustomClass)
    End Function

End Class
