﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP1110Controller
    Public Function GetSIPP1110(ByVal oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110
        Dim SIPP1110BO As ISIPP1110
        SIPP1110BO = ComponentFactory.CreateSIPP1110()
        Return SIPP1110BO.GetSIPP1110(oCustomClass)
    End Function

    Public Function GetSelectSIPP1110(ByVal oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110
        Dim SIPP1110BO As ISIPP1110
        SIPP1110BO = ComponentFactory.CreateSIPP1110()
        Return SIPP1110BO.GetSIPP1110(oCustomClass)
    End Function

    Public Function SIPP1110Edit(ByVal oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110
        Dim SIPP1110BO As ISIPP1110
        SIPP1110BO = ComponentFactory.CreateSIPP1110()
        Return SIPP1110BO.SIPP1110Edit(oCustomClass)
    End Function

    Public Function SIPP1110Save(ByVal oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110
        Dim SIPP1110BO As ISIPP1110
        SIPP1110BO = ComponentFactory.CreateSIPP1110()
        Return SIPP1110BO.SIPP1110Save(oCustomClass)
    End Function

    Public Function SIPP1110Add(ByVal oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110
        Dim SIPP1110BO As ISIPP1110
        SIPP1110BO = ComponentFactory.CreateSIPP1110()
        Return SIPP1110BO.SIPP1110Add(oCustomClass)
    End Function
    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110
        Dim SIPP1110BO As ISIPP1110
        SIPP1110BO = ComponentFactory.CreateSIPP1110()
        Return SIPP1110BO.GetCbo(oCustomClass)
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP1110) As Parameter.SIPP1110
        Dim SIPP1110BO As ISIPP1110
        SIPP1110BO = ComponentFactory.CreateSIPP1110()
        Return SIPP1110BO.GetCboBulandataSIPP(oCustomClass)
    End Function
    Public Function SIPP1110Delete(ByVal oCustomClass As Parameter.SIPP1110) As String
        Dim SIPP1110BO As ISIPP1110
        SIPP1110BO = ComponentFactory.CreateSIPP1110()
        Return SIPP1110BO.SIPP1110Delete(oCustomClass)
    End Function
End Class

