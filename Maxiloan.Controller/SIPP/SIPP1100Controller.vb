﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Parameter
#End Region

Public Class SIPP1100Controller
    Public Function GetSIPP1100List(ByVal ocustomClass As Parameter.SIPP1100) As Parameter.SIPP1100
        Dim SIPP1100BO As ISIPP1100
        SIPP1100BO = ComponentFactory.CreateSIPP1100()
        Return SIPP1100BO.GetSIPP1100List(ocustomClass)
    End Function

    Public Function SIPP1100SaveAdd(ByVal ocustomClass As Parameter.SIPP1100)
        Dim SIPP1100BO As ISIPP1100
        SIPP1100BO = ComponentFactory.CreateSIPP1100()
        Return SIPP1100BO.SIPP1100SaveAdd(ocustomClass)
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP1100) As Parameter.SIPP1100
        Dim SIPP1100BO As ISIPP1100
        SIPP1100BO = ComponentFactory.CreateSIPP1100()
        Return SIPP1100BO.GetCboBulandataSIPP(oCustomClass)
    End Function
    Public Function SIPP1100Edit(ByVal ocustomClass As Parameter.SIPP1100) As Parameter.SIPP1100
        Dim SIPP1100BO As ISIPP1100
        SIPP1100BO = ComponentFactory.CreateSIPP1100()
        Return SIPP1100BO.GetSIPP1100Edit(ocustomClass)
    End Function
    Public Function SIPP1100Delete(ByVal ocustomClass As Parameter.SIPP1100) As String
        Dim SIPP1100BO As ISIPP1100
        SIPP1100BO = ComponentFactory.CreateSIPP1100()
        Return SIPP1100BO.SIPP1100Delete(ocustomClass)
    End Function
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP1100) As Parameter.SIPP1100
        'Dim SIPP1100BO As ISIPP1100
        'SIPP1100BO = ComponentFactory.CreateSIPP1100()
        'Return SIPP1100BO.GetCbo(ocustomClass)
    End Function

    Public Function SIPP1100SaveEdit(ocustomClass As SIPP1100) As SIPP1100
        Dim SIPP1100BO As ISIPP1100
        SIPP1100BO = ComponentFactory.CreateSIPP1100()
        SIPP1100BO.SIPP1100SaveEdit(ocustomClass)
    End Function
End Class
