﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP2200Controller
    Public Function GetSIPP2200(ByVal ocustomClass As Parameter.SIPP2200) As Parameter.SIPP2200
        Dim SIPP2200BO As ISIPP2200
        SIPP2200BO = ComponentFactory.CreateSIPP2200()
        Return SIPP2200BO.GetSIPP2200(ocustomClass)
    End Function

    Public Function SIPP2200Add(ByVal ocustomClass As Parameter.SIPP2200)
        Dim SIPP2200BO As ISIPP2200
        SIPP2200BO = ComponentFactory.CreateSIPP2200()
        Return SIPP2200BO.SIPP2200SaveAdd(ocustomClass)
    End Function
    Public Sub SIPP2200Update(ByVal ocustomClass As Parameter.SIPP2200)
        Dim SIPP2200BO As ISIPP2200
        SIPP2200BO = ComponentFactory.CreateSIPP2200()
        SIPP2200BO.SIPP2200SaveEdit(ocustomClass)
    End Sub
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP2200) As Parameter.SIPP2200
        Dim SIPP2200BO As ISIPP2200
        SIPP2200BO = ComponentFactory.CreateSIPP2200()
        Return SIPP2200BO.GetCboBulandataSIPP(oCustomClass)
    End Function
    Public Function GetSIPP2200Edit(ByVal ocustomClass As Parameter.SIPP2200) As Parameter.SIPP2200
        Dim SIPP2200BO As ISIPP2200
        SIPP2200BO = ComponentFactory.CreateSIPP2200()
        Return SIPP2200BO.GetSIPP2200Edit(ocustomClass)
    End Function
    Public Function SIPP2200Delete(ByVal ocustomClass As Parameter.SIPP2200) As String
        Dim SIPP2200BO As ISIPP2200
        SIPP2200BO = ComponentFactory.CreateSIPP2200()
        Return SIPP2200BO.SIPP2200Delete(ocustomClass)
    End Function
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP2200) As Parameter.SIPP2200
        Dim SIPP2200BO As ISIPP2200
        SIPP2200BO = ComponentFactory.CreateSIPP2200()
        Return SIPP2200BO.GetCbo(ocustomClass)
    End Function

End Class
