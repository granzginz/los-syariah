﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP1300Controller
    Public Function GetSIPP1300(ByVal oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300
        Dim SIPP1300BO As ISIPP1300
        SIPP1300BO = ComponentFactory.CreateSIPP1300()
        Return SIPP1300BO.GetSIPP1300(oCustomClass)
    End Function

    Public Function GetSelectSIPP1300(ByVal oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300
        Dim SIPP1300BO As ISIPP1300
        SIPP1300BO = ComponentFactory.CreateSIPP1300()
        Return SIPP1300BO.GetSIPP1300(oCustomClass)
    End Function

    Public Function SIPP1300Edit(ByVal oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300
        Dim SIPP1300BO As ISIPP1300
        SIPP1300BO = ComponentFactory.CreateSIPP1300()
        Return SIPP1300BO.SIPP1300Edit(oCustomClass)
    End Function

    Public Function SIPP1300Save(ByVal oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300
        Dim SIPP1300BO As ISIPP1300
        SIPP1300BO = ComponentFactory.CreateSIPP1300()
        Return SIPP1300BO.SIPP1300Save(oCustomClass)
    End Function

    Public Function SIPP1300Add(ByVal oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300
        Dim SIPP1300BO As ISIPP1300
        SIPP1300BO = ComponentFactory.CreateSIPP1300()
        Return SIPP1300BO.SIPP1300Add(oCustomClass)
    End Function
    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300
        Dim SIPP1300BO As ISIPP1300
        SIPP1300BO = ComponentFactory.CreateSIPP1300()
        Return SIPP1300BO.GetCbo(oCustomClass)
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP1300) As Parameter.SIPP1300
        Dim SIPP1300BO As ISIPP1300
        SIPP1300BO = ComponentFactory.CreateSIPP1300()
        Return SIPP1300BO.GetCboBulandataSIPP(oCustomClass)
    End Function

    Public Function SIPP1300Delete(ByVal oCustomClass As Parameter.SIPP1300) As String
        Dim SIPP1300BO As ISIPP1300
        SIPP1300BO = ComponentFactory.CreateSIPP1300()
        Return SIPP1300BO.SIPP1300Delete(oCustomClass)
    End Function
End Class

