﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP0046Controller
    Public Function GetSIPP0046(ByVal oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046
        Dim SIPP0046BO As ISIPP0046
        SIPP0046BO = ComponentFactory.CreateSIPP0046()
        Return SIPP0046BO.GetSIPP0046(oCustomClass)
    End Function

    Public Function GetSelectSIPP0046(ByVal oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046
        Dim SIPP0046BO As ISIPP0046
        SIPP0046BO = ComponentFactory.CreateSIPP0046()
        Return SIPP0046BO.GetSIPP0046(oCustomClass)
    End Function

    Public Function SIPP0046Edit(ByVal oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046
        Dim SIPP0046BO As ISIPP0046
        SIPP0046BO = ComponentFactory.CreateSIPP0046()
        Return SIPP0046BO.SIPP0046Edit(oCustomClass)
    End Function

    Public Function SIPP0046Save(ByVal oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046
        Dim SIPP0046BO As ISIPP0046
        SIPP0046BO = ComponentFactory.CreateSIPP0046()
        Return SIPP0046BO.SIPP0046Save(oCustomClass)
    End Function

    Public Function SIPP0046Add(ByVal oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046
        Dim SIPP0046BO As ISIPP0046
        SIPP0046BO = ComponentFactory.CreateSIPP0046()
        Return SIPP0046BO.SIPP0046Add(oCustomClass)
    End Function
    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046
        Dim SIPP0046BO As ISIPP0046
        SIPP0046BO = ComponentFactory.CreateSIPP0046()
        Return SIPP0046BO.GetCbo(oCustomClass)
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0046) As Parameter.SIPP0046
        Dim SIPP0046BO As ISIPP0046
        SIPP0046BO = ComponentFactory.CreateSIPP0046()
        Return SIPP0046BO.GetCboBulandataSIPP(oCustomClass)
    End Function

    Public Function SIPP0046Delete(ByVal oCustomClass As Parameter.SIPP0046) As String
        Dim SIPP0046BO As ISIPP0046
        SIPP0046BO = ComponentFactory.CreateSIPP0046()
        Return SIPP0046BO.SIPP0046Delete(oCustomClass)
    End Function
End Class
