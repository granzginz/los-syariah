﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP2490Controller
    Public Function GetSIPP2490(ByVal oCustomClass As Parameter.SIPP2490) As Parameter.SIPP2490
        Dim SIPP2490BO As ISIPP2490
        SIPP2490BO = ComponentFactory.CreateSIPP2490()
        Return SIPP2490BO.GetSIPP2490(oCustomClass)
    End Function

    Public Function GetSelectSIPP2490(ByVal oCustomClass As Parameter.SIPP2490) As Parameter.SIPP2490
        Dim SIPP2490BO As ISIPP2490
        SIPP2490BO = ComponentFactory.CreateSIPP2490()
        Return SIPP2490BO.GetSIPP2490(oCustomClass)
    End Function

    Public Function SIPP2490Edit(ByVal oCustomClass As Parameter.SIPP2490) As Parameter.SIPP2490
        Dim SIPP2490BO As ISIPP2490
        SIPP2490BO = ComponentFactory.CreateSIPP2490()
        Return SIPP2490BO.SIPP2490Edit(oCustomClass)
    End Function

    Public Function SIPP2490Save(ByVal oCustomClass As Parameter.SIPP2490) As Parameter.SIPP2490
        Dim SIPP2490BO As ISIPP2490
        SIPP2490BO = ComponentFactory.CreateSIPP2490()
        Return SIPP2490BO.SIPP2490Save(oCustomClass)
    End Function

    Public Function SIPP2490Add(ByVal oCustomClass As Parameter.SIPP2490) As Parameter.SIPP2490
        Dim SIPP2490BO As ISIPP2490
        SIPP2490BO = ComponentFactory.CreateSIPP2490()
        Return SIPP2490BO.SIPP2490Add(oCustomClass)
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP2490) As Parameter.SIPP2490
        Dim SIPP2490BO As ISIPP2490
        SIPP2490BO = ComponentFactory.CreateSIPP2490()
        Return SIPP2490BO.GetCbo(oCustomClass)
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP2490) As Parameter.SIPP2490
        Dim SIPP2490BO As ISIPP2490
        SIPP2490BO = ComponentFactory.CreateSIPP2490()
        Return SIPP2490BO.GetCboBulandataSIPP(oCustomClass)
    End Function

    Public Function SIPP2490Delete(ByVal oCustomClass As Parameter.SIPP2490) As String
        Dim SIPP2490BO As ISIPP2490
        SIPP2490BO = ComponentFactory.CreateSIPP2490()
        Return SIPP2490BO.SIPP2490Delete(oCustomClass)
    End Function
End Class
