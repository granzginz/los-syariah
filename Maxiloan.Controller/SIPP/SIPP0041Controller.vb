﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Parameter
#End Region

Public Class SIPP0041Controller
    Public Function GetSIPP0041Edit(ByVal ocustomClass As Parameter.SIPP0041) As Parameter.SIPP0041
        Dim SIPP0041BO As ISIPP0041
        SIPP0041BO = ComponentFactory.CreateSIPP0041()
        Return SIPP0041BO.GetSIPP0041Edit(ocustomClass)
    End Function

    Public Function SIPP0041Add(ByVal ocustomClass As Parameter.SIPP0041)
        Dim SIPP0041BO As ISIPP0041
        SIPP0041BO = ComponentFactory.CreateSIPP0041()
        Return SIPP0041BO.SIPP0041SaveAdd(ocustomClass)
    End Function

    Public Function GetSIPP0041List(ByVal ocustomClass As Parameter.SIPP0041) As Parameter.SIPP0041
        Dim SIPP0041BO As ISIPP0041
        SIPP0041BO = ComponentFactory.CreateSIPP0041()
        Return SIPP0041BO.GetSIPP0041List(ocustomClass)
    End Function
    Public Function SIPP0041Delete(ByVal ocustomClass As Parameter.SIPP0041) As String
        Dim SIPP0041BO As ISIPP0041
        SIPP0041BO = ComponentFactory.CreateSIPP0041()
        Return SIPP0041BO.SIPP0041Delete(ocustomClass)
    End Function
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP0041) As Parameter.SIPP0041
        Dim SIPP0041BO As ISIPP0041
        SIPP0041BO = ComponentFactory.CreateSIPP0041()
        Return SIPP0041BO.GetCbo(ocustomClass)
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0041) As Parameter.SIPP0041
        Dim SIPP0041BO As ISIPP0041
        SIPP0041BO = ComponentFactory.CreateSIPP0041()
        Return SIPP0041BO.GetCboBulandataSIPP(oCustomClass)
    End Function
    Function SIPP0041AddEdit(ocustomClass As SIPP0041) As SIPP0041
        Dim SIPP0041BO As ISIPP0041
        SIPP0041BO = ComponentFactory.CreateSIPP0041()
        SIPP0041BO.SIPP0041AddEdit(ocustomClass)
    End Function
End Class
