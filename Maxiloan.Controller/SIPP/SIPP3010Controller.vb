﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP3010Controller
    Public Function GetSIPP3010(ByVal ocustomClass As Parameter.SIPP3010) As Parameter.SIPP3010
        Dim SIPP3010BO As ISIPP3010
        SIPP3010BO = ComponentFactory.CreateSIPP3010()
        Return SIPP3010BO.GetSIPP3010(ocustomClass)
    End Function

    Public Function SIPP3010Add(ByVal ocustomClass As Parameter.SIPP3010)
        Dim SIPP3010BO As ISIPP3010
        SIPP3010BO = ComponentFactory.CreateSIPP3010()
        Return SIPP3010BO.SIPP3010SaveAdd(ocustomClass)
    End Function
    Public Sub SIPP3010Update(ByVal ocustomClass As Parameter.SIPP3010)
        Dim SIPP3010BO As ISIPP3010
        SIPP3010BO = ComponentFactory.CreateSIPP3010()
        SIPP3010BO.SIPP3010SaveEdit(ocustomClass)
    End Sub
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP3010) As Parameter.SIPP3010
        Dim SIPP3010BO As ISIPP3010
        SIPP3010BO = ComponentFactory.CreateSIPP3010()
        Return SIPP3010BO.GetCboBulandataSIPP(oCustomClass)
    End Function
    Public Function GetSIPP3010Edit(ByVal ocustomClass As Parameter.SIPP3010) As Parameter.SIPP3010
        Dim SIPP3010BO As ISIPP3010
        SIPP3010BO = ComponentFactory.CreateSIPP3010()
        Return SIPP3010BO.GetSIPP3010Edit(ocustomClass)
    End Function
    Public Function SIPP3010Delete(ByVal ocustomClass As Parameter.SIPP3010) As String
        Dim SIPP3010BO As ISIPP3010
        SIPP3010BO = ComponentFactory.CreateSIPP3010()
        Return SIPP3010BO.SIPP3010Delete(ocustomClass)
    End Function
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP3010) As Parameter.SIPP3010
        Dim SIPP3010BO As ISIPP3010
        SIPP3010BO = ComponentFactory.CreateSIPP3010()
        Return SIPP3010BO.GetCbo(ocustomClass)
    End Function

End Class
