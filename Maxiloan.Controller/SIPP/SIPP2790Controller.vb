﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP2790Controller

    Public Function GetSIPP2790(ByVal oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790
        Dim SIPP2790BO As ISIPP2790
        SIPP2790BO = ComponentFactory.CreateSIPP2790()
        Return SIPP2790BO.GetSIPP2790(oCustomClass)
    End Function

    Public Function GetSelectSIPP2790(ByVal oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790
        Dim SIPP2790BO As ISIPP2790
        SIPP2790BO = ComponentFactory.CreateSIPP2790()
        Return SIPP2790BO.GetSIPP2790(oCustomClass)
    End Function

    Public Function SIPP2790Edit(ByVal oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790
        Dim SIPP2790BO As ISIPP2790
        SIPP2790BO = ComponentFactory.CreateSIPP2790()
        Return SIPP2790BO.SIPP2790Edit(oCustomClass)
    End Function

    Public Function SIPP2790Save(ByVal oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790
        Dim SIPP2790BO As ISIPP2790
        SIPP2790BO = ComponentFactory.CreateSIPP2790()
        Return SIPP2790BO.SIPP2790Save(oCustomClass)
    End Function

    Public Function SIPP2790Add(ByVal oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790
        Dim SIPP2790BO As ISIPP2790
        SIPP2790BO = ComponentFactory.CreateSIPP2790()
        Return SIPP2790BO.SIPP2790Add(oCustomClass)
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790
        Dim SIPP2790BO As ISIPP2790
        SIPP2790BO = ComponentFactory.CreateSIPP2790()
        Return SIPP2790BO.GetCbo(oCustomClass)
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP2790) As Parameter.SIPP2790
        Dim SIPP2790BO As ISIPP2790
        SIPP2790BO = ComponentFactory.CreateSIPP2790()
        Return SIPP2790BO.GetCboBulandataSIPP(oCustomClass)
    End Function

    Public Function SIPP2790Delete(ByVal oCustomClass As Parameter.SIPP2790) As String
        Dim SIPP2790BO As ISIPP2790
        SIPP2790BO = ComponentFactory.CreateSIPP2790()
        Return SIPP2790BO.SIPP2790Delete(oCustomClass)
    End Function
End Class
