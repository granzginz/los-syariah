﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Parameter
#End Region

Public Class SIPP1200Controller
    Public Function GetSIPP1200List(ByVal ocustomClass As Parameter.SIPP1200) As Parameter.SIPP1200
        Dim SIPP1200BO As ISIPP1200
        SIPP1200BO = ComponentFactory.CreateSIPP1200()
        Return SIPP1200BO.GetSIPP1200List(ocustomClass)
    End Function

    Public Function SIPP1200SaveEdit(ByVal ocustomClass As Parameter.SIPP1200)
        Dim SIPP1200BO As ISIPP1200
        SIPP1200BO = ComponentFactory.CreateSIPP1200()
        Return SIPP1200BO.SIPP1200SaveEdit(ocustomClass)
    End Function


    Public Function GetSIPP1200Edit(ByVal ocustomClass As Parameter.SIPP1200) As Parameter.SIPP1200
        Dim SIPP1200BO As ISIPP1200
        SIPP1200BO = ComponentFactory.CreateSIPP1200()
        Return SIPP1200BO.GetSIPP1200Edit(ocustomClass)
    End Function
    Public Function SIPP1200Delete(ByVal ocustomClass As Parameter.SIPP1200) As String
        Dim SIPP1200BO As ISIPP1200
        SIPP1200BO = ComponentFactory.CreateSIPP1200()
        Return SIPP1200BO.SIPP1200Delete(ocustomClass)
    End Function
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP1200) As Parameter.SIPP1200
        'Dim SIPP1200BO As ISIPP1200
        'SIPP1200BO = ComponentFactory.CreateSIPP1200()
        'Return SIPP1200BO.GetCbo(ocustomClass)
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP1200) As Parameter.SIPP1200
        Dim SIPP1200BO As ISIPP1200
        SIPP1200BO = ComponentFactory.CreateSIPP1200()
        Return SIPP1200BO.GetCboBulandataSIPP(oCustomClass)
    End Function

    Public Function SIPP1200SaveAdd(ocustomClass As SIPP1200) As SIPP1200
        Dim SIPP1200BO As ISIPP1200
        SIPP1200BO = ComponentFactory.CreateSIPP1200()
        SIPP1200BO.SIPP1200SaveAdd(ocustomClass)
    End Function
End Class
