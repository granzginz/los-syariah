﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP0000Controller
    Public Function GetSIPP0000(ByVal oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000
        Dim SIPP0000BO As ISIPP0000
        SIPP0000BO = ComponentFactory.CreateSIPP0000()
        Return SIPP0000BO.GetSIPP0000(oCustomClass)
    End Function

    Public Function GetSelectSIPP0000(ByVal oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000
        Dim SIPP0000BO As ISIPP0000
        SIPP0000BO = ComponentFactory.CreateSIPP0000()
        Return SIPP0000BO.GetSIPP0000(oCustomClass)
    End Function

    Public Function SIPP0000Edit(ByVal oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000
        Dim SIPP0000BO As ISIPP0000
        SIPP0000BO = ComponentFactory.CreateSIPP0000()
        Return SIPP0000BO.SIPP0000Edit(oCustomClass)
    End Function

    Public Function SIPP0000Save(ByVal oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000
        Dim SIPP0000BO As ISIPP0000
        SIPP0000BO = ComponentFactory.CreateSIPP0000()
        Return SIPP0000BO.SIPP0000Save(oCustomClass)
    End Function

    Public Function SIPP0000Add(ByVal oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000
        Dim SIPP0000BO As ISIPP0000
        SIPP0000BO = ComponentFactory.CreateSIPP0000()
        Return SIPP0000BO.SIPP0000Add(oCustomClass)
    End Function
    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000
        Dim SIPP0000BO As ISIPP0000
        SIPP0000BO = ComponentFactory.CreateSIPP0000()
        Return SIPP0000BO.GetCbo(oCustomClass)
    End Function
    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0000) As Parameter.SIPP0000
        Dim SIPP0000BO As ISIPP0000
        SIPP0000BO = ComponentFactory.CreateSIPP0000()
        Return SIPP0000BO.GetCboBulandataSIPP(oCustomClass)
    End Function

    Public Function SIPP0000Delete(ByVal oCustomClass As Parameter.SIPP0000) As String
        Dim SIPP0000BO As ISIPP0000
        SIPP0000BO = ComponentFactory.CreateSIPP0000()
        Return SIPP0000BO.SIPP0000Delete(oCustomClass)
    End Function
End Class
