﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SIPP2600Controller
    Public Function GetSIPP2600(ByVal oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600
        Dim SIPP2600BO As ISIPP2600
        SIPP2600BO = ComponentFactory.CreateSIPP2600()
        Return SIPP2600BO.GetSIPP2600(oCustomClass)
    End Function

    Public Function GetSelectSIPP2600(ByVal oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600
        Dim SIPP2600BO As ISIPP2600
        SIPP2600BO = ComponentFactory.CreateSIPP2600()
        Return SIPP2600BO.GetSIPP2600(oCustomClass)
    End Function

    Public Function SIPP2600Edit(ByVal oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600
        Dim SIPP2600BO As ISIPP2600
        SIPP2600BO = ComponentFactory.CreateSIPP2600()
        Return SIPP2600BO.SIPP2600Edit(oCustomClass)
    End Function

    Public Function SIPP2600Save(ByVal oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600
        Dim SIPP2600BO As ISIPP2600
        SIPP2600BO = ComponentFactory.CreateSIPP2600()
        Return SIPP2600BO.SIPP2600Save(oCustomClass)
    End Function

    Public Function SIPP2600Add(ByVal oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600
        Dim SIPP2600BO As ISIPP2600
        SIPP2600BO = ComponentFactory.CreateSIPP2600()
        Return SIPP2600BO.SIPP2600Add(oCustomClass)
    End Function
    Public Function GetCbo(ByVal oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600
        Dim SIPP2600BO As ISIPP2600
        SIPP2600BO = ComponentFactory.CreateSIPP2600()
        Return SIPP2600BO.GetCbo(oCustomClass)
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP2600) As Parameter.SIPP2600
        Dim SIPP2600BO As ISIPP2600
        SIPP2600BO = ComponentFactory.CreateSIPP2600()
        Return SIPP2600BO.GetCboBulandataSIPP(oCustomClass)
    End Function

    Public Function SIPP2600Delete(ByVal oCustomClass As Parameter.SIPP2600) As String
        Dim SIPP2600BO As ISIPP2600
        SIPP2600BO = ComponentFactory.CreateSIPP2600()
        Return SIPP2600BO.SIPP2600Delete(oCustomClass)
    End Function
End Class

