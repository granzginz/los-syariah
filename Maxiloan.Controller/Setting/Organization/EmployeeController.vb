

#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class EmployeeController
    Function ListEmployee(ByVal Employee As Parameter.Employee) As Parameter.Employee
        Dim oEmployee As IEmployee
        oEmployee = ComponentFactory.CreateEmployee()
        Return oEmployee.EmployeeList(Employee)
    End Function

    Function DeleteEmployee(ByVal Employee As Parameter.Employee) As Parameter.Employee
        Dim oEmployee As IEmployee
        oEmployee = ComponentFactory.CreateEmployee()
        oEmployee.EmployeeDelete(Employee)
    End Function

    Public Sub AddEmployee(ByVal Employee As Parameter.Employee, ByVal oAddress As Parameter.Address, ByVal oPerson As Parameter.Personal)
        Dim oEmployee As IEmployee
        oEmployee = ComponentFactory.CreateEmployee()
        oEmployee.EmployeeAdd(Employee, oAddress, oPerson)
    End Sub

    Function EditEmployee(ByVal Employee As Parameter.Employee, ByVal oAddress As Parameter.Address, ByVal oPerson As Parameter.Personal) As Parameter.Employee
        Dim oEmployee As IEmployee
        oEmployee = ComponentFactory.CreateEmployee()
        oEmployee.EmployeeEdit(Employee, oAddress, oPerson)
    End Function

    Function getCombo(ByVal strCon As String, ByVal strCombo As String, ByVal BranchID As String) As DataTable
        Dim oEmployee As IEmployee
        oEmployee = ComponentFactory.CreateEmployee()
        Return oEmployee.GetCombo(strCon, strCombo, BranchID)
    End Function

    Function EmployeeByID(ByVal Employee As Parameter.Employee) As Parameter.Employee
        Dim oEmployee As IEmployee
        oEmployee = ComponentFactory.CreateEmployee()
        Return oEmployee.EmployeeByID(Employee)
    End Function

    Function EmployeeReport(ByVal Employee As Parameter.Employee) As DataTable
        Dim oEmployee As IEmployee
        oEmployee = ComponentFactory.CreateEmployee()
        Return oEmployee.EmployeeReport(Employee)
    End Function

    Function EmployeePindahCabang(ByVal Employee As Parameter.Employee) As Parameter.Employee
        Dim oEmployee As IEmployee
        oEmployee = ComponentFactory.CreateEmployee()
        oEmployee.EmployeePindahCabang(Employee)
    End Function
End Class
