﻿Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class AuthorizedSignerController


    Public Function GetAuthorizedSigner(ByVal ocustomClass As Parameter.AuthorizedSigner) As Parameter.AuthorizedSigner
        Dim AuthorizedSignerBO As IAuthorizedSigner
        AuthorizedSignerBO = ComponentFactory.CreateAuthorizedSigner()
        Return AuthorizedSignerBO.GetAuthorizedSigner(ocustomClass)
    End Function
    Public Function GetAuthorizedSignerList(ByVal ocustomClass As Parameter.AuthorizedSigner) As Parameter.AuthorizedSigner
        Dim AuthorizedSignerBO As IAuthorizedSigner
        AuthorizedSignerBO = ComponentFactory.CreateAuthorizedSigner()
        Return AuthorizedSignerBO.GetAuthorizedSignerList(ocustomClass)
    End Function

    Public Function AuthorizedSignerSaveAdd(ByVal ocustomClass As Parameter.AuthorizedSigner) As String
        Dim AuthorizedSignerBO As IAuthorizedSigner
        AuthorizedSignerBO = ComponentFactory.CreateAuthorizedSigner()
        Return AuthorizedSignerBO.AuthorizedSignerSaveAdd(ocustomClass)
    End Function

    Public Sub AuthorizedSignerSaveEdit(ByVal ocustomClass As Parameter.AuthorizedSigner)
        Dim AuthorizedSignerBO As IAuthorizedSigner
        AuthorizedSignerBO = ComponentFactory.CreateAuthorizedSigner()
        AuthorizedSignerBO.AuthorizedSignerSaveEdit(ocustomClass)
    End Sub

    Public Function AuthorizedSignerDelete(ByVal ocustomClass As Parameter.AuthorizedSigner) As String
        Dim AuthorizedSignerBO As IAuthorizedSigner
        AuthorizedSignerBO = ComponentFactory.CreateAuthorizedSigner()
        Return AuthorizedSignerBO.AuthorizedSignerDelete(ocustomClass)
    End Function


End Class
