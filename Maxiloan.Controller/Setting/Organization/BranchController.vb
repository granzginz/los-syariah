

#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class BranchController

    Function BranchList(ByVal oCustomClass As Parameter.Branch) As Parameter.Branch
        Dim oBranchList As IBranch
        oBranchList = ComponentFactory.CreateBranch()
        Return oBranchList.BranchList(oCustomClass)
    End Function

    Function BranchByID(ByVal oCustomClass As Parameter.Branch) As Parameter.Branch
        Dim oBranchList As IBranch
        oBranchList = ComponentFactory.CreateBranch()
        Return oBranchList.BranchByID(oCustomClass)
    End Function

    Sub BranchAdd(ByVal oCustomClass As Parameter.Branch, ByVal oAddress As Parameter.Address, ByVal oPerson As Parameter.Personal)
        Dim oBranch As IBranch
        oBranch = ComponentFactory.CreateBranch()
        oBranch.BranchAdd(oCustomClass, oAddress, oPerson)
    End Sub

    Sub BranchEdit(ByVal oCustomClass As Parameter.Branch, ByVal oAddress As Parameter.Address, ByVal oPerson As Parameter.Personal)
        Dim oBranch As IBranch
        oBranch = ComponentFactory.CreateBranch()
        oBranch.BranchEdit(oCustomClass, oAddress, oPerson)
    End Sub

    Sub BranchDelete(ByVal oCustomClass As Parameter.Branch)
        Dim oBranch As IBranch
        oBranch = ComponentFactory.CreateBranch()
        oBranch.BranchDelete(oCustomClass)
    End Sub


    Function GetCombo(ByVal strcon As String, ByVal strcombo As String) As DataTable
        Dim oBranch As IBranch
        oBranch = ComponentFactory.CreateBranch()
        Return oBranch.GetCombo(strcon, strcombo)
    End Function

    Function BranchReport(ByVal oCustomClass As Parameter.Branch) As Parameter.Branch
        Dim oBranch As IBranch
        oBranch = ComponentFactory.CreateBranch()
        Return oBranch.BranchReport(oCustomClass)
    End Function
    Public Function BranchBudgetPrint(ByVal customclass As Parameter.Branch) As Parameter.Branch
        Dim oBranch As IBranch
        oBranch = ComponentFactory.CreateBranch()
        Return oBranch.BranchBudgetPrint(customclass)
    End Function
    Public Function BranchBudgetListing(ByVal customclass As Parameter.Branch) As Parameter.Branch
        Dim oBranch As IBranch
        oBranch = ComponentFactory.CreateBranch()
        Return oBranch.BranchBudgetListing(customclass)
    End Function

    Public Function BranchForecastPrint(ByVal customclass As Parameter.Branch) As Parameter.Branch
        Dim oBranch As IBranch
        oBranch = ComponentFactory.CreateBranch()
        Return oBranch.BranchForecastPrint(customclass)
    End Function
    Public Function BranchForecastListing(ByVal customclass As Parameter.Branch) As Parameter.Branch
        Dim oBranch As IBranch
        oBranch = ComponentFactory.CreateBranch()
        Return oBranch.BranchForecastListing(customclass)
    End Function
End Class
