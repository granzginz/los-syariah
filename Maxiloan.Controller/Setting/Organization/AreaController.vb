

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AreaController
    Function AreaList(ByVal oCustomClass As Parameter.Area) As Parameter.Area
        Dim oAreaList As IArea
        oAreaList = ComponentFactory.CreateAreaMnt()
        Return oAreaList.ListArea(oCustomClass)
    End Function

    Function SaveArea(ByVal oCustomClass As Parameter.Area) As String
        Dim oSaveArea As IArea
        oSaveArea = ComponentFactory.CreateAreaMnt()
        Return oSaveArea.SaveArea(oCustomClass)
    End Function

    Function UpdateArea(ByVal oCustomClass As Parameter.Area) As String
        Dim oUpdateArea As IArea
        oUpdateArea = ComponentFactory.CreateAreaMnt()
        Return oUpdateArea.UpdateArea(oCustomClass)
    End Function

    Function DeleteArea(ByVal oCustomClass As Parameter.Area) As String
        Dim oDeleteArea As IArea
        oDeleteArea = ComponentFactory.CreateAreaMnt()
        Return oDeleteArea.DeleteArea(oCustomClass)
    End Function

    Function AreaInfo(ByVal oCustomClass As Parameter.Area) As Parameter.Area
        Dim oAreaInfo As IArea
        oAreaInfo = ComponentFactory.CreateAreaMnt()
        Return oAreaInfo.AreaInfo(oCustomClass)
    End Function

    Function ReportArea(ByVal oCustomClass As Parameter.Area) As Parameter.Area
        Dim oReportArea As IArea
        oReportArea = ComponentFactory.CreateAreaMnt()
        Return oReportArea.ReportArea(oCustomClass)
    End Function
End Class
