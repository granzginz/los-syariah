

#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class CompanyController

    Function ListCompany(ByVal Company As Parameter.Company) As Parameter.Company
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        Return oCompany.CompanyList(Company)
    End Function

    Function DeleteCompany(ByVal Company As Parameter.Company) As Parameter.Company
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        oCompany.CompanyDelete(Company)
    End Function

    Public Sub AddCompany(ByVal Company As Parameter.Company, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        oCompany.CompanyAdd(Company, oClassAddress, oClassPersonal)
    End Sub

    Function EditCompany(ByVal Company As Parameter.Company, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As Parameter.Company
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        oCompany.CompanyEdit(Company, oClassAddress, oClassPersonal)
    End Function

    Function ListCompanyByID(ByVal Company As Parameter.Company) As Parameter.Company
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        Return oCompany.CompanyListByID(Company)
    End Function

    Function ListCompanyReport(ByVal Company As Parameter.Company) As Parameter.Company
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        Return oCompany.CompanyReport(Company)
    End Function

    Function ListCompanyBODReport(ByVal Company As Parameter.CompanyBOD) As Parameter.CompanyBOD
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        Return oCompany.CompanyBODReport(Company)
    End Function

    Function ListCompanyCommisionerReport(ByVal Company As Parameter.CompanyCommisioner) As Parameter.CompanyCommisioner
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        Return oCompany.CompanyCommisionerReport(Company)
    End Function



#Region "BOD"

    Function ListCompanyBOD(ByVal Company As Parameter.CompanyBOD) As Parameter.CompanyBOD
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        Return oCompany.CompanyBODList(Company)
    End Function

    Function GetComboInputType(ByVal strcon As String) As DataTable
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        Return oCompany.GetComboInputType(strcon)
    End Function

    Function DeleteCompanyBOD(ByVal Company As Parameter.CompanyBOD) As Parameter.CompanyBOD
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        oCompany.CompanyBODDelete(Company)

    End Function

    Public Sub AddCompanyBOD(ByVal Company As Parameter.CompanyBOD, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        oCompany.CompanyBODAdd(Company, oClassAddress, oClassPersonal)
    End Sub

    Public Sub EditCompanyBOD(ByVal Company As Parameter.CompanyBOD, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        oCompany.CompanyBODEdit(Company, oClassAddress, oClassPersonal)
    End Sub

    Function ListCompanyBODByID(ByVal Company As Parameter.CompanyBOD) As Parameter.CompanyBOD
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        Return oCompany.CompanyBODListByID(Company)
    End Function

#End Region


#Region "Commisioner"

    Function ListCompanyCommisioner(ByVal Company As Parameter.CompanyCommisioner) As Parameter.CompanyCommisioner
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        Return oCompany.CompanyCommisionerList(Company)
    End Function

    Function DeleteCompanyCommisioner(ByVal Company As Parameter.CompanyCommisioner) As Parameter.CompanyCommisioner
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        oCompany.CompanyCommisionerDelete(Company)
    End Function

    Public Sub AddCompanyCommisioner(ByVal Company As Parameter.CompanyCommisioner, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        oCompany.CompanyCommisionerAdd(Company, oClassAddress, oClassPersonal)
    End Sub

    Public Sub EditCompanyCommisioner(ByVal Company As Parameter.CompanyCommisioner, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        oCompany.CompanyCommisionerEdit(Company, oClassAddress, oClassPersonal)
    End Sub

    Function ListCompanyCommisionerByID(ByVal Company As Parameter.CompanyCommisioner) As Parameter.CompanyCommisioner
        Dim oCompany As ICompany
        oCompany = ComponentFactory.CreateCompany()
        Return oCompany.CompanyCommisionerListByID(Company)

    End Function

#End Region



End Class
