Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Parameter


Public Class MasterTCController
    Public Function ListMasterTC(ByVal customClass As Parameter.MasterTC) As Parameter.MasterTC
        Dim MasterTCBO As IMasterTC
        MasterTCBO = ComponentFactory.CreateMasterTC()
        Return MasterTCBO.MasterTCList(customClass)
    End Function

    Public Function MasterTCByID(ByVal customClass As Parameter.MasterTC) As Parameter.MasterTC
        Dim MasterTCBO As IMasterTC
        MasterTCBO = ComponentFactory.CreateMasterTC()
        Return MasterTCBO.MasterTCByID(customClass)
    End Function

    Public Function ListMasterTCReport(ByVal customClass As Parameter.MasterTC) As Parameter.MasterTC
        Dim MasterTCBO As IMasterTC
        MasterTCBO = ComponentFactory.CreateMasterTC()
        Return MasterTCBO.MasterTCReport(customClass)
    End Function

    Public Sub AddMasterTC(ByVal customClass As Parameter.MasterTC)
        Dim MasterTCBO As IMasterTC
        MasterTCBO = ComponentFactory.CreateMasterTC()
        MasterTCBO.MasterTCAdd(customClass)
    End Sub

    Public Sub EditMasterTC(ByVal customClass As Parameter.MasterTC)
        Dim MasterTCBO As IMasterTC
        MasterTCBO = ComponentFactory.CreateMasterTC()
        MasterTCBO.MasterTCEdit(customClass)
    End Sub

    Public Sub DeleteMasterTC(ByVal customClass As Parameter.MasterTC)
        Dim MasterTCBO As IMasterTC
        MasterTCBO = ComponentFactory.CreateMasterTC()
        MasterTCBO.MasterTCDelete(customClass)
    End Sub





#Region "TC Check List"
    Public Function ListTCCheckList(ByVal customClass As Parameter.MasterTC) As Parameter.MasterTC
        Dim TCCheckListBO As IMasterTC
        TCCheckListBO = ComponentFactory.CreateMasterTC()
        Return TCCheckListBO.TCCLList(customClass)
    End Function

    Public Function TCCheckListByID(ByVal customClass As Parameter.MasterTC) As Parameter.MasterTC
        Dim TCCheckListBO As IMasterTC
        TCCheckListBO = ComponentFactory.CreateMasterTC()
        Return TCCheckListBO.TCCLList(customClass)
    End Function

    Public Function ListTCCheckListReport(ByVal customClass As Parameter.MasterTC) As Parameter.MasterTC
        Dim TCCheckListBO As IMasterTC
        TCCheckListBO = ComponentFactory.CreateMasterTC()
        Return TCCheckListBO.TCCLReport(customClass)
    End Function

    Public Sub AddTCCheckList(ByVal customClass As Parameter.MasterTC)
        Dim TCCheckListBO As IMasterTC
        TCCheckListBO = ComponentFactory.CreateMasterTC()
        TCCheckListBO.TCCLAdd(customClass)
    End Sub

    Public Sub EditTCCheckList(ByVal customClass As Parameter.MasterTC)
        Dim TCCheckListBO As IMasterTC
        TCCheckListBO = ComponentFactory.CreateMasterTC()
        TCCheckListBO.TCCLEdit(customClass)
    End Sub

    Public Sub DeleteTCCheckList(ByVal customClass As Parameter.MasterTC)
        Dim TCCheckListBO As IMasterTC
        TCCheckListBO = ComponentFactory.CreateMasterTC()
        TCCheckListBO.TCCLDelete(customClass)
    End Sub


#End Region





End Class
