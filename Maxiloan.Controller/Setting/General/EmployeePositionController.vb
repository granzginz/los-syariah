
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class EmployeePositionController

    Public Function EmployeePositionList(ByVal customClass As Parameter.EmployeePosition) As Parameter.EmployeePosition
        Dim EmployeePositionBO As IEmployeePosition
        EmployeePositionBO = ComponentFactory.CreateEmployeePosition()
        Return EmployeePositionBO.EmployeePositionList(customClass)
    End Function

    Public Function EmployeePositionReport(ByVal customClass As Parameter.EmployeePosition) As DataTable
        Dim EmployeePositionBO As IEmployeePosition
        EmployeePositionBO = ComponentFactory.CreateEmployeePosition()
        Return EmployeePositionBO.EmployeePositionReport(customClass)
    End Function

    Public Sub EmployeePositionAdd(ByVal customClass As Parameter.EmployeePosition)
        Dim EmployeePositionBO As IEmployeePosition
        EmployeePositionBO = ComponentFactory.CreateEmployeePosition()
        EmployeePositionBO.EmployeePositionAdd(customClass)
    End Sub

    Public Sub EmployeePositionEdit(ByVal customClass As Parameter.EmployeePosition)
        Dim EmployeePositionBO As IEmployeePosition
        EmployeePositionBO = ComponentFactory.CreateEmployeePosition()
        EmployeePositionBO.EmployeePositionEdit(customClass)
    End Sub

    Public Sub EmployeePositionDelete(ByVal customClass As Parameter.EmployeePosition)
        Dim EmployeePositionBO As IEmployeePosition
        EmployeePositionBO = ComponentFactory.CreateEmployeePosition()
        EmployeePositionBO.EmployeePositionDelete(customClass)
    End Sub

End Class
