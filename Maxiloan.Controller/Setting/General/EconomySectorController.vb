Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess


Public Class EconomySectorController
    Public Function GetEconomySector(ByVal customClass As Parameter.EconomySector) As Parameter.EconomySector
        Dim EconomySectorBO As IEconomySector
        EconomySectorBO = ComponentFactory.CreateEconomySector()
        Return EconomySectorBO.GetEconomySector(customClass)
    End Function
    Public Function GetEconomySectorReport(ByVal customClass As Parameter.EconomySector) As Parameter.EconomySector
        Dim EconomySectorBO As IEconomySector
        EconomySectorBO = ComponentFactory.CreateEconomySector()
        Return EconomySectorBO.GetEconomySectorReport(customClass)
    End Function
    Public Function EconomySectorAdd(ByVal customClass As Parameter.EconomySector) As String
        Dim EconomySectorBO As IEconomySector
        EconomySectorBO = ComponentFactory.CreateEconomySector()
        Return EconomySectorBO.EconomySectorSaveAdd(customClass)
    End Function

    Public Sub EconomySectorUpdate(ByVal customClass As Parameter.EconomySector)
        Dim EconomySectorBO As IEconomySector
        EconomySectorBO = ComponentFactory.CreateEconomySector()
        EconomySectorBO.EconomySectorSaveEdit(customClass)
    End Sub

    Public Function EconomySectorDelete(ByVal customClass As Parameter.EconomySector) As String
        Dim EconomySectorBO As IEconomySector
        EconomySectorBO = ComponentFactory.CreateEconomySector()
        Return EconomySectorBO.EconomySectorDelete(customClass)
    End Function

End Class

