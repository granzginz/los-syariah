
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class MasterController
    Public Function GetMaster(ByVal customClass As Parameter.Master) As Parameter.Master
        Dim MasterBO As IMaster
        MasterBO = ComponentFactory.CreateMaster()
        Return MasterBO.GetMaster(customClass)
    End Function
    Public Function GetMasterReport(ByVal customClass As Parameter.Master) As Parameter.Master
        Dim MasterBO As IMaster
        MasterBO = ComponentFactory.CreateMaster()
        Return MasterBO.GetMasterReport(customClass)
    End Function
    Public Function GetKeyWord(ByVal customClass As Parameter.Master) As Parameter.Master
        Dim MasterBO As IMaster
        MasterBO = ComponentFactory.CreateMaster()
        Return MasterBO.GetKeyWord(customClass)
    End Function

    Public Function MasterAdd(ByVal customClass As Parameter.Master) As String
        Dim MasterBO As IMaster
        MasterBO = ComponentFactory.CreateMaster()
        Return MasterBO.MasterSaveAdd(customClass)
    End Function

    Public Sub MasterUpdate(ByVal customClass As Parameter.Master)
        Dim MasterBO As IMaster
        MasterBO = ComponentFactory.CreateMaster()
        MasterBO.MasterSaveEdit(customClass)
    End Sub

    Public Function MasterDelete(ByVal customClass As Parameter.Master) As String
        Dim MasterBO As IMaster
        MasterBO = ComponentFactory.CreateMaster()
        Return MasterBO.MasterDelete(customClass)
    End Function

End Class

