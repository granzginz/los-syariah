
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class VendorController
    Public Function GetVendor(ByVal ocustomClass As Parameter.Vendor) As Parameter.Vendor
        Dim VendorBO As IVendor
        VendorBO = ComponentFactory.CreateVendor()
        Return VendorBO.GetVendor(ocustomClass)
    End Function
    Public Function GetVendorList(ByVal ocustomClass As Parameter.Vendor) As Parameter.Vendor
        Dim VendorBO As IVendor
        VendorBO = ComponentFactory.CreateVendor()
        Return VendorBO.GetVendorList(ocustomClass)
    End Function

    Public Function VendorSaveAdd(ByVal ocustomClass As Parameter.Vendor) As String
        Dim VendorBO As IVendor
        VendorBO = ComponentFactory.CreateVendor()
        Return VendorBO.VendorSaveAdd(ocustomClass)
    End Function

    Public Sub VendorSaveEdit(ByVal ocustomClass As Parameter.Vendor)
        Dim VendorBO As IVendor
        VendorBO = ComponentFactory.CreateVendor()
        VendorBO.VendorSaveEdit(ocustomClass)
    End Sub

    Public Function VendorDelete(ByVal ocustomClass As Parameter.Vendor) As String
        Dim VendorBO As IVendor
        VendorBO = ComponentFactory.CreateVendor()
        Return VendorBO.VendorDelete(ocustomClass)
    End Function

End Class

