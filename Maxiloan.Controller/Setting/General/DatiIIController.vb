
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class DatiIIController
    Public Function GetDati(ByVal ocustomClass As Parameter.Dati) As Parameter.Dati
        Dim DatiBO As IDatiII
        DatiBO = ComponentFactory.CreateDatiII()
        Return DatiBO.GetDati(ocustomClass)
    End Function
    Public Function GetDatiReport(ByVal ocustomClass As Parameter.Dati) As Parameter.Dati
        Dim DatiBO As IDatiII
        DatiBO = ComponentFactory.CreateDatiII()
        Return DatiBO.GetDatiReport(ocustomClass)
    End Function
    Public Function GetDatiList(ByVal ocustomClass As Parameter.Dati) As Parameter.Dati
        Dim DatiBO As IDatiII
        DatiBO = ComponentFactory.CreateDatiII()
        Return DatiBO.GetDatiList(ocustomClass)
    End Function

    Public Function DatiSaveAdd(ByVal ocustomClass As Parameter.Dati) As String
        Dim DatiBO As IDatiII
        DatiBO = ComponentFactory.CreateDatiII()
        Return DatiBO.DatiSaveAdd(ocustomClass)
    End Function

    Public Sub DatiSaveEdit(ByVal ocustomClass As Parameter.Dati)
        Dim DatiBO As IDatiII
        DatiBO = ComponentFactory.CreateDatiII()
        DatiBO.DatiSaveEdit(ocustomClass)
    End Sub

    Public Function DatiDelete(ByVal ocustomClass As Parameter.Dati) As String
        Dim DatiBO As IDatiII
        DatiBO = ComponentFactory.CreateDatiII()
        Return DatiBO.DatiDelete(ocustomClass)
    End Function

End Class

