﻿
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class MailTypeController
    Public Function GetMailType(ByVal ocustomClass As Parameter.MailType) As Parameter.MailType
        Dim MailTypeBO As IMailType
        MailTypeBO = ComponentFactory.CreateMailType()
        Return MailTypeBO.GetMailType(ocustomClass)
    End Function
    Public Function GetMailTypeReport(ByVal ocustomClass As Parameter.MailType) As Parameter.MailType
        Dim MailTypeBO As IMailType
        MailTypeBO = ComponentFactory.CreateMailType()
        Return MailTypeBO.GetMailTypeReport(ocustomClass)
    End Function
    Public Function Getsector(ByVal ocustomClass As Parameter.MailType) As Parameter.MailType
        Dim MailTypeBO As IMailType
        MailTypeBO = ComponentFactory.CreateMailType()
        Return MailTypeBO.GetSector(ocustomClass)
    End Function
    Public Function GetMailTypeEdit(ByVal ocustomClass As Parameter.MailType) As Parameter.MailType
        Dim MailTypeBO As IMailType
        MailTypeBO = ComponentFactory.CreateMailType()
        Return MailTypeBO.GetMailTypeEdit(ocustomClass)
    End Function

    Public Function MailTypeAdd(ByVal ocustomClass As Parameter.MailType) As String
        Dim MailTypeBO As IMailType
        MailTypeBO = ComponentFactory.CreateMailType()
        Return MailTypeBO.MailTypeSaveAdd(ocustomClass)
    End Function

    Public Sub MailTypeUpdate(ByVal ocustomClass As Parameter.MailType)
        Dim MailTypeBO As IMailType
        MailTypeBO = ComponentFactory.CreateMailType()
        MailTypeBO.MailTypeSaveEdit(ocustomClass)
    End Sub

    Public Function MailTypeDelete(ByVal ocustomClass As Parameter.MailType) As String
        Dim MailTypeBO As IMailType
        MailTypeBO = ComponentFactory.CreateMailType()
        Return MailTypeBO.MailTypeDelete(ocustomClass)
    End Function

End Class

