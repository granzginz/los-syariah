
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class PropinsiController
    Public Function GetPropinsi(ByVal ocustomClass As Parameter.Propinsi) As Parameter.Propinsi
        Dim PropinsiBO As IPropinsi
        PropinsiBO = ComponentFactory.CreatePropinsi()
        Return PropinsiBO.GetPropinsi(ocustomClass)
    End Function
    Public Function GetPropinsiList(ByVal ocustomClass As Parameter.Propinsi) As Parameter.Propinsi
        Dim PropinsiBO As IPropinsi
        PropinsiBO = ComponentFactory.CreatePropinsi()
        Return PropinsiBO.GetPropinsiList(ocustomClass)
    End Function
    Public Function GetPropinsiCombo(ByVal ocustomClass As Parameter.Propinsi) As DataTable
        Dim PropinsiBO As IPropinsi
        PropinsiBO = ComponentFactory.CreatePropinsi()
        Return PropinsiBO.GetPropinsiCombo(ocustomClass)
    End Function
    Public Function ProvinceSaveAdd(ByVal ocustomClass As Parameter.Propinsi) As String
        Dim PropinsiBO As IPropinsi
        PropinsiBO = ComponentFactory.CreatePropinsi()
        Return PropinsiBO.ProvinceSaveAdd(ocustomClass)
    End Function
    Public Sub ProvinceSaveEdit(ByVal ocustomClass As Parameter.Propinsi)
        Dim PropinsiBO As IPropinsi
        PropinsiBO = ComponentFactory.CreatePropinsi()
        PropinsiBO.ProvinceSaveEdit(ocustomClass)
    End Sub
End Class

