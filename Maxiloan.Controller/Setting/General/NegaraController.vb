
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class NegaraController
    Public Function GetNegara(ByVal ocustomClass As Parameter.Negara) As Parameter.Negara
        Dim NegaraBO As INegara
        NegaraBO = ComponentFactory.CreateNegara()
        Return NegaraBO.GetNegara(ocustomClass)
    End Function
    Public Function GetNegaraList(ByVal ocustomClass As Parameter.Negara) As Parameter.Negara
        Dim NegaraBO As INegara
        NegaraBO = ComponentFactory.CreateNegara()
        Return NegaraBO.GetNegaraList(ocustomClass)
    End Function
    Public Function GetNegaraCombo(ByVal ocustomClass As Parameter.Negara) As DataTable
        Dim NegaraBO As INegara
        NegaraBO = ComponentFactory.CreateNegara()
        Return NegaraBO.GetNegaraCombo(ocustomClass)
    End Function
End Class

