
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class NewMasterController
    Public Function GetMaster(ByVal customClass As Parameter.NewMaster) As Parameter.NewMaster
        Dim MasterBO As INewMaster
        MasterBO = ComponentFactory.CreateNewMaster()
        Return MasterBO.GetMaster(customClass)
    End Function
    Public Function GetMasterReport(ByVal customClass As Parameter.NewMaster) As Parameter.NewMaster
        Dim MasterBO As INewMaster
        MasterBO = ComponentFactory.CreateNewMaster()
        Return MasterBO.GetMasterReport(customClass)
    End Function
    Public Function GetKeyWord(ByVal customClass As Parameter.NewMaster) As Parameter.NewMaster
        Dim MasterBO As INewMaster
        MasterBO = ComponentFactory.CreateNewMaster()
        Return MasterBO.GetKeyWord(customClass)
    End Function

    Public Function MasterAdd(ByVal customClass As Parameter.NewMaster) As String
        Dim MasterBO As INewMaster
        MasterBO = ComponentFactory.CreateNewMaster()
        Return MasterBO.MasterSaveAdd(customClass)
    End Function

    Public Sub MasterUpdate(ByVal customClass As Parameter.NewMaster)
        Dim MasterBO As INewMaster
        MasterBO = ComponentFactory.CreateNewMaster()
        MasterBO.MasterSaveEdit(customClass)
    End Sub

    Public Function MasterDelete(ByVal customClass As Parameter.NewMaster) As String
        Dim MasterBO As INewMaster
        MasterBO = ComponentFactory.CreateNewMaster()
        Return MasterBO.MasterDelete(customClass)
    End Function

End Class

