
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class IndustryTypeController
    Public Function GetIndustryType(ByVal ocustomClass As Parameter.IndustryType) As Parameter.IndustryType
        Dim IndustryTypeBO As IIndustryType
        IndustryTypeBO = ComponentFactory.CreateIndustryType()
        Return IndustryTypeBO.GetIndustryType(ocustomClass)
    End Function
    Public Function GetIndustryTypeReport(ByVal ocustomClass As Parameter.IndustryType) As Parameter.IndustryType
        Dim IndustryTypeBO As IIndustryType
        IndustryTypeBO = ComponentFactory.CreateIndustryType()
        Return IndustryTypeBO.GetIndustryTypeReport(ocustomClass)
    End Function
    Public Function Getsector(ByVal ocustomClass As Parameter.IndustryType) As Parameter.IndustryType
        Dim IndustryTypeBO As IIndustryType
        IndustryTypeBO = ComponentFactory.CreateIndustryType()
        Return IndustryTypeBO.GetSector(ocustomClass)
    End Function
    Public Function GetIndustryTypeEdit(ByVal ocustomClass As Parameter.IndustryType) As Parameter.IndustryType
        Dim IndustryTypeBO As IIndustryType
        IndustryTypeBO = ComponentFactory.CreateIndustryType()
        Return IndustryTypeBO.GetIndustryTypeEdit(ocustomClass)
    End Function

    Public Function IndustryTypeAdd(ByVal ocustomClass As Parameter.IndustryType) As String
        Dim IndustryTypeBO As IIndustryType
        IndustryTypeBO = ComponentFactory.CreateIndustryType()
        Return IndustryTypeBO.IndustryTypeSaveAdd(ocustomClass)
    End Function

    Public Sub IndustryTypeUpdate(ByVal ocustomClass As Parameter.IndustryType)
        Dim IndustryTypeBO As IIndustryType
        IndustryTypeBO = ComponentFactory.CreateIndustryType()
        IndustryTypeBO.IndustryTypeSaveEdit(ocustomClass)
    End Sub

    Public Function IndustryTypeDelete(ByVal ocustomClass As Parameter.IndustryType) As String
        Dim IndustryTypeBO As IIndustryType
        IndustryTypeBO = ComponentFactory.CreateIndustryType()
        Return IndustryTypeBO.IndustryTypeDelete(ocustomClass)
    End Function

End Class

