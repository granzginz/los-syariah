Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Parameter

Public Class MasterZipCodeController

#Region " GetMasterZipCode "
    Public Function GetMasterZipCode(ByVal oMasterZipCode As Parameter.MasterZipCode) As Parameter.MasterZipCode
        Dim masterZipCodeBO As IZipCode
        masterZipCodeBO = ComponentFactory.CreateZipCode
        Return masterZipCodeBO.GetMasterZipCode(oMasterZipCode)
    End Function
#End Region

#Region " GetZipCodeIdReport "
    Public Function GetZipCodeIdReport(ByVal customClass As Parameter.MasterZipCode) As Parameter.MasterZipCode
        Dim masterZipCodeBO As IZipCode
        masterZipCodeBO = ComponentFactory.CreateZipCode
        Return masterZipCodeBO.GetZipCodeIdReport(customClass)
    End Function
#End Region

#Region " MasterZipCodeAdd "
    Public Function MasterZipCodeAdd(ByVal customClass As Parameter.MasterZipCode) As String
        Dim MasterZipCodeBO As IZipCode
        MasterZipCodeBO = ComponentFactory.CreateZipCode
        Return MasterZipCodeBO.MasterZipCodeAdd(customClass)
    End Function
#End Region

#Region " MasterZipCodeDelete "
    Public Function MasterZipCodeDelete(ByVal customClass As Parameter.MasterZipCode) As String
        Dim MasterZipCodeBO As IZipCode
        MasterZipCodeBO = ComponentFactory.CreateZipCode
        Return MasterZipCodeBO.MasterZipCodeDelete(customClass)
    End Function
#End Region

End Class
