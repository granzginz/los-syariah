
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Parameter
#End Region

Public Class ReasonController
    Public Sub ReasonAdd(ByVal customclass As Parameter.Reason)
        Dim ReasonBO As IReason
        ReasonBO = ComponentFactory.CreateReason
        ReasonBO.ReasonAdd(customclass)
    End Sub

    Public Sub ReasonEdit(ByVal customclass As Parameter.Reason)
        Dim ReasonBO As IReason
        ReasonBO = ComponentFactory.CreateReason
        ReasonBO.ReasonEdit(customclass)
    End Sub

    Public Sub ReasonDelete(ByVal customclass As Parameter.Reason)
        Dim ReasonBO As IReason
        ReasonBO = ComponentFactory.CreateReason
        ReasonBO.ReasonDelete(customclass)
    End Sub

    Public Function ReasonView(ByVal customclass As Parameter.Reason) As Parameter.Reason
        Dim ReasonBO As IReason
        ReasonBO = ComponentFactory.CreateReason
        Return ReasonBO.ReasonView(customclass)
    End Function

    Public Function GetReasonTypeCombo(ByVal customclass As Parameter.Reason) As DataTable
        Dim ReasonBO As IReason
        ReasonBO = ComponentFactory.CreateReason
        Return ReasonBO.GetReasonTypeCombo(customclass)
    End Function

End Class
