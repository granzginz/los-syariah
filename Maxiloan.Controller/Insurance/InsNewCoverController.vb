
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region
Public Class InsNewCoverController
    Public Function InsNewCoverView(ByVal customclass As Parameter.InsNewCover) As Parameter.InsNewCover
        Dim oInsNewCover As IInsNewCover
        oInsNewCover = ComponentFactory.CreateInsNewCover
        Return oInsNewCover.InsNewCoverView(customclass)
    End Function
    Public Function InsNewCoverGetInsured(ByVal customclass As Parameter.InsNewCover) As Parameter.InsNewCover
        Dim oInsNewCover As IInsNewCover
        oInsNewCover = ComponentFactory.CreateInsNewCover
        Return oInsNewCover.InsNewCoverGetInsured(customclass)
    End Function
    Public Function InsNewCoverGetPaid(ByVal customclass As Parameter.InsNewCover) As Parameter.InsNewCover
        Dim oInsNewCover As IInsNewCover
        oInsNewCover = ComponentFactory.CreateInsNewCover
        Return oInsNewCover.InsNewCoverGetPaid(customclass)
    End Function


    Public Function FillInsuranceComBranch(ByVal customClass As Parameter.InsNewCover) As DataTable
        Dim oInsNewCover As IInsNewCover
        oInsNewCover = ComponentFactory.CreateInsNewCover
        Return oInsNewCover.FillInsuranceComBranch(customClass)
    End Function
    Public Function GetInsuranceEntryStep2List(ByVal customClass As Parameter.InsNewCover) As Parameter.InsNewCover
        Dim oInsNewCover As IInsNewCover
        oInsNewCover = ComponentFactory.CreateInsNewCover
        Return oInsNewCover.GetInsuranceEntryStep2List(customClass)
    End Function

    Public Function GetInsuranceEntryStep1List(ByVal Customclass As Parameter.InsNewCover) As Parameter.InsNewCover
        Dim oInsNewCover As IInsNewCover
        oInsNewCover = ComponentFactory.CreateInsNewCover
        Return oInsNewCover.GetInsuranceEntryStep1List(Customclass)
    End Function
    Public Function CheckProspect(ByVal CustomClass As Parameter.InsNewCover) As Parameter.InsNewCover
        Dim oInsNewCover As IInsNewCover
        oInsNewCover = ComponentFactory.CreateInsNewCover
        Return oInsNewCover.CheckProspect(CustomClass)
    End Function
    Public Sub ProcessSaveInsuranceApplicationLastProcess(ByVal customClass As Parameter.InsNewCover)
        Dim oInsNewCover As IInsNewCover
        oInsNewCover = ComponentFactory.CreateInsNewCover
        oInsNewCover.ProcessSaveInsuranceApplicationLastProcess(customClass)
    End Sub
    Public Sub ProcessSaveInsuranceByCustomer(ByVal customClass As Parameter.InsNewCover)
        Dim oInsNewCover As IInsNewCover
        oInsNewCover = ComponentFactory.CreateInsNewCover
        oInsNewCover.ProcessSaveInsuranceByCustomer(customClass)
    End Sub
    Public Function GetInsuredByCustomer(ByVal customClass As Parameter.InsNewCover) As Parameter.InsNewCover
        Dim oInsNewCover As IInsNewCover
        oInsNewCover = ComponentFactory.CreateInsNewCover
        Return oInsNewCover.GetInsuredByCustomer(customClass)
    End Function
    Public Sub InsNewCoverInsuredByCustDelete(ByVal customClass As Parameter.InsNewCover)
        Dim oInsNewCover As IInsNewCover
        oInsNewCover = ComponentFactory.CreateInsNewCover
        oInsNewCover.InsNewCoverInsuredByCustDelete(customClass)
    End Sub
End Class
