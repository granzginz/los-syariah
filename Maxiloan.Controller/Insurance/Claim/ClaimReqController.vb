#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Imports Maxiloan
#End Region
Public Class ClaimReqController
    Public Function ClaimRequestList(ByVal customClass As Parameter.ClaimRequest) As Parameter.ClaimRequest
        Dim oClaimReq As IClaimReq
        oClaimReq = ComponentFactory.CreateClaimrequest
        Return oClaimReq.ClaimrequestList(customClass)
    End Function
    Public Function ClaimRequestDetail(ByVal customClass As Parameter.ClaimRequest) As Parameter.ClaimRequest
        Dim oClaimReq As IClaimReq
        oClaimReq = ComponentFactory.CreateClaimrequest
        Return oClaimReq.ClaimrequesDetail(customClass)
    End Function
    Public Function ClaimrequestCoverage(ByVal customClass As Parameter.ClaimRequest) As Parameter.ClaimRequest
        Dim oClaimReq As IClaimReq
        oClaimReq = ComponentFactory.CreateClaimrequest
        Return oClaimReq.ClaimrequestCoverage(customClass)
    End Function
    Public Function ClaimrequestSave(ByVal customClass As Parameter.ClaimRequest) As Boolean
        Dim oClaimReq As IClaimReq
        oClaimReq = ComponentFactory.CreateClaimrequest
        Return oClaimReq.ClaimrequestSave(customClass)
    End Function
    Public Function ClaimrequestGetClaimType(ByVal customClass As Parameter.ClaimRequest) As Parameter.ClaimRequest
        Dim oClaimReq As IClaimReq
        oClaimReq = ComponentFactory.CreateClaimrequest
        Return oClaimReq.ClaimrequestGetClaimType(customClass)
    End Function
    Public Function ClaimrequestPrint(ByVal customClass As Parameter.ClaimRequest) As Parameter.ClaimRequest
        Dim oClaimReq As IClaimReq
        oClaimReq = ComponentFactory.CreateClaimrequest
        Return oClaimReq.ClaimrequestPrint(customClass)
    End Function
    Public Function ViewInsuraceClaimInq(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest
        Dim oClaimReq As IClaimReq
        oClaimReq = ComponentFactory.CreateClaimrequest
        Return oClaimReq.ViewInsuraceClaimInq(customclass)
    End Function
    Public Function ClaimrequestEdit(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest
        Dim oClaimReq As IClaimReq
        oClaimReq = ComponentFactory.CreateClaimrequest
        Return oClaimReq.ClaimrequestEdit(customclass)
    End Function
    Public Function ClaimrequestListApproval(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct
        Dim oClaimReq As IClaimReq
        oClaimReq = ComponentFactory.CreateClaimrequest
        Return oClaimReq.ClaimrequestListApproval(customclass)
    End Function
    Public Function ClaimrequestSaveApprove(ByVal customClass As Parameter.InsClaimAct) As Boolean
        Dim oClaimReq As IClaimReq
        oClaimReq = ComponentFactory.CreateClaimrequest
        Return oClaimReq.ClaimrequestSaveApprove(customClass)
    End Function
End Class
