#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region
Public Class InsClaimActController
#Region "Activity"


    Public Function ClaimActList(ByVal customClass As Parameter.InsClaimAct) As Parameter.InsClaimAct
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.ClaimActList(customClass)
    End Function
    Public Function ClaimActDetail(ByVal customClass As Parameter.InsClaimAct) As Parameter.InsClaimAct
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.ClaimActDetail(customClass)
    End Function
    Public Function ClaimActGetDoc(ByVal customClass As Parameter.InsClaimAct) As Parameter.InsClaimAct
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.ClaimActGetDoc(customClass)
    End Function

    Public Function ClaimActsave(ByVal customClass As Parameter.InsClaimAct) As Boolean
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.ClaimActsave(customClass)
    End Function
    Public Function ClaimRejectsave(ByVal customClass As Parameter.InsClaimAct) As Boolean
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.ClaimRejectsave(customClass)
    End Function

    Public Function ClaimAcceptsave(ByVal customClass As Parameter.InsClaimAct) As Boolean
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.ClaimAcceptsave(customClass)
    End Function
#End Region
#Region "setting"


    Public Function GetInsRateCategory(ByVal customClass As Parameter.InsClaimAct) As DataSet
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.GetInsRateCategory(customClass)
    End Function
    Public Function SaveClaimDoc(ByVal customClass As Parameter.InsClaimAct) As Boolean
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.SaveClaimDoc(customClass)
    End Function

    Public Function DeleteClaimDoc(ByVal customClass As Parameter.InsClaimAct) As Boolean
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.DeleteClaimDoc(customClass)
    End Function

    Public Function DocclaimList(ByVal customClass As Parameter.InsClaimAct) As Parameter.InsClaimAct
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.DocclaimList(customClass)
    End Function

    Public Function GetDocMaster(ByVal customClass As Parameter.InsClaimAct) As DataSet
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.GetDocMaster(customClass)
    End Function
    Public Function ClaimDocReport(ByVal customClass As Parameter.InsClaimAct) As DataSet
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.ClaimDocReport(customClass)
    End Function
#End Region
#Region "nsuranceBilling"
    Public Function InsuranceBillingReport(ByVal customClass As Parameter.InsClaimAct) As DataSet
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.InsuranceBillingReport(customClass)
    End Function
#End Region
#Region "ClaimInquery"
    Public Function GetInsCo(ByVal customClass As Parameter.InsClaimAct) As DataSet
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.GetInsCo(customClass)
    End Function
    Public Function ClaimInquiryList(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.ClaimInquiryList(customclass)
    End Function
    Public Function GetClaimInqDet(ByVal customclass As Parameter.InsClaimAct) As DataSet
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.GetClaimInqDet(customclass)
    End Function

    Public Function GetClaimActList(ByVal customclass As Parameter.InsClaimAct) As DataSet
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.GetClaimActList(customclass)
    End Function
    Public Function ViewPolicy(ByVal customclass As Parameter.InsClaimAct) As DataSet
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.ViewPolicy(customclass)
    End Function
    Public Function GetClaimActHistList(ByVal customclass As Parameter.InsClaimAct) As DataSet
        Dim oClaimAct As IInsClaimAct
        oClaimAct = ComponentFactory.CreateInsClaimAct
        Return oClaimAct.GetClaimActHistList(customclass)
    End Function
#End Region
End Class
