#Region "Import"
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface

#End Region
Public Class AdvClaimReqControler
    Public Function AdvClaimReqPaging(ByVal customClass As Parameter.AdvClaimReq) As Parameter.AdvClaimReq
        Dim oAdvClaimReq As IAdvClaimReq
        oAdvClaimReq = ComponentFactory.CreateAdvClaimReq
        Return oAdvClaimReq.AdvClaimReqPaging(customClass)

    End Function
    Public Function AdvanceClaimCostRequestView(ByVal customclass As Parameter.AdvClaimReq) As Parameter.AdvClaimReq
        Dim oAdvClaimReq As IAdvClaimReq
        oAdvClaimReq = ComponentFactory.CreateAdvClaimReq
        Return oAdvClaimReq.AdvanceClaimCostRequestView(customclass)

    End Function
    Public Function AdvanceClaimCostRequestViewGrid(ByVal customClass As Parameter.AdvClaimReq) As Parameter.AdvClaimReq
        Dim oAdvClaimReq As IAdvClaimReq
        oAdvClaimReq = ComponentFactory.CreateAdvClaimReq
        Return oAdvClaimReq.AdvanceClaimCostRequestViewGrid(customClass)

    End Function
    Public Sub AdvanceClaimCostRequestSave(ByVal customClass As Parameter.AdvClaimReq)
        Dim oAdvClaimReq As IAdvClaimReq
        oAdvClaimReq = ComponentFactory.CreateAdvClaimReq
        oAdvClaimReq.AdvanceClaimCostRequestSave(customClass)
    End Sub
    Public Function AdvanceClaimCostRequestViewApproval(ByVal customclass As Parameter.AdvClaimReq) As Parameter.AdvClaimReq
        Dim oAdvClaimReq As IAdvClaimReq
        oAdvClaimReq = ComponentFactory.CreateAdvClaimReq
        Return oAdvClaimReq.AdvanceClaimCostRequestViewApproval(customclass)

    End Function

End Class
