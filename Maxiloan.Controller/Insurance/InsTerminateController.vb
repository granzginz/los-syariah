
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InsTerminateController
    Public Function InsTerminateList(ByVal customClass As Parameter.InsTerminate) As Parameter.InsTerminate
        Dim oInsTerminate As IInsTerminate
        oInsTerminate = ComponentFactory.CreateInsTerminate
        Return oInsTerminate.InsTerminateList(customClass)
    End Function
    Public Function InsrequestPrint(ByVal customClass As Parameter.InsTerminate) As DataSet
        Dim oInsTerminate As IInsTerminate
        oInsTerminate = ComponentFactory.CreateInsTerminate
        Return oInsTerminate.InsrequestPrint(customClass)
    End Function
    Public Function InsrequestPrintRefund(ByVal customClass As Parameter.InsTerminate) As DataSet
        Dim oInsTerminate As IInsTerminate
        oInsTerminate = ComponentFactory.CreateInsTerminate
        Return oInsTerminate.InsrequestPrintRefund(customClass)
    End Function
    Public Function InsTerminateDetail(ByVal customclass As Parameter.InsTerminate) As Parameter.InsTerminate
        Dim oInsTerminate As IInsTerminate
        oInsTerminate = ComponentFactory.CreateInsTerminate
        Return oInsTerminate.InsTerminateDetail(customclass)
    End Function
    Public Function InsCalculate(ByVal customclass As Parameter.InsTerminate) As Parameter.InsTerminate
        Dim oInsTerminate As IInsTerminate
        oInsTerminate = ComponentFactory.CreateInsTerminate
        Return oInsTerminate.InsCalculate(customclass)
    End Function
    Public Function InsSaveCalculate(ByVal customclass As Parameter.InsTerminate) As Boolean
        Dim oInsTerminate As IInsTerminate
        oInsTerminate = ComponentFactory.CreateInsTerminate
        Return oInsTerminate.InsSaveCalculate(customclass)
    End Function
    Public Function InsRefundCalculate(ByVal customclass As Parameter.InsTerminate) As DataSet
        Dim oInsTerminate As IInsTerminate
        oInsTerminate = ComponentFactory.CreateInsTerminate
        Return oInsTerminate.InsRefundCalculate(customClass)
    End Function
    Public Sub InsInsertMail(ByVal customclass As Parameter.InsTerminate)
        Dim oInsTerminate As IInsTerminate
        oInsTerminate = ComponentFactory.CreateInsTerminate
        oInsTerminate.InsInsertMail(customclass)
    End Sub
    Public Sub InsInsertMailRefund(ByVal customclass As Parameter.InsTerminate)
        Dim oInsTerminate As IInsTerminate
        oInsTerminate = ComponentFactory.CreateInsTerminate
        oInsTerminate.InsInsertMailRefund(customclass)
    End Sub

    Public Function InsApproveRejectCalculate(ByVal customclass As Parameter.InsTerminate) As String
        Dim oInsTerminate As IInsTerminate
        oInsTerminate = ComponentFactory.CreateInsTerminate
        Return oInsTerminate.InsApproveRejectCalculate(customclass)
    End Function

End Class
