


#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InsCoAllocationDetailListController

    Public Function GetListByAgreementNo(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
        Dim BO As IInsCoAllocationDetailList
        BO = ComponentFactory.CreateInsCoAllocationDetailList
        Return BO.GetListByAgreementNo(customClass)
    End Function

    Public Function GetListByApplicationId(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
        Dim BO As IInsCoAllocationDetailList
        BO = ComponentFactory.CreateInsCoAllocationDetailList
        Return BO.GetListByApplicationId(customClass)
    End Function


    Public Function GetGridInsuranceStatistic(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
        Dim BO As IInsCoAllocationDetailList
        BO = ComponentFactory.CreateInsCoAllocationDetailList
        Return BO.GetGridInsuranceStatistic(customClass)
    End Function

    Public Function GetGridTenor(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
        Dim BO As IInsCoAllocationDetailList
        BO = ComponentFactory.CreateInsCoAllocationDetailList
        Return BO.GetGridTenor(customClass)

    End Function

    Function GetGridInsCoPremium(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
        Dim BO As IInsCoAllocationDetailList
        BO = ComponentFactory.CreateInsCoAllocationDetailList
        Return BO.GetGridInsCoPremium(customClass)

    End Function

    Public Function GetGridInsCoPremiumView(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
        Dim BO As IInsCoAllocationDetailList
        BO = ComponentFactory.CreateInsCoAllocationDetailList
        Return BO.GetGridInsCoPremiumView(customClass)
    End Function
    Function SaveInsuranceCompanySelectionLastProcess(ByVal customClass As Parameter.InsCoAllocationDetailList) As String
        Dim BO As IInsCoAllocationDetailList
        BO = ComponentFactory.CreateInsCoAllocationDetailList
        Return BO.SaveInsuranceCompanySelectionLastProcess(customClass)

    End Function


    Public Function PenutupanAsuransiManualUpdate(ByVal customClass As Parameter.InsCoAllocationDetailList) As String
        Dim BO As IInsCoAllocationDetailList
        BO = ComponentFactory.CreateInsCoAllocationDetailList
        Return BO.PenutupanAsuransiManualUpdate(customClass) 
    End Function


    Public Sub InitMaskAssComponent(ByVal customClass As Parameter.MaskAssCalcEng)
        Dim BO As IInsCoAllocationDetailList
        BO = ComponentFactory.CreateInsCoAllocationDetailList
        BO.InitMaskAssComponent(customClass)
    End Sub
    Public Function CalcKoreksiMaskapaiAss(ByVal customClass As Parameter.MaskAssCalcEng) As DataSet
        Dim BO As IInsCoAllocationDetailList
        BO = ComponentFactory.CreateInsCoAllocationDetailList
        Return BO.CalcKoreksiMaskapaiAss(customClass)
    End Function

    Public Function KoreksiMaskapaiAss(ByVal customClass As Parameter.MaskAssCalcEng) As String
        Dim BO As IInsCoAllocationDetailList
        BO = ComponentFactory.CreateInsCoAllocationDetailList
        Return BO.KoreksiMaskapaiAss(customClass)
    End Function
End Class
