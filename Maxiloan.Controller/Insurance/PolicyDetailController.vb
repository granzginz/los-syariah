

#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class PolicyDetailController

    Public Function GetPopUpEndorsmentHistory(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
        Dim BO As IPolicyDetail
        BO = ComponentFactory.CreatePolicyDetail
        Return BO.GetPopUpEndorsmentHistory(customClass)
    End Function
    Public Function GetPolicyDetail(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
        Dim BO As IPolicyDetail
        BO = ComponentFactory.CreatePolicyDetail
        Return BO.GetPolicyDetail(customClass)
    End Function


End Class
