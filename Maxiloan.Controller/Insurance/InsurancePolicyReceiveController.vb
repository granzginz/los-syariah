

#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class InsurancePolicyReceiveController

    Public Function InsurancePolicyReceive(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
        Dim BO As IPolicyReceive
        BO = ComponentFactory.CreateInsurancePolicyReceive
        Return BO.getInsurancePolicyReceive(customClass)
    End Function

    Public Sub savePolicyUpload(ByVal strConnection As String, ByVal strAgreementNo As String, ByVal strPolicyNumber As String, ByVal PolicyReceiveDate As Date, ByVal strReceiveBy As String)
        Dim BO As IPolicyReceive
        BO = ComponentFactory.CreateInsurancePolicyReceive
        BO.savePolicyUpload(strConnection, strAgreementNo, strPolicyNumber, PolicyReceiveDate, strReceiveBy)
    End Sub

    Public Function getPolicyUpload(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
        Dim BO As IPolicyReceive
        BO = ComponentFactory.CreateInsurancePolicyReceive
        Return BO.getPolicyUpload(customClass)
    End Function


    Public Sub savePolicyUploadXLS(ByVal customClass As Parameter.InsuranceStandardPremium)
        Dim BO As IPolicyReceive
        BO = ComponentFactory.CreateInsurancePolicyReceive
        BO.savePolicyUploadXLS(customClass)
    End Sub

    Public Sub NotaAsuransiSave(ByVal oCustomer As Parameter.InsuranceStandardPremium)
        Dim BO As IPolicyReceive
        BO = ComponentFactory.CreateInsurancePolicyReceive
        BO.NotaAsuransiSave(oCustomer)
    End Sub
End Class
