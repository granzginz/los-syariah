
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InsPolicyNumberControler
    Public Function GetInsPolicyNumber(ByVal customClass As Parameter.InsPolicyNumber) As Parameter.InsPolicyNumber
        Dim BO As IInsurancePolicyNumber
        BO = ComponentFactory.CreatePolicyNumber
        Return BO.GetInsPolicyNumber(customClass)
    End Function
    Public Sub InsurancePolicyNumberSaveEdit(ByVal customClass As Parameter.InsPolicyNumber)
        Dim BO As IInsurancePolicyNumber
        BO = ComponentFactory.CreatePolicyNumber
        BO.InsurancePolicyNumberSaveEdit(customClass)
    End Sub
Public Function BankersClauseDSaveAdd(ByVal customClass As Parameter.BankersClause) As String
        Dim BO As IInsurancePolicyNumber
        BO = ComponentFactory.CreatePolicyNumber
        Return BO.BankersClauseDSaveAdd(customClass)
    End Function
    Public Function BankersClauseHSaveAdd(ByVal customClass As Parameter.BankersClause) As String
        Dim BO As IInsurancePolicyNumber
        BO = ComponentFactory.CreatePolicyNumber
        Return BO.BankersClauseHSaveAdd(customClass)
    End Function
    Public Function GetBankersClause(ByVal customClass As Parameter.BankersClause) As Parameter.BankersClause
        Dim BO As IInsurancePolicyNumber
        BO = ComponentFactory.CreatePolicyNumber
        Return BO.GetBankersClause(customClass)
    End Function
    Public Function GetInsCompanyBranchByID(ByVal customClass As Parameter.BankersClause) As Parameter.BankersClause
        Dim BO As IInsurancePolicyNumber
        BO = ComponentFactory.CreatePolicyNumber
        Return BO.GetInsCompanyBranchByID(customClass)
    End Function
    Public Function GetBankersClauseViewPrint(ByVal customClass As Parameter.BankersClause) As Parameter.BankersClause
        Dim BO As IInsurancePolicyNumber
        BO = ComponentFactory.CreatePolicyNumber
        Return BO.GetBankersClauseViewPrint(customClass)
    End Function
    Public Function GetBankersClausePrint(ByVal customClass As Parameter.BankersClause) As DataSet
        Dim BO As IInsurancePolicyNumber
        BO = ComponentFactory.CreatePolicyNumber
        Return BO.GetBankersClausePrint(customClass)
    End Function
End Class
