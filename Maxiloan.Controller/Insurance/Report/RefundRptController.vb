
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Imports Maxiloan
#End Region
Public Class RefundRptController
    Public Function ViewRefundReport(ByVal customClass As Parameter.RefundRpt) As Parameter.RefundRpt
        Dim oRefundRpt As IInsRefundRpt
        oRefundRpt = ComponentFactory.CreateInsRefundRpt
        Return oRefundRpt.ViewRefundReport(customClass)
    End Function
End Class
