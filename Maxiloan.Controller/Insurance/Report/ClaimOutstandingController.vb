
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region
Public Class ClaimOutstandingController
    Public Function ViewClaimReport(ByVal customClass As Parameter.ClaimOutstanding) As Parameter.ClaimOutstanding
        Dim oClaim As IInsClaimOutstanding
        oClaim = ComponentFactory.CreateInsClaimOutstanding
        Return oClaim.ViewClaimReport(customClass)
    End Function
    Public Function getInsuranceCoverageList(ByVal customClass As Parameter.ClaimOutstanding) As Parameter.ClaimOutstanding
        Dim oClaim As IInsClaimOutstanding
        oClaim = ComponentFactory.CreateInsClaimOutstanding
        Return oClaim.getInsuranceCoverageList(customClass)
    End Function

End Class
