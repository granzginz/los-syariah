
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region
Public Class InsEndorsmentReportController
    Public Function ViewEndorsmentReport(ByVal customClass As Parameter.InsEndorsmentReport) As Parameter.InsEndorsmentReport
        Dim oEndorsment As IInsEndorsmentReport
        oEndorsment = ComponentFactory.CreateInsEndorsmentReport
        Return oEndorsment.ViewEndorsmentReport(customClass)
    End Function
End Class
