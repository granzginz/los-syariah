
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class ClaimFinishedController
    Public Function ViewClaimFinishedReport(ByVal customClass As Parameter.ClaimFinished) As Parameter.ClaimFinished
        Dim oClaim As IInsClaimFinished
        oClaim = ComponentFactory.CreateInsClaimFinished
        Return oClaim.ViewClaimFinishedReport(customClass)
    End Function
End Class
