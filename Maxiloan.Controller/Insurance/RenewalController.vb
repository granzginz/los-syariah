

#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region



Public Class RenewalController
#Region "GetRenewalDetailTop"
    Public Function GetRenewalDetailTop(ByVal customClass As Parameter.Renewal) As Parameter.Renewal
        Dim BO As IRenewal
        BO = ComponentFactory.CreateRenewal
        Return BO.GetRenewalDetailTop(customClass)
    End Function
#End Region
#Region "GetRenewalNewCoverage"
    Public Function GetRenewalNewCoverage(ByVal customClass As Parameter.Renewal) As Parameter.Renewal
        Dim BO As IRenewal
        BO = ComponentFactory.CreateRenewal
        Return BO.GetRenewalNewCoverage(customClass)
    End Function
#End Region
#Region "CalculateSaveRenewal"

    Public Function CalculateAndSaveRenewal(ByVal customClass As Parameter.Renewal) As Parameter.Renewal
        Dim BO As IRenewal
        BO = ComponentFactory.CreateRenewal
        Return BO.CalculateAndSaveRenewal(customClass)
    End Function
#End Region
#Region "GetCoverageRenewalGridInsCALCULATE"
    Public Function GetCoverageRenewalGridInsCALCULATE(ByVal customClass As Parameter.Renewal) As Parameter.Renewal
        Dim BO As IRenewal
        BO = ComponentFactory.CreateRenewal
        Return BO.GetCoverageRenewalGridInsCALCULATE(customClass)
    End Function

#End Region

End Class
