

#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class InsCoAllocationController

    Public Function getInsuranceCompanySelection(ByVal customClass As Parameter.InsCoAllocation) As Parameter.InsCoAllocation
        Dim BO As IInsCoAllocation
        BO = ComponentFactory.CreateInsuranceCompanySelection
        Return BO.getInsuranceCompanySelection(customClass)
    End Function



End Class
