

#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region

Public Class SPPAController
    Public Function GetListCreateSPPA(ByVal customClass As Parameter.SPPA) As Parameter.SPPA
        Dim BO As ISPPA
        BO = ComponentFactory.CreateSPPA
        Return BO.GetListCreateSPPA(customClass)
    End Function

    Public Function GenerateSPPA(ByVal customClass As Parameter.SPPA) As Parameter.SPPA
        Dim BO As ISPPA
        BO = ComponentFactory.CreateSPPA
        Return BO.GenerateSPPA(customClass)
    End Function
    Public Function GenerateSPPACP(ByVal customClass As Parameter.SPPA) As Parameter.SPPA
        Dim BO As ISPPA
        BO = ComponentFactory.CreateSPPA
        Return BO.GenerateSPPA(customClass)
    End Function

    Public Function GetSPPAList(ByVal customClass As Parameter.SPPA) As Parameter.SPPA
        Dim BO As ISPPA
        BO = ComponentFactory.CreateSPPA
        Return BO.GetSPPAList(customClass)
    End Function


    Public Function GetSPPAReport(ByVal customClass As Parameter.SPPA) As DataSet
        Dim BO As ISPPA
        BO = ComponentFactory.CreateSPPA
        Return BO.GetSPPAReport(customClass)
    End Function

    Public Function GetSPPAReportCP(ByVal customClass As Parameter.SPPA) As DataSet
        Dim BO As ISPPA
        BO = ComponentFactory.CreateSPPA
        Return BO.GetSPPAReportCP(customClass)
    End Function
    Public Function GetSPPAReportJK(ByVal customClass As Parameter.SPPA) As DataSet
        Dim BO As ISPPA
        BO = ComponentFactory.CreateSPPA
        Return BO.GetSPPAReportJK(customClass)
    End Function
End Class
