


#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class InsuranceAssetDetailController

    Public Function GetViewInsuranceDetail(ByVal customClass As Parameter.InsuranceAssetDetail) As Parameter.InsuranceAssetDetail
        Dim BO As IInsuranceAssetDetail
        BO = ComponentFactory.CreateInsuranceAssetDetail
        Return BO.GetViewInsuranceDetail(customClass)

    End Function

End Class
