
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InsuranceBillingSelectController

    Public Sub SaveInsuranceBillingSelectDetail(ByVal customClass As Parameter.InsCoAllocationDetailList)
        Dim BO As IInsuranceBillingSelect
        BO = ComponentFactory.CreateInsuranceBillingSelect
        BO.SaveInsuranceBillingSelectDetail(customClass)
    End Sub
    Public Function CheckInvoiceNo(ByVal customClass As Parameter.InsCoAllocationDetailList) As String
        Dim BO As IInsuranceBillingSelect
        BO = ComponentFactory.CreateInsuranceBillingSelect
        Return BO.CheckInvoiceNo(customClass)
    End Function
    Public Sub SaveInsuranceBillingSave(ByVal customClass As Parameter.InsCoAllocationDetailList, ByVal customClassList As List(Of Parameter.InsCoAllocationDetailList))
        Dim BO As IInsuranceBillingSelect
        BO = ComponentFactory.CreateInsuranceBillingSelect
        BO.SaveInsuranceBillingSave(customClass, customClassList)
    End Sub
End Class
