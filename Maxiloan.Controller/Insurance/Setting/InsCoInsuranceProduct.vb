Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class InsCoInsuranceProduct

    Public Function GetInsCoProductList(ByVal oCustomClass As Parameter.InsCoProduct) As Parameter.InsCoProduct
        Dim InsCo As IInsCoProduct
        InsCo = ComponentFactory.CreateInsuranceProduct()
        Return InsCo.ListInsCoProductMaster(oCustomClass)
    End Function
    Public Function InsCoProductSaveAdd(ByVal oCustomClass As Parameter.InsCoProduct) As String
        Dim InsCo As IInsCoProduct
        InsCo = ComponentFactory.CreateInsuranceProduct()
        Return InsCo.AddInsCoProductMaster(oCustomClass)
    End Function
    Public Function InsCoProductDelete(ByVal oCustomClass As Parameter.InsCoProduct) As String
        Dim InsCo As IInsCoProduct
        InsCo = ComponentFactory.CreateInsuranceProduct()
        Return InsCo.DeletInsCoProductMaster(oCustomClass)
    End Function


End Class
