

#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class InsuranceStandardPremiumController
    Public Function processCopyStandardPremiumRate(ByVal customClass As Parameter.InsuranceStandardPremium) As String
        Dim InsuranceStandardPremiumBO As IInsuranceStandardPremium
        InsuranceStandardPremiumBO = ComponentFactory.CreateInsuranceStandardPremium
        Return InsuranceStandardPremiumBO.processCopyStandardPremiumRate(customClass)
    End Function

    Public Function getInsuranceStandardPremium(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
        Dim InsuranceStandardPremiumBO As IInsuranceStandardPremium
        InsuranceStandardPremiumBO = ComponentFactory.CreateInsuranceStandardPremium()
        Return InsuranceStandardPremiumBO.getInsuranceStandardPremium(customClass)
    End Function
    Public Function InsuranceStandardPremiumSaveAdd(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
        Dim InsuranceStandardPremiumBO As IInsuranceStandardPremium
        InsuranceStandardPremiumBO = ComponentFactory.CreateInsuranceStandardPremium()
        Return InsuranceStandardPremiumBO.InsuranceStandardPremiumSaveAdd(customClass)
    End Function
    Public Function InsuranceStandardPremiumSaveEdit(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
        Dim InsuranceStandardPremiumBO As IInsuranceStandardPremium
        InsuranceStandardPremiumBO = ComponentFactory.CreateInsuranceStandardPremium()
        InsuranceStandardPremiumBO.InsuranceStandardPremiumSaveEdit(customClass)
    End Function

    Public Function InsuranceStandardPremiumDelete(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
        Dim InsuranceStandardPremiumBO As IInsuranceStandardPremium
        InsuranceStandardPremiumBO = ComponentFactory.CreateInsuranceStandardPremium()
        InsuranceStandardPremiumBO.InsuranceStandardPremiumDelete(customClass)
    End Function

    Public Function getInsuranceStandardPremiumReport(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
        Dim InsuranceStandardPremiumBO As IInsuranceStandardPremium
        InsuranceStandardPremiumBO = ComponentFactory.CreateInsuranceStandardPremium()
        Return InsuranceStandardPremiumBO.getInsuranceStandardPremiumReport(customClass)
    End Function

End Class
