#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region

Public Class cInsuranceRateCard
    Function GetInsuranceRateCardHSave(ByVal customClass As Maxiloan.Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard
        Dim SaveH As iInsuranceRateCard
        SaveH = ComponentFactory.CreateInsuranceRateCard()
        Return SaveH.GetInsuranceRateCardHSave(customClass)
    End Function
    Function GetInsuranceRateCardHDelete(ByVal customClass As Maxiloan.Parameter.eInsuranceRateCard) As String
        Dim SaveH As iInsuranceRateCard
        SaveH = ComponentFactory.CreateInsuranceRateCard()
        Return SaveH.GetInsuranceRateCardHDelete(customClass)
    End Function
    Function GetInsuranceRateCardDSave(ByVal customClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard
        Dim SaveD As iInsuranceRateCard
        SaveD = ComponentFactory.CreateInsuranceRateCard()
        Return SaveD.GetInsuranceRateCardDSave(customClass)
    End Function
    Public Function GetInsuranceRateCardDDelete(ByVal customClass As Parameter.eInsuranceRateCard) As String
        Dim DeleteD As iInsuranceRateCard
        DeleteD = ComponentFactory.CreateInsuranceRateCard()
        Return DeleteD.GetInsuranceRateCardDDelete(customClass)
    End Function
    Public Function GetInsuranceRateCardDCopy(ByVal customClass As Parameter.eInsuranceRateCard) As String
        Dim CopyD As iInsuranceRateCard
        CopyD = ComponentFactory.CreateInsuranceRateCard()
        Return CopyD.GetInsuranceRateCardDCopy(customClass)
    End Function
    Function GetInsuranceMasterRateDSave(ByVal customClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard
        Dim SaveMasterD As iInsuranceRateCard
        SaveMasterD = ComponentFactory.CreateInsuranceRateCard()
        Return SaveMasterD.GetInsuranceMasterRateDSave(customClass)
    End Function
    Public Function GetInsuranceMasterRateDDelete(ByVal customClass As Parameter.eInsuranceRateCard) As String
        Dim DeleteMasterD As iInsuranceRateCard
        DeleteMasterD = ComponentFactory.CreateInsuranceRateCard()
        Return DeleteMasterD.GetInsuranceMasterRateDDelete(customClass)
    End Function
    Function GetNewInsuranceRateCardDSave(ByVal customClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard
        Dim SaveNewD As iInsuranceRateCard
        SaveNewD = ComponentFactory.CreateInsuranceRateCard()
        Return SaveNewD.GetNewInsuranceRateCardDSave(customClass)
    End Function
    Public Function GetNewInsuranceRateCardDDelete(ByVal customClass As Parameter.eInsuranceRateCard) As String
        Dim DeleteNewD As iInsuranceRateCard
        DeleteNewD = ComponentFactory.CreateInsuranceRateCard()
        Return DeleteNewD.GetNewInsuranceRateCardDDelete(customClass)
    End Function
    Function GetTPLToCustSave(ByVal customClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard
        Dim SaveTPL As iInsuranceRateCard
        SaveTPL = ComponentFactory.CreateInsuranceRateCard()
        Return SaveTPL.GetTPLToCustSave(customClass)
    End Function
    Public Function GetTPLToCustDelete(ByVal customClass As Parameter.eInsuranceRateCard) As String
        Dim DeleteTPL As iInsuranceRateCard
        DeleteTPL = ComponentFactory.CreateInsuranceRateCard()
        Return DeleteTPL.GetTPLToCustDelete(customClass)
    End Function
    Function GetOtherCoverage(ByVal customClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard
        Dim SaveOtherCoverage As iInsuranceRateCard
        SaveOtherCoverage = ComponentFactory.CreateInsuranceRateCard()
        Return SaveOtherCoverage.GetOtherCoverage(customClass)
    End Function
End Class
