


#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region



Public Class InsQuotaStatisticController

    Public Function ProcessInsuranceQuotaYear(ByVal customClass As Parameter.InsQuotaStatistic) As String
        Dim ProcessInsuranceQuotaYearBO As IInsQuotaStatistic
        ProcessInsuranceQuotaYearBO = ComponentFactory.CreateInsuranceQuotaStatistic
        Return ProcessInsuranceQuotaYearBO.ProcessInsuranceQuotaYear(customClass)
    End Function

    Public Function getInsuranceQuotaListing(ByVal customClass As Parameter.InsQuotaStatistic) As Parameter.InsQuotaStatistic
        Dim getInsuranceQuotaListingBO As IInsQuotaStatistic
        getInsuranceQuotaListingBO = ComponentFactory.CreateInsuranceQuotaStatistic
        Return getInsuranceQuotaListingBO.getInsuranceQuotaListing(customClass)
    End Function

    Public Function UpdateInsuranceQuotaYear(ByVal customClass As Parameter.InsQuotaStatistic) As Parameter.InsQuotaStatistic
        Dim UpdateInsuranceQuotaYearBO As IInsQuotaStatistic
        UpdateInsuranceQuotaYearBO = ComponentFactory.CreateInsuranceQuotaStatistic
        UpdateInsuranceQuotaYearBO.UpdateInsuranceQuotaYear(customClass)
    End Function

    Public Function GetInsuranceQuotaReport(ByVal CustomClass As Parameter.InsQuotaStatistic) As Parameter.InsQuotaStatistic
        Dim BO As IInsQuotaStatistic
        BO = ComponentFactory.CreateInsuranceQuotaStatistic
        Return BO.GetInsuranceQuotaReport(CustomClass)
    End Function

End Class
