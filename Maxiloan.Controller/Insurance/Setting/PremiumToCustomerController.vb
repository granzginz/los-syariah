
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class PremiumToCustomerController
    Public Function ProcessCopyRateFromAnotherBranch(ByVal customClass As Parameter.PremiumToCustomer) As String
        Dim PremiumToCustomerBO As IPremiumToCustomer
        PremiumToCustomerBO = ComponentFactory.CreatePremiumToCustomer()
        Return PremiumToCustomerBO.ProcessCopyRateFromAnotherBranch(customClass)
    End Function
    Public Function GetPremiumToCustomer(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer
        Dim PremiumToCustomerBO As IPremiumToCustomer
        PremiumToCustomerBO = ComponentFactory.CreatePremiumToCustomer()
        Return PremiumToCustomerBO.GetPremiumToCustomer(customClass)
    End Function
    Public Function GetPremiumToCustomerReport(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer
        Dim PremiumToCustomerBO As IPremiumToCustomer
        PremiumToCustomerBO = ComponentFactory.CreatePremiumToCustomer()
        Return PremiumToCustomerBO.GetPremiumToCustomerReport(customClass)
    End Function

    Public Function GetPremiumToCustomerBranch(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer
        Dim PremiumToCustomerBO As IPremiumToCustomer
        PremiumToCustomerBO = ComponentFactory.CreatePremiumToCustomer()
        Return PremiumToCustomerBO.GetPremiumToCustomerBranch(customClass)
    End Function

    Public Function GetPremiumToCustomerInsType(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer
        Dim PremiumToCustomerBO As IPremiumToCustomer
        PremiumToCustomerBO = ComponentFactory.CreatePremiumToCustomer()
        Return PremiumToCustomerBO.GetPremiumToCustomerInsType(customClass)
    End Function
    'Public Function GetPremiumToCustomerTPLEdit(ByVal BranchID As String, ByVal TPLAmount As String) As Parameter.PremiumToCustomer
    '    Dim PremiumToCustomerBO As IPremiumToCustomer
    '    PremiumToCustomerBO = ComponentFactory.CreatePremiumToCustomer()
    '    Return PremiumToCustomerBO.GetPremiumToCustomerTPLEdit(BranchID, TPLAmount)
    'End Function

    Public Function PremiumToCustomerTPLSaveAdd(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer
        Dim PremiumToCustomerBO As IPremiumToCustomer
        PremiumToCustomerBO = ComponentFactory.CreatePremiumToCustomer()
        Return PremiumToCustomerBO.PremiumToCustomerTPLSaveAdd(customClass)
    End Function

    Public Sub PremiumToCustomerTPLSaveEdit(ByVal customClass As Parameter.PremiumToCustomer)
        Dim PremiumToCustomerBO As IPremiumToCustomer
        PremiumToCustomerBO = ComponentFactory.CreatePremiumToCustomer()
        PremiumToCustomerBO.PremiumToCustomerTPLSaveEdit(customClass)
    End Sub

    Public Sub PremiumToCustomerTPLDelete(ByVal customClass As Parameter.PremiumToCustomer)
        Dim PremiumToCustomerBO As IPremiumToCustomer
        PremiumToCustomerBO = ComponentFactory.CreatePremiumToCustomer()
        PremiumToCustomerBO.PremiumToCustomerTPLDelete(customClass)
    End Sub

    'Public Function GetPremiumToCustomerRateCustEdit(ByVal BranchID As String, ByVal InsType As String, ByVal YearNum As String) As Parameter.PremiumToCustomer
    '    Dim PremiumToCustomerBO As IPremiumToCustomer
    '    PremiumToCustomerBO = ComponentFactory.CreatePremiumToCustomer()
    '    Return PremiumToCustomerBO.GetPremiumToCustomerRateCustEdit(BranchID, InsType, YearNum)
    'End Function

    Public Function PremiumToCustomerRateCustSaveAdd(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer
        Dim PremiumToCustomerBO As IPremiumToCustomer
        PremiumToCustomerBO = ComponentFactory.CreatePremiumToCustomer()
        Return PremiumToCustomerBO.PremiumToCustomerRateCustSaveAdd(customClass)
    End Function

    Public Sub PremiumToCustomerRateCustSaveEdit(ByVal customClass As Parameter.PremiumToCustomer)
        Dim PremiumToCustomerBO As IPremiumToCustomer
        PremiumToCustomerBO = ComponentFactory.CreatePremiumToCustomer()
        PremiumToCustomerBO.PremiumToCustomerRateCustSaveEdit(customClass)
    End Sub

    Public Sub PremiumToCustomerRateCustDelete(ByVal customClass As Parameter.PremiumToCustomer)
        Dim PremiumToCustomerBO As IPremiumToCustomer
        PremiumToCustomerBO = ComponentFactory.CreatePremiumToCustomer()
        PremiumToCustomerBO.PremiumToCustomerRateCustDelete(customClass)
    End Sub
End Class

