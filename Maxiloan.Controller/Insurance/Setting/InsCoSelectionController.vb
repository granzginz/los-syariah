Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess


Public Class InsCoSelectionController
    Function GetAgreementList(ByVal oInsCo As Parameter.InsCoSelectionList) As Parameter.InsCoSelectionList
        Dim InsCoSelectionBO As IInsCoSelection
        InsCoSelectionBO = ComponentFactory.CreateInsCoSelection()
        Return InsCoSelectionBO.GetAgreementList(oinsco)
    End Function

    Function GetInsCompanyList(ByVal oInsCo As Parameter.InsCoSelectionList) As Parameter.InsCoSelectionList
        Dim InsCoSelectionBO As IInsCoSelection
        InsCoSelectionBO = ComponentFactory.CreateInsCoSelection()
        Return InsCoSelectionBO.GetInsCompanyList(oInsCo)
    End Function

    Function GetInsCompanyListDataSet(ByVal oInsCo As Parameter.InsCoSelectionList) As Parameter.InsCoSelectionList
        Dim InsCoSelectionBO As IInsCoSelection
        InsCoSelectionBO = ComponentFactory.CreateInsCoSelection()
        Return InsCoSelectionBO.GetInsCompanyListDataSet(oInsCo)
    End Function

    Public Function InsCoAdd(ByVal customClass As Parameter.InsCo, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As String
        Dim InsCoAddBO As IInsCoSelection
        InsCoAddBO = ComponentFactory.CreateInsCoSelection()
        Return InsCoAddBO.InsCompanyAdd(customClass, oClassAddress, oClassPersonal)
    End Function

    Public Sub InsCoEdit(ByVal customClass As Parameter.InsCo, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim InsCoEdtBO As IInsCoSelection
        InsCoEdtBO = ComponentFactory.CreateInsCoSelection
        InsCoEdtBO.InsCompanyEdit(customClass, oClassAddress, oClassPersonal)
    End Sub
    Public Sub InsCoDelete(ByVal customClass As Parameter.InsCo)
        Dim InsCoDelBO As IInsCoSelection
        InsCoDelBO = ComponentFactory.CreateInsCoSelection
        InsCoDelBO.InsCompanyDelete(customClass)
    End Sub

    Function GetInsCompanyByID(ByVal strConnection As String, ByVal InsCoID As String) As Parameter.InsCoSelectionList
        Dim InsCoSelectionBO As IInsCoSelection
        InsCoSelectionBO = ComponentFactory.CreateInsCoSelection()
        Return InsCoSelectionBO.GetInsCompanyByID(strConnection, InsCoID)
    End Function

    Function GetInsCompanyBranchList(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList
        Dim InsCoSelectionBO As IInsCoSelection
        InsCoSelectionBO = ComponentFactory.CreateInsCoSelection()
        Return InsCoSelectionBO.GetInsCompanyBranchList(InsCoBranch)
    End Function

    Function GetInsCompanyBranchByID(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList
        Dim InsCoSelectionBO As IInsCoSelection
        InsCoSelectionBO = ComponentFactory.CreateInsCoSelection()
        Return InsCoSelectionBO.GetInsCompanyBranchByID(InsCoBranch)
    End Function


    Public Sub InsCoBranchAdd(ByVal customClass As Parameter.InsCoBranch, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal, ByVal oClassBankAccount As Parameter.BankAccount)
        Dim InsCoBranchAddBO As IInsCoSelection
        InsCoBranchAddBO = ComponentFactory.CreateInsCoSelection()
        InsCoBranchAddBO.InsCompanyBranchAdd(customClass, oClassAddress, oClassPersonal, oClassBankAccount)
    End Sub

    Public Sub InsCoBranchEdit(ByVal customClass As Parameter.InsCoBranch, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal, ByVal oClassBankAccount As Parameter.BankAccount)
        Dim InsCoBranchEdtBO As IInsCoSelection
        InsCoBranchEdtBO = ComponentFactory.CreateInsCoSelection()
        InsCoBranchEdtBO.InsCompanyBranchEdit(customClass, oClassAddress, oClassPersonal, oClassBankAccount)
    End Sub

    Public Sub InsCoBranchDelete(ByVal customClass As Parameter.InsCoBranch)
        Dim InsCoBranchDelBO As IInsCoSelection
        InsCoBranchDelBO = ComponentFactory.CreateInsCoSelection()
        InsCoBranchDelBO.InsCompanyBranchDelete(customClass)
    End Sub

    Function GetRateList(ByVal InsCoRate As Parameter.InsCoRate) As Parameter.InsCoSelectionList
        Dim InsCoSelectionBO As IInsCoSelection
        InsCoSelectionBO = ComponentFactory.CreateInsCoSelection()
        Return InsCoSelectionBO.GetRateList(InsCoRate)
    End Function

    Public Sub RateAdd(ByVal InsCoRate As Parameter.InsCoRate)
        Dim RateBO As IInsCoSelection
        RateBO = ComponentFactory.CreateInsCoSelection()
        RateBO.RateAdd(InsCoRate)
    End Sub

    Public Sub RateEdit(ByVal InsCoRate As Parameter.InsCoRate)
        Dim RateBO As IInsCoSelection
        RateBO = ComponentFactory.CreateInsCoSelection()
        RateBO.RateEdit(InsCoRate)
    End Sub

    Public Sub RateDelete(ByVal InsCoRate As Parameter.InsCoRate)
        Dim RateBO As IInsCoSelection
        RateBO = ComponentFactory.CreateInsCoSelection()
        RateBO.RateDelete(InsCoRate)
    End Sub

    Public Sub RateInsCoBranchCopy(ByVal InsCoRate As Parameter.InsCoRate)
        Dim RateBO As IInsCoSelection
        RateBO = ComponentFactory.CreateInsCoSelection()
        RateBO.CopyInsCoBranchRate(InsCoRate)
    End Sub


    Function GetTPLList(ByVal InsCoTPL As Parameter.InsCoTPL) As Parameter.InsCoSelectionList
        Dim InsCoSelectionBO As IInsCoSelection
        InsCoSelectionBO = ComponentFactory.CreateInsCoSelection()
        Return InsCoSelectionBO.GetTPLList(InsCoTPL)
    End Function

    Function GetTPLByID(ByVal InsCoTPL As Parameter.InsCoTPL) As Parameter.InsCoSelectionList
        Dim InsCoSelectionBO As IInsCoSelection
        InsCoSelectionBO = ComponentFactory.CreateInsCoSelection()
        Return InsCoSelectionBO.GetTPLByID(InsCoTPL)
    End Function

    Public Sub TPLAdd(ByVal InsCoTPL As Parameter.InsCoTPL)
        Dim TPLAddBO As IInsCoSelection
        TPLAddBO = ComponentFactory.CreateInsCoSelection()
        TPLAddBO.TPLAdd(InsCoTPL)
    End Sub

    Public Sub TPLEdit(ByVal InsCoTPL As Parameter.InsCoTPL)
        Dim TPLEditBO As IInsCoSelection
        TPLEditBO = ComponentFactory.CreateInsCoSelection()
        TPLEditBO.TPLEdit(InsCoTPL)
    End Sub

    Public Sub TPLDelete(ByVal InsCoTPL As Parameter.InsCoTPL)
        Dim TPLDeleteBO As IInsCoSelection
        TPLDeleteBO = ComponentFactory.CreateInsCoSelection()
        TPLDeleteBO.TPLDelete(InsCoTPL)
    End Sub

    Function GetTPLReport(ByVal InsCoTPL As Parameter.InsCoTPL) As Parameter.InsCoSelectionList
        Dim TPLReportBO As IInsCoSelection
        TPLReportBO = ComponentFactory.CreateInsCoSelection()
        Return TPLReportBO.GetTPLReport(InsCoTPL)
    End Function

    Function GetRateReport(ByVal InsCoRate As Parameter.InsCoRate) As Parameter.InsCoSelectionList
        Dim RateReportBO As IInsCoSelection
        RateReportBO = ComponentFactory.CreateInsCoSelection()
        Return RateReportBO.GetRateReport(InsCoRate)
    End Function

    Function GetInsAssetTypeCombo(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList
        Dim InsCoSelectionBO As IInsCoSelection
        InsCoSelectionBO = ComponentFactory.CreateInsCoSelection()
        Return InsCoSelectionBO.GetinsAssetTypeCombo(InsCoBranch)
    End Function

    Function GetCoverageTypeCombo(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList
        Dim InsCoSelectionBO As IInsCoSelection
        InsCoSelectionBO = ComponentFactory.CreateInsCoSelection()
        Return InsCoSelectionBO.GetCoverageTypeCombo(InsCoBranch)
    End Function

    Function GetAssetUsageCombo(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList
        Dim InsCoSelectionBO As IInsCoSelection
        InsCoSelectionBO = ComponentFactory.CreateInsCoSelection()
        Return InsCoSelectionBO.GetAssetUsageCombo(InsCoBranch)
    End Function

    Function GetRateByID(ByVal InsCoRate As Parameter.InsCoRate) As Parameter.InsCoSelectionList
        Dim RateBO As IInsCoSelection
        RateBO = ComponentFactory.CreateInsCoSelection()
        Return RateBO.GetRateByID(InsCoRate)
    End Function

    Function GetInsuranceComByProduct(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList
        Dim InsCoSelectionBO As IInsCoSelection
        InsCoSelectionBO = ComponentFactory.CreateInsCoSelection()
        Return InsCoSelectionBO.GetInsuranceComByProduct(InsCoBranch)
    End Function

End Class
