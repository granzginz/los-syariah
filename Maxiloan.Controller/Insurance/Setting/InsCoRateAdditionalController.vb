Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class InsCoRateAdditionalController

    Public Function GetInsCoProductList(ByVal oCustomClass As Parameter.InsCoRateAdditional) As Parameter.InsCoRateAdditional
        Dim InsCo As [Interface].IInsCoRateAdditional
        InsCo = ComponentFactory.CreateInsuranceRateAdditional()
        Return InsCo.ListInsCoCPMaster(oCustomClass)
    End Function
    Public Function InsCoProductSaveAdd(ByVal oCustomClass As Parameter.InsCoRateAdditional) As String
        Dim InsCo As [Interface].IInsCoRateAdditional
        InsCo = ComponentFactory.CreateInsuranceRateAdditional()
        Return InsCo.AddInsCoCPMaster(oCustomClass)
    End Function
    Public Function InsCoProductSaveEdit(ByVal oCustomClass As Parameter.InsCoRateAdditional) As String
        Dim InsCo As [Interface].IInsCoRateAdditional
        InsCo = ComponentFactory.CreateInsuranceRateAdditional()
        Return InsCo.EditInsCoCPMaster(oCustomClass)
    End Function

    Public Function InsCoProductDelete(ByVal oCustomClass As Parameter.InsCoRateAdditional) As String
        Dim InsCo As [Interface].IInsCoRateAdditional
        InsCo = ComponentFactory.CreateInsuranceRateAdditional()
        Return InsCo.DeletInsCoCPMaster(oCustomClass)
    End Function

End Class
