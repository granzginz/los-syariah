Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class cInsuranceCompany

    Public Function InsCoSave(ByVal customClass As Parameter.eInsuranceCompany, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As Parameter.eInsuranceCompany
        Dim InsCo As iInsuranceCompany
        InsCo = ComponentFactory.CreateInsuranceCompany
        Return InsCo.InsCompanySave(customClass, oClassAddress, oClassPersonal)
    End Function

    Public Sub InsCoDelete(ByVal customClass As Parameter.eInsuranceCompany)
        Dim InsCoDelBO As iInsuranceCompany
        InsCoDelBO = ComponentFactory.CreateInsuranceCompany
        InsCoDelBO.InsCompanyDelete(customClass)
    End Sub

    Public Function InsCoBranchSave(ByVal customClass As Parameter.eInsuranceCompany, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal, ByVal oClassBankAccount As Parameter.BankAccount) As Parameter.eInsuranceCompany
        Dim InsCoBranchSaveBO As iInsuranceCompany
        InsCoBranchSaveBO = ComponentFactory.CreateInsuranceCompany()
        Return InsCoBranchSaveBO.InsCompanyBranchSave(customClass, oClassAddress, oClassPersonal, oClassBankAccount)
    End Function

    Public Sub InsCoBranchDelete(ByVal customClass As Parameter.eInsuranceCompany)
        Dim InsCoBranchDelBO As iInsuranceCompany
        InsCoBranchDelBO = ComponentFactory.CreateInsuranceCompany()
        InsCoBranchDelBO.InsCompanyBranchDelete(customClass)
    End Sub


    Public Function RateSave(ByVal InsCoRate As Parameter.eInsuranceCompany) As Parameter.eInsuranceCompany
        Dim RateBO As iInsuranceCompany
        RateBO = ComponentFactory.CreateInsuranceCompany()
        Return RateBO.RateSave(InsCoRate)
    End Function

    Public Sub RateDelete(ByVal InsCoRate As Parameter.eInsuranceCompany)
        Dim RateBO As iInsuranceCompany
        RateBO = ComponentFactory.CreateInsuranceCompany()
        RateBO.RateDelete(InsCoRate)
    End Sub

    Public Sub RateInsCoBranchCopy(ByVal InsCoRate As Parameter.eInsuranceCompany)
        Dim RateBO As iInsuranceCompany
        RateBO = ComponentFactory.CreateInsuranceCompany()
        RateBO.CopyInsCoBranchRate(InsCoRate)
    End Sub


    Public Function TPLSave(ByVal InsCoTPL As Parameter.eInsuranceCompany) As Parameter.eInsuranceCompany
        Dim TPLSaveBO As iInsuranceCompany
        TPLSaveBO = ComponentFactory.CreateInsuranceCompany()
        Return TPLSaveBO.TPLSave(InsCoTPL)
    End Function

    Public Sub TPLDelete(ByVal InsCoTPL As Parameter.eInsuranceCompany)
        Dim TPLDeleteBO As iInsuranceCompany
        TPLDeleteBO = ComponentFactory.CreateInsuranceCompany()
        TPLDeleteBO.TPLDelete(InsCoTPL)
    End Sub


    Public Function CoverageSave(ByVal InsCoCoverage As Parameter.eInsuranceCompany) As Parameter.eInsuranceCompany
        Dim CoverageSaveBO As iInsuranceCompany
        CoverageSaveBO = ComponentFactory.CreateInsuranceCompany()
        Return CoverageSaveBO.CoverageSave(InsCoCoverage)
    End Function

    Public Function InsCoBranchSaveEdit(ByVal oClassEdit As Parameter.eInsuranceCompany) As Parameter.eInsuranceCompany
        Dim InsCoBranchSatetSave As iInsuranceCompany
        InsCoBranchSatetSave = ComponentFactory.CreateInsuranceCompany()
        Return InsCoBranchSatetSave.InsCompanyBranchSaveEdit(oClassEdit)
    End Function

End Class
