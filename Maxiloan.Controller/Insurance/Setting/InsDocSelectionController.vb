
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class InsDocSelectionController
#Region "PrivateConst"
    Private _InsDocSelectionBO As IInsDocSelection
#End Region
    Function GetInsDocList(ByVal oInsDoc As Parameter.InsDocSelectionList) As Parameter.InsDocSelectionList
        Try
            Return _InsDocSelectionBO.GetInsDocList(oInsDoc)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetInsDocRpt(ByVal oInsDoc As Parameter.InsDocSelectionList) As Parameter.InsDocSelectionList
        Try
            Return _InsDocSelectionBO.GetInsDocRpt(oInsDoc)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub AddInsDoc(ByVal oInsDoc As Parameter.InsDocSelectionList)
        Try
            _InsDocSelectionBO.AddInsDoc(oInsDoc)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub UpdateInsDoc(ByVal oInsDoc As Parameter.InsDocSelectionList)
        Try
            _InsDocSelectionBO.UpdateInsDoc(oInsDoc)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub DeleteInsDoc(ByVal oInsDoc As Parameter.InsDocSelectionList)
        Try
            _InsDocSelectionBO.DeleteInsDoc(oInsDoc)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub New()
        Try
            _InsDocSelectionBO = ComponentFactory.CreateInsDocSelection()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
