
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class InsInqRefundController
    Public Function InsInqRefundList(ByVal customclass As Parameter.InsInqRefund) As Parameter.InsInqRefund

        Dim oInsInqRefund As IInsInqRefund
        oInsInqRefund = ComponentFactory.CreateInsInqRefund
        Return oInsInqRefund.InsInqRefundList(customclass)
    End Function

    Public Function InsInqRefundDetail(ByVal customclass As Parameter.InsInqRefund) As Parameter.InsInqRefund
        Dim oInsInqRefund As IInsInqRefund
        oInsInqRefund = ComponentFactory.CreateInsInqRefund
        Return oInsInqRefund.InsInqRefundDetail(customclass)
    End Function
End Class
