
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region

Public Class InsuranceDuecontroller
    Public Function InsuranceDuepaging(ByVal oCustomclass As Parameter.InsuranceDue) As Parameter.InsuranceDue
        Dim oInsInqRefund As IInsuranceDue
        oInsInqRefund = ComponentFactory.InsuranceDue
        Return oInsInqRefund.InsuranceDuepaging(oCustomclass)
    End Function
    Public Function InsuranceDueReport(ByVal oCustomclass As Parameter.InsuranceDue) As Parameter.InsuranceDue
        Dim oInsInqRefund As IInsuranceDue
        oInsInqRefund = ComponentFactory.InsuranceDue
        Return oInsInqRefund.InsuranceDueReport(oCustomclass)
    End Function
End Class
