
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class InsuranceInq
    Public Function InqInsInvoice(ByVal ocustomclass As Parameter.InsInqEntities) As Parameter.InsInqEntities
        Dim Ins As IInsuranceInq
        Ins = ComponentFactory.CreateInsuranceInq
        Return Ins.InqInsInvoice(ocustomclass)
    End Function

    Public Function InqInsExpired(ByVal ocustomclass As Parameter.InsInqEntities) As Parameter.InsInqEntities
        Dim Ins As IInsuranceInq
        Ins = ComponentFactory.CreateInsuranceInq
        Return Ins.InqInsExpired(ocustomclass)
    End Function
    Public Function GetReportInsStatitistic(ByVal ocustomclass As Parameter.InsInqEntities) As Parameter.InsInqEntities
        Dim Ins As IInsuranceInq
        Ins = ComponentFactory.CreateInsuranceInq
        Return Ins.GetReportInsStatitistic(ocustomclass)
    End Function
End Class
