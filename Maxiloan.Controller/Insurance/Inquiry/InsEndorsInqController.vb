#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Imports Maxiloan
#End Region
Public Class InsEndorsInqController
    Public Function InsInqEndorsList(ByVal customclass As Parameter.InsEndorsInq) As Parameter.InsEndorsInq
        Dim oInsInqEndors As IInsEndorsInq
        oInsInqEndors = ComponentFactory.CreateInsEndorsmentInquiry
        Return oInsInqEndors.InsInqEndorsList(customclass)
    End Function
    Public Function InsInqEndorsDetail(ByVal customclass As Parameter.InsEndorsInq) As Parameter.InsEndorsInq
        Dim oInsInqEndors As IInsEndorsInq
        oInsInqEndors = ComponentFactory.CreateInsEndorsmentInquiry
        Return oInsInqEndors.InsInqEndorsDetail(customclass)
    End Function
End Class
