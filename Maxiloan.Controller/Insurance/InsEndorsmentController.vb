
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InsEndorsmentController
#Region "InsEndorsmentList"
    Public Function InsEndorsmentList(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
        Dim BO As IInsEndorsment
        BO = ComponentFactory.CreateInsEndorsment
        Return BO.InsEndorsmentList(customClass)
    End Function
#End Region
#Region "GetData"
    Public Function GetData(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
        Dim BO As IInsEndorsment
        BO = ComponentFactory.CreateInsEndorsment
        Return BO.GetData(customClass)
    End Function
#End Region
#Region "GetDataInsDetail"
    Public Function GetDataInsDetail(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
        Dim BO As IInsEndorsment
        BO = ComponentFactory.CreateInsEndorsment
        Return BO.GetDataInsDetail(customClass)
    End Function
#End Region
#Region "GetDataCbTPL"
    Public Function GetDataCbTPL(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
        Dim BO As IInsEndorsment
        BO = ComponentFactory.CreateInsEndorsment
        Return BO.GetDataCbTPL(customClass)
    End Function
#End Region
#Region "GetDataCbCoverage"
    Public Function GetDataCbCoverage(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
        Dim BO As IInsEndorsment
        BO = ComponentFactory.CreateInsEndorsment
        Return BO.GetDataCbCoverage(customClass)
    End Function
#End Region
#Region "GetEndorsCalculationResult"
    Public Function GetEndorsCalculationResult(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
        Dim BO As IInsEndorsment
        BO = ComponentFactory.CreateInsEndorsment
        Return BO.GetEndorsCalculationResult(customClass)
    End Function
#End Region
#Region "GetInsured_Coverage"
    Public Function GetInsured_Coverage(ByVal oCustomClass As Parameter.InsEndorsment, ByVal oData1 As DataTable) As Parameter.InsEndorsment
        Dim BO As IInsEndorsment
        BO = ComponentFactory.CreateInsEndorsment
        Return BO.GetInsured_Coverage(oCustomClass, oData1)
    End Function
#End Region
    Public Function SaveData(ByVal customClass As Parameter.InsEndorsment, ByVal result As DataTable) As Parameter.InsEndorsment
        Dim BO As IInsEndorsment
        BO = ComponentFactory.CreateInsEndorsment
        Return BO.SaveData(customClass, result)
    End Function

    Public Function PrintForm(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
        Dim BO As IInsEndorsment
        BO = ComponentFactory.CreateInsEndorsment
        Return BO.PrintForm(customClass)
    End Function
    Public Function ViewData(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
        Dim BO As IInsEndorsment
        BO = ComponentFactory.CreateInsEndorsment
        Return BO.ViewData(customClass)
    End Function
End Class
