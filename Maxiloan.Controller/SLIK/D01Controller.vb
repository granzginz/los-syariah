﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class D01Controller
    Public Function GetD01(ByVal oCustomClass As Parameter.D01) As Parameter.D01
        Dim D01BO As ID01
        D01BO = ComponentFactory.CreateD01()
        Return D01BO.GetD01(oCustomClass)
    End Function

    Public Function GetSelectD01(ByVal oCustomClass As Parameter.D01) As Parameter.D01
        Dim D01BO As ID01
        D01BO = ComponentFactory.CreateD01()
        Return D01BO.GetD01(oCustomClass)
    End Function

    Public Function GetD01Edit(ByVal oCustomClass As Parameter.D01) As Parameter.D01
        Dim D01BO As ID01
        D01BO = ComponentFactory.CreateD01()
        Return D01BO.GetD01Edit(oCustomClass)
    End Function

    Public Function GetD01Save(ByVal oCustomClass As Parameter.D01) As Parameter.D01
        Dim D01BO As ID01
        D01BO = ComponentFactory.CreateD01()
        Return D01BO.GetD01Save(oCustomClass)
    End Function

    Public Function GetD01Add(ByVal oCustomClass As Parameter.D01) As Parameter.D01
        Dim D01BO As ID01
        D01BO = ComponentFactory.CreateD01()
        Return D01BO.GetD01Add(oCustomClass)
    End Function

    Public Function GetD01Delete(ByVal oCustomClass As Parameter.D01) As String
        Dim D01BO As ID01
        D01BO = ComponentFactory.CreateD01()
        Return D01BO.GetD01Delete(oCustomClass)
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.D01) As Parameter.D01
        Dim D01BO As ID01
        D01BO = ComponentFactory.CreateD01()
        Return D01BO.GetCboBulandataSIPP(oCustomClass)
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.D01) As Parameter.D01
        Dim D01BO As ID01
        D01BO = ComponentFactory.CreateD01()
        Return D01BO.GetCbo(oCustomClass)
    End Function

    Public Function GetCbo1(ByVal oCustomClass As Parameter.D01) As Parameter.D01
        Dim D01BO As ID01
        D01BO = ComponentFactory.CreateD01()
        Return D01BO.GetCbo1(oCustomClass)
    End Function
End Class
