﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class F01Controller
    Public Function GetF01(ByVal oCustomClass As Parameter.F01) As Parameter.F01
        Dim F01BO As IF01
        F01BO = ComponentFactory.CreateF01()
        Return F01BO.GetF01(oCustomClass)
    End Function

    Public Function GetSelectF01(ByVal oCustomClass As Parameter.F01) As Parameter.F01
        Dim F01BO As IF01
        F01BO = ComponentFactory.CreateF01()
        Return F01BO.GetF01(oCustomClass)
    End Function

    Public Function GetF01Edit(ByVal oCustomClass As Parameter.F01) As Parameter.F01
        Dim F01BO As IF01
        F01BO = ComponentFactory.CreateF01()
        Return F01BO.GetF01Edit(oCustomClass)
    End Function

    Public Function GetF01Save(ByVal oCustomClass As Parameter.F01) As Parameter.F01
        Dim F01BO As IF01
        F01BO = ComponentFactory.CreateF01()
        Return F01BO.GetF01Save(oCustomClass)
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.F01) As Parameter.F01
        Dim F01BO As IF01
        F01BO = ComponentFactory.CreateF01()
        Return F01BO.GetCbo(oCustomClass)
    End Function

    Public Function GetF01Add(ByVal oCustomClass As Parameter.F01) As Parameter.F01
        Dim F01BO As IF01
        F01BO = ComponentFactory.CreateF01()
        Return F01BO.GetF01Add(oCustomClass)
    End Function

    Public Function GetF01Delete(ByVal oCustomClass As Parameter.F01) As String
        Dim F01BO As IF01
        F01BO = ComponentFactory.CreateF01()
        Return F01BO.GetF01Delete(oCustomClass)
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.F01) As Parameter.F01
        Dim F01BO As IF01
        F01BO = ComponentFactory.CreateF01()
        Return F01BO.GetCboBulandataSIPP(oCustomClass)
    End Function
End Class
