﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class S01Controller
    Public Function GetS01(ByVal oCustomClass As Parameter.S01) As Parameter.S01
        Dim S01BO As IS01
        S01BO = ComponentFactory.CreateS01()
        Return S01BO.GetS01(oCustomClass)
	End Function

	Public Function GetSelectS01(ByVal oCustomClass As Parameter.S01) As Parameter.S01
		Dim S01BO As IS01
        S01BO = ComponentFactory.CreateS01()
        Return S01BO.GetS01(oCustomClass)
	End Function

	Public Function GetS01Edit(ByVal oCustomClass As Parameter.S01) As Parameter.S01
		Dim S01BO As IS01
        S01BO = ComponentFactory.CreateS01()
        Return S01BO.GetS01Edit(oCustomClass)
	End Function

	Public Function GetS01Save(ByVal oCustomClass As Parameter.S01) As Parameter.S01
		Dim S01BO As IS01
        S01BO = ComponentFactory.CreateS01()
        Return S01BO.GetS01Save(oCustomClass)
	End Function

	Public Function GetS01Add(ByVal oCustomClass As Parameter.S01) As Parameter.S01
		Dim S01BO As IS01
        S01BO = ComponentFactory.CreateS01()
        Return S01BO.GetS01Add(oCustomClass)
	End Function

	Public Function GetS01Delete(ByVal oCustomClass As Parameter.S01) As String
		Dim S01BO As IS01
        S01BO = ComponentFactory.CreateS01()
        Return S01BO.GetS01Delete(oCustomClass)
	End Function

	Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.S01) As Parameter.S01
		Dim S01BO As IS01
        S01BO = ComponentFactory.CreateS01()
        Return S01BO.GetCboBulandataSIPP(oCustomClass)
	End Function

	Public Function GetCbo(ByVal oCustomClass As Parameter.S01) As Parameter.S01
		Dim S01BO As IS01
        S01BO = ComponentFactory.CreateS01()
        Return S01BO.GetCbo(oCustomClass)
	End Function

	Public Function GetCbo1(ByVal oCustomClass As Parameter.S01) As Parameter.S01
		Dim S01BO As IS01
        S01BO = ComponentFactory.CreateS01()
        Return S01BO.GetCbo1(oCustomClass)
    End Function
End Class
