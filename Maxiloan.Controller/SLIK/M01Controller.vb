﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class M01Controller
    Public Function GetM01(ByVal oCustomClass As Parameter.M01) As Parameter.M01
        Dim M01BO As IM01
        M01BO = ComponentFactory.CreateM01()
        Return M01BO.GetM01(oCustomClass)
    End Function

    Public Function GetSelectM01(ByVal oCustomClass As Parameter.M01) As Parameter.M01
        Dim M01BO As IM01
        M01BO = ComponentFactory.CreateM01()
        Return M01BO.GetM01(oCustomClass)
    End Function

    Public Function GetM01Edit(ByVal oCustomClass As Parameter.M01) As Parameter.M01
        Dim M01BO As IM01
        M01BO = ComponentFactory.CreateM01()
        Return M01BO.GetM01Edit(oCustomClass)
    End Function

    Public Function GetM01Save(ByVal oCustomClass As Parameter.M01) As Parameter.M01
        Dim M01BO As IM01
        M01BO = ComponentFactory.CreateM01()
        Return M01BO.GetM01Save(oCustomClass)
    End Function

    Public Function GetM01Add(ByVal oCustomClass As Parameter.M01) As Parameter.M01
        Dim M01BO As IM01
        M01BO = ComponentFactory.CreateM01()
        Return M01BO.GetM01Add(oCustomClass)
    End Function

    Public Function GetM01Delete(ByVal oCustomClass As Parameter.M01) As String
        Dim M01BO As IM01
        M01BO = ComponentFactory.CreateM01()
        Return M01BO.GetM01Delete(oCustomClass)
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.M01) As Parameter.M01
        Dim M01BO As IM01
        M01BO = ComponentFactory.CreateM01()
        Return M01BO.GetCboBulandataSIPP(oCustomClass)
    End Function
End Class
