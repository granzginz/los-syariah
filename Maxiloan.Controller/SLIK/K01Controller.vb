﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class K01Controller
	Public Function GetK01(ByVal oCustomClass As Parameter.K01) As Parameter.K01
		Dim K01BO As IK01
		K01BO = ComponentFactory.CreateK01()
		Return K01BO.GetK01(oCustomClass)
	End Function

	Public Function GetSelectK01(ByVal oCustomClass As Parameter.K01) As Parameter.K01
		Dim K01BO As IK01
		K01BO = ComponentFactory.CreateK01()
		Return K01BO.GetK01(oCustomClass)
	End Function

	Public Function GetK01Edit(ByVal oCustomClass As Parameter.K01) As Parameter.K01
		Dim K01BO As IK01
		K01BO = ComponentFactory.CreateK01()
		Return K01BO.GetK01Edit(oCustomClass)
	End Function

	Public Function GetK01Save(ByVal oCustomClass As Parameter.K01) As Parameter.K01
		Dim K01BO As IK01
		K01BO = ComponentFactory.CreateK01()
		Return K01BO.GetK01Save(oCustomClass)
	End Function

	Public Function GetK01Add(ByVal oCustomClass As Parameter.K01) As Parameter.K01
		Dim K01BO As IK01
		K01BO = ComponentFactory.CreateK01()
		Return K01BO.GetK01Add(oCustomClass)
	End Function

	Public Function GetK01Delete(ByVal oCustomClass As Parameter.K01) As String
		Dim K01BO As IK01
		K01BO = ComponentFactory.CreateK01()
		Return K01BO.GetK01Delete(oCustomClass)
	End Function

	Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.K01) As Parameter.K01
		Dim K01BO As IK01
		K01BO = ComponentFactory.CreateK01()
		Return K01BO.GetCboBulandataSIPP(oCustomClass)
	End Function

	Public Function GetCbo(ByVal oCustomClass As Parameter.K01) As Parameter.K01
		Dim K01BO As IK01
		K01BO = ComponentFactory.CreateK01()
		Return K01BO.GetCbo(oCustomClass)
	End Function
End Class
