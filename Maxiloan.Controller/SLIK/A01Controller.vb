﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class A01Controller
	Public Function GetA01(ByVal oCustomClass As Parameter.A01) As Parameter.A01
		Dim A01BO As IA01
		A01BO = ComponentFactory.CreateA01()
		Return A01BO.GetA01(oCustomClass)
	End Function

	Public Function GetSelectA01(ByVal oCustomClass As Parameter.A01) As Parameter.A01
		Dim A01BO As IA01
		A01BO = ComponentFactory.CreateA01()
		Return A01BO.GetA01(oCustomClass)
	End Function

	Public Function GetA01Edit(ByVal oCustomClass As Parameter.A01) As Parameter.A01
		Dim A01BO As IA01
		A01BO = ComponentFactory.CreateA01()
		Return A01BO.GetA01Edit(oCustomClass)
	End Function

	Public Function GetA01Save(ByVal oCustomClass As Parameter.A01) As Parameter.A01
		Dim A01BO As IA01
		A01BO = ComponentFactory.CreateA01()
		Return A01BO.GetA01Save(oCustomClass)
	End Function

	Public Function GetA01Add(ByVal oCustomClass As Parameter.A01) As Parameter.A01
		Dim A01BO As IA01
		A01BO = ComponentFactory.CreateA01()
		Return A01BO.GetA01Add(oCustomClass)
	End Function

	Public Function GetA01Delete(ByVal oCustomClass As Parameter.A01) As String
		Dim A01BO As IA01
		A01BO = ComponentFactory.CreateA01()
		Return A01BO.GetA01Delete(oCustomClass)
	End Function

	Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.A01) As Parameter.A01
		Dim A01BO As IA01
		A01BO = ComponentFactory.CreateA01()
		Return A01BO.GetCboBulandataSIPP(oCustomClass)
	End Function

	Public Function GetCbo(ByVal oCustomClass As Parameter.A01) As Parameter.A01
		Dim A01BO As IA01
		A01BO = ComponentFactory.CreateA01()
		Return A01BO.GetCbo(oCustomClass)
	End Function
End Class
