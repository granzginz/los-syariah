﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class P01Controller
    Public Function GetP01(ByVal oCustomClass As Parameter.P01) As Parameter.P01
        Dim P01BO As IP01
        P01BO = ComponentFactory.CreateP01()
        Return P01BO.GetP01(oCustomClass)
    End Function

    Public Function GetSelectP01(ByVal oCustomClass As Parameter.P01) As Parameter.P01
        Dim P01BO As IP01
        P01BO = ComponentFactory.CreateP01()
        Return P01BO.GetP01(oCustomClass)
    End Function

    Public Function GetP01Edit(ByVal oCustomClass As Parameter.P01) As Parameter.P01
        Dim P01BO As IP01
        P01BO = ComponentFactory.CreateP01()
        Return P01BO.GetP01Edit(oCustomClass)
    End Function

    Public Function GetP01Save(ByVal oCustomClass As Parameter.P01) As Parameter.P01
        Dim P01BO As IP01
        P01BO = ComponentFactory.CreateP01()
        Return P01BO.GetP01Save(oCustomClass)
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.P01) As Parameter.P01
        Dim P01BO As IP01
        P01BO = ComponentFactory.CreateP01()
        Return P01BO.GetCbo(oCustomClass)
    End Function

    Public Function GetP01Add(ByVal oCustomClass As Parameter.P01) As Parameter.P01
        Dim P01BO As IP01
        P01BO = ComponentFactory.CreateP01()
        Return P01BO.GetP01Add(oCustomClass)
    End Function

    Public Function GetP01Delete(ByVal oCustomClass As Parameter.P01) As String
        Dim P01BO As IP01
        P01BO = ComponentFactory.CreateP01()
        Return P01BO.GetP01Delete(oCustomClass)
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.P01) As Parameter.P01
        Dim P01BO As IP01
        P01BO = ComponentFactory.CreateP01()
        Return P01BO.GetCboBulandataSIPP(oCustomClass)
    End Function
End Class

