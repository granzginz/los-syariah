﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class F06Controller
    Public Function GetF06(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim F06BO As IF06
        F06BO = ComponentFactory.CreateF06()
        Return F06BO.GetF06(oCustomClass)
    End Function

    Public Function GetSelectF06(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim F06BO As IF06
        F06BO = ComponentFactory.CreateF06()
        Return F06BO.GetF06(oCustomClass)
    End Function

    Public Function GetF06Edit(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim F06BO As IF06
        F06BO = ComponentFactory.CreateF06()
        Return F06BO.GetF06Edit(oCustomClass)
    End Function

    Public Function GetF06Save(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim F06BO As IF06
        F06BO = ComponentFactory.CreateF06()
        Return F06BO.GetF06Save(oCustomClass)
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim F06BO As IF06
        F06BO = ComponentFactory.CreateF06()
        Return F06BO.GetCbo(oCustomClass)
    End Function

    Public Function GetCbo1(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim F06BO As IF06
        F06BO = ComponentFactory.CreateF06()
        Return F06BO.GetCbo1(oCustomClass)
    End Function

    Public Function GetCbo2(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim F06BO As IF06
        F06BO = ComponentFactory.CreateF06()
        Return F06BO.GetCbo2(oCustomClass)
    End Function

    Public Function GetCbo3(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim F06BO As IF06
        F06BO = ComponentFactory.CreateF06()
        Return F06BO.GetCbo3(oCustomClass)
    End Function

    Public Function GetCbo4(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim F06BO As IF06
        F06BO = ComponentFactory.CreateF06()
        Return F06BO.GetCbo4(oCustomClass)
    End Function

    Public Function GetF06Add(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim F06BO As IF06
        F06BO = ComponentFactory.CreateF06()
        Return F06BO.GetF06Add(oCustomClass)
    End Function

    Public Function GetF06Delete(ByVal oCustomClass As Parameter.F06) As String
        Dim F06BO As IF06
        F06BO = ComponentFactory.CreateF06()
        Return F06BO.GetF06Delete(oCustomClass)
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.F06) As Parameter.F06
        Dim F06BO As IF06
        F06BO = ComponentFactory.CreateF06()
        Return F06BO.GetCboBulandataSIPP(oCustomClass)
    End Function
End Class
