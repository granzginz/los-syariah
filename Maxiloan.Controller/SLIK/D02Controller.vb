﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class D02Controller

    Public Function GetD02(ByVal oCustomClass As Parameter.D02) As Parameter.D02
        Dim D02BO As ID02
        D02BO = ComponentFactory.CreateD02()
        Return D02BO.GetD02(oCustomClass)
    End Function

    Public Function GetSelectD02(ByVal oCustomClass As Parameter.D02) As Parameter.D02
        Dim D02BO As ID02
        D02BO = ComponentFactory.CreateD02()
        Return D02BO.GetD02(oCustomClass)
    End Function

    Public Function GetD02Edit(ByVal oCustomClass As Parameter.D02) As Parameter.D02
        Dim D02BO As ID02
        D02BO = ComponentFactory.CreateD02()
        Return D02BO.GetD02Edit(oCustomClass)
    End Function

    Public Function GetD02Save(ByVal oCustomClass As Parameter.D02) As Parameter.D02
        Dim D02BO As ID02
        D02BO = ComponentFactory.CreateD02()
        Return D02BO.GetD02Save(oCustomClass)
    End Function
    Public Function GetD02Add(ByVal oCustomClass As Parameter.D02) As Parameter.D02
        Dim D02BO As ID02
        D02BO = ComponentFactory.CreateD02()
        Return D02BO.GetD02Add(oCustomClass)
    End Function
    Public Function GetD02Delete(ByVal oCustomClass As Parameter.D02) As String
        Dim D02BO As ID02
        D02BO = ComponentFactory.CreateD02()
        Return D02BO.GetD02Delete(oCustomClass)
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.D02) As Parameter.D02
        Dim D02BO As ID02
        D02BO = ComponentFactory.CreateD02()
        Return D02BO.GetCbo(oCustomClass)
    End Function

    Public Function GetCbo1(ByVal oCustomClass As Parameter.D02) As Parameter.D02
        Dim D02BO As ID02
        D02BO = ComponentFactory.CreateD02()
        Return D02BO.GetCbo1(oCustomClass)
    End Function

    Public Function GetCbo2(ByVal oCustomClass As Parameter.D02) As Parameter.D02
        Dim D02BO As ID02
        D02BO = ComponentFactory.CreateD02()
        Return D02BO.GetCbo2(oCustomClass)
    End Function

    Public Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.D02) As Parameter.D02
        Dim D02BO As ID02
        D02BO = ComponentFactory.CreateD02()
        Return D02BO.GetCboBulandataSIPP(oCustomClass)
    End Function
End Class
