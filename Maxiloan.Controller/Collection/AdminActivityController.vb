
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AdminActivityController
    Public Function AdminActivityList(ByVal oCustomClass As Parameter.AdminActivity) As Parameter.AdminActivity
        Dim oAdmActivity As IAdminActivity
        oAdmActivity = ComponentFactory.CreateAdminActivity
        Return oAdmActivity.AdminActivityList(oCustomClass)
    End Function

    Public Function AdminActivityDataAgreement(ByVal oCustomClass As Parameter.AdminActivity) As Parameter.AdminActivity
        Dim oAdmActivity As IAdminActivity
        oAdmActivity = ComponentFactory.CreateAdminActivity
        Return oAdmActivity.AdminActivityDataAgreement(oCustomClass)
    End Function
    Public Function AdminActivityAction(ByVal oCustomClass As Parameter.AdminActivity) As Parameter.AdminActivity
        Dim oAdmActivity As IAdminActivity
        oAdmActivity = ComponentFactory.CreateAdminActivity
        Return oAdmActivity.AdminActivityAction(oCustomClass)
    End Function
    Public Function AdminActivitySave(ByVal oCustomClass As Parameter.AdminActivity) As Parameter.AdminActivity
        Dim oAdmActivity As IAdminActivity
        oAdmActivity = ComponentFactory.CreateAdminActivity
        Return oAdmActivity.AdminActivitySave(oCustomClass)
    End Function
End Class
