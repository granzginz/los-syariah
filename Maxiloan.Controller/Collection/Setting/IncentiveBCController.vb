
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class IncentiveBCController
    Public Function IncentiveBCPaging(ByVal oCustomClass As Parameter.IncentiveBC) As Parameter.IncentiveBC
        Dim oIncentiveBC As IIncentiveBC
        oIncentiveBC = ComponentFactory.CreateIncentiveBC
        Return oIncentiveBC.IncentiveBCPaging(oCustomClass)
    End Function
    Public Sub IncentiveBCSave(ByVal oCustomClass As Parameter.IncentiveBC)
        Dim oIncentiveBC As IIncentiveBC
        oIncentiveBC = ComponentFactory.CreateIncentiveBC
        oIncentiveBC.IncentiveBCSave(oCustomClass)
    End Sub
    Public Sub IncentiveBCDelete(ByVal oCustomClass As Parameter.IncentiveBC)
        Dim oIncentiveBC As IIncentiveBC
        oIncentiveBC = ComponentFactory.CreateIncentiveBC
        oIncentiveBC.IncentiveBCDelete(oCustomClass)

    End Sub
    Public Sub IncentiveBCUpdate(ByVal oCustomClass As Parameter.IncentiveBC)
        Dim oIncentiveBC As IIncentiveBC
        oIncentiveBC = ComponentFactory.CreateIncentiveBC
        oIncentiveBC.IncentiveBCUpdate(oCustomClass)
    End Sub
    Public Function IncentiveBCReports(ByVal oCustomClass As Parameter.IncentiveBC) As Parameter.IncentiveBC
        Dim oincinterface As IIncentiveBC
        oincinterface = ComponentFactory.CreateIncentiveBC
        Return oincinterface.IncentiveBCReports(oCustomClass)

    End Function
End Class
