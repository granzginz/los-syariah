

Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class CollZipCodeController

    Function GetCollZipCodeList(ByVal oCollZipCode As Parameter.CollZipCode) As Parameter.CollZipCode
        Dim CollZipCodeBO As ICollZipCode
        CollZipCodeBO = ComponentFactory.CreateCollZipCode
        Return CollZipCodeBO.GetCollZipCodeList(oCollZipCode)
    End Function



    Public Sub CollZipCodeEdit(ByVal oCollZipCode As Parameter.CollZipCode)
        Dim CollZipCodeBO As ICollZipCode
        CollZipCodeBO = ComponentFactory.CreateCollZipCode
        CollZipCodeBO.CollZipCodeEdit(oCollZipCode)
    End Sub


    Function GetCityCombo(ByVal strconn As String) As DataTable
        Dim CollZipCodeBO As ICollZipCode
        CollZipCodeBO = ComponentFactory.CreateCollZipCode
        Return CollZipCodeBO.GetCityCombo(strconn)
    End Function

    Function GetCollectionGroupCombo(ByVal strconn As String) As DataTable
        Dim CollZipCodeBO As ICollZipCode
        CollZipCodeBO = ComponentFactory.CreateCollZipCode
        Return CollZipCodeBO.getCGCombo(strconn)
    End Function


    Function GetCollectorZipCodeList(ByVal oCollZipCode As Parameter.CollZipCode) As Parameter.CollZipCode
        Dim CollZipCodeBO As ICollZipCode
        CollZipCodeBO = ComponentFactory.CreateCollZipCode
        Return CollZipCodeBO.GetCollectorZipCodeList(oCollZipCode)
    End Function

    Function GetCollectorCombo(ByVal oCollZipCode As Parameter.CollZipCode) As DataTable
        Dim CollZipCodeBO As ICollZipCode
        CollZipCodeBO = ComponentFactory.CreateCollZipCode
        Return CollZipCodeBO.getCollectorCombo(oCollZipCode)
    End Function

    Public Sub CollectorZipCodeEdit(ByVal oCollZipCode As Parameter.CollZipCode)
        Dim CollZipCodeBO As ICollZipCode
        CollZipCodeBO = ComponentFactory.CreateCollZipCode
        CollZipCodeBO.CollectorZipCodeEdit(oCollZipCode)
    End Sub

    Function GetCollZipCodeReport(ByVal strconn As String, ByVal city As String) As Parameter.CollZipCode
        Dim CollZipCodeBO As ICollZipCode
        CollZipCodeBO = ComponentFactory.CreateCollZipCode
        Return CollZipCodeBO.GetCollZipCodeReport(strconn, city)
    End Function

    Function GetCollectorZipCodeReport(ByVal strconn As String, ByVal cgid As String) As Parameter.CollZipCode
        Dim CollZipCodeBO As ICollZipCode
        CollZipCodeBO = ComponentFactory.CreateCollZipCode
        Return CollZipCodeBO.GetCollectorZipCodeReport(strconn, cgid)
    End Function


End Class
