

Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class CollectorController
    Function GetCollectorList(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.GetCollectorList(oCollector)
    End Function

    Function GetCollectorByID(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.GetCollectorByID(oCollector)
    End Function

    Function GetCollectorReport(ByVal strconn As String) As Parameter.Collector
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.GetCollectorReport(strconn)
    End Function

    Public Sub CollectorEdit(ByVal oCollector As Parameter.Collector)
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        CollectorBO.CollectorEdit(oCollector)
    End Sub

    Public Sub CollectorAdd(ByVal oCollector As Parameter.Collector)
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        CollectorBO.CollectorAdd(oCollector)
    End Sub

    Public Sub CollectorDelete(ByVal oCollector As Parameter.Collector)
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        CollectorBO.CollectorDelete(oCollector)
    End Sub

    Function GetBranchAddress(ByVal oCollector As Parameter.Collector) As String
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.GetBranchAddress(oCollector)
    End Function

    Function GetCollectorZipCode(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.GetCollectorZipCode(oCollector)
    End Function

    Function GetCollectorRAL(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.GetCollectorRAL(oCollector)
    End Function

    Function GetCollectorDetail(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.GetCollectorDetail(oCollector)
    End Function

    Function GetCollectorCombo(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.GetCollectorCombo(oCollector)
    End Function

    Function GetCollectorTypeCombo(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.GetCollectorTypeCombo(oCollector)
    End Function

    Function GetSupervisorCombo(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.GetSupervisorCombo(oCollector)
    End Function

    Function GetCollectionAllocationPaging(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.GetCollectorAllocationPaging(oCollector)
    End Function

    Sub CollectionAllocationSave(ByVal oCollector As Parameter.Collector)
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        CollectorBO.CollectorAllocationSave(oCollector)
    End Sub

    Function GetCollectorZPList(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.GetCollectorZPList(oCollector)
    End Function

    Function CollectorZPSave(ByVal oCollector As Parameter.Collector, ByVal oData As DataTable) As Parameter.Collector
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.CollectorZPSave(oCollector, oData)
    End Function

    Function GetCollectorZPDetail(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.GetCollectorZPDetail(oCollector)
    End Function

    Function GetCollectorZPByKecamatan(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.GetCollectorZPByKecamatan(oCollector)
    End Function

    Public Sub CollectorEditMaxAcc(ByVal oCollector As Parameter.Collector)
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        CollectorBO.CollectorEditMaxAcc(oCollector)
    End Sub

    Function GetCollectorDeskCollByID(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.GetCollectorDeskCollByID(oCollector)
    End Function

    Public Sub CollectorDeskCollSave(ByVal oCollector As Parameter.Collector)
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        CollectorBO.CollectorDeskCollSave(oCollector)
    End Sub

    Public Sub RollingWilayahSave(ByVal oCollector As Parameter.Collector)
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        CollectorBO.RollingWilayahSave(oCollector)
    End Sub

    Public Sub RollingCollectorSave(ByVal oCollector As Parameter.Collector)
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        CollectorBO.RollingCollectorSave(oCollector)
    End Sub

    Public Sub GantiCollectorSave(ByVal oCollector As Parameter.Collector)
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        CollectorBO.GantiCollectorSave(oCollector)
    End Sub

    Function GetPemberiKuasaSKEList(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.GetPemberiKuasaSKEList(oCollector)
    End Function

    Function PemberiKuasaSKEDelete(ByVal oCollector As Parameter.Collector) As String
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.PemberiKuasaSKEDelete(oCollector)
    End Function

    Function PemberiKuasaSKEAdd(ByVal oCollector As Parameter.Collector) As String
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.PemberiKuasaSKEAdd(oCollector)
    End Function

    Function PemberiKuasaSKEEdit(ByVal oCollector As Parameter.Collector) As String
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.PemberiKuasaSKEEdit(oCollector)
    End Function
End Class
