

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class CasesSettingController
#Region "Cases Maintenance"
    Public Function CasesSettingList(ByVal oCustomClass As Parameter.CasesSetting) As Parameter.CasesSetting
        Dim oCasesSettingList As ICasesSetting
        oCasesSettingList = ComponentFactory.CreateCasesSetting()
        Return oCasesSettingList.ListCasesSetting(oCustomClass)
    End Function

    Public Function CasesSettingReport(ByVal oCustomClass As Parameter.CasesSetting) As Parameter.CasesSetting
        Dim oCasesSettingList As ICasesSetting
        oCasesSettingList = ComponentFactory.CreateCasesSetting()
        Return oCasesSettingList.ReportCasesSetting(oCustomClass)
    End Function

    Public Function CasesSettingAdd(ByVal oCustomClass As Parameter.CasesSetting) As String
        Dim oCasesSettingList As ICasesSetting
        oCasesSettingList = ComponentFactory.CreateCasesSetting()
        Return oCasesSettingList.AddCasesSetting(oCustomClass)
    End Function

    Public Function CasesSettingEdit(ByVal oCustomClass As Parameter.CasesSetting) As String
        Dim oCasesSettingList As ICasesSetting
        oCasesSettingList = ComponentFactory.CreateCasesSetting()
        Return oCasesSettingList.EditCasesSetting(oCustomClass)
    End Function

    Public Function CasesSettingDelete(ByVal oCustomClass As Parameter.CasesSetting) As String
        Dim oCasesSettingList As ICasesSetting
        oCasesSettingList = ComponentFactory.CreateCasesSetting()
        Return oCasesSettingList.DeleteCasesSetting(oCustomClass)
    End Function
    Public Function ListCases(ByVal customclass As Parameter.CasesSetting) As Parameter.CasesSetting
        Dim oCasesSettingList As ICasesSetting
        oCasesSettingList = ComponentFactory.CreateCasesSetting()
        Return oCasesSettingList.ListCases(customclass)
    End Function
#End Region
End Class
