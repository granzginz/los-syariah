﻿#Region "import'"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class LokasiAssetController
    Public Function LokasiAsset(ByVal oCustomClass As Parameter.LokasiAsset) As Parameter.LokasiAsset
        Dim oLokasiAsset As ILokasiAsset
        oLokasiAsset = ComponentFactory.CreateLokasiAsset()
        Return oLokasiAsset.ListLokasiAsset(oCustomClass)
    End Function
    Public Function LokasiAssetEdit(ByVal oCustomClass As Parameter.LokasiAsset, ByVal oAddress As Parameter.Address) As String
        Dim oLokasiAsset As ILokasiAsset
        oLokasiAsset = ComponentFactory.CreateLokasiAsset()
        Return oLokasiAsset.EditLokasiAsset(oCustomClass, oAddress)
    End Function
    Public Function LokasiAssetAdd(ByVal oCustomClass As Parameter.LokasiAsset, ByVal oAddress As Parameter.Address) As String
        Dim oLokasiAsset As ILokasiAsset
        oLokasiAsset = ComponentFactory.CreateLokasiAsset()
        Return oLokasiAsset.AddLokasiAsset(oCustomClass, oAddress)
    End Function
    Public Function LokasiAssetDelete(ByVal oCustomClass As Parameter.LokasiAsset) As String
        Dim oLokasiAsset As ILokasiAsset
        oLokasiAsset = ComponentFactory.CreateLokasiAsset()
        Return oLokasiAsset.DeleteLokasiAsset(oCustomClass)
    End Function
    Public Function LokasiAssetReport(ByVal oCustomClass As Parameter.LokasiAsset) As Parameter.LokasiAsset
        Dim oLokasiAsset As ILokasiAsset
        oLokasiAsset = ComponentFactory.CreateLokasiAsset()
        Return oLokasiAsset.ReportLokasiAsset(oCustomClass)
    End Function
End Class
