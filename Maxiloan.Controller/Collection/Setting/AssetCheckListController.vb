
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AssetCheckListController
    Public Function AssetCheckList(ByVal oCustomClass As Parameter.AssetCheckList) As Parameter.AssetCheckList
        Dim oAssetCheckList As IAssetCheckList
        oAssetCheckList = ComponentFactory.CreateAssetCheckList()
        Return oAssetCheckList.ListAssetCheckList(oCustomClass)
    End Function

    Public Function AssetCheckListEdit(ByVal oCustomClass As Parameter.AssetCheckList) As String
        Dim oAssetCheckList As IAssetCheckList
        oAssetCheckList = ComponentFactory.CreateAssetCheckList()
        Return oAssetCheckList.EditAssetCheckList(oCustomClass)
    End Function

    Public Function AssetCheckListAdd(ByVal oCustomClass As Parameter.AssetCheckList) As String
        Dim oAssetCheckList As IAssetCheckList
        oAssetCheckList = ComponentFactory.CreateAssetCheckList()
        Return oAssetCheckList.AddAssetCheckList(oCustomClass)
    End Function

    Public Function AssetCheckListLookUp(ByVal oCustomClass As Parameter.AssetCheckList) As Parameter.AssetCheckList
        Dim oAssetCheckList As IAssetCheckList
        oAssetCheckList = ComponentFactory.CreateAssetCheckList()
        Return oAssetCheckList.ListAssetCheckListLookUp(oCustomClass)
    End Function

    Public Function AssetCheckListDelete(ByVal oCustomClass As Parameter.AssetCheckList) As String
        Dim oAssetCheckList As IAssetCheckList
        oAssetCheckList = ComponentFactory.CreateAssetCheckList()
        Return oAssetCheckList.DeleteAssetCheckList(oCustomClass)
    End Function


    Public Function ReportAssetCheckList(ByVal oCustomClass As Parameter.AssetCheckList) As Parameter.AssetCheckList
        Dim oAssetCheckList As IAssetCheckList
        oAssetCheckList = ComponentFactory.CreateAssetCheckList()
        Return oAssetCheckList.ReportAssetCheckList(oCustomClass)
    End Function
End Class

