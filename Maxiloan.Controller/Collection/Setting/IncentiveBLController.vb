
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class IncentiveBLController
    Public Function IncentiveBLPaging(ByVal oCustomClass As Parameter.IncentiveBL) As Parameter.IncentiveBL
        Dim oIncentiveBL As IIncentiveBL
        oIncentiveBL = ComponentFactory.CreateIncentiveBL
        Return oIncentiveBL.IncentiveBLPaging(oCustomClass)

    End Function
    Public Sub IncentiveBLSave(ByVal oCustomClass As Parameter.IncentiveBL)
        Dim oIncentiveBL As IIncentiveBL
        oIncentiveBL = ComponentFactory.CreateIncentiveBL
        oIncentiveBL.IncentiveBLSave(oCustomClass)

    End Sub
    Public Sub IncentiveBLUpdate(ByVal oCustomClass As Parameter.IncentiveBL)
        Dim oIncentiveBL As IIncentiveBL
        oIncentiveBL = ComponentFactory.CreateIncentiveBL
        oIncentiveBL.IncentiveBLUpdate(oCustomClass)
    End Sub
    Public Sub IncentiveBLDelete(ByVal oCustomClass As Parameter.IncentiveBL)
        Dim oIncentiveBL As IIncentiveBL
        oIncentiveBL = ComponentFactory.CreateIncentiveBL
        oIncentiveBL.IncentiveBLDelete(oCustomClass)
    End Sub
    Public Function IncentiveBLReports(ByVal oCustomClass As Parameter.IncentiveBL) As Parameter.IncentiveBL
        Dim oIncentiveBL As IIncentiveBL
        oIncentiveBL = ComponentFactory.CreateIncentiveBL
        Return oIncentiveBL.IncentiveBLReports(oCustomClass)
    End Function

End Class
