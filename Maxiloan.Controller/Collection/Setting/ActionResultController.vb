

Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class ActionResultController

    Function GetActionList(ByVal oAction As Parameter.ActionResult) As Parameter.ActionResult
        Dim ActionBO As IActionResult
        ActionBO = ComponentFactory.CreateActionResult
        Return ActionBO.GetActionList(oAction)
    End Function

    Function GetActionByID(ByVal oAction As Parameter.ActionResult) As Parameter.ActionResult
        Dim ActionBO As IActionResult
        ActionBO = ComponentFactory.CreateActionResult
        Return ActionBO.GetActionByID(oAction)
    End Function

    Public Sub ActionAdd(ByVal oAction As Parameter.ActionResult)
        Dim ActionBO As IActionResult
        ActionBO = ComponentFactory.CreateActionResult
        ActionBO.ActionAdd(oAction)
    End Sub

    Function GetActionReport(ByVal strconn As String) As Parameter.ActionResult
        Dim ActionBO As IActionResult
        ActionBO = ComponentFactory.CreateActionResult
        Return ActionBO.GetActionReport(strconn)
    End Function


    Public Sub ActionEdit(ByVal oAction As Parameter.ActionResult)
        Dim ActionBO As IActionResult
        ActionBO = ComponentFactory.CreateActionResult
        ActionBO.ActionEdit(oAction)
    End Sub

    Public Sub ActionDelete(ByVal oAction As Parameter.ActionResult)
        Dim ActionBO As IActionResult
        ActionBO = ComponentFactory.CreateActionResult
        ActionBO.ActionDelete(oAction)
    End Sub


    Function GetResultList(ByVal oResult As Parameter.ActionResult) As Parameter.ActionResult
        Dim ResultBO As IActionResult
        ResultBO = ComponentFactory.CreateActionResult
        Return ResultBO.GetResultList(oResult)
    End Function

    Function GetResultbyID(ByVal oResult As Parameter.ActionResult) As Parameter.ActionResult
        Dim ResultBO As IActionResult
        ResultBO = ComponentFactory.CreateActionResult
        Return ResultBO.GetResultByID(oResult)
    End Function

    Public Sub ResultAdd(ByVal oResult As Parameter.ActionResult)
        Dim ResultBO As IActionResult
        ResultBO = ComponentFactory.CreateActionResult
        ResultBO.ResultAdd(oResult)
    End Sub

    Public Sub ResultEdit(ByVal oResult As Parameter.ActionResult)
        Dim ResultBO As IActionResult
        ResultBO = ComponentFactory.CreateActionResult
        ResultBO.ResultEdit(oResult)
    End Sub

    Public Sub ResultDelete(ByVal oResult As Parameter.ActionResult)
        Dim ResultBO As IActionResult
        ResultBO = ComponentFactory.CreateActionResult
        ResultBO.ResultDelete(oResult)
    End Sub


    Function GetResultCategory(ByVal strConn As String) As Parameter.ActionResult
        Dim resultBO As IActionResult
        resultBO = ComponentFactory.CreateActionResult
        Return resultBO.GetResultCategory(strConn)
    End Function


End Class
