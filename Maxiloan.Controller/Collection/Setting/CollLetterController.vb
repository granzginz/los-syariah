
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class CollLetterController
    Public Function CollLetterFieldPaging(ByVal oCustomClass As Parameter.CollLetter) As Parameter.CollLetter
        Dim oCollLetter As ICollLetter
        oCollLetter = ComponentFactory.CollLetter
        Return oCollLetter.CollLetterFieldPaging(oCustomClass)
    End Function
    Public Function CollLetterPaging(ByVal oCustomClass As Parameter.CollLetter) As Parameter.CollLetter
        Dim oCollLetter As ICollLetter
        oCollLetter = ComponentFactory.CollLetter
        Return oCollLetter.CollLetterPaging(oCustomClass)
    End Function
    Public Sub CollLetterSave(ByVal oCustomClass As Parameter.CollLetter)
        Dim oCollLetter As ICollLetter
        oCollLetter = ComponentFactory.CollLetter
        oCollLetter.CollLetterSave(oCustomClass)
    End Sub
    Public Sub CollLetterUpdate(ByVal oCustomClass As Parameter.CollLetter)
        Dim oCollLetter As ICollLetter
        oCollLetter = ComponentFactory.CollLetter
        oCollLetter.CollLetterUpdate(oCustomClass)
    End Sub
    Public Sub CollLetterDelete(ByVal oCustomClass As Parameter.CollLetter)
        Dim oCollLetter As ICollLetter
        oCollLetter = ComponentFactory.CollLetter
        oCollLetter.CollLetterDelete(oCustomClass)
    End Sub
    Public Function CollLetterReports(ByVal oCustomClass As Parameter.CollLetter) As Parameter.CollLetter
        Dim oCollLetter As ICollLetter
        oCollLetter = ComponentFactory.CollLetter
        Return oCollLetter.CollLetterReports(oCustomClass)
    End Function

End Class
