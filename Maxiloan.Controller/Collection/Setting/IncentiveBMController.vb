
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Public Class IncentiveBMController
    Public Function IncentiveBMPaging(ByVal oCustomClass As Parameter.IncentiveBM) As Parameter.IncentiveBM
        Dim oIncBMInterface As IIncentiveBM
        oIncBMInterface = ComponentFactory.CreateIncentiveBM
        Return oIncBMInterface.IncentiveBMPaging(oCustomClass)

    End Function
    Public Sub IncentiveBmUpdate(ByVal oCustomClass As Parameter.IncentiveBM)
        Dim oIncBMInterface As IIncentiveBM
        oIncBMInterface = ComponentFactory.CreateIncentiveBM
        oIncBMInterface.IncentiveBmUpdate(oCustomClass)
    End Sub
    Public Sub IncentiveBMSave(ByVal oCustomClass As Parameter.IncentiveBM)
        Dim oincbminterface As IIncentiveBM
        oincbminterface = ComponentFactory.CreateIncentiveBM
        oincbminterface.IncentiveBMSave(oCustomClass)
    End Sub
    Public Function IncentiveBMComboList(ByVal oCustomClass As Parameter.IncentiveBM) As DataTable
        Dim oincbminterface As IIncentiveBM
        oincbminterface = ComponentFactory.CreateIncentiveBM
        Return oincbminterface.IncentiveBMComboList(oCustomClass)
    End Function
    Public Function IncentiveBMReports(ByVal oCustomClass As Parameter.IncentiveBM) As Parameter.IncentiveBM
        Dim oincinterface As IIncentiveBM
        oincinterface = ComponentFactory.CreateIncentiveBM
        Return oincinterface.IncentiveBMReports(oCustomClass)

    End Function
End Class
