

Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class CollGroupController
    Function GetCollGroupList(ByVal oCollGroup As Parameter.CollGroup) As Parameter.CollGroup
        Dim CollGroupBO As ICollGroup
        CollGroupBO = ComponentFactory.CreateCollGroup
        Return CollGroupBO.GetCollGroupList(oCollGroup)
    End Function

    Function GetCollGroupByID(ByVal oCollGroup As Parameter.CollGroup) As Parameter.CollGroup
        Dim CollGroupBO As ICollGroup
        CollGroupBO = ComponentFactory.CreateCollGroup
        Return CollGroupBO.GetCollGroupByID(oCollGroup)
    End Function

    Function GetCollGroupReport(ByVal strconn As String) As Parameter.CollGroup
        Dim CollGroupBO As ICollGroup
        CollGroupBO = ComponentFactory.CreateCollGroup
        Return CollGroupBO.GetCollGroupReport(strconn)
    End Function

    Public Sub CollGroupEdit(ByVal oCollGroup As Parameter.CollGroup, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim CollGroupBO As ICollGroup
        CollGroupBO = ComponentFactory.CreateCollGroup
        CollGroupBO.CollGroupEdit(oCollGroup, oClassAddress, oClassPersonal)
    End Sub

    Public Sub CollGroupAdd(ByVal oCollGroup As Parameter.CollGroup, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim CollGroupBO As ICollGroup
        CollGroupBO = ComponentFactory.CreateCollGroup
        CollGroupBO.CollGroupAdd(oCollGroup, oClassAddress, oClassPersonal)
    End Sub

    Public Sub CollGroupDelete(ByVal oCollGroup As Parameter.CollGroup)
        Dim CollGroupBO As ICollGroup
        CollGroupBO = ComponentFactory.CreateCollGroup
        CollGroupBO.CollGroupDelete(oCollGroup)
    End Sub

    Function GetBranchAddress(ByVal oCollGroup As Parameter.CollGroup) As DataTable
        Dim CollGroupBO As ICollGroup
        CollGroupBO = ComponentFactory.CreateCollGroup
        Return CollGroupBO.GetBranchAddress(oCollGroup)
    End Function

    Function GetBranchCombo(ByVal oCollGroup As Parameter.CollGroup) As Parameter.CollGroup
        Dim CollGroupBO As ICollGroup
        CollGroupBO = ComponentFactory.CreateCollGroup
        Return CollGroupBO.GetBranchCombo(oCollGroup)
    End Function


End Class
