

Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class CollectorProfController

    Function GetCollectorProfList(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollectorProf
        CollectorBO = ComponentFactory.CreateCollectorProf
        Return CollectorBO.GetCollectorProfList(oCollector)
    End Function

    Function GetCollectorProfByID(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollectorProf
        CollectorBO = ComponentFactory.CreateCollectorProf
        Return CollectorBO.GetCollectorProfByID(oCollector)
    End Function

    Function GetCollectorProfReport(ByVal strconn As String) As Parameter.Collector
        Dim CollectorBO As ICollectorProf
        CollectorBO = ComponentFactory.CreateCollectorProf
        Return CollectorBO.GetCollectorProfReport(strconn)
    End Function

    Public Sub CollectorProfEdit(ByVal oCollector As Parameter.Collector, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim CollectorBO As ICollectorProf
        CollectorBO = ComponentFactory.CreateCollectorProf
        CollectorBO.CollectorProfEdit(oCollector, oClassAddress, oClassPersonal)
    End Sub

    Public Sub CollectorProfAdd(ByVal oCollector As Parameter.Collector, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim CollectorBO As ICollectorProf
        CollectorBO = ComponentFactory.CreateCollectorProf
        CollectorBO.CollectorProfAdd(oCollector, oClassAddress, oClassPersonal)
    End Sub

    Public Sub CollectorProfDelete(ByVal oCollector As Parameter.Collector)
        Dim CollectorBO As ICollectorProf
        CollectorBO = ComponentFactory.CreateCollectorProf
        CollectorBO.CollectorProfDelete(oCollector)
    End Sub

    Function GetCollectorRAL(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollector
        CollectorBO = ComponentFactory.CreateCollector
        Return CollectorBO.GetCollectorRAL(oCollector)
    End Function

    Function GetCollectorProfDetail(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollectorProf
        CollectorBO = ComponentFactory.CreateCollectorProf
        Return CollectorBO.GetCollectorProfDetail(oCollector)
    End Function

    Function GetSupervisorCombo(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollectorProf
        CollectorBO = ComponentFactory.CreateCollectorProf
        Return CollectorBO.GetSupervisorCombo(oCollector)
    End Function


    'CollectorMOU

    Function GetCollectorMOUList(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollectorProf
        CollectorBO = ComponentFactory.CreateCollectorProf
        Return CollectorBO.GetCollectorMOUList(oCollector)
    End Function

    Function GetCollectorMOUByID(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollectorProf
        CollectorBO = ComponentFactory.CreateCollectorProf
        Return CollectorBO.GetCollectorMOUByID(oCollector)
    End Function

    Public Sub CollectorMOUEdit(ByVal oCollector As Parameter.Collector)
        Dim CollectorBO As ICollectorProf
        CollectorBO = ComponentFactory.CreateCollectorProf
        CollectorBO.CollectorMOUEdit(oCollector)
    End Sub

    Public Sub CollectorMOUAdd(ByVal oCollector As Parameter.Collector)
        Dim CollectorBO As ICollectorProf
        CollectorBO = ComponentFactory.CreateCollectorProf
        CollectorBO.CollectorMOUAdd(oCollector)
    End Sub

    Public Sub CollectorMOUDelete(ByVal oCollector As Parameter.Collector)
        Dim CollectorBO As ICollectorProf
        CollectorBO = ComponentFactory.CreateCollectorProf
        CollectorBO.CollectorMOUDelete(oCollector)
    End Sub

    Function GetBranchAddress(ByVal oCollector As Parameter.Collector) As String
        Dim CollectorBO As ICollectorProf
        CollectorBO = ComponentFactory.CreateCollectorProf
        Return CollectorBO.GetBranchAddress(oCollector)
    End Function

    Function GetCollectorMOUDetail(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollectorProf
        CollectorBO = ComponentFactory.CreateCollectorProf
        Return CollectorBO.GetCollectorMOUDetail(oCollector)
    End Function
    Public Sub CollectorMOUExtendUpdate(ByVal oCollector As Parameter.Collector)
        Dim CollectorBO As ICollectorProf
        CollectorBO = ComponentFactory.CreateCollectorProf
        CollectorBO.CollectorMOUExtendUpdate(oCollector)
    End Sub

    Function CollectorMOUExtendHistory(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim CollectorBO As ICollectorProf
        CollectorBO = ComponentFactory.CreateCollectorProf
        Return CollectorBO.CollectorMOUExtendHistory(oCollector)
    End Function
End Class
