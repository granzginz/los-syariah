
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class IncentiveRNController
    Public Function IncentiveRNPaging(ByVal oCustomClass As Parameter.IncentiveRN) As Parameter.IncentiveRN
        Dim oIncentiveRN As IIncentiveRN
        oIncentiveRN = ComponentFactory.CreateIncentiveRN
        Return oIncentiveRN.IncentiveRNPaging(oCustomClass)
    End Function
    Public Function IncentiveRNReports(ByVal oCustomClass As Parameter.IncentiveRN) As Parameter.IncentiveRN
        Dim oIncentiveRN As IIncentiveRN
        oIncentiveRN = ComponentFactory.CreateIncentiveRN
        Return oIncentiveRN.IncentiveRNReports(oCustomClass)
    End Function
    Public Sub IncentiveRNDelete(ByVal oCustomClass As Parameter.IncentiveRN)
        Dim oIncentiveRN As IIncentiveRN
        oIncentiveRN = ComponentFactory.CreateIncentiveRN
        oIncentiveRN.IncentiveRNDelete(oCustomClass)
    End Sub
    Public Sub IncentiveRNUpdate(ByVal oCustomClass As Parameter.IncentiveRN)
        Dim oIncentiveRN As IIncentiveRN
        oIncentiveRN = ComponentFactory.CreateIncentiveRN
        oIncentiveRN.IncentiveRNUpdate(oCustomClass)
    End Sub
    Public Sub IncentiveRNSave(ByVal oCustomClass As Parameter.IncentiveRN)
        Dim oIncentiveRN As IIncentiveRN
        oIncentiveRN = ComponentFactory.CreateIncentiveRN
        oIncentiveRN.IncentiveRNSave(oCustomClass)
    End Sub

End Class
