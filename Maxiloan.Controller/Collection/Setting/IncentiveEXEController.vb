
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class IncentiveEXEController
    Public Function IncentiveEXEPaging(ByVal oCustomClass As Parameter.IncentiveEXE) As Parameter.IncentiveEXE
        Dim oIncentiveEXE As IIncentiveEXE
        oIncentiveEXE = ComponentFactory.IncentiveEXE
        Return oIncentiveEXE.IncentiveEXEPaging(oCustomClass)
    End Function
    Public Sub IncentiveEXESave(ByVal oCustomClass As Parameter.IncentiveEXE)
        Dim oIncentiveEXE As IIncentiveEXE
        oIncentiveEXE = ComponentFactory.IncentiveEXE
        oIncentiveEXE.IncentiveEXESave(oCustomClass)
    End Sub
    Public Sub IncentiveEXEUpdate(ByVal oCustomClass As Parameter.IncentiveEXE)
        Dim oIncentiveEXE As IIncentiveEXE
        oIncentiveEXE = ComponentFactory.IncentiveEXE
        oIncentiveEXE.IncentiveEXEUpdate(oCustomClass)
    End Sub
    Public Sub IncentiveEXEDelete(ByVal oCustomClass As Parameter.IncentiveEXE)
        Dim oIncentiveEXE As IIncentiveEXE
        oIncentiveEXE = ComponentFactory.IncentiveEXE
        oIncentiveEXE.IncentiveEXEDelete(oCustomClass)
    End Sub
    Public Function IncentiveEXEReports(ByVal oCustomClass As Parameter.IncentiveEXE) As Parameter.IncentiveEXE
        Dim oIncentiveEXE As IIncentiveEXE
        oIncentiveEXE = ComponentFactory.IncentiveEXE
        Return oIncentiveEXE.IncentiveEXEReports(oCustomClass)
    End Function

End Class
