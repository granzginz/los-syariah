
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SPPrintController
    Public Function ListCGSPPrint(ByVal oCustomClass As Parameter.SPPrint) As Parameter.SPPrint
        Dim oSPPrint As ISPPrint
        oSPPrint = ComponentFactory.CreateSPPrint
        Return oSPPrint.ListCGSPPrint(oCustomClass)
    End Function

    Public Function ListSPPrinting(ByVal oCustomClass As Parameter.SPPrint) As Parameter.SPPrint
        Dim oSPPrint As ISPPrint
        oSPPrint = ComponentFactory.CreateSPPrint
        Return oSPPrint.ListSPPrinting(oCustomClass)
    End Function

    Public Function ListReportSPPrinting(ByVal oCustomclass As Parameter.SPPrint) As Parameter.SPPrint
        Dim oSPPrint As ISPPrint
        oSPPrint = ComponentFactory.CreateSPPrint
        Return oSPPrint.ListReportSPPrinting(oCustomclass)
    End Function

    Public Function SPPrintProcess(ByVal oCustomclass As Parameter.SPPrint) As Parameter.SPPrint
        Dim oSPPrint As ISPPrint
        oSPPrint = ComponentFactory.CreateSPPrint
        Return oSPPrint.SPPrintProcess(oCustomclass)
    End Function
    Public Function SSPrintProcess(ByVal oCustomclass As Parameter.SPPrint) As Parameter.SPPrint
        Dim oSPPrint As ISPPrint
        oSPPrint = ComponentFactory.CreateSPPrint
        Return oSPPrint.SSPrintProcess(oCustomclass)
    End Function
    Public Function ListSSPrinting(ByVal oCustomClass As Parameter.SPPrint) As Parameter.SPPrint
        Dim oSPPrint As ISPPrint
        oSPPrint = ComponentFactory.CreateSPPrint
        Return oSPPrint.ListSSPrinting(oCustomClass)
    End Function
    Public Function ListReportSSPrinting(ByVal oCustomclass As Parameter.SPPrint) As Parameter.SPPrint
        Dim oSPPrint As ISPPrint
        oSPPrint = ComponentFactory.CreateSPPrint
        Return oSPPrint.ListReportSSPrinting(oCustomclass)
    End Function

    Public Function ListRubrikPrint(ByVal oCustomclass As Parameter.SPPrint) As Parameter.SPPrint
        Dim oSPPrint As ISPPrint
        oSPPrint = ComponentFactory.CreateSPPrint
        Return oSPPrint.ListRubrikPrint(oCustomclass)
    End Function
End Class
