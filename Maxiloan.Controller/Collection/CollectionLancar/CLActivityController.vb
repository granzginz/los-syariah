

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class CLActivityController
    Public Function CLActivityList(ByVal oCustomClass As Parameter.CLActivity) As Parameter.CLActivity
        Dim oCLActivity As ICLActivity
        oCLActivity = ComponentFactory.CreateCLActivity
        Return oCLActivity.CLActivityList(oCustomClass)
    End Function

    Public Function CLActivityListRemidial(ByVal oCustomClass As Parameter.CLActivity) As Parameter.CLActivity
        Dim oCLActivity As ICLActivity
        oCLActivity = ComponentFactory.CreateCLActivity
        Return oCLActivity.CLActivityListRemidial(oCustomClass)
    End Function

    Public Function CLActivityListCollector(ByVal oCustomClass As Parameter.CLActivity) As Parameter.CLActivity
        Dim oCLActivity As ICLActivity
        oCLActivity = ComponentFactory.CreateCLActivity
        Return oCLActivity.CLActivityListCollector(oCustomClass)
    End Function

    Public Function CLActivityDataAgreement(ByVal oCustomClass As Parameter.CLActivity) As Parameter.CLActivity
        Dim oCLActivity As ICLActivity
        oCLActivity = ComponentFactory.CreateCLActivity
        Return oCLActivity.CLActivityDataAgreement(oCustomClass)
    End Function

    Public Function CLActivityActionResult(ByVal oCustomClass As Parameter.CLActivity) As Parameter.CLActivity
        Dim oCLActivity As ICLActivity
        oCLActivity = ComponentFactory.CreateCLActivity
        Return oCLActivity.CLActivityActionResult(oCustomClass)
    End Function
    Public Function CLActivitySave(ByVal oCustomClass As Parameter.CLActivity) As Parameter.CLActivity
        Dim oCLActivity As ICLActivity
        oCLActivity = ComponentFactory.CreateCLActivity
        Return oCLActivity.CLActivitySave(oCustomClass)
    End Function
    Public Function GetResult(ByVal oCustomClass As Parameter.CLActivity) As Parameter.CLActivity
        Dim oCLActivity As ICLActivity
        oCLActivity = ComponentFactory.CreateCLActivity
        Return oCLActivity.GetResult(oCustomClass)
    End Function

    Public Sub DCRSave(ByVal oCustomClass As Parameter.CLActivity)
        Dim DCR As ICLActivity
        DCR = ComponentFactory.CreateCLActivity
        DCR.DCRSave(oCustomClass)
    End Sub
    Public Function GetDCRList(ByVal oCustomClass As Parameter.CLActivity) As Parameter.CLActivity
        Dim oCLActivity As ICLActivity
        oCLActivity = ComponentFactory.CreateCLActivity
        Return oCLActivity.GetDCRList(oCustomClass)
    End Function

    Public Function CLActivityListMobCollector(ByVal oCustomClass As Parameter.CLActivity) As Parameter.CLActivity
        Dim oCLActivity As ICLActivity
        oCLActivity = ComponentFactory.CreateCLActivity
        Return oCLActivity.CLActivityListMobCollector(oCustomClass)
    End Function
End Class
