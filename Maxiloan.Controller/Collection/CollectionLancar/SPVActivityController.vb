


#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SPVActivityController

    Public Function SPVActivityList(ByVal SPVAct As Parameter.SPVActivity) As Parameter.SPVActivity
        Dim oSPVAct As ISPVActivity
        oSPVAct = ComponentFactory.CreateSPVActivity
        Return oSPVAct.SPVActivityListAgreement(SPVAct)
    End Function

    Public Function SPVActivityChoosenList(ByVal SPVAct As Parameter.SPVActivity) As Parameter.SPVActivity
        Dim oSPVAct As ISPVActivity
        oSPVAct = ComponentFactory.CreateSPVActivity
        Return oSPVAct.SPVActivityListAgreementChoosen(SPVAct)
    End Function

    Public Function GetCollectorCombo(ByVal SPVAct As Parameter.SPVActivity) As Parameter.SPVActivity
        Dim oSPVAct As ISPVActivity
        oSPVAct = ComponentFactory.CreateSPVActivity
        Return oSPVAct.GetComboCollector(SPVAct)
    End Function

    Public Function GetCasesCombo(ByVal SPVAct As Parameter.SPVActivity) As Parameter.SPVActivity
        Dim oSPVAct As ISPVActivity
        oSPVAct = ComponentFactory.CreateSPVActivity
        Return oSPVAct.GetComboCases(SPVAct)
    End Function

    Public Sub SPVActFixed(ByVal SPVAct As Parameter.SPVActivity)
        Dim oSPVAct As ISPVActivity
        oSPVAct = ComponentFactory.CreateSPVActivity
        oSPVAct.SPVActivityFixed(SPVAct)
    End Sub

    Public Sub SPVActRAL(ByVal SPVAct As Parameter.SPVActivity)
        Dim oSPVAct As ISPVActivity
        oSPVAct = ComponentFactory.CreateSPVActivity
        oSPVAct.SPVActivityRAL(SPVAct)
    End Sub

    Public Sub SPVActCases(ByVal SPVAct As Parameter.SPVActivity)
        Dim oSPVAct As ISPVActivity
        oSPVAct = ComponentFactory.CreateSPVActivity
        oSPVAct.SPVActivityCases(SPVAct)
    End Sub

    Public Sub SPVActRedDot(ByVal SPVAct As Parameter.SPVActivity)
        Dim oSPVAct As ISPVActivity
        oSPVAct = ComponentFactory.CreateSPVActivity
        oSPVAct.SPVActivityRedDot(SPVAct)
    End Sub

    Public Function CheckCollectorType(ByVal SPVAct As Parameter.SPVActivity) As String
        Dim oSPVAct As ISPVActivity
        oSPVAct = ComponentFactory.CreateSPVActivity
        Return oSPVAct.GetCollectorType(SPVAct)
    End Function

    Public Function SPVListResultAction(ByVal SPVAct As Parameter.SPVActivity) As dataset
        Dim oSPVAct As ISPVActivity
        oSPVAct = ComponentFactory.CreateSPVActivity
        Return oSPVAct.SPVListResultAction(SPVAct)
    End Function
    Public Function SPVActivityPlanSave(ByVal SPVAct As Parameter.SPVActivity) As Boolean
        Dim oSPVAct As ISPVActivity
        oSPVAct = ComponentFactory.CreateSPVActivity
        Return oSPVAct.SPVActivityPlanSave(SPVAct)
    End Function

End Class
