
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class ReceiptNotesOnRequestController
    Public Function ReceiptNotesOnRequestListCG(ByVal oCustomClass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
        Dim objReceiptNotesOnRequest As IReceiptNotesOnRequest
        objReceiptNotesOnRequest = ComponentFactory.CreateReceiptNotesOnRequest
        Return objReceiptNotesOnRequest.ReceiptNotesOnRequestListCG(oCustomClass)
    End Function

    Public Function ReceiptNotesOnRequestList(ByVal oCustomClass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
        Dim objReceiptNotesOnRequest As IReceiptNotesOnRequest
        objReceiptNotesOnRequest = ComponentFactory.CreateReceiptNotesOnRequest
        Return objReceiptNotesOnRequest.ReceiptNotesOnRequestList(oCustomClass)
    End Function


    Public Function ViewDataInstallment(ByVal oCustomClass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
        Dim objReceiptNotesOnRequest As IReceiptNotesOnRequest
        objReceiptNotesOnRequest = ComponentFactory.CreateReceiptNotesOnRequest
        Return objReceiptNotesOnRequest.ViewDataInstallment(oCustomClass)
    End Function

    Public Function GenerateKuitansiTagihOnRequest(ByVal oCustomClass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
        Dim objReceiptNotesOnRequest As IReceiptNotesOnRequest
        objReceiptNotesOnRequest = ComponentFactory.CreateReceiptNotesOnRequest
        Return objReceiptNotesOnRequest.GenerateKuitansiTagihOnRequest(oCustomClass)
    End Function

    Public Function RequestDataSelect(ByVal oCustomClass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
        Dim objReceiptNotesOnRequest As IReceiptNotesOnRequest
        objReceiptNotesOnRequest = ComponentFactory.CreateReceiptNotesOnRequest
        Return objReceiptNotesOnRequest.RequestDataSelect(oCustomClass)
    End Function



    'Public Function RALSaveDataPrintRAL(ByVal oCustomClass As Parameter.RALChangeExec) As Parameter.RALChangeExec
    '    Dim objRALChangeExec As IRALChangeExec
    '    objRALChangeExec = ComponentFactory.CreateRALChangeExec
    '    Return objRALChangeExec.RALSaveDataPrintRAL(oCustomClass)
    'End Function

    Public Function DataReceiptNotesOnRequest(ByVal oCustomClass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
        Dim objReceiptNotesOnRequest As IReceiptNotesOnRequest
        objReceiptNotesOnRequest = ComponentFactory.CreateReceiptNotesOnRequest
        Return objReceiptNotesOnRequest.DataReceiptNotesOnRequest(oCustomClass)
    End Function


End Class
