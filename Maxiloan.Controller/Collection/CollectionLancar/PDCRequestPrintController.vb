
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class PDCRequestPrintController

    Public Function PDCListCG(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint
        Dim oPDC As IPDCRequestPrint
        oPDC = ComponentFactory.CreatePDCRequestPrint
        Return oPDC.PDCRequestListCG(customclass)
    End Function

    Public Function PDCRequestList(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint
        Dim oPDC As IPDCRequestPrint
        oPDC = ComponentFactory.CreatePDCRequestPrint
        Return oPDC.PDCRequestList(customclass)
    End Function

    Public Function PDCRequestSave(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint
        Dim oPDC As IPDCRequestPrint
        oPDC = ComponentFactory.CreatePDCRequestPrint
        Return oPDC.PDCRequestSave(customclass)
    End Function

    Public Function PDCRequestReport(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint
        Dim oPDC As IPDCRequestPrint
        oPDC = ComponentFactory.CreatePDCRequestPrint
        Return oPDC.PDCRequestReport(customclass)
    End Function
    Public Function PDCRequestSurat(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint
        Dim oPDC As IPDCRequestPrint
        oPDC = ComponentFactory.CreatePDCRequestPrint
        Return oPDC.PDCRequestSurat(customclass)
    End Function
End Class
