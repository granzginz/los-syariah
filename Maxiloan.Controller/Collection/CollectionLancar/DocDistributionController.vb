
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class DocDistributionController
    Public Function InqDocumentDistributionPaging(ByVal oCustomClass As Parameter.DocDistribution) As Parameter.DocDistribution
        Dim objInqDocumentDistributionPaging As IDocDistribution
        objInqDocumentDistributionPaging = ComponentFactory.CreateDocumentDistribution
        Return objInqDocumentDistributionPaging.InqDocumentDistributionPaging(oCustomClass)
    End Function

    Public Function DocumentDistributionPaging(ByVal oCustomClass As Parameter.DocDistribution) As Parameter.DocDistribution
        Dim objDocumentDistributionPaging As IDocDistribution
        objDocumentDistributionPaging = ComponentFactory.CreateDocumentDistribution
        Return objDocumentDistributionPaging.DocumentDistributionPaging(oCustomClass)
    End Function


    Public Function DocumentDistributionSave(ByVal oCustomClass As Parameter.DocDistribution) As Parameter.DocDistribution
        Dim objDocumentDistributionSave As IDocDistribution
        objDocumentDistributionSave = ComponentFactory.CreateDocumentDistribution
        Return objDocumentDistributionSave.DocumentDistributionSave(oCustomClass)
    End Function

    ''Public Function GenerateKuitansiTagihOnRequest(ByVal oCustomClass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
    ''    Dim objReceiptNotesOnRequest As IReceiptNotesOnRequest
    ''    objReceiptNotesOnRequest = ComponentFactory.CreateReceiptNotesOnRequest
    ''    Return objReceiptNotesOnRequest.GenerateKuitansiTagihOnRequest(oCustomClass)
    ''End Function

    Public Function RequestDataDocDistribution(ByVal oCustomClass As Parameter.DocDistribution) As Parameter.DocDistribution
        Dim objRequestDataDocDistribution As IDocDistribution
        objRequestDataDocDistribution = ComponentFactory.CreateDocumentDistribution
        Return objRequestDataDocDistribution.RequestDataDocDistribution(oCustomClass)
    End Function



    '''Public Function RALSaveDataPrintRAL(ByVal oCustomClass As Parameter.RALChangeExec) As Parameter.RALChangeExec
    '''    Dim objRALChangeExec As IRALChangeExec
    '''    objRALChangeExec = ComponentFactory.CreateRALChangeExec
    '''    Return objRALChangeExec.RALSaveDataPrintRAL(oCustomClass)
    '''End Function

    'Public Function DataInventoryAppraisal(ByVal oCustomClass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
    '    Dim objInventoryAppraisal As IInventoryAppraisal
    '    objInventoryAppraisal = ComponentFactory.CreateInventoryAppraisal
    '    Return objInventoryAppraisal.DataInventoryAppraisal(oCustomClass)
    'End Function


    'Public Function ViewAppraisal(ByVal oCustomClass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
    '    Dim objInventoryAppraisal As IInventoryAppraisal
    '    objInventoryAppraisal = ComponentFactory.CreateInventoryAppraisal
    '    Return objInventoryAppraisal.ViewAppraisal(oCustomClass)
    'End Function
    'Public Function ViewAppraisalbidder(ByVal oCustomClass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
    '    Dim objInventoryAppraisal As IInventoryAppraisal
    '    objInventoryAppraisal = ComponentFactory.CreateInventoryAppraisal
    '    Return objInventoryAppraisal.ViewAppraisalbidder(oCustomClass)
    'End Function
    'Public Function GetAccruedInterest(ByVal oCustomClass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
    '    Dim objInventoryAppraisal As IInventoryAppraisal
    '    objInventoryAppraisal = ComponentFactory.CreateInventoryAppraisal
    '    Return objInventoryAppraisal.GetAccruedInterest(oCustomClass)
    'End Function
End Class
