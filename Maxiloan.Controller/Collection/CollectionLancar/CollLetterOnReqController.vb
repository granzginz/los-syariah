
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class CollLetterOnReqController
    Public Function CollLetterOnReqPaging(ByVal oCustomClass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq
        Dim oCollLetterOnReq As ICollLetterOnReq
        oCollLetterOnReq = ComponentFactory.CollLetterOnReq
        Return oCollLetterOnReq.CollLetterOnReqPaging(oCustomClass)

    End Function
    Public Function CollLetterOnReqView(ByVal ocustomclass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq
        Dim oCollLetterOnReq As ICollLetterOnReq
        oCollLetterOnReq = ComponentFactory.CollLetterOnReq
        Return oCollLetterOnReq.CollLetterOnReqView(ocustomclass)
    End Function
    Public Function LetterNameOnReqCombo(ByVal ocustomclass As Parameter.CollLetterOnReq) As DataTable
        Dim oCollLetterOnReq As ICollLetterOnReq
        oCollLetterOnReq = ComponentFactory.CollLetterOnReq
        Return oCollLetterOnReq.LetterNameOnReqCombo(ocustomclass)
    End Function
    Public Function LetterNameOnReqComboView(ByVal ocustomclass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq
        Dim oCollLetterOnReq As ICollLetterOnReq
        oCollLetterOnReq = ComponentFactory.CollLetterOnReq
        Return oCollLetterOnReq.LetterNameOnReqComboView(ocustomclass)
    End Function
    Public Function CollLetterTemplateField(ByVal oCustomClass As Parameter.CollLetterOnReq) As DataTable
        Dim oCollLetterOnReq As ICollLetterOnReq
        oCollLetterOnReq = ComponentFactory.CollLetterOnReq
        Return oCollLetterOnReq.CollLetterTemplateField(oCustomClass)
    End Function
    Public Function CollLetterTemplateFieldName(ByVal ocustomclass As Parameter.CollLetterOnReq) As DataTable
        Dim oCollLetterOnReq As ICollLetterOnReq
        oCollLetterOnReq = ComponentFactory.CollLetterOnReq
        Return oCollLetterOnReq.CollLetterTemplateFieldName(ocustomclass)
    End Function
    Public Sub CollLetterOnReqSaving(ByVal oCustomClass As Parameter.CollLetterOnReq)
        Dim oCollLetterOnReq As ICollLetterOnReq
        oCollLetterOnReq = ComponentFactory.CollLetterOnReq
        oCollLetterOnReq.CollLetterOnReqSaving(oCustomClass)
    End Sub
    Public Function CollLetterOnReqViewTextToPrint(ByVal ocustomclass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq
        Dim oCollLetterOnReq As ICollLetterOnReq
        oCollLetterOnReq = ComponentFactory.CollLetterOnReq
        Return oCollLetterOnReq.CollLetterOnReqViewTextToPrint(ocustomclass)
    End Function
    Public Function CollLetterReports(ByVal oCustomClass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq
        Dim oCollLetterOnReq As ICollLetterOnReq
        oCollLetterOnReq = ComponentFactory.CollLetterOnReq
        Return oCollLetterOnReq.CollLetterReports(oCustomClass)
    End Function
End Class
