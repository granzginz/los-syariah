

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InqAssetInventoryController

    Public Function AssetInventoryList(ByVal oCustomClass As Parameter.AssetInventory) As Parameter.AssetInventory
        Dim oAssetInventory As IInqAssetInventory
        oAssetInventory = ComponentFactory.CreateInqAssetInventory
        Return oAssetInventory.InqAssetInventoryList(oCustomClass)
    End Function


    Public Function AssetInventoryDetail(ByVal oCustomClass As Parameter.AssetInventory) As Parameter.AssetInventory
        Dim oAssetInventory As IInqAssetInventory
        oAssetInventory = ComponentFactory.CreateInqAssetInventory
        Return oAssetInventory.InqAssetInventoryDetail(oCustomClass)
    End Function

End Class

