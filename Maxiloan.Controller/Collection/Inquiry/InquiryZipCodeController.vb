
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InquiryZipCodeController
    Function InqZipCode(ByVal oCollZipCode As Parameter.CollZipCode) As Parameter.CollZipCode
        Dim InqZipCodeBO As IInqZipCode
        InqZipCodeBO = ComponentFactory.CreateInqZipCode
        Return InqZipCodeBO.InqZipCode(oCollZipCode)
    End Function
    Function GetCityCombo(ByVal strconn As String) As DataTable
        Dim InqZipCodeBO As IInqZipCode
        InqZipCodeBO = ComponentFactory.CreateInqZipCode
        Return InqZipCodeBO.GetCityCombo(strconn)
    End Function

End Class
