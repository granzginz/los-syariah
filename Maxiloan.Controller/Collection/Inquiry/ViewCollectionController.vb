

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class ViewCollectionController

    Public Function ViewCollection(ByVal oCustomClass As Parameter.Viewcollection) As Parameter.ViewCollection
        Dim oViewCollection As IViewCollection
        oViewCollection = ComponentFactory.CreateViewCollection
        Return oViewCollection.ViewCollection(oCustomClass)
    End Function

    Public Function ViewKewajibanNasabah(ByVal oCustomClass As Parameter.ViewCollection) As Parameter.ViewCollection
        Dim oViewKewajibanNasabah As IViewCollection
        oViewKewajibanNasabah = ComponentFactory.CreateViewCollection
        Return oViewKewajibanNasabah.ViewKewajibanNasabah(oCustomClass)
    End Function

End Class

