
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class DeskCollPerformance
    Public Function DeskCollPerformancePaging(ByVal oCustomClass As Parameter.DeskCollPerformance) As Parameter.DeskCollPerformance
        Dim oDeskCollPerformance As IDeskCollPerformance
        oDeskCollPerformance = ComponentFactory.DeskCollPerformance
        Return oDeskCollPerformance.DeskCollPerformancePaging(oCustomClass)
    End Function
    Public Function GetComboCG(ByVal strConnection As String) As DataTable
        Dim oDeskCollPerformance As IDeskCollPerformance
        oDeskCollPerformance = ComponentFactory.DeskCollPerformance
        Return oDeskCollPerformance.GetComboCG(strConnection)
    End Function
    Public Function GetPeriod(ByVal strConnection As String) As DataTable
        Dim oDeskCollPerformance As IDeskCollPerformance
        oDeskCollPerformance = ComponentFactory.DeskCollPerformance
        Return oDeskCollPerformance.GetPeriod(strConnection)
    End Function
    Public Function GetBeginStatus(ByVal strConnection As String) As DataTable
        Dim oDeskCollPerformance As IDeskCollPerformance
        oDeskCollPerformance = ComponentFactory.DeskCollPerformance
        Return oDeskCollPerformance.GetBeginStatus(strConnection)
    End Function
    Public Function GetEndStatus(ByVal strConnection As String) As DataTable
        Dim oDeskCollPerformance As IDeskCollPerformance
        oDeskCollPerformance = ComponentFactory.DeskCollPerformance
        Return oDeskCollPerformance.GetEndStatus(strConnection)
    End Function

End Class
