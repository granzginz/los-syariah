﻿
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InquiryEndPastDueDaysController

    Function InqEndPastDueDays(ByVal oEndPastDueDays As Parameter.InqEndPastDueDays) As Parameter.InqEndPastDueDays
        Dim InqEndPastDueDaysBO As IInqEndPastDueDays
        InqEndPastDueDaysBO = ComponentFactory.CreateInqEndPastDueDays
        Return InqEndPastDueDaysBO.InqEndPastDueDays(oEndPastDueDays)
    End Function

End Class
