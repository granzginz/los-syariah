

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class RALInquiryController
    Public Function RALInqListCG(ByVal oCustomClass As Parameter.RALInquiry) As Parameter.RALInquiry
        Dim oInq As IRALInquiry
        oInq = ComponentFactory.CreateRALInquiry()
        Return oInq.RALInqListCG(oCustomClass)
    End Function
    Public Function RALInqListExecutor(ByVal oCustomClass As Parameter.RALInquiry) As Parameter.RALInquiry
        Dim oInq As IRALInquiry
        oInq = ComponentFactory.CreateRALInquiry()
        Return oInq.RALInqListExecutor(oCustomClass)
    End Function
    Public Function RALInqListExecutorCombo(ByVal customclass As Parameter.RALInquiry) As Parameter.RALInquiry
        Dim oInq As IRALInquiry
        oInq = ComponentFactory.CreateRALInquiry()
        Return oInq.RALInqListExecutorCombo(customclass)
    End Function
    Public Function RALInqList(ByVal oCustomClass As Parameter.RALInquiry) As Parameter.RALInquiry
        Dim oInq As IRALInquiry
        oInq = ComponentFactory.CreateRALInquiry()
        Return oInq.RALInqList(oCustomClass)
    End Function

    Public Function RALInqListExtend(ByVal oCustomClass As Parameter.RALInquiry) As Parameter.RALInquiry
        Dim oInq As IRALInquiry
        oInq = ComponentFactory.CreateRALInquiry()
        Return oInq.RALInqListExtend(oCustomClass)
    End Function
End Class
