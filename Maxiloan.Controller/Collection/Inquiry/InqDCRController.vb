
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InqDCRController
    Public Function InqDCRList(ByVal DCR As Parameter.InqDCR) As Parameter.InqDCR
        Dim oDRC As IDCR
        oDRC = ComponentFactory.CreateInqDCR()
        Return oDRC.GetInqDCRList(DCR)
    End Function

    Public Function GetComboCG(ByVal strconn As String) As DataTable
        Dim oDRC As IDCR
        oDRC = ComponentFactory.CreateInqDCR()
        Return oDRC.GetCG(strconn)
    End Function

    Public Function GetComboTaskType(ByVal strconn As String) As DataTable
        Dim oDRC As IDCR
        oDRC = ComponentFactory.CreateInqDCR()
        Return oDRC.GetTaskType(strconn)
    End Function

    Public Function ListReport(ByVal oCustomClass As Parameter.InqDCR) As DataTable
        Dim oDRC As IDCR
        oDRC = ComponentFactory.CreateInqDCR
        Return oDRC.ListReport(oCustomClass)
    End Function
End Class
