
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class CollActivityController
    Public Function GetCollActivityList(ByVal CollAct As Parameter.CollActivity) As Parameter.CollActivity
        Dim oCollAct As ICollActivity
        oCollAct = ComponentFactory.CreateCollActivity
        Return oCollAct.GetCollActivityList(CollAct)
    End Function

    Public Function GetCollActivityResultList(ByVal CollAct As Parameter.CollActivity) As Parameter.CollActivity
        Dim oCollAct As ICollActivity
        oCollAct = ComponentFactory.CreateCollActivity
        Return oCollAct.GetCollActivityResultList(CollAct)
    End Function

    Public Function GetCollActivityDetail(ByVal CollAct As Parameter.CollActivity) As Parameter.CollActivity
        Dim oCollAct As ICollActivity
        oCollAct = ComponentFactory.CreateCollActivity
        Return oCollAct.GetCollActivityDetail(CollAct)
    End Function

    Public Function GetComboCG(ByVal strconn As String) As DataTable
        Dim oCollAct As ICollActivity
        oCollAct = ComponentFactory.CreateCollActivity
        Return oCollAct.GetCGCombo(strconn)
    End Function

    Public Function GetComboCollector(ByVal CollAct As Parameter.CollActivity) As DataTable
        Dim oCollAct As ICollActivity
        oCollAct = ComponentFactory.CreateCollActivity
        Return oCollAct.GetCollectorCombo(CollAct)
    End Function

    Public Function GetCollActivityHistory(ByVal CollAct As Parameter.CollActivity) As Parameter.CollActivity
        Dim oCollAct As ICollActivity
        oCollAct = ComponentFactory.CreateCollActivity
        Return oCollAct.GetCollActivityHistory(CollAct)
    End Function

    Public Function GetCollActivityResultListPrint(ByVal CollAct As Parameter.CollActivity) As DataTable
        Dim oCollAct As ICollActivity
        oCollAct = ComponentFactory.CreateCollActivity
        Return oCollAct.GetCollActivityResultListPrint(CollAct)
    End Function

End Class
