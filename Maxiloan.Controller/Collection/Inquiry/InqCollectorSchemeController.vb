
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InqCollectorSchemeController
    Public Function InqCollectorSchemeList(ByVal oCustomClass As Parameter.InqCollectorScheme) As Parameter.InqCollectorScheme
        Dim oCollectorScheme As ICollectorScheme
        oCollectorScheme = ComponentFactory.CreateCollectorScheme()
        Return oCollectorScheme.InqCollectorSchemeList(oCustomClass)
    End Function
End Class
