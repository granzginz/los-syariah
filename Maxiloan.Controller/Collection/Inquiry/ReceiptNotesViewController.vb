
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class ReceiptNotesViewController
    Function ReceiptNotesViewPaging(ByVal oCustomClass As Parameter.ReceiptNotesView) As Parameter.ReceiptNotesView
        Dim oReceiptNotesView As IReceiptNotesView
        oReceiptNotesView = ComponentFactory.ReceiptNotesView
        Return oReceiptNotesView.ReceiptNotesViewPaging(oCustomClass)
    End Function
End Class
