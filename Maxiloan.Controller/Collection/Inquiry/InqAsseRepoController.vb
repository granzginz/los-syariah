

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InqAssetRepoController

    Public Function AssetRepoList(ByVal oCustomClass As Parameter.AssetRepo) As Parameter.AssetRepo
        Dim oAssetRepo As IInqAssetRepo
        oAssetRepo = ComponentFactory.CreateInqAssetRepo
        Return oAssetRepo.InqAssetRepoList(oCustomClass)
    End Function


    Public Function AssetRepoDetail(ByVal oCustomClass As Parameter.AssetRepo) As Parameter.AssetRepo
        Dim oAssetRepo As IInqAssetRepo
        oAssetRepo = ComponentFactory.CreateInqAssetRepo
        Return oAssetRepo.InqAssetRepoDetail(oCustomClass)
    End Function

End Class

