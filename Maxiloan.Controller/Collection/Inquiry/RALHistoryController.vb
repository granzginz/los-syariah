#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class RALHistoryController

    Public Function RALHistoryList(ByVal oCustomClass As Parameter.RALHistory) As Parameter.RALHistory
        Dim oRALHistory As IRALHistory
        oRALHistory = ComponentFactory.CreateRALHistory
        Return oRALHistory.RALHistoryList(oCustomClass)
    End Function
End Class
