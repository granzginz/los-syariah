
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class ReminderCallActivityController
    Public Function DeskCollActivityList(ByVal oCustomClass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
        Dim oDeskCollReminder As IDeskCollReminderActivity
        oDeskCollReminder = ComponentFactory.CreateDeskCollReminder
        Return oDeskCollReminder.DeskCollReminderList(oCustomClass)
    End Function

    Public Function DeskCollActivityView(ByVal oCustomClass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
        Dim oDeskCollReminder As IDeskCollReminderActivity
        oDeskCollReminder = ComponentFactory.CreateDeskCollReminder
        Return oDeskCollReminder.DeskCollReminderView(oCustomClass)
    End Function

    Public Function ListResultAction(ByVal oCustomClass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
        Dim oDeskCollReminder As IDeskCollReminderActivity
        oDeskCollReminder = ComponentFactory.CreateDeskCollReminder
        Return oDeskCollReminder.ListResultAction(oCustomClass)
    End Function

    Public Function SaveDeskCollActivity(ByVal oCustomClass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
        Dim oDeskCollReminder As IDeskCollReminderActivity
        oDeskCollReminder = ComponentFactory.CreateDeskCollReminder
        Return oDeskCollReminder.SaveDeskCollActivity(oCustomClass)
    End Function

    Public Function DownloadSMSReminderList(ByVal oCustomClass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
        Dim oDeskCollReminder As IDeskCollReminderActivity
        oDeskCollReminder = ComponentFactory.CreateDeskCollReminder
        Return oDeskCollReminder.DownloadSMSReminderList(oCustomClass)
    End Function

    Public Function DownloadSMSReminderPaging(ByVal oCustomClass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
        Dim oDeskCollReminder As IDeskCollReminderActivity
        oDeskCollReminder = ComponentFactory.CreateDeskCollReminder
        Return oDeskCollReminder.DownloadSMSReminderPaging(oCustomClass)
    End Function



End Class
