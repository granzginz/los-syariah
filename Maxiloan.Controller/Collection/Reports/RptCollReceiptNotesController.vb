#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region
Public Class RptCollReceiptNotesController
    Function OutStandingreport(ByVal oCustomClass As Parameter.RptCollReceiptNotes) As Parameter.RptCollReceiptNotes
        Dim oRptCollReceiptNotes As IRptCollReceiptNotes
        oRptCollReceiptNotes = ComponentFactory.CreateRptCollReceiptNotes
        Return oRptCollReceiptNotes.OutStandingReport(oCustomClass)
    End Function
End Class
