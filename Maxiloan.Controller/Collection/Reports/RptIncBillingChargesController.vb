
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class RptIncBillingChargesController
    Public Function IncBillingChargesReports(ByVal oCustomClass As Parameter.RptIncBillingCharges) As Parameter.RptIncBillingCharges
        Dim oBillingCharges As IRptBllingCharges
        oBillingCharges = ComponentFactory.RptBillingCharges
        Return oBillingCharges.IncBillingChargesReports(oCustomClass)
    End Function


End Class
