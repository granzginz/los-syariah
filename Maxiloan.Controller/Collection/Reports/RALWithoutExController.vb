
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class RALWithoutExController
    Public Function ViewReportRALWithoutEx(ByVal oCustomClass As Parameter.RALWithoutEx) As Parameter.RALWithoutEx
        Dim oRALWithoutEx As IRALWithoutEx
        oRALWithoutEx = ComponentFactory.CreateRALWithoutEx
        Return oRALWithoutEx.ViewReportRALWithoutEx(oCustomClass)
    End Function
End Class
