
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class RptIncOverDueRatingController
    Public Function IncOverDueRatingReports(ByVal oCustomClass As Parameter.RptIncOverDueRating) As Parameter.RptIncOverDueRating
        Dim oOverDueRatingReports As IRptIncOverDueRating
        oOverDueRatingReports = ComponentFactory.RptIncOverDueRating
        Return oOverDueRatingReports.IncOverDueRatingReports(oCustomClass)

    End Function

End Class
