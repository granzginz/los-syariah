
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class RptCollExecutorController
    Public Function ViewDataCollExecutor(ByVal oCustomClass As Parameter.RptCollOverDue) As Parameter.RptCollOverDue
        Dim oDeskCollTask As IRptCollExecutor
        oDeskCollTask = ComponentFactory.CreateRptCollExecutor
        Return oDeskCollTask.ViewDataCollExecutor(oCustomClass)
    End Function
    Public Function ViewReportCollExecutor(ByVal oCustomClass As Parameter.RptCollOverDue) As Parameter.RptCollOverDue
        Dim oDeskCollTask As IRptCollExecutor
        oDeskCollTask = ComponentFactory.CreateRptCollExecutor
        Return oDeskCollTask.ViewReportCollExecutor(oCustomClass)
    End Function
End Class
