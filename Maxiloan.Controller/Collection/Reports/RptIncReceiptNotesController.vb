#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class RptIncReceiptNotesController
    Public Function IncReceiptNotesReports(ByVal oCustomClass As Parameter.RptIncReceiptNotes) As Parameter.RptIncReceiptNotes
        Dim oReceiptNotes As IRptReceiptNotes
        oReceiptNotes = ComponentFactory.RptReceiptNotes
        Return oReceiptNotes.IncReceiptNotesReports(oCustomClass)

    End Function
End Class
