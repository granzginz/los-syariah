
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class RptCollActController
    Public Function ViewDataCollector(ByVal oCustomClass As Parameter.RptCollAct) As Parameter.RptCollAct
        Dim oDeskCollTask As IRptCollAct
        oDeskCollTask = ComponentFactory.CreateRptCollAct
        Return oDeskCollTask.ViewDataCollector(oCustomClass)
    End Function

    Public Function ViewReportColAct(ByVal oCustomClass As Parameter.RptCollAct) As Parameter.RptCollAct
        Dim oDeskCollTask As IRptCollAct
        oDeskCollTask = ComponentFactory.CreateRptCollAct
        Return oDeskCollTask.ViewReportColAct(oCustomClass)
    End Function
    Public Function ViewSubReportCollAct(ByVal oCustomClass As Parameter.RptCollAct) As Parameter.RptCollAct
        Dim oDeskCollTask As IRptCollAct
        oDeskCollTask = ComponentFactory.CreateRptCollAct
        Return oDeskCollTask.ViewSubReportCollAct(oCustomClass)
    End Function
End Class
