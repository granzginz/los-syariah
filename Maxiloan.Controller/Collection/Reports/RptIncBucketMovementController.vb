#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class RptIncBucketMovementController
    Public Function IncBucketMovementReports(ByVal oCustomClass As Parameter.RptIncBucketMovement) As Parameter.RptIncBucketMovement
        Dim oRptBucketMovement As IRptBucketMovement
        oRptBucketMovement = ComponentFactory.RptBucketMovement
        Return oRptBucketMovement.IncBucketMovementReports(oCustomClass)
    End Function
    Public Function GetPeriod(ByVal strConnection As String) As DataTable
        Dim oRptBucketMovement As IRptBucketMovement
        oRptBucketMovement = ComponentFactory.RptBucketMovement
        Return oRptBucketMovement.GetPeriod(strConnection)
    End Function
    Public Function GetBeginStatus(ByVal strConnection As String) As DataTable
        Dim oRptBucketMovement As IRptBucketMovement
        oRptBucketMovement = ComponentFactory.RptBucketMovement
        Return oRptBucketMovement.GetBeginStatus(strConnection)
    End Function
    Public Function GetBeginHolder(ByVal oCustomClass As Parameter.RptIncBucketMovement) As DataTable
        Dim oRptBucketMovement As IRptBucketMovement
        oRptBucketMovement = ComponentFactory.RptBucketMovement
        Return oRptBucketMovement.GetBeginHolder(oCustomClass)
    End Function

End Class
