
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class RptCollOverDueController
    Public Function ViewDataCollector(ByVal oCustomClass As Parameter.RptCollOverDue) As Parameter.RptCollOverDue
        Dim oDeskCollTask As IRptCollOverDue
        oDeskCollTask = ComponentFactory.CreateRptCollOverDue
        Return oDeskCollTask.ViewDataCollector(oCustomClass)
    End Function

    Public Function ViewReportCollOverDue(ByVal oCustomClass As Parameter.RptCollOverDue) As Parameter.RptCollOverDue
        Dim oDeskCollTask As IRptCollOverDue
        oDeskCollTask = ComponentFactory.CreateRptCollOverDue
        Return oDeskCollTask.ViewReportCollOverDue(oCustomClass)
    End Function
    Public Function ViewRptCreditDeliquency(ByVal oCustomClass As Parameter.RptCollOverDue) As Parameter.RptCollOverDue
        Dim oDeskCollTask As IRptCollOverDue
        oDeskCollTask = ComponentFactory.CreateRptCollOverDue
        Return oDeskCollTask.ViewRptCreditDeliquency(oCustomClass)
    End Function
End Class
