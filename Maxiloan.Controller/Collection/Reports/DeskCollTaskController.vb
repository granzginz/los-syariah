

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class DeskCollTaskController
    Public Function ViewDataCollector(ByVal oCustomClass As Parameter.DeskCollTask) As Parameter.DeskCollTask
        Dim oDeskCollTask As IDeskCollTask
        oDeskCollTask = ComponentFactory.CreateDeskCollTask
        Return oDeskCollTask.ViewDataCollector(oCustomClass)
    End Function

    Public Function ListReport(ByVal oCustomClass As Parameter.DeskCollTask) As Parameter.DeskCollTask
        Dim oDeskCollTask As IDeskCollTask
        oDeskCollTask = ComponentFactory.CreateDeskCollTask
        Return oDeskCollTask.ListReport(oCustomClass)
    End Function
End Class
