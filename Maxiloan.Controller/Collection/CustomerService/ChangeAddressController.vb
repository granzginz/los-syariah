
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class ChangeAddressController
    Public Function AddressListCG(ByVal oCustomClass As Parameter.ChangeAddress) As Parameter.ChangeAddress
        Dim oAddress As IChangeAddress
        oAddress = ComponentFactory.CreateChangeAddress
        Return oAddress.AddressListCG(oCustomClass)
    End Function

    Public Function ChangeAddressList(ByVal oCustomClass As Parameter.ChangeAddress) As Parameter.ChangeAddress
        Dim oAddress As IChangeAddress
        oAddress = ComponentFactory.CreateChangeAddress
        Return oAddress.ChangeAddressList(oCustomClass)
    End Function

    Public Function ChangeAddressView(ByVal oCustomClass As Parameter.ChangeAddress) As Parameter.ChangeAddress
        Dim oAddress As IChangeAddress
        oAddress = ComponentFactory.CreateChangeAddress
        Return oAddress.ChangeAddressView(oCustomClass)
    End Function
    Public Function ChangeAddressSave(ByVal oCustomClass As Parameter.ChangeAddress) As Parameter.ChangeAddress
        Dim oAddress As IChangeAddress
        oAddress = ComponentFactory.CreateChangeAddress
        Return oAddress.ChangeAddressSave(oCustomClass)
    End Function

    Public Function GetAddress(ByVal oCustomClass As Parameter.ChangeAddress) As Parameter.ChangeAddress
        Dim oAddress As IChangeAddress
        oAddress = ComponentFactory.CreateChangeAddress
        Return oAddress.GetAddress(oCustomClass)
    End Function
End Class
