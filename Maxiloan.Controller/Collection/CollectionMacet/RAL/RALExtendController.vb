
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class RALExtendController
    Public Function RALListCG(ByVal oCustomClass As Parameter.RALExtend) As Parameter.RALExtend
        Dim objRALPrinting As IRALExtend
        objRALPrinting = ComponentFactory.CreateRALExtend
        Return objRALPrinting.RALListCG(oCustomClass)
    End Function

    Public Function RALExtendList(ByVal oCustomClass As Parameter.RALExtend) As Parameter.RALExtend
        Dim objRALPrinting As IRALExtend
        objRALPrinting = ComponentFactory.CreateRALExtend
        Return objRALPrinting.RALExtendList(oCustomClass)
    End Function

    Public Function RALExtendView(ByVal oCustomClass As Parameter.RALExtend) As Parameter.RALExtend
        Dim objRALPrinting As IRALExtend
        objRALPrinting = ComponentFactory.CreateRALExtend
        Return objRALPrinting.RALExtendView(oCustomClass)
    End Function

    Public Function RALExtendSave(ByVal oCustomClass As Parameter.RALExtend) As Parameter.RALExtend
        Dim objRALPrinting As IRALExtend
        objRALPrinting = ComponentFactory.CreateRALExtend
        Return objRALPrinting.RALExtendSave(oCustomClass)
    End Function

    Public Function RALDataExtend(ByVal oCustomClass As Parameter.RALExtend) As Parameter.RALExtend
        Dim objRALPrinting As IRALExtend
        objRALPrinting = ComponentFactory.CreateRALExtend
        Return objRALPrinting.RALDataExtend(oCustomClass)
    End Function

    Public Function RALSaveDataPrint(ByVal oCustomClass As Parameter.RALExtend) As Parameter.RALExtend
        Dim objRALPrinting As IRALExtend
        objRALPrinting = ComponentFactory.CreateRALExtend
        Return objRALPrinting.RALSaveDataPrint(oCustomClass)
    End Function
End Class
