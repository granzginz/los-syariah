
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class RALChangeExecController
    Public Function RALListCG(ByVal oCustomClass As Parameter.RALChangeExec) As Parameter.RALChangeExec
        Dim objRALChangeExec As IRALChangeExec
        objRALChangeExec = ComponentFactory.CreateRALChangeExec
        Return objRALChangeExec.RALListCG(oCustomClass)
    End Function

    Public Function RALChangeExecList(ByVal oCustomClass As Parameter.RALChangeExec) As Parameter.RALChangeExec
        Dim objRALChangeExec As IRALChangeExec
        objRALChangeExec = ComponentFactory.CreateRALChangeExec
        Return objRALChangeExec.RALChangeExecList(oCustomClass)
    End Function


    Public Function RALViewDataCollector(ByVal oCustomClass As Parameter.RALChangeExec) As Parameter.RALChangeExec
        Dim objRALChangeExec As IRALChangeExec
        objRALChangeExec = ComponentFactory.CreateRALChangeExec
        Return objRALChangeExec.RALViewDataCollector(oCustomClass)
    End Function

    Public Function RALSaveDataExecutor(ByVal oCustomClass As Parameter.RALChangeExec) As Parameter.RALChangeExec
        Dim objRALChangeExec As IRALChangeExec
        objRALChangeExec = ComponentFactory.CreateRALChangeExec
        Return objRALChangeExec.RALSaveDataExecutor(oCustomClass)
    End Function

    Public Function RequestDataSelect(ByVal oCustomClass As Parameter.RALChangeExec) As Parameter.RALChangeExec
        Dim objRALChangeExec As IRALChangeExec
        objRALChangeExec = ComponentFactory.CreateRALChangeExec
        Return objRALChangeExec.RequestDataSelect(oCustomClass)
    End Function



    Public Function RALSaveDataPrintRAL(ByVal oCustomClass As Parameter.RALChangeExec) As Parameter.RALChangeExec
        Dim objRALChangeExec As IRALChangeExec
        objRALChangeExec = ComponentFactory.CreateRALChangeExec
        Return objRALChangeExec.RALSaveDataPrintRAL(oCustomClass)
    End Function

    Public Function RALDataChangeExec(ByVal oCustomClass As Parameter.RALChangeExec) As Parameter.RALChangeExec
        Dim objRALChangeExec As IRALChangeExec
        objRALChangeExec = ComponentFactory.CreateRALChangeExec
        Return objRALChangeExec.RALDataChangeExec(oCustomClass)
    End Function


End Class
