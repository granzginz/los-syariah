

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class RALPrintingController
    Public Function RALListCG(ByVal oCustomClass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim objRALPrinting As IRALPrinting
        objRALPrinting = ComponentFactory.CreateRALPrinting
        Return objRALPrinting.RALListCG(oCustomClass)
    End Function

    Public Function RALPrintingList(ByVal oCustomClass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim objRALPrinting As IRALPrinting
        objRALPrinting = ComponentFactory.CreateRALPrinting
        Return objRALPrinting.RALPrintingList(oCustomClass)
    End Function

    Public Function RALViewDataSelect(ByVal oCustomClass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim objRALPrinting As IRALPrinting
        objRALPrinting = ComponentFactory.CreateRALPrinting
        Return objRALPrinting.RALViewDataSelect(oCustomClass)
    End Function

    Public Function RALViewDataCollector(ByVal oCustomClass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim objRALPrinting As IRALPrinting
        objRALPrinting = ComponentFactory.CreateRALPrinting
        Return objRALPrinting.RALViewDataCollector(oCustomClass)
    End Function

    Public Function RALSaveDataExecutor(ByVal oCustomClass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim objRALPrinting As IRALPrinting
        objRALPrinting = ComponentFactory.CreateRALPrinting
        Return objRALPrinting.RALSaveDataExecutor(oCustomClass)
    End Function

    Public Function RequestDataSelect(ByVal oCustomClass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim objRALPrinting As IRALPrinting
        objRALPrinting = ComponentFactory.CreateRALPrinting
        Return objRALPrinting.RequestDataSelect(oCustomClass)
    End Function

    Public Function RALViewHistoryExecutor(ByVal oCustomClass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim objRALPrinting As IRALPrinting
        objRALPrinting = ComponentFactory.CreateRALPrinting
        Return objRALPrinting.RALViewHistoryExecutor(oCustomClass)
    End Function

    Public Function RALSaveDataPrintRAL(ByVal oCustomClass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim objRALPrinting As IRALPrinting
        objRALPrinting = ComponentFactory.CreateRALPrinting
        Return objRALPrinting.RALSaveDataPrintRAL(oCustomClass)
    End Function

    Public Function RALListReport(ByVal oCustomClass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim objRALPrinting As IRALPrinting
        objRALPrinting = ComponentFactory.CreateRALPrinting
        Return objRALPrinting.RALListReport(oCustomClass)
    End Function

    Public Function RALListReportCheckList(ByVal oCustomClass As Parameter.RALPrinting) As Parameter.RALPrinting
        Dim objRALPrinting As IRALPrinting
        objRALPrinting = ComponentFactory.CreateRALPrinting
        Return objRALPrinting.RALListReportCheckList(oCustomClass)
    End Function
End Class
