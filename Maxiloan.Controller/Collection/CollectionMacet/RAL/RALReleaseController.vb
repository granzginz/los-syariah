

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class RALReleaseController
    Public Function RALListCG(ByVal oCustomClass As Parameter.RALRelease) As Parameter.RALRelease
        Dim objRALRelease As IRALRelease
        objRALRelease = ComponentFactory.CreateRALRelease()
        Return objRALRelease.RALListCG(oCustomClass)
    End Function
    Public Function RALReleaseList(ByVal oCustomClass As Parameter.RALRelease) As Parameter.RALRelease
        Dim objRALRelease As IRALRelease
        objRALRelease = ComponentFactory.CreateRALRelease
        Return objRALRelease.RALReleaseList(oCustomClass)
    End Function

    Public Function RALDataExtend(ByVal oCustomClass As Parameter.RALRelease) As Parameter.RALRelease
        Dim objRALRelease As IRALRelease
        objRALRelease = ComponentFactory.CreateRALRelease
        Return objRALRelease.RALDataExtend(oCustomClass)
    End Function

    Public Function RALReleaseView(ByVal oCustomClass As Parameter.RALRelease) As Parameter.RALRelease
        Dim objRALRelease As IRALRelease
        objRALRelease = ComponentFactory.CreateRALRelease
        Return objRALRelease.RALReleaseView(oCustomClass)
    End Function
    Public Function RALReleaseSave(ByVal oCustomClass As Parameter.RALRelease) As Parameter.RALRelease
        Dim objRALRelease As IRALRelease
        objRALRelease = ComponentFactory.CreateRALRelease
        Return objRALRelease.RALReleaseSave(oCustomClass)
    End Function
    Public Function CekPrepayment(ByVal oCustomClass As Parameter.RALRelease) As Boolean
        Dim objRALRelease As IRALRelease
        objRALRelease = ComponentFactory.CreateRALRelease
        Return objRALRelease.CekPrepayment(oCustomClass)
    End Function


End Class
