
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InventoryAppraisalController
    Public Function InventoryAppraisalListCG(ByVal oCustomClass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
        Dim objInventoryAppraisal As IInventoryAppraisal
        objInventoryAppraisal = ComponentFactory.CreateInventoryAppraisal
        Return objInventoryAppraisal.InventoryAppraisalListCG(oCustomClass)
    End Function

    Public Function InventoryAppraisalList(ByVal oCustomClass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
        Dim objInventoryAppraisal As IInventoryAppraisal
        objInventoryAppraisal = ComponentFactory.CreateInventoryAppraisal
        Return objInventoryAppraisal.InventoryAppraisalList(oCustomClass)
    End Function


    Public Function SaveInventoryAppraisal(ByVal oCustomClass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
        Dim objInventoryAppraisal As IInventoryAppraisal
        objInventoryAppraisal = ComponentFactory.CreateInventoryAppraisal
        Return objInventoryAppraisal.SaveInventoryAppraisal(oCustomClass)
    End Function

    'Public Function GenerateKuitansiTagihOnRequest(ByVal oCustomClass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
    '    Dim objReceiptNotesOnRequest As IReceiptNotesOnRequest
    '    objReceiptNotesOnRequest = ComponentFactory.CreateReceiptNotesOnRequest
    '    Return objReceiptNotesOnRequest.GenerateKuitansiTagihOnRequest(oCustomClass)
    'End Function

    Public Function RequestDataSelectInventory(ByVal oCustomClass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
        Dim objInventoryAppraisal As IInventoryAppraisal
        objInventoryAppraisal = ComponentFactory.CreateInventoryAppraisal
        Return objInventoryAppraisal.RequestDataSelectInventory(oCustomClass)
    End Function



    ''Public Function RALSaveDataPrintRAL(ByVal oCustomClass As Parameter.RALChangeExec) As Parameter.RALChangeExec
    ''    Dim objRALChangeExec As IRALChangeExec
    ''    objRALChangeExec = ComponentFactory.CreateRALChangeExec
    ''    Return objRALChangeExec.RALSaveDataPrintRAL(oCustomClass)
    ''End Function

    Public Function DataInventoryAppraisal(ByVal oCustomClass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
        Dim objInventoryAppraisal As IInventoryAppraisal
        objInventoryAppraisal = ComponentFactory.CreateInventoryAppraisal
        Return objInventoryAppraisal.DataInventoryAppraisal(oCustomClass)
    End Function


    Public Function ViewAppraisal(ByVal oCustomClass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
        Dim objInventoryAppraisal As IInventoryAppraisal
        objInventoryAppraisal = ComponentFactory.CreateInventoryAppraisal
        Return objInventoryAppraisal.ViewAppraisal(oCustomClass)
    End Function
    Public Function ViewAppraisalbidder(ByVal oCustomClass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
        Dim objInventoryAppraisal As IInventoryAppraisal
        objInventoryAppraisal = ComponentFactory.CreateInventoryAppraisal
        Return objInventoryAppraisal.ViewAppraisalbidder(oCustomClass)
    End Function
    Public Function GetAccruedInterest(ByVal oCustomClass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
        Dim objInventoryAppraisal As IInventoryAppraisal
        objInventoryAppraisal = ComponentFactory.CreateInventoryAppraisal
        Return objInventoryAppraisal.GetAccruedInterest(oCustomClass)
    End Function
    Public Sub GetMRP(ByRef p_Class As Parameter.InventoryAppraisal)
        Dim objInventoryAppraisal As IInventoryAppraisal
        objInventoryAppraisal = ComponentFactory.CreateInventoryAppraisal
        objInventoryAppraisal.GetMRP(p_Class)
    End Sub
    Public Sub PrintFormPersetujuanAppraisal(p_Class As Parameter.InventoryAppraisal)
        Dim objInventoryAppraisal As IInventoryAppraisal
        objInventoryAppraisal = ComponentFactory.CreateInventoryAppraisal
        objInventoryAppraisal.PrintFormPersetujuanAppraisal(p_Class)
    End Sub
    Public Sub PrintFormPersetujuanAppraisalSubBid(p_Class As Parameter.InventoryAppraisal)
        Dim objInventoryAppraisal As IInventoryAppraisal
        objInventoryAppraisal = ComponentFactory.CreateInventoryAppraisal
        objInventoryAppraisal.PrintFormPersetujuanAppraisalSubBid(p_Class)
    End Sub
    Public Sub PrintFormPersetujuanList(p_Class As Parameter.InventoryAppraisal)
        Dim objInventoryAppraisal As IInventoryAppraisal
        objInventoryAppraisal = ComponentFactory.CreateInventoryAppraisal
        objInventoryAppraisal.PrintFormPersetujuanList(p_Class)
    End Sub
End Class
