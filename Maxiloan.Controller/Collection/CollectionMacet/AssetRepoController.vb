

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AssetRepoController

    Public Function AssetRepoList(ByVal oCustomClass As Parameter.AssetRepo) As Parameter.AssetRepo
        Dim oAssetRepo As IAssetRepo
        oAssetRepo = ComponentFactory.CreateAssetRepo()
        Return oAssetRepo.AssetRepoList(oCustomClass)
    End Function
    Public Function AssetRepoApprovalList(ByVal oCustomClass As Parameter.AssetRepo) As Parameter.AssetRepo
        Dim oAssetRepo As IAssetRepo
        oAssetRepo = ComponentFactory.CreateAssetRepo()
        Return oAssetRepo.AssetRepoApprovalList(oCustomClass)
    End Function
    Public Function AssetRepoCheckList(ByVal oCustomClass As Parameter.AssetRepo) As Parameter.AssetRepo
        Dim oAssetRepo As IAssetRepo
        oAssetRepo = ComponentFactory.CreateAssetRepo()
        Return oAssetRepo.AssetRepoCheckList(oCustomClass)
    End Function

    Public Sub AssetRepoCheckSave(ByVal oCustomClass As Parameter.AssetRepo)
        Dim oAssetRepo As IAssetRepo
        oAssetRepo = ComponentFactory.CreateAssetRepo()
        oAssetRepo.AssetRepoCheckListSave(oCustomClass)
    End Sub

    Public Function AssetRepoDetail(ByVal oCustomClass As Parameter.AssetRepo) As Parameter.AssetRepo
        Dim oAssetRepo As IAssetRepo
        oAssetRepo = ComponentFactory.CreateAssetRepo
        Return oAssetRepo.AssetRepoListDetail(oCustomClass)
    End Function

    Public Sub AssetRepoSave(ByVal oCustomClass As Parameter.AssetRepo)
        Dim oAssetRepo As IAssetRepo
        oAssetRepo = ComponentFactory.CreateAssetRepo
        oAssetRepo.AssetRepoSave(oCustomClass)
    End Sub

    Public Sub AssetRepoRejectApprove(ByVal oCustomClass As Parameter.AssetRepo, status As String)
        Dim oAssetRepo As IAssetRepo
        oAssetRepo = ComponentFactory.CreateAssetRepo
        oAssetRepo.AssetRepoApproveReject(oCustomClass, status)
    End Sub
 


    Public Function AssetRepoCLPrintList(ByVal oCustomClass As Parameter.AssetRepo) As Parameter.AssetRepo
        Dim oAssetRepo As IAssetRepo
        oAssetRepo = ComponentFactory.CreateAssetRepo()
        Return oAssetRepo.AssetRepoCLPrintList(oCustomClass)
    End Function

    Public Function AssetRepoCLPrintReport(ByVal oCustomClass As Parameter.AssetRepo) As Parameter.AssetRepo
        Dim oAssetRepo As IAssetRepo
        oAssetRepo = ComponentFactory.CreateAssetRepo()
        Return oAssetRepo.AssetRepoCLPrintReport(oCustomClass)
    End Function
    Public Function AssetRepoPrepaymentLetterPrint(ByVal oCustomClass As Parameter.AssetRepo) As Parameter.AssetRepo
        Dim oAssetRepo As IAssetRepo
        oAssetRepo = ComponentFactory.CreateAssetRepo()
        Return oAssetRepo.AssetRepoPrepaymentLetterPrint(oCustomClass)
    End Function
    Public Sub GetLokasiPenyimpanan(ByRef p_Class As Parameter.AssetRepo)
        Dim oAssetRepo As IAssetRepo
        oAssetRepo = ComponentFactory.CreateAssetRepo()
        oAssetRepo.GetLokasiPenyimpanan(p_Class)
    End Sub
    Public Function BiayaPenangananSave(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo
        Dim oAssetRepo As IAssetRepo
        oAssetRepo = ComponentFactory.CreateAssetRepo()
        Return oAssetRepo.BiayaPenangananSave(customclass)
    End Function


    Public Function JenisBiayaBOP(cnn As String) As IList(Of Parameter.CommonValueText)
        Dim oAssetRepo As IAssetRepo
        oAssetRepo = ComponentFactory.CreateAssetRepo()
        Return oAssetRepo.JenisBiayaBOP(cnn)
    End Function
    Public Function AssetGrade(cnn As String) As IList(Of Parameter.CommonValueText)
        Dim oAssetRepo As IAssetRepo
        oAssetRepo = ComponentFactory.CreateAssetRepo()
        Return oAssetRepo.AssetGrade(cnn)
    End Function
    Public Function AssetGradeValue(cnn As String) As IList(Of Parameter.CommonValueText)
        Dim oAssetRepo As IAssetRepo
        oAssetRepo = ComponentFactory.CreateAssetRepo()
        Return oAssetRepo.AssetGradeValue(cnn)
    End Function
End Class
