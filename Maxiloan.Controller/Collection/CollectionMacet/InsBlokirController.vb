﻿
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class InsBlokirController
    Public Function GetInsBlokir(ByVal ocustomClass As Parameter.InsBlokir) As Parameter.InsBlokir
        Dim InsBlokir As IInsBlokir
        InsBlokir = ComponentFactory.InsBlokir()
        Return InsBlokir.GetInsBlokir(ocustomClass)
    End Function
    Public Sub ReleaseBlokirBayar(ByVal ocustomClass As Parameter.InsBlokir)
        Dim InsBlokir As IInsBlokir
        InsBlokir = ComponentFactory.InsBlokir()
        InsBlokir.ReleaseBlokirBayar(ocustomClass)
    End Sub
End Class
