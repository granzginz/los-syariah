

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class CCRequestController

    Public Function CCRequestList(ByVal oCustomClass As Parameter.CCRequest) As Parameter.CCRequest
        Dim oCCRequest As ICCRequest
        oCCRequest = ComponentFactory.CreateCCRequest()
        Return oCCRequest.CCRequestList(oCustomClass)
    End Function

    Public Function CCRequestView(ByVal oCustomClass As Parameter.CCRequest) As Parameter.CCRequest
        Dim oCCRequest As ICCRequest
        oCCRequest = ComponentFactory.CreateCCRequest()
        Return oCCRequest.CCRequestView(oCustomClass)
    End Function


    Public Function CCRequestDetail(ByVal oCustomClass As Parameter.CCRequest) As Parameter.CCRequest
        Dim oCCRequest As ICCRequest
        oCCRequest = ComponentFactory.CreateCCRequest
        Return oCCRequest.CCRequestDetail(oCustomClass)
    End Function

    Public Sub CCRequestSave(ByVal oCustomClass As Parameter.CCRequest)
        Dim oCCRequest As ICCRequest
        oCCRequest = ComponentFactory.CreateCCRequest
        oCCRequest.CCRequestSave(oCustomClass)
    End Sub

    Public Function GetComboReason(ByVal oCustomClass As Parameter.CCRequest) As DataTable
        Dim oCCRequest As ICCRequest
        oCCRequest = ComponentFactory.CreateCCRequest
        Return oCCRequest.GetComboReason(oCustomClass)
    End Function
    Public Function ContinueCreditReqReport(ByVal oCustomClass As Parameter.CCRequest) As Parameter.CCRequest
        Dim oCCRequest As ICCRequest
        oCCRequest = ComponentFactory.CreateCCRequest
        Return oCCRequest.ContinueCreditReqReport(oCustomClass)
    End Function

    Public Function CCRequestListInq(ByVal oCustomClass As Parameter.CCRequest) As Parameter.CCRequest
        Dim oCCRequest As ICCRequest
        oCCRequest = ComponentFactory.CreateCCRequest()
        Return oCCRequest.CCRequestListInq(oCustomClass)
    End Function
End Class
