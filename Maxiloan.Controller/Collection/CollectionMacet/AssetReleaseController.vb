

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AssetReleaseController

    Public Function AssetReleaseList(ByVal oCustomClass As Parameter.AssetRelease) As Parameter.AssetRelease
        Dim oAssetRelease As IAssetRelease
        oAssetRelease = ComponentFactory.CreateAssetRelease()
        Return oAssetRelease.AssetReleaseList(oCustomClass)
    End Function

    Public Function AssetReleaseCheckList(ByVal oCustomClass As Parameter.AssetRelease) As Parameter.AssetRelease
        Dim oAssetRelease As IAssetRelease
        oAssetRelease = ComponentFactory.CreateAssetRelease()
        Return oAssetRelease.AssetReleaseCheckList(oCustomClass)
    End Function

    Public Function AssetReleaseCheckSave(ByVal oCustomClass As Parameter.AssetRelease) As Boolean
        Dim oAssetRelease As IAssetRelease
        oAssetRelease = ComponentFactory.CreateAssetRelease()
        Return oAssetRelease.AssetReleaseCheckListSave(oCustomClass)
    End Function

    Public Function AssetReleaseDetail(ByVal oCustomClass As Parameter.AssetRelease) As Parameter.AssetRelease
        Dim oAssetRelease As IAssetRelease
        oAssetRelease = ComponentFactory.CreateAssetRelease
        Return oAssetRelease.AssetReleaseListDetail(oCustomClass)
    End Function



End Class
