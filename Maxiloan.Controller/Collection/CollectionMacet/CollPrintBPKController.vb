#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class CollPrintBPKController
    Public Function PrintBPKList(ByVal oCustomClass As Parameter.CollPrintBPK) As Parameter.CollPrintBPK
        Dim oCollPrintBPK As ICollPrintBPK
        oCollPrintBPK = ComponentFactory.CreateCollPrintBPK()
        Return oCollPrintBPK.PrintBPKList(oCustomClass)
    End Function
    Public Function PrintBPKReport(ByVal oCustomClass As Parameter.CollPrintBPK) As Parameter.CollPrintBPK
        Dim oCollPrintBPK As ICollPrintBPK
        oCollPrintBPK = ComponentFactory.CreateCollPrintBPK()
        Return oCollPrintBPK.PrintBPKReport(oCustomClass)
    End Function
End Class
