

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class ChgInvExpDateController

    Public Function InvChgExpDateList(ByVal oCustomClass As Parameter.InvChgExpDate) As Parameter.InvChgExpDate
        Dim oInvChgExpDate As IInvChgExpDate
        oInvChgExpDate = ComponentFactory.CreateInvChgExpDate()
        Return oInvChgExpDate.InvChgExpDateList(oCustomClass)
    End Function

    Public Function InvChgExpDateView(ByVal oCustomClass As Parameter.InvChgExpDate) As Parameter.InvChgExpDate
        Dim oInvChgExpDate As IInvChgExpDate
        oInvChgExpDate = ComponentFactory.CreateInvChgExpDate()
        Return oInvChgExpDate.InvChgExpDateView(oCustomClass)
    End Function


    Public Function InvChgExpDateDetail(ByVal oCustomClass As Parameter.InvChgExpDate) As Parameter.InvChgExpDate
        Dim oInvChgExpDate As IInvChgExpDate
        oInvChgExpDate = ComponentFactory.CreateInvChgExpDate
        Return oInvChgExpDate.InvChgExpDateDetail(oCustomClass)
    End Function

    Public Function InvChgExpDateSave(ByVal oCustomClass As Parameter.InvChgExpDate) As Parameter.InvChgExpDate
        Dim oInvChgExpDate As IInvChgExpDate
        oInvChgExpDate = ComponentFactory.CreateInvChgExpDate
        Return oInvChgExpDate.InvChgExpDateSave(oCustomClass)
    End Function

    Public Function GetComboReason(ByVal oCustomClass As Parameter.InvChgExpDate) As DataTable
        Dim oInvChgExpDate As IInvChgExpDate
        oInvChgExpDate = ComponentFactory.CreateInvChgExpDate
        Return oInvChgExpDate.GetComboReason(oCustomClass)
    End Function
End Class
