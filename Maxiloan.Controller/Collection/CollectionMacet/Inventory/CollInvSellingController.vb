#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class CollInvSellingController
    Public Function InvSellingList(ByVal oCustomClass As Parameter.CollInvSelling) As Parameter.CollInvSelling
        Dim oCollInvSelling As ICollInvSelling
        oCollInvSelling = ComponentFactory.CreateCollInvSelling
        Return oCollInvSelling.InvSellingList(oCustomClass)
    End Function
    Public Function InvSellingDetail(ByVal oCustomClass As Parameter.CollInvSelling) As Parameter.CollInvSelling
        Dim oCollInvSelling As ICollInvSelling
        oCollInvSelling = ComponentFactory.CreateCollInvSelling
        Return oCollInvSelling.InvSellingDetail(oCustomClass)
    End Function
    Public Function InvSellingSave(ByVal oCustomClass As Parameter.CollInvSelling) As Boolean
        Dim oCollInvSelling As ICollInvSelling
        oCollInvSelling = ComponentFactory.CreateCollInvSelling
        Return oCollInvSelling.InvSellingSave(oCustomClass)
    End Function
    Public Function ViewAppraisalbidder(ByVal oCustomClass As Parameter.CollInvSelling) As Parameter.CollInvSelling
        Dim objCollInvSelling As ICollInvSelling
        objCollInvSelling = ComponentFactory.CreateCollInvSelling
        Return objCollInvSelling.ViewAppraisalbidder(oCustomClass)
    End Function

End Class
