

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class CollInvOnRequestController
    Public Function CollInvOnRequest(ByVal customclass As Parameter.CollInvSelling) As Parameter.CollInvSelling
        Dim oCollInvOnRequest As ICollInvOnRequest
        oCollInvOnRequest = ComponentFactory.CreateCollInvOnRequest
        Return oCollInvOnRequest.CollInvOnRequest(customclass)
    End Function
    Public Sub InvOnRequestProses(ByVal customclass As Parameter.CollInvSelling)
        Dim oCollInvOnRequest As ICollInvOnRequest
        oCollInvOnRequest = ComponentFactory.CreateCollInvOnRequest
        oCollInvOnRequest.InvOnRequestProses(customclass)
    End Sub
End Class
