#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class CollInvReturnController
    Public Function InvReturnList(ByVal oCustomClass As Parameter.CollInvSelling) As Parameter.CollInvSelling
        Dim oCollInvReturn As ICollInvReturn
        oCollInvReturn = ComponentFactory.CreateCollInvReturn
        Return oCollInvReturn.InvReturnList(oCustomClass)
    End Function
    Public Function InvReturnDetail(ByVal oCustomClass As Parameter.CollInvSelling) As Parameter.CollInvSelling
        Dim oCollInvReturn As ICollInvReturn
        oCollInvReturn = ComponentFactory.CreateCollInvReturn
        Return oCollInvReturn.InvReturnDetail(oCustomClass)
    End Function
    Public Function InvReturnSave(ByVal oCustomClass As Parameter.CollInvSelling) As Boolean
        Dim oCollInvReturn As ICollInvReturn
        oCollInvReturn = ComponentFactory.CreateCollInvReturn
        Return oCollInvReturn.InvReturnSave(oCustomClass)
    End Function

End Class
