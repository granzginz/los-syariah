
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class CollExpenseController
    Public Function CollExpenseList(ByVal oCustomClass As CollExpense) As CollExpense
        Dim oCollExpense As ICollExpense
        oCollExpense = ComponentFactory.CreateCollExpense
        Return oCollExpense.CollExpenseList(oCustomClass)
    End Function
    Public Function CollExpenseReqList(ByVal oCustomClass As CollExpense) As CollExpense
        Dim oCollExpense As ICollExpense
        oCollExpense = ComponentFactory.CreateCollExpense
        Return oCollExpense.CollExpenseReqList(oCustomClass)
    End Function
    Public Sub CollExpenseSave(ByVal oCustomClass As CollExpense)
        Dim oCollExpense As ICollExpense
        oCollExpense = ComponentFactory.CreateCollExpense
        oCollExpense.CollExpenseSave(oCustomClass)
    End Sub
End Class
