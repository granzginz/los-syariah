﻿#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AuthorizationController
    Public Function CheckForm(ByVal oCustomClass As Parameter.Common) As Boolean
        Dim oCekForm As IAuthorize
        oCekForm = ComponentFactory.CreateAuthorize()
        Return oCekForm.CheckForm(oCustomClass)
    End Function

    Public Function CheckFeature(ByVal oCustomClass As Parameter.Common) As Boolean
        Dim oCekFeature As IAuthorize
        oCekFeature = ComponentFactory.CreateAuthorize()
        Return oCekFeature.CheckFeature(oCustomClass)
    End Function
End Class
