
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class GeneralPagingController
    Public Function GetReportWithBranchAndTwoPeriod(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.GetReportWithBranchAndTwoPeriod(customClass)
    End Function
    Public Function GetReportWithParameterWhereAndSort(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.GetReportWithParameterWhereAndSort(customClass)
    End Function
    Public Function GetReportWithTwoParameter(ByVal oCustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.GetReportWithTwoParameter(oCustomClass)
    End Function

    Public Function GetDailyTransReport(ByVal oCustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.GetDailyTransReport(oCustomClass)
    End Function


    Public Function GetReportWithParameterWhereCond(ByVal oCustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.GetReportWithParameterWhereCond(oCustomClass)
    End Function

    Public Function DeleteGeneral(ByVal customClass As Parameter.GeneralPaging) As Boolean
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.DeleteGeneral(customClass)
    End Function

    Public Function GetARMutationReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
        Dim oARReport As IGeneralPaging
        oARReport = ComponentFactory.CreateGeneralPaging
        Return oARReport.ARMutationReport(customclass)
    End Function
    Public Function GetPrepaidReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
        Dim oARReport As IGeneralPaging
        oARReport = ComponentFactory.CreateGeneralPaging
        Return oARReport.PrepaidReport(customclass)
    End Function
    Public Function GetAPMutationReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
        Dim oARReport As IGeneralPaging
        oARReport = ComponentFactory.CreateGeneralPaging
        Return oARReport.APMutationReport(customclass)
    End Function
    Public Function GetAPToInsReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
        Dim oARReport As IGeneralPaging
        oARReport = ComponentFactory.CreateGeneralPaging
        Return oARReport.APToInsReport(customclass)
    End Function
    Public Function GetSuspendReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
        Dim oARReport As IGeneralPaging
        oARReport = ComponentFactory.CreateGeneralPaging
        Return oARReport.SuspendReport(customclass)
    End Function
    Public Function GetBIQualityReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
        Dim oARReport As IGeneralPaging
        oARReport = ComponentFactory.CreateGeneralPaging
        Return oARReport.BIQualityReport(customclass)
    End Function

    Public Function GetGeneralPaging(ByVal oCustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.GetGeneralPaging(oCustomClass)
    End Function
    Public Function GetRecordWithoutParameter(ByVal strConnection As String) As DataTable
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.GetRecordWithoutParameter(strConnection)
    End Function

    Public Function GetBranchCombo(ByVal oClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.GetBranchCombo(oClass)
    End Function


    Public Function GetInsuranceBranchCombo(ByVal oClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.GetInsuranceBranchCombo(oClass)
    End Function

    Public Function GetGeneralSP(ByVal oCustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.GetGeneralSP(oCustomClass)
    End Function
    Public Function GetReportWithThreeParameterWhereCond(ByVal oCustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim RptWithThreeCondition As IGeneralPaging
        RptWithThreeCondition = ComponentFactory.CreateGeneralPaging
        Return RptWithThreeCondition.GetReportWithThreeParameterWhereCond(oCustomClass)
    End Function
    Public Function GetReportWithParamApplicationID(ByVal oCustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.GetReportWithParamApplicationID(oCustomClass)
    End Function

    Public Function PrepaymentTerminationReport(ByVal oCustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.PrepaymentTerminationReport(oCustomClass)
    End Function
    Public Function AssetRepossessionOrderTerminationReport(ByVal oCustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.AssetRepossessionOrderTerminationReport(oCustomClass)
    End Function
    Public Function GetReportWithBranchAndstrDate(ByVal customclass As Parameter.GeneralPaging) As DataSet
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.GetReportWithBranchAndstrDate(customclass)
    End Function
    Public Function GetGeneralPagingWithTwoWhereCondition(ByVal oCustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.GetGeneralPagingWithTwoWhereCondition(oCustomClass)
    End Function
    Public Function GetReportWithBranch_strDate_Where(ByVal oCustomClass As Parameter.GeneralPaging) As DataSet
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.GetReportWithBranch_strDate_Where(oCustomClass)
    End Function
    Public Function GetReportWithFiveParameterWhereCond(ByVal ocustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim RptWithFiveCondition As IGeneralPaging
        RptWithFiveCondition = ComponentFactory.CreateGeneralPaging
        Return RptWithFiveCondition.GetReportWithFiveParameterWhereCond(ocustomClass)
    End Function
    Function GetRecordWithoutParameterAll(ByVal oCustom As Parameter.GeneralPaging) As DataTable
        Dim BO As IGeneralPaging
        BO = ComponentFactory.CreateGeneralPaging
        Return BO.GetRecordWithoutParameterAll(oCustom)
    End Function
End Class
