﻿
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class LoginController
    Function ListPasswordHistory(ByVal oCustomClass As Parameter.Login) As Parameter.Login
        Dim GetLogin As ILogin
        GetLogin = ComponentFactory.CreateLogin()
        Return GetLogin.ListPasswordHistory(oCustomClass)
    End Function
    Function IsPasswordExpired(ByVal oCustomClass As Parameter.Login) As Boolean
        Dim GetLogin As ILogin
        GetLogin = ComponentFactory.CreateLogin()
        Return GetLogin.IsPasswordExpired(oCustomClass)
    End Function

    Function PasswordSetting() As Parameter.Login
        Dim GetLogin As ILogin
        GetLogin = ComponentFactory.CreateLogin()
        Return GetLogin.PasswordSetting()
    End Function
    Public Function CekBranch(ByVal oCustomClass As Parameter.Login) As Parameter.Login
        Dim GetCheckBrancClosed As ILogin
        GetCheckBrancClosed = ComponentFactory.CreateLogin()
        Return GetCheckBrancClosed.CheckBranch(oCustomClass)
    End Function

    Public Function CheckMultiBranch(ByVal ocustomclass As Parameter.Login) As Parameter.Login
        Dim LoginApplication As ILogin
        LoginApplication = ComponentFactory.CreateLogin()
        Return LoginApplication.CheckMultiBranch(ocustomclass)
    End Function

    Public Function GetLoginData() As Parameter.Login
        Dim LoginApplication As ILogin
        LoginApplication = ComponentFactory.CreateLogin()
        Return LoginApplication.GetLoginData()
    End Function

    Public Function GetBusinessDate(ByVal strConnection As String) As Date
        Dim GetBDate As ILogin
        GetBDate = ComponentFactory.CreateLogin()
        Return GetBDate.GetBusinessDate(strConnection)
    End Function

    Public Function GetUser(ByVal oCustomClass As Parameter.Login) As Parameter.Login
        Dim oGetUser As ILogin
        oGetUser = ComponentFactory.CreateLogin()
        Return oGetUser.GetUser(oCustomClass)
    End Function

    Public Function GetEmployee(ByVal oCustomClass As Parameter.Login) As String
        Dim oGetUser As ILogin
        oGetUser = ComponentFactory.CreateLogin()
        Return oGetUser.GetEmployee(oCustomClass)
    End Function

    Public Function SetSession(ByVal oCustomClass As Parameter.Login) As Parameter.Login
        Dim oSetSession As ILogin
        oSetSession = ComponentFactory.CreateLogin()
        Return oSetSession.SetSession(oCustomClass)
    End Function

    Public Function ListTreeMenu(ByVal oCustomClass As Parameter.Login) As Boolean
        Dim oListTreeMenu As ILogin
        oListTreeMenu = ComponentFactory.CreateLogin()
        Return oListTreeMenu.ListTreeMenu(oCustomClass)
    End Function

    Public Function UpdateTreeMenu(ByVal oCustomClass As Parameter.Login) As String
        Dim oUpdateTreeMenu As ILogin
        oUpdateTreeMenu = ComponentFactory.CreateLogin()
        Return oUpdateTreeMenu.UpdateTreeMenu(oCustomClass)
    End Function

    Public Function WriteFileXML(ByVal oCustomClass As Parameter.Login) As DataTable
        Dim oWriteFileXML As ILogin
        oWriteFileXML = ComponentFactory.CreateLogin()
        Return oWriteFileXML.WriteFileXML(oCustomClass)
    End Function

    Public Function WriteXMLForUser(ByVal ocustomclass As Parameter.Login) As DataTable
        Dim oWriteXMLForUser As ILogin
        oWriteXMLForUser = ComponentFactory.CreateLogin()
        Return oWriteXMLForUser.WriteXMLForUser(ocustomclass)
    End Function

    Public Function GetCompanyAddress(ByVal strConnection As String, ByVal StrCompanyID As String) As String
        Dim oGetCompanyAddress As ILogin
        oGetCompanyAddress = ComponentFactory.CreateLogin()
        Return oGetCompanyAddress.GetCompanyAddress(strConnection, StrCompanyID)
    End Function
    Public Function CountWrongPwd(ByVal oCustomClass As Parameter.Login) As Int16
        Dim oGetCompanyAddress As ILogin
        oGetCompanyAddress = ComponentFactory.CreateLogin()
        Return oGetCompanyAddress.CountWrongPwd(oCustomClass)
    End Function
    Public Sub setloghistory(ByVal oCustomClass As Parameter.Login)
        Dim oGetCompanyAddress As ILogin
        oGetCompanyAddress = ComponentFactory.CreateLogin()
        oGetCompanyAddress.setloghistory(oCustomClass)
    End Sub
End Class
