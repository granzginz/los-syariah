
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class MailTransactionController

    Public Function PrintMailTransaction(ByVal customClass As Parameter.MailTransaction) As Parameter.MailTransaction
        Dim BO As IMailTransaction
        BO = ComponentFactory.CreateMailTransaction
        Return BO.PrintMailTransaction(customClass)
    End Function


End Class
