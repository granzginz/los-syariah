

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class ExampleNameController
    Function ExampleNameList(ByVal oCustomClass As Parameter.ExampleName) As Parameter.ExampleName
        Dim oExampleNameList As IExampleName
        oExampleNameList = ComponentFactory.ExampleName()
        Return oExampleNameList.ListExampleName(oCustomClass)
    End Function
    Function ExampleNameInfo(ByVal oCustomClass As Parameter.ExampleName) As Parameter.ExampleName
        Dim oExampleNameInfo As IExampleName
        oExampleNameInfo = ComponentFactory.ExampleName()
        Return oExampleNameInfo.ExampleNameInfo(oCustomClass)
    End Function
    Function DeleteExampleName(ByVal oCustomClass As Parameter.ExampleName) As String
        Dim oDeleteExampleName As IExampleName
        oDeleteExampleName = ComponentFactory.ExampleName()
        Return oDeleteExampleName.DeleteExampleName(oCustomClass)
    End Function
End Class
