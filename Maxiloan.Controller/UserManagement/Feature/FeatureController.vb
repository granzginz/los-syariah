

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region

Public Class FeatureController
    Public Function ListMasterForm(ByVal customclass As Parameter.Feature) As Parameter.Feature
        Dim oListMasterForm As IFeature
        oListMasterForm = ComponentFactory.CreateFeature()
        Return oListMasterForm.ListMasterForm(customclass)
    End Function

    Public Function ListFeatureUser(ByVal customclass As Parameter.Feature) As Parameter.Feature
        Dim oListMasterForm As IFeature
        oListMasterForm = ComponentFactory.CreateFeature()
        Return oListMasterForm.ListFeatureUser(customclass)
    End Function

    Public Function UpdateFeatureUser(ByVal customclass As Parameter.Feature) As String
        Dim oListMasterForm As IFeature
        oListMasterForm = ComponentFactory.CreateFeature()
        Return oListMasterForm.UpdateFeatureUser(customclass)
    End Function
End Class
