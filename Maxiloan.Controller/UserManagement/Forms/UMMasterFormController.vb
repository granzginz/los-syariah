
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class UMMasterFormController
    Public Function ListMasterForm(ByVal customclass As Parameter.MasterForm) As Parameter.MasterForm
        Dim oListMasterForm As IUserManagementMasterForm
        oListMasterForm = ComponentFactory.CreateUserManagementMasterForm()
        Return oListMasterForm.ListMasterForm(customclass)
    End Function
    Public Function AddMasterForm(ByVal customclass As Parameter.MasterForm) As String
        Dim oAddMasterForm As IUserManagementMasterForm
        oAddMasterForm = ComponentFactory.CreateUserManagementMasterForm()
        Return oAddMasterForm.AddMasterForm(customclass)
    End Function
    Public Function UpdateMasterForm(ByVal customclass As Parameter.MasterForm) As String
        Dim oUpdateMasterForm As IUserManagementMasterForm
        oUpdateMasterForm = ComponentFactory.CreateUserManagementMasterForm()
        Return oUpdateMasterForm.UpdateMasterForm(customclass)
    End Function
    Public Function DeleteMasterForm(ByVal customclass As Parameter.MasterForm) As String
        Dim oDeleteMasterForm As IUserManagementMasterForm
        oDeleteMasterForm = ComponentFactory.CreateUserManagementMasterForm()
        Return oDeleteMasterForm.DeleteMasterForm(customclass)
    End Function
    Public Function ShowMasterForm(ByVal customclass As Parameter.MasterForm) As Parameter.MasterForm
        Dim oShowMasterForm As IUserManagementMasterForm
        oShowMasterForm = ComponentFactory.CreateUserManagementMasterForm()
        Return oShowMasterForm.ShowMasterForm(customclass)
    End Function
End Class
