
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class UMGroupUserController
    Public Function ListGroupUser(ByVal customclass As Parameter.GroupUser) As Parameter.GroupUser
        Dim oListGroupUser As IUserManagementGroupUser
        oListGroupUser = ComponentFactory.CreateUserManagementGroupUser()
        Return oListGroupUser.ListGroupUser(customclass)
    End Function

    Public Function DeleteGroupUser(ByVal customclass As Parameter.GroupUser) As String
        Dim oDeleteGroupUser As IUserManagementGroupUser
        oDeleteGroupUser = ComponentFactory.CreateUserManagementGroupUser()
        Return oDeleteGroupUser.DeleteGroupUser(customclass)
    End Function

    Public Function UpdateGroupUser(ByVal customclass As Parameter.GroupUser) As String
        Dim oUpdateGroupUser As IUserManagementGroupUser
        oUpdateGroupUser = ComponentFactory.CreateUserManagementGroupUser()
        Return oUpdateGroupUser.UpdateGroupUser(customclass)
    End Function
    Public Function AddGroupUser(ByVal customclass As Parameter.GroupUser) As String
        Dim oAddGroupUser As IUserManagementGroupUser
        oAddGroupUser = ComponentFactory.CreateUserManagementGroupUser()
        Return oAddGroupUser.AddGroupUser(customclass)
    End Function
End Class
