
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class UMUserGroupMenuController
    Public Function ListUserGroupMenu(ByVal customclass As Parameter.UserGroupMenu) As Parameter.UserGroupMenu
        Dim GetListUserGroupMenu As IUserManagementUserGroupMenu
        GetListUserGroupMenu = ComponentFactory.CreateUserManagementUserGroupMenu()
        Return GetListUserGroupMenu.ListUserGroupMenu(customclass)
    End Function

    Public Function UpdateUserGroupMenu(ByVal customclass As Parameter.UserGroupMenu) As String
        Dim oUpdateUserGroupMenu As IUserManagementUserGroupMenu
        oUpdateUserGroupMenu = ComponentFactory.CreateUserManagementUserGroupMenu()
        Return oUpdateUserGroupMenu.UpdateUserGroupMenu(customclass)
    End Function
End Class
