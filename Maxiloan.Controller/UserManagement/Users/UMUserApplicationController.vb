
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region

Public Class UMUserApplicationController
    Public Function ListUserApplication(ByVal customclass As Parameter.UserApplication) As Parameter.UserApplication
        Dim GetListUserApplication As IUserManagementUserApplication
        GetListUserApplication = ComponentFactory.CreateUserManagementUserApplication()
        Return GetListUserApplication.ListUserApplication(customclass)
    End Function

    Public Function DeleteUserApplication(ByVal customclass As Parameter.UserApplication) As String
        Dim oDeleteUserApplication As IUserManagementUserApplication
        oDeleteUserApplication = ComponentFactory.CreateUserManagementUserApplication()
        Return oDeleteUserApplication.DeleteUserApplication(customclass)
    End Function

    Public Function UpdateUserApplication(ByVal customclass As Parameter.UserApplication) As String
        Dim oUpdateUserApplication As IUserManagementUserApplication
        oUpdateUserApplication = ComponentFactory.CreateUserManagementUserApplication()
        Return oUpdateUserApplication.UpdateUserApplication(customclass)
    End Function
    Public Function AddUserApplication(ByVal customclass As Parameter.UserApplication) As String
        Dim oAddUserApplication As IUserManagementUserApplication
        oAddUserApplication = ComponentFactory.CreateUserManagementUserApplication()
        Return oAddUserApplication.AddUserApplication(customclass)
    End Function
End Class
