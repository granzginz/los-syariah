﻿Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class umUserController
    Public Function ListUserList(ByVal customclass As Parameter.am_user001) As Parameter.am_user001
        Dim GetListUserList As IUserManagementUserList
        GetListUserList = ComponentFactory.CreateUserManagementUserList()
        Return GetListUserList.ListUserList(customclass)
    End Function

    Public Function AddUserList(ByVal customclass As Parameter.am_user001) As String
        Dim oAddUserList As IUserManagementUserList
        oAddUserList = ComponentFactory.CreateUserManagementUserList()
        Return oAddUserList.AddUserList(customclass)
    End Function

    'Public Function deleteuserlist(ByVal customclass As Parameter.am_user001) As String
    '    Dim odeleteuserlist As IUserManagementUserList
    '    odeleteuserlist = ComponentFactory.CreateUserManagementUserList()
    '    Return odeleteuserlist.DeleteUserList(customclass)
    'End Function

    Public Function DeleteUserList(ByVal customclass As Parameter.am_user001) As Parameter.am_user001
        Dim oDeleteUserList As IUserManagementUserList
        oDeleteUserList = ComponentFactory.CreateUserManagementUserList()
        Return oDeleteUserList.DeleteUserList(customclass)
    End Function

    Public Function UpdateUserList(ByVal customclass As Parameter.am_user001) As String
        Dim oUpdateUserList As IUserManagementUserList
        oUpdateUserList = ComponentFactory.CreateUserManagementUserList()
        Return oUpdateUserList.UpdateUserList(customclass)
    End Function

    Public Function ListEmployee(ByVal customclass As Parameter.am_user001) As Parameter.am_user001
        Dim GetListEmployee As IUserManagementUserList
        GetListEmployee = ComponentFactory.CreateUserManagementUserList()
        Return GetListEmployee.ListEmployee(customclass)
    End Function
End Class
