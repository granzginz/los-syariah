
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class UMUserGroupDBController
    Public Function ListUserGroupDB(ByVal customclass As Parameter.UserGroupDB) As Parameter.UserGroupDB
        Dim GetListUserGroupDB As IUserManagementUserGroupDB
        GetListUserGroupDB = ComponentFactory.CreateUserManagementUserGroupDB()
        Return GetListUserGroupDB.ListUserGroupDb(customclass)
    End Function

    Public Function UpdateUserGroupDb(ByVal customclass As Parameter.UserGroupDB) As String
        Dim oUpdateUserGroupDb As IUserManagementUserGroupDB
        oUpdateUserGroupDb = ComponentFactory.CreateUserManagementUserGroupDB()
        Return oUpdateUserGroupDb.UpdateUserGroupDb(customclass)
    End Function
End Class
