

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InfoBoxController
    Public Sub MasterInfoBoxAdd(ByVal customclass As Parameter.InfoBox)
        Dim oMasterInfoBoxAdd As IInfoBox
        oMasterInfoBoxAdd = ComponentFactory.CreateInfoBox()
        oMasterInfoBoxAdd.MasterInfoBoxAdd(customclass)
    End Sub

    Public Sub MasterInfoBoxEdit(ByVal customclass As Parameter.InfoBox)
        Dim oMasterInfoBoxEdit As IInfoBox
        oMasterInfoBoxEdit = ComponentFactory.CreateInfoBox()
        oMasterInfoBoxEdit.MasterInfoBoxEdit(customclass)
    End Sub

    Public Sub MasterInfoBoxDelete(ByVal customclass As Parameter.InfoBox)
        Dim oMasterInfoBoxDelete As IInfoBox
        oMasterInfoBoxDelete = ComponentFactory.CreateInfoBox()
        oMasterInfoBoxDelete.MasterInfoBoxDelete(customclass)
    End Sub

    Public Function MasterInfoBoxView(ByVal customclass As Parameter.InfoBox) As Parameter.InfoBox
        Dim oMasterInfoBoxView As IInfoBox
        oMasterInfoBoxView = ComponentFactory.CreateInfoBox()
        Return oMasterInfoBoxView.MasterInfoBoxView(customclass)
    End Function

#Region "UserInfoBox"
    Public Function UserInfoBoxPaging(ByVal customclass As Parameter.InfoBox) As Parameter.InfoBox
        Dim oUserInfoBoxPaging As IInfoBox
        oUserInfoBoxPaging = ComponentFactory.CreateInfoBox()
        Return oUserInfoBoxPaging.UserInfoBoxPaging(customclass)
    End Function
    Public Sub UserInfoBoxUpdate(ByVal customclass As Parameter.InfoBox)
        Dim oUserInfoBoxUpdate As IInfoBox
        oUserInfoBoxUpdate = ComponentFactory.CreateInfoBox()
        oUserInfoBoxUpdate.UserInfoBoxUpdate(customclass)
    End Sub
#End Region

End Class
