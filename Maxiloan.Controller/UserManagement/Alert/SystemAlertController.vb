
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
#End Region

Public Class SystemAlertController
#Region "SystemAlert"
    Public Sub SystemAlertAdd(ByVal customclass As Parameter.SystemAlert)
        Dim oSystemAlertAdd As ISystemAlert
        oSystemAlertAdd = ComponentFactory.CreateSystemAlert()
        oSystemAlertAdd.SystemAlertAdd(customclass)
    End Sub

    Public Sub SystemAlertEdit(ByVal customclass As Parameter.SystemAlert)
        Dim oSystemAlertEdit As ISystemAlert
        oSystemAlertEdit = ComponentFactory.CreateSystemAlert()
        oSystemAlertEdit.SystemAlertEdit(customclass)
    End Sub

    Public Sub SystemAlertDelete(ByVal customclass As Parameter.SystemAlert)
        Dim oSystemAlertEdit As ISystemAlert
        oSystemAlertEdit = ComponentFactory.CreateSystemAlert()
        oSystemAlertEdit.SystemAlertDelete(customclass)
    End Sub

    Public Function SystemAlertView(ByVal customclass As Parameter.SystemAlert) As Parameter.SystemAlert
        Dim oSystemAlertEdit As ISystemAlert
        oSystemAlertEdit = ComponentFactory.CreateSystemAlert()
        Return oSystemAlertEdit.SystemAlertView(customclass)
    End Function

    Public Function GetGroupAlert(ByVal customclass As Parameter.SystemAlert) As DataTable
        Dim oGetGroupAlert As ISystemAlert
        oGetGroupAlert = ComponentFactory.CreateSystemAlert()
        Return oGetGroupAlert.GetGroupAlert(customclass)
    End Function
#End Region

#Region "User Alert"
    Public Function UserAlertPaging(ByVal customclass As Parameter.SystemAlert) As Parameter.SystemAlert
        Dim oUserAlertPaging As ISystemAlert
        oUserAlertPaging = ComponentFactory.CreateSystemAlert()
        Return oUserAlertPaging.UserAlertPaging(customclass)
    End Function
    Public Sub UserAlertUpdate(ByVal customclass As Parameter.SystemAlert)
        Dim oUserAlertUpdate As ISystemAlert
        oUserAlertUpdate = ComponentFactory.CreateSystemAlert()
        oUserAlertUpdate.UserAlertUpdate(customclass)
    End Sub
#End Region

#Region "Group Alert"
    Public Sub GroupAlertAdd(ByVal customclass As Parameter.SystemAlert)
        Dim oUserAlertAdd As ISystemAlert
        oUserAlertAdd = ComponentFactory.CreateSystemAlert()
        oUserAlertAdd.GroupAlertAdd(customclass)
    End Sub

    Public Sub GroupAlertEdit(ByVal customclass As Parameter.SystemAlert)
        Dim oGroupAlertEdit As ISystemAlert
        oGroupAlertEdit = ComponentFactory.CreateSystemAlert()
        oGroupAlertEdit.GroupAlertEdit(customclass)
    End Sub

    Public Sub GroupAlertDelete(ByVal customclass As Parameter.SystemAlert)
        Dim oGroupAlertEdit As ISystemAlert
        oGroupAlertEdit = ComponentFactory.CreateSystemAlert()
        oGroupAlertEdit.GroupAlertDelete(customclass)
    End Sub

    Public Function GroupAlertView(ByVal customclass As Parameter.SystemAlert) As Parameter.SystemAlert
        Dim oGroupAlertEdit As ISystemAlert
        oGroupAlertEdit = ComponentFactory.CreateSystemAlert()
        Return oGroupAlertEdit.GroupAlertView(customclass)
    End Function
#End Region
End Class