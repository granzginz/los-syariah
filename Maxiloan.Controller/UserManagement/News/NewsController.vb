
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class NewsController
    Public Sub MasterNewsAdd(ByVal customclass As Parameter.News)
        Dim oMasterNewsAdd As INews
        oMasterNewsAdd = ComponentFactory.CreateNews()
        oMasterNewsAdd.MasterNewsAdd(customclass)
    End Sub

    Public Sub MasterNewsEdit(ByVal customclass As Parameter.News)
        Dim oMasterNewsEdit As INews
        oMasterNewsEdit = ComponentFactory.CreateNews()
        oMasterNewsEdit.MasterNewsEdit(customclass)
    End Sub

    Public Sub MasterNewsDelete(ByVal customclass As Parameter.News)
        Dim oMasterNewsDelete As INews
        oMasterNewsDelete = ComponentFactory.CreateNews()
        oMasterNewsDelete.MasterNewsDelete(customclass)
    End Sub

    Public Function MasterNewsView(ByVal customclass As Parameter.News) As Parameter.News
        Dim oMasterNewsView As INews
        oMasterNewsView = ComponentFactory.CreateNews()
        Return oMasterNewsView.MasterNewsView(customclass)
    End Function

    Public Function NewsDetailView(ByVal customclass As Parameter.News) As Parameter.News
        Dim oNewsDetailView As INews
        oNewsDetailView = ComponentFactory.CreateNews()
        Return oNewsDetailView.NewsDetailView(customclass)
    End Function

    Public Sub NewsDetailUpdate(ByVal customclass As Parameter.News)
        Dim oNewsDetailUpdate As INews
        oNewsDetailUpdate = ComponentFactory.CreateNews()
        oNewsDetailUpdate.NewsDetailUpdate(customclass)
    End Sub
End Class
