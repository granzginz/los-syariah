
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class APDisbInqController
    Public Function APDisbInqList(ByVal oCustomClass As Parameter.APDisbInq) As Parameter.APDisbInq
        Dim oAPDisbInqList As IAPDisbInq
        oAPDisbInqList = ComponentFactory.CreateAPDisbInq()
        Return oAPDisbInqList.ListAPDisbInq(oCustomClass)
    End Function

    Public Function APDisbInqReport(ByVal oCustomClass As Parameter.APDisbInq) As Parameter.APDisbInq
        Dim oAPDisbInqReport As IAPDisbInq
        oAPDisbInqReport = ComponentFactory.CreateAPDisbInq()
        Return oAPDisbInqReport.ReportAPDisbInq(oCustomClass)
    End Function

#Region "PV Inquiry"
    Public Function PVInquiryList(ByVal oCustomClass As Parameter.APDisbInq) As Parameter.APDisbInq
        Dim oPVInquiryList As IAPDisbInq
        oPVInquiryList = ComponentFactory.CreateAPDisbInq()
        Return oPVInquiryList.ListPVInquiry(oCustomClass)
    End Function
    Public Function PVInquiryReport(ByVal oCustomClass As Parameter.APDisbInq) As Parameter.APDisbInq
        Dim oPVInquiryReport As IAPDisbInq
        oPVInquiryReport = ComponentFactory.CreateAPDisbInq()
        Return oPVInquiryReport.ReportPVInquiry(oCustomClass)
    End Function
#End Region

    Public Function APDisbInqListPrintSelection(ByVal oCustomClass As Parameter.APDisbInq) As Parameter.APDisbInq
        Dim oAPDisbInqList As IAPDisbInq
        oAPDisbInqList = ComponentFactory.CreateAPDisbInq()
        Return oAPDisbInqList.ListAPDisbInqPrintSelection(oCustomClass)
    End Function

    Public Sub savePrintSelection(ByVal oCustomClass As Parameter.APDisbInq)
        Dim oAPDisbInqList As IAPDisbInq
        oAPDisbInqList = ComponentFactory.CreateAPDisbInq()
        oAPDisbInqList.savePrintSelection(oCustomClass)
    End Sub
End Class
