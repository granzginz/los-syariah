
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess


Public Class APDisbAppController
    Public Function ListApOtorTrans(ByVal oCustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim oAPDisbAppList As IAPDisbApp
        oAPDisbAppList = ComponentFactory.CreateAPDisbApp()
        Return oAPDisbAppList.ListApOtorTrans(oCustomClass)
    End Function


    Public Function APDisbAppList(ByVal oCustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim oAPDisbAppList As IAPDisbApp
        oAPDisbAppList = ComponentFactory.CreateAPDisbApp()
        Return oAPDisbAppList.ListAPDisbApp(oCustomClass)
    End Function

    Public Function APDisbAppInformasi(ByVal oCustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim oAPDisbAppInformasi As IAPDisbApp
        oAPDisbAppInformasi = ComponentFactory.CreateAPDisbApp()
        Return oAPDisbAppInformasi.InformasiAPDisbApp(oCustomClass)
    End Function

    Public Function UpdateAPDisbApp(ByVal oCustomClass As Parameter.APDisbApp) As String
        Dim oUpdateAPDisbApp As IAPDisbApp
        oUpdateAPDisbApp = ComponentFactory.CreateAPDisbApp()
        Return oUpdateAPDisbApp.UpdateAPDisbApp(oCustomClass)
    End Function

    Public Function UpdateAPDisbApp2(ByVal oCustomClass As Parameter.APDisbApp) As String
        Dim oUpdateAPDisbApp As IAPDisbApp
        oUpdateAPDisbApp = ComponentFactory.CreateAPDisbApp()
        Return oUpdateAPDisbApp.UpdateAPDisbApp2(oCustomClass)
    End Function

    Public Function APDisbAppList2(ByVal oCustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim oAPDisbAppList As IAPDisbApp
        oAPDisbAppList = ComponentFactory.CreateAPDisbApp()
        Return oAPDisbAppList.ListAPDisbApp2(oCustomClass)
    End Function

    Public Function APDisbAppList3(ByVal oCustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim oAPDisbAppList As IAPDisbApp
        oAPDisbAppList = ComponentFactory.CreateAPDisbApp()
        Return oAPDisbAppList.ListAPDisbApp3(oCustomClass)
    End Function

    Public Function APDisbAppList4(ByVal oCustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim oAPDisbAppList As IAPDisbApp
        oAPDisbAppList = ComponentFactory.CreateAPDisbApp()
        Return oAPDisbAppList.ListAPDisbApp4(oCustomClass)
    End Function

    Public Function MemoPembayaranReport(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim oAPDisbAppList As IAPDisbApp
        oAPDisbAppList = ComponentFactory.CreateAPDisbApp()
        Return oAPDisbAppList.MemoPembayaranReport(customclass)
    End Function

    Public Function MemoPembayaranDetail1Report(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim oAPDisbAppList As IAPDisbApp
        oAPDisbAppList = ComponentFactory.CreateAPDisbApp()
        Return oAPDisbAppList.MemoPembayaranDetail1Report(customclass)
    End Function

    Public Function MemoPembayaranDetail2Report(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim oAPDisbAppList As IAPDisbApp
        oAPDisbAppList = ComponentFactory.CreateAPDisbApp()
        Return oAPDisbAppList.MemoPembayaranDetail2Report(customclass)
    End Function

    Public Function GetReleaseEbanking(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim oAPDisbAppList As IAPDisbApp
        oAPDisbAppList = ComponentFactory.CreateAPDisbApp()
        Return oAPDisbAppList.GetReleaseEbanking(customclass)
    End Function

    Public Sub ReleaseEbankingUpdateStatus(ByVal ocustomClass As Parameter.APDisbApp)
        Dim oAPDisbAppList As IAPDisbApp
        oAPDisbAppList = ComponentFactory.CreateAPDisbApp()
        oAPDisbAppList.ReleaseEbankingUpdateStatus(ocustomClass)
    End Sub
    Public Sub declinceReleaseEbanking(ByVal ocustomClass As Parameter.APDisbApp)
        Dim oAPDisbAppList As IAPDisbApp
        oAPDisbAppList = ComponentFactory.CreateAPDisbApp()
        oAPDisbAppList.declinceReleaseEbanking(ocustomClass)
    End Sub
    Public Function UpdateMultiAPDisbApp(cnn As String, status As String, branchId As String, datas As IList(Of Parameter.ValueTextObject), ByVal ParamArray bankSelected() As String) As String
        Dim oAPDisbAppList As IAPDisbApp
        oAPDisbAppList = ComponentFactory.CreateAPDisbApp()
        Return oAPDisbAppList.UpdateMultiAPDisbApp(cnn, status, branchId, datas, bankSelected)
    End Function
    Public Function UpdateMultiAPDisbApp2(ByVal ocustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim oAPDisbAppList As IAPDisbApp
        oAPDisbAppList = ComponentFactory.CreateAPDisbApp()
        Return oAPDisbAppList.UpdateMultiAPDisbApp2(ocustomClass)
    End Function
    Public Function LoadComboRekeningBank(cnn As String, caraBayar As String) As IList(Of Parameter.CommonValueText)
        Dim oAPDisbAppList As IAPDisbApp
        oAPDisbAppList = ComponentFactory.CreateAPDisbApp()
        Return oAPDisbAppList.LoadComboRekeningBank(cnn, caraBayar)
    End Function
    Public Function APGAntiCaraBayar(ByVal oCustomClass As Parameter.APDisbApp) As String
        Dim oUpdateAPDisbApp As IAPDisbApp
        oUpdateAPDisbApp = ComponentFactory.CreateAPDisbApp()
        Return oUpdateAPDisbApp.APgantiCaraBayar(oCustomClass)
    End Function
    Public Function UpdateMultiAPDisbApp2_Report(ByVal oCustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim oUpdateAPDisbApp As IAPDisbApp
        oUpdateAPDisbApp = ComponentFactory.CreateAPDisbApp
        Return oUpdateAPDisbApp.UpdateMultiAPDisbApp2_Report(oCustomClass)
    End Function

#Region "APDisbCash"
    Public Sub UpdateAPDisbCash(ByVal oCustomClass As Parameter.APDisbApp, Optional isApDisbTransBeforeDisburse As Boolean = True)
        Dim oUpdateAPDisbCash As IAPDisbApp
        oUpdateAPDisbCash = ComponentFactory.CreateAPDisbApp()
        oUpdateAPDisbCash.UpdateAPDisbCash(oCustomClass, isApDisbTransBeforeDisburse)
    End Sub
#End Region
End Class
