
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess


Public Class APDisbSelecController
    Public Function APSeleList(ByVal oCustomClass As Parameter.APDisbSelec) As Parameter.APDisbSelec
        Dim oAPSeleList As IAPDisbSelec
        oAPSeleList = ComponentFactory.CreateAPDisbSelec()
        Return oAPSeleList.ListAPSele(oCustomClass)
    End Function

    Public Function APSelectionList(ByVal oCustomClass As Parameter.APDisbSelec) As DataTable
        Dim oAPSeleList As IAPDisbSelec
        oAPSeleList = ComponentFactory.CreateAPDisbSelec()
        Return oAPSeleList.APSelectionList(oCustomClass)
    End Function

    Public Function APGroupSelectionList(ByVal oCustomClass As Parameter.APDisbSelec) As Parameter.APDisbSelec
        Dim oAPSeleList As IAPDisbSelec
        oAPSeleList = ComponentFactory.CreateAPDisbSelec()
        Return oAPSeleList.APGroupSelectionList(oCustomClass)
    End Function

    Public Function SavingToPaymentVoucher(ByVal oCustomClass As Parameter.APDisbSelec) As Parameter.APDisbSelec
        Dim oSavingPaymentVoucher As IAPDisbSelec
        oSavingPaymentVoucher = ComponentFactory.CreateAPDisbSelec()
        Return oSavingPaymentVoucher.SavingToPaymentVoucher(oCustomClass)
    End Function

    Public Sub SaveChangeLoc(ByVal oCustomClass As Parameter.APDisbSelec)
        Dim oSaveLoc As IAPDisbSelec
        oSaveLoc = ComponentFactory.CreateAPDisbSelec()
        oSaveLoc.SavingChangeLoc(oCustomClass)
    End Sub
    Public Function EBankList(ByVal oCustomClass As Parameter.APDisbSelec) As DataTable
        Dim oAPSeleList As IAPDisbSelec
        oAPSeleList = ComponentFactory.CreateAPDisbSelec()
        Return oAPSeleList.ListEBanking(oCustomClass)
    End Function
End Class
