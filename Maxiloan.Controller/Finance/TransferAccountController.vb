

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class TransferAccountController
    Public Function GetBankType(ByVal oCustomClass As Parameter.TransferAccount) As Parameter.TransferAccount
        Dim oGetBankType As ITransferAccount
        oGetBankType = ComponentFactory.CreateTransferAccount
        Return oGetBankType.GetBankType(oCustomClass)
    End Function

    Public Sub SaveTransferAccount(ByVal oCustomClass As Parameter.TransferAccount)
        Dim oSaveTransferAccount As ITransferAccount
        oSaveTransferAccount = ComponentFactory.CreateTransferAccount
        oSaveTransferAccount.SaveTrasnferAccount(oCustomClass)
    End Sub

    Public Function EBankTRACCOUNTAdd(ByVal oCustomClass As Parameter.TransferAccount) As Parameter.TransferAccount
        Dim oGetBankType As ITransferAccount
        oGetBankType = ComponentFactory.CreateTransferAccount
        Return oGetBankType.EbankTRACCOUNTAdd(oCustomClass)
    End Function

    Public Sub SaveTransferAccountOtorisasi(ByVal oCustomClass As Parameter.TransferAccount)
        Dim oSaveTransferAccount As ITransferAccount
        oSaveTransferAccount = ComponentFactory.CreateTransferAccount
        oSaveTransferAccount.SaveTransferAccountOtorisasi(oCustomClass)
    End Sub
End Class
