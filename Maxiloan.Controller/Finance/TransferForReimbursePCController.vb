

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class TransferForReimbursePCController
    Public Function ListPCReimburse(ByVal oCustomClass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim oTRFund As ITransferForReimbursePC
        oTRFund = ComponentFactory.CreateTransferReimbursePC
        Return oTRFund.ListPCReimburse(oCustomClass)
    End Function

    Public Function ListBranchHO(ByVal oCustomClass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim oTRFund As ITransferForReimbursePC
        oTRFund = ComponentFactory.CreateTransferReimbursePC
        Return oTRFund.ListBranchHO(oCustomClass)
    End Function

    Public Function ListPettyCash(ByVal oCustomClass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim oTRFund As ITransferForReimbursePC
        oTRFund = ComponentFactory.CreateTransferReimbursePC
        Return oTRFund.ListPettyCash(oCustomClass)
    End Function

    Public Function ListBankAccount(ByVal oCustomClass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim oTRFund As ITransferForReimbursePC
        oTRFund = ComponentFactory.CreateTransferReimbursePC
        Return oTRFund.ListBankAccount(oCustomClass)
    End Function

    Public Function ListBilyetGiro(ByVal oCustomClass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim oTRFund As ITransferForReimbursePC
        oTRFund = ComponentFactory.CreateTransferReimbursePC
        Return oTRFund.ListBilyetGiro(oCustomClass)
    End Function

    Public Sub SaveReimbursePC(ByVal oCustomClass As Parameter.TransferForReimbursePC)
        Dim oTRFund As ITransferForReimbursePC
        oTRFund = ComponentFactory.CreateTransferReimbursePC
        oTRFund.SaveReimbursePC(oCustomClass)
    End Sub

    Public Function CheckEndingBalance(ByVal oCustomClass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim oTRFund As ITransferForReimbursePC
        oTRFund = ComponentFactory.CreateTransferReimbursePC
        Return oTRFund.CheckEndingBalance(oCustomClass)
    End Function

    Public Function EbankPCREIAdd(ByVal oCustomClass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim oTRFund As ITransferForReimbursePC
        oTRFund = ComponentFactory.CreateTransferReimbursePC
        Return oTRFund.EbankPCREIAdd(oCustomClass)
    End Function

    Public Function ListPCReimburseOtorisasi(ByVal oCustomClass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim oTRFund As ITransferForReimbursePC
        oTRFund = ComponentFactory.CreateTransferReimbursePC
        Return oTRFund.ListPCReimburseOtorisasi(oCustomClass)
    End Function

    Public Function ListPettyCashOtorisasi(ByVal oCustomClass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim oTRFund As ITransferForReimbursePC
        oTRFund = ComponentFactory.CreateTransferReimbursePC
        Return oTRFund.ListPettyCashOtorisasi(oCustomClass)
    End Function

    Public Sub SaveReimbursePCOtorisasi(ByVal oCustomClass As Parameter.TransferForReimbursePC)
        Dim oTRFund As ITransferForReimbursePC
        oTRFund = ComponentFactory.CreateTransferReimbursePC
        oTRFund.SaveReimbursePCOtorisasi(oCustomClass)
    End Sub

    Public Function ListPettyCashApr(ByVal oCustomClass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim oTRFund As ITransferForReimbursePC
        oTRFund = ComponentFactory.CreateTransferReimbursePC
        Return oTRFund.ListPettyCashApr(oCustomClass)
    End Function

    Public Function ListBankAccount2(ByVal oCustomClass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
        Dim oTRFund As ITransferForReimbursePC
        oTRFund = ComponentFactory.CreateTransferReimbursePC
        Return oTRFund.ListBankAccount2(oCustomClass)
    End Function
End Class
