
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess


Public Class EBankTransferController

    Public Function EBankTransferSave(ByVal listdata As ArrayList, ByVal pvdata As Parameter.APDisbApp) As String
        Dim oEBankTransferSave As IEBankTranfer
        oEBankTransferSave = ComponentFactory.CreateEBankTransfer()
        Return oEBankTransferSave.EBankTransferSave(listdata, pvdata)
    End Function

    Public Function EBankTransferList(ByVal oCustomClass As Parameter.EBankTransfer) As Parameter.EBankTransfer
        Dim oEBankTransferList As IEBankTranfer
        oEBankTransferList = ComponentFactory.CreateEBankTransfer()
        Return oEBankTransferList.EBankTransferList(oCustomClass)
    End Function

    Public Function EBankTransferList2(ByVal oCustomClass As Parameter.EBankTransfer) As Parameter.EBankTransfer
        Dim oEBankTransferList As IEBankTranfer
        oEBankTransferList = ComponentFactory.CreateEBankTransfer()
        Return oEBankTransferList.EBankTransferList2(oCustomClass)
    End Function

    'Public Function EBankExecute(ByVal oCustomClass As Parameter.APDisbApp) As String
    '    Dim oEBankTransferList As IEBankTranfer
    '    oEBankTransferList = ComponentFactory.CreateEBankTransfer()
    '    Return oEBankTransferList.EBankExecute(oCustomClass)
    'End Function

    Public Function EBankTransferChangeStatus(ByVal oCustomClass As Parameter.EBankTransfer) As String
        Dim oEBankTransferList As IEBankTranfer
        oEBankTransferList = ComponentFactory.CreateEBankTransfer()
        Return oEBankTransferList.EBankTransferChangeStatus(oCustomClass)
    End Function

    Public Function EBankRejectSave(ByVal oCustomClass As Parameter.EBankReject) As String
        Dim oEBankRejectSave As IEBankTranfer
        oEBankRejectSave = ComponentFactory.CreateEBankTransfer()
        Return oEBankRejectSave.EBankRejectSave(oCustomClass)
    End Function

    Public Function EBankExecuteReject(ByVal oCustomClass As Parameter.APDisbApp) As String
        Dim oEBankTransferList As IEBankTranfer
        oEBankTransferList = ComponentFactory.CreateEBankTransfer()
        Return oEBankTransferList.EBankExecuteReject(oCustomClass)
    End Function

    'Public Function EBankPYREQExec(ByVal oCustomClass As Parameter.APDisbApp) As String
    '    Dim oEBankTransferList As IEBankTranfer
    '    oEBankTransferList = ComponentFactory.CreateEBankTransfer()
    '    Return oEBankTransferList.EBankPYREQExec(oCustomClass)
    'End Function

    'Public Function EBankTRFAAExec(ByVal oCustomClass As Parameter.APDisbApp) As String
    '    Dim oEBankTransferList As IEBankTranfer
    '    oEBankTransferList = ComponentFactory.CreateEBankTransfer()
    '    Return oEBankTransferList.EBankTRFAAExec(oCustomClass)
    'End Function

    'Public Function EBankTRFUNDExec(ByVal oCustomClass As Parameter.APDisbApp) As String
    '    Dim oEBankTransferList As IEBankTranfer
    '    oEBankTransferList = ComponentFactory.CreateEBankTransfer()
    '    Return oEBankTransferList.EBankTRFUNDExec(oCustomClass)
    'End Function

    'Public Function EBankPCREIExec(ByVal oCustomClass As Parameter.APDisbApp) As String
    '    Dim oEBankTransferList As IEBankTranfer
    '    oEBankTransferList = ComponentFactory.CreateEBankTransfer()
    '    Return oEBankTransferList.EBankPCREIExec(oCustomClass)
    'End Function

    Public Function EBankExecuteNew(ByVal listdata As ArrayList, ByVal ebankdata As Parameter.EBankTransfer) As String
        Dim oEBankTransferList As IEBankTranfer
        oEBankTransferList = ComponentFactory.CreateEBankTransfer()
        Return oEBankTransferList.EBankExecuteNew(listdata, ebankdata)
    End Function

    Public Function GetRequestEdit(ByVal customclass As Parameter.EBankTransfer) As Parameter.EBankTransfer
        Dim oEBankTransferList As IEBankTranfer
        oEBankTransferList = ComponentFactory.CreateEBankTransfer()
        Return oEBankTransferList.GetRequestEdit(customclass)
    End Function
End Class
