Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class AgentFeeDisbursementController
    Public Function getAgentFeeDisbursemnet(ByVal customClass As Parameter.AgentFeeDisbursement) As Parameter.AgentFeeDisbursement
        Dim oInterface As IAgentFeeDisbursement
        oInterface = ComponentFactory.CreateAgentFeeDisbursement()
        Return oInterface.getAgentFeeDisbursementList(customClass)
    End Function

    Public Function saveAgentFeeDisbursement(ByVal customClass As Parameter.AgentFeeDisbursement) As String
        Dim oInterface As IAgentFeeDisbursement
        oInterface = ComponentFactory.CreateAgentFeeDisbursement()
        Return oInterface.saveAgentFeeDisbursement(customClass)
    End Function
End Class
