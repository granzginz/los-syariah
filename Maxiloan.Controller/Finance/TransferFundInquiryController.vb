
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class TransferFundInquiryController
    Public Function InqTransferFund(ByVal ocustomclass As Parameter.TransferFundInquiry) As Parameter.TransferFundInquiry
        Dim TransferFund As ITransferAccount
        TransferFund = ComponentFactory.CreateTransferAccount
        Return TransferFund.InqTransferFund(ocustomclass)
    End Function
    Public Function ViewRptTransferFund(ByVal oCustomClass As Parameter.TransferFundInquiry) As Parameter.TransferFundInquiry
        Dim RptTransferFund As ITransferAccount
        RptTransferFund = ComponentFactory.CreateTransferAccount
        Return RptTransferFund.ViewRptTransferFund(oCustomClass)
    End Function
    Public Function GetViewTransferFund(ByVal oCustomClass As Parameter.TransferFundInquiry) As Parameter.TransferFundInquiry
        Dim ViewTransferFund As ITransferAccount
        ViewTransferFund = ComponentFactory.CreateTransferAccount
        Return ViewTransferFund.GetViewTransferFund(oCustomClass)
    End Function
End Class
