﻿Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class CTKonfirmasiEbankExec
    Public Function GetListing(ByVal oCostom As Parameter.PEKonfirmasiEbankExec) As Parameter.PEKonfirmasiEbankExec
        Dim iKonfirmasiEbankExec As IEKonfirmasiEbankExec
        iKonfirmasiEbankExec = ComponentFactory.CreateKonfirmasiEbankExec
        Return iKonfirmasiEbankExec.GetListing(oCostom)
    End Function
    Public Function Saving(ByVal oCustom As Parameter.PEKonfirmasiEbankExec) As String
        Dim iKonfirmasiEbankExec As IEKonfirmasiEbankExec
        iKonfirmasiEbankExec = ComponentFactory.CreateKonfirmasiEbankExec
        Return iKonfirmasiEbankExec.Saving(oCustom)
    End Function
End Class
