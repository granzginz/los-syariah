

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class TransferForPaymentRequestController
    Public Function ListRequest(ByVal oCustomClass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest
        Dim oTransfer As ITransferForPaymentRequest
        oTransfer = ComponentFactory.CreateTransferPaymentRequest
        Return oTransfer.ListRequest(oCustomClass)
    End Function

    Public Function ListPaymentDetail(ByVal oCustomClass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest
        Dim oTransfer As ITransferForPaymentRequest
        oTransfer = ComponentFactory.CreateTransferPaymentRequest
        Return oTransfer.ListPaymentDetail(oCustomClass)
    End Function

    Public Sub SavePaymentRequest(ByVal oCustomClass As Parameter.TransferForPaymentRequest)
        Dim oTransfer As ITransferForPaymentRequest
        oTransfer = ComponentFactory.CreateTransferPaymentRequest
        oTransfer.SavePaymentRequest(oCustomClass)
    End Sub

    Public Function EbankPYREQAdd(ByVal oCustomClass As Parameter.TransferForPaymentRequest) As String
        Dim oTransfer As ITransferForPaymentRequest
        oTransfer = ComponentFactory.CreateTransferPaymentRequest
        Return oTransfer.EbankPYREQAdd(oCustomClass)
    End Function

    Public Function ListRequestOtorisasi(ByVal oCustomClass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest
        Dim oTransfer As ITransferForPaymentRequest
        oTransfer = ComponentFactory.CreateTransferPaymentRequest
        Return oTransfer.ListRequestOtorisasi(oCustomClass)
    End Function

    Public Function ListPaymentDetailOtorisasi(ByVal oCustomClass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest
        Dim oTransfer As ITransferForPaymentRequest
        oTransfer = ComponentFactory.CreateTransferPaymentRequest
        Return oTransfer.ListPaymentDetailOtorisasi(oCustomClass)
    End Function

    Public Sub SavePaymentRequestOtorisasi(ByVal oCustomClass As Parameter.TransferForPaymentRequest)
        Dim oTransfer As ITransferForPaymentRequest
        oTransfer = ComponentFactory.CreateTransferPaymentRequest
        oTransfer.SavePaymentRequestOtorisasi(oCustomClass)
    End Sub
    Public Function ListPaymentHistoryReject(ByVal oCustomClass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest
        Dim oTransfer As ITransferForPaymentRequest
        oTransfer = ComponentFactory.CreateTransferPaymentRequest
        Return oTransfer.ListPaymentHistoryReject(oCustomClass)
    End Function

End Class
