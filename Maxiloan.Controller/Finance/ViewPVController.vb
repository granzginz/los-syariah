
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class ViewPVController
    Public Function ViewList(ByVal oCustomClass As Parameter.APDisbSelec) As Parameter.APDisbSelec
        Dim oViewList As IViewPV
        oViewList = ComponentFactory.CreateViewPV
        Return oViewList.ViewList(oCustomClass)
    End Function

    Public Function ViewListPV(ByVal oCustomClass As Parameter.APDisbSelec) As Parameter.APDisbSelec
        Dim oViewListPV As IViewPV
        oViewListPV = ComponentFactory.CreateViewPV
        Return oViewListPV.viewListPV(oCustomClass)
    End Function
End Class
