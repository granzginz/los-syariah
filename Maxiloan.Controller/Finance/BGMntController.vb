
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess


Public Class BGMntController

#Region "BGMaintenance"
    'Function GetBankAccountName(ByVal oCollector As Parameter.DataCollection) As Parameter.DataCollection
    '    Dim CollectorBO As IDataCollection
    '    CollectorBO = ComponentFactory.CreateDataCollection
    '    Return CollectorBO.GetBankAccountName(oCollector)
    'End Function

    Function BGMntList(ByVal oCustomClass As Parameter.BGMnt) As Parameter.BGMnt
        Dim oBankAccountList As IBGMnt
        oBankAccountList = ComponentFactory.CreateBGMnt()
        Return oBankAccountList.ListBGMnt(oCustomClass)
    End Function

    'Function BGMntAdd(ByVal oCustomClass As Parameter.BGMnt) As String
    '    Dim oBankAccountList As IBGMnt
    '    oBankAccountList = ComponentFactory.CreateBGMnt()
    '    Return oBankAccountList.AddBGMnt(oCustomClass)
    'End Function

    Function BGMntCmbBankAccount(ByVal oCustomClass As Parameter.BGMnt) As Parameter.BGMnt
        Dim oBankAccountList As IBGMnt
        Try
            oBankAccountList = ComponentFactory.CreateBGMnt()
        Catch exp As Exception
            Dim strError As String
            strError = exp.Message
        End Try

        Return oBankAccountList.BGMntCmbBankAccount(oCustomClass)
    End Function

    Function GetTableBGMnt(ByVal oCustomClass As Parameter.BGMnt, ByVal StrName As String) As Parameter.BGMnt
        Dim oBankAccountList As IBGMnt
        oBankAccountList = ComponentFactory.CreateBGMnt()
        Return oBankAccountList.GetTableBGMnt(oCustomClass, StrName)
    End Function

    Function SaveBGMnt(ByVal oCustomClass As Parameter.BGMnt, ByVal StrName As String) As String
        Dim oSaveBGMnt As IBGMnt
        oSaveBGMnt = ComponentFactory.CreateBGMnt()
        Return oSaveBGMnt.SaveBGMnt(oCustomClass, StrName)
    End Function

    Function UpdateBGMnt(ByVal oCustomClass As Parameter.BGMnt) As String
        Dim oUpdateBGMnt As IBGMnt
        oUpdateBGMnt = ComponentFactory.CreateBGMnt()
        Return oUpdateBGMnt.UpdateBGMnt(oCustomClass)
    End Function

    Function BGMntReport(ByVal oCustomClass As Parameter.BGMnt) As Parameter.BGMnt
        Dim oReportBGMnt As IBGMnt
        oReportBGMnt = ComponentFactory.CreateBGMnt()
        Return oReportBGMnt.ReportBGMnt(oCustomClass)
    End Function
#End Region

#Region "APSelection"
    Function APSeleList(ByVal oCustomClass As Parameter.BGMnt) As Parameter.BGMnt
        Dim oAPSeleList As IBGMnt
        oAPSeleList = ComponentFactory.CreateBGMnt()
        Return oAPSeleList.ListAPSele(oCustomClass)
    End Function
#End Region

End Class
