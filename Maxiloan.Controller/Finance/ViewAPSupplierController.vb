


#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class ViewAPSupplierController
    Public Function ViewList(ByVal oCustomClass As Parameter.APDisbSelec) As Parameter.APDisbSelec
        Dim oViewList As IViewAPSupplier
        oViewList = ComponentFactory.CreateViewAPSupplier
        Return oViewList.ViewList(oCustomClass)
    End Function

    Public Function ViewListPO(ByVal oCustomClass As Parameter.APDisbSelec) As Parameter.APDisbSelec
        Dim oViewListPO As IViewAPSupplier
        oViewListPO = ComponentFactory.CreateViewAPSupplier
        Return oViewListPO.ViewListPO(oCustomClass)
    End Function

    Public Function ViewInvoiceDeduction(ByVal oCustomClass As Parameter.APDisbSelec) As Parameter.APDisbSelec
        Dim oViewInvoiceDeduction As IViewAPSupplier
        oViewInvoiceDeduction = ComponentFactory.CreateViewAPSupplier
        Return oViewInvoiceDeduction.ViewInvoiceDeduction(oCustomClass)
    End Function

    Public Function ListViewAP(ByVal oCustomClass As Parameter.APDisbSelec) As Parameter.APDisbSelec
        Dim oViewListAP As IViewAPSupplier
        oViewListAP = ComponentFactory.CreateViewAPSupplier
        Return oViewListAP.ListViewAP(oCustomClass)
    End Function
End Class
