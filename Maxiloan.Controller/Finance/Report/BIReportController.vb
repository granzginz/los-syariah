
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class BIReportController
    Public Sub BIReportSave(ByVal strConnection As String, ByVal Tahun As String, ByVal Bulan As String)
        Dim oBIReportSave As IBIReport
        oBIReportSave = ComponentFactory.CreateBIReport()
        oBIReportSave.BIReportSave(strConnection, Tahun, Bulan)
    End Sub
    Public Function BIReportViewer(ByVal strConnection As String, ByVal Tahun As String, ByVal Bulan As String) As DataSet
        Dim oBIReportSave As IBIReport
        oBIReportSave = ComponentFactory.CreateBIReport()
        Return oBIReportSave.BIReportViewer(strConnection, Tahun, Bulan)
    End Function
    Public Function BIReportSemiAnual(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim oBIReportSave As IBIReport
        oBIReportSave = ComponentFactory.CreateBIReport()
        Return oBIReportSave.BIReportSemiAnual(customclass)
    End Function
End Class
