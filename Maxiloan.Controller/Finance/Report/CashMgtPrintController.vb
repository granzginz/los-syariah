Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Public Class CashMgtPrintController
    Public Function getInstallmentDueDate(ByVal customClass As Parameter.InstallmentDueDate) As Parameter.InstallmentDueDate
        Dim oInterface As ICashMgtPrint
        oInterface = ComponentFactory.CreateInstallmentDueDate()
        Return oInterface.getInstallmentDueDate(customClass)
    End Function

    Public Function getInstallmentDueDateCollection(ByVal customClass As Parameter.InstallmentDueDate) As Parameter.InstallmentDueDate
        Dim oInterface As ICashMgtPrint
        oInterface = ComponentFactory.CreateInstallmentDueDate()
        Return oInterface.getInstallmentDueDateCollection(customClass)
    End Function
End Class
