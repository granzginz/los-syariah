﻿Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class CTEBankPendingReq
    Public Function GetListing(ByVal oCostom As Parameter.PEBankPendingReq) As Parameter.PEBankPendingReq
        Dim iBankPending As IEBankPendingReq
        iBankPending = ComponentFactory.CreateBankPendingReq
        Return iBankPending.GetListing(oCostom)
    End Function
    Public Function GetDetailPay(ByVal oCostom As Parameter.PEBankPendingReq) As Parameter.PEBankPendingReq
        Dim iBankPending As IEBankPendingReq
        iBankPending = ComponentFactory.CreateBankPendingReq
        Return iBankPending.GetDetailPay(oCostom)
    End Function
    Public Function SaveUpdate(ByVal oCostom As Parameter.PEBankPendingReq) As String
        Dim iBankPending As IEBankPendingReq
        iBankPending = ComponentFactory.CreateBankPendingReq
        Return iBankPending.SaveUpdate(oCostom)
    End Function
End Class
