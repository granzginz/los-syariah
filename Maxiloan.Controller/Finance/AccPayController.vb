Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class AccPayController
    Public Function getStatusPencairanPinjamanList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
        Dim oInterface As IAccPay
        oInterface = ComponentFactory.CreateAccPay()
        Return oInterface.getPencairanPinjamaList(customClass)
    End Function
    Public Function savePencairanPinjamanHO(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
        Dim oInterface As IAccPay
        oInterface = ComponentFactory.CreateAccPay()
        Return oInterface.savePencairanPinjamanHO(customClass)
    End Function
    Public Function getPencairanPinjamanCustomerList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
        Dim oInterface As IAccPay
        oInterface = ComponentFactory.CreateAccPay()
        Return oInterface.getPencairanPinjamanCustomerList(customClass)
    End Function
    Public Sub savePencairanPinjamanCustomer(ByVal customClass As Parameter.AccPayParameter)
        Dim oInterface As IAccPay
        oInterface = ComponentFactory.CreateAccPay()
        oInterface.SavePencairanPinjamanCustomer(customClass)
    End Sub
    Public Function getInquiryStatusPencairanPinjamanList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
        Dim oInterface As IAccPay
        oInterface = ComponentFactory.CreateAccPay()
        Return oInterface.getInquiryPencairanPinjamanList(customClass)
    End Function
    Public Function getOutstandingPencairanPinjamanList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
        Dim oInterface As IAccPay
        oInterface = ComponentFactory.CreateAccPay()
        Return oInterface.getOutstandingPencairanPinjamanList(customClass)
    End Function
    Public Function getBuktiKasKeluarList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
        Dim oInterface As IAccPay
        oInterface = ComponentFactory.CreateAccPay()
        Return oInterface.getBuktiKasKeluarList(customClass)
    End Function

    Public Sub saveBiayaProses(ByVal customClass As Parameter.AccPayParameter)
        Dim oInterface As IAccPay
        oInterface = ComponentFactory.CreateAccPay()
        oInterface.saveBiayaProses(customClass)
    End Sub

    Public Function BiayaProsesReportDetail(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
        Dim oInterface As IAccPay
        oInterface = ComponentFactory.CreateAccPay()
        Return oInterface.BiayaProsesReportDetail(customClass)
    End Function

    Public Function BiayaProsesReportSummary(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
        Dim oInterface As IAccPay
        oInterface = ComponentFactory.CreateAccPay()
        Return oInterface.BiayaProsesReportSummary(customClass)
    End Function
End Class
