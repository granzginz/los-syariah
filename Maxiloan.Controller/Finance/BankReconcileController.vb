


#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class BankReconcileController
    Public Function BankReconList(ByVal oCustomClass As Parameter.BankReconcile) As Parameter.BankReconcile
        Dim oBankReconList As IBankReconcile
        oBankReconList = ComponentFactory.CreateBankReconcile
        Return oBankReconList.ListBankRecon(oCustomClass)
    End Function

    Public Function BankReconListDet(ByVal oCustomClass As Parameter.BankReconcile) As Parameter.BankReconcile
        Dim oBankReconList As IBankReconcile
        oBankReconList = ComponentFactory.CreateBankReconcile
        Return oBankReconList.ListBankReconDet(oCustomClass)
    End Function

    Public Sub SaveBankRecon(ByVal oCustomClass As Parameter.BankReconcile)
        Dim oBankReconList As IBankReconcile
        oBankReconList = ComponentFactory.CreateBankReconcile
        oBankReconList.SaveBankRecon(oCustomClass)
    End Sub
    Public Function InfoReconBalance(ByVal oCustomClass As Parameter.BankReconcile) As Parameter.BankReconcile
        Dim oBankReconList As IBankReconcile
        oBankReconList = ComponentFactory.CreateBankReconcile
        Return oBankReconList.InfoReconBalance(oCustomClass)
    End Function

End Class
