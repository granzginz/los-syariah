

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class TransferHOBranchController
    Public Function GetBankType(ByVal oCustomClass As Parameter.TransferAccount) As Parameter.TransferAccount
        Dim oGetBankType As ITransferHOBranch
        oGetBankType = ComponentFactory.CreateTransferHOBranch
        Return oGetBankType.GetBankType(oCustomClass)
    End Function

    Public Sub SaveTransferHOBranch(ByVal oCustomClass As Parameter.TransferAccount)
        Dim oSaveTransferHOBranch As ITransferHOBranch
        oSaveTransferHOBranch = ComponentFactory.CreateTransferHOBranch
        oSaveTransferHOBranch.SaveTransferHOBranch(oCustomClass)
    End Sub

    Public Function EBankTRFFUNDAdd(ByVal oCustomClass As Parameter.TransferAccount) As Parameter.TransferAccount
        Dim oGetBankType As ITransferHOBranch
        oGetBankType = ComponentFactory.CreateTransferHOBranch
        Return oGetBankType.EBankTRFUNDAdd(oCustomClass)
    End Function

    Sub PenarikanDanaCabangSave(ByVal customclass As Parameter.TransferAccount)
        Dim oSaveTransferHOBranch As ITransferHOBranch
        oSaveTransferHOBranch = ComponentFactory.CreateTransferHOBranch
        oSaveTransferHOBranch.PenarikanDanaCabangSave(customclass)
    End Sub

    Sub OtorisasiPenarikanDanaCabangSave(ByVal customclass As Parameter.TransferAccount)
        Dim oSaveTransferHOBranch As ITransferHOBranch
        oSaveTransferHOBranch = ComponentFactory.CreateTransferHOBranch
        oSaveTransferHOBranch.OtorisasiPenarikanDanaCabangSave(customclass)
    End Sub

    Sub OtorisasiPenarikanDanaCabangReject(ByVal customclass As Parameter.TransferAccount)
        Dim oSaveTransferHOBranch As ITransferHOBranch
        oSaveTransferHOBranch = ComponentFactory.CreateTransferHOBranch
        oSaveTransferHOBranch.OtorisasiPenarikanDanaCabangReject(customclass)
    End Sub
End Class
