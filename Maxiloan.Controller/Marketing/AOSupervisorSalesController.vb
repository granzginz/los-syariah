
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AOSupervisorSalesController
    Public Function AOSupervisorListing(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
        Dim oAOListing As IAOSupervisorSales
        oAOListing = ComponentFactory.CreateAOSupervisorSales
        Return oAOListing.AOSupervisorListing(customclass)
    End Function

    Public Function AOSupervisorBudgetListing(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
        Dim oAOBudgetListing As IAOSupervisorSales
        oAOBudgetListing = ComponentFactory.CreateAOSupervisorSales
        Return oAOBudgetListing.AOSupervisorBudgetListing(customclass)
    End Function
    Public Function AOSupervisorBudgetView(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
        Dim oAOBudgetView As IAOSupervisorSales
        oAOBudgetView = ComponentFactory.CreateAOSupervisorSales
        Return oAOBudgetView.AOSupervisorBudgetView(customclass)
    End Function
    Public Function AOSupervisorBudgetPrint(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
        Dim oAOBudgetPrint As IAOSupervisorSales
        oAOBudgetPrint = ComponentFactory.CreateAOSupervisorSales
        Return oAOBudgetPrint.AOSupervisorBudgetPrint(customclass)
    End Function
    Public Function AOSupervisorForecastListing(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
        Dim oAOForecastListing As IAOSupervisorSales
        oAOForecastListing = ComponentFactory.CreateAOSupervisorSales
        Return oAOForecastListing.AOSupervisorForecastListing(customclass)
    End Function
    Public Function AOSupervisorForecastView(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
        Dim oAOForecastView As IAOSupervisorSales
        oAOForecastView = ComponentFactory.CreateAOSupervisorSales
        Return oAOForecastView.AOSupervisorForecastView(customclass)
    End Function
    Public Function AOSupervisorForecastPrint(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
        Dim oAOForecastPrint As IAOSupervisorSales
        oAOForecastPrint = ComponentFactory.CreateAOSupervisorSales
        Return oAOForecastPrint.AOSupervisorForecastPrint(customclass)
    End Function
End Class
