
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AODirectSalesController
    Public Function AOListing(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
        Dim oAOListing As IAODirectSales
        oAOListing = ComponentFactory.CreateAODirectSales
        Return oAOListing.AOListing(customclass)
    End Function
    Public Function AOBudgetListing(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
        Dim oAOBudget As IAODirectSales
        oAOBudget = ComponentFactory.CreateAODirectSales
        Return oAOBudget.AOBudgetListing(customclass)
    End Function
    Public Sub AOBudgetAdd(ByVal customclass As Parameter.AODirectSales)
        Dim oAOBudgetAdd As IAODirectSales
        oAOBudgetAdd = ComponentFactory.CreateAODirectSales
        oAOBudgetAdd.AOBudgetAdd(customclass)
    End Sub
    Public Sub AOBudgetEdit(ByVal customclass As Parameter.AODirectSales)
        Dim oAOBudgetEdit As IAODirectSales
        oAOBudgetEdit = ComponentFactory.CreateAODirectSales
        oAOBudgetEdit.AOBudgetEdit(customclass)
    End Sub
    Public Function AOBudgetView(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
        Dim oAOBudgetView As IAODirectSales
        oAOBudgetView = ComponentFactory.CreateAODirectSales
        Return oAOBudgetView.AOBudgetView(customclass)
    End Function
    Public Function AOForecastListing(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
        Dim oAOForecast As IAODirectSales
        oAOForecast = ComponentFactory.CreateAODirectSales
        Return oAOForecast.AOForecastListing(customclass)
    End Function
    Public Sub AOForecastAdd(ByVal customclass As Parameter.AODirectSales)
        Dim oAOForecastAdd As IAODirectSales
        oAOForecastAdd = ComponentFactory.CreateAODirectSales
        oAOForecastAdd.AOForecastAdd(customclass)
    End Sub
    Public Sub AOForecastEdit(ByVal customclass As Parameter.AODirectSales)
        Dim oAOForecastEdit As IAODirectSales
        oAOForecastEdit = ComponentFactory.CreateAODirectSales
        oAOForecastEdit.AOForecastEdit(customclass)
    End Sub
    Public Sub AOBudgetSaveEdit(ByVal customclass As Parameter.AODirectSales)
        Dim oAOBudgetSaveEdit As IAODirectSales
        oAOBudgetSaveEdit = ComponentFactory.CreateAODirectSales
        oAOBudgetSaveEdit.AOBudgetSaveEdit(customclass)
    End Sub
    Public Function AOForecastView(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
        Dim oAOForecastView As IAODirectSales
        oAOForecastView = ComponentFactory.CreateAODirectSales
        Return oAOForecastView.AOForecastView(customclass)
    End Function

    Public Function AOForecastPrint(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
        Dim oAOForecastPrint As IAODirectSales
        oAOForecastPrint = ComponentFactory.CreateAODirectSales
        Return oAOForecastPrint.AOForecastPrint(customclass)
    End Function
    Public Function AOBudgetPrint(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
        Dim oAOBudgetPrint As IAODirectSales
        oAOBudgetPrint = ComponentFactory.CreateAODirectSales
        Return oAOBudgetPrint.AOBudgetPrint(customclass)
    End Function

    Public Function AOBudgetList(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
        Dim oAOBudgetList As IAODirectSales
        oAOBudgetList = ComponentFactory.CreateAODirectSales
        Return oAOBudgetList.AOBudgetList(customclass)
    End Function
End Class
