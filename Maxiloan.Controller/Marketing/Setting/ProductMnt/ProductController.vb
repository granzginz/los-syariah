

#Region "Imports"

Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region

Public Class ProductController

    Public Function LoadKegiatanUsaha(cnn As String) As IList(Of Parameter.KegiatanUsaha)
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.LoadKegiatanUsaha(cnn)
    End Function
    Public Function ProductPaging(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductPaging(customClass)
    End Function

    Public Function ProductReport(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductReport(customClass)
    End Function

    Public Function ProductEdit(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductEdit(customClass)
    End Function

    Public Function ProductView(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductView(customClass)
    End Function

    Public Function ProductSaveAdd(ByVal customClass As Parameter.Product) As String
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductSaveAdd(customClass)
    End Function

    Public Sub ProductSaveEdit(ByVal customClass As Parameter.Product)
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        ProductBO.ProductSaveEdit(customClass)
    End Sub

    Public Function Get_Combo_AssetType(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.Get_Combo_AssetType(customClass)
    End Function

    Public Function Get_Combo_ScoreSchemeMaster(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.Get_Combo_ScoreSchemeMaster(customClass)
    End Function

    Public Function Get_Combo_CreditScoreSchemeMaster(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.Get_Combo_CreditScoreSchemeMaster(customClass)
    End Function

    Public Function Get_Combo_JournalScheme(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.Get_Combo_JournalScheme(customClass)
    End Function

    Public Function Get_Combo_ApprovalTypeScheme(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.Get_Combo_ApprovalTypeScheme(customClass)
    End Function

    Public Function Get_Combo_Term_n_Condition(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.Get_Combo_Term_n_Condition(customClass)
    End Function

    Public Function Get_Combo_Term_n_Condition_Brc(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.Get_Combo_Term_n_Condition_Brc(customClass)
    End Function

    Public Function ProductTCPaging(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductTCPaging(customClass)
    End Function

    Public Function ProductTCReport(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductTCReport(customClass)
    End Function

    Public Function ProductTCEdit(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductTCEdit(customClass)
    End Function

    Public Function ProductTCDelete(ByVal customClass As Parameter.Product) As String
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductTCDelete(customClass)
    End Function

    Public Function ProductTCSaveAdd(ByVal customClass As Parameter.Product) As String
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductTCSaveAdd(customClass)
    End Function

    Public Sub ProductTCSaveEdit(ByVal customClass As Parameter.Product)
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        ProductBO.ProductTCSaveEdit(customClass)
    End Sub


    Public Function ProductBranchHOPaging(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductBranchHOPaging(customClass)
    End Function

    Public Function ProductBranchHOReport(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductBranchHOReport(customClass)
    End Function

    Public Function ProductBranchHOEdit(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductBranchHOEdit(customClass)
    End Function

    Public Function ProductBranchHOSaveAdd(ByVal customClass As Parameter.Product) As String
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductBranchHOSaveAdd(customClass)
    End Function

    Public Sub ProductBranchHOSaveEdit(ByVal customClass As Parameter.Product)
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        ProductBO.ProductBranchHOSaveEdit(customClass)
    End Sub
    Public Sub CopyProduct(ByVal customClass As Parameter.CopyProduct)
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        ProductBO.CopyProduct(customClass)
    End Sub

    Public Function GetBranch(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.GetBranch(customClass)
    End Function

    Public Function GetBranchAll(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.GetBranchAll(customClass)
    End Function

    Public Function ProductBranchPaging(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductBranchPaging(customClass)
    End Function

    Public Function ProductBranchReport(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductBranchReport(customClass)
    End Function

    Public Function ProductBranchEdit(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductBranchEdit(customClass)
    End Function

    Public Sub ProductBranchSaveEdit(ByVal customClass As Parameter.Product)
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        ProductBO.ProductBranchSaveEdit(customClass)
    End Sub

    Public Function ProductBranchTCPaging(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductBranchTCPaging(customClass)
    End Function

    Public Function ProductBranchTCReport(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductBranchTCReport(customClass)
    End Function

    Public Function ProductBranchTCEdit(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductBranchTCEdit(customClass)
    End Function

    Public Function ProductBranchTCDelete(ByVal customClass As Parameter.Product) As String
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductBranchTCDelete(customClass)
    End Function

    Public Function ProductBranchTCSaveAdd(ByVal customClass As Parameter.Product) As String
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductBranchTCSaveAdd(customClass)
    End Function

    Public Sub ProductBranchTCSaveEdit(ByVal customClass As Parameter.Product)
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        ProductBO.ProductBranchTCSaveEdit(customClass)
    End Sub

    Public Function ProductOfferingPaging(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductOfferingPaging(customClass)
    End Function

    Public Function ProductOfferingReport(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductOfferingReport(customClass)
    End Function

    Public Function ProductOfferingEdit(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductOfferingEdit(customClass)
    End Function

    Public Function ProductOfferingAdd(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductOfferingAdd(customClass)
    End Function

    Public Function ProductOfferingView(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductOfferingView(customClass)
    End Function

    Public Function ProductOfferingSaveAdd(ByVal customClass As Parameter.Product) As String
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductOfferingSaveAdd(customClass)
    End Function

    Public Sub ProductOfferingSaveEdit(ByVal customClass As Parameter.Product)
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        ProductBO.ProductOfferingSaveEdit(customClass)
    End Sub

    Public Function ProductOfferingDelete(ByVal customClass As Parameter.Product) As String
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductOfferingDelete(customClass)
    End Function
    Public Function ProductByKUJP(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.ProductByKUJP(customClass)
    End Function

    Public Function DropDownListProduct(ByVal customClass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.DropDownListProduct(customClass)
    End Function

    Public Function GetCboSkePemb(ByVal Customclass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.GetCboSkePemb(Customclass)
    End Function
    Public Function GetCboJenPemb(ByVal Customclass As Parameter.Product) As Parameter.Product
        Dim ProductBO As IProduct
        ProductBO = ComponentFactory.Product
        Return ProductBO.GetCboJenPemb(Customclass)
    End Function
End Class
