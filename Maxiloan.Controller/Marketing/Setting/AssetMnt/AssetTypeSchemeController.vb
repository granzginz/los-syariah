Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess


Public Class AssetTypeSchemeController
    Public Function GetAssetTypeScheme(ByVal customClass As Parameter.AssetTypeScheme) As Parameter.AssetTypeScheme
        Dim AssetTypeSchemeBO As IAssetType
        AssetTypeSchemeBO = ComponentFactory.CreateAssetType()
        Return AssetTypeSchemeBO.GetAssetTypeScheme(customClass)
    End Function

    Public Function GetAssetTypeSchemeReport(ByVal customClass As Parameter.AssetTypeScheme) As Parameter.AssetTypeScheme
        Dim AssetTypeSchemeBO As IAssetType
        AssetTypeSchemeBO = ComponentFactory.CreateAssetType()
        Return AssetTypeSchemeBO.GetAssetTypeSchemeReport(customClass)
    End Function

    Public Function GetAssetTypeSchemeEdit(ByVal customClass As Parameter.AssetTypeScheme) As Parameter.AssetTypeScheme
        Dim AssetTypeSchemeBO As IAssetType
        AssetTypeSchemeBO = ComponentFactory.CreateAssetType()
        Return AssetTypeSchemeBO.GetAssetTypeSchemeEdit(customClass)
    End Function

    Public Function AssetTypeSchemeSaveAdd(ByVal customClass As Parameter.AssetTypeScheme) As String
        Dim AssetTypeSchemeBO As IAssetType
        AssetTypeSchemeBO = ComponentFactory.CreateAssetType()
        Return AssetTypeSchemeBO.AssetTypeSchemeSaveAdd(customClass)
    End Function

    Public Sub AssetTypeSchemeSaveEdit(ByVal customClass As Parameter.AssetTypeScheme)
        Dim AssetTypeSchemeBO As IAssetType
        AssetTypeSchemeBO = ComponentFactory.CreateAssetType()
        AssetTypeSchemeBO.AssetTypeSchemeSaveEdit(customClass)
    End Sub

    Public Function AssetTypeSchemeDelete(ByVal customClass As Parameter.AssetTypeScheme) As String
        Dim AssetTypeSchemeBO As IAssetType
        AssetTypeSchemeBO = ComponentFactory.CreateAssetType()
        Return AssetTypeSchemeBO.AssetTypeSchemeDelete(customClass)
    End Function

    Public Function AssetTypeSchemeAdd(ByVal customClass As Parameter.AssetTypeScheme) As String
        Dim AssetTypeSchemeBO As IAssetType
        AssetTypeSchemeBO = ComponentFactory.CreateAssetType()
        Return AssetTypeSchemeBO.AssetTypeSchemeAdd(customClass)
    End Function

End Class

