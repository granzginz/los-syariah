
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class AssetTypeOriginationController
    Public Function GetAssetTypeOrigination(ByVal customClass As Parameter.AssetTypeOrigination) As Parameter.AssetTypeOrigination
        Dim AssetTypeOriginationBO As IAssetType
        AssetTypeOriginationBO = ComponentFactory.CreateAssetType()
        Return AssetTypeOriginationBO.GetAssetTypeOrigination(customClass)
    End Function

    Public Function GetAssetTypeOriginationReport(ByVal customClass As Parameter.AssetTypeOrigination) As Parameter.AssetTypeOrigination
        Dim AssetTypeoAssetTypeOriginationBO As IAssetType
        AssetTypeoAssetTypeOriginationBO = ComponentFactory.CreateAssetType()
        Return AssetTypeoAssetTypeOriginationBO.GetAssetTypeOriginationReport(customClass)
    End Function

    Public Function GetAssetTypeOriginationEdit(ByVal customClass As Parameter.AssetTypeOrigination) As Parameter.AssetTypeOrigination
        Dim AssetTypeOriginationBO As IAssetType
        AssetTypeOriginationBO = ComponentFactory.CreateAssetType()
        Return AssetTypeOriginationBO.GetAssetTypeOriginationEdit(customClass)
    End Function

    Public Function AssetTypeOriginationSaveAdd(ByVal customClass As Parameter.AssetTypeOrigination) As String
        Dim AssetTypeOriginationBO As IAssetType
        AssetTypeOriginationBO = ComponentFactory.CreateAssetType()
        Return AssetTypeOriginationBO.AssetTypeOriginationSaveAdd(customClass)
    End Function

    Public Sub AssetTypeOriginationSaveEdit(ByVal customClass As Parameter.AssetTypeOrigination)
        Dim AssetTypeOriginationBO As IAssetType
        AssetTypeOriginationBO = ComponentFactory.CreateAssetType()
        AssetTypeOriginationBO.AssetTypeOriginationSaveEdit(customClass)
    End Sub

    Public Function AssetTypeOriginationDelete(ByVal customClass As Parameter.AssetTypeOrigination) As String
        Dim AssetTypeOriginationBO As IAssetType
        AssetTypeOriginationBO = ComponentFactory.CreateAssetType()
        Return AssetTypeOriginationBO.AssetTypeOriginationDelete(customClass)
    End Function

End Class

