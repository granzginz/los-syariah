
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class AssetMasterPriceController
    Public Function GetAssetMasterPrice(ByVal ocustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
        Dim AssetMasterPriceBO As IAssetMasterPrice
        AssetMasterPriceBO = ComponentFactory.CreateAssetMasterPrice()
        Return AssetMasterPriceBO.GetAssetMasterPrice(ocustomClass)
    End Function
    Public Function GetAssetMaster(ByVal ocustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
        Dim AssetMasterPriceBO As IAssetMasterPrice
        AssetMasterPriceBO = ComponentFactory.CreateAssetMasterPrice()
        Return AssetMasterPriceBO.GetAssetMaster(ocustomClass)
    End Function
    Public Function GetAssetMasterPriceReport(ByVal ocustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
        Dim AssetMasterPriceBO As IAssetMasterPrice
        AssetMasterPriceBO = ComponentFactory.CreateAssetMasterPrice()
        Return AssetMasterPriceBO.GetAssetMasterPriceReport(ocustomClass)
    End Function
    Public Function GetAssetMasterPriceList(ByVal ocustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
        Dim AssetMasterPriceBO As IAssetMasterPrice
        AssetMasterPriceBO = ComponentFactory.CreateAssetMasterPrice()
        Return AssetMasterPriceBO.GetAssetMasterPriceList(ocustomClass)
    End Function

    Public Function AssetMasterPriceSaveAdd(ByVal ocustomClass As Parameter.AssetMasterPrice) As String
        Dim AssetMasterPriceBO As IAssetMasterPrice
        AssetMasterPriceBO = ComponentFactory.CreateAssetMasterPrice()
        Return AssetMasterPriceBO.AssetMasterPriceSaveAdd(ocustomClass)
    End Function

    Public Sub AssetMasterPriceSaveEdit(ByVal ocustomClass As Parameter.AssetMasterPrice)
        Dim AssetMasterPriceBO As IAssetMasterPrice
        AssetMasterPriceBO = ComponentFactory.CreateAssetMasterPrice()
        AssetMasterPriceBO.AssetMasterPriceSaveEdit(ocustomClass)
    End Sub

    Public Function AssetMasterPriceDelete(ByVal ocustomClass As Parameter.AssetMasterPrice) As String
        Dim AssetMasterPriceBO As IAssetMasterPrice
        AssetMasterPriceBO = ComponentFactory.CreateAssetMasterPrice()
        Return AssetMasterPriceBO.AssetMasterPriceDelete(ocustomClass)
    End Function

    Public Function GetAssetTypeCombo(ByVal ocustomClass As Parameter.AssetMasterPrice) As DataTable
        Dim AssetMasterPriceBO As IAssetMasterPrice
        AssetMasterPriceBO = ComponentFactory.CreateAssetMasterPrice()
        Return AssetMasterPriceBO.GetAssetTypeCombo(ocustomClass)
    End Function

    Public Function GetAssetCodeCombo(ByVal ocustomClass As Parameter.AssetMasterPrice) As DataTable
        Dim AssetMasterPriceBO As IAssetMasterPrice
        AssetMasterPriceBO = ComponentFactory.CreateAssetMasterPrice()
        Return AssetMasterPriceBO.GetAssetCodeCombo(ocustomClass)
    End Function

    Public Function GetAllAssetCode(ByVal ocustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
        Dim AssetMasterPriceBO As IAssetMasterPrice
        AssetMasterPriceBO = ComponentFactory.CreateAssetMasterPrice()
        Return AssetMasterPriceBO.GetAllAssetCode(ocustomClass)
    End Function

    Public Function GetAssetMasterByAssetCode(ByVal ocustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
        Dim AssetMasterPriceBO As IAssetMasterPrice
        AssetMasterPriceBO = ComponentFactory.CreateAssetMasterPrice()
        Return AssetMasterPriceBO.GetAssetMasterByAssetCode(ocustomClass)
    End Function
End Class
