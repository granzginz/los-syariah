

Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class AssetTypeAttributeController
    Public Function GetAssetTypeAttribute(ByVal customClass As Parameter.AssetTypeAttribute) As Parameter.AssetTypeAttribute
        Dim AssetTypeAttributeBO As IAssetType
        AssetTypeAttributeBO = ComponentFactory.CreateAssetType()
        Return AssetTypeAttributeBO.GetAssetTypeAttribute(customClass)
    End Function

    Public Function GetAssetTypeAttributeReport(ByVal customClass As Parameter.AssetTypeAttribute) As Parameter.AssetTypeAttribute
        Dim AssetTypeAttributeBO As IAssetType
        AssetTypeAttributeBO = ComponentFactory.CreateAssetType()
        Return AssetTypeAttributeBO.GetAssetTypeAttributeReport(customClass)
    End Function

    Public Function GetAssetTypeAttributeEdit(ByVal customClass As Parameter.AssetTypeAttribute) As Parameter.AssetTypeAttribute
        Dim AssetTypeAttributeBO As IAssetType
        AssetTypeAttributeBO = ComponentFactory.CreateAssetType()
        Return AssetTypeAttributeBO.GetAssetTypeAttributeEdit(customClass)
    End Function

    Public Function AssetTypeAttributeSaveAdd(ByVal customClass As Parameter.AssetTypeAttribute) As String
        Dim AssetTypeAttributeBO As IAssetType
        AssetTypeAttributeBO = ComponentFactory.CreateAssetType()
        Return AssetTypeAttributeBO.AssetTypeAttributeSaveAdd(customClass)
    End Function

    Public Sub AssetTypeAttributeSaveEdit(ByVal customClass As Parameter.AssetTypeAttribute)
        Dim AssetTypeAttributeBO As IAssetType
        AssetTypeAttributeBO = ComponentFactory.CreateAssetType()
        AssetTypeAttributeBO.AssetTypeAttributeSaveEdit(customClass)
    End Sub

    Public Function AssetTypeAttributeDelete(ByVal customClass As Parameter.AssetTypeAttribute) As String
        Dim AssetTypeAttributeBO As IAssetType
        AssetTypeAttributeBO = ComponentFactory.CreateAssetType()
        Return AssetTypeAttributeBO.AssetTypeAttributeDelete(customClass)
    End Function

End Class


