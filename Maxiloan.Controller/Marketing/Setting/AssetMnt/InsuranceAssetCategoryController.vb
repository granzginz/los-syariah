

Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class InsuranceAssetCategoryController

#Region " GetInsuranceAssetCategory "
    Public Function GetInsuranceAssetCategory(ByVal oInsuranceAssetCategory As Parameter.InsuranceAssetCategory) As Parameter.InsuranceAssetCategory
        Dim InsuranceAssetCategoryBO As IInsuranceAssetCategory
        InsuranceAssetCategoryBO = ComponentFactory.InsRateCategory
        Return InsuranceAssetCategoryBO.GetInsuranceAssetCategory(oInsuranceAssetCategory)
    End Function
#End Region

#Region " GetInsuranceAssetCategoryReport "
    Public Function GetInsuranceAssetCategoryReport(ByVal oInsuranceAssetCategory As Parameter.InsuranceAssetCategory) As Parameter.InsuranceAssetCategory
        Dim InsuranceAssetCategoryBO As IInsuranceAssetCategory
        InsuranceAssetCategoryBO = ComponentFactory.InsRateCategory
        Return InsuranceAssetCategoryBO.GetInsuranceAssetCategoryReport(oInsuranceAssetCategory)
    End Function
#End Region

#Region " InsuranceAssetCategoryAdd "
    Public Function InsuranceAssetCategoryAdd(ByVal oInsuranceAssetCategory As Parameter.InsuranceAssetCategory) As String
        Dim InsuranceAssetCategoryBO As IInsuranceAssetCategory
        InsuranceAssetCategoryBO = ComponentFactory.InsRateCategory
        Return InsuranceAssetCategoryBO.InsuranceAssetCategoryAdd(oInsuranceAssetCategory)
    End Function
#End Region

#Region " InsuranceAssetCategoryUpdate "
    Public Function InsuranceAssetCategoryUpdate(ByVal oInsuranceAssetCategory As Parameter.InsuranceAssetCategory) As String
        Dim InsuranceAssetCategoryBO As IInsuranceAssetCategory
        InsuranceAssetCategoryBO = ComponentFactory.InsRateCategory
        Return InsuranceAssetCategoryBO.InsuranceAssetCategoryUpdate(oInsuranceAssetCategory)
    End Function
#End Region

#Region " InsuranceAssetCategoryDelete "
    Public Function InsuranceAssetCategoryDelete(ByVal oInsuranceAssetCategory As Parameter.InsuranceAssetCategory) As String
        Dim InsuranceAssetCategoryBO As IInsuranceAssetCategory
        InsuranceAssetCategoryBO = ComponentFactory.InsRateCategory
        Return InsuranceAssetCategoryBO.InsuranceAssetCategoryDelete(oInsuranceAssetCategory)
    End Function
#End Region

End Class
