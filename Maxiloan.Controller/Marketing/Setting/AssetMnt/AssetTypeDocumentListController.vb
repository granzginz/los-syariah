
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class AssetTypeDocumentListController

    Public Function GetAssetTypeDocumentList(ByVal customClass As Parameter.AssetTypeDocumentList) As Parameter.AssetTypeDocumentList
        Dim AssetTypeDocumentListBO As IAssetType
        AssetTypeDocumentListBO = ComponentFactory.CreateAssetType()
        Return AssetTypeDocumentListBO.GetAssetTypeDocumentList(customClass)
    End Function

    Public Function GetAssetTypeDocumentListReport(ByVal customClass As Parameter.AssetTypeDocumentList) As Parameter.AssetTypeDocumentList
        Dim AssetTypeDocumentListBO As IAssetType
        AssetTypeDocumentListBO = ComponentFactory.CreateAssetType()
        Return AssetTypeDocumentListBO.GetAssetTypeDocumentListReport(customClass)
    End Function

    Public Function GetAssetTypeDocumentListEdit(ByVal customClass As Parameter.AssetTypeDocumentList) As Parameter.AssetTypeDocumentList
        Dim AssetTypeDocumentListBO As IAssetType
        AssetTypeDocumentListBO = ComponentFactory.CreateAssetType()
        Return AssetTypeDocumentListBO.GetAssetTypeDocumentListEdit(customClass)
    End Function

    Public Function AssetTypeDocumentListSaveAdd(ByVal customClass As Parameter.AssetTypeDocumentList) As String
        Dim AssetTypeDocumentListBO As IAssetType
        AssetTypeDocumentListBO = ComponentFactory.CreateAssetType()
        Return AssetTypeDocumentListBO.AssetTypeDocumentListSaveAdd(customClass)
    End Function

    Public Function AssetTypeDocumentListSaveEdit(ByVal customClass As Parameter.AssetTypeDocumentList) As String
        Dim AssetTypeDocumentListBO As IAssetType
        AssetTypeDocumentListBO = ComponentFactory.CreateAssetType()
        Return AssetTypeDocumentListBO.AssetTypeDocumentListSaveEdit(customClass)
    End Function

    Public Function AssetTypeDocumentListDelete(ByVal customClass As Parameter.AssetTypeDocumentList) As String
        Dim AssetTypeDocumentListBO As IAssetType
        AssetTypeDocumentListBO = ComponentFactory.CreateAssetType()
        Return AssetTypeDocumentListBO.AssetTypeDocumentListDelete(customClass)
    End Function

End Class

