
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess


Public Class AssetTypeController
    Public Function GetAssetType(ByVal customClass As Parameter.AssetType) As Parameter.AssetType
        Dim AssetTypeBO As IAssetType
        AssetTypeBO = ComponentFactory.CreateAssetType()
        Return AssetTypeBO.GetAssetType(customClass)
    End Function
    Public Function GetAssetTypeReport(ByVal customClass As Parameter.AssetType) As Parameter.AssetType
        Dim AssetTypeBO As IAssetType
        AssetTypeBO = ComponentFactory.CreateAssetType()
        Return AssetTypeBO.GetAssetTypeReport(customClass)
    End Function
    Public Function GetAssetTypeEdit(ByVal customClass As Parameter.AssetType) As Parameter.AssetType
        Dim AssetTypeBO As IAssetType
        AssetTypeBO = ComponentFactory.CreateAssetType()
        Return AssetTypeBO.GetAssetTypeEdit(customClass)
    End Function

    Public Function AssetTypeAdd(ByVal customClass As Parameter.AssetType) As String
        Dim AssetTypeBO As IAssetType
        AssetTypeBO = ComponentFactory.CreateAssetType()
        Return AssetTypeBO.AssetTypeSaveAdd(customClass)
    End Function

    Public Sub AssetTypeUpdate(ByVal customClass As Parameter.AssetType)
        Dim AssetTypeBO As IAssetType
        AssetTypeBO = ComponentFactory.CreateAssetType()
        AssetTypeBO.AssetTypeSaveEdit(customClass)
    End Sub

    Public Function AssetTypeDelete(ByVal customClass As Parameter.AssetType) As String
        Dim AssetTypeBO As IAssetType
        AssetTypeBO = ComponentFactory.CreateAssetType()
        Return AssetTypeBO.AssetTypeDelete(customClass)
    End Function

    Public Function GetMaxLevel(ByVal strAssetTypeID As String, ByVal strConnection As String) As Integer
        Dim AssetTypeBO As IAssetType
        AssetTypeBO = ComponentFactory.CreateAssetType()
        Return AssetTypeBO.GetMaxLevel(strAssetTypeID, strConnection)
    End Function

End Class

