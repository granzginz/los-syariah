
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class AssetTypeCategoryController
    Public Function GetAssetTypeCategory(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory
        Dim AssetTypeCategoryBO As IAssetType
        AssetTypeCategoryBO = ComponentFactory.CreateAssetType()
        Return AssetTypeCategoryBO.GetAssetTypeCategory(customClass)
    End Function
    Public Function GetAssetTypeCategoryReport(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory
        Dim AssetTypeCategoryBO As IAssetType
        AssetTypeCategoryBO = ComponentFactory.CreateAssetType()
        Return AssetTypeCategoryBO.GetAssetTypeCategoryReport(customClass)
    End Function

    Public Function GetAssetTypeCategoryEdit(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory
        Dim AssetTypeCategoryBO As IAssetType
        AssetTypeCategoryBO = ComponentFactory.CreateAssetType()
        Return AssetTypeCategoryBO.GetAssetTypeCategoryEdit(customClass)
    End Function

    Public Function GetAssetTypeCategoryInsRateforSpecificAssetType(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory
        Dim AssetTypeCategoryBO As IAssetType
        AssetTypeCategoryBO = ComponentFactory.CreateAssetType()
        Return AssetTypeCategoryBO.GetAssetTypeCategoryInsRateforSpecificAssetType(customClass)
    End Function
    Public Function GetAssetTypeCategoryInsRate(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory
        Dim AssetTypeCategoryBO As IAssetType
        AssetTypeCategoryBO = ComponentFactory.CreateAssetType()
        Return AssetTypeCategoryBO.GetAssetTypeCategoryInsRate(customClass)
    End Function

    Public Function AssetTypeCategorySaveAdd(ByVal customClass As Parameter.AssetTypeCategory) As String
        Dim AssetTypeCategoryBO As IAssetType
        AssetTypeCategoryBO = ComponentFactory.CreateAssetType()
        Return AssetTypeCategoryBO.AssetTypeCategorySaveAdd(customClass)
    End Function

    Public Sub AssetTypeCategorySaveEdit(ByVal customClass As Parameter.AssetTypeCategory)
        Dim AssetTypeCategoryBO As IAssetType
        AssetTypeCategoryBO = ComponentFactory.CreateAssetType()
        AssetTypeCategoryBO.AssetTypeCategorySaveEdit(customClass)
    End Sub

    Public Function AssetTypeCategoryDelete(ByVal customClass As Parameter.AssetTypeCategory) As String
        Dim AssetTypeCategoryBO As IAssetType
        AssetTypeCategoryBO = ComponentFactory.CreateAssetType()
        Return AssetTypeCategoryBO.AssetTypeCategoryDelete(customClass)
    End Function

End Class

