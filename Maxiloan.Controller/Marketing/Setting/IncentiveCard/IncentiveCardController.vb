
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class IncentiveCardController
    Public Function GetViewByID(ByVal oCustomClass As Parameter.IncentiveCard) As Parameter.IncentiveCard
        Dim oIncentiveCard As IIncentiveCard
        oIncentiveCard = ComponentFactory.CreateIncentiveCard()
        Return oIncentiveCard.GetViewByID(oCustomClass)
    End Function
    Sub SupplierINCTVHeaderAdd(ByVal oCustomClass As Parameter.IncentiveCard)
        Dim oIncentiveCard As IIncentiveCard
        oIncentiveCard = ComponentFactory.CreateIncentiveCard()
        oIncentiveCard.SupplierINCTVHeaderAdd(oCustomClass)
    End Sub
    Sub SupplierINCTVHeaderEdit(ByVal oCustomClass As Parameter.IncentiveCard)
        Dim oIncentiveCard As IIncentiveCard
        oIncentiveCard = ComponentFactory.CreateIncentiveCard()
        oIncentiveCard.SupplierINCTVHeaderEdit(oCustomClass)
    End Sub
    Sub SupplierINCTVHeaderDelete(ByVal oCustomClass As Parameter.IncentiveCard)
        Dim oIncentiveCard As IIncentiveCard
        oIncentiveCard = ComponentFactory.CreateIncentiveCard()
        oIncentiveCard.SupplierINCTVHeaderDelete(oCustomClass)
    End Sub
    Public Function GetComboSupplierIncentiveCard(ByVal oCustomClass As Parameter.IncentiveCard) As DataTable
        Dim oIncentiveCard As IIncentiveCard
        oIncentiveCard = ComponentFactory.CreateIncentiveCard()
        Return oIncentiveCard.GetComboSupplierIncentiveCard(oCustomClass)
    End Function
    Sub SupplierINCTVDetailUnitAdd(ByVal oCustomClass As Parameter.IncentiveCard)
        Dim oIncentiveCard As IIncentiveCard
        oIncentiveCard = ComponentFactory.CreateIncentiveCard()
        oIncentiveCard.SupplierINCTVDetailUnitAdd(oCustomClass)
    End Sub
    Sub SupplierINCTVDetailUnitEdit(ByVal oCustomClass As Parameter.IncentiveCard)
        Dim oIncentiveCard As IIncentiveCard
        oIncentiveCard = ComponentFactory.CreateIncentiveCard()
        oIncentiveCard.SupplierINCTVDetailUnitEdit(oCustomClass)
    End Sub
    Sub SupplierINCTVDetailAFAdd(ByVal oCustomClass As Parameter.IncentiveCard)
        Dim oIncentiveCard As IIncentiveCard
        oIncentiveCard = ComponentFactory.CreateIncentiveCard()
        oIncentiveCard.SupplierINCTVDetailAFAdd(oCustomClass)
    End Sub
    Sub SupplierINCTVDetailAFEdit(ByVal oCustomClass As Parameter.IncentiveCard)
        Dim oIncentiveCard As IIncentiveCard
        oIncentiveCard = ComponentFactory.CreateIncentiveCard()
        oIncentiveCard.SupplierINCTVDetailAFEdit(oCustomClass)
    End Sub
    Sub SupplierINCTVDailyDAdd(ByVal oCustomClass As Parameter.IncentiveCard)
        Dim oIncentiveCard As IIncentiveCard
        oIncentiveCard = ComponentFactory.CreateIncentiveCard()
        oIncentiveCard.SupplierINCTVDailyDAdd(oCustomClass)
    End Sub
    Sub SupplierINCTVUpdateIncentiveRestAmount(ByVal oCustomClass As Parameter.IncentiveCard)
        Dim oIncentiveCard As IIncentiveCard
        oIncentiveCard = ComponentFactory.CreateIncentiveCard()
        oIncentiveCard.SupplierINCTVUpdateIncentiveRestAmount(oCustomClass)
    End Sub
    Sub SupplierINCTVUpdateIncentiveRestEmployeeAmount(ByVal oCustomClass As Parameter.IncentiveCard)
        Dim oIncentiveCard As IIncentiveCard
        oIncentiveCard = ComponentFactory.CreateIncentiveCard()
        oIncentiveCard.SupplierINCTVUpdateIncentiveRestEmployeeAmount(oCustomClass)
    End Sub
    Sub SupplierINCTVIncentiveRestAmountExecution(ByVal oCustomClass As Parameter.IncentiveCard)
        Dim oIncentiveCard As IIncentiveCard
        oIncentiveCard = ComponentFactory.CreateIncentiveCard()
        oIncentiveCard.SupplierINCTVIncentiveRestAmountExecution(oCustomClass)
    End Sub
    Sub SupplierINCTVEmployeeIncentiveRestAmountExecution(ByVal oCustomClass As Parameter.IncentiveCard)
        Dim oIncentiveCard As IIncentiveCard
        oIncentiveCard = ComponentFactory.CreateIncentiveCard()
        oIncentiveCard.SupplierINCTVEmployeeIncentiveRestAmountExecution(oCustomClass)
    End Sub
    Sub SupplierINCTVDailyDEdit(ByVal oCustomClass As Parameter.IncentiveCard)
        Dim oIncentiveCard As IIncentiveCard
        oIncentiveCard = ComponentFactory.CreateIncentiveCard()
        oIncentiveCard.SupplierINCTVDailyDEdit(oCustomClass)
    End Sub
End Class
