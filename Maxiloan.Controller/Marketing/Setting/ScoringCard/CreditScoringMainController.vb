

#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class CreditScoringMainController

#Region "Main"
    Public Function GetCreditScoringMain(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard
        Return CreditBO.GetCreditScoringMain(customClass)
    End Function
    Public Function GetCreditScoringMainReport(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard()
        Return CreditBO.GetCreditScoringMainReport(customClass)
    End Function
    Public Function CreditScoringMainEdit(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard()
        Return CreditBO.CreditScoringMainEdit(customClass)
    End Function

    Public Function CreditScoringMainAdd(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard()
        Return CreditBO.CreditScoringMainAdd(customClass)
    End Function


    Public Function CreditScoringMainSaveAdd(ByVal customClass As Parameter.CreditScoringMain) As String
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard()
        Return CreditBO.CreditScoringMainSaveAdd(customClass)
    End Function

    Public Sub CreditScoringMainSaveEdit(ByVal customClass As Parameter.CreditScoringMain)
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard()
        CreditBO.CreditScoringMainSaveEdit(customClass)
    End Sub

    Public Function CreditScoringMainDelete(ByVal customClass As Parameter.CreditScoringMain) As String
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard()
        Return CreditBO.CreditScoringMainDelete(customClass)
    End Function
#End Region

#Region "View"
    Public Function CreditScoringMainView(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard()
        Return CreditBO.CreditScoringMainView(customClass)
    End Function
#End Region

#Region "EditContent"
    Public Function GetEditContentPaging(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard
        Return CreditBO.GetEditContentPaging(customClass)
    End Function

    Public Function EditContentEdit(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard()
        Return CreditBO.EditContentEdit(customClass)
    End Function

    Public Function EditContentSaveAdd(ByVal customClass As Parameter.CreditScoringMain) As String
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard()
        Return CreditBO.EditContentSaveAdd(customClass)
    End Function

    Public Sub EditContentSaveEdit(ByVal customClass As Parameter.CreditScoringMain)
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard()
        CreditBO.EditContentSaveEdit(customClass)
    End Sub

    Public Function EditContentDelete(ByVal customClass As Parameter.CreditScoringMain) As String
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard()
        Return CreditBO.EditContentDelete(customClass)
    End Function
#End Region

#Region "EditContent_Table"
    Public Function GetEditContentPaging_Table(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard
        Return CreditBO.GetEditContentPaging_Table(customClass)
    End Function

    Public Function EditContentEdit_Table(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard()
        Return CreditBO.EditContentEdit_Table(customClass)
    End Function

    Public Sub EditContentSaveEdit_Table(ByVal customClass As Parameter.CreditScoringMain)
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard()
        CreditBO.EditContentSaveEdit_Table(customClass)
    End Sub
#End Region

#Region "Cut Off Scoring"
    Public Function CreditScoringCutOff(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard()
        Return CreditBO.CreditScoringCutOff(customClass)
    End Function

    Public Function CreditScoringCutOffAdd(ByVal customClass As Parameter.CreditScoringMain) As String
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard()
        Return CreditBO.CreditScoringCutOffAdd(customClass)
    End Function

    Public Function CreditScoringCutOffEdit(ByVal customClass As Parameter.CreditScoringMain) As String
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard()
        Return CreditBO.CreditScoringCutOffEdit(customClass)
    End Function
    Public Function CreditScoringCutOffDelete(ByVal customClass As Parameter.CreditScoringMain) As String
        Dim CreditBO As ICreditScoringMain
        CreditBO = ComponentFactory.CreditScoringCard()
        Return CreditBO.CreditScoringCutOffDelete(customClass)
    End Function
#End Region

End Class
