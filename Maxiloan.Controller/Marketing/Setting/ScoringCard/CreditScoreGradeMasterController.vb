
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class CreditScoreGradeMasterController

    Public Function GetCreditScoreGradeMasterList(ByVal ocustomClass As Parameter.CreditScoreGradeMaster) As List(Of Parameter.CreditScoreGradeMaster)
        Dim CreditScoreGradeMasterBO As ICreditScoreGradeMaster
        CreditScoreGradeMasterBO = ComponentFactory.CreateCreditScoreGradeMaster()
        Return CreditScoreGradeMasterBO.GetCreditScoreGradeMasterList(ocustomClass)
    End Function

    Public Function CreditScoreGradeMasterSaveAdd(ByVal ocustomClass As Parameter.CreditScoreGradeMaster) As String
        Dim CreditScoreGradeMasterBO As ICreditScoreGradeMaster
        CreditScoreGradeMasterBO = ComponentFactory.CreateCreditScoreGradeMaster()
        Return CreditScoreGradeMasterBO.CreditScoreGradeMasterSaveAdd(ocustomClass)
    End Function

    Public Sub CreditScoreGradeMasterSaveEdit(ByVal ocustomClass As Parameter.CreditScoreGradeMaster)
        Dim CreditScoreGradeMasterBO As ICreditScoreGradeMaster
        CreditScoreGradeMasterBO = ComponentFactory.CreateCreditScoreGradeMaster()
        CreditScoreGradeMasterBO.CreditScoreGradeMasterSaveEdit(ocustomClass)
    End Sub

    Public Function CreditScoreGradeMasterDelete(ByVal ocustomClass As Parameter.CreditScoreGradeMaster) As String
        Dim CreditScoreGradeMasterBO As ICreditScoreGradeMaster
        CreditScoreGradeMasterBO = ComponentFactory.CreateCreditScoreGradeMaster()
        Return CreditScoreGradeMasterBO.CreditScoreGradeMasterDelete(ocustomClass)
    End Function

End Class

