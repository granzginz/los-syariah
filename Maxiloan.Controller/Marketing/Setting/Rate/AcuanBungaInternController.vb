﻿Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Public Class AcuanBungaInternController
    Public Function GetListingUCL(ByVal oCustom As Parameter.PAcuanBungaIntern, ByVal Tipe As String) As DataTable
        Dim iAcuanBungaIntern As IAcuanBungaIntern
        iAcuanBungaIntern = ComponentFactory.CreateAcuanBungaIntern
        Return iAcuanBungaIntern.GetListingUCL(oCustom, Tipe)
    End Function
    Public Function GetListing(ByVal oCustom As Parameter.PAcuanBungaIntern) As Parameter.PAcuanBungaIntern
        Dim iAcuanBungaIntern As IAcuanBungaIntern
        iAcuanBungaIntern = ComponentFactory.CreateAcuanBungaIntern
        Return iAcuanBungaIntern.GetListing(oCustom)
    End Function
    Public Function SaveUpdate(ByVal oCustome As Parameter.PAcuanBungaIntern) As String
        Dim iAcuanBungaIntern As IAcuanBungaIntern
        iAcuanBungaIntern = ComponentFactory.CreateAcuanBungaIntern
        Return iAcuanBungaIntern.SaveUpdate(oCustome)
    End Function
    Public Function Delete(ByVal oCustome As Parameter.PAcuanBungaIntern) As String
        Dim iAcuanBungaIntern As IAcuanBungaIntern
        iAcuanBungaIntern = ComponentFactory.CreateAcuanBungaIntern
        Return iAcuanBungaIntern.Delete(oCustome)
    End Function
    Public Function UpdateAssetAcuan(ByVal oCustome As Parameter.PAcuanBungaIntern, ByVal tipe As String) As String
        Dim iAcuanBungaIntern As IAcuanBungaIntern
        iAcuanBungaIntern = ComponentFactory.CreateAcuanBungaIntern
        Return iAcuanBungaIntern.UpdateAssetAcuan(oCustome, tipe)
    End Function
End Class
