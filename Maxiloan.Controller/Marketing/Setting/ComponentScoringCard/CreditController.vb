
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Public Class CreditController
    Public Function GetCredit(ByVal customClass As Parameter.Credit) As Parameter.Credit
        Dim CreditBO As ICredit
        CreditBO = ComponentFactory.CreditScoreComponent
        Return CreditBO.GetCredit(customClass)
    End Function
    Public Function GetCreditReport(ByVal customClass As Parameter.Credit) As Parameter.Credit
        Dim CreditBO As ICredit
        CreditBO = ComponentFactory.CreditScoreComponent()
        Return CreditBO.GetCreditReport(customClass)
    End Function
    Public Function CreditEdit(ByVal customClass As Parameter.Credit) As Parameter.Credit
        Dim CreditBO As ICredit
        CreditBO = ComponentFactory.CreditScoreComponent()
        Return CreditBO.CreditEdit(customClass)
    End Function

    Public Function CreditSaveAdd(ByVal customClass As Parameter.Credit) As String
        Dim CreditBO As ICredit
        CreditBO = ComponentFactory.CreditScoreComponent()
        Return CreditBO.CreditSaveAdd(customClass)
    End Function

    Public Sub CreditSaveEdit(ByVal customClass As Parameter.Credit)
        Dim CreditBO As ICredit
        CreditBO = ComponentFactory.CreditScoreComponent()
        CreditBO.CreditSaveEdit(customClass)
    End Sub

    Public Function CreditDelete(ByVal customClass As Parameter.Credit) As String
        Dim CreditBO As ICredit
        CreditBO = ComponentFactory.CreditScoreComponent()
        Return CreditBO.CreditDelete(customClass)
    End Function

End Class
