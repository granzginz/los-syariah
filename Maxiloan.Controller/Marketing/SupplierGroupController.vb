
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class SupplierGroupController
    Public Function GetSupplierGroup(ByVal ocustomClass As Parameter.SupplierGroup) As Parameter.SupplierGroup
        Dim SupplierGroupBO As ISupplierGroup
        SupplierGroupBO = ComponentFactory.CreateSupplierGroup()
        Return SupplierGroupBO.GetSupplierGroup(ocustomClass)
    End Function
    Public Function GetSupplierGroupReport(ByVal ocustomClass As Parameter.SupplierGroup) As Parameter.SupplierGroup
        Dim SupplierGroupBO As ISupplierGroup
        SupplierGroupBO = ComponentFactory.CreateSupplierGroup()
        Return SupplierGroupBO.GetSupplierGroupReport(ocustomClass)
    End Function
    Public Function GetSupplierGroupList(ByVal ocustomClass As Parameter.SupplierGroup) As Parameter.SupplierGroup
        Dim SupplierGroupBO As ISupplierGroup
        SupplierGroupBO = ComponentFactory.CreateSupplierGroup()
        Return SupplierGroupBO.GetSupplierGroupList(ocustomClass)
    End Function

    Public Function SupplierGroupSaveAdd(ByVal ocustomClass As Parameter.SupplierGroup) As String
        Dim SupplierGroupBO As ISupplierGroup
        SupplierGroupBO = ComponentFactory.CreateSupplierGroup()
        Return SupplierGroupBO.SupplierGroupSaveAdd(ocustomClass)
    End Function

    Public Sub SupplierGroupSaveEdit(ByVal ocustomClass As Parameter.SupplierGroup)
        Dim SupplierGroupBO As ISupplierGroup
        SupplierGroupBO = ComponentFactory.CreateSupplierGroup()
        SupplierGroupBO.SupplierGroupSaveEdit(ocustomClass)
    End Sub

    Public Function SupplierGroupDelete(ByVal ocustomClass As Parameter.SupplierGroup) As String
        Dim SupplierGroupBO As ISupplierGroup
        SupplierGroupBO = ComponentFactory.CreateSupplierGroup()
        Return SupplierGroupBO.SupplierGroupDelete(ocustomClass)
    End Function

    Public Function GetJobPositionCombo(ByVal ocustomClass As Parameter.SupplierGroup) As DataTable
        Dim GroupSupplierAccountBO As ISupplierGroup
        GroupSupplierAccountBO = ComponentFactory.CreateSupplierGroup()
        Return GroupSupplierAccountBO.GetJobPositionCombo(ocustomClass)
    End Function
    Function DeleteSupplierGroup(ByVal ocustomClass As Parameter.SupplierGroup) As String
        Dim SupplierGroupBO As ISupplierGroup
        SupplierGroupBO = ComponentFactory.CreateSupplierGroup()
        'Return SupplierGroupBO.DeleteSupplierGroup(ocustomClass)
        Return SupplierGroupBO.SupplierGroupDelete(ocustomClass)
    End Function
End Class

