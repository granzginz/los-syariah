

#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class NegativeCustomerController
    Function ListNegativeCustomer(ByVal customClass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
        Dim oCustomer As INegativeCustomer
        oCustomer = ComponentFactory.CreateNegativeCustomer()
        Return oCustomer.ListNegativeCustomer(customClass)
    End Function

    Function NegativeCustomerAdd(ByVal customClass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
        Dim oCustomer As INegativeCustomer
        oCustomer = ComponentFactory.CreateNegativeCustomer()
        Return oCustomer.NegativeCustomerAdd(customClass)
    End Function

    Function NegativeCustomerEdit(ByVal customClass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
        Dim oCustomer As INegativeCustomer
        oCustomer = ComponentFactory.CreateNegativeCustomer()
        Return oCustomer.NegativeCustomerEdit(customClass)
    End Function

    Function NegativeCustomerView(ByVal customClass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
        Dim oCustomer As INegativeCustomer
        oCustomer = ComponentFactory.CreateNegativeCustomer()
        Return oCustomer.NegativeCustomerView(customClass)
    End Function

    Function ListIDType(ByVal customClass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
        Dim oCustomer As INegativeCustomer
        oCustomer = ComponentFactory.CreateNegativeCustomer()
        Return oCustomer.ListIDType(customClass)
    End Function

    Function ListdataSource(ByVal customClass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
        Dim oCustomer As INegativeCustomer
        oCustomer = ComponentFactory.CreateNegativeCustomer()
        Return oCustomer.ListDataSource(customClass)
    End Function

    Function NegativeCustomerRpt(ByVal customClass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
        Dim oCustomer As INegativeCustomer
        oCustomer = ComponentFactory.CreateNegativeCustomer()
        Return oCustomer.NegativeCustomerRpt(customClass)
    End Function

    Function GetApprovalNumberForHistory(ByVal data As Parameter.NegativeCustomer) As String
        Dim oCustomer As INegativeCustomer
        oCustomer = ComponentFactory.CreateNegativeCustomer()
        Return oCustomer.GetApprovalNumberForHistory(data)
    End Function
    Public Function DoUploadNegativeCsv(cnn As String, BusinessDate As DateTime, branchid As String, params As DataTable) As String
        Dim oCustomer As INegativeCustomer
        oCustomer = ComponentFactory.CreateNegativeCustomer
        Return oCustomer.DoUploadNegativeCsv(cnn, BusinessDate, branchid, params)
    End Function
End Class
