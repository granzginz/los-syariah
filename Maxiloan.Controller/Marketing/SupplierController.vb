
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SupplierController
#Region "Supplier"
    Function GetSupplierReport(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierReport(customClass)
    End Function
    Function GetSupplier(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplier(customClass)
    End Function
    Function SupplierSaveAdd(ByVal oSupplier As Parameter.Supplier, _
     ByVal oAddress As Parameter.Address, ByVal oAddressNPWP As Parameter.Address) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.SupplierSaveAdd(oSupplier, oAddress, oAddressNPWP)
    End Function
    Function GetBranch(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetBranch(customClass)
    End Function
    Sub SupplierSaveBranch(ByVal oSupplier As Parameter.Supplier, ByVal oDataBranch As DataTable)
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        SupplierBO.SupplierSaveBranch(oSupplier, oDataBranch)
    End Sub
    Function DeleteSupplier(ByVal customClass As Parameter.Supplier) As String
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.DeleteSupplier(customClass)
    End Function
    Function SupplierEdit(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.SupplierEdit(customClass)
    End Function
    Sub SupplierSaveEdit(ByVal oSupplier As Parameter.Supplier, ByVal oAddress As Parameter.Address, ByVal oAddressNPWP As Parameter.Address)
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        SupplierBO.SupplierSaveEdit(oSupplier, oAddress, oAddressNPWP)
    End Sub
    Function SupplierSaveAddPrivate(ByVal oSupplier As Parameter.Supplier, ByVal oAddress As Parameter.Address) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.SupplierSaveAddPrivate(oSupplier, oAddress)
    End Function
    Function GetProduk(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetProduk(customClass)
    End Function

    Function GetSupplierID(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierID(customClass)
    End Function
    Function GetNoPKS(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetNoPKS(customClass)
    End Function
#End Region
#Region "SupplierBranch"
    Function GetSupplierBranchAdd(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierBranchAdd(customClass)
    End Function
    Function GetSupplierBranch(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierBranch(customClass)
    End Function
    Function GetSupplierBranchIncentiveCard(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierBranchIncentiveCard(customClass)
    End Function
    Function GetSupplierBranchEdit(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierBranchEdit(customClass)
    End Function
    Sub SupplierBranchSaveEdit(ByVal oSupplier As Parameter.Supplier)
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        SupplierBO.SupplierBranchSaveEdit(oSupplier)
    End Sub
    Function GetSupplierBranchDelete(ByVal oSupplier As Parameter.Supplier) As String
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierBranchDelete(oSupplier)
    End Function
    Function GetSupplierEmployeePosition(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier
        Return SupplierBO.GetSupplierEmployeePosition(customClass)
    End Function

    Function GetSupplierBranchRefund(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier
        Return SupplierBO.GetSupplierBranchRefund(customClass)
    End Function

    Sub SupplierBranchRefundHeaderSaveEdit(ByVal oSupplier As Parameter.Supplier)
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        SupplierBO.SupplierBranchRefundHeaderSaveEdit(oSupplier)
    End Sub

    Sub SupplierBranchRefundSaveEdit(ByVal oSupplier As Parameter.Supplier)
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        SupplierBO.SupplierBranchRefundSaveEdit(oSupplier)
    End Sub
#End Region
#Region "SupplierOwner"
    Function GetSupplierOwner(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierOwner(customClass)
    End Function
    Function GetSupplierOwnerIDType(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierOwnerIDType(customClass)
    End Function
    Function SupplierOwnerSaveAdd(ByVal oSupplier As Parameter.Supplier, _
    ByVal oAddress As Parameter.Address) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.SupplierOwnerSaveAdd(oSupplier, oAddress)
    End Function
    Function GetSupplierOwnerEdit(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierOwnerEdit(customClass)
    End Function
    Function GetSupplierOwnerDetele(ByVal customClass As Parameter.Supplier) As String
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierOwnerDetele(customClass)
    End Function
    Function GetSupplierOwnerView(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierOwnerView(customClass)
    End Function
    'Function GetSupplierOwnerID(ByVal customClass As Parameter.Supplier) As String
    '    Dim SupplierBO As ISupplier
    '    SupplierBO = ComponentFactory.CreateSupplier()
    '    Return SupplierBO.GetSupplierOwnerID(customClass)
    'End Function
#End Region
#Region "SupplierAccount"
    Function GetSupplierAccount(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierAccount(customClass)
    End Function
    Function SupplierAccountSave(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.SupplierAccountSave(customClass)
    End Function
    Function SupplierAccountDelete(ByVal customClass As Parameter.Supplier) As String
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.SupplierAccountDelete(customClass)
    End Function
#End Region
#Region "SupplierSignature"
    Function GetSupplierSignature(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierSignature(customClass)
    End Function
    Sub SupplierSignatureSaveAdd(ByVal customClass As Parameter.Supplier)
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        SupplierBO.SupplierSignatureSaveAdd(customClass)
    End Sub
    Sub SupplierSignatureSaveEdit(ByVal customClass As Parameter.Supplier)
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        SupplierBO.SupplierSignatureSaveEdit(customClass)
    End Sub
    Function SupplierSignatureDelete(ByVal customClass As Parameter.Supplier) As String
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.SupplierSignatureDelete(customClass)
    End Function
    Function SupplierSignaturePath(ByVal customClass As Parameter.Supplier) As String
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.SupplierSignaturePath(customClass)
    End Function
    Function SupplierSignatureGetSequenceNo(ByVal SupplierID As String, ByVal strConnection As String) As String
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.SupplierSignatureGetSequenceNo(SupplierID, strConnection)
    End Function
#End Region
#Region "SupplierEmployee"
    Function GetSupplierEmployee(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierEmployee(customClass)
    End Function
    Function SupplierEmployeeSave(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.SupplierEmployeeSave(customClass)
    End Function
    Function SupplierEmployeeDelete(ByVal customClass As Parameter.Supplier) As String
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.SupplierEmployeeDelete(customClass)
    End Function
#End Region
#Region "Supplier Budget"
    Function AddSupplierBudget(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBU As ISupplier
        SupplierBU = ComponentFactory.CreateSupplier()
        Return SupplierBU.AddSupplierBudget(customClass)
    End Function

    Function EditSupplierBudget(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim EditSB As ISupplier
        EditSB = ComponentFactory.CreateSupplier()
        Return EditSB.EditSupplierBudget(customClass)
    End Function
#End Region

#Region "Supplier Forecast"
    Function AddSupplierForecast(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBF As ISupplier
        SupplierBF = ComponentFactory.CreateSupplier()
        Return SupplierBF.AddSupplierForecast(customClass)
    End Function

    Function EditSupplierForecast(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim EditSF As ISupplier
        EditSF = ComponentFactory.CreateSupplier()
        Return EditSF.EditSupplierForecast(customClass)
    End Function
#End Region
#Region "SupplierView"
    Public Function SupplierView(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim ViewSupplier As ISupplier
        ViewSupplier = ComponentFactory.CreateSupplier()
        Return ViewSupplier.SupplierView(customClass)
    End Function
    Public Function SupplierViewBudgetForecast(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim ViewSupplier As ISupplier
        ViewSupplier = ComponentFactory.CreateSupplier()
        Return ViewSupplier.SupplierViewBudgetForecast(customClass)
    End Function
#End Region
#Region "SupplierLevelStatus"
    Function SupplierLevelStatus(ByVal strconnection As String) As DataTable
        Dim GetLevelStatus As ISupplier
        GetLevelStatus = ComponentFactory.CreateSupplier
        Return GetLevelStatus.SupplierLevelStatus(strconnection)
    End Function
#End Region
#Region "GetSupplier Account bySupplierID"
    Function GetSupplierAccountbySupplierID(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierAccountbySupplierID(customClass)
    End Function
#End Region

#Region "SupplierRelasi"
    Public Function GetSupplierEmployeeSPV(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierEmployeeSPV(customClass)
    End Function
    Public Function GetSupplierEmployeeSales(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierEmployeeSales(customClass)
    End Function
    Public Function SaveSupplierEmployeeRelasi(ByVal customClass As Parameter.Supplier, ByVal tblEmployee As DataTable) As String
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.SaveSupplierEmployeeRelasi(customClass, tblEmployee)
    End Function
    Public Function GetSupplierEmployeeSPVBySales(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.GetSupplierEmployeeSPVBySales(customClass)
    End Function
    Function SupplierAccountSave2(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
        Dim SupplierBO As ISupplier
        SupplierBO = ComponentFactory.CreateSupplier()
        Return SupplierBO.SupplierAccountSave2(customClass)
    End Function
#End Region
End Class
