Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class ProspectResultAppraiserController

    Public Function GetProspect(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.GetProspect(oCustomClass)
    End Function

    Public Function ProspectSaveAdd(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.ProspectSaveAdd(oCustomClass)
    End Function

    Public Function ProspectSaveAsset(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.ProspectSaveAsset(oCustomClass)
    End Function

    Public Function ProspectSaveDemografi(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.ProspectSaveDemografi(oCustomClass)
    End Function

    Public Function ProspectSaveFinancial(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.ProspectSaveFinancial(oCustomClass)
    End Function

    Public Function GetViewProspect(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.GetViewProspect(oCustomClass)
    End Function

    Public Function GetCbo(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.GetCbo(oCustomClass)
    End Function

    Public Function GetCboGeneral(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.GetCboGeneral(oCustomClass)
    End Function
    Function DoProceeded(ByVal cnn As String, prospectId As String) As Boolean
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.DoProceeded(cnn, prospectId)
    End Function
    Function DoProspectCreaditScoring(cnn As String, branchid As String, prospectAppId As String, ScoringType As String, bnsDate As Date) As String
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.DoProspectCreaditScoring(cnn, branchid, prospectAppId, ScoringType, bnsDate)
    End Function

    Function DoProspectDecision(cnn As String, prospectAppId As String, appr As Integer)
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.DoProspectDecision(cnn, prospectAppId, appr)
    End Function
    Function DispositionCreditRpt(cnn As String, whereCond As String(), tp As String) As DataSet
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.DispositionCreditRpt(cnn, whereCond, tp)
    End Function
    Public Function GetProspectScorePolicyResult(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.GetProspectScorePolicyResult(oCustomClass)
    End Function

    Function MasterDataReviewRpt(cnn As String, whereCond As String(), tp As String) As DataSet
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.MasterDataReviewRpt(cnn, whereCond, tp)
    End Function

    Public Function GetInitial(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.GetInitial(oCustomClass)
    End Function

    Public Function InitialSaveAdd(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.InitialSaveAdd(oCustomClass)
    End Function

    Function DoBIChecking(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.DoBIChecking(oCustomClass)
    End Function

    Public Function DataSurveySaveAdd(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.DataSurveySaveAdd(oCustomClass)
    End Function

    Public Function DataSurveySaveEdit(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.DataSurveySaveEdit(oCustomClass)
    End Function

    Public Function ProspectReturnUpdate(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As String
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.ProspectReturnUpdate(oCustomClass)
    End Function

    Public Function ProspectLogSave(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.ProspectLogSave(oCustomClass)
    End Function

    Public Function GetInqProspect(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.GetInqProspect(oCustomClass)
    End Function

    Function DoProspectCreaditScoringProceed(cnn As String, branchid As String, prospectAppId As String, ScoringType As String, bnsDate As Date, proceed As Boolean) As String
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.DoProspectCreaditScoringProceed(cnn, branchid, prospectAppId, ScoringType, bnsDate, proceed)
    End Function

    Function GetApprovalprospect(ByVal oCustomClass As Parameter.ProspectResultAppraiser, ByVal strApproval As String)
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.GetApprovalprospect(oCustomClass, strApproval)
    End Function

    Function SaveProspectAssignAppraisal(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
        Dim BO As IProspectResultAppraiser
        BO = ComponentFactory.CreateProspectResultAppraiser()
        Return BO.SaveProspectAssignAppraisal(oCustomClass)
    End Function
End Class
