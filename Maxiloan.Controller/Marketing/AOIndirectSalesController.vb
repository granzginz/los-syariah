
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AOIndirectSalesController
    Public Function AOIndirectListing(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
        Dim oAOListing As IAOIndirectSales
        oAOListing = ComponentFactory.CreateAOInDirectSales
        Return oAOListing.AOIndirectListing(customclass)
    End Function
    Public Function AOBudgetListing(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
        Dim oAOBudget As IAOIndirectSales
        oAOBudget = ComponentFactory.CreateAOInDirectSales
        Return oAOBudget.AOBudgetListing(customclass)
    End Function

    Public Function AOBudgetView(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
        Dim oAOBudgetView As IAOIndirectSales
        oAOBudgetView = ComponentFactory.CreateAOInDirectSales
        Return oAOBudgetView.AOBudgetView(customclass)
    End Function
    Public Function AOBudgetPrint(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
        Dim oAOBudgetPrint As IAOIndirectSales
        oAOBudgetPrint = ComponentFactory.CreateAOInDirectSales
        Return oAOBudgetPrint.AOBudgetPrint(customclass)
    End Function
    Public Function AOForecastListing(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
        Dim oAOForecast As IAOIndirectSales
        oAOForecast = ComponentFactory.CreateAOInDirectSales
        Return oAOForecast.AOForecastListing(customclass)
    End Function
    Public Function AOForecastView(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
        Dim oAOForecastView As IAOIndirectSales
        oAOForecastView = ComponentFactory.CreateAOInDirectSales
        Return oAOForecastView.AOForecastView(customclass)
    End Function
    Public Function AOForecastPrint(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
        Dim oAOForecastPrint As IAOIndirectSales
        oAOForecastPrint = ComponentFactory.CreateAOInDirectSales
        Return oAOForecastPrint.AOForecastPrint(customclass)
    End Function
End Class
