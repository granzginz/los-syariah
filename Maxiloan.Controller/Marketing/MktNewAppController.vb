
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class MktNewAppController
    Public Function MktNewAppPaging(ByVal oCustomClass As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim oMktNewApp As IMktNewApp
        oMktNewApp = ComponentFactory.MktNewApp
        Return oMktNewApp.MktNewAppPaging(oCustomClass)
    End Function
    Public Function MktNewAppView(ByVal oCustomClass As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim omktnewapp As IMktNewApp
        omktnewapp = ComponentFactory.MktNewApp
        Return omktnewapp.MktNewAppView(oCustomClass)
    End Function
    Public Function GetPersonalIdType(ByVal oCustomClass As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim omktnewapp As IMktNewApp
        omktnewapp = ComponentFactory.MktNewApp
        Return omktnewapp.GetPersonalIdType(oCustomClass)
    End Function
    Public Function GetPersonalHomeStatus(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim omktnewapp As IMktNewApp
        omktnewapp = ComponentFactory.MktNewApp
        Return omktnewapp.GetPersonalHomeStatus(ocustomclass)
    End Function
    Public Function GetProfession(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim omktnewapp As IMktNewApp
        omktnewapp = ComponentFactory.MktNewApp
        Return omktnewapp.GetProfession(ocustomclass)
    End Function
    Sub Generate_XML(ByVal pStrFile As String, _
                         ByVal oCustomClass As Parameter.MktNewApp)
        Dim CustomerBO As IMktNewApp
        CustomerBO = ComponentFactory.MktNewApp
        CustomerBO.Generate_XML(pStrFile, oCustomClass)
    End Sub
    Public Function BindCustomer1_002(ByVal Customclass As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim omktnewapp As IMktNewApp
        omktnewapp = ComponentFactory.MktNewApp
        Return omktnewapp.BindCustomer1_002(Customclass)
    End Function
    Public Function BindCustomer2_002(ByVal Customclass As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim omktnewapp As IMktNewApp
        omktnewapp = ComponentFactory.MktNewApp
        Return omktnewapp.BindCustomer2_002(Customclass)
    End Function
    Public Function BindCustomerC1_002(ByVal oCustomer As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim omktnewapp As IMktNewApp
        omktnewapp = ComponentFactory.MktNewApp
        Return omktnewapp.BindCustomerC1_002(oCustomer)
    End Function
    Public Function BindCustomerC2_002(ByVal oCustomer As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim omktnewapp As IMktNewApp
        omktnewapp = ComponentFactory.MktNewApp
        Return omktnewapp.BindCustomerC2_002(oCustomer)
    End Function
    Public Sub MktNewAppSave(ByVal oCustomClass As Parameter.MktNewApp)
        Dim omktnewapp As IMktNewApp
        omktnewapp = ComponentFactory.MktNewApp
        omktnewapp.MktNewAppSave(oCustomClass)
    End Sub
    Public Sub MktNewAppEdit(ByVal oCustomClass As Parameter.MktNewApp)
        Dim omktnewapp As IMktNewApp
        omktnewapp = ComponentFactory.MktNewApp
        omktnewapp.MktNewAppEdit(oCustomClass)
    End Sub
    Public Function GetAssetDescription(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim omktnewapp As IMktNewApp
        omktnewapp = ComponentFactory.MktNewApp
        Return omktnewapp.GetAssetDescription(ocustomclass)
    End Function
    Public Function GetInsuranceCo(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim omktnewapp As IMktNewApp
        omktnewapp = ComponentFactory.MktNewApp
        Return omktnewapp.GetInsuranceCo(ocustomclass)
    End Function
    Public Function GetIndustryType(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim omktnewapp As IMktNewApp
        omktnewapp = ComponentFactory.MktNewApp
        Return omktnewapp.GetIndustryType(ocustomclass)
    End Function
    Public Function GetCompanyStatus(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim omktnewapp As IMktNewApp
        omktnewapp = ComponentFactory.MktNewApp
        Return omktnewapp.GetCompanyStatus(ocustomclass)
    End Function
    Public Sub MktNewAppSaveCompany(ByVal oCustomClass As Parameter.MktNewApp)
        Dim omktnewapp As IMktNewApp
        omktnewapp = ComponentFactory.MktNewApp
        omktnewapp.MktNewAppSaveCompany(oCustomClass)
    End Sub
    Public Function MktNewAppViewCompany(ByVal oCustomClass As Parameter.MktNewApp) As Parameter.MktNewApp
        Dim omktnewapp As IMktNewApp
        omktnewapp = ComponentFactory.MktNewApp
        Return omktnewapp.MktNewAppViewCompany(oCustomClass)
    End Function
    Public Sub MktNewAppEditCompany(ByVal oCustomClass As Parameter.MktNewApp)
        Dim omktnewapp As IMktNewApp
        omktnewapp = ComponentFactory.MktNewApp
        omktnewapp.MktNewAppEditCompany(oCustomClass)
    End Sub
End Class
