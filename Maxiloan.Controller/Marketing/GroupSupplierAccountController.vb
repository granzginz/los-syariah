
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class GroupSupplierAccountController
    Public Function GetGroupSupplierAccount(ByVal ocustomClass As Parameter.GroupSupplierAccount) As Parameter.GroupSupplierAccount
        Dim GroupSupplierAccountBO As IGroupSupplierAccount
        GroupSupplierAccountBO = ComponentFactory.CreateGroupSupplierAccount()
        Return GroupSupplierAccountBO.GetGroupSupplierAccount(ocustomClass)
    End Function
    Public Function GetGroupSupplierAccountReport(ByVal ocustomClass As Parameter.GroupSupplierAccount) As Parameter.GroupSupplierAccount
        Dim GroupSupplierAccountBO As IGroupSupplierAccount
        GroupSupplierAccountBO = ComponentFactory.CreateGroupSupplierAccount()
        Return GroupSupplierAccountBO.GetGroupSupplierAccountReport(ocustomClass)
    End Function
    Public Function GetGroupSupplierAccountList(ByVal ocustomClass As Parameter.GroupSupplierAccount) As Parameter.GroupSupplierAccount
        Dim GroupSupplierAccountBO As IGroupSupplierAccount
        GroupSupplierAccountBO = ComponentFactory.CreateGroupSupplierAccount()
        Return GroupSupplierAccountBO.GetGroupSupplierAccountList(ocustomClass)
    End Function
    Public Function GroupSupplierAccountSaveAdd(ByVal ocustomClass As Parameter.GroupSupplierAccount) As String
        Dim GroupSupplierAccountBO As IGroupSupplierAccount
        GroupSupplierAccountBO = ComponentFactory.CreateGroupSupplierAccount()
        Return GroupSupplierAccountBO.GroupSupplierAccountSaveAdd(ocustomClass)
    End Function

    Public Sub GroupSupplierAccountSaveEdit(ByVal ocustomClass As Parameter.GroupSupplierAccount)
        Dim GroupSupplierAccountBO As IGroupSupplierAccount
        GroupSupplierAccountBO = ComponentFactory.CreateGroupSupplierAccount()
        GroupSupplierAccountBO.GroupSupplierAccountSaveEdit(ocustomClass)
    End Sub

    Public Function GroupSupplierAccountDelete(ByVal ocustomClass As Parameter.GroupSupplierAccount) As String
        Dim GroupSupplierAccountBO As IGroupSupplierAccount
        GroupSupplierAccountBO = ComponentFactory.CreateGroupSupplierAccount()
        Return GroupSupplierAccountBO.GroupSupplierAccountDelete(ocustomClass)
    End Function

End Class

