﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class JFCriteriaController
    Public Function GetJFCriteria(ByVal ocustomClass As Parameter.JFCriteria) As Parameter.JFCriteria
        Dim JFCriteriaBO As IJFCriteria
        JFCriteriaBO = ComponentFactory.CreateJFCriteria()
        Return JFCriteriaBO.GetJFCriteria(ocustomClass)
    End Function

    Public Function GetJFCriteriaEdit(ByVal ocustomClass As Parameter.JFCriteria) As Parameter.JFCriteria
        Dim JFCriteriaBO As IJFCriteria
        JFCriteriaBO = ComponentFactory.CreateJFCriteria()
        Return JFCriteriaBO.GetJFCriteriaEdit(ocustomClass)
    End Function

    Public Function JFCriteriaAdd(ByVal ocustomClass As Parameter.JFCriteria) As String
        Dim JFCriteriaBO As IJFCriteria
        JFCriteriaBO = ComponentFactory.CreateJFCriteria()
        Return JFCriteriaBO.JFCriteriaSaveAdd(ocustomClass)
    End Function

    Public Sub JFCriteriaUpdate(ByVal ocustomClass As Parameter.JFCriteria)
        Dim JFCriteriaBO As IJFCriteria
        JFCriteriaBO = ComponentFactory.CreateJFCriteria()
        JFCriteriaBO.JFCriteriaSaveEdit(ocustomClass)
    End Sub

    Public Function JFCriteriaDelete(ByVal ocustomClass As Parameter.JFCriteria) As String
        Dim JFCriteriaBO As IJFCriteria
        JFCriteriaBO = ComponentFactory.CreateJFCriteria()
        Return JFCriteriaBO.JFCriteriaDelete(ocustomClass)
    End Function

    Public Function GetSelectJFCriteria(ByVal ocustomClass As Parameter.JFCriteria) As Parameter.JFCriteria
        Dim JFCriteriaBO As IJFCriteria
        JFCriteriaBO = ComponentFactory.CreateJFCriteria()
        Return JFCriteriaBO.GetSelectJFCriteria(ocustomClass)
    End Function
End Class
