﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class JFProductController
#Region "1"


    'Function JFListProduct(ByVal product As Parameter.JFProduct) As Parameter.JFProduct
    '    Dim oProduct As IJFProduct
    '    oProduct = ComponentFactory.CreateJFProduct()
    '    Return oProduct.JFListProduct(product)
    'End Function

    'Public Sub JFDeleteProduct(ByVal product As Parameter.JFProduct)
    '    Dim oProduct As IJFProduct
    '    oProduct = ComponentFactory.CreateJFProduct()
    '    oProduct.JFDeleteProduct(product)
    'End Sub

    'Public Sub JFAddProduct(ByVal product As Parameter.JFProduct, ByVal oClassAddress As Parameter.Address)
    '    Dim oProduct As IJFProduct
    '    oProduct = ComponentFactory.CreateJFProduct()
    '    oProduct.JFAddProduct(product, oClassAddress)
    'End Sub

    'Public Sub JFEditProduct(ByVal product As Parameter.JFProduct, ByVal oClassAddress As Parameter.Address)
    '    Dim oProduct As IJFProduct
    '    oProduct = ComponentFactory.CreateJFProduct()
    '    oProduct.JFEditProduct(product, oClassAddress)
    'End Sub

    'Function JFListProductByID(ByVal product As Parameter.JFProduct) As Parameter.JFProduct
    '    Dim oProduct As IJFProduct
    '    oProduct = ComponentFactory.CreateJFProduct()
    '    Return oProduct.JFListProductByID(product)
    'End Function
#End Region
    Public Function GetJFProduct(ByVal ocustomClass As Parameter.JFProduct) As Parameter.JFProduct
        Dim JFProductBO As IJFProduct
        JFProductBO = ComponentFactory.CreateJFProduct()
        Return JFProductBO.GetJFProduct(ocustomClass)
    End Function
    'Public Function GetReferalReport(ByVal ocustomClass As Parameter.JFProduct) As Parameter.JFProduct
    '    Dim JFProductBO As IJFProduct
    '    JFProductBO = ComponentFactory.CreateJFProduct()
    '    Return JFProductBO.GetJFProductReport(ocustomClass)
    'End Function
    'Public Function Getsector(ByVal ocustomClass As Parameter.JFProduct) As Parameter.JFProduct
    '    Dim JFProductBO As IJFProduct
    '    JFProductBO = ComponentFactory.CreateJFProduct()
    '    Return JFProductBO.GetJFProductSector(ocustomClass)
    'End Function
    Public Function GetJFProductEdit(ByVal ocustomClass As Parameter.JFProduct) As Parameter.JFProduct
        Dim JFProductBO As IJFProduct
        JFProductBO = ComponentFactory.CreateJFProduct()
        Return JFProductBO.GetJFProductEdit(ocustomClass)
    End Function

    Public Function JFProductAdd(ByVal ocustomClass As Parameter.JFProduct) As String
        Dim JFProductBO As IJFProduct
        JFProductBO = ComponentFactory.CreateJFProduct()
        Return JFProductBO.JFProductSaveAdd(ocustomClass)
    End Function

    Public Sub JFProductUpdate(ByVal ocustomClass As Parameter.JFProduct)
        Dim JFProductBO As IJFProduct
        JFProductBO = ComponentFactory.CreateJFProduct()
        JFProductBO.JFProductSaveEdit(ocustomClass)
    End Sub

    Public Function JFProductDelete(ByVal ocustomClass As Parameter.JFProduct) As String
        Dim JFProductBO As IJFProduct
        JFProductBO = ComponentFactory.CreateJFProduct()
        Return JFProductBO.JFProductDelete(ocustomClass)
    End Function
    Public Function GetSelectJFProduct(ByVal ocustomClass As Parameter.JFProduct) As Parameter.JFProduct
        Dim JFProductBO As IJFProduct
        JFProductBO = ComponentFactory.CreateJFProduct()
        Return JFProductBO.GetSelectJFProduct(ocustomClass)
    End Function


End Class
