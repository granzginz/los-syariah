﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class JFDocumentController
    Public Function GetJFDocument(ByVal ocustomClass As Parameter.JFDocument) As Parameter.JFDocument
        Dim JFDocumentBO As IJFDocument
        JFDocumentBO = ComponentFactory.CreateJFDocument()
        Return JFDocumentBO.GetJFDocument(ocustomClass)
    End Function

    Public Function GetJFDocumentEdit(ByVal ocustomClass As Parameter.JFDocument) As Parameter.JFDocument
        Dim JFDocumentBO As IJFDocument
        JFDocumentBO = ComponentFactory.CreateJFDocument()
        Return JFDocumentBO.GetJFDocumentEdit(ocustomClass)
    End Function
    Public Function JFDocumentAdd(ByVal ocustomClass As Parameter.JFDocument) As String
        Dim JFDocumentBO As IJFDocument
        JFDocumentBO = ComponentFactory.CreateJFDocument()
        Return JFDocumentBO.JFDocumentSaveAdd(ocustomClass)
    End Function
    Public Sub JFDocumentUpdate(ByVal ocustomClass As Parameter.JFDocument)
        Dim JFDocumentBO As IJFDocument
        JFDocumentBO = ComponentFactory.CreateJFDocument()
        JFDocumentBO.JFDocumentSaveEdit(ocustomClass)
    End Sub
    Public Function JFDocumentDelete(ByVal ocustomClass As Parameter.JFDocument) As String
        Dim JFDocumentBO As IJFDocument
        JFDocumentBO = ComponentFactory.CreateJFDocument()
        Return JFDocumentBO.JFDocumentDelete(ocustomClass)
    End Function
    Public Function GetSelectJFDocument(ByVal ocustomClass As Parameter.JFDocument) As Parameter.JFDocument
        Dim JFDocumentBO As IJFDocument
        JFDocumentBO = ComponentFactory.CreateJFDocument()
        Return JFDocumentBO.GetSelectJFDocument(ocustomClass)
    End Function
End Class
