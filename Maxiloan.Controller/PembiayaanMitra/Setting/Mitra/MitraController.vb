﻿

#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class MitraController

    Function ListMitra(ByVal mitra As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.MitraList(mitra)
    End Function

    Function DeleteMitra(ByVal mitra As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        oMitraMultifinance.MitraDelete(mitra)
    End Function

    Public Sub AddMitra(ByVal mitra As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address)
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        oMitraMultifinance.MitraAdd(mitra, oClassAddress)
    End Sub

    Function EditMitra(ByVal mitra As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address) As Parameter.JFReference
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        oMitraMultifinance.MitraEdit(mitra, oClassAddress)
    End Function

    Function ListMitraByID(ByVal mitra As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.MitraListByID(mitra)
    End Function

    Function ListMitraReport(ByVal mitra As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.MitraReport(mitra)
    End Function

    Function ListRekeningMitra(ByVal mitra As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.RekeningMitraList(mitra)
    End Function
    Function DeleteRekeningMitra(ByVal mitra As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        oMitraMultifinance.RekeningMitraDelete(mitra)
    End Function

    Public Sub AddRekeningMitra(ByVal mitra As Parameter.MitraMultifinance)
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        oMitraMultifinance.RekeningMitraAdd(mitra)
    End Sub

    Function EditRekeningMitra(ByVal mitra As Parameter.MitraMultifinance) As Parameter.JFReference
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        oMitraMultifinance.RekeningMitraEdit(mitra)
    End Function

    Function ListRekeningMitraByID(ByVal mitra As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.RekeningMitraListByID(mitra)
    End Function
End Class
