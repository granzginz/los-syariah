﻿

#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class JFReferenceController

    Function JFReferenceList(ByVal jfref As Parameter.JFReference) As Parameter.JFReference
        Dim oJFReference As IJFReference
        oJFReference = ComponentFactory.CreateJFReference()
        Return oJFReference.JFReferenceList(jfref)
    End Function

    Function JFReferenceDelete(ByVal jfref As Parameter.JFReference) As Parameter.JFReference
        Dim oJFReference As IJFReference
        oJFReference = ComponentFactory.CreateJFReference()
        oJFReference.JFReferenceDelete(jfref)
    End Function

    Public Sub JFReferenceAdd(ByVal jfref As Parameter.JFReference)
        Dim oJFReference As IJFReference
        oJFReference = ComponentFactory.CreateJFReference()
        oJFReference.JFReferenceAdd(jfref)
    End Sub

    Function JFReferenceEdit(ByVal jfref As Parameter.JFReference) As Parameter.JFReference
        Dim oJFReference As IJFReference
        oJFReference = ComponentFactory.CreateJFReference()
        oJFReference.JFReferenceEdit(jfref)
    End Function

    Function JFReferenceListByID(ByVal jfref As Parameter.JFReference) As Parameter.JFReference
        Dim oJFReference As IJFReference
        oJFReference = ComponentFactory.CreateJFReference()
        Return oJFReference.JFReferenceListByID(jfref)
    End Function

End Class
