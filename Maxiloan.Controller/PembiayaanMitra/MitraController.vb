﻿

#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class MitraController

    Function ListMitra(ByVal mitra As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.MitraList(mitra)
    End Function

    Function DeleteMitra(ByVal mitra As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        oMitraMultifinance.MitraDelete(mitra)
    End Function

    Public Sub AddMitra(ByVal mitra As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address)
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        oMitraMultifinance.MitraAdd(mitra, oClassAddress)
    End Sub

    Function EditMitra(ByVal mitra As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address) As Parameter.JFReference
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        oMitraMultifinance.MitraEdit(mitra, oClassAddress)
    End Function

    Function ListMitraByID(ByVal mitra As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.MitraListByID(mitra)
    End Function

    Function LendingPaymentSave(ByVal ocls As Parameter.Lending) As Parameter.Lending
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.LendingPament(ocls)
    End Function

    Function ListMitraReport(ByVal mitra As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.MitraReport(mitra)
    End Function

    Function ListRekeningMitra(ByVal mitra As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.RekeningMitraList(mitra)
    End Function
    Function DeleteRekeningMitra(ByVal mitra As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        oMitraMultifinance.RekeningMitraDelete(mitra)
    End Function

    Public Sub AddRekeningMitra(ByVal mitra As Parameter.MitraMultifinance)
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        oMitraMultifinance.RekeningMitraAdd(mitra)
    End Sub

    Function EditRekeningMitra(ByVal mitra As Parameter.MitraMultifinance) As Parameter.JFReference
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        oMitraMultifinance.RekeningMitraEdit(mitra)
    End Function

    Function PaymentByDueListPaging(ByVal customclass As Parameter.Lending) As Parameter.Lending
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.PaymentByDueDateListPaging(customclass)
    End Function

    Function ListRekeningMitraByID(ByVal mitra As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.RekeningMitraListByID(mitra)
    End Function

    Function uploadlist(ByVal ocls As Parameter.Lending) As Parameter.Lending
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.uploadlist(ocls)
    End Function

    Function batchlist(ByVal ocls As Parameter.Lending) As Parameter.Lending
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.batchlist(ocls)
    End Function

    Function lendingdrawdownrequest(ByVal ocls As Parameter.Lending) As Parameter.Lending
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.lendingdrawdownrequest(ocls)
    End Function

    Function lendingdrawdownapproval(ByVal ocls As Parameter.Lending) As Parameter.Lending
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.lendingdrawdownapproval(ocls)
    End Function

    Function disburselist(ByVal ocls As Parameter.Lending) As Parameter.Lending
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.disburselist(ocls)
    End Function
    Function disburse(ByVal ocls As Parameter.Lending) As Parameter.Lending
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.disburse(ocls)
    End Function

    Public Function DisbursementHeaderList(ByVal strConnection As String) As DataTable
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.DisbursementHeaderList(strConnection)
    End Function

    Public Function LendingValidationList(ByVal strConnection As String) As List(Of Parameter.AgreementToValidate)
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.LendingValidationList(strConnection)
    End Function

    Public Function DisburseAgreementApproveReject(ByVal oDisburseAgreement As Parameter.DisburseAgreement) As Boolean
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.DisburseAgreementApproveReject(oDisburseAgreement)
    End Function

    Public Function DisburseHeaderUpdate(ByVal oDisburseAgreement As Parameter.DisburseAgreement) As Boolean
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.DisburseHeaderUpdate(oDisburseAgreement)
    End Function

    Public Function GetLendingFacilityByCode(ByVal mfCode As String, ByVal facilityNo As String, ByVal strConnection As String) As Parameter.LendingFacility
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.GetLendingFacilityByCode(mfCode, facilityNo, strConnection)
    End Function

    Function lendingRateListPaging(ByVal customclass As Parameter.LendingRateProperty) As Parameter.LendingRateProperty
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.lendingRateListPaging(customclass)
    End Function

    Function lendingBatchListPaging(ByVal customclass As Parameter.Lending) As Parameter.Lending
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.lendingBatchListPaging(customclass)
    End Function

    Function DraftSoftcopy(ByVal customClass As Parameter.Lending) As DataSet
        Dim oCompany As IMitraMultifinance
        oCompany = ComponentFactory.CreateMitraMultifinance()
        Return oCompany.DraftSoftcopy(customClass)
    End Function

    Sub lendingRateDML(ByVal dml As String, ByVal customclass As Parameter.LendingRateProperty)
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        oMitraMultifinance.lendingRateDML(dml, customclass)
    End Sub

    Function paymentCalculate(ByVal customclass As Parameter.PaymentCalculateProperty) As Parameter.PaymentCalculateProperty
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.paymentCalculate(customclass)
    End Function

    Sub paymentCalculateDML(ByVal customclass As Parameter.PaymentCalculateProperty)
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        oMitraMultifinance.paymentCalculateDML(customclass)
    End Sub

    Sub lendingFacilityDML(ByVal dml As String, ByVal customclass As Parameter.LendingFacility)
        Dim tempInterface As IMitraMultifinance
        tempInterface = ComponentFactory.CreateMitraMultifinance()
        tempInterface.lendingFacilityDML(dml, customclass)
    End Sub
    Function lendingpayment(ByVal customclass As Parameter.Lending) As Parameter.Lending
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.lendingpayment(customclass)
    End Function
    Function lendinginstallmentinfo(ByVal customclass As Parameter.Lending) As Parameter.Lending
        Dim oMitraMultifinance As IMitraMultifinance
        oMitraMultifinance = ComponentFactory.CreateMitraMultifinance()
        Return oMitraMultifinance.lendinginstallmentinfo(customclass)
    End Function
End Class
