﻿'Imports Maxiloan.Interface.Lending
'Imports Maxiloan.Parameter.Lending
'Imports Maxiloan.BusinessProcess.Lending
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface
Imports Maxiloan.Parameter

Public Class LendingProcessController
    Private oInterface As ILendingProcess

    Public Sub New()
        oInterface = ComponentFactory.CreateLandingProcessApp()
    End Sub


    Public Function ProcessUploadCsvLending(cnn As String, data As Dictionary(Of String, IList(Of AbsDisb))) As String
        Return oInterface.ProcessUploadCsvLending(cnn, data)
    End Function
End Class
