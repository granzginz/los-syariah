
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class UCExposureController
    Public Function GetAgreeExposure(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim AEx As IExposure
        AEx = ComponentFactory.CreateExposure
        Return AEx.GetAgreeExposure(oCustomClass)
    End Function

    Public Function GetListAgree(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim AList As IExposure
        AList = ComponentFactory.CreateExposure
        Return AList.GetListAgree(oCustomClass)
    End Function

    Public Function GetCustExposure(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim AEx As IExposure
        AEx = ComponentFactory.CreateExposure
        Return AEx.GetCustExposure(oCustomClass)
    End Function
End Class
