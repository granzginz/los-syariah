
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Public Class UcAgreementListController
    Public Function UcAgreementList(ByVal oCustomClass As Parameter.AccMntBase) As DataTable
        Dim oUcAgreementList As IAgreementList
        oUcAgreementList = ComponentFactory.CreateUcAgreementListExtend()
        Return oUcAgreementList.UcAgreementList(oCustomClass)
    End Function

    Public Function UcAgreementListFactoring(ByVal oCustomClass As Parameter.AccMntBase) As DataTable
        Dim oUcAgreementList As IAgreementList
        oUcAgreementList = ComponentFactory.CreateUcAgreementListExtend()
        Return oUcAgreementList.UcAgreementListFactoring(oCustomClass)
    End Function

    Public Function UcAgreementListMdKj(ByVal oCustomClass As Parameter.AccMntBase) As DataTable
        Dim oUcAgreementList As IAgreementList
        oUcAgreementList = ComponentFactory.CreateUcAgreementListExtend()
        Return oUcAgreementList.UcAgreementListMdKj(oCustomClass)
    End Function

    Public Function UcAgreementListInv(ByVal oCustomClass As Parameter.AccMntBase) As DataTable
        Dim oUcAgreementList As IAgreementList
        oUcAgreementList = ComponentFactory.CreateUcAgreementListExtend()
        Return oUcAgreementList.UcAgreementListInv(oCustomClass)
    End Function
End Class
