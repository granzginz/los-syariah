

Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class UCAssetListController
    Public Function UCAssetList(ByVal oCustomClass As Parameter.AccMntBase) As DataTable
        Dim oUCAssetList As IUCAssetList
        oUCAssetList = ComponentFactory.CreateUCAssetList()
        Return oUCAssetList.UCAssetList(oCustomClass)
    End Function
End Class
