
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class UCPaymentInfoController
    Public Function GetPaymentInfo(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim PaymentInfoBO As IPaymentInfo
        PaymentInfoBO = ComponentFactory.CreatePaymentInfo
        Return PaymentInfoBO.GetPaymentInfo(oCustomClass)
    End Function

    Public Function GetFullPrepaymentInfo(ByVal oCustomClass As Parameter.FullPrepay) As Parameter.FullPrepay
        Dim PaymentInfoBO As IPaymentInfo
        PaymentInfoBO = ComponentFactory.CreatePaymentInfo
        Return PaymentInfoBO.GetFullPrepaymentInfo(oCustomClass)
    End Function
    Public Function GetFullPrepaymentInfoNormal(ByVal oCustomClass As Parameter.FullPrepay) As Parameter.FullPrepay
        Dim PaymentInfoBO As IPaymentInfo
        PaymentInfoBO = ComponentFactory.CreatePaymentInfo
        Return PaymentInfoBO.GetFullPrepaymentInfoNormal(oCustomClass)
    End Function
    Public Function GetFullPrepaymentInfoOL(ByVal oCustomClass As Parameter.FullPrepay) As Parameter.FullPrepay
        Dim PaymentInfoBO As IPaymentInfo
        PaymentInfoBO = ComponentFactory.CreatePaymentInfo
        Return PaymentInfoBO.GetFullPrePaymentInfoOL(oCustomClass)
    End Function

    Public Function GetPaymentInfoOL(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim PaymentInfoBO As IPaymentInfo
        PaymentInfoBO = ComponentFactory.CreatePaymentInfo
        Return PaymentInfoBO.GetPaymentInfoOL(oCustomClass)
    End Function

    Public Function GetIsPaymentHaveOtor(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim PaymentInfoBO As IPaymentInfo
        PaymentInfoBO = ComponentFactory.CreatePaymentInfo
        Return PaymentInfoBO.GetIsPaymentHaveOtor(oCustomClass)
    End Function

    Public Function GetPaymentInfoFactAndMDKJ(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim PaymentInfoBO As IPaymentInfo
        PaymentInfoBO = ComponentFactory.CreatePaymentInfo
        Return PaymentInfoBO.GetPaymentInfoFactAndMDKJ(oCustomClass)
    End Function
    Public Function GetFullPrepaymentInfoFactAndMDKJ(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim PaymentInfoBO As IPaymentInfo
        PaymentInfoBO = ComponentFactory.CreatePaymentInfo
        Return PaymentInfoBO.GetFullPrepaymentInfoFactAndMDKJ(oCustomClass)
    End Function

    Public Function GetFullPrepaymentInfoView(ByVal oCustomClass As Parameter.FullPrepay) As Parameter.FullPrepay
        Dim PaymentInfoBO As IPaymentInfo
        PaymentInfoBO = ComponentFactory.CreatePaymentInfo
        Return PaymentInfoBO.GetFullPrepaymentInfoView(oCustomClass)
    End Function
End Class
