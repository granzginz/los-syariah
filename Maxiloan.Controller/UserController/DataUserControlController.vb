Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class DataUserControlController

    Public Function GetCoverageType(ByVal strconnection As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetCoverageType(strconnection)
    End Function
    Public Function GetApplicationType(ByVal strconnection As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetApplicationType(strconnection)
    End Function

    Public Function GetBankName(ByVal strConnection As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.getBankName(strConnection)
    End Function

    Public Overloads Function GetInsuranceBranchName(ByVal strConnection As String, ByVal strBranch As String) As DataTable
        Dim BO As IDataUserControl
        BO = ComponentFactory.CreateDataUserControl
        Return BO.GetInsuranceBranchName(strConnection, strBranch)
    End Function

    Public Overloads Function GetInsuranceBranchName(ByVal strConnection As String, ByVal strBranch As String, ByVal strLoginID As String) As DataTable
        Dim BO As IDataUserControl
        BO = ComponentFactory.CreateDataUserControl
        Return BO.GetInsuranceBranchName(strConnection, strBranch, strLoginID)
    End Function

    Public Function GetMasterNotary(ByVal strConnection As String) As DataTable
        Dim BO As IDataUserControl
        BO = ComponentFactory.CreateDataUserControl
        Return BO.GetMasterNotary(strConnection)
    End Function

    Public Function GetBranchName(ByVal strConnection As String, ByVal strBranch As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.getBranchName(strConnection, strBranch)
    End Function
    Public Function GetAssetType(ByVal strConnection As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetAssetType(strConnection)
    End Function

    Public Function GetBranchName2(ByVal strConnection As String, ByVal strBranch As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.getBranchName2(strConnection, strBranch)
    End Function

    Public Function GetSupplierName(ByVal strConnection As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.getSupplierName(strConnection)
    End Function

    Public Function GetHOBranch(ByVal strConnection As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetHOBranch(strConnection)
    End Function
    Public Function GetBranchFromCopyRate(ByVal strConnection As String, ByVal strBranchSource As String, ByVal strBranch As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.getBranchFromCopyRate(strConnection, strBranchSource, strBranch)
    End Function
    Public Function BranchCashierList(ByVal strConnection As String, ByVal strBranch As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.BranchCashierList(strConnection, strBranch)
    End Function
    Public Function BranchHeadCashierList(ByVal strConnection As String, ByVal strBranch As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.BranchHeadCashierList(strConnection, strBranch)
    End Function

    Public Function GetBranchCollectionName(ByVal strConnection As String, ByVal strBranch As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.getBranchCollectionName(strConnection, strBranch)
    End Function
    Public Function GetAllBranchCollectionName(ByVal strConnection As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.getAllBranchCollectionName(strConnection)
    End Function
    Public Function GetAllCollectorTypeCL(ByVal strConnection As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetAllCollectorTypeCL(strConnection)
    End Function
    Public Function GetBankAccount(ByVal strConnection As String, ByVal strBranch As String, _
                                    ByVal strBankType As String, ByVal strBankPurpose As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetBankAccount(strConnection, strBranch, strBankType, strBankPurpose)
    End Function

    Public Function GetBranchAll(ByVal strConnection As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.getBranchAll(strConnection)
    End Function

    Public Function GetReason(ByVal strConnection As String, ByVal strReasonTypeID As String) As DataTable
        Dim DataUserControlReason As IDataUserControl
        DataUserControlReason = ComponentFactory.CreateDataUserControl
        Return DataUserControlReason.GetReason(strConnection, strReasonTypeID)
    End Function

    Public Function GetBGNumber(ByVal strConnection As String, ByVal strWhere As String) As DataTable
        Dim DataUserControlReason As IDataUserControl
        DataUserControlReason = ComponentFactory.CreateDataUserControl
        Return DataUserControlReason.GetBGNumber(strConnection, strWhere)
    End Function

    Public Function GetBranchHO(ByVal strConnection As String, ByVal strWhere As String) As DataTable
        Dim DataUserControlReason As IDataUserControl
        DataUserControlReason = ComponentFactory.CreateDataUserControl
        Return DataUserControlReason.GetBranchHO(strConnection, strWhere)
    End Function

    Public Function GetBankAccountbranch(ByVal strConnection As String, ByVal strBranch As String) As DataTable
        Dim DataUserControlReason As IDataUserControl
        DataUserControlReason = ComponentFactory.CreateDataUserControl
        Return DataUserControlReason.GetBankAccountbranch(strConnection, strBranch)
    End Function
    
    'Public Function GetCollectorSPVAct(ByVal oCollector As Collector) As Collector
    '    Dim DataUserCollectorSPV As IDataUserControl
    '    DataUserCollectorSPV = ComponentFactory.CreateDataUserControl
    '    Return DataUserCollectorSPV.GetCollectorSPVAct(oCollector)
    'End Function

    Public Function ViewBankAccount(ByVal strConnection As String) As DataTable
        Dim BAccount As IDataUserControl
        BAccount = ComponentFactory.CreateDataUserControl
        Return BAccount.ViewBankAccount(strConnection)
    End Function

    Public Function GetDepartement(ByVal strConnection As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetDepartement(strConnection)
    End Function

    Public Function GetBankMaster(ByVal strConnection As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetBankMaster(strConnection)
    End Function

    Public Function ViewEmployee(ByVal strConnection As String) As DataTable
        Dim Emp As IDataUserControl
        Emp = ComponentFactory.CreateDataUserControl
        Return Emp.ViewEmployee(strConnection)
    End Function

    Public Function ViewProduct(ByVal strConnection As String) As DataTable
        Dim prod As IDataUserControl
        prod = ComponentFactory.CreateDataUserControl
        Return prod.ViewProduct(strConnection)
    End Function

    Public Function ViewSupplier(ByVal strConnection As String) As DataTable
        Dim Supp As IDataUserControl
        Supp = ComponentFactory.CreateDataUserControl
        Return Supp.ViewSupplier(strConnection)
    End Function

    Public Function ViewArea(ByVal strConnection As String) As DataTable
        Dim VA As IDataUserControl
        VA = ComponentFactory.CreateDataUserControl
        Return VA.ViewArea(strConnection)
    End Function

    Public Function GetAgingList(ByVal customclass As Parameter.Sales) As Parameter.Sales
        Dim DataUserControlAG As IDataUserControl
        DataUserControlAG = ComponentFactory.CreateDataUserControl
        Return DataUserControlAG.GetAgingList(customclass)
    End Function

    Public Function GetSerialNo1Label(ByVal strConnection As String, ByVal Entities As Parameter.DocumentChangeLocation) As String
        Dim DataUserControlAG As IDataUserControl
        DataUserControlAG = ComponentFactory.CreateDataUserControl
        Return DataUserControlAG.GetSerialNo1Label(strConnection, Entities)
    End Function
    Public Function GetSerialNo2Label(ByVal strConnection As String, ByVal Entities As Parameter.DocumentChangeLocation) As String
        Dim DataUserControlAG As IDataUserControl
        DataUserControlAG = ComponentFactory.CreateDataUserControl
        Return DataUserControlAG.GetSerialNo2Label(strConnection, Entities)
    End Function
    Public Function GetGeneralSetting(ByVal strConnection As String, ByVal Entities As Parameter.GeneralSetting) As String
        Dim DataUserControlAG As IDataUserControl
        DataUserControlAG = ComponentFactory.CreateDataUserControl
        Return DataUserControlAG.GetGeneralSetting(strConnection, Entities)
    End Function
    Public Function GetProductBranch(ByVal strConnection As String, ByVal strBranch As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetProductBranch(strConnection, strBranch)
    End Function
    Public Function GetAPBank(ByVal strConnection As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.getAPBank(strConnection)
    End Function
    Public Function popCollector(ByVal strConnection As String, ByVal strCGID As String, ByVal strCollectorType As String) As DataTable
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.popCollector(strConnection, strCGID, strCollectorType)
    End Function
    Public Function GetCollectorSPVAct(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim DataUserCollectorSPV As IDataUserControl
        DataUserCollectorSPV = ComponentFactory.CreateDataUserControl
        Return DataUserCollectorSPV.GetCollectorSPVAct(oCollector)
    End Function
    Public Function viewCustomerDetailShort(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer
        Dim oInterface As IDataUserControl
        oInterface = ComponentFactory.CreateDataUserControl
        Return oInterface.viewCustomerDetailShort(oCustomClass)
    End Function
    Public Function getKotaKabupaten(ByVal oCustomClass As Parameter.Dati) As Parameter.Dati
        Dim oInterface As IDataUserControl
        oInterface = ComponentFactory.CreateDataUserControl
        Return oInterface.getKotaKabupaten(oCustomClass)
    End Function
    Public Function getAgreementGuarantor(ByVal oCustomClass As Parameter.AgreementGuarantor) As Parameter.AgreementGuarantor
        Dim oInterface As IDataUserControl
        oInterface = ComponentFactory.CreateDataUserControl
        Return oInterface.getAgreementGuarantor(oCustomClass)
    End Function

    Public Function GetCollectorReportLapHasilKunColl(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        'Public Function GetCollectorReportLapHasilKunColl(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetCollectorReportLapHasilKunColl(oClass)
    End Function

    Public Function GetCollectorReportLapKegDeskColl(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetCollectorReportLapKegDeskColl(oClass)
    End Function


    Public Function GetReportKasir(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetReportKasir(oClass)
    End Function

    Public Function GetReportNamaKonsumen(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetReportNamaKonsumen(oClass)
    End Function

    Public Function GetInsuranceCompanyBranchALL(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetInsuranceCompanyBranchALL(oClass)
    End Function

    Public Function GetReportTransactionType(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetReportTransactionType(oClass)
    End Function

    Public Function GetReportNoKontrak(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetReportNoKontrak(oClass)
    End Function

    Public Function GetReportNoAplikasi(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetReportNoAplikasi(oClass)
    End Function

    Public Function GetReportComboContract(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim DataUserControlBO As IDataUserControl
        DataUserControlBO = ComponentFactory.CreateDataUserControl
        Return DataUserControlBO.GetReportComboContract(oClass)
    End Function
End Class
