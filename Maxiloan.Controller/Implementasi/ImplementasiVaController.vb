﻿Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class ImplementasiVaController
     private BO As IImplementasi

    Public Sub New
         BO = ComponentFactory.CreateImplementasi
    End Sub


     Public Function GetVirtualAccount(strConn As string) As DataSet 
        Return BO.GetVirtualAccount(strConn)
    End Function


    Public Function GenerateVitualAccount(ByVal cnn As String, vabankid As IList(Of String), ByVal oDataTable As DataTable) As IList(Of Parameter.GeneratedVaToExcellLog)
        Return BO.GenerateVitualAccount(cnn, vabankid, oDataTable)
    End Function

    Public Function GetVirtualAccountInsAgreementLog(cnn As String, processid As String) As Parameter.VirtualAccountInsAgreementLog
        Return BO.GetVirtualAccountInsAgreementLog(cnn, processid)
    End Function

    Public Function GeneratePaymentGatewayFile(cnn As String, uploadId As String, dir As String, processDate As Date) As Boolean
        Return BO.GeneratePaymentGatewayFile(cnn, uploadId, dir, processDate)
    End Function


    Public Function VaGenerateSettingLog(cnn As String, processDate() As DateTime) As String()
        Return BO.VaGenerateSettingLog(cnn, processDate)
    End Function


End Class
