﻿Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface

Public Class SimulasiPerhitunganPembiayaanController
    Public Function SimulasiPerhitunganPembiayaanPaging(ByVal oCustomClass As Parameter.SimulasiPerhitunganPembiayaan) As Parameter.SimulasiPerhitunganPembiayaan
        Dim BO As ISimulasiPerhitunganPembiayaan
        BO = ComponentFactory.CreateSimulasiPerhitunganPembiayaan
        Return BO.SimulasiPerhitunganPembiayaanPaging(oCustomClass)
    End Function

    Public Function GenerateSimulasiPerhitunganPembiayaan(ByVal oCustomClass As Parameter.SimulasiPerhitunganPembiayaan) As Parameter.SimulasiPerhitunganPembiayaan
        Dim BO As ISimulasiPerhitunganPembiayaan
        BO = ComponentFactory.CreateSimulasiPerhitunganPembiayaan()
        Return BO.GenerateSimulasiPerhitunganPembiayaan(oCustomClass)
    End Function

    Public Function CetakKartuPiutangList(ByVal oCustomClass As Parameter.CetakKartuPiutang) As Parameter.CetakKartuPiutang
        Dim BO As ISimulasiPerhitunganPembiayaan
        BO = ComponentFactory.CreateSimulasiPerhitunganPembiayaan()
        Return BO.CetakKartuPiutangList(oCustomClass)
    End Function
End Class
