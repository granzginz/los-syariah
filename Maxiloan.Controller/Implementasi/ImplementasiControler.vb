
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class ImplementasiControler
    Public Function GetSP(ByVal customClass As Parameter.Implementasi) As Parameter.Implementasi
        Dim BO As IImplementasi
        BO = ComponentFactory.CreateImplementasi
        Return BO.GetSP(customClass)
    End Function
    Public Sub UpdateOutstandingPokok(ByVal customClass As Parameter.Implementasi)
        Dim BO As IImplementasi
        BO = ComponentFactory.CreateImplementasi
        BO.UpdateOutstandingPokok(customClass)
    End Sub
    Public Function GetGeneralPaging(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim BO As IImplementasi
        BO = ComponentFactory.CreateImplementasi
        Return BO.GetGeneralPaging(customClass)
    End Function
    Public Sub UpdateBiayaTarik(ByVal oCustomClass As Parameter.Implementasi)
        Dim BO As IImplementasi
        BO = ComponentFactory.CreateImplementasi
        BO.UpdateBiayaTarik(oCustomClass)
    End Sub
    Public Sub UpdateNDT(ByVal oCustomClass As Parameter.Implementasi)
        Dim BO As IImplementasi
        BO = ComponentFactory.CreateImplementasi
        BO.UpdateNDT(oCustomClass)
    End Sub
    Public Sub SavePembayaranTDP(ByVal oCustomClass As Parameter.Implementasi)
        Dim BO As IImplementasi
        BO = ComponentFactory.CreateImplementasi
        BO.SavePembayaranTDP(oCustomClass)
    End Sub
    Public Sub TDPSplit(ByVal oCustomClass As Parameter.TerimaTDP)
        Dim oTDPAllocation As IImplementasi
        oTDPAllocation = ComponentFactory.CreateImplementasi
        oTDPAllocation.TDPSplit(oCustomClass)
    End Sub
    'Tambahan Taufik 26 Feb 2016
    'Public Function AreaList(ByVal strConnection As String, ByVal mWhere As String) As DataTable
    '    Dim BO As IImplementasi
    '    BO = ComponentFactory.CreateImplementasi
    '    Return BO.AreaList(strConnection, mWhere)
    'End Function

    'Public Function Branch2List(ByVal strConnection As String, ByVal mWhere As String) As DataTable
    '    Dim BO As IImplementasi
    '    BO = ComponentFactory.CreateImplementasi
    '    Return BO.Branch2List(strConnection, mWhere)
    'End Function

    'Public Function GetBankByBranch(ByVal strConnection As String, ByVal cabang As String) As DataTable
    '    Dim BO As IImplementasi
    '    BO = ComponentFactory.CreateImplementasi
    '    Return BO.GetBankByBranch(strConnection, cabang)
    'End Function

    'Public Function GetBankAccountBranch(ByVal strConnection As String, ByVal cabang As String) As DataTable
    '    Dim BO As IImplementasi
    '    BO = ComponentFactory.CreateImplementasi
    '    Return BO.GetBankAccountBranch(strConnection, cabang)
    'End Function
    'Public Function GetBankAccountBranchAll(ByVal strConnection As String, ByVal cabang As String) As DataTable
    '    Dim BO As IImplementasi
    '    BO = ComponentFactory.CreateImplementasi
    '    Return BO.GetBankAccountBranchAll(strConnection, cabang)
    'End Function
    'Public Function GetKasir(ByVal strConnection As String, ByVal cabang As String) As DataTable
    '    Dim BO As IImplementasi
    '    BO = ComponentFactory.CreateImplementasi
    '    Return BO.GetKasir(strConnection, cabang)
    'End Function
    'Public Function GetCollector(ByVal strConnection As String, ByVal cabang As String) As DataTable
    '    Dim BO As IImplementasi
    '    BO = ComponentFactory.CreateImplementasi
    '    Return BO.GetCollector(strConnection, cabang)
    'End Function
    'Public Function GetInsuranceByBranch(ByVal strConnection As String, ByVal cabang As String) As DataTable
    '    Dim BO As IImplementasi
    '    BO = ComponentFactory.CreateImplementasi
    '    Return BO.GetInsuranceByBranch(strConnection, cabang)
    'End Function
    'Public Function GetInsurance(ByVal strConnection As String, ByVal cabang As String) As DataTable
    '    Dim BO As IImplementasi
    '    BO = ComponentFactory.CreateImplementasi
    '    Return BO.GetInsurance(strConnection, cabang)
    'End Function

    'Public Function GetCmoByBranch(ByVal strConnection As String, ByVal cabang As String) As DataTable
    '    Dim BO As IImplementasi
    '    BO = ComponentFactory.CreateImplementasi
    '    Return BO.GetCmoByBranch(strConnection, cabang)
    'End Function
    'Public Function GetCollectorByBranch(ByVal strConnection As String, ByVal cabang As String) As DataTable
    '    Dim BO As IImplementasi
    '    BO = ComponentFactory.CreateImplementasi
    '    Return BO.GetCollectorByBranch(strConnection, cabang)
    'End Function
    'Public Function GetHeadCollector(ByVal strConnection As String, ByVal cabang As String) As DataTable
    '    Dim BO As IImplementasi
    '    BO = ComponentFactory.CreateImplementasi
    '    Return BO.GetHeadCollector(strConnection, cabang)
    'End Function

    'Public Function GetEmployeePosition(ByVal strConnection As String, branchId As String) As String
    '    Dim BO As IImplementasi
    '    BO = ComponentFactory.CreateImplementasi
    '    Return BO.GetEmployeePosition(strConnection, branchId)
    'End Function
    'Public Function DropDownListCOA(ByVal customClass As Parameter.Implementasi) As Parameter.Implementasi
    '    Dim BO As IImplementasi
    '    BO = ComponentFactory.CreateImplementasi
    '    Return BO.DropDownListCOA(customClass)
    'End Function
    'Public Function GetCoaBranch(ByVal strConnection As String, ByVal cabang As String) As DataTable
    '    Dim BO As IImplementasi
    '    BO = ComponentFactory.CreateImplementasi
    '    Return BO.GetCoaBranch(strConnection, cabang)
    'End Function

    'Public Function DropDownListDivisi(ByVal customClass As Parameter.Implementasi) As Parameter.Implementasi
    '    Dim Divisi As IImplementasi
    '    Divisi = ComponentFactory.CreateImplementasi
    '    Return Divisi.DropDownListDivisi(customClass)
    'End Function

    'Public Function GetAsset(ByVal Customclass As Parameter.Implementasi) As Parameter.Implementasi
    '    Dim CustomerBO As IImplementasi
    '    CustomerBO = ComponentFactory.CreateImplementasi()
    '    Return CustomerBO.GetAsset(Customclass)
    'End Function
    'Public Function GetChartOfAccount(ByVal Customclass As Parameter.Implementasi) As Parameter.Implementasi
    '    Dim GetChartOfAccountBO As IImplementasi
    '    GetChartOfAccountBO = ComponentFactory.CreateImplementasi()
    '    Return GetChartOfAccountBO.GetChartOfAccount(Customclass)
    'End Function

    Public Function GetBankAccountCabang(ByVal oCustomclass As Parameter.Implementasi) As Parameter.Implementasi
        Dim oPC As IImplementasi
        oPC = ComponentFactory.CreateImplementasi()
        Return oPC.GetBankAccountCabang(oCustomclass)
    End Function
    'Public Function GetCoaPaging(ByVal Customclass As Parameter.Implementasi) As Parameter.Implementasi
    '    Dim GetChartOfAccountBO As IImplementasi
    '    GetChartOfAccountBO = ComponentFactory.CreateImplementasi()
    '    Return GetChartOfAccountBO.GetCoaPaging(Customclass)
    'End Function
    'Public Function GetGroupCoaPaging(ByVal Customclass As Parameter.Implementasi) As Parameter.Implementasi
    '    Dim GetChartOfAccountBO As IImplementasi
    '    GetChartOfAccountBO = ComponentFactory.CreateImplementasi()
    '    Return GetChartOfAccountBO.GetGroupCoaPaging(Customclass)
    'End Function

    'Public Function GetBankAccountHOTransfer(ByVal strConnection As String, ByVal BankID As String) As DataTable
    '    Dim BankHO As IImplementasi
    '    BankHO = ComponentFactory.CreateImplementasi()
    '    Return BankHO.GetBankAccountHOTransfer(strConnection, BankID)
    'End Function

    Public Function GetPeriodJournal(ByVal customClass As Parameter.Implementasi) As Parameter.Implementasi
        Dim PRBO As IImplementasi
        PRBO = ComponentFactory.CreateImplementasi()
        Return PRBO.GetPeriodJournal(customClass)
    End Function
    Public Function GetBankAccountFunding(ByVal strConnection As String) As DataTable
        Dim BO As IImplementasi
        BO = ComponentFactory.CreateImplementasi
        Return BO.GetBankAccountFunding(strConnection)
    End Function
    Public Function GetFundingCoyID(ByVal strConnection As String, FundingCoyID As String) As DataTable
        Dim BO As IImplementasi
        BO = ComponentFactory.CreateImplementasi
        Return BO.GetFundingCoyID(strConnection, FundingCoyID)
    End Function
    Public Function GetFundingBatch(ByVal strConnection As String, FundingContractNo As String) As DataTable
        Dim BO As IImplementasi
        BO = ComponentFactory.CreateImplementasi
        Return BO.GetFundingBatch(strConnection, FundingContractNo)
    End Function
    Public Function GetBankAccountHOTransfer(ByVal strConnection As String, ByVal BankID As String) As DataTable
        Dim BankHO As IImplementasi
        BankHO = ComponentFactory.CreateImplementasi()
        Return BankHO.GetBankAccountHOTransfer(strConnection, BankID)
    End Function
    Public Sub SaveTerimaTDP(ByVal oCustomClass As Parameter.TerimaTDP)
        Dim oSaveTerimaTDP As IImplementasi
        oSaveTerimaTDP = ComponentFactory.CreateImplementasi()
        oSaveTerimaTDP.SaveTerimaTDP(oCustomClass)
    End Sub
    Public Sub SaveOtorisasiTDP(ByVal oEntities As Parameter.TerimaTDP)
        Dim oSaveOtorisasiTDP As IImplementasi
        oSaveOtorisasiTDP = ComponentFactory.CreateImplementasi()
        oSaveOtorisasiTDP.SaveOtorisasiTDP(oEntities)
    End Sub
    Public Function TDPReversalList(ByVal oCustomClass As Parameter.TerimaTDP) As Parameter.TerimaTDP
        Dim oTDPReversalList As IImplementasi
        oTDPReversalList = ComponentFactory.CreateImplementasi()
        Return oTDPReversalList.TDPReversalList(oCustomClass)
    End Function
    Public Function ReverseTDP(ByVal oCustomClass As Parameter.TerimaTDP) As Parameter.TerimaTDP
        Dim oReverseTDP As IImplementasi
        oReverseTDP = ComponentFactory.CreateImplementasi()
        Return oReverseTDP.ReverseTDP(oCustomClass)
    End Function
    Public Sub SaveTDPReverse(ByVal oCustomClass As Parameter.TerimaTDP)
        Dim oSaveTDPReverse As IImplementasi
        oSaveTDPReverse = ComponentFactory.CreateImplementasi()
        oSaveTDPReverse.SaveTDPReverse(oCustomClass)
    End Sub
#Region "Fixed Asset"
	Public Function PaymentRequestFAList(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
		Dim oPaymentRequestFAList As IImplementasi
		oPaymentRequestFAList = ComponentFactory.CreateImplementasi()
		Return oPaymentRequestFAList.PaymentRequestFixedAssetList(oCustomClass)
	End Function
    Public Function PaymentRequestFAOtor(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
        Dim oPaymentRequestFAOtor As IImplementasi
        oPaymentRequestFAOtor = ComponentFactory.CreateImplementasi()
        Return oPaymentRequestFAOtor.PaymentRequestFixedAssetListOtor(oCustomClass)
    End Function
    Public Function PaymentRequestFAOtorSave(ByVal oCustomClass As Parameter.TransFixedAsset)
		Dim oPaymentRequestFAOtorSave As IImplementasi
		oPaymentRequestFAOtorSave = ComponentFactory.CreateImplementasi()
		Return oPaymentRequestFAOtorSave.PaymentRequestFixedAssetOtorSave(oCustomClass)
	End Function
	Public Function PaymentRequestFASave(ByVal oCustomClass As Parameter.TransFixedAsset)
		Dim oPaymentRequestFASave As IImplementasi
		oPaymentRequestFASave = ComponentFactory.CreateImplementasi()
		Return oPaymentRequestFASave.PaymentRequestFixedAssetSave(oCustomClass)
	End Function

	Public Function RequestReceiveFAList(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
		Dim oRequestReceiveFAList As IImplementasi
		oRequestReceiveFAList = ComponentFactory.CreateImplementasi()
		Return oRequestReceiveFAList.RequestReceiveFixedAssetList(oCustomClass)
	End Function
	Public Function RequestReceiveFAOtor(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
		Dim oRequestReceiveFAOtor As IImplementasi
		oRequestReceiveFAOtor = ComponentFactory.CreateImplementasi()
		Return oRequestReceiveFAOtor.RequestReceiveFixedAssetListOtor(oCustomClass)
	End Function
	Public Function RequestReceiveFAOtorSave(ByVal oCustomClass As Parameter.TransFixedAsset)
		Dim oRequestReceiveFAOtorSave As IImplementasi
		oRequestReceiveFAOtorSave = ComponentFactory.CreateImplementasi()
		Return oRequestReceiveFAOtorSave.RequestReceiveFixedAssetOtorSave(oCustomClass)
	End Function
    Public Function RequestReceiveFASave(ByVal oCustomClass As Parameter.TransFixedAsset)
        Dim oRequestReceiveFASave As IImplementasi
        oRequestReceiveFASave = ComponentFactory.CreateImplementasi()
        Return oRequestReceiveFASave.RequestReceiveFixedAssetSave(oCustomClass)
    End Function

    Public Function PenjualanFAList(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
        Dim oPenjualanFAList As IImplementasi
        oPenjualanFAList = ComponentFactory.CreateImplementasi()
        Return oPenjualanFAList.PenjualanFixedAssetList(oCustomClass)
    End Function
    Public Function PenjualanFAOtor(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
        Dim oPenjualanFAOtor As IImplementasi
        oPenjualanFAOtor = ComponentFactory.CreateImplementasi()
        Return oPenjualanFAOtor.PenjualanFixedAssetListOtor(oCustomClass)
    End Function
    Public Function PenjualanFAOtorSave(ByVal oCustomClass As Parameter.TransFixedAsset)
        Dim oPenjualanFAOtorSave As IImplementasi
        oPenjualanFAOtorSave = ComponentFactory.CreateImplementasi()
        Return oPenjualanFAOtorSave.PenjualanFixedAssetOtorSave(oCustomClass)
    End Function
    Public Function PenjualanFASave(ByVal oCustomClass As Parameter.TransFixedAsset)
        Dim oPenjualanFASave As IImplementasi
        oPenjualanFASave = ComponentFactory.CreateImplementasi()
        Return oPenjualanFASave.PenjualanFixedAssetSave(oCustomClass)
    End Function

    Public Function PenghapusanFAList(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
        Dim oPenjualanFAList As IImplementasi
        oPenjualanFAList = ComponentFactory.CreateImplementasi()
        Return oPenjualanFAList.PenghapusanFixedAssetList(oCustomClass)
    End Function
    Public Function PenghapusanFAOtor(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
        Dim oPenghapusanFAOtor As IImplementasi
        oPenghapusanFAOtor = ComponentFactory.CreateImplementasi()
        Return oPenghapusanFAOtor.PenghapusanFixedAssetListOtor(oCustomClass)
    End Function
    Public Function PenghapusanFAOtorSave(ByVal oCustomClass As Parameter.TransFixedAsset)
        Dim oPenghapusanFAOtorSave As IImplementasi
        oPenghapusanFAOtorSave = ComponentFactory.CreateImplementasi()
        Return oPenghapusanFAOtorSave.PenghapusanFixedAssetOtorSave(oCustomClass)
    End Function
    Public Function PenghapusanFASave(ByVal oCustomClass As Parameter.TransFixedAsset)
        Dim oPenghapusanFASave As IImplementasi
        oPenghapusanFASave = ComponentFactory.CreateImplementasi()
        Return oPenghapusanFASave.PenghapusanFixedAssetSave(oCustomClass)
    End Function
#End Region

#Region "Agreement Unit Expense"
    Public Function PaymentRequestAgreementUnitExpenseList(ByVal oCustomClass As Parameter.AgreementUnitExpense) As Parameter.AgreementUnitExpense
        Dim oPaymentRequestAgreementUnitExpenseList As IImplementasi
        oPaymentRequestAgreementUnitExpenseList = ComponentFactory.CreateImplementasi()
        Return oPaymentRequestAgreementUnitExpenseList.PaymentRequestAgreementUnitExpenseList(oCustomClass)
    End Function
#End Region

    'Public Function CetakKartuPiutangPaging(ByVal oCustomClass As Parameter.CetakKartuPiutang) As Parameter.CetakKartuPiutang
    '    Dim BO As IImplementasi
    '    BO = ComponentFactory.CreateImplementasi()
    '    Return BO.CetakKartuPiutangPaging(oCustomClass)
    'End Function

End Class
