
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Parameter

Public Class CustomerFacilityController

    Public Function CustomerFacilitySaveAdd(oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim DataBO As ICustomerFacility
        DataBO = ComponentFactory.CreateCustomerFacility()
        Return DataBO.CustomerFacilitySaveAdd(oCustomClass)
    End Function

    Public Function GetFacilityDetail(oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim DataBO As ICustomerFacility
        DataBO = ComponentFactory.CreateCustomerFacility()
        Return DataBO.GetFacilityDetail(oCustomClass)
    End Function

    Public Function GetAddendumFacilityDetail(oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim DataBO As ICustomerFacility
        DataBO = ComponentFactory.CreateCustomerFacility()
        Return DataBO.GetAddendumFacilityDetail(oCustomClass)
    End Function

    Public Function GetFacilityList(oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim DataBO As ICustomerFacility
        DataBO = ComponentFactory.CreateCustomerFacility()
        Return DataBO.GetFacilityList(oCustomClass)
    End Function

    Public Function CustomerFacilityApprovalList(oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim DataBO As ICustomerFacility
        DataBO = ComponentFactory.CreateCustomerFacility()
        Return DataBO.CustomerFacilityApprovalList(oCustomClass)
    End Function

    Public Function GetCboUserAprPCAll(oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim DataBO As ICustomerFacility
        DataBO = ComponentFactory.CreateCustomerFacility()
        Return DataBO.GetCboUserAprPCAll(oCustomClass)
    End Function

    Public Function GetCboUserApproval(oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim DataBO As ICustomerFacility
        DataBO = ComponentFactory.CreateCustomerFacility()
        Return DataBO.GetCboUserApproval(oCustomClass)
    End Function

    Public Function CustFacApproveSaveToNextPerson(oCustomClass As Parameter.CustomerFacility) As String
        Dim DataBO As ICustomerFacility
        DataBO = ComponentFactory.CreateCustomerFacility()
        Return DataBO.CustFacApproveSaveToNextPerson(oCustomClass)
    End Function

    Public Function CustFacApproveSave(oCustomClass As Parameter.CustomerFacility) As String
        Dim DataBO As ICustomerFacility
        DataBO = ComponentFactory.CreateCustomerFacility()
        Return DataBO.CustFacApproveSave(oCustomClass)
    End Function

    Public Function CustFacApproveisFinal(oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim DataBO As ICustomerFacility
        DataBO = ComponentFactory.CreateCustomerFacility()
        Return DataBO.CustFacApproveisFinal(oCustomClass)
    End Function

    Public Function CustFacApproveIsValidApproval(oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim DataBO As ICustomerFacility
        DataBO = ComponentFactory.CreateCustomerFacility()
        Return DataBO.CustFacApproveIsValidApproval(oCustomClass)
    End Function

    Public Function AddendumCustomerFacilitySaveAdd(oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim DataBO As ICustomerFacility
        DataBO = ComponentFactory.CreateCustomerFacility()
        Return DataBO.AddendumCustomerFacilitySaveAdd(oCustomClass)
    End Function

    Public Function AddendumCustomerFacilityApprovalList(oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim DataBO As ICustomerFacility
        DataBO = ComponentFactory.CreateCustomerFacility()
        Return DataBO.AddendumCustomerFacilityApprovalList(oCustomClass)
    End Function

    Public Function AddendumCustFacApproveSave(oCustomClass As Parameter.CustomerFacility) As String
        Dim DataBO As ICustomerFacility
        DataBO = ComponentFactory.CreateCustomerFacility()
        Return DataBO.AddendumCustFacApproveSave(oCustomClass)
    End Function

    Public Function AddendumCustFacApproveSaveToNextPerson(oCustomClass As Parameter.CustomerFacility) As String
        Dim DataBO As ICustomerFacility
        DataBO = ComponentFactory.CreateCustomerFacility()
        Return DataBO.AddendumCustFacApproveSaveToNextPerson(oCustomClass)
    End Function
End Class
