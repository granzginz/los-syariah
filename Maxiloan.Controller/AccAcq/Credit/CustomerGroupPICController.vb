
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class CustomerGroupPICController
    Public Function GetCustomerGroupPIC(ByVal ocustomClass As Parameter.CustomerGroupPIC) As Parameter.CustomerGroupPIC
        Dim CustomerGroupPICBO As ICustomerGroupPIC
        CustomerGroupPICBO = ComponentFactory.CreateCustomerGroupPIC()
        Return CustomerGroupPICBO.GetCustomerGroupPIC(ocustomClass)
    End Function
    Public Function GetCustomerGroupPICReport(ByVal ocustomClass As Parameter.CustomerGroupPIC) As Parameter.CustomerGroupPIC
        Dim CustomerGroupPICBO As ICustomerGroupPIC
        CustomerGroupPICBO = ComponentFactory.CreateCustomerGroupPIC()
        Return CustomerGroupPICBO.GetCustomerGroupPICReport(ocustomClass)
    End Function
    Public Function GetCustomerGroupPICList(ByVal ocustomClass As Parameter.CustomerGroupPIC) As Parameter.CustomerGroupPIC
        Dim CustomerGroupPICBO As ICustomerGroupPIC
        CustomerGroupPICBO = ComponentFactory.CreateCustomerGroupPIC()
        Return CustomerGroupPICBO.GetCustomerGroupPICList(ocustomClass)
    End Function
    Public Function GetCustomerGroupPICListByCustGroupID(ByVal ocustomClass As Parameter.CustomerGroupPIC) As Parameter.CustomerGroupPIC
        Dim CustomerGroupPICBO As ICustomerGroupPIC
        CustomerGroupPICBO = ComponentFactory.CreateCustomerGroupPIC()
        Return CustomerGroupPICBO.GetCustomerGroupPICListByCustGroupID(ocustomClass)
    End Function

    Public Function CustomerGroupPICSaveAdd(ByVal ocustomClass As Parameter.CustomerGroupPIC) As String
        Dim CustomerGroupPICBO As ICustomerGroupPIC
        CustomerGroupPICBO = ComponentFactory.CreateCustomerGroupPIC()
        Return CustomerGroupPICBO.CustomerGroupPICSaveAdd(ocustomClass)
    End Function

    Public Sub CustomerGroupPICSaveEdit(ByVal ocustomClass As Parameter.CustomerGroupPIC)
        Dim CustomerGroupPICBO As ICustomerGroupPIC
        CustomerGroupPICBO = ComponentFactory.CreateCustomerGroupPIC()
        CustomerGroupPICBO.CustomerGroupPICSaveEdit(ocustomClass)
    End Sub

    Public Function CustomerGroupPICDelete(ByVal ocustomClass As Parameter.CustomerGroupPIC) As String
        Dim CustomerGroupPICBO As ICustomerGroupPIC
        CustomerGroupPICBO = ComponentFactory.CreateCustomerGroupPIC()
        Return CustomerGroupPICBO.CustomerGroupPICDelete(ocustomClass)
    End Function

End Class
