
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class CustomerCasesController
    Public Function GetCustomerCases(ByVal ocustomClass As Parameter.CustomerCases) As Parameter.CustomerCases
        Dim CustomerCasesBO As ICustomerCases
        CustomerCasesBO = ComponentFactory.CreateCustomerCases()
        Return CustomerCasesBO.GetCustomerCases(ocustomClass)
    End Function
    Public Function GetCustomerCasesList(ByVal ocustomClass As Parameter.CustomerCases) As Parameter.CustomerCases
        Dim CustomerCasesBO As ICustomerCases
        CustomerCasesBO = ComponentFactory.CreateCustomerCases()
        Return CustomerCasesBO.GetCustomerCasesList(ocustomClass)
    End Function

    Public Function CustomerCasesSaveAdd(ByVal ocustomClass As Parameter.CustomerCases) As String
        Dim CustomerCasesBO As ICustomerCases
        CustomerCasesBO = ComponentFactory.CreateCustomerCases()
        Return CustomerCasesBO.CustomerCasesSaveAdd(ocustomClass)
    End Function

    Public Sub CustomerCasesSaveEdit(ByVal ocustomClass As Parameter.CustomerCases)
        Dim CustomerCasesBO As ICustomerCases
        CustomerCasesBO = ComponentFactory.CreateCustomerCases()
        CustomerCasesBO.CustomerCasesSaveEdit(ocustomClass)
    End Sub

    Public Function CustomerCasesDelete(ByVal ocustomClass As Parameter.CustomerCases) As String
        Dim CustomerCasesBO As ICustomerCases
        CustomerCasesBO = ComponentFactory.CreateCustomerCases()
        Return CustomerCasesBO.CustomerCasesDelete(ocustomClass)
    End Function

    Public Function GetComboCases(ByVal ocustomClass As Parameter.CustomerCases) As DataTable
        Dim CustomerCasesBO As ICustomerCases
        CustomerCasesBO = ComponentFactory.CreateCustomerCases()
        Return CustomerCasesBO.GetComboCases(ocustomClass)
    End Function
End Class
