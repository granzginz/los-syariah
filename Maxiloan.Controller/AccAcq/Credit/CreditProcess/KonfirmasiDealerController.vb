﻿Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface

Public Class KonfirmasiDealerController
    Public Function KonfirmasiDealerPaging(ByVal customClass As Parameter.KonfirmasiDealer) As Parameter.KonfirmasiDealer
        Dim oInterface As IKonfirmasiDealer
        oInterface = ComponentFactory.CreateKonfirmasiDealer
        Return oInterface.KonfirmasiDealerPaging(customClass)
    End Function

    Public Function ValidationSerialNo(ByVal customClass As Parameter.KonfirmasiDealer) As DataTable
        Dim oInterface As IKonfirmasiDealer
        oInterface = ComponentFactory.CreateKonfirmasiDealer
        Return oInterface.ValidationSerialNo(customClass)
    End Function

    Public Sub KonfirmasiDealerSave(ByVal customClass As Parameter.KonfirmasiDealer)
        Dim oInterface As IKonfirmasiDealer
        oInterface = ComponentFactory.CreateKonfirmasiDealer
        oInterface.KonfirmasiDealerSave(customClass)
    End Sub
End Class
