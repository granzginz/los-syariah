
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class DOController
    Public Function DOPaging(ByVal oCustomClass As Parameter._DO) As Parameter._DO
        Dim DOBO As IDO
        DOBO = ComponentFactory.CreateDO()
        Return DOBO.DOPaging(oCustomClass)
    End Function

    Public Function Get_PONumber(ByVal customClass As Parameter._DO) As String
        Dim DOBO As IDO
        DOBO = ComponentFactory.CreateDO
        Return DOBO.Get_PONumber(customClass)
    End Function

    Public Function GetDO_CheckBackDate(ByVal customClass As Parameter._DO) As Integer
        Dim DOBO As IDO
        DOBO = ComponentFactory.CreateDO
        Return DOBO.GetDO_CheckBackDate(customClass)
    End Function

    Public Function Get_DOProcess(ByVal customClass As Parameter._DO) As Parameter._DO
        Dim DOBO As IDO
        DOBO = ComponentFactory.CreateDO
        Return DOBO.Get_DOProcess(customClass)
    End Function

    Public Function GetDO_AssetDoc(ByVal customClass As Parameter._DO) As Parameter._DO
        Dim DOBO As IDO
        DOBO = ComponentFactory.CreateDO
        Return DOBO.GetDO_AssetDoc(customClass)
    End Function

    Public Function GetAttribute(ByVal oCustomClass As Parameter._DO) As Parameter._DO
        Dim DOBO As IDO
        DOBO = ComponentFactory.CreateDO()
        Return DOBO.GetAttribute(oCustomClass)
    End Function

    Public Sub DOSave(ByVal customClass As Parameter._DO, ByVal oData1 As DataTable, ByVal oData2 As DataTable, ByVal oData3 As DataTable, ByVal oData4 As DataTable, ByVal oData5 As DataTable)
        Dim DOBO As IDO
        DOBO = ComponentFactory.CreateDO()
        DOBO.DOSave(customClass, oData1, oData2, oData3, oData4, oData5)
    End Sub

    Function CheckSerial(ByVal oCustomClass As Parameter._DO) As Parameter._DO
        Dim DOBO As IDO
        DOBO = ComponentFactory.CreateDO()
        Return DOBO.CheckSerial(oCustomClass)
    End Function
    Function CheckAttribute(ByVal oCustomClass As Parameter._DO) As Parameter._DO
        Dim DOBO As IDO
        DOBO = ComponentFactory.CreateDO()
        Return DOBO.CheckAttribute(oCustomClass)
    End Function

    Function CheckAssetDoc(ByVal oCustomClass As Parameter._DO) As Parameter._DO
        Dim DOBO As IDO
        DOBO = ComponentFactory.CreateDO()
        Return DOBO.CheckAssetDoc(oCustomClass)
    End Function

    Public Sub ApplicationReturnUpdate(ByVal oCustomClass As Parameter._DO)
        Dim DOBO As IDO
        DOBO = ComponentFactory.CreateDO()
        DOBO.ApplicationReturnUpdate(oCustomClass)
    End Sub
End Class
