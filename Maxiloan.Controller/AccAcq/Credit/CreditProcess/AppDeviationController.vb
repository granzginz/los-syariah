﻿Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class AppDeviationController
    Public Function GetAppDeviation(ByVal ocustomClass As Parameter.AppDeviation) As Parameter.AppDeviation
        Dim AppDeviation As IAppDeviation
        AppDeviation = ComponentFactory.AppDeviation()
        Return AppDeviation.GetAppDeviation(ocustomClass)
    End Function
    Public Function GetDeviationView(ByVal ocustomClass As Parameter.AppDeviation) As Parameter.AppDeviation
        Dim AppDeviation As IAppDeviation
        AppDeviation = ComponentFactory.AppDeviation()
        Return AppDeviation.GetDeviationView(ocustomClass)
    End Function
    Public Sub AppDeviationPrint(ByVal ocustomClass As Parameter.AppDeviation)
        Dim AppDeviation As IAppDeviation
        AppDeviation = ComponentFactory.AppDeviation()
        AppDeviation.AppDeviationPrint(ocustomClass)
    End Sub
    Public Function GetApprovalPath(ByVal oCustomClass As Parameter.ApprovalScheme) As Parameter.ApprovalScheme
        Dim AppDeviation As IAppDeviation
        AppDeviation = ComponentFactory.AppDeviation()
        Return AppDeviation.GetApprovalPath(oCustomClass)
    End Function
    Public Function GetApprovalPathTreeMember(ByVal oCustomClass As Parameter.ApprovalScheme) As Parameter.ApprovalScheme
        Dim AppDeviation As IAppDeviation
        AppDeviation = ComponentFactory.AppDeviation()
        Return AppDeviation.GetApprovalPathTreeMember(oCustomClass)
    End Function
End Class
