
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class CreditScoringController
    Public Function CreditScoringPaging(ByVal oCustomClass As Parameter.CreditScoring) As Parameter.CreditScoring
        Dim CreditScoringBO As ICreditScoring
        CreditScoringBO = ComponentFactory.CreateCreditScoring()
        Return CreditScoringBO.CreditScoringPaging(oCustomClass)
    End Function

    Public Function CreditScoringSaveAdd(ByVal customClass As Parameter.CreditScoring) As String
        Dim CreditScoringBO As ICreditScoring
        CreditScoringBO = ComponentFactory.CreateCreditScoring
        Return CreditScoringBO.CreditScoringSaveAdd(customClass)
    End Function

    Public Function CreditScoringSaveAdd(ByVal customClass As Parameter.CreditScoring, ByVal isSavingGrid As Boolean) As String
        Dim CreditScoringBO As ICreditScoring
        CreditScoringBO = ComponentFactory.CreateCreditScoring
        Return CreditScoringBO.CreditScoringSaveAdd(customClass, isSavingGrid)
    End Function
    Function DoCreaditScoringSave(cnn As String, loginId As String, branchid As String, AppId As String, scoreResult As Parameter.ScoreResults, bnsDate As Date) As String
        Dim CreditScoringBO As ICreditScoring
        CreditScoringBO = ComponentFactory.CreateCreditScoring
        Return CreditScoringBO.DoCreaditScoringSave(cnn, loginId, branchid, AppId, scoreResult, bnsDate)
    End Function

    Public Function GetCreditScorePolicyResult(ByVal oCustomClass As Parameter.CreditScoring) As Parameter.CreditScoring
        Dim CreditScoringBO As ICreditScoring
        CreditScoringBO = ComponentFactory.CreateCreditScoring
        Return CreditScoringBO.GetCreditScorePolicyResult(oCustomClass)
    End Function


    Function CrDispositionCreditRpt(cnn As String, whereCond As String(), tp As String) As DataSet
        Dim CreditScoringBO As ICreditScoring
        CreditScoringBO = ComponentFactory.CreateCreditScoring
        Return CreditScoringBO.CrDispositionCreditRpt(cnn, whereCond, tp)
    End Function

    Function DoCreditScoringProceed(cnn As String, branchid As String, ApplicationID As String, ScoringType As String, bnsDate As Date)
        Dim BO As ICreditScoring
        BO = ComponentFactory.CreateCreditScoring()
        Return BO.DoCreditScoringProceed(cnn, branchid, ApplicationID, ScoringType, bnsDate)
    End Function

    Function GetApprovalCreditScoring(ByVal oCustomClass As Parameter.CreditScoring, ByVal strApproval As String)
        Dim BO As ICreditScoring
        BO = ComponentFactory.CreateCreditScoring()
        Return BO.GetApprovalCreditScoring(oCustomClass, strApproval)
    End Function
End Class
