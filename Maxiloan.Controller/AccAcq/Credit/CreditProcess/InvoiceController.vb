
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InvoiceController
    Public Function InvoicePaging(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice
        Dim InvoiceBO As IInvoice
        InvoiceBO = ComponentFactory.CreateInvoice()
        Return InvoiceBO.InvoicePaging(oCustomClass)
    End Function
    Public Function ListRecATPM(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice
        Dim InvoiceBO As IInvoice
        InvoiceBO = ComponentFactory.CreateInvoice()
        Return InvoiceBO.GetInvoiceATPMList(oCustomClass)
    End Function
    Public Function GetInvoiceATPMList(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice
        Dim InvoiceBO As IInvoice
        InvoiceBO = ComponentFactory.CreateInvoice()
        Return InvoiceBO.GetInvoiceATPMList(oCustomClass)
    End Function
    Public Function GetInvoiceAgreementList(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice
        Dim InvoiceBO As IInvoice
        InvoiceBO = ComponentFactory.CreateInvoice()
        Return InvoiceBO.GetInvoiceAgreementList(oCustomClass)
    End Function
    Public Function GetInvoiceATPMAgreementList(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice
        Dim InvoiceBO As IInvoice
        InvoiceBO = ComponentFactory.CreateInvoice()
        Return InvoiceBO.GetInvoiceATPMAgreementList(oCustomClass)
    End Function

    Public Function GetInvoiceMaxBackDate(ByVal customClass As Parameter.Invoice) As Integer
        Dim InvoiceBO As IInvoice
        InvoiceBO = ComponentFactory.CreateInvoice
        Return InvoiceBO.GetInvoiceMaxBackDate(customClass)
    End Function

    Public Function GetInvoiceSuppTopDays(ByVal customClass As Parameter.Invoice) As Integer
        Dim InvoiceBO As IInvoice
        InvoiceBO = ComponentFactory.CreateInvoice
        Return InvoiceBO.GetInvoiceSuppTopDays(customClass)
    End Function

    Public Function InvoiceSave(ByVal customClass As Parameter.Invoice) As String
        Dim InvoiceBO As IInvoice
        InvoiceBO = ComponentFactory.CreateInvoice
        Return InvoiceBO.InvoiceSave(customClass)
    End Function

    Public Function InvoiceATPMSave(ByVal customClass As Parameter.Invoice) As String
        Dim InvoiceBO As IInvoice
        InvoiceBO = ComponentFactory.CreateInvoice
        Return InvoiceBO.InvoiceATPMSave(customClass)
    End Function

    Public Function LoadCombo(cnn As String, ty As String, Optional id As String = "") As IList(Of Parameter.CommonValueText)
        Return CType(ComponentFactory.CreateInvoice(), IInvoice).LoadCombo(cnn, ty, id)
    End Function

    Public Function InvoiceSuppSave(cnn As String, ByVal invoiceSupp As Parameter.InvoiceSupp) As String
        Return CType(ComponentFactory.CreateInvoice(), IInvoice).InvoiceSuppSave(cnn, invoiceSupp)
    End Function
    Function GetNoInvoiceATPM(ByVal customClass As Parameter.Invoice) As Parameter.Invoice
        Dim InvATPMBO As IInvoice
        InvATPMBO = ComponentFactory.CreateInvoice()
        Return InvATPMBO.GetNoInvoiceATPM(customClass)
    End Function
    Public Function ReceiveATPMList(ByVal customClass As Parameter.Invoice) As Parameter.Invoice
        Dim InvATPMBO As IInvoice
        InvATPMBO = ComponentFactory.CreateInvoice()
        Return InvATPMBO.ReceiveATPMList(customClass)
    End Function
    Public Sub SaveATPMOtorisasi(ByVal oCustomClass As Parameter.Invoice)
        Dim oSaveATPMOtorisasi As IInvoice
        oSaveATPMOtorisasi = ComponentFactory.CreateInvoice
        oSaveATPMOtorisasi.SaveATPMOtorisasi(oCustomClass)
    End Sub
    Public Sub RejectOtorisasi(ByVal oCustomClass As Parameter.Invoice)
        Dim oRejectATPM As IInvoice
        oRejectATPM = ComponentFactory.CreateInvoice
        oRejectATPM.RejectOtorisasi(oCustomClass)
    End Sub
    Public Sub EditOtorisasi(ByVal customclass As Parameter.Invoice)
        Dim oEditATPM As IInvoice
        oEditATPM = ComponentFactory.CreateInvoice
        oEditATPM.EditOtorisasi(customclass)
    End Sub
    Public Function GetInvoiceNo(ByVal customclass As Parameter.Invoice) As Parameter.Invoice
        Dim InvATPMBO As IInvoice
        InvATPMBO = ComponentFactory.CreateInvoice()
        Return InvATPMBO.GetInvoiceNo(customclass)
    End Function
End Class
