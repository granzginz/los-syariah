﻿Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface

Public Class ApplicationDetailTransactionController
    Function getApplicationDetailTransaction(ByVal oCustomClass As Parameter.ApplicationDetailTransaction) As Parameter.ApplicationDetailTransaction
        Dim oInterface As IApplicationDetailTransaction
        oInterface = ComponentFactory.CreateApplicationDetailTransaction
        Return oInterface.getApplicationDetailTransaction(oCustomClass)
    End Function

    Sub ApplicationDetailTransactionSave(ByVal oCustomClass As Parameter.ApplicationDetailTransaction)
        Dim oInterface As IApplicationDetailTransaction
        oInterface = ComponentFactory.CreateApplicationDetailTransaction
        oInterface.ApplicationDetailTransactionSave(oCustomClass)
    End Sub

    Function getBungaNett(ByVal oClass As Parameter.ApplicationDetailTransaction) As Parameter.ApplicationDetailTransaction
        Dim oInterface As IApplicationDetailTransaction
        oInterface = ComponentFactory.CreateApplicationDetailTransaction
        Return oInterface.getBungaNett(oClass)
    End Function
    Function getBungaNettKPR(ByVal oClass As Parameter.ApplicationDetailTransaction) As Parameter.ApplicationDetailTransaction
        Dim oInterface As IApplicationDetailTransaction
        oInterface = ComponentFactory.CreateApplicationDetailTransaction
        Return oInterface.getBungaNettKPR(oClass)
    End Function

    Function getBungaNettOperatingLease(ByVal oClass As Parameter.ApplicationDetailTransaction) As Parameter.ApplicationDetailTransaction
        Dim oInterface As IApplicationDetailTransaction
        oInterface = ComponentFactory.CreateApplicationDetailTransaction
        Return oInterface.getBungaNettOperatingLease(oClass)
    End Function
End Class
