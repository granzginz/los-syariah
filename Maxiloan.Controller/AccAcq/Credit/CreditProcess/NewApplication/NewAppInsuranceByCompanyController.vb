

#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region



Public Class NewAppInsuranceByCompanyController

    Public Function CheckProspect(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
        Dim CheckProspectBO As INewAppInsuranceByCompany
        CheckProspectBO = ComponentFactory.CreateNewAppInsuranceByCompany
        Return CheckProspectBO.CheckProspect(CustomClass)
    End Function


    Public Function GetInsuranceEntryFactoring(ByVal Customclass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
        Dim GetInsuranceEntryStep1ListBO As INewAppInsuranceByCompany
        GetInsuranceEntryStep1ListBO = ComponentFactory.CreateNewAppInsuranceByCompany
        Return GetInsuranceEntryStep1ListBO.GetInsuranceEntryFactoring(Customclass)
    End Function

    Public Function GetInsuranceEntryStep1List(ByVal Customclass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
        Dim GetInsuranceEntryStep1ListBO As INewAppInsuranceByCompany
        GetInsuranceEntryStep1ListBO = ComponentFactory.CreateNewAppInsuranceByCompany
        Return GetInsuranceEntryStep1ListBO.GetInsuranceEntryStep1List(Customclass)
    End Function
    Public Function GetInsuranceEntryStep2List(ByVal Customclass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
        Dim GetInsuranceEntryStep1ListBO As INewAppInsuranceByCompany
        GetInsuranceEntryStep1ListBO = ComponentFactory.CreateNewAppInsuranceByCompany
        Return GetInsuranceEntryStep1ListBO.GetInsuranceEntryStep2List(Customclass)
    End Function
    Public Function EditGetInsuranceEntryStep1List(ByVal Customclass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
        Dim BO As INewAppInsuranceByCompany
        BO = ComponentFactory.CreateNewAppInsuranceByCompany
        Return BO.EditGetInsuranceEntryStep1List(Customclass)
    End Function
    Public Function EditGetInsuranceEntryStep2List(ByVal Customclass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
        Dim BO As INewAppInsuranceByCompany
        BO = ComponentFactory.CreateNewAppInsuranceByCompany
        Return BO.EditGetInsuranceEntryStep2List(Customclass)
    End Function

    Public Function EditInsuranceSelect(ByVal Customclass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
        Dim BO As INewAppInsuranceByCompany
        BO = ComponentFactory.CreateNewAppInsuranceByCompany
        Return BO.EditInsuranceSelect(Customclass)
    End Function
    Public Function FillInsuranceComBranch(ByVal customclass As Parameter.NewAppInsuranceByCompany) As DataTable
        Dim FillInsuranceComBranchBO As INewAppInsuranceByCompany
        FillInsuranceComBranchBO = ComponentFactory.CreateNewAppInsuranceByCompany
        Return FillInsuranceComBranchBO.FillInsuranceComBranch(customclass)
    End Function

    Public Function HandleDropDownOnDataBound(ByVal customclass As Parameter.NewAppInsuranceByCompany) As DataTable
        Dim HandleDropDownOnDataBoundBO As INewAppInsuranceByCompany
        HandleDropDownOnDataBoundBO = ComponentFactory.CreateNewAppInsuranceByCompany
        Return HandleDropDownOnDataBoundBO.HandleDropDownOnDataBound(customclass)
    End Function

    Public Function GetInsurancehComByBranch(ByVal customclass As Parameter.NewAppInsuranceByCompany) As DataTable
        Dim BO As INewAppInsuranceByCompany
        BO = ComponentFactory.CreateNewAppInsuranceByCompany
        Return BO.GetInsurancehComByBranch(customclass)
    End Function

    Public Function GetInsurancehComByBranchProduct(ByVal customclass As Parameter.InsCoBranch) As Parameter.InsCoBranch
        Dim BO As INewAppInsuranceByCompany
        BO = ComponentFactory.CreateNewAppInsuranceByCompany
        Return BO.GetInsurancehComByBranchProduct(customclass)
    End Function

    Public Function GetAssetCategory(ByVal customclass As Parameter.NewAppInsuranceByCompany) As DataTable
        Dim BO As INewAppInsuranceByCompany
        BO = ComponentFactory.CreateNewAppInsuranceByCompany
        Return BO.GetAssetCategory(customclass)
    End Function

    Public Function GetInsuranceEntryStep1ListAgriculture(ByVal Customclass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
        Dim BO As INewAppInsuranceByCompany
        BO = ComponentFactory.CreateNewAppInsuranceByCompany
        Return BO.GetInsuranceEntryStep1ListAgriculture(Customclass)
    End Function

    Public Function GetInsuranceBranchNameRateCard(connString As String, applicationid As String, MaskAssBranchID As String, CoverageType As String, yearnum As Integer) As DataTable
        Dim BO As INewAppInsuranceByCompany
        BO = ComponentFactory.CreateNewAppInsuranceByCompany
        Return BO.GetInsuranceBranchNameRateCard(connString, applicationid, MaskAssBranchID, CoverageType, yearnum)
    End Function

    Public Function GetInsuranceComBranch(ByVal Customclass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
        Dim BO As INewAppInsuranceByCompany
        BO = ComponentFactory.CreateNewAppInsuranceByCompany()
        Return BO.GetInsuranceComBranch(Customclass)
    End Function

End Class
