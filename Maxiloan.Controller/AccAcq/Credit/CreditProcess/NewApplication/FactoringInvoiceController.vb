﻿Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class FactoringInvoiceController

    Public Function GetInvoiceList(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim oInterface As IFactoringInvoice
        oInterface = ComponentFactory.CreateFactoringInvoice
        Return oInterface.GetInvoiceList(oCustomClass)
    End Function

    Public Function GetInvoiceDetail(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim oInterface As IFactoringInvoice
        oInterface = ComponentFactory.CreateFactoringInvoice
        Return oInterface.GetInvoiceDetail(ocustomClass)
    End Function

    Public Function InvoiceSaveAdd(ByVal ocustomClass As Parameter.FactoringInvoice) As String
        Dim oInterface As IFactoringInvoice
        oInterface = ComponentFactory.CreateFactoringInvoice
        Return oInterface.InvoiceSaveAdd(ocustomClass)
    End Function

    Public Function InvoiceSaveEdit(ByVal ocustomClass As Parameter.FactoringInvoice) As String
        Dim oInterface As IFactoringInvoice
        oInterface = ComponentFactory.CreateFactoringInvoice
        Return oInterface.InvoiceSaveEdit(ocustomClass)
    End Function

    Public Function InvoiceDelete(ByVal ocustomClass As Parameter.FactoringInvoice) As String
        Dim oInterface As IFactoringInvoice
        oInterface = ComponentFactory.CreateFactoringInvoice
        Return oInterface.InvoiceDelete(ocustomClass)
    End Function

    Public Function InvoiceSaveApplication(ByVal ocustomClass As Parameter.Application) As String
        Dim oInterface As IFactoringInvoice
        oInterface = ComponentFactory.CreateFactoringInvoice
        Return oInterface.InvoiceSaveApplication(ocustomClass)
    End Function

    Public Function GetFactoringFee(ByVal ocustomClass As Parameter.Application) As Parameter.FactoringInvoice
        Dim oInterface As IFactoringInvoice
        oInterface = ComponentFactory.CreateFactoringInvoice
        Return oInterface.GetFactoringFee(ocustomClass)
    End Function

    Public Function FactoringApplicationFinancialSave(ByVal ocustomClass As Parameter.Application) As String
        Dim oInterface As IFactoringInvoice
        oInterface = ComponentFactory.CreateFactoringInvoice
        Return oInterface.FactoringApplicationFinancialSave(ocustomClass)
    End Function

    Public Function FactoringInvoiceDisburse(ByVal ocustomClass As Parameter.FactoringInvoice) As String
        Dim oInterface As IFactoringInvoice
        oInterface = ComponentFactory.CreateFactoringInvoice
        Return oInterface.FactoringInvoiceDisburse(ocustomClass)
    End Function

    Public Function RestruckFactoringPaging(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim oInterface As IFactoringInvoice
        oInterface = ComponentFactory.CreateFactoringInvoice
        Return oInterface.RestruckFactoringPaging(ocustomClass)
    End Function

    Public Function GetRestructFactoringData(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim oInterface As IFactoringInvoice
        oInterface = ComponentFactory.CreateFactoringInvoice
        Return oInterface.GetRestructFactoringData(oCustomClass)
    End Function

    Public Function GetInvoiceDetail2(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim oInterface As IFactoringInvoice
        oInterface = ComponentFactory.CreateFactoringInvoice
        Return oInterface.GetInvoiceDetail2(ocustomClass)
    End Function
    Public Function GetPPH(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim oInterface As IFactoringInvoice
        oInterface = ComponentFactory.CreateFactoringInvoice
        Return oInterface.GetPPH(ocustomClass)
    End Function

    Public Function RestructFactoringSave(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim oInterface As IFactoringInvoice
        oInterface = ComponentFactory.CreateFactoringInvoice
        Return oInterface.RestructFactoringSave(oCustomClass)
    End Function
    Public Function GetInvoiceChangeDueDateList(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim oInterface As IFactoringInvoice
        oInterface = ComponentFactory.CreateFactoringInvoice
        Return oInterface.GetInvoiceChangeDueDateList(oCustomClass)
    End Function
    Public Function GetInvoiceListPaid(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim oInterface As IFactoringInvoice
        oInterface = ComponentFactory.CreateFactoringInvoice
        Return oInterface.GetInvoiceListPaid(oCustomClass)
    End Function
    'Public Function GetFinancialData_002(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    '    Dim oInterface As IFactoringInvoice
    '    oInterface = ComponentFactory.CreateFactoringInvoice
    '    Return oInterface.GetFinancialData_002(oCustomClass)
    'End Function
    'Function GetTerm(ByVal intInstTerm As Integer) As Integer
    '    Dim FinancialDataBO As IFactoringInvoice
    '    FinancialDataBO = ComponentFactory.CreateFactoringInvoice()
    '    Return FinancialDataBO.GetTerm(intInstTerm)
    'End Function
    'Function SaveFinancialDataTab2End(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    '    Dim BO As IFactoringInvoice
    '    BO = ComponentFactory.CreateFactoringInvoice
    '    Return BO.SaveFinancialDataTab2End(oCustomClass)
    'End Function
    'Function GetRefundPremiumToSupplier(ByVal data As Parameter.Supplier) As Parameter.Supplier
    '    Dim BO As IFactoringInvoice
    '    BO = ComponentFactory.CreateFactoringInvoice
    '    Return BO.GetRefundPremiumToSupplier(data)
    'End Function
    'Function saveBungaNett(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    '    Dim BO As IFactoringInvoice
    '    BO = ComponentFactory.CreateFactoringInvoice
    '    Return BO.saveBungaNett(oCustomClass)
    'End Function
    'Function SaveFinancialDataTab2(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    '    Dim BO As IFactoringInvoice
    '    BO = ComponentFactory.CreateFactoringInvoice
    '    Return BO.SaveFinancialDataTab2(oCustomClass)
    'End Function
End Class
