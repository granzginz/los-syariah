Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class ApplicationController

    Public Function GetDataProspectType(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetDataProspectType(oCustomClass)
    End Function

    Public Function GetApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetApplication(oCustomClass)
    End Function

    Public Function GetApplicationFactoring(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetApplicationFactoring(oCustomClass)
    End Function
    Public Function GetApplicationModalKerja(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetApplicationModalKerja(oCustomClass)
    End Function
    Public Function GetCountAppliationId(ByVal oCustomClass As Parameter.Application) As Integer
        Dim CountAppliationId As IApplication
        CountAppliationId = ComponentFactory.CreateApplication()
        Return CountAppliationId.GetCountAppliationId(oCustomClass)
    End Function

    Function GetFee(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetFee(oCustomClass)
    End Function

    Function GetCopyFrom(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetCopyFrom(oCustomClass)
    End Function

    Function GetTC(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetTC(oCustomClass)
    End Function

    Function GetTC2(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetTC2(oCustomClass)
    End Function

    Function GetAddress(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetAddress(oCustomClass)
    End Function

    Function GetShowDataAgreement(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetShowDataAgreement(oCustomClass)
    End Function

    Function ApplicationSaveAdd(ByVal oApplication As Parameter.Application,
                                 ByVal oRefPersonal As Parameter.Personal,
                                 ByVal oRefAddress As Parameter.Address,
                                 ByVal oMailingAddress As Parameter.Address,
                                 ByVal oData1 As DataTable,
                                 ByVal oData2 As DataTable,
                                 ByVal oData3 As DataTable,
                                 ByVal oGuarantorPersonal As Parameter.Personal,
                                 ByVal oGuarantorAddress As Parameter.Address) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.ApplicationSaveAdd(oApplication,
                                                 oRefPersonal,
                                                 oRefAddress,
                                                 oMailingAddress,
                                                 oData1,
                                                 oData2,
                                                 oData3,
                                                 oGuarantorPersonal,
                                                 oGuarantorAddress)
    End Function

    Function GetViewApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetViewApplication(oCustomClass)
    End Function

    Function GetApplicationMaintenance(ByVal customClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetApplicationMaintenance(customClass)
    End Function
    Function GetCD(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetCD(oCustomClass)
    End Function

    Function GetApplicationEdit(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetApplicationEdit(oCustomClass)
    End Function
    Function GetApplicationEditPPH(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetApplicationEditPPH(oCustomClass)
    End Function

    Function ApplicationSaveEdit(ByVal oApplication As Parameter.Application,
                                 ByVal oRefPersonal As Parameter.Personal,
                                 ByVal oRefAddress As Parameter.Address,
                                 ByVal oMailingAddress As Parameter.Address,
                                 ByVal oData1 As DataTable,
                                 ByVal oData2 As DataTable,
                                 ByVal oData3 As DataTable,
                                 ByVal oGuarantorPersonal As Parameter.Personal,
                                 ByVal oGuarantorAddress As Parameter.Address) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.ApplicationSaveEdit(oApplication,
                                                 oRefPersonal,
                                                 oRefAddress,
                                                 oMailingAddress,
                                                 oData1,
                                                 oData2,
                                                 oData3,
                                                 oGuarantorPersonal,
                                                 oGuarantorAddress)
    End Function

    Function GetGoLive(ByVal customClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetGoLive(customClass)
    End Function

    Function GetGoLiveCancelPaging(ByVal customClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetGoLiveCancelPaging(customClass)
    End Function

    Function GetGoLiveBackDated(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetGoLiveBackDated(oCustomClass)
    End Function

    Function GoLiveSave(ByVal oGoLive As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GoLiveSave(oGoLive)
    End Function

    Function GoLiveCancel(ByVal oGoLive As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GoLiveCancel(oGoLive)
    End Function

    Function GetShowDataProspect(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetShowDataProspect(oCustomClass)
    End Function

    Function GetDataAppIDProspect(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetDataAppIDProspect(oCustomClass)
    End Function

    Function GetCustomerProspect(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetCustomerProspect(oCustomClass)
    End Function

    Function ViewProspectApplication(ByVal oCustomClass As Parameter.Application) As DataTable
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.ViewProspectApplication(oCustomClass)
    End Function

    Function GetViewMKKFasilitas(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetViewMKKFasilitas(oCustomClass)
    End Function

    Function saveApplicationProductOffering(ByVal oApplication As Parameter.Application, ByVal oAssetData As Parameter.AssetData) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.saveApplicationProductOffering(oApplication, oAssetData)
    End Function

    Function saveApplicationProductOfferingEdit(ByVal oApplication As Parameter.Application, ByVal oAssetData As Parameter.AssetData) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.saveApplicationProductOfferingEdit(oApplication, oAssetData)
    End Function
    Function GetSyaratCair(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication
        Return ApplicationBO.GetSyaratCair(oCustomClass)
    End Function
    Function GetSyaratCairPO(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication
        Return ApplicationBO.GetSyaratCairPO(oCustomClass)
    End Function
    Public Function DuplikasiApplication(ByVal oCustomClass As Parameter.Application) As String
        Dim CountAppliationId As IApplication
        CountAppliationId = ComponentFactory.CreateApplication()
        Return CountAppliationId.DuplikasiApplication(oCustomClass)
    End Function

    Function ApplicationSaveEditNonFinancial(ByVal oApplication As Parameter.Application,
                                ByVal oRefPersonal As Parameter.Personal,
                                ByVal oRefAddress As Parameter.Address,
                                ByVal oMailingAddress As Parameter.Address,
                                ByVal oData1 As DataTable,
                                ByVal oData2 As DataTable,
                                ByVal oData3 As DataTable) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.ApplicationSaveEditNonFinancial(oApplication,
                                                 oRefPersonal,
                                                 oRefAddress,
                                                 oMailingAddress,
                                                 oData1,
                                                 oData2,
                                                 oData3)
    End Function

    Function GetApplicationHasilSurveydanKYC(ByVal customClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IApplication
        ApplicationBO = ComponentFactory.CreateApplication()
        Return ApplicationBO.GetApplicationHasilSurveydanKYC(customClass)
    End Function

    Function ValidasiCollector(ByVal customClass As Parameter.Application) As Parameter.Application
        Dim BO As IApplication
        BO = ComponentFactory.CreateApplication()
        Return BO.ValidasiCollector(customClass)
    End Function
    Function ModalKerjaApplicationFinancialSave(ByVal ocustomClass As Parameter.Application) As String
        Dim BO As IApplication
        BO = ComponentFactory.CreateApplication()
        Return BO.ModalKerjaApplicationFinancialSave(ocustomClass)
    End Function

    Function GetGoLiveModalKerja(ByVal ocustomClass As Parameter.Application) As Parameter.Application
        Dim BO As IApplication
        BO = ComponentFactory.CreateApplication()
        Return BO.GetGoLiveModalKerja(ocustomClass)
    End Function

    Function GoLiveSaveModalKerja(ByVal ocustomClass As Parameter.Application) As Parameter.Application
        Dim BO As IApplication
        BO = ComponentFactory.CreateApplication()
        Return BO.GoLiveSaveModalKerja(ocustomClass)
    End Function

    Function GetGoLiveFactoring(ByVal ocustomClass As Parameter.Application) As Parameter.Application
        Dim BO As IApplication
        BO = ComponentFactory.CreateApplication()
        Return BO.GetGoLiveFactoring(ocustomClass)
    End Function

    Function GoLiveSaveFactoring(ByVal ocustomClass As Parameter.Application) As Parameter.Application
        Dim BO As IApplication
        BO = ComponentFactory.CreateApplication()
        Return BO.GoLiveSaveFactoring(ocustomClass)
    End Function
    Function ActivityLogSave(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim BO As IApplication
        BO = ComponentFactory.CreateApplication()
        Return BO.ActivityLogSave(oCustomClass)
    End Function

    Function DisburseLogSave(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim BO As IApplication
        BO = ComponentFactory.CreateApplication()
        Return BO.DisburseLogSave(oCustomClass)
    End Function
    Function getdataNPP(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim BO As IApplication
        BO = ComponentFactory.CreateApplication()
        Return BO.getdataNPP(oCustomClass)
    End Function

    Function InstallmentDrawDownSave(ByVal oDrawdown As Parameter.Drawdown) As String
        Dim BO As IApplication
        BO = ComponentFactory.CreateApplication()
        Return BO.InstallmentDrawDownSave(oDrawdown)
    End Function

    Function InstallmentDrawDownCheckRequest(ByVal odrawdown As Parameter.Drawdown) As Boolean
        Dim BO As IApplication
        BO = ComponentFactory.CreateApplication()
        Return BO.InstallmentDrawDownCheckRequest(odrawdown)
    End Function

    Function InstallmentDrawDownList(ByVal odrawdown As Parameter.Drawdown) As Parameter.Drawdown
        Dim BO As IApplication
        BO = ComponentFactory.CreateApplication()
        Return BO.InstallmentDrawDownList(odrawdown)
    End Function
    Public Function GetApplicationDeduction(ByVal customClass As Parameter.Application) As Parameter.Application
        Dim ApplicationDeduction As IApplication
        ApplicationDeduction = ComponentFactory.CreateApplication()
        Return ApplicationDeduction.GetApplicationDeduction(customClass)
    End Function
    Public Function GetCustomerFacility(ByVal oCustomClass As Parameter.Application) As Integer
        Dim CountAppliationId As IApplication
        CountAppliationId = ComponentFactory.CreateApplication()
        Return CountAppliationId.GetCustomerFacility(oCustomClass)
    End Function
    Public Function WayOfPaymentList(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim CountAppliationId As IApplication
        CountAppliationId = ComponentFactory.CreateApplication
        Return CountAppliationId.WayOfPaymentList(oCustomClass)
    End Function
End Class
