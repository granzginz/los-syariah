﻿Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class ModalKerjaInvoiceController

    Public Function GetInvoiceList(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
        Dim oInterface As IModalKerjaInvoice
        oInterface = ComponentFactory.CreateModalKerjaInvoice
        Return oInterface.GetInvoiceList(oCustomClass)
    End Function
    Public Function GetInvoiceList2(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
        Dim oInterface As IModalKerjaInvoice
        oInterface = ComponentFactory.CreateModalKerjaInvoice
        Return oInterface.GetInvoiceList2(oCustomClass)
    End Function
    Public Function GetJatuhTempoList(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
        Dim oInterface As IModalKerjaInvoice
        oInterface = ComponentFactory.CreateModalKerjaInvoice
        Return oInterface.GetJatuhTempoList(oCustomClass)
    End Function

    Public Function GetInvoiceDetail(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
        Dim oInterface As IModalKerjaInvoice
        oInterface = ComponentFactory.CreateModalKerjaInvoice
        Return oInterface.GetInvoiceDetail(ocustomClass)
    End Function

    Public Function InvoiceSaveAdd(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
        Dim oInterface As IModalKerjaInvoice
        oInterface = ComponentFactory.CreateModalKerjaInvoice
        Return oInterface.InvoiceSaveAdd(ocustomClass)
    End Function

    Public Function InvoiceSaveEdit(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
        Dim oInterface As IModalKerjaInvoice
        oInterface = ComponentFactory.CreateModalKerjaInvoice
        Return oInterface.InvoiceSaveEdit(ocustomClass)
    End Function

    Public Function InvoiceSaveAdd2(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
        Dim oInterface As IModalKerjaInvoice
        oInterface = ComponentFactory.CreateModalKerjaInvoice
        Return oInterface.InvoiceSaveAdd2(ocustomClass)
    End Function

    Public Function InvoiceSaveEdit2(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
        Dim oInterface As IModalKerjaInvoice
        oInterface = ComponentFactory.CreateModalKerjaInvoice
        Return oInterface.InvoiceSaveEdit2(ocustomClass)
    End Function

    Public Function InvoiceDelete(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
        Dim oInterface As IModalKerjaInvoice
        oInterface = ComponentFactory.CreateModalKerjaInvoice
        Return oInterface.InvoiceDelete(ocustomClass)
    End Function

    Public Function InvoiceSaveApplication(ByVal ocustomClass As Parameter.Application, ByVal oDataAmortisasi As DataTable) As String
        Dim oInterface As IModalKerjaInvoice
        oInterface = ComponentFactory.CreateModalKerjaInvoice
        Return oInterface.InvoiceSaveApplication(ocustomClass, oDataAmortisasi)
    End Function

    Public Function GetFactoringFee(ByVal ocustomClass As Parameter.Application) As Parameter.ModalKerjaInvoice
        Dim oInterface As IModalKerjaInvoice
        oInterface = ComponentFactory.CreateModalKerjaInvoice
        Return oInterface.GetFactoringFee(ocustomClass)
    End Function

    Public Function FactoringApplicationFinancialSave(ByVal ocustomClass As Parameter.Application) As String
        Dim oInterface As IModalKerjaInvoice
        oInterface = ComponentFactory.CreateModalKerjaInvoice
        Return oInterface.FactoringApplicationFinancialSave(ocustomClass)
    End Function

    Public Function ModalKerjaInvoiceDisburse(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
        Dim oInterface As IModalKerjaInvoice
        oInterface = ComponentFactory.CreateModalKerjaInvoice
        Return oInterface.ModalKerjaInvoiceDisburse(ocustomClass)
    End Function

    Public Function GetDataByLastPaymentDate(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
        Dim oInterface As IModalKerjaInvoice
        oInterface = ComponentFactory.CreateModalKerjaInvoice
        Return oInterface.GetDataByLastPaymentDate(oCustomClass)
    End Function

    Public Function GetPaymentHistory(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
        Dim oInterface As IModalKerjaInvoice
        oInterface = ComponentFactory.CreateModalKerjaInvoice
        Return oInterface.GetPaymentHistory(oCustomClass)
    End Function

    Public Function GetInvoiceDetail2(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
        Dim oInterface As IModalKerjaInvoice
        oInterface = ComponentFactory.CreateModalKerjaInvoice
        Return oInterface.GetInvoiceDetail2(ocustomClass)
    End Function
End Class
