﻿Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface
Public Class CreditAssesmentController
    Dim oInterface As INewAppInsurance
    Public Sub New()
        oInterface = ComponentFactory.CreateNewAppInsurance
    End Sub

    Public Function getCreditAssesment(ByVal oCustomClass As Parameter.CreditAssesment) As Parameter.CreditAssesment 
        Return oInterface.getCreditAssesment(oCustomClass)
    End Function
    Public Sub CreditAssesmentSave(ByVal oCustomClass As Parameter.CreditAssesment) 
        oInterface.CreditAssesmentSave(oCustomClass)
    End Sub
    Public Function Proceed(ByVal oCustomClass As Parameter.CreditAssesment) As String
        Return oInterface.Proceed(oCustomClass)
    End Function
    Public Function getApprovalSchemeID(cnn As String, appId As String) As String
        Return oInterface.getApprovalSchemeID(cnn, appId)
    End Function
End Class
