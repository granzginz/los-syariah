

#Region "Imports"


Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region


Public Class NewAppInsuranceController
    Public Function getNewAppInsuranceListing(ByVal Customclass As Parameter.NewAppInsurance) As Parameter.NewAppInsurance
        Dim getNewAppInsuranceListingBO As INewAppInsurance
        getNewAppInsuranceListingBO = ComponentFactory.CreateNewAppInsurance
        Return getNewAppInsuranceListingBO.getNewAppInsuranceListing(Customclass)
    End Function

End Class
