
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class FinancialDataController

    'modify kpr

    Function getDataKPRHeader(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.getDataKPRHeader(oCustomClass)
    End Function
    Function getDataKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.getDataKPR(oCustomClass)
    End Function
    Function HitungAngsuranKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.HitungAngsuranKPR(oCustomClass)
    End Function
    Function GetDataGridKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.GetDataGridKPR(oCustomClass)
    End Function
    Function DeleteAngsuranKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.DeleteAngsuranKPR(oCustomClass)
    End Function
    Function SaveKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.SaveKPR(oCustomClass)
    End Function
    Function SaveFinancialDataTab2EndKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.SaveFinancialDataTab2EndKPR(oCustomClass)
    End Function
    'end



    Function GetFinancialData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.GetFinancialData(oCustomClass)
    End Function
    Function GetDataDrop(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.GetDataDrop(oCustomClass)
    End Function
    Function GetFinancialData_002(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.GetFinancialData_002(oCustomClass)
    End Function
    Function GetInstallmentSchedule(ByVal ocustomClass As Parameter.FinancialData) As DataTable
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.GetInstallmentSchedule(ocustomClass)
    End Function
    Function GetInstAmtAdv(ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intNumInstallment As Double, ByVal dblNTF As Double) As Double
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.GetInstAmtAdv(dblRunRate, intPayFreq, intNumInstallment, dblNTF)
    End Function
    Function GetInstAmtArr(ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intNumInstallment As Double, ByVal dblNTF As Double) As Double
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.GetInstAmtArr(dblRunRate, intPayFreq, intNumInstallment, dblNTF)
    End Function


    Function GetInstAmtAdvFlat(ByVal FlatRate As Double, ByVal intPayFreq As Integer, ByVal intNumInstallment As Double, ByVal dblNTF As Double) As Double
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.GetInstAmtAdvFlatRate(FlatRate, intPayFreq, intNumInstallment, dblNTF)
    End Function


    Function GetEffectiveRateArr(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer) As Double
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.GetEffectiveRateArr(intNumInstallment, dblInstAmt, dblNTF, intPayFreq)
    End Function
    Function GetEffectiveRateAdv(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer) As Double
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.GetEffectiveRateAdv(intNumInstallment, dblInstAmt, dblNTF, intPayFreq)
    End Function
    Function GetEffectiveRateRO(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As Double
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.GetEffectiveRateRO(intNumInstallment, dblInstAmt, dblNTF, intPayFreq, intGrace)
    End Function
    Function GetInstAmtArrRO(ByVal EffectiveRate As Double, ByVal intNumInstallment As Integer, ByVal Runrate As Double, ByVal NTF As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As Double
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.GetInstAmtArrRO(EffectiveRate, intNumInstallment, Runrate, NTF, intPayFreq, intGrace)
    End Function
    Function GetTerm(ByVal intInstTerm As Integer) As Integer
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.GetTerm(intInstTerm)
    End Function
    Function GetFlatRate(ByVal dblInterestTotal As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer, ByVal Tenor As Integer) As Double
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.GetFlatRate(dblInterestTotal, dblNTF, intPayFreq, Tenor)
    End Function
    Function SaveFinancialData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.SaveFinancialData(oCustomClass)
    End Function
    Function GetViewFinancialData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.GetViewFinancialData(oCustomClass)
    End Function
    Function ClearingInstallmentSchedule(ByVal oCustomClass As Parameter.FinancialData) As String
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.ClearingInstallmentSchedule(oCustomClass)
    End Function
    Function GetPrincipleAmount(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.GetPrincipleAmount(oCustomClass)
    End Function
    Function SaveAmortisasi(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.SaveAmortisasi(oCustomClass)
    End Function
    Function SaveAmortisasiSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.SaveAmortisasiSiml(oCustomClass)
    End Function
    Function SaveAmortisasiIRR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.SaveAmortisasiIRR(oCustomClass)
    End Function
    Function SaveAmortisasiIRRSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.SaveAmortisasiIRRSiml(oCustomClass)
    End Function
    Function GetNewEffRateIRR(ByVal oCustomClass As Parameter.FinancialData) As Double
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.GetNewEffRateIRR(oCustomClass)
    End Function
    Function GetStepUpStepDownType(ByVal oCustomClass As Parameter.FinancialData) As String
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.GetStepUpStepDownType(oCustomClass)
    End Function
    Function SaveAmortisasiStepUpDown(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.SaveAmortisasiStepUpDown(oCustomClass)
    End Function
    Function SaveAmortisasiStepUpDownSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.SaveAmortisasiStepUpDownSiml(oCustomClass)
    End Function
    Function GetInstAmtStepUpDownRegLeasing(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.GetInstAmtStepUpDownRegLeasing(oCustomClass)
    End Function
    Function GetStepUpDownRegLeasingTBL(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.GetStepUpDownRegLeasingTBL(oCustomClass)
    End Function
    Function GetNewEffRateRegLeasing(ByVal oCustomClass As Parameter.FinancialData) As Double
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.GetNewEffRateRegLeasing(oCustomClass)
    End Function
    Function GetNewEffRateLeasing(ByVal oCustomClass As Parameter.FinancialData) As Double
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.GetNewEffRateLeasing(oCustomClass)
    End Function
    Function ListAmortisasiStepUpDown(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.ListAmortisasiStepUpDown(oCustomClass)
    End Function
    Function ListAmortisasiStepUpDownSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.ListAmortisasiStepUpDownSiml(oCustomClass)
    End Function
    Sub saveDefaultAdminFee(ByVal oCustomClass As Parameter.FinancialData)
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        BO.saveDefaultAdminFee(oCustomClass)
    End Sub
    Sub saveDefaultAgentFee(ByVal oCustomClass As Parameter.FinancialData)
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        BO.saveDefaultAgentFee(oCustomClass)
    End Sub
    Function SaveFinancialDataSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.SaveFinancialDataSiml(oCustomClass)
    End Function
    Function GetInstallmentSchSiml(ByVal oCustomClass As Parameter.FinancialData) As DataSet
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.GetInstallmentSchSiml(oCustomClass)
    End Function

    Function GetRefundPremiumToSupplier(ByVal data As Parameter.Supplier) As Parameter.Supplier
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.GetRefundPremiumToSupplier(data)
    End Function

    Function saveBungaNett(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.saveBungaNett(oCustomClass)
    End Function

    Function ListAmortisasiIRR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.ListAmortisasiIRR(oCustomClass)
    End Function

    Function SaveFinancialDataTab2(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.SaveFinancialDataTab2(oCustomClass)
    End Function

    Function SaveFinancialDataTab2End(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.SaveFinancialDataTab2End(oCustomClass)
    End Function

    Function SaveFinancialDataTab2OperatingLease(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.SaveFinancialDataTab2OperatingLease(oCustomClass)
    End Function

    Function SaveFinancialDataTab2EndOperatingLease(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.SaveFinancialDataTab2EndOperatingLease(oCustomClass)
    End Function

    Function GoLiveJournalDummyOperatingLease(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim BO As IFinancialData
        BO = ComponentFactory.CreateFinancialData
        Return BO.GoLiveJournalDummyOperatingLease(oCustomClass)
    End Function

    Function SaveFinancialDataOperatingLease(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim FinancialDataBO As IFinancialData
        FinancialDataBO = ComponentFactory.CreateFinancialData()
        Return FinancialDataBO.SaveFinancialDataOperatingLease(oCustomClass)
    End Function

    Function GetViewInstallmentResch(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData

    End Function

End Class
