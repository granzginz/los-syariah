﻿Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface

Public Class HasilSurveyController
    Public Function getHasilSurvey(ByVal oCustomClass As Parameter.HasilSurvey) As Parameter.HasilSurvey
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        Return oInterface.getHasilSurvey(oCustomClass)
    End Function

    Public Sub HasilSurveySave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        oInterface.HasilSurveySave(oCustomClass)
    End Sub

    Public Function GetApplicationReject(ByVal oCustomClass As Parameter.HasilSurvey) As Parameter.HasilSurvey
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        Return oInterface.GetApplicationReject(oCustomClass)
    End Function

    Public Sub ApplicationRejectUpdate(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        oInterface.ApplicationRejectUpdate(oCustomClass)
    End Sub

    Public Sub HasilSurveyJournalSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        oInterface.HasilSurveyJournalSave(oCustomClass)
    End Sub
    Public Sub Proceed(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        oInterface.Proceed(oCustomClass)
    End Sub

    Public Sub HasilSurvey002Save(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        oInterface.HasilSurvey002Save(oCustomClass)
    End Sub

    Public Sub HasilSurvey002CharacterSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        oInterface.HasilSurvey002CharacterSave(oCustomClass)
    End Sub

    Public Sub HasilSurvey002CapacitySave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        oInterface.HasilSurvey002CapacitySave(oCustomClass)
    End Sub

    Public Sub HasilSurvey002ConditionSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        oInterface.HasilSurvey002ConditionSave(oCustomClass)
    End Sub

    Public Sub HasilSurvey002CapitalSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        oInterface.HasilSurvey002CapitalSave(oCustomClass)
    End Sub

    Public Sub HasilSurvey002CollateralSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        oInterface.HasilSurvey002CollateralSave(oCustomClass)
    End Sub

    Public Sub HasilSurvey002CatatanSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        oInterface.HasilSurvey002CatatanSave(oCustomClass)
    End Sub

    Public Sub HasilSurvey002KYCSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        oInterface.HasilSurvey002KYCSave(oCustomClass)
    End Sub

    Public Sub HasilSurvey002CharacterCLSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        oInterface.HasilSurvey002CharacterCLSave(oCustomClass)
    End Sub

    Public Sub HasilSurvey002AplStepSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        oInterface.HasilSurvey002AplStepSave(oCustomClass)
    End Sub

    Public Function getHasilMobileSurvey(ByVal oCustomClass As Parameter.HasilSurvey) As Parameter.HasilSurvey
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        Return oInterface.getHasilMobileSurvey(oCustomClass)
    End Function

    Public Function getHasilScore(ByVal scoringData As Parameter.HasilSurvey) As Parameter.HasilSurvey
        Dim oInterface As IHasilSurvey
        oInterface = ComponentFactory.CreateHasilSurvey
        Return oInterface.getHasilScore(scoringData)
    End Function
End Class
