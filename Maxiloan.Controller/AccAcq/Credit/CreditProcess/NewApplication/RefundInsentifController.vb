﻿Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class RefundInsentifController
    Public Function getDistribusiNilaiInsentif(ByVal oCustomClass As Parameter.RefundInsentif) As Parameter.RefundInsentif
        Dim oInterface As IRefundInsentif
        oInterface = ComponentFactory.CreateRefundInsentif
        Return oInterface.getDistribusiNilaiInsentif(oCustomClass)
    End Function

    Sub SupplierINCTVDailyDAdd(ByVal oCustomClass As Parameter.RefundInsentif)
        Dim oIncentiveCard As IRefundInsentif
        oIncentiveCard = ComponentFactory.CreateRefundInsentif()
        oIncentiveCard.SupplierINCTVDailyDAdd(oCustomClass)
    End Sub

    Public Function GetSPBy(ByVal ocustomclass As Parameter.RefundInsentif) As Parameter.RefundInsentif
        Dim oInterface As IRefundInsentif
        oInterface = ComponentFactory.CreateRefundInsentif
        Return oInterface.GetSPBy(ocustomclass)
    End Function

    Public Function SupplierIncentiveDailyDSave(ByVal customclass As Parameter.RefundInsentif) As String
        Dim oIncentiveCard As IRefundInsentif
        oIncentiveCard = ComponentFactory.CreateRefundInsentif()
        Return oIncentiveCard.SupplierIncentiveDailyDSave(customclass)
    End Function

    Public Sub SupplierIncentiveDailyDDelete(ByVal customclass As Parameter.RefundInsentif)
        Dim oIncentiveCard As IRefundInsentif
        oIncentiveCard = ComponentFactory.CreateRefundInsentif()
        oIncentiveCard.SupplierIncentiveDailyDDelete(customclass)
    End Sub

    Public Sub AlokasiInsentifInternalDelete(ByVal customclass As Parameter.RefundInsentif)
        Dim oIncentiveCard As IRefundInsentif
        oIncentiveCard = ComponentFactory.CreateRefundInsentif()
        oIncentiveCard.AlokasiInsentifInternalDelete(customclass)
    End Sub

    Public Sub AlokasiInsentifInternalSave(ByVal customclass As Parameter.RefundInsentif)
        Dim oIncentiveCard As IRefundInsentif
        oIncentiveCard = ComponentFactory.CreateRefundInsentif()
        oIncentiveCard.AlokasiInsentifInternalSave(customclass)
    End Sub
    Public Function UpdateDateEntryInsentif(ByVal customclass As Parameter.RefundInsentif) As String
        Dim oIncentiveCard As IRefundInsentif
        oIncentiveCard = ComponentFactory.CreateRefundInsentif()
        Return oIncentiveCard.UpdateDateEntryInsentif(customclass)
    End Function

    Public Function GoLiveJournalDummy(ByVal customClass As Parameter.RefundInsentif) As Parameter.RefundInsentif
        Dim oIncentiveCard As IRefundInsentif
        oIncentiveCard = ComponentFactory.CreateRefundInsentif
        Return oIncentiveCard.GoLiveJournalDummy(customClass)
    End Function

    'Public Function SupplierIncentiveDailyDSaveFactoring(ByVal customclass As Parameter.RefundInsentif) As String
    '    Dim oIncentiveCard As IRefundInsentif
    '    oIncentiveCard = ComponentFactory.CreateRefundInsentif()
    '    Return oIncentiveCard.SupplierIncentiveDailyDSaveFactoring(customclass)
    'End Function
    'Sub SupplierINCTVDailyDAddFACT(ByVal oCustomClass As Parameter.RefundInsentif)
    '    Dim oIncentiveCard As IRefundInsentif
    '    oIncentiveCard = ComponentFactory.CreateRefundInsentif()
    '    oIncentiveCard.SupplierINCTVDailyDAddFACT(oCustomClass)
    'End Sub
    'Sub SupplierINCTVDailyDAddMDKJ(ByVal oCustomClass As Parameter.RefundInsentif)
    '    Dim oIncentiveCard As IRefundInsentif
    '    oIncentiveCard = ComponentFactory.CreateRefundInsentif()
    '    oIncentiveCard.SupplierINCTVDailyDAddMDKJ(oCustomClass)
    'End Sub
End Class
