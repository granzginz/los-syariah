

#Region "Imports"

Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region


Public Class InsuranceCalculationResultController

#Region "DisplayResultInsCalculationStep1"
    Public Function DisplayResultInsCalculationStep1(ByVal Customclass As Parameter.InsuranceCalculationResult) As Parameter.InsuranceCalculationResult
        Dim BO As IInsuranceCalculationResult
        BO = ComponentFactory.CreateInsuranceCalculationResult
        Return BO.DisplayResultInsCalculationStep1(Customclass)
    End Function
#End Region

#Region "DisplayResultOnGrid"

    Public Function DisplayResultOnGrid(ByVal CustomClass As Parameter.InsuranceCalculationResult) As Parameter.InsuranceCalculationResult

        Dim BO As IInsuranceCalculationResult
        BO = ComponentFactory.CreateInsuranceCalculationResult
        Return BO.DisplayResultOnGrid(CustomClass)
    End Function

    Function DisplayResultOnGridAdditional(ByVal customClass As Parameter.InsuranceCalculationResult, ByVal InsuranceProductID As String) As Parameter.InsuranceCalculationResult
        Dim BO As IInsuranceCalculationResult
        BO = ComponentFactory.CreateInsuranceCalculationResult
        Return BO.DisplayResultOnGridAdditional(customClass, InsuranceProductID)
    End Function


#End Region


End Class
