
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AssetDataController
    Public Function GetAssetData(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.GetAssetData(oCustomClass)
    End Function
    Function GetCboEmp(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.GetCboEmp(oCustomClass)
    End Function
    Function GetCboEmpReport(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.GetCboEmpReport(oCustomClass)
    End Function
    Function GetCbo(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.GetCbo(oCustomClass)
    End Function
    Function GetAttribute(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.GetAttribute(oCustomClass)
    End Function
    Function GetSerial(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.GetSerial(oCustomClass)
    End Function
    Function GetUsedNew(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim BO As IAssetData
        BO = ComponentFactory.CreateAssetData()
        Return BO.GetUsedNew(oCustomClass)
    End Function

    Function GetAO(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.GetAO(oCustomClass)
    End Function
    Function GetSupplierGroupID(ByVal oCustomClass As Parameter.AssetData) As String
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.GetSupplierGroupID(oCustomClass)
    End Function
    Function CheckSerial(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.CheckSerial(oCustomClass)
    End Function
    Function CheckAttribute(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.CheckAttribute(oCustomClass)
    End Function
    Function GetAssetDoc(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.GetAssetDoc(oCustomClass)
    End Function
    Function CheckAssetDoc(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.CheckAssetDoc(oCustomClass)
    End Function
    Function AssetDataSaveAdd(ByVal oAssetData As Parameter.AssetData, _
      ByVal oAddress As Parameter.Address, ByVal oData1 As DataTable, _
      ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.AssetData
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.AssetDataSaveAdd(oAssetData, oAddress, oData1, oData2, oData3)
    End Function
    Function GetAssetID(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.GetAssetID(oCustomClass)
    End Function
    Function GetViewAssetData(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.GetViewAssetData(oCustomClass)
    End Function
    Function EditAssetDataGetDefaultSupplierID(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim BO As IAssetData
        BO = ComponentFactory.CreateAssetData()
        Return BO.EditAssetDataGetDefaultSupplierID(oCustomClass)
    End Function
    Function EditGetAttribute(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.EditGetAttribute(oCustomClass)
    End Function
    Function EditGetAssetRegistration(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim BO As IAssetData
        BO = ComponentFactory.CreateAssetData()
        Return BO.EditGetAssetRegistration(oCustomClass)
    End Function
    Function EditAssetInsuranceEmployee(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim BO As IAssetData
        BO = ComponentFactory.CreateAssetData()
        Return BO.EditAssetInsuranceEmployee(oCustomClass)
    End Function
    Function EditAssetDataDocument(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim Bo As IAssetData
        Bo = ComponentFactory.CreateAssetData()
        Return Bo.EditAssetDataDocument(oCustomClass)
    End Function
    Function GetcboAlasanPenolakan(ByVal oCustomClass As Parameter.TolakanNonCust) As Parameter.TolakanNonCust
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.GetcboAlasanPenolakan(oCustomClass)
    End Function
    Function TolakanNonCustSaveAdd(ByVal oAssetData As Parameter.TolakanNonCust, ByVal oAddress As Parameter.Address) As Parameter.TolakanNonCust
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.TolakanNonCustSaveAdd(oAssetData, oAddress)
    End Function
  
    Public Function GetTolakanNonCustPaging(ByVal oCustomClass As Parameter.TolakanNonCust) As Parameter.TolakanNonCust
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.GetTolakanNonCustPaging(oCustomClass)
    End Function

    Public Function GetTolakanNonCustEdit(ByVal oCustomClass As Parameter.TolakanNonCust) As Parameter.TolakanNonCust
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.GetTolakanNonCustEdit(oCustomClass)
    End Function
    Public Function GetTolakanNonCustDelete(ByVal oCustomClass As Parameter.TolakanNonCust) As String
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.GetTolakanNonCustDelete(oCustomClass)
    End Function

    Public Function ListReportTolakanNonCust(ByVal oCustomClass As Parameter.TolakanNonCust) As Parameter.TolakanNonCust
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.ListReportTolakanNonCust(oCustomClass)
    End Function

    Public Function GetGradeAsset(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData
        Return AssetDataBO.GetGradeAsset(oCustomClass)
    End Function
    Function GetAssetDocWhere(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim AssetDataBO As IAssetData
        AssetDataBO = ComponentFactory.CreateAssetData()
        Return AssetDataBO.GetAssetDocWhere(oCustomClass)
    End Function


End Class
