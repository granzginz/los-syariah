

#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class InsuranceApplicationController

#Region "ProcessSaveDataInsuranceApplicationHeaderAndDetail"

    Public Function ProcessSaveDataInsuranceApplicationHeaderAndDetail(ByVal oDataTable As DataTable, ByVal customClass As Parameter.InsuranceCalculation) As Parameter.InsuranceCalculation
        Dim BO As IInsuranceCalculation
        BO = ComponentFactory.CreateInsuranceCalculation
        Return BO.ProcessSaveDataInsuranceApplicationHeaderAndDetail(oDataTable, customClass)
    End Function

#End Region

#Region "ProcessNewAppInsuranceByCompanySaveAddTemporary"
    Public Function ProcessNewAppInsuranceByCompanySaveAddTemporary(ByVal Customclass As Parameter.InsuranceCalculation) As String
        Dim BO As IInsuranceCalculation
        BO = ComponentFactory.CreateInsuranceCalculation
        Return BO.ProcessNewAppInsuranceByCompanySaveAddTemporary(Customclass)
    End Function
#End Region

#Region "ProcessNewAppInsuranceByCompanyDetailSaveAddTemporary"
    Public Function ProcessNewAppInsuranceByCompanyDetailSaveAddTemporary(ByVal Customclass As Parameter.InsuranceCalculation) As String
        Dim BO As IInsuranceCalculation
        BO = ComponentFactory.CreateInsuranceCalculation
        Return BO.ProcessNewAppInsuranceByCompanyDetailSaveAddTemporary(Customclass)
    End Function

    Public Function ProcessAppInsuranceAgricultureDetailSave(ByVal Customclass As Parameter.InsuranceCalculation) As String
        Dim BO As IInsuranceCalculation
        BO = ComponentFactory.CreateInsuranceCalculation
        Return BO.ProcessAppInsuranceAgricultureDetailSave(Customclass)
    End Function
#End Region

    Public Function ProcessSaveInsuranceApplicationLastProcess(ByVal customClass As Parameter.InsuranceCalculationResult) As String
        Dim BO As IInsuranceCalculation
        BO = ComponentFactory.CreateInsuranceCalculation
        Return BO.ProcessSaveInsuranceApplicationLastProcess(customClass)
    End Function



    Public Function ProcessKoreksiAsuransi(ByVal customClass As Parameter.InsuranceCalculationResult) As String
        Dim BO As IInsuranceCalculation
        BO = ComponentFactory.CreateInsuranceCalculation
        Return BO.ProcessKoreksiAsuransi(customClass)
    End Function

    'GetDateEntryInsuranceData
    Public Function GetDateEntryInsuranceData(ByVal customClass As Parameter.InsuranceCalculation) As Parameter.InsuranceCalculation
        Dim BO As IInsuranceCalculation
        BO = ComponentFactory.CreateInsuranceCalculation
        Return BO.GetDateEntryInsuranceData(customClass)
    End Function

    Public Function DisplaySelectedPremiumOnGrid(ByVal CustomClass As Parameter.InsuranceCalculation) As Parameter.InsuranceCalculation

        Dim BO As IInsuranceCalculation
        BO = ComponentFactory.CreateInsuranceCalculation
        Return BO.DisplaySelectedPremiumOnGrid(CustomClass)
    End Function

End Class
