

#Region "Imports"

Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region

Public Class NewAppInsuranceByCustController

    Public Function GetInsuredByCustomer(ByVal Customclass As Parameter.NewAppInsuranceByCust) As Parameter.NewAppInsuranceByCust
        Dim BO As INewAppInsuranceByCust
        BO = ComponentFactory.CreateNewAppInsuranceByCust
        Return BO.GetInsuredByCustomer(Customclass)
    End Function

    Public Function ProcessSaveInsuranceByCustomer(ByVal customClass As Parameter.NewAppInsuranceByCust) As String
        Dim BO As INewAppInsuranceByCust
        BO = ComponentFactory.CreateNewAppInsuranceByCust
        Return BO.ProcessSaveInsuranceByCustomer(customClass)
    End Function


End Class
