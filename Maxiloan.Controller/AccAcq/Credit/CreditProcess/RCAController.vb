
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class RCAController

    Function GetRCA(ByVal oCustomClass As Parameter.RCA) As Parameter.RCA
        Dim RCABO As IRCA
        RCABO = ComponentFactory.CreateRCA()
        Return RCABO.GetRCA(oCustomClass)
    End Function

    Function RCASave(ByVal oRCA As Parameter.RCA, ByVal oData1 As DataTable, _
    ByVal oData2 As DataTable) As Parameter.RCA
        Dim RCABO As IRCA
        RCABO = ComponentFactory.CreateRCA()
        Return RCABO.RCASave(oRCA, oData1, oData2)
    End Function


    Function GetApprovalBM(ByVal oCustomClass As Parameter.RCA) As Parameter.RCA
        Dim RCABO As IRCA
        RCABO = ComponentFactory.CreateRCA()
        Return RCABO.GetApprovalBM(oCustomClass)
    End Function

    Function ApprovalBMSave(ByVal oCustomClass As Parameter.RCA, Optional isApr As Boolean = True) As Parameter.RCA
        Dim RCABO As IRCA
        RCABO = ComponentFactory.CreateRCA()
        Return RCABO.ApprovalBMSave(oCustomClass, isApr)
    End Function

    Function GetCreditAssesmentList(ByVal oCustomClass As Parameter.RCA) As Parameter.RCA
        Dim RCABO As IRCA
        RCABO = ComponentFactory.CreateRCA()
        Return RCABO.GetCreditAssesmentList(oCustomClass)
    End Function
End Class
