
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AgreementCancellationController
    Public Sub ProcessAgreementCancellation(ByVal oCustomClass As Parameter.AgreementCancellation)
        Dim AgreementCancellationBO As IAgreementCancellation
        Try
            AgreementCancellationBO = ComponentFactory.CreateAgreementCancellation()
            If AgreementCancellationBO Is Nothing Then
                Throw New Exception("ERROR: Unable to create AgreementCancellation component.")
            End If
            AgreementCancellationBO.ProcessAgreementCancellation(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub ProcessGoLiveCancellation(ByVal customClass As Parameter.AgreementCancellation)

    End Sub
End Class
