
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class POController
    Public Function POPaging(ByVal oCustomClass As Parameter.PO) As Parameter.PO
        Dim POBO As IPO
        POBO = ComponentFactory.CreatePO()
        Return POBO.POPaging(oCustomClass)
    End Function

    Public Function POSave(ByVal customClass As Parameter.PO) As String
        Dim POBO As IPO
        POBO = ComponentFactory.CreatePO
        Return POBO.POSave(customClass)
    End Function

    'Public Function POSave(ByVal customClass As Parameter.PO, ByVal oData1 As DataTable, ByVal oData2 As DataTable) As String
    '    Dim POBO As IPO
    '    POBO = ComponentFactory.CreatePO
    '    Return POBO.POSave(customClass, oData1, oData2)
    'End Function

    Public Function PO_Kepada(ByVal customClass As Parameter.PO) As Parameter.PO
        Dim POBO As IPO
        POBO = ComponentFactory.CreatePO
        Return POBO.PO_Kepada(customClass)
    End Function

    Public Function PO_DikirimKe(ByVal customClass As Parameter.PO) As Parameter.PO
        Dim POBO As IPO
        POBO = ComponentFactory.CreatePO
        Return POBO.PO_DikirimKe(customClass)
    End Function

    Public Function PO_ItemFee(ByVal customClass As Parameter.PO) As Parameter.PO
        Dim POBO As IPO
        POBO = ComponentFactory.CreatePO
        Return POBO.PO_ItemFee(customClass)
    End Function

    Public Function Get_Combo_SupplierAccount(ByVal customClass As Parameter.PO) As Parameter.PO
        Dim POBO As IPO
        POBO = ComponentFactory.CreatePO
        Return POBO.Get_Combo_SupplierAccount(customClass)
    End Function

    Public Function Get_PONumber(ByVal customClass As Parameter.PO) As String
        Dim POBO As IPO
        POBO = ComponentFactory.CreatePO
        Return POBO.Get_PONumber(customClass)
    End Function

    Public Function PO_Account(ByVal customClass As Parameter.PO) As Parameter.PO
        Dim POBO As IPO
        POBO = ComponentFactory.CreatePO
        Return POBO.PO_Account(customClass)
    End Function

    Public Function POExtendPaging(ByVal oCustomClass As Parameter.PO) As Parameter.PO
        Dim POBO As IPO
        POBO = ComponentFactory.CreatePO()
        Return POBO.POExtendPaging(oCustomClass)
    End Function

    Public Sub POExtendSaveEdit(ByVal customClass As Parameter.PO)
        Dim POBO As IPO
        POBO = ComponentFactory.CreatePO()
        POBO.POExtendSaveEdit(customClass)
    End Sub

    Public Function POViewExtend(ByVal customClass As Parameter.PO) As Parameter.PO
        Dim POBO As IPO
        POBO = ComponentFactory.CreatePO
        Return POBO.POViewExtend(customClass)
    End Function

    Public Function GetIDKurs(ByVal customclass As Parameter.PO) As Parameter.PO
        Dim POBO As IPO
        POBO = ComponentFactory.CreatePO
        Return POBO.GetIDKurs(customclass)
    End Function
End Class
