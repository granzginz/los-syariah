
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class EditApplicationController
    Public Function GetApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IEditApplication
        ApplicationBO = ComponentFactory.ApplicationMaintenance()
        Return ApplicationBO.GetApplication(oCustomClass)
    End Function
    Public Function GetCountAppliationId(ByVal oCustomClass As Parameter.Application) As Integer
        Dim CountAppliationId As IEditApplication
        CountAppliationId = ComponentFactory.ApplicationMaintenance()
        Return CountAppliationId.GetCountAppliationId(oCustomClass)
    End Function
    Function GetFee(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IEditApplication
        ApplicationBO = ComponentFactory.ApplicationMaintenance()
        Return ApplicationBO.GetFee(oCustomClass)
    End Function
    Function GetCopyFrom(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IEditApplication
        ApplicationBO = ComponentFactory.ApplicationMaintenance()
        Return ApplicationBO.GetCopyFrom(oCustomClass)
    End Function
    Function GetTC(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IEditApplication
        ApplicationBO = ComponentFactory.ApplicationMaintenance()
        Return ApplicationBO.GetTC(oCustomClass)
    End Function
    Function GetTC2(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IEditApplication
        ApplicationBO = ComponentFactory.ApplicationMaintenance()
        Return ApplicationBO.GetTC2(oCustomClass)
    End Function
    Function GetAddress(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IEditApplication
        ApplicationBO = ComponentFactory.ApplicationMaintenance()
        Return ApplicationBO.GetAddress(oCustomClass)
    End Function
    Function GetShowDataAgreement(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IEditApplication
        ApplicationBO = ComponentFactory.ApplicationMaintenance()
        Return ApplicationBO.GetShowDataAgreement(oCustomClass)
    End Function
    Function ApplicationSaveAdd(ByVal oApplication As Parameter.Application, _
       ByVal oRefPersonal As Parameter.Personal, _
       ByVal oRefAddress As Parameter.Address, ByVal oMailingAddress As Parameter.Address, _
       ByVal oData1 As DataTable, ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.Application
        Dim ApplicationBO As IEditApplication
        ApplicationBO = ComponentFactory.ApplicationMaintenance()
        Return ApplicationBO.ApplicationSaveAdd(oApplication, oRefPersonal, oRefAddress, oMailingAddress, _
                oData1, oData2, oData3)
    End Function
    Function GetViewApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IEditApplication
        ApplicationBO = ComponentFactory.ApplicationMaintenance()
        Return ApplicationBO.GetViewApplication(oCustomClass)
    End Function
    Function GetApplicationMaintenance(ByVal customClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IEditApplication
        ApplicationBO = ComponentFactory.ApplicationMaintenance()
        Return ApplicationBO.GetApplicationMaintenance(customClass)
    End Function
    Function GetCD(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IEditApplication
        ApplicationBO = ComponentFactory.ApplicationMaintenance()
        Return ApplicationBO.GetCD(oCustomClass)
    End Function
    Function GetApplicationEdit(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As IEditApplication
        ApplicationBO = ComponentFactory.ApplicationMaintenance()
        Return ApplicationBO.GetApplicationEdit(oCustomClass)
    End Function
    Function ApplicationSaveEdit(ByVal oApplication As Parameter.Application, _
       ByVal oRefPersonal As Parameter.Personal, _
       ByVal oRefAddress As Parameter.Address, ByVal oMailingAddress As Parameter.Address, _
       ByVal oData1 As DataTable, ByVal oData2 As DataTable, ByVal oData3 As DataTable, _
       ByVal MyDataTable As DataTable) As Parameter.Application
        Dim ApplicationBO As IEditApplication
        ApplicationBO = ComponentFactory.ApplicationMaintenance()
        Return ApplicationBO.ApplicationSaveEdit(oApplication, oRefPersonal, oRefAddress, oMailingAddress, _
                oData1, oData2, oData3, MyDataTable)
    End Function
    Function EditAssetDataGetInfoAssetData(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim BO As IEditApplication
        BO = ComponentFactory.ApplicationMaintenance()
        Return BO.EditAssetDataGetInfoAssetData(oCustomClass)
    End Function
    Function AssetDataGetInfoAssetDataAwal(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim BO As IEditApplication
        BO = ComponentFactory.ApplicationMaintenance()
        Return BO.AssetDataGetInfoAssetDataAwal(oCustomClass)
    End Function
End Class
