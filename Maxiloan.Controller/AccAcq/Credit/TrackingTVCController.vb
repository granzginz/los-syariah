
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class TrackingTVCController
    Public Function GetApplication(ByVal ocustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As ITrackingTVC
        ApplicationBO = ComponentFactory.CreateTrackingTVC()
        Return ApplicationBO.GetApplication(ocustomClass)
    End Function

    Public Function GetApplicatioList(ByVal ocustomClass As Parameter.Application) As Parameter.Application
        Dim ApplicationBO As ITrackingTVC
        ApplicationBO = ComponentFactory.CreateTrackingTVC()
        Return ApplicationBO.GetApplicatioList(ocustomClass)
    End Function
   
    Public Sub ApplicationSaveEdit(ByVal ocustomClass As Parameter.Application)
        Dim ApplicationBO As ITrackingTVC
        ApplicationBO = ComponentFactory.CreateTrackingTVC()
        ApplicationBO.ApplicationSaveEdit(ocustomClass)
    End Sub

    Public Function GetCombo(ByVal ocustomClass As Parameter.Application) As DataTable
        Dim ApplicationBO As ITrackingTVC
        ApplicationBO = ComponentFactory.CreateTrackingTVC()
        Return ApplicationBO.GetCombo(ocustomClass)
    End Function
End Class
