Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class PernyataanSetujuController
    Public Function listPernyataanSetuju(ByVal customClass As Parameter.PernyataanSetuju) As Parameter.PernyataanSetuju
        Dim oPernyataanSetuju As IPernyataanSetuju
        oPernyataanSetuju = ComponentFactory.CreatePernyataanSetuju()
        Return oPernyataanSetuju.listPernyataanSetuju(customClass)
    End Function

    Public Function savePrintPernyataanSetuju(ByVal customClass As Parameter.PernyataanSetuju) As Parameter.PernyataanSetuju
        Dim oPernyataanSetuju As IPernyataanSetuju
        oPernyataanSetuju = ComponentFactory.CreatePernyataanSetuju()
        Return oPernyataanSetuju.savePrintPernyataanSetuju(customClass)
    End Function

End Class
