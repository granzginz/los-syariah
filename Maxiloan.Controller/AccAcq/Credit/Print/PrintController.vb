Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Public Class PrintController
    Public Function getAgentFeeReceipt(ByVal customClass As Parameter.AgentFeeReceipt) As Parameter.AgentFeeReceipt
        Dim oInterface As IPrint
        oInterface = ComponentFactory.CreateAgentFeeReceipt()
        Return oInterface.getAgentFeeReceipt(customClass)
    End Function
    Public Function saveAgentFeeReceiptPrint(ByVal customClass As Parameter.AgentFeeReceipt) As Parameter.AgentFeeReceipt
        Dim oInterface As IPrint
        oInterface = ComponentFactory.CreateAgentFeeReceipt()
        Return oInterface.saveAgentFeeReceiptPrint(customClass)
    End Function
    Public Function getCreditFundingReceipt(ByVal customClass As Parameter.CreditFundingReceipt) As Parameter.CreditFundingReceipt
        Dim oInterface As IPrint
        oInterface = ComponentFactory.CreateCreditFundingReceipt()
        Return oInterface.getCreditFundingReceipt(customClass)
    End Function
    Public Function saveCreditFundingReceiptPrint(ByVal customClass As Parameter.CreditFundingReceipt) As Parameter.CreditFundingReceipt
        Dim oInterface As IPrint
        oInterface = ComponentFactory.CreateCreditFundingReceipt()
        Return oInterface.saveCreditFundingReceiptPrint(customClass)
    End Function
    Public Function getSuratKuasaFiducia(ByVal customClass As Parameter.SuratKuasaFiducia) As Parameter.SuratKuasaFiducia
        Dim oInterface As IPrint
        oInterface = ComponentFactory.CreateSuratKuasaFiducia()
        Return oInterface.getSuratKuasaFiducia(customClass)
    End Function
    Public Function saveSuratKuasaPrint(ByVal customClass As Parameter.SuratKuasaFiducia) As Parameter.SuratKuasaFiducia
        Dim oInterface As IPrint
        oInterface = ComponentFactory.CreateSuratKuasaFiducia()
        Return oInterface.saveSuratKuasaPrint(customClass)
    End Function
    Public Function getMKK(ByVal customClass As Parameter.Common) As DataSet
        Dim oInterface As IPrint
        oInterface = ComponentFactory.CreateAgentFeeReceipt()
        Return oInterface.getMKK(customClass)
    End Function
    Public Function getMKKReport(ByVal customClass As Parameter.MKK) As Parameter.MKK
        Dim oInterface As IPrint
        oInterface = ComponentFactory.CreateAgentFeeReceipt()
        Return oInterface.getMKKReport(customClass)
    End Function

    Public Function listAgreementMailForPrint(ByVal customClass As Parameter.AgreementList) As DataTable
        Dim oInterface As IPrint
        oInterface = ComponentFactory.CreateAgentFeeReceipt()
        Return oInterface.listAgreementMailForPrint(customClass)
    End Function

    Public Function saveAgreementMailForPrint(ByVal customClass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oInterface As IPrint
        oInterface = ComponentFactory.CreateAgentFeeReceipt()
        Return oInterface.saveAgreementMailForPrint(customClass)
    End Function

    Public Function printAgreementMailForPrint(ByVal customClass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oInterface As IPrint
        oInterface = ComponentFactory.CreateAgentFeeReceipt()
        Return oInterface.printAgreementMailForPrint(customClass)
    End Function
End Class
