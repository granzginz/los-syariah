

#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class PPKController

#Region "List Agreement"
    Public Function ListAgreement(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListAgreement(Customclass)
    End Function

    Public Function ListAgreementFL(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListAgreementFL(Customclass)
    End Function

#End Region
#Region "Save Print"
    Public Function SavePrint(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.SavePrint(Customclass)
    End Function

    Public Function SavePrintFL(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.SavePrintFL(Customclass)
    End Function
#End Region
#Region "List PPK Report"
    Public Function ListPPKReport(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListPPKReport(Customclass)
    End Function

    Public Function ListPPKReportFL(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListPPKReportFL(Customclass)
    End Function
#End Region

    '=================================== Print Surat Kuasa =============================================
#Region "Save Print Surat Kuasa"
    Public Function SavePrintSuratKuasa(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.SavePrintSuratKuasa(Customclass)
    End Function
#End Region
#Region "List Surat Kuasa Report"
    Public Function ListSuratKuasaReport(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListSuratKuasaReport(Customclass)
    End Function
#End Region

    '=================================== Print PO ===============================
#Region "Save Print PO"
    Public Function SavePrintPO(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.SavePrintPO(Customclass)
    End Function
#End Region
#Region "List PO Report"
    Public Function ListReportPO(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListReportPO(Customclass)
    End Function
#End Region

    '================================= Print Kwitansi ========================
#Region "Save Print Kwitansi"
    Public Function SavePrintKwitansi(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.SavePrintKwitansi(Customclass)
    End Function
#End Region
#Region "List Print Kwitansi"
    Public Function ListPrintKwitansi(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListPrintKwitansi(Customclass)
    End Function
#End Region

    Public Function ListPrintBASTK(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListPrintBASTK(Customclass)
    End Function

    '================================= Print Slip Setoran Bank ========================
#Region "List Account"
    Public Function ListAccount(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListAccount(Customclass)
    End Function
#End Region
#Region "Save Print Slip Setoran Bank"
    Public Function SavePrintSlipSetoranBank(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.SavePrintSlipSetoranBank(Customclass)
    End Function
#End Region
#Region "List Print SSB"
    Public Function ListPrintSSB(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListPrintSSB(Customclass)
    End Function
#End Region
#Region "GetBankID"
    Public Function GetBankID(ByVal oCustomClass As Parameter.PPK) As String
        Dim AssetModel As IPPK
        AssetModel = ComponentFactory.CreatePPK
        Return AssetModel.GetBankID(oCustomClass)
    End Function
#End Region

    '================================= ReportAgingByModel ================================
#Region "GetAssetModel"
    Public Function GetAssetModel(ByVal oCustomClass As Parameter.PPK) As DataTable
        Dim AssetModel As IPPK
        AssetModel = ComponentFactory.CreatePPK
        Return AssetModel.GetAssetModel(oCustomClass)
    End Function
#End Region
#Region "GetRptAging"
    Public Function ViewReportAging(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim ReportAging As IPPK
        ReportAging = ComponentFactory.CreatePPK
        Return ReportAging.ViewReportAging(oCustomClass)
    End Function
#End Region
#Region "GetAssetType"
    Public Function GetAssetType(ByVal oCustomClass As Parameter.PPK) As DataTable
        Dim AssetType As IPPK
        AssetType = ComponentFactory.CreatePPK
        Return AssetType.GetAssetType(oCustomClass)
    End Function
#End Region
#Region "GetCreditScoringCustomer"
    Public Function GetCreditScoringCustomer(ByVal oCustomClass As Parameter.PPK) As DataTable
        Dim CreditScoringCustomer As IPPK
        CreditScoringCustomer = ComponentFactory.CreatePPK
        Return CreditScoringCustomer.GetCreditScoringCustomer(oCustomClass)
    End Function
#End Region

    '================================= ReportAmortization ========================================
#Region "ListRptAmortization"
    Public Function ListRptAmortization(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListRptAmortization(oCustomClass)
    End Function

#End Region

    '================================= Report Confirmation Letter =============================
#Region "ListReportConfLetter"
    Public Function ListReportConfLetter(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListReportConfLetter(oCustomClass)
    End Function

#End Region
#Region "List Confirmation Letter"
    Public Function ListConfLetter(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListConfLetter(Customclass)
    End Function
#End Region

#Region "List Waad Letter"
    Public Function ListWaadLetter(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListWaadLetter(Customclass)
    End Function
#End Region

#Region "List Wakalah Letter"
    Public Function ListWakalahLetter(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListWakalahLetter(Customclass)
    End Function
#End Region
#Region "ListReportConfLetter"
    Public Function ListReportWakalahLetter(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK
        Return oPPK.ListReportWakalahLetter(oCustomClass)
    End Function

#End Region


#Region "List PKS"
    Public Function ListPKS(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListPKS(Customclass)
    End Function
#End Region
#Region "Save Print Confirmation Letter"
    Public Function SavePrintConfLetter(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.SavePrintConfLetter(Customclass)
    End Function
#End Region
#Region "Save Print Waad Letter"
    Public Function SavePrintWaadLetter(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.SavePrintWaadLetter(Customclass)
    End Function
#End Region
#Region "Save Print Wakalah Letter"
    Public Function SavePrintWakalahLetter(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.SavePrintWakalahLetter(Customclass)
    End Function
#End Region

    '================================== Report Bon Hijau ==============================
#Region "ListBonHijau"
    Public Function ListBonHijau(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListBonHijau(oCustomClass)
    End Function

#End Region
#Region "ListBonHijauReport"
    Public Function ListBonHijauReport(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListBonHijauReport(Customclass)
    End Function
#End Region
#Region "Save Print Bon Hijau"
    Public Function SavePrintBonHijau(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.SavePrintBonHijau(Customclass)
    End Function
#End Region

    '================================= Report Tanda Terima Dokumen =============================
#Region "ListReportTandaTerimaDokumen"
    Public Function ListReportTandaTerimaDokumen(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListReportTandaTerimaDokumen(oCustomClass)
    End Function


    Public Function ListReportSuratPernyataanNasabah(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListReportSuratPernyataanNasabah(oCustomClass)
    End Function
    Public Function ListReportPernyataanNPWP(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListReportPernyataanNPWP(oCustomClass)
    End Function

    Public Function ListReportPernyataanBedaTandaTangan(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListReportPernyataanBedaTandaTangan(oCustomClass)
    End Function

    Public Function ListReportSuratPenolakan(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListReportSuratPenolakan(oCustomClass)
    End Function

    Public Function ListReportBastk(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListReportBastk(oCustomClass)
    End Function

    Public Function ListReportSPTransfer(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListReportSPTransfer(oCustomClass)
    End Function

    Public Function ListReportGesekanRangkaMesin(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListReportGesekanRangkaMesin(oCustomClass)
    End Function

#End Region


#Region "List TandaTerimaDokumen"
    Public Function ListTandaTerimaDokumen(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListTandaTerimaDokumen(Customclass)
    End Function
#End Region
    Public Function ListSuratPernyataanNasabah(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListSuratPernyataanNasabah(customclass)
    End Function
    Public Function ListPernyataanNPWP(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListPernyataanNPWP(Customclass)
    End Function

    Public Function ListPernyataanBedaTandaTangan(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListPernyataanBedaTandaTangan(Customclass)
    End Function

    Public Function ListSuratPenolakan(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListSuratPenolakan(Customclass)
    End Function

    Public Function ListBastk(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListBastk(Customclass)
    End Function

    Public Function ListSuratPermohonanTransfer(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListSuratPermohonanTransfer(Customclass)
    End Function

    Public Function ListGesekanRangkaMesin(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListGesekanRangkaMesin(Customclass)
    End Function


    '================================= Report Kelengkapan Data =============================
#Region "ListReportKelengkapanData"
    Public Function ListReportKelengkapanData(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListReportKelengkapanData(oCustomClass)
    End Function

#End Region
#Region "List KelengkapanData"
    Public Function ListKelengkapanData(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListKelengkapanData(Customclass)
    End Function
#End Region

    '================================= Listing Surat Kontrak =============================
#Region "ListListingSuratKontrak"
    Public Function ListListingSuratKontrak(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListListingSuratKontrak(oCustomClass)
    End Function

#End Region
#Region "ListReportListingSuratKontrak"
    Public Function ListReportListingSuratKontrak(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListReportListingSuratKontrak(Customclass)
    End Function
#End Region
#Region "GetCMO"
    Public Function GetCMO(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim AssetModel As IPPK
        AssetModel = ComponentFactory.CreatePPK
        Return AssetModel.GetCMO(oCustomClass)
    End Function
#End Region

    '================================= Report OM =============================
#Region "ListReportOM"
    Public Function ListReportOM(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListReportOM(oCustomClass)
    End Function

#End Region

    Public Function ListAccountByPayment(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListAccountByPayment(customclass)
    End Function

    '================================= Report Persetujuan Pengalihan Kreditor =============================
#Region "ListReportPPKreditor"
    Public Function ListReportPPKreditor(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListReportPPKreditor(oCustomClass)
    End Function

#End Region
#Region "List PPKreditor"
    Public Function ListPPKreditor(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListPPKreditor(Customclass)
    End Function
#End Region
#Region "Save Print PPKreditor"
    Public Function SavePrintPPKreditor(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.SavePrintPPKreditor(Customclass)
    End Function
#End Region

    '================================= Report Welcome Letter =============================
#Region "ListReportWLetter"
    Public Function ListReportWLetter(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListReportWLetter(oCustomClass)
    End Function

#End Region
#Region "List WLetter"
    Public Function ListWLetter(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListWLetter(Customclass)
    End Function
#End Region
#Region "Save Print WLetter"
    Public Function SavePrintWLetter(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.SavePrintWLetter(Customclass)
    End Function
#End Region
    Public Function SavePrintSuratPernyataanNasabah(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.SavePrintSuratPernyataanNasabah(customclass)
    End Function

#Region "ListReportWLetterSubReport"
    Public Function ListReportWLetterSubRpt(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListReportWLetterSubRpt(oCustomClass)
    End Function

#End Region
    '================================= Report Surat Kuasa Eksekusi dan Penjualan Obyek Jaminan Fidusia =============================
#Region "ListReportSKEPO"
    Public Function ListReportSKEPO(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListReportSKEPO(oCustomClass)
    End Function

#End Region
#Region "List SKEPO"
    Public Function ListSKEPO(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListSKEPO(Customclass)
    End Function
#End Region
#Region "Save Print SKEPO"
    Public Function SavePrintSKEPO(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.SavePrintSKEPO(Customclass)
    End Function
#End Region

    '=================================== Print PO Karoseri===============================
#Region "Save Print PO Karoseri"
    Public Function SavePrintPOKaroseri(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.SavePrintPOKaroseri(Customclass)
    End Function
#End Region
#Region "List PO Report Karoseri"
    Public Function ListReportPOKaroseri(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListReportPOKaroseri(Customclass)
    End Function
#End Region


    '================================= Report SK Potong Gaji =============================
#Region "ListReportSKPotongGaji"
    Public Function ListReportSKPotongGaji(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListReportSKPotongGaji(oCustomClass)
    End Function

#End Region
#Region "List SKPotongGaji"
    Public Function ListSKPotongGaji(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.ListSKPotongGaji(Customclass)
    End Function
#End Region
#Region "Save Print SKPotongGaji"
    Public Function SavePrintSKPotongGaji(ByVal Customclass As Parameter.PPK) As Parameter.PPK
        Dim oPPK As IPPK
        oPPK = ComponentFactory.CreatePPK()
        Return oPPK.SavePrintSKPotongGaji(Customclass)
    End Function
#End Region
#Region "ListReportSKPotongGajiSubReport"
    Public Function ListReportSKPotongGajiSubRpt(ByVal oCustomClass As Parameter.PPK) As Parameter.PPK
        Dim RptAmortization As IPPK
        RptAmortization = ComponentFactory.CreatePPK
        Return RptAmortization.ListReportSKPotongGajiSubRpt(oCustomClass)
    End Function

#End Region


End Class
