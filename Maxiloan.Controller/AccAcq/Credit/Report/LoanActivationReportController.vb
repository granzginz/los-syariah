Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class LoanActivationReportController
    Public Function listLoanActivationReport(ByVal customClass As Parameter.LoanActivationReport) As Parameter.LoanActivationReport
        Dim oLoanActivationReport As ILoanActivationReport
        oLoanActivationReport = ComponentFactory.CreateLoanActivation()
        Return oLoanActivationReport.listLoanActivationReport(customClass)
    End Function
    Public Function listDailySalesReport(ByVal customClass As Parameter.LoanActivationReport) As Parameter.LoanActivationReport
        Dim oLoanActivationReport As ILoanActivationReport
        oLoanActivationReport = ComponentFactory.CreateLoanActivation()
        Return oLoanActivationReport.listDailySalesReport(customClass)
    End Function
    Public Function ListAgreementDownloadReport(ByVal customClass As Parameter.LoanActivationReport) As Parameter.LoanActivationReport
        Dim oLoanActivationReport As ILoanActivationReport
        oLoanActivationReport = ComponentFactory.CreateLoanActivation()
        Return oLoanActivationReport.listAgreementDownloadReport(customClass)
    End Function
End Class
