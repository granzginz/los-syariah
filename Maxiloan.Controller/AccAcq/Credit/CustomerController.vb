
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Parameter

Public Class CustomerController
    Public Function GetCustomerFromProspect(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetCustomerFromProspect(Customclass)
    End Function
    Public Function GetCustomer(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetCustomer(Customclass)
    End Function
    Public Function CustomerPersonalGetIDType(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.CustomerPersonalGetIDType(Customclass)
    End Function

    Public Function CustomerCompanyGetIDType(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.CustomerCompanyGetIDType(Customclass)
    End Function

    Public Function CustomerPersonalAdd(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.CustomerPersonalAdd(Customclass)
    End Function
    Public Function BindCustomer1_002(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.BindCustomer1_002(Customclass)
    End Function
    Public Function BindCustomer2_002(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.BindCustomer2_002(Customclass)
    End Function
    Public Function BindCustomerC1_002(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.BindCustomerC1_002(Customclass)
    End Function
    Public Function BindCustomerC2_002(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.BindCustomerC2_002(Customclass)
    End Function
    Public Function PersonalSaveAdd(ByVal oCustomer As Parameter.Customer, _
                                    ByVal oLegalAddress As Parameter.Address, _
                                    ByVal oResAddress As Parameter.Address, _
                                    ByVal oJDAddress As Parameter.Address, _
                                    ByVal oDataOmset As DataTable, _
                                    ByVal oDataFamily As DataTable, _
                                    ByVal oPCEX As Parameter.CustomerEX, _
                                    ByVal oPCSI As Parameter.CustomerSI,
                                    ByVal oPCSIAddress As Parameter.Address, _
                                    ByVal oPCER As Parameter.CustomerER, _
                                    ByVal oPCERAddress As Parameter.Address) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.PersonalSaveAdd(oCustomer, _
                                          oLegalAddress, _
                                          oResAddress, _
                                          oJDAddress, _
                                          oDataOmset, _
                                          oDataFamily, _
                                          oPCEX, _
                                          oPCSI, _
                                          oPCSIAddress, _
                                          oPCER, _
                                          oPCERAddress)
    End Function
    Public Function PersonalSaveEdit(ByVal oCustomer As Parameter.Customer, _
                                     ByVal oLegalAddress As Parameter.Address, _
                                     ByVal oResAddress As Parameter.Address, _
                                     ByVal oJDAddress As Parameter.Address, _
                                     ByVal oDataOmset As DataTable, _
                                     ByVal oDataFamily As DataTable, _
                                     ByVal oPCEX As Parameter.CustomerEX, _
                                     ByVal oPCSI As Parameter.CustomerSI, _
                                     ByVal oPCSIAddress As Parameter.Address, _
                                     ByVal oPCER As Parameter.CustomerER, _
                                     ByVal oPCERAddress As Parameter.Address) As String
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.PersonalSaveEdit(oCustomer, _
                                           oLegalAddress, _
                                           oResAddress, _
                                           oJDAddress, _
                                           oDataOmset, _
                                           oDataFamily, _
                                           oPCEX, _
                                           oPCSI, _
                                           oPCSIAddress, _
                                           oPCER, _
                                           oPCERAddress)
    End Function
    Function CompanySaveAdd(ByVal oCustomer As Parameter.Customer, _
                            ByVal oAddress As Parameter.Address, _
                            ByVal oWarehouseAddress As Parameter.Address, _
                            ByVal oData1 As DataTable, _
                            ByVal oData2 As DataTable, _
                            ByVal CompanyCustomerDPEx As Parameter.CompanyCustomerDPEx, _
                            ByVal CompanyCustomerPJ As Parameter.CompanyCustomerPJ, _
                            ByVal CompanyCustomerPJAddress As Parameter.Address,
                             ByVal companySIUPAddress As Parameter.Address) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.CompanySaveAdd(oCustomer, _
                                         oAddress, _
                                         oWarehouseAddress, _
                                         oData1, _
                                         oData2, _
                                         CompanyCustomerDPEx, _
                                         CompanyCustomerPJ, _
                                         CompanyCustomerPJAddress,
                                         companySIUPAddress)
    End Function
    Function CompanyDataPerusahaanSaveAdd(ByVal oCustomer As Parameter.Customer, _
                                   ByVal oAddress As Parameter.Address, _
                                   ByVal oWarehouseAddress As Parameter.Address, _
                                   ByVal CompanyCustomerDPEx As Parameter.CompanyCustomerDPEx, _
                                   ByVal companySIUPAddress As Parameter.Address
                                   ) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.CompanyDataPerusahaanSaveAdd(oCustomer, _
                                         oAddress, _
                                         oWarehouseAddress, _
                                         CompanyCustomerDPEx, _
                                         companySIUPAddress)
    End Function
    Function CompanySaveEdit(ByVal oCustomer As Parameter.Customer, _
                             ByVal oAddress As Parameter.Address, _
                             ByVal oWarehouseAddress As Parameter.Address, _
                             ByVal oData1 As DataTable, _
                             ByVal oData2 As DataTable, _
                             ByVal CompanyCustomerDPEx As Parameter.CompanyCustomerDPEx, _
                             ByVal CompanyCustomerPJ As Parameter.CompanyCustomerPJ, _
                             ByVal CompanyCustomerPJAddress As Parameter.Address,
                              ByVal companySIUPAddress As Parameter.Address) As String
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.CompanySaveEdit(oCustomer, _
                                         oAddress, _
                                         oWarehouseAddress, _
                                         oData1, _
                                         oData2, _
                                         CompanyCustomerDPEx, _
                                         CompanyCustomerPJ, _
                                         CompanyCustomerPJAddress,
                                         companySIUPAddress)
    End Function
    Function CompanyDataPerusahaanSaveEdit(ByVal oCustomer As Parameter.Customer, _
                                    ByVal oAddress As Parameter.Address, _
                                    ByVal oWarehouseAddress As Parameter.Address, _
                                    ByVal CompanyCustomerDPEx As Parameter.CompanyCustomerDPEx, _
                                    ByVal companySIUPAddress As Parameter.Address) As String
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.CompanyDataPerusahaanSaveEdit(oCustomer, _
                                         oAddress, _
                                         oWarehouseAddress, _
                                         CompanyCustomerDPEx, _
                                         companySIUPAddress)
    End Function
    Function CompanyKeuanganSaveEdit(ByVal oCustomer As Parameter.Customer) As String
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.CompanyKeuanganSaveEdit(oCustomer)
    End Function
    Function CompanyLegalitasSaveEdit(ByVal oCustomer As Parameter.Customer,
                                    ByVal oData2 As DataTable) As String
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.CompanyLegalitasSaveEdit(oCustomer, oData2)
    End Function
    Function CompanyManagementSaveEdit(ByVal oCustomer As Parameter.Customer, _
                                    ByVal oData1 As DataTable, _
                                    ByVal CompanyCustomerPJ As Parameter.CompanyCustomerPJ, _
                                    ByVal CompanyCustomerPJAddress As Parameter.Address) As String
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.CompanyManagementSaveEdit(oCustomer, oData1, CompanyCustomerPJ, CompanyCustomerPJAddress)
    End Function
    Function GetCustomerEdit(ByVal oCustomClass As Parameter.Customer, ByVal Level As String) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetCustomerEdit(oCustomClass, Level)
    End Function
    Sub Generate_XMLP(ByVal pStrFile As String, ByVal pStrGelarDepan As String, ByVal pStrName As String, ByVal pStrGelarBelakang As String,
                         ByVal pStrType As String, ByVal pStrIDType As String, ByVal pStrIDTypeName As String,
                         ByVal pStrNumber As String, ByVal IDExpiredDate As String, ByVal pStrGender As String,
                         ByVal pStrBirthPlace As String, ByVal pStrBirthDate As String,
                         ByRef pStrSuccess As Boolean, ByVal pMotherName As String,
                         ByVal pKartuKeluarga As String, ByVal pNamaPasangan As String, ByVal pStatusPerkawinan As String)
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        CustomerBO.Generate_XMLP(pStrFile, pStrGelarDepan, pStrName, pStrGelarBelakang, pStrType, pStrIDType, pStrIDTypeName,
                          pStrNumber, IDExpiredDate, pStrGender, pStrBirthPlace, pStrBirthDate,
                          pStrSuccess, pMotherName, pKartuKeluarga, pNamaPasangan, pStatusPerkawinan)
    End Sub
    Sub Generate_XMLC(ByVal pStrFile As String, ByVal pStrName As String, _
                          ByVal pStrNPWP As String, ByVal pStrAddress As String, ByVal pStrRT As String, _
                          ByVal pStrRW As String, ByVal pStrKelurahan As String, _
                          ByVal pStrKecamatan As String, ByVal pStrCity As String, _
                          ByVal pStrZipCode As String, ByVal pStrAPhone1 As String, _
                          ByVal pStrPhone1 As String, ByVal pStrAPhone2 As String, _
                          ByVal pStrPhone2 As String, ByVal pStrAFax As String, _
                          ByVal pStrFax As String, ByRef pStrSuccess As Boolean)
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        CustomerBO.Generate_XMLC(pStrFile, pStrName, pStrNPWP, pStrAddress, pStrRT, _
                           pStrRW, pStrKelurahan, pStrKecamatan, pStrCity, _
                           pStrZipCode, pStrAPhone1, pStrPhone1, pStrAPhone2, _
                           pStrPhone2, pStrAFax, pStrFax, pStrSuccess)
    End Sub


#Region "View Customer"
    Public Function GetCustomerType(ByVal Customclass As Parameter.Customer) As String
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetCustomerType(Customclass)
    End Function

    Public Function GetViewCustomer(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetViewCustomer(Customclass)
    End Function

    Public Function GetViewCustomerEntOmset(ByVal Customclass As Parameter.Customer) As DataTable
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetViewCustomerEntOmset(Customclass)
    End Function

    Public Function GetViewCustomerEmpOmset(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetViewCustomerEmpOmset(Customclass)
    End Function

    Public Function GetViewCustomerFamily(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetViewCustomerFamily(Customclass)
    End Function

    Public Function GetViewCustomer_Summary(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetViewCustomer_Summary(Customclass)
    End Function

    Public Function GetViewCustomerEntOmset_Summary(ByVal Customclass As Parameter.Customer) As DataTable
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetViewCustomerEntOmset_Summary(Customclass)
    End Function

    Public Function GetViewCustomerEmpOmset_Summary(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetViewCustomerEmpOmset_Summary(Customclass)
    End Function

    Public Function GetViewCustomerFamily_Summary(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetViewCustomerFamily_Summary(Customclass)
    End Function
    Public Function GetViewCompanyCustomer(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetViewCompanyCustomer(Customclass)
    End Function

    Public Function GetViewCompanyCustomerManagement(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetViewCompanyCustomerManagement(Customclass)
    End Function

    Public Function GetViewCompanyCustomerDocument(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetViewCompanyCustomerDocument(Customclass)
    End Function
    Public Function GetAgreementListCompRpt(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetAgreementListCompRpt(Customclass)
    End Function
    Public Function GetAgreementListCompRpt_Summary(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetAgreementListCompRpt_Summary(Customclass)
    End Function
    Public Function GetAgreementListSummary1(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetAgreementListSummary1(Customclass)
    End Function
    Public Function GetAgreementListSummary2(ByVal Customclass As Parameter.Customer) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetAgreementListSummary2(Customclass)
    End Function

#End Region


    Public Function GetTotalCustomer(ByVal strCustomerID As String, ByVal strConnection As String) As Integer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.GetTotalCustomer(strCustomerID, strConnection)
    End Function

    Public Function BindCustomer1_002_withSI(ByVal Customclass As Parameter.Customer, ByVal SIData As Parameter.CustomerSI) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.BindCustomer1_002_withSI(Customclass, SIData)
    End Function

    Public Function PersonalCustomerIdentitasAdd(ByVal oCustomer As Parameter.Customer, _
                                    ByVal oLegalAddress As Parameter.Address, _
                                    ByVal oResAddress As Parameter.Address,
                                    ByVal oPCEX As Parameter.CustomerEX) As Parameter.Customer
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.PersonalCustomerIdentitasAdd(oCustomer, _
                                          oLegalAddress, _
                                          oResAddress, _
                                          oPCEX)
    End Function
    Public Function PersonalCustomerPekerjaanSaveEdit(ByVal oCustomer As Parameter.Customer, _
                                     ByVal oJDAddress As Parameter.Address, _
                                     ByVal oPCEX As Parameter.CustomerEX) As String
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.PersonalCustomerPekerjaanSaveEdit(oCustomer, _
                                           oJDAddress, _
                                           oPCEX)
    End Function
    Public Function PersonalCustomerKeuanganSaveEdit(ByVal oCustomer As Parameter.Customer) As String
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.PersonalCustomerKeuanganSaveEdit(oCustomer)
    End Function
    Public Function PersonalCustomerPasanganSaveEdit(ByVal oCustomer As Parameter.Customer, _
                                     ByVal oPCSI As Parameter.CustomerSI, _
                                     ByVal oPCSIAddress As Parameter.Address) As String
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.PersonalCustomerPasanganSaveEdit(oCustomer, _
                                           oPCSI, _
                                           oPCSIAddress)
    End Function

    Public Function PersonalCustomerGuarantorSaveEdit(ByVal oCustomer As Parameter.Customer, _
                                    ByVal oPCSI As Parameter.CustomerSI, _
                                    ByVal oPCSIAddress As Parameter.Address) As String
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.PersonalCustomerGuarantorSaveEdit(oCustomer, _
                                           oPCSI, _
                                           oPCSIAddress)
    End Function
    Public Function PersonalCustomerEmergencySaveEdit(ByVal oCustomer As Parameter.Customer, _
                                     ByVal oPCER As Parameter.CustomerER, _
                                     ByVal oPCERAddress As Parameter.Address) As String
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.PersonalCustomerEmergencySaveEdit(oCustomer, _
                                           oPCER, _
                                           oPCERAddress)
    End Function
    Public Function PersonalCustomerKeluargaSaveEdit(ByVal oCustomer As Parameter.Customer, _
                                     ByVal oDataFamily As DataTable) As String
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.PersonalCustomerKeluargaSaveEdit(oCustomer, _
                                           oDataFamily)
    End Function
    Public Function PersonalCustomerOLSaveEdit(cnn As String, custid As String, data As IList(Of PinjamanLain)) As String
        '(ByVaoCustomerl  As Parameter.Customer,   ByVal oPersonalCustumerOL As DataTable) As String
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.PersonalCustomerOLSaveEdit(cnn, custid, data)
    End Function

    Public Function PersonalCustomerIdentitasSaveEdit(ByVal oCustomer As Parameter.Customer, _
                                    ByVal oLegalAddress As Parameter.Address, _
                                    ByVal oResAddress As Parameter.Address,
                                    ByVal oPCEX As Parameter.CustomerEX) As String
        Dim CustomerBO As ICustomer
        CustomerBO = ComponentFactory.CreateCustomer()
        Return CustomerBO.PersonalCustomerIdentitasSaveEdit(oCustomer, _
                                          oLegalAddress, _
                                          oResAddress, _
                                          oPCEX)
    End Function
    Public Function LoadCombo(cnn As String, ty As String) As IList(Of Parameter.CommonValueText)
        Return CType(ComponentFactory.CreateCustomer(), ICustomer).LoadCombo(cnn, ty)
    End Function

    Public Function GetCompanyCustomerEC(cnn As String, oCustomClass As Parameter.CompanyCustomerEC) As Parameter.CompanyCustomerEC
        Return CType(ComponentFactory.CreateCustomer(), ICustomer).GetCompanyCustomerEC(cnn, oCustomClass)
    End Function

    Public Function CompanyCustomerECSaveEdit(cnn As String, oCustomClass As Parameter.CompanyCustomerEC) As String
        Return CType(ComponentFactory.CreateCustomer(), ICustomer).CompanyCustomerECSaveEdit(cnn, oCustomClass)
    End Function

    Public Function GetCompanyCustomerGuarantor(cnn As String, customerid As String) As IList(Of Parameter.CompanyCustomerGuarantor)
        Return CType(ComponentFactory.CreateCustomer(), ICustomer).GetCompanyCustomerGuarantor(cnn, customerid)
    End Function

    Public Function CompanyCustomerGuarantorSaveEdit(cnn As String, customerid As String, ccGuarantors As IList(Of Parameter.CompanyCustomerGuarantor)) As String
        Return CType(ComponentFactory.CreateCustomer(), ICustomer).CompanyCustomerGuarantorSaveEdit(cnn, customerid, ccGuarantors)
    End Function
    Public Function GetPersonalOL(cnn As String, custId As String) As IList(Of PinjamanLain)
        Dim oPersonalOl As ICustomer
        oPersonalOl = ComponentFactory.CreateCustomer()
        Return oPersonalOl.GetPersonalOL(cnn, custId)
    End Function

	Public Function CheckPersonalCustomerDocument(ByVal Customclass As Parameter.Customer) As Integer
		Dim CustomerBO As ICustomer
		CustomerBO = ComponentFactory.CreateCustomer()
		Return CustomerBO.CheckPersonalCustomerDocument(Customclass)
	End Function

	Public Function GetCompanyCustomerPK(cnn As String, oCustomClass As Parameter.CompanyCustomerPK) As Parameter.CompanyCustomerPK
		Return CType(ComponentFactory.CreateCustomer(), ICustomer).GetCompanyCustomerPK(cnn, oCustomClass)
	End Function

	Function CompanyCustomerPKSaveAdd(ByVal oCustomClass As Parameter.CompanyCustomerPK) As Parameter.CompanyCustomerPK
		Dim CustomerBO As ICustomer
		CustomerBO = ComponentFactory.CreateCustomer()
		Return CustomerBO.CompanyCustomerPKSaveAdd(oCustomClass)
	End Function

	Function CompanyCustomerPKSaveEdit(ByVal oCustomClass As Parameter.CompanyCustomerPK) As Parameter.CompanyCustomerPK
		Dim CustomerBO As ICustomer
		CustomerBO = ComponentFactory.CreateCustomer()
		Return CustomerBO.CompanyCustomerPKSaveEdit(oCustomClass)
	End Function
End Class
