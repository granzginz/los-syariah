
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class CustomerGroupController
    Public Function GetCustomerGroup(ByVal ocustomClass As Parameter.CustomerGroup) As Parameter.CustomerGroup
        Dim CustomerGroupBO As ICustomerGroup
        CustomerGroupBO = ComponentFactory.CreateCustomerGroup()
        Return CustomerGroupBO.GetCustomerGroup(ocustomClass)
    End Function
    Public Function GetCustomerGroupReport(ByVal ocustomClass As Parameter.CustomerGroup) As Parameter.CustomerGroup
        Dim CustomerGroupBO As ICustomerGroup
        CustomerGroupBO = ComponentFactory.CreateCustomerGroup()
        Return CustomerGroupBO.GetCustomerGroupReport(ocustomClass)
    End Function
    Public Function GetCustomerGroupListCustomer(ByVal ocustomClass As Parameter.CustomerGroup) As Parameter.CustomerGroup
        Dim CustomerGroupBO As ICustomerGroup
        CustomerGroupBO = ComponentFactory.CreateCustomerGroup()
        Return CustomerGroupBO.GetCustomerGroupListCustomer(ocustomClass)
    End Function
    Public Function GetCustomerGroupList(ByVal ocustomClass As Parameter.CustomerGroup) As Parameter.CustomerGroup
        Dim CustomerGroupBO As ICustomerGroup
        CustomerGroupBO = ComponentFactory.CreateCustomerGroup()
        Return CustomerGroupBO.GetCustomerGroupList(ocustomClass)
    End Function

    Public Function CustomerGroupSaveAdd(ByVal ocustomClass As Parameter.CustomerGroup) As String
        Dim CustomerGroupBO As ICustomerGroup
        CustomerGroupBO = ComponentFactory.CreateCustomerGroup()
        Return CustomerGroupBO.CustomerGroupSaveAdd(ocustomClass)
    End Function

    Public Sub CustomerGroupSaveEdit(ByVal ocustomClass As Parameter.CustomerGroup)
        Dim CustomerGroupBO As ICustomerGroup
        CustomerGroupBO = ComponentFactory.CreateCustomerGroup()
        CustomerGroupBO.CustomerGroupSaveEdit(ocustomClass)
    End Sub

    Public Function CustomerGroupDelete(ByVal ocustomClass As Parameter.CustomerGroup) As String
        Dim CustomerGroupBO As ICustomerGroup
        CustomerGroupBO = ComponentFactory.CreateCustomerGroup()
        Return CustomerGroupBO.CustomerGroupDelete(ocustomClass)
    End Function

End Class
