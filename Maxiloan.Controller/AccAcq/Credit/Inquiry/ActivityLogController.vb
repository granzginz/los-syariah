
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class ActivityLogController

#Region "List Activity User"
    Public Function ListActivityUser(ByVal Customclass As Parameter.ActivityLog) As Parameter.ActivityLog
        Dim oActivity As IActivityLog
        oActivity = ComponentFactory.CreateActivityLog
        Return oActivity.ListActivityUser(Customclass)
    End Function
#End Region

#Region "List Activity"
    Public Function ListActivity(ByVal Customclass As Parameter.ActivityLog) As Parameter.ActivityLog
        Dim oActivity As IActivityLog
        oActivity = ComponentFactory.CreateActivityLog
        Return oActivity.ListActivity(Customclass)
    End Function
#End Region

#Region "AgreementCancel"
    Public Function AgreementCancelRpt(ByVal Customclass As Parameter.ActivityLog) As Parameter.ActivityLog
        Dim oActivity As IActivityLog
        oActivity = ComponentFactory.CreateActivityLog
        Return oActivity.AgreementCancelRpt(Customclass)
    End Function
#End Region
End Class
