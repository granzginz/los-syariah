
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class PendingController
    Function GetPendingDataEntry(ByVal customClass As Parameter.Pending) As Parameter.Pending
        Dim PendingBO As IPending
        PendingBO = ComponentFactory.CreatePending()
        Return PendingBO.GetPendingDataEntry(customClass)
    End Function
    Function GetAO(ByVal customclass As Parameter.Pending) As DataTable
        Dim PendingBO As IPending
        PendingBO = ComponentFactory.CreatePending()
        Return PendingBO.GetAO(customclass)
    End Function
    Function GetSupplier(ByVal customclass As Parameter.Pending) As DataTable
        Dim PendingBO As IPending
        PendingBO = ComponentFactory.CreatePending()
        Return PendingBO.GetSupplier(customclass)
    End Function
    Function GetApproved(ByVal customclass As Parameter.Pending) As DataTable
        Dim PendingBO As IPending
        PendingBO = ComponentFactory.CreatePending()
        Return PendingBO.GetApproved(customclass)
    End Function
    Function GetApplicationPendingInquiry(ByVal customClass As Parameter.Pending) As Parameter.Pending
        Dim PendingBO As IPending
        PendingBO = ComponentFactory.CreatePending()
        Return PendingBO.GetApplicationPendingInquiry(customClass)
    End Function
End Class
