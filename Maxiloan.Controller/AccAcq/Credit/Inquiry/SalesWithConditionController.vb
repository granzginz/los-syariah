

#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class SalesWithConditionController
#Region "List Sales with Condition"
    Public Function ListSalesWithCondition(ByVal Customclass As Parameter.SalesWithCondition) As Parameter.SalesWithCondition
        Dim oSales As ISalesWithCondition
        oSales = ComponentFactory.CreateSalesCondition()
        Return oSales.ListSalesWithCondition(Customclass)
    End Function
#End Region



    Public Function ListTboInquiry(cnn As String, currentPage As Integer, PageSize As Integer, branchid As String, where As String) As Parameter.SalesWithCondition
        Dim oSales As ISalesWithCondition
        oSales = ComponentFactory.CreateSalesCondition()
        Return oSales.ListTboInquiry(cnn, currentPage, PageSize, branchid, where)
    End Function
End Class
