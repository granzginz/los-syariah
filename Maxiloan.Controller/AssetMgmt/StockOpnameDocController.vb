﻿Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class StockOpnameDocController

    Public Function DoCreateStockOpname(cnn As String, rows As Parameter.StockOpDoc) As String
        Dim oStockOpnameDo As IDoc= ComponentFactory.CreateDocReceive
        Return oStockOpnameDo.DoCreateStockOpname(cnn, rows)
    End Function

    Public Function GetResultOpnamePage(cnn As String, currentPage As Integer, pageSize As Integer, opNameNo As String) As Parameter.StockOpDoc
        Dim oStockOpnameDo As IDoc = ComponentFactory.CreateDocReceive
        Return oStockOpnameDo.GetResultOpnamePage(cnn, currentPage, pageSize, opNameNo)
    End Function

    Public Function getOpnameNo(cnn As String, branch As String, prm As DateTime, isOnhand As Boolean) As String
        Dim oStockOpnameDo As IDoc = ComponentFactory.CreateDocReceive

        Dim str = String.Format("SELECT StockOpnameNo FROM  {0} where convert(date ,OpnameDate ) =@OpnameDate", IIf(isOnhand, "StockOpnameBPKBheader", "StockOpnameActualBPKBheader"))
        Return oStockOpnameDo.getOpnameNo(cnn, prm, str)
    End Function

    Public Function DoProsesCompareStockOpname(cnn As String, StockOpnameNo As String, ActStockOpnameNo As String, prosesDate As DateTime) As String
        Dim oStockOpnameDo As IDoc = ComponentFactory.CreateDocReceive
        Return oStockOpnameDo.ProsesStockOpname(cnn, StockOpnameNo, ActStockOpnameNo, prosesDate)
    End Function

    Public Function GetProcesResultOnhandOpnamePage(cnn As String, currentPage As Integer, pageSize As Integer, opNameNo As String) As Parameter.StockOpDoc
        Dim oStockOpnameDo As IDoc = ComponentFactory.CreateDocReceive
        Return oStockOpnameDo.GetProcesResultOnhandOpnamePage(cnn, currentPage, pageSize, opNameNo)
    End Function

    Public Function GetProcesResultActualOpnamePage(cnn As String, currentPage As Integer, pageSize As Integer, opNameNo As String) As Parameter.StockOpDoc
        Dim oStockOpnameDo As IDoc = ComponentFactory.CreateDocReceive
        Return oStockOpnameDo.GetProcesResultActualOpnamePage(cnn, currentPage, pageSize, opNameNo)
    End Function

    Public Function GetResultActualOpnamePage(cnn As String, currentPage As Integer, pageSize As Integer, opNameNo As String) As Parameter.StockOpDoc
        Dim oStockOpnameDo As IDoc = ComponentFactory.CreateDocReceive
        Return oStockOpnameDo.GetResultActualOpnamePage(cnn, currentPage, pageSize, opNameNo)
    End Function

    Public Function DocInformation(ByVal oCustomClass As Parameter.DocRec) As DataTable
        Dim oStockOpnameDo As IDoc = ComponentFactory.CreateDocReceive
        Return oStockOpnameDo.DocInformation(oCustomClass)
    End Function

    Public Sub SaveStockOpnameActual(ByVal oCustomClass As Parameter.StockOpDoc)
        Dim oStockOpnameDo As IDoc = ComponentFactory.CreateDocReceive
        oStockOpnameDo.SaveStockOpnameActual(oCustomClass)
    End Sub
End Class
