
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class DocReceiveController
#Region "DocReceive"

    Public Function ListDoc(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.ListDoc(oCustomClass)
    End Function

    Public Function ListDocAssetBNI(ByVal oCustomClass As Parameter.DocAssetBNI) As Parameter.DocAssetBNI
        Dim oListDoc As IDoc
        oListDoc = ComponentFactory.CreateDocReceive
        Return oListDoc.ListDocAssetBNI(oCustomClass)
    End Function

    Public Function InqListDocAssetBNI(ByVal oCustomClass As Parameter.DocAssetBNI) As Parameter.DocAssetBNI
        Dim oInqListDoc As IDoc
        oInqListDoc = ComponentFactory.CreateDocReceive
        Return oInqListDoc.InqListDocAssetBNI(oCustomClass)
    End Function

    Public Function SPPADList(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.SPPADList(oCustomClass)
    End Function

    Function GetFillLoc(ByVal oCustomClass As Parameter.DocRec) As DataTable
        Dim FLoc As IDoc
        FLoc = ComponentFactory.CreateDocReceive
        Return FLoc.GetFLoc(oCustomClass)
    End Function

    Public Function GetRack(ByVal strConnection As String, ByVal strBranch As String, Optional isFund As Boolean = False) As DataTable
        Dim GRack As IDoc
        GRack = ComponentFactory.CreateDocReceive
        Return GRack.GetRack(strConnection, strBranch, isFund)
    End Function

    Public Function DocListPaging(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oDList As IDoc
        oDList = ComponentFactory.CreateDocReceive
        Return oDList.DocListPaging(oCustomClass)
    End Function
    Public Function DocListPagingAssetBNI(ByVal oCustomClass As Parameter.DocAssetBNI) As Parameter.DocAssetBNI
        Dim oDListPagingBNI As IDoc
        oDListPagingBNI = ComponentFactory.CreateDocReceive
        Return oDListPagingBNI.DocListPagingAssetBNI(oCustomClass)
    End Function
    Public Function DocRecSave(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oRSave As IDoc
        oRSave = ComponentFactory.CreateDocReceive
        Return oRSave.DocRecSave(oCustomClass)
    End Function
    Public Function SaveDocReceiveJanji(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oRSave As IDoc
        oRSave = ComponentFactory.CreateDocReceive
        Return oRSave.SaveDocReceiveJanji(oCustomClass)
    End Function
    Public Function GetAssetRegistration(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oRSave As IDoc
        oRSave = ComponentFactory.CreateDocReceive
        Return oRSave.GetAssetRegistration(oCustomClass)
    End Function
    Public Function GetTaxDate(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oRSave As IDoc
        oRSave = ComponentFactory.CreateDocReceive
        Return oRSave.GetTaxDate(oCustomClass)
    End Function
    Public Function GetBranchName(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oRSave As IDoc
        oRSave = ComponentFactory.CreateDocReceive
        Return oRSave.GetBranchName(oCustomClass)
    End Function
#End Region
#Region "DocBorrow"
    Public Function DocBorrowSave(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oBSave As IDoc
        oBSave = ComponentFactory.CreateDocReceive
        Return oBSave.DocBorrowSave(oCustomClass)
    End Function
#End Region
#Region "DocBorrowPledging"
    Public Function DocBorrowPledgingSave(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oBSave As IDoc
        oBSave = ComponentFactory.CreateDocReceive
        Return oBSave.DocBorrowPledgingSave(oCustomClass)
    End Function
#End Region
#Region "DocRelease"
    Public Function DocReleaseSave(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oRSave As IDoc
        oRSave = ComponentFactory.CreateDocReceive
        Return oRSave.DocReleaseSave(oCustomClass)
    End Function

    Public Function ListInfo(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.ListInfo(oCustomClass)
    End Function

    Public Function PrintBPKBSerahTerima(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.PrintBPKBSerahTerima(oCustomClass)
    End Function
#End Region
#Region "DocChangeLoc"
    Public Function DocChangeLocSave(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oLocSave As IDoc
        oLocSave = ComponentFactory.CreateDocReceive
        Return oLocSave.DocChangeLocSave(oCustomClass)
    End Function
#End Region
#Region "Inquiry"
    Function GetRackBranch(ByVal oCustomClass As Parameter.DocRec) As DataTable
        Dim RLoc As IDoc
        RLoc = ComponentFactory.CreateDocReceive
        Return RLoc.GetRackBranch(oCustomClass)
    End Function

    Public Function ListInquiry(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.ListInquiry(oCustomClass)
    End Function

    Public Function ListTboInquiry(cnn As String, currentPage As Integer, PageSize As Integer, branchid As String, businessdate As DateTime, aging As DateTime, where As String) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.ListTboInquiry(cnn, currentPage, PageSize, branchid, businessdate, aging, where)
    End Function

    Function GetAssetHistory(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim RLoc As IDoc
        RLoc = ComponentFactory.CreateDocReceive
        Return RLoc.GetAssetHistory(oCustomClass)
    End Function
    Function GetBPKBPosition(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim RLoc As IDoc
        RLoc = ComponentFactory.CreateDocReceive
        Return RLoc.GetBPKBPosition(oCustomClass)
    End Function

    Function SaveDocAssetBNI(ByVal oCustomClass As Parameter.DocAssetBNI) As Parameter.DocAssetBNI
        Dim SDocAssetBNI As IDoc
        SDocAssetBNI = ComponentFactory.CreateDocReceive
        Return SDocAssetBNI.SaveDocAssetBNI(oCustomClass)
    End Function
#End Region
#Region "Rack"
    Function AddRack(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim ARack As IDoc
        ARack = ComponentFactory.CreateDocReceive
        Return ARack.AddRack(oCustomClass)
    End Function


    Function DeleteRack(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim DRack As IDoc
        DRack = ComponentFactory.CreateDocReceive
        Return DRack.DeleteRack(oCustomClass)
    End Function
#End Region
#Region "Filling"
    Function AddFill(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim AFill As IDoc
        AFill = ComponentFactory.CreateDocReceive
        Return AFill.AddFill(oCustomClass)
    End Function


    Function DeleteFill(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim DFill As IDoc
        DFill = ComponentFactory.CreateDocReceive
        Return DFill.DeleteFill(oCustomClass)
    End Function
#End Region
#Region "Reports"
    Public Function NotExistsMainDoc(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.NotExistsMainDocReport(oCustomClass)
    End Function
    Public Function SummaryBPKBReport(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.SummaryBPKBReport(oCustomClass)
    End Function
    Public Function CreateSPAssetDocument(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.CreateSPAssetDocument(oCustomClass)
    End Function
    Public Function SavePrintSPAssetDoc(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.SavePrintSPAssetDoc(oCustomClass)
    End Function
    Public Function SavePrintSPPADoc(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.SavePrintSPPADoc(oCustomClass)
    End Function
    Public Function AgreementListADWithDrawal(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.AgreementListADWithDrawal(oCustomClass)
    End Function
    Public Function ListPemeriksaanBPKB(ByVal Customclass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.ListPemeriksaanBPKB(Customclass)
    End Function

    Public Function ListReportPemeriksaanBPKB(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.ListReportPemeriksaanBPKB(oCustomClass)
    End Function

    Public Function ListBlokirBPKB(ByVal Customclass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.ListBlokirBPKB(Customclass)
    End Function

    Public Function ListReportBlokirBPKB(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.ListReportBlokirBPKB(oCustomClass)
    End Function

    Public Function ListBukaBlokirBPKB(ByVal Customclass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.ListBukaBlokirBPKB(Customclass)
    End Function

    Public Function ListReportBukaBlokirBPKB(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.ListReportBukaBlokirBPKB(oCustomClass)
    End Function

    Public Function ListPinjamNamaBPKB(ByVal Customclass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.ListPinjamNamaBPKB(Customclass)
    End Function

    Public Function ListReportPinjamNamaBPKB(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.ListReportPinjamNamaBPKB(oCustomClass)
    End Function

    Public Function GetSPReport(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.GetSPReport(customclass)
    End Function
#End Region
#Region "DockPledging"
    Public Function DocPledgeProcess(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oBSave As IDoc
        oBSave = ComponentFactory.CreateDocReceive
        Return oBSave.DocPledgeProcess(oCustomClass)
    End Function
    Public Function GetDocPledgePaging(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim DocPlede As IDoc
        DocPlede = ComponentFactory.CreateDocReceive
        Return DocPlede.GetDocPledgePaging(oCustomClass)
    End Function
    Public Function ProcessReportPrepaymentRequest(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim ReportPrepaymentRequest As IDoc
        ReportPrepaymentRequest = ComponentFactory.CreateDocReceive
        Return ReportPrepaymentRequest.ProcessReportPrepaymentRequest(oCustomClass)
    End Function
#End Region
#Region "AdditionalProcess"
    Public Function SaveEdit(ByVal CustomClass As Parameter.DocRec, ByVal oData1 As DataTable) As Parameter.DocRec
        Dim oRSave As IDoc
        oRSave = ComponentFactory.CreateDocReceive
        Return oRSave.SaveEdit(CustomClass, oData1)
    End Function
#End Region
#Region "DocPledgeReceive"
    Public Function DocPledgeRecSave(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oRSave As IDoc
        oRSave = ComponentFactory.CreateDocReceive
        Return oRSave.DocPledgeRecSave(oCustomClass)
    End Function
#End Region
    Public Function getAssetDocumentStock(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.getAssetDocumentStock(oCustomClass)
    End Function
    Public Function PemeriksaanBPKBSave(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.PemeriksaanBPKBSave(oCustomClass)
    End Function
    Public Function HasilCekBPKBSave(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim oList As IDoc
        oList = ComponentFactory.CreateDocReceive
        Return oList.HasilCekBPKBSave(oCustomClass)
    End Function

    Public Function ProcessReportBBNRequest(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
        Dim ReportBBNRequest As IDoc
        ReportBBNRequest = ComponentFactory.CreateDocReceive
        Return ReportBBNRequest.ProcessReportBBNRequest(oCustomClass)
    End Function

End Class
