

#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class ChangeAddressInqController
    Public Function ListAddress(ByVal customclass As Parameter.ChangeAddressInq) As Parameter.ChangeAddressInq
        Dim oChange As IChangeAddressInq
        oChange = ComponentFactory.CreateChangeAddressInquiry
        Return oChange.ListAddress(customclass)
    End Function

    Public Function ViewAddress(ByVal customclass As Parameter.ChangeAddressInq) As Parameter.ChangeAddressInq
        Dim oChange As IChangeAddressInq
        oChange = ComponentFactory.CreateChangeAddressInquiry
        Return oChange.ViewAddress(customclass)
    End Function

    Public Function ViewDetail(ByVal customclass As Parameter.ChangeAddressInq) As Parameter.ChangeAddressInq
        Dim oChange As IChangeAddressInq
        oChange = ComponentFactory.CreateChangeAddressInquiry
        Return oChange.ViewDetail(customclass)
    End Function
End Class
