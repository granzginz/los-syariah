
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class ChangeAddressControllerARM
    Public Function AddressListCG(ByVal oCustomClass As Parameter.ChangeAddressARM) As Parameter.ChangeAddressARM
        Dim oAddress As IChangeAddressARM
        oAddress = ComponentFactory.CreateChangeAddressARM
        Return oAddress.AddressListCG(oCustomClass)
    End Function

    Public Function ChangeAddressList(ByVal oCustomClass As Parameter.ChangeAddressARM) As Parameter.ChangeAddressARM
        Dim oAddress As IChangeAddressARM
        oAddress = ComponentFactory.CreateChangeAddressARM
        Return oAddress.ChangeAddressList(oCustomClass)
    End Function

    Public Function ChangeAddressView(ByVal oCustomClass As Parameter.ChangeAddressARM) As Parameter.ChangeAddressARM
        Dim oAddress As IChangeAddressARM
        oAddress = ComponentFactory.CreateChangeAddressARM
        Return oAddress.ChangeAddressView(oCustomClass)
    End Function
    Public Function ChangeAddressSave(ByVal oCustomClass As Parameter.ChangeAddressARM) As Parameter.ChangeAddressARM
        Dim oAddress As IChangeAddressARM
        oAddress = ComponentFactory.CreateChangeAddressARM
        Return oAddress.ChangeAddressSave(oCustomClass)
    End Function

    Public Function GetAddress(ByVal oCustomClass As Parameter.ChangeAddressARM) As Parameter.ChangeAddressARM
        Dim oAddress As IChangeAddressARM
        oAddress = ComponentFactory.CreateChangeAddressARM
        Return oAddress.GetAddress(oCustomClass)
    End Function
    Public Function ViewReportAR(ByVal oCustomClass As Parameter.ChangeAddressARM) As Parameter.ChangeAddressARM
        Dim BO As IChangeAddressARM
        BO = ComponentFactory.CreateChangeAddressARM
        Return BO.ViewReportAR(oCustomClass)
    End Function
End Class
