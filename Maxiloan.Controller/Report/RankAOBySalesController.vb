
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region
Public Class RankAOBySalesController

    Public Function AORankView(ByVal customclass As Parameter.RankAOBySales) As Parameter.RankAOBySales
        Dim AORank As IRankAOBySales
        AORank = ComponentFactory.CreateRankAOBySalesReport
        Return AORank.AORankView(customclass)
    End Function
End Class
