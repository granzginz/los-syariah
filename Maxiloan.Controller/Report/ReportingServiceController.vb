﻿Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class ReportingServiceController
    Function getCatalogRS(ByVal oClass As Parameter.CatalogRS) As Parameter.CatalogRS
        Dim oInterface As IReportingService

        oInterface = ComponentFactory.CreateReportingService
        Return oInterface.getCatalogRS(oClass)
    End Function
End Class
