
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AOLevelReportController
    Public Function AOLevelEvaluation(ByVal customclass As Parameter.aolevelreport) As Parameter.aolevelreport
        Dim AOLevel As IAOLevel
        AOLevel = ComponentFactory.CreateAOLevelReport
        Return AOLevel.AOLevelEvaluation(customclass)
    End Function
    Public Function GetAOSupervisor(ByVal customclass As Parameter.AOLevelReport) As DataTable
        Dim AOSupervisor As IAOLevel
        AOSupervisor = ComponentFactory.CreateAOLevelReport
        Return AOSupervisor.GetAOSupervisor(customclass)
    End Function
    Public Function GetAO(ByVal customclass As Parameter.AOLevelReport) As DataTable
        Dim AO As IAOLevel
        AO = ComponentFactory.CreateAOLevelReport
        Return AO.GetAO(customclass)
    End Function

End Class
