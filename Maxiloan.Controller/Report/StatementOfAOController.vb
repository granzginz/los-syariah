
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class StatementOfAOController
    Public Function ReportView(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
        Dim AOStatement As IStatementOfAO
        AOStatement = ComponentFactory.CreateStatementOfAOReport
        Return AOStatement.ReportView(customclass)
    End Function
    Public Function GetAO(ByVal customclass As Parameter.StatementOfAO) As DataTable
        Dim AO As IStatementOfAO
        AO = ComponentFactory.CreateStatementOfAOReport
        Return AO.GetAO(customclass)
    End Function
    Public Function SubReportApproved(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
        Dim AOStatement As IStatementOfAO
        AOStatement = ComponentFactory.CreateStatementOfAOReport
        Return AOStatement.SubReportApproved(customclass)
    End Function
    Public Function SubReportFunding(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
        Dim AOStatement As IStatementOfAO
        AOStatement = ComponentFactory.CreateStatementOfAOReport
        Return AOStatement.SubReportFunding(customclass)
    End Function
    Public Function SubReportGrossYield(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
        Dim AOStatement As IStatementOfAO
        AOStatement = ComponentFactory.CreateStatementOfAOReport
        Return AOStatement.SubReportGrossYield(customclass)
    End Function
    Public Function SubReportBucket(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
        Dim AOStatement As IStatementOfAO
        AOStatement = ComponentFactory.CreateStatementOfAOReport
        Return AOStatement.SubReportBucket(customclass)
    End Function
    Public Function SubReportReppossess(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
        Dim AOStatement As IStatementOfAO
        AOStatement = ComponentFactory.CreateStatementOfAOReport
        Return AOStatement.SubReportReppossess(customclass)
    End Function
    Public Function SubReportBPKB(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
        Dim AOStatement As IStatementOfAO
        AOStatement = ComponentFactory.CreateStatementOfAOReport
        Return AOStatement.SubReportBPKB(customclass)
    End Function
    Public Function MainReport(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
        Dim AOStatement As IStatementOfAO
        AOStatement = ComponentFactory.CreateStatementOfAOReport
        Return AOStatement.MainReport(customclass)
    End Function
    Public Function SubReportBPKBOverdue(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
        Dim AOStatement As IStatementOfAO
        AOStatement = ComponentFactory.CreateStatementOfAOReport
        Return AOStatement.SubReportBPKBOverdue(customclass)
    End Function
    Public Function SubReportBucketTotal(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
        Dim AOStatement As IStatementOfAO
        AOStatement = ComponentFactory.CreateStatementOfAOReport
        Return AOStatement.SubReportBucketTotal(customclass)
    End Function
    Public Function MainReportBranch(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
        Dim AOStatement As IStatementOfAO
        AOStatement = ComponentFactory.CreateStatementOfAOReport
        Return AOStatement.MainReportBranch(customclass)
    End Function
End Class
