
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class RptSalesController
    Function GetAOSupervisor(ByVal oCustomClass As Parameter.Sales) As DataTable
        Dim AOSupervisor As ISalesReport
        AOSupervisor = ComponentFactory.CreateSalesReport
        Return AOSupervisor.GetAOSupervisor(oCustomClass)
    End Function

    Function GetAO(ByVal oCustomClass As Parameter.Sales) As DataTable
        Dim AO As ISalesReport
        AO = ComponentFactory.CreateSalesReport
        Return AO.GetAO(oCustomClass)
    End Function
    Public Function ViewRptDailySalesByAO(ByVal oCustomClass As Parameter.Sales) As Parameter.Sales
        Dim DailySalesByAO As ISalesReport
        DailySalesByAO = ComponentFactory.CreateSalesReport
        Return DailySalesByAO.ViewRptDailySalesByAO(oCustomClass)
    End Function
    Public Function GetArea(ByVal ocustomclass As Parameter.Sales) As DataTable
        Dim area As ISalesReport
        area = ComponentFactory.CreateSalesReport
        Return area.GetArea(ocustomclass)
    End Function
    Function GetCASupervisor(ByVal oCustomClass As Parameter.Sales) As DataTable
        Dim CASupervisor As ISalesReport
        CASupervisor = ComponentFactory.CreateSalesReport
        Return CASupervisor.GetCASupervisor(oCustomClass)
    End Function

    Function GetCA(ByVal oCustomClass As Parameter.Sales) As DataTable
        Dim CA As ISalesReport
        CA = ComponentFactory.CreateSalesReport
        Return CA.GetCA(oCustomClass)
    End Function
    Public Function GetBrand(ByVal ocustomclass As Parameter.Sales) As DataTable
        Dim Brand As ISalesReport
        Brand = ComponentFactory.CreateSalesReport
        Return Brand.GetBrand(ocustomclass)
    End Function
    Public Function GetSupplier(ByVal ocustomclass As Parameter.Sales) As DataTable
        Dim Supplier As ISalesReport
        Supplier = ComponentFactory.CreateSalesReport
        Return Supplier.GetSupplier(ocustomclass)
    End Function
    Public Function GetPercentDP(ByVal ocustomclass As Parameter.Sales) As DataTable
        Dim PercentDP As ISalesReport
        PercentDP = ComponentFactory.CreateSalesReport
        Return PercentDP.GetPercentDP(ocustomclass)
    End Function
    Public Function GetPercentEffective(ByVal ocustomclass As Parameter.Sales) As DataTable
        Dim PercentEffective As ISalesReport
        PercentEffective = ComponentFactory.CreateSalesReport
        Return PercentEffective.GetPercentEffective(ocustomclass)
    End Function
    Public Function GetAmountFinance(ByVal ocustomclass As Parameter.Sales) As DataTable
        Dim AmountFinance As ISalesReport
        AmountFinance = ComponentFactory.CreateSalesReport
        Return AmountFinance.GetAmountFinance(ocustomclass)
    End Function
    Public Function GetInstallmentAmount(ByVal ocustomclass As Parameter.Sales) As DataTable
        Dim InstallmentAmount As ISalesReport
        InstallmentAmount = ComponentFactory.CreateSalesReport
        Return InstallmentAmount.GetInstallmentAmount(ocustomclass)
    End Function
    Public Function GetProduct(ByVal ocustomclass As Parameter.Sales) As DataTable
        Dim Product As ISalesReport
        Product = ComponentFactory.CreateSalesReport
        Return Product.GetProduct(ocustomclass)
    End Function
    Public Function InqSalesPerPeriod(ByVal ocustomclass As Parameter.Sales) As Parameter.Sales
        Dim sales As ISalesReport
        sales = ComponentFactory.CreateSalesReport
        Return sales.InqSalesPerPeriod(ocustomclass)
    End Function
    Public Function InqAgingStatus(ByVal ocustomclass As Parameter.Sales) As Parameter.Sales
        Dim Aging As ISalesReport
        Aging = ComponentFactory.CreateSalesReport
        Return Aging.InqAgingStatus(ocustomclass)
    End Function
    Function GetProductWhere(ByVal oCustomClass As Parameter.Sales) As DataTable
        Dim Prod As ISalesReport
        Prod = ComponentFactory.CreateSalesReport
        Return Prod.GetProductWhere(oCustomClass)
    End Function
    Public Function GetProductBranch(ByVal ocustomclass As Parameter.Sales) As DataTable
        Dim Supplier As ISalesReport
        Supplier = ComponentFactory.CreateSalesReport
        Return Supplier.GetProductBranch(ocustomclass)
    End Function
    Public Function GetAssetCode(ByVal ocustomclass As Parameter.Sales) As DataTable
        Dim Supplier As ISalesReport
        Supplier = ComponentFactory.CreateSalesReport
        Return Supplier.GetAssetCode(ocustomclass)
    End Function

    Public Function DailySalesCMOCrosstab(ByVal oCustomClass As Parameter.Sales) As Parameter.Sales
        Dim DailySalesByAO As ISalesReport
        DailySalesByAO = ComponentFactory.CreateSalesReport
        Return DailySalesByAO.DailySalesCMOCrosstab(oCustomClass)
    End Function

    Public Function DailySalesSupplierCrosstab(ByVal oCustomClass As Parameter.Sales) As Parameter.Sales
        Dim DailySalesByAO As ISalesReport
        DailySalesByAO = ComponentFactory.CreateSalesReport
        Return DailySalesByAO.DailySalesSupplierCrosstab(oCustomClass)
    End Function
End Class
