
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region


Public Class FiduciaController

    Public Function GetNotary(ByVal customClass As Parameter.Fiducia) As Parameter.Fiducia
        Dim BO As IFiducia
        BO = ComponentFactory.CreateFiducia()
        Return BO.GetNotary(customClass)
    End Function

    Public Function GetNotaryCharge(ByVal customClass As Parameter.Fiducia) As Parameter.Fiducia
        Dim BO As IFiducia
        BO = ComponentFactory.CreateFiducia()
        Return BO.GetNotaryCharge(customClass)
    End Function

    Public Sub FiduciaAktaReceive(ByVal customclass As Parameter.Fiducia)
        Dim BO As IFiducia
        BO = ComponentFactory.CreateFiducia()
        BO.FiduciaAktaReceive(customclass)
    End Sub

    Public Sub FiduciaFarsial(ByVal customclass As Parameter.Fiducia)
        Dim BO As IFiducia
        BO = ComponentFactory.CreateFiducia()
        BO.FiduciaFarsial(customclass)
    End Sub
    Public Sub GenerateFiduciaRoyaRegisterNo(ByVal customclass As Parameter.Fiducia)
        Dim BO As IFiducia
        BO = ComponentFactory.CreateFiducia()
        BO.GenerateFiduciaRoyaRegisterNo(customclass)
    End Sub
    Public Sub FiduciaAdd(ByVal customclass As Parameter.Fiducia)
        Dim BO As IFiducia
        BO = ComponentFactory.CreateFiducia()
        BO.FiduciaAdd(customclass)
    End Sub
    Public Sub NotaryChargeAdd(ByVal customclass As Parameter.Fiducia)
        Dim BO As IFiducia
        BO = ComponentFactory.CreateFiducia()
        BO.NotaryChargeAdd(customclass)
    End Sub
    Public Sub AddNotary(ByVal customclass As Parameter.Fiducia)
        Dim BO As IFiducia
        BO = ComponentFactory.CreateFiducia()
        BO.AddNotary(customclass)
    End Sub

End Class
