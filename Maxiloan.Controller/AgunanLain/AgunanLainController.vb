﻿

#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AgunanLainController

    Function ListJenisAgunan(ByVal Agunan As Parameter.AgunanLain) As Parameter.AgunanLain
        Dim oAgunanLain As IAgunanLain
        oAgunanLain = ComponentFactory.CreateAgunanLain()
        Return oAgunanLain.JenisAgunanList(Agunan)
    End Function

    Function ListAgunan(ByVal Agunan As Parameter.AgunanLain) As Parameter.AgunanLain
        Dim oAgunanLain As IAgunanLain
        oAgunanLain = ComponentFactory.CreateAgunanLain()
        Return oAgunanLain.ListAgunan(Agunan)
    End Function

	Public Sub AddJenisAgunan(ByVal Agunan As Parameter.AgunanLain)
		Dim oAgunanLain As IAgunanLain
		oAgunanLain = ComponentFactory.CreateAgunanLain()
		oAgunanLain.JenisAgunanAdd(Agunan)
	End Sub

	Public Sub EditJenisAgunan(ByVal Agunan As Parameter.AgunanLain)
		Dim oAgunanLain As IAgunanLain
		oAgunanLain = ComponentFactory.CreateAgunanLain()
		oAgunanLain.JenisAgunanEdit(Agunan)
	End Sub

	Public Sub DelJenisAgunan(ByVal Agunan As Parameter.AgunanLain)
		Dim oAgunanLain As IAgunanLain
		oAgunanLain = ComponentFactory.CreateAgunanLain()
		oAgunanLain.JenisAgunanDelete(Agunan)
	End Sub

	Public Function getDataInvoice(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
		Dim oInterface As IAgunanLain
		oInterface = ComponentFactory.CreateAgunanLain
		Return oInterface.getDataInvoice(oCustomClass)
	End Function

	Public Function getDataBPKB(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
		Dim oInterface As IAgunanLain
		oInterface = ComponentFactory.CreateAgunanLain
		Return oInterface.getDataBPKB(oCustomClass)
	End Function

	Public Function saveEditDataBPKB(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
		Dim oInterface As IAgunanLain
		oInterface = ComponentFactory.CreateAgunanLain
		Return oInterface.saveEditDataBPKB(oCustomClass)
	End Function

	Public Function saveAddDataBPKB(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
		Dim oInterface As IAgunanLain
		oInterface = ComponentFactory.CreateAgunanLain
		Return oInterface.saveAddDataBPKB(oCustomClass)
	End Function

	Public Function saveEditDataInvoice(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
		Dim oInterface As IAgunanLain
		oInterface = ComponentFactory.CreateAgunanLain
		Return oInterface.saveEditDataInvoice(oCustomClass)
	End Function

	Public Function saveAddDataInvoice(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
		Dim oInterface As IAgunanLain
		oInterface = ComponentFactory.CreateAgunanLain
		Return oInterface.saveAddDataInvoice(oCustomClass)
	End Function

	Public Function saveEditDataLandBuilding(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
		Dim oInterface As IAgunanLain
		oInterface = ComponentFactory.CreateAgunanLain
		Return oInterface.saveEditDataLandBuilding(oCustomClass)
	End Function

	Public Function saveAddDataLandBuilding(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
		Dim oInterface As IAgunanLain
		oInterface = ComponentFactory.CreateAgunanLain
		Return oInterface.saveAddDataLandBuilding(oCustomClass)
	End Function
	Public Function getDataLandBuildidng(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
		Dim oInterface As IAgunanLain
		oInterface = ComponentFactory.CreateAgunanLain
		Return oInterface.getDataLandBuildidng(oCustomClass)
	End Function

	Function getAgunanNett(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
		Dim oInterface As IAgunanLain
		oInterface = ComponentFactory.CreateAgunanLain
		Return oInterface.getAgunanNett(oCustomClass)
	End Function
	Function GetCboCollateralType(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
		Dim oInterface As IAgunanLain
		oInterface = ComponentFactory.CreateAgunanLain
		Return oInterface.GetCboCollateralType(oCustomClass)
	End Function
	Function GetCollateralId(ByVal oCustomClass As Parameter.AgunanLain) As String
		Dim oInterface As IAgunanLain
		oInterface = ComponentFactory.CreateAgunanLain
		Return oInterface.GetCollateralId(oCustomClass)
	End Function

	Public Sub PledgingAdd(ByVal Agunan As Parameter.AgunanLain)
		Dim oAgunanLain As IAgunanLain
		oAgunanLain = ComponentFactory.CreateAgunanLain()
		oAgunanLain.PledgingAdd(Agunan)
	End Sub

	Public Function AddCollateral(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
		Dim oInterface As IAgunanLain
		oInterface = ComponentFactory.CreateAgunanLain
		Return oInterface.AddCollateral(oCustomClass)
	End Function


#Region "GetDetailTable"
	Public Function GetDetailPledging(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
        Dim oAgunanLain As IAgunanLain
        Try
            oAgunanLain = ComponentFactory.CreateAgunanLain()
            Return oAgunanLain.GetDetailPledging(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region
End Class
