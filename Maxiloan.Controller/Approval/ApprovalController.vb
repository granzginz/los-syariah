
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class ApprovalController
    Public Function GetUserApproval(ByVal customclass As Parameter.Approval) As DataTable

        Dim UserApproval As IApproval
        UserApproval = ComponentFactory.CreateApproval()
        Return UserApproval.GetUserApproval(customclass)
    End Function
End Class
