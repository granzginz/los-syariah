
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class ApprovalScreenController
    Public Function ApprovalList(ByVal customclass As Parameter.ApprovalScreen) As Parameter.ApprovalScreen
        Dim DAApprovalList As IApprovalScreen
        DAApprovalList = ComponentFactory.CreateApprovalScreen()
        Return DAApprovalList.ApprovalList(customclass)
    End Function
    Public Function ApprovalListUser(ByVal customclass As Parameter.ApprovalScreen) As DataTable
        Dim DAApprovalListUser As IApprovalScreen
        DAApprovalListUser = ComponentFactory.CreateApprovalScreen()
        Return DAApprovalListUser.ApprovalListUser(customclass)
    End Function

    Public Function ApprovalListTypeDescription(ByVal customclass As Parameter.ApprovalScreen) As String
        Dim DAApprovalListTypeDescription As IApprovalScreen
        DAApprovalListTypeDescription = ComponentFactory.CreateApprovalScreen()
        Return DAApprovalListTypeDescription.ApprovalListTypeDescription(customclass)
    End Function

    Public Function ApprovalListHistory(ByVal customclass As Parameter.ApprovalScreen) As DataTable
        Dim DAApprovalListHistory As IApprovalScreen
        DAApprovalListHistory = ComponentFactory.CreateApprovalScreen()
        Return DAApprovalListHistory.ApprovalListHistory(customclass)
    End Function

    Public Function ApprovalScreenUserRequest(ByVal customclass As Parameter.ApprovalScreen) As String
        Dim DAApprovalScreenUserRequest As IApprovalScreen
        DAApprovalScreenUserRequest = ComponentFactory.CreateApprovalScreen()
        Return DAApprovalScreenUserRequest.ApprovalScreenUserRequest(customclass)
    End Function

    Public Function ApprovalScreenViewScheme(ByVal customclass As Parameter.ApprovalScreen) As DataTable
        Dim DAApprovalScreenViewScheme As IApprovalScreen
        DAApprovalScreenViewScheme = ComponentFactory.CreateApprovalScreen()
        Return DAApprovalScreenViewScheme.ApprovalScreenViewScheme(customclass)
    End Function

    Public Function ApprovalScreenUserScheme(ByVal customclass As Parameter.ApprovalScreen) As Parameter.ApprovalScreen
        Dim DAApprovalScreenUserScheme As IApprovalScreen
        DAApprovalScreenUserScheme = ComponentFactory.CreateApprovalScreen()
        Return DAApprovalScreenUserScheme.ApprovalScreenUserScheme(customclass)
    End Function

    Public Function ApprovalScreenIsValidApproval(ByVal customclass As Parameter.ApprovalScreen) As Parameter.ApprovalScreen
        Dim DAApprovalScreenIsValidApproval As IApprovalScreen
        DAApprovalScreenIsValidApproval = ComponentFactory.CreateApprovalScreen()
        Return DAApprovalScreenIsValidApproval.ApprovalScreenIsValidApproval(customclass)
    End Function

    Public Function ApprovalScreenSetLimit(ByVal customclass As Parameter.ApprovalScreen) As Parameter.ApprovalScreen
        Dim DAApprovalScreenSetLimit As IApprovalScreen
        DAApprovalScreenSetLimit = ComponentFactory.CreateApprovalScreen()
        Return DAApprovalScreenSetLimit.ApprovalScreenSetLimit(customclass)
    End Function

    'Public Function GetApprovalItems(ByVal customclass As Parameter.ApprovalScreen) As Parameter.ApprovalScreen
    '    Dim IApproval As IApprovalScreen
    '    IApproval = ComponentFactory.CreateApprovalScreen()
    '    Return IApproval.getApprovalItems(customclass)
    'End Function
End Class
