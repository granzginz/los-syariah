
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class ApprovalSchemeController
    Public Sub ApprovalSchemeAdd(ByVal customclass As Parameter.ApprovalScheme)
        Dim DAApprovalSchemeAdd As IApprovalScheme
        DAApprovalSchemeAdd = ComponentFactory.CreateApprovalScheme()
        DAApprovalSchemeAdd.ApprovalSchemeAdd(customclass)
    End Sub

    Public Sub ApprovalSchemeEdit(ByVal customclass As Parameter.ApprovalScheme)
        Dim DAApprovalSchemeEdit As IApprovalScheme
        DAApprovalSchemeEdit = ComponentFactory.CreateApprovalScheme()
        DAApprovalSchemeEdit.ApprovalSchemeEdit(customclass)
    End Sub

    Public Sub ApprovalSchemeDelete(ByVal customclass As Parameter.ApprovalScheme)
        Dim DAApprovalSchemeDelete As IApprovalScheme
        DAApprovalSchemeDelete = ComponentFactory.CreateApprovalScheme()
        DAApprovalSchemeDelete.ApprovalSchemeDelete(customclass)
    End Sub

    Public Function ApprovalSchemeView(ByVal customclass As Parameter.ApprovalScheme) As Parameter.ApprovalScheme
        Dim DAApprovalSchemeView As IApprovalScheme
        DAApprovalSchemeView = ComponentFactory.CreateApprovalScheme()
        Return DAApprovalSchemeView.ApprovalSchemeView(customclass)
    End Function
    Public Function GetApprovalType(ByVal strConnection As String) As DataTable
        Dim DAApprovalSchemeView As IApprovalScheme
        DAApprovalSchemeView = ComponentFactory.CreateApprovalScheme()
        Return DAApprovalSchemeView.GetApprovalType(strConnection)
    End Function

#Region "Approval Tree"

    Public Function ApprovalPathTree(ByVal customclass As Parameter.ApprovalScheme) As DataTable
        Dim DAApprovalPathTree As IApprovalScheme
        DAApprovalPathTree = ComponentFactory.CreateApprovalScheme()
        Return DAApprovalPathTree.ApprovalPathTree(customclass)
    End Function
#Region "Approval Member"
    Public Sub ApprovalMemberAdd(ByVal customclass As Parameter.ApprovalScheme)
        Dim DAApprovalMemberAdd As IApprovalScheme
        DAApprovalMemberAdd = ComponentFactory.CreateApprovalScheme()
        DAApprovalMemberAdd.ApprovalMemberAdd(customclass)
    End Sub

    Public Sub ApprovalMemberDelete(ByVal customclass As Parameter.ApprovalScheme)
        Dim DAApprovalMemberDelete As IApprovalScheme
        DAApprovalMemberDelete = ComponentFactory.CreateApprovalScheme()
        DAApprovalMemberDelete.ApprovalMemberDelete(customclass)
    End Sub

    Public Sub ApprovalMemberEdit(ByVal customclass As Parameter.ApprovalScheme)
        Dim DAApprovalMemberEdit As IApprovalScheme
        DAApprovalMemberEdit = ComponentFactory.CreateApprovalScheme()
        DAApprovalMemberEdit.ApprovalMemberEdit(customclass)
    End Sub
    Public Function ApprovalMemberView(ByVal customclass As Parameter.ApprovalScheme) As DataTable
        Dim DAApprovalMemberView As IApprovalScheme
        DAApprovalMemberView = ComponentFactory.CreateApprovalScheme()
        Return DAApprovalMemberView.ApprovalMemberView(customclass)
    End Function

    Public Function ApprovalMemberLoginList(ByVal customclass As Parameter.ApprovalScheme) As DataTable
        Dim DAApprovalMemberLoginList As IApprovalScheme
        DAApprovalMemberLoginList = ComponentFactory.CreateApprovalScheme()
        Return DAApprovalMemberLoginList.ApprovalMemberLoginList(customclass)
    End Function

    Public Function ApprovalMemberSetMember(ByVal customclass As Parameter.ApprovalScheme) As DataTable
        Dim DAApprovalMemberSetMember As IApprovalScheme
        DAApprovalMemberSetMember = ComponentFactory.CreateApprovalScheme()
        Return DAApprovalMemberSetMember.ApprovalMemberSetMember(customclass)
    End Function
#End Region

#Region "ApprovalPath"
    Public Sub ApprovalPathDelete(ByVal customclass As Parameter.ApprovalScheme)
        Dim DAApprovalPathDelete As IApprovalScheme
        DAApprovalPathDelete = ComponentFactory.CreateApprovalScheme()
        DAApprovalPathDelete.ApprovalPathDelete(customclass)
    End Sub
    Public Sub ApprovalPathAdd(ByVal customclass As Parameter.ApprovalScheme)
        Dim DAApprovalPathAdd As IApprovalScheme
        DAApprovalPathAdd = ComponentFactory.CreateApprovalScheme()
        DAApprovalPathAdd.ApprovalPathAdd(customclass)
    End Sub

    Public Sub ApprovalPathEdit(ByVal customclass As Parameter.ApprovalScheme)
        Dim DAApprovalPathEdit As IApprovalScheme
        DAApprovalPathEdit = ComponentFactory.CreateApprovalScheme()
        DAApprovalPathEdit.ApprovalPathEdit(customclass)
    End Sub

    Public Function ApprovalPathGetLimit(ByVal customclass As Parameter.ApprovalScheme) As Double
        Dim DAApprovalPathGetLimit As IApprovalScheme
        DAApprovalPathGetLimit = ComponentFactory.CreateApprovalScheme()
        Return DAApprovalPathGetLimit.ApprovalPathGetLimit(customclass)
    End Function

    Public Function ApprovalPathView(ByVal customclass As Parameter.ApprovalScheme) As Parameter.ApprovalScheme
        Dim DAApprovalPathView As IApprovalScheme
        DAApprovalPathView = ComponentFactory.CreateApprovalScheme()
        Return DAApprovalPathView.ApprovalPathView(customclass)
    End Function
#End Region
#End Region
End Class
