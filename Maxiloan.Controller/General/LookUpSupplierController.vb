
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface


Public Class LookUpSupplierController
    Public Function GetListDataSupplierBranch(ByVal oEntities As Parameter.LookUpSupplier) As Parameter.LookUpSupplier
        Dim LookUpSupplierBO As ILookUpSupplier
        LookUpSupplierBO = ComponentFactory.CreateLookUpSupplier
        Return LookUpSupplierBO.GetListDataSupplierBranch(oEntities)
    End Function

    Public Function GetListDataMulti(ByVal oEntities As Parameter.LookUpSupplier) As Parameter.LookUpSupplier
        Dim LookUpSupplierBO As ILookUpSupplier
        LookUpSupplierBO = ComponentFactory.CreateLookUpSupplier
        Return LookUpSupplierBO.GetListDataMulti(oEntities)
    End Function

    Public Function GetListDataMultiConfirm(ByVal oEntities As Parameter.LookUpSupplier) As Parameter.LookUpSupplier
        Dim LookUpSupplierBO As ILookUpSupplier
        LookUpSupplierBO = ComponentFactory.CreateLookUpSupplier
        Return LookUpSupplierBO.GetListDataMulti_Confirm(oEntities)
    End Function

    Public Function GetListData(ByVal oEntities As Parameter.LookUpSupplier) As Parameter.LookUpSupplier
        Dim LookUpSupplierBO As ILookUpSupplier
        LookUpSupplierBO = ComponentFactory.CreateLookUpSupplier
        Return LookUpSupplierBO.GetListData(oEntities)
    End Function
End Class
