

#Region "Import"

Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
Imports Maxiloan

#End Region
Public Class GenerateReportController
    Public Function ListReport(ByVal oEntities As Parameter.GenerateReport) As DataSet
        Dim GenerateReportBO As IGenerateReport
        GenerateReportBO = ComponentFactory.CreateGenerateReport
        Return GenerateReportBO.ListReport(oEntities)
    End Function
End Class
