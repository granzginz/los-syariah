Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface

Public Class LookUpZipCodeController
    Public Function GetCitySearch(ByVal strConnection As String) As DataTable
        Dim LookUpZipCodeBO As ILookUpZipCode
        LookUpZipCodeBO = ComponentFactory.CreateLookUpZipCode()
        Return LookUpZipCodeBO.GetCitySearch(strConnection)
    End Function

    Public Function GetListData(ByVal strConnection As String, ByVal cmdWhere As String, ByVal currentPage As Integer, ByVal pagesize As Integer, ByVal SortBy As String) As Parameter.LookUpZipCode
        Dim LookUpZipCodeBO As ILookUpZipCode
        LookUpZipCodeBO = ComponentFactory.CreateLookUpZipCode()
        Return LookUpZipCodeBO.GetListData(strConnection, cmdWhere, currentPage, pagesize, SortBy)
    End Function
End Class
