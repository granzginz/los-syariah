

Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface
Imports Maxiloan

Public Class LookUpCustomerController

    Public Function GetListData(ByVal oEntities As Parameter.LookUpCustomer) As Parameter.LookUpCustomer
        Dim LookUpCustomerBO As ILookUpCustomer
        LookUpCustomerBO = ComponentFactory.CreateLookUpCustomer
        Return LookUpCustomerBO.GetListData(oEntities)
    End Function


End Class
