
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface

Public Class LookUpProductOfferingController
    Public Function GetProductBranch(ByVal branchid As String, ByVal strconnection As String) As DataTable
        Dim LookUpProductOfferingBO As ILookUpProductOffering
        LookUpProductOfferingBO = ComponentFactory.CreateLookUpProductOffering
        Return LookUpProductOfferingBO.GetProductBranch(branchid, strconnection)
    End Function

    Public Function GetListData(ByVal oEntities As Parameter.LookUpProductOffering) As Parameter.LookUpProductOffering
        Dim LookUpProductOfferingBO As ILookUpProductOffering
        LookUpProductOfferingBO = ComponentFactory.CreateLookUpProductOffering
        Return LookUpProductOfferingBO.GetListData(oEntities)
    End Function

End Class


