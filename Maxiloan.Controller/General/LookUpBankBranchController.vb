Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface

Public Class LookUpBankBranchController
    Public Function GetCitySearch(ByVal strConnection As String) As DataTable
        Dim LookUp As ILookUpBankBranch
        LookUp = ComponentFactory.CreateLookUpBankBranch()
        Return LookUp.GetCitySearch(strConnection)
    End Function

    Public Function GetListData(ByVal strConnection As String, ByVal cmdWhere As String, _
    ByVal currentPage As Integer, ByVal pagesize As Integer, ByVal SortBy As String) As Parameter.LookUpBankBranch
        Dim LookUp As ILookUpBankBranch
        LookUp = ComponentFactory.CreateLookUpBankBranch()
        Return LookUp.GetListData(strConnection, cmdWhere, currentPage, pagesize, SortBy)
    End Function

End Class
