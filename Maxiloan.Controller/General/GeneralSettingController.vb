

#Region "Import"

Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

#End Region

Public Class GeneralSettingController
    Public Function GetGeneralSetting(ByVal oEntities As Parameter.GeneralSetting) As Parameter.GeneralSetting
        Dim GetGeneralSettingBO As IGeneralSetting
        GetGeneralSettingBO = ComponentFactory.CreateGeneralSetting
        Return GetGeneralSettingBO.GetGeneralSetting(oEntities)
    End Function

    Public Function GeneralSettingSaveEdit(ByVal oEntities As Parameter.GeneralSetting) As Boolean
        Dim GeneralSettingSaveEditBO As IGeneralSetting
        GeneralSettingSaveEditBO = ComponentFactory.CreateGeneralSetting
        Return GeneralSettingSaveEditBO.GeneralSettingSaveEdit(oEntities)
    End Function

    Public Function GetGeneralSettingReport(ByVal customClass As Parameter.GeneralSetting) As Parameter.GeneralSetting
        Dim BO As IGeneralSetting
        BO = ComponentFactory.CreateGeneralSetting
        Return BO.GetGeneralSettingReport(customClass)
    End Function

    Public Function GetGeneralSettingByID(ByVal oCustomClass As Parameter.GeneralSetting) As Parameter.GeneralSetting
        Dim GetGeneralSettingBO As IGeneralSetting
        GetGeneralSettingBO = ComponentFactory.CreateGeneralSetting
        Return GetGeneralSettingBO.GetGeneralSettingByID(oCustomClass)
    End Function

    Public Function GetGeneralSettingValue(ByVal oCustomClass As Parameter.GeneralSetting) As String
        Dim oInterface As IGeneralSetting
        oInterface = ComponentFactory.CreateGeneralSetting
        Return oInterface.GetGeneralSettingValue(oCustomClass)
    End Function
End Class
