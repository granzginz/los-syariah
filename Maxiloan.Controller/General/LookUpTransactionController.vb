

Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface


Public Class LookUpTransactionController
    Public Function GetProcessID(ByVal strConnection As String, ByVal TransactionID As String) As String
        Dim LookUpTransactionBO As ILookUpTransaction
        LookUpTransactionBO = ComponentFactory.CreateLookUpTransaction()
        Return LookUpTransactionBO.GetProcessID(strConnection, TransactionID)
    End Function

    Public Function GetListData(ByVal strConnection As String, ByVal cmdWhere As String, ByVal currentPage As Integer, ByVal pagesize As Integer, ByVal SortBy As String) As Parameter.LookUpTransaction
        Dim LookUpTransactionBO As ILookUpTransaction
        LookUpTransactionBO = ComponentFactory.CreateLookUpTransaction()
        Return LookUpTransactionBO.GetListData(strConnection, cmdWhere, currentPage, pagesize, SortBy)
    End Function
End Class
