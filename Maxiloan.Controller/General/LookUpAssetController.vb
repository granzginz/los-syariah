
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface
Imports Maxiloan

Public Class LookUpAssetController
    Public Function GetListData(ByVal oEntities As Parameter.LookUpAsset) As Parameter.LookUpAsset
        Dim LookUpAssetBO As ILookUpAsset
        LookUpAssetBO = ComponentFactory.CreateLookUpAsset
        Return LookUpAssetBO.GetListData(oEntities)
    End Function

    Public Function GetListDataMulti(ByVal oEntities As Parameter.LookUpAsset) As Parameter.LookUpAsset
        Dim LookUpAssetBO As ILookUpAsset
        LookUpAssetBO = ComponentFactory.CreateLookUpAsset
        Return LookUpAssetBO.GetListDataMulti(oEntities)
    End Function

    Public Function GetListDataMultiConfirm(ByVal oEntities As Parameter.LookUpAsset) As Parameter.LookUpAsset
        Dim LookUpAssetBO As ILookUpAsset
        LookUpAssetBO = ComponentFactory.CreateLookUpAsset
        Return LookUpAssetBO.GetListDataMulti_Confirm(oEntities)
    End Function
End Class
