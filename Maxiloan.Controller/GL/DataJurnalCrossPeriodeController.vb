﻿Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface

Public Class DataJurnalCrossPeriodeController
    Public Function GetDataJurnal(ByVal oCustomClass As Parameter.DataJurnalCrossPeriode) As Parameter.DataJurnalCrossPeriode
        Dim oGetDataJurnal As IDataJurnalCrossPeriode
        oGetDataJurnal = ComponentFactory.CreateDataJurnalCrossPeriode()
        Return oGetDataJurnal.GetDataJurnal(oCustomClass)
    End Function
    Public Function GetTr_Nomor(ByVal oCustomclass As Parameter.DataJurnalCrossPeriode) As Parameter.DataJurnalCrossPeriode
        Dim oGetTr_Nomor As IDataJurnalCrossPeriode
        oGetTr_Nomor = ComponentFactory.CreateDataJurnalCrossPeriode()
        Return oGetTr_Nomor.GetTr_Nomor(oCustomclass)
    End Function
End Class
