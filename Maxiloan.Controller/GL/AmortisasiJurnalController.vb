﻿Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface

Public Class AmortisasiJurnalController
    Public Function GetData(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oGetDataJurnal As IAmortisasiJurnal
        oGetDataJurnal = ComponentFactory.CreateAmortisasiJurnal()
        Return oGetDataJurnal.GetData(oCustomClass)
    End Function
    Public Function GetDataEdit(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oGetDataJurnal As IAmortisasiJurnal
        oGetDataJurnal = ComponentFactory.CreateAmortisasiJurnal()
        Return oGetDataJurnal.GetDataEdit(oCustomClass)
    End Function
    Public Function GetDataDelete(ByVal oCustomClass As Parameter.AmortisasiJurnal) As String
        Dim oGetDataDelete As IAmortisasiJurnal
        oGetDataDelete = ComponentFactory.CreateAmortisasiJurnal()
        Return oGetDataDelete.GetDataDelete(oCustomClass)
    End Function
    Public Function SaveData(ByVal oParameter As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oSaveData As IAmortisasiJurnal
        oSaveData = ComponentFactory.CreateAmortisasiJurnal()
        Return oSaveData.SaveData(oParameter)
    End Function
    Public Function SaveEditData(ByVal oParameter As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oSaveData As IAmortisasiJurnal
        oSaveData = ComponentFactory.CreateAmortisasiJurnal()
        Return oSaveData.SaveEditData(oParameter)
    End Function
    'AmortisasiBiaya
    Public Function GetDataAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oGetDataJurnal As IAmortisasiJurnal
        oGetDataJurnal = ComponentFactory.CreateAmortisasiJurnal()
        Return oGetDataJurnal.GetDataAmortisasiBiaya(oCustomClass)
    End Function
    Public Function GetDataSchemeAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oGetDataJurnal As IAmortisasiJurnal
        oGetDataJurnal = ComponentFactory.CreateAmortisasiJurnal()
        Return oGetDataJurnal.GetDataSchemeAmortisasiBiaya(oCustomClass)
    End Function
    Public Function GetDataGridAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oGetDataJurnal As IAmortisasiJurnal
        oGetDataJurnal = ComponentFactory.CreateAmortisasiJurnal()
        Return oGetDataJurnal.GetDataGridAmortisasiBiaya(oCustomClass)
    End Function
    Public Function AmortisasiBiayaSave(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oGetDataJurnal As IAmortisasiJurnal
        oGetDataJurnal = ComponentFactory.CreateAmortisasiJurnal()
        Return oGetDataJurnal.AmortisasiBiayaSave(oCustomClass)
    End Function
    Public Function GenerateJournal(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oGetDataJurnal As IAmortisasiJurnal
        oGetDataJurnal = ComponentFactory.CreateAmortisasiJurnal()
        Return oGetDataJurnal.GenerateJournal(oCustomClass)
    End Function
    Public Function GenerateJournalList(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oGetDataJurnal As IAmortisasiJurnal
        oGetDataJurnal = ComponentFactory.CreateAmortisasiJurnal()
        Return oGetDataJurnal.GenerateJournalList(oCustomClass)
    End Function

    Public Function GetApprovalAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal, ByVal strApproval As String) As Parameter.AmortisasiJurnal
        Dim oGetDataJurnal As IAmortisasiJurnal
        oGetDataJurnal = ComponentFactory.CreateAmortisasiJurnal()
        Return oGetDataJurnal.GetApprovalAmortisasiBiaya(oCustomClass, strApproval)
    End Function
    Public Function GetGridApprovalAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oGetDataJurnal As IAmortisasiJurnal
        oGetDataJurnal = ComponentFactory.CreateAmortisasiJurnal()
        Return oGetDataJurnal.GetGridApprovalAmortisasiBiaya(oCustomClass)
    End Function

    Public Function GetViewSchemeAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oGetDataJurnal As IAmortisasiJurnal
        oGetDataJurnal = ComponentFactory.CreateAmortisasiJurnal()
        Return oGetDataJurnal.GetViewSchemeAmortisasiBiaya(oCustomClass)
    End Function
    Public Function Get_RequestNo(ByVal oCustomClass As Parameter.AmortisasiJurnal) As String
        Dim BO As IAmortisasiJurnal
        BO = ComponentFactory.CreateAmortisasiJurnal()
        Return BO.Get_RequestNo(oCustomClass)
    End Function
End Class
