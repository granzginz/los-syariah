Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess

Public Class BalanceSheetSetupController
    Public Function SelectData(ByRef totalRec As Integer, cnn As String, ByVal ParamArray param() As Object) As IList(Of BalanceSheetSetup)
        Return ComponentFactory.CreateBalanceSheet().SelectData(totalRec, cnn, param)
    End Function
    Public Sub InsertData(cnn As String, ByVal data As BalanceSheetSetup)
        ComponentFactory.CreateBalanceSheet().InsertData(cnn, data)
    End Sub

    Public Sub UpdateData(cnn As String, ByVal data As BalanceSheetSetup)
        ComponentFactory.CreateBalanceSheet().UpdateData(cnn, data)
    End Sub

    Public Sub DeleteData(cnn As String, ByVal data As String)
        ComponentFactory.CreateBalanceSheet().DeleteData(cnn, data)
    End Sub

    Public Function FindById(ByVal strCnn As String, ByVal reportId As String, ByVal companyId As String) As BalanceSheetSetup
        Return ComponentFactory.CreateBalanceSheet().FindById(strCnn, reportId, companyId)
    End Function

    Public Function SelectBSAccGroup(ByRef totalRec As Integer, cnn As String, ByVal ParamArray param() As Object) As IList(Of BalanceSheetAccGroup)
        Return ComponentFactory.CreateBalanceSheet().SelectBSAccGroup(totalRec, cnn, param)
    End Function
    Public Sub InsertBSAccGroup(cnn As String, ByVal data As BalanceSheetAccGroup)
        ComponentFactory.CreateBalanceSheet().InsertBSAccGroup(cnn, data)
    End Sub

    Public Sub UpdateBSAccGroup(cnn As String, ByVal data As BalanceSheetAccGroup)
        ComponentFactory.CreateBalanceSheet().UpdateBSAccGroup(cnn, data)
    End Sub

    Public Sub DeleteBSAccGroup(cnn As String, ByVal data As Integer)
        ComponentFactory.CreateBalanceSheet().DeleteBSAccGroup(cnn, data)
    End Sub

    Public Function FindByIdBSAccGroup(ByVal strCnn As String, ByVal rgid As Integer) As BalanceSheetAccGroup
        Return ComponentFactory.CreateBalanceSheet().FindByIdBSAccGroup(strCnn, rgid)
    End Function

    Public Function SelectAllBSAccGroup(ByVal cnn As String) As DataTable
        Return ComponentFactory.CreateBalanceSheet().SelectAllBSAccGroup(cnn)
    End Function

    Public Function SelectBSAccDetail(ByRef totalRec As Integer, cnn As String, ByVal ParamArray param() As Object) As IList(Of BalanceSheetAccDetail)
        Return ComponentFactory.CreateBalanceSheet().SelectBSAccDetail(totalRec, cnn, param)
    End Function
    Public Sub InsertBSAccDetail(cnn As String, ByVal data As BalanceSheetAccDetail)
        ComponentFactory.CreateBalanceSheet().InsertBSAccDetail(cnn, data)
    End Sub

    Public Sub UpdateBSAccDetail(cnn As String, ByVal data As BalanceSheetAccDetail)
        ComponentFactory.CreateBalanceSheet().UpdateBSAccDetail(cnn, data)
    End Sub

    Public Sub DeleteBSAccDetail(cnn As String, ByVal data As Integer, ByVal coa As String)
        ComponentFactory.CreateBalanceSheet().DeleteBSAccDetail(cnn, data, coa)
    End Sub

    Public Function FindByIdBSAccDetail(ByVal strCnn As String, ByVal rgid As Integer, ByVal coa As String) As BalanceSheetAccDetail
        Return ComponentFactory.CreateBalanceSheet().FindByIdBSAccDetail(strCnn, rgid, coa)
    End Function

    Public Function SelectAllCOA(ByVal cnn As String) As DataTable
        Return ComponentFactory.CreateBalanceSheet().SelectAllCOA(cnn)
    End Function


    Public Function SelectBSGroupDetail(ByRef totalRec As Integer, cnn As String, ByVal ParamArray param() As Object) As IList(Of BalanceSheetGroupDetail)
        Return ComponentFactory.CreateBalanceSheet().SelectBSGroupDetail(totalRec, cnn, param)
    End Function
    Public Sub InsertBSGroupDetail(cnn As String, ByVal data As BalanceSheetGroupDetail)
        ComponentFactory.CreateBalanceSheet().InsertBSGroupDetail(cnn, data)
    End Sub

    Public Sub UpdateBSGroupDetail(cnn As String, ByVal data As BalanceSheetGroupDetail)
        ComponentFactory.CreateBalanceSheet().UpdateBSGroupDetail(cnn, data)
    End Sub

    Public Sub DeleteBSGroupDetail(cnn As String, ByVal reportgroupid As Integer, ByVal groupid As Integer)
        ComponentFactory.CreateBalanceSheet().DeleteBSGroupDetail(cnn, reportgroupid, groupid)
    End Sub

    Public Function FindByIdBSGroupDetail(ByVal strCnn As String, ByVal reportgroupid As Integer, ByVal groupid As Integer) As BalanceSheetGroupDetail
        Return ComponentFactory.CreateBalanceSheet().FindByIdBSGroupDetail(strCnn, reportgroupid, groupid)
    End Function

    Public Function SelectAllBSGroupDetail(ByVal cnn As String, ByVal RID As String) As DataTable
        Return ComponentFactory.CreateBalanceSheet().SelectAllBSGroupDetail(cnn, RID)
    End Function
    Public Function GetAccountCOA(ByVal strConnection As String, ByVal ReportGroupId As String) As DataTable
        Return ComponentFactory.CreateBalanceSheet().GetAccountCOA(strConnection, ReportGroupId)
    End Function

End Class
