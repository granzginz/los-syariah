Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess 

Public Class COAController
    Public Function SelectData(ByRef totalRec As Integer, cnn As String, ByVal fromLookup As Boolean, isLeaf As Boolean, ByVal ParamArray param() As Object) As IList(Of MasterAccountObject)
        Return ComponentFactory.CreateCOA().SelectData(totalRec, cnn, fromLookup, isLeaf, param)
    End Function
    Public Function SelectDataIsLeaf(ByRef totalRec As Integer, cnn As String, ByVal fromLookup As Boolean, isLeaf As Boolean, ByVal ParamArray param() As Object) As IList(Of MasterAccountObject)
        Return ComponentFactory.CreateCOA().SelectDataIsLeaf(totalRec, cnn, fromLookup, isLeaf, param)
    End Function

    Public Sub InsertData(cnn As String, ByVal data As MasterAccountObject)
        ComponentFactory.CreateCOA().InsertData(cnn, data)
    End Sub

    Public Sub UpdateData(cnn As String, ByVal data As MasterAccountObject)
        ComponentFactory.CreateCOA().UpdateData(cnn, data)
    End Sub

    Public Sub DeleteData(cnn As String, ByVal data As String, ByVal addfree As String)
        ComponentFactory.CreateCOA().DeleteData(cnn, data, addfree)
    End Sub

    Public Function FindById(ByVal strCnn As String, ByVal coaId As String, ByVal branchId As String, ByVal companyId As String) As MasterAccountObject
        Return ComponentFactory.CreateCOA().FindById(strCnn, coaId, branchId, companyId)
    End Function
    Public Function FindByDesc(ByVal strCnn As String, ByVal Description As String, ByVal branchId As String, ByVal companyId As String) As MasterAccountObject
        Return ComponentFactory.CreateCOA().FindByDesc(strCnn, Description, branchId, companyId)
    End Function

    Public Function AutoComplateCoaId(strCnn As String, coaid As String) As List(Of String)
        Return ComponentFactory.CreateCOA().AutoComplateCoaId(strCnn, coaid)
    End Function
    Public Function AutoComplateCoaDesc(strCnn As String, Description As String) As List(Of String)
        Return ComponentFactory.CreateCOA().AutoComplateCoaDesc(strCnn, Description)
    End Function
End Class
