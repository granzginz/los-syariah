﻿Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface

Public Class GlYearPeriodController
    Private oInterface As IJournalVoucher

    Sub New()
        oInterface = ComponentFactory.CreateJournalVoucher()
    End Sub

    Public Function YearPeriods(ByVal cnn As String, ByVal companyid As String, ByVal branchid As String, ByVal year As Integer, month As Integer) As IList(Of GlPeriod)
        Return oInterface.YearPeriods(cnn, companyid, branchid, year, month)
    End Function

    Public Function GenerateYearPeriods(cnn As String, year As GlPeriodYear) As String
        Return oInterface.GenerateNewYearPeriods(cnn, year)
    End Function

    Public Function IsYearValid(ByVal cnn As String, year As Integer, companyId As String, branchid As String) As Boolean
        Return oInterface.IsYearValid(cnn, year, companyId, branchid)
    End Function
    'Taufik
    'Public Function CurrentYearPeriod(cnn As String, companyid As String, branchid As String) As GlPeriod
    '    Return oInterface.CurrentYearPeriod(cnn, companyid, branchid) 
    'End Function

    Public Function YearlyCloseValidate(ByVal cnn As String, ByVal companyId As String) As String
        Return oInterface.YearlyCloseValidate(cnn, companyId)
    End Function


End Class
