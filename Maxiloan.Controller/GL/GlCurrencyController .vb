﻿Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess

Public Class GlCurrencyController 

    Public Function GetGlCurrencies(cnn As String) As IList(Of GlCurrency)
        Return ComponentFactory.CreateCOA().GetGlCurrencies(cnn)
    End Function
     
    Public Function GetHomeCurrency(cnn As String) As IList(Of GlCurrency)
        Return ComponentFactory.CreateCOA().GetHomeCurrency(cnn)
    End Function
    Public Function GetDefaultCurrency(cnn As String) As IList(Of GlCurrency)
        Return ComponentFactory.CreateCOA().GetDefaultCurrency(cnn)
    End Function


End Class
