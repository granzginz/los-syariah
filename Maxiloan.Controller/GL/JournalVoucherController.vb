﻿Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess


Public Class JournalVoucherController
    Private oInterface As IJournalVoucher

    Sub New()
        oInterface = ComponentFactory.CreateJournalVoucher()
    End Sub 
    Public Function GetGLMasterSequence(strCnn As String, ByVal ParamArray param() As Object) As IList(Of ValueTextObject)
        Return oInterface.GetGlMasterSequence(strCnn, param)
    End Function
     
    Public Function GetJurnalVoucherByTrNo(ByVal cnn As String, ByVal transNo As String) As GlJournalVoucher
        Return oInterface.GetJurnalVoucherByTrNo(cnn, transNo)
    End Function


    Public Function GetCompanyId(ccn As String) As String
        Return oInterface.GetCompanyId(ccn)
    End Function 
    Public Function DownloadToDBF(ByVal cnn As String, period1 As DateTime, period2 As DateTime, branch As String) As DataTable
        Return oInterface.DownloadToDBF(cnn, period1, period2, branch)
    End Function

    Public Sub ValidateCsvConponen(cnn As String, components As IList(Of GlJournalVoucher))
        oInterface.ValidateCsvComponen(cnn, components)
    End Sub

    Public Function DoUploadCsvGlJournals(cnn As String, component As IList(Of GlJournalVoucher)) As String
        Return oInterface.DoUploadCsvGlJournals(cnn, component)
    End Function

    Public Function IsJournals(cnn As String, trDate As DateTime) As Boolean
        Return oInterface.IsJournals(cnn, trDate)
    End Function

    Public Function DoDailyJournalPost(cnn As String, ByVal ParamArray param As Object()) As String
        Return oInterface.DoDailyJournalPost(cnn, param)
    End Function

    Public Function GetGlJournalAvailablePost(ByRef totalrec As Integer, ByVal strCnn As String, ByVal ParamArray param As Object()) As GlJournalPostAvailable ' IList(Of GlJournalPostAvailable)
        Return oInterface.GetGlJournalAvailablePost(totalrec, strCnn, param)
    End Function

    Public Function SelectInitialBudget(ByRef totRecord As Integer, strCnn As String, ByVal ParamArray param() As Object) As IList(Of InitialBudget)
        Return oInterface.SelectInitialBudget(totRecord, strCnn, param)
    End Function

    Public Function GetGlInitialFindOne(ByVal strCnn As String, ByVal param As InitialBudget) As InitialBudget
        Return oInterface.GetGlInitialFindOne(strCnn, param)
    End Function

    Public Function GlJournalAddUpdate(ByVal cnn As String, ByVal isAdd As Boolean, ByVal component As IList(Of GlJournalVoucher)) As String
        Return oInterface.GlJournalAddUpdate(cnn, isAdd, component)
    End Function
    Public Function GlJournalAddDelete(ByVal cnn As String, ByVal isAdd As Boolean, ByVal component As IList(Of GlJournalVoucher)) As String
        Return oInterface.GlJournalAddDelete(cnn, isAdd, component)
    End Function
    Public Function GlJournalHold(ByVal cnn As String, ByVal component As IList(Of GlJournalVoucher)) As String
        Return oInterface.GlJournalHold(cnn, component)
    End Function

    Public Function GlInitialBudgetUpdate(ByVal cnn As String, ByVal component As IList(Of InitialBudget)) As String
        Return oInterface.GlInitialBudgetUpdate(cnn, component)
    End Function

    Public Function GlInitialBudgetAdd(ByVal cnn As String, ByVal component As IList(Of InitialBudget)) As String
        Return oInterface.GlInitialBudgetAdd(cnn, component)
    End Function

    Public Function GlJournalVerify(ByVal cnn As String, ByVal param As IList(Of String)) As String
        Return oInterface.GlJournalVerify(cnn, param)
    End Function
    Public Function GlJournalApproval(ByVal cnn As String, ByVal param As JournalApproval, Optional ByVal approve As Boolean = False) As JournalApproval
        Return oInterface.GlJournalApproval(cnn, param, approve)
    End Function
    Public Function GlJournalPost(ByVal cnn As String, ByVal param As IList(Of String)) As String
        Return oInterface.GlJournalPost(cnn, param)
    End Function
     
    Public Function GlJournalApproval(ByVal cnn As String, ByVal param As IList(Of String)) As String
        Return oInterface.GlJournalApproval(cnn, param)
    End Function
    Public Function GLHistoryCOASelect(ByRef totalrec As Integer, ByVal strCnn As String, ByVal ParamArray param() As Object) As DataView
        Return oInterface.GLHistoryCOASelect(totalrec, strCnn, param)
    End Function
    Public Function GlJournalApprovalPage(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64) As IList(Of JournalApproval)
        Return oInterface.GlJournalApprovalPage(cnn, param, pageSz, currPg, totalReq)
    End Function
    'Function GlGelMasVirifyUpload(ByVal cnn As String, ByVal component As GelMas) As String
    '    Return oInterface.GlGelMasVirifyUpload(cnn, component)
    'End Function

    'Public Function GlGelMasUpload(ByVal cnn As String, ByVal component As GelMas) As String
    '    Return oInterface.GlGelMasUpload(cnn, component)
    'End Function

    'Public Function GlGelMasUploadValidate(ByVal cnn As String, ByVal component As GelMas) As String
    '    Return oInterface.GlGelMasUploadValidate(cnn, component)
    'End Function

    Public Function GlGelMasValidateCoaSelect(ByRef totRecord As Integer, strCnn As String, ByVal ParamArray param() As Object) As IList(Of ValueTextObject)
        Return oInterface.GLGelMASValidateCOASelect(totRecord, strCnn, param)
    End Function



    Public Function GetGlJournalAvailablePostPage(ByVal cnn As String,
                                                 ByVal companyId As String,
                                                 ByVal branchId As String,
                                                 ByVal trDateFrom As DateTime,
                                                 ByVal trDateTo As DateTime,
                                                 pageSz As Integer, currPg As Integer, ByRef totalReq As Int64) As IList(Of JournalApproval)
        Return oInterface.GetGlJournalAvailablePostPage(cnn, companyId, branchId, trDateFrom, trDateTo, pageSz, currPg, totalReq)
    End Function
    Public Function GlJournalVerifyPage(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64, ByVal wOpt As Integer, ByVal cmdwhere As String) As IList(Of JournalApproval)
        Return oInterface.GlJournalVerifyPage(cnn, param, pageSz, currPg, totalReq, wOpt, cmdwhere)
    End Function




    Public Function DoJournalMonthlyClose(ByVal cnn As String, ByVal ParamArray param As Object())
        Return oInterface.DoJournalMonthlyClose(cnn, param)
    End Function

    Public Function DoJournalYearlyClose(ByVal cnn As String, ByVal companyId As String, ByVal userId As String) As String
        Return oInterface.DoJournalYearlyClose(cnn, companyId, userId)
    End Function
    Public Function GetHifrontDetails(cnn As String, dateProc As Date) As System.Text.StringBuilder
        Return oInterface.GetHifrontDetails(cnn, dateProc)
    End Function
    Public Function GetJournalTransaction(oCustom As Parameter.JournalTransaction) As Parameter.JournalTransaction
        Return oInterface.GetJournalTransaction(oCustom)
    End Function
    Public Function GlJournalUnPostPage(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64) As IList(Of JournalApproval)
        Return oInterface.GlJournalUnPostPage(cnn, param, pageSz, currPg, totalReq)
    End Function
    Public Function GlJournalUnPost(ByVal cnn As String, ByVal param As IList(Of String)) As String
        Return oInterface.GlJournalUnPost(cnn, param)
    End Function
    Function GlJournalUnPostPageFilter(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64, ByVal wOpt As Integer, ByVal cmdwhere As String) As IList(Of JournalApproval)
        Return oInterface.GlJournalUnPostPageFilter(cnn, param, pageSz, currPg, totalReq, wOpt, cmdwhere)
    End Function

    Public Function GlJournalPageNotSuccess(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64) As IList(Of JournalApproval)
        Return oInterface.GlJournalPageNotSuccess(cnn, param, pageSz, currPg, totalReq)
    End Function
End Class
