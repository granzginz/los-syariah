#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AssetDocCheckInController
    Public Function GetDocumentCheckInH(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
        Dim DocumentCheckInBO As IDocumentCheckIn
        Try
            DocumentCheckInBO = ComponentFactory.CreateAssetDocCheckIn()
        Catch ex As Exception
            Throw ex
        End Try
        Try
            Return DocumentCheckInBO.GetDocumentCheckInH(aDocumentCheckIn)
        Catch ex As Exception
            Throw ex
        End Try
        Return aDocumentCheckIn
    End Function
    Public Function GetDocumentCheckInD(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
        Dim DocumentCheckInBO As IDocumentCheckIn
        Try
            DocumentCheckInBO = ComponentFactory.CreateAssetDocCheckIn()
        Catch ex As Exception
            Throw ex
        End Try
        Try
            Return DocumentCheckInBO.GetDocumentCheckInD(aDocumentCheckIn)
        Catch ex As Exception
            Throw ex
        End Try
        Return aDocumentCheckIn
    End Function
    Public Function GetDocumentSenderList(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
        Dim DocumentCheckInBO As IDocumentCheckIn
        Try
            DocumentCheckInBO = ComponentFactory.CreateAssetDocCheckIn()
        Catch ex As Exception
            Throw ex
        End Try
        Try
            Return DocumentCheckInBO.GetDocumentSenderList(aDocumentCheckIn)
        Catch ex As Exception
            Throw ex
        End Try
        Return aDocumentCheckIn
    End Function
    Public Function GetOutstandingDocument(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
        Dim DocumentCheckInBO As IDocumentCheckIn
        Try
            DocumentCheckInBO = ComponentFactory.CreateAssetDocCheckIn()
        Catch ex As Exception
            Throw ex
        End Try
        Try
            Return DocumentCheckInBO.GetOutstandingDocument(aDocumentCheckIn)
        Catch ex As Exception
            Throw ex
        End Try
        Return aDocumentCheckIn
    End Function
    Public Function SaveDocumentCheckIn(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
        Dim DocumentCheckInBO As IDocumentCheckIn
        Try
            DocumentCheckInBO = ComponentFactory.CreateAssetDocCheckIn()
        Catch ex As Exception
            Throw ex
        End Try
        Try
            Return DocumentCheckInBO.SaveDocumentCheckIn(aDocumentCheckIn)
        Catch ex As Exception
            Throw ex
        End Try
        Return aDocumentCheckIn
    End Function
    Public Function DeleteDocumentCheckIn(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
        Dim DocumentCheckInBO As IDocumentCheckIn
        Try
            DocumentCheckInBO = ComponentFactory.CreateAssetDocCheckIn()
        Catch ex As Exception
            Throw ex
        End Try
        Try
            Return DocumentCheckInBO.DeleteDocumentCheckIn(aDocumentCheckIn)
        Catch ex As Exception
            Throw ex
        End Try
        Return aDocumentCheckIn
    End Function
    Public Function GetDocumentCheckInRpt(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
        Dim DocumentCheckInBO As IDocumentCheckIn
        Try
            DocumentCheckInBO = ComponentFactory.CreateAssetDocCheckIn()
        Catch ex As Exception
            Throw ex
        End Try
        Try
            Return DocumentCheckInBO.GetDocumentCheckInRpt(aDocumentCheckIn)
        Catch ex As Exception
            Throw ex
        End Try
        Return aDocumentCheckIn
    End Function
    Public Function PostDocumentCheckIn(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
        Dim DocumentCheckInBO As IDocumentCheckIn
        Try
            DocumentCheckInBO = ComponentFactory.CreateAssetDocCheckIn
        Catch ex As Exception
            Throw ex
        End Try
        Try
            Return DocumentCheckInBO.PostDocumentCheckIn(aDocumentCheckIn)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetBranchDocumentFilling(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
        Dim DocumentCheckInBO As IDocumentCheckIn
        Try
            DocumentCheckInBO = ComponentFactory.CreateAssetDocCheckIn()
        Catch ex As Exception
            Throw ex
        End Try
        Try
            Return DocumentCheckInBO.GetBranchDocumentFilling(aDocumentCheckIn)
        Catch ex As Exception
            Throw ex
        End Try
        Return aDocumentCheckIn
    End Function

    Public Function GetBranchDocumentRack(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
        Dim DocumentCheckInBO As IDocumentCheckIn
        Try
            DocumentCheckInBO = ComponentFactory.CreateAssetDocCheckIn()
        Catch ex As Exception
            Throw ex
        End Try
        Try
            Return DocumentCheckInBO.GetBranchDocumentRack(aDocumentCheckIn)
        Catch ex As Exception
            Throw ex
        End Try
        Return aDocumentCheckIn
    End Function
End Class
