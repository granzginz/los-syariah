#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class DocumentRackController
#Region "Document Rack"
    Public Function GetDocumentRack(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
        Dim DocumentRackBO As IDocumentRack
        Try
            DocumentRackBO = ComponentFactory.CreateDocumentRack()
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
            Return aoDocumentRack
        End Try
        Try
            Return DocumentRackBO.GetDocumentRack(aoDocumentRack)
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return aoDocumentRack
    End Function
    Public Function DeleteDocumentRack(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
        aoDocumentRack.UpdateAction = 3
        Dim DocumentRackBO As IDocumentRack
        Try
            DocumentRackBO = ComponentFactory.CreateDocumentRack()
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
            Return aoDocumentRack
        End Try
        Try
            Return DocumentRackBO.DeleteDocumentRack(aoDocumentRack)
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return aoDocumentRack
    End Function
    Public Function GetDocumentRackEdit(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
        aoDocumentRack.UpdateAction = 4 'Retrieve a record by RackID
        Dim DocumentRackBO As IDocumentRack
        Try
            DocumentRackBO = ComponentFactory.CreateDocumentRack()
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
            Return aoDocumentRack
        End Try
        Try
            Return DocumentRackBO.GetDocumentRackEdit(aoDocumentRack)
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return aoDocumentRack
    End Function
    Public Function InsertDocumentRack(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
        aoDocumentRack.UpdateAction = 1 'Insert a record 
        Dim DocumentRackBO As IDocumentRack
        Try
            DocumentRackBO = ComponentFactory.CreateDocumentRack()
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
            Return aoDocumentRack
        End Try
        Try
            Return DocumentRackBO.SaveDocumentRack(aoDocumentRack)
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return aoDocumentRack
    End Function
    Public Function UpdateDocumentRack(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
        aoDocumentRack.UpdateAction = 2 'Update a record 
        Dim DocumentRackBO As IDocumentRack
        Try
            DocumentRackBO = ComponentFactory.CreateDocumentRack()
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
            Return aoDocumentRack
        End Try
        Try
            Return DocumentRackBO.SaveDocumentRack(aoDocumentRack)
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return aoDocumentRack
    End Function
    Public Function GetBranchListData(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
        Dim DocumentRackBO As IDocumentRack
        Try
            DocumentRackBO = ComponentFactory.CreateDocumentRack()
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
            Return aoDocumentRack
        End Try
        Try
            Return DocumentRackBO.GetBranchListData(aoDocumentRack)
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return aoDocumentRack
    End Function
    Public Function GetDocumentRackReport(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
        Return aoDocumentRack
    End Function
#End Region

#Region "Document Filing"
    Public Function GetDocumentFiling(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
        Dim DocumentRackBO As IDocumentRack
        Try
            DocumentRackBO = ComponentFactory.CreateDocumentRack()
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Try
            Return DocumentRackBO.GetDocumentFiling(aoDocumentRack)
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
    End Function
    Public Function DeleteDocumentFiling(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
        aoDocumentRack.UpdateAction = 3
        Dim DocumentRackBO As IDocumentRack
        Try
            DocumentRackBO = ComponentFactory.CreateDocumentRack()
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
            Return aoDocumentRack
        End Try
        Try
            Return DocumentRackBO.DeleteDocumentFiling(aoDocumentRack)
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return aoDocumentRack
    End Function
    Public Function GetDocumentFilingEdit(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
        aoDocumentRack.UpdateAction = 4 'Retrieve a record by RackID
        Dim DocumentRackBO As IDocumentRack
        Try
            DocumentRackBO = ComponentFactory.CreateDocumentRack()
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
            Return aoDocumentRack
        End Try
        Try
            Return DocumentRackBO.GetDocumentFilingEdit(aoDocumentRack)
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return aoDocumentRack
    End Function
    Public Function InsertDocumentFiling(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
        aoDocumentRack.UpdateAction = 1 'Insert a record 
        Dim DocumentRackBO As IDocumentRack
        Try
            DocumentRackBO = ComponentFactory.CreateDocumentRack()
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
            Return aoDocumentRack
        End Try
        Try
            Return DocumentRackBO.SaveDocumentFiling(aoDocumentRack)
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return aoDocumentRack
    End Function
    Public Function UpdateDocumentFiling(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
        aoDocumentRack.UpdateAction = 2 'Update a record 
        Dim DocumentRackBO As IDocumentRack
        Try
            DocumentRackBO = ComponentFactory.CreateDocumentRack()
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
            Return aoDocumentRack
        End Try
        Try
            Return DocumentRackBO.SaveDocumentFiling(aoDocumentRack)
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return aoDocumentRack
    End Function
    Public Function GetDocumentFilingReport(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
        Return aoDocumentRack
    End Function
#End Region
End Class
