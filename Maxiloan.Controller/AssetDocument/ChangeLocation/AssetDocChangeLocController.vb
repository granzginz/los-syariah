#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AssetDocChangeLocController
#Region "AssetDocumentReport"
    Public Function AssetDocumentReport(ByVal customClass As Parameter.DocumentChangeLocation) As DataSet
        Dim oDocAct As IDocumentChangeLocation
        oDocAct = ComponentFactory.CreateAssetDocRep
        Return oDocAct.AssetDocumentReport(customClass)
    End Function
#End Region
End Class
