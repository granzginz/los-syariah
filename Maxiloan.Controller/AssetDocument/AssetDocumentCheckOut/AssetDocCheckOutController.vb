#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AssetDocCheckOutController
    Public Function GetDocumentCheckOutH(ByVal aDocumentCheckOut As Parameter.DocumentCheckOut) As Parameter.DocumentCheckOut
        Dim DocumentCheckOutBO As IDocumentCheckOut
        Try
            DocumentCheckOutBO = ComponentFactory.CreateAssetDocCheckOut
            Return DocumentCheckOutBO.GetDocumentCheckOutH(aDocumentCheckOut)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetDocumentCheckOutD(ByVal aDocumentCheckOut As Parameter.DocumentCheckOut) As Parameter.DocumentCheckOut
        Dim DocumentCheckOutBO As IDocumentCheckOut
        Try
            DocumentCheckOutBO = ComponentFactory.CreateAssetDocCheckOut
            Return DocumentCheckOutBO.GetDocumentCheckOutD(aDocumentCheckOut)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetDocumentRecipientList(ByVal aDocumentCheckOut As Parameter.DocumentCheckOut) As Parameter.DocumentCheckOut
        Dim DocumentCheckOutBO As IDocumentCheckOut
        Try
            DocumentCheckOutBO = ComponentFactory.CreateAssetDocCheckOut
            Return DocumentCheckOutBO.GetDocumentRecipientList(aDocumentCheckOut)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetDocumentOnHandForCheckOut(ByVal aDocumentCheckOut As Parameter.DocumentCheckOut) As Parameter.DocumentCheckOut
        Dim DocumentCheckOutBO As IDocumentCheckOut
        Try
            DocumentCheckOutBO = ComponentFactory.CreateAssetDocCheckOut
            Return DocumentCheckOutBO.GetDocumentOnHandForCheckOut(aDocumentCheckOut)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SaveDocumentCheckOut(ByVal aDocumentCheckOut As Parameter.DocumentCheckOut) As Parameter.DocumentCheckOut
        Dim DocumentCheckOutBO As IDocumentCheckOut
        Try
            DocumentCheckOutBO = ComponentFactory.CreateAssetDocCheckOut
            Return DocumentCheckOutBO.SaveDocumentCheckOut(aDocumentCheckOut)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function DeleteDocumentCheckOut(ByVal aDocumentCheckOut As Parameter.DocumentCheckOut) As Parameter.DocumentCheckOut
        Dim DocumentCheckOutBO As IDocumentCheckOut
        Try
            DocumentCheckOutBO = ComponentFactory.CreateAssetDocCheckOut
            Return DocumentCheckOutBO.DeleteDocumentCheckOut(aDocumentCheckOut)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetDocumentCheckOutRpt(ByVal aDocumentCheckOut As Parameter.DocumentCheckOut) As Parameter.DocumentCheckOut
        Dim DocumentCheckOutBO As IDocumentCheckOut
        Try
            DocumentCheckOutBO = ComponentFactory.CreateAssetDocCheckOut
            Return DocumentCheckOutBO.GetDocumentCheckOutRpt(aDocumentCheckOut)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function PostDocumentCheckOut(ByVal aDocumentCheckOut As Parameter.DocumentCheckOut) As Parameter.DocumentCheckOut
        Dim DocumentCheckOutBO As IDocumentCheckOut
        Try
            DocumentCheckOutBO = ComponentFactory.CreateAssetDocCheckOut
            Return DocumentCheckOutBO.PostDocumentCheckOut(aDocumentCheckOut)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
