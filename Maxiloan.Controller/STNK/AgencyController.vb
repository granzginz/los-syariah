
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class AgencyController
    Public Function STNKAgencyPaging(ByVal customclass As Parameter.Agency) As Parameter.Agency
        Dim oSTNKAgent As IAgency
        oSTNKAgent = ComponentFactory.CreateAgency
        Return oSTNKAgent.STNKAgencyPaging(customclass)
    End Function


    Public Function STNKAgencyView(ByVal strConnection As String, _
                                    ByVal BranchId As String, _
                                    ByVal AgentId As String) As DataTable
        Dim oSTNKAgent As IAgency
        oSTNKAgent = ComponentFactory.CreateAgency
        Return oSTNKAgent.STNKAgencyView(strConnection, BranchId, AgentId)
    End Function
    Public Sub STNKAgencySave(ByVal oAgentcy As Parameter.Agency, _
                                    ByVal oBankAccount As Parameter.BankAccount, _
                                    ByVal oAddress As Parameter.Address, _
                                    ByVal oPersonal As Parameter.Personal)
        Dim oSTNKAgent As IAgency
        oSTNKAgent = ComponentFactory.CreateAgency
        oSTNKAgent.STNKAgencySave(oAgentcy, oBankAccount, oAddress, oPersonal)

    End Sub
    Public Sub STNKAgencyUpdate(ByVal oAgentcy As Parameter.Agency, _
                                  ByVal oBankAccount As Parameter.BankAccount, _
                                  ByVal oAddress As Parameter.Address, _
                                  ByVal oPersonal As Parameter.Personal)

        Dim oSTNKAgent As IAgency
        oSTNKAgent = ComponentFactory.CreateAgency
        oSTNKAgent.STNKAgencyUpdate(oAgentcy, oBankAccount, oAddress, oPersonal)

    End Sub
    Public Sub STNKAgencyDelete(ByVal oAgentcy As Parameter.Agency)
        Dim oSTNKAgent As IAgency
        oSTNKAgent = ComponentFactory.CreateAgency
        oSTNKAgent.STNKAgencyDelete(oAgentcy)
    End Sub
    Public Function STNKAgencyReport(ByVal oCustomClass As Parameter.Agency) As Parameter.Agency
        Dim oSTNKAgent As IAgency
        oSTNKAgent = ComponentFactory.CreateAgency
        Return oSTNKAgent.STNKAgencyReport(oCustomClass)
    End Function
End Class
