
#Region "Imports"
Imports System.Diagnostics
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class STNKRenewalController
    Public Function GetSTNKRequest(ByVal customClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
        Dim BO As ISTNKRenewal
        BO = ComponentFactory.CreateSTNKRenewal
        Return BO.GetSTNKRequest(customClass)
    End Function
    Public Function SaveSTNKRequest(ByVal customClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
        Dim BO As ISTNKRenewal
        BO = ComponentFactory.CreateSTNKRenewal
        Return BO.SaveSTNKRequest(customClass)
    End Function
    Function SaveSTNKRegister(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
        Dim BO As ISTNKRenewal
        BO = ComponentFactory.CreateSTNKRenewal
        Return BO.SaveSTNKRegister(oCustomClass)
    End Function
    Function CancelSTNKRegister(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
        Dim BO As ISTNKRenewal
        BO = ComponentFactory.CreateSTNKRenewal
        Return BO.CancelSTNKRegister(oCustomClass)
    End Function
    Function STNKView(ByVal strConnection As String, ByVal BranchId As String) As DataTable
        Dim oSTNKAgent As ISTNKRenewal
        oSTNKAgent = ComponentFactory.CreateSTNKRenewal
        Return oSTNKAgent.STNKView(strConnection, BranchId)
    End Function
    Function SaveSTNKInvoicing(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
        Dim BO As ISTNKRenewal
        BO = ComponentFactory.CreateSTNKRenewal
        Return BO.SaveSTNKInvoicing(oCustomClass)
    End Function
    Function STNKInquiryReport(ByVal customclass As Parameter.STNKRenewal) As Parameter.STNKRenewal
        Dim BO As ISTNKRenewal
        BO = ComponentFactory.CreateSTNKRenewal
        Return BO.STNKInquiryReport(customclass)
    End Function
    Function STNKRenewalUpdatePrint(ByVal customclass As Parameter.STNKRenewal) As Boolean
        Dim BO As ISTNKRenewal
        BO = ComponentFactory.CreateSTNKRenewal
        Return BO.STNKRenewalUpdatePrint(customclass)
    End Function
    Function GetLastDate(ByVal strConnection As String) As String
        Dim BO As ISTNKRenewal
        BO = ComponentFactory.CreateSTNKRenewal
        Return BO.GetLastDate(strConnection)
    End Function
    Function GetReportSTNKRenewalIncome(ByVal strConnection As String, ByVal BranchID As String, ByVal BusinessDate As String) As DataSet
        Dim BO As ISTNKRenewal
        BO = ComponentFactory.CreateSTNKRenewal
        Return BO.GetReportSTNKRenewalIncome(strConnection, BranchID, BusinessDate)

    End Function

    Public Function ReportSTNKRenewal(ByVal oCustomClass As Parameter.STNKRenewal) As DataSet
        Dim BO As ISTNKRenewal
        BO = ComponentFactory.CreateSTNKRenewal
        Return BO.ReportSTNKRenewal(oCustomClass)
    End Function
    Function STNKRequestInquiryPaging(ByVal customclass As Parameter.STNKRenewal) As Parameter.STNKRenewal
        Dim BO As ISTNKRenewal
        BO = ComponentFactory.CreateSTNKRenewal
        Return BO.STNKRequestInquiryPaging(customclass)
    End Function
    Function SaveEstimasiBiayaSTNK(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
        Dim BO As ISTNKRenewal
        BO = ComponentFactory.CreateSTNKRenewal
        Return BO.SaveEstimasiBiayaSTNK(oCustomClass)
    End Function
End Class
