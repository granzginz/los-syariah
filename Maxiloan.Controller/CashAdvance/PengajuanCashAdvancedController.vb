﻿
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface
Public Class PengajuanCashAdvancedController

    Public Function GetListCashAdvance(ByVal oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance
        Dim PromissoryNoteBO As ICashAdvance
        PromissoryNoteBO = ComponentFactory.CreateCashAdvance()
        Return PromissoryNoteBO.GetListCashAdvance(oCustomClass)
    End Function

    Public Function GetPagingCashAdvance(ByVal oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance
        Dim PromissoryNoteBO As ICashAdvance
        PromissoryNoteBO = ComponentFactory.CreateCashAdvance()
        Return PromissoryNoteBO.GetPagingCashAdvance(oCustomClass)
    End Function

    Public Function SaveCashAdvance(ByVal oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance
        Dim PromissoryNoteBO As ICashAdvance
        PromissoryNoteBO = ComponentFactory.CreateCashAdvance()
        Return PromissoryNoteBO.SaveCashAdvance(oCustomClass)
    End Function

    Public Function SaveCashAdvanceRealisasiDetail(ByVal oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance
        Dim PromissoryNoteBO As ICashAdvance
        PromissoryNoteBO = ComponentFactory.CreateCashAdvance()
        Return PromissoryNoteBO.SaveCashAdvanceRealisasiDetail(oCustomClass)
    End Function

    Public Function GetListCashAdvanceDetail(ByVal oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance
        Dim PromissoryNoteBO As ICashAdvance
        PromissoryNoteBO = ComponentFactory.CreateCashAdvance()
        Return PromissoryNoteBO.GetListCashAdvanceDetail(oCustomClass)
    End Function

    Public Function ReverseReversalCashAdvanced(ByVal oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance
        Dim PromissoryNoteBO As ICashAdvance
        PromissoryNoteBO = ComponentFactory.CreateCashAdvance()
        Return PromissoryNoteBO.ReverseReversalCashAdvanced(oCustomClass)
    End Function

    Public Function SaveReverseReversalCashAdvanced(ByVal oCustomClass As Parameter.CashAdvance) As Parameter.CashAdvance
        Dim PromissoryNoteBO As ICashAdvance
        PromissoryNoteBO = ComponentFactory.CreateCashAdvance()
        Return PromissoryNoteBO.SaveReverseReversalCashAdvanced(oCustomClass)
    End Function

    'Public Sub SaveReverseReversalCashAdvanced(ByVal oCustomClass As Parameter.CashAdvance)
    '    Dim oSaveReverseReversalCashAdvanced As ICashAdvance
    '    oSaveReverseReversalCashAdvanced = ComponentFactory.CreateImplementasi()
    '    oSaveReverseReversalCashAdvanced.SaveReverseReversalCashAdvanced(oCustomClass)
    'End Sub
End Class
