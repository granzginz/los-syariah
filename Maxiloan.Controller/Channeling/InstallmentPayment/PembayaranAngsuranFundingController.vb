Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class PembayaranAngsuranFundingController
    Public Function GetListPembayaranAngsuran(ByVal oCustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        Return oInterface.getListPembayaranAngsuran(oCustomClass)
    End Function


    Public Sub DisbursePembayaranAngsuranFunding(ByVal oCustomClass As Parameter.DisbursePembayaranAngsuranFunding)
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        oInterface.DisbursePembayaranAngsuran(oCustomClass)
    End Sub

    Public Function GetListBonHijauFundingDisburse(ByVal oCustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        Return oInterface.getListBonHijauFundingDisburse(oCustomClass)
    End Function

    Public Function GetListData(ByVal strConnection As String, ByVal cmdWhere As String, ByVal currentPage As Integer, ByVal pagesize As Integer, ByVal SortBy As String) As Parameter.PembayaranAngsuranFunding
        Dim LookUpZipCodeBO As IPembayaranAngsuranFunding
        LookUpZipCodeBO = ComponentFactory.CreatePembayaranAngsuranFunding()
        Return LookUpZipCodeBO.GetListData(strConnection, cmdWhere, currentPage, pagesize, SortBy)
    End Function

    Public Function getFundingAngsJthTempoSummary(ByVal oCustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        Return oInterface.getFundingAngsJthTempoSummary(oCustomClass)
    End Function

    Public Function disburseFundingAngsuranJthTempo(ByVal oCustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        oInterface.disburseFundingAngsuranJthTempo(oCustomClass)
    End Function

    Sub prepaymentBatchProcess(ByVal CustomClass As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.PrepaymentBatchProcess(CustomClass)
    End Sub

    Public Function FundingPrepaymentReport(ByVal oCustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        Return oInterface.FundingPrepaymentReport(oCustomClass)
    End Function

    Public Function BPKBLoanReport(ByVal oCustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        Return oInterface.BPKBLoanReport(oCustomClass)
    End Function

    Public Function BPKBFotoCopyRequestReport(ByVal oCustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        Return oInterface.BPKBFotoCopyRequestReport(oCustomClass)
    End Function

    Public Function BPKBPickupReport(ByVal oCustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        Return oInterface.BPKBPickupReport(oCustomClass)
    End Function

    Public Function TAFPrepaymentReport(ByVal oCustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        Return oInterface.TAFPrepaymentReport(oCustomClass)
    End Function

    Public Function RequestCalculationFundingPrepaymentReport(ByVal oCustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        Return oInterface.RequestCalculationFundingPrepaymentReport(oCustomClass)
    End Function

    Public Function getFundingpaymentAdvance(ByVal oCustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        Return oInterface.getFundingpaymentAdvance(oCustomClass)
    End Function

    Public Function disburseFundingPaymentAdvance(ByVal oCustomClass As Parameter.DisbursePembayaranAngsuranFunding) As Parameter.DisbursePembayaranAngsuranFunding
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        Return oInterface.disburseFundingpaymentAdvance(oCustomClass)
    End Function

    Public Function BayarBatchJthTempoList(ByVal oCustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        Return oInterface.BayarBatchJthTempoList(oCustomClass)
    End Function

    Public Function disburseBatchPerjtTempo(ByVal oCustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        Return oInterface.disburseBatchPerjtTempo(oCustomClass)
    End Function
    Public Function RemoveSelectionAgreementBatch(ByVal customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        Return oInterface.RemoveSelectionAgreementBatch(customClass)
    End Function
    Public Function AppendSelectionAgreementBatch(ByVal oCustom As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        Return oInterface.AppendSelectionAgreementBatch(oCustom)
    End Function
    Public Function InstallmentScheduleTL(customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim oInterface As IPembayaranAngsuranFunding
        oInterface = ComponentFactory.CreatePembayaranAngsuranFunding()
        Return oInterface.InstallmentScheduleTL(customClass)
    End Function
End Class
