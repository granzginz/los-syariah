


#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class FundingCompanyController

    Function ListFundingCompany(ByVal FundingCompany As Parameter.FundingCompany) As Parameter.FundingCompany
        Dim oFundingCompany As IFundingCompany
        oFundingCompany = ComponentFactory.CreateFundingCompany()
        Return oFundingCompany.FundingCompanyList(FundingCompany)
    End Function

    Function AddFundingCompany(ByVal Company As Parameter.FundingCompany, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As Parameter.FundingCompany
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingCompanyAdd(Company, oClassAddress, oClassPersonal)
    End Function

    Function GetComboInputBank(ByVal strcon As String) As DataTable
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.GetComboInputBank(strcon)
    End Function

    Function ListFundingCompanyByID(ByVal Company As Parameter.FundingCompany) As Parameter.FundingCompany
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.FundingCompanyListByID(Company)
    End Function

    Function EditFundingCompany(ByVal Company As Parameter.FundingCompany, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As Parameter.FundingCompany
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingCompanyEdit(Company, oClassAddress, oClassPersonal)
    End Function

    Function DeleteFundingCompany(ByVal Company As Parameter.FundingCompany) As Parameter.FundingCompany
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingCompanyDelete(Company)
    End Function

    Function ListFundingCompanyReport(ByVal Company As Parameter.FundingCompany) As Parameter.FundingCompany
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.FundingCompanyReport(Company)
    End Function

    Function AddFundingContract(ByVal Company As Parameter.FundingContract) As Parameter.FundingContract
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingContractAdd(Company)
    End Function

    Function ListFundingContractByID(ByVal Company As Parameter.FundingContract) As Parameter.FundingContract
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.FundingContractByID(Company)
    End Function

    Function EditFundingContract(ByVal Company As Parameter.FundingContract) As Parameter.FundingContract
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingContractEdit(Company)
    End Function

    Function DeleteFundingContract(ByVal Company As Parameter.FundingContract) As Parameter.FundingContract
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingContractDelete(Company)
    End Function

    Function AddFundingContractPlafondBranch(ByVal Company As Parameter.FundingContractBranch) As Parameter.FundingContractBranch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingContractPlafondBranchAdd(Company)
    End Function

    Function ListFundingContractPlafondBranchByID(ByVal Company As Parameter.FundingContractBranch) As Parameter.FundingContractBranch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.FundingContractPlafondBranchByID(Company)
    End Function

    Function EditFundingContractPlafondBranch(ByVal Company As Parameter.FundingContractBranch) As Parameter.FundingContractBranch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingContractPlafondBranchEdit(Company)
    End Function

    Function DeleteFundingContractPlafondBranch(ByVal Company As Parameter.FundingContractBranch) As Parameter.FundingContractBranch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingContractPlafondBranchDelete(Company)
    End Function

    Function DeleteFundingContractDoc(ByVal Company As Parameter.FundingContract) As Parameter.FundingContract
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingContractDocDelete(Company)
    End Function

    Function AddFundingContractDoc(ByVal Company As Parameter.FundingContract) As Parameter.FundingContract
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingContractDocAdd(Company)
    End Function

    Function AddFundingContractBatch(ByVal Company As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingContractBatchAdd(Company)
    End Function

    Function AddFundingContractBatchIns(ByVal Company As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingContractBatchInsAdd(Company)
    End Function
    Function EditFundingContractBatch(ByVal Company As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingContractBatchEdit(Company)
    End Function
    Function UpdateFundingAgreementSelected(ByVal Company As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingUpdateAgreementSelected(Company)
    End Function
    Function UpdateFundingAgreementExecution(ByVal Company As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingUpdateAgreementExecution(Company)
    End Function
    Function FundingContractNegCovGet(ByVal CustomClass As Parameter.FundingContract) As Parameter.FundingContract
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.FundingContractNegCovGet(CustomClass)
    End Function
    Sub FundingUpdateAgreementSecondExecution(ByVal CustomClass As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingUpdateAgreementSecondExecution(CustomClass)
    End Sub
    Sub FundingDrowDownReceive(ByVal CustomClass As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingDrowDownReceive(CustomClass)
    End Sub
    Public Function GetGeneralEditView(ByVal oCustomClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.GetGeneralEditView(oCustomClass)
    End Function
    Sub PaymentOutInstallmentProcess(ByVal CustomClass As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.PaymentOutInstallmentProcess(CustomClass)
    End Sub
    Sub PrepaymentBPKBReplacing(ByVal CustomClass As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.PrepaymentBPKBReplacing(CustomClass)
    End Sub
    Sub PrepaymentPaymentOut(ByVal CustomClass As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.PrepaymentPaymentOut(CustomClass)
    End Sub
    Sub PaymentOutFees(ByVal CustomClass As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.PaymentOutFees(CustomClass)
    End Sub
    Sub AddFundingAgreementInstallment(ByVal Company As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.AddFundingAgreementInstallment(Company)
    End Sub
    Sub FundingRescheduleAgreementInstallment(ByVal Company As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingRescheduleAgreementInstallment(Company)
    End Sub
    Sub FundingUpdateContractBatchInst(ByVal Company As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingUpdateContractBatchInst(Company)
    End Sub
    Sub FundingAgreementSelect(ByVal CustomClass As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingAgreementSelect(CustomClass)
    End Sub
    Sub FundingAgreementUploadUpdateExcel(ByVal CustomClass As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingAgreementUploadUpdateExcel(CustomClass)
    End Sub
    Sub FundingAgreementSelectFromUpload(ByVal CustomClass As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingAgreementSelectFromUpload(CustomClass)
    End Sub

    Sub FundingContractNegCovUpdate(ByVal strcon As String, ByVal setField As String, ByVal WhereBy As String)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingContractNegCovUpdate(strcon, setField, WhereBy)
    End Sub

    Public Function GetPrepaymentEditView(ByVal oCustomClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.GetPrepaymentEditView(oCustomClass)
    End Function
    Public Sub DueDateChangeSave(ByVal oCustomClass As Parameter.FundingContractBatch)
        Dim DChangeSave As IFundingCompany
        DChangeSave = ComponentFactory.CreateFundingCompany()
        DChangeSave.DueDateChangeSave(oCustomClass)
    End Sub
    Sub FundingReschedulePrepaymentAgreementInstallment(ByVal Company As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingReschedulePrepaymentAgreementInstallment(Company)
    End Sub
    Sub AddFundingAgreementInstallmentDraft(ByVal Company As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.AddFundingAgreementInstallmentDraft(Company)
    End Sub

    Sub AddFundingContractBatchIns2(ByVal Company As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingContractBatchInsAdd2(Company)
    End Sub

    Function FundingDraftSoftcopy(ByVal customClass As Parameter.FundingContractBatch) As DataSet
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.FundingDraftSoftcopy(customClass)
    End Function

    Function AddFundingContractBatchInsAddKMK(ByVal Company As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingContractBatchInsAddKMK(Company)
    End Function

    Sub AddFundingAgreementInstallmentKMK(ByVal Company As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.AddFundingAgreementInstallmentKMK(Company)
    End Sub


    Sub genFAIEfektif(ByVal Company As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.genFAIEfektif(Company)
    End Sub

    Function FundingAgreementSelection(oClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.FundingAgreementSelection(oClass)
    End Function

    Function FundingAgreementList(oClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.FundingAgreementList(oClass)
    End Function

    Sub FundingContractRateEdit(ByVal oClass As Parameter.FundingContractRate)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingContractRateEdit(oClass)
    End Sub

    Sub FundingDrowDownReceiveCheckPeriod(ByVal CustomClass As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingDrowDownReceiveCheckPeriod(CustomClass)
    End Sub

    Sub FundingDrowDownReceiveCheckScheme(ByVal CustomClass As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingDrowDownReceiveCheckScheme(CustomClass)
    End Sub

    Sub FundingDrowDownReceiveDueDateRangeAdd(ByVal CustomClass As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingDrowDownReceiveDueDateRangeAdd(CustomClass)
    End Sub

    Function FundingTotalSelected(oClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.FundingTotalSelected(oClass)
    End Function

    Function FundingAgreementInstallmentView(customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany
        Return oCompany.FundingAgreementInstallmentView(customClass)
    End Function


    Public Function ListSPCReport(ByVal Customclass As Parameter.FundingCompany) As DataSet
        Dim oSPC As IFundingCompany
        oSPC = ComponentFactory.CreateFundingCompany()
        Return oSPC.ListSPCReport(Customclass)
    End Function

    Public Function FundPrintSave(ByVal ocustomClass As Parameter.FundingCompany) As String
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.FundPrintSave(ocustomClass)
    End Function

    Public Function ListPrint(ByVal Customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.ListPrint(Customclass)
    End Function
    Public Function SavePrint(ByVal Customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.SavePrint(Customclass)
    End Function
    Public Function ListReportPrepay(ByVal oCustomClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany
        Return oCompany.ListReportPrepay(oCustomClass)
    End Function
    Sub FundingAgreementJnFnc(ByVal CustomClass As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingAgreementJnFnc(CustomClass)
    End Sub

    Public Function GetFundingInquiryAgreementPledgeWillFinish(ByVal ocustomClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.GetFundingInquiryAgreementPledgeWillFinish(ocustomClass)
    End Function
    Function GetComboInputBankBeban(ByVal strcon As String) As DataTable
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.GetComboInputBankBeban(strcon)
    End Function
    Function GetComboInputBankAdministrasi(ByVal strcon As String) As DataTable
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.GetComboInputBankAdministrasi(strcon)
    End Function
    Public Function ListPrintPaymentout(ByVal Customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.ListPrintPaymentOut(Customclass)
    End Function
    Public Function SavePrintPaymentOut(ByVal Customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.SavePrintPaymentOut(Customclass)
    End Function
    Public Function ListReportPaymentOut(ByVal oCustomClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany
        Return oCompany.ListReportPaymentOut(oCustomClass)
    End Function

    Sub FundingBatchInstallmentSelectFromUpload(ByVal CustomClass As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.FundingBatchInstallmentSelectFromUpload(CustomClass)
    End Sub
    Public Function FundingBatchRate(ByVal oClass As Parameter.FundingContractRate) As Parameter.FundingContractRate
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        Return oCompany.FundingBatchRate(oClass)
    End Function

    Sub UpdateFundingAgreementSelect(ByVal CustomClass As Parameter.FundingContractBatch)
        Dim oCompany As IFundingCompany
        oCompany = ComponentFactory.CreateFundingCompany()
        oCompany.UpdateFundingAgreementSelect(CustomClass)
    End Sub

End Class
