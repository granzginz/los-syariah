
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class FundingCriteriaController
    Public Function GetFundingCriteria(ByVal ocustomClass As Parameter.FundingCriteria) As Parameter.FundingCriteria
        Dim FundingCriteriaBO As IFundingCriteria
        FundingCriteriaBO = ComponentFactory.CreateFundingCriteria()
        Return FundingCriteriaBO.GetFundingCriteria(ocustomClass)
    End Function
    Public Function GetFundingCriteriaList(ByVal ocustomClass As Parameter.FundingCriteria) As Parameter.FundingCriteria
        Dim FundingCriteriaBO As IFundingCriteria
        FundingCriteriaBO = ComponentFactory.CreateFundingCriteria()
        Return FundingCriteriaBO.GetFundingCriteriaList(ocustomClass)
    End Function
    Public Function FundingCriteriaSaveAdd(ByVal ocustomClass As Parameter.FundingCriteria) As String
        Dim FundingCriteriaBO As IFundingCriteria
        FundingCriteriaBO = ComponentFactory.CreateFundingCriteria()
        Return FundingCriteriaBO.FundingCriteriaSaveAdd(ocustomClass)
    End Function

    Public Sub FundingCriteriaSaveEdit(ByVal ocustomClass As Parameter.FundingCriteria)
        Dim FundingCriteriaBO As IFundingCriteria
        FundingCriteriaBO = ComponentFactory.CreateFundingCriteria()
        FundingCriteriaBO.FundingCriteriaSaveEdit(ocustomClass)
    End Sub

    Public Function FundingCriteriaDelete(ByVal ocustomClass As Parameter.FundingCriteria) As String
        Dim FundingCriteriaBO As IFundingCriteria
        FundingCriteriaBO = ComponentFactory.CreateFundingCriteria()
        Return FundingCriteriaBO.FundingCriteriaDelete(ocustomClass)
    End Function
    Public Sub ExecQuery(ByVal oCustomClass As Parameter.FundingCriteria)
        Dim FundingCriteriaBO As IFundingCriteria
        FundingCriteriaBO = ComponentFactory.CreateFundingCriteria()
        FundingCriteriaBO.ExecQuery(oCustomClass)
    End Sub
    Public Sub SaveReq(ByVal oCustomClass As Parameter.FundingCriteria)
        Dim FundingCriteriaBO As IFundingCriteria
        FundingCriteriaBO = ComponentFactory.CreateFundingCriteria()
        FundingCriteriaBO.SaveReq(oCustomClass)
    End Sub
    Public Function GetSQLList(ByVal ocustomClass As Parameter.FundingCriteria) As Parameter.FundingCriteria
        Dim FundingCriteriaBO As IFundingCriteria
        FundingCriteriaBO = ComponentFactory.CreateFundingCriteria()
        Return FundingCriteriaBO.GetSQLList(ocustomClass)
    End Function

End Class
