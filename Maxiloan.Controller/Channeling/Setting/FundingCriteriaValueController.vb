
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class FundingCriteriaValueController
    Public Function GetFundingCriteriaValue(ByVal ocustomClass As Parameter.FundingCriteriaValue) As Parameter.FundingCriteriaValue
        Dim FundingCriteriaValueBO As IFundingCriteriaValue
        FundingCriteriaValueBO = ComponentFactory.CreateFundingCriteriaValue()
        Return FundingCriteriaValueBO.GetFundingCriteriaValue(ocustomClass)
    End Function
    Public Function GetFundingCriteriaValueList(ByVal ocustomClass As Parameter.FundingCriteriaValue) As Parameter.FundingCriteriaValue
        Dim FundingCriteriaValueBO As IFundingCriteriaValue
        FundingCriteriaValueBO = ComponentFactory.CreateFundingCriteriaValue()
        Return FundingCriteriaValueBO.GetFundingCriteriaValueList(ocustomClass)
    End Function
    Public Function FundingCriteriaValueSaveAdd(ByVal ocustomClass As Parameter.FundingCriteriaValue) As String
        Dim FundingCriteriaValueBO As IFundingCriteriaValue
        FundingCriteriaValueBO = ComponentFactory.CreateFundingCriteriaValue()
        Return FundingCriteriaValueBO.FundingCriteriaValueSaveAdd(ocustomClass)
    End Function

    Public Sub FundingCriteriaValueSaveEdit(ByVal ocustomClass As Parameter.FundingCriteriaValue)
        Dim FundingCriteriaValueBO As IFundingCriteriaValue
        FundingCriteriaValueBO = ComponentFactory.CreateFundingCriteriaValue()
        FundingCriteriaValueBO.FundingCriteriaValueSaveEdit(ocustomClass)
    End Sub

    Public Function FundingCriteriaValueDelete(ByVal ocustomClass As Parameter.FundingCriteriaValue) As String
        Dim FundingCriteriaValueBO As IFundingCriteriaValue
        FundingCriteriaValueBO = ComponentFactory.CreateFundingCriteriaValue()
        Return FundingCriteriaValueBO.FundingCriteriaValueDelete(ocustomClass)
    End Function

End Class
