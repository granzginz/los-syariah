
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess

Public Class FundingContractCriteriaController
    Public Function GetFundingContractCriteriaListColl(ByVal ocustomClass As Parameter.FundingContractCriteria) As List(Of Parameter.FundingContractCriteria)
        Dim FundingContractCriteriaBO As IFundingContractCriteria
        FundingContractCriteriaBO = ComponentFactory.CreateFundingContractCriteria()
        Return FundingContractCriteriaBO.GetFundingContractCriteriaListColl(ocustomClass)
    End Function
    Public Function GetFundingContractCriteriaList(ByVal ocustomClass As Parameter.FundingContractCriteria) As Parameter.FundingContractCriteria
        Dim FundingContractCriteriaBO As IFundingContractCriteria
        FundingContractCriteriaBO = ComponentFactory.CreateFundingContractCriteria()
        Return FundingContractCriteriaBO.GetFundingContractCriteriaList(ocustomClass)
    End Function
    Public Function FundingContractCriteriaSaveAdd(ByVal ocustomClass As Parameter.FundingContractCriteria) As String
        Dim FundingContractCriteriaBO As IFundingContractCriteria
        FundingContractCriteriaBO = ComponentFactory.CreateFundingContractCriteria()
        Return FundingContractCriteriaBO.FundingContractCriteriaSaveAdd(ocustomClass)
    End Function

    Public Sub FundingContractCriteriaSaveEdit(ByVal ocustomClass As Parameter.FundingContractCriteria)
        Dim FundingContractCriteriaBO As IFundingContractCriteria
        FundingContractCriteriaBO = ComponentFactory.CreateFundingContractCriteria()
        FundingContractCriteriaBO.FundingContractCriteriaSaveEdit(ocustomClass)
    End Sub

    Public Function FundingContractCriteriaDelete(ByVal ocustomClass As Parameter.FundingContractCriteria) As String
        Dim FundingContractCriteriaBO As IFundingContractCriteria
        FundingContractCriteriaBO = ComponentFactory.CreateFundingContractCriteria()
        Return FundingContractCriteriaBO.FundingContractCriteriaDelete(ocustomClass)
    End Function

    Public Function GetCombo(ByVal ocustomClass As Parameter.FundingContractCriteria) As DataTable
        Dim FundingContractCriteriaBO As IFundingContractCriteria
        FundingContractCriteriaBO = ComponentFactory.CreateFundingContractCriteria()
        Return FundingContractCriteriaBO.GetCombo(ocustomClass)
    End Function
End Class
