
#Region "Import"
Imports Maxiloan.Interface
Imports Maxiloan.BusinessProcess
#End Region

Public Class ApplicationChannelingControler

    Public Sub EksekusiDelete(ByVal oCustomClass As Parameter.UploadChanneling)
        Dim BO As IApplicationChanneling
        BO = ComponentFactory.CreateApplicationChanneling()
        BO.EksekusiDelete(oCustomClass)
    End Sub

    Public Sub EksekusiSave(ByVal oCustomClass As Parameter.UploadChanneling)
        Dim BO As IApplicationChanneling
        BO = ComponentFactory.CreateApplicationChanneling()
        BO.EksekusiSave(oCustomClass)
    End Sub
End Class
