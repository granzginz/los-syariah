
Imports Maxiloan.Interface

Public Class DataUserControl
    Implements IDataUserControl


    Public Function GetCoverageType(ByVal strconnection As String) As DataTable Implements IDataUserControl.GetCoverageType
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetCoverageType(strconnection)
    End Function

    Public Function GetApplicationType(ByVal strconnection As String) As DataTable Implements IDataUserControl.GetApplicationType
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetApplicationType(strconnection)
    End Function

    Public Function getBankName(ByVal strConnection As String) As DataTable Implements IDataUserControl.getBankName
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetBankName(strConnection)
    End Function

    Public Function getBranchName(ByVal strConnection As String, ByVal strBranch As String) As DataTable Implements IDataUserControl.getBranchName
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetBranchName(strConnection, strBranch)
    End Function

    Public Function getBranchName2(ByVal strConnection As String, ByVal strBranch As String) As DataTable Implements IDataUserControl.getBranchName2
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetBranchName2(strConnection, strBranch)
    End Function


    Public Function GetSupplierName(ByVal strConnection As String) As DataTable Implements IDataUserControl.getSupplierName
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetSupplierName(strConnection)
    End Function

    Public Overloads Function GetInsuranceBranchName(ByVal strConnection As String, ByVal strBranch As String) As DataTable Implements IDataUserControl.GetInsuranceBranchName
        Dim DA As New SQLEngine.General.DataUserControl
        Return DA.GetInsuranceBranchName(strConnection, strBranch)
    End Function

    Public Overloads Function GetInsuranceBranchName(ByVal strConnection As String, ByVal strBranch As String, ByVal strLoginID As String) As DataTable Implements IDataUserControl.GetInsuranceBranchName
        Dim DA As New SQLEngine.General.DataUserControl
        Return DA.GetInsuranceBranchName(strConnection, strBranch, strLoginID)
    End Function

    Public Function getBranchFromCopyRate(ByVal strConnection As String, ByVal strBranchSource As String, ByVal strBranch As String) As DataTable Implements IDataUserControl.getBranchFromCopyRate
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.getBranchfromCopyRate(strConnection, strBranchSource, strBranch)
    End Function
    Public Function BranchCashierList(ByVal strConnection As String, ByVal strBranch As String) As DataTable Implements IDataUserControl.BranchCashierList
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.BranchCashierList(strConnection, strBranch)
    End Function
    Public Function getBranchCollectionName(ByVal strConnection As String, ByVal strBranch As String) As DataTable Implements IDataUserControl.getBranchCollectionName
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetBranchCollectionName(strConnection, strBranch)
    End Function
    Public Function getAllBranchCollectionName(ByVal strConnection As String) As DataTable Implements IDataUserControl.getAllBranchCollectionName
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetAllBranchCollectionName(strConnection)
    End Function
    Public Function GetAllCollectorTypeCL(ByVal strConnection As String) As DataTable Implements IDataUserControl.GetAllCollectorTypeCL
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetAllCollectorTypeCL(strConnection)
    End Function
    Public Function getBranchAll(ByVal strConnection As String) As DataTable Implements IDataUserControl.getBranchAll
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetBranchAll(strConnection)
    End Function

    Public Function GetBankAccount(ByVal strConnection As String, ByVal strBranchID As String, ByVal strBankType As String, _
                                    ByVal strBankPurpose As String) As DataTable Implements IDataUserControl.GetBankAccount

        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetBankAccount(strConnection, strBranchID, strBankType, strBankPurpose)
    End Function

    Public Function GetReason(ByVal strConnection As String, ByVal strReasonTypeID As String) As System.Data.DataTable Implements [Interface].IDataUserControl.GetReason
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetReason(strConnection, strReasonTypeID)
    End Function
    Public Function BranchHeadCashierList(ByVal strConnection As String, ByVal strBranch As String) As System.Data.DataTable Implements [Interface].IDataUserControl.BranchHeadCashierList
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.BranchHeadCashierList(strConnection, strBranch)
    End Function
    Public Function GetBGNumber(ByVal strConnection As String, ByVal strWhere As String) As System.Data.DataTable Implements [Interface].IDataUserControl.GetBGNumber
        Dim DataUserControlBG As New SQLEngine.General.DataUserControl
        Return DataUserControlBG.GetBGNumber(strConnection, strWhere)
    End Function
    Public Function GetHOBranch(ByVal strConnection As String) As System.Data.DataTable Implements [Interface].IDataUserControl.GetHOBranch
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetHOBranch(strConnection)
    End Function
    Public Function GetBranchHO(ByVal strConnection As String, ByVal strWhere As String) As System.Data.DataTable Implements [Interface].IDataUserControl.GetBranchHO
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetBranchHO(strConnection, strWhere)
    End Function

    Public Function GetBankAccountbranch(ByVal strConnection As String, ByVal strBranch As String) As System.Data.DataTable Implements [Interface].IDataUserControl.GetBankAccountbranch
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetBankAccountbranch(strConnection, strBranch)
    End Function
    Public Function GetCollectorSPVAct(ByVal Collector As Parameter.Collector) As Parameter.Collector Implements [Interface].IDataUserControl.GetCollectorSPVAct
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetCollectorComboForSupervisorActivity(Collector)
    End Function
    Public Function ViewBankAccount(ByVal strconnection As String) As DataTable Implements [Interface].IDataUserControl.ViewBankAccount
        Dim DAViewBankAccount As New SQLEngine.General.DataUserControl
        Return DAViewBankAccount.ViewBankAccount(strconnection)
    End Function

    Public Function GetDepartement(ByVal strConnection As String) As System.Data.DataTable Implements [Interface].IDataUserControl.GetDepartement
        Dim DA As New SQLEngine.General.DataUserControl
        Return DA.GetDepartement(strConnection)
    End Function
    Public Function GetBankMaster(ByVal strConnection As String) As System.Data.DataTable Implements [Interface].IDataUserControl.GetBankMaster
        Dim DA As New SQLEngine.General.DataUserControl
        Return DA.GetBankMaster(strConnection)
    End Function
    Public Function ViewEmployee(ByVal strConnection As String) As System.Data.DataTable Implements [Interface].IDataUserControl.ViewEmployee
        Dim DAViewEmp As New SQLEngine.General.DataUserControl
        Return DAViewEmp.ViewEmployee(strConnection)
    End Function
    Public Function ViewProduct(ByVal strConnection As String) As System.Data.DataTable Implements [Interface].IDataUserControl.ViewProduct
        Dim DAViewProd As New SQLEngine.General.DataUserControl
        Return DAViewProd.ViewProduct(strConnection)
    End Function
    Public Function ViewSupplier(ByVal strConnection As String) As System.Data.DataTable Implements [Interface].IDataUserControl.ViewSupplier
        Dim DAViewSupplier As New SQLEngine.General.DataUserControl
        Return DAViewSupplier.ViewSupplier(strConnection)
    End Function
    Public Function ViewArea(ByVal strConnection As String) As System.Data.DataTable Implements [Interface].IDataUserControl.ViewArea
        Dim DAViewArea As New SQLEngine.General.DataUserControl
        Return DAViewArea.ViewArea(strConnection)
    End Function
    Public Function GetAgingList(ByVal customclass As Parameter.Sales) As Parameter.Sales Implements [Interface].IDataUserControl.GetAgingList
        Dim DAAgingList As New SQLEngine.General.DataUserControl
        Return DAAgingList.GetAgingList(customclass)
    End Function
    Public Function GetAssetType(ByVal strConnection As String) As System.Data.DataTable Implements [Interface].IDataUserControl.GetAssetType
        Dim DAAssetList As New SQLEngine.General.DataUserControl
        Return DAAssetList.GetAssetType(strConnection)
    End Function
    Public Function GetSerialNo1Label(ByVal strConnection As String, ByVal Entities As Parameter.DocumentChangeLocation) As String Implements [Interface].IDataUserControl.GetSerialNo1Label
        Dim DAAssetList As New SQLEngine.General.DataUserControl
        Return DAAssetList.GetSerialNo1Label(strConnection, Entities)
    End Function
    Public Function GetSerialNo2Label(ByVal strConnection As String, ByVal Entities As Parameter.DocumentChangeLocation) As String Implements [Interface].IDataUserControl.GetSerialNo2Label
        Dim DAAssetList As New SQLEngine.General.DataUserControl
        Return DAAssetList.GetSerialNo2Label(strConnection, Entities)
    End Function
    Public Function GetGeneralSetting(ByVal strConnection As String, ByVal Entities As Parameter.GeneralSetting) As String Implements [Interface].IDataUserControl.GetGeneralSetting
        Dim DA As New SQLEngine.General.DataUserControl
        Return DA.GetGeneralSetting(strConnection, Entities)
    End Function
    Public Function GetMasterNotary(ByVal strConnection As String) As System.Data.DataTable Implements [Interface].IDataUserControl.GetMasterNotary
        Dim DA As New SQLEngine.General.DataUserControl
        Return DA.GetMasterNotary(strConnection)
    End Function
    Public Function GetProductBranch(ByVal strConnection As String, ByVal strBranch As String) As DataTable Implements IDataUserControl.GetProductBranch
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetProductBranch(strConnection, strBranch)
    End Function
    Public Function getAPBank(ByVal strConnection As String) As System.Data.DataTable Implements [Interface].IDataUserControl.getAPBank
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetAPBank(strConnection)
    End Function
    Public Function popCollector(ByVal strConnection As String, ByVal strCGID As String, ByVal strCollectorType As String) As System.Data.DataTable Implements [Interface].IDataUserControl.popCollector
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.popCollector(strConnection, strCGID, strCollectorType)
    End Function

    Public Function ViewCustomerDetailShort(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer Implements [Interface].IDataUserControl.viewCustomerDetailShort
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.viewCustomerDetailShort(oCustomClass)
    End Function

    Public Function getKotaKabupaten(ByVal oCustomClass As Parameter.Dati) As Parameter.Dati Implements [Interface].IDataUserControl.getKotaKabupaten
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.getKotaKabupaten(oCustomClass)
    End Function

    Public Function getAgreementGuarantor(ByVal oCustomClass As Parameter.AgreementGuarantor) As Parameter.AgreementGuarantor Implements [Interface].IDataUserControl.getAgreementGuarantor
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.getAgreementGuarantor(oCustomClass)
    End Function


    Public Function GetCollectorReportLapHasilKunColl(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS Implements [Interface].IDataUserControl.GetCollectorReportLapHasilKunColl
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetCollectorReportLapHasilKunColl(oClass)
    End Function

    Public Function GetCollectorReportLapKegDeskColl(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS Implements [Interface].IDataUserControl.GetCollectorReportLapKegDeskColl
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetCollectorReportLapKegDeskColl(oClass)
    End Function

    Public Function GetReportKasir(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS Implements [Interface].IDataUserControl.GetReportKasir
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetReportKasir(oClass)
    End Function

    Public Function GetReportNamaKonsumen(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS Implements [Interface].IDataUserControl.GetReportNamaKonsumen
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetReportNamaKonsumen(oClass)
    End Function
    Public Function GetInsuranceCompanyBranchALL(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS Implements [Interface].IDataUserControl.GetInsuranceCompanyBranchALL
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetInsuranceCompanyBranchALL(oClass)
    End Function
    Public Function GetReportTransactionType(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS Implements [Interface].IDataUserControl.GetReportTransactionType
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetReportTransactionType(oClass)
    End Function
    Public Function GetReportNoKontrak(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS Implements [Interface].IDataUserControl.GetReportNoKontrak
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetReportNoKontrak(oClass)
    End Function

    Public Function GetReportNoAplikasi(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS Implements [Interface].IDataUserControl.GetReportNoAplikasi
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetReportNoAplikasi(oClass)
    End Function

    Public Function GetReportComboContract(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS Implements [Interface].IDataUserControl.GetReportComboContract
        Dim DataUserControlDA As New SQLEngine.General.DataUserControl
        Return DataUserControlDA.GetReportComboContract(oClass)
    End Function


    'GetReportComboContract

End Class
