﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class MailTransaction
    Implements IMailTransaction

#Region "PrintMailTransaction"
    Public Function PrintMailTransaction(ByVal customClass As Parameter.MailTransaction) As Parameter.MailTransaction Implements IMailTransaction.PrintMailTransaction
        Dim DA As New SQLEngine.General.MailTransaction
        DA.PrintMailTransaction(customClass)
    End Function
#End Region
End Class
