﻿Imports Maxiloan.Interface
Public Class GeneralPaging
    Implements IGeneralPaging


    Public Function GetReportWithTwoParameter(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging Implements [Interface].IGeneralPaging.GetReportWithTwoParameter
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.GetReportWithTwoParameter(customClass)

    End Function

    Public Function GetRecordWithoutParameter(ByVal strConnection As String) As DataTable Implements [Interface].IGeneralPaging.GetRecordWithoutParameter
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.GetRecordWithoutParameter(strConnection)
    End Function

    Public Function GetGeneralPaging(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging Implements [Interface].IGeneralPaging.GetGeneralPaging
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.GetGeneralPaging(customClass)
    End Function


    Public Function GetBranchCombo(ByVal oClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList Implements [Interface].IGeneralPaging.GetBranchCombo
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.GetBranchCombo(oClass)
    End Function


    Public Function GetInsuranceBranchCombo(ByVal oClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList Implements [Interface].IGeneralPaging.GetInsuranceBranchCombo
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.GetInsuranceBranchCombo(oClass)
    End Function

    Public Function GetReportWithParameterWhereCond(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging Implements [Interface].IGeneralPaging.GetReportWithParameterWhereCond
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.GetReportWithParameterWhereCond(customClass)
    End Function

    Public Function GetGeneralSP(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging Implements [Interface].IGeneralPaging.GetGeneralSP

        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.GetGeneralSP(customClass)

    End Function

    Public Function GetReportWithParameterWhereAndSort(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging Implements [Interface].IGeneralPaging.GetReportWithParameterWhereAndSort
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.GetReportWithParameterWhereAndSort(customClass)
    End Function
    Public Function GetReportWithThreeParameterWhereCond(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging Implements [Interface].IGeneralPaging.GetReportWithThreeParameterWhereCond
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.GetReportWithThreeParameterWhereCond(customClass)
    End Function

    Public Function GetReportWithParamApplicationID(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging Implements [Interface].IGeneralPaging.GetReportWithParamApplicationID
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.GetReportWithParamApplicationID(customClass)
    End Function
    Public Function DeleteGeneral(ByVal customclass As Parameter.GeneralPaging) As Boolean Implements [Interface].IGeneralPaging.DeleteGeneral
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.DeleteGeneral(customclass)
    End Function

    Public Function ARMutationReport(ByVal customclass As Parameter.GeneralPaging) As System.Data.DataSet Implements [Interface].IGeneralPaging.ARMutationReport
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.ARMutationReport(customclass)
    End Function

    Public Function APMutationReport(ByVal customclass As Parameter.GeneralPaging) As System.Data.DataSet Implements [Interface].IGeneralPaging.APMutationReport
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.APMutationReport(customclass)
    End Function

    Public Function APToInsReport(ByVal customclass As Parameter.GeneralPaging) As System.Data.DataSet Implements [Interface].IGeneralPaging.APToInsReport
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.APToInsReport(customclass)
    End Function

    Public Function SuspendReport(ByVal customclass As Parameter.GeneralPaging) As System.Data.DataSet Implements [Interface].IGeneralPaging.SuspendReport
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.SuspendReport(customclass)
    End Function

    Public Function PrepaidReport(ByVal customclass As Parameter.GeneralPaging) As System.Data.DataSet Implements [Interface].IGeneralPaging.PrepaidReport
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.PrepaidReport(customclass)
    End Function

    Public Function BIQualityReport(ByVal customclass As Parameter.GeneralPaging) As System.Data.DataSet Implements [Interface].IGeneralPaging.BIQualityReport
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.BIQualityReport(customclass)
    End Function
    Public Function PrepaymentTerminationReport(ByVal customclass As Parameter.GeneralPaging) As Parameter.GeneralPaging Implements [Interface].IGeneralPaging.PrepaymentTerminationReport
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.PrepaymentTerminationReport(customclass)
    End Function
    Public Function AssetRepossessionOrderTerminationReport(ByVal customclass As Parameter.GeneralPaging) As Parameter.GeneralPaging Implements [Interface].IGeneralPaging.AssetRepossessionOrderTerminationReport
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.AssetRepossessionOrderTerminationReport(customclass)
    End Function

    Public Function GetReportWithBranchAndstrDate(ByVal customclass As Parameter.GeneralPaging) As System.Data.DataSet Implements [Interface].IGeneralPaging.GetReportWithBranchAndstrDate
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.GetReportWithBranchAndstrDate(customclass)
    End Function

    Public Function GetReportWithBranchAndTwoPeriod(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging Implements [Interface].IGeneralPaging.GetReportWithBranchAndTwoPeriod
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.GetReportWithBranchAndTwoPeriod(customClass)
    End Function
    Public Function GetGeneralPagingWithTwoWhereCondition(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging Implements [Interface].IGeneralPaging.GetGeneralPagingWithTwoWhereCondition
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.GetGeneralPagingWithTwoWhereCondition(customClass)
    End Function

    Public Function GetReportWithBranch_strDate_Where(ByVal customclass As Parameter.GeneralPaging) As System.Data.DataSet Implements [Interface].IGeneralPaging.GetReportWithBranch_strDate_Where
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.GetReportWithBranch_strDate_Where(customclass)
    End Function

    Public Function GetReportWithFiveParameterWhereCond(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging Implements [Interface].IGeneralPaging.GetReportWithFiveParameterWhereCond
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.GetReportWithFiveParameterWhereCond(customClass)
    End Function

    Public Function GetRecordWithoutParameterAll(ByVal oCustom As Parameter.GeneralPaging) As System.Data.DataTable Implements [Interface].IGeneralPaging.GetRecordWithoutParameterAll
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.GetRecordWithoutParameterAll(oCustom)
    End Function

    Public Function GetDailyTransReport(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging Implements [Interface].IGeneralPaging.GetDailyTransReport
        Dim DA As New SQLEngine.General.GeneralPaging
        Return DA.GetDailyTransReport(customClass)
    End Function
End Class
