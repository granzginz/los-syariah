﻿Imports Maxiloan.Interface

Public Class Authorize
    Implements IAuthorize

    Public Function CheckForm(ByVal oCustomClass As Parameter.Common) As Boolean Implements IAuthorize.CheckForm
        Dim DACheckForm As New SQLEngine.General.Authorize
        Return DACheckForm.CheckForm(oCustomClass)
    End Function

    Public Function CheckFeature(ByVal oCustomClass As Parameter.Common) As Boolean Implements IAuthorize.CheckFeature
        Dim DACheckForm As New SQLEngine.General.Authorize
        Return DACheckForm.CheckFeature(oCustomClass)
    End Function
End Class
