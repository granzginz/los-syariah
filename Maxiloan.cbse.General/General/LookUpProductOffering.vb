#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class LookUpProductOffering
    Implements ILookUpProductOffering

    Public Function GetProductBranch(ByVal BranchId As String, ByVal strconnection As String) As DataTable Implements ILookUpProductOffering.GetProductBranch
        Dim LookUpProductOfferingDA As New SQLEngine.General.LookUpProductOffering
        Return LookUpProductOfferingDA.GetProductBranch(BranchId, strconnection)
    End Function

    Public Function GetListData(ByVal customClass As Parameter.LookUpProductOffering) As Parameter.LookUpProductOffering Implements ILookUpProductOffering.GetListData
        Dim LookUpProductOfferingDA As New SQLEngine.General.LookUpProductOffering
        Return LookUpProductOfferingDA.GetListData(customClass)        
    End Function
End Class
