#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region
Public Class LookUpZipCode
    Implements ILookUpZipCode
    Public Function GetCitySearch(ByVal strConnection As String) As DataTable Implements ILookUpZipCode.GetCitySearch
        Dim LookUpZipCodeDA As New SQLEngine.General.LookUpZipCode
        Return LookUpZipCodeDA.GetCitySearch(strConnection)
    End Function
    Public Function GetListData(ByVal strConnection As String, ByVal cmdWhere As String, ByVal currentPage As Integer, ByVal pagesize As Integer, ByVal SortBy As String) As Parameter.LookUpZipCode Implements ILookUpZipCode.GetListData
        Dim LookUpZipCodeDA As New SQLEngine.General.LookUpZipCode
        Return LookUpZipCodeDA.GetListData(strConnection, cmdWhere, currentPage, pagesize, SortBy)
    End Function
End Class
