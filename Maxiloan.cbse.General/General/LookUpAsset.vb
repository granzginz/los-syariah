

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class LookUpAsset
    Implements ILookUpAsset
    
    Public Function GetListData(ByVal customClass As Parameter.LookUpAsset) As Parameter.LookUpAsset Implements ILookUpAsset.GetListData        
            Dim LookUpAssetDA As New SQLEngine.General.LookUpAsset
            Return LookUpAssetDA.GetListData(customClass)        
    End Function

    Public Function GetListDataMulti(ByVal customClass As Parameter.LookUpAsset) As Parameter.LookUpAsset Implements ILookUpAsset.GetListDataMulti        
            Dim LookUpAssetDA As New SQLEngine.General.LookUpAsset
            Return LookUpAssetDA.GetListDataMulti(customClass)       
    End Function

    Public Function GetListDataMultiConfirm(ByVal customClass As Parameter.LookUpAsset) As Parameter.LookUpAsset Implements ILookUpAsset.GetListDataMulti_Confirm        
            Dim LookUpAssetDA As New SQLEngine.General.LookUpAsset
            Return LookUpAssetDA.GetListDataMultiConfirm(customClass)       
    End Function

End Class
