Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

Public Class LookUpBankBranch
    Implements ILookUpBankBranch

    Public Function GetCitySearch(ByVal strConnection As String) As System.Data.DataTable Implements [Interface].ILookUpBankBranch.GetCitySearch
        Dim LookUp As New SQLEngine.General.LookUpBankBranch
        Return LookUp.GetCitySearch(strConnection)
    End Function

    Public Function GetListData(ByVal strConnection As String, ByVal cmdWhere As String, ByVal currentPage As Integer, ByVal pagesize As Integer, ByVal SortBy As String) As Parameter.LookUpBankBranch Implements [Interface].ILookUpBankBranch.GetListData
        Dim LookUp As New SQLEngine.General.LookUpBankBranch
        Return LookUp.GetListData(strConnection, cmdWhere, currentPage, pagesize, SortBy)
    End Function
End Class
