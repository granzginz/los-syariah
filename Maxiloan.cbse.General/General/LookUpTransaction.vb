

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class LookUpTransaction
    Implements ILookUpTransaction
    Public Function GetListData(ByVal strConnection As String, ByVal cmdWhere As String, ByVal currentPage As Integer, ByVal pagesize As Integer, ByVal SortBy As String) As Parameter.LookUpTransaction Implements ILookUpTransaction.GetListData
        Try
            Dim LookUpTransactionDA As New SQLEngine.General.LookUpTransaction
            Return LookUpTransactionDA.GetListData(strConnection, cmdWhere, currentPage, pagesize, SortBy)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetProcessID(ByVal strConnection As String, ByVal TransactionID As String) As String Implements ILookUpTransaction.GetProcessID
        Try
            Dim LookUpTransactionDA As New SQLEngine.General.LookUpTransaction
            Return LookUpTransactionDA.GetProcessID(strConnection, TransactionID)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
End Class
