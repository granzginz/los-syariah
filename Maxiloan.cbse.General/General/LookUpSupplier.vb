

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class LookUpSupplier
    Implements ILookUpSupplier
    Public Function GetListData(ByVal customClass As Parameter.LookUpSupplier) As Parameter.LookUpSupplier Implements ILookUpSupplier.GetListData
        Try
            Dim LookUpSupplierDA As New SQLEngine.General.LookUpSupplier
            Return LookUpSupplierDA.GetListData(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetListDataMulti(ByVal customClass As Parameter.LookUpSupplier) As Parameter.LookUpSupplier Implements ILookUpSupplier.GetListDataMulti
        Try
            Dim LookUpSupplierDA As New SQLEngine.General.LookUpSupplier
            Return LookUpSupplierDA.GetListDataMulti(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetListDataMultiConfirm(ByVal customClass As Parameter.LookUpSupplier) As Parameter.LookUpSupplier Implements ILookUpSupplier.GetListDataMulti_Confirm
        Try
            Dim LookUpSupplierDA As New SQLEngine.General.LookUpSupplier
            Return LookUpSupplierDA.GetListDataMultiConfirm(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetListDataSupplierBranch(ByVal customClass As Parameter.LookUpSupplier) As Parameter.LookUpSupplier Implements ILookUpSupplier.GetListDataSupplierBranch
        Try
            Dim LookUpSupplierDA As New SQLEngine.General.LookUpSupplier
            Return LookUpSupplierDA.GetListDataSupplierBranch(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
End Class
