﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
Imports Maxiloan.SQLEngine.General
#End Region

Public Class Login
    Implements ILogin

    Public Function CheckBranch(ByVal oCustomClass As Parameter.Login) As Parameter.Login Implements ILogin.CheckBranch
        Dim DACheckBranchClosed As New SQLEngine.General.Login
        Return DACheckBranchClosed.CheckBranch(oCustomClass)
    End Function

    Public Function CheckMultiBranch(ByVal oCustomClass As Parameter.Login) As Parameter.Login Implements ILogin.CheckMultiBranch
        Dim DACheckBranchClosed As New SQLEngine.General.Login
        Return DACheckBranchClosed.CheckMultiBranch(oCustomClass)
    End Function

    Public Function SetSession(ByVal oCustomClass As Parameter.Login) As Parameter.Login Implements ILogin.SetSession
        Dim DASetSession As New SQLEngine.General.Login
        Return DASetSession.SetSession(oCustomClass)
    End Function

    Public Function GetLoginData() As Parameter.Login Implements ILogin.GetLoginData
        Dim DACheckIsEOD As New SQLEngine.General.Login
        Return DACheckIsEOD.GetLoginData()
    End Function

    Public Function GetBusinessDate(ByVal StrConnection As String, Optional ByVal strBranchId As String = "00") As Date Implements ILogin.GetBusinessDate
        Dim DACheckIsEOD As New SQLEngine.General.Login
        Return DACheckIsEOD.GetBusinessDate(StrConnection, strBranchId)
    End Function

    Public Function GetUser(ByVal oCustomClass As Parameter.Login) As Parameter.Login Implements ILogin.GetUser
        Dim DAGetUser As New SQLEngine.General.Login
        Return DAGetUser.GetUser(oCustomClass)
    End Function

    Public Function GetEmployee(ByVal oCustomClass As Parameter.Login) As String Implements ILogin.GetEmployee
        Dim DAGetUser As New SQLEngine.General.Login
        Return DAGetUser.GetEmployee(oCustomClass)
    End Function

    Public Function ListTreeMenu(ByVal oCustomClass As Parameter.Login) As Boolean Implements ILogin.ListTreeMenu
        Dim DAListTreeMenu As New SQLEngine.General.Login
        Return DAListTreeMenu.ListTreeMenu(oCustomClass)
    End Function

    Public Function UpdateTreeMenu(ByVal oCustomClass As Parameter.Login) As String Implements ILogin.UpdateTreeMenu
        Dim DAUpdateTreeMenu As New SQLEngine.General.Login
        Return DAUpdateTreeMenu.UpdateTreeMenu(oCustomClass)
    End Function

    Public Function WriteFileXML(ByVal oCustomClass As Parameter.Login) As DataTable Implements ILogin.WriteFileXML
        Dim DAWriteFileXML As New SQLEngine.General.Login
        Return DAWriteFileXML.WriteFileXML(oCustomClass)
    End Function

    Public Function WriteXMLForUser(ByVal ocustomclass As Parameter.Login) As DataTable Implements ILogin.WriteXMLForUser
        Dim DAWriteXMLForUser As New SQLEngine.General.Login
        Return DAWriteXMLForUser.WriteXMLForUser(ocustomclass)
    End Function

    Public Function GetCompanyAddress(ByVal strConnection As String, ByVal StrCompanyID As String) As String Implements [Interface].ILogin.GetCompanyAddress
        Dim DAGetCompanyAddress As New SQLEngine.General.Login
        Return DAGetCompanyAddress.GetCompanyAddress(strConnection, StrCompanyID)
    End Function

    Public Function CountWrongPwd(ByVal oCustomClass As Parameter.Login) As Int16 Implements [Interface].ILogin.CountWrongPwd
        Dim DAGetCompanyAddress As New SQLEngine.General.Login
        Return DAGetCompanyAddress.CountWrongPwd(oCustomClass)
    End Function

    Function IsPasswordExpired(ByVal oCustomClass As Parameter.Login) As Boolean Implements [Interface].ILogin.IsPasswordExpired
        Dim DALogin As New SQLEngine.General.Login
        Return DALogin.IsPasswordExpired(oCustomClass)
    End Function

    Public Function PasswordSetting() As Parameter.Login Implements [Interface].ILogin.PasswordSetting
        Dim DALogin As New SQLEngine.General.Login
        Return DALogin.PasswordSetting()
    End Function

    Public Function ListPasswordHistory(ByVal oCustomClass As Parameter.Login) As Parameter.Login Implements [Interface].ILogin.ListPasswordHistory
        Dim DALogin As New SQLEngine.General.Login
        Return DALogin.ListPasswordHistory(oCustomClass)
    End Function

    Public Sub setloghistory(oCustomClass As Parameter.Login) Implements ILogin.setloghistory
        Dim DALogin As New SQLEngine.General.Login
        DALogin.setloghistory(oCustomClass)
    End Sub
End Class

