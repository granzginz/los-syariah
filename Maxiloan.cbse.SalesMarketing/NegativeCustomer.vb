

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class NegativeCustomer : Inherits ComponentBase
    Implements INegativeCustomer

    Public Function ListNegativeCustomer(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer Implements [Interface].INegativeCustomer.ListNegativeCustomer
        Dim DANegative As New SQLEngine.SalesMarketing.NegativeCustomer
        Return DANegative.ListNegativeCustomer(customclass)
    End Function

    Public Function NegativeCustomerAdd(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer Implements [Interface].INegativeCustomer.NegativeCustomerAdd
        Dim DANegative As New SQLEngine.SalesMarketing.NegativeCustomer
        Return DANegative.NegativeCustomerAdd(customclass)
    End Function

    Public Function NegativeCustomerView(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer Implements [Interface].INegativeCustomer.NegativeCustomerView
        Dim DANegative As New SQLEngine.SalesMarketing.NegativeCustomer
        Return DANegative.NegativeCustomerView(customclass)
    End Function

    Public Function ListDataSource(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer Implements [Interface].INegativeCustomer.ListDataSource
        Dim DANegative As New SQLEngine.SalesMarketing.NegativeCustomer
        Return DANegative.ListDataSource(customclass)
    End Function

    Public Function ListIDType(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer Implements [Interface].INegativeCustomer.ListIDType
        Dim DANegative As New SQLEngine.SalesMarketing.NegativeCustomer
        Return DANegative.ListIDType(customclass)
    End Function

    Public Function NegativeCustomerEdit(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer Implements [Interface].INegativeCustomer.NegativeCustomerEdit
        Dim DANegative As New SQLEngine.SalesMarketing.NegativeCustomer
        Return DANegative.NegativeCustomerEdit(customclass)
    End Function

    Public Function NegativeCustomerRpt(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer Implements [Interface].INegativeCustomer.NegativeCustomerRpt
        Dim DANegative As New SQLEngine.SalesMarketing.NegativeCustomer
        Return DANegative.NegativeCustomerRpt(customclass)
    End Function

    Public Function GetApprovalNumberForHistory(ByVal data As Parameter.NegativeCustomer) As String Implements [Interface].INegativeCustomer.GetApprovalNumberForHistory
        Dim DANegative As New SQLEngine.SalesMarketing.NegativeCustomer
        Return DANegative.GetApprovalNumberForHistory(data)
    End Function

    Public Function DoUploadNegativeCsv(cnn As String, BusinessDate As DateTime, BranchId As String, params As DataTable) As String Implements [Interface].INegativeCustomer.DoUploadNegativeCsv
        Dim DANegative As New SQLEngine.SalesMarketing.NegativeCustomer
        Return DANegative.DoUploadNegativeCsv(cnn, BusinessDate, BranchId, params)
    End Function
End Class
