
Public Class AOSupervisorSales
    Implements [Interface].IAOSupervisorSales


    Public Function AOSupervisorBudgetListing(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales Implements [Interface].IAOSupervisorSales.AOSupervisorBudgetListing
        Dim DAAOBudgetListing As New SQLEngine.SalesMarketing.AOSupervisorSales
        Return DAAOBudgetListing.AOSupervisorBudgetListing(customclass)
    End Function

    Public Function AOSupervisorListing(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales Implements [Interface].IAOSupervisorSales.AOSupervisorListing
        Dim DAAOListing As New SQLEngine.SalesMarketing.AOSupervisorSales
        Return DAAOListing.AOSupervisorListing(customclass)
    End Function

    Public Function AOSupervisorBudgetPrint(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales Implements [Interface].IAOSupervisorSales.AOSupervisorBudgetPrint
        Dim DAAOBudgetPrint As New SQLEngine.SalesMarketing.AOSupervisorSales
        Return DAAOBudgetPrint.AOSupervisorBudgetPrint(customclass)
    End Function

    Public Function AOSupervisorBudgetView(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales Implements [Interface].IAOSupervisorSales.AOSupervisorBudgetView
        Dim DAAOBudgetView As New SQLEngine.SalesMarketing.AOSupervisorSales
        Return DAAOBudgetView.AOSupervisorBudgetView(customclass)
    End Function

    Public Function AOSupervisorForecastListing(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales Implements [Interface].IAOSupervisorSales.AOSupervisorForecastListing
        Dim DAAOListing As New SQLEngine.SalesMarketing.AOSupervisorSales
        Return DAAOListing.AOSupervisorForecastListing(customclass)
    End Function

    Public Function AOSupervisorForecastPrint(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales Implements [Interface].IAOSupervisorSales.AOSupervisorForecastPrint
        Dim DAAOForecastPrint As New SQLEngine.SalesMarketing.AOSupervisorSales
        Return DAAOForecastPrint.AOSupervisorForecastPrint(customclass)
    End Function

    Public Function AOSupervisorForecastView(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales Implements [Interface].IAOSupervisorSales.AOSupervisorForecastView
        Dim DAAOForecastView As New SQLEngine.SalesMarketing.AOSupervisorSales
        Return DAAOForecastView.AOSupervisorForecastView(customclass)
    End Function
End Class
