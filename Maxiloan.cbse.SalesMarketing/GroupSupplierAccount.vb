
#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class GroupSupplierAccount : Inherits ComponentBase
    Implements IGroupSupplierAccount


    Dim oSQLE As New SQLEngine.SalesMarketing.GroupSupplierAccount

    Public Function GroupSupplierAccountDelete(ByVal oCustomClass As Parameter.GroupSupplierAccount) As String Implements [Interface].IGroupSupplierAccount.GroupSupplierAccountDelete
        Return oSQLE.GroupSupplierAccountDelete(oCustomClass)
    End Function

    Public Function GroupSupplierAccountSaveAdd(ByVal oCustomClass As Parameter.GroupSupplierAccount) As String Implements [Interface].IGroupSupplierAccount.GroupSupplierAccountSaveAdd
        Return oSQLE.GroupSupplierAccountSaveAdd(oCustomClass)
    End Function

    Public Sub GroupSupplierAccountSaveEdit(ByVal oCustomClass As Parameter.GroupSupplierAccount) Implements [Interface].IGroupSupplierAccount.GroupSupplierAccountSaveEdit
        oSQLE.GroupSupplierAccountSaveEdit(oCustomClass)
    End Sub

    Public Function GetGroupSupplierAccount(ByVal oCustomClass As Parameter.GroupSupplierAccount) As Parameter.GroupSupplierAccount Implements [Interface].IGroupSupplierAccount.GetGroupSupplierAccount
        Return oSQLE.GetGroupSupplierAccount(oCustomClass)
    End Function

    Public Function GetGroupSupplierAccountList(ByVal oCustomClass As Parameter.GroupSupplierAccount) As Parameter.GroupSupplierAccount Implements [Interface].IGroupSupplierAccount.GetGroupSupplierAccountList
        Return oSQLE.GetGroupSupplierAccountList(oCustomClass)
    End Function

    Public Function GetGroupSupplierAccountReport(ByVal oCustomClass As Parameter.GroupSupplierAccount) As Parameter.GroupSupplierAccount Implements [Interface].IGroupSupplierAccount.GetGroupSupplierAccountReport
        Return oSQLE.GetGroupSupplierAccountReport(oCustomClass)
    End Function

End Class