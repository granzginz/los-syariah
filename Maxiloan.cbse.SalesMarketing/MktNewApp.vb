
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region
Public Class MktNewApp : Inherits ComponentBase
    Implements IMktNewApp


    Public Function MktNewAppPaging(ByVal oCustomClass As Parameter.MktNewApp) As Parameter.MktNewApp Implements [Interface].IMktNewApp.MktNewAppPaging
        Dim DAMktNewApp As New SQLEngine.SalesMarketing.MktNewApp
        Return DAMktNewApp.MktNewAppPaging(oCustomClass)
    End Function

    Public Function MktNewAppView(ByVal oCustomClass As Parameter.MktNewApp) As Parameter.MktNewApp Implements [Interface].IMktNewApp.MktNewAppView
        Dim damktnewapp As New SQLEngine.SalesMarketing.MktNewApp
        Return damktnewapp.MktNewAppView(oCustomClass)
    End Function

    Public Function GetPersonalHomeStatus(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp Implements [Interface].IMktNewApp.GetPersonalHomeStatus
        Dim damktnewapp As New SQLEngine.SalesMarketing.MktNewApp
        Return damktnewapp.GetPersonalHomeStatus(ocustomclass)
    End Function

    Public Function GetPersonalIdType(ByVal oCustomClass As Parameter.MktNewApp) As Parameter.MktNewApp Implements [Interface].IMktNewApp.GetPersonalIdType
        Dim damktnewapp As New SQLEngine.SalesMarketing.MktNewApp
        Return damktnewapp.GetPersonalIdType(oCustomClass)
    End Function

    Public Function GetProfession(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp Implements [Interface].IMktNewApp.GetProfession
        Dim damktnewapp As New SQLEngine.SalesMarketing.MktNewApp
        Return damktnewapp.GetProfession(ocustomclass)
    End Function


    Public Sub Generate_XML(ByVal pStrFile As String, ByVal oCustomClass As Parameter.MktNewApp) Implements [Interface].IMktNewApp.Generate_XML
        Dim lObjDataSet As DataSet
        Dim lObjDataTable As DataTable
        Dim lObjDataRow As DataRow

        lObjDataTable = New DataTable("Cust")

        lObjDataTable.Columns.Add("BranchId", System.Type.GetType("System.String"))
        'lObjDataTable.Columns.Add("ProspectAppID", System.Type.GetType("System.String"))
        'lObjDataTable.Columns.Add("ApplicationID", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("CustomerType", System.Type.GetType("System.String"))

        lObjDataTable.Columns.Add("ProspectAppDate", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("Name", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("IDType", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("IDNumber", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("Gender", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("BirthPlace", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("BirthDate", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("HomeStatus", System.Type.GetType("System.String"))

        lObjDataTable.Columns.Add("Address", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("RT", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("RW", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("Kelurahan", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("Kecamatan", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("City", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("ZipCode", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("AreaPhone1", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("Phone1", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("AreaPhone2", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("Phone2", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("AreaFak", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("Fak", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("MobilePhone", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("Email", System.Type.GetType("System.String"))


        lObjDataTable.Columns.Add("ProfessionID", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("EmploymentSinceYear", System.Type.GetType("System.String"))

        lObjDataTable.Columns.Add("ProductID", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("ProductOfferingID", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("AssetCode", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("OTRPrice", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("DPAmount", System.Type.GetType("System.String"))
        'lObjDataTable.Columns.Add("NTF", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("Tenor", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("InstallmentAmount", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("FlatRate", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("FirstInstallment", System.Type.GetType("System.String"))


        'lObjDataTable.Columns.Add("ResaleValue", System.Type.GetType("System.String"))
        '        lObjDataTable.Columns.Add("ResikoAngsuran", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("DueDate", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("MaskAssID", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("InsuranceComBranchID", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("ExistingPolicyNo", System.Type.GetType("System.String"))

        lObjDataTable.Columns.Add("IndustryTypeID", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("CompanyStatus", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("EstablishedSinceYear", System.Type.GetType("System.String"))
        'lObjDataTable.Columns.Add("CreditScore", System.Type.GetType("System.String"))
        ' lObjDataTable.Columns.Add("CreditScoringResult", System.Type.GetType("System.String"))
        'lObjDataTable.Columns.Add("IsHold", System.Type.GetType("System.String"))



        lObjDataRow = lObjDataTable.NewRow
        With oCustomClass
            lObjDataRow("BranchId") = .BranchId
            'lObjDataRow("ProspectAppID") = .ProspectAppId
            'lObjDataRow("ApplicationID") = .ApplicationId
            lObjDataRow("CustomerType") = .CustomerType

            lObjDataRow("ProspectAppDate") = .ProspectAppDate
            lObjDataRow("Name") = .Name
            lObjDataRow("IDType") = .IdType
            lObjDataRow("IDNumber") = .IdNumber
            lObjDataRow("Gender") = .Gender
            lObjDataRow("BirthPlace") = .BirthPlace
            lObjDataRow("BirthDate") = .BirthDate
            lObjDataRow("HomeStatus") = .HomeStatus

            lObjDataRow("Address") = .Address
            lObjDataRow("RT") = .RT
            lObjDataRow("RW") = .RW
            lObjDataRow("Kelurahan") = .Kelurahan
            lObjDataRow("Kecamatan") = .Kecamatan
            lObjDataRow("City") = .City
            lObjDataRow("ZipCode") = .ZIPCode
            lObjDataRow("AreaPhone1") = .AreaPhone1
            lObjDataRow("Phone1") = .Phone1
            lObjDataRow("AreaPhone2") = .AreaPhone2
            lObjDataRow("Phone2") = .Phone2
            lObjDataRow("AreaFak") = .AreaFak
            lObjDataRow("Fak") = .FakNo

            lObjDataRow("MobilePhone") = .mobilephone
            lObjDataRow("Email") = .Email

            lObjDataRow("ProfessionID") = .Profession
            lObjDataRow("EmploymentSinceYear") = .LengthOfEmployment

            lObjDataRow("ProductID") = .Product
            lObjDataRow("ProductOfferingID") = .ProductOfferingId
            lObjDataRow("AssetCode") = .AssetDescription
            lObjDataRow("OTRPrice") = .OTRPrice
            lObjDataRow("DPAmount") = .DownPayment
            'lObjDataRow("NTF") =
            lObjDataRow("Tenor") = .Tenor
            lObjDataRow("InstallmentAmount") = .InstallAmount
            lObjDataRow("FlatRate") = .FlateRate
            lObjDataRow("FirstInstallment") = .FirstIntallment

            'lobjdatarow ("ResaleValue") =
            '        lobjdatarow ("ResikoAngsuran") =
            lObjDataRow("DueDate") = .Estimateduedate
            lObjDataRow("MaskAssID") = .InsuranceCo
            lObjDataRow("InsuranceComBranchID") = .InsuranceCoBranch
            lObjDataRow("ExistingPolicyNo") = .ExistingPolicyNo

            lObjDataRow("IndustryTypeID") = .IndustryType
            lObjDataRow("CompanyStatus") = .BusinessPlaceStatus
            lObjDataRow("EstablishedSinceYear") = .YearInBusiness
            'lobjdatarow ("CreditScore") =
            ' lobjdatarow ("CreditScoringResult") =
            'lobjdatarow ("IsHold") =
        End With


        lObjDataTable.Rows.Add(lObjDataRow)
        lObjDataSet = New DataSet("Cust")
        lObjDataSet.Tables.Add(lObjDataTable)

        lObjDataRow = Nothing
        lObjDataTable = Nothing

        Try
            lObjDataSet.WriteXml(pStrFile)
            'pStrSuccess = True
        Catch
            'pStrSuccess = False
        End Try
        lObjDataSet = Nothing



    End Sub

    Public Function BindCustomer1_002(ByVal oCustomer As Parameter.MktNewApp) As Parameter.MktNewApp Implements [Interface].IMktNewApp.BindCustomer1_002
        Dim damktnewapp As New SQLEngine.SalesMarketing.MktNewApp
        Return damktnewapp.BindCustomer1_002(oCustomer)
    End Function

    Public Function BindCustomer2_002(ByVal oCustomer As Parameter.MktNewApp) As Parameter.MktNewApp Implements [Interface].IMktNewApp.BindCustomer2_002
        Dim damktnewapp As New SQLEngine.SalesMarketing.MktNewApp
        Return damktnewapp.BindCustomer2_002(oCustomer)
    End Function

    Public Function BindCustomerC1_002(ByVal oCustomer As Parameter.MktNewApp) As Parameter.MktNewApp Implements [Interface].IMktNewApp.BindCustomerC1_002
        Dim damktnewapp As New SQLEngine.SalesMarketing.MktNewApp
        Return damktnewapp.BindCustomerC1_002(oCustomer)
    End Function

    Public Function BindCustomerC2_002(ByVal oCustomer As Parameter.MktNewApp) As Parameter.MktNewApp Implements [Interface].IMktNewApp.BindCustomerC2_002
        Dim damktnewapp As New SQLEngine.SalesMarketing.MktNewApp
        Return damktnewapp.BindCustomerC2_002(oCustomer)
    End Function

    Public Sub MktNewAppEdit(ByVal oCustomClass As Parameter.MktNewApp) Implements [Interface].IMktNewApp.MktNewAppEdit
        Dim damktnewapp As New SQLEngine.SalesMarketing.MktNewApp
        damktnewapp.MktNewAppEdit(oCustomClass)
    End Sub

    Public Sub MktNewAppSave(ByVal oCustomClass As Parameter.MktNewApp) Implements [Interface].IMktNewApp.MktNewAppSave
        Dim damktnewapp As New SQLEngine.SalesMarketing.MktNewApp
        damktnewapp.MktNewAppSave(oCustomClass)
    End Sub

    Public Function GetAssetDescription(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp Implements [Interface].IMktNewApp.GetAssetDescription
        Dim damktnewapp As New SQLEngine.SalesMarketing.MktNewApp
        Return damktnewapp.GetAssetDescription(ocustomclass)
    End Function

    Public Function GetInsuranceCo(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp Implements [Interface].IMktNewApp.GetInsuranceCo
        Dim damktnewapp As New SQLEngine.SalesMarketing.MktNewApp
        Return damktnewapp.GetInsuranceCo(ocustomclass)
    End Function

    Public Function GetIndustryType(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp Implements [Interface].IMktNewApp.GetIndustryType
        Dim damktnewapp As New SQLEngine.SalesMarketing.MktNewApp
        Return damktnewapp.GetIndustryType(ocustomclass)
    End Function

    Public Function GetCompanyStatus(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp Implements [Interface].IMktNewApp.GetCompanyStatus
        Dim damktnewapp As New SQLEngine.SalesMarketing.MktNewApp
        Return damktnewapp.GetCompanyStatus(ocustomclass)
    End Function

    Public Sub MktNewAppSaveCompany(ByVal oCustomClass As Parameter.MktNewApp) Implements [Interface].IMktNewApp.MktNewAppSaveCompany
        Dim damktnewapp As New SQLEngine.SalesMarketing.MktNewApp
        damktnewapp.MktNewAppSaveCompany(oCustomClass)
    End Sub

    Public Function MktNewAppViewCompany(ByVal oCustomClass As Parameter.MktNewApp) As Parameter.MktNewApp Implements [Interface].IMktNewApp.MktNewAppViewCompany
        Dim damktnewapp As New SQLEngine.SalesMarketing.MktNewApp
        Return damktnewapp.MktNewAppViewCompany(oCustomClass)
    End Function

    Public Sub MktNewAppEditCompany(ByVal oCustomClass As Parameter.MktNewApp) Implements [Interface].IMktNewApp.MktNewAppEditCompany
        Dim damktnewapp As New SQLEngine.SalesMarketing.MktNewApp
        damktnewapp.MktNewAppEditCompany(oCustomClass)
    End Sub
End Class
