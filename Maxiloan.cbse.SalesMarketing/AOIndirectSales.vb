
Public Class AOIndirectSales
    Implements [Interface].IAOIndirectSales

    Public Function AOBudgetListing(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales Implements [Interface].IAOIndirectSales.AOBudgetListing
        Dim DAAOListing As New SQLEngine.SalesMarketing.AOIndirectSales
        Return DAAOListing.AOBudgetListing(customclass)
    End Function

    Public Function AOBudgetPrint(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales Implements [Interface].IAOIndirectSales.AOBudgetPrint
        Dim DAAOPrint As New SQLEngine.SalesMarketing.AOIndirectSales
        Return DAAOPrint.AOBudgetPrint(customclass)
    End Function

    Public Function AOBudgetView(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales Implements [Interface].IAOIndirectSales.AOBudgetView
        Dim DAAOView As New SQLEngine.SalesMarketing.AOIndirectSales
        Return DAAOView.AOBudgetView(customclass)
    End Function

    Public Function AOIndirectListing(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales Implements [Interface].IAOIndirectSales.AOIndirectListing
        Dim DAAOIndirectListing As New SQLEngine.SalesMarketing.AOIndirectSales
        Return DAAOIndirectListing.AOIndirectListing(customclass)
    End Function

    Public Function AOForecastListing(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales Implements [Interface].IAOIndirectSales.AOForecastListing
        Dim DAAOListing As New SQLEngine.SalesMarketing.AOIndirectSales
        Return DAAOListing.AOForecastListing(customclass)
    End Function

    Public Function AOForecastPrint(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales Implements [Interface].IAOIndirectSales.AOForecastPrint
        Dim DAAOPrint As New SQLEngine.SalesMarketing.AOIndirectSales
        Return DAAOPrint.AOForecastPrint(customclass)
    End Function

    Public Function AOForecastView(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales Implements [Interface].IAOIndirectSales.AOForecastView
        Dim DAAOView As New SQLEngine.SalesMarketing.AOIndirectSales
        Return DAAOView.AOForecastView(customclass)
    End Function
End Class
