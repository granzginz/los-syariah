
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class ProspectResultAppraiser
    Implements IProspectResultAppraiser

    Function GetProspect(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser Implements [Interface].IProspectResultAppraiser.GetProspect
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.GetProspect(oCustomClass)
    End Function
    Function ProspectSaveAdd(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser Implements [Interface].IProspectResultAppraiser.ProspectSaveAdd
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.ProspectSaveAdd(oCustomClass)
    End Function
    Function ProspectSaveAsset(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser Implements [Interface].IProspectResultAppraiser.ProspectSaveAsset
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.ProspectSaveAsset(oCustomClass)
    End Function
    Function ProspectSaveDemografi(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser Implements [Interface].IProspectResultAppraiser.ProspectSaveDemografi
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.ProspectSaveDemografi(oCustomClass)
    End Function
    Function ProspectSaveFinancial(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser Implements [Interface].IProspectResultAppraiser.ProspectSaveFinancial
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.ProspectSaveFinancial(oCustomClass)
    End Function
    Function GetViewProspect(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser Implements [Interface].IProspectResultAppraiser.GetViewProspect
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.GetViewProspect(oCustomClass)
    End Function
    Function GetCbo(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser Implements [Interface].IProspectResultAppraiser.GetCbo
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.GetCbo(oCustomClass)
    End Function
    Function GetCboGeneral(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser Implements [Interface].IProspectResultAppraiser.GetCboGeneral
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.GetCboGeneral(oCustomClass)
    End Function

    Function DoProceeded(ByVal cnn As String, prospectId As String) As Boolean Implements [Interface].IProspectResultAppraiser.DoProceeded
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.DoProceeded(cnn, prospectId)
    End Function


    Public Function DispositionCreditRpt(cnn As String, whereCond As String(), tp As String) As System.Data.DataSet Implements [Interface].IProspectResultAppraiser.DispositionCreditRpt
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.DispositionCreditRpt(cnn, whereCond, tp)
    End Function

    Public Function DoProspectCreaditScoring(cnn As String, branchId As String, prospectAppId As String, ScoringType As String, bnsDate As Date) As String Implements [Interface].IProspectResultAppraiser.DoProspectCreaditScoring
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.DoProspectCreaditScoring(cnn, branchId, prospectAppId, ScoringType, bnsDate)
    End Function


    Public Function GetProspectScorePolicyResult(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser Implements [Interface].IProspectResultAppraiser.GetProspectScorePolicyResult
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.GetProspectScorePolicyResult(oCustomClass)
    End Function

    Public Function DoProspectDecision(cnn As String, prospectAppId As String, appr As Integer) Implements [Interface].IProspectResultAppraiser.DoProspectDecision
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.DoProspectDecision(cnn, prospectAppId, appr)
    End Function

    Public Function MasterDataReviewRpt(cnn As String, whereCond As String(), tp As String) As System.Data.DataSet Implements [Interface].IProspectResultAppraiser.MasterDataReviewRpt
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.MasterDataReviewRpt(cnn, whereCond, tp)
    End Function

    Function GetInitial(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser Implements [Interface].IProspectResultAppraiser.GetInitial
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.GetInitial(oCustomClass)
    End Function

    Function InitialSaveAdd(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser Implements [Interface].IProspectResultAppraiser.InitialSaveAdd
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.InitialSaveAdd(oCustomClass)
    End Function

    Function DoBIChecking(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser Implements [Interface].IProspectResultAppraiser.DoBIChecking
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.DoBIChecking(oCustomClass)
    End Function

    Function DataSurveySaveAdd(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser Implements [Interface].IProspectResultAppraiser.DataSurveySaveAdd
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.DataSurveySaveAdd(oCustomClass)
    End Function

    Function DataSurveySaveEdit(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser Implements [Interface].IProspectResultAppraiser.DataSurveySaveEdit
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.DataSurveySaveEdit(oCustomClass)
    End Function

    Function ProspectReturnUpdate(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As String Implements [Interface].IProspectResultAppraiser.ProspectReturnUpdate
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.ProspectReturnUpdate(oCustomClass)
    End Function

    Function ProspectLogSave(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser Implements [Interface].IProspectResultAppraiser.ProspectLogSave
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.ProspectLogSave(oCustomClass)
    End Function

    Function GetInqProspect(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser Implements [Interface].IProspectResultAppraiser.GetInqProspect
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.GetInqProspect(oCustomClass)
    End Function

    Public Function DoProspectCreaditScoringProceed(cnn As String, branchId As String, prospectAppId As String, ScoringType As String, bnsDate As Date, proceed As Boolean) As String Implements [Interface].IProspectResultAppraiser.DoProspectCreaditScoringProceed
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.DoProspectCreaditScoringProceed(cnn, branchId, prospectAppId, ScoringType, bnsDate, proceed)
    End Function

    Public Function GetApprovalprospect(ByVal oCustomClass As Parameter.ProspectResultAppraiser, ByVal strApproval As String) Implements [Interface].IProspectResultAppraiser.GetApprovalprospect
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.GetApprovalprospect(oCustomClass, strApproval)
    End Function


    Function SaveProspectAssignAppraisal(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser Implements [Interface].IProspectResultAppraiser.SaveProspectAssignAppraisal
        Dim DA As New SQLEngine.SalesMarketing.ProspectResultAppraiser
        Return DA.SaveProspectAssignAppraisal(oCustomClass)
    End Function

End Class

