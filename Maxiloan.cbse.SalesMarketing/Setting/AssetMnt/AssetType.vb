
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class AssetType : Inherits ComponentBase
    Implements IAssetType
#Region "AssetType"
    Public Function GetAssetType(ByVal customClass As Parameter.AssetType) As Parameter.AssetType Implements [Interface].IAssetType.GetAssetType        
            Dim AssetTypeDA As New SQLEngine.SalesMarketing.AssetType
            Return AssetTypeDA.GetAssetType(customClass)       
    End Function
    Public Function GetAssetTypeReport(ByVal customClass As Parameter.AssetType) As Parameter.AssetType Implements [Interface].IAssetType.GetAssetTypeReport
        Try
            Dim AssetTypeDA As New SQLEngine.SalesMarketing.AssetType
            Return AssetTypeDA.GetAssetTypeReport(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetAssetTypeEdit(ByVal customClass As Parameter.AssetType) As Parameter.AssetType Implements [Interface].IAssetType.GetAssetTypeEdit
        Try
            Dim AssetTypeDA As New SQLEngine.SalesMarketing.AssetType
            Return AssetTypeDA.GetAssetTypeEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function AssetTypeSaveAdd(ByVal customClass As Parameter.AssetType) As String Implements IAssetType.AssetTypeSaveAdd
        'Trace.WriteIf(MethodTrace.Enabled, "Enter")
        Try
            Dim AssetTypeDA As New SQLEngine.SalesMarketing.AssetType
            Return AssetTypeDA.AssetTypeSaveAdd(customClass)
            'Throw busEx
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

    Public Function AssetTypeDelete(ByVal customClass As Parameter.AssetType) As String Implements IAssetType.AssetTypeDelete
        Try
            Dim AssetTypeDA As New SQLEngine.SalesMarketing.AssetType
            Return AssetTypeDA.AssetTypeDelete(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            'Throw New Exception("Error when Delete AssetType", ex)
        End Try
    End Function


    Public Sub AssetTypeSaveEdit(ByVal customClass As Parameter.AssetType) Implements IAssetType.AssetTypeSaveEdit
        Try
            Dim AssetTypeDA As New SQLEngine.SalesMarketing.AssetType
            AssetTypeDA.AssetTypeSaveEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when saving Update AssetType", ex)
        End Try
    End Sub
#End Region

#Region "AssetTypeAttribute"
    Public Function GetAssetTypeAttribute(ByVal customClass As Parameter.AssetTypeAttribute) As Parameter.AssetTypeAttribute Implements [Interface].IAssetType.GetAssetTypeAttribute
        Try
            Dim AssetTypeAttributeDA As New SQLEngine.SalesMarketing.AssetTypeAttribute
            Return AssetTypeAttributeDA.GetAssetTypeAttribute(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetAssetTypeAttributeReport(ByVal customClass As Parameter.AssetTypeAttribute) As Parameter.AssetTypeAttribute Implements [Interface].IAssetType.GetAssetTypeAttributeReport
        Try
            Dim AssetTypeAttributeDA As New SQLEngine.SalesMarketing.AssetTypeAttribute
            Return AssetTypeAttributeDA.GetAssetTypeAttributeReport(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetAssetTypeAttributeEdit(ByVal customClass As Parameter.AssetTypeAttribute) As Parameter.AssetTypeAttribute Implements [Interface].IAssetType.GetAssetTypeAttributeEdit
        Try
            Dim AssetTypeAttributeDA As New SQLEngine.SalesMarketing.AssetTypeAttribute
            Return AssetTypeAttributeDA.GetAssetTypeAttributeEdit(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function AssetTypeAttributeSaveAdd(ByVal customClass As Parameter.AssetTypeAttribute) As String Implements IAssetType.AssetTypeAttributeSaveAdd
        'Trace.WriteIf(MethodTrace.Enabled, "Enter")
        Try
            Dim AssetTypeAttributeDA As New SQLEngine.SalesMarketing.AssetTypeAttribute
            Return AssetTypeAttributeDA.AssetTypeAttributeSaveAdd(customClass)

            'Catch busEx As BusinessRuleException
            'Throw busEx
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function
    Public Sub AssetTypeAttributeSaveEdit(ByVal customClass As Parameter.AssetTypeAttribute) Implements IAssetType.AssetTypeAttributeSaveEdit
        Try
            Dim AssetTypeAttributeDA As New SQLEngine.SalesMarketing.AssetTypeAttribute
            AssetTypeAttributeDA.AssetTypeAttributeSaveEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when saving Update AssetTypeAttribute", ex)

        End Try
    End Sub
    Public Function AssetTypeAttributeDelete(ByVal customClass As Parameter.AssetTypeAttribute) As String Implements IAssetType.AssetTypeAttributeDelete
        Try
            Dim AssetTypeAttributeDA As New SQLEngine.SalesMarketing.AssetTypeAttribute
            Return AssetTypeAttributeDA.AssetTypeAttributeDelete(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when Delete AssetTypeAttribute", ex)

        End Try
    End Function
#End Region

#Region "AssetTypeCategory"
    Public Function GetAssetTypeCategory(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory Implements [Interface].IAssetType.GetAssetTypeCategory
        Try
            Dim AssetTypeCategoryDA As New SQLEngine.SalesMarketing.AssetTypeCategory
            Return AssetTypeCategoryDA.GetAssetTypeCategory(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetAssetTypeCategoryReport(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory Implements [Interface].IAssetType.GetAssetTypeCategoryReport
        Try
            Dim AssetTypeCategoryDA As New SQLEngine.SalesMarketing.AssetTypeCategory
            Return AssetTypeCategoryDA.GetAssetTypeCategoryReport(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetAssetTypeCategoryEdit(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory Implements [Interface].IAssetType.GetAssetTypeCategoryEdit
        Try
            Dim AssetTypeCategoryDA As New SQLEngine.SalesMarketing.AssetTypeCategory
            Return AssetTypeCategoryDA.GetAssetTypeCategoryEdit(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetAssetTypeCategoryInsRate(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory Implements [Interface].IAssetType.GetAssetTypeCategoryInsRate
        Try
            Dim AssetTypeCategoryDA As New SQLEngine.SalesMarketing.AssetTypeCategory
            Return AssetTypeCategoryDA.GetAssetTypeCategoryInsRate(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetAssetTypeCategoryInsRateforSpecificAssetType(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory Implements [Interface].IAssetType.GetAssetTypeCategoryInsRateforSpecificAssetType
        Try
            Dim AssetTypeCategoryDA As New SQLEngine.SalesMarketing.AssetTypeCategory
            Return AssetTypeCategoryDA.GetAssetTypeCategoryInsRateforSpecificAssetType(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function AssetTypeCategorySaveAdd(ByVal customClass As Parameter.AssetTypeCategory) As String Implements IAssetType.AssetTypeCategorySaveAdd
        'Trace.WriteIf(MethodTrace.Enabled, "Enter")
        Try
            Dim AssetTypeCategoryDA As New SQLEngine.SalesMarketing.AssetTypeCategory
            Return AssetTypeCategoryDA.AssetTypeCategorySaveAdd(customClass)

            'Catch busEx As BusinessRuleException
            'Throw busEx
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function
    Public Sub AssetTypeCategorySaveEdit(ByVal customClass As Parameter.AssetTypeCategory) Implements IAssetType.AssetTypeCategorySaveEdit
        Try
            Dim AssetTypeCategoryDA As New SQLEngine.SalesMarketing.AssetTypeCategory
            AssetTypeCategoryDA.AssetTypeCategorySaveEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when saving Update AssetTypeCategory", ex)

        End Try
    End Sub
    Public Function AssetTypeCategoryDelete(ByVal customClass As Parameter.AssetTypeCategory) As String Implements IAssetType.AssetTypeCategoryDelete
        Try
            Dim AssetTypeCategoryDA As New SQLEngine.SalesMarketing.AssetTypeCategory
            Return AssetTypeCategoryDA.AssetTypeCategoryDelete(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when Delete AssetTypeCategory", ex)

        End Try
    End Function
#End Region

#Region "AssetTypeDocumentList"
    Public Function GetAssetTypeDocumentList(ByVal customClass As Parameter.AssetTypeDocumentList) As Parameter.AssetTypeDocumentList Implements [Interface].IAssetType.GetAssetTypeDocumentList
        Try
            Dim AssetTypeDocumentListDA As New SQLEngine.SalesMarketing.AssetTypeDocumentList
            Return AssetTypeDocumentListDA.GetAssetTypeDocumentList(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetAssetTypeDocumentListReport(ByVal customClass As Parameter.AssetTypeDocumentList) As Parameter.AssetTypeDocumentList Implements [Interface].IAssetType.GetAssetTypeDocumentListReport
        Try
            Dim AssetTypeDocumentListDA As New SQLEngine.SalesMarketing.AssetTypeDocumentList
            Return AssetTypeDocumentListDA.GetAssetTypeDocumentListReport(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetAssetTypeDocumentListEdit(ByVal customClass As Parameter.AssetTypeDocumentList) As Parameter.AssetTypeDocumentList Implements [Interface].IAssetType.GetAssetTypeDocumentListEdit
        Try
            Dim AssetTypeDocumentListDA As New SQLEngine.SalesMarketing.AssetTypeDocumentList
            Return AssetTypeDocumentListDA.GetAssetTypeDocumentListEdit(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function AssetTypeDocumentListSaveAdd(ByVal customClass As Parameter.AssetTypeDocumentList) As String Implements IAssetType.AssetTypeDocumentListSaveAdd
        'Trace.WriteIf(MethodTrace.Enabled, "Enter")
        Try
            Dim AssetTypeDocumentListDA As New SQLEngine.SalesMarketing.AssetTypeDocumentList
            Return AssetTypeDocumentListDA.AssetTypeDocumentListSaveAdd(customClass)

            'Catch busEx As BusinessRuleException
            'Throw busEx
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            'Throw ex
        End Try
    End Function
    Public Function AssetTypeDocumentListSaveEdit(ByVal customClass As Parameter.AssetTypeDocumentList) As String Implements IAssetType.AssetTypeDocumentListSaveEdit
        Try
            Dim AssetTypeDocumentListDA As New SQLEngine.SalesMarketing.AssetTypeDocumentList
            Return AssetTypeDocumentListDA.AssetTypeDocumentListSaveEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            'Throw New Exception("Error when saving Update AssetTypeDocumentList", ex)
        End Try
    End Function
    Public Function AssetTypeDocumentListDelete(ByVal customClass As Parameter.AssetTypeDocumentList) As String Implements IAssetType.AssetTypeDocumentListDelete
        Try
            Dim AssetTypeDocumentListDA As New SQLEngine.SalesMarketing.AssetTypeDocumentList
            Return AssetTypeDocumentListDA.AssetTypeDocumentListDelete(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            'Throw New Exception("Error when Delete AssetTypeDocumentList", ex)
        End Try
    End Function
#End Region

#Region "AssetTypeOrigination"
    Public Function GetAssetTypeOrigination(ByVal customClass As Parameter.AssetTypeOrigination) As Parameter.AssetTypeOrigination Implements [Interface].IAssetType.GetAssetTypeOrigination
        Try
            Dim AssetTypeOriginationDA As New SQLEngine.SalesMarketing.AssetTypeOrigination
            Return AssetTypeOriginationDA.GetAssetTypeOrigination(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetAssetTypeOriginationReport(ByVal customClass As Parameter.AssetTypeOrigination) As Parameter.AssetTypeOrigination Implements [Interface].IAssetType.GetAssetTypeOriginationReport
        Try
            Dim AssetTypeOriginationDA As New SQLEngine.SalesMarketing.AssetTypeOrigination
            Return AssetTypeOriginationDA.GetAssetTypeOriginationReport(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetAssetTypeOriginationEdit(ByVal customClass As Parameter.AssetTypeOrigination) As Parameter.AssetTypeOrigination Implements [Interface].IAssetType.GetAssetTypeOriginationEdit
        Try
            Dim AssetTypeOriginationDA As New SQLEngine.SalesMarketing.AssetTypeOrigination
            Return AssetTypeOriginationDA.GetAssetTypeOriginationEdit(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function AssetTypeOriginationSaveAdd(ByVal customClass As Parameter.AssetTypeOrigination) As String Implements IAssetType.AssetTypeOriginationSaveAdd
        'Trace.WriteIf(MethodTrace.Enabled, "Enter")
        Try
            Dim AssetTypeOriginationDA As New SQLEngine.SalesMarketing.AssetTypeOrigination
            Return AssetTypeOriginationDA.AssetTypeOriginationSaveAdd(customClass)
            'Catch busEx As BusinessRuleException
            'Throw busEx
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function
    Public Sub AssetTypeOriginationSaveEdit(ByVal customClass As Parameter.AssetTypeOrigination) Implements IAssetType.AssetTypeOriginationSaveEdit
        Try
            Dim AssetTypeOriginationDA As New SQLEngine.SalesMarketing.AssetTypeOrigination
            AssetTypeOriginationDA.AssetTypeOriginationSaveEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when saving Update AssetTypeOrigination", ex)
        End Try
    End Sub
    Public Function AssetTypeOriginationDelete(ByVal customClass As Parameter.AssetTypeOrigination) As String Implements IAssetType.AssetTypeOriginationDelete
        Try
            Dim AssetTypeOriginationDA As New SQLEngine.SalesMarketing.AssetTypeOrigination
            Return AssetTypeOriginationDA.AssetTypeOriginationDelete(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when Delete AssetTypeOrigination", ex)
        End Try
    End Function
#End Region

#Region "AssetTypeScheme"
    Public Function GetAssetTypeScheme(ByVal customClass As Parameter.AssetTypeScheme) As Parameter.AssetTypeScheme Implements [Interface].IAssetType.GetAssetTypeScheme
        Try
            Dim AssetTypeSchemeDA As New SQLEngine.SalesMarketing.AssetTypeScheme
            Return AssetTypeSchemeDA.GetAssetTypeScheme(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetAssetTypeSchemeReport(ByVal customClass As Parameter.AssetTypeScheme) As Parameter.AssetTypeScheme Implements [Interface].IAssetType.GetAssetTypeSchemeReport
        Try
            Dim AssetTypeSchemeDA As New SQLEngine.SalesMarketing.AssetTypeScheme
            Return AssetTypeSchemeDA.GetAssetTypeSchemeReport(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetAssetTypeSchemeEdit(ByVal customClass As Parameter.AssetTypeScheme) As Parameter.AssetTypeScheme Implements [Interface].IAssetType.GetAssetTypeSchemeEdit
        Try
            Dim AssetTypeSchemeDA As New SQLEngine.SalesMarketing.AssetTypeScheme
            Return AssetTypeSchemeDA.GetAssetTypeSchemeEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function AssetTypeSchemeSaveAdd(ByVal customClass As Parameter.AssetTypeScheme) As String Implements IAssetType.AssetTypeSchemeSaveAdd
        Try
            Dim AssetTypeSchemeDA As New SQLEngine.SalesMarketing.AssetTypeScheme
            Return AssetTypeSchemeDA.AssetTypeSchemeSaveAdd(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            'Throw ex
        End Try
    End Function
    Public Sub AssetTypeSchemeSaveEdit(ByVal customClass As Parameter.AssetTypeScheme) Implements IAssetType.AssetTypeSchemeSaveEdit
        Try
            Dim AssetTypeSchemeDA As New SQLEngine.SalesMarketing.AssetTypeScheme
            AssetTypeSchemeDA.AssetTypeSchemeSaveEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            'Throw New Exception("Error when saving Update AssetTypeScheme", ex)
        End Try
    End Sub
    Public Function AssetTypeSchemeDelete(ByVal customClass As Parameter.AssetTypeScheme) As String Implements IAssetType.AssetTypeSchemeDelete
        Try
            Dim AssetTypeSchemeDA As New SQLEngine.SalesMarketing.AssetTypeScheme
            Return AssetTypeSchemeDA.AssetTypeSchemeDelete(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            'Throw New Exception("Error when Delete AssetTypeScheme", ex)

        End Try
    End Function

    Public Function AssetTypeSchemeAdd(ByVal customClass As Parameter.AssetTypeScheme) As String Implements IAssetType.AssetTypeSchemeAdd
        Try
            Dim AssetTypeSchemeDA As New SQLEngine.SalesMarketing.AssetTypeScheme
            Return AssetTypeSchemeDA.AssetTypeSchemeAdd(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            'Throw New Exception("Error when Delete AssetTypeScheme", ex)
        End Try
    End Function
#End Region

#Region "AssetMaster"
    Public Function GetMaxLevel(ByVal strAssetTypeID As String, ByVal strConnection As String) As Integer Implements [Interface].IAssetType.GetMaxLevel
        Try
            Dim AssetTypeSchemeDA As New SQLEngine.SalesMarketing.AssetType
            Return AssetTypeSchemeDA.GetMaxLevel(strAssetTypeID, strConnection)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
#End Region

End Class