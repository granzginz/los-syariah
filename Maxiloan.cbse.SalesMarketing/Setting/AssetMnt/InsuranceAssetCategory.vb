
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class InsuranceAssetCategory : Inherits ComponentBase
    Implements IInsuranceAssetCategory

    Public Function GetInsuranceAssetCategory(ByVal oInsuranceAssetCategory As Parameter.InsuranceAssetCategory) As Parameter.InsuranceAssetCategory Implements IInsuranceAssetCategory.GetInsuranceAssetCategory
        Try
            Dim InsuranceAssetCategoryListDA As New SQLEngine.SalesMarketing.InsuranceAssetCategory
            Return InsuranceAssetCategoryListDA.GetInsuranceAssetCategory(oInsuranceAssetCategory)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetInsuranceAssetCategoryReport(ByVal oInsuranceAssetCategory As Parameter.InsuranceAssetCategory) As Parameter.InsuranceAssetCategory Implements IInsuranceAssetCategory.GetInsuranceAssetCategoryReport
        Try
            Dim InsuranceAssetCategoryListDA As New SQLEngine.SalesMarketing.InsuranceAssetCategory
            Return InsuranceAssetCategoryListDA.GetInsuranceAssetCategoryReport(oInsuranceAssetCategory)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function InsuranceAssetCategoryAdd(ByVal oInsuranceAssetCategory As Parameter.InsuranceAssetCategory) As String Implements [Interface].IInsuranceAssetCategory.InsuranceAssetCategoryAdd
        Try
            Dim InsuranceAssetCategoryDA As New SQLEngine.SalesMarketing.InsuranceAssetCategory
            Return InsuranceAssetCategoryDA.InsuranceAssetCategoryAdd(oInsuranceAssetCategory)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function InsuranceAssetCategoryUpdate(ByVal oInsuranceAssetCategory As Parameter.InsuranceAssetCategory) As String Implements [Interface].IInsuranceAssetCategory.InsuranceAssetCategoryUpdate
        Try
            Dim InsuranceAssetCategoryDA As New SQLEngine.SalesMarketing.InsuranceAssetCategory
            Return InsuranceAssetCategoryDA.InsuranceAssetCategoryUpdate(oInsuranceAssetCategory)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function InsuranceAssetCategoryDelete(ByVal oInsuranceAssetCategory As Parameter.InsuranceAssetCategory) As String Implements [Interface].IInsuranceAssetCategory.InsuranceAssetCategoryDelete
        Try
            Dim InsuranceAssetCategoryDA As New SQLEngine.SalesMarketing.InsuranceAssetCategory
            Return InsuranceAssetCategoryDA.InsuranceAssetCategoryDelete(oInsuranceAssetCategory)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

End Class
