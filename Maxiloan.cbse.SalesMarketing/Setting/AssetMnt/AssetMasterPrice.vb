﻿
#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class AssetMasterPrice : Inherits ComponentBase
    Implements IAssetMasterPrice
    Dim oSQLE As New SQLEngine.SalesMarketing.AssetMasterPrice

    Public Function AssetMasterPriceDelete(ByVal oCustomClass As Parameter.AssetMasterPrice) As String Implements [Interface].IAssetMasterPrice.AssetMasterPriceDelete
        Return oSQLE.AssetMasterPriceDelete(oCustomClass)
    End Function

    Public Function AssetMasterPriceSaveAdd(ByVal oCustomClass As Parameter.AssetMasterPrice) As String Implements [Interface].IAssetMasterPrice.AssetMasterPriceSaveAdd
        Return oSQLE.AssetMasterPriceSaveAdd(oCustomClass)
    End Function

    Public Sub AssetMasterPriceSaveEdit(ByVal oCustomClass As Parameter.AssetMasterPrice) Implements [Interface].IAssetMasterPrice.AssetMasterPriceSaveEdit
        oSQLE.AssetMasterPriceSaveEdit(oCustomClass)
    End Sub

    Public Function GetAssetMasterPrice(ByVal oCustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice Implements [Interface].IAssetMasterPrice.GetAssetMasterPrice
        Return oSQLE.GetAssetMasterPrice(oCustomClass)
    End Function

    Public Function GetAssetMasterPriceList(ByVal oCustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice Implements [Interface].IAssetMasterPrice.GetAssetMasterPriceList
        Return oSQLE.GetAssetMasterPriceList(oCustomClass)
    End Function

    Public Function GetAssetMasterPriceReport(ByVal oCustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice Implements [Interface].IAssetMasterPrice.GetAssetMasterPriceReport
        Return oSQLE.GetAssetMasterPriceReport(oCustomClass)
    End Function

    Public Function GetAssetTypeCombo(ByVal oCustomClass As Parameter.AssetMasterPrice) As DataTable Implements [Interface].IAssetMasterPrice.GetAssetTypeCombo
        Return oSQLE.GetAssetTypeCombo(oCustomClass)
    End Function

    Public Function GetAssetMaster(ByVal oCustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice Implements [Interface].IAssetMasterPrice.GetAssetMaster
        Return oSQLE.GetAssetMaster(oCustomClass)
    End Function

    Public Function GetAssetCodeCombo(ByVal oCustomClass As Parameter.AssetMasterPrice) As DataTable Implements [Interface].IAssetMasterPrice.GetAssetCodeCombo
        Return oSQLE.GetAssetCodeCombo(oCustomClass)
    End Function

    Public Function GetAllAssetCode(ByVal oCustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice Implements [Interface].IAssetMasterPrice.GetAllAssetCode
        Return oSQLE.GetAllAssetCode(oCustomClass)
    End Function

    Public Function GetAssetMasterByAssetCode(ByVal oCustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice Implements [Interface].IAssetMasterPrice.GetAssetMasterByAssetCode
        Return oSQLE.GetAssetMasterByAssetCode(oCustomClass)
    End Function
End Class
