

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class CreditScoringMain : Inherits ComponentBase
    Implements ICreditScoringMain

#Region "Main"
    Public Function CreditScoringMainSaveAdd(ByVal customClass As Parameter.CreditScoringMain) As String Implements [Interface].ICreditScoringMain.CreditScoringMainSaveAdd
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            Return CreditDA.CreditScoringMainSaveAdd(customClass)
            'Throw busEx
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

    Public Function CreditScoringMainDelete(ByVal customClass As Parameter.CreditScoringMain) As String Implements [Interface].ICreditScoringMain.CreditScoringMainDelete
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            Return CreditDA.CreditScoringMainDelete(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Sub CreditScoringMainSaveEdit(ByVal customClass As Parameter.CreditScoringMain) Implements [Interface].ICreditScoringMain.CreditScoringMainSaveEdit
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            CreditDA.CreditScoringMainSaveEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            'Throw New Exception("Error when saving Update AssetType", ex)
        End Try
    End Sub

    Public Function GetCreditScoringMain(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain Implements [Interface].ICreditScoringMain.GetCreditScoringMain
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            Return CreditDA.GetCreditScoringMain(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function


    Public Function CreditScoringMainEdit(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain Implements [Interface].ICreditScoringMain.CreditScoringMainEdit
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            Return CreditDA.CreditScoringMainEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function CreditScoringMainAdd(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain Implements [Interface].ICreditScoringMain.CreditScoringMainAdd
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            Return CreditDA.CreditScoringMainAdd(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetCreditScoringMainReport(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain Implements [Interface].ICreditScoringMain.GetCreditScoringMainReport
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            Return CreditDA.GetCreditScoringMainReport(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
#End Region

#Region "View"
    Public Function CreditScoringMainView(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain Implements [Interface].ICreditScoringMain.CreditScoringMainView
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            Return CreditDA.CreditScoringMainView(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
#End Region

#Region "EditContent"
    Public Function EditContentSaveAdd(ByVal customClass As Parameter.CreditScoringMain) As String Implements [Interface].ICreditScoringMain.EditContentSaveAdd
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            Return CreditDA.EditContentSaveAdd(customClass)
            'Throw busEx
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

    Public Function EditContentDelete(ByVal customClass As Parameter.CreditScoringMain) As String Implements [Interface].ICreditScoringMain.EditContentDelete
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            Return CreditDA.EditContentDelete(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Sub EditContentSaveEdit(ByVal customClass As Parameter.CreditScoringMain) Implements [Interface].ICreditScoringMain.EditContentSaveEdit
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            CreditDA.EditContentSaveEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            'Throw New Exception("Error when saving Update AssetType", ex)
        End Try
    End Sub

    Public Function GetEditContentPaging(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain Implements [Interface].ICreditScoringMain.GetEditContentPaging
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            Return CreditDA.GetEditContentPaging(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function


    Public Function EditContentEdit(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain Implements [Interface].ICreditScoringMain.EditContentEdit
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            Return CreditDA.EditContentEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function



#End Region

#Region "EditContent_Table"
    Public Sub EditContentSaveEdit_Table(ByVal customClass As Parameter.CreditScoringMain) Implements [Interface].ICreditScoringMain.EditContentSaveEdit_Table
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            CreditDA.EditContentSaveEdit_Table(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            'Throw New Exception("Error when saving Update AssetType", ex)
        End Try
    End Sub

    Public Function GetEditContentPaging_Table(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain Implements [Interface].ICreditScoringMain.GetEditContentPaging_Table
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            Return CreditDA.GetEditContentPaging_Table(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function


    Public Function EditContentEdit_Table(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain Implements [Interface].ICreditScoringMain.EditContentEdit_Table
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            Return CreditDA.EditContentEdit_Table(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

#End Region

#Region "Cut Off Score"
    Public Function CreditScoringCutOff(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain Implements [Interface].ICreditScoringMain.CreditScoringCutOff
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            Return CreditDA.CreditScoringCutOff(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function CreditScoringCutOffAdd(ByVal customClass As Parameter.CreditScoringMain) As String Implements [Interface].ICreditScoringMain.CreditScoringCutOffAdd
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            Return CreditDA.CreditScoringCutOffAdd(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function CreditScoringCutOffEdit(ByVal customClass As Parameter.CreditScoringMain) As String Implements [Interface].ICreditScoringMain.CreditScoringCutOffEdit
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            Return CreditDA.CreditScoringCutOffEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function CreditScoringCutOffDelete(ByVal customClass As Parameter.CreditScoringMain) As String Implements [Interface].ICreditScoringMain.CreditScoringCutOffDelete
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.CreditScoringMain
            Return CreditDA.CreditScoringCutOffDelete(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
#End Region
End Class

