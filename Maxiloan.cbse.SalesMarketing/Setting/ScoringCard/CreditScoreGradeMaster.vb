
#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class CreditScoreGradeMaster : Inherits ComponentBase
    Implements ICreditScoreGradeMaster
    Dim oSQLE As New SQLEngine.SalesMarketing.CreditScoreGradeMaster

    Public Function CreditScoreGradeMasterDelete(ByVal oCustomClass As Parameter.CreditScoreGradeMaster) As String Implements [Interface].ICreditScoreGradeMaster.CreditScoreGradeMasterDelete
        Return oSQLE.CreditScoreGradeMasterDelete(oCustomClass)
    End Function

    Public Function CreditScoreGradeMasterSaveAdd(ByVal oCustomClass As Parameter.CreditScoreGradeMaster) As String Implements [Interface].ICreditScoreGradeMaster.CreditScoreGradeMasterSaveAdd
        Return oSQLE.CreditScoreGradeMasterSaveAdd(oCustomClass)
    End Function

    Public Sub CreditScoreGradeMasterSaveEdit(ByVal oCustomClass As Parameter.CreditScoreGradeMaster) Implements [Interface].ICreditScoreGradeMaster.CreditScoreGradeMasterSaveEdit
        oSQLE.CreditScoreGradeMasterSaveEdit(oCustomClass)
    End Sub

    Public Function GetCreditScoreGradeMasterList(ByVal oCustomClass As Parameter.CreditScoreGradeMaster) As List(Of Parameter.CreditScoreGradeMaster) Implements [Interface].ICreditScoreGradeMaster.GetCreditScoreGradeMasterList
        Return oSQLE.GetCreditScoreGradeMasterList(oCustomClass)
    End Function

End Class