
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class IncentiveCard : Inherits ComponentBase
    Implements IIncentiveCard
    Public Function GetViewByID(ByVal oCustomClass As Parameter.IncentiveCard) As Parameter.IncentiveCard Implements [Interface].IIncentiveCard.GetViewByID
        Try
            Dim DA As New SQLEngine.SalesMarketing.IncentiveCard
            Return DA.GetViewByID(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function
    Public Sub SupplierINCTVHeaderAdd(ByVal CustomClass As Parameter.IncentiveCard) Implements [Interface].IIncentiveCard.SupplierINCTVHeaderAdd
        Try
            Dim DA As New SQLEngine.SalesMarketing.IncentiveCard
            DA.SupplierINCTVHeaderAdd(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub SupplierINCTVHeaderEdit(ByVal CustomClass As Parameter.IncentiveCard) Implements [Interface].IIncentiveCard.SupplierINCTVHeaderEdit
        Try
            Dim DA As New SQLEngine.SalesMarketing.IncentiveCard
            DA.SupplierINCTVHeaderEdit(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub SupplierINCTVHeaderDelete(ByVal CustomClass As Parameter.IncentiveCard) Implements [Interface].IIncentiveCard.SupplierINCTVHeaderDelete
        Try
            Dim DA As New SQLEngine.SalesMarketing.IncentiveCard
            DA.SupplierINCTVHeaderDelete(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Function GetComboSupplierIncentiveCard(ByVal oCustomClass As Parameter.IncentiveCard) As DataTable Implements [Interface].IIncentiveCard.GetComboSupplierIncentiveCard
        Try
            Dim DA As New SQLEngine.SalesMarketing.IncentiveCard
            Return DA.GetComboSupplierIncentiveCard(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function
    Public Sub SupplierINCTVDetailUnitAdd(ByVal CustomClass As Parameter.IncentiveCard) Implements [Interface].IIncentiveCard.SupplierINCTVDetailUnitAdd
        Try
            Dim DA As New SQLEngine.SalesMarketing.IncentiveCard
            DA.SupplierINCTVDetailUnitAdd(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub SupplierINCTVDetailUnitEdit(ByVal CustomClass As Parameter.IncentiveCard) Implements [Interface].IIncentiveCard.SupplierINCTVDetailUnitEdit
        Try
            Dim DA As New SQLEngine.SalesMarketing.IncentiveCard
            DA.SupplierINCTVDetailUnitEdit(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub SupplierINCTVDetailAFAdd(ByVal CustomClass As Parameter.IncentiveCard) Implements [Interface].IIncentiveCard.SupplierINCTVDetailAFAdd
        Try
            Dim DA As New SQLEngine.SalesMarketing.IncentiveCard
            DA.SupplierINCTVDetailAFAdd(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub SupplierINCTVDetailAFEdit(ByVal CustomClass As Parameter.IncentiveCard) Implements [Interface].IIncentiveCard.SupplierINCTVDetailAFEdit
        Try
            Dim DA As New SQLEngine.SalesMarketing.IncentiveCard
            DA.SupplierINCTVDetailAFEdit(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub SupplierINCTVDailyDAdd(ByVal CustomClass As Parameter.IncentiveCard) Implements [Interface].IIncentiveCard.SupplierINCTVDailyDAdd
        Try
            Dim DA As New SQLEngine.SalesMarketing.IncentiveCard
            DA.SupplierINCTVDailyDAdd(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub SupplierINCTVUpdateIncentiveRestAmount(ByVal CustomClass As Parameter.IncentiveCard) Implements [Interface].IIncentiveCard.SupplierINCTVUpdateIncentiveRestAmount
        Try
            Dim DA As New SQLEngine.SalesMarketing.IncentiveCard
            DA.SupplierINCTVUpdateIncentiveRestAmount(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub SupplierINCTVUpdateIncentiveRestEmployeeAmount(ByVal CustomClass As Parameter.IncentiveCard) Implements [Interface].IIncentiveCard.SupplierINCTVUpdateIncentiveRestEmployeeAmount
        Try
            Dim DA As New SQLEngine.SalesMarketing.IncentiveCard
            DA.SupplierINCTVUpdateIncentiveRestEmployeeAmount(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub SupplierINCTVIncentiveRestAmountExecution(ByVal CustomClass As Parameter.IncentiveCard) Implements [Interface].IIncentiveCard.SupplierINCTVIncentiveRestAmountExecution
        Try
            Dim DA As New SQLEngine.SalesMarketing.IncentiveCard
            DA.SupplierINCTVIncentiveRestAmountExecution(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub SupplierINCTVEmployeeIncentiveRestAmountExecution(ByVal CustomClass As Parameter.IncentiveCard) Implements [Interface].IIncentiveCard.SupplierINCTVEmployeeIncentiveRestAmountExecution
        Try
            Dim DA As New SQLEngine.SalesMarketing.IncentiveCard
            DA.SupplierINCTVEmployeeIncentiveRestAmountExecution(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub SupplierINCTVDailyDEdit(ByVal CustomClass As Parameter.IncentiveCard) Implements [Interface].IIncentiveCard.SupplierINCTVDailyDEdit
        Try
            Dim DA As New SQLEngine.SalesMarketing.IncentiveCard
            DA.SupplierINCTVDailyDEdit(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
End Class
