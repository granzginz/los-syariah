
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class CreditScore : Inherits ComponentBase
    Implements ICredit

    Public Function CreditSaveAdd(ByVal customClass As Parameter.Credit) As String Implements [Interface].ICredit.CreditSaveAdd
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.Credit
            Return CreditDA.CreditSaveAdd(customClass)
            'Throw busEx
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

    Public Function CreditDelete(ByVal customClass As Parameter.Credit) As String Implements [Interface].ICredit.CreditDelete
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.Credit
            Return CreditDA.CreditDelete(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Sub CreditSaveEdit(ByVal customClass As Parameter.Credit) Implements [Interface].ICredit.CreditSaveEdit
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.Credit
            CreditDA.CreditSaveEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            'Throw New Exception("Error when saving Update AssetType", ex)
        End Try
    End Sub

    Public Function GetCredit(ByVal customClass As Parameter.Credit) As Parameter.Credit Implements [Interface].ICredit.GetCredit
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.Credit
            Return CreditDA.GetCredit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetCreditReport(ByVal customClass As Parameter.Credit) As Parameter.Credit Implements [Interface].ICredit.GetCreditReport
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.Credit
            Return CreditDA.GetCreditReport(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function


    Public Function CreditEdit(ByVal customClass As Parameter.Credit) As Parameter.Credit Implements [Interface].ICredit.CreditEdit
        Try
            Dim CreditDA As New SQLEngine.SalesMarketing.Credit
            Return CreditDA.CreditEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
End Class


