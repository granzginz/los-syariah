﻿Imports Maxiloan.Interface
Public Class cbseAcuanBungaIntern : Inherits ComponentBase
    Implements IAcuanBungaIntern


    Public Function Delete(oCustome As Parameter.PAcuanBungaIntern) As String Implements [Interface].IAcuanBungaIntern.Delete
        Dim sql As New SQLEngine.SalesMarketing.QAcuanBungaIntern
        Return sql.Delete(oCustome)
    End Function
    Public Function SaveUpdate(oCustome As Parameter.PAcuanBungaIntern) As String Implements [Interface].IAcuanBungaIntern.SaveUpdate
        Dim sql As New SQLEngine.SalesMarketing.QAcuanBungaIntern
        Return sql.SaveUpdate(oCustome)
    End Function

    Public Function GetListing(oCustom As Parameter.PAcuanBungaIntern) As Parameter.PAcuanBungaIntern Implements [Interface].IAcuanBungaIntern.GetListing
        Dim sql As New SQLEngine.SalesMarketing.QAcuanBungaIntern
        Return sql.GetListing(oCustom)
    End Function

    Public Function GetListingUCL(ByVal oCustom As Parameter.PAcuanBungaIntern, Tipe As String) As System.Data.DataTable Implements [Interface].IAcuanBungaIntern.GetListingUCL
        Dim sql As New SQLEngine.SalesMarketing.QAcuanBungaIntern
        Return sql.GetListingUCL(oCustom, Tipe)
    End Function

    Public Function UpdateAssetAcuan(oCustome As Parameter.PAcuanBungaIntern, tipe As String) As String Implements [Interface].IAcuanBungaIntern.UpdateAssetAcuan
        Dim sql As New SQLEngine.SalesMarketing.QAcuanBungaIntern
        Return sql.UpdateAssetAcuan(oCustome, tipe)
    End Function

End Class
