


#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class Product : Inherits ComponentBase
    Implements IProduct

    Public Function LoadKegiatanUsaha(cnn As String) As IList(Of Parameter.KegiatanUsaha) Implements [Interface].IProduct.LoadKegiatanUsaha
        Dim ProductDA As New SQLEngine.SalesMarketing.Product
        Return ProductDA.LoadKegiatanUsaha(cnn)
    End Function
    Public Function ProductSaveAdd(ByVal customClass As Parameter.Product) As String Implements [Interface].IProduct.ProductSaveAdd
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductSaveAdd(customClass)
            'Throw busEx
        Catch ex As Exception
            Throw New Exception(ex.Message)
            'Throw ex
        End Try
    End Function

    Public Sub ProductSaveEdit(ByVal customClass As Parameter.Product) Implements [Interface].IProduct.ProductSaveEdit
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            ProductDA.ProductSaveEdit(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            'Throw New Exception("Error when saving Update AssetType", ex)
        End Try
    End Sub

    Public Function ProductReport(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductReport
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductReport(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function ProductPaging(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductPaging
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductPaging(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function ProductEdit(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductEdit
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductEdit(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function ProductView(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductView
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductView(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function Get_Combo_AssetType(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.Get_Combo_AssetType
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.Get_Combo_AssetType(customClass)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function Get_Combo_ScoreSchemeMaster(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.Get_Combo_ScoreSchemeMaster
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.Get_Combo_ScoreSchemeMaster(customClass)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function Get_Combo_CreditScoreSchemeMaster(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.Get_Combo_CreditScoreSchemeMaster
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.Get_Combo_CreditScoreSchemeMaster(customClass)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function Get_Combo_JournalScheme(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.Get_Combo_JournalScheme
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.Get_Combo_JournalScheme(customClass)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function Get_Combo_ApprovalTypeScheme(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.Get_Combo_ApprovalTypeScheme
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.Get_Combo_ApprovalTypeScheme(customClass)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function Get_Combo_Term_n_Condition(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.Get_Combo_Term_n_Condition
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.Get_Combo_Term_n_Condition(customClass)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function Get_Combo_Term_n_Condition_Brc(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.Get_Combo_Term_n_Condition_Brc
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.Get_Combo_Term_n_Condition_Brc(customClass)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function ProductTCSaveAdd(ByVal customClass As Parameter.Product) As String Implements [Interface].IProduct.ProductTCSaveAdd
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductTCSaveAdd(customClass)
            'Throw busEx
        Catch ex As Exception
            Throw New Exception(ex.Message)
            'Throw ex
        End Try
    End Function

    Public Sub ProductTCSaveEdit(ByVal customClass As Parameter.Product) Implements [Interface].IProduct.ProductTCSaveEdit
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            ProductDA.ProductTCSaveEdit(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            'Throw New Exception("Error when saving Update AssetType", ex)
        End Try
    End Sub

    Public Function ProductTCReport(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductTCReport
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductTCReport(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function ProductTCPaging(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductTCPaging
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductTCPaging(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function ProductTCEdit(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductTCEdit
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductTCEdit(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function ProductTCDelete(ByVal customClass As Parameter.Product) As String Implements [Interface].IProduct.ProductTCDelete
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductTCDelete(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function ProductBranchHOSaveAdd(ByVal customClass As Parameter.Product) As String Implements [Interface].IProduct.ProductBranchHOSaveAdd
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductBranchHOSaveAdd(customClass)
            'Throw busEx
        Catch ex As Exception
            Throw New Exception(ex.Message)
            'Throw ex
        End Try
    End Function

    Public Sub ProductBranchHOSaveEdit(ByVal customClass As Parameter.Product) Implements [Interface].IProduct.ProductBranchHOSaveEdit
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            ProductDA.ProductBranchHOSaveEdit(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            'Throw New Exception("Error when saving Update AssetType", ex)
        End Try
    End Sub

    Public Sub CopyProduct(ByVal customClass As Parameter.CopyProduct) Implements [Interface].IProduct.CopyProduct
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            ProductDA.CopyProduct(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Function ProductBranchHOReport(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductBranchHOReport
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductBranchHOReport(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function ProductBranchHOPaging(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductBranchHOPaging
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductBranchHOPaging(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function ProductBranchHOEdit(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductBranchHOEdit
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductBranchHOEdit(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetBranch(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.GetBranch
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.GetBranch(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetBranchAll(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.GetBranchAll
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.GetBranchAll(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub ProductBranchSaveEdit(ByVal customClass As Parameter.Product) Implements [Interface].IProduct.ProductBranchSaveEdit
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            ProductDA.ProductBranchSaveEdit(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            'Throw New Exception("Error when saving Update AssetType", ex)
        End Try
    End Sub

    Public Function ProductBranchReport(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductBranchReport
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductBranchReport(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function ProductBranchPaging(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductBranchPaging
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductBranchPaging(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function ProductBranchEdit(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductBranchEdit
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductBranchEdit(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function



    Public Function ProductBranchTCSaveAdd(ByVal customClass As Parameter.Product) As String Implements [Interface].IProduct.ProductBranchTCSaveAdd
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductBranchTCSaveAdd(customClass)
            'Throw busEx
        Catch ex As Exception
            Throw New Exception(ex.Message)
            'Throw ex
        End Try
    End Function

    Public Sub ProductBranchTCSaveEdit(ByVal customClass As Parameter.Product) Implements [Interface].IProduct.ProductBranchTCSaveEdit
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            ProductDA.ProductBranchTCSaveEdit(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            'Throw New Exception("Error when saving Update AssetType", ex)
        End Try
    End Sub

    Public Function ProductBranchTCReport(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductBranchTCReport
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductBranchTCReport(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function ProductBranchTCPaging(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductBranchTCPaging
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductBranchTCPaging(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function ProductBranchTCEdit(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductBranchTCEdit
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductBranchTCEdit(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function ProductBranchTCDelete(ByVal customClass As Parameter.Product) As String Implements [Interface].IProduct.ProductBranchTCDelete
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductBranchTCDelete(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function ProductOfferingSaveAdd(ByVal customClass As Parameter.Product) As String Implements [Interface].IProduct.ProductOfferingSaveAdd
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductOfferingSaveAdd(customClass)
            'Throw busEx
        Catch ex As Exception
            Throw New Exception(ex.Message)
            'Throw ex
        End Try
    End Function

    Public Sub ProductOfferingSaveEdit(ByVal customClass As Parameter.Product) Implements [Interface].IProduct.ProductOfferingSaveEdit
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            ProductDA.ProductOfferingSaveEdit(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            'Throw New Exception("Error when saving Update AssetType", ex)
        End Try
    End Sub

    Public Function ProductOfferingReport(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductOfferingReport
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductOfferingReport(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function ProductOfferingPaging(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductOfferingPaging
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductOfferingPaging(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function ProductOfferingEdit(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductOfferingEdit
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductOfferingEdit(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function ProductOfferingAdd(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductOfferingAdd
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductOfferingAdd(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function ProductOfferingView(ByVal customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductOfferingView
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductOfferingView(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function ProductOfferingDelete(ByVal customClass As Parameter.Product) As String Implements [Interface].IProduct.ProductOfferingDelete
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductOfferingDelete(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function ProductByKUJP(customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.ProductByKUJP
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.ProductByKUJP(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function DropDownListProduct(customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.DropDownListProduct
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.DropDownListProduct(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetCboSkePemb(customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.GetCboSkePemb
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.GetCboSkePemb(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetCboJenPemb(customClass As Parameter.Product) As Parameter.Product Implements [Interface].IProduct.GetCboJenPemb
        Try
            Dim ProductDA As New SQLEngine.SalesMarketing.Product
            Return ProductDA.GetCboJenPemb(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
