
Public Class AODirectSales
    Implements [Interface].IAODirectSales


    Public Function AOListing(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales Implements [Interface].IAODirectSales.AOListing
        Dim DAAOListing As New SQLEngine.SalesMarketing.AODirectSales
        Return DAAOListing.AOListing(customclass)
    End Function

    Public Function AOBudgetListing(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales Implements [Interface].IAODirectSales.AOBudgetListing
        Dim DAAOBudgetListing As New SQLEngine.SalesMarketing.AODirectSales
        Return DAAOBudgetListing.AOBudgetListing(customclass)
    End Function

    Public Sub AOBudgetAdd(ByVal customclass As Parameter.AODirectSales) Implements [Interface].IAODirectSales.AOBudgetAdd
        Dim DAAOBudgetAdd As New SQLEngine.SalesMarketing.AODirectSales
        DAAOBudgetAdd.AOBudgetAdd(customclass)
    End Sub

    Public Sub AOBudgetEdit(ByVal customclass As Parameter.AODirectSales) Implements [Interface].IAODirectSales.AOBudgetEdit
        Dim DAAOBudgetEdit As New SQLEngine.SalesMarketing.AODirectSales
        DAAOBudgetEdit.AOBudgetEdit(customclass)
    End Sub

    Public Function AOBudgetView(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales Implements [Interface].IAODirectSales.AOBudgetView
        Dim DAAOBudgetView As New SQLEngine.SalesMarketing.AODirectSales
        Return DAAOBudgetView.AOBudgetView(customclass)
    End Function

    Public Sub AOForecastAdd(ByVal customclass As Parameter.AODirectSales) Implements [Interface].IAODirectSales.AOForecastAdd
        Dim DAAOForecastAdd As New SQLEngine.SalesMarketing.AODirectSales
        DAAOForecastAdd.AOForecastAdd(customclass)
    End Sub

    Public Sub AOForecastEdit(ByVal customclass As Parameter.AODirectSales) Implements [Interface].IAODirectSales.AOForecastEdit
        Dim DAAOForecastEdit As New SQLEngine.SalesMarketing.AODirectSales
        DAAOForecastEdit.AOForecastEdit(customclass)
    End Sub
    Public Sub AOBudgetSaveEdit(ByVal customclass As Parameter.AODirectSales) Implements [Interface].IAODirectSales.AOBudgetSaveEdit
        Dim DAAOBudgetSaveEdit As New SQLEngine.SalesMarketing.AODirectSales
        DAAOBudgetSaveEdit.AOBudgetSaveEdit(customclass)
    End Sub

    Public Function AOForecastListing(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales Implements [Interface].IAODirectSales.AOForecastListing
        Dim DAAOForecastListing As New SQLEngine.SalesMarketing.AODirectSales
        Return DAAOForecastListing.AOForecastListing(customclass)
    End Function

    Public Function AOForecastView(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales Implements [Interface].IAODirectSales.AOForecastView
        Dim DAAOForecastView As New SQLEngine.SalesMarketing.AODirectSales
        Return DAAOForecastView.AOForecastView(customclass)
    End Function

    Public Function AOBudgetPrint(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales Implements [Interface].IAODirectSales.AOBudgetPrint
        Dim DAAOBudgetPrint As New SQLEngine.SalesMarketing.AODirectSales
        Return DAAOBudgetPrint.AOBudgetPrint(customclass)
    End Function

    Public Function AOForecastPrint(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales Implements [Interface].IAODirectSales.AOForecastPrint
        Dim DAAOForecastPrint As New SQLEngine.SalesMarketing.AODirectSales
        Return DAAOForecastPrint.AOForecastPrint(customclass)
    End Function

    Public Function AOBudgetList(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales Implements [Interface].IAODirectSales.AOBudgetList
        Dim DAAOBudgetList As New SQLEngine.SalesMarketing.AODirectSales
        Return DAAOBudgetList.AOBudgetList(customclass)
    End Function
End Class
