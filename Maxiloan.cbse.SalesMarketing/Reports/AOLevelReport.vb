
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class AOLevelReport : Inherits ComponentBase
    Implements IAOLevel



    Public Function AOLevelEvaluation(ByVal customclass As Parameter.AOLevelReport) As Parameter.AOLevelReport Implements [Interface].IAOLevel.AOLevelEvaluation
        Dim DAAOLevel As New SQLEngine.SalesMarketing.AOLevelReport
        Return DAAOLevel.AOLevelEvaluation(customclass)
    End Function

    Public Function GetAO(ByVal customclass As Parameter.AOLevelReport) As System.Data.DataTable Implements [Interface].IAOLevel.GetAO
        Dim DAAOLevel As New SQLEngine.SalesMarketing.AOLevelReport
        Return DAAOLevel.GetAO(customclass)
    End Function

    Public Function GetAOSupervisor(ByVal customclass As Parameter.AOLevelReport) As System.Data.DataTable Implements [Interface].IAOLevel.GetAOSupervisor
        Dim DAAOLevel As New SQLEngine.SalesMarketing.AOLevelReport
        Return DAAOLevel.GetAOSupervisor(customclass)
    End Function
End Class
