
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class RptSales : Inherits ComponentBase
    Implements ISalesReport

    Public Function GetAO(ByVal customclass As Parameter.Sales) As DataTable Implements [Interface].ISalesReport.GetAO
        Dim DASalesByAO As New SQLEngine.SalesMarketing.RptSales
        Return DASalesByAO.GetAO(customclass)
    End Function

    Public Function GetAOSupervisor(ByVal customclass As Parameter.Sales) As DataTable Implements [Interface].ISalesReport.GetAOSupervisor
        Dim DASalesByAO As New SQLEngine.SalesMarketing.RptSales
        Return DASalesByAO.GetAOSupervisor(customclass)
    End Function
    Public Function GetArea(ByVal customclass As Parameter.Sales) As DataTable Implements [Interface].ISalesReport.GetArea
        Dim DAarea As New SQLEngine.SalesMarketing.RptSales
        Return DAarea.GetArea(customclass)
    End Function
    Public Function ViewRptDailySalesByAO(ByVal customclass As Parameter.Sales) As Parameter.Sales Implements [Interface].ISalesReport.ViewRptDailySalesByAO
        Dim DASalesByAO As New SQLEngine.SalesMarketing.RptSales
        Return DASalesByAO.ViewRptDailySalesByAO(customclass)
    End Function
    Public Function GetCA(ByVal customclass As Parameter.Sales) As DataTable Implements [Interface].ISalesReport.GetCA
        Dim DASalesByCA As New SQLEngine.SalesMarketing.RptSales
        Return DASalesByCA.GetCA(customclass)
    End Function

    Public Function GetCASupervisor(ByVal customclass As Parameter.Sales) As DataTable Implements [Interface].ISalesReport.GetCASupervisor
        Dim DASalesByCA As New SQLEngine.SalesMarketing.RptSales
        Return DASalesByCA.GetCASupervisor(customclass)
    End Function
    Public Function GetBrand(ByVal customclass As Parameter.Sales) As DataTable Implements [Interface].ISalesReport.GetBrand
        Dim DABrand As New SQLEngine.SalesMarketing.RptSales
        Return DABrand.GetBrand(customclass)
    End Function
    Public Function GetSupplier(ByVal customclass As Parameter.Sales) As DataTable Implements [Interface].ISalesReport.GetSupplier
        Dim DASupplier As New SQLEngine.SalesMarketing.RptSales
        Return DASupplier.GetSupplier(customclass)
    End Function
    Public Function GetPercentDP(ByVal customclass As Parameter.Sales) As DataTable Implements [Interface].ISalesReport.GetPercentDP
        Dim DAPercentDP As New SQLEngine.SalesMarketing.RptSales
        Return DAPercentDP.GetPercentDP(customclass)
    End Function
    Public Function GetPercentEffective(ByVal customclass As Parameter.Sales) As DataTable Implements [Interface].ISalesReport.GetPercentEffective
        Dim DAPercentEffective As New SQLEngine.SalesMarketing.RptSales
        Return DAPercentEffective.GetPercentEffective(customclass)
    End Function
    Public Function GetAmountFinance(ByVal customclass As Parameter.Sales) As DataTable Implements [Interface].ISalesReport.GetAmountFinance
        Dim DAAmountFinance As New SQLEngine.SalesMarketing.RptSales
        Return DAAmountFinance.GetAmountFinance(customclass)
    End Function
    Public Function GetInstallmentAmount(ByVal customclass As Parameter.Sales) As DataTable Implements [Interface].ISalesReport.GetInstallmentAmount
        Dim DAInstallmentAmount As New SQLEngine.SalesMarketing.RptSales
        Return DAInstallmentAmount.GetInstallmentAmount(customclass)
    End Function
    Public Function GetProduct(ByVal customclass As Parameter.Sales) As DataTable Implements [Interface].ISalesReport.GetProduct
        Dim DAProduct As New SQLEngine.SalesMarketing.RptSales
        Return DAProduct.GetProduct(customclass)
    End Function
    Public Function InqSalesPerPeriod(ByVal ocustomclass As Parameter.Sales) As Parameter.Sales Implements [Interface].ISalesReport.InqSalesPerPeriod
        Dim daSalesPerPeriod As New SQLEngine.SalesMarketing.RptSales
        Return daSalesPerPeriod.InqSalesPerPeriod(ocustomclass)
    End Function
    Public Function InqAgingStatus(ByVal ocustomclass As Parameter.Sales) As Parameter.Sales Implements [Interface].ISalesReport.InqAgingStatus
        Dim daAgingStatus As New SQLEngine.SalesMarketing.RptSales
        Return daAgingStatus.InqAgingStatus(ocustomclass)
    End Function
    Public Function GetProductWhere(ByVal customclass As Parameter.Sales) As DataTable Implements [Interface].ISalesReport.GetProductWhere
        Dim daAgingStatus As New SQLEngine.SalesMarketing.RptSales
        Return daAgingStatus.GetProductWhere(customclass)
    End Function
    Public Function GetProductBranch(ByVal customclass As Parameter.Sales) As DataTable Implements [Interface].ISalesReport.GetProductBranch
        Dim DAProduct As New SQLEngine.SalesMarketing.RptSales
        Return DAProduct.GetProductBranch(customclass)
    End Function
    Public Function GetAssetCode(ByVal customclass As Parameter.Sales) As DataTable Implements [Interface].ISalesReport.GetAssetCode
        Dim DAProduct As New SQLEngine.SalesMarketing.RptSales
        Return DAProduct.GetAssetCode(customclass)
    End Function

    Public Function DailySalesCMOCrosstab(ByVal customclass As Parameter.Sales) As Parameter.Sales Implements [Interface].ISalesReport.DailySalesCMOCrosstab
        Dim DASalesByAO As New SQLEngine.SalesMarketing.RptSales
        Return DASalesByAO.DailySalesCMOCrosstab(customclass)
    End Function

    Public Function DailySalesSupplierCrosstab(ByVal customclass As Parameter.Sales) As Parameter.Sales Implements [Interface].ISalesReport.DailySalesSupplierCrosstab
        Dim DASalesByAO As New SQLEngine.SalesMarketing.RptSales
        Return DASalesByAO.DailySalesSupplierCrosstab(customclass)
    End Function
End Class
