
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class StatementOfAO : Inherits ComponentBase
    Implements IStatementOfAO


    Public Function ReportView(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO Implements [Interface].IStatementOfAO.ReportView
        Dim DAAOStatement As New SQLEngine.SalesMarketing.StatementOfAO
        Return DAAOStatement.ReportView(customclass)
    End Function

    Public Function GetAO(ByVal customclass As Parameter.StatementOfAO) As System.Data.DataTable Implements [Interface].IStatementOfAO.GetAO
        Dim DAAOStatement As New SQLEngine.SalesMarketing.StatementOfAO
        Return DAAOStatement.GetAO(customclass)
    End Function

    Public Function SubReportApproved(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO Implements [Interface].IStatementOfAO.SubReportApproved
        Dim DAAOStatement As New SQLEngine.SalesMarketing.StatementOfAO
        Return DAAOStatement.SubReportApproved(customclass)
    End Function

    Public Function SubReportBPKB(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO Implements [Interface].IStatementOfAO.SubReportBPKB
        Dim DAAOStatement As New SQLEngine.SalesMarketing.StatementOfAO
        Return DAAOStatement.SubReportBPKB(customclass)
    End Function

    Public Function SubReportBucket(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO Implements [Interface].IStatementOfAO.SubReportBucket
        Dim DAAOStatement As New SQLEngine.SalesMarketing.StatementOfAO
        Return DAAOStatement.SubReportBucket(customclass)
    End Function

    Public Function SubReportFunding(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO Implements [Interface].IStatementOfAO.SubReportFunding
        Dim DAAOStatement As New SQLEngine.SalesMarketing.StatementOfAO
        Return DAAOStatement.SubReportFunding(customclass)
    End Function

    Public Function SubReportGrossYield(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO Implements [Interface].IStatementOfAO.SubReportGrossYield
        Dim DAAOStatement As New SQLEngine.SalesMarketing.StatementOfAO
        Return DAAOStatement.SubReportGrossYield(customclass)
    End Function

    Public Function SubReportReppossess(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO Implements [Interface].IStatementOfAO.SubReportReppossess
        Dim DAAOStatement As New SQLEngine.SalesMarketing.StatementOfAO
        Return DAAOStatement.SubReportReppossess(customclass)
    End Function

    Public Function MainReport(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO Implements [Interface].IStatementOfAO.MainReport
        Dim DAAOStatement As New SQLEngine.SalesMarketing.StatementOfAO
        Return DAAOStatement.MainReport(customclass)
    End Function

    Public Function SubReportBPKBOverdue(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO Implements [Interface].IStatementOfAO.SubReportBPKBOverdue
        Dim DAAOStatement As New SQLEngine.SalesMarketing.StatementOfAO
        Return DAAOStatement.SubReportBPKBOverdue(customclass)
    End Function

    Public Function SubReportBucketTotal(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO Implements [Interface].IStatementOfAO.SubReportBucketTotal
        Dim DAAOStatement As New SQLEngine.SalesMarketing.StatementOfAO
        Return DAAOStatement.SubReportBucketTotal(customclass)
    End Function

    Public Function MainReportBranch(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO Implements [Interface].IStatementOfAO.MainReportBranch
        Dim DAAOStatement As New SQLEngine.SalesMarketing.StatementOfAO
        Return DAAOStatement.MainReportBranch(customclass)
    End Function
End Class
