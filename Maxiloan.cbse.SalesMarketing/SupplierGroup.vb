
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region

Public Class SupplierGroup : Inherits ComponentBase
    Implements ISupplierGroup
    Dim oSQLE As New SQLEngine.SalesMarketing.SupplierGroup

    Public Function SupplierGroupDelete(ByVal oCustomClass As Parameter.SupplierGroup) As String Implements [Interface].ISupplierGroup.SupplierGroupDelete
        Return oSQLE.SupplierGroupDelete(oCustomClass)
    End Function

    Public Function SupplierGroupSaveAdd(ByVal oCustomClass As Parameter.SupplierGroup) As String Implements [Interface].ISupplierGroup.SupplierGroupSaveAdd
        Return oSQLE.SupplierGroupSaveAdd(oCustomClass)
    End Function

    Public Sub SupplierGroupSaveEdit(ByVal oCustomClass As Parameter.SupplierGroup) Implements [Interface].ISupplierGroup.SupplierGroupSaveEdit
        oSQLE.SupplierGroupSaveEdit(oCustomClass)
    End Sub

    Public Function GetSupplierGroup(ByVal oCustomClass As Parameter.SupplierGroup) As Parameter.SupplierGroup Implements [Interface].ISupplierGroup.GetSupplierGroup
        Return oSQLE.GetSupplierGroup(oCustomClass)
    End Function

    Public Function GetSupplierGroupList(ByVal oCustomClass As Parameter.SupplierGroup) As Parameter.SupplierGroup Implements [Interface].ISupplierGroup.GetSupplierGroupList
        Return oSQLE.GetSupplierGroupList(oCustomClass)
    End Function

    Public Function GetSupplierGroupReport(ByVal oCustomClass As Parameter.SupplierGroup) As Parameter.SupplierGroup Implements [Interface].ISupplierGroup.GetSupplierGroupReport
        Return oSQLE.GetSupplierGroupReport(oCustomClass)
    End Function

    Public Function GetJobPositionCombo(ByVal oCustomClass As Parameter.SupplierGroup) As System.Data.DataTable Implements [Interface].ISupplierGroup.GetJobPositionCombo
        Return oSQLE.GetJobPositionCombo(oCustomClass)
    End Function

    'Public Function DeleteSupplierGroup(customClass As Parameter.Supplier) As String Implements ISupplierGroup.DeleteSupplierGroup
    '    Throw New NotImplementedException()
    'End Function
End Class