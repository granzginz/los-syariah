
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class ProspectAssignAppraiser
    Implements IProspectAssignAppraiser

    Function GetProspect(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.GetProspect
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.GetProspect(oCustomClass)
    End Function
    Function ProspectSaveAdd(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.ProspectSaveAdd
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.ProspectSaveAdd(oCustomClass)
    End Function
    Function ProspectSaveAsset(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.ProspectSaveAsset
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.ProspectSaveAsset(oCustomClass)
    End Function
    Function ProspectSaveDemografi(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.ProspectSaveDemografi
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.ProspectSaveDemografi(oCustomClass)
    End Function
    Function ProspectSaveFinancial(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.ProspectSaveFinancial
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.ProspectSaveFinancial(oCustomClass)
    End Function
    Function GetViewProspect(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.GetViewProspect
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.GetViewProspect(oCustomClass)
    End Function
    Function GetCbo(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.GetCbo
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.GetCbo(oCustomClass)
    End Function
    Function GetCboGeneral(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.GetCboGeneral
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.GetCboGeneral(oCustomClass)
    End Function

    Function DoProceeded(ByVal cnn As String, prospectId As String) As Boolean Implements [Interface].IProspectAssignAppraiser.DoProceeded
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.DoProceeded(cnn, prospectId)
    End Function


    Public Function DispositionCreditRpt(cnn As String, whereCond As String(), tp As String) As System.Data.DataSet Implements [Interface].IProspectAssignAppraiser.DispositionCreditRpt
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.DispositionCreditRpt(cnn, whereCond, tp)
    End Function

    Public Function DoProspectCreaditScoring(cnn As String, branchId As String, prospectAppId As String, ScoringType As String, bnsDate As Date) As String Implements [Interface].IProspectAssignAppraiser.DoProspectCreaditScoring
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.DoProspectCreaditScoring(cnn, branchId, prospectAppId, ScoringType, bnsDate)
    End Function


    Public Function GetProspectScorePolicyResult(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.GetProspectScorePolicyResult
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.GetProspectScorePolicyResult(oCustomClass)
    End Function

    Public Function DoProspectDecision(cnn As String, prospectAppId As String, appr As Integer) Implements [Interface].IProspectAssignAppraiser.DoProspectDecision
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.DoProspectDecision(cnn, prospectAppId, appr)
    End Function

    Public Function MasterDataReviewRpt(cnn As String, whereCond As String(), tp As String) As System.Data.DataSet Implements [Interface].IProspectAssignAppraiser.MasterDataReviewRpt
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.MasterDataReviewRpt(cnn, whereCond, tp)
    End Function

    Function GetInitial(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.GetInitial
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.GetInitial(oCustomClass)
    End Function

    Function InitialSaveAdd(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.InitialSaveAdd
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.InitialSaveAdd(oCustomClass)
    End Function

    Function DoBIChecking(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.DoBIChecking
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.DoBIChecking(oCustomClass)
    End Function

    Function DataSurveySaveAdd(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.DataSurveySaveAdd
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.DataSurveySaveAdd(oCustomClass)
    End Function

    Function DataSurveySaveEdit(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.DataSurveySaveEdit
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.DataSurveySaveEdit(oCustomClass)
    End Function

    Function ProspectReturnUpdate(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As String Implements [Interface].IProspectAssignAppraiser.ProspectReturnUpdate
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.ProspectReturnUpdate(oCustomClass)
    End Function

    Function ProspectLogSave(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.ProspectLogSave
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.ProspectLogSave(oCustomClass)
    End Function

    Function GetInqProspect(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.GetInqProspect
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.GetInqProspect(oCustomClass)
    End Function

    Public Function DoProspectCreaditScoringProceed(cnn As String, branchId As String, prospectAppId As String, ScoringType As String, bnsDate As Date, proceed As Boolean) As String Implements [Interface].IProspectAssignAppraiser.DoProspectCreaditScoringProceed
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.DoProspectCreaditScoringProceed(cnn, branchId, prospectAppId, ScoringType, bnsDate, proceed)
    End Function

    Public Function GetApprovalprospect(ByVal oCustomClass As Parameter.ProspectAssignAppraiser, ByVal strApproval As String) Implements [Interface].IProspectAssignAppraiser.GetApprovalprospect
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.GetApprovalprospect(oCustomClass, strApproval)
    End Function


    Function SaveProspectAssignAppraisal(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.SaveProspectAssignAppraisal
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.SaveProspectAssignAppraisal(oCustomClass)
    End Function

    Function GetDataAppraisal(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser Implements [Interface].IProspectAssignAppraiser.GetDataAppraisal
        Dim DA As New SQLEngine.SalesMarketing.ProspectAssignAppraiser
        Return DA.GetDataAppraisal(oCustomClass)
    End Function

End Class

