
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.Parameter

#End Region

Public Class Supplier : Inherits ComponentBase
    Implements ISupplier

#Region "Supplier"
    Function GetSupplierReport(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierReport
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetSupplierReport(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetSupplier(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplier
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetSupplier(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetSupplierID(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierID
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetSupplierID(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SupplierSaveAdd(ByVal oSupplier As Parameter.Supplier,
     ByVal oAddress As Parameter.Address, ByVal oAddressNPWP As Parameter.Address) As Parameter.Supplier Implements [Interface].ISupplier.SupplierSaveAdd
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.SupplierSaveAdd(oSupplier, oAddress, oAddressNPWP)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetBranch(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetBranch
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetBranch(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Sub SupplierSaveBranch(ByVal oSupplier As Parameter.Supplier, ByVal oDataBranch As DataTable) Implements [Interface].ISupplier.SupplierSaveBranch
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            SupplierDA.SupplierSaveBranch(oSupplier, oDataBranch)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Sub
    Function DeleteSupplier(ByVal customClass As Parameter.Supplier) As String Implements [Interface].ISupplier.DeleteSupplier
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.DeleteSupplier(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SupplierEdit(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.SupplierEdit
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.SupplierEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Sub SupplierSaveEdit(ByVal oSupplier As Parameter.Supplier,
      ByVal oAddress As Parameter.Address, ByVal oAddressNPWP As Parameter.Address) Implements [Interface].ISupplier.SupplierSaveEdit
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            SupplierDA.SupplierSaveEdit(oSupplier, oAddress, oAddressNPWP)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Sub

    Public Function GetProduk(ByVal oCustomClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetProduk
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetProduk(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetNoPKS(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetNoPKS
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetNoPKS(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
#End Region
#Region "SupplierBranch"
    Function GetSupplierBranchAdd(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierBranchAdd
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetSupplierBranchAdd(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetSupplierBranch(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierBranch
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetSupplierBranch(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetSupplierBranchIncentiveCard(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierBranchIncentiveCard
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetSupplierBranchIncentiveCard(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetSupplierBranchEdit(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierBranchEdit
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetSupplierBranchEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Sub SupplierBranchSaveEdit(ByVal oSupplier As Parameter.Supplier) Implements [Interface].ISupplier.SupplierBranchSaveEdit
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            SupplierDA.SupplierBranchSaveEdit(oSupplier)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Sub
    Function GetSupplierBranchDelete(ByVal oSupplier As Parameter.Supplier) As String Implements [Interface].ISupplier.GetSupplierBranchDelete
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetSupplierBranchDelete(oSupplier)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
#End Region

#Region "SupplierBranchRefund"
    Sub SupplierBranchRefundHeaderSaveEdit(ByVal oSupplier As Parameter.Supplier) Implements [Interface].ISupplier.SupplierBranchRefundHeaderSaveEdit
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            SupplierDA.SupplierBranchRefundHeaderSaveEdit(oSupplier)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Sub

    Sub SupplierBranchRefundSaveEdit(ByVal oSupplier As Parameter.Supplier) Implements [Interface].ISupplier.SupplierBranchRefundSaveEdit
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            SupplierDA.SupplierBranchRefundSaveEdit(oSupplier)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Sub

#End Region
#Region "SupplierOwner"
    Function GetSupplierOwner(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierOwner
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetSupplierOwner(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetSupplierOwnerIDType(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierOwnerIDType
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetSupplierOwnerIDType(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SupplierOwnerSaveAdd(ByVal oSupplier As Parameter.Supplier,
     ByVal oAddress As Parameter.Address) As Parameter.Supplier Implements [Interface].ISupplier.SupplierOwnerSaveAdd
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.SupplierOwnerSaveAdd(oSupplier, oAddress)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetSupplierOwnerEdit(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierOwnerEdit
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetSupplierOwnerEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetSupplierOwnerDetele(ByVal customClass As Parameter.Supplier) As String Implements [Interface].ISupplier.GetSupplierOwnerDetele
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetSupplierOwnerDetele(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetSupplierOwnerView(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierOwnerView
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetSupplierOwnerView(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    'Function GetSupplierOwnerID(ByVal customClass As Parameter.Supplier) As String Implements [Interface].ISupplier.GetSupplierOwnerID
    '    Try
    '        Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
    '        Return SupplierDA.GetSupplierOwnerID(customClass)
    '    Catch ex As Exception
    '        'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
    '    End Try
    'End Function
#End Region
#Region "SupplierAccount"
    Function GetSupplierAccount(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierAccount
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetSupplierAccount(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SupplierAccountSave(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.SupplierAccountSave
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.SupplierAccountSave(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SupplierAccountDelete(ByVal customClass As Parameter.Supplier) As String Implements [Interface].ISupplier.SupplierAccountDelete
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.SupplierAccountDelete(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
#End Region
#Region "SupplierSignature"
    Function GetSupplierSignature(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierSignature
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetSupplierSignature(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Sub SupplierSignatureSaveAdd(ByVal customClass As Parameter.Supplier) Implements [Interface].ISupplier.SupplierSignatureSaveAdd
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            SupplierDA.SupplierSignatureSaveAdd(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Sub
    Sub SupplierSignatureSaveEdit(ByVal customClass As Parameter.Supplier) Implements [Interface].ISupplier.SupplierSignatureSaveEdit
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            SupplierDA.SupplierSignatureSaveEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Sub
    Function SupplierSignatureDelete(ByVal customClass As Parameter.Supplier) As String Implements [Interface].ISupplier.SupplierSignatureDelete
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.SupplierSignatureDelete(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SupplierSignaturePath(ByVal customClass As Parameter.Supplier) As String Implements [Interface].ISupplier.SupplierSignaturePath
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.SupplierSignaturePath(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SupplierSignatureGetSequenceNo(ByVal SupplierID As String, ByVal strConnection As String) As String Implements [Interface].ISupplier.SupplierSignatureGetSequenceNo
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.SupplierSignatureGetSequenceNo(SupplierID, strConnection)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
#End Region
#Region "SupplierEmployee"
    Function GetSupplierEmployee(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierEmployee
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.GetSupplierEmployee(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SupplierEmployeeSave(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.SupplierEmployeeSave
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.SupplierEmployeeSave(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function SupplierEmployeeDelete(ByVal customClass As Parameter.Supplier) As String Implements [Interface].ISupplier.SupplierEmployeeDelete
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.SupplierEmployeeDelete(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
#End Region
#Region "Supplier Budget"

    Public Function AddSupplierBudget(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.AddSupplierBudget
        Dim SupplierBU As New SQLEngine.SalesMarketing.Supplier
        Return SupplierBU.AddSupplierBudget(customClass)
    End Function
    Public Function EditSupplierBudget(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.EditSupplierBudget
        Try
            Dim EditSB As New SQLEngine.SalesMarketing.Supplier
            Return EditSB.EditSupplierBudget(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
#End Region

#Region "Supplier Forecast"
    Public Function AddSupplierForecast(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.AddSupplierForecast
        Dim SupplierF As New SQLEngine.SalesMarketing.Supplier
        Return SupplierF.AddSupplierForecast(customClass)
    End Function

    Public Function EditSupplierForecast(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.EditSupplierForecast
        Dim EditF As New SQLEngine.SalesMarketing.Supplier
        Return EditF.EditSupplierForecast(customClass)
    End Function
#End Region
#Region " SupplierView "
    Public Function SupplierView(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.SupplierView
        Try
            Dim SupplierF As New SQLEngine.SalesMarketing.Supplier
            Return SupplierF.SupplierView(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function SupplierViewBudgetForecast(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.SupplierViewBudgetForecast
        Try
            Dim SupplierF As New SQLEngine.SalesMarketing.Supplier
            Return SupplierF.SupplierViewBudgetForecast(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
#End Region
#Region "SupplierLevelStatus"
    Public Function SupplierLevelStatus(ByVal strconnection As String) As DataTable Implements [Interface].ISupplier.SupplierLevelStatus
        Try
            Dim SupplierF As New SQLEngine.SalesMarketing.Supplier
            Return SupplierF.SupplierLevelStatus(strconnection)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
#End Region


    Public Function GetSupplierAccountbySupplierID(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierAccountbySupplierID
        Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
        Return SupplierDA.GetSupplierAccountbySupplierID(customClass)
    End Function

    Public Function SupplierSaveAddPrivate(oSupplier As Parameter.Supplier, oAddress As Parameter.Address) As Parameter.Supplier Implements [Interface].ISupplier.SupplierSaveAddPrivate
        Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
        Return SupplierDA.SupplierSaveAddPrivate(oSupplier, oAddress)
    End Function

    Public Function GetSupplierEmployeeSPV(customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierEmployeeSPV
        Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
        Return SupplierDA.GetSupplierEmployeeSPV(customClass)
    End Function
    Public Function GetSupplierEmployeeSales(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierEmployeeSales
        Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
        Return SupplierDA.GetSupplierEmployeeSales(customClass)
    End Function

    Public Function SaveSupplierEmployeeRelasi(customClass As Parameter.Supplier, tblEmployee As System.Data.DataTable) As String Implements [Interface].ISupplier.SaveSupplierEmployeeRelasi
        Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
        Return SupplierDA.SaveSupplierEmployeeRelasi(customClass, tblEmployee)
    End Function

    Public Function GetSupplierEmployeeSPVBySales(customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierEmployeeSPVBySales
        Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
        Return SupplierDA.GetSupplierEmployeeSPVBySales(customClass)
    End Function

    Public Function GetSupplierEmployeePosition(customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierEmployeePosition
        Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
        Return SupplierDA.GetSupplierEmployeePosition(customClass)
    End Function

    Public Function GetSupplierBranchRefund(customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.GetSupplierBranchRefund
        Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
        Return SupplierDA.GetSupplierBranchRefund(customClass)
    End Function

    Function SupplierAccountSave2(ByVal customClass As Parameter.Supplier) As Parameter.Supplier Implements [Interface].ISupplier.SupplierAccountSave2
        Try
            Dim SupplierDA As New SQLEngine.SalesMarketing.Supplier
            Return SupplierDA.SupplierAccountSave2(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
End Class
