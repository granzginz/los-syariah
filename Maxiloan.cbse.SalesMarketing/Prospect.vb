
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class Prospect
    Implements IProspect

    Function GetProspect(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect Implements [Interface].IProspect.GetProspect
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.GetProspect(oCustomClass)
    End Function
    Function ProspectSaveAdd(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect Implements [Interface].IProspect.ProspectSaveAdd
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.ProspectSaveAdd(oCustomClass)
    End Function
    Function ProspectSaveAsset(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect Implements [Interface].IProspect.ProspectSaveAsset
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.ProspectSaveAsset(oCustomClass)
    End Function
    Function ProspectSaveDemografi(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect Implements [Interface].IProspect.ProspectSaveDemografi
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.ProspectSaveDemografi(oCustomClass)
    End Function
    Function ProspectSaveFinancial(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect Implements [Interface].IProspect.ProspectSaveFinancial
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.ProspectSaveFinancial(oCustomClass)
    End Function
    Function GetViewProspect(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect Implements [Interface].IProspect.GetViewProspect
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.GetViewProspect(oCustomClass)
    End Function
    Function GetCbo(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect Implements [Interface].IProspect.GetCbo
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.GetCbo(oCustomClass)
    End Function
    Function GetCboGeneral(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect Implements [Interface].IProspect.GetCboGeneral
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.GetCboGeneral(oCustomClass)
    End Function

    Function DoProceeded(ByVal cnn As String, prospectId As String) As Boolean Implements [Interface].IProspect.DoProceeded
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.DoProceeded(cnn, prospectId)
    End Function


    Public Function DispositionCreditRpt(cnn As String, whereCond As String(), tp As String) As System.Data.DataSet Implements [Interface].IProspect.DispositionCreditRpt
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.DispositionCreditRpt(cnn, whereCond, tp)
    End Function

    Public Function DoProspectCreaditScoring(cnn As String, branchId As String, prospectAppId As String, ScoringType As String, bnsDate As Date) As String Implements [Interface].IProspect.DoProspectCreaditScoring
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.DoProspectCreaditScoring(cnn, branchId, prospectAppId, ScoringType, bnsDate)
    End Function


    Public Function GetProspectScorePolicyResult(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect Implements [Interface].IProspect.GetProspectScorePolicyResult
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.GetProspectScorePolicyResult(oCustomClass)
    End Function

    Public Function DoProspectDecision(cnn As String, prospectAppId As String, appr As Integer) Implements [Interface].IProspect.DoProspectDecision
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.DoProspectDecision(cnn, prospectAppId, appr)
    End Function

    Public Function MasterDataReviewRpt(cnn As String, whereCond As String(), tp As String) As System.Data.DataSet Implements [Interface].IProspect.MasterDataReviewRpt
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.MasterDataReviewRpt(cnn, whereCond, tp)
    End Function

    Function GetInitial(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect Implements [Interface].IProspect.GetInitial
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.GetInitial(oCustomClass)
    End Function

    Function InitialSaveAdd(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect Implements [Interface].IProspect.InitialSaveAdd
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.InitialSaveAdd(oCustomClass)
    End Function

    Function DoBIChecking(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect Implements [Interface].IProspect.DoBIChecking
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.DoBIChecking(oCustomClass)
    End Function

    Function DataSurveySaveAdd(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect Implements [Interface].IProspect.DataSurveySaveAdd
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.DataSurveySaveAdd(oCustomClass)
    End Function

    Function DataSurveySaveEdit(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect Implements [Interface].IProspect.DataSurveySaveEdit
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.DataSurveySaveEdit(oCustomClass)
    End Function

    Function ProspectReturnUpdate(ByVal oCustomClass As Parameter.Prospect) As String Implements [Interface].IProspect.ProspectReturnUpdate
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.ProspectReturnUpdate(oCustomClass)
    End Function

    Function ProspectLogSave(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect Implements [Interface].IProspect.ProspectLogSave
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.ProspectLogSave(oCustomClass)
    End Function

    Function GetInqProspect(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect Implements [Interface].IProspect.GetInqProspect
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.GetInqProspect(oCustomClass)
    End Function

    Public Function DoProspectCreaditScoringProceed(cnn As String, branchId As String, prospectAppId As String, ScoringType As String, bnsDate As Date, proceed As Boolean) As String Implements [Interface].IProspect.DoProspectCreaditScoringProceed
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.DoProspectCreaditScoringProceed(cnn, branchId, prospectAppId, ScoringType, bnsDate, proceed)
    End Function

    Public Function GetApprovalprospect(ByVal oCustomClass As Parameter.Prospect, ByVal strApproval As String) Implements [Interface].IProspect.GetApprovalprospect
        Dim DA As New SQLEngine.SalesMarketing.Prospect
        Return DA.GetApprovalprospect(oCustomClass, strApproval)
    End Function

End Class

