﻿
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
Imports Maxiloan.SQLEngine.PembiayaanMitra

Public Class LendingProcess : Inherits ComponentBase
    Implements ILendingProcess

    'Implements ILendingProcess

    Private ReadOnly _da As New LendingProcessDataAccess

    Public Function ProcessUploadCsvLending(cnn As String, data As Dictionary(Of String, IList(Of AbsDisb))) As String Implements ILendingProcess.ProcessUploadCsvLending
        Return _da.ProcessUploadCsvLending(cnn, data)
    End Function
End Class

