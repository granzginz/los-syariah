﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class JFCriteria : Inherits ComponentBase
    Implements IJFCriteria

    Public Sub JFCriteriaSaveEdit(ocustomClass As Parameter.JFCriteria) Implements IJFCriteria.JFCriteriaSaveEdit
        Try
            Dim JFCriteriaDA As New SQLEngine.PembiayaanMitra.JFCriteria
        Catch ex As Exception

        End Try
        'Throw New NotImplementedException()
    End Sub

    Public Function GetJFCriteria(ocustomClass As Parameter.JFCriteria) As Parameter.JFCriteria Implements IJFCriteria.GetJFCriteria
        Try
            Dim JFCriteriaDA As New SQLEngine.PembiayaanMitra.JFCriteria
            Return JFCriteriaDA.GetJFCriteria(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetJFCriteriaEdit(ocustomClass As Parameter.JFCriteria) As Parameter.JFCriteria Implements IJFCriteria.GetJFCriteriaEdit
        Try
            Dim JFCriteriaDA As New SQLEngine.PembiayaanMitra.JFCriteria
            Return JFCriteriaDA.GetJFCriteriaEdit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSelectJFCriteria(ocustomClass As Parameter.JFCriteria) As Parameter.JFCriteria Implements IJFCriteria.GetSelectJFCriteria
        Try
            Dim JFCriteriaDA As New SQLEngine.PembiayaanMitra.JFCriteria
            Return JFCriteriaDA.GetSelectJFCriteria(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function JFCriteriaDelete(ocustomClass As Parameter.JFCriteria) As String Implements IJFCriteria.JFCriteriaDelete
        Try
            Dim JFCriteriaDA As New SQLEngine.PembiayaanMitra.JFCriteria
            Return JFCriteriaDA.JFCriteriaDelete(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function JFCriteriaSaveAdd(ocustomClass As Parameter.JFCriteria) As String Implements IJFCriteria.JFCriteriaSaveAdd
        Try
            Dim JFCriteriaDA As New SQLEngine.PembiayaanMitra.JFCriteria
            Return JFCriteriaDA.JFCriteriaSaveAdd(ocustomClass)
        Catch ex As Exception
            'Throw New Exception("Error ", ex)
            Return ex.Message
        End Try
        'Throw New NotImplementedException()
    End Function
End Class
