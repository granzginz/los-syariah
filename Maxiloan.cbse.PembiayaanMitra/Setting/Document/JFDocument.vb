﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region

Public Class JFDocument : Inherits ComponentBase
    Implements IJFDocument

    Public Sub JFDocumentSaveEdit(ocustomClass As Parameter.JFDocument) Implements IJFDocument.JFDocumentSaveEdit
        Try
            Dim JFDocumentDA As New SQLEngine.PembiayaanMitra.JFDocument
        Catch ex As Exception

        End Try
        'Throw New NotImplementedException()
    End Sub

    Public Function GetJFDocument(ocustomClass As Parameter.JFDocument) As Parameter.JFDocument Implements IJFDocument.GetJFDocument
        Try
            Dim JFDocumentDA As New SQLEngine.PembiayaanMitra.JFDocument
            Return JFDocumentDA.GetJFDocument(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetJFDocumentEdit(ocustomClass As Parameter.JFDocument) As Parameter.JFDocument Implements IJFDocument.GetJFDocumentEdit
        Try
            Dim JFDocumentDA As New SQLEngine.PembiayaanMitra.JFDocument
            Return JFDocumentDA.GetJFDocumentEdit(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetSelectJFDocument(ocustomClass As Parameter.JFDocument) As Parameter.JFDocument Implements IJFDocument.GetSelectJFDocument
        Try
            Dim JFDocumentDA As New SQLEngine.PembiayaanMitra.JFDocument
            Return JFDocumentDA.GetSelectJFDocument(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function JFDocumentDelete(ocustomClass As Parameter.JFDocument) As String Implements IJFDocument.JFDocumentDelete
        Try
            Dim JFDocumentDA As New SQLEngine.PembiayaanMitra.JFDocument
            Return JFDocumentDA.JFDocumentDelete(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function JFDocumentSaveAdd(ocustomClass As Parameter.JFDocument) As String Implements IJFDocument.JFDocumentSaveAdd
        Try
            Dim JFDocumentDA As New SQLEngine.PembiayaanMitra.JFDocument
            Return JFDocumentDA.JFDocumentSaveAdd(ocustomClass)
        Catch ex As Exception
            'Throw New Exception("Error ", ex)
            Return ex.Message
        End Try
        'Throw New NotImplementedException()
    End Function
End Class
