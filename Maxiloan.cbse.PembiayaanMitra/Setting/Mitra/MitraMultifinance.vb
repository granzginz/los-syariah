﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
#End Region

Public Class MitraMultifinance : Inherits ComponentBase
    Implements IMitraMultifinance

    Public Function MitraList(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance Implements [Interface].IMitraMultifinance.MitraList
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.MitraList(co)
    End Function

    Public Function MitraAdd(ByVal co As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address) As String Implements [Interface].IMitraMultifinance.MitraAdd
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
            DA.MitraAdd(co, oClassAddress)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub MitraDelete(ByVal co As Parameter.MitraMultifinance) Implements [Interface].IMitraMultifinance.MitraDelete
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
            DA.MitraDelete(co)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Sub MitraEdit(ByVal co As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address) Implements [Interface].IMitraMultifinance.MitraEdit
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
            DA.MitraEdit(co, oClassAddress)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Function MitraListByID(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance Implements [Interface].IMitraMultifinance.MitraListByID
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.ListMitraByID(co)
    End Function

    Public Function MitraReport(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance Implements [Interface].IMitraMultifinance.MitraReport
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.ListMitraByID(co)
    End Function

#Region "Rekening Mitra"
    Public Function RekeningMitraList(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance Implements [Interface].IMitraMultifinance.RekeningMitraList
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.RekeningMitraList(co)
    End Function
    Public Function RekeningMitraAdd(ByVal co As Parameter.MitraMultifinance) As String Implements [Interface].IMitraMultifinance.RekeningMitraAdd
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
            DA.RekeningMitraAdd(co)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub RekeningMitraDelete(ByVal co As Parameter.MitraMultifinance) Implements [Interface].IMitraMultifinance.RekeningMitraDelete
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
            DA.RekeningMitraDelete(co)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Sub RekeningMitraEdit(ByVal co As Parameter.MitraMultifinance) Implements [Interface].IMitraMultifinance.RekeningMitraEdit
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
            DA.RekeningMitraEdit(co)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub
    Public Function RekeningMitraListByID(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance Implements [Interface].IMitraMultifinance.RekeningMitraListByID
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.RekeningMitraListByID(co)
    End Function
#End Region
End Class
