﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
#End Region

Public Class JFReference : Inherits ComponentBase
    Implements IJFReference

    Public Function JFReferenceList(ByVal co As Parameter.JFReference) As Parameter.JFReference Implements [Interface].IJFReference.JFReferenceList
        Dim DA As New SQLEngine.PembiayaanMitra.JFReference
        Return DA.JFReferenceList(co)
    End Function

    Public Function JFReferenceAdd(ByVal co As Parameter.JFReference) As String Implements [Interface].IJFReference.JFReferenceAdd
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.JFReference
            DA.JFReferenceAdd(co)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub JFReferenceDelete(ByVal co As Parameter.JFReference) Implements [Interface].IJFReference.JFReferenceDelete
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.JFReference
            DA.JFReferenceDelete(co)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Sub JFReferenceEdit(ByVal co As Parameter.JFReference) Implements [Interface].IJFReference.JFReferenceEdit
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.JFReference
            DA.JFReferenceEdit(co)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub
    Public Function JFReferenceListByID(ByVal co As Parameter.JFReference) As Parameter.JFReference Implements IJFReference.JFReferenceListByID
        Dim DA As New SQLEngine.PembiayaanMitra.JFReference
        Return DA.JFReferenceListByID(co)
    End Function
End Class
