﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region

Public Class JFProduct : Inherits ComponentBase
    Implements IJFProduct

    Public Sub JFProductSaveEdit(oCustomClass As Parameter.JFProduct) Implements [Interface].IJFProduct.JFProductSaveEdit
        Try
            Dim JFProductDA As New SQLEngine.PembiayaanMitra.JFProduct
        Catch ex As Exception

        End Try
        'Throw New NotImplementedException()
    End Sub

    Public Function GetJFProduct(oCustomClass As Parameter.JFProduct) As Parameter.JFProduct Implements [Interface].IJFProduct.GetJFProduct
        Try
            Dim JFProductDA As New SQLEngine.PembiayaanMitra.JFProduct
            Return JFProductDA.GetJFProduct(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        'Throw New NotImplementedException()
    End Function

    Public Function GetJFProductEdit(oCustomClass As Parameter.JFProduct) As Parameter.JFProduct Implements [Interface].IJFProduct.GetJFProductEdit
        Try
            Dim JFProductDA As New SQLEngine.PembiayaanMitra.JFProduct
            Return JFProductDA.GetJFProductEdit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
    End Function

    Public Function GetSelectJFProduct(ocustomClass As Parameter.JFProduct) As Parameter.JFProduct Implements IJFProduct.GetSelectJFProduct
        Try
            Dim JFProductDA As New SQLEngine.PembiayaanMitra.JFProduct
            Return JFProductDA.GetSelectJFProduct(ocustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function JFProductDelete(oCustomClass As Parameter.JFProduct) As String Implements [Interface].IJFProduct.JFProductDelete
        Try
            Dim JFProductDA As New SQLEngine.PembiayaanMitra.JFProduct
            Return JFProductDA.JFProductDelete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try

    End Function

    Public Function JFProductSaveAdd(oCustomClass As Parameter.JFProduct) As String Implements [Interface].IJFProduct.JFProductSaveAdd
        Try
            Dim JFProductDA As New SQLEngine.PembiayaanMitra.JFProduct
            Return JFProductDA.JFProductSaveAdd(oCustomClass)
        Catch ex As Exception
            'Throw New Exception("Error ", ex)
            Return ex.Message
        End Try
    End Function


End Class
