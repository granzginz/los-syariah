﻿
Imports System.IO
Imports System.Data
Imports Maxiloan.SQLEngine
Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Imports Maxiloan.SQLEngine.PembiayaanMitra

Public Class MitraMultifinance : Inherits ComponentBase
    Implements IMitraMultifinance

    Public Function MitraList(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance Implements IMitraMultifinance.MitraList
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.MitraList(co)
    End Function


    Function MitraAdd(ByVal co As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address) As String Implements IMitraMultifinance.MitraAdd
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
            DA.MitraAdd(co, oClassAddress)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub MitraDelete(ByVal co As Parameter.MitraMultifinance) Implements IMitraMultifinance.MitraDelete
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
            DA.MitraDelete(co)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Sub MitraEdit(ByVal co As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address) Implements IMitraMultifinance.MitraEdit
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
            DA.MitraEdit(co, oClassAddress)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Function MitraListByID(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance Implements IMitraMultifinance.MitraListByID
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.ListMitraByID(co)
    End Function

    Public Function MitraReport(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance Implements IMitraMultifinance.MitraReport
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.ListMitraByID(co)
    End Function

    Public Function DisbursementHeaderList(ByVal strConnection As String) As DataTable Implements IMitraMultifinance.DisbursementHeaderList
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.DisbursementHeaderList(strConnection)
    End Function

    Public Function LendingValidationList(ByVal strConnection As String) As List(Of AgreementToValidate) Implements IMitraMultifinance.LendingValidationList
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.LendingValidationList(strConnection)
    End Function
    Public Function DisburseAgreementApproveReject(ByVal oDisburseAgreement As DisburseAgreement) As Boolean Implements IMitraMultifinance.DisburseAgreementApproveReject
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.DisburseAgreementApproveReject(oDisburseAgreement)
    End Function

    Public Function DisburseHeaderUpdate(ByVal oDisburseAgreement As DisburseAgreement) As Boolean Implements IMitraMultifinance.DisburseHeaderUpdate
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.DisburseHeaderUpdate(oDisburseAgreement)
    End Function
    Public Function RekeningMitraList(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance Implements IMitraMultifinance.RekeningMitraList
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.RekeningMitraList(co)
    End Function
    Public Function RekeningMitraAdd(ByVal co As Parameter.MitraMultifinance) As String Implements IMitraMultifinance.RekeningMitraAdd
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
            DA.RekeningMitraAdd(co)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub RekeningMitraDelete(ByVal co As Parameter.MitraMultifinance) Implements IMitraMultifinance.RekeningMitraDelete
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
            DA.RekeningMitraDelete(co)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Sub RekeningMitraEdit(ByVal co As Parameter.MitraMultifinance) Implements IMitraMultifinance.RekeningMitraEdit
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
            DA.RekeningMitraEdit(co)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub
    Public Function RekeningMitraListByID(co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance Implements IMitraMultifinance.RekeningMitraListByID
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.RekeningMitraListByID(co)
    End Function

    Public Function uploadlist(ocls As Lending) As Lending Implements IMitraMultifinance.uploadlist
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.uploadlist(ocls)
    End Function

    Public Function lendingdrawdownrequest(customclass As Lending) As Lending Implements IMitraMultifinance.lendingdrawdownrequest
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.lendingdrawdownrequest(customclass)
    End Function
    Public Function lendingdrawdownapproval(customclass As Lending) As Lending Implements IMitraMultifinance.lendingdrawdownapproval
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.lendingdrawdownapproval(customclass)
    End Function
    Public Function disburselist(customclass As Lending) As Lending Implements IMitraMultifinance.disburselist
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.disburselist(customclass)
    End Function

    Public Function disburse(customclass As Lending) As Lending Implements IMitraMultifinance.disburse
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.disburse(customclass)
    End Function

    Public Function batchlist(customclass As Lending) As Lending Implements IMitraMultifinance.batchlist
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.batchlist(customclass)
    End Function

    Public Function LendingPaymentSave(ByVal oDisburseAgreement As Lending) As Lending Implements IMitraMultifinance.LendingPament
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.lendingpayment(oDisburseAgreement)
    End Function

    Public Function PaymentByDueDateListPaging(customclass As Lending) As Lending Implements IMitraMultifinance.PaymentByDueDateListPaging
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
            Return DA.PaymentByDueDateListPaging(customclass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetLendingFacilityByCode(ByVal mfCode As String, ByVal facilityNo As String, ByVal strConnection As String) As LendingFacility Implements IMitraMultifinance.GetLendingFacilityByCode
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.GetLendingFacilityByCode(mfCode, facilityNo, strConnection)
    End Function

    Public Function paymentCalculate(customclass As Parameter.PaymentCalculateProperty) As Parameter.PaymentCalculateProperty Implements [Interface].IMitraMultifinance.paymentCalculate
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.LendingPaymentCalculateSQLEngine
            Return DA.lendingPrepaymentCalculate(customclass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub paymentCalculateDML(customclass As PaymentCalculateProperty) Implements IMitraMultifinance.paymentCalculateDML
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.LendingPaymentCalculateSQLEngine
            DA.setLendingCalculate(customclass)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub lendingRateDML(dml As String, customclass As LendingRateProperty) Implements IMitraMultifinance.lendingRateDML
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.LendingRateSQLEngine
            DA.lendingRateDML(dml, customclass)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function lendingRateListPaging(customclass As LendingRateProperty) As LendingRateProperty Implements IMitraMultifinance.lendingRateListPaging
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.LendingRateSQLEngine
            Return DA.lendingRateListPaging(customclass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function lendingBatchListPaging(customclass As Lending) As Lending Implements IMitraMultifinance.lendingBatchListPaging
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.LendingRateSQLEngine
            Return DA.lendingBatchListPaging(customclass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DraftSoftcopy(ByVal customClass As Lending) As System.Data.DataSet Implements IMitraMultifinance.DraftSoftcopy
        Try
            Dim DA As New SQLEngine.PembiayaanMitra.LendingRateSQLEngine
            Return DA.DraftSoftcopy(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub lendingFacilityDML(dml As String, customclass As LendingFacility) Implements IMitraMultifinance.lendingFacilityDML
        Dim DA As New SQLEngine.PembiayaanMitra.LendingFacilitySQLEngine
        DA.lendingFacilityDML(dml, customclass)
    End Sub

    Public Function lendingpayment(customclass As Lending) As Lending Implements IMitraMultifinance.lendingpayment
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.lendingpayment(customclass)
    End Function

    Public Function lendinginstallmentinfo(customclass As Lending) As Lending Implements IMitraMultifinance.lendinginstallmentinfo
        Dim DA As New SQLEngine.PembiayaanMitra.MitraMultifinance
        Return DA.lendinginstallmentinfo(customclass)
    End Function
End Class
