﻿Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Framework.SQLEngine

Imports System.Linq
Public Class COA : Inherits DataAccessBase

    Const SP_SELECT As String = "GLMasterAccount_select"
    Const SP_INSERT As String = "GLMasterAccount_insert"
    Const SP_UPDATE As String = "GLMasterAccount_update"
    Const SP_DELETE As String = "GLMasterAccount_delete"

    Public Function AutoComplateCoaId(strCnn As String, coaid As String) As List(Of String)
        Dim query = "select top 10 coaid from GLMasterAcc E where isLeaf =1 and isactive=1 and coaid like @coaId order by coaid"
        Dim data As DataTable = SqlHelper.ExecuteDataset(strCnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@coaId", String.Format("{0}%", coaid.Trim()))}).Tables(0)
        Dim list As IList(Of String) = New List(Of String)
        For Each item As DataRow In data.Rows
            list.Add(item("coaid").ToString().Trim())
        Next

        Return list
    End Function
    Public Function AutoComplateCoaDesc(strCnn As String, Description As String) As List(Of String)
        Dim query = "select top 10 Description from GLMasterAcc E where isLeaf =1 and isactive=1 and Description like @Description order by coaid"
        Dim data As DataTable = SqlHelper.ExecuteDataset(strCnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@Description", String.Format("{0}%", Description.Trim()))}).Tables(0)
        Dim list As IList(Of String) = New List(Of String)
        For Each item As DataRow In data.Rows
            list.Add(item("Description").ToString().Trim())
        Next

        Return list
    End Function
    Public Function SelectData(ByRef totalrec As Integer, ByVal strCnn As String, ByVal fromLookup As Boolean, ByVal isLeaf As Boolean, ByVal ParamArray param As Object()) As IList(Of MasterAccountObject)
        Try

            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

            params.Add(New SqlParameter("@CompanyId", param(0)))
            params.Add(New SqlParameter("@BranchId", param(1)))
            params.Add(New SqlParameter("@currPage", CType(param(2), Integer)))
            params.Add(New SqlParameter("@pageSize", CType(param(3), Integer)))
            If (param.Length > 4) Then

                If (param(4) <> "0") Then
                    params.Add(New SqlParameter("@type", CType(param(4), Integer)))
                End If

                If (param(6) <> "") Then
                    params.Add(New SqlParameter("@wOpt", CType(param(5), Integer)))
                    params.Add(New SqlParameter("@where", param(6)))
                End If
            End If

            If (fromLookup) Then
                params.Add(New SqlParameter(IIf(isLeaf, "@isleaf", "@isEdit"), 1))
            End If

            Dim prm = New SqlParameter("@totalRecords", SqlDbType.Int)
            prm.Direction = ParameterDirection.Output
            params.Add(prm)
            totalrec = 0
            Dim prms As SqlParameter() = params.ToArray
            Dim data As DataTable = SqlHelper.ExecuteDataset(strCnn, CommandType.StoredProcedure, "GLMasterAccPage_Select", prms).Tables(0)

            If (data.Rows.Count > 0) Then
                totalrec = CType(data.Rows(0)("rows"), Int64)
            End If

            Dim list As IList(Of MasterAccountObject) = (From item As DataRow In data.Rows Select New MasterAccountObject(item("CoAId"), item("Description"), item("Type"), item("Currency"), item("FullyQualifiedName"), item("BranchId"), item("Level"), item("AddFreeNo"), item("AddFreeDesc"))).ToList()

            Return list
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    Public Function SelectDataIsLeaf(ByRef totalrec As Integer, ByVal strCnn As String, ByVal fromLookup As Boolean, ByVal isLeaf As Boolean, ByVal ParamArray param As Object()) As IList(Of MasterAccountObject)
        Try

            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

            params.Add(New SqlParameter("@CompanyId", param(0)))
            params.Add(New SqlParameter("@BranchId", param(1)))
            params.Add(New SqlParameter("@currPage", CType(param(2), Integer)))
            params.Add(New SqlParameter("@pageSize", CType(param(3), Integer)))
            If (param.Length > 4) Then

                If (param(4) <> "0") Then
                    params.Add(New SqlParameter("@type", CType(param(4), Integer)))
                End If

                If (param(6) <> "") Then
                    params.Add(New SqlParameter("@wOpt", CType(param(5), Integer)))
                    params.Add(New SqlParameter("@where", param(6)))
                End If
            End If

            If (fromLookup) Then
                params.Add(New SqlParameter(IIf(isLeaf, "@isleaf", "@isEdit"), 1))
            End If

            Dim prm = New SqlParameter("@totalRecords", SqlDbType.Int)
            prm.Direction = ParameterDirection.Output
            params.Add(prm)
            totalrec = 0
            Dim prms As SqlParameter() = params.ToArray
            Dim data As DataTable = SqlHelper.ExecuteDataset(strCnn, CommandType.StoredProcedure, "GLMasterAccPageIsLeaf_Select", prms).Tables(0)

            If (data.Rows.Count > 0) Then
                totalrec = CType(data.Rows(0)("rows"), Int64)
            End If

            Dim list As IList(Of MasterAccountObject) = (From item As DataRow In data.Rows Select New MasterAccountObject(item("CoAId"), item("Description"), item("Type"), item("Currency"), item("FullyQualifiedName"), item("BranchId"), item("Level"), item("AddFreeNo"), item("AddFreeDesc"))).ToList()

            Return list
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Sub DeleteData(ByVal strCnn As String, ByVal coaId As String, ByVal addfree As String)
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        params.Add(New SqlParameter("@GLAccountNo", SqlDbType.VarChar) With {.Value = coaId})
        'params(1) = New SqlParameter("@AddFreeNo", SqlDbType.VarChar)
        'params(1).Value = addfree

        SqlHelper.ExecuteNonQuery(strCnn, CommandType.StoredProcedure, SP_DELETE, params.ToArray)
    End Sub

    Private Function IsDuplicateCode(ByVal strCnn As String, ByVal coaId As String, ByVal branchId As String, ByVal CompanyId As String, ByVal addfree As String) As Boolean
        Dim query = "Select count(*) from GLMasterAcc where CoAId=@coaId and branchId=@branchId and CompanyId=@CompanyId  "
        Dim ret = Convert.ToInt32(SqlHelper.ExecuteScalar(strCnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@coaId", coaId), New SqlParameter("@branchId", branchId), New SqlParameter("@CompanyId", CompanyId)}))
        Return ret > 0
    End Function

    Public Sub InsertData(ByVal strCnn As String, ByVal data As MasterAccountObject)

        If (IsDuplicateCode(strCnn, data.CoaId, data.Branch, data.CompanyId, "")) Then
            Throw New DuplicateNameException("COA Code sudah Ada.")
        End If
         
        SqlHelper.ExecuteNonQuery(strCnn, CommandType.StoredProcedure, SP_INSERT, CreateParam(data, True))

    End Sub
    Public Sub UpdateData(ByVal strCnn As String, ByVal data As MasterAccountObject)
        SqlHelper.ExecuteNonQuery(strCnn, CommandType.StoredProcedure, SP_UPDATE, CreateParam(data, False))
    End Sub
     

    Private Function CreateParam(ByVal data As MasterAccountObject, ByVal create As Boolean) As SqlParameter()
        Dim param As SqlParameter 
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        param = New SqlParameter("@CompanyId", SqlDbType.VarChar)
        param.Value = data.CompanyId
        params.Add(param)

        param = New SqlParameter("@GLAccountNo", SqlDbType.VarChar)
        param.Value = data.CoaId
        params.Add(param)

        param = New SqlParameter("@GLAccountName", SqlDbType.VarChar)
        param.Value = data.Description
        params.Add(param)

        param = New SqlParameter("@CurrencyId", SqlDbType.VarChar)
        param.Value = data.Currency
        params.Add(param)
        param = New SqlParameter("@AccountType", SqlDbType.Int)
        param.Value = CInt(data.Type)
        params.Add(param)
        param = New SqlParameter("@ParentAccount", SqlDbType.VarChar)
        param.Value = data.ParentCoa
        params.Add(param)
        param = New SqlParameter("@BranchId", SqlDbType.VarChar)
        param.Value = data.Branch
        params.Add(param)
        param = New SqlParameter("@isActive", SqlDbType.Bit)
        param.Value = data.IsActive
        params.Add(param)
        If (create) Then
            param = New SqlParameter("@Createdby", SqlDbType.VarChar)
        Else
            param = New SqlParameter("@Updateby", SqlDbType.VarChar)
        End If 
        param.Value = IIf(create, data.UserCreate, data.UserUpdate)
        params.Add(param)

        param = New SqlParameter("@Func", SqlDbType.Int)
        param.Value = data.Function
        params.Add(param)

        param = New SqlParameter("@IsForeignCurr", SqlDbType.Bit)
        param.Value = data.IsForeignCurr
        params.Add(param)

        param = New SqlParameter("@LBU", SqlDbType.VarChar)
        param.Value = data.LBU
        params.Add(param)

        param = New SqlParameter("@SIPP", SqlDbType.VarChar)
        param.Value = data.SIPP
        params.Add(param)


        'param = New SqlParameter("@TaxAccountNo", SqlDbType.VarChar)
        'param.Value = data.CoaIdTax
        'params.Add(param)

        'param = New SqlParameter("@AddFreeNo", SqlDbType.VarChar)
        'param.Value = data.AddFreeNo
        'params.Add(param)

        'param = New SqlParameter("@AddFreeDesc", SqlDbType.VarChar)
        'param.Value = data.AddFreeDesc
        'params.Add(param)

        'param = New SqlParameter("@ParentAddFree", SqlDbType.VarChar)
        'param.Value = data.ParentAddFree
        'params.Add(param)

        'param = New SqlParameter("@TaxAccountNoAddFree", SqlDbType.VarChar)
        'param.Value = data.CoaIdTaxAddFree
        'params.Add(param)

        Return params.ToArray
    End Function

    Public Function FindById(ByVal strCnn As String, ByVal coaId As String, ByVal branchId As String, ByVal companyId As String) As MasterAccountObject
        Dim query = "select c.CoAId,  c.Description, c.Type,  c.Currency,c.BranchId,  c.Parent, c.Description,c.IsActive,c.lvl, c.func, '', isnull(c.Description,'') TaxDesc, '', '',ISNULL(LBU,'') AS LBU,ISNULL(SIPP,'') AS SIPP from dbo.GLMasterAcc c where c.coaid=@coaid "
        Dim paramchar = New Char() {" "}
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(strCnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@coaId", coaId.Trim())}) '  , New SqlParameter("@branchId", branchId.Trim()), New SqlParameter("@CompanyId", companyId.Trim())})


        If (reader.HasRows) Then
            reader.Read()
            Dim result As New MasterAccountObject(
                reader.GetString(0).Trim(),
                reader.GetString(1).Trim(),
                reader.GetInt32(2),
                reader.GetString(3).Trim(),
                "",
                reader.GetString(4).Trim(),
                reader.GetString(12).Trim(),
                reader.GetString(13).Trim(),
                reader.GetInt32(8),
                reader.GetInt32(9))
            result.ParentCoa = ""
            result.ParentDesc = ""
            result.CoaIdTax = reader.GetString(10).Trim()
            result.CoaTaxDesc = reader.GetString(11).Trim()

            If (Not reader.IsDBNull(5)) Then
                result.ParentCoa = reader.GetString(5)
                result.ParentDesc = reader.GetString(6).Trim()
            End If
            result.IsActive = reader.GetBoolean(7)
            result.LBU = reader.GetString(14).Trim()
            result.SIPP = reader.GetString(15).Trim()
            Return result
        End If
        Return Nothing
    End Function
    Public Function FindByDesc(ByVal strCnn As String, ByVal Description As String, ByVal branchId As String, ByVal companyId As String) As MasterAccountObject
        Dim query = "select       c.CoAId,  c.Description, c.Type,  c.Currency,c.BranchId,  c.Parent, c.Description,c.IsActive,c.lvl, c.func, '' coaidTax, isnull(c.Description,'') TaxDesc, '', '' from dbo.GLMasterAcc c where c.Description=@Description "
        Dim paramchar = New Char() {" "}
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(strCnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@Description", Description.Trim())})


        If (reader.HasRows) Then
            reader.Read()
            Dim result As New MasterAccountObject(
                reader.GetString(0).Trim(),
                reader.GetString(1).Trim(),
                reader.GetInt32(2),
                reader.GetString(3).Trim(),
                "",
                reader.GetString(4).Trim(),
                reader.GetString(12).Trim(),
                reader.GetString(13).Trim(),
                reader.GetInt32(8),
                reader.GetInt32(9))
            result.ParentCoa = ""
            result.ParentDesc = ""
            result.CoaIdTax = reader.GetString(10).Trim()
            result.CoaTaxDesc = reader.GetString(11).Trim()

            If (Not reader.IsDBNull(5)) Then
                result.ParentCoa = reader.GetString(5)
                result.ParentDesc = reader.GetString(6).Trim()
            End If
            result.IsActive = reader.GetBoolean(7)
            Return result
        End If
        Return Nothing
    End Function
End Class
