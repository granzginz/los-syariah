﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class DataJurnalCrossPeriode : Inherits Maxiloan.SQLEngine.DataAccessBase

    Public Function GetDataJurnal(ByVal oCustomClass As Parameter.DataJurnalCrossPeriode) As Parameter.DataJurnalCrossPeriode
        Dim oReturnValue As New Parameter.DataJurnalCrossPeriode
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@TglPeriode1", SqlDbType.Date)
        params(0).Value = oCustomClass.TglPeriode1
        params(1) = New SqlParameter("@TglPeriode2", SqlDbType.Date)
        params(1).Value = oCustomClass.TglPeriode2
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetDataJurnalCrossPeriode", params).Tables(0)
            'oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GL.DataJurnalCrossPeriode.GetDataJurnal")
        End Try
    End Function
    Public Function GetTr_Nomor(ByVal oCustomClass As Parameter.DataJurnalCrossPeriode) As Parameter.DataJurnalCrossPeriode
        Dim oReturnValue As New Parameter.DataJurnalCrossPeriode
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim params() As SqlParameter = New SqlParameter(0) {}

        If ErrMessage = "" Then
            Try
                If conn.State = ConnectionState.Closed Then conn.Open()
                transaction = conn.BeginTransaction

                params(0) = New SqlParameter("@dt", SqlDbType.Structured)
                params(0).Value = oCustomClass.ListData

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPindahTanggalJurnal", params)

                transaction.Commit()
                oReturnValue.Err = ErrMessage
                Return oReturnValue
            Catch exp As Exception
                transaction.Rollback()
                ErrMessage = exp.Message
                oReturnValue.Err = ErrMessage
                Throw New Exception(exp.Message)
                Return oReturnValue
            Finally
                If conn.State = ConnectionState.Open Then conn.Close()
                conn.Dispose()
            End Try
        Else
            Return oReturnValue
        End If

        'Dim params() As SqlParameter = New SqlParameter(3) {}
        'Dim oReturnValue As New Parameter.DataJurnalCrossPeriode
        'Dim transaction As SqlTransaction

        'params(0) = New SqlParameter("@TR_NOMOR", SqlDbType.VarChar)
        'params(0).Value = dtAdd.Rows.Item("TR_NOMOR")
        'params(1) = New SqlParameter("@TR_DATE", SqlDbType.Date)
        'params(1).Value = dtAdd.Rows.Item("TR_DATE")
        'params(2) = New SqlParameter("@PeriodMonth", SqlDbType.VarChar)
        'params(2).Value = dtAdd.Rows.Item("PeriodMonth")
        'params(3) = New SqlParameter("@PeriodYear", SqlDbType.VarChar)
        'params(3).Value = dtAdd.Rows.Item("PeriodYear")
        'Try
        '    'oReturnValue.ListData = SqlHelper.ExecuteDataset(dtAdd.strConnection, CommandType.StoredProcedure, "spUpdateJournalPeriodDate", params).Tables(0)
        '    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPindahTanggalJurnal", params)
        '    Return oReturnValue
        'Catch ex As Exception
        '    Throw New Exception("Error On DataAccess.GL.DataJurnalCrossPeriode.GetTr_Nomor")
        'End Try
    End Function
End Class
