﻿Imports Maxiloan.Parameter
Imports Maxiloan.Framework.SQLEngine
Imports System.Data.SqlClient

Public Class GlYearPeriodDataAccess : Inherits DataAccessBase


    Public Function IsYearValid(ByVal cnn As String, year As Integer, companyId As String, branchid As String) As Boolean
        Dim query = "select top 1 1 from GLYear d where  CompanyId=@company  and YearId=@year"
        Dim ret = Convert.ToInt32(SqlHelper.ExecuteScalar(cnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@company", companyId), New SqlParameter("@year", year)}))
        Return ret > 0
    End Function

    Public Function YearlyCloseValidate(ByVal cnn As String, companyId As String) As String
        Dim currYear = currentYear(cnn, companyId)
        Dim currMonth = currentMonth(cnn, companyId)
        If (currYear < 0) Then
            Throw New InvalidOperationException("Tahun Jurnal Tidak ditemukan.")
        End If
        Dim prds = YearPeriods(cnn, companyId, "099", currYear, currMonth)
        If IsNothing(prds) Then
            Throw New InvalidOperationException("Periode Journal tidak ditemukan/Generate..")
        End If

        If prds.Count > 0 Then
            Dim isLastPeriodClosed = prds.OrderByDescending(Function(x) x.Start).FirstOrDefault().IsClosed()
            If (Not isLastPeriodClosed) Then
                Throw New InvalidOperationException("Tidak Dapat Melakukan close tahunan karena periode bulan Desember belum di Close..")
            End If
        End If
        Return currYear
    End Function

    Public Sub NextYearPeriodGenerate(cnn As String, companyId As String)
        Dim currYear = currentYear(cnn, companyId)

        Dim year = New GlPeriodYear(companyId, "2014", "2014", New DateTime(2014, 1, 1), New DateTime(2014, 12, 31), companyId, "900")

        Dim isNextYearPeriodExist = IsYearValid(cnn, year.StartDate.Year, year.CompanyId, year.BranchId)

        If (Not isNextYearPeriodExist) Then
            NewYear(cnn, year)
        End If

    End Sub



    Function currentYear(ByVal cnn As String, companyId As String) As Integer
        Dim query = "select yearid from GLYear d where  CompanyId=@company  and YearStatus=1"

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@company", companyId)})
        If (reader.HasRows) Then

            If (reader Is Nothing) Then
                Throw New ArgumentNullException()
            End If

            If (reader.IsClosed) Then
                Throw New InvalidOperationException("DataReader Tidak dapat dibaca.  closed.")
            End If

            Dim yr = reader.GetOrdinal("yearId")
            If (reader.Read()) Then
                Return IIf(reader.IsDBNull(yr), -1, reader.GetInt32(yr))
            End If
        End If
        Return -1
    End Function

    Function currentMonth(ByVal cnn As String, companyId As String) As Integer
        Dim query = "select month(dbo.fnGetBusinessDate()) as Month"
        'Dim query = "select 12 as Month" 'keperluan testing

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.Text, query)
        If (reader.HasRows) Then

            If (reader Is Nothing) Then
                Throw New ArgumentNullException()
            End If

            Dim mth = reader.GetOrdinal("Month")
            If (reader.Read()) Then
                Return IIf(reader.IsDBNull(mth), -1, reader.GetInt32(mth))
            End If
        End If
        Return -1
    End Function

    'Public Function CurrentYearPeriod(cnn As String, companyid As String, branchid As String) As GlPeriod
    '    Dim currYear = currentYear(cnn, companyid)

    '    If (currYear < 0) Then
    '        Throw New Exception(String.Format("Tahun Transaksi GL tidak tersedia."))
    '    End If

    '    Dim prds = YearPeriods(cnn, companyid, branchid, currYear)

    '    If IsNothing(prds) Then
    '        Throw New Exception("Periode Journal tidak ditemukan/Generate..")
    '    End If

    '    Dim prd = prds.SingleOrDefault(Function(r) r.Status = PeriodStatus.Current)

    '    If (IsNothing(prd)) Then
    '        Throw New Exception("Periode Current Journal tidak ditemukan..")
    '    End If

    '    Return prd
    'End Function


    Public Function NewYear(cnn As String, year As GlPeriodYear) As String

        Dim validYear = IsYearValid(cnn, year.StartDate.Year, year.CompanyId, year.BranchId)

        If (validYear) Then
            Throw New Exception(String.Format("Tahun GL {0}  telah tersedia", year.StartDate.Year))
        End If
        year.GeneratePeriods()

        Dim tblPeriod = year.GetPeriods().ToDataTable()



        Dim param As SqlParameter
        Dim m_connection = New SqlConnection(cnn)
        If m_connection.State = ConnectionState.Closed Then m_connection.Open()

        Dim sqlcommand = New SqlCommand()
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.CommandText = "sp_NewGlYearPeriod"
        sqlcommand.Connection = m_connection

        param = New SqlParameter("@companyId", SqlDbType.VarChar, 3)
        param.Value = year.CompanyId
        sqlcommand.Parameters.Add(param)


        'param = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
        'param.Value = year.BranchId
        'sqlcommand.Parameters.Add(param)

        param = New SqlParameter("@yearId", SqlDbType.Int)
        param.Value = year.YearId
        sqlcommand.Parameters.Add(param)

        param = New SqlParameter("@yearCode", SqlDbType.VarChar, 20)
        param.Value = year.YearCode
        sqlcommand.Parameters.Add(param)

        param = New SqlParameter("@yearName", SqlDbType.VarChar, 50)
        param.Value = year.YearName
        sqlcommand.Parameters.Add(param)

        param = New SqlParameter("@yearStart", SqlDbType.DateTime)
        param.Value = year.StartDate
        sqlcommand.Parameters.Add(param)

        param = New SqlParameter("@yearEnd", SqlDbType.DateTime)
        param.Value = year.EndDate
        sqlcommand.Parameters.Add(param)

        param = New SqlParameter("@glPeriod", SqlDbType.Structured)
        param.Value = tblPeriod
        sqlcommand.Parameters.Add(param)

        Dim param1 = New SqlParameter("@errMsg", SqlDbType.VarChar, 500)
        param1.Direction = ParameterDirection.Output
        sqlcommand.Parameters.Add(param1)

        ' SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, "sp_NewGlYearPeriod", params.ToArray)
        sqlcommand.ExecuteNonQuery()
        Dim message = param1.Value.ToString()
        Return message
    End Function

    Public Function YearPeriods(cnn As String, companyid As String, branchid As String, year As Integer, month As Integer) As IList(Of GlPeriod)

        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@companyid", companyid))
        'params.Add(New SqlParameter("@branchid", branchid))
        params.Add(New SqlParameter("@year", year))
        params.Add(New SqlParameter("@month", month))

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.StoredProcedure, "GetYearPeriodList", params.ToArray)

        Dim result = New List(Of GlPeriod)


        If (reader.HasRows) Then

            If (reader Is Nothing) Then
                Throw New ArgumentNullException()
            End If
            If (reader.IsClosed) Then
                Throw New InvalidOperationException("This datareader cannot be read from.  It is closed.")
            End If

            Dim _cID = reader.GetOrdinal("CompanyID")
            Dim _BID = reader.GetOrdinal("BranchID")
            Dim prd = reader.GetOrdinal("PeriodId")
            Dim prdName = reader.GetOrdinal("PeriodName")
            Dim prdStar = reader.GetOrdinal("PeriodStart")
            Dim prdEnd = reader.GetOrdinal("PeriodEnd")
            Dim yr = reader.GetOrdinal("year")
            Dim prdSt = reader.GetOrdinal("PeriodStatus")

            Dim prdClsOn = reader.GetOrdinal("PeriodClosedOn")
            Dim prdEvCls = reader.GetOrdinal("PeriodEverClosed")

            Dim prdCActM = reader.GetOrdinal("PeriodChangeActiveMonthOn")
            Dim prdEvCngAct = reader.GetOrdinal("PeriodEverChangeActiveMonth")

            While (reader.Read())

                Dim _prd = IIf(reader.IsDBNull(prd), 0, reader.GetString(prd))
                Dim _yr = IIf(reader.IsDBNull(yr), 0, reader.GetInt32(yr))
                Dim _str = IIf(reader.IsDBNull(prdStar), New DateTime(1900, 1, 1), reader.GetDateTime(prdStar))
                Dim _prdEnd = IIf(reader.IsDBNull(prdEnd), New DateTime(1900, 1, 1), reader.GetDateTime(prdEnd))
                Dim _prdStr = IIf(reader.IsDBNull(prdSt), 0, reader.GetValue(prdSt))
                Dim _comp = IIf(reader.IsDBNull(_cID), String.Empty, reader.GetString(_cID))
                Dim _brnch = (IIf(reader.IsDBNull(_BID), String.Empty, reader.GetString(_BID)))


                result.Add(New GlPeriod(_prd, _yr, _str, _prdEnd, _prdStr, _comp, _brnch) With
                                     {
                 .Name = IIf(reader.IsDBNull(prdName), String.Empty, reader.GetString(prdName)),
                 .ClosedOn = IIf(reader.IsDBNull(prdClsOn), New DateTime(1900, 1, 1), reader.GetDateTime(prdClsOn)),
                .EverClosed = IIf(reader.IsDBNull(prdEvCls), 0, reader.GetBoolean(prdEvCls)),
                .ChangeActiveMonthOn = IIf(reader.IsDBNull(prdCActM), New DateTime(1900, 1, 1), reader.GetDateTime(prdCActM)),
                .EverChangeActiveMonth = IIf(reader.IsDBNull(prdEvCngAct), 0, reader.GetBoolean(prdEvCngAct))
                                     })

            End While
        End If
        Return result

    End Function

    Public Function SetCurrentGlPeriod(cnn As String, periodId As String) As String
        Dim spName As String = "sp_SetGlPeriodCurrent"

        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@PeriodId", SqlDbType.Char)
        params(0).Value = periodId

        params(1) = New SqlParameter("@errMsg", SqlDbType.VarChar, 500)
        params(1).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, spName, params)
        Return params(1).Value.ToString()
    End Function



End Class
