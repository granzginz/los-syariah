﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region
Public Class GL_Period : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spPagingGLPeriod"
    Private Const LIST_BY_ID As String = "spGLPeriodList"
    Private Const LIST_ADD As String = "spGLPeriodSaveAdd"
    Private Const LIST_UPDATE As String = "spGLPeriodSaveEdit"
    Private Const LIST_DELETE As String = "spGLPeriodDelete"
#End Region
    Public Function GetGLPeriod(ByVal oCustomClass As Parameter.GlPeriod) As Parameter.GlPeriod
        Dim oReturnValue As New Parameter.GlPeriod
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.GLPeriod.GetGLPeriod")
        End Try
    End Function

    Public Function GetGLPeriodList(ByVal ocustomClass As Parameter.GlPeriod) As Parameter.GlPeriod
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.GlPeriod
        params(0) = New SqlParameter("@PeriodId", SqlDbType.Char, 100)
        params(0).Value = ocustomClass.PeriodId
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.PeriodName = reader("PeriodName").ToString
                oReturnValue.PeriodStart = reader("PeriodStart").ToString
                oReturnValue.PeriodEnd = reader("PeriodEnd").ToString
                oReturnValue.PeriodSts = reader("PeriodStatus").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.GLPeriod.GetGLPeriodList")
        End Try
    End Function

    Public Function GLPeriodSaveAdd(ByVal ocustomClass As Parameter.GlPeriod) As String
        Dim ErrMessage As String = ""
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@PeriodId", SqlDbType.VarChar, 100)
        params(0).Value = ocustomClass.PeriodId
        params(1) = New SqlParameter("@PeriodName", SqlDbType.VarChar, 100)
        params(1).Value = ocustomClass.PeriodName
        params(2) = New SqlParameter("@PeriodStart", SqlDbType.DateTime)
        params(2).Value = ocustomClass.PeriodStart
        params(3) = New SqlParameter("@PeriodEnd", SqlDbType.DateTime)
        params(3).Value = ocustomClass.PeriodEnd
        params(4) = New SqlParameter("@PeriodStatus", SqlDbType.VarChar, 100)
        params(4).Value = ocustomClass.PeriodSts
        params(5) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(5).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(5).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.GLPeriod.GLPeriodSaveAdd")
        End Try
    End Function

    Public Sub GLPeriodSaveEdit(ByVal ocustomClass As Parameter.GlPeriod)
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@PeriodId", SqlDbType.VarChar, 100)
        params(0).Value = ocustomClass.PeriodId
        params(1) = New SqlParameter("@PeriodName", SqlDbType.VarChar, 100)
        params(1).Value = ocustomClass.PeriodName
        params(2) = New SqlParameter("@PeriodStart", SqlDbType.DateTime)
        params(2).Value = ocustomClass.PeriodStart
        params(3) = New SqlParameter("@PeriodEnd", SqlDbType.DateTime)
        params(3).Value = ocustomClass.PeriodEnd
        params(4) = New SqlParameter("@PeriodStatus", SqlDbType.VarChar, 100)
        params(4).Value = ocustomClass.PeriodSts
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.GLPeriod.GLPeriodSaveEdit")
        End Try
    End Sub

    Public Function GLPeriodDelete(ByVal ocustomClass As Parameter.GlPeriod) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@PeriodId", SqlDbType.Char, 100)
        params(0).Value = ocustomClass.PeriodId
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

End Class