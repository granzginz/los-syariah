﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class AmortisasiJurnal : Inherits Maxiloan.SQLEngine.DataAccessBase

    Public Function GetData(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oReturnValue As New Parameter.AmortisasiJurnal
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spAmortisasiJurnalPaging", params).Tables(0)
            'oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GL.DataJurnalCrossPeriode.GetDataJurnal")
        End Try
    End Function
    Public Function GetDataEdit(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oReturnValue As New Parameter.AmortisasiJurnal
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("ID", SqlDbType.VarChar, 100)
        params(0).Value = oCustomClass.ID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spAmortisasiJurnalEdit", params).Tables(0)
            'oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GL.DataJurnalCrossPeriode.GetDataEdit")
        End Try
    End Function
    Public Function GetDataDelete(ByVal oCustomClass As Parameter.AmortisasiJurnal) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int)
        params(0).Value = oCustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spAmortisasiJurnalDelete", params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function
    Public Function SaveData(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim params(2) As SqlParameter
        Try
            params(0) = New SqlParameter("@Nama", SqlDbType.VarChar, 100)
            params(0).Value = oCustomClass.Nama
            params(1) = New SqlParameter("@CoaDebit", SqlDbType.VarChar, 100)
            params(1).Value = oCustomClass.CoaDebit
            params(2) = New SqlParameter("@CoaCredit", SqlDbType.VarChar, 100)
            params(2).Value = oCustomClass.CoaCredit

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spAmortisasiJurnalSave", params).Tables(0)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GL.DataJurnalCrossPeriode.GetDataEdit")
        End Try
    End Function
    Public Function SaveEditData(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim params(3) As SqlParameter
        Try
            params(0) = New SqlParameter("@SeqNo", SqlDbType.VarChar, 100)
            params(0).Value = oCustomClass.SeqNo
            params(1) = New SqlParameter("@Nama", SqlDbType.VarChar, 100)
            params(1).Value = oCustomClass.Nama
            params(2) = New SqlParameter("@CoaDebit", SqlDbType.VarChar, 100)
            params(2).Value = oCustomClass.CoaDebit
            params(3) = New SqlParameter("@CoaCredit", SqlDbType.VarChar, 100)
            params(3).Value = oCustomClass.CoaCredit

            'oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spAmortisasiJurnalSaveEdit", params).Tables(0)
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spAmortisasiJurnalSaveEdit", params)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GL.DataJurnalCrossPeriode.GetDataEdit")
        End Try
    End Function

    'AmortisasiBiaya
    Public Function GetDataAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oReturnValue As New Parameter.AmortisasiJurnal
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spAmortisasiBiayaPaging", params).Tables(0)
            'oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GL.DataJurnalCrossPeriode.GetDataJurnal")
        End Try
    End Function
    Public Function GetDataSchemeAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oReturnValue As New Parameter.AmortisasiJurnal
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(0).Value = oCustomClass.WhereCond
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spSchemeAmortisasiBiaya", params).Tables(0)
            'oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GL.DataJurnalCrossPeriode.GetDataJurnal")
        End Try
    End Function
    Public Function GetDataGridAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oReturnValue As New Parameter.AmortisasiJurnal
        Dim params(6) As SqlParameter
        params(0) = New SqlParameter("@Tr_Nomor", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.Tr_Nomor
        params(1) = New SqlParameter("@Nilai", SqlDbType.Decimal)
        params(1).Value = oCustomClass.Nilai
        params(2) = New SqlParameter("@JangkaWaktu", SqlDbType.Int)
        params(2).Value = oCustomClass.JangkaWaktu
        params(3) = New SqlParameter("@AmountRec", SqlDbType.Decimal)
        params(3).Value = oCustomClass.AmountRec
        params(4) = New SqlParameter("@TglMulai", SqlDbType.Date)
        params(4).Value = oCustomClass.TglMulai
        params(5) = New SqlParameter("@CoaDebit", SqlDbType.VarChar, 20)
        params(5).Value = oCustomClass.CoaDebit.Trim
        params(6) = New SqlParameter("@CoaCredit", SqlDbType.VarChar, 20)
        params(6).Value = oCustomClass.CoaCredit.Trim
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGridAmortisasiBiaya", params).Tables(0)
            'oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GL.DataJurnalCrossPeriode.GetDataGridAmortisasiBiaya")
        End Try
    End Function
    Public Function AmortisasiBiayaSave(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oReturnValue As New Parameter.AmortisasiJurnal
        Dim params(17) As SqlParameter
        params(0) = New SqlParameter("@Tr_Nomor", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.Tr_Nomor
        params(1) = New SqlParameter("@NoReference", SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.NoReference
        params(2) = New SqlParameter("@TglEfektif", SqlDbType.Date)
        params(2).Value = oCustomClass.TglEfektif
        params(3) = New SqlParameter("@TglMulai", SqlDbType.Date)
        params(3).Value = oCustomClass.TglMulai
        params(4) = New SqlParameter("@TglAkhir", SqlDbType.Date)
        params(4).Value = oCustomClass.TglAkhir
        params(5) = New SqlParameter("@Nilai", SqlDbType.Decimal)
        params(5).Value = oCustomClass.Nilai
        params(6) = New SqlParameter("@JangkaWaktu", SqlDbType.Int)
        params(6).Value = oCustomClass.JangkaWaktu
        params(7) = New SqlParameter("@AmountRec", SqlDbType.Decimal)
        params(7).Value = oCustomClass.AmountRec
        params(8) = New SqlParameter("@lblNo", SqlDbType.VarChar, 3)
        params(8).Value = oCustomClass.lblNo
        params(9) = New SqlParameter("@lblDate", SqlDbType.Date)
        params(9).Value = oCustomClass.lblDate
        params(10) = New SqlParameter("@lblAmount", SqlDbType.Decimal)
        params(10).Value = oCustomClass.lblAmount
        params(11) = New SqlParameter("@lblDEBET", SqlDbType.VarChar, 100)
        params(11).Value = oCustomClass.lblDEBET
        params(12) = New SqlParameter("@lblDEBETDESC", SqlDbType.VarChar, 100)
        params(12).Value = oCustomClass.lblDEBETDESC
        params(13) = New SqlParameter("@lblCREDIT", SqlDbType.VarChar, 100)
        params(13).Value = oCustomClass.lblCREDIT
        params(14) = New SqlParameter("@lblCREDITDESC", SqlDbType.VarChar, 100)
        params(14).Value = oCustomClass.lblCREDITDESC
        params(15) = New SqlParameter("@AbNo", SqlDbType.VarChar, 20)
        params(15).Value = oCustomClass.AbNo
        params(16) = New SqlParameter("@CoaDebit", SqlDbType.VarChar, 20)
        params(16).Value = oCustomClass.CoaDebit.Trim
        params(17) = New SqlParameter("@CoaCredit", SqlDbType.VarChar, 20)
        params(17).Value = oCustomClass.CoaCredit.Trim
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spAmortisasiBiayaSave", params).Tables(0)
            'oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GL.DataJurnalCrossPeriode.GetDataGridAmortisasiBiaya")
        End Try
    End Function
    Public Function GenerateJournalList(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oReturnValue As New Parameter.AmortisasiJurnal
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGenerateJournalList", params).Tables(0)
            'oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GL.DataJurnalCrossPeriode.GenerateJournalList")
        End Try
    End Function
    Public Function GenerateJournal(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oReturnValue As New Parameter.AmortisasiJurnal
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@StartDate", SqlDbType.Date)
        params(0).Value = oCustomClass.StartDate
        params(1) = New SqlParameter("@EndDate", SqlDbType.Date)
        params(1).Value = oCustomClass.EndDate

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGenerateJournal", params).Tables(0)
            'oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GL.DataJurnalCrossPeriode.GenerateJournal")
        End Try
    End Function

    Public Function GetApprovalAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal, ByVal strApproval As String)
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim transaction As SqlTransaction = Nothing

        Dim par() As SqlParameter = New SqlParameter(1) {}

        par(0) = New SqlParameter("@NoReference", SqlDbType.Char, 100)
        par(0).Value = oCustomClass.NoReference
        par(1) = New SqlParameter("@Approval", SqlDbType.Char, 3)
        par(1).Value = strApproval

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            If (strApproval.Trim = "R") Then
                oCustomClass.Approval.ApprovalTransaction = transaction
                'oClass.Approval.TransactionNo = CStr(params(2).Value)
                Dim strApprovalNo = oApprovalID.RequestForApproval(oCustomClass.Approval)
            End If

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spRequestApprovalAmortisasiBiaya", par)
            transaction.Commit()

        Catch ex As Exception
            transaction.Rollback()
            Throw New Exception(ex.Message)

        Finally
            If conn.State = ConnectionState.Open Then conn.Dispose()
            conn.Dispose()

        End Try

    End Function

    Public Function GetGridApprovalAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oReturnValue As New Parameter.AmortisasiJurnal
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(0).Value = oCustomClass.WhereCond
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spSchemeAmortisasiBiaya", params).Tables(0)
            'oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GL.DataJurnalCrossPeriode.GetDataJurnal")
        End Try
    End Function
    Public Function GetViewSchemeAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
        Dim oReturnValue As New Parameter.AmortisasiJurnal
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(0).Value = oCustomClass.WhereCond
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewSchemeAmortisasiBiaya", params).Tables(0)
            'oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GL.DataJurnalCrossPeriode.GetViewSchemeAmortisasiBiaya")
        End Try
    End Function

#Region "Get_RequestNo"
    Public Function Get_RequestNo(ByVal customClass As Parameter.AmortisasiJurnal) As String
        Dim Result As String
        Dim params() As SqlParameter = New SqlParameter(3) {}

        params(0) = New SqlParameter("@branchId", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@businessdate", SqlDbType.DateTime)
        params(1).Value = customClass.BusinessDate
        params(2) = New SqlParameter("@ID", SqlDbType.VarChar, 10)
        params(2).Value = customClass.ID1
        params(3) = New SqlParameter("@sequenceNo", SqlDbType.VarChar, 20)
        params(3).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, "spGetNoTransaction", params)
            Result = CType(params(3).Value, String)
            If Result <> "" Then
                Return Result
            End If
            Return ""
        Catch exp As Exception
            WriteException("AmortisasiJurnal", "Get_RequestNo", exp.Message)
        End Try

    End Function
#End Region
End Class
