﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region
Public Class JournalTransaction : Inherits DataAccessBase
    Public Function GetJournalTransaction(oCustom As Parameter.JournalTransaction) As Parameter.JournalTransaction
        Dim oReturn As New Parameter.JournalTransaction
        Dim params() As SqlParameter = New SqlParameter(6) {}

        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = oCustom.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = oCustom.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = oCustom.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = oCustom.SortBy

        params(4) = New SqlParameter("@TransactioNO", SqlDbType.VarChar, 20)
        params(4).Value = oCustom.TransactionNo

        params(5) = New SqlParameter("@TransactionType", SqlDbType.VarChar, 3)
        params(5).Value = oCustom.TransactionType

        params(6) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(6).Direction = ParameterDirection.Output

        Try

            oReturn.ListData = SqlHelper.ExecuteDataset(oCustom.strConnection, CommandType.StoredProcedure, "spGetJournalTransaction", params).Tables(0)
            oReturn.TotalRecord = CInt(params(6).Value)


        Catch exp As Exception
            WriteException("GL", "GetJoutnalTransaction", exp.Message + exp.StackTrace)
        End Try
        Return oReturn
    End Function
End Class
