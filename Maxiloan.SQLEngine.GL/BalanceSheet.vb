﻿Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Framework.SQLEngine

Imports System.Linq
Public Class BalanceSheet : Inherits DataAccessBase

    Const SP_SELECT As String = "BalanceSheetSetup_select"
    Const SP_INSERT As String = "BalanceSheetSetup_insert"
    Const SP_UPDATE As String = "BalanceSheetSetup_update"
    Const SP_DELETE As String = "BalanceSheetSetup_delete"


    Public Function SelectData(ByRef totalrec As Integer, ByVal strCnn As String, ByVal ParamArray param As Object()) As IList(Of BalanceSheetSetup)
        Try

            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

            params.Add(New SqlParameter("@CompanyId", param(0)))
            params.Add(New SqlParameter("@currPage", CType(param(1), Integer)))
            params.Add(New SqlParameter("@pageSize", CType(param(2), Integer)))
            If (param.Length > 3) Then
                If (param(3) <> "") Then
                    params.Add(New SqlParameter("@wOpt", CType(param(3), Integer)))
                    params.Add(New SqlParameter("@where", param(4)))
                End If
            End If

            Dim prm = New SqlParameter("@totalRecords", SqlDbType.Int)
            prm.Direction = ParameterDirection.Output
            params.Add(prm)
            totalrec = 0
            Dim prms As SqlParameter() = params.ToArray
            Dim data As DataTable = SqlHelper.ExecuteDataset(strCnn, CommandType.StoredProcedure, "BalanceSheetSetupPage_Select", prms).Tables(0)

            If (data.Rows.Count > 0) Then
                totalrec = CType(data.Rows(0)("rows"), Int64)
            End If

            Dim list As IList(Of BalanceSheetSetup) = (From item As DataRow In data.Rows Select New BalanceSheetSetup(item("ReportID"), item("ReportTitle"), item("ReportText1"), item("ReportText2"), item("PrintLastMonth"), item("PrintVariance"), item("CompanyID"))).ToList()

            Return list
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Sub DeleteData(ByVal strCnn As String, ByVal ReportId As String)
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter("@ReportID", SqlDbType.VarChar)
        params(0).Value = ReportId

        SqlHelper.ExecuteNonQuery(strCnn, CommandType.StoredProcedure, SP_DELETE, params)
    End Sub

    Private Function IsDuplicateCode(ByVal strCnn As String, ByVal ReportId As String, ByVal CompanyId As String) As Boolean
        Dim query = "Select count(*) from GLBalanceSheetSetup where ReportID=@ReportId and CompanyId=@CompanyId"
        Dim ret = Convert.ToInt32(SqlHelper.ExecuteScalar(strCnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@ReportId", ReportId), New SqlParameter("@CompanyId", CompanyId)}))
        Return ret > 0
    End Function

    Public Sub InsertData(ByVal strCnn As String, ByVal data As BalanceSheetSetup)

        If (IsDuplicateCode(strCnn, data.ReportId, data.CompanyId)) Then
            Throw New DuplicateNameException("Report id sudah Ada.")
        End If

        SqlHelper.ExecuteNonQuery(strCnn, CommandType.StoredProcedure, SP_INSERT, CreateParam(data, True))

    End Sub

    Public Sub UpdateData(ByVal strCnn As String, ByVal data As BalanceSheetSetup)
        SqlHelper.ExecuteNonQuery(strCnn, CommandType.StoredProcedure, SP_UPDATE, CreateParam(data, False))
    End Sub


    Private Function CreateParam(ByVal data As BalanceSheetSetup, ByVal create As Boolean) As SqlParameter()
        Dim param As SqlParameter
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        param = New SqlParameter("@CompanyId", SqlDbType.VarChar)
        param.Value = data.CompanyId
        params.Add(param)

        param = New SqlParameter("@ReportID", SqlDbType.VarChar)
        param.Value = data.ReportId
        params.Add(param)

        param = New SqlParameter("@ReportTitle", SqlDbType.VarChar)
        param.Value = data.ReportTitle
        params.Add(param)

        param = New SqlParameter("@ReportText1", SqlDbType.VarChar)
        param.Value = data.ReportText1
        params.Add(param)

        param = New SqlParameter("@ReportText2", SqlDbType.VarChar)
        param.Value = data.ReportText2
        params.Add(param)

        param = New SqlParameter("@PrintLastMonth", SqlDbType.Bit)
        param.Value = data.IsPrintLastMonth
        params.Add(param)

        param = New SqlParameter("@PrintVariance", SqlDbType.Bit)
        param.Value = data.IsPrintVariance
        params.Add(param)

        If (create) Then
            param = New SqlParameter("@Createdby", SqlDbType.VarChar)
        Else
            param = New SqlParameter("@Updateby", SqlDbType.VarChar)
        End If
        param.Value = IIf(create, data.UserCreate, data.UserUpdate)
        params.Add(param)

        Return params.ToArray
    End Function

    Public Function FindById(ByVal strCnn As String, ByVal ReportID As String, ByVal companyId As String) As BalanceSheetSetup
        Dim query = "select *  from dbo.GLBalanceSheetSetup " +
            " where ReportID=@reportID and CompanyId=@CompanyId"
        Dim paramchar = New Char() {" "}
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(strCnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@reportID", ReportID.Trim()), New SqlParameter("@CompanyId", companyId.Trim())})

        If (reader.HasRows) Then
            reader.Read()
            Dim result As New BalanceSheetSetup(reader.GetString(0).Trim(), reader.GetString(2).Trim(), reader.GetString(3).Trim(), reader.GetString(4).Trim(), reader.GetBoolean(5), reader.GetBoolean(6), reader.GetString(1).Trim())
            result.IsPrintLastMonth = reader.GetBoolean(5)
            result.IsPrintVariance = reader.GetBoolean(6)
            Return result
        End If
        Return Nothing
    End Function


#Region "Balance Sheet Group"

    Public Function SelectBSAccGroup(ByRef totalrec As Integer, ByVal strCnn As String, ByVal ParamArray param As Object()) As IList(Of BalanceSheetAccGroup)
        Try

            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

            params.Add(New SqlParameter("@ReportID", param(0)))
            params.Add(New SqlParameter("@currPage", CType(param(1), Integer)))
            params.Add(New SqlParameter("@pageSize", CType(param(2), Integer)))
            If (param.Length > 3) Then
                If (param(3) <> "") Then
                    params.Add(New SqlParameter("@where", param(3)))
                End If
            End If

            Dim prm = New SqlParameter("@totalRecords", SqlDbType.Int)
            prm.Direction = ParameterDirection.Output
            params.Add(prm)
            totalrec = 0
            Dim prms As SqlParameter() = params.ToArray
            Dim data As DataTable = SqlHelper.ExecuteDataset(strCnn, CommandType.StoredProcedure, "BalanceSheetAccGroup_Select", prms).Tables(0)

            If (data.Rows.Count > 0) Then
                totalrec = CType(data.Rows(0)("rows"), Int64)
            End If

            Dim list As IList(Of BalanceSheetAccGroup) = (From item As DataRow In data.Rows Select New BalanceSheetAccGroup(item("ReportGroupID"), item("ReportID"), item("GroupDesc"), item("ParentGroupName"), item("IsBar"), item("LayoutSequence"), item("GroupType"), item("Level"))).ToList()

            Return list
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Sub DeleteBSAccGroup(ByVal strCnn As String, ByVal ReportGroupId As Integer)
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter("@ReportGroupID", SqlDbType.Int)
        params(0).Value = ReportGroupId

        SqlHelper.ExecuteNonQuery(strCnn, CommandType.StoredProcedure, "BalanceSheetAccGroup_Delete", params)
    End Sub

    Private Function IsDuplicateBSAccGroup(ByVal strCnn As String, ByVal ReportGroupId As String) As Boolean
        Dim query = "Select count(*) from GLBalanceSheetAccGroup where ReportGroupId=@ReportGroupId"
        Dim ret = Convert.ToInt32(SqlHelper.ExecuteScalar(strCnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@ReportGroupId", ReportGroupId)}))
        Return ret > 0
    End Function

    Public Sub InsertBSAccGroup(ByVal strCnn As String, ByVal data As BalanceSheetAccGroup)

        If (IsDuplicateBSAccGroup(strCnn, data.ReportGroupId)) Then
            Throw New DuplicateNameException("Report id sudah Ada.")
        End If

        SqlHelper.ExecuteNonQuery(strCnn, CommandType.StoredProcedure, "BalanceSheetAccGroup_Insert", CreateParamBSAccGroup(data, True))

    End Sub

    Public Sub UpdateBSAccGroup(ByVal strCnn As String, ByVal data As BalanceSheetAccGroup)
        SqlHelper.ExecuteNonQuery(strCnn, CommandType.StoredProcedure, "BalanceSheetAccGroup_Update", CreateParamBSAccGroup(data, False))
    End Sub


    Private Function CreateParamBSAccGroup(ByVal data As BalanceSheetAccGroup, ByVal create As Boolean) As SqlParameter()
        Dim param As SqlParameter
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        param = New SqlParameter("@ReportID", SqlDbType.VarChar)
        param.Value = data.ReportID
        params.Add(param)

        param = New SqlParameter("@GroupDesc", SqlDbType.VarChar)
        param.Value = data.groupDesc
        params.Add(param)

        param = New SqlParameter("@ParentReportGroupID", SqlDbType.Int)
        param.Value = data.ParentGroupName
        params.Add(param)

        param = New SqlParameter("@IsBar", SqlDbType.Bit)
        param.Value = data.IsBar
        params.Add(param)

        param = New SqlParameter("@GroupType", SqlDbType.VarChar)
        param.Value = data.groupType
        params.Add(param)

        If (create) Then
            param = New SqlParameter("@Createdby", SqlDbType.VarChar)
        Else
            param = New SqlParameter("@Updateby", SqlDbType.VarChar)
        End If
        param.Value = IIf(create, data.UserCreate, data.UserUpdate)
        params.Add(param)

        If (Not create) Then
            param = New SqlParameter("@ReportGroupId", SqlDbType.Int)
            param.Value = data.ReportGroupId
            params.Add(param)
        End If

        Return params.ToArray
    End Function

    Public Function FindByIdBSAccGroup(ByVal strCnn As String, ByVal ReportGroupID As Integer) As BalanceSheetAccGroup
        Dim query = "select *  from dbo.GLBalanceSheetAccGroup " +
            " where ReportGroupID=@reportgroupID"
        Dim paramchar = New Char() {" "}
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(strCnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@reportgroupID", ReportGroupID)})
        If (reader.HasRows) Then
            reader.Read()
            Dim parentid As Integer = 0
            If (Not reader.IsDBNull(reader.GetOrdinal("ParentReportGroupID"))) Then
                parentid = reader.GetInt32(4)
            End If
            Dim result As New BalanceSheetAccGroup(reader.GetInt32(0), reader.GetString(1).Trim(), reader.GetString(2).Trim(), parentid.ToString, reader.GetBoolean(7), reader.GetInt32(5), reader.GetString(8), reader.GetInt32(6))
            result.ParentGroup = parentid
            Return result
        End If
        Return Nothing
    End Function

    Public Function SelectAllBSAccGroup(ByVal strCnn As String) As DataTable
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(strCnn, CommandType.StoredProcedure, "BalanceSheetAccGroup_SelectAll")
        Return ds.Tables(0)
    End Function

#End Region


#Region "Balance Sheet Group Detail"

    Public Function SelectBSAccDetail(ByRef totalrec As Integer, ByVal strCnn As String, ByVal ParamArray param As Object()) As IList(Of BalanceSheetAccDetail)
        Try

            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

            params.Add(New SqlParameter("@ReportGroupID", param(0)))
            params.Add(New SqlParameter("@currPage", CType(param(1), Integer)))
            params.Add(New SqlParameter("@pageSize", CType(param(2), Integer)))
            If (param.Length > 3) Then
                If (param(3) <> "") Then
                    params.Add(New SqlParameter("@where", param(3)))
                End If
            End If

            Dim prm = New SqlParameter("@totalRecords", SqlDbType.Int)
            prm.Direction = ParameterDirection.Output
            params.Add(prm)
            totalrec = 0
            Dim prms As SqlParameter() = params.ToArray
            Dim data As DataTable = SqlHelper.ExecuteDataset(strCnn, CommandType.StoredProcedure, "BalanceSheetAccDetail_Select", prms).Tables(0)

            If (data.Rows.Count > 0) Then
                totalrec = CType(data.Rows(0)("rows"), Int64)
            End If

            Dim list As IList(Of BalanceSheetAccDetail) = (From item As DataRow In data.Rows Select New BalanceSheetAccDetail(item("ReportGroupID"), item("CoaId"), item("AccountType"), item("CoaDescription"), item("IsPositif"))).ToList()

            Return List
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Sub DeleteBSAccDetail(ByVal strCnn As String, ByVal ReportGroupId As Integer, ByVal CoaId As String)
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@ReportGroupID", SqlDbType.Int)
        params(0).Value = ReportGroupId
        params(1) = New SqlParameter("@CoaId", SqlDbType.VarChar)
        params(1).Value = CoaId

        SqlHelper.ExecuteNonQuery(strCnn, CommandType.StoredProcedure, "BalanceSheetAccDetail_Delete", params)
    End Sub

    Private Function IsDuplicateBSAccDetail(ByVal strCnn As String, ByVal ReportGroupId As String, ByVal coaId As String) As Boolean
        Dim query = "Select count(*) from GLBalanceSheetAccDetail where ReportGroupId=@ReportGroupId and CoAId= @CoaId"
        Dim ret = Convert.ToInt32(SqlHelper.ExecuteScalar(strCnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@ReportGroupId", ReportGroupId), New SqlParameter("@CoaId", coaId)}))

        Return ret > 0
    End Function

    Public Sub InsertBSAccDetail(ByVal strCnn As String, ByVal data As BalanceSheetAccDetail)

        If (IsDuplicateBSAccDetail(strCnn, data.ReportGroupId, data.CoaId)) Then
            Throw New DuplicateNameException("No Coa sudah Di input.")
        End If
        SqlHelper.ExecuteNonQuery(strCnn, CommandType.StoredProcedure, "BalanceSheetAccDetail_Insert", CreateParamBSAccDetail(data, True))

    End Sub

    Public Sub UpdateBSAccDetail(ByVal strCnn As String, ByVal data As BalanceSheetAccDetail)
        SqlHelper.ExecuteNonQuery(strCnn, CommandType.StoredProcedure, "BalanceSheetAccDetail_Update", CreateParamBSAccDetail(data, False))
    End Sub

    Private Function CreateParamBSAccDetail(ByVal data As BalanceSheetAccDetail, ByVal create As Boolean) As SqlParameter()
        Dim param As SqlParameter
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        param = New SqlParameter("@ReportGroupID", SqlDbType.Int)
        param.Value = data.ReportGroupId
        params.Add(param)

        param = New SqlParameter("@CoaId", SqlDbType.VarChar)
        param.Value = data.CoaId
        params.Add(param)

        param = New SqlParameter("@IsPositif", SqlDbType.Int)
        param.Value = data.IsPositif
        params.Add(param)

        param = New SqlParameter("@AccountType", SqlDbType.VarChar)
        param.Value = CInt(data.accType)
        params.Add(param)

        Return params.ToArray
    End Function

    Public Function FindByIdBSAccDetail(ByVal strCnn As String, ByVal ReportGroupID As Integer, ByVal coaid As String) As BalanceSheetAccDetail
        Dim query = "select DISTINCT a.ReportGroupID,a.CoAId,AccountType, (b.Description + ' ' + SUBSTRING(REPLACE(a.CoAId,' ',''),1,CHARINDEX('-',REPLACE(a.CoAId,' ',''))-1)) + '/'+ LTRIM(RTRIM(b.CoAIdAsli)) + ' - ' + BranchFullName as CoaDescription, a.IsPositif from dbo.GLBalanceSheetAccDetail a inner join dbo.GLMasterAcc b on a.CoaId = b.CoaId INNER JOIN dbo.Branch ON b.BranchId = dbo.Branch.BranchID" +
            " where a.ReportGroupID=@reportgroupID and a.CoAId=@CoaId"
        Dim paramchar = New Char() {" "}
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(strCnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@reportgroupID", ReportGroupID), New SqlParameter("@CoaId", coaid)})
        If (reader.HasRows) Then
            reader.Read()
            Dim result As New BalanceSheetAccDetail(reader.GetInt32(0), reader.GetString(1).Trim(), reader.GetString(2).Trim(), reader.GetString(3).Trim(), reader.GetInt32(4))
            Return Result
        End If
        Return Nothing
    End Function

    Public Function SelectAllCOA(ByVal strCnn As String) As DataTable
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(strCnn, CommandType.StoredProcedure, "GLMasterAcc_SelectAll")
        Return ds.Tables(0)
    End Function




    Public Function SelectBSGroupDetail(ByRef totalrec As Integer, ByVal strCnn As String, ByVal ParamArray param As Object()) As IList(Of BalanceSheetGroupDetail)
        Try

            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

            params.Add(New SqlParameter("@ReportGroupID", param(0)))
            params.Add(New SqlParameter("@currPage", CType(param(1), Integer)))
            params.Add(New SqlParameter("@pageSize", CType(param(2), Integer)))
            If (param.Length > 3) Then
                If (param(3) <> "") Then
                    params.Add(New SqlParameter("@where", param(3)))
                End If
            End If

            Dim prm = New SqlParameter("@totalRecords", SqlDbType.Int)
            prm.Direction = ParameterDirection.Output
            params.Add(prm)
            totalrec = 0
            Dim prms As SqlParameter() = params.ToArray
            Dim data As DataTable = SqlHelper.ExecuteDataset(strCnn, CommandType.StoredProcedure, "BalanceSheetGroupDetail_Select", prms).Tables(0)

            If (data.Rows.Count > 0) Then
                totalrec = CType(data.Rows(0)("rows"), Int64)
            End If

            Dim list As IList(Of BalanceSheetGroupDetail) = (From item As DataRow In data.Rows Select New BalanceSheetGroupDetail(item("ReportGroupID"), item("GroupId"), item("AccountType"), item("GroupDescription"))).ToList()

            Return list
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Sub DeleteBSGroupDetail(ByVal strCnn As String, ByVal ReportGroupId As Integer, ByVal GroupID As Integer)
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@ReportGroupID", SqlDbType.Int)
        params(0).Value = ReportGroupId
        params(1) = New SqlParameter("@GroupId", SqlDbType.Int)
        params(1).Value = GroupID

        SqlHelper.ExecuteNonQuery(strCnn, CommandType.StoredProcedure, "BalanceSheetGroupDetail_Delete", params)
    End Sub

    Public Sub InsertBSGroupDetail(ByVal strCnn As String, ByVal data As BalanceSheetGroupDetail)

        If (IsDuplicateBSGroupDetail(strCnn, data.ReportGroupId, data.GroupID)) Then
            Throw New DuplicateNameException("Group id sudah Ada.")
        End If
        SqlHelper.ExecuteNonQuery(strCnn, CommandType.StoredProcedure, "BalanceSheetGroupDetail_Insert", CreateParamBSGroupDetail(data, True))

    End Sub

    Private Function IsDuplicateBSGroupDetail(ByVal strCnn As String, ByVal ReportGroupId As String, ByVal groupid As Integer) As Boolean
        Dim query = "Select count(*) from GLBalanceSheetGroupDetail where ReportGroupId=@ReportGroupId and GroupId = @GroupId"
        Dim ret = Convert.ToInt32(SqlHelper.ExecuteScalar(strCnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@ReportGroupId", ReportGroupId), New SqlParameter("@GroupId", groupid)}))
        Return ret > 0
    End Function

    Public Sub UpdateBSGroupDetail(ByVal strCnn As String, ByVal data As BalanceSheetGroupDetail)
        SqlHelper.ExecuteNonQuery(strCnn, CommandType.StoredProcedure, "BalanceSheetGroupDetail_Update", CreateParamBSGroupDetail(data, False))
    End Sub

    Private Function CreateParamBSGroupDetail(ByVal data As BalanceSheetGroupDetail, ByVal create As Boolean) As SqlParameter()
        Dim param As SqlParameter
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        param = New SqlParameter("@ReportGroupID", SqlDbType.Int)
        param.Value = data.ReportGroupId
        params.Add(param)

        param = New SqlParameter("@GroupId", SqlDbType.Int)
        param.Value = data.GroupID
        params.Add(param)

        param = New SqlParameter("@AccountType", SqlDbType.VarChar)
        param.Value = CInt(data.accType)
        params.Add(param)

        Return params.ToArray
    End Function

    Public Function FindByIdBSGroupDetail(ByVal strCnn As String, ByVal ReportGroupID As Integer, ByVal groupid As Integer) As BalanceSheetGroupDetail
        Dim query = "select a.ReportGroupID,GroupID,a.AccountType, (select GroupDesc From GLBalanceSheetAccGroup Where ReportGroupid = a.GroupID) as GroupDescription  from GLBalanceSheetGroupDetail  a" +
            " where a.ReportGroupID=@reportgroupID and a.GroupID=@GroupId"
        Dim paramchar = New Char() {" "}
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(strCnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@reportgroupID", ReportGroupID), New SqlParameter("@GroupId", groupid)})
        If (reader.HasRows) Then
            reader.Read()
            Dim result As New BalanceSheetGroupDetail(reader.GetInt32(0), reader.GetInt32(1), reader.GetString(2).Trim(), reader.GetString(3).Trim())
            Return result
        End If
        Return Nothing
    End Function

    Public Function SelectAllBSGroupDetail(ByVal strCnn As String, ByVal RID As String) As DataTable
        'Dim ds As DataSet
        'ds = SqlHelper.ExecuteDataset(strCnn, CommandType.StoredProcedure, "BalanceSheetGroupDetail_SelectAll")
        'Return ds.Tables(0)

        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("GroupId", GetType(String))
        dttResult.Columns.Add("GroupDesc", GetType(String))

        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@ReportID", SqlDbType.VarChar, 1000)
        params(0).Value = RID

        Try
            reader = SqlHelper.ExecuteReader(strCnn, CommandType.StoredProcedure, "BalanceSheetGroupDetail_SelectAll", params)

            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("GroupId") = reader("GroupId").ToString
                dtrResult("GroupDesc") = reader("GroupDesc").ToString

                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "BalanceSheetGroupDetail_SelectAll", exp.Message + exp.StackTrace)
        End Try
    End Function


#End Region

    Public Function GetAccountCOA(ByVal strConnection As String, ByVal ReportGroupId As String) As DataTable

        Dim dttResult As New DataTable

        dttResult.Columns.Add("CoAId", GetType(String))
        dttResult.Columns.Add("Description", GetType(String))

        Dim sqlConnection As New SqlConnection
        sqlConnection.ConnectionString = strConnection

        Try

            Dim sql As String = "SELECT RTRIM(CoAId) AS CoAId, " &
                                        "(Description + ' - ' + CoAId) AS Description " &
                                "FROM GLMasterAcc " &
                                "LEFT JOIN GLBalanceSheetAccGroup ON GLBalanceSheetAccGroup.GroupDesc = GLMasterAcc.Description " &
                                "WHERE GLBalanceSheetAccGroup.reportgroupID IN (@ReportGroupId) " &
                                "ORDER BY CoAId "

            Dim params() As SqlParameter = New SqlParameter(0) {}
            params(0) = New SqlParameter("@ReportGroupId", SqlDbType.VarChar, 1000)
            params(0).Value = ReportGroupId

            Dim sqlCommand As New SqlCommand()
            sqlCommand.Connection = sqlConnection
            sqlCommand.CommandText = sql
            sqlCommand.CommandType = CommandType.Text
            sqlCommand.Parameters.AddRange(params)

            sqlConnection.Open()

            Dim reader As SqlDataReader
            reader = sqlCommand.ExecuteReader()

            While reader.Read
                Dim dtrResult As DataRow
                dtrResult = dttResult.NewRow
                dtrResult("CoAId") = reader("CoAId").ToString
                dtrResult("Description") = reader("Description").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
        Catch ex As Exception
        Finally
            If (sqlConnection.State = ConnectionState.Open) Then
                sqlConnection.Close()
            End If
        End Try

        Return dttResult

    End Function
End Class
