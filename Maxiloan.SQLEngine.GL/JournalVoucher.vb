﻿Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Framework.SQLEngine


Public Class JournalVoucher : Inherits DataAccessBase
    'Const SP_SELECT As String = "spGLJournalH_Get_JurnalVoucher"
    'Const SP_INSERT As String = "spGLJournalH_Save_JurnalVoucher"
    'Const SP_UPDATE As String = "spGLJournalH_Update_JurnalVoucher"
    'Const SP_DELETE As String = "spGLJournalH_delete_JurnalVoucher"

    'Const SP_INSERT_DETAIL As String = "spGLJournalD_Save_JurnalVoucher"
    'Const SP_SELECT_DETAIL As String = "spGLJournalD_Get_JurnalVoucher"
    'Const SP_DELETE_ALL_DETAIL As String = "spGLJournalD_DeleteAll_JurnalVoucher"



    Public Function GetGlMasterSequence(ByVal strCnn As String, ByVal ParamArray param() As Object) As IList(Of ValueTextObject)
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@BranchId", SqlDbType.Char)
        params(0).Value = param(0)

        Dim data As DataTable = SqlHelper.ExecuteDataset(strCnn, CommandType.StoredProcedure, "spGetGLMASTERSEQUENCE", params).Tables(0)
        Return (From item As DataRow In data.Rows Select New ValueTextObject(item("transactionid"), item("description"))).ToList()

    End Function
    Public Function GetCompanyId(cnn As String) As String
        Dim dr As Object = SqlHelper.ExecuteScalar(cnn, CommandType.StoredProcedure, "spGetFNCompanyID")
        Return dr.ToString()
    End Function



    Public Function GetJurnalVoucherByTrNo(ByVal cnn As String, transNo As String) As GlJournalVoucher
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@Tr_Nomor", transNo))
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.StoredProcedure, "spJurnalVoucherByTrNo", params.ToArray)

        Dim result = New GlJournalVoucher
        Dim _read As Boolean = False

        If (reader.HasRows) Then

            If (reader Is Nothing) Then
                Throw New ArgumentNullException()
            End If
            If (reader.IsClosed) Then
                Throw New InvalidOperationException("This datareader cannot be read from.  It is closed.")
            End If

            Dim _cID = reader.GetOrdinal("CompanyID")
            Dim _BID = reader.GetOrdinal("BranchID")
            Dim _TrNo = reader.GetOrdinal("Tr_Nomor")
            Dim _TrDt = reader.GetOrdinal("Tr_Date")
            Dim _TrID = reader.GetOrdinal("TransactionID")
            Dim _ReffNo = reader.GetOrdinal("Reff_No")
            Dim _ReffDt = reader.GetOrdinal("Reff_Date")
            Dim _ket = reader.GetOrdinal("keterangan")
            Dim _Jenis_Reff = reader.GetOrdinal("Jenis_Reff")

            Dim _coaId = reader.GetOrdinal("coaid")
            Dim _seq = reader.GetOrdinal("SequenceNo")
            Dim _coaName = reader.GetOrdinal("coaName")
            Dim _post = reader.GetOrdinal("post")
            Dim _tr_desc = reader.GetOrdinal("tr_desc")
            Dim _amount = reader.GetOrdinal("Amount")
            Dim _addfree = ""


            While (reader.Read())

                If (Not _read) Then
                    With result
                        .CompanyId = IIf(reader.IsDBNull(_cID), String.Empty, reader.GetString(_cID))
                        .BranchId = IIf(reader.IsDBNull(_BID), String.Empty, reader.GetString(_BID))
                        .TransactionNomor = IIf(reader.IsDBNull(_TrNo), String.Empty, reader.GetString(_TrNo))

                        .TransactionDate = IIf(reader.IsDBNull(_TrDt), New DateTime(2000, 1, 1), reader.GetDateTime(_TrDt))
                        .TransactionId = IIf(reader.IsDBNull(_TrID), String.Empty, reader.GetString(_TrID))
                        .ReffNomor = IIf(reader.IsDBNull(_ReffNo), String.Empty, reader.GetString(_ReffNo))
                        .ReffDate = IIf(reader.IsDBNull(_ReffDt), New DateTime(2000, 1, 1), reader.GetDateTime(_ReffDt))
                        .TransactionDesc = IIf(reader.IsDBNull(_ket), String.Empty, reader.GetString(_ket))
                        .ReffDesc = IIf(reader.IsDBNull(_Jenis_Reff), String.Empty, reader.GetString(_Jenis_Reff))
                    End With
                    _read = True
                End If
                result.JournalItems.Add(New JournalVoucherItem(result.CompanyId,
                        result.BranchId,
                        IIf(reader.IsDBNull(_seq), 0, reader.GetInt32(_seq)),
                        IIf(reader.IsDBNull(_coaId), String.Empty, reader.GetString(_coaId)),
                        IIf(reader.IsDBNull(_coaName), String.Empty, reader.GetString(_coaName)),
                        IIf(reader.IsDBNull(_tr_desc), String.Empty, reader.GetString(_tr_desc)),
                        IIf(reader.IsDBNull(_post), String.Empty, reader.GetString(_post)),
                        IIf(reader.IsDBNull(_amount), 0, reader.GetDecimal(_amount)),
                     ""))

            End While

            Return result
        End If

        Return Nothing
    End Function


    Public Function DownloadToDBF(ByVal cnn As String, period1 As DateTime, period2 As DateTime, branch As String) As DataTable
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim ds As DataSet

        params(0) = New SqlParameter("@Periode1", SqlDbType.DateTime)
        params(0).Value = period1

        params(1) = New SqlParameter("@Periode2", SqlDbType.DateTime)
        params(1).Value = period2

        params(2) = New SqlParameter("@Branchid", SqlDbType.Char, 3)
        params(2).Value = branch

        ds = SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, "spExportDBFSave", params)

        Return ds.Tables(0)
    End Function


#Region "csv upload"
    Public Sub ValidateCsvComponen(ByVal cnn As String, ByVal component As IList(Of GlJournalVoucher))
        Dim spName = "spGlVoucherDetailCsvValidate"
        Dim params() As SqlParameter = New SqlParameter(4) {}

        Dim param As SqlParameter
        Dim m_connection = New SqlConnection(cnn)
        If m_connection.State = ConnectionState.Closed Then m_connection.Open()


        Dim sqlcommand = New SqlCommand()
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.CommandText = spName
        sqlcommand.Connection = m_connection
        For Each e In component
            For Each item In e.JournalItems
                sqlcommand.Parameters.Clear()
                param = New SqlParameter("@Branchid", SqlDbType.Char)
                param.Value = CType(e.BranchId, String)
                sqlcommand.Parameters.Add(param)

                param = New SqlParameter("@CoAId", SqlDbType.Char)
                param.Value = CType(item.CoaId, String)
                sqlcommand.Parameters.Add(param)

                param = New SqlParameter("@PaymentAllocationID", SqlDbType.Char)
                param.Value = CType(item.PaymentAllocationId, String)
                sqlcommand.Parameters.Add(param)

                'param = New SqlParameter("@AddFreeNo", SqlDbType.Char)
                'param.Value = CType(item.AddFree, String)
                'sqlcommand.Parameters.Add(param)

                param = New SqlParameter("@TransactionId", SqlDbType.Char)
                param.Value = CType(item.TransactionId, String)
                sqlcommand.Parameters.Add(param)

                param = New SqlParameter("@result", SqlDbType.VarChar, 500)
                param.Direction = ParameterDirection.Output
                sqlcommand.Parameters.Add(param)

                sqlcommand.ExecuteNonQuery()
                item.Message = param.Value.ToString()

            Next
        Next
    End Sub

    Public Function GlJournalAddUpdate(ByVal cnn As String, isAdd As Boolean, ByVal component As IList(Of GlJournalVoucher)) As String
        Dim _yearPrd = New GlYearPeriodDataAccess()


        If (component Is Nothing) Then
            Throw New Exception("Invalid Journal Voucher Parameter")
        End If

        Dim vochr = component(0)

        Dim prds = _yearPrd.YearPeriods(cnn, vochr.CompanyId, vochr.BranchId, vochr.BusinessDate.Year, vochr.TransactionDate.Month)

        'If (prds.Count = 0) Then
        '    Throw New Exception(String.Format("Periode Journal tidak terdifinisi.", vochr.BusinessDate.Year))
        'End If

        For Each v In component
            v.ValidateGlPeriod(prds)
        Next

        Dim spName As String = IIf(isAdd, "spGlVoucherCsvUpload", "spGlVoucherUpdate")
        Dim table = component.ToDataTable()

        Return executeParamTable(cnn, table, spName, "@glVouchers", component(0).ReffNomor)

    End Function
    Public Function GlJournalAddDelete(ByVal cnn As String, isAdd As Boolean, ByVal component As IList(Of GlJournalVoucher)) As String
        Dim _yearPrd = New GlYearPeriodDataAccess()


        If (component Is Nothing) Then
            Throw New Exception("Invalid Journal Voucher Parameter")
        End If

        Dim vochr = component(0)

        Dim prds = _yearPrd.YearPeriods(cnn, vochr.CompanyId, vochr.BranchId, vochr.TransactionDate.Year, vochr.TransactionDate.Month)

        'If (prds.Count = 0) Then
        '    Throw New Exception(String.Format("Periode Journal Tahun {0} tidak terdifinisi.", vochr.BusinessDate.Year))
        'End If

        For Each v In component
            v.ValidateGlPeriod(prds)
        Next

        Dim spName As String = IIf(isAdd, "spGlVoucherCsvUpload", "spGlVoucherDelete")
        Dim table = component.ToDataTable()

        Return executeParamTable(cnn, table, spName, "@glVouchers", component(0).ReffNomor)

    End Function


    Public Function DoUploadCsvGlJournals(ByVal cnn As String, ByVal component As IList(Of GlJournalVoucher)) As String
        Dim table = component.ToDataTable()
        Return executeParamTable(cnn, table, "spGlVoucherCsvUpload", "@glVouchers")
    End Function

#End Region


    Function executeParamTable(ByVal cnn As String, ByVal table As DataTable, spName As String, paramTbl As String, Optional ByVal refno As String = Nothing) As String


        Dim param As SqlParameter
        Dim m_connection = New SqlConnection(cnn)
        If m_connection.State = ConnectionState.Closed Then m_connection.Open()

        Dim sqlcommand = New SqlCommand()
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.CommandText = spName
        sqlcommand.Connection = m_connection


        param = New SqlParameter(paramTbl, SqlDbType.Structured)
        param.Value = table
        sqlcommand.Parameters.Add(param)

        If (refno <> Nothing) Then
            param = New SqlParameter("@refNo", SqlDbType.VarChar, 200)
            param.Value = refno
            sqlcommand.Parameters.Add(param)
        End If


        param = New SqlParameter("@errMsg", SqlDbType.VarChar, 500)
        param.Direction = ParameterDirection.Output
        sqlcommand.Parameters.Add(param)

        sqlcommand.ExecuteNonQuery()
        Dim message = param.Value.ToString()
        Return message

    End Function

    Public Function IsJournals(ByVal cnn As String, ByVal trDate As Date) As Boolean
        Dim query = "select top 1 1 from GLJournalD d left join GLJournalH h on d.CompanyID=h.CompanyID  and d.BranchID=h.BranchID and d.Tr_Nomor=h.Tr_Nomor where  Tr_Date=@trDate and isnull(h.Status,'')='' and CoaId is not null"
        Dim ret = Convert.ToInt32(SqlHelper.ExecuteScalar(cnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@trDate ", trDate)}))
        Return ret > 0
    End Function


#Region "Daily POsting"
    Function getPostScheduleDate(cnn As String, compId As String, branchId As String) As DateTime()
        Dim result() As DateTime = New DateTime(2) {New DateTime(1900, 1, 1), New DateTime(1900, 1, 1), New DateTime(1900, 1, 1)}

        Dim query = "select PrevPostDate , CurrPostDate, NextPostDate from glschedulepostingdate where  CompanyId=@CompanyId and  BranchId=@BranchId"
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@CompanyId", compId))
        params.Add(New SqlParameter("@BranchId", branchId))

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.Text, query, params.ToArray)
        If (reader.HasRows) Then
            Dim prevPostDate = reader.GetOrdinal("PrevPostDate")
            Dim currPostDate = reader.GetOrdinal("CurrPostDate")
            Dim nextPostDate = reader.GetOrdinal("NextPostDate")


            reader.Read()

            If (Not reader.IsDBNull(0)) Then
                result(0) = reader.GetDateTime(prevPostDate)
            End If

            If (Not reader.IsDBNull(1)) Then
                result(1) = reader.GetDateTime(currPostDate)
            End If
            If (Not reader.IsDBNull(2)) Then
                result(2) = reader.GetDateTime(nextPostDate)
            End If
        End If
        Return result
    End Function

    Public Function GetGlJournalAvailablePost(ByRef totalrec As Integer, ByVal strCnn As String, ByVal ParamArray param As Object()) As GlJournalPostAvailable ' IList(Of GlJournalPostAvailable)
        Dim _yearPrd = New GlYearPeriodDataAccess()
        Dim result = New GlJournalPostAvailable() 'List(Of GlJournalPostAvailable)
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        Dim _comp = param(0)
        Dim _branch = param(1)

        Dim m_year = 0
        Dim m_month = 0

        params.Add(New SqlParameter("@companyId", _comp))
        params.Add(New SqlParameter("@branchId", _branch))


        If (param.Length > 2) Then
            m_year = CType(param(2), DateTime).Year
            m_month = CType(param(2), DateTime).Month
            params.Add(New SqlParameter("@tr_date", param(2)))
        Else
            Dim postDate = getPostScheduleDate(strCnn, _comp, _branch)
            m_year = postDate(1).Year
            result.SetPostSchedule = postDate
        End If
 

        Dim isyearOk = _yearPrd.IsYearValid(strCnn, m_year, _comp, _branch)
        If (isyearOk) Then
            Dim prds = _yearPrd.YearPeriods(strCnn, _comp, _branch, m_year,m_month)
            result.SetValidateGlPeriod(prds)
        End If


        result.IsNoTrans = True
          
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(strCnn, CommandType.StoredProcedure, "spGlJournalPostAvailable", params.ToArray)
        If (reader.HasRows) Then
            result.IsNoTrans = False
            

            Dim tr_date = reader.GetOrdinal("tr_date")
            Dim journals = reader.GetOrdinal("journals")
            Dim debit = reader.GetOrdinal("debit")
            Dim credit = reader.GetOrdinal("credit")
            Dim open = reader.GetOrdinal("op")

            reader.Read()
            result.SetGlJournalPostAvailable(
                       IIf(reader.IsDBNull(tr_date), New DateTime(1900, 1, 1), reader.GetDateTime(tr_date)),
                       IIf(reader.IsDBNull(journals), String.Empty, reader.GetString(journals)),
                       IIf(reader.IsDBNull(debit), 0, reader.GetDecimal(debit)),
                        IIf(reader.IsDBNull(credit), 0, reader.GetDecimal(credit)),
                        IIf(reader.IsDBNull(open), 0, reader.GetInt32(open)))

        End If

        Return result
    End Function


    Public Function GetGlJournalAvailablePostPage(ByVal cnn As String,
                                                  ByVal companyId As String,
                                                  ByVal branchId As String,
                                                  ByVal trDateFrom As DateTime,
                                                  ByVal trDateTo As DateTime,
                                                  pageSz As Integer, currPg As Integer, ByRef totalReq As Int64) As IList(Of JournalApproval)


        Dim _yearPrd = New GlYearPeriodDataAccess()
        Dim isyearOk = _yearPrd.IsYearValid(cnn, trDateFrom.Year, companyId, branchId)

        If Not (isyearOk) Then
            Throw New Exception(String.Format("Tahun Journal (Table GLYEAR) - {0} tidak ditemukan..", trDateFrom.Year))
        End If

        Dim prds = _yearPrd.YearPeriods(cnn, companyId, branchId, trDateFrom.Year, trDateFrom.Month)

        If IsNothing(prds) Then
            Throw New Exception("Periode Journal tidak ditemukan/Generate..")
        End If

        Dim prd = prds.SingleOrDefault(Function(r) r.IsInRange(trDateFrom))
        If (IsNothing(prd)) Then
            Throw New Exception("Periode Journal tidak ditemukan/Generate..")
        End If

        If (prd.Status = PeriodStatus.Close) Then
            Throw New Exception(String.Format("Journal Periode {0} {1} telah di close", trDateFrom.ToString("MMMM"), trDateFrom.Year))
        End If

        Dim result As IList(Of JournalApproval) = New List(Of JournalApproval)()
        Dim reader As SqlDataReader
        Dim m_connection = New SqlConnection(cnn)
        If m_connection.State = ConnectionState.Closed Then m_connection.Open()
        Dim sqlcommand = New SqlCommand()
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.CommandText = "sp_GlVoucherPostPageList"
        sqlcommand.Connection = m_connection

        If (branchId <> "0") Then
            sqlcommand.Parameters.Add(New SqlParameter("@branchid", branchId))
        End If

        sqlcommand.Parameters.Add(New SqlParameter("@companyid", companyId))

        sqlcommand.Parameters.Add(New SqlParameter("@trDateF", trDateFrom))
        sqlcommand.Parameters.Add(New SqlParameter("@trDateTo", trDateTo))
        sqlcommand.Parameters.Add(New SqlParameter("@currPage", currPg))
        sqlcommand.Parameters.Add(New SqlParameter("@pageSize", pageSz))
        Dim prm = New SqlParameter("@totalRecords", SqlDbType.Int)
        prm.Direction = ParameterDirection.Output
        sqlcommand.Parameters.Add(prm)
        Try
            reader = sqlcommand.ExecuteReader()
            If (reader.HasRows) Then
                If (reader Is Nothing) Then
                    Throw New ArgumentNullException()
                End If
                If (reader.IsClosed) Then
                    Throw New InvalidOperationException("This datareader cannot be read from.  It is closed.")
                End If
                result = JournalApproval.Builder(reader)
                reader.Close()
                totalReq = CType(prm.Value, Int32)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            m_connection.Close()
        End Try
        Return result

    End Function


    Public Function DoDailyJournalPost(ByVal cnn As String, ByVal ParamArray param As Object()) As String
        Dim spName As String = "spGlVoucherDailyPost"

        'Dim params() As SqlParameter = New SqlParameter(1) {}

        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)


        params.Add(New SqlParameter("@companyId", param(0)))
        params.Add(New SqlParameter("@branchId", param(1)))


        params.Add(New SqlParameter("@Description", param(2)))
        params.Add(New SqlParameter("@userid", param(3)))

        params.Add(New SqlParameter("@skipDate", param(4)))
        If (param.Count > 5) Then
            Dim prm = New SqlParameter("@trDate", SqlDbType.DateTime)
            prm.Value = param(5)
            params.Add(prm)
        End If
         
        Dim pmsg = New SqlParameter("@errMsg", SqlDbType.VarChar, 500)
        pmsg.Direction = ParameterDirection.Output
        params.Add(pmsg)

        SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, spName, params.ToArray)
        Return pmsg.Value.ToString()

    End Function
     

    Public Function DoJournalMonthlyClose(ByVal cnn As String, ByVal ParamArray param As Object()) As String
        Dim spName As String = "spGlMonthlyClose" 
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter) 
        params.Add(New SqlParameter("@PeriodId", param(0)))
        params.Add(New SqlParameter("@userid", param(1)))  
        Dim pmsg = New SqlParameter("@errMsg", SqlDbType.VarChar, 500)
        pmsg.Direction = ParameterDirection.Output
        params.Add(pmsg) 
        SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, spName, params.ToArray)
        Return pmsg.Value.ToString() 
    End Function
     
    Public Function DoJournalYearlyClose(ByVal cnn As String, ByVal companyId As String, ByVal userId As String) As String
        Dim _yearPrd = New GlYearPeriodDataAccess()
        _yearPrd.NextYearPeriodGenerate(cnn, companyId)

        Dim spName As String = "spGlClosingYear"
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@year", userId))
        'Dim pmsg = New SqlParameter("@errMsg", SqlDbType.VarChar, 500)
        'pmsg.Direction = ParameterDirection.Output
        'params.Add(pmsg)
        SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, spName, params.ToArray)
        Return companyId 'pmsg.Value.ToString() 
    End Function

#End Region

    Public Function GlJournalHold(ByVal cnn As String, ByVal component As IList(Of GlJournalVoucher)) As String
        Dim spName As String = "spGlVoucherHold"
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@refNo", SqlDbType.VarChar, 20)
        params(0).Value = component(0).TransactionNomor

        params(1) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
        params(1).Value = component(0).BranchId

        params(2) = New SqlParameter("@companyid", SqlDbType.VarChar, 3)
        params(2).Value = component(0).CompanyId

        params(3) = New SqlParameter("@errMsg", SqlDbType.VarChar, 500)
        params(3).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, spName, params)
        Return params(3).Value.ToString()

    End Function


#Region "BUDGETING"


    Public Function GetGlInitialFindOne(ByVal strCnn As String, ByVal param As InitialBudget) As InitialBudget
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        params.Add(New SqlParameter("@CompanyId", param.CompanyId))
        params.Add(New SqlParameter("@BranchId", param.BranchId))
        params.Add(New SqlParameter("@year", param.Year))
        params.Add(New SqlParameter("@CoaId", param.CoAId))


        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(strCnn, CommandType.StoredProcedure, "spGlBudgetFindOne", params.ToArray)

        If (reader.HasRows) Then
            Return readInitialBudgetFromReader(reader)
        End If

        Return Nothing

    End Function


    Public Function GlInitialBudgetAdd(ByVal cnn As String, ByVal component As IList(Of InitialBudget)) As String
        If (Not GetGlInitialFindOne(cnn, component(0)) Is Nothing) Then
            Return String.Format("Budget COA ID{0} ({1}) tahun {2} Sudah di initialisasi.", component(0).CoAId, component(0).CoaName, component(0).Year)
        End If

        Return GlInitialBudgetUpdate(cnn, component)

    End Function

    Public Function GlInitialBudgetUpdate(ByVal cnn As String, ByVal component As IList(Of InitialBudget)) As String
        Dim table = component.ToDataTable()
        Return executeParamTable(cnn, table, "spGlBudgetUpdate", "@glBudgets")
    End Function

    Public Function GetGlInitialBudget(ByRef totalrec As Integer, ByVal strCnn As String, ByVal ParamArray param As Object()) As IList(Of InitialBudget)
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        params.Add(New SqlParameter("@CompanyId", param(0)))
        params.Add(New SqlParameter("@BranchId", param(1)))
        params.Add(New SqlParameter("@year", CType(param(2), Integer)))
        params.Add(New SqlParameter("@currPage", CType(param(3), Integer)))
        params.Add(New SqlParameter("@pageSize", CType(param(4), Integer)))
        params.Add(New SqlParameter("@type", CType(param(5), Integer)))
        If (param.Length > 6) Then
            If (param(7) <> "") Then
                params.Add(New SqlParameter("@wOpt", CType(param(6), Integer)))
                params.Add(New SqlParameter("@where", param(7)))
            End If
        End If

        Dim prm = New SqlParameter("@totalRecords", SqlDbType.Int)
        prm.Direction = ParameterDirection.Output
        params.Add(prm)
        totalrec = 0

        Dim data As DataTable = SqlHelper.ExecuteDataset(strCnn, CommandType.StoredProcedure, "spGLInitialBudget", params.ToArray).Tables(0)
        Dim list As IList(Of InitialBudget) = (From item As DataRow In data.Rows Select New InitialBudget(item("CompanyId"), item("BranchId"), item("Year"), item("CoAId"), item("CoaName"), item("Jan"), item("Feb"), item("Mar"), item("Apr"), item("Mei"), item("Jun"), item("Jul"), item("Agt"), item("Sep"), item("Okt"), item("Nov"), item("Des"))).ToList()
        If (data.Rows.Count > 0) Then
            totalrec = CType(data.Rows(0)("rows"), Int64) 'CType(prm.Value, Int64)
        End If

        Return list
    End Function
#End Region

    Private Function readInitialBudgetFromReader(reader As IDataReader) As InitialBudget

        If (reader Is Nothing) Then
            Throw New ArgumentNullException("Datareader object cannot be null.")
        End If
        If (reader.IsClosed) Then
            Throw New InvalidOperationException("This datareader cannot be read from.  It is closed.")
        End If

        Dim result = New List(Of InitialBudget)

        Dim companyIdColumn = reader.GetOrdinal("CompanyId")
        Dim BranchIdColumn = reader.GetOrdinal("BranchId")
        Dim YearColumn = reader.GetOrdinal("Year")
        Dim CoAIdColumn = reader.GetOrdinal("CoAId")
        Dim CoaNameColumn = reader.GetOrdinal("CoaName")
        Dim JanColumn = reader.GetOrdinal("Jan")
        Dim FebColumn = reader.GetOrdinal("Feb")
        Dim MarColumn = reader.GetOrdinal("Mar")
        Dim AprColumn = reader.GetOrdinal("Apr")
        Dim MeiColumn = reader.GetOrdinal("Mei")
        Dim JunColumn = reader.GetOrdinal("Jun")
        Dim JulColumn = reader.GetOrdinal("Jul")
        Dim AgtColumn = reader.GetOrdinal("Agt")
        Dim SepColumn = reader.GetOrdinal("Sep")
        Dim OktColumn = reader.GetOrdinal("Okt")
        Dim NovColumn = reader.GetOrdinal("Nov")
        Dim DesColumn = reader.GetOrdinal("Des")


        While (reader.Read())
            result.Add(New InitialBudget(IIf(reader.IsDBNull(companyIdColumn), String.Empty, reader.GetString(companyIdColumn)),
                                        IIf(reader.IsDBNull(BranchIdColumn), String.Empty, reader.GetString(BranchIdColumn)),
                                        IIf(reader.IsDBNull(YearColumn), 0, reader.GetInt32(YearColumn)),
                                        IIf(reader.IsDBNull(CoAIdColumn), String.Empty, reader.GetString(CoAIdColumn)),
                                        IIf(reader.IsDBNull(CoaNameColumn), String.Empty, reader.GetString(CoaNameColumn)),
                                        IIf(reader.IsDBNull(JanColumn), 0, reader.GetDecimal(JanColumn)),
                                        IIf(reader.IsDBNull(FebColumn), 0, reader.GetDecimal(FebColumn)),
                                        IIf(reader.IsDBNull(MarColumn), 0, reader.GetDecimal(MarColumn)),
                                        IIf(reader.IsDBNull(AprColumn), 0, reader.GetDecimal(AprColumn)),
                                        IIf(reader.IsDBNull(MeiColumn), 0, reader.GetDecimal(MeiColumn)),
                                        IIf(reader.IsDBNull(JunColumn), 0, reader.GetDecimal(JunColumn)),
                                        IIf(reader.IsDBNull(JulColumn), 0, reader.GetDecimal(JulColumn)),
                                        IIf(reader.IsDBNull(AgtColumn), 0, reader.GetDecimal(AgtColumn)),
                                        IIf(reader.IsDBNull(SepColumn), 0, reader.GetDecimal(SepColumn)),
                                        IIf(reader.IsDBNull(OktColumn), 0, reader.GetDecimal(OktColumn)),
                                        IIf(reader.IsDBNull(NovColumn), 0, reader.GetDecimal(NovColumn)),
                                        IIf(reader.IsDBNull(DesColumn), 0, reader.GetDecimal(DesColumn))))

        End While

        Return result.FirstOrDefault()
    End Function


#Region "Virifikasi"


    Public Function GlJournalVerifyPage(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64, ByVal wOpt As Integer, ByVal cmdwhere As String) As IList(Of JournalApproval)
        Dim result As IList(Of JournalApproval) = New List(Of JournalApproval)()
        Dim reader As SqlDataReader
        Dim m_connection = New SqlConnection(cnn)
        If m_connection.State = ConnectionState.Closed Then m_connection.Open()
        Dim sqlcommand = New SqlCommand()
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.CommandText = "sp_GlVoucherVerifyPageList"
        sqlcommand.Connection = m_connection

        sqlcommand.Parameters.Add(New SqlParameter("@companyid", param.CompanyId))
        sqlcommand.Parameters.Add(New SqlParameter("@branchId", param.BranchId))
        sqlcommand.Parameters.Add(New SqlParameter("@trDate", param.TransactionDate))
        sqlcommand.Parameters.Add(New SqlParameter("@isValid", param.TransactionType))

        sqlcommand.Parameters.Add(New SqlParameter("@currPage", currPg))
        sqlcommand.Parameters.Add(New SqlParameter("@pageSize", pageSz))
        Dim prm = New SqlParameter("@totalRecords", SqlDbType.Int)
        prm.Direction = ParameterDirection.Output
        sqlcommand.Parameters.Add(prm)
        sqlcommand.Parameters.Add(New SqlParameter("@wOpt", wOpt))
        sqlcommand.Parameters.Add(New SqlParameter("@cmdwhere", cmdwhere))
        Try
            reader = sqlcommand.ExecuteReader()
            If (reader.HasRows) Then
                If (reader Is Nothing) Then
                    Throw New ArgumentNullException()
                End If
                If (reader.IsClosed) Then
                    Throw New InvalidOperationException("This datareader cannot be read from.  It is closed.")
                End If
                result = JournalApproval.Builder(reader)
                reader.Close()
                totalReq = CType(prm.Value, Int32)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            m_connection.Close()
        End Try
        Return result

    End Function
#End Region



#Region "APPROVAL"

    Public Function GlJournalApprovalPage(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64) As IList(Of JournalApproval)
        Dim result As IList(Of JournalApproval) = New List(Of JournalApproval)()
        Dim reader As SqlDataReader
        Dim m_connection = New SqlConnection(cnn)
        If m_connection.State = ConnectionState.Closed Then m_connection.Open()
        Dim sqlcommand = New SqlCommand()
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.CommandText = "sp_GlVoucherApprovalPageList"
        sqlcommand.Connection = m_connection


        sqlcommand.Parameters.Add(New SqlParameter("@companyid", param.CompanyId))
        sqlcommand.Parameters.Add(New SqlParameter("@branchid", param.BranchId))
        sqlcommand.Parameters.Add(New SqlParameter("@trDate", param.TransactionDate))
        sqlcommand.Parameters.Add(New SqlParameter("@currPage", currPg))
        sqlcommand.Parameters.Add(New SqlParameter("@pageSize", pageSz))
        Dim prm = New SqlParameter("@totalRecords", SqlDbType.Int)
        prm.Direction = ParameterDirection.Output
        sqlcommand.Parameters.Add(prm)
        Try
            reader = sqlcommand.ExecuteReader()
            If (reader.HasRows) Then
                If (reader Is Nothing) Then
                    Throw New ArgumentNullException()
                End If
                If (reader.IsClosed) Then
                    Throw New InvalidOperationException("This datareader cannot be read from.  It is closed.")
                End If
                result = JournalApproval.Builder(reader)
                reader.Close()
                totalReq = CType(prm.Value, Int32)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            m_connection.Close()
        End Try
        Return result

    End Function

    Public Function GlJournalVerify(ByVal cnn As String, ByVal component As IList(Of String)) As String
        Dim table = component.ToDataTable()
        Return executeParamTable(cnn, table, "sp_GlVoucherVerify", "@transactions")
    End Function

    Public Function GlJournalApproval(ByVal cnn As String, ByVal component As IList(Of String)) As String
        Dim table = component.ToDataTable()
        Return executeParamTable(cnn, table, "sp_GlVoucherApproval", "@transactions")
    End Function


    Public Function GlJournalPost(ByVal cnn As String, ByVal component As IList(Of String)) As String
        Dim table = component.ToDataTable()
        Return executeParamTable(cnn, table, "sp_GlVoucherPost", "@transactions")
    End Function

    Public Function GlJournalApproval(ByVal cnn As String, ByVal param As JournalApproval, Optional ByVal approve As Boolean = False) As JournalApproval
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@companyid", param.CompanyId))
        params.Add(New SqlParameter("@branchid", param.BranchId))
        params.Add(New SqlParameter("@trDate", param.TransactionDate))

        If (approve = True) Then
            params.Add(New SqlParameter("@isApprove", 1))
        End If

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.StoredProcedure, "sp_GlVoucherApprovalList", params.ToArray)

        If (approve = True) Then
            Return param
        End If
        Dim result = param

        If (reader.HasRows) Then
            If (reader Is Nothing) Then
                Throw New ArgumentNullException()
            End If
            If (reader.IsClosed) Then
                Throw New InvalidOperationException("This datareader cannot be read from.  It is closed.")
            End If


            Dim xxx = JournalApproval.Builder(reader)
        End If

        glImbalanceJournalApprovalChecK(cnn, result)

        Return result
    End Function



    Private Sub glImbalanceJournalApprovalChecK(ByVal cnn As String, param As JournalApproval)

        If param.IsBalance = False Then
            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
            params.Add(New SqlParameter("@companyid", param.CompanyId))
            params.Add(New SqlParameter("@branchid", param.BranchId))
            params.Add(New SqlParameter("@trDate", param.TransactionDate))

            Dim imtr = param.ImbalanceTrNo
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.StoredProcedure, "sp_GlVoucherImbalanceApproval", params.ToArray)


            If (reader.HasRows) Then
                If (reader Is Nothing) Then
                    'Throw New ArgumentNullException()
                    Return
                End If
                If (reader.IsClosed) Then
                    'Throw New InvalidOperationException("This datareader cannot be read from.  It is closed.") 
                    Return
                End If

                Dim _cID = reader.GetOrdinal("Value")
                Dim _desc = reader.GetOrdinal("Text")


                While (reader.Read())
                    imtr.Add(New ValueTextObject(IIf(reader.IsDBNull(_cID), String.Empty, reader.GetString(_cID)),
                                                           IIf(reader.IsDBNull(_desc), "0", reader.GetDecimal(_desc).ToString())))
                End While
            End If
        End If

    End Sub
#End Region

#Region "DBF BALANCE UPLOAD"



    Public Function GLGelMASValidateCOASelect(ByRef totalrec As Integer, ByVal strCnn As String, ByVal ParamArray param() As Object) As IList(Of ValueTextObject)
        Try

            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

            params.Add(New SqlParameter("@CompanyId", param(0)))
            params.Add(New SqlParameter("@BranchId", param(1)))
            params.Add(New SqlParameter("@currPage", CType(param(2), Integer)))
            params.Add(New SqlParameter("@pageSize", CType(param(3), Integer)))



            Dim prm = New SqlParameter("@totalRecords", SqlDbType.Int)
            prm.Direction = ParameterDirection.Output
            params.Add(prm)
            totalrec = 0
            Dim prms As SqlParameter() = params.ToArray
            Dim data As DataTable = SqlHelper.ExecuteDataset(strCnn, CommandType.StoredProcedure, "GLGelMASValidateCOAPage_Select", prms).Tables(0)
            totalrec = CType(prm.Value, Int64)


            Dim list As IList(Of ValueTextObject) = (From item As DataRow In data.Rows
                                                     Select New ValueTextObject(item("CoAId"), item("description"))).ToList()

            Return list
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try


    End Function

    Public Function GLHistoryCOASelect(ByRef totalrec As Integer, ByVal strCnn As String, ByVal ParamArray param() As Object) As DataView
        Try
            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
            params.Add(New SqlParameter("@currPage", CType(param(0), Integer)))
            params.Add(New SqlParameter("@pageSize", CType(param(1), Integer)))
            Dim prm = New SqlParameter("@totalRecords", SqlDbType.Int)
            prm.Direction = ParameterDirection.Output
            params.Add(prm)
            totalrec = 0
            Dim prms As SqlParameter() = params.ToArray
            Dim data As DataTable = SqlHelper.ExecuteDataset(strCnn, CommandType.StoredProcedure, "GLGELMASHISTORYPage_Select", prms).Tables(0)
            totalrec = CType(prm.Value, Int64)
            Return data.DefaultView
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function GetHifrontDetails(cnn As String, dateProc As Date) As System.Text.StringBuilder
        Try
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.StoredProcedure, "sp_HiFrontsDtlList", New SqlParameter("@date", dateProc)) 
            Dim builder = New System.Text.StringBuilder()

            'Dim data As DataTable = SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, "sp_HiFrontsDtlList", New SqlParameter("@date", dateProc)).Tables(0)
            'For Each item As DataRow In data.Rows
            '    builder.AppendLine(   IIf(reader.IsDBNull(_cID), String.Empty, reader.GetString(_cID) item("data"))
            'Next

            builder.AppendLine("*DocNo	Type	CompID	DocDate	PostDate	FiscalPriod	RptDate	DocType	Cur	RefDoc	HdrTxt	Leger	CalTax	ExchRate	Line	PostKey	GL	CustID	AccID	SGL	TGL	TransType	AmtDocCur	AmtLocCur	AmtLocCur2	SalesTax	TaxBaseAmtDoc	TaxAmt	TaxBaseAmtLoc	TaxAmtLoc	Segment	Business	Profit	Cost	OrderNo	GnG	FreeInfo	Country	CustNo	VendorNo	Comm3	Comm4	Comm5	Comp1	Comp2	Comp3	Comp4	Comp5	Comp6	Comp7	Comp8	Comp9	Comp10	BusRef1	BusRef2	RefKey4Line	InvRef	FiscalYear	LineItem	ContrNo	ContrType	PayRef	ValueDate	BsLineDate	TermsOfPay	Day1	CashDisc1	Day2	CashDisc2	NetPay	DiscBaseAmt	DiscAmt	PayMethod	ShortKey4Bank	PartBankType	PayBlok	ReasonPay	SCB	SuppCountry	ExtOrderNo	AssNo	ItemText	Qty	BsUnit	CompCode4NextLine	Indi4Hold1	HoldTaxCode1	HoldTaxBase1	HoldAmt1	Indi4Hold2	HoldTaxCode2	HoldTaxBase2	HoldAmt2	Indi4Hold3	HoldTaxCode3	HoldTaxBase3	HoldAmt3	Indi4Hold4	HoldTaxCode4	HoldTaxBase4	HoldAmt4	TaxJurisCode	BranchId	NumOfPageInv	NegativPost	BusPlace	SectCode	PayMethod	Payer	Flag	Plan	Cur4AutoPay	Part1	Part2	Part3	WBS	LongText")

            If (reader.HasRows) Then
                If (reader Is Nothing) Then 
                    Return builder
                End If
                If (reader.IsClosed) Then 
                    Return builder
                End If

                Dim _cID = reader.GetOrdinal("data")  
                While (reader.Read())
                    If (reader.IsDBNull(_cID)) Then
                        Continue While
                    End If
                    builder.AppendLine(reader.GetString(_cID))
                End While
            End If


            Return builder

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    'Public Function GlGelMasUpload(ByVal cnn As String, ByVal component As GelMas, Optional validate As Boolean = False, Optional upload As Boolean = False) As String

    '    Dim param As SqlParameter
    '    Dim mConnection = New SqlConnection(cnn)
    '    If mConnection.State = ConnectionState.Closed Then mConnection.Open()

    '    Dim sqlcommand = New SqlCommand()
    '    sqlcommand.CommandType = CommandType.StoredProcedure
    '    sqlcommand.CommandText = IIf(validate = True, "spGlGelMasValidate", "spGlGelMasUpload")

    '    sqlcommand.Connection = mConnection
    '    sqlcommand.Parameters.Add(New SqlParameter("@CompanyId", component.CompanyId))
    '    sqlcommand.Parameters.Add(New SqlParameter("@BranchId", component.BranchId))
    '    If (validate = False) Then
    '        sqlcommand.Parameters.Add(New SqlParameter("@year", component.Year))
    '    End If

    '    If (upload = True) Then
    '        sqlcommand.Parameters.Add(New SqlParameter("@history", 0))
    '    End If
    '    sqlcommand.Parameters.Add(New SqlParameter("@user", component.User))

    '    param = New SqlParameter("@glGelMas", SqlDbType.Structured)
    '    If (upload = False) Then
    '        param.Value = component.ToDataTable
    '    Else
    '        param.Value = Nothing
    '    End If

    '    sqlcommand.Parameters.Add(param)

    '    param = New SqlParameter("@errMsg", SqlDbType.VarChar, 500)
    '    param.Direction = ParameterDirection.Output
    '    sqlcommand.Parameters.Add(param)

    '    sqlcommand.ExecuteNonQuery()
    '    Dim message = param.Value.ToString()
    '    Return message

    'End Function
#End Region

#Region "UnPost"
    Public Function GlJournalUnPostPage(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64) As IList(Of JournalApproval)
        Dim result As IList(Of JournalApproval) = New List(Of JournalApproval)()
        Dim reader As SqlDataReader
        Dim m_connection = New SqlConnection(cnn)
        If m_connection.State = ConnectionState.Closed Then m_connection.Open()
        Dim sqlcommand = New SqlCommand()
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.CommandText = "sp_GlVoucherUnPostPageList"
        sqlcommand.Connection = m_connection


        sqlcommand.Parameters.Add(New SqlParameter("@companyid", param.CompanyId))
        sqlcommand.Parameters.Add(New SqlParameter("@branchid", param.BranchId))
        sqlcommand.Parameters.Add(New SqlParameter("@trDate", param.TransactionDate))
        sqlcommand.Parameters.Add(New SqlParameter("@currPage", currPg))
        sqlcommand.Parameters.Add(New SqlParameter("@pageSize", pageSz))
        Dim prm = New SqlParameter("@totalRecords", SqlDbType.Int)
        prm.Direction = ParameterDirection.Output
        sqlcommand.Parameters.Add(prm)
        Try
            reader = sqlcommand.ExecuteReader()
            If (reader.HasRows) Then
                If (reader Is Nothing) Then
                    Throw New ArgumentNullException()
                End If
                If (reader.IsClosed) Then
                    Throw New InvalidOperationException("This datareader cannot be read from.  It is closed.")
                End If
                result = JournalApproval.Builder(reader)
                reader.Close()
                totalReq = CType(prm.Value, Int32)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            m_connection.Close()
        End Try
        Return result

    End Function
    Public Function GlJournalUnPost(ByVal cnn As String, ByVal component As IList(Of String)) As String
        Dim table = component.ToDataTable()
        Return executeParamTable(cnn, table, "sp_GlVoucherUnPost", "@transactions", )
    End Function
    Public Function GlJournalUnPostPageFilter(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64, ByVal wOpt As Integer, ByVal cmdwhere As String) As IList(Of JournalApproval)
        Dim result As IList(Of JournalApproval) = New List(Of JournalApproval)()
        Dim reader As SqlDataReader
        Dim m_connection = New SqlConnection(cnn)
        If m_connection.State = ConnectionState.Closed Then m_connection.Open()
        Dim sqlcommand = New SqlCommand()
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.CommandText = "sp_GlVoucherUnPostPageListFilter"
        sqlcommand.Connection = m_connection


        sqlcommand.Parameters.Add(New SqlParameter("@companyid", param.CompanyId))
        sqlcommand.Parameters.Add(New SqlParameter("@branchid", param.BranchId))
        sqlcommand.Parameters.Add(New SqlParameter("@trDate", param.TransactionDate))
        sqlcommand.Parameters.Add(New SqlParameter("@wOpt", wOpt))
        sqlcommand.Parameters.Add(New SqlParameter("@cmdwhere", cmdwhere))
        sqlcommand.Parameters.Add(New SqlParameter("@currPage", currPg))
        sqlcommand.Parameters.Add(New SqlParameter("@pageSize", pageSz))
        Dim prm = New SqlParameter("@totalRecords", SqlDbType.Int)
        prm.Direction = ParameterDirection.Output
        sqlcommand.Parameters.Add(prm)
        Try
            reader = sqlcommand.ExecuteReader()
            If (reader.HasRows) Then
                If (reader Is Nothing) Then
                    Throw New ArgumentNullException()
                End If
                If (reader.IsClosed) Then
                    Throw New InvalidOperationException("This datareader cannot be read from.  It is closed.")
                End If
                result = JournalApproval.Builder(reader)
                reader.Close()
                totalReq = CType(prm.Value, Int32)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            m_connection.Close()
        End Try
        Return result

    End Function
#End Region

    Public Function GlJournalPageNotSuccess(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64) As IList(Of JournalApproval)
        Dim result As IList(Of JournalApproval) = New List(Of JournalApproval)()
        Dim reader As SqlDataReader
        Dim m_connection = New SqlConnection(cnn)
        If m_connection.State = ConnectionState.Closed Then m_connection.Open()
        Dim sqlcommand = New SqlCommand()
        sqlcommand.CommandType = CommandType.StoredProcedure
        sqlcommand.CommandText = "spGlVoucherPageList"
        sqlcommand.Connection = m_connection

        sqlcommand.Parameters.Add(New SqlParameter("@companyid", param.CompanyId))

        sqlcommand.Parameters.Add(New SqlParameter("@currPage", currPg))
        sqlcommand.Parameters.Add(New SqlParameter("@pageSize", pageSz))
        Dim prm = New SqlParameter("@totalRecords", SqlDbType.Int)
        prm.Direction = ParameterDirection.Output
        sqlcommand.Parameters.Add(prm)
        Try
            reader = sqlcommand.ExecuteReader()
            If (reader.HasRows) Then
                If (reader Is Nothing) Then
                    Throw New ArgumentNullException()
                End If
                If (reader.IsClosed) Then
                    Throw New InvalidOperationException("This datareader cannot be read from.  It is closed.")
                End If
                result = JournalApproval.Builder(reader)
                reader.Close()
                totalReq = CType(prm.Value, Int32)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            m_connection.Close()
        End Try
        Return result

    End Function
End Class
