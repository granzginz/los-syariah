﻿Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Framework.SQLEngine

Public Class GlCurrencyDataAccess : Inherits DataAccessBase
     
    Public Function GetHomeCurrency(ByVal cnn As String) As GlCurrency
        Return _getGlCurrencies(cnn, " IsHomeCurrency=1").FirstOrDefault()
    End Function


    Public Function GetDefaultCurrency(ByVal cnn As String) As GlCurrency
        Return _getGlCurrencies(cnn, " IsDefault=1").FirstOrDefault()
    End Function

    Public Function GetGlCurrencies(ByVal cnn As String) As IList(Of GlCurrency)
        Return _getGlCurrencies(cnn, "")
    End Function

    Function _getGlCurrencies(ByVal cnn As String, where As String) As IList(Of GlCurrency)
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@wherecond", SqlDbType.VarChar)
        params(0).Value = where

        Try
            Dim data As DataTable = SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, "GLMasterCurrency_select", params).Tables(0)

            Dim list As IList(Of GlCurrency) = (From item As DataRow In data.Rows Select New GlCurrency With {
                                                         .CurrencyId = item("CurrencyId"),
                                                         .CurrencyCode = item("CurrencyCode"),
                                                         .CurrencyName = item("CurrencyName"),
                                                         .CurrencySymbol = item("CurrencySymbol"),
                                                         .CurrencyDecimalPlace = CType(item("CurrencyDecimalPlace"), Integer),
                                                         .IsBaseExchangeRate = CType(item("IsBaseExchangeRate"), Boolean),
                                                         .IsDefault = CType(item("IsDefault"), Boolean),
                                                         .IsHomeCurrency = CType(item("IsHomeCurrency"), Boolean),
                                                         .IsLocalCurrency = CType(item("IsLocalCurrency"), Boolean),
                                                         .MaxRate = CType(item("MaxRate"), Decimal),
                                                         .MinRate = CType(item("MinRate"), Decimal),
                                                         .CurrencyRounding = CType(item("CurrencyRounding"), Integer)
                                                         }).ToList()
            Return list
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Currency.CurrenciesList")
        End Try
    End Function
End Class
