
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class News
    Implements INews


    Public Sub MasterNewsAdd(ByVal customclass As Parameter.News) Implements [Interface].INews.MasterNewsAdd
        Dim DaMasterNewsAdd As New SQLEngine.AppMgt.MasterNews
        DaMasterNewsAdd.MasterNewsAdd(customclass)
    End Sub

    Public Sub MasterNewsDelete(ByVal customclass As Parameter.News) Implements [Interface].INews.MasterNewsDelete
        Dim DaMasterNewsDelete As New SQLEngine.AppMgt.MasterNews
        DaMasterNewsDelete.MasterNewsDelete(customclass)
    End Sub

    Public Sub MasterNewsEdit(ByVal customclass As Parameter.News) Implements [Interface].INews.MasterNewsEdit
        Dim DaMasterNewsEdit As New SQLEngine.AppMgt.MasterNews
        DaMasterNewsEdit.MasterNewsEdit(customclass)
    End Sub

    Public Function MasterNewsView(ByVal customclass As Parameter.News) As Parameter.News Implements [Interface].INews.MasterNewsView
        Dim DaMasterNewsView As New SQLEngine.AppMgt.MasterNews
        Return DaMasterNewsView.MasterNewsView(customclass)
    End Function

    Public Sub NewsDetailUpdate(ByVal customclass As Parameter.News) Implements [Interface].INews.NewsDetailUpdate
        Dim DaNewsDetailUpdate As New SQLEngine.AppMgt.MasterNews
        DaNewsDetailUpdate.NewsDetailUpdate(customclass)
    End Sub

    Public Function NewsDetailView(ByVal customclass As Parameter.News) As Parameter.News Implements [Interface].INews.NewsDetailView
        Dim DaNewsDetailView As New SQLEngine.AppMgt.MasterNews
        Return DaNewsDetailView.NewsDetailView(customclass)
    End Function
End Class