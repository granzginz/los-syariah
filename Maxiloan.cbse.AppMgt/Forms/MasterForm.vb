

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class MasterForm
    Implements IUserManagementMasterForm

    Public Function ListMasterForm(ByVal customclass As Parameter.MasterForm) As Parameter.MasterForm Implements [Interface].IUserManagementMasterForm.ListMasterForm
        Dim DaListMasterForm As New SQLEngine.AppMgt.MasterForm
        Return DaListMasterForm.ListMasterForm(customclass)
    End Function

    Public Function AddMasterForm(ByVal customclass As Parameter.MasterForm) As String Implements [Interface].IUserManagementMasterForm.AddMasterForm
        Dim DaAddMasterForm As New SQLEngine.AppMgt.MasterForm
        Return DaAddMasterForm.AddMasterForm(customclass)
    End Function
    Public Function UpdateMasterForm(ByVal customclass As Parameter.MasterForm) As String Implements [Interface].IUserManagementMasterForm.UpdateMasterForm
        Dim DaUpdateMasterForm As New SQLEngine.AppMgt.MasterForm
        Return DaUpdateMasterForm.UpdateMasterForm(customclass)
    End Function
    Public Function DeleteMasterForm(ByVal customclass As Parameter.MasterForm) As String Implements [Interface].IUserManagementMasterForm.DeleteMasterForm
        Dim DaDeleteMasterForm As New SQLEngine.AppMgt.MasterForm
        Return DaDeleteMasterForm.DeleteMasterForm(customclass)
    End Function

    Public Function ShowMasterForm(ByVal customclass As Parameter.MasterForm) As Parameter.MasterForm Implements [Interface].IUserManagementMasterForm.ShowMasterForm
        Dim DaShowMasterForm As New SQLEngine.AppMgt.MasterForm
        Return DaShowMasterForm.ShowMasterForm(customclass)
    End Function
End Class
