
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class Feature
    Implements IFeature

    Public Function ListMasterForm(ByVal customclass As Parameter.Feature) As Parameter.Feature Implements [Interface].IFeature.ListMasterForm
        Dim DaListMasterForm As New SQLEngine.AppMgt.Feature
        Return DaListMasterForm.ListMasterForm(customclass)
    End Function

    Public Function ListFeatureUser(ByVal customclass As Parameter.Feature) As Parameter.Feature Implements [Interface].IFeature.ListFeatureUser
        Dim DaListMasterForm As New SQLEngine.AppMgt.Feature
        Return DaListMasterForm.ListFeatureUser(customclass)
    End Function

    Public Function UpdateFeatureUser(ByVal customclass As Parameter.Feature) As String Implements [Interface].IFeature.UpdateFeatureUser
        Dim DaListMasterForm As New SQLEngine.AppMgt.Feature
        Return DaListMasterForm.UpdateFeatureUser(customclass)
    End Function
End Class
