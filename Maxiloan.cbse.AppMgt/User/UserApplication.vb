

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class UserApplication
    Implements IUserManagementUserApplication

    Public Function UpdateUserApplication(ByVal customclass As Parameter.UserApplication) As String Implements IUserManagementUserApplication.UpdateUserApplication
        Dim DaUpdateUserList As New SQLEngine.AppMgt.UserApplication
        Return DaUpdateUserList.UpdateUserApplication(customclass)
    End Function
    Public Function DeleteUserApplication(ByVal customclass As Parameter.UserApplication) As String Implements IUserManagementUserApplication.DeleteUserApplication
        Dim DaDeleteUserList As New SQLEngine.AppMgt.UserApplication
        Return DaDeleteUserList.DeleteUserApplication(customclass)
    End Function
    Public Function ListUserApplication(ByVal customclass As Parameter.UserApplication) As Parameter.UserApplication Implements IUserManagementUserApplication.ListUserApplication
        Dim DaListUserList As New SQLEngine.AppMgt.UserApplication
        Return DaListUserList.ListUserApplication(customclass)
    End Function
    Public Function AddUserApplication(ByVal customclass As Parameter.UserApplication) As String Implements IUserManagementUserApplication.AddUserApplication
        Dim DaAddUserList As New SQLEngine.AppMgt.UserApplication
        Return DaAddUserList.AddUserApplication(customclass)
    End Function
End Class
