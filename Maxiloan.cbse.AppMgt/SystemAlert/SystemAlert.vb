
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class SystemAlert
    Implements ISystemAlert

#Region "System Alert"
    Public Sub SystemAlertAdd(ByVal customclass As Parameter.SystemAlert) Implements [Interface].ISystemAlert.SystemAlertAdd
        Dim DaSystemAlertAdd As New SQLEngine.AppMgt.SystemAlert
        DaSystemAlertAdd.SystemAlertAdd(customclass)
    End Sub

    Public Sub SystemAlertDelete(ByVal customclass As Parameter.SystemAlert) Implements [Interface].ISystemAlert.SystemAlertDelete
        Dim DaSystemAlertDelete As New SQLEngine.AppMgt.SystemAlert
        DaSystemAlertDelete.SystemAlertDelete(customclass)
    End Sub

    Public Sub SystemAlertEdit(ByVal customclass As Parameter.SystemAlert) Implements [Interface].ISystemAlert.SystemAlertEdit
        Dim DaSystemAlertEdit As New SQLEngine.AppMgt.SystemAlert
        DaSystemAlertEdit.SystemAlertEdit(customclass)
    End Sub

    Public Function SystemAlertView(ByVal customclass As Parameter.SystemAlert) As Parameter.SystemAlert Implements [Interface].ISystemAlert.SystemAlertView
        Dim DaSystemAlertView As New SQLEngine.AppMgt.SystemAlert
        Return DaSystemAlertView.SystemAlertView(customclass)
    End Function

    Public Function GetGroupAlert(ByVal customclass As Parameter.SystemAlert) As System.Data.DataTable Implements [Interface].ISystemAlert.GetGroupAlert
        Dim DaSystemAlertView As New SQLEngine.AppMgt.SystemAlert
        Return DaSystemAlertView.GetGroupAlert(customclass)
    End Function
#End Region

#Region "Group Alert"
    Public Sub GroupAlertAdd(ByVal customclass As Parameter.SystemAlert) Implements [Interface].ISystemAlert.GroupAlertAdd
        Dim DaGroupAlertEdit As New SQLEngine.AppMgt.SystemAlert
        DaGroupAlertEdit.GroupAlertAdd(customclass)
    End Sub

    Public Sub GroupAlertDelete(ByVal customclass As Parameter.SystemAlert) Implements [Interface].ISystemAlert.GroupAlertDelete
        Dim DaGroupAlertEdit As New SQLEngine.AppMgt.SystemAlert
        DaGroupAlertEdit.GroupAlertDelete(customclass)
    End Sub

    Public Sub GroupAlertEdit(ByVal customclass As Parameter.SystemAlert) Implements [Interface].ISystemAlert.GroupAlertEdit
        Dim DaGroupAlertEdit As New SQLEngine.AppMgt.SystemAlert
        DaGroupAlertEdit.GroupAlertEdit(customclass)
    End Sub

    Public Function GroupAlertView(ByVal customclass As Parameter.SystemAlert) As Parameter.SystemAlert Implements [Interface].ISystemAlert.GroupAlertView
        Dim DaGroupAlertEdit As New SQLEngine.AppMgt.SystemAlert
        Return DaGroupAlertEdit.GroupAlertView(customclass)
    End Function
#End Region

#Region "User Alert"
    Public Sub UserAlertUpdate(ByVal customclass As Parameter.SystemAlert) Implements [Interface].ISystemAlert.UserAlertUpdate
        Dim DaUserAlertUpdate As New SQLEngine.AppMgt.SystemAlert
        DaUserAlertUpdate.UserAlertUpdate(customclass)
    End Sub
    Public Function UserAlertPaging(ByVal customclass As Parameter.SystemAlert) As Parameter.SystemAlert Implements [Interface].ISystemAlert.UserAlertPaging
        Dim DaUserAlertPaging As New SQLEngine.AppMgt.SystemAlert
        Return DaUserAlertPaging.UserAlertPaging(customclass)
    End Function
#End Region

End Class
