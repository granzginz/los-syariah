
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class GroupUser
    Implements IUserManagementGroupUser
    Public Function ListGroupUser(ByVal customclass As Parameter.GroupUser) As Parameter.GroupUser Implements [Interface].IUserManagementGroupUser.ListGroupUser
        Dim DaListGroupUser As New SQLEngine.AppMgt.GroupUser
        Return DaListGroupUser.ListGroupUser(customclass)
    End Function

    Public Function AddGroupUser(ByVal customclass As Parameter.GroupUser) As String Implements [Interface].IUserManagementGroupUser.AddGroupUser
        Dim DaAddGroupUser As New SQLEngine.AppMgt.GroupUser
        Return DaAddGroupUser.AddGroupUser(customclass)
    End Function
    Public Function UpdateGroupUser(ByVal customclass As Parameter.GroupUser) As String Implements [Interface].IUserManagementGroupUser.UpdateGroupUser
        Dim DaUpdateGroupUser As New SQLEngine.AppMgt.GroupUser
        Return DaUpdateGroupUser.UpdateGroupUser(customclass)
    End Function
    Public Function DeleteGroupUser(ByVal customclass As Parameter.GroupUser) As String Implements [Interface].IUserManagementGroupUser.DeleteGroupUser
        Dim DaDeleteGroupUser As New SQLEngine.AppMgt.GroupUser
        Return DaDeleteGroupUser.DeleteGroupUser(customclass)
    End Function
End Class
