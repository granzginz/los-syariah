
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region
Public Class InfoBox
    Implements IInfoBox


    Public Sub MasterInfoBoxAdd(ByVal customclass As Parameter.InfoBox) Implements [Interface].IInfoBox.MasterInfoBoxAdd
        Dim DaMasterInfoBoxAdd As New SQLEngine.AppMgt.InfoBox
        DaMasterInfoBoxAdd.InfoBoxAdd(customclass)
    End Sub

    Public Sub MasterInfoBoxDelete(ByVal customclass As Parameter.InfoBox) Implements [Interface].IInfoBox.MasterInfoBoxDelete
        Dim DaMasterInfoBoxDelete As New SQLEngine.AppMgt.InfoBox
        DaMasterInfoBoxDelete.InfoBoxDelete(customclass)
    End Sub

    Public Sub MasterInfoBoxEdit(ByVal customclass As Parameter.InfoBox) Implements [Interface].IInfoBox.MasterInfoBoxEdit
        Dim DaMasterInfoBoxEdit As New SQLEngine.AppMgt.InfoBox
        DaMasterInfoBoxEdit.InfoBoxEdit(customclass)
    End Sub

    Public Function MasterInfoBoxView(ByVal customclass As Parameter.InfoBox) As Parameter.InfoBox Implements [Interface].IInfoBox.MasterInfoBoxView
        Dim DaMasterInfoBoxView As New SQLEngine.AppMgt.InfoBox
        Return DaMasterInfoBoxView.InfoBoxView(customclass)
    End Function

    Public Function UserInfoBoxPaging(ByVal customclass As Parameter.InfoBox) As Parameter.InfoBox Implements [Interface].IInfoBox.UserInfoBoxPaging
        Dim DaMasterInfoBoxPaging As New SQLEngine.AppMgt.InfoBox
        Return DaMasterInfoBoxPaging.UserInfoBoxPaging(customclass)
    End Function

    Public Sub UserInfoBoxUpdate(ByVal customclass As Parameter.InfoBox) Implements [Interface].IInfoBox.UserInfoBoxUpdate
        Dim DaMasterInfoBoxUpdate As New SQLEngine.AppMgt.InfoBox
        DaMasterInfoBoxUpdate.UserInfoBoxUpdate(customclass)
    End Sub
End Class
