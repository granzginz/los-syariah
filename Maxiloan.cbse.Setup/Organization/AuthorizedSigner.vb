﻿#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class AuthorizedSigner : Inherits ComponentBase
    Implements IAuthorizedSigner
    Dim oSQLE As New SQLEngine.Setup.AuthorizedSigner

    Public Function AuthorizedSignerDelete(ByVal oCustomClass As Parameter.AuthorizedSigner) As String Implements [Interface].IAuthorizedSigner.AuthorizedSignerDelete
        Return oSQLE.AuthorizedSignerDelete(oCustomClass)
    End Function

    Public Function AuthorizedSignerSaveAdd(ByVal oCustomClass As Parameter.AuthorizedSigner) As String Implements [Interface].IAuthorizedSigner.AuthorizedSignerSaveAdd
        Return oSQLE.AuthorizedSignerSaveAdd(oCustomClass)
    End Function

    Public Sub AuthorizedSignerSaveEdit(ByVal oCustomClass As Parameter.AuthorizedSigner) Implements [Interface].IAuthorizedSigner.AuthorizedSignerSaveEdit
        oSQLE.AuthorizedSignerSaveEdit(oCustomClass)
    End Sub

    Public Function GetAuthorizedSigner(ByVal oCustomClass As Parameter.AuthorizedSigner) As Parameter.AuthorizedSigner Implements [Interface].IAuthorizedSigner.GetAuthorizedSigner
        Return oSQLE.GetAuthorizedSigner(oCustomClass)
    End Function

    Public Function GetAuthorizedSignerList(ByVal oCustomClass As Parameter.AuthorizedSigner) As Parameter.AuthorizedSigner Implements [Interface].IAuthorizedSigner.GetAuthorizedSignerList
        Return oSQLE.GetAuthorizedSignerList(oCustomClass)
    End Function
End Class



