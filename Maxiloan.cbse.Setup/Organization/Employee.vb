

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class Employee : Inherits ComponentBase
    Implements IEmployee


    Public Sub EmployeeAdd(ByVal oEmployee As Parameter.Employee, ByVal oAddress As Parameter.Address, ByVal oPerson As Parameter.Personal) Implements [Interface].IEmployee.EmployeeAdd
        Try
            Dim DA As New SQLEngine.Setup.Employee
            DA.EmployeeAdd(oEmployee, oAddress, oPerson)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function EmployeeByID(ByVal oEmployee As Parameter.Employee) As Parameter.Employee Implements [Interface].IEmployee.EmployeeByID
        Dim DA As New SQLEngine.Setup.Employee
        Return DA.ListEmployeeByID(oEmployee)
    End Function

    Public Sub EmployeeDelete(ByVal oEmployee As Parameter.Employee) Implements [Interface].IEmployee.EmployeeDelete
        Try
            Dim DA As New SQLEngine.Setup.Employee
            DA.EmployeeDelete(oEmployee)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub EmployeeEdit(ByVal oEmployee As Parameter.Employee, ByVal oAddress As Parameter.Address, ByVal oPerson As Parameter.Personal) Implements [Interface].IEmployee.EmployeeEdit
        Try
            Dim DA As New SQLEngine.Setup.Employee
            DA.EmployeeEdit(oEmployee, oAddress, oPerson)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function EmployeeList(ByVal oEmployee As Parameter.Employee) As Parameter.Employee Implements [Interface].IEmployee.EmployeeList
        Dim DA As New SQLEngine.Setup.Employee
        Return DA.ListEmployee(oEmployee)
    End Function

    Public Function EmployeeReport(ByVal oEmployee As Parameter.Employee) As System.Data.DataTable Implements [Interface].IEmployee.EmployeeReport
        Dim DA As New SQLEngine.Setup.Employee
        Return DA.EmployeeReport(oEmployee)
    End Function

    Public Function GetCombo(ByVal strCon As String, ByVal strCombo As String, ByVal BranchID As String) As System.Data.DataTable Implements [Interface].IEmployee.GetCombo
        Dim DA As New SQLEngine.Setup.Employee
        Return DA.GetCombo(strCon, strCombo, BranchID)
    End Function

    Public Sub EmployeePindahCabang(ByVal oEmployee As Parameter.Employee) Implements [Interface].IEmployee.EmployeePindahCabang
        Try
            Dim DA As New SQLEngine.Setup.Employee
            DA.EmployeePindahCabang(oEmployee)

        Catch ex As Exception            
            Throw ex
        End Try
    End Sub
End Class
