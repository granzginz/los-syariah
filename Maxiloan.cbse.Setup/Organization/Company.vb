#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
#End Region

Public Class Company : Inherits ComponentBase
    Implements ICompany

    Public Function CompanyList(ByVal co As Parameter.Company) As Parameter.Company Implements [Interface].ICompany.CompanyList
        Dim DA As New SQLEngine.Setup.Company
        Return DA.ListCompany(co)
    End Function


    Public Function CompanyAdd(ByVal co As Parameter.Company, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As String Implements [Interface].ICompany.CompanyAdd
        Try
            Dim DA As New SQLEngine.Setup.Company
            DA.CompanyAdd(co, oClassAddress, oClassPersonal)
        Catch ex As Exception            
            Throw ex
        End Try
    End Function

    Public Sub CompanyDelete(ByVal co As Parameter.Company) Implements [Interface].ICompany.CompanyDelete
        Try
            Dim DA As New SQLEngine.Setup.Company
            DA.CompanyDelete(co)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Sub CompanyEdit(ByVal co As Parameter.Company, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) Implements [Interface].ICompany.CompanyEdit
        Try
            Dim DA As New SQLEngine.Setup.Company
            DA.CompanyEdit(co, oClassAddress, oClassPersonal)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Function CompanyListByID(ByVal co As Parameter.Company) As Parameter.Company Implements [Interface].ICompany.CompanyListByID
        Dim DA As New SQLEngine.Setup.Company
        Return DA.ListCompanyByID(co)
    End Function


#Region "BOD"
    Public Function CompanyBODAdd(ByVal customClass As Parameter.CompanyBOD, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As String Implements [Interface].ICompany.CompanyBODAdd
        Try
            Dim DA As New SQLEngine.Setup.Company
            DA.CompanyBODAdd(customClass, oClassAddress, oClassPersonal)

        Catch ex As Exception

            Throw ex
        End Try
    End Function

    Public Sub CompanyBODDelete(ByVal customclass As Parameter.CompanyBOD) Implements [Interface].ICompany.CompanyBODDelete
        Try
            Dim DA As New SQLEngine.Setup.Company
            DA.CompanyBODDelete(customclass)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Sub CompanyBODEdit(ByVal customClass As Parameter.CompanyBOD, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) Implements [Interface].ICompany.CompanyBODEdit
        Try
            Dim DA As New SQLEngine.Setup.Company
            DA.CompanyBODEdit(customClass, oClassAddress, oClassPersonal)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Function CompanyBODList(ByVal co As Parameter.CompanyBOD) As Parameter.CompanyBOD Implements [Interface].ICompany.CompanyBODList
        Dim DA As New SQLEngine.Setup.Company
        Return DA.ListCompanyBOD(co)
    End Function

    Public Function CompanyBODListByID(ByVal co As Parameter.CompanyBOD) As Parameter.CompanyBOD Implements [Interface].ICompany.CompanyBODListByID
        Dim DA As New SQLEngine.Setup.Company
        Return DA.ListCompanyBODByID(co)
    End Function

#End Region


    Public Function GetComboInputType(ByVal strcon As String) As System.Data.DataTable Implements [Interface].ICompany.GetComboInputType
        Dim DA As New SQLEngine.Setup.Company
        Return DA.GetComboInputType(strcon)
    End Function


#Region "Commisioner"
    Public Function CompanyCommisionerAdd(ByVal customClass As Parameter.CompanyCommisioner, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As String Implements [Interface].ICompany.CompanyCommisionerAdd
        Try
            Dim DA As New SQLEngine.Setup.Company
            DA.CompanyCommisionerAdd(customClass, oClassAddress, oClassPersonal)

        Catch ex As Exception

            Throw ex
        End Try
    End Function

    Public Sub CompanyCommisionerDelete(ByVal customclass As Parameter.CompanyCommisioner) Implements [Interface].ICompany.CompanyCommisionerDelete
        Try
            Dim DA As New SQLEngine.Setup.Company
            DA.CompanyCommisionerDelete(customclass)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Sub CompanyCommisionerEdit(ByVal customClass As Parameter.CompanyCommisioner, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) Implements [Interface].ICompany.CompanyCommisionerEdit
        Try
            Dim DA As New SQLEngine.Setup.Company
            DA.CompanyCommisionerEdit(customClass, oClassAddress, oClassPersonal)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Function CompanyCommisionerList(ByVal co As Parameter.CompanyCommisioner) As Parameter.CompanyCommisioner Implements [Interface].ICompany.CompanyCommisionerList
        Dim DA As New SQLEngine.Setup.Company
        Return DA.ListCompanyCommisioner(co)
    End Function

    Public Function CompanyCommisionerListByID(ByVal co As Parameter.CompanyCommisioner) As Parameter.CompanyCommisioner Implements [Interface].ICompany.CompanyCommisionerListByID
        Dim DA As New SQLEngine.Setup.Company
        Return DA.ListCompanyCommisionerByID(co)
    End Function

#End Region

    Public Function CompanyReport(ByVal co As Parameter.Company) As Parameter.Company Implements [Interface].ICompany.CompanyReport
        Dim DA As New SQLEngine.Setup.Company
        Return DA.ListCompanyReport(co)
    End Function

    Public Function CompanyBODReport(ByVal co As Parameter.CompanyBOD) As Parameter.CompanyBOD Implements [Interface].ICompany.CompanyBODReport
        Dim DA As New SQLEngine.Setup.Company
        Return DA.ListCompanyBODReport(co)
    End Function

    Public Function CompanyCommisionerReport(ByVal co As Parameter.CompanyCommisioner) As Parameter.CompanyCommisioner Implements [Interface].ICompany.CompanyCommisionerReport
        Dim DA As New SQLEngine.Setup.Company
        Return DA.ListCompanyCommisionerReport(co)
    End Function
End Class
