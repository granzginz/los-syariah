
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class NewMaster : Inherits ComponentBase
    Implements INewMaster

    Public Function GetMaster(ByVal customClass As Parameter.NewMaster) As Parameter.NewMaster Implements [Interface].INewMaster.GetMaster
        Try
            Dim MasterDA As New SQLEngine.Setup.NewMaster
            Return MasterDA.GetMaster(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetMasterReport(ByVal customClass As Parameter.NewMaster) As Parameter.NewMaster Implements [Interface].INewMaster.GetMasterReport
        Try
            Dim MasterDA As New SQLEngine.Setup.NewMaster
            Return MasterDA.GetMasterReport(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetKeyWord(ByVal customClass As Parameter.NewMaster) As Parameter.NewMaster Implements [Interface].INewMaster.GetKeyWord
        Try
            Dim MasterDA As New SQLEngine.Setup.NewMaster
            Return MasterDA.GetKeyWord(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function MasterSaveAdd(ByVal customClass As Parameter.NewMaster) As String Implements INewMaster.MasterSaveAdd
        Try
            Dim MasterDA As New SQLEngine.Setup.NewMaster
            Return MasterDA.MasterSaveAdd(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub MasterSaveEdit(ByVal customClass As Parameter.NewMaster) Implements INewMaster.MasterSaveEdit
        Try
            Dim MasterDA As New SQLEngine.Setup.NewMaster
            MasterDA.MasterSaveEdit(customClass)
        Catch ex As Exception
            Throw New Exception("Error when saving Update Master", ex)
        End Try
    End Sub

    Public Function MasterDelete(ByVal customClass As Parameter.NewMaster) As String Implements INewMaster.MasterDelete
        'Try
        Dim MasterDA As New SQLEngine.Setup.NewMaster
        Return MasterDA.MasterDelete(customClass)
        'Catch ex As Exception
        '    Throw New Exception("Error when Delete Master", ex)
        'End Try
    End Function
End Class