
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class Reason
    Implements IReason

    Public Sub ReasonAdd(ByVal customclass As Parameter.Reason) Implements [Interface].IReason.ReasonAdd
        Dim ReasonDA As New SQLEngine.Setup.Reason
        ReasonDA.ReasonAdd(customclass)
    End Sub

    Public Sub ReasonDelete(ByVal customclass As Parameter.Reason) Implements [Interface].IReason.ReasonDelete
        Dim ReasonDA As New SQLEngine.Setup.Reason
        ReasonDA.ReasonDelete(customclass)
    End Sub

    Public Sub ReasonEdit(ByVal customclass As Parameter.Reason) Implements [Interface].IReason.ReasonEdit
        Dim ReasonDA As New SQLEngine.Setup.Reason
        ReasonDA.ReasonEdit(customclass)
    End Sub

    Public Function ReasonView(ByVal customclass As Parameter.Reason) As Parameter.Reason Implements [Interface].IReason.ReasonView
        Dim ReasonDA As New SQLEngine.Setup.Reason
        Return ReasonDA.ReasonView(customclass)
    End Function
    Public Function GetReasonTypeCombo(ByVal customclass As Parameter.Reason) As DataTable Implements [Interface].IReason.GetReasonTypeCombo
        Dim ReasonDA As New SQLEngine.Setup.Reason
        Return ReasonDA.GetReasonTypeCombo(customclass)
    End Function
End Class