

#Region "Imports"

Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class MasterTC : Inherits ComponentBase
    Implements IMasterTC

    Public Sub MasterTCAdd(ByVal oTC As Parameter.MasterTC) Implements [Interface].IMasterTC.MasterTCAdd
        Try
            Dim DA As New SQLEngine.Setup.MasterTC
            DA.MasterTCAdd(oTC)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub MasterTCDelete(ByVal oTC As Parameter.MasterTC) Implements [Interface].IMasterTC.MasterTCDelete
        Try
            Dim DA As New SQLEngine.Setup.MasterTC
            DA.MasterTCDelete(oTC)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub MasterTCEdit(ByVal oTC As Parameter.MasterTC) Implements [Interface].IMasterTC.MasterTCEdit
        Try
            Dim DA As New SQLEngine.Setup.MasterTC
            DA.MasterTCEdit(oTC)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function MasterTCList(ByVal oTC As Parameter.MasterTC) As Parameter.MasterTC Implements [Interface].IMasterTC.MasterTCList
        Dim DA As New SQLEngine.Setup.MasterTC
        Return DA.ListMasterTC(oTC)
    End Function

    Public Function MasterTCReport(ByVal oTC As Parameter.MasterTC) As Parameter.MasterTC Implements [Interface].IMasterTC.MasterTCReport
        Dim DA As New SQLEngine.Setup.MasterTC
        Return DA.MasterTCReport(oTC)
    End Function

    Public Function MasterTCByID(ByVal oTC As Parameter.MasterTC) As Parameter.MasterTC Implements [Interface].IMasterTC.MasterTCByID
        Dim DA As New SQLEngine.Setup.MasterTC
        Return DA.ListMasterTCByID(oTC)
    End Function





#Region "TCCL"

    Public Sub TCCLAdd(ByVal oTC As Parameter.MasterTC) Implements [Interface].IMasterTC.TCCLAdd
        Try
            Dim DA As New SQLEngine.Setup.MasterTC
            DA.TCCLAdd(oTC)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function TCCLByID(ByVal oTC As Parameter.MasterTC) As Parameter.MasterTC Implements [Interface].IMasterTC.TCCLByID
        Dim DA As New SQLEngine.Setup.MasterTC
        Return DA.ListTCCLByID(oTC)
    End Function

    Public Sub TCCLDelete(ByVal oTC As Parameter.MasterTC) Implements [Interface].IMasterTC.TCCLDelete
        Try
            Dim DA As New SQLEngine.Setup.MasterTC
            DA.TCCLDelete(oTC)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub TCCLEdit(ByVal oTC As Parameter.MasterTC) Implements [Interface].IMasterTC.TCCLEdit
        Try
            Dim DA As New SQLEngine.Setup.MasterTC
            DA.TCCLEdit(oTC)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function TCCLList(ByVal oTC As Parameter.MasterTC) As Parameter.MasterTC Implements [Interface].IMasterTC.TCCLList
        Dim DA As New SQLEngine.Setup.MasterTC
        Return DA.ListTCCL(oTC)
    End Function

    Public Function TCCLReport(ByVal oTC As Parameter.MasterTC) As Parameter.MasterTC Implements [Interface].IMasterTC.TCCLReport
        Dim DA As New SQLEngine.Setup.MasterTC
        Return DA.TCCLReport(oTC)
    End Function

#End Region

End Class
