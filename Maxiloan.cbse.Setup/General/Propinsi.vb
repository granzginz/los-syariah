﻿
#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class Propinsi : Inherits ComponentBase
    Implements IPropinsi

    Dim oSQLE As New SQLEngine.Setup.Propinsi

    Public Function GetPropinsi(ByVal oCustomClass As Parameter.Propinsi) As Parameter.Propinsi Implements [Interface].IPropinsi.GetPropinsi
        Return oSQLE.GetPropinsi(oCustomClass)
    End Function

    Public Function GetPropinsiList(ByVal oCustomClass As Parameter.Propinsi) As Parameter.Propinsi Implements [Interface].IPropinsi.GetPropinsiList
        Return oSQLE.GetPropinsiList(oCustomClass)
    End Function

    Public Function GetPropinsiCombo(ByVal customclass As Parameter.Propinsi) As System.Data.DataTable Implements [Interface].IPropinsi.GetPropinsiCombo
        Return oSQLE.GetPropinsiCombo(customclass)
    End Function
    Public Function ProvinceSaveAdd(ByVal ocustomClass As Parameter.Propinsi) As String Implements [Interface].IPropinsi.ProvinceSaveAdd
        Return oSQLE.ProvinceSaveAdd(ocustomClass)
    End Function
    Public Sub ProvinceSaveEdit(ByVal ocustomClass As Parameter.Propinsi) Implements [Interface].IPropinsi.ProvinceSaveEdit
        oSQLE.ProvinceSaveEdit(ocustomClass)
    End Sub
End Class
