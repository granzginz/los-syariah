﻿#Region "Imports"
Imports Maxiloan.Interface
#End Region
Public Class Currency : Inherits ComponentBase
    Implements ICurrency
    Dim oSQLE As New SQLEngine.Setup.Currency

    Public Sub GetCurrency(ByRef dto As Parameter.Currency) Implements [Interface].ICurrency.GetCurrency
        oSQLE.GetCurrency(dto)
    End Sub
End Class
