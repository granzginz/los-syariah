

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class EmployeePosition : Inherits ComponentBase
    Implements IEmployeePosition


    Public Sub EmployeePositionAdd(ByVal oEmpPos As Parameter.EmployeePosition) Implements [Interface].IEmployeePosition.EmployeePositionAdd
        Try
            Dim DAEmpPos As New SQLEngine.Setup.EmployeePosition
            DAEmpPos.EmployeePositionAdd(oEmpPos)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub EmployeePositionDelete(ByVal oEmpPos As Parameter.EmployeePosition) Implements [Interface].IEmployeePosition.EmployeePositionDelete
        Try
            Dim DAEmpPos As New SQLEngine.Setup.EmployeePosition
            DAEmpPos.EmployeePositionDelete(oEmpPos)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub EmployeePositionEdit(ByVal oEmpPos As Parameter.EmployeePosition) Implements [Interface].IEmployeePosition.EmployeePositionEdit
        Try
            Dim DAEmpPos As New SQLEngine.Setup.EmployeePosition
            DAEmpPos.EmployeePositionEdit(oEmpPos)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function EmployeePositionList(ByVal oEmpPos As Parameter.EmployeePosition) As Parameter.EmployeePosition Implements [Interface].IEmployeePosition.EmployeePositionList
        Try
            Dim DAEmpPos As New SQLEngine.Setup.EmployeePosition
            Return DAEmpPos.EmployeePositionList(oEmpPos)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function EmployeePositionReport(ByVal oEmpPos As Parameter.EmployeePosition) As System.Data.DataTable Implements [Interface].IEmployeePosition.EmployeePositionReport
        Try
            Dim DAEmpPos As New SQLEngine.Setup.EmployeePosition
            Return DAEmpPos.EmployeePositionReport(oEmpPos)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
