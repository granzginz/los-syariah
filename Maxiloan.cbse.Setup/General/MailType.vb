﻿
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class MailType : Inherits ComponentBase
    Implements IMailType

    Public Function GetMailType(ByVal oCustomClass As Parameter.MailType) As Parameter.MailType Implements [Interface].IMailType.GetMailType
        Try
            Dim MailTypeDA As New SQLEngine.Setup.MailType
            Return MailTypeDA.GetMailType(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetMailTypeReport(ByVal oCustomClass As Parameter.MailType) As Parameter.MailType Implements [Interface].IMailType.GetMailTypeReport
        Try
            Dim MailTypeDA As New SQLEngine.Setup.MailType
            Return MailTypeDA.GetMailTypeReport(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetSector(ByVal oCustomClass As Parameter.MailType) As Parameter.MailType Implements [Interface].IMailType.GetSector
        Try
            Dim MailTypeDA As New SQLEngine.Setup.MailType
            Return MailTypeDA.GetSector(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetMailTypeEdit(ByVal oCustomClass As Parameter.MailType) As Parameter.MailType Implements [Interface].IMailType.GetMailTypeEdit
        Try
            Dim MailTypeDA As New SQLEngine.Setup.MailType
            Return MailTypeDA.GetMailTypeEdit(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function MailTypeSaveAdd(ByVal oCustomClass As Parameter.MailType) As String Implements IMailType.MailTypeSaveAdd
        'Trace.WriteIf(MethodTrace.Enabled, "Enter")
        Try
            Dim MailTypeDA As New SQLEngine.Setup.MailType
            Return MailTypeDA.MailTypeSaveAdd(oCustomClass)
            'Throw busEx
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

    Public Sub MailTypeSaveEdit(ByVal oCustomClass As Parameter.MailType) Implements IMailType.MailTypeSaveEdit
        Try
            Dim MailTypeDA As New SQLEngine.Setup.MailType
            MailTypeDA.MailTypeSaveEdit(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when saving Update MailType", ex)
        End Try
    End Sub

    Public Function MailTypeDelete(ByVal oCustomClass As Parameter.MailType) As String Implements IMailType.MailTypeDelete
        Try
            Dim MailTypeDA As New SQLEngine.Setup.MailType
            Return MailTypeDA.MailTypeDelete(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when Delete MailType", ex)
        End Try
    End Function
End Class