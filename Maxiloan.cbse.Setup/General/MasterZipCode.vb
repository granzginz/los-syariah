

#Region " Imports "
Imports Maxiloan.cbse
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class MasterZipCode : Inherits ComponentBase
    Implements IZipCode

    Public Function GetMasterZipCode(ByVal oMasterZipCode As Parameter.MasterZipCode) As Parameter.MasterZipCode Implements IZipCode.GetMasterZipCode
            Dim MasterZipCodeListDA As New SQLEngine.Setup.MasterZipCode
            Return MasterZipCodeListDA.GetMasterZipCode(oMasterZipCode)
    End Function

    Public Function GetZipCodeIdReport(ByVal customClass As Parameter.MasterZipCode) As Parameter.MasterZipCode Implements IZipCode.GetZipCodeIdReport
        Try
            Dim MasterZipCodeListDA As New SQLEngine.Setup.MasterZipCode
            Return MasterZipCodeListDA.GetZipCodeIdReport(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function MasterZipCodeAdd(ByVal customClass As Parameter.MasterZipCode) As String Implements [Interface].IZipCode.MasterZipCodeAdd
        Try
            Dim MasterZipCodeDA As New SQLEngine.Setup.MasterZipCode
            Return MasterZipCodeDA.MasterZipCodeAdd(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    'Public Sub MasterZipCodeUpdate(ByVal customClass As Parameter.MasterZipCode) Implements [Interface].IZipCode.MasterZipCodeUpdate
    '    Try
    '        Dim MasterZipCodeDA As New DataAccess.ZipCode.MasterZipCode
    '        MasterZipCodeDA.MasterZipCodeUpdate(customClass)

    '    Catch ex As Exception
    '        'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
    '    End Try
    'End Sub

    Public Function MasterZipCodeDelete(ByVal customClass As Parameter.MasterZipCode) As String Implements [Interface].IZipCode.MasterZipCodeDelete
        Try
            Dim MasterZipCodeDA As New SQLEngine.Setup.MasterZipCode
            Return MasterZipCodeDA.MasterZipCodeDelete(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

End Class
