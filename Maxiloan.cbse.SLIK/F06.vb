﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region


Public Class F06 : Inherits ComponentBase
    Implements IF06

    Public Function GetF06(oCustomClass As Parameter.F06) As Parameter.F06 Implements IF06.GetF06
        Try
            Dim F06DA As New SQLEngine.SLIK.F06
            Return F06DA.GetF06(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSelectF06(oCustomClass As Parameter.F06) As Parameter.F06 Implements IF06.GetSelectF06
        Try
            Dim F06DA As New SQLEngine.SLIK.F06
            Return F06DA.GetSelectF06(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetF06Edit(oCustomClass As Parameter.F06) As Parameter.F06 Implements IF06.GetF06Edit
        Try
            Dim F06DA As New SQLEngine.SLIK.F06
            Return F06DA.GetF06Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetF06Save(oCustomClass As Parameter.F06) As Parameter.F06 Implements IF06.GetF06Save
        Try
            Dim F06DA As New SQLEngine.SLIK.F06
            Return F06DA.GetF06Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo(oCustomClass As Parameter.F06) As Parameter.F06 Implements IF06.GetCbo
        Try
            Dim F06DA As New SQLEngine.SLIK.F06
            Return F06DA.GetCbo(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo1(oCustomClass As Parameter.F06) As Parameter.F06 Implements IF06.GetCbo1
        Try
            Dim F06DA As New SQLEngine.SLIK.F06
            Return F06DA.GetCbo1(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo2(oCustomClass As Parameter.F06) As Parameter.F06 Implements IF06.GetCbo2
        Try
            Dim F06DA As New SQLEngine.SLIK.F06
            Return F06DA.GetCbo2(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo3(oCustomClass As Parameter.F06) As Parameter.F06 Implements IF06.GetCbo3
        Try
            Dim F06DA As New SQLEngine.SLIK.F06
            Return F06DA.GetCbo3(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo4(oCustomClass As Parameter.F06) As Parameter.F06 Implements IF06.GetCbo4
        Try
            Dim F06DA As New SQLEngine.SLIK.F06
            Return F06DA.GetCbo4(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetF06Add(oCustomClass As Parameter.F06) As Parameter.F06 Implements IF06.GetF06Add
        Try
            Dim F06DA As New SQLEngine.SLIK.F06
            Return F06DA.GetF06Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetF06Delete(oCustomClass As Parameter.F06) As String Implements IF06.GetF06Delete
        Try
            Dim F06DA As New SQLEngine.SLIK.F06
            Return F06DA.GetF06Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.F06) As Parameter.F06 Implements IF06.GetCboBulandataSIPP
        Try
            Dim F06DA As New SQLEngine.SLIK.F06
            Return F06DA.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class
