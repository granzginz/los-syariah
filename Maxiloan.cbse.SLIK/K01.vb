﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class K01 : Inherits ComponentBase
	Implements IK01

	Public Function GetK01(oCustomClass As Parameter.K01) As Parameter.K01 Implements IK01.GetK01
		Try
			Dim K01DA As New SQLEngine.SLIK.K01
			Return K01DA.GetK01(oCustomClass)
		Catch ex As Exception
			Throw New Exception("Error ", ex)
		End Try
		Throw New NotImplementedException()
	End Function

	Public Function GetSelectK01(oCustomClass As Parameter.K01) As Parameter.K01 Implements IK01.GetSelectK01
		Try
			Dim K01DA As New SQLEngine.SLIK.K01
			Return K01DA.GetSelectK01(oCustomClass)
		Catch ex As Exception
			Throw New Exception("Error ", ex)
		End Try
		Throw New NotImplementedException()
	End Function

	Public Function GetK01Edit(oCustomClass As Parameter.K01) As Parameter.K01 Implements IK01.GetK01Edit
		Try
			Dim K01DA As New SQLEngine.SLIK.K01
			Return K01DA.GetK01Edit(oCustomClass)
		Catch ex As Exception
			Throw New Exception("Error ", ex)
		End Try
		Throw New NotImplementedException()
	End Function

	Public Function GetK01Save(oCustomClass As Parameter.K01) As Parameter.K01 Implements IK01.GetK01Save
		Try
			Dim K01DA As New SQLEngine.SLIK.K01
			Return K01DA.GetK01Save(oCustomClass)
		Catch ex As Exception
			Throw New Exception("Error ", ex)
		End Try
		Throw New NotImplementedException()
	End Function

	Public Function GetK01Add(oCustomClass As Parameter.K01) As Parameter.K01 Implements IK01.GetK01Add
		Try
			Dim K01DA As New SQLEngine.SLIK.K01
			Return K01DA.GetK01Add(oCustomClass)
		Catch ex As Exception
			Throw New Exception("Error ", ex)
		End Try
		Throw New NotImplementedException()
	End Function

	Public Function GetK01Delete(oCustomClass As Parameter.K01) As String Implements IK01.GetK01Delete
		Try
			Dim K01DA As New SQLEngine.SLIK.K01
			Return K01DA.GetK01Delete(oCustomClass)
		Catch ex As Exception
			Throw New Exception("Error ", ex)
		End Try
		Throw New NotImplementedException()
	End Function
	Public Function GetCboBulandataSIPP(oCustomClass As Parameter.K01) As Parameter.K01 Implements IK01.GetCboBulandataSIPP
		Try
			Dim K01DA As New SQLEngine.SLIK.K01
			Return K01DA.GetCboBulandataSIPP(oCustomClass)
		Catch ex As Exception
			Throw New Exception("Error ", ex)
		End Try
		Throw New NotImplementedException()
	End Function
	Public Function GetCbo(oCustomClass As Parameter.K01) As Parameter.K01 Implements IK01.GetCbo
		Try
			Dim K01DA As New SQLEngine.SLIK.K01
			Return K01DA.GetCbo(oCustomClass)
		Catch ex As Exception
			Throw New Exception("Error ", ex)
		End Try
		Throw New NotImplementedException()
	End Function

End Class

