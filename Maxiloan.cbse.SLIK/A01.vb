﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class A01 : Inherits ComponentBase
	Implements IA01

	Public Function GetA01(oCustomClass As Parameter.A01) As Parameter.A01 Implements IA01.GetA01
		Try
			Dim A01DA As New SQLEngine.SLIK.A01
			Return A01DA.GetA01(oCustomClass)
		Catch ex As Exception
			Throw New Exception("Error ", ex)
		End Try
		Throw New NotImplementedException()
	End Function

	Public Function GetSelectA01(oCustomClass As Parameter.A01) As Parameter.A01 Implements IA01.GetSelectA01
		Try
			Dim A01DA As New SQLEngine.SLIK.A01
			Return A01DA.GetSelectA01(oCustomClass)
		Catch ex As Exception
			Throw New Exception("Error ", ex)
		End Try
		Throw New NotImplementedException()
	End Function

	Public Function GetA01Edit(oCustomClass As Parameter.A01) As Parameter.A01 Implements IA01.GetA01Edit
		Try
			Dim A01DA As New SQLEngine.SLIK.A01
			Return A01DA.GetA01Edit(oCustomClass)
		Catch ex As Exception
			Throw New Exception("Error ", ex)
		End Try
		Throw New NotImplementedException()
	End Function

	Public Function GetA01Save(oCustomClass As Parameter.A01) As Parameter.A01 Implements IA01.GetA01Save
		Try
			Dim A01DA As New SQLEngine.SLIK.A01
			Return A01DA.GetA01Save(oCustomClass)
		Catch ex As Exception
			Throw New Exception("Error ", ex)
		End Try
		Throw New NotImplementedException()
	End Function

	Public Function GetA01Add(oCustomClass As Parameter.A01) As Parameter.A01 Implements IA01.GetA01Add
		Try
			Dim A01DA As New SQLEngine.SLIK.A01
			Return A01DA.GetA01Add(oCustomClass)
		Catch ex As Exception
			Throw New Exception("Error ", ex)
		End Try
		Throw New NotImplementedException()
	End Function

	Public Function GetA01Delete(oCustomClass As Parameter.A01) As String Implements IA01.GetA01Delete
		Try
			Dim A01DA As New SQLEngine.SLIK.A01
			Return A01DA.GetA01Delete(oCustomClass)
		Catch ex As Exception
			Throw New Exception("Error ", ex)
		End Try
		Throw New NotImplementedException()
	End Function
	Public Function GetCboBulandataSIPP(oCustomClass As Parameter.A01) As Parameter.A01 Implements IA01.GetCboBulandataSIPP
		Try
			Dim A01DA As New SQLEngine.SLIK.A01
			Return A01DA.GetCboBulandataSIPP(oCustomClass)
		Catch ex As Exception
			Throw New Exception("Error ", ex)
		End Try
		Throw New NotImplementedException()
	End Function
	Public Function GetCbo(oCustomClass As Parameter.A01) As Parameter.A01 Implements IA01.GetCbo
		Try
			Dim A01DA As New SQLEngine.SLIK.A01
			Return A01DA.GetCbo(oCustomClass)
		Catch ex As Exception
			Throw New Exception("Error ", ex)
		End Try
		Throw New NotImplementedException()
	End Function

End Class

