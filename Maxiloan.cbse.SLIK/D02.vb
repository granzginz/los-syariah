﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region

Public Class D02 : Inherits ComponentBase
    Implements ID02

    Public Function GetD02(oCustomClass As Parameter.D02) As Parameter.D02 Implements ID02.GetD02
        Try
            Dim D02DA As New SQLEngine.SLIK.D02
            Return D02DA.GetD02(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSelectD02(oCustomClass As Parameter.D02) As Parameter.D02 Implements ID02.GetSelectD02
        Try
            Dim D02DA As New SQLEngine.SLIK.D02
            Return D02DA.GetSelectD02(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetD02Edit(oCustomClass As Parameter.D02) As Parameter.D02 Implements ID02.GetD02Edit
        Try
            Dim D02DA As New SQLEngine.SLIK.D02
            Return D02DA.GetD02Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetD02Save(oCustomClass As Parameter.D02) As Parameter.D02 Implements ID02.GetD02Save
        Try
            Dim D02DA As New SQLEngine.SLIK.D02
            Return D02DA.GetD02Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetD02Add(oCustomClass As Parameter.D02) As Parameter.D02 Implements ID02.GetD02Add
        Try
            Dim D02DA As New SQLEngine.SLIK.D02
            Return D02DA.GetD02Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetD02Delete(oCustomClass As Parameter.D02) As String Implements ID02.GetD02Delete
        Try
            Dim D02DA As New SQLEngine.SLIK.D02
            Return D02DA.GetD02Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo(oCustomClass As Parameter.D02) As Parameter.D02 Implements ID02.GetCbo
        Try
            Dim D02DA As New SQLEngine.SLIK.D02
            Return D02DA.GetCbo(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo1(oCustomClass As Parameter.D02) As Parameter.D02 Implements ID02.GetCbo1
        Try
            Dim D02DA As New SQLEngine.SLIK.D02
            Return D02DA.GetCbo1(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo2(oCustomClass As Parameter.D02) As Parameter.D02 Implements ID02.GetCbo2
        Try
            Dim D02DA As New SQLEngine.SLIK.D02
            Return D02DA.GetCbo2(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.D02) As Parameter.D02 Implements ID02.GetCboBulandataSIPP
        Try
            Dim D02DA As New SQLEngine.SLIK.D02
            Return D02DA.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class
