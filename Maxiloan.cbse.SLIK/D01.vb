﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class D01 : Inherits ComponentBase
    Implements ID01

    Public Function GetD01(oCustomClass As Parameter.D01) As Parameter.D01 Implements ID01.GetD01
        Try
            Dim D01DA As New SQLEngine.SLIK.D01
            Return D01DA.GetD01(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSelectD01(oCustomClass As Parameter.D01) As Parameter.D01 Implements ID01.GetSelectD01
        Try
            Dim D01DA As New SQLEngine.SLIK.D01
            Return D01DA.GetSelectD01(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetD01Edit(oCustomClass As Parameter.D01) As Parameter.D01 Implements ID01.GetD01Edit
        Try
            Dim D01DA As New SQLEngine.SLIK.D01
            Return D01DA.GetD01Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetD01Save(oCustomClass As Parameter.D01) As Parameter.D01 Implements ID01.GetD01Save
        Try
            Dim D01DA As New SQLEngine.SLIK.D01
            Return D01DA.GetD01Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetD01Add(oCustomClass As Parameter.D01) As Parameter.D01 Implements ID01.GetD01Add
        Try
            Dim D01DA As New SQLEngine.SLIK.D01
            Return D01DA.GetD01Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetD01Delete(oCustomClass As Parameter.D01) As String Implements ID01.GetD01Delete
        Try
            Dim D01DA As New SQLEngine.SLIK.D01
            Return D01DA.GetD01Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.D01) As Parameter.D01 Implements ID01.GetCboBulandataSIPP
        Try
            Dim D01DA As New SQLEngine.SLIK.D01
            Return D01DA.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetCbo(oCustomClass As Parameter.D01) As Parameter.D01 Implements ID01.GetCbo
        Try
            Dim D01DA As New SQLEngine.SLIK.D01
            Return D01DA.GetCbo(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo1(oCustomClass As Parameter.D01) As Parameter.D01 Implements ID01.GetCbo1
        Try
            Dim D01DA As New SQLEngine.SLIK.D01
            Return D01DA.GetCbo1(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

End Class

