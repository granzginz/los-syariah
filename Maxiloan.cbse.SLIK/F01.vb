﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region


Public Class F01 : Inherits ComponentBase
    Implements IF01

    Public Function GetF01(oCustomClass As Parameter.F01) As Parameter.F01 Implements IF01.GetF01
        Try
            Dim F01DA As New SQLEngine.SLIK.F01
            Return F01DA.GetF01(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSelectF01(oCustomClass As Parameter.F01) As Parameter.F01 Implements IF01.GetSelectF01
        Try
            Dim F01DA As New SQLEngine.SLIK.F01
            Return F01DA.GetSelectF01(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetF01Edit(oCustomClass As Parameter.F01) As Parameter.F01 Implements IF01.GetF01Edit
        Try
            Dim F01DA As New SQLEngine.SLIK.F01
            Return F01DA.GetF01Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetF01Save(oCustomClass As Parameter.F01) As Parameter.F01 Implements IF01.GetF01Save
        Try
            Dim F01DA As New SQLEngine.SLIK.F01
            Return F01DA.GetF01Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo(oCustomClass As Parameter.F01) As Parameter.F01 Implements IF01.GetCbo
        Try
            Dim F01DA As New SQLEngine.SLIK.F01
            Return F01DA.GetCbo(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetF01Add(oCustomClass As Parameter.F01) As Parameter.F01 Implements IF01.GetF01Add
        Try
            Dim F01DA As New SQLEngine.SLIK.F01
            Return F01DA.GetF01Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetF01Delete(oCustomClass As Parameter.F01) As String Implements IF01.GetF01Delete
        Try
            Dim F01DA As New SQLEngine.SLIK.F01
            Return F01DA.GetF01Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.F01) As Parameter.F01 Implements IF01.GetCboBulandataSIPP
        Try
            Dim F01DA As New SQLEngine.SLIK.F01
            Return F01DA.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class

