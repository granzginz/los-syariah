﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region


Public Class P01 : Inherits ComponentBase
    Implements IP01

    Public Function GetP01(oCustomClass As Parameter.P01) As Parameter.P01 Implements IP01.GetP01
        Try
            Dim P01DA As New SQLEngine.SLIK.P01
            Return P01DA.GetP01(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSelectP01(oCustomClass As Parameter.P01) As Parameter.P01 Implements IP01.GetSelectP01
        Try
            Dim P01DA As New SQLEngine.SLIK.P01
            Return P01DA.GetSelectP01(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetP01Edit(oCustomClass As Parameter.P01) As Parameter.P01 Implements IP01.GetP01Edit
        Try
            Dim P01DA As New SQLEngine.SLIK.P01
            Return P01DA.GetP01Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetP01Save(oCustomClass As Parameter.P01) As Parameter.P01 Implements IP01.GetP01Save
        Try
            Dim P01DA As New SQLEngine.SLIK.P01
            Return P01DA.GetP01Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo(oCustomClass As Parameter.P01) As Parameter.P01 Implements IP01.GetCbo
        Try
            Dim P01DA As New SQLEngine.SLIK.P01
            Return P01DA.GetCbo(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function


    Public Function GetP01Add(oCustomClass As Parameter.P01) As Parameter.P01 Implements IP01.GetP01Add
        Try
            Dim P01DA As New SQLEngine.SLIK.P01
            Return P01DA.GetP01Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetP01Delete(oCustomClass As Parameter.P01) As String Implements IP01.GetP01Delete
        Try
            Dim P01DA As New SQLEngine.SLIK.P01
            Return P01DA.GetP01Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.P01) As Parameter.P01 Implements IP01.GetCboBulandataSIPP
        Try
            Dim P01DA As New SQLEngine.SLIK.P01
            Return P01DA.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
End Class

