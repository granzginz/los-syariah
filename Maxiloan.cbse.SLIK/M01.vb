﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class M01 : Inherits ComponentBase
    Implements IM01

    Public Function GetM01(oCustomClass As Parameter.M01) As Parameter.M01 Implements IM01.GetM01
        Try
            Dim M01DA As New SQLEngine.SLIK.M01
            Return M01DA.GetM01(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSelectM01(oCustomClass As Parameter.M01) As Parameter.M01 Implements IM01.GetSelectM01
        Try
            Dim M01DA As New SQLEngine.SLIK.M01
            Return M01DA.GetSelectM01(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetM01Edit(oCustomClass As Parameter.M01) As Parameter.M01 Implements IM01.GetM01Edit
        Try
            Dim M01DA As New SQLEngine.SLIK.M01
            Return M01DA.GetM01Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetM01Save(oCustomClass As Parameter.M01) As Parameter.M01 Implements IM01.GetM01Save
        Try
            Dim M01DA As New SQLEngine.SLIK.M01
            Return M01DA.GetM01Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetM01Add(oCustomClass As Parameter.M01) As Parameter.M01 Implements IM01.GetM01Add
        Try
            Dim M01DA As New SQLEngine.SLIK.M01
            Return M01DA.GetM01Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetM01Delete(oCustomClass As Parameter.M01) As String Implements IM01.GetM01Delete
        Try
            Dim M01DA As New SQLEngine.SLIK.M01
            Return M01DA.GetM01Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.M01) As Parameter.M01 Implements IM01.GetCboBulandataSIPP
        Try
            Dim M01DA As New SQLEngine.SLIK.M01
            Return M01DA.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function


End Class
