﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class S01 : Inherits ComponentBase
    Implements IS01

    Public Function GetS01(oCustomClass As Parameter.S01) As Parameter.S01 Implements IS01.GetS01
        Try
            Dim S01DA As New SQLEngine.SLIK.S01
            Return S01DA.GetS01(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetSelectS01(oCustomClass As Parameter.S01) As Parameter.S01 Implements IS01.GetSelectS01
        Try
            Dim S01DA As New SQLEngine.SLIK.S01
            Return S01DA.GetSelectS01(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetS01Edit(oCustomClass As Parameter.S01) As Parameter.S01 Implements IS01.GetS01Edit
        Try
            Dim S01DA As New SQLEngine.SLIK.S01
            Return S01DA.GetS01Edit(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetS01Save(oCustomClass As Parameter.S01) As Parameter.S01 Implements IS01.GetS01Save
        Try
            Dim S01DA As New SQLEngine.SLIK.S01
            Return S01DA.GetS01Save(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetS01Add(oCustomClass As Parameter.S01) As Parameter.S01 Implements IS01.GetS01Add
        Try
            Dim S01DA As New SQLEngine.SLIK.S01
            Return S01DA.GetS01Add(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetS01Delete(oCustomClass As Parameter.S01) As String Implements IS01.GetS01Delete
        Try
            Dim S01DA As New SQLEngine.SLIK.S01
            Return S01DA.GetS01Delete(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetCboBulandataSIPP(oCustomClass As Parameter.S01) As Parameter.S01 Implements IS01.GetCboBulandataSIPP
        Try
            Dim S01DA As New SQLEngine.SLIK.S01
            Return S01DA.GetCboBulandataSIPP(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function
    Public Function GetCbo(oCustomClass As Parameter.S01) As Parameter.S01 Implements IS01.GetCbo
        Try
            Dim S01DA As New SQLEngine.SLIK.S01
            Return S01DA.GetCbo(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

    Public Function GetCbo1(oCustomClass As Parameter.S01) As Parameter.S01 Implements IS01.GetCbo1
        Try
            Dim S01DA As New SQLEngine.SLIK.S01
            Return S01DA.GetCbo1(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error ", ex)
        End Try
        Throw New NotImplementedException()
    End Function

End Class

