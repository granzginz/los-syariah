﻿Imports Maxiloan.Interface.Lending
Imports Maxiloan.Parameter.Lending

Public Class LendingProcess
    Implements ILendingProcess

    Private ReadOnly _da As New SQLEngine.Lending.LendingProcessDataAccess

    Public Function ProcessUploadCsvLending(cnn As String, data As Dictionary(Of String, IList(Of AbsDisb))) As String Implements ILendingProcess.ProcessUploadCsvLending
        Return _da.ProcessUploadCsvLending(cnn, data)
    End Function
End Class
