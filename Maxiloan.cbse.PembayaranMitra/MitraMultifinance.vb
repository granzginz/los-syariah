﻿
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface.Lending
Imports Maxiloan.Parameter.Lending
Imports Maxiloan.SQLEngine.Lending

Public Class MitraMultifinance : Inherits ComponentBase
    Implements IMitraMultifinance

    Public Function MitraList(ByVal co As Parameter.Lending.MitraMultifinance) As Parameter.Lending.MitraMultifinance Implements [Interface].Lending.IMitraMultifinance.MitraList
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.MitraList(co)
    End Function

    Public Function MitraAdd(ByVal co As Parameter.Lending.MitraMultifinance, ByVal oClassAddress As Parameter.Address) As String Implements [Interface].Lending.IMitraMultifinance.MitraAdd
        Try
            Dim DA As New SQLEngine.Lending.MitraMultifinance
            DA.MitraAdd(co, oClassAddress)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub MitraDelete(ByVal co As Parameter.Lending.MitraMultifinance) Implements [Interface].Lending.IMitraMultifinance.MitraDelete
        Try
            Dim DA As New SQLEngine.Lending.MitraMultifinance
            DA.MitraDelete(co)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Sub MitraEdit(ByVal co As Parameter.Lending.MitraMultifinance, ByVal oClassAddress As Parameter.Address) Implements [Interface].Lending.IMitraMultifinance.MitraEdit
        Try
            Dim DA As New SQLEngine.Lending.MitraMultifinance
            DA.MitraEdit(co, oClassAddress)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Function MitraListByID(ByVal co As Parameter.Lending.MitraMultifinance) As Parameter.Lending.MitraMultifinance Implements [Interface].Lending.IMitraMultifinance.MitraListByID
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.ListMitraByID(co)
    End Function

    Public Function MitraReport(ByVal co As Parameter.Lending.MitraMultifinance) As Parameter.Lending.MitraMultifinance Implements [Interface].Lending.IMitraMultifinance.MitraReport
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.ListMitraByID(co)
    End Function

    Public Function DisbursementHeaderList(ByVal strConnection As String) As DataTable Implements [Interface].Lending.IMitraMultifinance.DisbursementHeaderList
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.DisbursementHeaderList(strConnection)
    End Function

    Public Function LendingValidationList(ByVal strConnection As String) As List(Of Parameter.Lending.AgreementToValidate) Implements [Interface].Lending.IMitraMultifinance.LendingValidationList
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.LendingValidationList(strConnection)
    End Function
    Public Function DisburseAgreementApproveReject(ByVal oDisburseAgreement As Parameter.Lending.DisburseAgreement) As Boolean Implements [Interface].Lending.IMitraMultifinance.DisburseAgreementApproveReject
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.DisburseAgreementApproveReject(oDisburseAgreement)
    End Function

    Public Function DisburseHeaderUpdate(ByVal oDisburseAgreement As Parameter.Lending.DisburseAgreement) As Boolean Implements [Interface].Lending.IMitraMultifinance.DisburseHeaderUpdate
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.DisburseHeaderUpdate(oDisburseAgreement)
    End Function
    Public Function RekeningMitraList(ByVal co As Parameter.Lending.MitraMultifinance) As Parameter.Lending.MitraMultifinance Implements [Interface].Lending.IMitraMultifinance.RekeningMitraList
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.RekeningMitraList(co)
    End Function
    Public Function RekeningMitraAdd(ByVal co As Parameter.Lending.MitraMultifinance) As String Implements [Interface].Lending.IMitraMultifinance.RekeningMitraAdd
        Try
            Dim DA As New SQLEngine.Lending.MitraMultifinance
            DA.RekeningMitraAdd(co)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub RekeningMitraDelete(ByVal co As Parameter.Lending.MitraMultifinance) Implements [Interface].Lending.IMitraMultifinance.RekeningMitraDelete
        Try
            Dim DA As New SQLEngine.Lending.MitraMultifinance
            DA.RekeningMitraDelete(co)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Sub RekeningMitraEdit(ByVal co As Parameter.Lending.MitraMultifinance) Implements [Interface].Lending.IMitraMultifinance.RekeningMitraEdit
        Try
            Dim DA As New SQLEngine.Lending.MitraMultifinance
            DA.RekeningMitraEdit(co)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub
    Public Function RekeningMitraListByID(ByVal co As Parameter.Lending.MitraMultifinance) As Parameter.Lending.MitraMultifinance Implements [Interface].Lending.IMitraMultifinance.RekeningMitraListByID
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.RekeningMitraListByID(co)
    End Function

    Public Function uploadlist(ocls As Parameter.Lending.Lending) As Parameter.Lending.Lending Implements IMitraMultifinance.uploadlist
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.uploadlist(ocls)
    End Function

    Public Function lendingdrawdownrequest(customclass As Parameter.Lending.Lending) As Parameter.Lending.Lending Implements IMitraMultifinance.lendingdrawdownrequest
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.lendingdrawdownrequest(customclass)
    End Function
    Public Function lendingdrawdownapproval(customclass As Parameter.Lending.Lending) As Parameter.Lending.Lending Implements IMitraMultifinance.lendingdrawdownapproval
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.lendingdrawdownapproval(customclass)
    End Function
    Public Function disburselist(customclass As Parameter.Lending.Lending) As Parameter.Lending.Lending Implements IMitraMultifinance.disburselist
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.disburselist(customclass)
    End Function

    Public Function disburse(customclass As Parameter.Lending.Lending) As Parameter.Lending.Lending Implements IMitraMultifinance.disburse
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.disburse(customclass)
    End Function

    Public Function batchlist(customclass As Parameter.Lending.Lending) As Parameter.Lending.Lending Implements IMitraMultifinance.batchlist
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.batchlist(customclass)
    End Function

    Public Function LendingPaymentSave(ByVal oDisburseAgreement As Parameter.Lending.Lending) As Parameter.Lending.Lending Implements IMitraMultifinance.LendingPament
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.lendingpayment(oDisburseAgreement)
    End Function

    Public Function PaymentByDueDateListPaging(customclass As Parameter.Lending.Lending) As Parameter.Lending.Lending Implements [Interface].Lending.IMitraMultifinance.PaymentByDueDateListPaging
        Try
            Dim DA As New Maxiloan.SQLEngine.Lending.MitraMultifinance
            Return DA.PaymentByDueDateListPaging(customclass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetLendingFacilityByCode(ByVal mfCode As String, ByVal facilityNo As String, ByVal strConnection As String) As Maxiloan.Parameter.Lending.LendingFacility Implements [Interface].Lending.IMitraMultifinance.GetLendingFacilityByCode
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.GetLendingFacilityByCode(mfCode, facilityNo, strConnection)
    End Function

    Public Function paymentCalculate(customclass As Parameter.Lending.PaymentCalculateProperty) As Parameter.Lending.PaymentCalculateProperty Implements [Interface].Lending.IMitraMultifinance.paymentCalculate
        Try
            Dim DA As New LendingPaymentCalculateSQLEngine
            Return DA.lendingPrepaymentCalculate(customclass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub paymentCalculateDML(customclass As Parameter.Lending.PaymentCalculateProperty) Implements [Interface].Lending.IMitraMultifinance.paymentCalculateDML
        Try
            Dim DA As New LendingPaymentCalculateSQLEngine
            DA.setLendingCalculate(customclass)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub lendingRateDML(dml As String, customclass As Parameter.Lending.LendingRateProperty) Implements [Interface].Lending.IMitraMultifinance.lendingRateDML
        Try
            Dim DA As New LendingRateSQLEngine
            DA.lendingRateDML(dml, customclass)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function lendingRateListPaging(customclass As Parameter.Lending.LendingRateProperty) As LendingRateProperty Implements [Interface].Lending.IMitraMultifinance.lendingRateListPaging
        Try
            Dim DA As New LendingRateSQLEngine
            Return DA.lendingRateListPaging(customclass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function lendingBatchListPaging(customclass As Parameter.Lending.Lending) As Parameter.Lending.Lending Implements [Interface].Lending.IMitraMultifinance.lendingBatchListPaging
        Try
            Dim DA As New LendingRateSQLEngine
            Return DA.lendingBatchListPaging(customclass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DraftSoftcopy(ByVal customClass As Parameter.Lending.Lending) As System.Data.DataSet Implements [Interface].Lending.IMitraMultifinance.DraftSoftcopy
        Try
            Dim DA As New LendingRateSQLEngine
            Return DA.DraftSoftcopy(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub lendingFacilityDML(dml As String, customclass As Parameter.Lending.LendingFacility) Implements [Interface].Lending.IMitraMultifinance.lendingFacilityDML
        Dim DA As New Maxiloan.SQLEngine.Lending.LendingFacilitySQLEngine
        DA.lendingFacilityDML(dml, customclass)
    End Sub

    Public Function lendingpayment(customclass As Parameter.Lending.Lending) As Parameter.Lending.Lending Implements [Interface].Lending.IMitraMultifinance.lendingpayment
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.lendingpayment(customclass)
    End Function

    Public Function lendinginstallmentinfo(customclass As Parameter.Lending.Lending) As Parameter.Lending.Lending Implements [Interface].Lending.IMitraMultifinance.lendinginstallmentinfo
        Dim DA As New SQLEngine.Lending.MitraMultifinance
        Return DA.lendinginstallmentinfo(customclass)
    End Function
End Class
