﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
#End Region

Public Class MitraMultifinance : Inherits ComponentBase
    Implements IMitraMultifinance

    Public Function MitraList(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance Implements [Interface].IMitraMultifinance.MitraList
        Dim DA As New SQLEngine.PembayaranMitra.MitraMultifinance
        Return DA.MitraList(co)
    End Function

    Public Function MitraAdd(ByVal co As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As String Implements [Interface].IMitraMultifinance.MitraAdd
        Try
            Dim DA As New SQLEngine.PembayaranMitra.MitraMultifinance
            DA.MitraAdd(co, oClassAddress, oClassPersonal)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub MitraDelete(ByVal co As Parameter.MitraMultifinance) Implements [Interface].IMitraMultifinance.MitraDelete
        Try
            Dim DA As New SQLEngine.PembayaranMitra.MitraMultifinance
            DA.MitraDelete(co)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Sub MitraEdit(ByVal co As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) Implements [Interface].IMitraMultifinance.MitraEdit
        Try
            Dim DA As New SQLEngine.PembayaranMitra.MitraMultifinance
            DA.MitraEdit(co, oClassAddress, oClassPersonal)

        Catch ex As Exception

            Throw ex
        End Try
    End Sub

    Public Function MitraListByID(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance Implements [Interface].IMitraMultifinance.MitraListByID
        Dim DA As New SQLEngine.PembayaranMitra.MitraMultifinance
        Return DA.ListMitraByID(co)
    End Function

    'Public Function GetComboInputType(ByVal strcon As String) As System.Data.DataTable Implements [Interface].IMitraMultifinance.GetComboInputType
    '    Dim DA As New SQLEngine.PembayaranMitra.MitraMultifinance
    '    Return DA.MitraEdit(strcon)
    'End Function

    Public Function MitraReport(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance Implements [Interface].IMitraMultifinance.MitraReport
        Dim DA As New SQLEngine.PembayaranMitra.MitraMultifinance
        Return DA.ListMitraByID(co)
    End Function

End Class
