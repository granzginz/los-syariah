
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class AdminActivity : Inherits ComponentBase
    Implements IAdminActivity

    Public Function AdminActivityList(ByVal customclass As Parameter.AdminActivity) As Parameter.AdminActivity Implements [Interface].IAdminActivity.AdminActivityList
        Dim DAAdmActivity As New SQLEngine.ARMgt.AdminActivity
        Return DAAdmActivity.ListAdminActivity(customclass)
    End Function

    Public Function AdminActivityDataAgreement(ByVal customclass As Parameter.AdminActivity) As Parameter.AdminActivity Implements [Interface].IAdminActivity.AdminActivityDataAgreement
        Dim DAAdmActivity As New SQLEngine.ARMgt.AdminActivity
        Return DAAdmActivity.AdminActivityDatAgreement(customclass)
    End Function

    Public Function AdminActivityAction(ByVal customclass As Parameter.AdminActivity) As Parameter.AdminActivity Implements [Interface].IAdminActivity.AdminActivityAction
        Dim DAAdmActivity As New SQLEngine.ARMgt.AdminActivity
        Return DAAdmActivity.AdminActivityAction(customclass)
    End Function

    Public Function AdminActivitySave(ByVal customclass As Parameter.AdminActivity) As Parameter.AdminActivity Implements [Interface].IAdminActivity.AdminActivitySave
        Dim DAAdmActivity As New SQLEngine.ARMgt.AdminActivity
        Return DAAdmActivity.AdminActivitySave(customclass)
    End Function
End Class
