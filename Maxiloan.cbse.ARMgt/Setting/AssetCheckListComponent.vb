

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region

Public Class AssetCheckListComponent : Inherits ComponentBase
    Implements IAssetCheckList

    Public Function ListAssetCheckList(ByVal costumclass As Parameter.AssetCheckList) As Parameter.AssetCheckList Implements [Interface].IAssetCheckList.ListAssetCheckList
        Dim DAAssetCheckList As New SQLEngine.ARMgt.AssetCheckList
        Return DAAssetCheckList.ListAssetCheckList(costumclass)
    End Function

    Public Function EditAssetCheckList(ByVal customclass As Parameter.AssetCheckList) As String Implements [Interface].IAssetCheckList.EditAssetCheckList
        Dim DAAssetCheckListEdit As New SQLEngine.ARMgt.AssetCheckList
        Return DAAssetCheckListEdit.EditAssetCheckList(customclass)
    End Function

    Public Function AddAssetCheckList(ByVal customclass As Parameter.AssetCheckList) As String Implements [Interface].IAssetCheckList.AddAssetCheckList
        Dim DAAssetCheckListAdd As New SQLEngine.ARMgt.AssetCheckList
        Return DAAssetCheckListAdd.AddAssetCheckList(customclass)
    End Function

    Public Function ListAssetCheckListLookUp(ByVal costumclass As Parameter.AssetCheckList) As Parameter.AssetCheckList Implements [Interface].IAssetCheckList.ListAssetCheckListLookUp
        Dim DAAssetCheckList As New SQLEngine.ARMgt.AssetCheckList
        Return DAAssetCheckList.ListAssetCheckListLookUp(costumclass)
    End Function

    Public Function DeleteAssetCheckList(ByVal customclass As Parameter.AssetCheckList) As String Implements [Interface].IAssetCheckList.DeleteAssetCheckList
        Dim DAAssetCheckListDelete As New SQLEngine.ARMgt.AssetCheckList
        Return DAAssetCheckListDelete.DeleteAssetCheckList(customclass)
    End Function
    Public Function ReportAssetCheckList(ByVal costumclass As Parameter.AssetCheckList) As Parameter.AssetCheckList Implements [Interface].IAssetCheckList.ReportAssetCheckList
        Dim DAAssetCheckList As New SQLEngine.ARMgt.AssetCheckList
        Return DAAssetCheckList.ReportAssetCheckList(costumclass)
    End Function
End Class
