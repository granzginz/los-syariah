
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ARMgt
Imports Maxiloan.Interface


Public Class ActionResultComponent : Inherits ComponentBase
    Implements IActionResult



    Public Sub ActionAdd(ByVal oActionResult As Parameter.ActionResult) Implements [Interface].IActionResult.ActionAdd
        Try
            Dim DA As New SQLEngine.ARMgt.ActionResult
            DA.ActionAdd(oActionResult)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub ActionDelete(ByVal oActionResult As Parameter.ActionResult) Implements [Interface].IActionResult.ActionDelete
        Try
            Dim DA As New SQLEngine.ARMgt.ActionResult
            DA.ActionDelete(oActionResult)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub ActionEdit(ByVal oActionResult As Parameter.ActionResult) Implements [Interface].IActionResult.ActionEdit
        Try
            Dim DA As New SQLEngine.ARMgt.ActionResult
            DA.ActionEdit(oActionResult)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function GetActionByID(ByVal oActionResult As Parameter.ActionResult) As Parameter.ActionResult Implements [Interface].IActionResult.GetActionByID
        Dim DA As New SQLEngine.ARMgt.ActionResult
        Return DA.GetActionByID(oActionResult)
    End Function

    Public Function GetActionList(ByVal oActionResult As Parameter.ActionResult) As Parameter.ActionResult Implements [Interface].IActionResult.GetActionList
        Dim DA As New SQLEngine.ARMgt.ActionResult
        Return DA.GetActionList(oActionResult)
    End Function

    Public Function GetResultByID(ByVal oActionResult As Parameter.ActionResult) As Parameter.ActionResult Implements [Interface].IActionResult.GetResultByID
        Dim DA As New SQLEngine.ARMgt.ActionResult
        Return DA.GetResultByID(oActionResult)
    End Function

    Public Function GetResultList(ByVal oActionResult As Parameter.ActionResult) As Parameter.ActionResult Implements [Interface].IActionResult.GetResultList
        Dim DA As New SQLEngine.ARMgt.ActionResult
        Return DA.GetResultList(oActionResult)
    End Function

    Public Sub ResultAdd(ByVal oActionResult As Parameter.ActionResult) Implements [Interface].IActionResult.ResultAdd
        Try
            Dim DA As New SQLEngine.ARMgt.ActionResult
            DA.ResultAdd(oActionResult)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub ResultDelete(ByVal oActionResult As Parameter.ActionResult) Implements [Interface].IActionResult.ResultDelete
        Try
            Dim DA As New SQLEngine.ARMgt.ActionResult
            DA.ResultDelete(oActionResult)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub ResultEdit(ByVal oActionResult As Parameter.ActionResult) Implements [Interface].IActionResult.ResultEdit
        Try
            Dim DA As New SQLEngine.ARMgt.ActionResult
            DA.ResultEdit(oActionResult)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function GetResultCategory(ByVal str As String) As Parameter.ActionResult Implements [Interface].IActionResult.GetResultCategory
        Dim DA As New SQLEngine.ARMgt.ActionResult
        Return DA.GetResultCategory(str)
    End Function

    Public Function GetActionReport(ByVal strconn As String) As Parameter.ActionResult Implements [Interface].IActionResult.GetActionReport
        Dim DA As New SQLEngine.ARMgt.ActionResult
        Return DA.GetActionReport(strconn)
    End Function
End Class
