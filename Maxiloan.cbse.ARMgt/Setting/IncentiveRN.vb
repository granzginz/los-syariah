
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ARMgt
#End Region

Public Class IncentiveRN : Inherits ComponentBase
    Implements IIncentiveRN

    Public Sub IncentiveRNDelete(ByVal oCustomClass As Parameter.IncentiveRN) Implements [Interface].IIncentiveRN.IncentiveRNDelete
        Dim DAIncentive As New SQLEngine.ARMgt.IncentiveRN
        DAIncentive.IncentiveRNDelete(oCustomClass)
    End Sub

    Public Function IncentiveRNPaging(ByVal oCustomClass As Parameter.IncentiveRN) As Parameter.IncentiveRN Implements [Interface].IIncentiveRN.IncentiveRNPaging
        Dim DAIncentive As New SQLEngine.ARMgt.IncentiveRN
        Return DAIncentive.IncentiveRNPaging(oCustomClass)
    End Function

    Public Function IncentiveRNReports(ByVal oCustomClass As Parameter.IncentiveRN) As Parameter.IncentiveRN Implements [Interface].IIncentiveRN.IncentiveRNReports
        Dim DAIncentive As New SQLEngine.ARMgt.IncentiveRN
        Return DAIncentive.IncentiveRNReports(oCustomClass)
    End Function

    Public Sub IncentiveRNSave(ByVal oCustomClass As Parameter.IncentiveRN) Implements [Interface].IIncentiveRN.IncentiveRNSave
        Dim DAIncentive As New SQLEngine.ARMgt.IncentiveRN
        DAIncentive.IncentiveRNSave(oCustomClass)
    End Sub

    Public Sub IncentiveRNUpdate(ByVal oCustomClass As Parameter.IncentiveRN) Implements [Interface].IIncentiveRN.IncentiveRNUpdate
        Dim DAIncentive As New SQLEngine.ARMgt.IncentiveRN
        DAIncentive.IncentiveRNUpdate(oCustomClass)
    End Sub
End Class
