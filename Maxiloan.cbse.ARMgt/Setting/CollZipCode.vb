

Imports Maxiloan.cbse
Imports Maxiloan.cbse.ARMgt
Imports Maxiloan.Interface

Public Class CollZipCode : Inherits ComponentBase
    Implements ICollZipCode

    Public Sub CollZipCodeEdit(ByVal oCollZipCode As Parameter.CollZipCode) Implements [Interface].ICollZipCode.CollZipCodeEdit
        Try
            Dim DA As New SQLEngine.ARMgt.CollZipCode
            DA.CollZipCodeEdit(oCollZipCode)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function getCGCombo(ByVal strconn As String) As System.Data.DataTable Implements [Interface].ICollZipCode.getCGCombo
        Dim DA As New SQLEngine.ARMgt.CollZipCode
        Return DA.GetCGCombo(strconn)
    End Function

    Public Function GetCollZipCodeList(ByVal oCollZipCode As Parameter.CollZipCode) As Parameter.CollZipCode Implements [Interface].ICollZipCode.GetCollZipCodeList
        Dim DA As New SQLEngine.ARMgt.CollZipCode
        Return DA.CollZipCodeList(oCollZipCode)
    End Function

    Public Function GetCityCombo(ByVal strconn As String) As System.Data.DataTable Implements [Interface].ICollZipCode.GetCityCombo
        Dim DA As New SQLEngine.ARMgt.CollZipCode
        Return DA.GetCityCombo(strconn)
    End Function

    Public Sub CollectorZipCodeEdit(ByVal oCollZipCode As Parameter.CollZipCode) Implements [Interface].ICollZipCode.CollectorZipCodeEdit
        Try
            Dim DA As New SQLEngine.ARMgt.CollZipCode
            DA.CollectorZipCodeEdit(oCollZipCode)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function getCollectorCombo(ByVal oCollZipCode As Parameter.CollZipCode) As System.Data.DataTable Implements [Interface].ICollZipCode.getCollectorCombo
        Dim DA As New SQLEngine.ARMgt.CollZipCode
        Return DA.GetCollectorCombo(oCollZipCode)
    End Function

    Public Function GetCollectorZipCodeList(ByVal oCollZipCode As Parameter.CollZipCode) As Parameter.CollZipCode Implements [Interface].ICollZipCode.GetCollectorZipCodeList
        Dim DA As New SQLEngine.ARMgt.CollZipCode
        Return DA.CollectorZipCodeList(oCollZipCode)
    End Function

    Public Function GetCollectorZipCodeReport(ByVal strconn As String, ByVal cgid As String) As Parameter.CollZipCode Implements [Interface].ICollZipCode.GetCollectorZipCodeReport
        Dim DA As New SQLEngine.ARMgt.CollZipCode
        Return DA.CollectorZipCodeReport(strconn, cgid)
    End Function

    Public Function GetCollZipCodeReport(ByVal strconn As String, ByVal city As String) As Parameter.CollZipCode Implements [Interface].ICollZipCode.GetCollZipCodeReport
        Dim DA As New SQLEngine.ARMgt.CollZipCode
        Return DA.CollGroupZipCodeReport(strconn, city)
    End Function


End Class
