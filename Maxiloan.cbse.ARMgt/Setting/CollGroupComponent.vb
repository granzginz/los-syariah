
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ARMgt
Imports Maxiloan.Interface



Public Class CollGroupComponent : Inherits ComponentBase
    Implements ICollGroup

    Public Sub CollGroupAdd(ByVal oCollGroup As Parameter.CollGroup, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) Implements [Interface].ICollGroup.CollGroupAdd
        Try
            Dim DA As New SQLEngine.ARMgt.CollGroup
            DA.CollGroupAdd(oCollGroup, oClassAddress, oClassPersonal)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub CollGroupDelete(ByVal oCollGroup As Parameter.CollGroup) Implements [Interface].ICollGroup.CollGroupDelete
        Try
            Dim DA As New SQLEngine.ARMgt.CollGroup
            DA.CollGroupDelete(oCollGroup)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub CollGroupEdit(ByVal oCollGroup As Parameter.CollGroup, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) Implements [Interface].ICollGroup.CollGroupEdit
        Try
            Dim DA As New SQLEngine.ARMgt.CollGroup
            DA.CollGroupEdit(oCollGroup, oClassAddress, oClassPersonal)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function GetCollGroupByID(ByVal oCollGroup As Parameter.CollGroup) As Parameter.CollGroup Implements [Interface].ICollGroup.GetCollGroupByID
        Dim DA As New SQLEngine.ARMgt.CollGroup
        Return DA.GetCollGroupByID(oCollGroup)
    End Function

    Public Function GetCollGroupList(ByVal oCollGroup As Parameter.CollGroup) As Parameter.CollGroup Implements [Interface].ICollGroup.GetCollGroupList
        Dim DA As New SQLEngine.ARMgt.CollGroup
        Return DA.GetCollGroupList(oCollGroup)
    End Function

    Public Function GetCollGroupReport(ByVal strconn As String) As Parameter.CollGroup Implements [Interface].ICollGroup.GetCollGroupReport
        Dim DA As New SQLEngine.ARMgt.CollGroup
        Return DA.GetCollGroupReport(strconn)
    End Function

    Public Function GetBranchAddress(ByVal oCollGroup As Parameter.CollGroup) As DataTable Implements [Interface].ICollGroup.GetBranchAddress
        Dim DA As New SQLEngine.ARMgt.CollGroup
        Return DA.GetBranchAddress(oCollGroup)
    End Function

    Public Function GetBranchCombo(ByVal oCollGroup As Parameter.CollGroup) As Parameter.CollGroup Implements [Interface].ICollGroup.GetBranchCombo
        Dim DA As New SQLEngine.ARMgt.CollGroup
        Return DA.GetBranchCombo(oCollGroup)
    End Function
End Class
