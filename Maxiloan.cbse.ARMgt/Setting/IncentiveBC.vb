
#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ARMgt
Imports Maxiloan.Interface
#End Region
Public Class IncentiveBC : Inherits ComponentBase
    Implements IIncentiveBC


    Public Sub IncentiveBCDelete(ByVal oCustomClass As Parameter.IncentiveBC) Implements [Interface].IIncentiveBC.IncentiveBCDelete
        Dim DAIncentiveBC As New SQLEngine.ARMgt.IncentiveBC
        DAIncentiveBC.IncentiveBCDelete(oCustomClass)

    End Sub

    Public Function IncentiveBCPaging(ByVal oCustomClass As Parameter.IncentiveBC) As Parameter.IncentiveBC Implements [Interface].IIncentiveBC.IncentiveBCPaging
        Dim DAIncentiveBC As New SQLEngine.ARMgt.IncentiveBC
        Return DAIncentiveBC.IncentiveBCPaging(oCustomClass)
    End Function

    Public Sub IncentiveBCSave(ByVal oCustomClass As Parameter.IncentiveBC) Implements [Interface].IIncentiveBC.IncentiveBCSave
        Dim DAIncentiveBC As New SQLEngine.ARMgt.IncentiveBC
        DAIncentiveBC.IncentiveBCSave(oCustomClass)
    End Sub

    Public Sub IncentiveBCUpdate(ByVal oCustomClass As Parameter.IncentiveBC) Implements [Interface].IIncentiveBC.IncentiveBCUpdate
        Dim DAIncentiveBC As New SQLEngine.ARMgt.IncentiveBC
        DAIncentiveBC.IncentiveBCUpdate(oCustomClass)
    End Sub

    Public Function IncentiveBCReports(ByVal oCustomClass As Parameter.IncentiveBC) As Parameter.IncentiveBC Implements [Interface].IIncentiveBC.IncentiveBCReports
        Dim DAIncentiveBC As New SQLEngine.ARMgt.IncentiveBC
        Return DAIncentiveBC.IncentiveBCReports(oCustomClass)
    End Function
End Class


