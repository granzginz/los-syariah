
Imports Maxiloan.Interface

Public Class CollectorComponent : Inherits ComponentBase
    Implements ICollector

    Public Function GetCollectorByID(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollector.GetCollectorByID
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.GetCollectorByID(oCollector)
    End Function

    Public Function GetCollectorList(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollector.GetCollectorList
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.GetCollectorList(oCollector)
    End Function

    Public Function GetCollectorReport(ByVal strconn As String) As Parameter.Collector Implements [Interface].ICollector.GetCollectorReport
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.GetCollectorReport(strconn)
    End Function

    Public Sub CollectorAdd(ByVal oCollector As Parameter.Collector) Implements [Interface].ICollector.CollectorAdd
        Try
            Dim DA As New SQLEngine.ARMgt.Collector
            DA.CollectorAdd(oCollector)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub CollectorDelete(ByVal oCollector As Parameter.Collector) Implements [Interface].ICollector.CollectorDelete
        Try
            Dim DA As New SQLEngine.ARMgt.Collector
            DA.CollectorDelete(oCollector)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub CollectorEdit(ByVal oCollector As Parameter.Collector) Implements [Interface].ICollector.CollectorEdit
        Try
            Dim DA As New SQLEngine.ARMgt.Collector
            DA.CollectorEdit(oCollector)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function GetBranchAddress(ByVal oCollector As Parameter.Collector) As String Implements [Interface].ICollector.GetBranchAddress

    End Function

    Public Function GetCollectorZipCode(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollector.GetCollectorZipCode
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.GetCollectorZipCode(oCollector)
    End Function

    Public Function GetCollectorRAL(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollector.GetCollectorRAL
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.GetCollectorRAL(oCollector)
    End Function

    Public Function GetCollectorDetail(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollector.GetCollectorDetail
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.GetCollectorDetail(oCollector)
    End Function

    Public Function GetCollectorCombo(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollector.GetCollectorCombo
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.GetCollectorCombo(oCollector)
    End Function

    Public Function GetCollectorTypeCombo(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollector.GetCollectorTypeCombo
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.GetCollectorTypeCombo(oCollector)
    End Function

    Public Function GetSupervisorCombo(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollector.GetSupervisorCombo
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.GetSupervisorCombo(oCollector)
    End Function

    Public Function GetCollectorAllocationPaging(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollector.GetCollectorAllocationPaging
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.GetCollectorAllocationPaging(oCollector)
    End Function

    Public Sub CollectorAllocationSave(ByVal oCollector As Parameter.Collector) Implements [Interface].ICollector.CollectorAllocationSave
        Dim DA As New SQLEngine.ARMgt.Collector
        DA.CollectorAllocationSave(oCollector)
    End Sub

    Public Function CollectorZPSave(ByVal oCollector As Parameter.Collector, ByVal oData As System.Data.DataTable) As Parameter.Collector Implements [Interface].ICollector.CollectorZPSave
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.CollectorZPSave(oCollector, oData)
    End Function

    Public Function GetCollectorZPList(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollector.GetCollectorZPList
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.GetCollectorZPList(oCollector)
    End Function

    Public Function GetCollectorZPByKecamatan(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollector.GetCollectorZPByKecamatan
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.GetCollectorZPByKecamatan(oCollector)
    End Function

    Public Function GetCollectorZPDetail(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollector.GetCollectorZPDetail
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.GetCollectorZPDetail(oCollector)
    End Function

    Public Sub CollectorEditMaxAcc(ByVal oCollector As Parameter.Collector) Implements [Interface].ICollector.CollectorEditMaxAcc
        Dim DA As New SQLEngine.ARMgt.Collector
        DA.CollectorEditMaxAcc(oCollector)
    End Sub

    Public Function GetCollectorDeskCollByID(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollector.GetCollectorDeskCollByID
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.GetCollectorDeskCollByID(oCollector)
    End Function

    Public Sub CollectorDeskCollSave(ByVal oCollector As Parameter.Collector) Implements [Interface].ICollector.CollectorDeskCollSave
        Dim DA As New SQLEngine.ARMgt.Collector
        DA.CollectorDeskCollSave(oCollector)
    End Sub

    Public Sub RollingWilayahSave(ByVal oCollector As Parameter.Collector) Implements [Interface].ICollector.RollingWilayahSave
        Dim DA As New SQLEngine.ARMgt.Collector
        DA.RollingWilayahSave(oCollector)
    End Sub

    Public Sub RollingCollectorSave(ByVal oCollector As Parameter.Collector) Implements [Interface].ICollector.RollingCollectorSave
        Dim DA As New SQLEngine.ARMgt.Collector
        DA.RollingCollectorSave(oCollector)
    End Sub

    Public Sub GantiCollectorSave(oCollector As Parameter.Collector) Implements [Interface].ICollector.GantiCollectorSave
        Dim DA As New SQLEngine.ARMgt.Collector
        DA.GantiCollectorSave(oCollector)
    End Sub

    Public Function GetPemberiKuasaSKEList(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollector.GetPemberiKuasaSKEList
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.GetPemberiKuasaSKEList(oCollector)
    End Function

    Public Function PemberiKuasaSKEAdd(ByVal oCollector As Parameter.Collector) As String Implements [Interface].ICollector.PemberiKuasaSKEAdd
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.PemberiKuasaSKEAdd(oCollector)
    End Function

    Public Function PemberiKuasaSKEEdit(ByVal oCollector As Parameter.Collector) As String Implements [Interface].ICollector.PemberiKuasaSKEEdit
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.PemberiKuasaSKEEdit(oCollector)
    End Function

    Public Function PemberiKuasaSKEDelete(ByVal oCollector As Parameter.Collector) As String Implements [Interface].ICollector.PemberiKuasaSKEDelete
        Dim DA As New SQLEngine.ARMgt.Collector
        Return DA.PemberiKuasaSKEDelete(oCollector)
    End Function
End Class
