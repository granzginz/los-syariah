
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ARMgt
#End Region

Public Class IncentiveEXE : Inherits ComponentBase
    Implements IIncentiveEXE


    Public Sub IncentiveEXEDelete(ByVal oCustomClass As Parameter.IncentiveEXE) Implements [Interface].IIncentiveEXE.IncentiveEXEDelete
        Dim DAIncentiveEXE As New SQLEngine.ARMgt.IncentiveEXE
        DAIncentiveEXE.IncentiveEXEDelete(oCustomClass)
    End Sub

    Public Function IncentiveEXEPaging(ByVal oCustomClass As Parameter.IncentiveEXE) As Parameter.IncentiveEXE Implements [Interface].IIncentiveEXE.IncentiveEXEPaging
        Dim DAIncentiveEXE As New SQLEngine.ARMgt.IncentiveEXE
        Return DAIncentiveEXE.IncentiveEXEPaging(oCustomClass)
    End Function

    Public Function IncentiveEXEReports(ByVal oCustomClass As Parameter.IncentiveEXE) As Parameter.IncentiveEXE Implements [Interface].IIncentiveEXE.IncentiveEXEReports
        Dim DAIncentiveEXE As New SQLEngine.ARMgt.IncentiveEXE
        Return DAIncentiveEXE.IncentiveEXEReports(oCustomClass)
    End Function

    Public Sub IncentiveEXESave(ByVal oCustomClass As Parameter.IncentiveEXE) Implements [Interface].IIncentiveEXE.IncentiveEXESave
        Dim DAIncentiveEXE As New SQLEngine.ARMgt.IncentiveEXE
        DAIncentiveEXE.IncentiveEXESave(oCustomClass)
    End Sub

    Public Sub IncentiveEXEUpdate(ByVal oCustomClass As Parameter.IncentiveEXE) Implements [Interface].IIncentiveEXE.IncentiveEXEUpdate
        Dim DAIncentiveEXE As New SQLEngine.ARMgt.IncentiveEXE
        DAIncentiveEXE.IncentiveEXEUpdate(oCustomClass)
    End Sub
End Class
