Imports Maxiloan.cbse
Imports Maxiloan.cbse.ARMgt
Imports Maxiloan.Interface

Public Class CollectorProfComponent : Inherits ComponentBase
    Implements ICollectorProf

    Public Sub CollectorProfAdd(ByVal oCollector As Parameter.Collector, ByVal oAddress As Parameter.Address, ByVal opersonal As Parameter.Personal) Implements [Interface].ICollectorProf.CollectorProfAdd
        Try
            Dim DA As New SQLEngine.ARMgt.CollectorProf
            DA.CollectorProfAdd(oCollector, oAddress, opersonal)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub CollectorProfDelete(ByVal oCollector As Parameter.Collector) Implements [Interface].ICollectorProf.CollectorProfDelete
        Try
            Dim DA As New SQLEngine.ARMgt.CollectorProf
            DA.CollectorProfDelete(oCollector)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub CollectorProfEdit(ByVal oCollector As Parameter.Collector, ByVal oAddress As Parameter.Address, ByVal opersonal As Parameter.Personal) Implements [Interface].ICollectorProf.CollectorProfEdit
        Try
            Dim DA As New SQLEngine.ARMgt.CollectorProf
            DA.CollectorProfEdit(oCollector, oAddress, opersonal)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function GetCollectorProfByID(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollectorProf.GetCollectorProfByID
        Dim DA As New SQLEngine.ARMgt.CollectorProf
        Return DA.GetCollectorProfByID(oCollector)
    End Function

    Public Function GetCollectorProfDetail(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollectorProf.GetCollectorProfDetail
        Dim DA As New SQLEngine.ARMgt.CollectorProf
        Return DA.GetCollectorProfDetail(oCollector)
    End Function

    Public Function GetCollectorProfList(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollectorProf.GetCollectorProfList
        Dim DA As New SQLEngine.ARMgt.CollectorProf
        Return DA.GetCollectorProfList(oCollector)
    End Function

    Public Function GetCollectorProfReport(ByVal strconn As String) As Parameter.Collector Implements [Interface].ICollectorProf.GetCollectorProfReport
        Dim DA As New SQLEngine.ARMgt.CollectorProf
        Return DA.GetCollectorProfReport(strconn)
    End Function

    Public Function GetSupervisorCombo(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollectorProf.GetSupervisorCombo
        Dim DA As New SQLEngine.ARMgt.CollectorProf
        Return DA.GetSupervisorCombo(oCollector)
    End Function



    'CollectorMOU
    Public Function GetCollectorMOUByID(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollectorProf.GetCollectorMOUByID
        Dim DA As New SQLEngine.ARMgt.CollectorProf
        Return DA.GetCollectorMOUByID(oCollector)
    End Function

    Public Function GetCollectorMOUList(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollectorProf.GetCollectorMOUList
        Dim DA As New SQLEngine.ARMgt.CollectorProf
        Return DA.GetCollectorMOUList(oCollector)
    End Function


    Public Sub CollectorMOUAdd(ByVal oCollector As Parameter.Collector) Implements [Interface].ICollectorProf.CollectorMOUAdd
        Try
            Dim DA As New SQLEngine.ARMgt.CollectorProf
            DA.CollectorMOUAdd(oCollector)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub CollectorMOUDelete(ByVal oCollector As Parameter.Collector) Implements [Interface].ICollectorProf.CollectorMOUDelete
        Try
            Dim DA As New SQLEngine.ARMgt.CollectorProf
            DA.CollectorMOUDelete(oCollector)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub CollectorMOUEdit(ByVal oCollector As Parameter.Collector) Implements [Interface].ICollectorProf.CollectorMOUEdit
        Try
            Dim DA As New SQLEngine.ARMgt.CollectorProf
            DA.CollectorMOUEdit(oCollector)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function GetBranchAddress(ByVal oCollector As Parameter.Collector) As String Implements [Interface].ICollectorProf.GetBranchAddress

    End Function


    Public Function GetCollectorMOUDetail(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollectorProf.GetCollectorMOUDetail
        Dim DA As New SQLEngine.ARMgt.CollectorProf
        Return DA.GetCollectorMOUDetail(oCollector)
    End Function
    Public Sub CollectorMOUExtendUpdate(ByVal oCollector As Parameter.Collector) Implements [Interface].ICollectorProf.CollectorMOUExtendUpdate
        Try
            Dim DA As New SQLEngine.ARMgt.CollectorProf
            DA.CollectorMOUExtendUpdate(oCollector)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    'Public Sub CollectorMOUExtendHistory(ByVal oCollector As Parameter.Collector) Implements [Interface].ICollectorProf.CollectorMOUExtendHistory
    '    Try
    '        Dim DA As New SQLEngine.ARMgt.CollectorProf
    '        DA.CollectorMOUExtendHistory(oCollector)

    '    Catch ex As Exception
    '        'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
    '        Throw ex
    '    End Try
    'End Sub

    Public Function CollectorMOUExtendHistory(ByVal oCollector As Parameter.Collector) As Parameter.Collector Implements [Interface].ICollectorProf.CollectorMOUExtendHistory
        Dim DA As New SQLEngine.ARMgt.CollectorProf
        Return DA.CollectorMOUExtendHistory(oCollector)
    End Function

End Class
