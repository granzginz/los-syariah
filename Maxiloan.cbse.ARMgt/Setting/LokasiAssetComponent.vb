﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region

Public Class LokasiAssetComponent : Inherits ComponentBase
    Implements ILokasiAsset

    Public Function AddLokasiAsset(customclass As Parameter.LokasiAsset, oAddress As Parameter.Address) As String Implements [Interface].ILokasiAsset.AddLokasiAsset
        Dim DALokasiAssetAdd As New SQLEngine.ARMgt.LokasiAsset
        Return DALokasiAssetAdd.AddLokasiAsset(customclass, oAddress)
    End Function

    Public Function DeleteLokasiAsset(customclass As Parameter.LokasiAsset) As String Implements [Interface].ILokasiAsset.DeleteLokasiAsset
        Dim DALokasiAssetDelete As New SQLEngine.ARMgt.LokasiAsset
        Return DALokasiAssetDelete.DeleteLokasiAsset(customclass)
    End Function

    Public Function EditLokasiAsset(customclass As Parameter.LokasiAsset, oAddress As Parameter.Address) As String Implements [Interface].ILokasiAsset.EditLokasiAsset
        Dim DALokasiAssetEdit As New SQLEngine.ARMgt.LokasiAsset
        Return DALokasiAssetEdit.EditLokasiAsset(customclass, oAddress)
    End Function

    Public Function ListLokasiAsset(customclass As Parameter.LokasiAsset) As Parameter.LokasiAsset Implements [Interface].ILokasiAsset.ListLokasiAsset
        Dim DALokasiAsset As New SQLEngine.ARMgt.LokasiAsset
        Return DALokasiAsset.ListLokasiAsset(customclass)
    End Function
    Public Function ReportLokasiAsset(customclass As Parameter.LokasiAsset) As Parameter.LokasiAsset Implements [Interface].ILokasiAsset.ReportLokasiAsset
        Dim DALokasiAssetReport As New SQLEngine.ARMgt.LokasiAsset
        Return DALokasiAssetReport.ReportLokasiAsset(customclass)
    End Function
End Class
