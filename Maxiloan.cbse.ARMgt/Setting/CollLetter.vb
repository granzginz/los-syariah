
#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ARMgt
Imports Maxiloan.Interface

#End Region

Public Class CollLetter : Inherits ComponentBase
    Implements ICollLetter


    Public Sub CollLetterDelete(ByVal oCustomClass As Parameter.CollLetter) Implements [Interface].ICollLetter.CollLetterDelete
        Dim DACollLetter As New SQLEngine.ARMgt.CollLetter
        DACollLetter.CollLetterDelete(oCustomClass)
    End Sub

    Public Function CollLetterPaging(ByVal oCustomClass As Parameter.CollLetter) As Parameter.CollLetter Implements [Interface].ICollLetter.CollLetterPaging
        Dim DACollLetter As New SQLEngine.ARMgt.CollLetter
        Return DACollLetter.CollLetterPaging(oCustomClass)
    End Function

    Public Function CollLetterReports(ByVal oCustomClass As Parameter.CollLetter) As Parameter.CollLetter Implements [Interface].ICollLetter.CollLetterReports
        Dim DACollLetter As New SQLEngine.ARMgt.CollLetter
        Return DACollLetter.CollLetterReports(oCustomClass)
    End Function

    Public Sub CollLetterSave(ByVal oCustomClass As Parameter.CollLetter) Implements [Interface].ICollLetter.CollLetterSave
        Dim DACollLetter As New SQLEngine.ARMgt.CollLetter
        DACollLetter.CollLetterSave(oCustomClass)
    End Sub

    Public Sub CollLetterUpdate(ByVal oCustomClass As Parameter.CollLetter) Implements [Interface].ICollLetter.CollLetterUpdate
        Dim DACollLetter As New SQLEngine.ARMgt.CollLetter
        DACollLetter.CollLetterUpdate(oCustomClass)
    End Sub

    Public Function CollLetterFieldPaging(ByVal oCustomClass As Parameter.CollLetter) As Parameter.CollLetter Implements [Interface].ICollLetter.CollLetterFieldPaging
        Dim DACollLetter As New SQLEngine.ARMgt.CollLetter
        Return DACollLetter.CollLetterFieldPaging(oCustomClass)
    End Function
End Class
