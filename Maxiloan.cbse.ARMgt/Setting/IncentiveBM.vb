
#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ARMgt

#End Region


Public Class IncentiveBM : Inherits ComponentBase
    Implements IIncentiveBM


    Public Function IncentiveBMPaging(ByVal oCustomClass As Parameter.IncentiveBM) As Parameter.IncentiveBM Implements [Interface].IIncentiveBM.IncentiveBMPaging
        Dim DAIncentiveBM As New SQLEngine.ARMgt.IncentiveBM
        Return DAIncentiveBM.IncentiveBMPaging(oCustomClass)
    End Function

    Public Sub IncentiveBMSave(ByVal oCustomClass As Parameter.IncentiveBM) Implements [Interface].IIncentiveBM.IncentiveBMSave
        Dim DAIncentiveBM As New SQLEngine.ARMgt.IncentiveBM
        DAIncentiveBM.IncentiveBMSave(oCustomClass)
    End Sub

    Public Sub IncentiveBmUpdate(ByVal oCustomClass As Parameter.IncentiveBM) Implements [Interface].IIncentiveBM.IncentiveBmUpdate
        Dim DAincentivebm As New SQLEngine.ARMgt.IncentiveBM
        DAincentivebm.IncentiveBmUpdate(oCustomClass)
    End Sub
    Public Function IncentiveBMComboList(ByVal oCustomClass As Parameter.IncentiveBM) As DataTable Implements [Interface].IIncentiveBM.IncentiveBMComboList
        Dim DAIncentiveBM As New SQLEngine.ARMgt.IncentiveBM
        Return DAIncentiveBM.IncentiveBMComboList(oCustomClass)
    End Function
    Public Function IncentiveBMReports(ByVal oCustomClass As Parameter.IncentiveBM) As Parameter.IncentiveBM Implements [Interface].IIncentiveBM.IncentiveBMreports
        Dim DAIncentiveBM As New SQLEngine.ARMgt.IncentiveBM
        Return DAIncentiveBM.IncentiveBMReports(oCustomClass)

    End Function

End Class
