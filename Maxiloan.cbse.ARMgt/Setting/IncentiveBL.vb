
#Region "Imports"

Imports Maxiloan.Interface
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ARMgt

#End Region

Public Class IncentiveBL : Inherits ComponentBase
    Implements IIncentiveBL

    Public Sub IncentiveBLDelete(ByVal oCustomClass As Parameter.IncentiveBL) Implements [Interface].IIncentiveBL.IncentiveBLDelete
        Dim DAIncentive As New SQLEngine.ARMgt.IncentiveBL
        DAIncentive.IncentiveBLDelete(oCustomClass)
    End Sub

    Public Function IncentiveBLPaging(ByVal oCustomClass As Parameter.IncentiveBL) As Parameter.IncentiveBL Implements [Interface].IIncentiveBL.IncentiveBLPaging
        Dim DAIncentive As New SQLEngine.ARMgt.IncentiveBL
        Return DAIncentive.IncentiveBLPaging(oCustomClass)
    End Function

    Public Function IncentiveBLReports(ByVal oCustomClass As Parameter.IncentiveBL) As Parameter.IncentiveBL Implements [Interface].IIncentiveBL.IncentiveBLReports
        Dim DAIncentive As New SQLEngine.ARMgt.IncentiveBL
        Return DAIncentive.IncentiveBLReports(oCustomClass)
    End Function

    Public Sub IncentiveBLSave(ByVal oCustomClass As Parameter.IncentiveBL) Implements [Interface].IIncentiveBL.IncentiveBLSave
        Dim DAIncentive As New SQLEngine.ARMgt.IncentiveBL
        DAIncentive.IncentiveBLSave(oCustomClass)
    End Sub

    Public Sub IncentiveBLUpdate(ByVal oCustomClass As Parameter.IncentiveBL) Implements [Interface].IIncentiveBL.IncentiveBLUpdate
        Dim DAIncentive As New SQLEngine.ARMgt.IncentiveBL
        DAIncentive.IncentiveBLUpdate(oCustomClass)
    End Sub
End Class
