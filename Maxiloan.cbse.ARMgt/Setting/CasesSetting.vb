

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class CasesSetting : Inherits ComponentBase
    Implements ICasesSetting

#Region "Case Maintenance"

    Public Function ListCasesSetting(ByVal customclass As Parameter.CasesSetting) As Parameter.CasesSetting Implements [Interface].ICasesSetting.ListCasesSetting
        Dim DAListCasesSetting As New SQLEngine.ARMgt.CasesSetting
        Return DAListCasesSetting.ListCasesSetting(customclass)
    End Function

    Public Function AddCasesSetting(ByVal customclass As Parameter.CasesSetting) As String Implements [Interface].ICasesSetting.AddCasesSetting
        Dim DAAddCasesSetting As New SQLEngine.ARMgt.CasesSetting
        Return DAAddCasesSetting.AddCasesSetting(customclass)
    End Function

    Public Function DeleteCasesSetting(ByVal customclass As Parameter.CasesSetting) As String Implements [Interface].ICasesSetting.DeleteCasesSetting
        Dim DADeleteCasesSetting As New SQLEngine.ARMgt.CasesSetting
        Return DADeleteCasesSetting.DeleteCasesSetting(customclass)
    End Function

    Public Function EditCasesSetting(ByVal customclass As Parameter.CasesSetting) As String Implements [Interface].ICasesSetting.EditCasesSetting
        Dim DAEditCasesSetting As New SQLEngine.ARMgt.CasesSetting
        Return DAEditCasesSetting.EditCasesSetting(customclass)
    End Function

    Public Function ReportCasesSetting(ByVal customclass As Parameter.CasesSetting) As Parameter.CasesSetting Implements [Interface].ICasesSetting.ReportCasesSetting
        Dim DAPrintCasesSetting As New SQLEngine.ARMgt.CasesSetting
        Return DAPrintCasesSetting.ReportCasesSetting(customclass)
    End Function
    Public Function ListCases(ByVal customclass As Parameter.CasesSetting) As Parameter.CasesSetting Implements [Interface].ICasesSetting.ListCases
        Dim DAPrintCasesSetting As New SQLEngine.ARMgt.CasesSetting
        Return DAPrintCasesSetting.ReportCasesSetting(customclass)
    End Function

#End Region


End Class
