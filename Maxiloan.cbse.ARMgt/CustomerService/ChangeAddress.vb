
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class ChangeAddress : Inherits ComponentBase
    Implements IChangeAddress

    Public Function AddressListCG(ByVal customclass As Parameter.ChangeAddress) As Parameter.ChangeAddress Implements [Interface].IChangeAddress.AddressListCG
        Dim DAAddress As New SQLEngine.ARMgt.ChangeAddress
        Return DAAddress.AddressListCG(customclass)
    End Function

    Public Function ChangeAddressList(ByVal customclass As Parameter.ChangeAddress) As Parameter.ChangeAddress Implements [Interface].IChangeAddress.ChangeAddressList
        Dim DAAddress As New SQLEngine.ARMgt.ChangeAddress
        Return DAAddress.ListAddress(customclass)
    End Function

    Public Function ChangeAddressView(ByVal customclass As Parameter.ChangeAddress) As Parameter.ChangeAddress Implements [Interface].IChangeAddress.ChangeAddressView
        Dim DAAddress As New SQLEngine.ARMgt.ChangeAddress
        Return DAAddress.ViewAddress(customclass)
    End Function

    Public Function ChangeAddressSave(ByVal customclass As Parameter.ChangeAddress) As Parameter.ChangeAddress Implements [Interface].IChangeAddress.ChangeAddressSave
        Dim DAAddress As New SQLEngine.ARMgt.ChangeAddress
        Return DAAddress.SaveAddress(customclass)
    End Function

    Public Function GetAddress(ByVal customclass As Parameter.ChangeAddress) As Parameter.ChangeAddress Implements [Interface].IChangeAddress.GetAddress
        Dim DAAddress As New SQLEngine.ARMgt.ChangeAddress
        Return DAAddress.GetAddress(customclass)
    End Function
End Class
