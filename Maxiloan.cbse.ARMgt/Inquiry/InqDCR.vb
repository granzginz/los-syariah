
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class InqDCR : Inherits ComponentBase
    Implements IDCR

    Public Function GetCG(ByVal strconn As String) As System.Data.DataTable Implements [Interface].IDCR.GetCG
        Dim DADCR As New SQLEngine.ARMgt.InqDCR
        Return DADCR.GetComboCG(strconn)
    End Function

    Public Function GetInqDCRList(ByVal oDCR As Parameter.InqDCR) As Parameter.InqDCR Implements [Interface].IDCR.GetInqDCRList
        Dim DADCR As New SQLEngine.ARMgt.InqDCR
        Return DADCR.GetInqDCRList(oDCR)
    End Function

    Public Function GetTaskType(ByVal strconn As String) As System.Data.DataTable Implements [Interface].IDCR.GetTaskType
        Dim DADCR As New SQLEngine.ARMgt.InqDCR
        Return DADCR.GetComboTaskType(strconn)
    End Function

    Public Function ListReport(ByVal oDCR As Parameter.InqDCR) As System.Data.DataTable Implements [Interface].IDCR.ListReport
        Dim DADCR As New SQLEngine.ARMgt.InqDCR
        Return DADCR.ListReport(oDCR)
    End Function
End Class
