

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class CollActivity : Inherits ComponentBase
    Implements ICollActivity

    Public Function GetCollActivityDetail(ByVal oCollAct As Parameter.CollActivity) As Parameter.CollActivity Implements [Interface].ICollActivity.GetCollActivityDetail
        Dim DACollAct As New SQLEngine.ARMgt.CollActivity
        Return DACollAct.GetCollActivityDetail(oCollAct)
    End Function

    Public Function GetCollActivityList(ByVal oCollAct As Parameter.CollActivity) As Parameter.CollActivity Implements [Interface].ICollActivity.GetCollActivityList
        Dim DACollAct As New SQLEngine.ARMgt.CollActivity
        Return DACollAct.GetCollActivityList(oCollAct)
    End Function

    Public Function GetCollActivityResultList(ByVal oCollAct As Parameter.CollActivity) As Parameter.CollActivity Implements [Interface].ICollActivity.GetCollActivityResultList
        Dim DACollAct As New SQLEngine.ARMgt.CollActivity
        Return DACollAct.GetCollActivityResultList(oCollAct)
    End Function

    Public Function GetCGCombo(ByVal strconn As String) As System.Data.DataTable Implements [Interface].ICollActivity.GetCGCombo
        Dim DACollAct As New SQLEngine.ARMgt.CollActivity
        Return DACollAct.GetComboCG(strconn)
    End Function

    Public Function GetCollectorCombo(ByVal oCollAct As Parameter.CollActivity) As System.Data.DataTable Implements [Interface].ICollActivity.GetCollectorCombo
        Dim DACollAct As New SQLEngine.ARMgt.CollActivity
        Return DACollAct.GetCollectorCombo(oCollAct)
    End Function

    Public Function GetCollActivityHistory(ByVal oCollAct As Parameter.CollActivity) As Parameter.CollActivity Implements [Interface].ICollActivity.GetCollActivityHistory
        Dim DACollAct As New SQLEngine.ARMgt.CollActivity
        Return DACollAct.GetCollActivityHistory(oCollAct)
    End Function

    Public Function GetCollActivityResultListPrint(ByVal oCollAct As Parameter.CollActivity) As DataTable Implements [Interface].ICollActivity.GetCollActivityResultListPrint
        Dim DACollAct As New SQLEngine.ARMgt.CollActivity
        Return DACollAct.GetCollActivityResultListPrint(oCollAct)
    End Function

End Class
