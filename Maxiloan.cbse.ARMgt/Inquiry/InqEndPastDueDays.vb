﻿
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ARMgt
Imports Maxiloan.Interface
Imports Maxiloan.Parameter

Public Class InqEndPastDueDays : Inherits ComponentBase
    Implements IInqEndPastDueDays

    Private Function InqEndPastDueDays(ByVal oInqEndPastDueDays As Parameter.InqEndPastDueDays) As Parameter.InqEndPastDueDays Implements [Interface].IInqEndPastDueDays.InqEndPastDueDays
        Try
            Dim DA As New SQLEngine.ARMgt.InqEndPastDueDays
            Return DA.InqEndPastDueDays(oInqEndPastDueDays)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
