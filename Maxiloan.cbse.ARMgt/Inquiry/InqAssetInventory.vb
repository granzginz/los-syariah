

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class InqAssetInventory : Inherits ComponentBase
    Implements IInqAssetInventory

    Public Function InqAssetInventoryList(ByVal customclass As Parameter.AssetInventory) As Parameter.AssetInventory Implements [Interface].IInqAssetInventory.InqAssetInventoryList
        Dim DA As New SQLEngine.ARMgt.InqAssetInventory
        Return DA.InqAssetInvList(customclass)
    End Function

    Public Function InqAssetInventoryDetail(ByVal customclass As Parameter.AssetInventory) As Parameter.AssetInventory Implements [Interface].IInqAssetInventory.InqAssetInventoryDetail
        Dim DA As New SQLEngine.ARMgt.InqAssetInventory
        Return DA.InqAssetInvDetail(customclass)
    End Function
End Class
