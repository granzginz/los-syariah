
#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ARMgt
Imports Maxiloan.Interface

#End Region
Public Class DeskCollPerformance : Inherits ComponentBase
    Implements IDeskCollPerformance


    Public Function DeskCollPerformancePaging(ByVal oCustomClass As Parameter.DeskCollPerformance) As Parameter.DeskCollPerformance Implements [Interface].IDeskCollPerformance.DeskCollPerformancePaging
        Dim DADeskCollPerformance As New SQLEngine.ARMgt.DeskCollPerformance
        Return DADeskCollPerformance.DeskCollPerformancePaging(oCustomClass)

    End Function

    Public Function GetBeginStatus(ByVal strConnection As String) As System.Data.DataTable Implements [Interface].IDeskCollPerformance.GetBeginStatus
        Dim DADeskCollPerformance As New SQLEngine.ARMgt.DeskCollPerformance
        Return DADeskCollPerformance.GetBeginStatus(strConnection)

    End Function

    Public Function GetComboCG(ByVal strConnection As String) As System.Data.DataTable Implements [Interface].IDeskCollPerformance.GetComboCG
        Dim DADeskCollPerformance As New SQLEngine.ARMgt.DeskCollPerformance
        Return DADeskCollPerformance.GetComboCG(strConnection)
    End Function

    Public Function GetPeriod(ByVal strConnection As String) As System.Data.DataTable Implements [Interface].IDeskCollPerformance.GetPeriod
        Dim DADeskCollPerformance As New SQLEngine.ARMgt.DeskCollPerformance
        Return DADeskCollPerformance.GetPeriod(strConnection)
    End Function

    Public Function GetEndStatus(ByVal strConnection As String) As System.Data.DataTable Implements [Interface].IDeskCollPerformance.GetEndStatus
        Dim DADeskCollPerformance As New SQLEngine.ARMgt.DeskCollPerformance
        Return DADeskCollPerformance.GetEndStatusStatus(strConnection)
    End Function
End Class
