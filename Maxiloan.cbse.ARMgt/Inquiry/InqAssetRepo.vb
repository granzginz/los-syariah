

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class InqAssetRepo : Inherits ComponentBase
    Implements IInqAssetRepo

    Public Function InqAssetRepoList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo Implements [Interface].IInqAssetRepo.InqAssetRepoList
        Dim DAAssetRepoList As New SQLEngine.ARMgt.InqInqAssetRepo
        Return DAAssetRepoList.InqAssetRepoList(customclass)
    End Function

    Public Function InqAssetRepoDetail(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo Implements [Interface].IInqAssetRepo.InqAssetRepoDetail
        Dim DAAssetRepoList As New SQLEngine.ARMgt.InqInqAssetRepo
        Return DAAssetRepoList.InqAssetRepoDetail(customclass)
    End Function

End Class
