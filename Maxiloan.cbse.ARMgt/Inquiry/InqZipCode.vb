
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ARMgt
Imports Maxiloan.Interface

Public Class InqZipCode : Inherits ComponentBase
    Implements IInqZipCode
    Public Function InqZipCode(ByVal oCollZipCode As Parameter.CollZipCode) As Parameter.CollZipCode Implements [Interface].IInqZipCode.InqZipCode
        Try
            Dim DA As New SQLEngine.ARMgt.InqZipCode
            Return DA.InqZipCode(oCollZipCode)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

    Public Function GetCityCombo(ByVal strconn As String) As System.Data.DataTable Implements [Interface].IInqZipCode.GetCityCombo
        Dim DA As New SQLEngine.ARMgt.InqZipCode
        Return DA.GetCityCombo(strconn)
    End Function
End Class
