#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class ViewCollection : Inherits ComponentBase
    Implements IViewCollection

    Public Function ViewCollection(ByVal ocustomclass As Parameter.ViewCollection) As Parameter.ViewCollection Implements [Interface].IViewCollection.ViewCollection
        Dim DA As New SQLEngine.ARMgt.ViewCollection
        Return DA.ViewCollection(ocustomclass)

    End Function
    Public Function ViewKewajibanNasabah(ByVal ocustomclass As Parameter.ViewCollection) As Parameter.ViewCollection Implements [Interface].IViewCollection.ViewKewajibanNasabah
        Dim DA As New SQLEngine.ARMgt.ViewCollection
        Return DA.ViewKewajibanNasabah(ocustomclass)

    End Function
End Class
