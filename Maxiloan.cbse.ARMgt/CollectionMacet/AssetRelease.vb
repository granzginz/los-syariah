

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class AssetRelease : Inherits ComponentBase
    Implements IAssetRelease

    Public Function AssetReleaseCheckList(ByVal customclass As Parameter.AssetRelease) As Parameter.AssetRelease Implements [Interface].IAssetRelease.AssetReleaseCheckList
        Dim DA As New SQLEngine.ARMgt.AssetRelease
        Return DA.AssetReleaseCheckList(customclass)
    End Function

    Public Function AssetReleaseList(ByVal customclass As Parameter.AssetRelease) As Parameter.AssetRelease Implements [Interface].IAssetRelease.AssetReleaseList
        Dim DA As New SQLEngine.ARMgt.AssetRelease
        Return DA.AssetReleaseList(customclass)
    End Function

    Public Function AssetReleaseListDetail(ByVal customclass As Parameter.AssetRelease) As Parameter.AssetRelease Implements [Interface].IAssetRelease.AssetReleaseListDetail
        Dim DA As New SQLEngine.ARMgt.AssetRelease
        Return DA.AssetReleaseDetail(customclass)
    End Function


    Public Function AssetReleaseCheckListSave(ByVal customclass As Parameter.AssetRelease) As Boolean Implements [Interface].IAssetRelease.AssetReleaseCheckListSave
        Dim DA As New SQLEngine.ARMgt.AssetRelease
        Return DA.AssetReleaseCheckListSave(customclass)
    End Function
End Class
