#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class CollInvSelling
    Implements ICollInvSelling


    Public Function InvSellingList(ByVal customclass As Parameter.CollInvSelling) As Parameter.CollInvSelling Implements [Interface].ICollInvSelling.InvSellingList
        Dim DA As New SQLEngine.ARMgt.CollInvSelling
        Return DA.InvSellingList(customclass)
    End Function

    Public Function InvSellingDetail(ByVal customclass As Parameter.CollInvSelling) As Parameter.CollInvSelling Implements [Interface].ICollInvSelling.InvSellingDetail
        Dim DA As New SQLEngine.ARMgt.CollInvSelling
        Return DA.InvSellingDetail(customclass)
    End Function

    Public Function InvSellingSave(ByVal customclass As Parameter.CollInvSelling) As Boolean Implements [Interface].ICollInvSelling.InvSellingSave
        Dim DA As New SQLEngine.ARMgt.CollInvSelling
        Return DA.InvSellingSave(customclass)
    End Function
    Public Function ViewAppraisalbidder(ByVal customclass As Parameter.CollInvSelling) As Parameter.CollInvSelling Implements [Interface].ICollInvSelling.ViewAppraisalbidder
        Dim DA As New SQLEngine.ARMgt.CollInvSelling
        Return DA.ViewAppraisalbidder(customclass)

    End Function
End Class
