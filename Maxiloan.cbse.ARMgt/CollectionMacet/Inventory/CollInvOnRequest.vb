

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class CollInvOnRequest
    Implements ICollInvOnRequest



    Public Function CollInvOnRequest(ByVal customclass As Parameter.CollInvSelling) As Parameter.CollInvSelling Implements [Interface].ICollInvOnRequest.CollInvOnRequest
        Dim DA As New SQLEngine.ARMgt.CollInvOnRequest
        Return DA.collInvOnRequest(customclass)
    End Function

    Public Sub InvOnRequestProses(ByVal customclass As Parameter.CollInvSelling) Implements [Interface].ICollInvOnRequest.InvOnRequestProses
        Dim DA As New SQLEngine.ARMgt.CollInvOnRequest
        DA.InvOnRequestProses(customclass)
    End Sub
End Class
