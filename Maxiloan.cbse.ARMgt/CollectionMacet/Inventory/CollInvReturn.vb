#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class CollInvReturn
    Implements ICollInvReturn

    Public Function InvReturnDetail(customclass As Parameter.CollInvSelling) As Parameter.CollInvSelling Implements [Interface].ICollInvReturn.InvReturnDetail
        Dim DA As New SQLEngine.ARMgt.CollInvReturn
        Return DA.InvReturnDetail(customclass)
    End Function

    Public Function InvReturnList(customclass As Parameter.CollInvSelling) As Parameter.CollInvSelling Implements [Interface].ICollInvReturn.InvReturnList
        Dim DA As New SQLEngine.ARMgt.CollInvReturn
        Return DA.InvReturnList(customclass)
    End Function

    Public Function InvReturnSave(customclass As Parameter.CollInvSelling) As Boolean Implements [Interface].ICollInvReturn.InvReturnSave
        Dim DA As New SQLEngine.ARMgt.CollInvReturn
        Return DA.InvReturnSave(customclass)
    End Function
End Class
