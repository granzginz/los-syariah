
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class RALChangeExecutor : Inherits ComponentBase
    Implements IRALChangeExec

    Public Function RALListCG(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec Implements [Interface].IRALChangeExec.RALListCG
        Dim DARALChangeExec As New SQLEngine.ARMgt.RALChangeExec
        Return DARALChangeExec.RALListCG(customclass)
    End Function

    Public Function RALChangeExecList(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec Implements [Interface].IRALChangeExec.RALChangeExecList
        Dim DARALChangeExec As New SQLEngine.ARMgt.RALChangeExec
        Return DARALChangeExec.RALChangeExecList(customclass)
    End Function

    Public Function RALViewDataCollector(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec Implements [Interface].IRALChangeExec.RALViewDataCollector
        Dim DARALChangeExec As New SQLEngine.ARMgt.RALChangeExec
        Return DARALChangeExec.RALViewDataCollector(customclass)
    End Function

    Public Function RALSaveDataExecutor(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec Implements [Interface].IRALChangeExec.RALSaveDataExecutor
        Dim DARALChangeExec As New SQLEngine.ARMgt.RALChangeExec
        Return DARALChangeExec.RALSaveDataExecutor(customclass)
    End Function


    Public Function RALSaveDataPrintRAL(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec Implements [Interface].IRALChangeExec.RALSaveDataPrintRAL
        Dim DARALChangeExec As New SQLEngine.ARMgt.RALChangeExec
        Return DARALChangeExec.RALSaveDataPrintRAL(customclass)
    End Function

    Public Function RALDataChangeExec(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec Implements [Interface].IRALChangeExec.RALDataChangeExec
        Dim DARALChangeExec As New SQLEngine.ARMgt.RALChangeExec
        Return DARALChangeExec.RALDataChangeExec(customclass)
    End Function
    Public Function RequestDataSelect(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec Implements [Interface].IRALChangeExec.RequestDataSelect
        Dim DARALChangeExec As New SQLEngine.ARMgt.RALChangeExec
        Return DARALChangeExec.RequestDataSelect(customclass)
    End Function
End Class
