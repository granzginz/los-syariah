
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class RALRelease : Inherits ComponentBase
    Implements IRALRelease

    Public Function RALListCG(ByVal customclass As Parameter.RALRelease) As Parameter.RALRelease Implements [Interface].IRALRelease.RALListCG
        Dim DARALRelease As New SQLEngine.ARMgt.RALRelease
        Return DARALRelease.RALListCG(customclass)
    End Function

    Public Function RALReleaseList(ByVal customclass As Parameter.RALRelease) As Parameter.RALRelease Implements [Interface].IRALRelease.RALReleaseList
        Dim DARALRelease As New SQLEngine.ARMgt.RALRelease
        Return DARALRelease.RALReleaseList(customclass)
    End Function

    Public Function RALDataExtend(ByVal customclass As Parameter.RALRelease) As Parameter.RALRelease Implements [Interface].IRALRelease.RALDataExtend
        Dim DARALRelease As New SQLEngine.ARMgt.RALRelease
        Return DARALRelease.RALDataExtend(customclass)
    End Function

    Public Function RALReleaseView(ByVal customclass As Parameter.RALRelease) As Parameter.RALRelease Implements [Interface].IRALRelease.RALReleaseView
        Dim DARALRelease As New SQLEngine.ARMgt.RALRelease
        Return DARALRelease.RALReleaseView(customclass)
    End Function

    Public Function RALReleaseSave(ByVal customclass As Parameter.RALRelease) As Parameter.RALRelease Implements [Interface].IRALRelease.RALReleaseSave
        Dim DARALRelease As New SQLEngine.ARMgt.RALRelease
        Return DARALRelease.RALReleaseSave(customclass)
    End Function

    Public Function CekPrepayment(ByVal customclass As Parameter.RALRelease) As Boolean Implements [Interface].IRALRelease.CekPrepayment
        Dim DARALRelease As New SQLEngine.ARMgt.RALRelease
        Return DARALRelease.CekPrepayment(customclass)
    End Function
End Class
