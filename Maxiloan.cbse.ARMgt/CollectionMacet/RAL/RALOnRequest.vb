

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class RALOnRequest : Inherits ComponentBase
    Implements IRALOnRequest

    Public Function RALListCG(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALOnRequest.RALListCG
        Dim DARALPrinting As New SQLEngine.ARMgt.RALOnRequest
        Return DARALPrinting.RALListCG(customclass)
    End Function

    Public Function RALPrintingList(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALOnRequest.RALPrintingList
        Dim DARALPrinting As New SQLEngine.ARMgt.RALOnRequest
        Return DARALPrinting.RALListPrinting(customclass)
    End Function

    Public Function RALViewDataCollector(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALOnRequest.RALViewDataCollector
        Dim DARALPrinting As New SQLEngine.ARMgt.RALOnRequest
        Return DARALPrinting.RALViewDataCollector(customclass)
    End Function

    Public Function RALSaveDataExecutor(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALOnRequest.RALSaveDataExecutor
        Dim DARALPrinting As New SQLEngine.ARMgt.RALOnRequest
        Return DARALPrinting.RALSaveDataExecutor(customclass)
    End Function

    Public Function RALViewHistoryExecutor(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALOnRequest.RALViewHistoryExecutor
        Dim DARALPrinting As New SQLEngine.ARMgt.RALOnRequest
        Return DARALPrinting.RALViewHistoryExecutor(customclass)
    End Function

    Public Function RALSaveDataPrintRAL(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALOnRequest.RALSaveDataPrintRAL
        Dim DARALPrinting As New SQLEngine.ARMgt.RALOnRequest
        Return DARALPrinting.RALSaveDataPrintRAL(customclass)
    End Function

    Public Function RALListReport(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALOnRequest.RALListReport
        Dim DARALPrinting As New SQLEngine.ARMgt.RALOnRequest
        Return DARALPrinting.RALListReport(customclass)
    End Function

    Public Function RALListReportCheckList(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALOnRequest.RALListReportCheckList
        Dim DARALPrinting As New SQLEngine.ARMgt.RALOnRequest
        Return DARALPrinting.RALListReportCheckList(customclass)
    End Function

    Public Function RequestDataSelect(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALOnRequest.RequestDataSelect
        Dim DARALPrinting As New SQLEngine.ARMgt.RALOnRequest
        Return DARALPrinting.RALViewDataSelectRequest(customclass)
    End Function
End Class
