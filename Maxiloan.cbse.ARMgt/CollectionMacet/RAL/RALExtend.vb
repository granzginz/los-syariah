
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class RALExtend : Inherits ComponentBase
    Implements IRALExtend

    Public Function RALListCG(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend Implements [Interface].IRALExtend.RALListCG
        Dim DARALExtend As New SQLEngine.ARMgt.RALExtend
        Return DARALExtend.RALListCG(customclass)
    End Function

    Public Function RALExtendList(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend Implements [Interface].IRALExtend.RALExtendList
        Dim DARALExtend As New SQLEngine.ARMgt.RALExtend
        Return DARALExtend.RALExtendList(customclass)
    End Function

    Public Function RALExtendView(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend Implements [Interface].IRALExtend.RALExtendView
        Dim DARALExtend As New SQLEngine.ARMgt.RALExtend
        Return DARALExtend.RALExtendView(customclass)
    End Function

    Public Function RALExtendSave(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend Implements [Interface].IRALExtend.RALExtendSave
        Dim DARALExtend As New SQLEngine.ARMgt.RALExtend
        Return DARALExtend.RALExtendSave(customclass)
    End Function

    Public Function RALDataExtend(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend Implements [Interface].IRALExtend.RALDataExtend
        Dim DARALExtend As New SQLEngine.ARMgt.RALExtend
        Return DARALExtend.RALDataExtend(customclass)
    End Function

    Public Function RALSaveDataPrint(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend Implements [Interface].IRALExtend.RALSaveDataPrint
        Dim DARALExtend As New SQLEngine.ARMgt.RALExtend
        Return DARALExtend.RALSaveDataPrint(customclass)
    End Function
End Class
