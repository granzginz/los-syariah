

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class RALPrinting : Inherits ComponentBase
    Implements IRALPrinting

    Public Function RALListCG(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALPrinting.RALListCG
        Dim DARALPrinting As New SQLEngine.ARMgt.RALPrinting
        Return DARALPrinting.RALListCG(customclass)
    End Function

    Public Function RALPrintingList(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALPrinting.RALPrintingList
        Dim DARALPrinting As New SQLEngine.ARMgt.RALPrinting
        Return DARALPrinting.RALListPrinting(customclass)
    End Function

    Public Function RALViewDataSelect(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALPrinting.RALViewDataSelect
        Dim DARALPrinting As New SQLEngine.ARMgt.RALPrinting
        Return DARALPrinting.RALViewDataSelect(customclass)
    End Function

    Public Function RALViewDataCollector(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALPrinting.RALViewDataCollector
        Dim DARALPrinting As New SQLEngine.ARMgt.RALPrinting
        Return DARALPrinting.RALViewDataCollector(customclass)
    End Function

    Public Function RALSaveDataExecutor(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALPrinting.RALSaveDataExecutor
        Dim DARALPrinting As New SQLEngine.ARMgt.RALPrinting
        Return DARALPrinting.RALSaveDataExecutor(customclass)
    End Function

    Public Function RALViewHistoryExecutor(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALPrinting.RALViewHistoryExecutor
        Dim DARALPrinting As New SQLEngine.ARMgt.RALPrinting
        Return DARALPrinting.RALViewHistoryExecutor(customclass)
    End Function

    Public Function RALSaveDataPrintRAL(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALPrinting.RALSaveDataPrintRAL
        Dim DARALPrinting As New SQLEngine.ARMgt.RALPrinting
        Return DARALPrinting.RALSaveDataPrintRAL(customclass)
    End Function

    Public Function RALListReport(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALPrinting.RALListReport
        Dim DARALPrinting As New SQLEngine.ARMgt.RALPrinting
        Return DARALPrinting.RALListReport(customclass)
    End Function

    Public Function RALListReportCheckList(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALPrinting.RALListReportCheckList
        Dim DARALPrinting As New SQLEngine.ARMgt.RALPrinting
        Return DARALPrinting.RALListReportCheckList(customclass)
    End Function

    Public Function RequestDataSelect(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting Implements [Interface].IRALPrinting.RequestDataSelect
        Dim DARALPrinting As New SQLEngine.ARMgt.RALPrinting
        Return DARALPrinting.RALViewDataSelectRequest(customclass)
    End Function
End Class
