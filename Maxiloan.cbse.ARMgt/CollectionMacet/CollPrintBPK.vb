#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class CollPrintBPK
    Implements ICollPrintBPK


    Public Function PrintBPKList(ByVal oCustomClass As Parameter.CollPrintBPK) As Parameter.CollPrintBPK Implements [Interface].ICollPrintBPK.PrintBPKList
        Dim DACollPrintBPK As New SQLEngine.ARMgt.CollPrintBPK
        Return DACollPrintBPK.PrintBPKList(oCustomClass)
    End Function

    Public Function PrintBPKReport(ByVal oCustomClass As Parameter.CollPrintBPK) As Parameter.CollPrintBPK Implements [Interface].ICollPrintBPK.PrintBPKReport
        Dim DACollPrintBPK As New SQLEngine.ARMgt.CollPrintBPK
        Return DACollPrintBPK.PrintBPKReport(oCustomClass)
    End Function
End Class
