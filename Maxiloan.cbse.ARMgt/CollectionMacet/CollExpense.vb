#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class CollExpense : Inherits ComponentBase
    Implements ICollExpense

    Public Function CollExpenseList(ByVal customclass As Parameter.CollExpense) As Parameter.CollExpense Implements [Interface].ICollExpense.CollExpenseList
        Dim DACollExpense As New SQLEngine.ARMgt.CollExpense
        Return DACollExpense.CollExpenseList(customclass)
    End Function
    Public Function CollExpenseReqList(ByVal customclass As Parameter.CollExpense) As Parameter.CollExpense Implements [Interface].ICollExpense.CollExpenseReqList
        Dim DACollExpense As New SQLEngine.ARMgt.CollExpense
        Return DACollExpense.CollExpenseReqList(customclass)
    End Function
    Public Sub CollExpenseSave(ByVal customclass As Parameter.CollExpense) Implements [Interface].ICollExpense.CollExpenseSave
        Dim DACollExpense As New SQLEngine.ARMgt.CollExpense
        DACollExpense.CollExpenseSave(customclass)
    End Sub
End Class
