

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class InventoryAppraisal : Inherits ComponentBase
    Implements IInventoryAppraisal

    Public Function InventoryAppraisalListCG(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal Implements [Interface].IInventoryAppraisal.InventoryAppraisalListCG
        Dim DAInventoryAppraisal As New SQLEngine.ARMgt.InventoryAppraisal
        Return DAInventoryAppraisal.InventoryAppraisalListCG(customclass)
    End Function

    Public Function InventoryAppraisalList(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal Implements [Interface].IInventoryAppraisal.InventoryAppraisalList
        Dim DAInventoryAppraisal As New SQLEngine.ARMgt.InventoryAppraisal
        Return DAInventoryAppraisal.InventoryAppraisalList(customclass)
    End Function

    Public Function DataInventoryAppraisal(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal Implements [Interface].IInventoryAppraisal.DataInventoryAppraisal
        Dim DAInventoryAppraisal As New SQLEngine.ARMgt.InventoryAppraisal
        Return DAInventoryAppraisal.DataInventoryAppraisal(customclass)
    End Function
    Public Function SaveInventoryAppraisal(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal Implements [Interface].IInventoryAppraisal.SaveInventoryAppraisal
        Dim DAInventoryAppraisal As New SQLEngine.ARMgt.InventoryAppraisal
        Return DAInventoryAppraisal.SaveInventoryAppraisal(customclass)
    End Function


    'Public Function GenerateKuitansiTagihOnRequest(ByVal customclass As Entities.ReceiptNotesOnRequest) As Entities.ReceiptNotesOnRequest Implements [Interface].IReceiptNotesOnRequest.GenerateKuitansiTagihOnRequest
    '    Dim DAReceiptNotesOnRequest As New DataAccess.Collection.ReceiptNotesOnRequest
    '    Return DAReceiptNotesOnRequest.GenerateKuitansiTagihOnRequest(customclass)
    'End Function


    Public Function RequestDataSelectInventory(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal Implements [Interface].IInventoryAppraisal.RequestDataSelectInventory
        Dim DAInventoryAppraisal As New SQLEngine.ARMgt.InventoryAppraisal
        Return DAInventoryAppraisal.RequestDataSelectInventory(customclass)
    End Function

    Public Function ViewAppraisal(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal Implements [Interface].IInventoryAppraisal.ViewAppraisal
        Dim DAInventoryAppraisal As New SQLEngine.ARMgt.InventoryAppraisal
        Return DAInventoryAppraisal.ViewAppraisal(customclass)

    End Function

    Public Function GetAccruedInterest(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal Implements [Interface].IInventoryAppraisal.GetAccruedInterest
        Dim DAInventoryAppraisal As New SQLEngine.ARMgt.InventoryAppraisal
        Return DAInventoryAppraisal.GetAccruedInterest(customclass)

    End Function
    Public Function ViewAppraisalbidder(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal Implements [Interface].IInventoryAppraisal.ViewAppraisalbidder
        Dim DAInventoryAppraisal As New SQLEngine.ARMgt.InventoryAppraisal
        Return DAInventoryAppraisal.ViewAppraisalbidder(customclass)

    End Function
    Public Sub GetMRP(ByRef p_Class As Parameter.InventoryAppraisal) Implements [Interface].IInventoryAppraisal.GetMRP
        Dim DAInventoryAppraisal As New SQLEngine.ARMgt.InventoryAppraisal
        DAInventoryAppraisal.GetMRP(p_Class)
    End Sub
    Public Sub PrintFormPersetujuanAppraisal(p_Class As Parameter.InventoryAppraisal) Implements [Interface].IInventoryAppraisal.PrintFormPersetujuanAppraisal
        Dim DAInventoryAppraisal As New SQLEngine.ARMgt.InventoryAppraisal
        DAInventoryAppraisal.PrintFormPersetujuanAppraisal(p_Class)
    End Sub
    Public Sub PrintFormPersetujuanAppraisalSubBid(p_Class As Parameter.InventoryAppraisal) Implements [Interface].IInventoryAppraisal.PrintFormPersetujuanAppraisalSubBid
        Dim DAInventoryAppraisal As New SQLEngine.ARMgt.InventoryAppraisal
        DAInventoryAppraisal.PrintFormPersetujuanAppraisalSubBid(p_Class)
    End Sub
    Public Sub PrintFormPersetujuanList(p_Class As Parameter.InventoryAppraisal) Implements [Interface].IInventoryAppraisal.PrintFormPersetujuanList
        Dim DAInventoryAppraisal As New SQLEngine.ARMgt.InventoryAppraisal
        DAInventoryAppraisal.PrintFormPersetujuanList(p_Class)
    End Sub
End Class
