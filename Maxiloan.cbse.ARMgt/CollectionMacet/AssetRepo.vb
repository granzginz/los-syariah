

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class AssetRepo : Inherits ComponentBase
    Implements IAssetRepo

    Public Function AssetRepoCheckList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo Implements [Interface].IAssetRepo.AssetRepoCheckList
        Dim DAAssetRepoCheckList As New SQLEngine.ARMgt.AssetRepo
        Return DAAssetRepoCheckList.AssetRepoCheckList(customclass)
    End Function
    Public Function AssetRepoApprovalList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo Implements [Interface].IAssetRepo.AssetRepoApprovalList
        Dim DAAssetRepoList As New SQLEngine.ARMgt.AssetRepo
        Return DAAssetRepoList.AssetRepoApprovalList(customclass)
    End Function
    Public Function AssetRepoList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo Implements [Interface].IAssetRepo.AssetRepoList
        Dim DAAssetRepoList As New SQLEngine.ARMgt.AssetRepo
        Return DAAssetRepoList.AssetRepoList(customclass)
    End Function

    Public Function AssetRepoListDetail(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo Implements [Interface].IAssetRepo.AssetRepoListDetail
        Dim DAAssetRepoList As New SQLEngine.ARMgt.AssetRepo
        Return DAAssetRepoList.AssetRepoDetail(customclass)
    End Function

    Public Sub AssetRepoSave(ByVal customclass As Parameter.AssetRepo) Implements [Interface].IAssetRepo.AssetRepoSave
        Dim DAAssetRepo As New SQLEngine.ARMgt.AssetRepo
        DAAssetRepo.AssetRepoSave(customclass)
    End Sub

    Public Sub AssetRepoApproveReject(ByVal customclass As Parameter.AssetRepo, status As String) Implements [Interface].IAssetRepo.AssetRepoApproveReject
        Dim DAAssetRepo As New SQLEngine.ARMgt.AssetRepo
        DAAssetRepo.AssetRepoApproveReject(customclass, status)
    End Sub

    Public Sub AssetRepoCheckListSave(ByVal customclass As Parameter.AssetRepo) Implements [Interface].IAssetRepo.AssetRepoCheckListSave
        Dim DAAssetRepo As New SQLEngine.ARMgt.AssetRepo
        DAAssetRepo.AssetRepoCheckListSave(customclass)
    End Sub
    Public Function AssetRepoCLPrintList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo Implements [Interface].IAssetRepo.AssetRepoCLPrintList
        Dim DAAssetRepoCheckList As New SQLEngine.ARMgt.AssetRepo
        Return DAAssetRepoCheckList.AssetRepoCLPrintList(customclass)
    End Function

    Public Function AssetRepoCLPrintReport(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo Implements [Interface].IAssetRepo.AssetRepoCLPrintReport
        Dim DAAssetRepoList As New SQLEngine.ARMgt.AssetRepo
        Return DAAssetRepoList.AssetRepoCLPrintReport(customclass)
    End Function
    Public Function AssetRepoPrepaymentLetterPrint(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo Implements [Interface].IAssetRepo.AssetRepoPrepaymentLetterPrint
        Dim DAAssetRepoPrepaymentLetter As New SQLEngine.ARMgt.AssetRepo
        Return DAAssetRepoPrepaymentLetter.AssetRepoPrepaymentLetterPrint(customclass)
    End Function

    Public Function AssetRepoChangeLocList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo Implements [Interface].IAssetRepo.AssetRepoChangeLocList
        Dim DAAssetRepoList As New SQLEngine.ARMgt.AssetRepo
        Return DAAssetRepoList.AssetRepoChangeLocList(customclass)
    End Function

    Public Sub AssetRepoChangeLocSave(ByVal customclass As Parameter.AssetRepo) Implements [Interface].IAssetRepo.AssetRepoChangeLocSave
        Dim DAAssetRepo As New SQLEngine.ARMgt.AssetRepo
        DAAssetRepo.AssetRepoChangeLocSave(customclass)
    End Sub

    Public Function AssetRepoChangeLocDetail(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo Implements [Interface].IAssetRepo.AssetRepoChangeLocDetail
        Dim DAAssetRepoList As New SQLEngine.ARMgt.AssetRepo
        Return DAAssetRepoList.AssetRepoChangeLocDetail(customclass)
    End Function

    Public Function AssetRepoChangeLocRecList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo Implements [Interface].IAssetRepo.AssetRepoChangeLocRecList
        Dim DAAssetRepoList As New SQLEngine.ARMgt.AssetRepo
        Return DAAssetRepoList.AssetRepoChangeLocRecList(customclass)
    End Function

    Public Sub AssetRepoChangeLocRecSave(ByVal customclass As Parameter.AssetRepo) Implements [Interface].IAssetRepo.AssetRepoChangeLocRecSave
        Dim DAAssetRepo As New SQLEngine.ARMgt.AssetRepo
        DAAssetRepo.AssetRepoChangeLocRecSave(customclass)
    End Sub
    Public Sub GetLokasiPenyimpanan(ByRef p_Class As Parameter.AssetRepo) Implements [Interface].IAssetRepo.GetLokasiPenyimpanan
        Dim DAAssetRepo As New SQLEngine.ARMgt.AssetRepo
        DAAssetRepo.GetLokasiPenyimpanan(p_Class)
    End Sub

    Public Function BiayaPenangananSave(customclass As Parameter.AssetRepo) As Parameter.AssetRepo Implements [Interface].IAssetRepo.BiayaPenangananSave
        Dim DAAssetRepoList As New SQLEngine.ARMgt.AssetRepo
        Return DAAssetRepoList.BiayaPenangananSave(customclass)
    End Function

    Public Function JenisBiayaBOP(cnn As String) As IList(Of Parameter.CommonValueText) Implements [Interface].IAssetRepo.JenisBiayaBOP
        Dim DAAssetRepoList As New SQLEngine.ARMgt.AssetRepo
        Return DAAssetRepoList.JenisBiayaBOP(cnn)
    End Function
    Public Function AssetGrade(cnn As String) As IList(Of Parameter.CommonValueText) Implements [Interface].IAssetRepo.AssetGrade
        Dim DAAssetRepoList As New SQLEngine.ARMgt.AssetRepo
        Return DAAssetRepoList.AssetGrade(cnn)
    End Function
    Public Function AssetGradeValue(cnn As String) As IList(Of Parameter.CommonValueText) Implements [Interface].IAssetRepo.AssetGradeValue
        Dim DAAssetRepoList As New SQLEngine.ARMgt.AssetRepo
        Return DAAssetRepoList.AssetGradeValue(cnn)
    End Function
End Class
