﻿#Region "Imports"
Imports Maxiloan.Interface
#End Region


Public Class InsBlokir : Inherits ComponentBase
    Implements IInsBlokir

    Dim oSQLE As New SQLEngine.ARMgt.InsBlokir

    Public Function GetInsBlokir(ByVal oCustomClass As Parameter.InsBlokir) As Parameter.InsBlokir Implements [Interface].IInsBlokir.GetInsBlokir
        Return oSQLE.GetInsBlokir(oCustomClass)
    End Function

    Public Sub ReleaseBlokirBayar(ByVal oCustomClass As Parameter.InsBlokir) Implements [Interface].IInsBlokir.ReleaseBlokirBayar
        oSQLE.ReleaseBlokirBayar(oCustomClass)
    End Sub
End Class
