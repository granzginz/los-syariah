

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class CCRequest : Inherits ComponentBase
    Implements ICCRequest

    Public Function CCRequestDetail(ByVal customclass As Parameter.CCRequest) As Parameter.CCRequest Implements [Interface].ICCRequest.CCRequestDetail
        Dim DA As New SQLEngine.ARMgt.CCRequest
        Return DA.CCRequestDetail(customclass)
    End Function

    Public Function CCRequestList(ByVal customclass As Parameter.CCRequest) As Parameter.CCRequest Implements [Interface].ICCRequest.CCRequestList
        Dim DA As New SQLEngine.ARMgt.CCRequest
        Return DA.CCRequestList(customclass)
    End Function

    Public Sub CCRequestSave(ByVal customclass As Parameter.CCRequest) Implements [Interface].ICCRequest.CCRequestSave
        Dim DA As New SQLEngine.ARMgt.CCRequest
        DA.CCRequestSave(customclass)
    End Sub

    Public Function GetComboReason(ByVal customclass As Parameter.CCRequest) As System.Data.DataTable Implements [Interface].ICCRequest.GetComboReason
        Dim DA As New SQLEngine.ARMgt.CCRequest
        Return DA.GetComboReason(customclass)
    End Function

    Public Function CCRequestView(ByVal customclass As Parameter.CCRequest) As Parameter.CCRequest Implements [Interface].ICCRequest.CCRequestView
        Dim DA As New SQLEngine.ARMgt.CCRequest
        Return DA.CCRequestView(customclass)
    End Function
    Public Function ContinueCreditReqReport(ByVal customclass As Parameter.CCRequest) As Parameter.CCRequest Implements [Interface].ICCRequest.ContinueCreditReqReport
        Dim DA As New SQLEngine.ARMgt.CCRequest
        Return DA.ContinueCreditReqReport(customclass)
    End Function

    Public Function CCRequestListInq(ByVal customclass As Parameter.CCRequest) As Parameter.CCRequest Implements [Interface].ICCRequest.CCRequestListInq
        Dim DA As New SQLEngine.ARMgt.CCRequest
        Return DA.CCRequestListInq(customclass)
    End Function
End Class
