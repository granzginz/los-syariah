
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class InvChgExpDate : Inherits ComponentBase
    Implements IInvChgExpDate


    Public Function InvChgExpDateList(ByVal inv As Parameter.InvChgExpDate) As Parameter.InvChgExpDate Implements [Interface].IInvChgExpDate.InvChgExpDateList
        Dim DA As New SQLEngine.ARMgt.InvChgExpDate
        Return DA.InvExpDateList(inv)
    End Function

    Public Function InvChgExpDateSave(ByVal inv As Parameter.InvChgExpDate) As Parameter.InvChgExpDate Implements [Interface].IInvChgExpDate.InvChgExpDateSave
        Dim DA As New SQLEngine.ARMgt.InvChgExpDate
        Return DA.InvExpDateSave(inv)
    End Function

    Public Function GetComboReason(ByVal customclass As Parameter.InvChgExpDate) As System.Data.DataTable Implements [Interface].IInvChgExpDate.GetComboReason
        Dim DA As New SQLEngine.ARMgt.InvChgExpDate
        Return DA.GetComboReason(customclass)
    End Function

    Public Function InvChgExpDateDetail(ByVal customclass As Parameter.InvChgExpDate) As Parameter.InvChgExpDate Implements [Interface].IInvChgExpDate.InvChgExpDateDetail
        Dim DA As New SQLEngine.ARMgt.InvChgExpDate
        Return DA.InvExpDateDetail(customclass)
    End Function

    Public Function InvChgExpDateView(ByVal customclass As Parameter.InvChgExpDate) As Parameter.InvChgExpDate Implements [Interface].IInvChgExpDate.InvChgExpDateView
        Dim DA As New SQLEngine.ARMgt.InvChgExpDate
        Return DA.InvExpDateView(customclass)
    End Function
End Class
