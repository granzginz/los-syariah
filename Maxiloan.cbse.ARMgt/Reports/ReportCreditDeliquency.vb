

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class ReportCreditDeliquency : Inherits ComponentBase
    Implements IReportCreditDeliquency

    Public Function ListCGID(ByVal customclass As Parameter.ReportCreditDeliquency) As Parameter.ReportCreditDeliquency Implements [Interface].IReportCreditDeliquency.ListCGID
        Dim DACredit As New SQLEngine.ARMgt.ReportCreditDeliquency
        Return DACredit.ListCGID(customclass)
    End Function

    Public Function ListCollector(ByVal customclass As Parameter.ReportCreditDeliquency) As Parameter.ReportCreditDeliquency Implements [Interface].IReportCreditDeliquency.ListCollector
        Dim DACredit As New SQLEngine.ARMgt.ReportCreditDeliquency
        Return DACredit.ListCollector(customclass)
    End Function

    Public Function ListReport(ByVal customclass As Parameter.ReportCreditDeliquency) As Parameter.ReportCreditDeliquency Implements [Interface].IReportCreditDeliquency.ListReport
        Dim DACredit As New SQLEngine.ARMgt.ReportCreditDeliquency
        Return DACredit.ListReport(customclass)
    End Function
End Class
