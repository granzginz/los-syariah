
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region
Public Class RptCollExecutor : Inherits ComponentBase
    Implements IRptCollExecutor

    Public Function ViewDataCollExecutor(ByVal customclass As Parameter.RptCollOverDue) As Parameter.RptCollOverDue Implements [Interface].IRptCollExecutor.ViewDataCollExecutor
        Dim DACollector As New SQLEngine.ARMgt.RptCollExecutor
        Return DACollector.ViewDataCollExecutor(customclass)
    End Function

    Public Function ViewReportCollExecutor(ByVal customclass As Parameter.RptCollOverDue) As Parameter.RptCollOverDue Implements [Interface].IRptCollExecutor.ViewReportCollExecutor
        Dim DACollector As New SQLEngine.ARMgt.RptCollExecutor
        Return DACollector.ViewReportCollExecutor(customclass)
    End Function
End Class
