
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class RptIncOverDueRating : Inherits ComponentBase
    Implements IRptIncOverDueRating


    Public Function IncOverDueRatingReports(ByVal oCustomClass As Parameter.RptIncOverDueRating) As Parameter.RptIncOverDueRating Implements [Interface].IRptIncOverDueRating.IncOverDueRatingReports
        Dim DAIncOverDueRating As New SQLEngine.ARMgt.RptIncOverDueRating
        Return DAIncOverDueRating.IncOverDueRatingReports(oCustomClass)
    End Function
End Class
