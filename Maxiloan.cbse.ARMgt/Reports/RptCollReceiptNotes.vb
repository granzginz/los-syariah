

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class RptCollReceiptNotes : Inherits ComponentBase
    Implements IRptCollReceiptNotes

    Public Function ViewRptCollReceiptNotes(ByVal customclass As Parameter.RptCollReceiptNotes) As Parameter.RptCollReceiptNotes Implements [Interface].IRptCollReceiptNotes.ViewRptCollReceiptNotes
        Dim DA As New SQLEngine.ARMgt.RptCollReceiptNotes
        Return DA.ViewReport(customclass)
    End Function
    Public Function OutStandingReport(ByVal customclass As Parameter.RptCollReceiptNotes) As Parameter.RptCollReceiptNotes Implements [Interface].IRptCollReceiptNotes.OutStandingReport
        Dim DA As New SQLEngine.ARMgt.RptCollReceiptNotes
        Return DA.OutStandingReport(customclass)
    End Function

End Class
