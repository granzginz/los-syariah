
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class RptCollOverDue : Inherits ComponentBase
    Implements IRptCollOverDue

    Public Function ViewDataCollector(ByVal customclass As Parameter.RptCollOverDue) As Parameter.RptCollOverDue Implements [Interface].IRptCollOverDue.ViewDataCollector
        Dim DACollector As New SQLEngine.ARMgt.RptCollOverDue
        Return DACollector.ViewDataCollector(customclass)
    End Function

    Public Function ViewReportCollOverDue(ByVal customclass As Parameter.RptCollOverDue) As Parameter.RptCollOverDue Implements [Interface].IRptCollOverDue.ViewReportCollOverDue
        Dim DACollector As New SQLEngine.ARMgt.RptCollOverDue
        Return DACollector.ViewReportCollOverDue(customclass)
    End Function

    Public Function ViewRptCreditDeliquency(ByVal customclass As Parameter.RptCollOverDue) As Parameter.RptCollOverDue Implements [Interface].IRptCollOverDue.ViewRptCreditDeliquency
        Dim DACollector As New SQLEngine.ARMgt.RptCollOverDue
        Return DACollector.ViewRptCreditDeliquency(customclass)
    End Function
End Class
