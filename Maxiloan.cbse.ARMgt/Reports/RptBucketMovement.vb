
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class RptBucketMovement : Inherits ComponentBase
    Implements IRptBucketMovement

    Public Function GetBeginStatus(ByVal strConnection As String) As System.Data.DataTable Implements [Interface].IRptBucketMovement.GetBeginStatus
        Dim DABucketMovement As New SQLEngine.ARMgt.RptIncBucketMovement
        Return DABucketMovement.GetBeginStatus(strConnection)
    End Function

    Public Function GetPeriod(ByVal strConnection As String) As System.Data.DataTable Implements [Interface].IRptBucketMovement.GetPeriod
        Dim DABucketMovement As New SQLEngine.ARMgt.RptIncBucketMovement
        Return DABucketMovement.GetPeriod(strConnection)
    End Function

    Public Function IncBucketMovementReports(ByVal oCustomClass As Parameter.RptIncBucketMovement) As Parameter.RptIncBucketMovement Implements [Interface].IRptBucketMovement.IncBucketMovementReports
        Dim DABucketMovement As New SQLEngine.ARMgt.RptIncBucketMovement
        Return DABucketMovement.IncBucketMovementReports(oCustomClass)
    End Function

    Public Function GetBeginHolder(ByVal oCustomClass As Parameter.RptIncBucketMovement) As System.Data.DataTable Implements [Interface].IRptBucketMovement.GetBeginHolder
        Dim DABucketMovement As New SQLEngine.ARMgt.RptIncBucketMovement
        Return DABucketMovement.GetBeginHolder(oCustomClass)
    End Function
End Class
