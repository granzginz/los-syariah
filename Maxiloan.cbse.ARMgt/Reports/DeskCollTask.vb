
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class DeskCollTask : Inherits ComponentBase
    Implements IDeskCollTask

    Public Function ViewDataCollector(ByVal customclass As Parameter.DeskCollTask) As Parameter.DeskCollTask Implements [Interface].IDeskCollTask.ViewDataCollector
        Dim DACollector As New SQLEngine.ARMgt.DeskCollTask
        Return DACollector.ViewDataCollector(customclass)
    End Function

    Public Function ListReport(ByVal customclass As Parameter.DeskCollTask) As Parameter.DeskCollTask Implements [Interface].IDeskCollTask.ListReport
        Dim DACollector As New SQLEngine.ARMgt.DeskCollTask
        Return DACollector.ListReport(customclass)
    End Function


End Class
