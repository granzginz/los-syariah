

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class RptCollAct : Inherits ComponentBase
    Implements IRptCollAct
    Public Function ViewDataCollector(ByVal customclass As Parameter.RptCollAct) As Parameter.RptCollAct Implements [Interface].IRptCollAct.ViewDataCollector
        Dim DACollector As New SQLEngine.ARMgt.RptCollAct
        Return DACollector.ViewDataCollector(customclass)
    End Function

    Public Function ViewReportColAct(ByVal customclass As Parameter.RptCollAct) As Parameter.RptCollAct Implements [Interface].IRptCollAct.ViewReportColAct
        Dim DACollector As New SQLEngine.ARMgt.RptCollAct
        Return DACollector.ViewReportCollAct(customclass)
    End Function

    Public Function ViewSubReportCollAct(ByVal customclass As Parameter.RptCollAct) As Parameter.RptCollAct Implements [Interface].IRptCollAct.ViewSubReportCollAct
        Dim DACollector As New SQLEngine.ARMgt.RptCollAct
        Return DACollector.ViewSubReportCollAct(customclass)
    End Function
End Class
