

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class DeskCollReminderActivity : Inherits ComponentBase
    Implements IDeskCollReminderActivity
    Public Function DeskCollReminderList(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity Implements [Interface].IDeskCollReminderActivity.DeskCollReminderList
        Dim DADeskCollReminder As New SQLEngine.ARMgt.DeskCollReminderActivity
        Return DADeskCollReminder.ListDeskCollReminder(customclass)
    End Function

    Public Function DeskCollReminderView(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity Implements [Interface].IDeskCollReminderActivity.DeskCollReminderView
        Dim DADeskCollReminder As New SQLEngine.ARMgt.DeskCollReminderActivity
        Return DADeskCollReminder.ListDeskCollReminderView(customclass)
    End Function

    Public Function ListResultAction(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity Implements [Interface].IDeskCollReminderActivity.ListResultAction
        Dim DADeskCollReminder As New SQLEngine.ARMgt.DeskCollReminderActivity
        Return DADeskCollReminder.ListResultAction(customclass)
    End Function
    Public Function SaveDeskCollActivity(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity Implements [Interface].IDeskCollReminderActivity.SaveDeskCollActivity
        Dim DADeskCollReminder As New SQLEngine.ARMgt.DeskCollReminderActivity
        Return DADeskCollReminder.SaveDeskCollActivity(customclass)
    End Function
    Public Function DownloadSMSReminderList(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity Implements [Interface].IDeskCollReminderActivity.DownloadSMSReminderList
        Dim DADeskCollReminder As New SQLEngine.ARMgt.DeskCollReminderActivity
        Return DADeskCollReminder.DownloadSMSReminderList(customclass)
    End Function
    Public Function DownloadSMSReminderPaging(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity Implements [Interface].IDeskCollReminderActivity.DownloadSMSReminderPaging
        Dim DADeskCollReminder As New SQLEngine.ARMgt.DeskCollReminderActivity
        Return DADeskCollReminder.DownloadSMSReminderPaging(customclass)
    End Function
End Class
