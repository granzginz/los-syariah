

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class RALInquiry : Inherits ComponentBase
    Implements IRALInquiry

    Public Function RALInqList(ByVal customclass As Parameter.RALInquiry) As Parameter.RALInquiry Implements [Interface].IRALInquiry.RALInqList
        Dim DARALInquiry As New SQLEngine.ARMgt.RALInquiry
        Return DARALInquiry.ListRALInq(customclass)
    End Function
    Public Function RALInqListCG(ByVal customclass As Parameter.RALInquiry) As Parameter.RALInquiry Implements [Interface].IRALInquiry.RALInqListCG
        Dim DARALInquiry As New SQLEngine.ARMgt.RALInquiry
        Return DARALInquiry.RALInqListCG(customclass)
    End Function

    Public Function RALInqListExecutor(ByVal customclass As Parameter.RALInquiry) As Parameter.RALInquiry Implements [Interface].IRALInquiry.RALInqListExecutor
        Dim DARALInquiry As New SQLEngine.ARMgt.RALInquiry
        Return DARALInquiry.RALInqListExecutor(customclass)
    End Function
    Public Function RALInqListExecutorCombo(ByVal customclass As Parameter.RALInquiry) As Parameter.RALInquiry Implements [Interface].IRALInquiry.RALInqListExecutorCombo
        Dim DARALInquiry As New SQLEngine.ARMgt.RALInquiry
        Return DARALInquiry.RALInqListExecutorCombo(customclass)
    End Function
    Public Function RALInqListExtend(ByVal customclass As Parameter.RALInquiry) As Parameter.RALInquiry Implements [Interface].IRALInquiry.RALInqListExtend
        Dim DARALInquiry As New SQLEngine.ARMgt.RALInquiry
        Return DARALInquiry.ListRALInqExtend(customclass)
    End Function
End Class
