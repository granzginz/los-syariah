

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class ReceiptNotesOnRequest : Inherits ComponentBase
    Implements IReceiptNotesOnRequest

    Public Function ReceiptNotesOnRequestListCG(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest Implements [Interface].IReceiptNotesOnRequest.ReceiptNotesOnRequestListCG
        Dim DAReceiptNotesOnRequest As New SQLEngine.ARMgt.ReceiptNotesOnRequest
        Return DAReceiptNotesOnRequest.ReceiptNotesOnRequestListCG(customclass)
    End Function

    Public Function ReceiptNotesOnRequestList(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest Implements [Interface].IReceiptNotesOnRequest.ReceiptNotesOnRequestList
        Dim DAReceiptNotesOnRequest As New SQLEngine.ARMgt.ReceiptNotesOnRequest
        Return DAReceiptNotesOnRequest.ReceiptNotesOnRequestList(customclass)
    End Function

    Public Function DataReceiptNotesOnRequest(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest Implements [Interface].IReceiptNotesOnRequest.DataReceiptNotesOnRequest
        Dim DAReceiptNotesOnRequest As New SQLEngine.ARMgt.ReceiptNotesOnRequest
        Return DAReceiptNotesOnRequest.DataReceiptNotesOnRequest(customclass)
    End Function
    Public Function ViewDataInstallment(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest Implements [Interface].IReceiptNotesOnRequest.ViewDataInstallment
        Dim DAReceiptNotesOnRequest As New SQLEngine.ARMgt.ReceiptNotesOnRequest
        Return DAReceiptNotesOnRequest.ViewDataInstallment(customclass)
    End Function


    Public Function GenerateKuitansiTagihOnRequest(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest Implements [Interface].IReceiptNotesOnRequest.GenerateKuitansiTagihOnRequest
        Dim DAReceiptNotesOnRequest As New SQLEngine.ARMgt.ReceiptNotesOnRequest
        Return DAReceiptNotesOnRequest.GenerateKuitansiTagihOnRequest(customclass)
    End Function

    'Public Function RALDataChangeExec(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest Implements [Interface].IReceiptNotesOnRequest.RALDataChangeExec
    '    Dim DAReceiptNotesOnRequest As New SQLEngine.ARMgt.ReceiptNotesOnRequest
    '    Return DAReceiptNotesOnRequest.RALDataChangeExec(customclass)
    'End Function
    Public Function RequestDataSelect(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest Implements [Interface].IReceiptNotesOnRequest.RequestDataSelect
        Dim DAReceiptNotesOnRequest As New SQLEngine.ARMgt.ReceiptNotesOnRequest
        Return DAReceiptNotesOnRequest.RequestDataSelect(customclass)
    End Function
End Class
