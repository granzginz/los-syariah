

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class DocDistribution : Inherits ComponentBase
    Implements IDocDistribution

    Public Function InqDocumentDistributionPaging(ByVal customclass As Parameter.DocDistribution) As Parameter.DocDistribution Implements [Interface].IDocDistribution.InqDocumentDistributionPaging
        Dim DAInqDocumentDistributionPaging As New SQLEngine.ARMgt.DocDistribution
        Return DAInqDocumentDistributionPaging.InqDocumentDistributionPaging(customclass)
    End Function

    Public Function DocumentDistributionPaging(ByVal customclass As Parameter.DocDistribution) As Parameter.DocDistribution Implements [Interface].IDocDistribution.DocumentDistributionPaging
        Dim DADocumentDistributionPaging As New SQLEngine.ARMgt.DocDistribution
        Return DADocumentDistributionPaging.DocumentDistributionPaging(customclass)
    End Function

    'Public Function DataInventoryAppraisal(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal Implements [Interface].IInventoryAppraisal.DataInventoryAppraisal
    '    Dim DAInventoryAppraisal As New DataAccess.Collection.InventoryAppraisal
    '    Return DAInventoryAppraisal.DataInventoryAppraisal(customclass)
    'End Function
    Public Function DocumentDistributionSave(ByVal customclass As Parameter.DocDistribution) As Parameter.DocDistribution Implements [Interface].IDocDistribution.DocumentDistributionSave
        Dim DADocumentDistributionSave As New SQLEngine.ARMgt.DocDistribution
        Return DADocumentDistributionSave.DocumentDistributionSave(customclass)
    End Function


    Public Function RequestDataDocDistribution(ByVal customclass As Parameter.DocDistribution) As Parameter.DocDistribution Implements [Interface].IDocDistribution.RequestDataDocDistribution
        Dim DARequestDataDocDistribution As New SQLEngine.ARMgt.DocDistribution
        Return DARequestDataDocDistribution.RequestDataDocDistribution(customclass)
    End Function

    'Public Function ViewAppraisal(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal Implements [Interface].IInventoryAppraisal.ViewAppraisal
    '    Dim DAInventoryAppraisal As New DataAccess.Collection.InventoryAppraisal
    '    Return DAInventoryAppraisal.ViewAppraisal(customclass)

    'End Function

    'Public Function GetAccruedInterest(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal Implements [Interface].IInventoryAppraisal.GetAccruedInterest
    '    Dim DAInventoryAppraisal As New DataAccess.Collection.InventoryAppraisal
    '    Return DAInventoryAppraisal.GetAccruedInterest(customclass)

    'End Function
    'Public Function ViewAppraisalbidder(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal Implements [Interface].IInventoryAppraisal.ViewAppraisalbidder
    '    Dim DAInventoryAppraisal As New DataAccess.Collection.InventoryAppraisal
    '    Return DAInventoryAppraisal.ViewAppraisalbidder(customclass)

    'End Function
End Class
