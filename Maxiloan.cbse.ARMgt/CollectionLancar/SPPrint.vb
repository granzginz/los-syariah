

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class SPPrint : Inherits ComponentBase
    Implements ISPPrint

    Public Function ListCGSPPrint(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint Implements [Interface].ISPPrint.ListCGSPPrint
        Dim DASPPrint As New SQLEngine.ARMgt.SPPrint
        Return DASPPrint.ListCGSPPrint(customclass)
    End Function

    Public Function ListSPPrinting(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint Implements [Interface].ISPPrint.ListSPPrinting
        Dim DASPPrint As New SQLEngine.ARMgt.SPPrint
        Return DASPPrint.ListSPPrinting(customclass)
    End Function

    Public Function ListReportSPPrinting(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint Implements [Interface].ISPPrint.ListReportSPPrinting
        Dim DASPPrint As New SQLEngine.ARMgt.SPPrint
        Return DASPPrint.ListReportSPPrint(customclass)
    End Function

    Public Function SPPrintProcess(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint Implements [Interface].ISPPrint.SPPrintProcess
        Dim DASPPrint As New SQLEngine.ARMgt.SPPrint
        Return DASPPrint.SPPrintProcess(customclass)
    End Function

    ''SSPRINT
    Public Function ListSSPrinting(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint Implements [Interface].ISPPrint.ListSSPrinting
        Dim DASPPrint As New SQLEngine.ARMgt.SPPrint 
        Return DASPPrint.ListSSPrinting(customclass) 
    End Function
  
    Public Function ListReportSSPrinting(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint Implements [Interface].ISPPrint.ListReportSSPrinting
        Dim DASSPrint As New SQLEngine.ARMgt.SPPrint
        Return DASSPrint.ListReportSSPrint(customclass)
    End Function
    Public Function SSPrintProcess(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint Implements [Interface].ISPPrint.SSPrintProcess
        Dim DASPPrint As New SQLEngine.ARMgt.SPPrint
        Return DASPPrint.SSPrintProcess(customclass)
    End Function

    Public Function ListRubrikPrint(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint Implements [Interface].ISPPrint.ListRubrikPrint
        Dim DASPPrint As New SQLEngine.ARMgt.SPPrint
        Return DASPPrint.ListRubrikPrint(customclass)
    End Function
End Class
