

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class SPVActivity : Inherits ComponentBase
    Implements ISPVActivity


    Public Function GetComboCases(ByVal SPVAct As Parameter.SPVActivity) As Parameter.SPVActivity Implements [Interface].ISPVActivity.GetComboCases
        Dim SPVActivity As New SQLEngine.ARMgt.SPVActivity
        Return SPVActivity.GetComboCases(SPVAct)
    End Function

    Public Function GetComboCollector(ByVal SPVAct As Parameter.SPVActivity) As Parameter.SPVActivity Implements [Interface].ISPVActivity.GetComboCollector
        Dim SPVActivity As New SQLEngine.ARMgt.SPVActivity
        Return SPVActivity.GetComboCollector(SPVAct)
    End Function

    Public Sub SPVActivityCases(ByVal SPVAct As Parameter.SPVActivity) Implements [Interface].ISPVActivity.SPVActivityCases
        Dim SPVActivity As New SQLEngine.ARMgt.SPVActivity
        Try
            SPVActivity.SPVActivityCases(SPVAct)
        Catch ex As Exception

        End Try

    End Sub

    Public Sub SPVActivityFixed(ByVal SPVAct As Parameter.SPVActivity) Implements [Interface].ISPVActivity.SPVActivityFixed
        Dim SPVActivity As New SQLEngine.ARMgt.SPVActivity
        Try
            SPVActivity.SPVActivityFixed(SPVAct)
        Catch ex As Exception

        End Try
    End Sub


    Public Sub SPVActivityRAL(ByVal SPVAct As Parameter.SPVActivity) Implements [Interface].ISPVActivity.SPVActivityRAL
        Dim SPVActivity As New SQLEngine.ARMgt.SPVActivity
        Try
            SPVActivity.SPVActivityRAL(SPVAct)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub SPVActivityRedDot(ByVal SPVAct As Parameter.SPVActivity) Implements [Interface].ISPVActivity.SPVActivityRedDot
        Dim SPVActivity As New SQLEngine.ARMgt.SPVActivity
        Try
            SPVActivity.SPVActivityRedDot(SPVAct)
        Catch ex As Exception

        End Try
    End Sub

    Public Function SPVActivityListAgreement(ByVal SPVAct As Parameter.SPVActivity) As Parameter.SPVActivity Implements [Interface].ISPVActivity.SPVActivityListAgreement
        Dim SPVActivity As New SQLEngine.ARMgt.SPVActivity
        Return SPVActivity.SPVActivity_List(SPVAct)
    End Function

    Public Function SPVActivityListAgreementChoosen(ByVal SPVAct As Parameter.SPVActivity) As Parameter.SPVActivity Implements [Interface].ISPVActivity.SPVActivityListAgreementChoosen
        Dim SPVActivity As New SQLEngine.ARMgt.SPVActivity
        Return SPVActivity.SPVActivityChoosen_List(SPVAct)
    End Function

    Public Function GetCollectorType(ByVal SPVAct As Parameter.SPVActivity) As String Implements [Interface].ISPVActivity.GetCollectorType
        Dim SPVActivity As New SQLEngine.ARMgt.SPVActivity
        Return SPVActivity.CheckCollectorType(SPVAct)
    End Function


    Public Function SPVListResultAction(ByVal SPVAct As Parameter.SPVActivity) As System.Data.DataSet Implements [Interface].ISPVActivity.SPVListResultAction
        Dim SPVActivity As New SQLEngine.ARMgt.SPVActivity
        Return SPVActivity.SPVListResultAction(SPVAct)
    End Function

    Public Function SPVActivityPlanSave(ByVal SPVAct As Parameter.SPVActivity) As Boolean Implements [Interface].ISPVActivity.SPVActivityPlanSave
        Dim SPVActivity As New SQLEngine.ARMgt.SPVActivity
        Return SPVActivity.SPVActivityPlanSave(SPVAct)
    End Function
End Class
