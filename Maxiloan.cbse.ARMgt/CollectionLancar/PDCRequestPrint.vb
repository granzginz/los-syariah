
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class PDCRequestPrint : Inherits ComponentBase
    Implements IPDCRequestPrint
    Public Function PDCRequestListCG(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint Implements [Interface].IPDCRequestPrint.PDCRequestListCG
        Dim DAPDC As New SQLEngine.ARMgt.PDCRequestPrint
        Return DAPDC.PDCRequestListCG(customclass)
    End Function

    Public Function PDCRequestList(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint Implements [Interface].IPDCRequestPrint.PDCRequestList
        Dim DAPDC As New SQLEngine.ARMgt.PDCRequestPrint
        Return DAPDC.PDCRequestList(customclass)
    End Function

    Public Function PDCRequestSave(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint Implements [Interface].IPDCRequestPrint.PDCRequestSave
        Dim DAPDC As New SQLEngine.ARMgt.PDCRequestPrint
        Return DAPDC.PDCRequestSave(customclass)
    End Function

    Public Function PDCRequestReport(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint Implements [Interface].IPDCRequestPrint.PDCRequestReport
        Dim DAPDC As New SQLEngine.ARMgt.PDCRequestPrint
        Return DAPDC.PDCRequestReport(customclass)
    End Function


    Public Function PDCRequestSurat(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint Implements [Interface].IPDCRequestPrint.PDCRequestSurat
        Dim DPAC As New SQLEngine.ARMgt.PDCRequestPrint
        Return DPAC.PDCRequestSurat(customclass)
    End Function
End Class
