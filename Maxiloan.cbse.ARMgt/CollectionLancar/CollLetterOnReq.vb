
#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.cbse.ARMgt
Imports Maxiloan.Interface
#End Region

Public Class CollLetterOnReq : Inherits ComponentBase
    Implements ICollLetterOnReq
    Public Function CollLetterOnReqPaging(ByVal oCustomClass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq Implements [Interface].ICollLetterOnReq.CollLetterOnReqPaging
        Dim DACollLetter As New SQLEngine.ARMgt.CollLetterOnReq
        Return DACollLetter.CollLetterOnReqPaging(oCustomClass)

    End Function

    Public Function CollLetterOnReqView(ByVal ocustomclass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq Implements [Interface].ICollLetterOnReq.CollLetterOnReqView
        Dim DACollLetter As New SQLEngine.ARMgt.CollLetterOnReq
        Return DACollLetter.CollLetterOnReqView(ocustomclass)
    End Function

    Public Function LetterNameOnReqCombo(ByVal ocustomclass As Parameter.CollLetterOnReq) As System.Data.DataTable Implements [Interface].ICollLetterOnReq.LetterNameOnReqCombo
        Dim DACollLetter As New SQLEngine.ARMgt.CollLetterOnReq
        Return DACollLetter.LetterNameOnReqCombo(ocustomclass)

    End Function

    Public Function LetterNameOnReqComboView(ByVal ocustomclass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq Implements [Interface].ICollLetterOnReq.LetterNameOnReqComboView
        Dim DACollLetter As New SQLEngine.ARMgt.CollLetterOnReq
        Return DACollLetter.LetterNameOnReqComboView(ocustomclass)
    End Function

    Public Function CollLetterTemplateField(ByVal oCustomClass As Parameter.CollLetterOnReq) As System.Data.DataTable Implements [Interface].ICollLetterOnReq.CollLetterTemplateField
        Dim DACollLetter As New SQLEngine.ARMgt.CollLetterOnReq
        Return DACollLetter.CollLetterTemplateField(oCustomClass)
    End Function

    Public Function CollLetterTemplateFieldName(ByVal ocustomclass As Parameter.CollLetterOnReq) As System.Data.DataTable Implements [Interface].ICollLetterOnReq.CollLetterTemplateFieldName
        Dim DACollLetter As New SQLEngine.ARMgt.CollLetterOnReq
        Return DACollLetter.CollLetterTemplateFieldName(ocustomclass)
    End Function

    Public Sub CollLetterOnReqSaving(ByVal oCustomClass As Parameter.CollLetterOnReq) Implements [Interface].ICollLetterOnReq.CollLetterOnReqSaving
        Dim DACollLetter As New SQLEngine.ARMgt.CollLetterOnReq
        DACollLetter.CollLetterOnReqSaving(oCustomClass)
    End Sub

    Public Function CollLetterOnReqViewTextToPrint(ByVal ocustomclass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq Implements [Interface].ICollLetterOnReq.CollLetterOnReqViewTextToPrint
        Dim DACollLetter As New SQLEngine.ARMgt.CollLetterOnReq
        Return DACollLetter.CollLetterOnReqViewTextToPrint(ocustomclass)
    End Function

    Public Function CollLetterReports(ByVal oCustomClass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq Implements [Interface].ICollLetterOnReq.CollLetterReports
        Dim DACollLetter As New SQLEngine.ARMgt.CollLetterOnReq
        Return DACollLetter.CollLetterReports(oCustomClass)
    End Function
End Class
