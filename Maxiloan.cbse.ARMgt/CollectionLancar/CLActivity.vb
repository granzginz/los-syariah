
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class CLActivity : Inherits ComponentBase
    Implements ICLActivity


    Public Function CLActivityList(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity Implements [Interface].ICLActivity.CLActivityList
        Dim DACLActivity As New SQLEngine.ARMgt.CLActivity
        Return DACLActivity.ListCLActivity(customclass)
    End Function

    Public Function CLActivityListRemidial(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity Implements [Interface].ICLActivity.CLActivityListRemidial
        Dim DACLActivity As New SQLEngine.ARMgt.CLActivity
        Return DACLActivity.ListCLActivityRemidial(customclass)
    End Function

    Public Function CLActivityListCollector(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity Implements [Interface].ICLActivity.CLActivityListCollector
        Dim DACLActivity As New SQLEngine.ARMgt.CLActivity
        Return DACLActivity.CLActivityListCollector(customclass)
    End Function

    Public Function CLActivityDataAgreement(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity Implements [Interface].ICLActivity.CLActivityDataAgreement
        Dim DACLActivity As New SQLEngine.ARMgt.CLActivity
        Return DACLActivity.CLActivityDataAgreement(customclass)
    End Function

    Public Function CLActivityActionResult(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity Implements [Interface].ICLActivity.CLActivityActionResult
        Dim DACLActivity As New SQLEngine.ARMgt.CLActivity
        Return DACLActivity.CLActivityActionResult(customclass)
    End Function

    Public Function CLActivitySave(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity Implements [Interface].ICLActivity.CLActivitySave
        Dim DACLActivity As New SQLEngine.ARMgt.CLActivity
        Return DACLActivity.CLActivitySave(customclass)
    End Function

    Public Function GetResult(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity Implements [Interface].ICLActivity.GetResult
        Dim DACLActivity As New SQLEngine.ARMgt.CLActivity
        Return DACLActivity.getResult(customclass)
    End Function

    Public Sub DCRSave(ByVal CustomClass As Parameter.CLActivity) Implements [Interface].ICLActivity.DCRSave
        Dim DADCRSave As New SQLEngine.ARMgt.CLActivity
        DADCRSave.DCRSave(CustomClass)
    End Sub

    Public Function GetDCRList(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity Implements [Interface].ICLActivity.GetDCRList
        Dim DACLActivity As New SQLEngine.ARMgt.CLActivity
        Return DACLActivity.GetDCRList(customclass)
    End Function

    Public Function CLActivityListMobCollector(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity Implements [Interface].ICLActivity.CLActivityListMobCollector
        Dim DACLActivity As New SQLEngine.ARMgt.CLActivity
        Return DACLActivity.CLActivityListMobCollector(customclass)
    End Function
End Class
