
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan
Imports Maxiloan.SQLEngine.LoanOrg
Imports Maxiloan.Parameter


#End Region
Public Class CustomerFacility : Inherits ComponentBase
    Implements ICustomerFacility

    Public Function CustomerFacilitySaveAdd(oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility Implements ICustomerFacility.CustomerFacilitySaveAdd
        Dim DataDA As New SQLEngine.LoanOrg.CustomerFacility
        Return DataDA.CustomerFacilitySaveAdd(oCustomClass)
    End Function

    Public Function GetFacilityDetail(oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility Implements ICustomerFacility.GetFacilityDetail
        Dim DataDA As New SQLEngine.LoanOrg.CustomerFacility
        Return DataDA.GetFacilityDetail(oCustomClass)
    End Function

    Public Function GetAddendumFacilityDetail(oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility Implements ICustomerFacility.GetAddendumFacilityDetail
        Dim DataDA As New SQLEngine.LoanOrg.CustomerFacility
        Return DataDA.GetAddendumFacilityDetail(oCustomClass)
    End Function

    Public Function GetFacilityList(ByVal oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility Implements [Interface].ICustomerFacility.GetFacilityList
        Dim DataDA As New SQLEngine.LoanOrg.CustomerFacility
        Return DataDA.GetFacilityList(oCustomClass)
    End Function

    Public Function CustomerFacilityApprovalList(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility Implements [Interface].ICustomerFacility.CustomerFacilityApprovalList
        Dim DataDA As New SQLEngine.LoanOrg.CustomerFacility
        Return DataDA.CustomerFacilityApprovalList(customclass)
    End Function

    Public Function GetCboUserAprPCAll(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility Implements [Interface].ICustomerFacility.GetCboUserAprPCAll
        Dim DataDA As New SQLEngine.LoanOrg.CustomerFacility
        Return DataDA.GetCboUserAprPCAll(customclass)
    End Function

    Public Function GetCboUserApproval(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility Implements [Interface].ICustomerFacility.GetCboUserApproval
        Dim DataDA As New SQLEngine.LoanOrg.CustomerFacility
        Return DataDA.GetCboUserApproval(customclass)
    End Function

    Public Function CustFacApproveSaveToNextPerson(ByVal customclass As Parameter.CustomerFacility) As String Implements [Interface].ICustomerFacility.CustFacApproveSaveToNextPerson
        Dim DataDA As New SQLEngine.LoanOrg.CustomerFacility
        Return DataDA.CustFacApproveSaveToNextPerson(customclass)
    End Function

    Public Function CustFacApproveSave(ByVal customclass As Parameter.CustomerFacility) As String Implements [Interface].ICustomerFacility.CustFacApproveSave
        Dim DataDA As New SQLEngine.LoanOrg.CustomerFacility
        Return DataDA.CustFacApproveSave(customclass)
    End Function

    Public Function CustFacApproveisFinal(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility Implements [Interface].ICustomerFacility.CustFacApproveisFinal
        Dim DataDA As New SQLEngine.LoanOrg.CustomerFacility
        Return DataDA.CustFacApproveisFinal(customclass)
    End Function

    Public Function CustFacApproveIsValidApproval(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility Implements [Interface].ICustomerFacility.CustFacApproveIsValidApproval
        Dim DataDA As New SQLEngine.LoanOrg.CustomerFacility
        Return DataDA.CustFacApproveIsValidApproval(customclass)
    End Function

    Public Function AddendumCustomerFacilitySaveAdd(oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility Implements ICustomerFacility.AddendumCustomerFacilitySaveAdd
        Dim DataDA As New SQLEngine.LoanOrg.CustomerFacility
        Return DataDA.AddendumCustomerFacilitySaveAdd(oCustomClass)
    End Function

    Public Function AddendumCustomerFacilityApprovalList(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility Implements [Interface].ICustomerFacility.AddendumCustomerFacilityApprovalList
        Dim DataDA As New SQLEngine.LoanOrg.CustomerFacility
        Return DataDA.AddendumCustomerFacilityApprovalList(customclass)
    End Function

    Public Function AddendumCustFacApproveSave(ByVal customclass As Parameter.CustomerFacility) As String Implements [Interface].ICustomerFacility.AddendumCustFacApproveSave
        Dim DataDA As New SQLEngine.LoanOrg.CustomerFacility
        Return DataDA.AddendumCustFacApproveSave(customclass)
    End Function

    Public Function AddendumCustFacApproveSaveToNextPerson(ByVal customclass As Parameter.CustomerFacility) As String Implements [Interface].ICustomerFacility.AddendumCustFacApproveSaveToNextPerson
        Dim DataDA As New SQLEngine.LoanOrg.CustomerFacility
        Return DataDA.AddendumCustFacApproveSaveToNextPerson(customclass)
    End Function
End Class
