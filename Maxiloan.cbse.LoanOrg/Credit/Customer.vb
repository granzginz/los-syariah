
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan
Imports Maxiloan.SQLEngine.LoanOrg
Imports Maxiloan.Parameter


#End Region
Public Class Customer : Inherits ComponentBase
    Implements ICustomer
    Public Function GetCustomerFromProspect(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.GetCustomerFromProspect
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.GetCustomerFromProspect(oCustomClass)
    End Function
    Public Function GetCustomer(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.GetCustomer
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.GetCustomer(oCustomClass)
    End Function

    Function GetCustomerEdit(ByVal oCustomClass As Parameter.Customer, ByVal Level As String) As Parameter.Customer Implements [Interface].ICustomer.GetCustomerEdit
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.GetCustomerEdit(oCustomClass, Level)
    End Function
    Function CompanySaveAdd(ByVal oCustomer As Parameter.Customer, _
                            ByVal oAddress As Parameter.Address, _
                            ByVal oWarehouseAddress As Parameter.Address, _
                            ByVal oData1 As DataTable, _
                            ByVal oData2 As DataTable, _
                            ByVal CompanyCustomerDPEx As Parameter.CompanyCustomerDPEx, _
                            ByVal CompanyCustomerPJ As Parameter.CompanyCustomerPJ, _
                            ByVal CompanyCustomerPJAddress As Parameter.Address, _
                             ByVal companySIUPAddress As Parameter.Address
                            ) As Parameter.Customer Implements [Interface].ICustomer.CompanySaveAdd
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.CompanySaveAdd(oCustomer, _
                                         oAddress, _
                                         oWarehouseAddress, _
                                         oData1, _
                                         oData2, _
                                         CompanyCustomerDPEx, _
                                         CompanyCustomerPJ, _
                                         CompanyCustomerPJAddress,
                                         companySIUPAddress)
    End Function
    Function CompanySaveEdit(ByVal oCustomer As Parameter.Customer, _
                             ByVal oAddress As Parameter.Address, _
                             ByVal oWarehouseAddress As Parameter.Address, _
                             ByVal oData1 As DataTable, _
                             ByVal oData2 As DataTable, _
                            ByVal CompanyCustomerDPEx As Parameter.CompanyCustomerDPEx, _
                            ByVal CompanyCustomerPJ As Parameter.CompanyCustomerPJ, _
                            ByVal CompanyCustomerPJAddress As Parameter.Address, _
                             ByVal companySIUPAddress As Parameter.Address) As String Implements [Interface].ICustomer.CompanySaveEdit
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.CompanySaveEdit(oCustomer, _
                                          oAddress, _
                                          oWarehouseAddress, _
                                          oData1, _
                                          oData2, _
                                          CompanyCustomerDPEx, _
                                         CompanyCustomerPJ, _
                                         CompanyCustomerPJAddress,
                                         companySIUPAddress)
    End Function
    Public Function BindCustomer1_002(ByVal oCustomer As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.BindCustomer1_002
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.BindCustomer1_002(oCustomer)
    End Function
    Public Function BindCustomer2_002(ByVal oCustomer As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.BindCustomer2_002
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.BindCustomer2_002(oCustomer)
    End Function
    Public Function BindCustomerC1_002(ByVal oCustomer As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.BindCustomerC1_002
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.BindCustomerC1_002(oCustomer)
    End Function
    Public Function BindCustomerC2_002(ByVal oCustomer As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.BindCustomerC2_002
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.BindCustomerC2_002(oCustomer)
    End Function
    Public Function CustomerPersonalGetIDType(ByVal oCustomer As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.CustomerPersonalGetIDType
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.CustomerPersonalGetIDType(oCustomer)
    End Function
    Public Function CustomerCompanyGetIDType(ByVal oCustomer As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.CustomerCompanyGetIDType
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.CustomerCompanyGetIDType(oCustomer)
    End Function
    Public Function CustomerPersonalAdd(ByVal oCustomer As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.CustomerPersonalAdd
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.CustomerPersonalAdd(oCustomer)
    End Function
    Public Function PersonalSaveAdd(ByVal oCustomer As Parameter.Customer, _
                                    ByVal oLegalAddress As Parameter.Address, _
                                    ByVal oResAddress As Parameter.Address, _
                                    ByVal oJDAddress As Parameter.Address, _
                                    ByVal oDataOmset As DataTable, _
                                    ByVal oDataFamily As DataTable, _
                                    ByVal oPCEX As Parameter.CustomerEX, _
                                    ByVal oPCSI As Parameter.CustomerSI,
                                    ByVal oPCSIAddress As Parameter.Address, _
                                    ByVal oPCER As Parameter.CustomerER, _
                                    ByVal oPCERAddress As Parameter.Address) As Parameter.Customer Implements [Interface].ICustomer.PersonalSaveAdd
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.PersonalSaveAdd(oCustomer, _
                                          oLegalAddress, _
                                          oResAddress, _
                                          oJDAddress, _
                                          oDataOmset, _
                                          oDataFamily, _
                                          oPCEX, _
                                          oPCSI, _
                                          oPCSIAddress, _
                                          oPCER, _
                                          oPCERAddress)
    End Function
    Public Function PersonalSaveEdit(ByVal oCustomer As Parameter.Customer, _
                                     ByVal oLegalAddress As Parameter.Address, _
                                     ByVal oResAddress As Parameter.Address, _
                                     ByVal oJDAddress As Parameter.Address, _
                                     ByVal oDataOmset As DataTable, _
                                     ByVal oDataFamily As DataTable, _
                                     ByVal oPCEX As Parameter.CustomerEX, _
                                     ByVal oPCSI As Parameter.CustomerSI, _
                                     ByVal oPCSIAddress As Parameter.Address, _
                                     ByVal oPCER As Parameter.CustomerER, _
                                     ByVal oPCERAddress As Parameter.Address) As String Implements [Interface].ICustomer.PersonalSaveEdit
        Dim CustEditDA As New SQLEngine.LoanOrg.Customer
        Return CustEditDA.PersonalSaveEdit(oCustomer, _
                                           oLegalAddress, _
                                           oResAddress, _
                                           oJDAddress, _
                                           oDataOmset, _
                                           oDataFamily, _
                                           oPCEX, _
                                           oPCSI, _
                                           oPCSIAddress, _
                                           oPCER, _
                                           oPCERAddress)
    End Function
    Sub Generate_XMLP(ByVal pStrFile As String, ByVal pStrGelarDepan As String, ByVal pStrName As String, ByVal pStrGelarBelakang As String, _
                         ByVal pStrType As String, ByVal pStrIDType As String, ByVal pStrIDTypeName As String, _
                         ByVal pStrNumber As String, ByVal IDExpiredDate As String, ByVal pStrGender As String, _
                         ByVal pStrBirthPlace As String, ByVal pStrBirthDate As String, _
                         ByRef pStrSuccess As Boolean, ByVal pMotherName As String, _
                         ByVal pKartuKeluarga As String, ByVal pNamaPasangan As String, ByVal pStatusPerkawinan As String) Implements [Interface].ICustomer.Generate_XMLP

        Dim lObjDataSet As DataSet
        Dim lObjDataTable As DataTable
        Dim lObjDataRow As DataRow

        lObjDataTable = New DataTable("Cust")
        lObjDataTable.Columns.Add("GelarDepan", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("CustName", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("GelarBelakang", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("CustType", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("CustIDType", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("CustIDTypeName", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("CustNumber", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("IDExpiredDate", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("CustGender", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("CustBirthPlace", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("CustBirthDate", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("MotherName", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("KartuKeluarga", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("NamaPasangan", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("StatusPerkawinan", System.Type.GetType("System.String"))


        lObjDataRow = lObjDataTable.NewRow
        lObjDataRow("GelarDepan") = pStrGelarDepan.Trim
        lObjDataRow("CustName") = pStrName.Trim
        lObjDataRow("GelarBelakang") = pStrGelarBelakang.Trim
        lObjDataRow("CustType") = pStrType.Trim
        lObjDataRow("CustIDType") = pStrIDType.Trim
        lObjDataRow("CustIDTypeName") = pStrIDTypeName.Trim
        lObjDataRow("CustNumber") = pStrNumber.Trim
        lObjDataRow("IDExpiredDate") = IDExpiredDate.Trim
        lObjDataRow("CustGender") = pStrGender.Trim
        lObjDataRow("CustBirthPlace") = pStrBirthPlace.Trim
        lObjDataRow("CustBirthDate") = pStrBirthDate.Trim
        lObjDataRow("MotherName") = pMotherName.Trim
        lObjDataRow("KartuKeluarga") = pKartuKeluarga.Trim
        lObjDataRow("NamaPasangan") = pNamaPasangan.Trim
        lObjDataRow("StatusPerkawinan") = pStatusPerkawinan.Trim
        lObjDataTable.Rows.Add(lObjDataRow)
        lObjDataSet = New DataSet("Cust")
        lObjDataSet.Tables.Add(lObjDataTable)

        lObjDataRow = Nothing
        lObjDataTable = Nothing

        Try
            lObjDataSet.WriteXml(pStrFile)
            pStrSuccess = True
        Catch ex As Exception
            pStrSuccess = False
        End Try
        lObjDataSet = Nothing
    End Sub
    Sub Generate_XMLC(ByVal pStrFile As String, ByVal pStrName As String, _
                          ByVal pStrNPWP As String, ByVal pStrAddress As String, ByVal pStrRT As String, _
                          ByVal pStrRW As String, ByVal pStrKelurahan As String, _
                          ByVal pStrKecamatan As String, ByVal pStrCity As String, _
                          ByVal pStrZipCode As String, ByVal pStrAPhone1 As String, _
                          ByVal pStrPhone1 As String, ByVal pStrAPhone2 As String, _
                          ByVal pStrPhone2 As String, ByVal pStrAFax As String, _
                          ByVal pStrFax As String, ByRef pStrSuccess As Boolean) Implements [Interface].ICustomer.Generate_XMLC

        Dim lObjDataSet As DataSet
        Dim lObjDataTable As DataTable
        Dim lObjDataRow As DataRow

        lObjDataTable = New DataTable("Cust")

        lObjDataTable.Columns.Add("Name", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("NPWP", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("Address", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("RT", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("RW", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("Kelurahan", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("Kecamatan", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("City", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("ZipCode", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("APhone1", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("Phone1", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("APhone2", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("Phone2", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("AFax", System.Type.GetType("System.String"))
        lObjDataTable.Columns.Add("Fax", System.Type.GetType("System.String"))
        lObjDataRow = lObjDataTable.NewRow
        lObjDataRow("Name") = pStrName.Trim
        lObjDataRow("NPWP") = pStrNPWP.Trim
        lObjDataRow("Address") = pStrAddress.Trim
        lObjDataRow("RT") = pStrRT.Trim
        lObjDataRow("RW") = pStrRW.Trim
        lObjDataRow("Kelurahan") = pStrKelurahan.Trim
        lObjDataRow("Kecamatan") = pStrKecamatan.Trim
        lObjDataRow("City") = pStrCity.Trim
        lObjDataRow("ZipCode") = pStrZipCode.Trim
        lObjDataRow("APhone1") = pStrAPhone1.Trim
        lObjDataRow("Phone1") = pStrPhone1.Trim
        lObjDataRow("APhone2") = pStrAPhone2.Trim
        lObjDataRow("Phone2") = pStrPhone2.Trim
        lObjDataRow("AFax") = pStrAFax.Trim
        lObjDataRow("Fax") = pStrFax.Trim
        lObjDataTable.Rows.Add(lObjDataRow)

        lObjDataSet = New DataSet("Cust")
        lObjDataSet.Tables.Add(lObjDataTable)

        lObjDataRow = Nothing
        lObjDataTable = Nothing

        Try
            lObjDataSet.WriteXml(pStrFile)
            pStrSuccess = True
        Catch
            pStrSuccess = False
        End Try
        lObjDataSet = Nothing
    End Sub


#Region "View Customer"

    Public Function GetViewCustomer(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.GetViewCustomer
        Try
            Dim CustomerDA As New SQLEngine.LoanOrg.Customer
            Return CustomerDA.GetViewCustomer(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetViewCustomerEmpOmset(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.GetViewCustomerEmpOmset
        Try
            Dim CustomerDA As New SQLEngine.LoanOrg.Customer
            Return CustomerDA.GetViewCustomerEmpOmset(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetViewCustomerEntOmset(ByVal oCustomClass As Parameter.Customer) As System.Data.DataTable Implements [Interface].ICustomer.GetViewCustomerEntOmset
        Try
            Dim CustomerDA As New SQLEngine.LoanOrg.Customer
            Return CustomerDA.GetViewCustomerEntOmset(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function GetViewCustomerFamily(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.GetViewCustomerFamily
        Try
            Dim CustomerDA As New SQLEngine.LoanOrg.Customer
            Return CustomerDA.GetViewCustomerFamily(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetViewCustomer_Summary(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.GetViewCustomer_Summary
        Try
            Dim CustomerDA As New SQLEngine.LoanOrg.Customer
            Return CustomerDA.GetViewCustomer_Summary(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetViewCustomerEmpOmset_Summary(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.GetViewCustomerEmpOmset_Summary
        Try
            Dim CustomerDA As New SQLEngine.LoanOrg.Customer
            Return CustomerDA.GetViewCustomerEmpOmset_Summary(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetViewCustomerEntOmset_Summary(ByVal oCustomClass As Parameter.Customer) As System.Data.DataTable Implements [Interface].ICustomer.GetViewCustomerEntOmset_Summary
        Try
            Dim CustomerDA As New SQLEngine.LoanOrg.Customer
            Return CustomerDA.GetViewCustomerEntOmset_Summary(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function GetViewCustomerFamily_Summary(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.GetViewCustomerFamily_Summary
        Try
            Dim CustomerDA As New SQLEngine.LoanOrg.Customer
            Return CustomerDA.GetViewCustomerFamily_Summary(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    '-------------------------------------------------------------------------

    Public Function GetViewCompanyCustomer(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.GetViewCompanyCustomer
        Try
            Dim CustomerDA As New SQLEngine.LoanOrg.Customer
            Return CustomerDA.GetViewCompanyCustomer(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetViewCompanyCustomerManagement(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.GetViewCompanyCustomerManagement
        Try
            Dim CustomerDA As New SQLEngine.LoanOrg.Customer
            Return CustomerDA.GetViewCompanyCustomerManagement(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetViewCompanyCustomerDocument(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.GetViewCompanyCustomerDocument
        Try
            Dim CustomerDA As New SQLEngine.LoanOrg.Customer
            Return CustomerDA.GetViewCompanyCustomerDocument(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetAgreementListCompRpt(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.GetAgreementListCompRpt
        Try
            Dim CustomerDA As New SQLEngine.LoanOrg.Customer
            Return CustomerDA.GetAgreementListCompRpt(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetAgreementListCompRpt_Summary(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.GetAgreementListCompRpt_Summary
        Try
            Dim CustomerDA As New SQLEngine.LoanOrg.Customer
            Return CustomerDA.GetAgreementListCompRpt_Summary(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetAgreementListSummary1(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.GetAgreementListSummary1
        Try
            Dim CustomerDA As New SQLEngine.LoanOrg.Customer
            Return CustomerDA.GetAgreementListSummary1(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetAgreementListSummary2(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer Implements [Interface].ICustomer.GetAgreementListSummary2
        Try
            Dim CustomerDA As New SQLEngine.LoanOrg.Customer
            Return CustomerDA.GetAgreementListSummary2(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

    Public Function GetCustomerType(ByVal CustomClass As Parameter.Customer) As String Implements [Interface].ICustomer.GetCustomerType
        Try
            Dim CustomerDA As New SQLEngine.LoanOrg.Customer
            Return CustomerDA.GetCustomerType(CustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetTotalCustomer(ByVal strCustomerID As String, ByVal strConnection As String) As Integer Implements [Interface].ICustomer.GetTotalCustomer
        Try
            Dim CustomerDA As New SQLEngine.LoanOrg.Customer
            Return CustomerDA.GetTotalCustomer(strCustomerID, strConnection)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Public Function GetPersonalOL(cnn As String, custId As String) As IList(Of PinjamanLain)
    '    Dim PersonalOL As New SQLEngine.LoanOrg.Customer
    '    Return PersonalOL.GetPersonalOL(cnn, custId)
    'End Function
    Public Function BindCustomer1_002_withSI(ByVal oCustomer As Parameter.Customer, ByVal SIdata As Parameter.CustomerSI) As Parameter.Customer Implements [Interface].ICustomer.BindCustomer1_002_withSI
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.BindCustomer1_002_withSI(oCustomer, SIdata)
    End Function

    Public Function PersonalCustomerIdentitasAdd(oCustomer As Parameter.Customer, oLegalAddress As Parameter.Address, oResAddress As Parameter.Address, oPCEX As Parameter.CustomerEX) As Parameter.Customer Implements [Interface].ICustomer.PersonalCustomerIdentitasAdd
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.PersonalCustomerIdentitasAdd(oCustomer, _
                                          oLegalAddress, _
                                          oResAddress, _
                                          oPCEX)
    End Function

    Public Function PersonalCustomerEmergencySaveEdit(oCustomer As Parameter.Customer, oPCER As Parameter.CustomerER, oPCERAddress As Parameter.Address) As String Implements [Interface].ICustomer.PersonalCustomerEmergencySaveEdit
        Dim CustEditDA As New SQLEngine.LoanOrg.Customer
        Return CustEditDA.PersonalCustomerEmergencySaveEdit(oCustomer, _
                                           oPCER, _
                                           oPCERAddress)
    End Function

    Public Function PersonalCustomerKeluargaSaveEdit(oCustomer As Parameter.Customer, oDataFamily As System.Data.DataTable) As String Implements [Interface].ICustomer.PersonalCustomerKeluargaSaveEdit
        Dim CustEditDA As New SQLEngine.LoanOrg.Customer
        Return CustEditDA.PersonalCustomerKeluargaSaveEdit(oCustomer, _
                                           oDataFamily)
    End Function
    'Public Function PersonalCustomerOLSaveEdit(cnn As String, custid As String, data As IList(Of PinjamanLain)) As String Implements [Interface].ICustomer.PersonalCustomerOLSaveEdit
    '    Dim CustEditDA As New SQLEngine.LoanOrg.Customer
    '    Return CustEditDA.PersonalCustomerOLSaveEdit(cnn, custid, data)
    'End Function
    Public Function PersonalCustomerOLSaveEdit(cnn As String, custid As String, data As IList(Of PinjamanLain)) As String Implements [Interface].ICustomer.PersonalCustomerOLSaveEdit
        Dim CustEditDA As New SQLEngine.LoanOrg.Customer
        Return CustEditDA.PersonalCustomerOLSaveEdit(cnn, custid, data)
    End Function


    Public Function PersonalCustomerKeuanganSaveEdit(oCustomer As Parameter.Customer) As String Implements [Interface].ICustomer.PersonalCustomerKeuanganSaveEdit
        Dim CustEditDA As New SQLEngine.LoanOrg.Customer
        Return CustEditDA.PersonalCustomerKeuanganSaveEdit(oCustomer)
    End Function

    Public Function PersonalCustomerPasanganSaveEdit(oCustomer As Parameter.Customer, oPCSI As Parameter.CustomerSI, oPCSIAddress As Parameter.Address) As String Implements [Interface].ICustomer.PersonalCustomerPasanganSaveEdit
        Dim CustEditDA As New SQLEngine.LoanOrg.Customer
        Return CustEditDA.PersonalCustomerPasanganSaveEdit(oCustomer, _
                                           oPCSI, _
                                           oPCSIAddress)
    End Function
    Public Function PersonalCustomerGuarantorSaveEdit(oCustomer As Parameter.Customer, oPCSI As Parameter.CustomerSI, oPCSIAddress As Parameter.Address) As String Implements [Interface].ICustomer.PersonalCustomerGuarantorSaveEdit
        Dim CustEditDA As New SQLEngine.LoanOrg.Customer
        Return CustEditDA.PersonalCustomerGuarantorSaveEdit(oCustomer, _
                                           oPCSI, _
                                           oPCSIAddress)
    End Function
    Public Function PersonalCustomerPekerjaanSaveEdit(oCustomer As Parameter.Customer, oJDAddress As Parameter.Address, oPCEX As Parameter.CustomerEX) As String Implements [Interface].ICustomer.PersonalCustomerPekerjaanSaveEdit
        Dim CustEditDA As New SQLEngine.LoanOrg.Customer
        Return CustEditDA.PersonalCustomerPekerjaanSaveEdit(oCustomer, _
                                           oJDAddress, _
                                           oPCEX)
    End Function

    Public Function PersonalCustomerIdentitasSaveEdit(oCustomer As Parameter.Customer, oLegalAddress As Parameter.Address, oResAddress As Parameter.Address, oPCEX As Parameter.CustomerEX) As String Implements [Interface].ICustomer.PersonalCustomerIdentitasSaveEdit
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.PersonalCustomerIdentitasSaveEdit(oCustomer, _
                                          oLegalAddress, _
                                          oResAddress, _
                                          oPCEX)
    End Function

    Public Function CompanyDataPerusahaanSaveAdd(oCustomer As Parameter.Customer, oAddress As Parameter.Address, oWarehouseAddress As Parameter.Address, CompanyCustomerDPEx As Parameter.CompanyCustomerDPEx, companySIUPAddress As Parameter.Address) As Parameter.Customer Implements [Interface].ICustomer.CompanyDataPerusahaanSaveAdd
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.CompanyDataPerusahaanSaveAdd(oCustomer, _
                                         oAddress, _
                                         oWarehouseAddress, _
                                         CompanyCustomerDPEx, _
                                         companySIUPAddress)
    End Function

    Public Function CompanyDataPerusahaanSaveEdit(oCustomer As Parameter.Customer, oAddress As Parameter.Address, oWarehouseAddress As Parameter.Address, CompanyCustomerDPEx As Parameter.CompanyCustomerDPEx, companySIUPAddress As Parameter.Address) As String Implements [Interface].ICustomer.CompanyDataPerusahaanSaveEdit
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.CompanyDataPerusahaanSaveEdit(oCustomer, _
                                          oAddress, _
                                          oWarehouseAddress, _
                                          CompanyCustomerDPEx, _
                                         companySIUPAddress)
    End Function

    Public Function CompanyKeuanganSaveEdit(oCustomer As Parameter.Customer) As String Implements [Interface].ICustomer.CompanyKeuanganSaveEdit
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.CompanyKeuanganSaveEdit(oCustomer)
    End Function

    Public Function CompanyLegalitasSaveEdit(oCustomer As Parameter.Customer, oData2 As System.Data.DataTable) As String Implements [Interface].ICustomer.CompanyLegalitasSaveEdit
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.CompanyLegalitasSaveEdit(oCustomer, oData2)
    End Function

    Public Function CompanyManagementSaveEdit(oCustomer As Parameter.Customer, oData1 As System.Data.DataTable, CompanyCustomerPJ As Parameter.CompanyCustomerPJ, CompanyCustomerPJAddress As Parameter.Address) As String Implements [Interface].ICustomer.CompanyManagementSaveEdit
        Dim CustomerDA As New SQLEngine.LoanOrg.Customer
        Return CustomerDA.CompanyManagementSaveEdit(oCustomer, oData1, CompanyCustomerPJ, CompanyCustomerPJAddress)
    End Function
    Public Function LoadCombo(cnn As String, ty As String) As IList(Of Parameter.CommonValueText) Implements [Interface].ICustomer.LoadCombo
        Return (New SQLEngine.LoanOrg.Customer).LoadCombo(cnn, ty)
    End Function
    Public Function GetCompanyCustomerEC(cnn As String, oCustomClass As Parameter.CompanyCustomerEC) As Parameter.CompanyCustomerEC Implements [Interface].ICustomer.GetCompanyCustomerEC
        Return (New SQLEngine.LoanOrg.Customer).GetCompanyCustomerEC(cnn, oCustomClass)
    End Function

    Public Function CompanyCustomerECSaveEdit(cnn As String, oCustomClass As Parameter.CompanyCustomerEC) As String Implements [Interface].ICustomer.CompanyCustomerECSaveEdit
        Return (New SQLEngine.LoanOrg.Customer).CompanyCustomerECSaveEdit(cnn, oCustomClass)
    End Function


    Public Function GetCompanyCustomerGuarantor(cnn As String, customerid As String) As IList(Of Parameter.CompanyCustomerGuarantor) Implements [Interface].ICustomer.GetCompanyCustomerGuarantor
        Return (New SQLEngine.LoanOrg.Customer).GetCompanyCustomerGuarantor(cnn, customerid)
    End Function

    'Public Function GetCompanyCustomerGuarantor(cnn As String, customerid As String) As Parameter.CompanyCustomerGuarantor Implements [Interface].ICustomer.GetCompanyCustomerGuarantor
    '    Return (New SQLEngine.LoanOrg.Customer).GetCompanyCustomerGuarantor(cnn, customerid)
    'End Function

    Public Function CompanyCustomerGuarantorSaveEdit(cnn As String, customerid As String, ccGuarantors As IList(Of Parameter.CompanyCustomerGuarantor)) As String Implements [Interface].ICustomer.CompanyCustomerGuarantorSaveEdit
        Return (New SQLEngine.LoanOrg.Customer).CompanyCustomerGuarantorSaveEdit(cnn, customerid, ccGuarantors)
    End Function
    Function GetPersonalOL(cnn As String, custId As String) As IList(Of PinjamanLain) Implements [Interface].ICustomer.GetPersonalOL
        Return (New SQLEngine.LoanOrg.Customer).GetPersonalOL(cnn, custId)
    End Function

	Public Function CheckPersonalCustomerDocument(ByVal oCustomClass As Parameter.Customer) As Integer Implements [Interface].ICustomer.CheckPersonalCustomerDocument
		Try
			Dim CustomerDA As New SQLEngine.LoanOrg.Customer
			Return CustomerDA.CheckPersonalCustomerDocument(oCustomClass)
		Catch ex As Exception
			Throw ex
		End Try
	End Function

	Public Function GetCompanyCustomerPK(cnn As String, oCustomClass As Parameter.CompanyCustomerPK) As Parameter.CompanyCustomerPK Implements [Interface].ICustomer.GetCompanyCustomerPK
		Return (New SQLEngine.LoanOrg.Customer).GetCompanyCustomerPK(cnn, oCustomClass)
	End Function

	Public Function CompanyCustomerPKSaveAdd(oCustomClass As Parameter.CompanyCustomerPK) As Parameter.CompanyCustomerPK Implements [Interface].ICustomer.CompanyCustomerPKSaveAdd
		Dim CustomerDA As New SQLEngine.LoanOrg.Customer
		Return CustomerDA.CompanyCustomerPKSaveAdd(oCustomClass)
	End Function

	Public Function CompanyCustomerPKSaveEdit(oCustomClass As Parameter.CompanyCustomerPK) As Parameter.CompanyCustomerPK Implements [Interface].ICustomer.CompanyCustomerPKSaveEdit
		Dim CustomerDA As New SQLEngine.LoanOrg.Customer
		Return CustomerDA.CompanyCustomerPKSaveEdit(oCustomClass)
	End Function
End Class
