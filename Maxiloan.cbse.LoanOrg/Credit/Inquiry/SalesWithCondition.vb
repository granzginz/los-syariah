

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class SalesWithCondition : Inherits ComponentBase
    Implements ISalesWithCondition

    Public Function ListSalesWithCondition(ByVal customclass As Parameter.SalesWithCondition) As Parameter.SalesWithCondition Implements [Interface].ISalesWithCondition.ListSalesWithCondition
        Dim DASales As New SQLEngine.LoanOrg.SalesWithCondition
        Return DASales.ListSalesWithCOndition(customclass)
    End Function


    Public Function ListTboInquiry(cnn As String, currentPage As Integer, PageSize As Integer, branchid As String, where As String) As Parameter.SalesWithCondition Implements [Interface].ISalesWithCondition.ListTboInquiry
        Dim DASales As New SQLEngine.LoanOrg.SalesWithCondition
        Return DASales.ListTboInquiry(cnn, currentPage, PageSize, branchid, where)
    End Function
End Class
