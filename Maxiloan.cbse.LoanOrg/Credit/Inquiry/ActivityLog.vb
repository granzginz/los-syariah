

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class ActivityLog : Inherits ComponentBase
    Implements IActivityLog
    Public Function ListActivityUser(ByVal customclass As Parameter.ActivityLog) As Parameter.ActivityLog Implements [Interface].IActivityLog.ListActivityUser
        Dim DAActivityLog As New SQLEngine.LoanOrg.ActivityLog
        Return DAActivityLog.ListActivityUser(customclass)
    End Function

    Public Function ListActivity(ByVal customclass As Parameter.ActivityLog) As Parameter.ActivityLog Implements [Interface].IActivityLog.ListActivity
        Dim DAActivityLog As New SQLEngine.LoanOrg.ActivityLog
        Return DAActivityLog.ListActivity(customclass)
    End Function

#Region "AgreementCancel"
    Public Function AgreementCancelRpt(ByVal customclass As Parameter.ActivityLog) As Parameter.ActivityLog Implements [Interface].IActivityLog.AgreementCancelRpt
        Dim DAActivityLog As New SQLEngine.LoanOrg.ActivityLog
        Return DAActivityLog.AgreementCancelRpt(customclass)
    End Function
#End Region
End Class
