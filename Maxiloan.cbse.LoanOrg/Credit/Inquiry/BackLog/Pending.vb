
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class Pending
    Implements IPending
    Function GetPendingDataEntry(ByVal customClass As Parameter.Pending) As Parameter.Pending Implements [Interface].IPending.GetPendingDataEntry
        Try
            Dim PendingDA As New SQLEngine.LoanOrg.Pending
            Return PendingDA.GetPendingDataEntry(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetAO(ByVal customclass As Parameter.Pending) As DataTable Implements [Interface].IPending.GetAO
        Try
            Dim PendingDA As New SQLEngine.LoanOrg.Pending
            Return PendingDA.GetAO(customclass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetSupplier(ByVal customclass As Parameter.Pending) As DataTable Implements [Interface].IPending.GetSupplier
        Try
            Dim PendingDA As New SQLEngine.LoanOrg.Pending
            Return PendingDA.GetSupplier(customclass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetApproved(ByVal customclass As Parameter.Pending) As DataTable Implements [Interface].IPending.GetApproved
        Try
            Dim PendingDA As New SQLEngine.LoanOrg.Pending
            Return PendingDA.GetApproved(customclass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetApplicationPendingInquiry(ByVal customClass As Parameter.Pending) As Parameter.Pending Implements [Interface].IPending.GetApplicationPendingInquiry
        Dim PendingDA As New SQLEngine.LoanOrg.Pending
        Return PendingDA.GetApplicationPendingInquiry(customClass)
    End Function
End Class
