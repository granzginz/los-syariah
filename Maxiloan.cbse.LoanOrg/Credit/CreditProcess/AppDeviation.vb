﻿#Region "Imports"
Imports Maxiloan.Interface
#End Region


Public Class AppDeviation : Inherits ComponentBase
    Implements IAppDeviation


    Dim oSQLE As New SQLEngine.LoanOrg.AppDeviation

    Public Sub AppDeviationPrint(ByVal oCustomClass As Parameter.AppDeviation) Implements [Interface].IAppDeviation.AppDeviationPrint
        oSQLE.AppDeviationPrint(oCustomClass)
    End Sub

    Public Function GetAppDeviation(ByVal oCustomClass As Parameter.AppDeviation) As Parameter.AppDeviation Implements [Interface].IAppDeviation.GetAppDeviation
        Return oSQLE.GetAppDeviation(oCustomClass)
    End Function

    Public Function GetDeviationView(ByVal oCustomClass As Parameter.AppDeviation) As Parameter.AppDeviation Implements [Interface].IAppDeviation.GetDeviationView
        Return oSQLE.GetDeviationView(oCustomClass)
    End Function

    Public Function GetApprovalPath(ByVal oCustomClass As Parameter.ApprovalScheme) As Parameter.ApprovalScheme Implements [Interface].IAppDeviation.GetApprovalPath
        Return oSQLE.GetApprovalPath(oCustomClass)
    End Function

    Public Function GetApprovalPathTreeMember(ByVal oCustomClass As Parameter.ApprovalScheme) As Parameter.ApprovalScheme Implements [Interface].IAppDeviation.GetApprovalPathTreeMember
        Return oSQLE.GetApprovalPathTreeMember(oCustomClass)
    End Function
End Class
