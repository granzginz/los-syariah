﻿Imports Maxiloan.Interface

Public Class KonfirmasiDealer
    Implements IKonfirmasiDealer



    Public Function KonfirmasiDealerPaging(ByVal customClass As Parameter.KonfirmasiDealer) As Parameter.KonfirmasiDealer Implements [Interface].IKonfirmasiDealer.KonfirmasiDealerPaging
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.KonfirmasiDealer
        Return oSQLE.KonfirmasiDealerPaging(customClass)
    End Function

    Public Sub KonfirmasiDealerSave(ByVal customClass As Parameter.KonfirmasiDealer) Implements [Interface].IKonfirmasiDealer.KonfirmasiDealerSave
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.KonfirmasiDealer
        oSQLE.KonfirmasiDealerSave(customClass)
    End Sub

    Public Function ValidationSerialNo(customClass As Parameter.KonfirmasiDealer) As System.Data.DataTable Implements [Interface].IKonfirmasiDealer.ValidationSerialNo
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.KonfirmasiDealer
        Return oSQLE.ValidationSerialNo(customClass)
    End Function
End Class
