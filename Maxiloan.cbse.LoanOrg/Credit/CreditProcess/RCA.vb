
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region
Public Class RCA
    Implements IRCA
    Function GetRCA(ByVal customClass As Parameter.RCA) As Parameter.RCA Implements [Interface].IRCA.GetRCA
        Try
            Dim RCADA As New SQLEngine.LoanOrg.RCA
            Return RCADA.GetRCA(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
        Return Nothing
    End Function


    Function GetApprovalBM(ByVal customClass As Parameter.RCA) As Parameter.RCA Implements [Interface].IRCA.GetApprovalBM

        Dim RCADA As New SQLEngine.LoanOrg.RCA
        Return RCADA.GetApprovalBM(customClass)

    End Function

    Function RCASave(ByVal oRCA As Parameter.RCA, ByVal oData1 As DataTable, _
   ByVal oData2 As DataTable) As Parameter.RCA Implements [Interface].IRCA.RCASave
        Dim RCADA As New SQLEngine.LoanOrg.RCA
        Return RCADA.RCASave(oRCA, oData1, oData2)
    End Function


    Function ApprovalBMSave(ByVal oRCA As Parameter.RCA, Optional isApr As Boolean = True) As Parameter.RCA Implements [Interface].IRCA.ApprovalBMSave
        Dim RCADA As New SQLEngine.LoanOrg.RCA
        Return RCADA.ApprovalBMSave(oRCA, isApr)
    End Function


    Function GetCreditAssesmentList(ByVal oRCA As Parameter.RCA) As Parameter.RCA Implements [Interface].IRCA.GetCreditAssesmentList
        Dim RCADA As New SQLEngine.LoanOrg.RCA
        Return RCADA.GetCreditAssesmentList(oRCA)
    End Function
End Class
