
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.SQLEngine.LoanOrg
#End Region

Public Class EditApplication
    Implements IEditApplication

    Function GetApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IEditApplication.GetApplication
        Dim ApplicationDA As New SQLEngine.LoanOrg.EditApplication
        Return ApplicationDA.GetApplication(oCustomClass)
    End Function
    Function GetCountAppliationId(ByVal oCustomClass As Parameter.Application) As Integer Implements [Interface].IEditApplication.GetCountAppliationId
        Dim CountAppliationId As New SQLEngine.LoanOrg.EditApplication
        Return CountAppliationId.GetCountAppliationId(oCustomClass)
    End Function
    Function GetFee(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IEditApplication.GetFee
        Dim ApplicationDA As New SQLEngine.LoanOrg.EditApplication
        Return ApplicationDA.GetFee(oCustomClass)
    End Function
    Function GetCopyFrom(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IEditApplication.GetCopyFrom
        Dim ApplicationDA As New SQLEngine.LoanOrg.EditApplication
        Return ApplicationDA.GetCopyFrom(oCustomClass)
    End Function
    Function GetTC(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IEditApplication.GetTC
        Dim ApplicationDA As New SQLEngine.LoanOrg.EditApplication
        Return ApplicationDA.GetTC(oCustomClass)
    End Function
    Function GetTC2(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IEditApplication.GetTC2
        Dim ApplicationDA As New SQLEngine.LoanOrg.EditApplication
        Return ApplicationDA.GetTC2(oCustomClass)
    End Function
    Function GetShowDataAgreement(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IEditApplication.GetShowDataAgreement
        Dim ApplicationDA As New SQLEngine.LoanOrg.EditApplication
        Return ApplicationDA.GetShowDataAgreement(oCustomClass)
    End Function
    Function ApplicationSaveAdd(ByVal oApplication As Parameter.Application, _
        ByVal oRefPersonal As Parameter.Personal, _
        ByVal oRefAddress As Parameter.Address, ByVal oMailingAddress As Parameter.Address, _
        ByVal oData1 As DataTable, ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.Application Implements [Interface].IEditApplication.ApplicationSaveAdd
        Dim ApplicationDA As New SQLEngine.LoanOrg.EditApplication
        Return ApplicationDA.ApplicationSaveAdd(oApplication, oRefPersonal, oRefAddress, oMailingAddress, _
            oData1, oData2, oData3)
    End Function
    Function GetAddress(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IEditApplication.GetAddress
        Dim ApplicationDA As New SQLEngine.LoanOrg.EditApplication
        Return ApplicationDA.GetAddress(oCustomClass)
    End Function
    Function GetViewApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IEditApplication.GetViewApplication
        Dim ApplicationDA As New SQLEngine.LoanOrg.EditApplication
        Return ApplicationDA.GetViewApplication(oCustomClass)
    End Function
    Function GetApplicationMaintenance(ByVal customClass As Parameter.Application) As Parameter.Application Implements [Interface].IEditApplication.GetApplicationMaintenance
        Dim ApplicationDA As New SQLEngine.LoanOrg.EditApplication
        Return ApplicationDA.GetApplicationMaintenance(customClass)
    End Function
    Function GetCD(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IEditApplication.GetCD
        Dim ApplicationDA As New SQLEngine.LoanOrg.EditApplication
        Return ApplicationDA.GetCD(oCustomClass)
    End Function
    Function GetApplicationEdit(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IEditApplication.GetApplicationEdit
        Dim ApplicationDA As New SQLEngine.LoanOrg.EditApplication
        Return ApplicationDA.GetApplicationEdit(oCustomClass)
    End Function
    Function ApplicationSaveEdit(ByVal oApplication As Parameter.Application, _
       ByVal oRefPersonal As Parameter.Personal, _
       ByVal oRefAddress As Parameter.Address, ByVal oMailingAddress As Parameter.Address, _
       ByVal oData1 As DataTable, ByVal oData2 As DataTable, ByVal oData3 As DataTable, _
       ByVal MyDataTable As DataTable) As Parameter.Application Implements [Interface].IEditApplication.ApplicationSaveEdit
        Dim ApplicationDA As New SQLEngine.LoanOrg.EditApplication
        Return ApplicationDA.ApplicationSaveEdit(oApplication, oRefPersonal, oRefAddress, oMailingAddress, _
            oData1, oData2, oData3, MyDataTable)
    End Function

    Public Function EditAssetDataGetInfoAssetData(ByVal oCustumClass As Parameter.Application) As Parameter.Application Implements [Interface].IEditApplication.EditAssetDataGetInfoAssetData
        Dim DA As New SQLEngine.LoanOrg.EditApplication
        Return DA.EditAssetDataGetInfoAssetData(oCustumClass)
    End Function
    Public Function AssetDataGetInfoAssetDataAwal(ByVal oCustumClass As Parameter.Application) As Parameter.Application Implements [Interface].IEditApplication.AssetDataGetInfoAssetDataAwal
        Dim DA As New SQLEngine.LoanOrg.EditApplication
        Return DA.AssetDataGetInfoAssetDataAwal(oCustumClass)
    End Function
End Class