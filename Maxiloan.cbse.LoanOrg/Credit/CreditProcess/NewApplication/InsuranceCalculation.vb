#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class InsuranceCalculation : Inherits ComponentBase
    Implements IInsuranceCalculation

#Region "ProcessSaveDataInsuranceApplicationHeaderAndDetail"

    Public Function ProcessSaveDataInsuranceApplicationHeaderAndDetail(ByVal oDataTableInsuranceAsset As DataTable, ByVal customClass As Parameter.InsuranceCalculation) As Parameter.InsuranceCalculation Implements IInsuranceCalculation.ProcessSaveDataInsuranceApplicationHeaderAndDetail
        Dim DA As New SQLEngine.LoanOrg.InsuranceCalculation
        DA.ProcessSaveDataInsuranceApplicationHeaderAndDetail(oDataTableInsuranceAsset, customClass)
    End Function

#End Region

    ' Ini Headernya
#Region "ProcessNewAppInsuranceByCompanySaveAddTemporary"
    Public Function ProcessNewAppInsuranceByCompanySaveAddTemporary(ByVal customClass As Parameter.InsuranceCalculation) As String Implements IInsuranceCalculation.ProcessNewAppInsuranceByCompanySaveAddTemporary
        Dim ProcessNewAppInsuranceByCompanyDetailSaveAddTemporaryDA As New SQLEngine.LoanOrg.InsuranceCalculation
        ProcessNewAppInsuranceByCompanyDetailSaveAddTemporaryDA.ProcessNewAppInsuranceByCompanySaveAddTemporary(customClass)
    End Function
#End Region

    ' Ini Detailnya 
#Region "ProcessNewAppInsuranceByCompanyDetailSaveAddTemporary"

    Public Function ProcessNewAppInsuranceByCompanyDetailSaveAddTemporary(ByVal customClass As Parameter.InsuranceCalculation) As String Implements IInsuranceCalculation.ProcessNewAppInsuranceByCompanyDetailSaveAddTemporary
        Dim ProcessNewAppInsuranceByCompanyDetailSaveAddTemporaryDA As New SQLEngine.LoanOrg.InsuranceCalculation
        ProcessNewAppInsuranceByCompanyDetailSaveAddTemporaryDA.ProcessNewAppInsuranceByCompanyDetailSaveAddTemporary(customClass)
    End Function

    Public Function ProcessAppInsuranceAgricultureDetailSave(ByVal customClass As Parameter.InsuranceCalculation) As String Implements IInsuranceCalculation.ProcessAppInsuranceAgricultureDetailSave
        Dim DA As New SQLEngine.LoanOrg.InsuranceCalculation
        DA.ProcessAppInsuranceAgricultureDetailSave(customClass)
    End Function


#End Region

    '  DisplayResultInsCalculationStep1
#Region "DisplayResultInsCalculationStep1"
    Public Function ProcessSaveInsuranceApplicationLastProcess(ByVal customClass As Parameter.InsuranceCalculationResult) As String Implements IInsuranceCalculation.ProcessSaveInsuranceApplicationLastProcess
        Dim DA As New SQLEngine.LoanOrg.InsuranceCalculation
        DA.ProcessSaveInsuranceApplicationLastProcess(customClass)
    End Function

    Public Function ProcessKoreksiAsuransi(ByVal customClass As Parameter.InsuranceCalculationResult) As String Implements [Interface].IInsuranceCalculation.ProcessKoreksiAsuransi
        Dim DA As New SQLEngine.LoanOrg.InsuranceCalculation
        DA.ProcessKoreksiAsuransi(customClass)
    End Function
#End Region

#Region "GetDateEntryInsuranceData"
    Public Function GetDateEntryInsuranceData(ByVal customClass As Parameter.InsuranceCalculation) As Parameter.InsuranceCalculation Implements [Interface].IInsuranceCalculation.GetDateEntryInsuranceData
        Dim DA As New SQLEngine.LoanOrg.InsuranceCalculation
        Return DA.GetDateEntryInsuranceData(customClass)
    End Function
#End Region

    Public Function DisplaySelectedPremiumOnGrid(ByVal CustomClass As Parameter.InsuranceCalculation) As Parameter.InsuranceCalculation Implements [Interface].IInsuranceCalculation.DisplaySelectedPremiumOnGrid
        Try
            Dim DA As New SQLEngine.LoanOrg.InsuranceCalculation
            Return DA.DisplaySelectedPremiumOnGrid(CustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function


End Class
