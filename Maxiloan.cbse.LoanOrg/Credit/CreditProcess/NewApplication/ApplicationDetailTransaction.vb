﻿Imports Maxiloan.Interface

Public Class ApplicationDetailTransaction
    Implements IApplicationDetailTransaction



    Public Function getApplicationDetailTransaction(ByVal oCustomClass As Parameter.ApplicationDetailTransaction) As Parameter.ApplicationDetailTransaction Implements [Interface].IApplicationDetailTransaction.getApplicationDetailTransaction
        Dim oSQLE As New SQLEngine.LoanOrg.ApplicationDetailTransaction
        Return oSQLE.getApplicationDetailTransaction(oCustomClass)
    End Function

    Public Sub ApplicationDetailTransactionSave(ByVal oCustomClass As Parameter.ApplicationDetailTransaction) Implements [Interface].IApplicationDetailTransaction.ApplicationDetailTransactionSave
        Dim oSQLE As New SQLEngine.LoanOrg.ApplicationDetailTransaction
        oSQLE.ApplicationDetailTransactionSave(oCustomClass)
    End Sub

    Public Function getBungaNett(ByVal oCustomClass As Parameter.ApplicationDetailTransaction) As Parameter.ApplicationDetailTransaction Implements [Interface].IApplicationDetailTransaction.getBungaNett
        Dim oSQLE As New SQLEngine.LoanOrg.ApplicationDetailTransaction
        Return oSQLE.getBungaNett(oCustomClass)
    End Function
    Public Function getBungaNettKPR(ByVal oCustomClass As Parameter.ApplicationDetailTransaction) As Parameter.ApplicationDetailTransaction Implements [Interface].IApplicationDetailTransaction.getBungaNettKPR
        Dim oSQLE As New SQLEngine.LoanOrg.ApplicationDetailTransaction
        Return oSQLE.getBungaNettKPR(oCustomClass)
    End Function
    Public Function getBungaNettOperatingLease(ByVal oCustomClass As Parameter.ApplicationDetailTransaction) As Parameter.ApplicationDetailTransaction Implements [Interface].IApplicationDetailTransaction.getBungaNettOperatingLease
        Dim oSQLE As New SQLEngine.LoanOrg.ApplicationDetailTransaction
        Return oSQLE.getBungaNettOperatingLease(oCustomClass)
    End Function

End Class
