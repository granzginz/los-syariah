

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region


Public Class NewAppInsuranceByCust : Inherits ComponentBase
    Implements INewAppInsuranceByCust


    Public Function GetInsuredByCustomer(ByVal customClass As Parameter.NewAppInsuranceByCust) As Parameter.NewAppInsuranceByCust Implements [Interface].INewAppInsuranceByCust.GetInsuredByCustomer
        Try
            Dim DA As New SQLEngine.LoanOrg.NewAppInsuranceByCust
            Return DA.GetInsuredByCustomer(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try

    End Function
    Public Function ProcessSaveInsuranceByCustomer(ByVal customClass As Parameter.NewAppInsuranceByCust) As String Implements INewAppInsuranceByCust.ProcessSaveInsuranceByCustomer
        Try
            Dim Da As New SQLEngine.LoanOrg.NewAppInsuranceByCust
            Da.ProcessSaveInsuranceByCustomer(customClass)

        Catch ex As Exception

        End Try
    End Function

End Class
