﻿
Imports Maxiloan.Interface
Public Class CreditAssesment
    Implements ICreditAssesment

    Public Function getCreditAssesment(ByVal oCustomClass As Parameter.CreditAssesment) As Parameter.CreditAssesment Implements [Interface].ICreditAssesment.getCreditAssesment
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.CreditAssesment
        Return oSQLE.getCreditAssesment(oCustomClass)
    End Function

    Public Sub CreditAssesmentSave(ByVal oCustomClass As Parameter.CreditAssesment) Implements [Interface].ICreditAssesment.CreditAssesmentSave
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.CreditAssesment
        oSQLE.CreditAssesmentSave(oCustomClass)
    End Sub
    Public Function Proceed(oCustomClass As Parameter.CreditAssesment) As String Implements [Interface].ICreditAssesment.Proceed
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.CreditAssesment
        Return oSQLE.Proceed(oCustomClass)
    End Function
End Class
