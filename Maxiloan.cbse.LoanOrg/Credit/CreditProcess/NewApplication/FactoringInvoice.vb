﻿Imports Maxiloan.Interface
Imports Maxiloan.SQLEngine

Public Class FactoringInvoice : Inherits ComponentBase
    Implements IFactoringInvoice

    Public Function GetInvoiceList(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IFactoringInvoice.GetInvoiceList
        Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
        Return oSQLE.GetInvoiceList(oCustomClass)
    End Function

    Public Function GetInvoiceDetail(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IFactoringInvoice.GetInvoiceDetail
        Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
        Return oSQLE.GetInvoiceDetail(ocustomClass)
    End Function

    Public Function GetInvoiceDetail2(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IFactoringInvoice.GetInvoiceDetail2
        Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
        Return oSQLE.GetInvoiceDetail2(ocustomClass)
    End Function



    Public Function GetPPH(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IFactoringInvoice.GetPPH
        Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
        Return oSQLE.GetPPH(ocustomClass)
    End Function

    Public Function InvoiceSaveAdd(ByVal ocustomClass As Parameter.FactoringInvoice) As String Implements [Interface].IFactoringInvoice.InvoiceSaveAdd
        Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
        Return oSQLE.InvoiceSaveAdd(ocustomClass)
    End Function

    Public Function InvoiceSaveEdit(ByVal ocustomClass As Parameter.FactoringInvoice) As String Implements [Interface].IFactoringInvoice.InvoiceSaveEdit
        Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
        Return oSQLE.InvoiceSaveEdit(ocustomClass)
    End Function

    Public Function InvoiceDelete(ByVal ocustomClass As Parameter.FactoringInvoice) As String Implements [Interface].IFactoringInvoice.InvoiceDelete
        Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
        Return oSQLE.InvoiceDelete(ocustomClass)
    End Function

    Public Function InvoiceSaveApplication(ByVal ocustomClass As Parameter.Application) As String Implements [Interface].IFactoringInvoice.InvoiceSaveApplication
        Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
        Return oSQLE.InvoiceSaveApplication(ocustomClass)
    End Function

    Public Function GetFactoringFee(ByVal ocustomClass As Parameter.Application) As Parameter.FactoringInvoice Implements [Interface].IFactoringInvoice.GetFactoringFee
        Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
        Return oSQLE.GetFactoringFee(ocustomClass)
    End Function

    Public Function FactoringApplicationFinancialSave(ByVal ocustomClass As Parameter.Application) As String Implements [Interface].IFactoringInvoice.FactoringApplicationFinancialSave
        Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
        Return oSQLE.FactoringApplicationFinancialSave(ocustomClass)
    End Function

    Public Function FactoringInvoiceDisburse(ByVal ocustomClass As Parameter.FactoringInvoice) As String Implements [Interface].IFactoringInvoice.FactoringInvoiceDisburse
        Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
        Return oSQLE.FactoringInvoiceDisburse(ocustomClass)
    End Function

    Public Function RestruckFactoringPaging(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IFactoringInvoice.RestruckFactoringPaging
        Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
        Return oSQLE.RestruckFactoringPaging(oCustomClass)
    End Function
    Public Function GetRestructFactoringData(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IFactoringInvoice.GetRestructFactoringData
        Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
        Return oSQLE.GetRestructFactoringData(ocustomClass)
    End Function

    Public Function RestructFactoringSave(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IFactoringInvoice.RestructFactoringSave
        Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
        Return oSQLE.RestructFactoringSave(ocustomClass)
    End Function
    Public Function GetInvoiceListPaid(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IFactoringInvoice.GetInvoiceListPaid
        Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
        Return oSQLE.GetInvoiceListPaid(oCustomClass)
    End Function
    Public Function GetInvoiceChangeDueDateList(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IFactoringInvoice.GetInvoiceChangeDueDateList
        Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
        Return oSQLE.GetInvoiceChangeDueDateList(oCustomClass)
    End Function
    'Public Function GetFinancialData_002(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IFactoringInvoice.GetFinancialData_002
    '    Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
    '    Return oSQLE.GetFinancialData_002(oCustomClass)
    'End Function
    'Public Function GetTerm(ByVal intInstTerm As Integer) As Integer Implements [Interface].IFactoringInvoice.GetTerm
    '    Select Case intInstTerm
    '        Case 1
    '            Return 12
    '        Case 2
    '            Return 3
    '        Case 3
    '            Return 2
    '        Case 6
    '            Return 1
    '        Case Else
    '            Return 0
    '    End Select
    'End Function
    'Public Function SaveFinancialDataTab2End(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IFactoringInvoice.SaveFinancialDataTab2End
    '    Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
    '    Return oSQLE.SaveFinancialDataTab2End(oCustomClass)
    'End Function
    'Public Function GetRefundPremiumToSupplier(ByVal data As Parameter.Supplier) As Parameter.Supplier Implements [Interface].IFactoringInvoice.GetRefundPremiumToSupplier
    '    Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
    '    Return oSQLE.GetRefundPremiumToSupplier(data)
    'End Function
    'Public Function saveBungaNett(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IFactoringInvoice.saveBungaNett
    '    Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
    '    Return oSQLE.saveBungaNett(oCustomClass)
    'End Function
    'Public Function SaveFinancialDataTab2(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IFactoringInvoice.SaveFinancialDataTab2
    '    Dim oSQLE As New SQLEngine.LoanOrg.FactoringInvoice
    '    Return oSQLE.SaveFinancialDataTab2(oCustomClass)
    'End Function
End Class
