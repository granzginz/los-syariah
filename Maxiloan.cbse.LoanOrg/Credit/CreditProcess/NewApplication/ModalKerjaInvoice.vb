﻿Imports Maxiloan.Interface
Imports Maxiloan.SQLEngine

Public Class ModalKerjaInvoice : Inherits ComponentBase
    Implements IModalKerjaInvoice

    Public Function GetInvoiceList(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice Implements [Interface].IModalKerjaInvoice.GetInvoiceList
        Dim oSQLE As New SQLEngine.LoanOrg.ModalKerjaInvoice
        Return oSQLE.GetInvoiceList(oCustomClass)
    End Function
    Public Function GetInvoiceList2(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice Implements [Interface].IModalKerjaInvoice.GetInvoiceList2
        Dim oSQLE As New SQLEngine.LoanOrg.ModalKerjaInvoice
        Return oSQLE.GetInvoiceList2(oCustomClass)
    End Function
    Public Function GetJatuhTempoList(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice Implements [Interface].IModalKerjaInvoice.GetJatuhTempoList
        Dim oSQLE As New SQLEngine.LoanOrg.ModalKerjaInvoice
        Return oSQLE.GetJatuhTempoList(oCustomClass)
    End Function
    Public Function GetInvoiceDetail(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice Implements [Interface].IModalKerjaInvoice.GetInvoiceDetail
        Dim oSQLE As New SQLEngine.LoanOrg.ModalKerjaInvoice
        Return oSQLE.GetInvoiceDetail(ocustomClass)
    End Function

    Public Function InvoiceSaveAdd(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String Implements [Interface].IModalKerjaInvoice.InvoiceSaveAdd
        Dim oSQLE As New SQLEngine.LoanOrg.ModalKerjaInvoice
        Return oSQLE.InvoiceSaveAdd(ocustomClass)
    End Function

    Public Function InvoiceSaveEdit(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String Implements [Interface].IModalKerjaInvoice.InvoiceSaveEdit
        Dim oSQLE As New SQLEngine.LoanOrg.ModalKerjaInvoice
        Return oSQLE.InvoiceSaveEdit(ocustomClass)
    End Function

    Public Function InvoiceSaveAdd2(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String Implements [Interface].IModalKerjaInvoice.InvoiceSaveAdd2
        Dim oSQLE As New SQLEngine.LoanOrg.ModalKerjaInvoice
        Return oSQLE.InvoiceSaveAdd2(ocustomClass)
    End Function

    Public Function InvoiceSaveEdit2(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String Implements [Interface].IModalKerjaInvoice.InvoiceSaveEdit2
        Dim oSQLE As New SQLEngine.LoanOrg.ModalKerjaInvoice
        Return oSQLE.InvoiceSaveEdit2(ocustomClass)
    End Function

    Public Function InvoiceDelete(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String Implements [Interface].IModalKerjaInvoice.InvoiceDelete
        Dim oSQLE As New SQLEngine.LoanOrg.ModalKerjaInvoice
        Return oSQLE.InvoiceDelete(ocustomClass)
    End Function

    Public Function InvoiceSaveApplication(ByVal ocustomClass As Parameter.Application, ByVal oDataAmortisasi As DataTable) As String Implements [Interface].IModalKerjaInvoice.InvoiceSaveApplication
        Dim oSQLE As New SQLEngine.LoanOrg.ModalKerjaInvoice
        Return oSQLE.InvoiceSaveApplication(ocustomClass, oDataAmortisasi)
    End Function

    Public Function GetFactoringFee(ByVal ocustomClass As Parameter.Application) As Parameter.ModalKerjaInvoice Implements [Interface].IModalKerjaInvoice.GetFactoringFee
        Dim oSQLE As New SQLEngine.LoanOrg.ModalKerjaInvoice
        Return oSQLE.GetFactoringFee(ocustomClass)
    End Function

    Public Function FactoringApplicationFinancialSave(ByVal ocustomClass As Parameter.Application) As String Implements [Interface].IModalKerjaInvoice.FactoringApplicationFinancialSave
        Dim oSQLE As New SQLEngine.LoanOrg.ModalKerjaInvoice
        Return oSQLE.FactoringApplicationFinancialSave(ocustomClass)
    End Function

    Public Function ModalKerjaInvoiceDisburse(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String Implements [Interface].IModalKerjaInvoice.ModalKerjaInvoiceDisburse
        Dim oSQLE As New SQLEngine.LoanOrg.ModalKerjaInvoice
        Return oSQLE.ModalKerjaInvoiceDisburse(ocustomClass)
    End Function

    Public Function GetDataByLastPaymentDate(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice Implements [Interface].IModalKerjaInvoice.GetDataByLastPaymentDate
        Dim oSQLE As New SQLEngine.LoanOrg.ModalKerjaInvoice
        Return oSQLE.GetDataByLastPaymentDate(oCustomClass)
    End Function

    Public Function GetPaymentHistory(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice Implements [Interface].IModalKerjaInvoice.GetPaymentHistory
        Dim oSQLE As New SQLEngine.LoanOrg.ModalKerjaInvoice
        Return oSQLE.GetPaymentHistory(oCustomClass)
    End Function

    Public Function GetInvoiceDetail2(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice Implements [Interface].IModalKerjaInvoice.GetInvoiceDetail2
        Dim oSQLE As New SQLEngine.LoanOrg.ModalKerjaInvoice
        Return oSQLE.GetInvoiceDetail2(ocustomClass)
    End Function
End Class
