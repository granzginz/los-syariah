
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region
Public Class AssetData
    Implements IAssetData
    Function GetAssetData(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.GetAssetData
        Try
            Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
            Return AssetDataDA.GetAssetData(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetCboEmp(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.GetCboEmp
        Try
            Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
            Return AssetDataDA.GetCboEmp(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetCboEmpReport(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.GetCboEmpReport
        Try
            Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
            Return AssetDataDA.GetCboEmpReport(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetCbo(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.GetCbo
        Try
            Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
            Return AssetDataDA.GetCbo(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetAttribute(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.GetAttribute
        Try
            Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
            Return AssetDataDA.GetAttribute(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetSerial(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.GetSerial
        Try
            Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
            Return AssetDataDA.GetSerial(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetAO(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.GetAO
        Try
            Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
            Return AssetDataDA.GetAO(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetSupplierGroupID(ByVal oCustomClass As Parameter.AssetData) As String Implements [Interface].IAssetData.GetSupplierGroupID
        Try
            Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
            Return AssetDataDA.GetSupplierGroupID(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function CheckSerial(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.CheckSerial
        Try
            Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
            Return AssetDataDA.CheckSerial(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function CheckAttribute(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.CheckAttribute
        Try
            Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
            Return AssetDataDA.CheckAttribute(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetAssetDoc(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.GetAssetDoc
        Try
            Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
            Return AssetDataDA.GetAssetDoc(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function CheckAssetDoc(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.CheckAssetDoc
        Try
            Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
            Return AssetDataDA.CheckAssetDoc(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function AssetDataSaveAdd(ByVal oAssetData As Parameter.AssetData, _
       ByVal oAddress As Parameter.Address, ByVal oData1 As DataTable, _
       ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.AssetData Implements [Interface].IAssetData.AssetDataSaveAdd
        Try
            Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
            Return AssetDataDA.AssetDataSaveAdd(oAssetData, oAddress, oData1, oData2, oData3)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetAssetID(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.GetAssetID
        Try
            Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
            Return AssetDataDA.GetAssetID(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetViewAssetData(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.GetViewAssetData
        Try
            Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
            Return AssetDataDA.GetViewAssetData(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetUsedNew(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.GetUsedNew
        Dim DA As New SQLEngine.LoanOrg.AssetData
        Return DA.GetUsedNew(oCustomClass)

    End Function

    Public Function EditAssetDataGetDefaultSupplierID(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.EditAssetDataGetDefaultSupplierID
        Dim DA As New SQLEngine.LoanOrg.AssetData
        Return DA.EditAssetDataGetDefaultSupplierID(oCustomClass)
    End Function

    Public Function EditGetAttribute(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.EditGetAttribute
        Dim DA As New SQLEngine.LoanOrg.AssetData
        Return DA.EditGetAttribute(oCustomClass)
    End Function

    Public Function EditGetAssetRegistration(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.EditGetAssetRegistration
        Dim DA As New SQLEngine.LoanOrg.AssetData
        Return DA.EditGetAssetRegistration(oCustomClass)
    End Function

    Public Function EditAssetInsuranceEmployee(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.EditAssetInsuranceEmployee
        Dim DA As New SQLEngine.LoanOrg.AssetData
        Return DA.EditAssetInsuranceEmployee(oCustomClass)

    End Function

    Public Function EditAssetDataDocument(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.EditAssetDataDocument
        Dim DA As New SQLEngine.LoanOrg.AssetData
        Return DA.EditAssetDataDocument(oCustomClass)
    End Function
    Function GetcboAlasanPenolakan(ByVal oCustomClass As Parameter.TolakanNonCust) As Parameter.TolakanNonCust Implements [Interface].IAssetData.GetcboAlasanPenolakan
        Try
            Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
            Return AssetDataDA.GetcboAlasanPenolakan(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function TolakanNonCustSaveAdd(ByVal oCustomClass As Parameter.TolakanNonCust, _
                                          ByVal oAddress As Parameter.Address) As Parameter.TolakanNonCust Implements [Interface].IAssetData.TolakanNonCustSaveAdd
        Dim DA As New SQLEngine.LoanOrg.AssetData
        Return DA.TolakanNonCustSaveAdd(oCustomClass, oAddress)
    End Function
  

    Public Function GetTolakanNonCustPaging(ByVal oCustomClass As Parameter.TolakanNonCust) As Parameter.TolakanNonCust Implements [Interface].IAssetData.GetTolakanNonCustPaging
        Dim DA As New SQLEngine.LoanOrg.AssetData
        Return DA.GetTolakanNonCustPaging(oCustomClass)
    End Function
    Public Function GetTolakanNonCustEdit(ByVal oCustomClass As Parameter.TolakanNonCust) As Parameter.TolakanNonCust Implements [Interface].IAssetData.GetTolakanNonCustEdit
        Dim DA As New SQLEngine.LoanOrg.AssetData
        Return DA.GetTolakanNonCustEdit(oCustomClass)
    End Function
    Public Function GetTolakanNonCustDelete(ByVal oCustomClass As Parameter.TolakanNonCust) As String Implements [Interface].IAssetData.GetTolakanNonCustDelete
        Dim DA As New SQLEngine.LoanOrg.AssetData
        Return DA.GetTolakanNonCustDelete(oCustomClass)
    End Function
    Public Function ListReportTolakanNonCust(ByVal oCustomClass As Parameter.TolakanNonCust) As Parameter.TolakanNonCust Implements _
        [Interface].IAssetData.ListReportTolakanNonCust
        Dim DA As New SQLEngine.LoanOrg.AssetData
        Return DA.ListReportTolakanNonCust(oCustomClass)
    End Function

    Public Function GetGradeAsset(oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.GetGradeAsset
        Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
        Return AssetDataDA.GetGradeAsset(oCustomClass)
    End Function

    Function GetAssetDocWhere(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData Implements [Interface].IAssetData.GetAssetDocWhere
        Try
            Dim AssetDataDA As New SQLEngine.LoanOrg.AssetData
            Return AssetDataDA.GetAssetDocWhere(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
End Class
