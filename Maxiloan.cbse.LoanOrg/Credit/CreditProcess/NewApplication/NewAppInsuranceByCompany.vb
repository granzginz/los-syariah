

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class NewAppInsuranceByCompany : Inherits ComponentBase
    Implements INewAppInsuranceByCompany



#Region "CheckProspect"
    Public Function CheckProspect(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany Implements INewAppInsuranceByCompany.CheckProspect
        Dim CheckProspectDA As New SQLEngine.LoanOrg.NewAppInsuranceByCompany
        Return CheckProspectDA.CheckProspect(CustomClass)
    End Function
#End Region
#Region "HandleDropDownOnDataBound"

    Public Function HandleDropDownOnDataBound(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As DataTable Implements INewAppInsuranceByCompany.HandleDropDownOnDataBound
        Dim HandleDropDownOnDataBoundDA As New SQLEngine.LoanOrg.NewAppInsuranceByCompany
        Return HandleDropDownOnDataBoundDA.HandleDropDownOnDataBound(CustomClass)
    End Function

#End Region
#Region "FillInsuranceComBranch"
    Public Function FillInsuranceComBranch(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As DataTable Implements INewAppInsuranceByCompany.FillInsuranceComBranch
        Dim FillInsuranceComBranchDA As New SQLEngine.LoanOrg.NewAppInsuranceByCompany
        Return FillInsuranceComBranchDA.FillInsuranceComBranch(CustomClass)
    End Function
#End Region
#Region "GetInsuranceEntryStep1List"
    Public Function GetInsuranceEntryStep1List(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany Implements [Interface].INewAppInsuranceByCompany.GetInsuranceEntryStep1List
        Dim GetInsuranceEntryStep1ListDA As New SQLEngine.LoanOrg.NewAppInsuranceByCompany
        Return GetInsuranceEntryStep1ListDA.GetInsuranceEntryStep1List(CustomClass)
    End Function
#End Region

    Public Function GetInsuranceEntryFactoring(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany Implements [Interface].INewAppInsuranceByCompany.GetInsuranceEntryFactoring
        Dim GetInsuranceEntryStep1ListDA As New SQLEngine.LoanOrg.NewAppInsuranceByCompany
        Return GetInsuranceEntryStep1ListDA.GetInsuranceEntryFactoring(CustomClass)
    End Function


#Region "GetInsuranceEntryStep2List"


    Public Function GetInsuranceEntryStep2List(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany Implements [Interface].INewAppInsuranceByCompany.GetInsuranceEntryStep2List
        Dim GetInsuranceEntryStep1ListDA As New SQLEngine.LoanOrg.NewAppInsuranceByCompany
        Return GetInsuranceEntryStep1ListDA.GetInsuranceEntryStep2List(CustomClass)
    End Function

#End Region

#Region "EditGetInsuranceEntryStep2List"
    Public Function EditGetInsuranceEntryStep2List(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany Implements [Interface].INewAppInsuranceByCompany.EditGetInsuranceEntryStep2List
        Dim DA As New SQLEngine.LoanOrg.NewAppInsuranceByCompany
        Return DA.EditGetInsuranceEntryStep2List(CustomClass)
    End Function
#End Region

#Region "EditGetInsuranceEntryStep1List"
    Public Function EditGetInsuranceEntryStep1List(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany Implements [Interface].INewAppInsuranceByCompany.EditGetInsuranceEntryStep1List
        Dim DA As New SQLEngine.LoanOrg.NewAppInsuranceByCompany
        Return DA.EditGetInsuranceEntryStep1List(CustomClass)
    End Function
#End Region

    Public Function EditInsuranceSelect(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany Implements [Interface].INewAppInsuranceByCompany.EditInsuranceSelect
        Dim DA As New SQLEngine.LoanOrg.NewAppInsuranceByCompany
        Return DA.EditInsuranceSelect(CustomClass)
    End Function


    Public Function GetInsurancehComByBranch(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As DataTable Implements INewAppInsuranceByCompany.GetInsurancehComByBranch
        Dim DA As New SQLEngine.LoanOrg.NewAppInsuranceByCompany
        Return DA.GetInsurancehComByBranch(CustomClass)
    End Function


    Public Function GetAssetCategory(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As DataTable Implements INewAppInsuranceByCompany.GetAssetCategory
        Dim DA As New SQLEngine.LoanOrg.NewAppInsuranceByCompany
        Return DA.GetAssetCategory(CustomClass)
    End Function

    Public Function GetInsuranceEntryStep1ListAgriculture(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany Implements [Interface].INewAppInsuranceByCompany.GetInsuranceEntryStep1ListAgriculture
        Dim DA As New SQLEngine.LoanOrg.NewAppInsuranceByCompany
        Return DA.GetInsuranceEntryStep1ListAgriculture(CustomClass)
    End Function

    Public Function GetInsuranceBranchNameRateCard(connString As String, applicationid As String, MaskAssBranchID As String, CoverageType As String, yearnum As Integer) As DataTable Implements [Interface].INewAppInsuranceByCompany.GetInsuranceBranchNameRateCard
        Dim DA As New SQLEngine.LoanOrg.NewAppInsuranceByCompany
        Return DA.GetInsuranceBranchNameRateCard(connString, applicationid, MaskAssBranchID, CoverageType, yearnum)
    End Function

    Public Function GetInsurancehComByBranchProduct(ByVal CustomClass As Parameter.InsCoBranch) As Parameter.InsCoBranch Implements INewAppInsuranceByCompany.GetInsurancehComByBranchProduct
        Dim DA As New SQLEngine.LoanOrg.NewAppInsuranceByCompany
        Return DA.GetInsurancehComByBranchProduct(CustomClass)
    End Function
    Public Function GetInsuranceComBranch(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany Implements [Interface].INewAppInsuranceByCompany.GetInsuranceComBranch
        Dim DA As New SQLEngine.LoanOrg.NewAppInsuranceByCompany
        Return DA.GetInsuranceComBranch(CustomClass)
    End Function

End Class
