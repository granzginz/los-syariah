
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class Application
    Implements IApplication


    Function GetDataProspectType(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetDataProspectType
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetDataProspectType(oCustomClass)
    End Function

    Function GetApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetApplication
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetApplication(oCustomClass)
    End Function
    Function GetApplicationFactoring(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetApplicationFactoring
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetApplicationFactoring(oCustomClass)
    End Function
    Function GetApplicationModalKerja(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetApplicationModalKerja
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetApplicationModalKerja(oCustomClass)
    End Function
    Function GetCountAppliationId(ByVal oCustomClass As Parameter.Application) As Integer Implements [Interface].IApplication.GetCountAppliationId
        Dim CountAppliationId As New SQLEngine.LoanOrg.Application
        Return CountAppliationId.GetCountAppliationId(oCustomClass)
    End Function
    Function GetFee(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetFee
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetFee(oCustomClass)
    End Function
    Function GetCopyFrom(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetCopyFrom
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetCopyFrom(oCustomClass)
    End Function
    Function GetTC(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetTC
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetTC(oCustomClass)
    End Function
    Function GetTC2(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetTC2
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetTC2(oCustomClass)
    End Function
    Function GetShowDataAgreement(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetShowDataAgreement
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetShowDataAgreement(oCustomClass)
    End Function
    Function ApplicationSaveAdd(ByVal oApplication As Parameter.Application,
                                ByVal oRefPersonal As Parameter.Personal,
                                ByVal oRefAddress As Parameter.Address,
                                ByVal oMailingAddress As Parameter.Address,
                                ByVal oData1 As DataTable,
                                ByVal oData2 As DataTable,
                                ByVal oData3 As DataTable,
                                ByVal oGuarantorPersonal As Parameter.Personal,
                                ByVal oGuarantorAddress As Parameter.Address) As Parameter.Application Implements [Interface].IApplication.ApplicationSaveAdd
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.ApplicationSaveAdd(oApplication,
                                                oRefPersonal,
                                                oRefAddress,
                                                oMailingAddress,
                                                oData1,
                                                oData2,
                                                oData3,
                                                oGuarantorPersonal,
                                                oGuarantorAddress)
    End Function
    Function GetAddress(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetAddress
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetAddress(oCustomClass)
    End Function
    Function GetViewApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetViewApplication
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetViewApplication(oCustomClass)
    End Function
    Function GetApplicationMaintenance(ByVal customClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetApplicationMaintenance
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetApplicationMaintenance(customClass)
    End Function
    Function GetCD(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetCD
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetCD(oCustomClass)
    End Function
    Function GetApplicationEdit(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetApplicationEdit
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetApplicationEdit(oCustomClass)
    End Function
    Function GetApplicationEditPPH(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetApplicationEditPPH
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetApplicationEditPPH(oCustomClass)
    End Function
    Function ApplicationSaveEdit(ByVal oApplication As Parameter.Application,
                                 ByVal oRefPersonal As Parameter.Personal,
                                 ByVal oRefAddress As Parameter.Address,
                                 ByVal oMailingAddress As Parameter.Address,
                                 ByVal oData1 As DataTable,
                                 ByVal oData2 As DataTable,
                                 ByVal oData3 As DataTable,
                                 ByVal oGuarantorPersonal As Parameter.Personal,
                                 ByVal oGuarantorAddress As Parameter.Address) As Parameter.Application Implements [Interface].IApplication.ApplicationSaveEdit
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.ApplicationSaveEdit(oApplication,
                                                 oRefPersonal,
                                                 oRefAddress,
                                                 oMailingAddress,
                                                 oData1,
                                                 oData2,
                                                 oData3,
                                                 oGuarantorPersonal,
                                                 oGuarantorAddress)
    End Function
    Function GetGoLive(ByVal customClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetGoLive
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetGoLive(customClass)
    End Function
    Function GetGoLiveBackDated(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetGoLiveBackDated
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetGoLiveBackDated(oCustomClass)
    End Function
    Function GoLiveSave(ByVal oGoLive As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GoLiveSave
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GoLiveSave(oGoLive)
    End Function
    Function GetShowDataProspect(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetShowDataProspect
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetShowDataProspect(oCustomClass)
    End Function
    Public Function GetDataAppIDProspect(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetDataAppIDProspect
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetDataAppIDProspect(oCustomClass)
    End Function
    Public Function GetCustomerProspect(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetCustomerProspect
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetCustomerProspect(oCustomClass)
    End Function

    Public Function ViewProspectApplication(ByVal oCustomClass As Parameter.Application) As System.Data.DataTable Implements [Interface].IApplication.ViewProspectApplication
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.ViewProspectApplication(oCustomClass)
    End Function

    Public Function GetViewMKKFasilitas(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetViewMKKFasilitas
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetViewMKKFasilitas(oCustomClass)
    End Function

    Public Function saveApplicationProductOffering(ByVal oApplication As Parameter.Application, ByVal oAssetData As Parameter.AssetData) As Parameter.Application Implements [Interface].IApplication.saveApplicationProductOffering
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.saveApplicationProductOffering(oApplication, oAssetData)
    End Function

    Public Function saveApplicationProductOfferingEdit(ByVal oApplication As Parameter.Application, ByVal oAssetData As Parameter.AssetData) As Parameter.Application Implements [Interface].IApplication.saveApplicationProductOfferingEdit
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.saveApplicationProductOfferingEdit(oApplication, oAssetData)
    End Function

    Public Function GetGoLiveCancelPaging(ByVal customClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetGoLiveCancelPaging
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetGoLiveCancelPaging(customClass)
    End Function

    Public Function GoLiveCancel(ByVal oGoLive As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GoLiveCancel
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GoLiveCancel(oGoLive)
    End Function


    Public Function GetSyaratCair(oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetSyaratCair
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetSyaratCair(oCustomClass)
    End Function

    Public Function DuplikasiApplication(oCustomClass As Parameter.Application) As String Implements [Interface].IApplication.DuplikasiApplication
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.DuplikasiApplication(oCustomClass)
    End Function

    Public Function GetSyaratCairPO(oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetSyaratCairPO
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetSyaratCairPO(oCustomClass)
    End Function

    Function ApplicationSaveEditNonFinancial(ByVal oApplication As Parameter.Application,
                                 ByVal oRefPersonal As Parameter.Personal,
                                 ByVal oRefAddress As Parameter.Address,
                                 ByVal oMailingAddress As Parameter.Address,
                                 ByVal oData1 As DataTable,
                                 ByVal oData2 As DataTable,
                                 ByVal oData3 As DataTable) As Parameter.Application Implements [Interface].IApplication.ApplicationSaveEditNonFinancial
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.ApplicationSaveEditNonFinancial(oApplication,
                                                 oRefPersonal,
                                                 oRefAddress,
                                                 oMailingAddress,
                                                 oData1,
                                                 oData2,
                                                 oData3)
    End Function

    Function GetApplicationHasilSurveydanKYC(ByVal customClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetApplicationHasilSurveydanKYC
        Dim ApplicationDA As New SQLEngine.LoanOrg.Application
        Return ApplicationDA.GetApplicationHasilSurveydanKYC(customClass)
    End Function

    Function ValidasiCollector(ByVal customClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.ValidasiCollector
        Dim DA As New SQLEngine.LoanOrg.Application
        Return DA.ValidasiCollector(customClass)
    End Function
    Function ModalKerjaApplicationFinancialSave(ByVal ocustomClass As Parameter.Application) As String Implements [Interface].IApplication.ModalKerjaApplicationFinancialSave
        Dim DA As New SQLEngine.LoanOrg.Application
        Return DA.ModalKerjaApplicationFinancialSave(ocustomClass)
    End Function

    Function GetGoLiveModalKerja(ByVal customClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetGoLiveModalKerja
        Dim DA As New SQLEngine.LoanOrg.Application
        Return DA.GetGoLiveModalKerja(customClass)
    End Function

    Function GoLiveSaveModalKerja(ByVal customClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GoLiveSaveModalKerja
        Dim DA As New SQLEngine.LoanOrg.Application
        Return DA.GoLiveSaveModalKerja(customClass)
    End Function

    Function GetGoLiveFactoring(ByVal customClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GetGoLiveFactoring
        Dim DA As New SQLEngine.LoanOrg.Application
        Return DA.GetGoLiveFactoring(customClass)
    End Function

    Function GoLiveSaveFactoring(ByVal customClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.GoLiveSaveFactoring
        Dim DA As New SQLEngine.LoanOrg.Application
        Return DA.GoLiveSaveFactoring(customClass)
    End Function
    Function ActivityLogSave(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.ActivityLogSave
        Dim DA As New SQLEngine.LoanOrg.Application
        Return DA.ActivityLogSave(oCustomClass)
    End Function

    Function DisburseLogSave(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.DisburseLogSave
        Dim DA As New SQLEngine.LoanOrg.Application
        Return DA.DisburseLogSave(oCustomClass)
    End Function
    Function getdataNPP(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].IApplication.getdataNPP
        Dim DA As New SQLEngine.LoanOrg.Application
        Return DA.getdataNPP(oCustomClass)
    End Function


    Function InstallmentDrawDownSave(ByVal oDrawdown As Parameter.Drawdown) As String Implements [Interface].IApplication.InstallmentDrawDownSave
        Dim DA As New SQLEngine.LoanOrg.Application
        Return DA.InstallmentDrawDownSave(oDrawdown)
    End Function

    Function InstallmentDrawDownCheckRequest(ByVal odrawdown As Parameter.Drawdown) As Boolean Implements [Interface].IApplication.InstallmentDrawDownCheckRequest
        Dim DA As New SQLEngine.LoanOrg.Application
        Return DA.InstallmentDrawDownCheckRequest(odrawdown)
    End Function

    Function InstallmentDrawDownList(ByVal odrawdown As Parameter.Drawdown) As Parameter.Drawdown Implements [Interface].IApplication.InstallmentDrawDownList
        Dim DA As New SQLEngine.LoanOrg.Application
        Return DA.InstallmentDrawDownList(odrawdown)
    End Function

    Public Function GetApplicationDeduction(customClass As Parameter.Application) As Parameter.Application Implements IApplication.GetApplicationDeduction
        Dim ApplicationDeduction As New SQLEngine.LoanOrg.Application
        Return ApplicationDeduction.GetApplicationDeduction(customClass)
    End Function

    Function GetCustomerFacility(ByVal oCustomClass As Parameter.Application) As Integer Implements [Interface].IApplication.GetCustomerFacility
        Dim CountAppliationId As New SQLEngine.LoanOrg.Application
        Return CountAppliationId.GetCustomerFacility(oCustomClass)
    End Function
    Function WayOfPaymentList(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements IApplication.WayOfPaymentList
        Dim CountAppliationId As New SQLEngine.LoanOrg.Application
        Return CountAppliationId.WayOfPaymentList(oCustomClass)
    End Function

End Class