

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class NewAppInsurance : Inherits ComponentBase
    Implements INewAppInsurance

    Public Function getNewAppInsuranceListing(ByVal oCustomClass As Parameter.NewAppInsurance) As Parameter.NewAppInsurance Implements [Interface].INewAppInsurance.getNewAppInsuranceListing
        Try
            Dim getNewAppInsuranceListingDA As New SQLEngine.LoanOrg.NewAppInsurance
            Return getNewAppInsuranceListingDA.getNewAppInsuranceListing(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function




    Public Function getCreditAssesment(ByVal oCustomClass As Parameter.CreditAssesment) As Parameter.CreditAssesment Implements [Interface].INewAppInsurance.getCreditAssesment
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.CreditAssesment
        Return oSQLE.getCreditAssesment(oCustomClass)
    End Function

    Public Sub CreditAssesmentSave(ByVal oCustomClass As Parameter.CreditAssesment) Implements [Interface].INewAppInsurance.CreditAssesmentSave
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.CreditAssesment
        oSQLE.CreditAssesmentSave(oCustomClass)
    End Sub
    Public Function Proceed(oCustomClass As Parameter.CreditAssesment) As String Implements [Interface].INewAppInsurance.Proceed
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.CreditAssesment
        Return oSQLE.Proceed(oCustomClass)
    End Function

    Public Function getApprovalSchemeID(cnn As String, appId As String) As String Implements [Interface].INewAppInsurance.getApprovalSchemeID
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.CreditAssesment
        Return oSQLE.getApprovalSchemeID(cnn, appId)
    End Function
End Class
