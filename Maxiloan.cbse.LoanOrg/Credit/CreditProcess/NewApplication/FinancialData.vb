
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.SQLEngine.LoanOrg
#End Region

Public Class FinancialData
    Implements IFinancialData



    'modify kpr
    Function SaveKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.SaveKPR
        Dim FinancialDataDA As New SQLEngine.LoanOrg.FinancialData
        Return FinancialDataDA.SaveKPR(oCustomClass)
    End Function
    Function getDataKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.getDataKPR
        Dim FinancialDataDA As New SQLEngine.LoanOrg.FinancialData
        Return FinancialDataDA.getDataKPR(oCustomClass)
    End Function
    Function getDataKPRHeader(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.getDataKPRHeader
        Dim FinancialDataDA As New SQLEngine.LoanOrg.FinancialData
        Return FinancialDataDA.getDataKPRHeader(oCustomClass)
    End Function

    Function HitungangsuranKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.HitungAngsuranKPR
        Dim FinancialDataDA As New SQLEngine.LoanOrg.FinancialData
        Return FinancialDataDA.HitungangsuranKPR(oCustomClass)
    End Function
    Function GetDataGridKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.GetDataGridKPR
        Dim FinancialDataDA As New SQLEngine.LoanOrg.FinancialData
        Return FinancialDataDA.GetDataGridKPR(oCustomClass)
    End Function
    Function DeleteAngsuranKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.DeleteAngsuranKPR
        Dim FinancialDataDA As New SQLEngine.LoanOrg.FinancialData
        Return FinancialDataDA.DeleteAngsuranKPR(oCustomClass)
    End Function
    Function GetDataDrop(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.GetDataDrop
        Dim FinancialDataDA As New SQLEngine.LoanOrg.FinancialData
        Return FinancialDataDA.GetDataDrop(oCustomClass)
    End Function
    Public Function SaveFinancialDataTab2EndKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.SaveFinancialDataTab2EndKPR
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        Return DA.SaveFinancialDataTab2EndKPR(oCustomClass)
    End Function
    'end







    Dim i As Integer

    Function GetFinancialData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.GetFinancialData
        Dim FinancialDataDA As New SQLEngine.LoanOrg.FinancialData
        Return FinancialDataDA.GetFinancialData(oCustomClass)
    End Function
    Function GetFinancialData_002(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.GetFinancialData_002
        Dim FinancialDataDA As New SQLEngine.LoanOrg.FinancialData
        Return FinancialDataDA.GetFinancialData_002(oCustomClass)
    End Function
    Public Function GetInstAmtAdv(ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intNumInstallment As Double, ByVal dblNTF As Double) As Double Implements [Interface].IFinancialData.GetInstAmtAdv
        Dim Value As Double
        Value = Pmt(dblRunRate, intNumInstallment, -dblNTF, 0, DueDate.BegOfPeriod)
        Return Value
    End Function
    Public Function GetInstAmtArr(ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intNumInstallment As Double, ByVal dblNTF As Double) As Double Implements [Interface].IFinancialData.GetInstAmtArr
        Dim Value As Double
        Value = Pmt(dblRunRate, intNumInstallment, -dblNTF, 0, DueDate.EndOfPeriod)
        Return Value
    End Function

    Public Function GetEffectiveRateAdv(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer) As Double Implements [Interface].IFinancialData.GetEffectiveRateAdv
        Dim Values() As Double
        Dim Ctr_Values As Integer
        Dim Total_In_Advanced As Double
        ReDim Values(intNumInstallment)

        Ctr_Values = 0
        Total_In_Advanced = dblInstAmt
        Values(0) = -dblNTF + Total_In_Advanced

        For Me.i = 2 To intNumInstallment
            Ctr_Values = Ctr_Values + 1
            Values(Ctr_Values) = dblInstAmt
        Next

        Return (IRR(Values, 0.00000001) * GetTerm(intPayFreq)) * 100
    End Function

    Public Function GetEffectiveRateArr(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer) As Double Implements [Interface].IFinancialData.GetEffectiveRateArr
        Dim Values() As Double
        ReDim Values(intNumInstallment + 1)

        Values(0) = -dblNTF

        For Me.i = 1 To intNumInstallment
            Values(i) = dblInstAmt
        Next

        Return (IRR(Values, 0.00000001) * GetTerm(intPayFreq)) * 100
    End Function

    Public Function GetTerm(ByVal intInstTerm As Integer) As Integer Implements [Interface].IFinancialData.GetTerm
        Select Case intInstTerm
            Case 1
                Return 12
            Case 2
                Return 3
            Case 3
                Return 2
            Case 6
                Return 1
            Case Else
                Return 0
        End Select
    End Function

    Public Function GetEffectiveRateRO(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As Double Implements [Interface].IFinancialData.GetEffectiveRateRO
        Dim Values() As Double
        ReDim Values(intNumInstallment + 1)
        Values(0) = -dblNTF
        For i = 1 To intGrace
            Values(i) = 0
        Next
        For Me.i = intGrace + 1 To intNumInstallment
            Values(i) = dblInstAmt
        Next

        Return (IRR(Values, 0.00000001) * GetTerm(intPayFreq)) * 100
    End Function

    Public Function GetInstAmtArrRO(ByVal EffectiveRate As Double, ByVal intNumInstallment As Integer, ByVal Runrate As Double, ByVal NTF As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As Double Implements [Interface].IFinancialData.GetInstAmtArrRO
        Dim PH As Double
        Dim hutang As Double
        Dim z As Integer
        PH = NTF
        hutang = NTF
        For z = 1 To intGrace
            PH = Math.Round(PH + ((hutang * EffectiveRate) / (GetTerm(CInt(intPayFreq)) * 100)), 2)
            hutang = PH
        Next
        Return GetInstAmtArr(Runrate, intPayFreq, intNumInstallment - intGrace, PH)
    End Function
    Public Function GetFlatRate(ByVal dblInterestTotal As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer, ByVal Tenor As Integer) As Double Implements [Interface].IFinancialData.GetFlatRate
        'Return (dblInterestTotal * 1200) / (dblNTF * Tenor)
        Return 100 * dblInterestTotal / dblNTF
    End Function
    Function SaveFinancialData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.SaveFinancialData
        Try
            Dim FinancialDA As New SQLEngine.LoanOrg.FinancialData
            Return FinancialDA.SaveFinancialData(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error when saving Insert Financial", ex)
        End Try
    End Function
    Function GetViewFinancialData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.GetViewFinancialData
        Dim oData1 As New DataTable
        Dim oData2 As New DataTable
        Dim oData3 As New DataTable
        Dim oDataNew As New DataTable
        Dim oRow3, oRow1 As DataRow
        Dim oRow2 As DataRow
        Dim int As Integer
        Dim oEntities As New Parameter.FinancialData
        Dim FinancialDA As New SQLEngine.LoanOrg.FinancialData
        Try
            With oDataNew
                .Columns.Add(New DataColumn("No", GetType(String)))
                .Columns.Add(New DataColumn("InstallmentAmount", GetType(Decimal)))
                .Columns.Add(New DataColumn("PrincipalAmount", GetType(Decimal)))
                .Columns.Add(New DataColumn("InterestAmount", GetType(Decimal)))
                .Columns.Add(New DataColumn("OutstandingPrincipal", GetType(Decimal)))
                .Columns.Add(New DataColumn("OutstandingInterest", GetType(Decimal)))
            End With
            oEntities = FinancialDA.GetViewFinancialData(oCustomClass)
            oData1 = oEntities.Data1
            oData2 = oEntities.data2
            oData3 = oEntities.data3
            If oData3.Rows.Count > 0 And oData1.Rows.Count > 0 Then
                oRow1 = oData1.Rows(0)
                oRow3 = oData3.Rows(0)
                oRow2 = oDataNew.NewRow()
                oRow2("OutstandingPrincipal") = CType(oRow1.Item(7), Decimal)
                oRow2("OutstandingInterest") = CType(oRow3.Item(2), Decimal)
                oDataNew.Rows.Add(oRow2)

                For int = 0 To oData2.Rows.Count - 1
                    oRow2 = oDataNew.NewRow()
                    oRow2("No") = CType(oData2.Rows(int).Item(0), String)
                    oRow2("InstallmentAmount") = CType(oData2.Rows(int).Item(1), Decimal)
                    oRow2("PrincipalAmount") = CType(oData2.Rows(int).Item(2), Decimal)
                    oRow2("InterestAmount") = CType(oData2.Rows(int).Item(3), Decimal)
                    oRow2("OutstandingPrincipal") = CType(oData2.Rows(int).Item(4), Decimal)
                    oRow2("OutstandingInterest") = CType(oData2.Rows(int).Item(5), Decimal)
                    oDataNew.Rows.Add(oRow2)
                Next

                oRow2 = oDataNew.NewRow()
                oRow2("InstallmentAmount") = CType(oRow3.Item(0), Decimal)
                oRow2("PrincipalAmount") = CType(oRow3.Item(1), Decimal)
                oRow2("InterestAmount") = CType(oRow3.Item(2), Decimal)
                oDataNew.Rows.Add(oRow2)

                oEntities.Data1 = oData1
                oEntities.data2 = oDataNew
                Return oEntities
            Else
                oEntities.Output = "Error"
                Return oEntities
            End If
        Catch ex As Exception
            oEntities.Output = "Error " & ex.Message & ""
            Return oEntities
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when saving Insert Financial", ex)
        End Try
    End Function

    Public Function ClearingInstallmentSchedule(ByVal oCustomClass As Parameter.FinancialData) As String Implements [Interface].IFinancialData.ClearingInstallmentSchedule
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        DA.ClearingInstallmentSchedule(oCustomClass)
    End Function

    Public Function GetPrincipleAmount(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.GetPrincipleAmount
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        Return DA.GetPrincipleAmount(oCustomClass)
    End Function

    Public Function SaveAmortisasi(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.SaveAmortisasi
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        Return DA.SaveAmortisasi(oCustomClass)
    End Function
    Public Function SaveAmortisasiIRR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.SaveAmortisasiIRR
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        Return DA.SaveAmortisasiIRR(oCustomClass)
    End Function

    Public Function GetStepUpStepDownType(ByVal oCustomClass As Parameter.FinancialData) As String Implements [Interface].IFinancialData.GetStepUpStepDownType
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        Return DA.GetStepUpStepDownType(oCustomClass)
    End Function

    Public Function SaveAmortisasiStepUpDown(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.SaveAmortisasiStepUpDown
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        Return DA.SaveAmortisasiStepUpDown(oCustomClass)
    End Function
    Public Function GetInstAmtStepUpDownRegLeasing(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.GetInstAmtStepUpDownRegLeasing
        Dim dblRunRate As Double
        Dim intPayFreq As Integer
        dblRunRate = oCustomClass.EffectiveRate
        intPayFreq = CType(oCustomClass.PaymentFrequency, Integer)
        If oCustomClass.FirstInstallment = "AR" Then
            oCustomClass.installmentamount = Pmt(dblRunRate / (GetTerm(intPayFreq) * 100), oCustomClass.CummulativeStart, (oCustomClass.NTF * -1), 0, DueDate.EndOfPeriod)
        Else
            oCustomClass.installmentamount = Pmt(dblRunRate / (GetTerm(intPayFreq) * 100), oCustomClass.CummulativeStart, (oCustomClass.NTF * -1), 0, DueDate.BegOfPeriod)
        End If

        Return oCustomClass
    End Function
    Public Function GetInstAmtStepUpDownLeasing(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.GetInstAmtStepUpDownLeasing
        Dim dblRunRate As Double
        Dim intPayFreq As Integer
        dblRunRate = oCustomClass.EffectiveRate
        intPayFreq = CType(oCustomClass.PaymentFrequency, Integer)
        If oCustomClass.FirstInstallment = "AR" Then
            oCustomClass.installmentamount = Pmt(dblRunRate / (GetTerm(intPayFreq) * 100), oCustomClass.CummulativeStart, (oCustomClass.NTF * -1), 0, DueDate.EndOfPeriod)
        Else
            oCustomClass.installmentamount = Pmt(dblRunRate / (GetTerm(intPayFreq) * 100), oCustomClass.CummulativeStart, (oCustomClass.NTF * -1), 0, DueDate.BegOfPeriod)
        End If

        Return oCustomClass
    End Function

    Public Function GetStepUpDownRegLeasingTBL(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.GetStepUpDownRegLeasingTBL
        Dim inc As Integer
        Dim intSisa As Integer
        Dim InstallmentAmount As Double
        Dim dblRunRate As Double
        Dim dblPokokHutangST As Double
        Dim intPayFreq As Integer
        Dim intNumOfInst As Integer
        Dim intCummulative As Integer
        intNumOfInst = oCustomClass.NumOfInstallment
        Dim PokokHutang(intNumOfInst) As Double
        Dim Interest(intNumOfInst) As Double
        Dim Principal(intNumOfInst) As Double
        Dim myDataTable As DataTable
        Dim myDataColumn As DataColumn
        Dim myDataRow As DataRow
        Dim myDataSet As New DataSet

        dblRunRate = oCustomClass.EffectiveRate
        intPayFreq = CInt(oCustomClass.PaymentFrequency)
        intCummulative = oCustomClass.CummulativeStart - 1
        PokokHutang(0) = oCustomClass.NTF
        Principal(0) = 0
        Interest(0) = 0

        myDataTable = New DataTable("Installment")
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)
        InstallmentAmount = oCustomClass.installmentamount

        For inc = 1 To intCummulative
            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc
            myDataRow("Amount") = InstallmentAmount
            myDataTable.Rows.Add(myDataRow)
            If inc = 1 And oCustomClass.FirstInstallment = "AD" Then
                Interest(inc) = 0
            Else
                Interest(inc) = Math.Round((PokokHutang(inc - 1) * dblRunRate) / (GetTerm(intPayFreq) * 100), 2)
            End If
            Principal(inc) = InstallmentAmount - Interest(inc)
            PokokHutang(inc) = PokokHutang(inc - 1) - Principal(inc)
        Next
        intSisa = (intNumOfInst - intCummulative)
        dblPokokHutangST = PokokHutang(intCummulative) * -1
        InstallmentAmount = Pmt(dblRunRate / (GetTerm(intPayFreq) * 100), intSisa, dblPokokHutangST, 0, DueDate.EndOfPeriod)
        For inc = (intCummulative + 1) To intNumOfInst
            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc
            myDataRow("Amount") = InstallmentAmount
            myDataTable.Rows.Add(myDataRow)
        Next
        myDataSet.Tables.Add(myDataTable)
        oCustomClass.MydataSet = myDataSet
        Return oCustomClass
    End Function

#Region "GetNewEffectiveRateIrregular"
    Public Function GetNewEffRateIRR(ByVal oCustomClass As Parameter.FinancialData) As Double Implements [Interface].IFinancialData.GetNewEffRateIRR
        Dim inc, Start, IncData As Integer
        Dim jmlRow As Integer
        Dim objDs As New DataSet
        Dim oFinancialData As New Parameter.Address
        objDs = oCustomClass.MydataSet
        jmlRow = (objDs.Tables("Installment").Rows.Count + 1)
        If oCustomClass.FirstInstallment = "AR" Then
            jmlRow = (jmlRow + 1)
        End If
        Dim Values(jmlRow) As Double
        If oCustomClass.FirstInstallment = "AR" Then
            Values(0) = CDbl(oCustomClass.NTF) * -1
            Start = 0
        Else
            Values(0) = (CDbl(oCustomClass.NTF) * -1) + CType(objDs.Tables("Installment").Rows(0)("Amount"), Double)
            Start = 1
        End If
        IncData = 1
        For inc = Start To objDs.Tables("Installment").Rows.Count - 1
            Values(IncData) = CType(objDs.Tables("Installment").Rows(inc)("Amount"), Double)
            IncData = IncData + 1
        Next
        If oCustomClass.InstallmentScheme <> "ST" Then
            Values(IncData) = CType(oCustomClass.installmentamount, Double)
        End If
        Return (IRR(Values, 0.00001) * GetTerm(CInt(oCustomClass.PaymentFrequency)) * 100)

    End Function

    Public Function GetNewEffRateRegLeasing(ByVal oCustomClass As Parameter.FinancialData) As Double Implements [Interface].IFinancialData.GetNewEffRateRegLeasing
        Dim inc, IncData As Integer
        Dim jmlRow As Integer
        jmlRow = oCustomClass.CummulativeStart - 1
        If oCustomClass.FirstInstallment = "AR" Then
            jmlRow = (jmlRow + 1)
        End If
        Dim Values(jmlRow) As Double
        If oCustomClass.FirstInstallment = "AR" Then
            Values(0) = CDbl(oCustomClass.NTF) * -1
        Else
            Values(0) = (CDbl(oCustomClass.NTF) * -1) + oCustomClass.installmentamount
        End If
        IncData = 1
        For inc = 0 To jmlRow - 1
            Values(IncData) = oCustomClass.installmentamount
            IncData = IncData + 1
        Next
        Return (IRR(Values, 0.00001) * GetTerm(CInt(oCustomClass.PaymentFrequency)) * 100)
    End Function

    Public Function GetNewEffRateLeasing(ByVal oCustomClass As Parameter.FinancialData) As Double Implements [Interface].IFinancialData.GetNewEffRateLeasing
        Dim inc, IncData As Integer
        Dim jmlRow As Integer
        Dim objDs As New DataSet
        Dim oFinancialData As New Parameter.Address
        objDs = oCustomClass.MydataSet
        jmlRow = oCustomClass.CummulativeStart
        Dim Values(jmlRow) As Double
        If oCustomClass.FirstInstallment = "AR" Then
            Values(0) = CDbl(oCustomClass.NTF) * -1
            inc = 0
        Else
            Values(0) = (CDbl(oCustomClass.NTF) * -1) + CType(objDs.Tables("Installment").Rows(0)("Amount"), Double)
            inc = 1
        End If
        IncData = 1
        If oCustomClass.FirstInstallment = "AD" Then
            jmlRow = (jmlRow - 1)
        End If
        While IncData <= jmlRow
            Values(IncData) = CType(objDs.Tables("Installment").Rows(inc)("Amount"), Double)
            IncData = IncData + 1
            inc = inc + 1
        End While
        Return (IRR(Values, 0.00001) * GetTerm(CInt(oCustomClass.PaymentFrequency)) * 100)

    End Function
#End Region

    Public Function ListAmortisasiStepUpDown(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.ListAmortisasiStepUpDown
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        Return DA.ListAmortisasiStepUpDown(oCustomClass)
    End Function
    Public Sub saveDefaultAdminFee(ByVal oCustomClass As Parameter.FinancialData) Implements [Interface].IFinancialData.saveDefaultAdminFee
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        DA.saveDefaultAdminFee(oCustomClass)
    End Sub

    Public Sub saveDefaultAgentFee(ByVal oCustomClass As Parameter.FinancialData) Implements [Interface].IFinancialData.saveDefaultAgentFee
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        DA.saveDefaultAgentFee(oCustomClass)
    End Sub

    Public Function SaveFinancialDataSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.SaveFinancialDataSiml
        Dim FinancialDA As New SQLEngine.LoanOrg.FinancialData
        Return FinancialDA.SaveFinancialDataSiml(oCustomClass)
    End Function

    Public Function SaveAmortisasiIRRSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.SaveAmortisasiIRRSiml
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        Return DA.SaveAmortisasiIRRSiml(oCustomClass)
    End Function

    Public Function SaveAmortisasiStepUpDownSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.SaveAmortisasiStepUpDownSiml
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        Return DA.SaveAmortisasiStepUpDownSiml(oCustomClass)
    End Function

    Public Function SaveAmortisasiSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.SaveAmortisasiSiml
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        Return DA.SaveAmortisasiSiml(oCustomClass)
    End Function

    Public Function ListAmortisasiStepUpDownSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.ListAmortisasiStepUpDownSiml
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        Return DA.ListAmortisasiStepUpDownSiml(oCustomClass)
    End Function

    Public Function GetInstallmentSchSiml(ByVal oCustomClass As Parameter.FinancialData) As System.Data.DataSet Implements [Interface].IFinancialData.GetInstallmentSchSiml
        Dim FinancialDataDA As New SQLEngine.LoanOrg.FinancialData
        Return FinancialDataDA.GetInstallmentSchSiml(oCustomClass)
    End Function

    'Public Function GetInstAmtAdvFlatRate(ByVal FlatRate As Double, ByVal intNumInstallment As Double, ByVal dblNTF As Double) As Double Implements [Interface].IFinancialData.GetInstAmtAdvFlatRate
    '    Return IPmt(FlatRate, intNumInstallment, -dblNTF, , 0, DueDate.BegOfPeriod)
    'End Function

    Public Function GetInstAmtAdvFlatRate(ByVal FlatRate As Double, ByVal intPayFreq As Integer, ByVal intNumInstallment As Double, ByVal dblNTF As Double) As Double Implements [Interface].IFinancialData.GetInstAmtAdvFlatRate
        Dim principal As Double
        Dim interest As Double

        principal = dblNTF / intNumInstallment
        interest = dblNTF * FlatRate / 100 * (intNumInstallment / intPayFreq) / intNumInstallment

        Return principal + interest
    End Function

    Public Function GetRefundPremiumToSupplier(ByVal data As Parameter.Supplier) As Parameter.Supplier Implements [Interface].IFinancialData.GetRefundPremiumToSupplier
        Dim FinancialDataDA As New SQLEngine.LoanOrg.FinancialData
        Return FinancialDataDA.GetRefundPremiumToSupplier(data)
    End Function

    Public Function saveBungaNett(oClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.saveBungaNett
        Dim FinancialDataDA As New SQLEngine.LoanOrg.FinancialData
        Return FinancialDataDA.saveBungaNett(oClass)
    End Function

    Public Function GetInstallmentSchedule(ocustomClass As Parameter.FinancialData) As DataTable Implements [Interface].IFinancialData.GetInstallmentSchedule
        Dim FinancialDataDA As New SQLEngine.LoanOrg.FinancialData
        Return FinancialDataDA.GetInstallmentSchedule(ocustomClass)
    End Function

    Public Function ListAmortisasiIRR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.ListAmortisasiIRR
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        Return DA.ListAmortisasiIRR(oCustomClass)
    End Function

    Public Function SaveFinancialDataTab2(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.SaveFinancialDataTab2
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        Return DA.SaveFinancialDataTab2(oCustomClass)
    End Function

    Public Function SaveFinancialDataTab2End(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.SaveFinancialDataTab2End
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        Return DA.SaveFinancialDataTab2End(oCustomClass)
    End Function

    Public Function SaveFinancialDataTab2OperatingLease(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.SaveFinancialDataTab2OperatingLease
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        Return DA.SaveFinancialDataTab2OperatingLease(oCustomClass)
    End Function

    Public Function SaveFinancialDataTab2EndOperatingLease(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.SaveFinancialDataTab2EndOperatingLease
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        Return DA.SaveFinancialDataTab2EndOperatingLease(oCustomClass)
    End Function

    Public Function GoLiveJournalDummyOperatingLease(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.GoLiveJournalDummyOperatingLease
        Dim DA As New SQLEngine.LoanOrg.FinancialData
        Return DA.GoLiveJournalDummyOperatingLease(oCustomClass)
    End Function

    Function SaveFinancialDataOperatingLease(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements [Interface].IFinancialData.SaveFinancialDataOperatingLease
        Try
            Dim FinancialDA As New SQLEngine.LoanOrg.FinancialData
            Return FinancialDA.SaveFinancialDataOperatingLease(oCustomClass)
        Catch ex As Exception
            Throw New Exception("Error when saving Insert Financial", ex)
        End Try
    End Function

End Class
