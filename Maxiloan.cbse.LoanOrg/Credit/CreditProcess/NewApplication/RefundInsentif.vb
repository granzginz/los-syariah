﻿Imports Maxiloan.Interface
Imports Maxiloan.SQLEngine

Public Class RefundInsentif : Inherits ComponentBase
    Implements IRefundInsentif


    Public Function getDistribusiNilaiInsentif(ByVal oCustomClass As Parameter.RefundInsentif) As Parameter.RefundInsentif Implements [Interface].IRefundInsentif.getDistribusiNilaiInsentif
        Dim oSQLE As New SQLEngine.LoanOrg.RefundInsentif
        Return oSQLE.getDistribusiNilaiInsentif(oCustomClass)
    End Function

    Public Sub SupplierINCTVDailyDAdd(ByVal oCustomClass As Parameter.RefundInsentif) Implements [Interface].IRefundInsentif.SupplierINCTVDailyDAdd
        Dim DA As New SQLEngine.LoanOrg.RefundInsentif
        DA.SupplierINCTVDailyDAdd(oCustomClass)
    End Sub

    Public Function GetSPBy(ocustomclass As Parameter.RefundInsentif) As Parameter.RefundInsentif Implements [Interface].IRefundInsentif.GetSPBy
        Dim oSQLE As New SQLEngine.LoanOrg.RefundInsentif
        Return oSQLE.GetSPBy(ocustomclass)
    End Function

    Public Sub SupplierIncentiveDailyDDelete(customclass As Parameter.RefundInsentif) Implements [Interface].IRefundInsentif.SupplierIncentiveDailyDDelete
        Dim DA As New SQLEngine.LoanOrg.RefundInsentif
        DA.SupplierIncentiveDailyDDelete(customclass)
    End Sub

    Public Sub AlokasiInsentifInternalDelete(customclass As Parameter.RefundInsentif) Implements [Interface].IRefundInsentif.AlokasiInsentifInternalDelete
        Dim DA As New SQLEngine.LoanOrg.RefundInsentif
        DA.AlokasiInsentifInternalDelete(customclass)
    End Sub

    Public Sub AlokasiInsentifInternalSave(customclass As Parameter.RefundInsentif) Implements [Interface].IRefundInsentif.AlokasiInsentifInternalSave
        Dim DA As New SQLEngine.LoanOrg.RefundInsentif
        DA.AlokasiInsentifInternalSave(customclass)
    End Sub

    Public Function SupplierIncentiveDailyDSav(customclass As Parameter.RefundInsentif) As String Implements [Interface].IRefundInsentif.SupplierIncentiveDailyDSave
        Dim DA As New SQLEngine.LoanOrg.RefundInsentif
        Return DA.SupplierIncentiveDailyDSave(customclass)
    End Function

    Public Function UpdateDateEntryInsentif(customclass As Parameter.RefundInsentif) As String Implements [Interface].IRefundInsentif.UpdateDateEntryInsentif
        Dim DA As New SQLEngine.LoanOrg.RefundInsentif
        Return DA.UpdateDateEntryInsentif(customclass)
    End Function

    Public Function GoLiveJournalDummy(ByVal customClass As Parameter.RefundInsentif) As Parameter.RefundInsentif Implements [Interface].IRefundInsentif.GoLiveJournalDummy
        Dim DA As New SQLEngine.LoanOrg.RefundInsentif
        Return DA.GoLiveJournalDummy(customClass)
    End Function

    'Public Function SupplierIncentiveDailyDSaveFactoring(customclass As Parameter.RefundInsentif) As String Implements [Interface].IRefundInsentif.SupplierIncentiveDailyDSaveFactoring
    '    Dim DA As New SQLEngine.LoanOrg.RefundInsentif
    '    Return DA.SupplierIncentiveDailyDSaveFactoring(customclass)
    'End Function
    'Public Sub SupplierINCTVDailyDAddFACT(ByVal oCustomClass As Parameter.RefundInsentif) Implements [Interface].IRefundInsentif.SupplierINCTVDailyDAddFACT
    '    Dim DA As New SQLEngine.LoanOrg.RefundInsentif
    '    DA.SupplierINCTVDailyDAddFACT(oCustomClass)
    'End Sub
    'Public Sub SupplierINCTVDailyDAddMDKJ(ByVal oCustomClass As Parameter.RefundInsentif) Implements [Interface].IRefundInsentif.SupplierINCTVDailyDAddMDKJ
    '    Dim DA As New SQLEngine.LoanOrg.RefundInsentif
    '    DA.SupplierINCTVDailyDAddMDKJ(oCustomClass)
    'End Sub
End Class
