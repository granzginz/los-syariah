﻿Imports Maxiloan.Interface

Public Class HasilSurvey
    Implements IHasilSurvey

    Public Function getHasilSurvey(ByVal oCustomClass As Parameter.HasilSurvey) As Parameter.HasilSurvey Implements [Interface].IHasilSurvey.getHasilSurvey
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        Return oSQLE.getHasilSurvey(oCustomClass)
    End Function

    Public Sub HasilSurveySave(ByVal oCustomClass As Parameter.HasilSurvey) Implements [Interface].IHasilSurvey.HasilSurveySave
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        oSQLE.HasilSurveySave(oCustomClass)
    End Sub

    Public Sub ApplicationRejectUpdate(ByVal oCustomClass As Parameter.HasilSurvey) Implements [Interface].IHasilSurvey.ApplicationRejectUpdate
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        oSQLE.ApplicationRejectUpdate(oCustomClass)
    End Sub

    Public Function GetApplicationReject(ByVal oCustomClass As Parameter.HasilSurvey) As Parameter.HasilSurvey Implements [Interface].IHasilSurvey.GetApplicationReject
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        Return oSQLE.GetApplicationReject(oCustomClass)
    End Function

    Public Sub HasilSurveyJournalSave(oCustomClass As Parameter.HasilSurvey) Implements [Interface].IHasilSurvey.HasilSurveyJournalSave
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        oSQLE.HasilSurveyJournalSave(oCustomClass)
    End Sub

    Public Sub Proceed(oCustomClass As Parameter.HasilSurvey) Implements [Interface].IHasilSurvey.Proceed
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        oSQLE.Proceed(oCustomClass)
    End Sub
    'wira 20150825
    Public Sub HasilSurvey002Save(ByVal oCustomClass As Parameter.HasilSurvey) Implements [Interface].IHasilSurvey.HasilSurvey002Save
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        oSQLE.HasilSurvey002Save(oCustomClass)
    End Sub

    Public Sub HasilSurvey002CharacterSave(ByVal oCustomClass As Parameter.HasilSurvey) Implements [Interface].IHasilSurvey.HasilSurvey002CharacterSave
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        oSQLE.HasilSurvey002CharacterSave(oCustomClass)
    End Sub

    Public Sub HasilSurvey002CapacitySave(ByVal oCustomClass As Parameter.HasilSurvey) Implements [Interface].IHasilSurvey.HasilSurvey002CapacitySave
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        oSQLE.HasilSurvey002CapacitySave(oCustomClass)
    End Sub

    Public Sub HasilSurvey002ConditionSave(ByVal oCustomClass As Parameter.HasilSurvey) Implements [Interface].IHasilSurvey.HasilSurvey002ConditionSave
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        oSQLE.HasilSurvey002ConditionSave(oCustomClass)
    End Sub

    Public Sub HasilSurvey002CapitalSave(ByVal oCustomClass As Parameter.HasilSurvey) Implements [Interface].IHasilSurvey.HasilSurvey002CapitalSave
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        oSQLE.HasilSurvey002CapitalSave(oCustomClass)
    End Sub

    Public Sub HasilSurvey002CollateralSave(ByVal oCustomClass As Parameter.HasilSurvey) Implements [Interface].IHasilSurvey.HasilSurvey002CollateralSave
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        oSQLE.HasilSurvey002CollateralSave(oCustomClass)
    End Sub

    Public Sub HasilSurvey002CatatanSave(ByVal oCustomClass As Parameter.HasilSurvey) Implements [Interface].IHasilSurvey.HasilSurvey002CatatanSave
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        oSQLE.HasilSurvey002CatatanSave(oCustomClass)
    End Sub

    Public Sub HasilSurvey002KYCSave(ByVal oCustomClass As Parameter.HasilSurvey) Implements [Interface].IHasilSurvey.HasilSurvey002KYCSave
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        oSQLE.HasilSurvey002KYCSave(oCustomClass)
    End Sub

    Public Sub HasilSurvey002CharacterCLSave(ByVal oCustomClass As Parameter.HasilSurvey) Implements [Interface].IHasilSurvey.HasilSurvey002CharacterCLSave
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        oSQLE.HasilSurvey002CharacterCLSave(oCustomClass)
    End Sub

    Public Sub HasilSurvey002AplStepSave(ByVal oCustomClass As Parameter.HasilSurvey) Implements [Interface].IHasilSurvey.HasilSurvey002AplStepSave
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        oSQLE.HasilSurvey002AplStepSave(oCustomClass)
    End Sub

    Public Function getHasilMobileSurvey(ByVal oCustomClass As Parameter.HasilSurvey) As Parameter.HasilSurvey Implements [Interface].IHasilSurvey.getHasilMobileSurvey
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        Return oSQLE.getHasilMobileSurvey(oCustomClass)
    End Function

    Public Function getHasilScore(ByVal scoringData As Parameter.HasilSurvey) As Parameter.HasilSurvey Implements [Interface].IHasilSurvey.getHasilScore
        Dim oSQLE As New Maxiloan.SQLEngine.LoanOrg.HasilSurvey
        Return oSQLE.getHasilScore(scoringData)
    End Function
End Class
