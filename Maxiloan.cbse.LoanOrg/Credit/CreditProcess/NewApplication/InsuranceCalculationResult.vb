

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class InsuranceCalculationResult : Inherits ComponentBase
    Implements IInsuranceCalculationResult

#Region "DisplayResultInsCalculationStep1"

    Public Function DisplayResultInsCalculationStep1(ByVal customClass As Parameter.InsuranceCalculationResult) As Parameter.InsuranceCalculationResult Implements IInsuranceCalculationResult.DisplayResultInsCalculationStep1
        Try
            Dim DA As New SQLEngine.LoanOrg.InsuranceCalculationResult
            Return DA.DisplayResultInsCalculationStep1(customClass)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try


    End Function

    Public Function DisplayResultOnGrid(ByVal CustomClass As Parameter.InsuranceCalculationResult) As Parameter.InsuranceCalculationResult Implements [Interface].IInsuranceCalculationResult.DisplayResultOnGrid
        Try
            Dim DA As New SQLEngine.LoanOrg.InsuranceCalculationResult
            Return DA.DisplayResultOnGrid(CustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function


    Public Function DisplayResultOnGridAdditional(ByVal customClass As Parameter.InsuranceCalculationResult, ByVal InsuranceProductID As String) As Parameter.InsuranceCalculationResult Implements [Interface].IInsuranceCalculationResult.DisplayResultOnGridAdditional
        Try
            Dim DA As New SQLEngine.LoanOrg.InsuranceCalculationResult
            Return DA.DisplayResultOnGridAdditional(customClass, InsuranceProductID)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

#End Region


End Class
