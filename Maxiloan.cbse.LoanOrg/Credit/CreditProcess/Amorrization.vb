
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region
Public Class Amortization
    Implements IMakeAmortization
#Region "Constanta"
    Dim i As Integer
    Dim RunRate As Double
    Dim PokokHutang() As Double
    Dim Hutang() As Double
    Dim PokokBunga() As Double
    Dim Bunga() As Double
    Dim HitungUlangSisa As Boolean = False
    Dim HitungUlangKurang As Boolean = False
    Dim SisaPrincipal As Double
    Dim KurangPrincipal As Double
    Dim myDataSet As New DataSet
    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim objrow As DataRow
    Dim dblInterestTotal As Double

#End Region

#Region "Buat Table"
    Public Sub BuatTable()
        ' Create new DataColumn, set DataType, ColumnName and add to DataTable.
        myDataTable = New DataTable("AmortTable")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Installment"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Principal"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Interest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincBalance"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincInterest"
        myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn
        'myDataColumn.DataType = System.Type.GetType("System.Double")
        'myDataColumn.ColumnName = "InterestTotal"
        'myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "InstallmentSisa"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepFrom"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepTo"
        myDataTable.Columns.Add(myDataColumn)
    End Sub
#End Region
    Public Function GetTerm(ByVal intInstTerm As Integer) As Integer
        Select Case intInstTerm
            Case 1 'Monthly
                Return 12
            Case 2 'Bimonthly
                Return 6
            Case 3 'Quarterly
                Return 4
            Case 6 'Semi Annualy
                Return 2
        End Select
    End Function

    Private Function MakeAmortTable1(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer) As DataSet
        'Regular Installment - Advance - No Grace Period
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        Dim TotalPrincipal As Double
        PokokHutang(0) = dblNTF
        Bunga(0) = 0
        BuatTable()
        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Advance dan installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            ElseIf i = 2 Then
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = 0
                myDataRow("Principal") = dblInstAmt
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("PrincBalance") = 0
                Else
                    myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
                End If
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next
        Dim myDataSet As DataSet
        myDataSet = New DataSet
        myDataSet.Tables.Add(myDataTable)
        Dim custRow As DataRow
        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
    Private Function MakeAmortTable2(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer) As DataSet
        'Regular Installment - Arrear - No Grace Period
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim TotalPrincipal As Double
        Bunga(0) = 0
        PokokHutang(0) = dblNTF
        BuatTable()
        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            ElseIf i = intNumInstallment + 1 Then
                myDataRow("PrincBalance") = 0
            Else
                myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next

        Dim myDataSet As DataSet
        myDataSet = New DataSet

        myDataSet.Tables.Add(myDataTable)

        Dim custRow As DataRow

        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
    Private Function MakeAmortTable3(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As DataSet
        'Regular Installment - Arrear - Grace Period (Roll Over)
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim intGracePeriod As Integer = intGrace + 1
        Dim TotalPrincipal As Double
        Dim SisaPrincipal As Double
        Dim KurangPrincipal As Double
        Dim HitungUlangSisa As Boolean = False
        Dim HitungUlangKurang As Boolean = False

        Bunga(0) = 0
        PokokHutang(0) = dblNTF

        BuatTable()

        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            ElseIf i <= intGracePeriod Then
                myDataRow("No") = i - 1
                myDataRow("Installment") = 0
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(CDbl(myDataRow("Installment")) - CDbl(myDataRow("Interest")), 2)
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("PrincBalance") = 0
                Else
                    myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
                End If
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next
        Dim myDataSet As DataSet
        myDataSet = New DataSet
        myDataSet.Tables.Add(myDataTable)
        Dim custRow As DataRow
        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
    Private Function MakeAmortTable6(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As DataSet
        'Regular Installment - Arrear - Grace Period (Interest Only)
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim intGracePeriod As Integer = intGrace + 1
        Dim TotalPrincipal As Double
        Dim SisaPrincipal As Double
        Dim KurangPrincipal As Double
        Dim HitungUlangSisa As Boolean = False
        Dim HitungUlangKurang As Boolean = False

        Bunga(0) = 0
        PokokHutang(0) = dblNTF

        BuatTable()

        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            ElseIf i <= intGracePeriod Then
                myDataRow("No") = i - 1
                myDataRow("Installment") = Math.Round(dblNTF * dblRunRate / GetTerm(intPayFreq) / 100, 2)
                myDataRow("Interest") = myDataRow("Installment")
                myDataRow("Principal") = 0
            ElseIf i = intGracePeriod + 1 Then
                myDataRow("No") = i - 1
                myDataRow("Interest") = Math.Round(dblNTF * dblRunRate / GetTerm(intPayFreq) / 100, 2)
                myDataRow("Installment") = dblInstAmt
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("PrincBalance") = 0
                Else
                    myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
                End If
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next

        Dim myDataSet As DataSet
        myDataSet = New DataSet

        myDataSet.Tables.Add(myDataTable)
        Dim custRow As DataRow
        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function

    Public Function MakeAmortization(ByVal customclass As Parameter.Amortization) As System.Data.DataSet Implements [Interface].IMakeAmortization.MakeAmortization

    End Function

End Class
