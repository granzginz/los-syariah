
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class PO
    Implements IPO

    Function POPaging(ByVal oCustomClass As Parameter.PO) As Parameter.PO Implements [Interface].IPO.POPaging
        Try
            Dim PODA As New SQLEngine.LoanOrg.PO
            Return PODA.POPaging(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    'Public Function POSave(ByVal customClass As Parameter.PO, ByVal oData1 As DataTable, ByVal oData2 As DataTable) As String Implements [Interface].IPO.POSave
    '    Dim PODA As New SQLEngine.LoanOrg.PO
    '    Return PODA.POSave(customClass, oData1, oData2)
    'End Function

    Public Function PO_Kepada(ByVal customClass As Parameter.PO) As Parameter.PO Implements [Interface].IPO.PO_Kepada
        Try
            Dim PODA As New SQLEngine.LoanOrg.PO
            Return PODA.PO_Kepada(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function PO_DikirimKe(ByVal customClass As Parameter.PO) As Parameter.PO Implements [Interface].IPO.PO_DikirimKe
        Try
            Dim PODA As New SQLEngine.LoanOrg.PO
            Return PODA.PO_DikirimKe(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function PO_ItemFee(ByVal customClass As Parameter.PO) As Parameter.PO Implements [Interface].IPO.PO_ItemFee
        Try
            Dim PODA As New SQLEngine.LoanOrg.PO
            Return PODA.PO_ItemFee(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function Get_Combo_SupplierAccount(ByVal customClass As Parameter.PO) As Parameter.PO Implements [Interface].IPO.Get_Combo_SupplierAccount
        Try
            Dim PODA As New SQLEngine.LoanOrg.PO
            Return PODA.Get_Combo_SupplierAccount(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function Get_PONumber(ByVal customClass As Parameter.PO) As String Implements [Interface].IPO.Get_PONumber
        Try
            Dim PODA As New SQLEngine.LoanOrg.PO
            Return PODA.Get_PONumber(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function PO_Account(ByVal customClass As Parameter.PO) As Parameter.PO Implements [Interface].IPO.PO_Account
        Try
            Dim PODA As New SQLEngine.LoanOrg.PO
            Return PODA.PO_Account(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Function POExtendPaging(ByVal oCustomClass As Parameter.PO) As Parameter.PO Implements [Interface].IPO.POExtendPaging
        Try
            Dim PODA As New SQLEngine.LoanOrg.PO
            Return PODA.POExtendPaging(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Sub POExtendSaveEdit(ByVal customClass As Parameter.PO) Implements [Interface].IPO.POExtendSaveEdit
        Try
            Dim PODA As New SQLEngine.LoanOrg.PO
            PODA.POExtendSaveEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            'Throw New Exception("Error when saving Update AssetType", ex)
        End Try
    End Sub

    Public Function POViewExtend(ByVal customClass As Parameter.PO) As Parameter.PO Implements [Interface].IPO.POViewExtend
        Try
            Dim PODA As New SQLEngine.LoanOrg.PO
            Return PODA.POViewExtend(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function POSave(ByVal oCustomClass As Parameter.PO) As String Implements [Interface].IPO.POSave
        Dim PODA As New SQLEngine.LoanOrg.PO
        Return PODA.POSave(oCustomClass)
    End Function

    Public Function GetIDKurs(ByVal customclass As Parameter.PO) As Parameter.PO Implements [Interface].IPO.GetIDKurs
        Dim PODA As New SQLEngine.LoanOrg.PO
        Return PODA.GetIDKurs(customclass)
    End Function
End Class
