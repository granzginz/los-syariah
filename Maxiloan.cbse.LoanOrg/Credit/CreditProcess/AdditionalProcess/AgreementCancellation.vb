#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class AgreementCancellation
    Implements IAgreementCancellation

    Public Sub ProcessAgreementCancellation(ByVal oCustomClass As Parameter.AgreementCancellation) Implements [Interface].IAgreementCancellation.ProcessAgreementCancellation
        Dim AgreementCancellationDA As SQLEngine.LoanOrg.AgreementCancellation
        Try
            AgreementCancellationDA = New SQLEngine.LoanOrg.AgreementCancellation
            AgreementCancellationDA.ProcessAgreementCancellation(oCustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
