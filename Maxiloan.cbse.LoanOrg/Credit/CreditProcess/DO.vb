
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class _DO
    Implements IDO

    Function DOPaging(ByVal oCustomClass As Parameter._DO) As Parameter._DO Implements [Interface].IDO.DOPaging
        Try
            Dim DODA As New SQLEngine.LoanOrg._DO
            Return DODA.DOPaging(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function Get_PONumber(ByVal customClass As Parameter._DO) As String Implements [Interface].IDO.Get_PONumber
        Try
            Dim DODA As New SQLEngine.LoanOrg._DO
            Return DODA.Get_PONumber(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetDO_CheckBackDate(ByVal customClass As Parameter._DO) As Integer Implements [Interface].IDO.GetDO_CheckBackDate
        Try
            Dim DODA As New SQLEngine.LoanOrg._DO
            Return DODA.GetDO_CheckBackDate(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function Get_DOProcess(ByVal customClass As Parameter._DO) As Parameter._DO Implements [Interface].IDO.Get_DOProcess
        Try
            Dim DODA As New SQLEngine.LoanOrg._DO
            Return DODA.Get_DOProcess(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetDO_AssetDoc(ByVal customClass As Parameter._DO) As Parameter._DO Implements [Interface].IDO.GetDO_AssetDoc
        Try
            Dim DODA As New SQLEngine.LoanOrg._DO
            Return DODA.GetDO_AssetDoc(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Function GetAttribute(ByVal oCustomClass As Parameter._DO) As Parameter._DO Implements [Interface].IDO.GetAttribute
        Try
            Dim DODA As New SQLEngine.LoanOrg._DO
            Return DODA.GetAttribute(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Sub DOSave(ByVal customClass As Parameter._DO, ByVal oData1 As DataTable, ByVal oData2 As DataTable, ByVal oData3 As DataTable, ByVal oData4 As DataTable, ByVal oData5 As DataTable) Implements [Interface].IDO.DOSave
        Dim DODA As New SQLEngine.LoanOrg._DO
        DODA.DOSave(customClass, oData1, oData2, oData3, oData4, oData5)
    End Sub

    Function CheckSerial(ByVal oCustomClass As Parameter._DO) As Parameter._DO Implements [Interface].IDO.CheckSerial
        Try
            Dim DODA As New SQLEngine.LoanOrg._DO
            Return DODA.CheckSerial(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function CheckAttribute(ByVal oCustomClass As Parameter._DO) As Parameter._DO Implements [Interface].IDO.CheckAttribute
        Try
            Dim DODA As New SQLEngine.LoanOrg._DO
            Return DODA.CheckAttribute(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Function CheckAssetDoc(ByVal oCustomClass As Parameter._DO) As Parameter._DO Implements [Interface].IDO.CheckAssetDoc
        Try
            Dim DODA As New SQLEngine.LoanOrg._DO
            Return DODA.CheckAssetDoc(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Sub ApplicationReturnUpdate(ByVal oCustomClass As Parameter._DO) Implements [Interface].IDO.ApplicationReturnUpdate
        Dim DODA As New SQLEngine.LoanOrg._DO
        DODA.ApplicationReturnUpdate(oCustomClass)
    End Sub
End Class
