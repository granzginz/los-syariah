
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class Invoice
    Implements IInvoice
    Function InvoicePaging(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice Implements [Interface].IInvoice.InvoicePaging
        Try
            Dim InvoiceDA As New SQLEngine.LoanOrg.Invoice
            Return InvoiceDA.InvoicePaging(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function ListRecATPM(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice Implements [Interface].IInvoice.ListRecATPM
        Try
            Dim InvoiceDA As New SQLEngine.LoanOrg.Invoice
            Return InvoiceDA.GetInvoiceATPMList(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetInvoiceATPMList(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice Implements [Interface].IInvoice.GetInvoiceATPMList
        Try
            Dim InvoiceDA As New SQLEngine.LoanOrg.Invoice
            Return InvoiceDA.GetInvoiceATPMList(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetInvoiceAgreementList(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice Implements [Interface].IInvoice.GetInvoiceAgreementList
        Try
            Dim InvoiceDA As New SQLEngine.LoanOrg.Invoice
            Return InvoiceDA.GetInvoiceAgreementList(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetInvoiceATPMAgreementList(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice Implements [Interface].IInvoice.GetInvoiceATPMAgreementList
        Try
            Dim InvoiceDA As New SQLEngine.LoanOrg.Invoice
            Return InvoiceDA.GetInvoiceATPMAgreementList(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetInvoiceMaxBackDate(ByVal customClass As Parameter.Invoice) As Integer Implements [Interface].IInvoice.GetInvoiceMaxBackDate
        Try
            Dim InvoiceDA As New SQLEngine.LoanOrg.Invoice
            Return InvoiceDA.GetInvoiceMaxBackDate(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetInvoiceSuppTopDays(ByVal customClass As Parameter.Invoice) As Integer Implements [Interface].IInvoice.GetInvoiceSuppTopDays
        Try
            Dim InvoiceDA As New SQLEngine.LoanOrg.Invoice
            Return InvoiceDA.GetInvoiceSuppTopDays(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function InvoiceSave(ByVal customClass As Parameter.Invoice) As String Implements [Interface].IInvoice.InvoiceSave
        Dim InvoiceDA As New SQLEngine.LoanOrg.Invoice
        Return InvoiceDA.InvoiceSave(customClass)
    End Function
    Public Function InvoiceATPMSave(ByVal customClass As Parameter.Invoice) As String Implements [Interface].IInvoice.InvoiceATPMSave
        Dim InvoiceDA As New SQLEngine.LoanOrg.Invoice
        Return InvoiceDA.InvoiceATPMSave(customClass)
    End Function


    Public Function LoadCombo(cnn As String, ty As String, Optional id As String = "") As IList(Of Parameter.CommonValueText) Implements [Interface].IInvoice.LoadCombo
        Return (New SQLEngine.LoanOrg.Invoice).LoadCombo(cnn, ty, id)
    End Function

    Public Function InvoiceSuppSave(cnn As String, ByVal invoiceSupp As Parameter.InvoiceSupp) As String Implements [Interface].IInvoice.InvoiceSuppSave
        Return (New SQLEngine.LoanOrg.Invoice).InvoiceSuppSave(cnn, invoiceSupp)
    End Function
    Function GetNoInvoiceATPM(ByVal customClass As Parameter.Invoice) As Parameter.Invoice Implements [Interface].IInvoice.GetNoInvoiceATPM
        Try
            Dim InvATPMDA As New SQLEngine.LoanOrg.Invoice
            Return InvATPMDA.GetNoInvoiceATPM(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Function GetInvoiceNo(ByVal customClass As Parameter.Invoice) As Parameter.Invoice Implements [Interface].IInvoice.GetInvoiceNo
        Try
            Dim InvATPMDA As New SQLEngine.LoanOrg.Invoice
            Return InvATPMDA.GetInvoiceNo(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function ReceiveATPMList(ByVal customClass As Parameter.Invoice) As Parameter.Invoice Implements [Interface].IInvoice.ReceiveATPMList
        Try
            Dim InvATPMRec As New SQLEngine.LoanOrg.Invoice
            Return InvATPMRec.ReceiveATPMList(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Sub SaveATPMOtorisasi(ByVal customclass As Parameter.Invoice) Implements [Interface].IInvoice.SaveATPMOtorisasi
        Dim DASaveATPMOtorisasi As New SQLEngine.LoanOrg.Invoice
        DASaveATPMOtorisasi.SaveATPMOtorisasi(customclass)
    End Sub

    Public Sub RejectOtorisasi(ByVal customclass As Parameter.Invoice) Implements [Interface].IInvoice.RejectOtorisasi
        Dim DARejectOtorisasi As New SQLEngine.LoanOrg.Invoice
        DARejectOtorisasi.RejectOtorisasi(customclass)
    End Sub
    Public Sub EditOtorisasi(ByVal customclass As Parameter.Invoice) Implements [Interface].IInvoice.EditOtorisasi
        Dim DAEditOtorisasi As New SQLEngine.LoanOrg.Invoice
        DAEditOtorisasi.EditOtorisasi(customclass)
    End Sub
End Class
