
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class CreditScoring
    Implements ICreditScoring
     
    Function CreditScoringPaging(ByVal oCustomClass As Parameter.CreditScoring) As Parameter.CreditScoring Implements [Interface].ICreditScoring.CreditScoringPaging
        Try
            Dim CreditScoringDA As New SQLEngine.LoanOrg.CreditScoring
            Return CreditScoringDA.CreditScoringPaging(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function CreditScoringSaveAdd(ByVal customClass As Parameter.CreditScoring) As String Implements [Interface].ICreditScoring.CreditScoringSaveAdd
        Dim CreditScoringDA As New SQLEngine.LoanOrg.CreditScoring
        Return CreditScoringDA.CreditScoringSaveAdd(customClass)
    End Function


    Public Function CreditScoringSaveAdd1(ByVal oCustomClass As Parameter.CreditScoring, ByVal isSavingGrid As Boolean) As String Implements [Interface].ICreditScoring.CreditScoringSaveAdd
        Dim CreditScoringDA As New SQLEngine.LoanOrg.CreditScoring
        Return CreditScoringDA.CreditScoringSaveAdd(oCustomClass, isSavingGrid)
    End Function

    Public Function DoCreaditScoringSave(cnn As String, loginId As String, branchid As String, AppId As String, scoreResult As Parameter.ScoreResults, bnsDate As Date) As String Implements [Interface].ICreditScoring.DoCreaditScoringSave
        Dim CreditScoringDA As New SQLEngine.LoanOrg.CreditScoring
        Return CreditScoringDA.DoCreaditScoringSave(cnn, loginId, branchid, AppId, scoreResult, bnsDate)
    End Function



    Public Function GetCreditScorePolicyResult(ByVal oCustomClass As Parameter.CreditScoring) As Parameter.CreditScoring Implements [Interface].ICreditScoring.GetCreditScorePolicyResult
        Dim CreditScoringDA As New SQLEngine.LoanOrg.CreditScoring
        Return CreditScoringDA.GetCreditScorePolicyResult(oCustomClass)
    End Function


    Public Function crDispositionCreditRpt(cnn As String, whereCond As String(), tp As String) As System.Data.DataSet Implements [Interface].ICreditScoring.CrDispositionCreditRpt
        Dim CreditScoringDA As New SQLEngine.LoanOrg.CreditScoring
        Return CreditScoringDA.crDispositionCreditRpt(cnn, whereCond, tp)
    End Function

    Public Function DoCreditScoringProceed(cnn As String, branchId As String, prospectAppId As String, ScoringType As String, bnsDate As Date) As String Implements [Interface].ICreditScoring.DoCreditScoringProceed
        Dim DA As New SQLEngine.LoanOrg.CreditScoring
        Return DA.DoCreditScoringProceed(cnn, branchId, prospectAppId, ScoringType, bnsDate)
    End Function

    Public Function GetApprovalCreditScoring(ByVal oCustomClass As Parameter.CreditScoring, ByVal strApproval As String) Implements [Interface].ICreditScoring.GetApprovalCreditScoring
        Dim DA As New SQLEngine.LoanOrg.CreditScoring
        Return DA.GetApprovalCreditScoring(oCustomClass, strApproval)
    End Function
End Class
