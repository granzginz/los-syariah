﻿
#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class CustomerCases : Inherits ComponentBase
    Implements ICustomerCases
    Dim oSQLE As New SQLEngine.LoanOrg.CustomerCases

    Public Function CustomerCasesDelete(ByVal oCustomClass As Parameter.CustomerCases) As String Implements [Interface].ICustomerCases.CustomerCasesDelete
        Return oSQLE.CustomerCasesDelete(oCustomClass)
    End Function

    Public Function CustomerCasesSaveAdd(ByVal oCustomClass As Parameter.CustomerCases) As String Implements [Interface].ICustomerCases.CustomerCasesSaveAdd
        Return oSQLE.CustomerCasesSaveAdd(oCustomClass)
    End Function

    Public Sub CustomerCasesSaveEdit(ByVal oCustomClass As Parameter.CustomerCases) Implements [Interface].ICustomerCases.CustomerCasesSaveEdit
        oSQLE.CustomerCasesSaveEdit(oCustomClass)
    End Sub

    Public Function GetCustomerCases(ByVal oCustomClass As Parameter.CustomerCases) As Parameter.CustomerCases Implements [Interface].ICustomerCases.GetCustomerCases
        Return oSQLE.GetCustomerCases(oCustomClass)
    End Function

    Public Function GetCustomerCasesList(ByVal oCustomClass As Parameter.CustomerCases) As Parameter.CustomerCases Implements [Interface].ICustomerCases.GetCustomerCasesList
        Return oSQLE.GetCustomerCasesList(oCustomClass)
    End Function

    Public Function GetComboCases(ByVal customclass As Parameter.CustomerCases) As System.Data.DataTable Implements [Interface].ICustomerCases.GetComboCases
        Return oSQLE.GetComboCases(customclass)
    End Function
End Class
