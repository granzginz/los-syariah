﻿#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class CustomerGroupPIC : Inherits ComponentBase
    Implements ICustomerGroupPIC
    Dim oSQLE As New SQLEngine.LoanOrg.CustomerGroupPIC

    Public Function CustomerGroupPICDelete(ByVal oCustomClass As Parameter.CustomerGroupPIC) As String Implements [Interface].ICustomerGroupPIC.CustomerGroupPICDelete
        Return oSQLE.CustomerGroupPICDelete(oCustomClass)
    End Function

    Public Function CustomerGroupPICSaveAdd(ByVal oCustomClass As Parameter.CustomerGroupPIC) As String Implements [Interface].ICustomerGroupPIC.CustomerGroupPICSaveAdd
        Return oSQLE.CustomerGroupPICSaveAdd(oCustomClass)
    End Function

    Public Sub CustomerGroupPICSaveEdit(ByVal oCustomClass As Parameter.CustomerGroupPIC) Implements [Interface].ICustomerGroupPIC.CustomerGroupPICSaveEdit
        oSQLE.CustomerGroupPICSaveEdit(oCustomClass)
    End Sub

    Public Function GetCustomerGroupPIC(ByVal oCustomClass As Parameter.CustomerGroupPIC) As Parameter.CustomerGroupPIC Implements [Interface].ICustomerGroupPIC.GetCustomerGroupPIC
        Return oSQLE.GetCustomerGroupPIC(oCustomClass)
    End Function

    Public Function GetCustomerGroupPICList(ByVal oCustomClass As Parameter.CustomerGroupPIC) As Parameter.CustomerGroupPIC Implements [Interface].ICustomerGroupPIC.GetCustomerGroupPICList
        Return oSQLE.GetCustomerGroupPICList(oCustomClass)
    End Function

    Public Function GetCustomerGroupPICListByCustGroupID(ByVal oCustomClass As Parameter.CustomerGroupPIC) As Parameter.CustomerGroupPIC Implements [Interface].ICustomerGroupPIC.GetCustomerGroupPICListByCustGroupID
        Return oSQLE.GetCustomerGroupPICListByCustGroupID(oCustomClass)
    End Function

    Public Function GetCustomerGroupPICReport(ByVal oCustomClass As Parameter.CustomerGroupPIC) As Parameter.CustomerGroupPIC Implements [Interface].ICustomerGroupPIC.GetCustomerGroupPICReport
        Return oSQLE.GetCustomerGroupPICReport(oCustomClass)
    End Function
End Class
