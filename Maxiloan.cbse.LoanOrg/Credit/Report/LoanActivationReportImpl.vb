Imports Maxiloan.SQLEngine.LoanOrg
Imports Maxiloan.Interface

Public Class LoanActivationReportImpl : Inherits ComponentBase
    Implements ILoanActivationReport


    Public Function listLoanActivationReport(ByVal customClass As Parameter.LoanActivationReport) As Parameter.LoanActivationReport Implements [Interface].ILoanActivationReport.listLoanActivationReport
        Dim oLoanActivationReportSQLE As New SQLEngine.LoanOrg.LoanActivationReportSQLE
        Return oLoanActivationReportSQLE.getLoanActivationList(customClass)
    End Function

    Public Function listDailySalesReport(ByVal customClass As Parameter.LoanActivationReport) As Parameter.LoanActivationReport Implements [Interface].ILoanActivationReport.listDailySalesReport
        Dim oLoanActivationReportSQLE As New SQLEngine.LoanOrg.LoanActivationReportSQLE
        Return oLoanActivationReportSQLE.getDailySalesReportList(customClass)
    End Function

    Public Function listAgreementDownloadReport(ByVal customClass As Parameter.LoanActivationReport) As Parameter.LoanActivationReport Implements [Interface].ILoanActivationReport.listAgreementDownloadReport
        Dim oLoanActivationReportSQLE As New SQLEngine.LoanOrg.LoanActivationReportSQLE
        Return oLoanActivationReportSQLE.getAgreementDownloadList(customClass)
    End Function
End Class
