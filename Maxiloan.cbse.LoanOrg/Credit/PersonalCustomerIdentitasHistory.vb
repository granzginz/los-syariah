﻿
#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class PersonalCustomerIdentitasHistory : Inherits ComponentBase
    Implements IPersonalCustomerIdentitasHistory

    Dim oSQLE As New SQLEngine.LoanOrg.PersonalCustomerIdentitasHistory


    Public Function PersonalCustomerIdentitasHistorySaveAdd1(ByVal oCustomClass As Parameter.Customer) As String Implements [Interface].IPersonalCustomerIdentitasHistory.PersonalCustomerIdentitasHistorySaveAdd
        Return oSQLE.PersonalCustomerIdentitasHistorySaveAdd(oCustomClass)
    End Function
End Class
