﻿
#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class TrackingTVC : Inherits ComponentBase
    Implements ITrackingTVC


    Dim oSQLE As New SQLEngine.LoanOrg.TrackingTVC

    Public Function GetApplicatioList(ByVal customclass As Parameter.Application) As System.Data.DataTable Implements [Interface].ITrackingTVC.GetCombo
        Return oSQLE.GetCombo(customclass)
    End Function
    Public Sub ApplicationSaveEdit(ByVal oCustomClass As Parameter.Application) Implements [Interface].ITrackingTVC.ApplicationSaveEdit
        oSQLE.ApplicationSaveEdit(oCustomClass)
    End Sub
    Public Function GetApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].ITrackingTVC.GetApplication
        Return oSQLE.GetApplication(oCustomClass)
    End Function

    Public Function GetApplicatioList1(ByVal oCustomClass As Parameter.Application) As Parameter.Application Implements [Interface].ITrackingTVC.GetApplicatioList
        Return oSQLE.GetApplicatioList(oCustomClass)
    End Function
End Class
