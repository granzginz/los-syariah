﻿
#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class CustomerGroup : Inherits ComponentBase
    Implements ICustomerGroup
    Dim oSQLE As New SQLEngine.LoanOrg.CustomerGroup

    Public Function CustomerGroupDelete(ByVal oCustomClass As Parameter.CustomerGroup) As String Implements [Interface].ICustomerGroup.CustomerGroupDelete
        Return oSQLE.CustomerGroupDelete(oCustomClass)
    End Function

    Public Function CustomerGroupSaveAdd(ByVal oCustomClass As Parameter.CustomerGroup) As String Implements [Interface].ICustomerGroup.CustomerGroupSaveAdd
        Return oSQLE.CustomerGroupSaveAdd(oCustomClass)
    End Function

    Public Sub CustomerGroupSaveEdit(ByVal oCustomClass As Parameter.CustomerGroup) Implements [Interface].ICustomerGroup.CustomerGroupSaveEdit
        oSQLE.CustomerGroupSaveEdit(oCustomClass)
    End Sub

    Public Function GetCustomerGroup(ByVal oCustomClass As Parameter.CustomerGroup) As Parameter.CustomerGroup Implements [Interface].ICustomerGroup.GetCustomerGroup
        Return oSQLE.GetCustomerGroup(oCustomClass)
    End Function

    Public Function GetCustomerGroupListCustomer(ByVal oCustomClass As Parameter.CustomerGroup) As Parameter.CustomerGroup Implements [Interface].ICustomerGroup.GetCustomerGroupListCustomer
        Return oSQLE.GetCustomerGroupListCustomer(oCustomClass)
    End Function

    Public Function GetCustomerGroupList(ByVal oCustomClass As Parameter.CustomerGroup) As Parameter.CustomerGroup Implements [Interface].ICustomerGroup.GetCustomerGroupList
        Return oSQLE.GetCustomerGroupList(oCustomClass)
    End Function

    Public Function GetCustomerGroupReport(ByVal oCustomClass As Parameter.CustomerGroup) As Parameter.CustomerGroup Implements [Interface].ICustomerGroup.GetCustomerGroupReport
        Return oSQLE.GetCustomerGroupReport(oCustomClass)
    End Function
End Class
