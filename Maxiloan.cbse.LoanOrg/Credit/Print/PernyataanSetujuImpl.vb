Imports maxiloan.SQLEngine.LoanOrg
Imports Maxiloan.Interface

Public Class PernyataanSetujuImpl : Inherits ComponentBase
    Implements IPernyataanSetuju

    Public Function listPernyataanSetuju(ByVal customClass As Parameter.PernyataanSetuju) As Parameter.PernyataanSetuju Implements [Interface].IPernyataanSetuju.listPernyataanSetuju
        Dim daPernyataanSetuju As New SQLEngine.LoanOrg.PernyataanSetujuSQLE
        Return daPernyataanSetuju.listPernyataanSetuju(customClass)
    End Function

    Public Function savePrintPernyataanSetuju(ByVal customClass As Parameter.PernyataanSetuju) As Parameter.PernyataanSetuju Implements [Interface].IPernyataanSetuju.savePrintPernyataanSetuju
        Dim daPernyataanSetuju As New SQLEngine.LoanOrg.PernyataanSetujuSQLE
        Return daPernyataanSetuju.savePrintPernyataanSetuju(customClass)
    End Function

End Class
