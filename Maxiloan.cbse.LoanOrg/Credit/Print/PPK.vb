

#Region "Imports"
Imports Maxiloan.Exceptions
Imports maxiloan.SQLEngine.LoanOrg
Imports Maxiloan.Interface
#End Region

Public Class PPK : Inherits ComponentBase
    Implements IPPK


#Region "List Agreement "
    Public Function ListAgreement(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListAgreement
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListAgreement(customclass)
    End Function

    Public Function ListAgreementFL(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListAgreementFL
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListAgreementFL(customclass)
    End Function
#End Region
#Region "Save Print"
    Public Function SavePrint(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.SavePrint
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.SavePrint(customclass)
    End Function

    Public Function SavePrintFL(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.SavePrintFL
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.SavePrintFL(customclass)
    End Function
#End Region
#Region "List PPK Report "
    Public Function ListPPKReport(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListPPKReport
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListPPKReport(customclass)
    End Function

    Public Function ListPPKReportFL(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListPPKReportFL
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListPPKReportFL(customclass)
    End Function
#End Region

    '========================= Print Surat Kuasa ===============================
#Region "Save Print Surat Kuasa"
    Public Function SavePrintSuratKuasa(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.SavePrintSuratKuasa
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.SavePrintSuratKuasa(customclass)
    End Function
#End Region
#Region "List Surat Kuasa Report"
    Public Function ListSuratKuasaReport(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListSuratKuasaReport
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListSuratKuasaReport(customclass)
    End Function
#End Region

    '========================= Print PO ========================
#Region "Save Print PO"
    Public Function SavePrintPO(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.SavePrintPO
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.SavePrintPO(customclass)
    End Function
#End Region
#Region "List Report PO "
    Public Function ListReportPO(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportPO
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListReportPO(customclass)
    End Function
#End Region

    '========================== Print surat Kwitansi =========================
#Region "Save Print Kwitansi"
    Public Function SavePrintKwitansi(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.SavePrintKwitansi
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.SavePrintKwitansi(customclass)
    End Function
#End Region
#Region "List Print Kwitansi"
    Public Function ListPrintKwitansi(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListPrintKwitansi
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListPrintKwitansi(customclass)
    End Function
#End Region

    '========================== Print Slip Setoran Bank =======================
#Region "ListAccount"
    Public Function ListAccount(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListAccount
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListAccount(customclass)
    End Function
#End Region
#Region " Save Print Slip Setoran Bank"
    Public Function SavePrintSlipSetoranBank(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.SavePrintSlipSetoranBank
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.SaveSlipSetoranBank(customclass)
    End Function
#End Region
#Region "List Print SSB"
    Public Function ListPrintSSB(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListPrintSSB
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListPrintSSB(customclass)
    End Function
#End Region
#Region "GetBankID"
    Public Function GetBankID(ByVal customclass As Parameter.PPK) As String Implements [Interface].IPPK.GetBankID
        Dim DAAssetModel As New SQLEngine.LoanOrg.PPK
        Return DAAssetModel.GetBankID(customclass)
    End Function
#End Region

    '========================== ReportAgingByModel ================================
#Region "GetAssetModel"
    Public Function GetAssetModel(ByVal customclass As Parameter.PPK) As DataTable Implements [Interface].IPPK.GetAssetModel
        Dim DAAssetModel As New SQLEngine.LoanOrg.PPK
        Return DAAssetModel.GetAssetModel(customclass)
    End Function
#End Region
#Region "viewReportAging"
    Public Function ViewReportAging(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ViewReportAging
        Dim DAReportAging As New SQLEngine.LoanOrg.PPK
        Return DAReportAging.ViewReportAging(customclass)
    End Function
#End Region
#Region "GetAssetType"
    Public Function GetAssetType(ByVal customclass As Parameter.PPK) As DataTable Implements [Interface].IPPK.GetAssetType
        Dim DAAssetType As New SQLEngine.LoanOrg.PPK
        Return DAAssetType.GetAssetType(customclass)
    End Function
#End Region

    '========================== ReportCreditScoringPersonalEmployee ================================
#Region "GetCreditScoringCustomer"
    Public Function GetCreditScoringCustomer(ByVal customclass As Parameter.PPK) As DataTable Implements [Interface].IPPK.GetCreditScoringCustomer
        Dim DACreditScoringCustomer As New SQLEngine.LoanOrg.PPK

        Select Case customclass.CustomerType
            Case "N"
                Return DACreditScoringCustomer.GetCreditScoringCustomerEnt(customclass)
            Case "P"
                Return DACreditScoringCustomer.GetCreditScoringCustomerProf(customclass)
            Case Else
                Return DACreditScoringCustomer.GetCreditScoringCustomer(customclass)
        End Select

    End Function
#End Region

    '========================== ReportAmortization ================================
#Region "ListRptAmortization"
    Public Function ListRptAmortization(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListRptAmortization
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListRptAmortization(customclass)
    End Function
#End Region

    '========================== Report Confirmation Letter ==========================
#Region "ListReportConfLetter"
    Public Function ListReportConfLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportConfLetter
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportConfLetter(customclass)
    End Function
#End Region
#Region "List Conf Letter"
    Public Function ListConfLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListConfLetter
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListConfLetter(customclass)
    End Function
#End Region
#Region "List Waad Letter"
    Public Function ListWaadLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListWaadLetter
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListWaadLetter(customclass)
    End Function
#End Region
#Region "List Wakalah Letter"
    Public Function ListWakalahLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListWakalahLetter
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListWakalahLetter(customclass)
    End Function
#End Region
#Region "ListReportWakalahLetter"
    Public Function ListReportWakalahLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportWakalahLetter
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListReportWakalahLetter(customclass)
    End Function
#End Region
#Region "List PKS"
    Public Function ListPKS(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListPKS
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListPKS(customclass)
    End Function
#End Region
#Region "Save Print Confirmation Letter"
    Public Function SavePrintConfLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.SavePrintConfLetter
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.SavePrintConfLetter(customclass)
    End Function
#End Region
#Region "Save Print Waad Letter"
    Public Function SavePrintWaadLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.SavePrintWaadLetter
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.SavePrintWaadLetter(customclass)
    End Function
#End Region
#Region "Save Print Wakalah Letter"
    Public Function SavePrintWakalahLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.SavePrintWakalahLetter
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.SavePrintWakalahLetter(customclass)
    End Function
#End Region

    '=========================== Report Bon Hijau ====================================
#Region "List Bon Hijau"
    Public Function ListBonHijau(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListBonHijau
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListBonHijau(customclass)
    End Function
#End Region
#Region "List Bon Hijau Report"
    Public Function ListBonHijauReport(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListBonHijauReport
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListBonHijauReport(customclass)
    End Function
#End Region
#Region "Save Print Bon Hijau"
    Public Function SavePrintBonHijau(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.SavePrintBonHijau
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.SavePrintBonHijau(customclass)
    End Function
#End Region

    '========================== Report Tanda Terima Dokumen ==========================
#Region "ListReportTandaTerimaDokumen"
    Public Function ListReportTandaTerimaDokumen(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportTandaTerimaDokumen
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportTandaTerimaDokumen(customclass)
    End Function
#End Region

    Public Function ListReportPernyataanNPWP(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportPernyataanNPWP
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportPernyataanNPWP(customclass)
    End Function
    Public Function ListReportSuratPernyataanNasabah(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportSuratPernyataanNasabah
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportSuratPernyataanNasabah(customclass)
    End Function
    Public Function ListReportPernyataanBedaTandaTangan(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportPernyataanBedaTandaTangan
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportPernyataanBedaTandaTangan(customclass)
    End Function

    Public Function ListReportSuratPenolakan(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportSuratPenolakan
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportSuratPenolakan(customclass)
    End Function

    Public Function ListReportBastk(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportBastk
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportBastk(customclass)
    End Function

    Public Function ListReportsptransfer(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportSpTransfer
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportSptransfer(customclass)
    End Function

    Public Function ListReportGesekanRangkaMesin(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportGesekanRangkaMesin
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportGesekanRangkaMesin(customclass)
    End Function

#Region "ListTandaTerimaDokumen"
    Public Function ListTandaTerimaDokumen(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListTandaTerimaDokumen
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListTandaTerimaDokumen(customclass)
    End Function
#End Region
    Public Function ListSuratPernyataanNasabah(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListSuratPernyataanNasabah
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListSuratPernyataanNasabah(customclass)
    End Function
    Public Function ListPernyataanNPWP(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListPernyataanNPWP
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListPernyataanNPWP(customclass)
    End Function

    Public Function ListPernyataanBedaTandaTangan(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListPernyataanBedaTandaTangan
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListPernyataanBedaTandaTangan(customclass)
    End Function

    Public Function ListSuratPenolakan(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListSuratPenolakan
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListSuratPenolakan(customclass)
    End Function

    Public Function ListBastk(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListBastk
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListBastk(customclass)
    End Function

    Public Function ListSuratPermohonanTransfer(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListSuratPermohonanTransfer
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListSuratPermohonanTransfer(customclass)
    End Function

    Public Function ListGesekanRangkaMesin(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListGesekanRangkaMesin
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListGesekanRangkaMesin(customclass)
    End Function

    '========================== Report Kelengkapan Data ==========================
#Region "ListReportKelengkapanData"
    Public Function ListReportKelengkapanData(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportKelengkapanData
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportKelengkapanData(customclass)
    End Function
#End Region
#Region "ListKelengkapanData"
    Public Function ListKelengkapanData(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListKelengkapanData
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListKelengkapanData(customclass)
    End Function
#End Region

    '========================== Report Listing Surat Kontrak ========================
#Region "GetCMO"
    Public Function GetCMO(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.GetCMO
        Dim DAAssetModel As New SQLEngine.LoanOrg.PPK
        Return DAAssetModel.GetCMO(customclass)
    End Function
#End Region
#Region "ListReportListingSuratKontrak"
    Public Function ListReportListingSuratKontrak(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportListingSuratKontrak
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportListingSuratKontrak(customclass)
    End Function
#End Region
#Region "ListListingSuratKontrak"
    Public Function ListListingSuratKontrak(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListListingSuratKontrak
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListListingSuratKontrak(customclass)
    End Function
#End Region

    '============================= Report OM =====================
#Region "ListReportOM"
    Public Function ListReportOM(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportOM
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportOM(customclass)
    End Function
#End Region

    Public Function ListPrintBASTK(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListPrintBASTK
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListPrintBASTK(customclass)
    End Function
    Public Function ListAccountByPayment(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListAccountByPayment
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListAccountByPayment(customclass)
    End Function

    '========================== Report Persetujuan Pengalihan Kreditor ==========================
#Region "ListReportPPKreditor"
    Public Function ListReportPPKreditor(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportPPKreditor
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportPPKreditor(customclass)
    End Function
#End Region
#Region "List PPKreditor"
    Public Function ListPPKreditor(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListPPKreditor
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListPPKreditor(customclass)
    End Function
#End Region
#Region "Save Print PPKreditor"
    Public Function SavePrintPPKreditor(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.SavePrintPPKreditor
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.SavePrintPPKreditor(customclass)
    End Function
#End Region
    Public Function SavePrintSuratPernyataanNasabah(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.SavePrintSuratPernyataanNasabah
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.SavePrintSuratPernyataanNasabah(customclass)
    End Function

    '========================== Report Welcome Letter ==========================
#Region "ListReportWLetter"
    Public Function ListReportWLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportWLetter
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportWLetter(customclass)
    End Function
#End Region
#Region "List WLetter"
    Public Function ListWLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListWLetter
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListWLetter(customclass)
    End Function
#End Region
#Region "Save Print WLetter"
    Public Function SavePrintWLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.SavePrintWLetter
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.SavePrintWLetter(customclass)
    End Function
#End Region
#Region "ListReportWLetterSubReport"
    Public Function ListReportWLetterSubRpt(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportWLetterSubRpt
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportWLetterSubRpt(customclass)
    End Function
#End Region

    '========================== Report Surat Kuasa Eksekusi dan Penjualan Obyek Jaminan Fidusia ==========================
#Region "ListReportSKEPO"
    Public Function ListReportSKEPO(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportSKEPO
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportSKEPO(customclass)
    End Function
#End Region
#Region "List SKEPO"
    Public Function ListSKEPO(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListSKEPO
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListSKEPO(customclass)
    End Function
#End Region
#Region "Save Print SKEPO"
    Public Function SavePrintSKEPO(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.SavePrintSKEPO
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.SavePrintSKEPO(customclass)
    End Function
#End Region

    '========================= Print PO Karoseri========================
#Region "Save Print PO Karoseri"
    Public Function spSavePrintPOKaroseri(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.SavePrintPOKaroseri
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.SavePrintPOKaroseri(customclass)
    End Function
#End Region
#Region "List Report PO Karoseri"
    Public Function ListReportPOKaroseri(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportPOKaroseri
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListReportPOKaroseri(customclass)
    End Function
#End Region

    '========================== Report SK Potong Gaji ==========================
#Region "ListReportSKPotongGaji"
    Public Function ListReportSKPotongGaji(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportSKPotongGaji
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportSKPotongGaji(customclass)
    End Function
#End Region
#Region "List SKPotongGaji"
    Public Function ListSKPotongGaji(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListSKPotongGaji
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.ListSKPotongGaji(customclass)
    End Function
#End Region
#Region "Save Print SKPotongGaji"
    Public Function SavePrintSKPotongGaji(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.SavePrintSKPotongGaji
        Dim DAPPK As New SQLEngine.LoanOrg.PPK
        Return DAPPK.SavePrintSKPotongGaji(customclass)
    End Function
#End Region
#Region "ListReportSKPotongGajiSubReport"
    Public Function ListReportSKPotongGajiSubRpt(ByVal customclass As Parameter.PPK) As Parameter.PPK Implements [Interface].IPPK.ListReportSKPotongGajiSubRpt
        Dim DARptAmortization As New SQLEngine.LoanOrg.PPK
        Return DARptAmortization.ListReportSKPotongGajiSubRpt(customclass)
    End Function
#End Region

End Class

