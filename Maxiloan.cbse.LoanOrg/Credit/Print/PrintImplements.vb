Imports Maxiloan.SQLEngine.LoanOrg
Imports Maxiloan.Interface

Public Class PrintImplements
    Implements IPrint


    Public Function getAgentFeeReceipt(ByVal customClass As Parameter.AgentFeeReceipt) As Parameter.AgentFeeReceipt Implements [Interface].IPrint.getAgentFeeReceipt
        Dim oSQLE As New SQLEngine.LoanOrg.PrintSQLE
        Return oSQLE.execGetAgentFeeReceipt(customClass)
    End Function
    Public Function saveAgentFeeReceiptPrint(ByVal customClass As Parameter.AgentFeeReceipt) As Parameter.AgentFeeReceipt Implements [Interface].IPrint.saveAgentFeeReceiptPrint
        Dim oSQLE As New SQLEngine.LoanOrg.PrintSQLE
        Return oSQLE.execSaveAgentFeeReceiptPrint(customClass)
    End Function

    Public Function getCreditFundingReceipt(ByVal customClass As Parameter.CreditFundingReceipt) As Parameter.CreditFundingReceipt Implements [Interface].IPrint.getCreditFundingReceipt
        Dim oSQLE As New SQLEngine.LoanOrg.PrintSQLE
        Return oSQLE.execGetCreditFundingReceipt(customClass)
    End Function

    Public Function saveCreditFundingReceiptPrint(ByVal customClass As Parameter.CreditFundingReceipt) As Parameter.CreditFundingReceipt Implements [Interface].IPrint.saveCreditFundingReceiptPrint
        Dim oSQLE As New SQLEngine.LoanOrg.PrintSQLE
        Return oSQLE.execSaveCreditFundingReceiptPrint(customClass)
    End Function

    Public Function getSuratKuasaFiducia(ByVal customClass As Parameter.SuratKuasaFiducia) As Parameter.SuratKuasaFiducia Implements [Interface].IPrint.getSuratKuasaFiducia
        Dim oSQLE As New SQLEngine.LoanOrg.PrintSQLE
        Return oSQLE.execGetSuratKuasaFiducia(customClass)
    End Function
    Public Function saveSuratKuasaPrint(ByVal customClass As Parameter.SuratKuasaFiducia) As Parameter.SuratKuasaFiducia Implements [Interface].IPrint.saveSuratKuasaPrint
        Dim oSQLE As New SQLEngine.LoanOrg.PrintSQLE
        Return oSQLE.execSaveSuratKuasaFidusiaPrint(customClass)
    End Function

    Public Function getMKK(ByVal customClass As Parameter.Common) As System.Data.DataSet Implements [Interface].IPrint.getMKK
        Dim oSQLE As New SQLEngine.LoanOrg.PrintSQLE
        Return oSQLE.getMKK(customClass)
    End Function


    Public Function getMKKReport(ByVal customClass As Parameter.MKK) As Parameter.MKK Implements [Interface].IPrint.getMKKReport
        Dim oSQLE As New SQLEngine.LoanOrg.PrintSQLE
        Return oSQLE.getMKKReport(customClass)
    End Function

    Public Function listAgreementMailForPrint(ByVal customClass As Parameter.AgreementList) As System.Data.DataTable Implements [Interface].IPrint.listAgreementMailForPrint
        Dim oSQLE As New SQLEngine.LoanOrg.PrintSQLE
        Return oSQLE.listAgreementMailForPrint(customClass)
    End Function

    Public Function saveAgreementMailForPrint(ByVal customClass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IPrint.saveAgreementMailForPrint
        Dim oSQLE As New SQLEngine.LoanOrg.PrintSQLE
        Return oSQLE.saveAgreementMailForPrint(customClass)
    End Function

    Public Function printAgreementMailForPrint(ByVal customClass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IPrint.printAgreementMailForPrint
        Dim oSQLE As New SQLEngine.LoanOrg.PrintSQLE
        Return oSQLE.printAgreementMailForPrint(customClass)
    End Function
End Class
