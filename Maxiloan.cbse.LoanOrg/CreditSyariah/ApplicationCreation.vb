
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class ApplicationCreation
    Implements IApplicationCreation

    Function GetProspect(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation Implements [Interface].IApplicationCreation.GetProspect
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.GetProspect(oCustomClass)
    End Function
    Function ProspectSaveAdd(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation Implements [Interface].IApplicationCreation.ProspectSaveAdd
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.ProspectSaveAdd(oCustomClass)
    End Function
    Function ProspectSaveAsset(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation Implements [Interface].IApplicationCreation.ProspectSaveAsset
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.ProspectSaveAsset(oCustomClass)
    End Function
    Function ProspectSaveDemografi(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation Implements [Interface].IApplicationCreation.ProspectSaveDemografi
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.ProspectSaveDemografi(oCustomClass)
    End Function
    Function ProspectSaveFinancial(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation Implements [Interface].IApplicationCreation.ProspectSaveFinancial
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.ProspectSaveFinancial(oCustomClass)
    End Function
    Function GetViewProspect(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation Implements [Interface].IApplicationCreation.GetViewProspect
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.GetViewProspect(oCustomClass)
    End Function
    Function GetCbo(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation Implements [Interface].IApplicationCreation.GetCbo
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.GetCbo(oCustomClass)
    End Function
    Function GetCboGeneral(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation Implements [Interface].IApplicationCreation.GetCboGeneral
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.GetCboGeneral(oCustomClass)
    End Function

    Function DoProceeded(ByVal cnn As String, prospectId As String) As Boolean Implements [Interface].IApplicationCreation.DoProceeded
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.DoProceeded(cnn, prospectId)
    End Function


    Public Function DispositionCreditRpt(cnn As String, whereCond As String(), tp As String) As System.Data.DataSet Implements [Interface].IApplicationCreation.DispositionCreditRpt
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.DispositionCreditRpt(cnn, whereCond, tp)
    End Function

    Public Function DoProspectCreaditScoring(cnn As String, branchId As String, prospectAppId As String, ScoringType As String, bnsDate As Date) As String Implements [Interface].IApplicationCreation.DoProspectCreaditScoring
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.DoProspectCreaditScoring(cnn, branchId, prospectAppId, ScoringType, bnsDate)
    End Function


    Public Function GetProspectScorePolicyResult(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation Implements [Interface].IApplicationCreation.GetProspectScorePolicyResult
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.GetProspectScorePolicyResult(oCustomClass)
    End Function

    Public Function DoProspectDecision(cnn As String, prospectAppId As String, appr As Integer) Implements [Interface].IApplicationCreation.DoProspectDecision
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.DoProspectDecision(cnn, prospectAppId, appr)
    End Function

    Public Function MasterDataReviewRpt(cnn As String, whereCond As String(), tp As String) As System.Data.DataSet Implements [Interface].IApplicationCreation.MasterDataReviewRpt
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.MasterDataReviewRpt(cnn, whereCond, tp)
    End Function

    Function GetInitial(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation Implements [Interface].IApplicationCreation.GetInitial
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.GetInitial(oCustomClass)
    End Function

    Function InitialSaveAdd(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation Implements [Interface].IApplicationCreation.InitialSaveAdd
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.InitialSaveAdd(oCustomClass)
    End Function

    Function DoBIChecking(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation Implements [Interface].IApplicationCreation.DoBIChecking
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.DoBIChecking(oCustomClass)
    End Function

    Function DataSurveySaveAdd(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation Implements [Interface].IApplicationCreation.DataSurveySaveAdd
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.DataSurveySaveAdd(oCustomClass)
    End Function

    Function DataSurveySaveEdit(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation Implements [Interface].IApplicationCreation.DataSurveySaveEdit
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.DataSurveySaveEdit(oCustomClass)
    End Function

    Function ProspectReturnUpdate(ByVal oCustomClass As Parameter.AppCreation) As String Implements [Interface].IApplicationCreation.ProspectReturnUpdate
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.ProspectReturnUpdate(oCustomClass)
    End Function

    Function ProspectLogSave(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation Implements [Interface].IApplicationCreation.ProspectLogSave
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.ProspectLogSave(oCustomClass)
    End Function

    Function GetInqProspect(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation Implements [Interface].IApplicationCreation.GetInqProspect
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.GetInqProspect(oCustomClass)
    End Function

    Public Function DoProspectCreaditScoringProceed(cnn As String, branchId As String, prospectAppId As String, ScoringType As String, bnsDate As Date, proceed As Boolean) As String Implements [Interface].IApplicationCreation.DoProspectCreaditScoringProceed
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.DoProspectCreaditScoringProceed(cnn, branchId, prospectAppId, ScoringType, bnsDate, proceed)
    End Function

    Public Function GetApprovalprospect(ByVal oCustomClass As Parameter.AppCreation, ByVal strApproval As String) Implements [Interface].IApplicationCreation.GetApprovalprospect
        Dim DA As New SQLEngine.LoanOrg.ApplicationCreation
        Return DA.GetApprovalprospect(oCustomClass, strApproval)
    End Function

End Class

