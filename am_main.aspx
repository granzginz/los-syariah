﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="am_main.aspx.vb" Inherits="Maxiloan.Webform.am_main" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Maxiloan</title>
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <link href="Include/Main.css" type="text/css" rel="stylesheet" />
    <link href="Include/dhtmlxmenu_dhx_skyblue.css" type="text/css" rel="stylesheet" />
    <script src="dhtmlxcommon.js" type="text/javascript"></script>
    <script src="dhtmlxmenu.js" type="text/javascript"></script>
    <script src="dhtmlxmenu_ext.js" type="text/javascript"></script>
    <script type="text/javascript">
        function setContentHeight() {
            parent.document.getElementById('icontent').height = document['body'].offsetHeight - parent.document.getElementById('iheader').offsetHeight - parent.document.getElementById('menuObj').offsetHeight;
        }
    </script>
</head>
<body onload="initMenu();setContentHeight();" onresize="setContentHeight();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <iframe name="header" id="iheader" src="am_header.aspx" scrolling="no" frameborder="0">
        </iframe>
        <div id="outtext" runat="server">
        </div>
        <div style="width: 100%">
            <div id="menuObj">
            </div>
        </div>
         <iframe name="content" id="icontent" src="Webform.AppMgt/Forms/am_inbox_004.aspx"
            scrolling="yes" frameborder="0"></iframe>  
        <%--<iframe name="content" id="icontent" src="Webform.CashMgt/AccPay/APDisbSelec.aspx?aptype=SPPL"
            scrolling="yes" frameborder="0"></iframe>--%>
    </div>
    </form>
</body>
</html>
