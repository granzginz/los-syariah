
Public Interface IApprovalScheme
    Sub ApprovalSchemeAdd(ByVal customclass As Parameter.ApprovalScheme)
    Sub ApprovalSchemeEdit(ByVal customclass As Parameter.ApprovalScheme)
    Sub ApprovalSchemeDelete(ByVal customclass As Parameter.ApprovalScheme)
    Function ApprovalSchemeView(ByVal customclass As Parameter.ApprovalScheme) As Parameter.ApprovalScheme
    Function GetApprovalType(ByVal strConnection As String) As DataTable
#Region "Approval Member"
    Function ApprovalMemberView(ByVal customclass As Parameter.ApprovalScheme) As DataTable
    Sub ApprovalMemberDelete(ByVal customclass As Parameter.ApprovalScheme)
    Sub ApprovalMemberEdit(ByVal customclass As Parameter.ApprovalScheme)
    Sub ApprovalMemberAdd(ByVal customclass As Parameter.ApprovalScheme)
    Function ApprovalMemberLoginList(ByVal customclass As Parameter.ApprovalScheme) As DataTable
    Function ApprovalMemberSetMember(ByVal customclass As Parameter.ApprovalScheme) As DataTable
#End Region
#Region "Approval Path"
    Sub ApprovalPathAdd(ByVal customclass As Parameter.ApprovalScheme)
    Sub ApprovalPathEdit(ByVal customclass As Parameter.ApprovalScheme)
    Sub ApprovalPathDelete(ByVal customclass As Parameter.ApprovalScheme)
#End Region

    Function ApprovalPathGetLimit(ByVal customclass As Parameter.ApprovalScheme) As Double
    Function ApprovalPathView(ByVal customclass As Parameter.ApprovalScheme) As Parameter.ApprovalScheme
    Function ApprovalPathTree(ByVal customclass As Parameter.ApprovalScheme) As DataTable
End Interface
