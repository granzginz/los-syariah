
Public Interface IApprovalScreen
    Function ApprovalList(ByVal customclass As Parameter.ApprovalScreen) As Parameter.ApprovalScreen
    Function ApprovalListUser(ByVal customclass As Parameter.ApprovalScreen) As DataTable
    Function ApprovalListTypeDescription(ByVal customclass As Parameter.ApprovalScreen) As String
    Function ApprovalListHistory(ByVal customclass As Parameter.ApprovalScreen) As DataTable
    Function ApprovalScreenUserRequest(ByVal customclass As Parameter.ApprovalScreen) As String
    Function ApprovalScreenViewScheme(ByVal customclass As Parameter.ApprovalScreen) As DataTable
    Function ApprovalScreenUserScheme(ByVal customclass As Parameter.ApprovalScreen) As Parameter.ApprovalScreen
    Function ApprovalScreenIsValidApproval(ByVal customclass As Parameter.ApprovalScreen) As Parameter.ApprovalScreen
    Function ApprovalScreenSetLimit(ByVal customclass As Parameter.ApprovalScreen) As Parameter.ApprovalScreen
    Function getApprovalItems(ByVal customclass As Parameter.ApprovalScreen) As Parameter.ApprovalScreen
End Interface
