﻿Imports Maxiloan.Parameter
Public Interface IM01
    Function GetM01(oCustomClass As M01) As M01
    Function GetSelectM01(ByVal oCustomClass As M01) As M01
    Function GetM01Edit(ByVal oCustomClass As M01) As M01
    Function GetM01Save(ByVal oCustomClass As M01) As M01
    Function GetM01Add(ByVal oCustomClass As M01) As M01
    Function GetCboBulandataSIPP(ByVal oCustomClass As M01) As M01
    Function GetM01Delete(ByVal oCustomClass As M01) As String
End Interface
