﻿Imports Maxiloan.Parameter

Public Interface ID02
    Function GetD02(oCustomClass As D02) As D02
    Function GetSelectD02(ByVal oCustomClass As D02) As D02
    Function GetD02Edit(ByVal oCustomClass As D02) As D02
    Function GetD02Save(ByVal oCustomClass As D02) As D02
    Function GetD02Add(ByVal oCustomClass As D02) As D02
    Function GetD02Delete(ByVal oCustomClass As D02) As String
    Function GetCbo(ByVal oCustomClass As D02) As D02
    Function GetCbo1(ByVal oCustomClass As D02) As D02
    Function GetCbo2(ByVal oCustomClass As D02) As D02
    Function GetCboBulandataSIPP(ByVal oCustomClass As D02) As D02
End Interface
