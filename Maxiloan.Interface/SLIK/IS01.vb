﻿Imports Maxiloan.Parameter
Public Interface IS01
    Function GetS01(oCustomClass As S01) As S01
    Function GetSelectS01(ByVal oCustomClass As S01) As S01
    Function GetS01Edit(ByVal oCustomClass As S01) As S01
    Function GetS01Save(ByVal oCustomClass As S01) As S01
    Function GetS01Add(ByVal oCustomClass As S01) As S01
    Function GetCboBulandataSIPP(ByVal oCustomClass As S01) As S01
    Function GetS01Delete(ByVal oCustomClass As S01) As String
    Function GetCbo(ByVal oCustomClass As S01) As S01
    Function GetCbo1(ByVal oCustomClass As S01) As S01
End Interface
