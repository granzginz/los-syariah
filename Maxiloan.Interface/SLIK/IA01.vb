﻿Imports Maxiloan.Parameter
Public Interface IA01
	Function GetA01(oCustomClass As A01) As A01
	Function GetSelectA01(ByVal oCustomClass As A01) As A01
	Function GetA01Edit(ByVal oCustomClass As A01) As A01
	Function GetA01Save(ByVal oCustomClass As A01) As A01
	Function GetA01Add(ByVal oCustomClass As A01) As A01
	Function GetCboBulandataSIPP(ByVal oCustomClass As A01) As A01
	Function GetA01Delete(ByVal oCustomClass As A01) As String
	Function GetCbo(ByVal oCustomClass As A01) As A01
End Interface
