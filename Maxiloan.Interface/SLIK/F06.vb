﻿Imports Maxiloan.Parameter

Public Interface IF06
    Function GetF06(oCustomClass As F06) As F06
    Function GetSelectF06(ByVal oCustomClass As F06) As F06
    Function GetF06Edit(ByVal oCustomClass As F06) As F06
    Function GetF06Save(ByVal oCustomClass As F06) As F06
    Function GetCbo(ByVal oCustomClass As F06) As F06
    Function GetCbo1(ByVal oCustomClass As F06) As F06
    Function GetCbo2(ByVal oCustomClass As F06) As F06
    Function GetCbo3(ByVal oCustomClass As F06) As F06
    Function GetCbo4(ByVal oCustomClass As F06) As F06
    Function GetF06Add(ByVal oCustomClass As F06) As F06
    Function GetCboBulandataSIPP(ByVal oCustomClass As F06) As F06
    Function GetF06Delete(ByVal oCustomClass As F06) As String
End Interface

