﻿Imports Maxiloan.Parameter
Public Interface ID01
    Function GetD01(oCustomClass As D01) As D01
    Function GetSelectD01(ByVal oCustomClass As D01) As D01
    Function GetD01Edit(ByVal oCustomClass As D01) As D01
    Function GetD01Save(ByVal oCustomClass As D01) As D01
    Function GetD01Add(ByVal oCustomClass As D01) As D01
    Function GetCboBulandataSIPP(ByVal oCustomClass As D01) As D01
    Function GetD01Delete(ByVal oCustomClass As D01) As String
    Function GetCbo(ByVal oCustomClass As D01) As D01
    Function GetCbo1(ByVal oCustomClass As D01) As D01
End Interface
