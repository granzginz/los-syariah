﻿Imports Maxiloan.Parameter
Public Interface IK01
	Function GetK01(oCustomClass As K01) As K01
	Function GetSelectK01(ByVal oCustomClass As K01) As K01
	Function GetK01Edit(ByVal oCustomClass As K01) As K01
	Function GetK01Save(ByVal oCustomClass As K01) As K01
	Function GetK01Add(ByVal oCustomClass As K01) As K01
	Function GetCboBulandataSIPP(ByVal oCustomClass As K01) As K01
	Function GetK01Delete(ByVal oCustomClass As K01) As String
	Function GetCbo(ByVal oCustomClass As K01) As K01
End Interface
