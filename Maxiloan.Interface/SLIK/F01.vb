﻿Imports Maxiloan.Parameter

Public Interface IF01
    Function GetF01(oCustomClass As F01) As F01
    Function GetSelectF01(ByVal oCustomClass As F01) As F01
    Function GetF01Edit(ByVal oCustomClass As F01) As F01
    Function GetF01Save(ByVal oCustomClass As F01) As F01
    Function GetCbo(ByVal oCustomClass As F01) As F01
    Function GetF01Add(ByVal oCustomClass As F01) As F01
    Function GetCboBulandataSIPP(ByVal oCustomClass As F01) As F01
    Function GetF01Delete(ByVal oCustomClass As F01) As String
End Interface

