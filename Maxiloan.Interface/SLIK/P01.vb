﻿Imports Maxiloan.Parameter

Public Interface IP01
    Function GetP01(oCustomClass As P01) As P01
    Function GetSelectP01(ByVal oCustomClass As P01) As P01
    Function GetP01Edit(ByVal oCustomClass As P01) As P01
    Function GetP01Save(ByVal oCustomClass As P01) As P01
    Function GetCbo(ByVal oCustomClass As P01) As P01
    Function GetP01Add(ByVal oCustomClass As P01) As P01
    Function GetCboBulandataSIPP(ByVal oCustomClass As P01) As P01
    Function GetP01Delete(ByVal oCustomClass As P01) As String
End Interface

