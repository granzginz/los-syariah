

Public Interface ITransferForPaymentRequest
    Function ListRequest(ByVal customclass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest
    Function ListPaymentDetail(ByVal customclass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest
    Sub SavePaymentRequest(ByVal customclass As Parameter.TransferForPaymentRequest)
    Function EbankPYREQAdd(ByVal oCustomClass As Parameter.TransferForPaymentRequest) As String

    Function ListRequestOtorisasi(ByVal customclass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest
    Function ListPaymentDetailOtorisasi(ByVal customclass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest
    Sub SavePaymentRequestOtorisasi(ByVal customclass As Parameter.TransferForPaymentRequest)
    Function ListPaymentHistoryReject(ByVal customclass As Parameter.TransferForPaymentRequest) As Parameter.TransferForPaymentRequest
End Interface
