


Public Interface IViewAPSupplier
    Function ViewList(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec
    Function ViewListPO(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec
    Function ViewInvoiceDeduction(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec
    Function ListViewAP(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec
End Interface
