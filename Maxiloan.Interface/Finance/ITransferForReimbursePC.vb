

Public Interface ITransferForReimbursePC
    Function ListPCReimburse(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
    Function ListBranchHO(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
    Function ListBankAccount(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
    Function ListPettyCash(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
    Function ListBilyetGiro(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
    Sub SaveReimbursePC(ByVal customclass As Parameter.TransferForReimbursePC)
    Function CheckEndingBalance(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
    Function EbankPCREIAdd(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC

    Function ListPCReimburseOtorisasi(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
    Function ListPettyCashOtorisasi(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
    Sub SaveReimbursePCOtorisasi(ByVal customclass As Parameter.TransferForReimbursePC)
    Function ListPettyCashApr(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
    Function ListBankAccount2(ByVal customclass As Parameter.TransferForReimbursePC) As Parameter.TransferForReimbursePC
End Interface
