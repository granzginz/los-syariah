

Public Interface IAPDisbInq
    Function ListAPDisbInq(ByVal customclass As Parameter.APDisbInq) As Parameter.APDisbInq
    Function ReportAPDisbInq(ByVal customclass As Parameter.APDisbInq) As Parameter.APDisbInq
    Function ListPVInquiry(ByVal customclass As Parameter.APDisbInq) As Parameter.APDisbInq
    Function ReportPVInquiry(ByVal customclass As Parameter.APDisbInq) As Parameter.APDisbInq

    Function ListAPDisbInqPrintSelection(ByVal customclass As Parameter.APDisbInq) As Parameter.APDisbInq
    Sub savePrintSelection(ByVal customclass As Parameter.APDisbInq)
End Interface
