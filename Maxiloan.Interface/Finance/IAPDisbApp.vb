

Imports Maxiloan.Parameter

Public Interface IAPDisbApp

    Function ListApOtorTrans(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
    Function ListAPDisbApp(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
    Function InformasiAPDisbApp(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
    Function UpdateAPDisbApp(ByVal customclass As Parameter.APDisbApp) As String

    Function ListAPDisbApp2(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp

    Function ListAPDisbApp3(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
    Function ListAPDisbApp4(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
    Function UpdateAPDisbApp2(ByVal customclass As Parameter.APDisbApp) As String
    Function GetReleaseEbanking(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
    Sub ReleaseEbankingUpdateStatus(ByVal ocustomClass As Parameter.APDisbApp)
    Sub declinceReleaseEbanking(ByVal ocustomClass As Parameter.APDisbApp)
    Function MemoPembayaranReport(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
    Function MemoPembayaranDetail1Report(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
    Function MemoPembayaranDetail2Report(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
    Function UpdateMultiAPDisbApp(cnn As String, status As String, branchId As String, datas As IList(Of Parameter.ValueTextObject), ByVal ParamArray bankSelected() As String) As String
#Region "APDisbCash"
    Sub UpdateAPDisbCash(ByVal customclass As Parameter.APDisbApp, Optional isApDisbTransBeforeDisburse As Boolean = True)
#End Region
    Function LoadComboRekeningBank(cnn As String, caraBayar As String) As IList(Of Parameter.CommonValueText)
    Function APgantiCaraBayar(ByVal customclass As Parameter.APDisbApp) As String
    Function UpdateMultiAPDisbApp2(ByVal ocustomClass As APDisbApp) As Parameter.APDisbApp
    Function UpdateMultiAPDisbApp2_Report(ByVal ocustomClass As APDisbApp) As Parameter.APDisbApp
End Interface
