

Public Interface IEBankTranfer
    Function EBankTransferSave(ByVal listdata As ArrayList, ByVal pvdata As Parameter.APDisbApp) As String
    Function EBankTransferList(ByVal customclass As Parameter.EBankTransfer) As Parameter.EBankTransfer
    Function EBankTransferList2(ByVal customclass As Parameter.EBankTransfer) As Parameter.EBankTransfer
    'Function EBankExecute(ByVal customclass As Parameter.APDisbApp) As String
    Function EBankTransferChangeStatus(ByVal customclass As Parameter.EBankTransfer) As String
    Function EBankRejectSave(ByVal customclass As Parameter.EBankReject) As String
    Function EBankExecuteReject(ByVal customclass As Parameter.APDisbApp) As String
    'Function EBankPYREQExec(ByVal customclass As Parameter.APDisbApp) As String
    'Function EBankTRFAAExec(ByVal customclass As Parameter.APDisbApp) As String
    'Function EBankTRFUNDExec(ByVal customclass As Parameter.APDisbApp) As String
    'Function EBankPCREIExec(ByVal customclass As Parameter.APDisbApp) As String
    Function EBankExecuteNew(ByVal listdata As ArrayList, ByVal ebankdata As Parameter.EBankTransfer) As String
    Function GetRequestEdit(ByVal customclass As Parameter.EBankTransfer) As Parameter.EBankTransfer
End Interface
