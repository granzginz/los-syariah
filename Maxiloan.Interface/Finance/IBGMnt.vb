

Public Interface IBGMnt
#Region "BGMaintenance"
    Function ListBGMnt(ByVal customclass As Parameter.BGMnt) As Parameter.BGMnt
    'Function AddBGMnt(ByVal customclass As Parameter.BGMnt) As String
    Function BGMntCmbBankAccount(ByVal customclass As Parameter.BGMnt) As Parameter.BGMnt
    Function GetTableBGMnt(ByVal customclass As Parameter.BGMnt, ByVal strName As String) As Parameter.BGMnt
    Function SaveBGMnt(ByVal customclass As Parameter.BGMnt, ByVal strName As String) As String
    Function UpdateBGMnt(ByVal customclass As Parameter.BGMnt) As String
    Function ReportBGMnt(ByVal customclass As Parameter.BGMnt) As Parameter.BGMnt
#End Region

#Region "AP Selection"
    Function ListAPSele(ByVal customclass As Parameter.BGMnt) As Parameter.BGMnt
#End Region

End Interface
