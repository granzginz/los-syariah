

Public Interface ITransferAccount
    Function GetBankType(ByVal customclass As Parameter.TransferAccount) As Parameter.TransferAccount
    Sub SaveTrasnferAccount(ByVal customclass As Parameter.TransferAccount)
    Function InqTransferFund(ByVal ocustomclass As Parameter.TransferFundInquiry) As Parameter.TransferFundInquiry
    Function ViewRptTransferFund(ByVal oCustomClass As Parameter.TransferFundInquiry) As Parameter.TransferFundInquiry
    Function GetViewTransferFund(ByVal oCustomClass As Parameter.TransferFundInquiry) As Parameter.TransferFundInquiry
    Function EbankTRACCOUNTAdd(ByVal customclass As Parameter.TransferAccount) As Parameter.TransferAccount
    Sub SaveTransferAccountOtorisasi(ByVal customclass As Parameter.TransferAccount)
End Interface
