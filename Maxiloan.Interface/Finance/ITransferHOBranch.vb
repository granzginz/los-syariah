

Public Interface ITransferHOBranch
    Function GetBankType(ByVal customclass As Parameter.TransferAccount) As Parameter.TransferAccount
    Sub SaveTransferHOBranch(ByVal customclass As Parameter.TransferAccount)
    Function EBankTRFUNDAdd(ByVal customclass As Parameter.TransferAccount) As Parameter.TransferAccount
    Sub PenarikanDanaCabangSave(ByVal customclass As Parameter.TransferAccount)
    Sub OtorisasiPenarikanDanaCabangSave(ByVal customclass As Parameter.TransferAccount)
    Sub OtorisasiPenarikanDanaCabangReject(ByVal customclass As Parameter.TransferAccount)
End Interface
