

Public Interface IAPDisbSelec
    Function ListAPSele(ByVal customclass As Parameter.APDisbSelec) As Parameter.APDisbSelec
    Function APSelectionList(ByVal oCustomClass As Parameter.APDisbSelec) As DataTable
    Function APGroupSelectionList(ByVal oCustomClass As Parameter.APDisbSelec) As Parameter.APDisbSelec
    Function SavingToPaymentVoucher(ByVal oCustomClass As Parameter.APDisbSelec) As Parameter.APDisbSelec
    Sub SavingChangeLoc(ByVal oCustomClass As Parameter.APDisbSelec)
    Function ListEBanking(ByVal customclass As Parameter.APDisbSelec) As DataTable
End Interface
