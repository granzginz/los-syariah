

Public Interface IBankReconcile
    Function ListBankRecon(ByVal customclass As Parameter.BankReconcile) As Parameter.BankReconcile
    Function ListBankReconDet(ByVal customclass As Parameter.BankReconcile) As Parameter.BankReconcile
    Sub SaveBankRecon(ByVal customclass As Parameter.BankReconcile)
    Function InfoReconBalance(ByVal customclass As Parameter.BankReconcile) As Parameter.BankReconcile
End Interface
