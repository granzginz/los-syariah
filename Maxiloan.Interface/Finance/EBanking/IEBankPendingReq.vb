﻿Public Interface IEBankPendingReq
    Function GetListing(ByVal oCostom As Parameter.PEBankPendingReq) As Parameter.PEBankPendingReq
    Function GetDetailPay(ByVal oCostom As Parameter.PEBankPendingReq) As Parameter.PEBankPendingReq
    Function SaveUpdate(ByVal oCostom As Parameter.PEBankPendingReq) As String
End Interface
