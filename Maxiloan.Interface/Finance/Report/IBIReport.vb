
Public Interface IBIReport
    Sub BIReportSave(ByVal strConnection As String, ByVal Tahun As String, ByVal Bulan As String)
    Function BIReportViewer(ByVal strConnection As String, ByVal Tahun As String, ByVal Bulan As String) As DataSet
    Function BIReportSemiAnual(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
End Interface
