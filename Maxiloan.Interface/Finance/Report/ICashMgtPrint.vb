Public Interface ICashMgtPrint

    Function getInstallmentDueDate(ByVal customClass As Parameter.InstallmentDueDate) As Parameter.InstallmentDueDate
    Function getInstallmentDueDateCollection(ByVal customClass As Parameter.InstallmentDueDate) As Parameter.InstallmentDueDate

End Interface
