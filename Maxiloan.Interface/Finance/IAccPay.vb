Public Interface IAccPay
    Function getPencairanPinjamaList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
    Function savePencairanPinjamanHO(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
    Function getPencairanPinjamanCustomerList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
    Sub SavePencairanPinjamanCustomer(ByVal customClass As Parameter.AccPayParameter)
    Function getInquiryPencairanPinjamanList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
    Function getOutstandingPencairanPinjamanList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
    Function getBuktiKasKeluarList(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter

    Sub saveBiayaProses(ByVal customClass As Parameter.AccPayParameter)
    Function BiayaProsesReportDetail(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
    Function BiayaProsesReportSummary(ByVal customClass As Parameter.AccPayParameter) As Parameter.AccPayParameter
End Interface

