﻿Imports Maxiloan.Parameter

Public Interface ISIPP1300
    Function GetSIPP1300(oCustomClass As SIPP1300) As SIPP1300
    Function GetSelectSIPP1300(ByVal oCustomClass As SIPP1300) As SIPP1300
    Function SIPP1300Edit(ByVal oCustomClass As SIPP1300) As SIPP1300
    Function SIPP1300Save(ByVal oCustomClass As SIPP1300) As SIPP1300
    Function SIPP1300Add(ByVal oCustomClass As SIPP1300) As SIPP1300
    Function SIPP1300Delete(ByVal oCustomClass As SIPP1300) As String
    Function GetCbo(ByVal oCustomClass As SIPP1300) As SIPP1300
    Function GetCboBulandataSIPP(ByVal oCustomClass As SIPP1300) As SIPP1300
End Interface

