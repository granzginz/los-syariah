﻿Public Interface ISIPP1200

    Function GetSIPP1200List(ByVal oCustomClass As Parameter.SIPP1200) As Parameter.SIPP1200
    Function SIPP1200SaveAdd(ByVal oCustomClass As Parameter.SIPP1200) As Parameter.SIPP1200
    Function SIPP1200SaveEdit(ByVal oCustomClass As Parameter.SIPP1200) As Parameter.SIPP1200
    Function GetSIPP1200Edit(ByVal oCustomClass As Parameter.SIPP1200) As Parameter.SIPP1200
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP1200) As Parameter.SIPP1200
    Function SIPP1200Delete(ByVal ocustomClass As Parameter.SIPP1200) As String
    Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP1200) As Parameter.SIPP1200

End Interface
