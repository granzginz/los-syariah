﻿Imports Maxiloan.Parameter

Public Interface ISIPP2600
    Function GetSIPP2600(oCustomClass As SIPP2600) As SIPP2600
    Function GetSelectSIPP2600(ByVal oCustomClass As SIPP2600) As SIPP2600
    Function SIPP2600Edit(ByVal oCustomClass As SIPP2600) As SIPP2600
    Function SIPP2600Save(ByVal oCustomClass As SIPP2600) As SIPP2600
    Function SIPP2600Add(ByVal oCustomClass As SIPP2600) As SIPP2600
    Function SIPP2600Delete(ByVal oCustomClass As SIPP2600) As String
    Function GetCbo(ByVal oCustomClass As SIPP2600) As SIPP2600
    Function GetCboBulandataSIPP(ByVal oCustomClass As SIPP2600) As SIPP2600
End Interface