﻿Public Interface ISIPP2200

    Function GetSIPP2200(ByVal oCustomClass As Parameter.SIPP2200) As Parameter.SIPP2200
    Function SIPP2200SaveAdd(ByVal oCustomClass As Parameter.SIPP2200) As Parameter.SIPP2200
    Function SIPP2200SaveEdit(ByVal oCustomClass As Parameter.SIPP2200) As Parameter.SIPP2200
    Function GetSIPP2200Edit(ByVal oCustomClass As Parameter.SIPP2200) As Parameter.SIPP2200
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP2200) As Parameter.SIPP2200
    Function SIPP2200Delete(ByVal ocustomClass As Parameter.SIPP2200) As String
    Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP2200) As Parameter.SIPP2200

End Interface
