﻿Public Interface ISIPP2100

    Function GetSIPP2100(ByVal oCustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
    Function GetSelectSIPP2100(ByVal oCustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
    Function SIPP2100SaveAdd(ByVal oCustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
    Function SIPP2100SaveEdit(ByVal oCustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
    Function GetSIPP2100Edit(ByVal oCustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP2100) As Parameter.SIPP2100
    Function SIPP2100Delete(ByVal ocustomClass As Parameter.SIPP2100) As String
    Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP2100) As Parameter.SIPP2100


End Interface
