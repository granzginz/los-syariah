﻿Imports Maxiloan.Parameter

Public Interface ISIPP1110
    Function GetSIPP1110(oCustomClass As SIPP1110) As SIPP1110
    Function GetSelectSIPP1110(ByVal oCustomClass As SIPP1110) As SIPP1110
    Function SIPP1110Edit(ByVal oCustomClass As SIPP1110) As SIPP1110
    Function SIPP1110Save(ByVal oCustomClass As SIPP1110) As SIPP1110
    Function SIPP1110Add(ByVal oCustomClass As SIPP1110) As SIPP1110
    Function SIPP1110Delete(ByVal oCustomClass As SIPP1110) As String
    Function GetCbo(ByVal oCustomClass As SIPP1110) As SIPP1110
    Function GetCboBulandataSIPP(ByVal oCustomClass As SIPP1110) As SIPP1110
End Interface

