﻿Imports Maxiloan.Parameter

Public Interface ISIPP2790
    Function GetSIPP2790(oCustomClass As SIPP2790) As SIPP2790
    Function GetSelectSIPP2790(ByVal oCustomClass As SIPP2790) As SIPP2790
    Function SIPP2790Edit(ByVal oCustomClass As SIPP2790) As SIPP2790
    Function SIPP2790Save(ByVal oCustomClass As SIPP2790) As SIPP2790
    Function SIPP2790Add(ByVal oCustomClass As SIPP2790) As SIPP2790
    Function SIPP2790Delete(ByVal oCustomClass As SIPP2790) As String
    Function GetCbo(ByVal oCustomClass As SIPP2790) As SIPP2790
    Function GetCboBulandataSIPP(ByVal oCustomClass As SIPP2790) As SIPP2790
End Interface
