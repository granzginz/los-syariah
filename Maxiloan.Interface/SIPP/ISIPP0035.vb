﻿Imports Maxiloan.Parameter
Public Interface ISIPP0035
    Function GetSIPP0035(oCustomClass As SIPP0035) As SIPP0035
    Function GetSelectSIPP0035(ByVal oCustomClass As SIPP0035) As SIPP0035
    Function SIPP0035Edit(ByVal oCustomClass As SIPP0035) As SIPP0035
    Function SIPP0035Save(ByVal oCustomClass As SIPP0035) As SIPP0035
    Function SIPP0035Add(ByVal oCustomClass As SIPP0035) As SIPP0035
    Function SIPP0035Delete(ByVal oCustomClass As SIPP0035) As String
    Function GetCbo(ByVal oCustomClass As SIPP0035) As SIPP0035
    Function GetCboBulandataSIPP(ByVal oCustomClass As SIPP0035) As SIPP0035
End Interface
