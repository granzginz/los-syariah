﻿Imports Maxiloan.Parameter

Public Interface ISIPP0025

    Function GetSIPP0025(ByVal oCustomClass As Parameter.SIPP0025) As Parameter.SIPP0025
    Function SIPP0025SaveAdd(ByVal oCustomClass As Parameter.SIPP0025) As Parameter.SIPP0025
    Function SIPP0025SaveEdit(ByVal oCustomClass As Parameter.SIPP0025) As Parameter.SIPP0025
    Function GetSIPP0025Edit(ByVal oCustomClass As Parameter.SIPP0025) As Parameter.SIPP0025
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP0025) As Parameter.SIPP0025
    Function SIPP0025Delete(ByVal ocustomClass As Parameter.SIPP0025) As String
    Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0025) As Parameter.SIPP0025
End Interface
