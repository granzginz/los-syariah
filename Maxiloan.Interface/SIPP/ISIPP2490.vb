﻿Imports Maxiloan.Parameter

Public Interface ISIPP2490
    Function GetSIPP2490(oCustomClass As SIPP2490) As SIPP2490
    Function GetSelectSIPP2490(ByVal oCustomClass As SIPP2490) As SIPP2490
    Function SIPP2490Edit(ByVal oCustomClass As SIPP2490) As SIPP2490
    Function SIPP2490Save(ByVal oCustomClass As SIPP2490) As SIPP2490
    Function SIPP2490Add(ByVal oCustomClass As SIPP2490) As SIPP2490
    Function SIPP2490Delete(ByVal oCustomClass As SIPP2490) As String
    Function GetCbo(ByVal oCustomClass As SIPP2490) As SIPP2490
    Function GetCboBulandataSIPP(ByVal oCustomClass As SIPP2490) As SIPP2490
End Interface
