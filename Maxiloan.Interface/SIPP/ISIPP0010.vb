﻿Public Interface ISIPP0010

    Function GetSIPP0010(ByVal oCustomClass As Parameter.SIPP0010) As Parameter.SIPP0010
    Function SIPP0010SaveAdd(ByVal oCustomClass As Parameter.SIPP0010) As Parameter.SIPP0010
    Function SIPP0010SaveEdit(ByVal oCustomClass As Parameter.SIPP0010) As Parameter.SIPP0010
    Function GetSIPP0010Edit(ByVal oCustomClass As Parameter.SIPP0010) As Parameter.SIPP0010
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP0010) As Parameter.SIPP0010
    Function SIPP0010Delete(ByVal ocustomClass As Parameter.SIPP0010) As String
    Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0010) As Parameter.SIPP0010

End Interface
