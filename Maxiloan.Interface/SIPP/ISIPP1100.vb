﻿Public Interface ISIPP1100

    Function GetSIPP1100List(ByVal oCustomClass As Parameter.SIPP1100) As Parameter.SIPP1100
    Function SIPP1100SaveAdd(ByVal oCustomClass As Parameter.SIPP1100) As Parameter.SIPP1100
    Function SIPP1100SaveEdit(ByVal oCustomClass As Parameter.SIPP1100) As Parameter.SIPP1100
    Function GetSIPP1100Edit(ByVal oCustomClass As Parameter.SIPP1100) As Parameter.SIPP1100
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP1100) As Parameter.SIPP1100
    Function SIPP1100Delete(ByVal ocustomClass As Parameter.SIPP1100) As String
    Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP1100) As Parameter.SIPP1100

End Interface
