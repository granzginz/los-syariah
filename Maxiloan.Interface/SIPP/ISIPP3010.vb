﻿Public Interface ISIPP3010

    Function GetSIPP3010(ByVal oCustomClass As Parameter.SIPP3010) As Parameter.SIPP3010
    Function SIPP3010SaveAdd(ByVal oCustomClass As Parameter.SIPP3010) As Parameter.SIPP3010
    Function SIPP3010SaveEdit(ByVal oCustomClass As Parameter.SIPP3010) As Parameter.SIPP3010
    Function GetSIPP3010Edit(ByVal oCustomClass As Parameter.SIPP3010) As Parameter.SIPP3010
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP3010) As Parameter.SIPP3010
    Function SIPP3010Delete(ByVal ocustomClass As Parameter.SIPP3010) As String
    Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP3010) As Parameter.SIPP3010

End Interface
