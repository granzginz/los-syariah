﻿Public Interface ISIPP0030

    Function GetSIPP0030(ByVal oCustomClass As Parameter.SIPP0030) As Parameter.SIPP0030
    Function SIPP0030SaveAdd(ByVal oCustomClass As Parameter.SIPP0030) As Parameter.SIPP0030
    Function SIPP0030SaveEdit(ByVal oCustomClass As Parameter.SIPP0030) As Parameter.SIPP0030
    Function GetSIPP0030Edit(ByVal oCustomClass As Parameter.SIPP0030) As Parameter.SIPP0030
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP0030) As Parameter.SIPP0030
    Function SIPP0030Delete(ByVal ocustomClass As Parameter.SIPP0030) As String
    Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP0030) As Parameter.SIPP0030

End Interface
