﻿Imports Maxiloan.Parameter
Public Interface ISIPP0020
    Function GetSIPP0020(oCustomClass As SIPP0020) As SIPP0020
    Function GetSelectSIPP0020(ByVal oCustomClass As SIPP0020) As SIPP0020
    Function SIPP0020Edit(ByVal oCustomClass As SIPP0020) As SIPP0020
    Function SIPP0020Save(ByVal oCustomClass As SIPP0020) As SIPP0020
    Function SIPP0020Add(ByVal oCustomClass As SIPP0020) As SIPP0020
    Function SIPP0020Delete(ByVal oCustomClass As SIPP0020) As String
    Function GetCbo(ByVal oCustomClass As SIPP0020) As SIPP0020
    Function GetCboBUlanDataSIPP(ByVal oCustomClass As SIPP0020) As SIPP0020
End Interface
