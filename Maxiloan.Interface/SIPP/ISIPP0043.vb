﻿Imports Maxiloan.Parameter

Public Interface ISIPP0043
    Function GetSIPP0043(oCustomClass As SIPP0043) As SIPP0043
    Function GetSelectSIPP0043(ByVal oCustomClass As SIPP0043) As SIPP0043
    Function SIPP0043Edit(ByVal oCustomClass As SIPP0043) As SIPP0043
    Function SIPP0043Save(ByVal oCustomClass As SIPP0043) As SIPP0043
    Function SIPP0043Add(ByVal oCustomClass As SIPP0043) As SIPP0043
    Function SIPP0043Delete(ByVal oCustomClass As SIPP0043) As String
    Function GetCbo(ByVal oCustomClass As SIPP0043) As SIPP0043
    Function GetCboBulandataSIPP(ByVal oCustomClass As SIPP0043) As SIPP0043
End Interface
