﻿Imports Maxiloan.Parameter

Public Interface ISIPP0000
    Function GetSIPP0000(oCustomClass As SIPP0000) As SIPP0000
    Function GetSelectSIPP0000(ByVal oCustomClass As SIPP0000) As SIPP0000
    Function SIPP0000Edit(ByVal oCustomClass As SIPP0000) As SIPP0000
    Function SIPP0000Save(ByVal oCustomClass As SIPP0000) As SIPP0000
    Function SIPP0000Add(ByVal oCustomClass As SIPP0000) As SIPP0000
    Function SIPP0000Delete(ByVal oCustomClass As SIPP0000) As String
    Function GetCbo(ByVal oCustomClass As SIPP0000) As SIPP0000
    Function GetCboBulandataSIPP(ByVal oCustomClass As SIPP0000) As SIPP0000
End Interface
