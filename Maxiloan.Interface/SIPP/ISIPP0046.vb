﻿Imports Maxiloan.Parameter

Public Interface ISIPP0046
    Function GetSIPP0046(oCustomClass As SIPP0046) As SIPP0046
    Function GetSelectSIPP0046(ByVal oCustomClass As SIPP0046) As SIPP0046
    Function SIPP0046Edit(ByVal oCustomClass As SIPP0046) As SIPP0046
    Function SIPP0046Save(ByVal oCustomClass As SIPP0046) As SIPP0046
    Function SIPP0046Add(ByVal oCustomClass As SIPP0046) As SIPP0046
    Function SIPP0046Delete(ByVal oCustomClass As SIPP0046) As String
    Function GetCbo(ByVal oCustomClass As SIPP0046) As SIPP0046
    Function GetCboBulandataSIPP(ByVal oCustomClass As SIPP0046) As SIPP0046
End Interface
