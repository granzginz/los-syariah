﻿Public Interface ISIPP2550

    Function GetSIPP2550(ByVal oCustomClass As Parameter.SIPP2550) As Parameter.SIPP2550
    Function SIPP2550SaveAdd(ByVal oCustomClass As Parameter.SIPP2550) As Parameter.SIPP2550
    Function SIPP2550SaveEdit(ByVal oCustomClass As Parameter.SIPP2550) As Parameter.SIPP2550
    Function GetSIPP2550Edit(ByVal oCustomClass As Parameter.SIPP2550) As Parameter.SIPP2550
    Function GetCbo(ByVal ocustomClass As Parameter.SIPP2550) As Parameter.SIPP2550
    Function SIPP2550Delete(ByVal ocustomClass As Parameter.SIPP2550) As String
    Function GetCboBulandataSIPP(ByVal oCustomClass As Parameter.SIPP2550) As Parameter.SIPP2550

End Interface
