﻿Imports Maxiloan.Parameter

Public Interface ISIPP3020
    Function GetSIPP3020(oCustomClass As SIPP3020) As SIPP3020
    Function GetSelectSIPP3020(ByVal oCustomClass As SIPP3020) As SIPP3020
    Function SIPP3020Edit(ByVal oCustomClass As SIPP3020) As SIPP3020
    Function SIPP3020Save(ByVal oCustomClass As SIPP3020) As SIPP3020
    Function SIPP3020Add(ByVal oCustomClass As SIPP3020) As SIPP3020
    Function SIPP3020Delete(ByVal oCustomClass As SIPP3020) As String
    Function GetCbo(ByVal oCustomClass As SIPP3020) As SIPP3020
    Function GetCboBulandataSIPP(ByVal oCustomClass As SIPP3020) As SIPP3020
End Interface
