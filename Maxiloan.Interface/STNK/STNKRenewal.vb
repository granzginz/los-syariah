
Public Interface ISTNKRenewal
    Function GetSTNKRequest(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
    Function SaveSTNKRequest(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
    Function ReportSTNKRenewal(ByVal oCustomClass As Parameter.STNKRenewal) As DataSet
    Function SaveSTNKRegister(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
    Function CancelSTNKRegister(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
    Function STNKView(ByVal strConnection As String, ByVal BranchId As String) As DataTable
    Function SaveSTNKInvoicing(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
    Function STNKInquiryReport(ByVal customclass As Parameter.STNKRenewal) As Parameter.STNKRenewal
    Function STNKRenewalUpdatePrint(ByVal customclass As Parameter.STNKRenewal) As Boolean
    Function GetLastDate(ByVal strConnection As String) As String
    Function GetReportSTNKRenewalIncome(ByVal strConnection As String, ByVal BranchID As String, ByVal BusinessDate As String) As DataSet
    Function STNKRequestInquiryPaging(ByVal customclass As Parameter.STNKRenewal) As Parameter.STNKRenewal
    Function SaveEstimasiBiayaSTNK(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
End Interface
