Public Interface IAgency
    Function STNKAgencyPaging(ByVal customclass As Parameter.Agency) As Parameter.Agency
    Function STNKAgencyView(ByVal strConnection As String, _
                                    ByVal BranchId As String, _
                                    ByVal AgentId As String) As DataTable
    Sub STNKAgencySave(ByVal oAgency As Parameter.Agency, _
                                    ByVal oBankAccount As Parameter.BankAccount, _
                                    ByVal oAddress As Parameter.Address, _
                                    ByVal oPersonal As Parameter.Personal)
    Sub STNKAgencyUpdate(ByVal oAgency As Parameter.Agency, _
                                  ByVal oBankAccount As Parameter.BankAccount, _
                                  ByVal oAddress As Parameter.Address, _
                                  ByVal oPersonal As Parameter.Personal)
    Sub STNKAgencyDelete(ByVal oAgency As Parameter.Agency)
    Function STNKAgencyReport(ByVal oCustomClass As Parameter.Agency) As Parameter.Agency

End Interface
