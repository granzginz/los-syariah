﻿Imports Maxiloan.Parameter

Public Interface ICashAdvance
    Function GetListCashAdvance(oCustomClass As CashAdvance) As CashAdvance
    Function GetPagingCashAdvance(oCustomClass As CashAdvance) As CashAdvance
    Function SaveCashAdvance(oCustomClass As CashAdvance) As CashAdvance
    Function SaveCashAdvanceRealisasiDetail(oCustomClass As CashAdvance) As CashAdvance
    Function GetListCashAdvanceDetail(oCustomClass As CashAdvance) As CashAdvance
    Function ReverseReversalCashAdvanced(oCustomClass As CashAdvance) As CashAdvance
    Function SaveReverseReversalCashAdvanced(oCustomClass As CashAdvance) As CashAdvance
End Interface
