﻿Imports Maxiloan.Parameter

Public Interface IJFDocument
    Function GetJFDocument(ocustomClass As JFDocument) As JFDocument
    Function GetJFDocumentEdit(ocustomClass As JFDocument) As JFDocument
    Function JFDocumentSaveAdd(ocustomClass As JFDocument) As String
    Sub JFDocumentSaveEdit(ocustomClass As JFDocument)
    Function JFDocumentDelete(ocustomClass As JFDocument) As String
    Function GetSelectJFDocument(ocustomClass As JFDocument) As JFDocument
End Interface
