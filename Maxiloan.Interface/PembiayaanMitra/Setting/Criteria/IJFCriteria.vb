﻿Imports Maxiloan.Parameter

Public Interface IJFCriteria
    Function GetJFCriteria(ocustomClass As JFCriteria) As JFCriteria
    Function GetJFCriteriaEdit(ocustomClass As JFCriteria) As JFCriteria
    Function JFCriteriaSaveAdd(ocustomClass As JFCriteria) As String
    Sub JFCriteriaSaveEdit(ocustomClass As JFCriteria)
    Function JFCriteriaDelete(ocustomClass As JFCriteria) As String
    Function GetSelectJFCriteria(ocustomClass As JFCriteria) As JFCriteria
End Interface
