﻿Public Interface IJFReference
    Function JFReferenceList(ByVal co As Parameter.JFReference) As Parameter.JFReference
    Function JFReferenceListByID(ByVal co As Parameter.JFReference) As Parameter.JFReference
    Function JFReferenceAdd(ByVal customClass As Parameter.JFReference) As String
    Sub JFReferenceEdit(ByVal customClass As Parameter.JFReference)
    Sub JFReferenceDelete(ByVal customclass As Parameter.JFReference)
End Interface
