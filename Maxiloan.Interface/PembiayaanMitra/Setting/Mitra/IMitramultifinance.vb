﻿

Public Interface IMitraMultifinance
    Function MitraList(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
    Function MitraListByID(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
    Function MitraAdd(ByVal customClass As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address) As String
    Sub MitraEdit(ByVal customClass As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address)
    Sub MitraDelete(ByVal customclass As Parameter.MitraMultifinance)
    Function MitraReport(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
    Function RekeningMitraList(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
    Function RekeningMitraListByID(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
    Function RekeningMitraAdd(ByVal customClass As Parameter.MitraMultifinance) As String
    Sub RekeningMitraEdit(ByVal customClass As Parameter.MitraMultifinance)
    Sub RekeningMitraDelete(ByVal customclass As Parameter.MitraMultifinance)

End Interface
