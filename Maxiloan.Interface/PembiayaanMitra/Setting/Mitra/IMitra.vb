﻿

Public Interface IMitra
    Function MitraList(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
    Function MitraListByID(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
    Function MitraAdd(ByVal customClass As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As String
    Sub MitraEdit(ByVal customClass As Parameter.MitraMultifinance, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
    Sub MitraDelete(ByVal customclass As Parameter.MitraMultifinance)
    Function MitraReport(ByVal co As Parameter.MitraMultifinance) As Parameter.MitraMultifinance
    'Function GetComboInputType(ByVal strcon As String) As DataTable

End Interface
