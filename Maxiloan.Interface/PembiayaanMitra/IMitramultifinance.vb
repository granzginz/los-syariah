﻿

Imports Maxiloan.Parameter

Public Interface IMitraMultifinance
    Function MitraList(ByVal co As MitraMultifinance) As MitraMultifinance
    Function MitraListByID(ByVal co As MitraMultifinance) As MitraMultifinance
    Function MitraAdd(ByVal customClass As MitraMultifinance, ByVal oClassAddress As Parameter.Address) As String
    Sub MitraEdit(ByVal customClass As MitraMultifinance, ByVal oClassAddress As Parameter.Address)
    Sub MitraDelete(ByVal customclass As MitraMultifinance)
    Function MitraReport(ByVal co As MitraMultifinance) As MitraMultifinance
    Function RekeningMitraList(ByVal co As MitraMultifinance) As MitraMultifinance
    Function RekeningMitraListByID(ByVal co As MitraMultifinance) As MitraMultifinance
    Function RekeningMitraAdd(ByVal customClass As MitraMultifinance) As String
    Sub RekeningMitraEdit(ByVal customClass As MitraMultifinance)
    Sub RekeningMitraDelete(ByVal customclass As MitraMultifinance)
    Function uploadlist(ByVal customclass As Lending) As Lending
    Function lendingdrawdownrequest(ByVal customclass As Lending) As Lending
    Function lendingdrawdownapproval(ByVal customclass As Lending) As Lending
    Function disburselist(ByVal customclass As Lending) As Lending
    Function disburse(ByVal customclass As Lending) As Lending
    Function batchlist(ByVal customclass As Lending) As Lending
    Function DisbursementHeaderList(ByVal strConnection As String) As DataTable
    Function LendingValidationList(ByVal strConnection As String) As List(Of AgreementToValidate)
    Function DisburseAgreementApproveReject(ByVal oDisburseAgreement As DisburseAgreement) As Boolean
    Function DisburseHeaderUpdate(ByVal oDisburseAgreement As DisburseAgreement) As Boolean
    Function GetLendingFacilityByCode(ByVal mfCode As String, ByVal facilityNo As String, ByVal strConnection As String) As LendingFacility
    Function LendingPament(ByVal oDisburseAgreement As Lending) As Lending
    Function lendingRateListPaging(ByVal customclass As LendingRateProperty) As LendingRateProperty
    Function lendingBatchListPaging(ByVal customclass As Lending) As Lending
    Function DraftSoftcopy(ByVal customClass As Lending) As DataSet
    Sub lendingRateDML(ByVal dml As String, ByVal customclass As LendingRateProperty)
    Function paymentCalculate(ByVal customclass As PaymentCalculateProperty) As PaymentCalculateProperty
    Sub paymentCalculateDML(ByVal customclass As PaymentCalculateProperty)
    Sub lendingFacilityDML(ByVal dml As String, ByVal customclass As LendingFacility)
    Function lendingpayment(ByVal customclass As Lending) As Lending
    Function lendinginstallmentinfo(ByVal customclass As Lending) As Lending
    Function PaymentByDueDateListPaging(ByVal customclass As Lending) As Lending
End Interface
