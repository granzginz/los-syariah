﻿Public Interface IAuthorize
    Function CheckForm(ByVal oCustomClass As Parameter.Common) As Boolean
    Function CheckFeature(ByVal oCustomClass As Parameter.Common) As Boolean
End Interface
