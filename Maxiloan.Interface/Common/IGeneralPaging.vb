

Public Interface IGeneralPaging

    Function GetDailyTransReport(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
    Function GetReportWithTwoParameter(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
    Function GetReportWithParameterWhereCond(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
    Function GetReportWithParameterWhereAndSort(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
    Function GetGeneralPaging(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
    Function GetRecordWithoutParameter(ByVal strConnection As String) As DataTable
    Function DeleteGeneral(ByVal customclass As Parameter.GeneralPaging) As Boolean
    Function GetBranchCombo(ByVal oClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
    Function GetInsuranceBranchCombo(ByVal oClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
    Function GetGeneralSP(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
    Function GetReportWithThreeParameterWhereCond(ByVal oCustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
    Function GetReportWithParamApplicationID(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
    Function ARMutationReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
    Function APMutationReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
    Function APToInsReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
    Function SuspendReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
    Function PrepaidReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
    Function BIQualityReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
    Function PrepaymentTerminationReport(ByVal customclass As Parameter.GeneralPaging) As Parameter.GeneralPaging
    Function AssetRepossessionOrderTerminationReport(ByVal customclass As Parameter.GeneralPaging) As Parameter.GeneralPaging
    Function GetReportWithBranchAndstrDate(ByVal customclass As Parameter.GeneralPaging) As DataSet
    Function GetReportWithBranchAndTwoPeriod(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
    Function GetGeneralPagingWithTwoWhereCondition(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
    Function GetReportWithBranch_strDate_Where(ByVal customclass As Parameter.GeneralPaging) As DataSet
    Function GetReportWithFiveParameterWhereCond(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
    Function GetRecordWithoutParameterAll(ByVal oCustom As Parameter.GeneralPaging) As DataTable
End Interface
