﻿Public Interface ILogin
    Function CheckBranch(ByVal oCustomClass As Parameter.Login) As Parameter.Login
    Function CheckMultiBranch(ByVal oCustomClass As Parameter.Login) As Parameter.Login
    Function GetLoginData() As Parameter.Login
    Function GetBusinessDate(ByVal strConnection As String, Optional ByVal strBranchId As String = "00") As Date
    Function GetUser(ByVal oCustomClass As Parameter.Login) As Parameter.Login
    Function GetEmployee(ByVal oCustomClass As Parameter.Login) As String
    Function SetSession(ByVal oCustomClass As Parameter.Login) As Parameter.Login
    Function ListTreeMenu(ByVal oCustomClass As Parameter.Login) As Boolean
    Function UpdateTreeMenu(ByVal oCustomClass As Parameter.Login) As String
    Function WriteFileXML(ByVal oCustomClass As Parameter.Login) As DataTable
    Function WriteXMLForUser(ByVal ocustomclass As Parameter.Login) As DataTable
    Function GetCompanyAddress(ByVal strConnection As String, ByVal StrCompanyID As String) As String
    Function CountWrongPwd(ByVal oCustomClass As Parameter.Login) As Int16
    Function IsPasswordExpired(ByVal oCustomClass As Parameter.Login) As Boolean
    Function PasswordSetting() As Parameter.Login
    Function ListPasswordHistory(ByVal oCustomClass As Parameter.Login) As Parameter.Login
    Sub setloghistory(ByVal oCustomClass As Parameter.Login)
End Interface
