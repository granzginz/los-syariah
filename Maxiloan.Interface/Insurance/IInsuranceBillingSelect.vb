

Public Interface IInsuranceBillingSelect


    Sub SaveInsuranceBillingSelectDetail(ByVal customClass As Parameter.InsCoAllocationDetailList)
    Function CheckInvoiceNo(ByVal customClass As Parameter.InsCoAllocationDetailList) As String
    Sub SaveInsuranceBillingSave(ByVal customClass As Parameter.InsCoAllocationDetailList, ByVal customClassList As List(Of Parameter.InsCoAllocationDetailList))
End Interface
