
Public Interface IInsNewCover
    Function InsNewCoverView(ByVal customclass As Parameter.InsNewCover) As Parameter.InsNewCover
    Function InsNewCoverGetInsured(ByVal customclass As Parameter.InsNewCover) As Parameter.InsNewCover
    Function InsNewCoverGetPaid(ByVal customclass As Parameter.InsNewCover) As Parameter.InsNewCover

    Function FillInsuranceComBranch(ByVal customClass As Parameter.InsNewCover) As DataTable
    Function GetInsuranceEntryStep2List(ByVal customClass As Parameter.InsNewCover) As Parameter.InsNewCover

    Function GetInsuranceEntryStep1List(ByVal customClass As Parameter.InsNewCover) As Parameter.InsNewCover
    Function CheckProspect(ByVal CustomClass As Parameter.InsNewCover) As Parameter.InsNewCover
    Sub ProcessSaveInsuranceApplicationLastProcess(ByVal customClass As Parameter.InsNewCover)
    Sub ProcessSaveInsuranceByCustomer(ByVal customClass As Parameter.InsNewCover)
    Function GetInsuredByCustomer(ByVal customClass As Parameter.InsNewCover) As Parameter.InsNewCover
    Sub InsNewCoverInsuredByCustDelete(ByVal customClass As Parameter.InsNewCover)
End Interface
