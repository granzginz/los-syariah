

Public Interface IInsQuotaStatistic

    Function ProcessInsuranceQuotaYear(ByVal customClass As Parameter.InsQuotaStatistic) As String
    Function getInsuranceQuotaListing(ByVal customClass As Parameter.InsQuotaStatistic) As Parameter.InsQuotaStatistic
    Function UpdateInsuranceQuotaYear(ByVal customClass As Parameter.InsQuotaStatistic) As String
    Function GetInsuranceQuotaReport(ByVal CustomClass As Parameter.InsQuotaStatistic) As Parameter.InsQuotaStatistic

End Interface
