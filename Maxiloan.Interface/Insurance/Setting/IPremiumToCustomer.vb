
Public Interface IPremiumToCustomer
    Function ProcessCopyRateFromAnotherBranch(ByVal customClass As Parameter.PremiumToCustomer) As String
    Function GetPremiumToCustomer(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer
    Function GetPremiumToCustomerBranch(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer
    Function GetPremiumToCustomerInsType(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer
    Function GetPremiumToCustomerReport(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer

    'TPL
    Function PremiumToCustomerTPLSaveAdd(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer
    Sub PremiumToCustomerTPLSaveEdit(ByVal customClass As Parameter.PremiumToCustomer)
    Sub PremiumToCustomerTPLDelete(ByVal customClass As Parameter.PremiumToCustomer)

    'Rate
    Function PremiumToCustomerRateCustSaveAdd(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer
    Sub PremiumToCustomerRateCustSaveEdit(ByVal customClass As Parameter.PremiumToCustomer)
    Sub PremiumToCustomerRateCustDelete(ByVal customClass As Parameter.PremiumToCustomer)
End Interface
