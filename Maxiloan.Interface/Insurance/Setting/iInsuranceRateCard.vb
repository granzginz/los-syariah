Public Interface iInsuranceRateCard
    Function GetInsuranceRateCardHSave(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard
    Function GetInsuranceRateCardDSave(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard
    Function GetInsuranceRateCardHDelete(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String
    Function GetInsuranceRateCardDDelete(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String
    Function GetInsuranceRateCardDCopy(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String
    Function GetInsuranceMasterRateDSave(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard
    Function GetInsuranceMasterRateDDelete(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String
    Function GetNewInsuranceRateCardDSave(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard
    Function GetNewInsuranceRateCardDDelete(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String
    Function GetTPLToCustSave(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard
    Function GetTPLToCustDelete(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String
    Function GetOtherCoverage(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard
End Interface
