


Public Interface IInsuranceStandardPremium

    Function processCopyStandardPremiumRate(ByVal customClass As Parameter.InsuranceStandardPremium) As String
    Function getInsuranceStandardPremium(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
    Function getInsuranceStandardPremiumReport(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
    Function InsuranceStandardPremiumSaveAdd(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
    Function InsuranceStandardPremiumSaveEdit(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
    Function InsuranceStandardPremiumDelete(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium

End Interface

