Public Interface IInsCoSelection
    Function GetAgreementList(ByVal oInsCo As Parameter.InsCoSelectionList) As Parameter.InsCoSelectionList
    Function GetInsCompanyList(ByVal oInsCo As Parameter.InsCoSelectionList) As Parameter.InsCoSelectionList
    Function GetInsCompanyListDataSet(ByVal oInsCo As Parameter.InsCoSelectionList) As Parameter.InsCoSelectionList
    Function GetInsCompanyByID(ByVal strConnection As String, ByVal InsCoID As String) As Parameter.InsCoSelectionList

    Function InsCompanyAdd(ByVal customClass As Parameter.InsCo, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As String
    Sub InsCompanyEdit(ByVal customClass As Parameter.InsCo, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
    Sub InsCompanyDelete(ByVal customclass As Parameter.InsCo)


    Function GetInsCompanyBranchList(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList
    Function GetInsCompanyBranchByID(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList

    Sub InsCompanyBranchAdd(ByVal customClass As Parameter.InsCoBranch, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal, ByVal oClassBankAccount As Parameter.BankAccount)
    Sub InsCompanyBranchEdit(ByVal customClass As Parameter.InsCoBranch, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal, ByVal oClassBankAccount As Parameter.BankAccount)
    Sub InsCompanyBranchDelete(ByVal customClass As Parameter.InsCoBranch)


    Function GetRateList(ByVal inscoRate As Parameter.InsCoRate) As Parameter.InsCoSelectionList
    Function GetRateByID(ByVal inscoRate As Parameter.InsCoRate) As Parameter.InsCoSelectionList
    Function GetRateReport(ByVal InsCoTPL As Parameter.InsCoRate) As Parameter.InsCoSelectionList

    Sub RateAdd(ByVal inscoRate As Parameter.InsCoRate)
    Sub RateEdit(ByVal inscoRate As Parameter.InsCoRate)
    Sub RateDelete(ByVal inscoRate As Parameter.InsCoRate)
    Sub CopyInsCoBranchRate(ByVal inscoRate As Parameter.InsCoRate)


    Function GetTPLList(ByVal inscoTPL As Parameter.InsCoTPL) As Parameter.InsCoSelectionList
    Function GetTPLByID(ByVal inscoTPL As Parameter.InsCoTPL) As Parameter.InsCoSelectionList
    Function GetTPLReport(ByVal InsCoTPL As Parameter.InsCoTPL) As Parameter.InsCoSelectionList

    Sub TPLAdd(ByVal inscoTPL As Parameter.InsCoTPL)
    Sub TPLEdit(ByVal inscoTPL As Parameter.InsCoTPL)
    Sub TPLDelete(ByVal inscoTPL As Parameter.InsCoTPL)


    Function GetCoverageTypeCombo(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList
    Function GetinsAssetTypeCombo(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList
    Function GetAssetUsageCombo(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList

    Function GetInsuranceComByProduct(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList

End Interface
