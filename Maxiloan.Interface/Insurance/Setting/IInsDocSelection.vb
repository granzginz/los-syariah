
Public Interface IInsDocSelection
#Region "QueryFunctions"
    Function GetInsDocList(ByVal oInsDoc As Parameter.InsDocSelectionList) As Parameter.InsDocSelectionList
    Function GetInsDocById(ByVal oInsDoc As Parameter.InsDocSelectionList) As Parameter.InsDocSelectionList
    Function GetInsDocRpt(ByVal oInsDoc As Parameter.InsDocSelectionList) As Parameter.InsDocSelectionList
#End Region
#Region "TableMaintenance Functions"
    Sub AddInsDoc(ByVal oInsDoc As Parameter.InsDocSelectionList)
    Sub UpdateInsDoc(ByVal oInsDoc As Parameter.InsDocSelectionList)
    Sub DeleteInsDoc(ByVal oInsDoc As Parameter.InsDocSelectionList)
#End Region
End Interface
