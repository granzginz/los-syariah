Public Interface iInsuranceCompany

    Function InsCompanySave(ByVal customClass As Parameter.eInsuranceCompany, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As Parameter.eInsuranceCompany
    Sub InsCompanyDelete(ByVal customclass As Parameter.eInsuranceCompany)

    Function InsCompanyBranchSave(ByVal customClass As Parameter.eInsuranceCompany, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal, ByVal oClassBankAccount As Parameter.BankAccount) As Parameter.eInsuranceCompany
    'Sub InsCompanyBranchEdit(ByVal customClass As Entities.InsCoBranch, ByVal oClassAddress As Entities.Address, ByVal oClassPersonal As Entities.Personal, ByVal oClassBankAccount As Entities.BankAccount)
    Sub InsCompanyBranchDelete(ByVal customClass As Parameter.eInsuranceCompany)


    'Function GetRateList(ByVal inscoRate As Entities.InsCoRate) As Entities.InsCoSelectionList
    'Function GetRateByID(ByVal inscoRate As Entities.InsCoRate) As Entities.InsCoSelectionList
    'Function GetRateReport(ByVal InsCoTPL As Entities.InsCoRate) As Entities.InsCoSelectionList

    Function RateSave(ByVal inscoRate As Parameter.eInsuranceCompany) As Parameter.eInsuranceCompany
    'Sub RateEdit(ByVal inscoRate As Entities.InsCoRate)
    Sub RateDelete(ByVal inscoRate As Parameter.eInsuranceCompany)
    Sub CopyInsCoBranchRate(ByVal inscoRate As Parameter.eInsuranceCompany)


    'Function GetTPLList(ByVal inscoTPL As Entities.InsCoTPL) As Entities.InsCoSelectionList
    'Function GetTPLByID(ByVal inscoTPL As Entities.InsCoTPL) As Entities.InsCoSelectionList
    'Function GetTPLReport(ByVal InsCoTPL As Entities.InsCoTPL) As Entities.InsCoSelectionList

    Function TPLSave(ByVal inscoTPL As Parameter.eInsuranceCompany) As Parameter.eInsuranceCompany
    'Sub TPLEdit(ByVal inscoTPL As Entities.InsCoTPL)
    Sub TPLDelete(ByVal inscoTPL As Parameter.eInsuranceCompany)


    'Function GetCoverageTypeCombo(ByVal InsCoBranch As Entities.InsCoBranch) As Entities.InsCoSelectionList
    'Function GetinsAssetTypeCombo(ByVal InsCoBranch As Entities.InsCoBranch) As Entities.InsCoSelectionList
    'Function GetAssetUsageCombo(ByVal InsCoBranch As Entities.InsCoBranch) As Entities.InsCoSelectionList

    Function CoverageSave(ByVal inscoCoverage As Parameter.eInsuranceCompany) As Parameter.eInsuranceCompany

    Function InsCompanyBranchSaveEdit(ByVal oClassEdit As Parameter.eInsuranceCompany) As Parameter.eInsuranceCompany

End Interface
