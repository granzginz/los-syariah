

Public Interface IInsEndorsment
    Function InsEndorsmentList(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
    Function GetData(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
    Function GetDataInsDetail(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
    Function GetDataCbTPL(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
    Function GetDataCbCoverage(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
    Function GetEndorsCalculationResult(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
    Function GetInsured_Coverage(ByVal oCustomClass As Parameter.InsEndorsment, ByVal oData1 As DataTable) As Parameter.InsEndorsment
    Function SaveData(ByVal customClass As Parameter.InsEndorsment, ByVal result As DataTable) As Parameter.InsEndorsment
    Function PrintForm(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
    Function ViewData(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
End Interface


