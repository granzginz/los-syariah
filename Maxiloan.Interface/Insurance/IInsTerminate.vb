
Public Interface IInsTerminate
    Function InsTerminateList(ByVal customclass As Parameter.InsTerminate) As Parameter.InsTerminate
    Function InsrequestPrint(ByVal customclass As Parameter.InsTerminate) As DataSet
    Function InsrequestPrintRefund(ByVal customclass As Parameter.InsTerminate) As DataSet
    Function InsTerminateDetail(ByVal customclass As Parameter.InsTerminate) As Parameter.InsTerminate
    Function InsCalculate(ByVal customclass As Parameter.InsTerminate) As Parameter.InsTerminate
    Function InsSaveCalculate(ByVal customclass As Parameter.InsTerminate) As Boolean
    Function InsRefundCalculate(ByVal customclass As Parameter.InsTerminate) As DataSet
    Sub InsInsertMail(ByVal customclass As Parameter.InsTerminate)
    Sub InsInsertMailRefund(ByVal customclass As Parameter.InsTerminate)
    Function InsApproveRejectCalculate(ByVal customclass As Parameter.InsTerminate) As String
End Interface
