
Public Interface IInsuranceInq
#Region "InsuranceInquiryInvoice"
    Function InqInsInvoice(ByVal ocustomclass As Parameter.InsInqEntities) As Parameter.InsInqEntities
#End Region
#Region "InsuranceInquiryExpired"
    Function InqInsExpired(ByVal ocustomclass As Parameter.InsInqEntities) As Parameter.InsInqEntities
#End Region
#Region "ReportInsStatistic"
    Function GetReportInsStatitistic(ByVal ocustomclass As Parameter.InsInqEntities) As Parameter.InsInqEntities
#End Region

End Interface
