Public Interface ISPPA
    Function GetListCreateSPPA(ByVal customClass As Parameter.SPPA) As Parameter.SPPA
    Function GenerateSPPA(ByVal customClass As Parameter.SPPA) As Parameter.SPPA
    Function PrintMailTransaction(ByVal customClass As Parameter.MailTransaction) As Parameter.SPPA
    Function GetSPPAList(ByVal customClass As Parameter.SPPA) As Parameter.SPPA
    Function GetSPPAReport(ByVal customClass As Parameter.SPPA) As DataSet
    Function GetSPPAReportCP(ByVal customClass As Parameter.SPPA) As DataSet
    Function GetSPPAReportJK(ByVal customClass As Parameter.SPPA) As DataSet
End Interface


