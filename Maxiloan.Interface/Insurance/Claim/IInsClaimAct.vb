Public Interface IInsClaimAct
#Region "Activity"


    Function ClaimActList(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct
    Function ClaimActDetail(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct
    Function ClaimActGetDoc(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct
    Function ClaimActsave(ByVal customclass As Parameter.InsClaimAct) As Boolean
    Function ClaimRejectsave(ByVal customclass As Parameter.InsClaimAct) As Boolean
    Function ClaimAcceptsave(ByVal customclass As Parameter.InsClaimAct) As Boolean
#End Region
#Region "Setting"
    Function GetInsRateCategory(ByVal customclass As Parameter.InsClaimAct) As DataSet
    Function SaveClaimDoc(ByVal customclass As Parameter.InsClaimAct) As Boolean
    Function DocclaimList(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct
    Function GetDocMaster(ByVal customclass As Parameter.InsClaimAct) As DataSet
    Function DeleteClaimDoc(ByVal customclass As Parameter.InsClaimAct) As Boolean
    Function ClaimDocReport(ByVal customclass As Parameter.InsClaimAct) As DataSet
#End Region
#Region "InsuranceBilling"
    Function InsuranceBillingReport(ByVal customclass As Parameter.InsClaimAct) As DataSet
#End Region

#Region "ClaimInquery"
    Function GetInsCo(ByVal customclass As Parameter.InsClaimAct) As DataSet
    Function ClaimInquiryList(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct
    Function GetClaimInqDet(ByVal customclass As Parameter.InsClaimAct) As DataSet
    Function GetClaimActList(ByVal customclass As Parameter.InsClaimAct) As DataSet
    Function ViewPolicy(ByVal customclass As Parameter.InsClaimAct) As DataSet
    Function GetClaimActHistList(ByVal customclass As Parameter.InsClaimAct) As DataSet
#End Region

End Interface
