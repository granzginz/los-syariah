Public Interface IAdvClaimReq
    Function AdvClaimReqPaging(ByVal customClass As Parameter.AdvClaimReq) As Parameter.AdvClaimReq
    Function AdvanceClaimCostRequestView(ByVal customclass As Parameter.AdvClaimReq) As Parameter.AdvClaimReq
    Function AdvanceClaimCostRequestViewGrid(ByVal customClass As Parameter.AdvClaimReq) As Parameter.AdvClaimReq
    Sub AdvanceClaimCostRequestSave(ByVal customClass As Parameter.AdvClaimReq)
    Function AdvanceClaimCostRequestViewApproval(ByVal customclass As Parameter.AdvClaimReq) As Parameter.AdvClaimReq


End Interface
