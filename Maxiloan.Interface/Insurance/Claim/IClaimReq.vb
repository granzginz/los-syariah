Public Interface IClaimReq
    Function ClaimrequestList(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest
    Function ClaimrequesDetail(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest
    Function ClaimrequestCoverage(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest
    Function ClaimrequestSave(ByVal customclass As Parameter.ClaimRequest) As Boolean
    Function ClaimrequestGetClaimType(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest
    Function ClaimrequestPrint(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest
    Function ViewInsuraceClaimInq(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest
    Function ClaimrequestEdit(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest
    Function ClaimrequestListApproval(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct
    Function ClaimrequestSaveApprove(ByVal customclass As Parameter.InsClaimAct) As Boolean
End Interface
