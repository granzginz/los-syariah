

Public Interface IPolicyReceive

    Function getInsurancePolicyReceive(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
    Sub savePolicyUpload(ByVal strConnection As String, ByVal strAgreementNo As String, ByVal strPolicyNumber As String, ByVal PolicyReceiveDate As Date, ByVal strReceiveBy As String)
    Function getPolicyUpload(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
    Sub savePolicyUploadXLS(ByVal customClass As Parameter.InsuranceStandardPremium)
    Sub NotaAsuransiSave(ByVal oCustomer As Parameter.InsuranceStandardPremium)
End Interface


