

Public Interface IInsCoAllocationDetailList

    Function GetListByAgreementNo(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
    Function GetListByApplicationId(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
    Function GetGridInsuranceStatistic(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
    Function GetGridTenor(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
    Function GetGridInsCoPremium(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
    Function SaveInsuranceCompanySelectionLastProcess(ByVal customClass As Parameter.InsCoAllocationDetailList) As String
    Function GetGridInsCoPremiumView(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
    Function PenutupanAsuransiManualUpdate(ByVal customClass As Parameter.InsCoAllocationDetailList) As String

    Sub InitMaskAssComponent(ByVal customClass As Parameter.MaskAssCalcEng)
    Function CalcKoreksiMaskapaiAss(ByVal customClass As Parameter.MaskAssCalcEng) As DataSet
    Function KoreksiMaskapaiAss(ByVal customClass As Parameter.MaskAssCalcEng) As String
End Interface


