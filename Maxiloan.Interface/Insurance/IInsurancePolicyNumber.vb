

Public Interface IInsurancePolicyNumber
    Function GetInsPolicyNumber(ByVal customClass As Parameter.InsPolicyNumber) As Parameter.InsPolicyNumber
    Sub InsurancePolicyNumberSaveEdit(ByVal customClass As Parameter.InsPolicyNumber)
    Function BankersClauseDSaveAdd(ByVal customClass As Parameter.BankersClause) As String
    Function BankersClauseHSaveAdd(ByVal customClass As Parameter.BankersClause) As String
    Function GetBankersClause(ByVal customClass As Parameter.BankersClause) As Parameter.BankersClause
    Function GetInsCompanyBranchByID(ByVal customClass As Parameter.BankersClause) As Parameter.BankersClause
    Function GetBankersClauseViewPrint(ByVal customClass As Parameter.BankersClause) As Parameter.BankersClause
    Function GetBankersClausePrint(ByVal customClass As Parameter.BankersClause) As DataSet
End Interface
