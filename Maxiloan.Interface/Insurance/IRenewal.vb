

Public Interface IRenewal

    Function GetRenewalDetailTop(ByVal customClass As Parameter.Renewal) As Parameter.Renewal
    Function GetRenewalNewCoverage(ByVal customClass As Parameter.Renewal) As Parameter.Renewal
    Function CalculateAndSaveRenewal(ByVal customClass As Parameter.Renewal) As Parameter.Renewal
    Function GetCoverageRenewalGridInsCALCULATE(ByVal customClass As Parameter.Renewal) As Parameter.Renewal


End Interface
