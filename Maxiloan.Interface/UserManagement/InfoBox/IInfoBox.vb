
Public Interface IInfoBox
    Sub MasterInfoBoxAdd(ByVal customclass As Parameter.InfoBox)
    Sub MasterInfoBoxEdit(ByVal customclass As Parameter.InfoBox)
    Sub MasterInfoBoxDelete(ByVal customclass As Parameter.InfoBox)
    Function MasterInfoBoxView(ByVal customclass As Parameter.InfoBox) As Parameter.InfoBox
    Function UserInfoBoxPaging(ByVal customclass As Parameter.InfoBox) As Parameter.InfoBox
    Sub UserInfoBoxUpdate(ByVal customclass As Parameter.InfoBox)
End Interface
