
Public Interface IUserManagementUserApplication
    Function UpdateUserApplication(ByVal customclass As Parameter.UserApplication) As String
    Function DeleteUserApplication(ByVal customclass As Parameter.UserApplication) As String
    Function ListUserApplication(ByVal customclass As Parameter.UserApplication) As Parameter.UserApplication
    Function AddUserApplication(ByVal customclass As Parameter.UserApplication) As String
End Interface
