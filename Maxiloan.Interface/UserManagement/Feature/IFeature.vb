
Public Interface IFeature
    Function ListMasterForm(ByVal customclass As Parameter.Feature) As Parameter.Feature
    Function ListFeatureUser(ByVal customclass As Parameter.Feature) As Parameter.Feature
    Function UpdateFeatureUser(ByVal customclass As Parameter.Feature) As String
End Interface
