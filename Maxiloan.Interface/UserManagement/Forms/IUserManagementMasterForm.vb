
Public Interface IUserManagementMasterForm
    Function ListMasterForm(ByVal customclass As Parameter.MasterForm) As Parameter.MasterForm
    Function ShowMasterForm(ByVal customclass As Parameter.MasterForm) As Parameter.MasterForm
    Function AddMasterForm(ByVal customclass As Parameter.MasterForm) As String
    Function UpdateMasterForm(ByVal customclass As Parameter.MasterForm) As String
    Function DeleteMasterForm(ByVal customclass As Parameter.MasterForm) As String
End Interface
