

Public Interface ISystemAlert
#Region "SystemAlert"
    Sub SystemAlertAdd(ByVal customclass As Parameter.SystemAlert)
    Sub SystemAlertEdit(ByVal customclass As Parameter.SystemAlert)
    Sub SystemAlertDelete(ByVal customclass As Parameter.SystemAlert)
    Function SystemAlertView(ByVal customclass As Parameter.SystemAlert) As Parameter.SystemAlert
    Function GetGroupAlert(ByVal customclass As Parameter.SystemAlert) As DataTable
#End Region

#Region "User Alert"
    Sub UserAlertUpdate(ByVal customclass As Parameter.SystemAlert)
    Function UserAlertPaging(ByVal customclass As Parameter.SystemAlert) As Parameter.SystemAlert
#End Region

#Region "Group Alert"
    Sub GroupAlertAdd(ByVal customclass As Parameter.SystemAlert)
    Sub GroupAlertEdit(ByVal customclass As Parameter.SystemAlert)
    Sub GroupAlertDelete(ByVal customclass As Parameter.SystemAlert)
    Function GroupAlertView(ByVal customclass As Parameter.SystemAlert) As Parameter.SystemAlert
#End Region

End Interface
