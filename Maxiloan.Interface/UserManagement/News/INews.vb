
Public Interface INews
    Sub MasterNewsAdd(ByVal customclass As Parameter.News)
    Sub MasterNewsEdit(ByVal customclass As Parameter.News)
    Sub MasterNewsDelete(ByVal customclass As Parameter.News)
    Function MasterNewsView(ByVal customclass As Parameter.News) As Parameter.News
    Function NewsDetailView(ByVal customclass As Parameter.News) As Parameter.News
    Sub NewsDetailUpdate(ByVal customclass As Parameter.News)
End Interface
