
Public Interface IUserManagementGroupUser
    Function ListGroupUser(ByVal customclass As Parameter.GroupUser) As Parameter.GroupUser
    Function AddGroupUser(ByVal customclass As Parameter.GroupUser) As String
    Function UpdateGroupUser(ByVal customclass As Parameter.GroupUser) As String
    Function DeleteGroupUser(ByVal customclass As Parameter.GroupUser) As String
End Interface

