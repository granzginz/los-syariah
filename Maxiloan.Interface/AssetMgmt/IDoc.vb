

Public Interface IDoc
#Region "DocReceive"
    Function ListDoc(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function ListDocAssetBNI(ByVal customclass As Parameter.DocAssetBNI) As Parameter.DocAssetBNI
    Function InqListDocAssetBNI(ByVal customclass As Parameter.DocAssetBNI) As Parameter.DocAssetBNI
    Function GetFLoc(ByVal customclass As Parameter.DocRec) As DataTable
    Function GetRack(ByVal strConnection As String, ByVal strBranch As String, Optional isFund As Boolean = False) As DataTable
    Function DocListPaging(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function DocListPagingAssetBNI(ByVal customclass As Parameter.DocAssetBNI) As Parameter.DocAssetBNI
    Function DocRecSave(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function GetAssetRegistration(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function GetTaxDate(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function GetBranchName(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function SPPADList(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function SaveDocAssetBNI(ByVal customclass As Parameter.DocAssetBNI) As Parameter.DocAssetBNI
#End Region
#Region "DocBorrow"
    Function DocBorrowSave(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
#End Region
#Region "DocBorrow"
    Function DocBorrowPledgingSave(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
#End Region
#Region "DocRelease"
    Function DocReleaseSave(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function ListInfo(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function PrintBPKBSerahTerima(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
#End Region
#Region "DocChangeLoc"
    Function DocChangeLocSave(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
#End Region
#Region "Inquiry"
    Function GetRackBranch(ByVal customclass As Parameter.DocRec) As DataTable
    Function ListInquiry(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function GetAssetHistory(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function GetBPKBPosition(ByVal customclass As Parameter.DocRec) As Parameter.DocRec

    Function ListTboInquiry(cnn As String, currentPage As Integer, PageSize As Integer, branchid As String, businessdate As DateTime, aging As DateTime, where As String) As Parameter.DocRec
#End Region
#Region "Setting"
#Region "Rack"
    Function AddRack(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function DeleteRack(ByVal customclass As Parameter.DocRec) As Parameter.DocRec

#End Region

#Region "Filling"
    Function AddFill(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function DeleteFill(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
#End Region
#End Region
#Region "Reports"
    Function NotExistsMainDocReport(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function SummaryBPKBReport(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function CreateSPAssetDocument(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function AgreementListADWithDrawal(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function SavePrintSPAssetDoc(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function SavePrintSPPADoc(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function ListPemeriksaanBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function ListReportPemeriksaanBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function ListBlokirBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function ListReportBlokirBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function ListReportBukaBlokirBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function ListBukaBlokirBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function ListPinjamNamaBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function ListReportPinjamNamaBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function GetSPReport(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
#End Region
#Region "DocPledging"
    Function DocPledgeProcess(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function GetDocPledgePaging(ByVal customClass As Parameter.DocRec) As Parameter.DocRec
    Function ProcessReportPrepaymentRequest(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
#End Region
#Region "AdditionalProcess"
    Function SaveEdit(ByVal customclass As Parameter.DocRec, ByVal oData1 As DataTable) As Parameter.DocRec
#End Region
#Region "DocPledgeReceive"
    Function DocPledgeRecSave(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
#End Region


    Function getAssetDocumentStock(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function PemeriksaanBPKBSave(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function HasilCekBPKBSave(ByVal customclass As Parameter.DocRec) As Parameter.DocRec

    Function DoCreateStockOpname(cnn As String, rows As Parameter.StockOpDoc) As String
    Function GetResultOpnamePage(cnn As String, currentPage As Integer, pageSize As Integer, opNameNo As String) As Parameter.StockOpDoc

    Function GetProcesResultOnhandOpnamePage(cnn As String, currentPage As Integer, pageSize As Integer, opNameNo As String) As Parameter.StockOpDoc
    Function GetProcesResultActualOpnamePage(cnn As String, currentPage As Integer, pageSize As Integer, opNameNo As String) As Parameter.StockOpDoc
    Function ProsesStockOpname(cnn As String, StockOpnameNo As String, ActStockOpnameNo As String, prosesDate As DateTime) As String

    Function getOpnameNo(cnn As String, prm As DateTime, str As String) As String
    Function SaveDocReceiveJanji(ByVal customclass As Parameter.DocRec) As Parameter.DocRec
    Function GetResultActualOpnamePage(cnn As String, currentPage As Integer, pageSize As Integer, opNameNo As String) As Parameter.StockOpDoc
    Function DocInformation(ByVal customclass As Parameter.DocRec) As DataTable
    Sub SaveStockOpnameActual(ByVal oCustomClass As Parameter.StockOpDoc)

    Function ProcessReportBBNRequest(ByVal oCustomClass As Parameter.DocRec) As Parameter.DocRec
End Interface
