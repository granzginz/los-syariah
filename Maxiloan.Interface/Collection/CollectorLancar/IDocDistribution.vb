

Public Interface IDocDistribution
    'Function InventoryAppraisalListCG(ByVal customclass As Entities.InventoryAppraisal) As Entities.InventoryAppraisal
    Function InqDocumentDistributionPaging(ByVal customclass As Parameter.DocDistribution) As Parameter.DocDistribution
    Function DocumentDistributionPaging(ByVal customclass As Parameter.DocDistribution) As Parameter.DocDistribution
    Function DocumentDistributionSave(ByVal customclass As Parameter.DocDistribution) As Parameter.DocDistribution
    ''Function GenerateKuitansiTagihOnRequest(ByVal customclass As Entities.ReceiptNotesOnRequest) As Entities.ReceiptNotesOnRequest
    '''Function RALSaveDataPrintRAL(ByVal customclass As Entities.RALChangeExec) As Entities.RALChangeExec
    'Function DataInventoryAppraisal(ByVal customclass As Entities.InventoryAppraisal) As Entities.InventoryAppraisal
    'Function ViewAppraisal(ByVal customclass As Entities.InventoryAppraisal) As Entities.InventoryAppraisal
    'Function ViewAppraisalbidder(ByVal customclass As Entities.InventoryAppraisal) As Entities.InventoryAppraisal
    'Function GetAccruedInterest(ByVal customclass As Entities.InventoryAppraisal) As Entities.InventoryAppraisal
    Function RequestDataDocDistribution(ByVal customclass As Parameter.DocDistribution) As Parameter.DocDistribution
End Interface
