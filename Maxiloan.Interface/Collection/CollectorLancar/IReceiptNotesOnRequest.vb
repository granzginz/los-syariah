

Public Interface IReceiptNotesOnRequest
    Function ReceiptNotesOnRequestListCG(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
    Function ReceiptNotesOnRequestList(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
    Function ViewDataInstallment(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
    Function GenerateKuitansiTagihOnRequest(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
    'Function RALSaveDataPrintRAL(ByVal customclass As Entities.RALChangeExec) As Entities.RALChangeExec
    Function DataReceiptNotesOnRequest(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
    Function RequestDataSelect(ByVal customclass As Parameter.ReceiptNotesOnRequest) As Parameter.ReceiptNotesOnRequest
End Interface
