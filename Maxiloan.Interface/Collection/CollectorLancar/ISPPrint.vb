
Public Interface ISPPrint
    Function ListCGSPPrint(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint
    Function ListSPPrinting(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint
    Function ListReportSPPrinting(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint
    Function SPPrintProcess(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint
    Function ListSSPrinting(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint
    Function ListReportSSPrinting(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint
    Function SSPrintProcess(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint
    Function ListRubrikPrint(ByVal customclass As Parameter.SPPrint) As Parameter.SPPrint

End Interface
