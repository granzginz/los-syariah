Public Interface ISPVActivity
    Function SPVActivityListAgreement(ByVal SPVAct As Parameter.SPVActivity) As Parameter.SPVActivity
    Function SPVActivityListAgreementChoosen(ByVal SPVAct As Parameter.SPVActivity) As Parameter.SPVActivity
    Function GetCollectorType(ByVal SPVAct As Parameter.SPVActivity) As String

    Sub SPVActivityFixed(ByVal SPVAct As Parameter.SPVActivity)
    Sub SPVActivityRAL(ByVal SPVAct As Parameter.SPVActivity)
    Sub SPVActivityRedDot(ByVal SPVAct As Parameter.SPVActivity)
    Sub SPVActivityCases(ByVal SPVAct As Parameter.SPVActivity)

    Function GetComboCollector(ByVal SPVAct As Parameter.SPVActivity) As Parameter.SPVActivity
    Function GetComboCases(ByVal SPVAct As Parameter.SPVActivity) As Parameter.SPVActivity

    Function SPVListResultAction(ByVal SPVAct As Parameter.SPVActivity) As DataSet
    Function SPVActivityPlanSave(ByVal SPVAct As Parameter.SPVActivity) As Boolean

End Interface
