

Public Interface IPDCRequestPrint
    Function PDCRequestListCG(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint
    Function PDCRequestList(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint
    Function PDCRequestSave(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint
    Function PDCRequestReport(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint
    Function PDCRequestSurat(ByVal customclass As Parameter.PDCRequestPrint) As Parameter.PDCRequestPrint
End Interface

