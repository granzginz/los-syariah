

Public Interface ICLActivity
    Function CLActivityList(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
    Function CLActivityListRemidial(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
    Function CLActivityListCollector(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
    Function CLActivityDataAgreement(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
    Function CLActivityActionResult(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
    Function CLActivitySave(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
    Function GetResult(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
    Sub DCRSave(ByVal CustomClass As Parameter.CLActivity)
    Function GetDCRList(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
    Function CLActivityListMobCollector(ByVal customclass As Parameter.CLActivity) As Parameter.CLActivity
End Interface
