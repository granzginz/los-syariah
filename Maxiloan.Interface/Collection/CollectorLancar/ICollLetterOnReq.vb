
Public Interface ICollLetterOnReq
    Function CollLetterOnReqView(ByVal ocustomclass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq
    Function LetterNameOnReqCombo(ByVal ocustomclass As Parameter.CollLetterOnReq) As DataTable
    Function CollLetterOnReqPaging(ByVal oCustomClass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq
    Function LetterNameOnReqComboView(ByVal ocustomclass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq
    Function CollLetterTemplateField(ByVal oCustomClass As Parameter.CollLetterOnReq) As DataTable
    Function CollLetterTemplateFieldName(ByVal ocustomclass As Parameter.CollLetterOnReq) As DataTable
    Sub CollLetterOnReqSaving(ByVal oCustomClass As Parameter.CollLetterOnReq)
    Function CollLetterOnReqViewTextToPrint(ByVal ocustomclass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq
    Function CollLetterReports(ByVal oCustomClass As Parameter.CollLetterOnReq) As Parameter.CollLetterOnReq

End Interface
