

Public Interface IDeskCollReminderActivity
    Function DeskCollReminderList(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
    Function DeskCollReminderView(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
    Function ListResultAction(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
    Function SaveDeskCollActivity(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
    Function DownloadSMSReminderList(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
    Function DownloadSMSReminderPaging(ByVal customclass As Parameter.DeskCollReminderActivity) As Parameter.DeskCollReminderActivity
End Interface
