﻿Public Interface ILokasiAsset
    Function ListLokasiAsset(ByVal customclass As Parameter.LokasiAsset) As Parameter.LokasiAsset
    Function EditLokasiAsset(ByVal customclass As Parameter.LokasiAsset, ByVal oAddress As Parameter.Address) As String
    Function AddLokasiAsset(ByVal customclass As Parameter.LokasiAsset, ByVal oAddress As Parameter.Address) As String
    Function DeleteLokasiAsset(ByVal customclass As Parameter.LokasiAsset) As String
    Function ReportLokasiAsset(ByVal customclass As Parameter.LokasiAsset) As Parameter.LokasiAsset
End Interface
