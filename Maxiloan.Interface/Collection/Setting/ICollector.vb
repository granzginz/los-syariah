Public Interface ICollector
    Function GetCollectorList(ByVal oCollector As Parameter.Collector) As Parameter.Collector
    Function GetCollectorByID(ByVal oCollector As Parameter.Collector) As Parameter.Collector

    Function GetCollectorZipCode(ByVal oCollector As Parameter.Collector) As Parameter.Collector

    Function GetCollectorCombo(ByVal oCollector As Parameter.Collector) As Parameter.Collector
    Function GetCollectorReport(ByVal strconn As String) As Parameter.Collector
    Function GetBranchAddress(ByVal oCollector As Parameter.Collector) As String
    Function GetCollectorRAL(ByVal oCollector As Parameter.Collector) As Parameter.Collector
    Function GetCollectorDetail(ByVal oCollector As Parameter.Collector) As Parameter.Collector

    Function GetCollectorTypeCombo(ByVal oCollector As Parameter.Collector) As Parameter.Collector
    Function GetSupervisorCombo(ByVal oCollector As Parameter.Collector) As Parameter.Collector

    Sub CollectorAdd(ByVal oCollector As Parameter.Collector)
    Sub CollectorEdit(ByVal oCollector As Parameter.Collector)
    Sub CollectorDelete(ByVal oCollector As Parameter.Collector)

    Sub CollectorAllocationSave(ByVal oCollector As Parameter.Collector)
    Function GetCollectorAllocationPaging(ByVal oCollector As Parameter.Collector) As Parameter.Collector
    Function GetCollectorZPList(ByVal oCollector As Parameter.Collector) As Parameter.Collector
    Function CollectorZPSave(ByVal oCollector As Parameter.Collector, ByVal oData As DataTable) As Parameter.Collector
    Function GetCollectorZPDetail(ByVal oCollector As Parameter.Collector) As Parameter.Collector
    Function GetCollectorZPByKecamatan(ByVal oCollector As Parameter.Collector) As Parameter.Collector
    Sub CollectorEditMaxAcc(ByVal oCollector As Parameter.Collector)
    Function GetCollectorDeskCollByID(ByVal oCollector As Parameter.Collector) As Parameter.Collector
    Sub CollectorDeskCollSave(ByVal oCollector As Parameter.Collector)
    Sub RollingWilayahSave(ByVal oCollector As Parameter.Collector)
    Sub RollingCollectorSave(ByVal oCollector As Parameter.Collector)
    Sub GantiCollectorSave(ByVal oCollector As Parameter.Collector)
    Function GetPemberiKuasaSKEList(ByVal oCollector As Parameter.Collector) As Parameter.Collector
    Function PemberiKuasaSKEDelete(ByVal oCollector As Parameter.Collector) As String
    Function PemberiKuasaSKEAdd(ByVal oCollector As Parameter.Collector) As String
    Function PemberiKuasaSKEEdit(ByVal oCollector As Parameter.Collector) As String
End Interface