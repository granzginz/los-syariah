Public Interface ICollectorProf
    Function GetCollectorProfList(ByVal oCollector As Parameter.Collector) As Parameter.Collector
    Function GetCollectorProfByID(ByVal oCollector As Parameter.Collector) As Parameter.Collector

    Function GetSupervisorCombo(ByVal oCollector As Parameter.Collector) As Parameter.Collector
    Function GetCollectorProfReport(ByVal strconn As String) As Parameter.Collector

    Function GetCollectorProfDetail(ByVal oCollector As Parameter.Collector) As Parameter.Collector

    Sub CollectorProfAdd(ByVal oCollector As Parameter.Collector, ByVal oAddress As Parameter.Address, ByVal personal As Parameter.Personal)
    Sub CollectorProfEdit(ByVal oCollector As Parameter.Collector, ByVal oAddress As Parameter.Address, ByVal personal As Parameter.Personal)
    Sub CollectorProfDelete(ByVal oCollector As Parameter.Collector)

    'CollectorMOU
    Function GetCollectorMOUList(ByVal oCollector As Parameter.Collector) As Parameter.Collector
    Function GetCollectorMOUByID(ByVal oCollector As Parameter.Collector) As Parameter.Collector

    Function GetBranchAddress(ByVal oCollector As Parameter.Collector) As String
    Function GetCollectorMOUDetail(ByVal oCollector As Parameter.Collector) As Parameter.Collector

    Sub CollectorMOUAdd(ByVal oCollector As Parameter.Collector)
    Sub CollectorMOUEdit(ByVal oCollector As Parameter.Collector)
    Sub CollectorMOUDelete(ByVal oCollector As Parameter.Collector)
    Sub CollectorMOUExtendUpdate(ByVal oCollector As Parameter.Collector)
    Function CollectorMOUExtendHistory(ByVal oCollector As Parameter.Collector) As Parameter.Collector



End Interface