
Public Interface IAssetCheckList
    Function ListAssetCheckList(ByVal customclass As Parameter.AssetCheckList) As Parameter.AssetCheckList
    Function EditAssetCheckList(ByVal customclass As Parameter.AssetCheckList) As String
    Function AddAssetCheckList(ByVal customclass As Parameter.AssetCheckList) As String
    Function DeleteAssetCheckList(ByVal customclass As Parameter.AssetCheckList) As String
    Function ListAssetCheckListLookUp(ByVal customclass As Parameter.AssetCheckList) As Parameter.AssetCheckList
    Function ReportAssetCheckList(ByVal customclass As Parameter.AssetCheckList) As Parameter.AssetCheckList
End Interface
