Public Interface IIncentiveBC
    Function IncentiveBCPaging(ByVal oCustomClass As Parameter.IncentiveBC) As Parameter.IncentiveBC
    Sub IncentiveBCSave(ByVal oCustomClass As Parameter.IncentiveBC)
    Sub IncentiveBCDelete(ByVal oCustomClass As Parameter.IncentiveBC)
    Sub IncentiveBCUpdate(ByVal oCustomClass As Parameter.IncentiveBC)
    Function IncentiveBCReports(ByVal oCustomClass As Parameter.IncentiveBC) As Parameter.IncentiveBC

End Interface
