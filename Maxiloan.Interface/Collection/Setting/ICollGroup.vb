Public Interface ICollGroup
    Function GetCollGroupList(ByVal oCollGroup As Parameter.CollGroup) As Parameter.CollGroup
    Function GetCollGroupByID(ByVal oCollGroup As Parameter.CollGroup) As Parameter.CollGroup
    Function GetCollGroupReport(ByVal strconn As String) As Parameter.CollGroup
    Function GetBranchAddress(ByVal oCollGroup As Parameter.CollGroup) As DataTable
    Function GetBranchCombo(ByVal oCollGroup As Parameter.CollGroup) As Parameter.CollGroup

    Sub CollGroupAdd(ByVal oCollGroupResult As Parameter.CollGroup, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
    Sub CollGroupEdit(ByVal oCollGroupResult As Parameter.CollGroup, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
    Sub CollGroupDelete(ByVal oCollGroupResult As Parameter.CollGroup)

End Interface