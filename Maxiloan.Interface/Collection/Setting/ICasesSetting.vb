
Public Interface ICasesSetting
    Function ListCasesSetting(ByVal customclass As Parameter.CasesSetting) As Parameter.CasesSetting
    Function ReportCasesSetting(ByVal customclass As Parameter.CasesSetting) As Parameter.CasesSetting
    Function AddCasesSetting(ByVal customclass As Parameter.CasesSetting) As String
    Function EditCasesSetting(ByVal customclass As Parameter.CasesSetting) As String
    Function DeleteCasesSetting(ByVal customclass As Parameter.CasesSetting) As String
    Function ListCases(ByVal customclass As Parameter.CasesSetting) As Parameter.CasesSetting
End Interface
