Public Interface IIncentiveRN
    Function IncentiveRNPaging(ByVal oCustomClass As Parameter.IncentiveRN) As Parameter.IncentiveRN
    Function IncentiveRNReports(ByVal oCustomClass As Parameter.IncentiveRN) As Parameter.IncentiveRN
    Sub IncentiveRNDelete(ByVal oCustomClass As Parameter.IncentiveRN)
    Sub IncentiveRNUpdate(ByVal oCustomClass As Parameter.IncentiveRN)
    Sub IncentiveRNSave(ByVal oCustomClass As Parameter.IncentiveRN)
End Interface
