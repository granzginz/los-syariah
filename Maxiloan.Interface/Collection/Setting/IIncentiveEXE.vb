
Public Interface IIncentiveEXE
    Function IncentiveEXEPaging(ByVal oCustomClass As Parameter.IncentiveEXE) As Parameter.IncentiveEXE
    Sub IncentiveEXESave(ByVal oCustomClass As Parameter.IncentiveEXE)
    Sub IncentiveEXEUpdate(ByVal oCustomClass As Parameter.IncentiveEXE)
    Sub IncentiveEXEDelete(ByVal oCustomClass As Parameter.IncentiveEXE)
    Function IncentiveEXEReports(ByVal oCustomClass As Parameter.IncentiveEXE) As Parameter.IncentiveEXE
End Interface
