Public Interface ICollZipCode
    Function GetCollZipCodeList(ByVal oCollZipCode As Parameter.CollZipCode) As Parameter.CollZipCode
    Function GetCollZipCodeReport(ByVal strconn As String, ByVal city As String) As Parameter.CollZipCode

    Function GetCityCombo(ByVal strconn As String) As DataTable
    Function getCGCombo(ByVal strconn As String) As DataTable
    Function getCollectorCombo(ByVal oCollZipCode As Parameter.CollZipCode) As DataTable

    Sub CollZipCodeEdit(ByVal oCollZipCode As Parameter.CollZipCode)

    Function GetCollectorZipCodeList(ByVal oCollZipCode As Parameter.CollZipCode) As Parameter.CollZipCode
    Function GetCollectorZipCodeReport(ByVal strconn As String, ByVal cgid As String) As Parameter.CollZipCode
    Sub CollectorZipCodeEdit(ByVal oCollZipCode As Parameter.CollZipCode)

End Interface
