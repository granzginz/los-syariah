Public Interface IIncentiveBL
    Function IncentiveBLPaging(ByVal oCustomClass As Parameter.IncentiveBL) As Parameter.IncentiveBL
    Sub IncentiveBLSave(ByVal oCustomClass As Parameter.IncentiveBL)
    Sub IncentiveBLUpdate(ByVal oCustomClass As Parameter.IncentiveBL)
    Sub IncentiveBLDelete(ByVal oCustomClass As Parameter.IncentiveBL)
    Function IncentiveBLReports(ByVal oCustomClass As Parameter.IncentiveBL) As Parameter.IncentiveBL

End Interface

