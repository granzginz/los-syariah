Public Interface IIncentiveBM
    Function IncentiveBMPaging(ByVal oCustomClass As Parameter.IncentiveBM) As Parameter.IncentiveBM
    Sub IncentiveBmUpdate(ByVal oCustomClass As Parameter.IncentiveBM)
    Sub IncentiveBMSave(ByVal oCustomClass As Parameter.IncentiveBM)
    Function IncentiveBMComboList(ByVal oCustomClass As Parameter.IncentiveBM) As DataTable
    Function IncentiveBMReports(ByVal oCustomClass As Parameter.IncentiveBM) As Parameter.IncentiveBM

End Interface
