
Public Interface ICollLetter
    Function CollLetterFieldPaging(ByVal oCustomClass As Parameter.CollLetter) As Parameter.CollLetter
    Function CollLetterPaging(ByVal oCustomClass As Parameter.CollLetter) As Parameter.CollLetter
    Sub CollLetterSave(ByVal oCustomClass As Parameter.CollLetter)
    Sub CollLetterUpdate(ByVal oCustomClass As Parameter.CollLetter)
    Sub CollLetterDelete(ByVal oCustomClass As Parameter.CollLetter)
    Function CollLetterReports(ByVal oCustomClass As Parameter.CollLetter) As Parameter.CollLetter
End Interface
