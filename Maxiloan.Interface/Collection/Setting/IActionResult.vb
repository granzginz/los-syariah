Public Interface IActionResult
    Function GetActionList(ByVal oActionResult As Parameter.ActionResult) As Parameter.ActionResult
    Function GetResultList(ByVal oActionResult As Parameter.ActionResult) As Parameter.ActionResult
    Function GetActionByID(ByVal oActionResult As Parameter.ActionResult) As Parameter.ActionResult
    Function GetResultByID(ByVal oActionResult As Parameter.ActionResult) As Parameter.ActionResult

    Function GetActionReport(ByVal strconn As String) As Parameter.ActionResult
    Function GetResultCategory(ByVal strConn As String) As Parameter.ActionResult

    Sub ActionAdd(ByVal oActionResult As Parameter.ActionResult)
    Sub ActionEdit(ByVal oActionResult As Parameter.ActionResult)
    Sub ActionDelete(ByVal oActionResult As Parameter.ActionResult)

    Sub ResultAdd(ByVal oActionResult As Parameter.ActionResult)
    Sub ResultEdit(ByVal oActionResult As Parameter.ActionResult)
    Sub ResultDelete(ByVal oActionResult As Parameter.ActionResult)

End Interface
