

Public Interface IChangeAddress
    Function AddressListCG(ByVal customclass As Parameter.ChangeAddress) As Parameter.ChangeAddress
    Function ChangeAddressList(ByVal customclass As Parameter.ChangeAddress) As Parameter.ChangeAddress
    Function ChangeAddressView(ByVal customclass As Parameter.ChangeAddress) As Parameter.ChangeAddress
    Function ChangeAddressSave(ByVal customclass As Parameter.ChangeAddress) As Parameter.ChangeAddress
    Function GetAddress(ByVal customclass As Parameter.ChangeAddress) As Parameter.ChangeAddress
End Interface
