

Public Interface IAdminActivity
    Function AdminActivityList(ByVal customclass As Parameter.AdminActivity) As Parameter.AdminActivity
    Function AdminActivityDataAgreement(ByVal customclass As Parameter.AdminActivity) As Parameter.AdminActivity
    Function AdminActivityAction(ByVal customclass As Parameter.AdminActivity) As Parameter.AdminActivity
    Function AdminActivitySave(ByVal customclass As Parameter.AdminActivity) As Parameter.AdminActivity

End Interface
