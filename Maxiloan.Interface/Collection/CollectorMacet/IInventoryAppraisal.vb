

Public Interface IInventoryAppraisal
    Function InventoryAppraisalListCG(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
    Function InventoryAppraisalList(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
    Function SaveInventoryAppraisal(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
    'Function GenerateKuitansiTagihOnRequest(ByVal customclass As Entities.ReceiptNotesOnRequest) As Entities.ReceiptNotesOnRequest
    ''Function RALSaveDataPrintRAL(ByVal customclass As Entities.RALChangeExec) As Entities.RALChangeExec
    Function DataInventoryAppraisal(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
    Function ViewAppraisal(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
    Function ViewAppraisalbidder(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
    Function GetAccruedInterest(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
    Function RequestDataSelectInventory(ByVal customclass As Parameter.InventoryAppraisal) As Parameter.InventoryAppraisal
    Sub GetMRP(ByRef p_Class As Parameter.InventoryAppraisal)
    Sub PrintFormPersetujuanAppraisal(p_Class As Parameter.InventoryAppraisal)
    Sub PrintFormPersetujuanAppraisalSubBid(p_Class As Parameter.InventoryAppraisal)
    Sub PrintFormPersetujuanList(p_Class As Parameter.InventoryAppraisal)
End Interface
