

Public Interface IAssetRepo

    Function AssetRepoList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo
    Function AssetRepoApprovalList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo
    Function AssetRepoListDetail(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo
    Function AssetRepoCheckList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo
    Sub AssetRepoSave(ByVal customclass As Parameter.AssetRepo)
    Sub AssetRepoApproveReject(ByVal customclass As Parameter.AssetRepo, status As String)
    Sub AssetRepoCheckListSave(ByVal customclass As Parameter.AssetRepo)
    Function AssetRepoCLPrintList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo
    Function AssetRepoCLPrintReport(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo
    Function AssetRepoPrepaymentLetterPrint(ByVal oCustomClass As Parameter.AssetRepo) As Parameter.AssetRepo

    Function AssetRepoChangeLocList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo
    Sub AssetRepoChangeLocSave(ByVal customclass As Parameter.AssetRepo)
    Function AssetRepoChangeLocDetail(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo
    Function AssetRepoChangeLocRecList(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo
    Sub AssetRepoChangeLocRecSave(ByVal customclass As Parameter.AssetRepo)
    Sub GetLokasiPenyimpanan(ByRef p_Class As Parameter.AssetRepo)
    Function BiayaPenangananSave(ByVal customclass As Parameter.AssetRepo) As Parameter.AssetRepo

    Function JenisBiayaBOP(cnn As String) As IList(Of Parameter.CommonValueText)
    Function AssetGrade(cnn As String) As IList(Of Parameter.CommonValueText)
    Function AssetGradeValue(cnn As String) As IList(Of Parameter.CommonValueText)
End Interface
