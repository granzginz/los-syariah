Public Interface ICollInvSelling
    Function InvSellingList(ByVal customclass As Parameter.CollInvSelling) As Parameter.CollInvSelling
    Function InvSellingDetail(ByVal customclass As Parameter.CollInvSelling) As Parameter.CollInvSelling
    Function InvSellingSave(ByVal customclass As Parameter.CollInvSelling) As Boolean
    Function ViewAppraisalbidder(ByVal customclass As Parameter.CollInvSelling) As Parameter.CollInvSelling
End Interface
