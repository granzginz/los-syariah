

Public Interface IAssetRelease

    Function AssetReleaseList(ByVal customclass As Parameter.AssetRelease) As Parameter.AssetRelease
    Function AssetReleaseListDetail(ByVal customclass As Parameter.AssetRelease) As Parameter.AssetRelease
    Function AssetReleaseCheckList(ByVal customclass As Parameter.AssetRelease) As Parameter.AssetRelease
    Function AssetReleaseCheckListSave(ByVal customclass As Parameter.AssetRelease) As Boolean
End Interface
