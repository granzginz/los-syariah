Public Interface ICollInvReturn
    Function InvReturnList(ByVal customclass As Parameter.CollInvSelling) As Parameter.CollInvSelling
    Function InvReturnDetail(ByVal customclass As Parameter.CollInvSelling) As Parameter.CollInvSelling
    Function InvReturnSave(ByVal customclass As Parameter.CollInvSelling) As Boolean
End Interface
