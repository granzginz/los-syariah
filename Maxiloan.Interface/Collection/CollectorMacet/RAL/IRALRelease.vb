

Public Interface IRALRelease
    Function RALListCG(ByVal customclass As Parameter.RALRelease) As Parameter.RALRelease
    Function RALReleaseList(ByVal customclass As Parameter.RALRelease) As Parameter.RALRelease
    Function RALDataExtend(ByVal customclass As Parameter.RALRelease) As Parameter.RALRelease
    Function RALReleaseView(ByVal customclass As Parameter.RALRelease) As Parameter.RALRelease
    Function RALReleaseSave(ByVal customclass As Parameter.RALRelease) As Parameter.RALRelease
    Function CekPrepayment(ByVal customclass As Parameter.RALRelease) As Boolean
End Interface
