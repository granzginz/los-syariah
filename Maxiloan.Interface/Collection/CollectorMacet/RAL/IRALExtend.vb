

Public Interface IRALExtend
    Function RALListCG(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend
    Function RALExtendList(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend
    Function RALExtendView(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend
    Function RALExtendSave(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend
    Function RALDataExtend(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend
    Function RALSaveDataPrint(ByVal customclass As Parameter.RALExtend) As Parameter.RALExtend

End Interface
