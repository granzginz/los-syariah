


Public Interface IRALPrinting
    Function RALListCG(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
    Function RALPrintingList(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
    Function RALViewDataSelect(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
    Function RALViewDataCollector(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
    Function RALSaveDataExecutor(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
    Function RALViewHistoryExecutor(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
    Function RALSaveDataPrintRAL(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
    Function RALListReport(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
    Function RALListReportCheckList(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
    Function RequestDataSelect(ByVal customclass As Parameter.RALPrinting) As Parameter.RALPrinting
End Interface
