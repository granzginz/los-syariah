

Public Interface IRALChangeExec
    Function RALListCG(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec
    Function RALChangeExecList(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec
    Function RALViewDataCollector(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec
    Function RALSaveDataExecutor(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec
    Function RALSaveDataPrintRAL(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec
    Function RALDataChangeExec(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec
    Function RequestDataSelect(ByVal customclass As Parameter.RALChangeExec) As Parameter.RALChangeExec
End Interface
