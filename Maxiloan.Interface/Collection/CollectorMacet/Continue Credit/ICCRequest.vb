

Public Interface ICCRequest

    Function CCRequestList(ByVal customclass As Parameter.CCRequest) As Parameter.CCRequest
    Function CCRequestDetail(ByVal customclass As Parameter.CCRequest) As Parameter.CCRequest
    Function CCRequestView(ByVal customclass As Parameter.CCRequest) As Parameter.CCRequest
    Sub CCRequestSave(ByVal customclass As Parameter.CCRequest)
    Function GetComboReason(ByVal customclass As Parameter.CCRequest) As DataTable
    Function ContinueCreditReqReport(ByVal oCustomClass As Parameter.CCRequest) As Parameter.CCRequest
    Function CCRequestListInq(ByVal customclass As Parameter.CCRequest) As Parameter.CCRequest
End Interface
