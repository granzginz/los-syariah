

Public Interface IInvChgExpDate
    Function InvChgExpDateList(ByVal inv As Parameter.InvChgExpDate) As Parameter.InvChgExpDate
    Function InvChgExpDateSave(ByVal inv As Parameter.InvChgExpDate) As Parameter.InvChgExpDate
    Function InvChgExpDateDetail(ByVal customclass As Parameter.InvChgExpDate) As Parameter.InvChgExpDate
    Function InvChgExpDateView(ByVal customclass As Parameter.InvChgExpDate) As Parameter.InvChgExpDate
    Function GetComboReason(ByVal customclass As Parameter.InvChgExpDate) As DataTable

End Interface
