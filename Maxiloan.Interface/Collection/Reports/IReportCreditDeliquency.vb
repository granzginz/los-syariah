

Public Interface IReportCreditDeliquency
    Function ListCGID(ByVal customclass As Parameter.ReportCreditDeliquency) As Parameter.ReportCreditDeliquency
    Function ListCollector(ByVal customclass As Parameter.ReportCreditDeliquency) As Parameter.ReportCreditDeliquency
    Function ListReport(ByVal customclass As Parameter.ReportCreditDeliquency) As Parameter.ReportCreditDeliquency
End Interface
