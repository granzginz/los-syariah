


Public Interface IRptCollOverDue
    Function ViewDataCollector(ByVal customclass As Parameter.RptCollOverDue) As Parameter.RptCollOverDue
    Function ViewReportCollOverDue(ByVal customclass As Parameter.RptCollOverDue) As Parameter.RptCollOverDue
    Function ViewRptCreditDeliquency(ByVal customclass As Parameter.RptCollOverDue) As Parameter.RptCollOverDue
End Interface
