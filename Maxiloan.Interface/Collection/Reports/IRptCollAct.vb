
Public Interface IRptCollAct
    Function ViewDataCollector(ByVal customclass As Parameter.RptCollAct) As Parameter.RptCollAct
    Function ViewReportColAct(ByVal customclass As Parameter.RptCollAct) As Parameter.RptCollAct
    Function ViewSubReportCollAct(ByVal customclass As Parameter.RptCollAct) As Parameter.RptCollAct
End Interface
