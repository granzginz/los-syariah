Public Interface IRptBucketMovement
    Function IncBucketMovementReports(ByVal oCustomClass As Parameter.RptIncBucketMovement) As Parameter.RptIncBucketMovement
    Function GetPeriod(ByVal strConnection As String) As DataTable
    Function GetBeginStatus(ByVal strConnection As String) As DataTable
    Function GetBeginHolder(ByVal oCustomClass As Parameter.RptIncBucketMovement) As DataTable

End Interface
