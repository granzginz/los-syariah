

Public Interface IRALInquiry
    Function RALInqListCG(ByVal customclass As Parameter.RALInquiry) As Parameter.RALInquiry
    Function RALInqListExecutor(ByVal customclass As Parameter.RALInquiry) As Parameter.RALInquiry
    Function RALInqList(ByVal customclass As Parameter.RALInquiry) As Parameter.RALInquiry
    Function RALInqListExtend(ByVal customclass As Parameter.RALInquiry) As Parameter.RALInquiry
    Function RALInqListExecutorCombo(ByVal customclass As Parameter.RALInquiry) As Parameter.RALInquiry

End Interface
