
Public Interface IDCR
    Function GetInqDCRList(ByVal oDCR As Parameter.InqDCR) As Parameter.InqDCR
    Function GetTaskType(ByVal strconn As String) As DataTable
    Function GetCG(ByVal strconn As String) As DataTable
    Function ListReport(ByVal oDCR As Parameter.InqDCR) As DataTable
End Interface
