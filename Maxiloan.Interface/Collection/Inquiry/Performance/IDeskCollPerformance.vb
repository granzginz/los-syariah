Public Interface IDeskCollPerformance
    Function DeskCollPerformancePaging(ByVal oCustomClass As Parameter.DeskCollPerformance) As Parameter.DeskCollPerformance
    Function GetComboCG(ByVal strConnection As String) As DataTable
    Function GetPeriod(ByVal strConnection As String) As DataTable
    Function GetBeginStatus(ByVal strConnection As String) As DataTable
    Function GetEndStatus(ByVal strConnection As String) As DataTable
End Interface
