Public Interface ICollActivity
    Function GetCollActivityList(ByVal oCollAct As Parameter.CollActivity) As Parameter.CollActivity
    Function GetCollActivityResultList(ByVal oCollAct As Parameter.CollActivity) As Parameter.CollActivity
    Function GetCollActivityDetail(ByVal oCollAct As Parameter.CollActivity) As Parameter.CollActivity
    Function GetCollActivityHistory(ByVal oCollAct As Parameter.CollActivity) As Parameter.CollActivity

    Function GetCGCombo(ByVal strconn As String) As DataTable
    Function GetCollectorCombo(ByVal oCollAct As Parameter.CollActivity) As DataTable
    'Function ViewCollection(ByVal customclass As Entities.viewcollection) As Entities.viewcollection
    Function GetCollActivityResultListPrint(ByVal oCollAct As Parameter.CollActivity) As DataTable
End Interface
