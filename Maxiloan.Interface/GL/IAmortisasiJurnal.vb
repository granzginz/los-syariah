﻿Imports Maxiloan.Parameter

Public Interface IAmortisasiJurnal
    Function GetData(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
    Function GetDataEdit(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
    Function GetDataDelete(ByVal oCustomClass As Parameter.AmortisasiJurnal) As String
    Function SaveData(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
    Function SaveEditData(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal

    Function GetDataAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
    Function GetDataSchemeAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
    Function GetDataGridAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
    Function AmortisasiBiayaSave(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
    Function GenerateJournal(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
    Function GenerateJournalList(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal

    Function GetApprovalAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal, ByVal strApproval As String)
    Function GetGridApprovalAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal
    Function GetViewSchemeAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal

    Function Get_RequestNo(ByVal oCustomClass As Parameter.AmortisasiJurnal) As String
End Interface
