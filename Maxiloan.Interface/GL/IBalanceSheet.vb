
Imports Maxiloan.Parameter

Public Interface IBalanceSheet

    Function SelectData(ByRef totRecord As Integer, strCnn As String, ByVal ParamArray param() As Object) As IList(Of BalanceSheetSetup)
    Sub DeleteData(ByVal strCnn As String, ByVal reportId As String)
    Sub InsertData(ByVal strCnn As String, ByVal data As BalanceSheetSetup)
    Sub UpdateData(ByVal strCnn As String, ByVal data As BalanceSheetSetup)
    Function FindById(ByVal strCnn As String, ByVal reportId As String, ByVal companyId As String) As BalanceSheetSetup

    Function SelectBSAccGroup(ByRef totRecord As Integer, strCnn As String, ByVal ParamArray param() As Object) As IList(Of BalanceSheetAccGroup)
    Sub DeleteBSAccGroup(ByVal strCnn As String, ByVal reportGroupId As Integer)
    Sub InsertBSAccGroup(ByVal strCnn As String, ByVal data As BalanceSheetAccGroup)
    Sub UpdateBSAccGroup(ByVal strCnn As String, ByVal data As BalanceSheetAccGroup)
    Function FindByIdBSAccGroup(ByVal strCnn As String, ByVal reportGroupId As Integer) As BalanceSheetAccGroup
    Function SelectAllBSAccGroup(ByVal strCnn As String) As DataTable

    Function SelectBSAccDetail(ByRef totRecord As Integer, strCnn As String, ByVal ParamArray param() As Object) As IList(Of BalanceSheetAccDetail)
    Sub DeleteBSAccDetail(ByVal strCnn As String, ByVal reportGroupId As Integer, coaId As String)
    Sub InsertBSAccDetail(ByVal strCnn As String, ByVal data As BalanceSheetAccDetail)
    Sub UpdateBSAccDetail(ByVal strCnn As String, ByVal data As BalanceSheetAccDetail)
    Function FindByIdBSAccDetail(ByVal strCnn As String, ByVal reportGroupId As Integer, coaId As String) As BalanceSheetAccDetail
    Function SelectAllCOA(ByVal strCnn As String) As DataTable
    Function GetAccountCOA(ByVal strConnection As String, ByVal ReportGroupId As String) As DataTable
    Function SelectBSGroupDetail(ByRef totRecord As Integer, strCnn As String, ByVal ParamArray param() As Object) As IList(Of BalanceSheetGroupDetail)
    Sub DeleteBSGroupDetail(ByVal strCnn As String, ByVal reportGroupId As Integer, groupId As Integer)
    Sub InsertBSGroupDetail(ByVal strCnn As String, ByVal data As BalanceSheetGroupDetail)
    Sub UpdateBSGroupDetail(ByVal strCnn As String, ByVal data As BalanceSheetGroupDetail)
    Function FindByIdBSGroupDetail(ByVal strCnn As String, ByVal reportGroupId As Integer, groupId As Integer) As BalanceSheetGroupDetail
    Function SelectAllBSGroupDetail(ByVal strCnn As String, ByVal RID As String) As DataTable


End Interface
