﻿Imports Maxiloan.Parameter

Public Interface IGlYearPeriod


    Function GenerateNewYearPeriods(cnn As String, year As GlPeriodYear) As String

    Function YearPeriods(cnn As String, companyid As String, branchid As String, year As Integer, month As Integer) As IList(Of GlPeriod)

    Function SetCurrentGlPeriod(cnn As String, periodId As String) As String

    Function IsYearValid(ByVal cnn As String, year As Integer, companyId As String, branchid As String) As Boolean
    'Taufik
    'Function CurrentYearPeriod(cnn As String, companyid As String, branchid As String) As GlPeriod

    Function YearlyCloseValidate(ByVal cnn As String, companyId As String) As String


End Interface
