﻿Imports Maxiloan.Parameter
Public Interface IGLPeriod
    Function GetGLPeriod(ByVal oCustomClass As Parameter.GlPeriod) As Parameter.GlPeriod
    Function GetGLPeriodList(ByVal oCustomClass As Parameter.GlPeriod) As Parameter.GlPeriod
    Function GLPeriodSaveAdd(ByVal oCustomClass As Parameter.GlPeriod) As String
    Sub GLPeriodSaveEdit(ByVal oCustomClass As Parameter.GlPeriod)
    Function GLPeriodDelete(ByVal oCustomClass As Parameter.GlPeriod) As String
End Interface
