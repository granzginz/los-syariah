﻿Imports Maxiloan.Parameter

Public Interface IDataJurnalCrossPeriode
    Function GetDataJurnal(ByVal oCustomClass As Parameter.DataJurnalCrossPeriode) As Parameter.DataJurnalCrossPeriode
    Function GetTr_Nomor(ByVal oCustomClass As Parameter.DataJurnalCrossPeriode) As Parameter.DataJurnalCrossPeriode
End Interface
