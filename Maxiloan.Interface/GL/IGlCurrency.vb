﻿Imports Maxiloan.Parameter

Public Interface IGlCurrency

    Function GetGlCurrencies(cnn As String) As IList(Of GlCurrency)
    Function GetHomeCurrency(ByVal cnn As String) As GlCurrency
    Function GetDefaultCurrency(ByVal cnn As String) As GlCurrency
End Interface
