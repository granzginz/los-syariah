﻿Imports Maxiloan.Parameter


Public Interface IJournalVoucher
    Inherits IGlYearPeriod


    Function GetGlMasterSequence(strCnn As String, ByVal ParamArray param() As Object) As IList(Of ValueTextObject)

    Function DownloadToDBF(ByVal cnn As String, period1 As DateTime, period2 As DateTime, branch As String) As DataTable

    Function GetCompanyId(cnn As String) As String

    Function GetJurnalVoucherByTrNo(ByVal cnn As String, transNo As String) As GlJournalVoucher
    Function GlJournalAddUpdate(ByVal cnn As String, isAdd As Boolean, ByVal component As IList(Of GlJournalVoucher)) As String
    Function GlJournalAddDelete(ByVal cnn As String, isAdd As Boolean, ByVal component As IList(Of GlJournalVoucher)) As String
    Function GlJournalHold(ByVal cnn As String, ByVal component As IList(Of GlJournalVoucher)) As String

    Sub ValidateCsvComponen(cnn As String, component As IList(Of GlJournalVoucher))
    Function DoUploadCsvGlJournals(cnn As String, component As IList(Of GlJournalVoucher)) As String
    Function IsJournals(cnn As String, trDate As DateTime) As Boolean

    Function GetGlJournalAvailablePost(ByRef totalrec As Integer, ByVal strCnn As String, ByVal ParamArray param As Object()) As GlJournalPostAvailable 'IList(Of GlJournalPostAvailable)
    Function DoDailyJournalPost(cnn As String, ByVal ParamArray param As Object()) As String

    Function SelectInitialBudget(ByRef totRecord As Integer, strCnn As String, ByVal ParamArray param() As Object) As IList(Of InitialBudget)
    Function GetGlInitialFindOne(ByVal strCnn As String, ByVal param As InitialBudget) As InitialBudget
    Function GlInitialBudgetUpdate(ByVal cnn As String, ByVal component As IList(Of InitialBudget)) As String
    Function GlInitialBudgetAdd(ByVal cnn As String, ByVal component As IList(Of InitialBudget)) As String

    Function GlJournalApproval(cnn As String, param As JournalApproval, Optional approve As Boolean = False) As JournalApproval
    Function GlJournalApproval(ByVal cnn As String, ByVal component As IList(Of String)) As String
    Function GlJournalVerify(ByVal cnn As String, ByVal component As IList(Of String)) As String

    'Function GlGelMasUpload(ByVal cnn As String, ByVal component As GelMas) As String
    'Function GlGelMasUploadValidate(ByVal cnn As String, ByVal component As GelMas) As String

    Function GLGelMASValidateCOASelect(ByRef totRecord As Integer, strCnn As String, ByVal ParamArray param() As Object) As IList(Of ValueTextObject)


    'Function GlGelMasVirifyUpload(ByVal cnn As String, ByVal component As GelMas) As String
    Function GLHistoryCOASelect(ByRef totalrec As Integer, ByVal strCnn As String, ByVal ParamArray param() As Object) As DataView
    Function GlJournalApprovalPage(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64) As IList(Of JournalApproval)

    Function GlJournalPost(ByVal cnn As String, ByVal component As IList(Of String)) As String
    Function GetGlJournalAvailablePostPage(ByVal cnn As String,
                                                 ByVal companyId As String,
                                                 ByVal branchId As String,
                                                 ByVal trDateFrom As DateTime,
                                                 ByVal trDateTo As DateTime,
                                                 pageSz As Integer, currPg As Integer, ByRef totalReq As Int64) As IList(Of JournalApproval)


    Function GlJournalVerifyPage(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64, ByVal wOpt As Integer, ByVal cmdwhere As String) As IList(Of JournalApproval)

    Function DoJournalMonthlyClose(ByVal cnn As String, ByVal ParamArray param As Object()) As String
    Function DoJournalYearlyClose(ByVal cnn As String, ByVal companyId As String, ByVal userId As String) As String

    Function GetHifrontDetails(cnn As String, dateProc As Date) As System.Text.StringBuilder
    Function GetJournalTransaction(oCustom As Parameter.JournalTransaction) As Parameter.JournalTransaction
    Function GlJournalUnPostPage(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64) As IList(Of JournalApproval)
    Function GlJournalUnPostPageFilter(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64, ByVal wOpt As Integer, ByVal cmdwhere As String) As IList(Of JournalApproval)
    Function GlJournalUnPost(ByVal cnn As String, ByVal component As IList(Of String)) As String
    Function GlJournalPageNotSuccess(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64) As IList(Of JournalApproval)
End Interface
