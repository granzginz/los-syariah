Imports Maxiloan.Parameter


Public Interface ICOA
    Inherits IGlCurrency
    Function SelectData(ByRef totRecord As Integer, strCnn As String, ByVal fromLookup As Boolean, isLeaf As Boolean, ByVal ParamArray param() As Object) As IList(Of MasterAccountObject)
    Function SelectDataIsLeaf(ByRef totRecord As Integer, strCnn As String, ByVal fromLookup As Boolean, isLeaf As Boolean, ByVal ParamArray param() As Object) As IList(Of MasterAccountObject)
    Sub DeleteData(ByVal strCnn As String, ByVal coaId As String, ByVal AddFree As String)
    Sub InsertData(ByVal strCnn As String, ByVal data As MasterAccountObject)
    Sub UpdateData(ByVal strCnn As String, ByVal data As MasterAccountObject)
    Function FindById(ByVal strCnn As String, ByVal coaId As String, ByVal branchId As String, ByVal companyId As String) As MasterAccountObject
    Function FindByDesc(ByVal strCnn As String, ByVal Description As String, ByVal branchId As String, ByVal companyId As String) As MasterAccountObject
    Function AutoComplateCoaId(strCnn As String, coaid As String) As List(Of String)
    Function AutoComplateCoaDesc(strCnn As String, Description As String) As List(Of String)

    'Sub SelectData(ByRef data As MasterAcc) 
    'Function SelectData2(ByVal data As MasterAcc) As DataTable  


End Interface
