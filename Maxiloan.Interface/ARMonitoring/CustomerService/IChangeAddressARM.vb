

Public Interface IChangeAddressARM
    Function AddressListCG(ByVal customclass As Parameter.ChangeAddressARM) As Parameter.ChangeAddressARM
    Function ChangeAddressList(ByVal customclass As Parameter.ChangeAddressARM) As Parameter.ChangeAddressARM
    Function ChangeAddressView(ByVal customclass As Parameter.ChangeAddressARM) As Parameter.ChangeAddressARM
    Function ChangeAddressSave(ByVal customclass As Parameter.ChangeAddressARM) As Parameter.ChangeAddressARM
    Function GetAddress(ByVal customclass As Parameter.ChangeAddressARM) As Parameter.ChangeAddressARM
    Function ViewReportAR(ByVal oCustomClass As Parameter.ChangeAddressARM) As Parameter.ChangeAddressARM
    Function getAROverdue(ByVal oCustomClass As Parameter.ChangeAddressARM) As Parameter.ChangeAddressARM
End Interface
