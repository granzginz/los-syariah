
Public Interface IFundingCriteria
    Function GetFundingCriteria(ByVal oCustomClass As Parameter.FundingCriteria) As Parameter.FundingCriteria    
    Function GetFundingCriteriaList(ByVal oCustomClass As Parameter.FundingCriteria) As Parameter.FundingCriteria
    Function FundingCriteriaSaveAdd(ByVal oCustomClass As Parameter.FundingCriteria) As String
    Sub FundingCriteriaSaveEdit(ByVal oCustomClass As Parameter.FundingCriteria)
    Function FundingCriteriaDelete(ByVal oCustomClass As Parameter.FundingCriteria) As String
    Sub ExecQuery(ByVal oCustomClass As Parameter.FundingCriteria)
    Sub SaveReq(ByVal oCustomClass As Parameter.FundingCriteria)
    Function GetSQLList(ByVal oCustomClass As Parameter.FundingCriteria) As Parameter.FundingCriteria
End Interface
