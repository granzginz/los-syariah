
Public Interface IFundingContractCriteria
    Function GetFundingContractCriteriaListColl(ByVal oCustomClass As Parameter.FundingContractCriteria) As List(Of Parameter.FundingContractCriteria)
    Function GetFundingContractCriteriaList(ByVal oCustomClass As Parameter.FundingContractCriteria) As Parameter.FundingContractCriteria
    Function FundingContractCriteriaSaveAdd(ByVal oCustomClass As Parameter.FundingContractCriteria) As String
    Sub FundingContractCriteriaSaveEdit(ByVal oCustomClass As Parameter.FundingContractCriteria)
    Function FundingContractCriteriaDelete(ByVal oCustomClass As Parameter.FundingContractCriteria) As String
    Function GetCombo(ByVal customclass As Parameter.FundingContractCriteria) As DataTable
End Interface
