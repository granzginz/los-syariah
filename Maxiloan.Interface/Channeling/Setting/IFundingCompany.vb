

Public Interface IFundingCompany
    Function FundingCompanyList(ByVal co As Parameter.FundingCompany) As Parameter.FundingCompany
    Function FundingCompanyAdd(ByVal customClass As Parameter.FundingCompany, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As String
    Function ListSPCReport(ByVal customclass As Parameter.FundingCompany) As DataSet
    Function FundingCompanyListByID(ByVal co As Parameter.FundingCompany) As Parameter.FundingCompany
    Sub FundingCompanyEdit(ByVal customClass As Parameter.FundingCompany, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
    Sub FundingCompanyDelete(ByVal customclass As Parameter.FundingCompany)
    Function FundingCompanyReport(ByVal co As Parameter.FundingCompany) As Parameter.FundingCompany

    Function FundingContractPlafondBranchAdd(ByVal customClass As Parameter.FundingContractBranch) As String
    Sub FundingContractPlafondBranchEdit(ByVal customClass As Parameter.FundingContractBranch)
    Sub FundingContractPlafondBranchDelete(ByVal customclass As Parameter.FundingContractBranch)
    Function FundingContractPlafondBranchByID(ByVal co As Parameter.FundingContractBranch) As Parameter.FundingContractBranch

    Function FundingContractAdd(ByVal customClass As Parameter.FundingContract) As String
    Sub FundingContractEdit(ByVal customClass As Parameter.FundingContract)
    Sub FundingContractDelete(ByVal customclass As Parameter.FundingContract)
    Function FundingContractByID(ByVal co As Parameter.FundingContract) As Parameter.FundingContract

    Function FundingContractDocAdd(ByVal customClass As Parameter.FundingContract) As String
    Sub FundingContractDocDelete(ByVal customclass As Parameter.FundingContract)

    Function FundingContractBatchAdd(ByVal customClass As Parameter.FundingContractBatch) As String
    Function FundingContractBatchInsAdd(ByVal customClass As Parameter.FundingContractBatch) As String
    Sub FundingContractBatchEdit(ByVal customClass As Parameter.FundingContractBatch)

    Function GetComboInputBank(ByVal strcon As String) As DataTable

    Sub FundingUpdateAgreementSelected(ByVal customClass As Parameter.FundingContractBatch)
    Sub FundingUpdateAgreementExecution(ByVal customClass As Parameter.FundingContractBatch)
    Sub FundingDrowDownReceive(ByVal CustomClass As Parameter.FundingContractBatch)
    Function GetGeneralEditView(ByVal oCustomClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
    Sub PaymentOutInstallmentProcess(ByVal CustomClass As Parameter.FundingContractBatch)
    Sub PrepaymentBPKBReplacing(ByVal CustomClass As Parameter.FundingContractBatch)
    Sub PrepaymentPaymentOut(ByVal CustomClass As Parameter.FundingContractBatch)
    Sub PaymentOutFees(ByVal CustomClass As Parameter.FundingContractBatch)
    Sub AddFundingAgreementInstallment(ByVal Company As Parameter.FundingContractBatch)
    Sub AddFundingAgreementInstallmentDraft(ByVal Company As Parameter.FundingContractBatch)
    Sub FundingRescheduleAgreementInstallment(ByVal Company As Parameter.FundingContractBatch)
    Sub FundingReschedulePrepaymentAgreementInstallment(ByVal Company As Parameter.FundingContractBatch)
    Sub FundingUpdateContractBatchInst(ByVal Company As Parameter.FundingContractBatch)
    Function GetPrepaymentEditView(ByVal oCustomClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
    Sub FundingUpdateAgreementSecondExecution(ByVal CustomClass As Parameter.FundingContractBatch)
    Sub FundingAgreementSelect(ByVal CustomClass As Parameter.FundingContractBatch)
    Sub FundingAgreementSelectFromUpload(ByVal CustomClass As Parameter.FundingContractBatch)
    Sub FundingAgreementUploadUpdateExcel(ByVal customClass As Parameter.FundingContractBatch)
    Sub DueDateChangeSave(ByVal oCustomClass As Parameter.FundingContractBatch)
    Function FundingContractNegCovGet(ByVal CustomClass As Parameter.FundingContract) As Parameter.FundingContract
    Sub FundingContractNegCovUpdate(ByVal strcon As String, ByVal setField As String, ByVal WhereBy As String)

    Sub FundingContractBatchInsAdd2(ByVal customClass As Parameter.FundingContractBatch)
    Function FundingDraftSoftcopy(ByVal customClass As Parameter.FundingContractBatch) As DataSet
    Function FundingContractBatchInsAddKMK(ByVal customClass As Parameter.FundingContractBatch) As String
    Sub AddFundingAgreementInstallmentKMK(ByVal Company As Parameter.FundingContractBatch)
    Sub genFAIEfektif(ByVal customClass As Parameter.FundingContractBatch)

    Function FundingAgreementSelection(ByVal customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
    Function FundingAgreementJnFnc(ByVal customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
    Function FundingAgreementList(ByVal customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch

    Sub FundingContractRateEdit(ByVal oClass As Parameter.FundingContractRate)
    Sub FundingDrowDownReceiveCheckPeriod(ByVal CustomClass As Parameter.FundingContractBatch)
    Sub FundingDrowDownReceiveCheckScheme(ByVal CustomClass As Parameter.FundingContractBatch)
    Sub FundingDrowDownReceiveDueDateRangeAdd(ByVal CustomClass As Parameter.FundingContractBatch)

    Function FundingTotalSelected(ByVal customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
    Sub PrepaymentBatchProcess(ByVal CustomClass As Parameter.FundingContractBatch)

    Function FundingAgreementInstallmentView(customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch

    Function FundPrintSave(ByVal oCustomClass As Parameter.FundingCompany) As String
    Function ListPrint(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
    Function SavePrint(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
    Function ListReportPrepay(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch

    Function GetFundingInquiryAgreementPledgeWillFinish(ByVal oCustomClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
    Function GetComboInputBankBeban(ByVal strcon As String) As DataTable
    Function GetComboInputBankAdministrasi(ByVal strcon As String) As DataTable

    Function ListPrintPaymentOut(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
    Function SavePrintPaymentOut(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
    Function ListReportPaymentOut(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch

    Sub FundingBatchInstallmentSelectFromUpload(ByVal CustomClass As Parameter.FundingContractBatch)

    Function FundingBatchRate(ByVal oClass As Parameter.FundingContractRate) As Parameter.FundingContractRate
    Sub UpdateFundingAgreementSelect(ByVal CustomClass As Parameter.FundingContractBatch)
End Interface