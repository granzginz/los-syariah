
Public Interface IFundingCriteriaValue
    Function GetFundingCriteriaValue(ByVal oCustomClass As Parameter.FundingCriteriaValue) As Parameter.FundingCriteriaValue    
    Function GetFundingCriteriaValueList(ByVal oCustomClass As Parameter.FundingCriteriaValue) As Parameter.FundingCriteriaValue
    Function FundingCriteriaValueSaveAdd(ByVal oCustomClass As Parameter.FundingCriteriaValue) As String
    Sub FundingCriteriaValueSaveEdit(ByVal oCustomClass As Parameter.FundingCriteriaValue)
    Function FundingCriteriaValueDelete(ByVal oCustomClass As Parameter.FundingCriteriaValue) As String
End Interface
