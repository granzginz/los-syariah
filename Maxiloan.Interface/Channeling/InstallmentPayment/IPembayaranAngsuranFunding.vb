Public Interface IPembayaranAngsuranFunding

    Function getListPembayaranAngsuran(ByVal CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
    Sub DisbursePembayaranAngsuran(ByVal CustomClass As Parameter.DisbursePembayaranAngsuranFunding)
    Function getListBonHijauFundingDisburse(ByVal CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
    Function GetListData(ByVal strConnection As String, ByVal cmdWhere As String, ByVal currentPage As Integer, ByVal pagesize As Integer, ByVal SortBy As String) As Parameter.PembayaranAngsuranFunding
    Function getFundingAngsJthTempoSummary(ByVal customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
    Sub disburseFundingAngsuranJthTempo(ByVal CustomClass As Parameter.DisbursePembayaranAngsuranFunding)
    Function FundingPrepaymentReport(ByVal CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
    Function BPKBLoanReport(ByVal CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
    Function BPKBFotoCopyRequestReport(ByVal CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
    Function BPKBPickupReport(oCustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding

    Function TAFPrepaymentReport(oCustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding

    Function RequestCalculationFundingPrepaymentReport(oCustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
    Function getFundingpaymentAdvance(ByVal customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
    Function disburseFundingpaymentAdvance(ByVal customClass As Parameter.DisbursePembayaranAngsuranFunding) As Parameter.DisbursePembayaranAngsuranFunding
    Function BayarBatchJthTempoList(ByVal customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
    Function disburseBatchPerjtTempo(ByVal customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
    Function RemoveSelectionAgreementBatch(ByVal customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
    Function AppendSelectionAgreementBatch(ByVal oCustom As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
    Function InstallmentScheduleTL(customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
End Interface
