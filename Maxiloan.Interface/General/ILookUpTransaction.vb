
Public Interface ILookUpTransaction
    Function GetListData(ByVal strConnection As String, ByVal cmdWhere As String, ByVal currentPage As Integer, ByVal pagesize As Integer, ByVal SortBy As String) As Parameter.LookUpTransaction
    Function GetProcessID(ByVal strConnection As String, ByVal TransactionID As String) As String
End Interface
