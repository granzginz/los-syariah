

Public Interface ILookUpSupplier
    Function GetListData(ByVal customClass As Parameter.LookUpSupplier) As Parameter.LookUpSupplier
    Function GetListDataMulti(ByVal customClass As Parameter.LookUpSupplier) As Parameter.LookUpSupplier
    Function GetListDataMulti_Confirm(ByVal customClass As Parameter.LookUpSupplier) As Parameter.LookUpSupplier
    Function GetListDataSupplierBranch(ByVal customClass As Parameter.LookUpSupplier) As Parameter.LookUpSupplier
End Interface
