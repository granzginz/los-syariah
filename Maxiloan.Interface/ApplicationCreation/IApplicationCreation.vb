
Imports Maxiloan.Parameter

Public Interface IApplicationCreation


    Function GetProspect(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
    Function ProspectSaveAdd(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
    Function ProspectSaveAsset(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
    Function ProspectSaveDemografi(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
    Function ProspectSaveFinancial(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
    Function GetViewProspect(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
    Function GetCbo(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
    Function GetCboGeneral(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
    Function DoProceeded(ByVal cnn As String, prospectId As String) As Boolean
    Function DoProspectCreaditScoring(cnn As String, branchId As String, prospectAppId As String, loginId As String, bnsDate As Date) As String
    Function DispositionCreditRpt(cnn As String, prospectAppIds As String(), tp As String) As DataSet
    Function GetProspectScorePolicyResult(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
    Function DoProspectDecision(cnn As String, prospectAppId As String, appr As Integer)
    Function MasterDataReviewRpt(cnn As String, prospectAppIds As String(), tp As String) As DataSet
    Function GetInitial(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
    Function InitialSaveAdd(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
    Function DoBIChecking(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
    Function DataSurveySaveAdd(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
    Function DataSurveySaveEdit(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
    Function ProspectReturnUpdate(ByVal oCustomClass As Parameter.AppCreation) As String
    Function ProspectLogSave(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
    Function GetInqProspect(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
    Function GetApprovalprospect(ByVal oCustomClass As Parameter.AppCreation, ByVal strApproval As String)

    Function DoProspectCreaditScoringProceed(cnn As String, branchId As String, prospectAppId As String, loginId As String, bnsDate As Date, proceed As Boolean) As String

End Interface
