

Public Interface IFiducia

    Function GetNotary(ByVal customclass As Parameter.Fiducia) As Parameter.Fiducia
    Function GetNotaryCharge(ByVal customclass As Parameter.Fiducia) As Parameter.Fiducia
    Sub FiduciaAdd(ByVal customclass As Parameter.Fiducia)
    Sub GenerateFiduciaRoyaRegisterNo(ByVal customclass As Parameter.Fiducia)
    Sub FiduciaAktaReceive(ByVal customclass As Parameter.Fiducia)
    Sub NotaryChargeAdd(ByVal customclass As Parameter.Fiducia)
    Sub AddNotary(ByVal customclass As Parameter.Fiducia)
    Sub FiduciaFarsial(ByVal customclass As Parameter.Fiducia)

End Interface
