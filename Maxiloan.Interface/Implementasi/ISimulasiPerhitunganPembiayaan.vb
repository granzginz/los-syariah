﻿Public Interface ISimulasiPerhitunganPembiayaan
    Function SimulasiPerhitunganPembiayaanPaging(ByVal oCustomClass As Parameter.SimulasiPerhitunganPembiayaan) As Parameter.SimulasiPerhitunganPembiayaan
    Function GenerateSimulasiPerhitunganPembiayaan(ByVal oCustomClass As Parameter.SimulasiPerhitunganPembiayaan) As Parameter.SimulasiPerhitunganPembiayaan
    Function CetakKartuPiutangList(ByVal oCustomClass As Parameter.CetakKartuPiutang) As Parameter.CetakKartuPiutang
End Interface
