

Public Interface IImplementasi
	Function GetSP(ByVal customclass As Parameter.Implementasi) As Parameter.Implementasi
	Sub UpdateOutstandingPokok(ByVal oCustomClass As Parameter.Implementasi)
	Function GetGeneralPaging(ByVal CustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
	Sub UpdateBiayaTarik(ByVal oCustomClass As Parameter.Implementasi)
	Sub UpdateNDT(ByVal oCustomClass As Parameter.Implementasi)
	Sub SavePembayaranTDP(ByVal oCustomClass As Parameter.Implementasi)
	'Tambahan Taufik 26 Feb 2016
	'Function AreaList(ByVal strConnection As String, ByVal mWhere As String) As DataTable
	'Function DropDownListCOA(ByVal customClass As Parameter.Implementasi) As Parameter.Implementasi
	'Function Branch2List(ByVal strConnection As String, ByVal mWhere As String) As DataTable
	'Function GetBankByBranch(ByVal strConnection As String, ByVal cabang As String) As DataTable
	'Function GetBankAccountBranch(ByVal strConnection As String, ByVal cabang As String) As DataTable
	'Function GetBankAccountBranchAll(ByVal strConnection As String, ByVal cabang As String) As DataTable
	'Function GetKasir(ByVal strConnection As String, ByVal cabang As String) As DataTable
	'Function GetCollector(ByVal strConnection As String, ByVal cabang As String) As DataTable
	'Function GetInsuranceByBranch(ByVal strConnection As String, ByVal cabang As String) As DataTable
	'Function GetInsurance(ByVal strConnection As String, ByVal cabang As String) As DataTable
	'Function GetCmoByBranch(ByVal strConnection As String, ByVal cabang As String) As DataTable
	'Function GetCoaBranch(ByVal strConnection As String, ByVal cabang As String) As DataTable
	'Function GetCollectorByBranch(ByVal strConnection As String, ByVal cabang As String) As DataTable
	'Function GetHeadCollector(ByVal strConnection As String, ByVal cabang As String) As DataTable
	'Function GetEmployeePosition(ByVal strConnection As String, branchId As String) As String
	'Function GetBankAccountHOTransfer(ByVal strConnection As String, ByVal BankID As String) As DataTable
	'Function DropDownListDivisi(ByVal customClass As Parameter.Implementasi) As Parameter.Implementasi
	'Function GetAsset(ByVal CustomClass As Parameter.Implementasi) As Parameter.Implementasi
	'Function GetChartOfAccount(ByVal CustomClass As Parameter.Implementasi) As Parameter.Implementasi
	Function GetBankAccountCabang(ByVal ocustomClass As Parameter.Implementasi) As Parameter.Implementasi
	'Function GetCoaPaging(ByVal CustomClass As Parameter.Implementasi) As Parameter.Implementasi
	'Function GetGroupCoaPaging(ByVal CustomClass As Parameter.Implementasi) As Parameter.Implementasi
	Function GetPeriodJournal(ByVal customClass As Parameter.Implementasi) As Parameter.Implementasi
	'VIRTUAL ACCOUNT
	Function GetVirtualAccount(strConn As String) As DataSet
	Function GenerateVitualAccount(ByVal cnn As String, vabankid As IList(Of String), ByVal oDataTable As DataTable) As IList(Of Parameter.GeneratedVaToExcellLog)
	Function GetVirtualAccountInsAgreementLog(cnn As String, processid As String) As Parameter.VirtualAccountInsAgreementLog
	Function GeneratePaymentGatewayFile(cnn As String, uploadId As String, dir As String, UploadDate As Date) As Boolean
	Function VaGenerateSettingLog(cnn As String, processDate() As DateTime) As String()

	Function GetBankAccountFunding(ByVal strConnection As String) As DataTable
	Function GetFundingCoyID(ByVal strConnection As String, FundingCoyID As String) As DataTable
	Function GetFundingBatch(ByVal strConnection As String, FundingContractNo As String) As DataTable
	Function GetBankAccountHOTransfer(ByVal strConnection As String, ByVal BankID As String) As DataTable

	Sub SaveTerimaTDP(ByVal customclass As Parameter.TerimaTDP)
	Sub SaveOtorisasiTDP(ByVal oEntities As Parameter.TerimaTDP)
	Function TDPReversalList(ByVal oCustomClass As Parameter.TerimaTDP) As Parameter.TerimaTDP
	Function ReverseTDP(ByVal oCustomClass As Parameter.TerimaTDP) As Parameter.TerimaTDP
    Sub SaveTDPReverse(ByVal customclass As Parameter.TerimaTDP)
    Sub TDPSplit(ByVal oCustomclass As Parameter.TerimaTDP)

    Function PaymentRequestFixedAssetList(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
	Function PaymentRequestFixedAssetListOtor(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
	Function PaymentRequestFixedAssetSave(ByVal oCustomClass As Parameter.TransFixedAsset)
	Function PaymentRequestFixedAssetOtorSave(ByVal oCustomClass As Parameter.TransFixedAsset)

	Function RequestReceiveFixedAssetList(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
	Function RequestReceiveFixedAssetListOtor(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
	Function RequestReceiveFixedAssetSave(ByVal oCustomClass As Parameter.TransFixedAsset)
    Function RequestReceiveFixedAssetOtorSave(ByVal oCustomClass As Parameter.TransFixedAsset)

    Function PenjualanFixedAssetList(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
    Function PenjualanFixedAssetListOtor(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
    Function PenjualanFixedAssetSave(ByVal oCustomClass As Parameter.TransFixedAsset)
    Function PenjualanFixedAssetOtorSave(ByVal oCustomClass As Parameter.TransFixedAsset)

    Function PenghapusanFixedAssetList(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
    Function PenghapusanFixedAssetListOtor(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
    Function PenghapusanFixedAssetSave(ByVal oCustomClass As Parameter.TransFixedAsset)
    Function PenghapusanFixedAssetOtorSave(ByVal oCustomClass As Parameter.TransFixedAsset)

    Function PaymentRequestAgreementUnitExpenseList(ByVal oCustomClass As Parameter.AgreementUnitExpense) As Parameter.AgreementUnitExpense
    'Function CetakKartuPiutangPaging(ByVal oCustomClass As Parameter.CetakKartuPiutang) As Parameter.CetakKartuPiutang

End Interface