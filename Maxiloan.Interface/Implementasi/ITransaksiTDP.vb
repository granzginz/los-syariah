

Public Interface ITransaksiTDP

    Sub SaveTerimaTDP(ByVal oCustomClass As Parameter.TerimaTDP)
    Sub SaveOtorisasiTDP(ByVal oEntities As Parameter.TerimaTDP)
    Function TDPReversalList(ByVal oCustomClass As Parameter.TerimaTDP) As Parameter.TerimaTDP
    Function ReverseTDP(ByVal oCustomClass As Parameter.TerimaTDP) As Parameter.TerimaTDP
End Interface
