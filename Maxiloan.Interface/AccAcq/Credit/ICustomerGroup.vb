
Public Interface ICustomerGroup
    Function GetCustomerGroup(ByVal oCustomClass As Parameter.CustomerGroup) As Parameter.CustomerGroup
    Function GetCustomerGroupReport(ByVal oCustomClass As Parameter.CustomerGroup) As Parameter.CustomerGroup
    Function GetCustomerGroupListCustomer(ByVal oCustomClass As Parameter.CustomerGroup) As Parameter.CustomerGroup
    Function GetCustomerGroupList(ByVal oCustomClass As Parameter.CustomerGroup) As Parameter.CustomerGroup
    Function CustomerGroupSaveAdd(ByVal oCustomClass As Parameter.CustomerGroup) As String
    Sub CustomerGroupSaveEdit(ByVal oCustomClass As Parameter.CustomerGroup)
    Function CustomerGroupDelete(ByVal oCustomClass As Parameter.CustomerGroup) As String
End Interface
