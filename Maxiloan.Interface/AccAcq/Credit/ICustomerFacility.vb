
Public Interface ICustomerFacility


    Function GetFacilityList(ByVal oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility
    Function GetFacilityDetail(ByVal oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility
    Function GetAddendumFacilityDetail(ByVal oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility
    Function CustomerFacilitySaveAdd(ByVal oCustomer As Parameter.CustomerFacility) As Parameter.CustomerFacility
    Function CustomerFacilityApprovalList(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility
    Function GetCboUserAprPCAll(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility
    Function GetCboUserApproval(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility
    Function CustFacApproveSaveToNextPerson(ByVal customclass As Parameter.CustomerFacility) As String
    Function CustFacApproveSave(ByVal customclass As Parameter.CustomerFacility) As String
    Function CustFacApproveisFinal(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility
    Function CustFacApproveIsValidApproval(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility
    Function AddendumCustomerFacilitySaveAdd(ByVal oCustomer As Parameter.CustomerFacility) As Parameter.CustomerFacility
    Function AddendumCustomerFacilityApprovalList(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility
    Function AddendumCustFacApproveSave(ByVal customclass As Parameter.CustomerFacility) As String
    Function AddendumCustFacApproveSaveToNextPerson(ByVal customclass As Parameter.CustomerFacility) As String
End Interface
