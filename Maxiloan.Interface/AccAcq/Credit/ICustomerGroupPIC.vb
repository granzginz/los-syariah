
Public Interface ICustomerGroupPIC
    Function GetCustomerGroupPIC(ByVal oCustomClass As Parameter.CustomerGroupPIC) As Parameter.CustomerGroupPIC
    Function GetCustomerGroupPICReport(ByVal oCustomClass As Parameter.CustomerGroupPIC) As Parameter.CustomerGroupPIC
    Function GetCustomerGroupPICListByCustGroupID(ByVal oCustomClass As Parameter.CustomerGroupPIC) As Parameter.CustomerGroupPIC
    Function GetCustomerGroupPICList(ByVal oCustomClass As Parameter.CustomerGroupPIC) As Parameter.CustomerGroupPIC
    Function CustomerGroupPICSaveAdd(ByVal oCustomClass As Parameter.CustomerGroupPIC) As String
    Sub CustomerGroupPICSaveEdit(ByVal oCustomClass As Parameter.CustomerGroupPIC)
    Function CustomerGroupPICDelete(ByVal oCustomClass As Parameter.CustomerGroupPIC) As String
End Interface
