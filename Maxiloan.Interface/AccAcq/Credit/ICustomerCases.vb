
Public Interface ICustomerCases
    Function GetCustomerCases(ByVal oCustomClass As Parameter.CustomerCases) As Parameter.CustomerCases    
    Function GetCustomerCasesList(ByVal oCustomClass As Parameter.CustomerCases) As Parameter.CustomerCases
    Function CustomerCasesSaveAdd(ByVal oCustomClass As Parameter.CustomerCases) As String
    Sub CustomerCasesSaveEdit(ByVal oCustomClass As Parameter.CustomerCases)
    Function CustomerCasesDelete(ByVal oCustomClass As Parameter.CustomerCases) As String
    Function GetComboCases(ByVal customclass As Parameter.CustomerCases) As DataTable
End Interface
