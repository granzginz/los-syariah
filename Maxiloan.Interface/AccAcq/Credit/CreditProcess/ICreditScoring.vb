
Public Interface ICreditScoring
    Function CreditScoringPaging(ByVal oCustomClass As Parameter.CreditScoring) As Parameter.CreditScoring
    Function CreditScoringSaveAdd(ByVal oCustomClass As Parameter.CreditScoring) As String
    Function CreditScoringSaveAdd(ByVal oCustomClass As Parameter.CreditScoring, ByVal isSavingGrid As Boolean) As String
    Function DoCreaditScoringSave(cnn As String, loginId As String, branchid As String, AppId As String, scoreResult As Parameter.ScoreResults, bnsDate As Date) As String
    Function GetCreditScorePolicyResult(ByVal oCustomClass As Parameter.CreditScoring) As Parameter.CreditScoring
    Function CrDispositionCreditRpt(cnn As String, whereCond As String(), tp As String) As DataSet
    Function DoCreditScoringProceed(cnn As String, branchId As String, ApplicationID As String, loginId As String, bnsDate As Date) As String
    Function GetApprovalCreditScoring(ByVal oCustomClass As Parameter.CreditScoring, ByVal strApproval As String)
End Interface
