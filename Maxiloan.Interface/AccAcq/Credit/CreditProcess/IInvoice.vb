
Public Interface IInvoice
    Function InvoicePaging(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice
    Function GetInvoiceATPMList(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice
    Function ListRecATPM(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice
    Function GetInvoiceAgreementList(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice
    Function GetInvoiceATPMAgreementList(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice
    Function GetInvoiceMaxBackDate(ByVal ocustomClass As Parameter.Invoice) As Integer
    Function GetInvoiceSuppTopDays(ByVal ocustomClass As Parameter.Invoice) As Integer
    Function InvoiceSave(ByVal oCustomClass As Parameter.Invoice) As String
    Function InvoiceATPMSave(ByVal oCustomClass As Parameter.Invoice) As String
    Function LoadCombo(cnn As String, ty As String, Optional id As String = "") As IList(Of Parameter.CommonValueText)
    Function InvoiceSuppSave(cnn As String, ByVal invoiceSupp As Parameter.InvoiceSupp) As String
    Function GetNoInvoiceATPM(ByVal customClass As Parameter.Invoice) As Parameter.Invoice
    Function ReceiveATPMList(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice
    Function GetInvoiceNo(ByVal customclass As Parameter.Invoice) As Parameter.Invoice
    Sub SaveATPMOtorisasi(ByVal customclass As Parameter.Invoice)
    Sub RejectOtorisasi(ByVal oCustomClass As Parameter.Invoice)
    Sub EditOtorisasi(ByVal customclass As Parameter.Invoice)

End Interface
