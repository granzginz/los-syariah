﻿Public Interface IHasilSurvey
    Function getHasilSurvey(ByVal oCustomClass As Parameter.HasilSurvey) As Parameter.HasilSurvey
    Sub HasilSurveySave(ByVal oCustomClass As Parameter.HasilSurvey)
    Function GetApplicationReject(ByVal oCustomClass As Parameter.HasilSurvey) As Parameter.HasilSurvey
    Sub ApplicationRejectUpdate(ByVal oCustomClass As Parameter.HasilSurvey)
    Sub HasilSurveyJournalSave(ByVal oCustomClass As Parameter.HasilSurvey)
    Sub Proceed(ByVal oCustomClass As Parameter.HasilSurvey)
    Sub HasilSurvey002Save(ByVal oCustomClass As Parameter.HasilSurvey)
    Sub HasilSurvey002CharacterSave(ByVal oCustomClass As Parameter.HasilSurvey)
    Sub HasilSurvey002CapacitySave(ByVal oCustomClass As Parameter.HasilSurvey)
    Sub HasilSurvey002ConditionSave(ByVal oCustomClass As Parameter.HasilSurvey)
    Sub HasilSurvey002CapitalSave(ByVal oCustomClass As Parameter.HasilSurvey)
    Sub HasilSurvey002CollateralSave(ByVal oCustomClass As Parameter.HasilSurvey)
    Sub HasilSurvey002CatatanSave(ByVal oCustomClass As Parameter.HasilSurvey)
    Sub HasilSurvey002KYCSave(ByVal oCustomClass As Parameter.HasilSurvey)
    Sub HasilSurvey002CharacterCLSave(ByVal oCustomClass As Parameter.HasilSurvey)
    Sub HasilSurvey002AplStepSave(ByVal oCustomClass As Parameter.HasilSurvey)
    Function getHasilMobileSurvey(ByVal oCustomClass As Parameter.HasilSurvey) As Parameter.HasilSurvey
    Function getHasilScore(ByVal scoringData As Parameter.HasilSurvey) As Parameter.HasilSurvey
End Interface
