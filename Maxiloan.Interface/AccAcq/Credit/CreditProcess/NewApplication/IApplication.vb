
Public Interface IApplication
    Function GetDataProspectType(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetApplicationFactoring(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetApplicationModalKerja(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetCountAppliationId(ByVal oCustomClass As Parameter.Application) As Integer
    Function GetCopyFrom(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetTC(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetTC2(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetShowDataAgreement(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function ApplicationSaveAdd(ByVal oApplication As Parameter.Application,
                                ByVal oRefPersonal As Parameter.Personal,
                                ByVal oRefAddress As Parameter.Address,
                                ByVal oMailingAddress As Parameter.Address,
                                ByVal oData1 As DataTable,
                                ByVal oData2 As DataTable,
                                ByVal oData3 As DataTable,
                                ByVal oGuarantorPersonal As Parameter.Personal,
                                ByVal oGuarantorAddress As Parameter.Address) As Parameter.Application
    Function GetAddress(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetFee(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetViewApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetApplicationMaintenance(ByVal customClass As Parameter.Application) As Parameter.Application
    Function GetCD(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetApplicationEdit(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetApplicationEditPPH(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function ApplicationSaveEdit(ByVal oApplication As Parameter.Application,
                                 ByVal oRefPersonal As Parameter.Personal,
                                 ByVal oRefAddress As Parameter.Address,
                                 ByVal oMailingAddress As Parameter.Address,
                                 ByVal oData1 As DataTable,
                                 ByVal oData2 As DataTable,
                                 ByVal oData3 As DataTable,
                                 ByVal oGuarantorPersonal As Parameter.Personal,
                                 ByVal oGuarantorAddress As Parameter.Address) As Parameter.Application
    Function GetGoLive(ByVal customClass As Parameter.Application) As Parameter.Application
    Function GetGoLiveCancelPaging(ByVal customClass As Parameter.Application) As Parameter.Application
    Function GetGoLiveBackDated(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GoLiveSave(ByVal oGoLive As Parameter.Application) As Parameter.Application
    Function GoLiveCancel(ByVal oGoLive As Parameter.Application) As Parameter.Application
    Function GetShowDataProspect(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetDataAppIDProspect(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetCustomerProspect(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function ViewProspectApplication(ByVal oCustomClass As Parameter.Application) As DataTable
    Function GetViewMKKFasilitas(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function saveApplicationProductOffering(ByVal oApplication As Parameter.Application, ByVal oAssetData As Parameter.AssetData) As Parameter.Application
    Function saveApplicationProductOfferingEdit(ByVal oApplication As Parameter.Application, ByVal oAssetData As Parameter.AssetData) As Parameter.Application
    Function GetSyaratCair(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetSyaratCairPO(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function DuplikasiApplication(ByVal data As Parameter.Application) As String
    Function ApplicationSaveEditNonFinancial(ByVal oApplication As Parameter.Application,
                                 ByVal oRefPersonal As Parameter.Personal,
                                 ByVal oRefAddress As Parameter.Address,
                                 ByVal oMailingAddress As Parameter.Address,
                                 ByVal oData1 As DataTable,
                                 ByVal oData2 As DataTable,
                                 ByVal oData3 As DataTable) As Parameter.Application
    Function GetApplicationHasilSurveydanKYC(ByVal customClass As Parameter.Application) As Parameter.Application
    Function ValidasiCollector(ByVal customClass As Parameter.Application) As Parameter.Application
    Function ModalKerjaApplicationFinancialSave(ByVal ocustomClass As Parameter.Application) As String
    Function GetGoLiveModalKerja(ByVal customClass As Parameter.Application) As Parameter.Application
    Function GoLiveSaveModalKerja(ByVal oGoLive As Parameter.Application) As Parameter.Application

    Function GetGoLiveFactoring(ByVal customClass As Parameter.Application) As Parameter.Application
    Function GoLiveSaveFactoring(ByVal oGoLive As Parameter.Application) As Parameter.Application
    Function ActivityLogSave(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function DisburseLogSave(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function getdataNPP(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function InstallmentDrawDownSave(ByVal oDrawdown As Parameter.Drawdown) As String
    Function InstallmentDrawDownCheckRequest(ByVal odrawdown As Parameter.Drawdown) As Boolean
    Function InstallmentDrawDownList(ByVal odrawdown As Parameter.Drawdown) As Parameter.Drawdown
    Function GetApplicationDeduction(ByVal customClass As Parameter.Application) As Parameter.Application
    Function GetCustomerFacility(ByVal oCustomClass As Parameter.Application) As Integer
    Function WayOfPaymentList(ByVal customclass As Parameter.Application) As Parameter.Application
End Interface
