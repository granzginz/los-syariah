

Public Interface INewAppInsurance

    Function getNewAppInsuranceListing(ByVal CustomClass As Parameter.NewAppInsurance) As Parameter.NewAppInsurance 
    Function getCreditAssesment(ByVal oCustomClass As Parameter.CreditAssesment) As Parameter.CreditAssesment
    Sub CreditAssesmentSave(ByVal oCustomClass As Parameter.CreditAssesment) 
    Function Proceed(ByVal oCustomClass As Parameter.CreditAssesment) As String
    Function getApprovalSchemeID(cnn As String, appId As String) As String
End Interface
