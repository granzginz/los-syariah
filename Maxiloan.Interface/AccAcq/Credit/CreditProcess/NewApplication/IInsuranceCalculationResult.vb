

Public Interface IInsuranceCalculationResult

    Function DisplayResultInsCalculationStep1(ByVal customClass As Parameter.InsuranceCalculationResult) As Parameter.InsuranceCalculationResult
    Function DisplayResultOnGrid(ByVal CustomClass As Parameter.InsuranceCalculationResult) As Parameter.InsuranceCalculationResult
    Function DisplayResultOnGridAdditional(ByVal customClass As Parameter.InsuranceCalculationResult, ByVal InsuranceProductID As String) As Parameter.InsuranceCalculationResult

End Interface

