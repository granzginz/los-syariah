﻿Public Interface IApplicationDetailTransaction
    Function getApplicationDetailTransaction(ByVal oCustomClass As Parameter.ApplicationDetailTransaction) As Parameter.ApplicationDetailTransaction
    Sub ApplicationDetailTransactionSave(ByVal oCustomClass As Parameter.ApplicationDetailTransaction)
    Function getBungaNett(ByVal oCustomClass As Parameter.ApplicationDetailTransaction) As Parameter.ApplicationDetailTransaction
    Function getBungaNettKPR(ByVal oCustomClass As Parameter.ApplicationDetailTransaction) As Parameter.ApplicationDetailTransaction
    Function getBungaNettOperatingLease(ByVal oCustomClass As Parameter.ApplicationDetailTransaction) As Parameter.ApplicationDetailTransaction
End Interface
