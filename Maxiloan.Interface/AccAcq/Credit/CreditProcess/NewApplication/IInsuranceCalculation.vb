Public Interface IInsuranceCalculation
    Function ProcessSaveDataInsuranceApplicationHeaderAndDetail(ByVal oDataTable As DataTable, ByVal customClass As Parameter.InsuranceCalculation) As Parameter.InsuranceCalculation
    Function ProcessNewAppInsuranceByCompanySaveAddTemporary(ByVal customClass As Parameter.InsuranceCalculation) As String
    Function ProcessNewAppInsuranceByCompanyDetailSaveAddTemporary(ByVal customClass As Parameter.InsuranceCalculation) As String
    Function ProcessSaveInsuranceApplicationLastProcess(ByVal customClass As Parameter.InsuranceCalculationResult) As String
    Function GetDateEntryInsuranceData(ByVal customClass As Parameter.InsuranceCalculation) As Parameter.InsuranceCalculation
    Function DisplaySelectedPremiumOnGrid(ByVal CustomClass As Parameter.InsuranceCalculation) As Parameter.InsuranceCalculation
    Function ProcessKoreksiAsuransi(ByVal customClass As Parameter.InsuranceCalculationResult) As String
    Function ProcessAppInsuranceAgricultureDetailSave(ByVal customClass As Parameter.InsuranceCalculation) As String

End Interface
