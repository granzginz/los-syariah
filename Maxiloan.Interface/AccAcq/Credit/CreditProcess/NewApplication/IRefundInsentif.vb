﻿Public Interface IRefundInsentif
    Function getDistribusiNilaiInsentif(ByVal oCustomClass As Parameter.RefundInsentif) As Parameter.RefundInsentif
    Sub SupplierINCTVDailyDAdd(ByVal oCustomClass As Parameter.RefundInsentif)
    Function GetSPBy(ByVal ocustomclass As Parameter.RefundInsentif) As Parameter.RefundInsentif
    Function SupplierIncentiveDailyDSave(ByVal customclass As Parameter.RefundInsentif) As String
    Sub SupplierIncentiveDailyDDelete(ByVal customclass As Parameter.RefundInsentif)
    Sub AlokasiInsentifInternalDelete(ByVal customclass As Parameter.RefundInsentif)
    Sub AlokasiInsentifInternalSave(ByVal customclass As Parameter.RefundInsentif)
    Function UpdateDateEntryInsentif(ByVal customclass As Parameter.RefundInsentif) As String
    Function GoLiveJournalDummy(ByVal customClass As Parameter.RefundInsentif) As Parameter.RefundInsentif
    'Function SupplierIncentiveDailyDSaveFactoring(ByVal customclass As Parameter.RefundInsentif) As String
    'Sub SupplierINCTVDailyDAddFACT(ByVal oCustomClass As Parameter.RefundInsentif)
    'Sub SupplierINCTVDailyDAddMDKJ(ByVal oCustomClass As Parameter.RefundInsentif)
End Interface
