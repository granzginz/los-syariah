


Public Interface INewAppInsuranceByCust

    Function GetInsuredByCustomer(ByVal customClass As Parameter.NewAppInsuranceByCust) As Parameter.NewAppInsuranceByCust
    Function ProcessSaveInsuranceByCustomer(ByVal customClass As Parameter.NewAppInsuranceByCust) As String

End Interface
