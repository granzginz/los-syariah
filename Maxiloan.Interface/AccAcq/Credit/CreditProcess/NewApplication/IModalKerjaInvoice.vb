﻿Public Interface IModalKerjaInvoice

    Function GetInvoiceList(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
    Function GetInvoiceList2(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
    Function GetJatuhTempoList(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
    Function GetInvoiceDetail(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
    Function InvoiceSaveAdd(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
    Function InvoiceSaveEdit(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
    Function InvoiceSaveAdd2(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
    Function InvoiceSaveEdit2(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
    Function InvoiceDelete(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
    Function InvoiceSaveApplication(ByVal ocustomClass As Parameter.Application, ByVal oDataAmortisasi As DataTable) As String
    Function GetFactoringFee(ByVal ocustomClass As Parameter.Application) As Parameter.ModalKerjaInvoice
    Function FactoringApplicationFinancialSave(ByVal ocustomClass As Parameter.Application) As String
    Function ModalKerjaInvoiceDisburse(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
    Function GetDataByLastPaymentDate(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
    Function GetPaymentHistory(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
    Function GetInvoiceDetail2(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
End Interface
