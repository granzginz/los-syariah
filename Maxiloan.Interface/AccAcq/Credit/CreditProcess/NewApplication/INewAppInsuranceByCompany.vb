

Public Interface INewAppInsuranceByCompany

    Function CheckProspect(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
    Function GetInsuranceEntryStep1List(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
    Function GetInsuranceEntryStep2List(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
    Function GetInsuranceEntryFactoring(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
    Function EditGetInsuranceEntryStep1List(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
    Function EditGetInsuranceEntryStep2List(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
    Function FillInsuranceComBranch(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As DataTable
    Function HandleDropDownOnDataBound(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As DataTable
    Function EditInsuranceSelect(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
    Function GetInsurancehComByBranch(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As DataTable
    Function GetAssetCategory(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As DataTable
    Function GetInsuranceEntryStep1ListAgriculture(ByVal CustomClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
    Function GetInsuranceBranchNameRateCard(connString As String, applicationid As String, MaskAssBranchID As String, CoverageType As String, yearnum As Integer) As DataTable
    Function GetInsurancehComByBranchProduct(ByVal customclass As Parameter.InsCoBranch) As Parameter.InsCoBranch
    Function GetInsuranceComBranch(ByVal BO As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
End Interface
