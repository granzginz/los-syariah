﻿Public Interface IFactoringInvoice

    Function GetInvoiceList(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    Function GetInvoiceDetail(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    Function InvoiceSaveAdd(ByVal ocustomClass As Parameter.FactoringInvoice) As String
    Function InvoiceSaveEdit(ByVal ocustomClass As Parameter.FactoringInvoice) As String
    Function InvoiceDelete(ByVal ocustomClass As Parameter.FactoringInvoice) As String
    Function InvoiceSaveApplication(ByVal ocustomClass As Parameter.Application) As String
    Function GetFactoringFee(ByVal ocustomClass As Parameter.Application) As Parameter.FactoringInvoice
    Function FactoringApplicationFinancialSave(ByVal ocustomClass As Parameter.Application) As String
    Function FactoringInvoiceDisburse(ByVal ocustomClass As Parameter.FactoringInvoice) As String
    Function RestruckFactoringPaging(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    Function GetRestructFactoringData(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    Function GetInvoiceDetail2(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    Function GetPPH(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    Function RestructFactoringSave(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    Function GetInvoiceListPaid(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    Function GetInvoiceChangeDueDateList(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    'Function GetFinancialData_002(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    'Function GetTerm(ByVal intInstTerm As Integer) As Integer
    'Function SaveFinancialDataTab2End(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    'Function GetRefundPremiumToSupplier(ByVal data As Parameter.Supplier) As Parameter.Supplier
    'Function saveBungaNett(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    'Function SaveFinancialDataTab2(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
End Interface
