
Public Interface IFinancialData
    'modify kpr
    Function SaveKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function getDataKPRHeader(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function HitungAngsuranKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function getDataKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function DeleteAngsuranKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function GetDataGridKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function GetDataDrop(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function SaveFinancialDataTab2EndKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData

    'end
    Function GetFinancialData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function GetFinancialData_002(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function GetInstallmentSchedule(ByVal ocustomClass As Parameter.FinancialData) As DataTable
    Function GetInstAmtAdv(ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intNumInstallment As Double, ByVal dblNTF As Double) As Double
    Function GetInstAmtArr(ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intNumInstallment As Double, ByVal dblNTF As Double) As Double
    Function GetInstAmtAdvFlatRate(ByVal FlatRate As Double, ByVal intPayFreq As Integer, ByVal intNumInstallment As Double, ByVal dblNTF As Double) As Double
    Function GetEffectiveRateAdv(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer) As Double
    Function GetEffectiveRateArr(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer) As Double
    Function GetTerm(ByVal intInstTerm As Integer) As Integer
    Function GetEffectiveRateRO(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As Double
    Function GetInstAmtArrRO(ByVal EffectiveRate As Double, ByVal intNumInstallment As Integer, ByVal Runrate As Double, ByVal NTF As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As Double
    Function GetFlatRate(ByVal dblInterestTotal As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer, ByVal Tenor As Integer) As Double
    Function SaveFinancialData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function SaveFinancialDataSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function GetInstallmentSchSiml(ByVal oCustomClass As Parameter.FinancialData) As DataSet
    Function GetViewFinancialData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function ClearingInstallmentSchedule(ByVal oCustomClass As Parameter.FinancialData) As String
    Function GetPrincipleAmount(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function SaveAmortisasi(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function SaveAmortisasiSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function SaveAmortisasiIRR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function SaveAmortisasiIRRSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function GetNewEffRateIRR(ByVal oCustomClass As Parameter.FinancialData) As Double
    Function GetStepUpStepDownType(ByVal oCustomClass As Parameter.FinancialData) As String
    Function SaveAmortisasiStepUpDown(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function SaveAmortisasiStepUpDownSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function GetInstAmtStepUpDownRegLeasing(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function GetStepUpDownRegLeasingTBL(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function GetNewEffRateRegLeasing(ByVal oCustomClass As Parameter.FinancialData) As Double
    Function GetInstAmtStepUpDownLeasing(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function GetNewEffRateLeasing(ByVal oCustomClass As Parameter.FinancialData) As Double
    Function ListAmortisasiStepUpDown(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function ListAmortisasiStepUpDownSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Sub saveDefaultAdminFee(ByVal oCustomClass As Parameter.FinancialData)
    Sub saveDefaultAgentFee(ByVal oCustomClass As Parameter.FinancialData)
    Function GetRefundPremiumToSupplier(ByVal data As Parameter.Supplier) As Parameter.Supplier
    Function saveBungaNett(oClass As Parameter.FinancialData) As Parameter.FinancialData
    Function ListAmortisasiIRR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function SaveFinancialDataTab2(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function SaveFinancialDataTab2End(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function SaveFinancialDataTab2OperatingLease(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function SaveFinancialDataTab2EndOperatingLease(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function GoLiveJournalDummyOperatingLease(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function SaveFinancialDataOperatingLease(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData

End Interface
