
Public Interface IAssetData
    Function GetAssetData(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function GetCboEmp(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function GetCboEmpReport(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function GetCbo(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function GetAttribute(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function GetSerial(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function GetUsedNew(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function GetAO(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function GetSupplierGroupID(ByVal oCustomClass As Parameter.AssetData) As String
    Function CheckSerial(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function CheckAttribute(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function GetAssetDoc(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function CheckAssetDoc(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function AssetDataSaveAdd(ByVal oAssetData As Parameter.AssetData, _
       ByVal oAddress As Parameter.Address, ByVal oData1 As DataTable, _
       ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.AssetData
    Function GetAssetID(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function GetViewAssetData(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function EditAssetDataGetDefaultSupplierID(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function EditGetAttribute(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function EditGetAssetRegistration(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function EditAssetInsuranceEmployee(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function EditAssetDataDocument(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function TolakanNonCustSaveAdd(ByVal oCustomClass As Parameter.TolakanNonCust, ByVal oAddress As Parameter.Address) As Parameter.TolakanNonCust
    Function GetcboAlasanPenolakan(ByVal oCustomClass As Parameter.TolakanNonCust) As Parameter.TolakanNonCust
    Function GetTolakanNonCustPaging(ByVal oCustomClass As Parameter.TolakanNonCust) As Parameter.TolakanNonCust
    Function GetTolakanNonCustEdit(ByVal oCustomClass As Parameter.TolakanNonCust) As Parameter.TolakanNonCust
    Function GetTolakanNonCustDelete(ByVal oCustomClass As Parameter.TolakanNonCust) As String
    Function ListReportTolakanNonCust(ByVal oCustomClass As Parameter.TolakanNonCust) As Parameter.TolakanNonCust
    Function GetGradeAsset(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
    Function GetAssetDocWhere(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
End Interface
