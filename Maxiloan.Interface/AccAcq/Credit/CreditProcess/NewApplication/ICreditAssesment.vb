﻿Public Interface ICreditAssesment
    Function getCreditAssesment(ByVal oCustomClass As Parameter.CreditAssesment) As Parameter.CreditAssesment
    Sub CreditAssesmentSave(ByVal oCustomClass As Parameter.CreditAssesment)
    Function Proceed(oCustomClass As Parameter.CreditAssesment) As String
End Interface
