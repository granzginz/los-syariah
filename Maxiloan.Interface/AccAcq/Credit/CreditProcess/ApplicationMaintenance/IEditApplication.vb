
Public Interface IEditApplication

    Function GetApplicationEdit(ByVal oCustomClass As Parameter.Application) As Parameter.Application

    Function GetApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetCountAppliationId(ByVal oCustomClass As Parameter.Application) As Integer
    Function GetCopyFrom(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetTC(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetTC2(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetShowDataAgreement(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function ApplicationSaveAdd(ByVal oApplication As Parameter.Application, _
        ByVal oRefPersonal As Parameter.Personal, _
        ByVal oRefAddress As Parameter.Address, ByVal oMailingAddress As Parameter.Address, _
        ByVal oData1 As DataTable, ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.Application
    Function GetAddress(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetFee(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetViewApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application
    Function GetApplicationMaintenance(ByVal customClass As Parameter.Application) As Parameter.Application
    Function GetCD(ByVal oCustomClass As Parameter.Application) As Parameter.Application

    Function ApplicationSaveEdit(ByVal oApplication As Parameter.Application, _
       ByVal oRefPersonal As Parameter.Personal, _
       ByVal oRefAddress As Parameter.Address, ByVal oMailingAddress As Parameter.Address, _
       ByVal oData1 As DataTable, ByVal oData2 As DataTable, ByVal oData3 As DataTable, _
       ByVal MyDataTable As DataTable) As Parameter.Application

    Function EditAssetDataGetInfoAssetData(ByVal oCustumClass As Parameter.Application) As Parameter.Application
    Function AssetDataGetInfoAssetDataAwal(ByVal oCustomClass As Parameter.Application) As Parameter.Application

End Interface
