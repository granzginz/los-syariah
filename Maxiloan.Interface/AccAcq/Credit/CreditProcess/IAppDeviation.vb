﻿Public Interface IAppDeviation
    Function GetAppDeviation(ByVal oCustomClass As Parameter.AppDeviation) As Parameter.AppDeviation
    Sub AppDeviationPrint(ByVal oCustomClass As Parameter.AppDeviation)
    Function GetDeviationView(ByVal oCustomClass As Parameter.AppDeviation) As Parameter.AppDeviation
    Function GetApprovalPath(ByVal oCustomClass As Parameter.ApprovalScheme) As Parameter.ApprovalScheme
    Function GetApprovalPathTreeMember(ByVal oCustomClass As Parameter.ApprovalScheme) As Parameter.ApprovalScheme
End Interface
