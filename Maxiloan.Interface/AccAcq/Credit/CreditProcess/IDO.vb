

Public Interface IDO
    Function DOPaging(ByVal oCustomClass As Parameter._DO) As Parameter._DO
    Function Get_PONumber(ByVal customClass As Parameter._DO) As String
    Function GetDO_CheckBackDate(ByVal ocustomClass As Parameter._DO) As Integer
    Function Get_DOProcess(ByVal ocustomClass As Parameter._DO) As Parameter._DO
    Function GetAttribute(ByVal oCustomClass As Parameter._DO) As Parameter._DO
    Function GetDO_AssetDoc(ByVal oCustomClass As Parameter._DO) As Parameter._DO
    Sub DOSave(ByVal oCustomClass As Parameter._DO, ByVal oData1 As DataTable, ByVal oData2 As DataTable, ByVal oData3 As DataTable, ByVal oData4 As DataTable, ByVal oData5 As DataTable)
    Function CheckSerial(ByVal oCustomClass As Parameter._DO) As Parameter._DO
    Function CheckAttribute(ByVal oCustomClass As Parameter._DO) As Parameter._DO
    Function CheckAssetDoc(ByVal oCustomClass As Parameter._DO) As Parameter._DO
    Sub ApplicationReturnUpdate(ByVal oCustomClass As Parameter._DO)
End Interface
