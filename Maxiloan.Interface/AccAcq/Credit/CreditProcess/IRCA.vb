
Public Interface IRCA
    Function GetRCA(ByVal customClass As Parameter.RCA) As Parameter.RCA
    Function GetApprovalBM(ByVal customClass As Parameter.RCA) As Parameter.RCA

    Function RCASave(ByVal oRCA As Parameter.RCA, ByVal oData1 As DataTable, _
    ByVal oData2 As DataTable) As Parameter.RCA


    Function ApprovalBMSave(ByVal oRCA As Parameter.RCA, Optional isApr As Boolean = True) As Parameter.RCA

    Function GetCreditAssesmentList(ByVal oRCA As Parameter.RCA) As Parameter.RCA
End Interface
