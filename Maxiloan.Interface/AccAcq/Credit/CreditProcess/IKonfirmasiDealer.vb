﻿Public Interface IKonfirmasiDealer
    Function ValidationSerialNo(ByVal customClass As Parameter.KonfirmasiDealer) As DataTable
    Function KonfirmasiDealerPaging(ByVal customClass As Parameter.KonfirmasiDealer) As Parameter.KonfirmasiDealer
    Sub KonfirmasiDealerSave(ByVal customClass As Parameter.KonfirmasiDealer)
End Interface
