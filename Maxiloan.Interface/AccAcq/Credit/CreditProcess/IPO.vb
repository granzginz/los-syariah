

Public Interface IPO
    Function POPaging(ByVal oCustomClass As Parameter.PO) As Parameter.PO
    'Function POSave(ByVal oCustomClass As Parameter.PO, ByVal oData1 As DataTable, ByVal oData2 As DataTable) As String
    Function POSave(ByVal oCustomClass As Parameter.PO) As String
    Function PO_Kepada(ByVal ocustomClass As Parameter.PO) As Parameter.PO
    Function PO_DikirimKe(ByVal ocustomClass As Parameter.PO) As Parameter.PO
    Function PO_ItemFee(ByVal ocustomClass As Parameter.PO) As Parameter.PO
    Function Get_Combo_SupplierAccount(ByVal customClass As Parameter.PO) As Parameter.PO
    Function Get_PONumber(ByVal customClass As Parameter.PO) As String
    Function PO_Account(ByVal customClass As Parameter.PO) As Parameter.PO
    Function POExtendPaging(ByVal oCustomClass As Parameter.PO) As Parameter.PO
    Sub POExtendSaveEdit(ByVal customClass As Parameter.PO)
    Function POViewExtend(ByVal ocustomClass As Parameter.PO) As Parameter.PO

    Function GetIDKurs(ByVal customClass As Parameter.PO) As Parameter.PO

End Interface
