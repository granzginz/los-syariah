
Public Interface IPending
    Function GetPendingDataEntry(ByVal customClass As Parameter.Pending) As Parameter.Pending
    Function GetAO(ByVal customclass As Parameter.Pending) As DataTable
    Function GetSupplier(ByVal customclass As Parameter.Pending) As DataTable
    Function GetApproved(ByVal customclass As Parameter.Pending) As DataTable
    Function GetApplicationPendingInquiry(ByVal customClass As Parameter.Pending) As Parameter.Pending
End Interface
