Public Interface IPrint    
    Function getAgentFeeReceipt(ByVal customClass As Parameter.AgentFeeReceipt) As Parameter.AgentFeeReceipt
    Function saveAgentFeeReceiptPrint(ByVal customClass As Parameter.AgentFeeReceipt) As Parameter.AgentFeeReceipt
    Function getCreditFundingReceipt(ByVal customClass As Parameter.CreditFundingReceipt) As Parameter.CreditFundingReceipt
    Function saveCreditFundingReceiptPrint(ByVal customClass As Parameter.CreditFundingReceipt) As Parameter.CreditFundingReceipt
    Function getSuratKuasaFiducia(ByVal customClass As Parameter.SuratKuasaFiducia) As Parameter.SuratKuasaFiducia
    Function saveSuratKuasaPrint(ByVal customClass As Parameter.SuratKuasaFiducia) As Parameter.SuratKuasaFiducia
    Function getMKK(ByVal customClass As Parameter.Common) As DataSet
    Function getMKKReport(ByVal customClass As Parameter.MKK) As Parameter.MKK
    Function listAgreementMailForPrint(ByVal customClass As Parameter.AgreementList) As DataTable
    Function saveAgreementMailForPrint(ByVal customClass As Parameter.AgreementList) As Parameter.AgreementList
    Function printAgreementMailForPrint(ByVal customClass As Parameter.AgreementList) As Parameter.AgreementList
End Interface
