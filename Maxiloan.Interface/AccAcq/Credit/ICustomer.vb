Imports Maxiloan.Parameter
Public Interface ICustomer
    Function GetCustomerFromProspect(ByVal CustomClass As Parameter.Customer) As Parameter.Customer
    Function GetCustomer(ByVal CustomClass As Parameter.Customer) As Parameter.Customer
    Function CustomerPersonalGetIDType(ByVal oCustomer As Parameter.Customer) As Parameter.Customer
    Function CustomerCompanyGetIDType(ByVal oCustomer As Parameter.Customer) As Parameter.Customer
    Function GetCustomerEdit(ByVal oCustomClass As Parameter.Customer, ByVal Level As String) As Parameter.Customer
    Function CustomerPersonalAdd(ByVal oCustomer As Parameter.Customer) As Parameter.Customer
    Function PersonalSaveAdd(ByVal oCustomer As Parameter.Customer, _
                             ByVal oLegalAddress As Parameter.Address, _
                             ByVal oResAddress As Parameter.Address, _
                             ByVal oJDAddress As Parameter.Address, _
                             ByVal oDataOmset As DataTable, _
                             ByVal oDataFamily As DataTable,
                             ByVal oPCEX As Parameter.CustomerEX, _
                             ByVal oPCSI As Parameter.CustomerSI,
                             ByVal oPCSIAddress As Parameter.Address, _
                             ByVal oPCER As Parameter.CustomerER, _
                             ByVal oPCERAddress As Parameter.Address) As Parameter.Customer
    Function PersonalSaveEdit(ByVal oCustomer As Parameter.Customer, _
                              ByVal oLegalAddress As Parameter.Address, _
                              ByVal oResAddress As Parameter.Address, _
                              ByVal oJDAddress As Parameter.Address, _
                              ByVal oDataOmset As DataTable, _
                              ByVal oDataFamily As DataTable, _
                               ByVal oPCEX As Parameter.CustomerEX, _
                             ByVal oPCSI As Parameter.CustomerSI,
                             ByVal oPCSIAddress As Parameter.Address, _
                             ByVal oPCER As Parameter.CustomerER, _
                             ByVal oPCERAddress As Parameter.Address) As String
    Function BindCustomer1_002(ByVal oCustomer As Parameter.Customer) As Parameter.Customer
    Function BindCustomer2_002(ByVal oCustomer As Parameter.Customer) As Parameter.Customer
    Function BindCustomerC1_002(ByVal oCustomer As Parameter.Customer) As Parameter.Customer
    Function BindCustomerC2_002(ByVal oCustomer As Parameter.Customer) As Parameter.Customer
    Function CompanySaveAdd(ByVal oCustomer As Parameter.Customer, _
                            ByVal oAddress As Parameter.Address, _
                            ByVal oWarehouseAddress As Parameter.Address, _
                            ByVal oData1 As DataTable, _
                            ByVal oData2 As DataTable, _
                            ByVal CompanyCustomerDPEx As Parameter.CompanyCustomerDPEx, _
                            ByVal CompanyCustomerPJ As Parameter.CompanyCustomerPJ, _
                            ByVal CompanyCustomerPJAddress As Parameter.Address, _
                            ByVal companySIUPAddress As Parameter.Address) As Parameter.Customer
    Function CompanyDataPerusahaanSaveAdd(ByVal oCustomer As Parameter.Customer, _
                                   ByVal oAddress As Parameter.Address, _
                                   ByVal oWarehouseAddress As Parameter.Address, _
                                   ByVal CompanyCustomerDPEx As Parameter.CompanyCustomerDPEx, _
                                   ByVal companySIUPAddress As Parameter.Address
                                   ) As Parameter.Customer
    Function CompanySaveEdit(ByVal oCustomer As Parameter.Customer, _
                             ByVal oAddress As Parameter.Address, _
                             ByVal oWarehouseAddress As Parameter.Address, _
                             ByVal oData1 As DataTable, _
                             ByVal oData2 As DataTable,
                             ByVal CompanyCustomerDPEx As Parameter.CompanyCustomerDPEx, _
                             ByVal CompanyCustomerPJ As Parameter.CompanyCustomerPJ, _
                             ByVal CompanyCustomerPJAddress As Parameter.Address, _
                              ByVal companySIUPAddress As Parameter.Address) As String
    Function CompanyDataPerusahaanSaveEdit(ByVal oCustomer As Parameter.Customer, _
                                    ByVal oAddress As Parameter.Address, _
                                    ByVal oWarehouseAddress As Parameter.Address, _
                                    ByVal CompanyCustomerDPEx As Parameter.CompanyCustomerDPEx, _
                                    ByVal companySIUPAddress As Parameter.Address) As String
    Function CompanyKeuanganSaveEdit(ByVal oCustomer As Parameter.Customer) As String
    Function CompanyLegalitasSaveEdit(ByVal oCustomer As Parameter.Customer,
                                    ByVal oData2 As DataTable) As String
    Function CompanyManagementSaveEdit(ByVal oCustomer As Parameter.Customer, _
                                    ByVal oData1 As DataTable, _
                                    ByVal CompanyCustomerPJ As Parameter.CompanyCustomerPJ, _
                                    ByVal CompanyCustomerPJAddress As Parameter.Address) As String
    Sub Generate_XMLC(ByVal pStrFile As String, ByVal pStrName As String, _
                          ByVal pStrNPWP As String, ByVal pStrAddress As String, ByVal pStrRT As String, _
                          ByVal pStrRW As String, ByVal pStrKelurahan As String, _
                          ByVal pStrKecamatan As String, ByVal pStrCity As String, _
                          ByVal pStrZipCode As String, ByVal pStrAPhone1 As String, _
                          ByVal pStrPhone1 As String, ByVal pStrAPhone2 As String, _
                          ByVal pStrPhone2 As String, ByVal pStrAFax As String, _
                          ByVal pStrFax As String, ByRef pStrSuccess As Boolean)
    Sub Generate_XMLP(ByVal pStrFile As String, ByVal pStrGelarDepan As String, ByVal pStrName As String, ByVal pStrGelarBelakang As String, _
                    ByVal pStrType As String, ByVal pStrIDType As String, ByVal pStrIDTypeName As String, _
                    ByVal pStrNumber As String, ByVal IDExpiredDate As String, ByVal pStrGender As String, _
                    ByVal pStrBirthPlace As String, ByVal pStrBirthDate As String, _
                    ByRef pStrSuccess As Boolean, ByVal pMotherName As String, ByVal pKartuKeluarga As String, _
                    ByVal pNamaPasangan As String, ByVal pStatusPerkawinan As String)

    Function GetViewCustomer(ByVal CustomClass As Parameter.Customer) As Parameter.Customer
    Function GetViewCustomerEmpOmset(ByVal CustomClass As Parameter.Customer) As Parameter.Customer
    Function GetViewCustomerEntOmset(ByVal CustomClass As Parameter.Customer) As DataTable
    Function GetViewCustomerFamily(ByVal CustomClass As Parameter.Customer) As Parameter.Customer

    Function GetViewCustomer_Summary(ByVal CustomClass As Parameter.Customer) As Parameter.Customer
    Function GetViewCustomerEmpOmset_Summary(ByVal CustomClass As Parameter.Customer) As Parameter.Customer
    Function GetViewCustomerEntOmset_Summary(ByVal CustomClass As Parameter.Customer) As DataTable
    Function GetViewCustomerFamily_Summary(ByVal CustomClass As Parameter.Customer) As Parameter.Customer

    Function GetViewCompanyCustomer(ByVal CustomClass As Parameter.Customer) As Parameter.Customer
    Function GetViewCompanyCustomerManagement(ByVal CustomClass As Parameter.Customer) As Parameter.Customer
    Function GetViewCompanyCustomerDocument(ByVal CustomClass As Parameter.Customer) As Parameter.Customer
    Function GetCustomerType(ByVal CustomClass As Parameter.Customer) As String
    Function GetAgreementListCompRpt(ByVal Customclass As Parameter.Customer) As Parameter.Customer
    Function GetAgreementListCompRpt_Summary(ByVal Customclass As Parameter.Customer) As Parameter.Customer
    Function GetAgreementListSummary1(ByVal Customclass As Parameter.Customer) As Parameter.Customer
    Function GetAgreementListSummary2(ByVal Customclass As Parameter.Customer) As Parameter.Customer
    Function GetTotalCustomer(ByVal strCustomerID As String, ByVal strConnection As String) As Integer
    Function BindCustomer1_002_withSI(ByVal oCustomer As Parameter.Customer, ByVal SIdata As Parameter.CustomerSI) As Parameter.Customer
    Function PersonalCustomerIdentitasAdd(ByVal oCustomer As Parameter.Customer, _
                                    ByVal oLegalAddress As Parameter.Address, _
                                    ByVal oResAddress As Parameter.Address,
                                    ByVal oPCEX As Parameter.CustomerEX) As Parameter.Customer
    Function PersonalCustomerPekerjaanSaveEdit(ByVal oCustomer As Parameter.Customer, _
                                     ByVal oJDAddress As Parameter.Address, _
                                     ByVal oPCEX As Parameter.CustomerEX) As String
    Function PersonalCustomerKeuanganSaveEdit(ByVal oCustomer As Parameter.Customer) As String
    Function PersonalCustomerPasanganSaveEdit(ByVal oCustomer As Parameter.Customer, _
                                     ByVal oPCSI As Parameter.CustomerSI, _
                                     ByVal oPCSIAddress As Parameter.Address) As String
    Function PersonalCustomerGuarantorSaveEdit(ByVal oCustomer As Parameter.Customer, _
                                    ByVal oPCSI As Parameter.CustomerSI, _
                                    ByVal oPCSIAddress As Parameter.Address) As String
    Function PersonalCustomerEmergencySaveEdit(ByVal oCustomer As Parameter.Customer, _
                                     ByVal oPCER As Parameter.CustomerER, _
                                     ByVal oPCERAddress As Parameter.Address) As String
    Function PersonalCustomerKeluargaSaveEdit(ByVal oCustomer As Parameter.Customer, _
                                     ByVal oDataFamily As DataTable) As String
    'Function PersonalCustomerOLSaveEdit(cnn As String, custid As String, data As IList(Of PinjamanLain)) As String
    Function PersonalCustomerOLSaveEdit(cnn As String, custid As String, data As IList(Of PinjamanLain)) As String
    Function PersonalCustomerIdentitasSaveEdit(ByVal oCustomer As Parameter.Customer, _
                                    ByVal oLegalAddress As Parameter.Address, _
                                    ByVal oResAddress As Parameter.Address,
                                    ByVal oPCEX As Parameter.CustomerEX) As String

    Function LoadCombo(cnn As String, ty As String) As IList(Of Parameter.CommonValueText)
    Function GetCompanyCustomerEC(cnn As String, oCustomClass As Parameter.CompanyCustomerEC) As Parameter.CompanyCustomerEC
    Function CompanyCustomerECSaveEdit(cnn As String, oCustomClass As Parameter.CompanyCustomerEC) As String

    Function GetCompanyCustomerGuarantor(cnn As String, customerid As String) As IList(Of Parameter.CompanyCustomerGuarantor)
    Function CompanyCustomerGuarantorSaveEdit(cnn As String, customerid As String, ccGuarantors As IList(Of Parameter.CompanyCustomerGuarantor)) As String
    Function GetPersonalOL(cnn As String, custId As String) As IList(Of PinjamanLain)
	Function CheckPersonalCustomerDocument(ByVal CustomClass As Parameter.Customer) As Integer

	Function GetCompanyCustomerPK(cnn As String, oCustomClass As Parameter.CompanyCustomerPK) As Parameter.CompanyCustomerPK
	Function CompanyCustomerPKSaveAdd(ByVal oCustomClass As Parameter.CompanyCustomerPK) As Parameter.CompanyCustomerPK
	Function CompanyCustomerPKSaveEdit(ByVal oCustomClass As Parameter.CompanyCustomerPK) As Parameter.CompanyCustomerPK

End Interface
