Public Interface ILoanActivationReport
    Function listLoanActivationReport(ByVal customClass As Parameter.LoanActivationReport) As Parameter.LoanActivationReport
    Function listDailySalesReport(ByVal customClass As Parameter.LoanActivationReport) As Parameter.LoanActivationReport
    Function listAgreementDownloadReport(ByVal customClass As Parameter.LoanActivationReport) As Parameter.LoanActivationReport
End Interface
