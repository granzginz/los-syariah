﻿

Public Interface IAgunanLain
    Function JenisAgunanList(ByVal co As Parameter.AgunanLain) As Parameter.AgunanLain
	Function JenisAgunanAdd(ByVal customClass As Parameter.AgunanLain) As String
	Function JenisAgunanEdit(ByVal customClass As Parameter.AgunanLain) As String
	Function JenisAgunanDelete(ByVal customClass As Parameter.AgunanLain) As String
	Function ListAgunan(ByVal customClass As Parameter.AgunanLain) As Parameter.AgunanLain
	Function getDataLandBuildidng(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
	Function getDataInvoice(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
	Function getDataBPKB(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
	Function getAgunanNett(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
	Function saveEditDataBPKB(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
	Function saveAddDataBPKB(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
	Function saveEditDataInvoice(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
	Function saveAddDataInvoice(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
	Function saveEditDataLandBuilding(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
	Function saveAddDataLandBuilding(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
	Function AddCollateral(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
	Function GetCollateralId(ByVal oCustomClass As Parameter.AgunanLain) As String
	Function GetCboCollateralType(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
	Function PledgingAdd(ByVal customClass As Parameter.AgunanLain) As String
	Function GetDetailPledging(ByVal customClass As Parameter.AgunanLain) As Parameter.AgunanLain
End Interface
