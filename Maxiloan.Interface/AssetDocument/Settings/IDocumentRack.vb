
Public Interface IDocumentRack
#Region "DocumentRack"
    Function GetDocumentRack(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
    Function GetDocumentRackEdit(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
    Function GetDocumentRackReport(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
    Function SaveDocumentRack(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
    Function DeleteDocumentRack(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
    Function GetBranchListData(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
#End Region
#Region "DocumentFiling"
    Function GetDocumentFiling(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
    Function GetDocumentFilingEdit(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
    Function GetDocumentFilingReport(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
    Function SaveDocumentFiling(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
    Function DeleteDocumentFiling(ByVal aoDocumentRack As Parameter.DocumentRack) As Parameter.DocumentRack
#End Region
End Interface
