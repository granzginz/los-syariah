
Public Interface IDocumentCheckOut
    Function GetDocumentCheckOutH(ByVal aDocumentCheckOut As Parameter.DocumentCheckOut) As Parameter.DocumentCheckOut
    Function GetDocumentCheckOutD(ByVal aDocumentCheckOut As Parameter.DocumentCheckOut) As Parameter.DocumentCheckOut
    Function GetDocumentRecipientList(ByVal aDocumentCheckOut As Parameter.DocumentCheckOut) As Parameter.DocumentCheckOut
    Function GetDocumentOnHandForCheckOut(ByVal aDocumentCheckOut As Parameter.DocumentCheckOut) As Parameter.DocumentCheckOut
    Function SaveDocumentCheckOut(ByVal aDocumentCheckOut As Parameter.DocumentCheckOut) As Parameter.DocumentCheckOut
    Function DeleteDocumentCheckOut(ByVal aDocumentCheckOut As Parameter.DocumentCheckOut) As Parameter.DocumentCheckOut
    Function GetDocumentCheckOutRpt(ByVal aDocumentCheckOut As Parameter.DocumentCheckOut) As Parameter.DocumentCheckOut
    Function PostDocumentCheckOut(ByVal aDocumentCheckOut As Parameter.DocumentCheckOut) As Parameter.DocumentCheckOut
End Interface
