
Public Interface IDocumentCheckIn
    Function GetDocumentCheckInH(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
    Function GetDocumentCheckInD(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
    Function GetDocumentSenderList(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
    Function GetOutstandingDocument(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
    Function SaveDocumentCheckIn(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
    Function DeleteDocumentCheckIn(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
    Function GetDocumentCheckInRpt(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
    Function GetBranchDocumentRack(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
    Function GetBranchDocumentFilling(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
    Function PostDocumentCheckIn(ByVal aDocumentCheckIn As Parameter.DocumentCheckIn) As Parameter.DocumentCheckIn
End Interface