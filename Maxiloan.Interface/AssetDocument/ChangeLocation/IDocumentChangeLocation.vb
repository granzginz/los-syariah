
Public Interface IDocumentChangeLocation
    Function GetOnHandDocumentList(ByVal aDocumentChangeLocation As Parameter.DocumentChangeLocation) As Parameter.DocumentChangeLocation
    Function SaveAssetDocumentChangeLoc(ByVal aDocumentChangeLocation As Parameter.DocumentChangeLocation) As Parameter.DocumentChangeLocation
    Function GetDocumentChangeLocationLog(ByVal aDocumentChangeLocation As Parameter.DocumentChangeLocation) As Parameter.DocumentChangeLocation
    Function AssetDocumentReport(ByVal customclass As Parameter.DocumentChangeLocation) As DataSet
End Interface