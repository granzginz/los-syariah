

Public Interface IPaymentRequest
    Function GetTableTransaction(ByVal customclass As Parameter.PaymentRequest, ByVal strFile As String) As Parameter.PaymentRequest
    Function Get_RequestNo(ByVal customClass As Parameter.PaymentRequest) As String
    Function SavePR(ByVal customclass As Parameter.PaymentRequest) As String
    Function GetPaymentRequestPaging(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
    Function GetPaymentRequestPagingFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
    Function GetPenjualanPagingFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
    Function GetPenghapusanPagingFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
    Function GetPaymentReceivePaging(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
    Function GetPaymentRequestHeaderAndDetail(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
    Function GetPaymentRequestHeaderAndDetailFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
    Function GetPaymentReceiveHeaderAndDetailFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
    Function GetPenjualanHeaderAndDetailFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
    Function GetPenghapusanHeaderAndDetailFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
    Function GetPaymentRequestDataset(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
    Sub saveApprovalPaymentRequest(ByVal oClass As Parameter.PaymentRequest, ByVal strApproval As String)
    Sub saveApprovalPaymentRequestFA(ByVal oClass As Parameter.PaymentRequest, ByVal strApproval As String)
    Sub saveApprovalPaymentReceiveFA(ByVal oClass As Parameter.PaymentRequest, ByVal strApproval As String)
    Sub saveApprovalPenjualanFA(ByVal oClass As Parameter.PaymentRequest, ByVal strApproval As String)
    Sub saveApprovalPenghapusanFA(ByVal oClass As Parameter.PaymentRequest, ByVal strApproval As String)
    Function GetPaymentRequestFormKuning(ByVal customClass As IList(Of String), cnn As String) As DataSet
    Sub PayReqStatusReject(ByVal oClass As Parameter.PaymentRequest, ByVal strApproval As String)
    Function PRCOAUpdate(ByVal customClass As Parameter.PaymentRequest) As String
    Function GetPaymentVoucherByRequestNoPrint(ByVal ocustom As Parameter.PaymentRequest) As Parameter.PaymentRequest
    Function GetBankAccountOther(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
    Function UpdatePR(ByVal customclass As Parameter.PaymentRequest) As String
    Function CekPR(ByVal customclass As Parameter.PaymentRequest) As String
    Function GetFAPaymentRequestDataSet(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest
End Interface
