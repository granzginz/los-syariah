
Public Interface IInvSelling
    Function GetInvSellingReceive(ByVal customClass As Parameter.InvSelling) As Parameter.InvSelling
    Function GetInvSellingReceiveApproval(ByVal customClass As Parameter.InvSelling) As Parameter.InvSelling

    Function InvSellingReceiveView(ByVal customclass As Parameter.InvSelling) As Parameter.InvSelling
    Function SavingInventorySellingReceive(ByVal oCustomClass As Parameter.InvSelling) As String
    Sub SavingInventorySellingExec(ByVal oCustomClass As Parameter.InvSelling)

    Function InvSellingReceiveViewRepo(ByVal customclass As Parameter.InvSelling) As Parameter.InvSelling
    Function SavingInventorySellingReceiveApproval(ByVal oCustomClass As Parameter.InvSelling) As String
End Interface
