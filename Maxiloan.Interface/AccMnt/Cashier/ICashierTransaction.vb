
Public Interface ICashierTransaction
    Function ListCashierHistory(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction
    Sub CashierOpen(ByVal oCustomClass As Parameter.CashierTransaction)
    Sub CashierClose(ByVal oCustomClass As Parameter.CashierTransaction)
    Function CashierCloseList(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction
    Function CHCloseList(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction
    Sub HeadCashierClose(ByVal oCustomClass As Parameter.CashierTransaction)
    Function CheckHeadCashier(ByVal oCustomClass As Parameter.CashierTransaction) As Boolean
    Function CheckAllCashierClose(ByVal oCustomClass As Parameter.CashierTransaction) As Boolean
    Function GetCashier(ByVal customclass As Parameter.CashierTransaction) As DataTable
    Sub HeadCashierBatchProcess(ByVal ocustomclass As Parameter.CashierTransaction)
    Function GLInterfaceHeader(ByVal ocustomclass As Parameter.CashierTransaction) As DataTable
    Function GLInterfaceDetail(ByVal ocustomclass As Parameter.CashierTransaction) As DataTable
    Function FileAllocationGLInterface(ByVal strConnection As String) As String
    Function GetTransaction(ByVal customclass As Parameter.CashierTransaction) As DataTable
    Function GetOutProc(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction
    Function GetListOutProc(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction
    Function getCashBankBalance(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction
    Function getCashBankBalanceHistory(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction


End Interface
