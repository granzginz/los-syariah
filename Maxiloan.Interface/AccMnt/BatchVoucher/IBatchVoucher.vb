
Public Interface IBatchVoucher
    Function BatchVoucherPaging(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
    Sub BatchVoucherAdd(ByVal oCustomClass As Parameter.BatchVoucher)
    Sub BatchVoucherEdit(ByVal oCustomClass As Parameter.BatchVoucher)
    Sub BatchVoucherDelete(ByVal oCustomClass As Parameter.BatchVoucher)
    Sub BatchVoucherPosted(ByVal oCustomClass As Parameter.BatchVoucher)
    Function BatchVoucherView(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
    Function CashBankBatchVoucherPaging(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
    Sub CashBankBatchVoucherAdd(ByVal oCustomClass As Parameter.BatchVoucher)
    Sub CashBankBatchVoucherEdit(ByVal oCustomClass As Parameter.BatchVoucher)
    Function CashBankBatchVoucherView(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
    Sub CashBankBatchVoucherDelete(ByVal oCustomClass As Parameter.BatchVoucher)
    Function getTransactionBatchUpload(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
    Sub saveSingleBatch(ByVal oCustomClass As Parameter.BatchVoucher)
    Sub DeleteUploadedBatch(ByVal oCustomClass As Parameter.BatchVoucher)
    Function getCompleteBatch(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher
End Interface
