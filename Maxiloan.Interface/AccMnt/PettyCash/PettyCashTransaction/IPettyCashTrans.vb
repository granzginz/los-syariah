
Public Interface IPettyCashTrans
    Function PCReimburseReconcile(ByVal customClass As Parameter.PettyCashReconcile) As Parameter.PettyCashReconcile
    Function GetTablePC(ByVal customclass As Parameter.PettyCash, ByVal strFile As String) As Parameter.PettyCash
    Function SavePCTrans(ByVal customclass As Parameter.PettyCash, ByVal strFile As String) As String
    Function SavePCReimburse(ByVal ustomclass As Parameter.PettyCash) As Parameter.PettyCash
    Function PCreimburseStatusUpdate(ByVal customClass As Parameter.PettyCash) As String
    Function PCreimburseStatusUpdateACC(ByVal customClass As Parameter.PettyCash) As String
    Function PCreimburseStatusUpdateACCReject(ByVal customClass As Parameter.PettyCash) As String
    Function GetPettyCashFormKuning(ByVal customClass As IList(Of String), cnn As String) As DataSet
    Function PCreimburseStatusReject(ByVal customclass As Parameter.PettyCash) As String
    Function PCreimburseCOAUpdate(ByVal customclass As Parameter.PettyCash) As String
End Interface
