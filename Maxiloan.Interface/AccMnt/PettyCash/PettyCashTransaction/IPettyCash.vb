

Public Interface IPettyCash
    Function GetARecord(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
    Function GetPagingTable(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
    Function GetARecordAndDetailTable(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
    Function GetARecordAndDetailTableACC(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
    Function GetDetailTable(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
    Function GetReportDataSet(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
    Function InqPCReimburse(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash
    Function InqPCReimburseACC(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash
    Function getViewPCReimburseLabel(ByVal oCustomClass As Parameter.PettyCash) As Parameter.PettyCash
    Function GetViewPCReimburseGrid(ByVal oCustomClass As Parameter.PettyCash) As Parameter.PettyCash
    Function GetViewPCReimburseGridACC(ByVal oCustomClass As Parameter.PettyCash) As Parameter.PettyCash
    Sub SavePettyCashReversal(ByVal oET As Parameter.PettyCash)
    Sub EditCOAPettyCash(ByVal oET As Parameter.PettyCash)
    Function GetPettyCashVoucher(ByVal oCustomClass As Parameter.PettyCash) As DataSet
    Function AprPCReimburse(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash
    Function AprPCReimburseisFinal(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash
    Function AprPCReimburseIsValidApproval(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash
    Function GetCboUserAprPC(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash
    Function PCAprReimburseSave(ByVal ocustomclass As Parameter.PettyCash) As String
    Function PCAprReimburseSaveToNextPerson(ByVal ocustomclass As Parameter.PettyCash) As String
    Function GetCboUserAprPCAll(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash
    Function GetARecordAndDetailTableACCWithSeq(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash
    Function GetARecordAndDetailTableWithSeq(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash

    Function GetViewPCReimburseGridHistoryReject(ByVal oCustomClass As Parameter.PettyCash) As Parameter.PettyCash
    Function GetViewPCReimburseGridHistoryApprovel(ByVal oCustomClass As Parameter.PettyCash) As Parameter.PettyCash
End Interface