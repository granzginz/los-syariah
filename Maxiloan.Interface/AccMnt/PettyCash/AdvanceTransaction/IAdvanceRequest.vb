

Public Interface IAdvanceRequest
    Sub SaveAdvanceTransaction(ByVal oETAdvanceRequest As Parameter.AdvanceRequest)
    Function GetPettyCashAdvanceList(ByVal oETAdvReq As Parameter.AdvanceRequest) As Parameter.AdvanceRequest
    Function GetAnAdvanceTransactionRecord(ByVal oETAdvReq As Parameter.AdvanceRequest) As Parameter.AdvanceRequest
    Function GetPettyCashAdvanceRpt(ByVal oETAdvReq As Parameter.AdvanceRequest) As Parameter.AdvanceRequest
    Sub SaveAdvanceReturnTransaction(ByVal oETAdvanceRequest As Parameter.AdvanceRequest)
    Function CreateAdvanceTransactionDetail(cnn As String, advanceNo As String, atDetail As IList(Of Parameter.AdvanceDetail)) As String
    'Function GetTableAR(ByVal customclass As Parameter.AdvanceRequest, ByVal strFile As String) As Parameter.AdvanceRequest
End Interface
