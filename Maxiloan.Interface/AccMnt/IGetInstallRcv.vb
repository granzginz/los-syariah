
Public Interface IGetInstallRcv
    Function GetOutstandingAR(ByVal applicationid As String, ByVal valuedate As Date) As Parameter.InstallRcv
    Function GetBankAccountID(ByVal BranchId As String) As DataTable
    Function GetCollector(ByVal BranchId As String) As DataTable
    Function GetTTSNo(ByVal CollectorId As String) As DataTable
End Interface
