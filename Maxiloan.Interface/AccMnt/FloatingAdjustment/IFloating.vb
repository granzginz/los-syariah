
Public Interface IFloating
    Function ReschedulingProduct(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function LastAdjustment(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function SaveFloatingData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function GetFloatingData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function FloatingPrintTrial(ByVal oCustomClass As Parameter.FinancialData) As System.Data.DataSet
End Interface


