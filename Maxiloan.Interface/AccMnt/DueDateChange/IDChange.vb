

Public Interface IDChange
    Function GetListAmor(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
    Function GetList(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
    Function GetMaxDate(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
    Function GetListExec(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
    Sub DueDateChangeRequest(ByVal oCustomClass As Parameter.DChange)
	Sub DueDateChangeExec(ByVal oCustomClass As Parameter.DChange)
    Function GetListAmorMDKJ(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
    Function GetListAmorFACT(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
#Region "Modal Kerja"
    Function GetListAmorModalKerja(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
	Function GetListAmorModalKerjaFromTable(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
	Function CreateAmorModalKerja(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
	Function GetListModalKerja(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
	'Function GetMaxDate(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
	Function GetListExecMDKJ(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
	Sub MDKJDueDateChangeRequest(ByVal oCustomClass As Parameter.DChange)
	Sub DueDateChangeExecMDKJ(ByVal oCustomClass As Parameter.DChange)
#End Region

#Region "Factoring"
	Function GetListAmorFactoring(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
	Function GetListInvoiceFactoring(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
	Function GetListAmorFactoringFromTable(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
	Function CreateAmorFactoring(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
	Function GetListFactoring(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
    'Function GetMaxDate(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
    Function GetListExecFactoring(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
    Function GetListExecFactoringApproval(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
    Sub DueDateChangeRequestFactoring(ByVal oCustomClass As Parameter.DChange)
    Sub DueDateChangeExecFactoring(ByVal oCustomClass As Parameter.DChange)
    Function GetListFactoringView(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
    Function GetListFactoringViewApproval(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
    Function GetListInvoiceFactoringView(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
    Function GetListAmorFactoringView(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
#End Region

    Function GetListFactAndMU(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
    Function GetListReschedulingApproval(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange

End Interface
