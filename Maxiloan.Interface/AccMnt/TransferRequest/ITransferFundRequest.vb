

Public Interface ITransferFundRequest
    Function GetTableTransaction(ByVal customclass As Parameter.TransferFundRequest, ByVal strFile As String) As Parameter.TransferFundRequest
    Function Get_RequestNo(ByVal customClass As Parameter.TransferFundRequest) As String
    Function SavePR(ByVal customclass As Parameter.TransferFundRequest) As String
    Function GetTransferFundRequestPaging(ByVal customClass As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
    Function GetTransferFundRequestHeaderAndDetail(ByVal customClass As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
    Function GetTransferFundRequestDataset(ByVal customClass As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
    Sub saveApprovalTransferFundRequest(ByVal oClass As Parameter.TransferFundRequest, ByVal strApproval As String)
    Function GetTransferFundRequestFormKuning(ByVal customClass As IList(Of String), cnn As String) As DataSet
    Sub PayReqStatusReject(ByVal oClass As Parameter.TransferFundRequest, ByVal strApproval As String)
    Function PRCOAUpdate(ByVal customClass As Parameter.TransferFundRequest) As String
    Function GetPaymentVoucherByRequestNoPrint(ByVal ocustom As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
    Function GetBankAccountOther(ByVal customClass As Parameter.TransferFundRequest) As Parameter.TransferFundRequest
    Function UpdatePR(ByVal customclass As Parameter.TransferFundRequest) As String
    Function CekPR(ByVal customclass As Parameter.TransferFundRequest) As String
End Interface
