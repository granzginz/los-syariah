Public Interface IWriteOff
    Function GetGeneralPagingWO(ByVal customClass As Parameter.WriteOff) As Parameter.WriteOff
    Function WriteOffGetSAAmount(ByVal oCustomClass As Parameter.WriteOff) As Parameter.WriteOff
    Sub WORequest(ByVal oCustomClass As Parameter.WriteOff)
    Function WriteOffInquiry(ByVal oCustomClass As Parameter.WriteOff) As Parameter.WriteOff
    Function WOInquiryReport(ByVal customclass As Parameter.WriteOff) As Parameter.WriteOff
    Function WriteOffView(ByVal oCustomClass As Parameter.WriteOff) As Parameter.WriteOff
    Sub WriteOffEditPotensiTagih(ByVal oCustomClass As Parameter.WriteOff)
End Interface


