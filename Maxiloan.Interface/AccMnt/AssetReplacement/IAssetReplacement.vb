
Public Interface IAssetReplacement
    Function AssetReplacementPaging(ByVal customClass As Parameter.AssetReplacement) As Parameter.AssetReplacement
    Function GetAssetRepSerial(ByVal oCustomClass As Parameter.AssetReplacement) As Parameter.AssetReplacement
    Sub AssetReplacementSaveAdd(ByVal oCustomClass As Parameter.AssetReplacement, _
          ByVal oAddress As Parameter.Address, ByVal oData1 As DataTable, _
          ByVal oData2 As DataTable)
    Function AssetRepApproval(ByVal oCustomClass As Parameter.AssetReplacement) As Parameter.AssetReplacement
    Function AssetInquiryReport(ByVal customclass As Parameter.AssetReplacement) As Parameter.AssetReplacement
    Function ViewAssetReplInquiry(ByVal oCustomClass As Parameter.AssetReplacement) As Parameter.AssetReplacement
    Sub SaveAssetReplacementCancel(ByVal customClass As Parameter.AssetReplacement)
    Sub SaveAssetReplacementExecute(ByVal customClass As Parameter.AssetReplacement)
End Interface


