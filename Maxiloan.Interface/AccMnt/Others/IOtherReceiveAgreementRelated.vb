
Public Interface IOtherReceiveAgreementRelated
    Sub OtherReceiveAgreementRelatedSaved(ByVal customclass As Parameter.OtherReceiveAgreement)
    Function GetTableOtherReceiveAgreementRelated(ByVal customclass As Parameter.OtherReceiveAgreement, ByVal strFile As String) As Parameter.OtherReceiveAgreement
End Interface
