

Public Interface IOtherDisburse
    Function GetTableTrans(ByVal customclass As Parameter.OtherReceive, ByVal strFile As String) As Parameter.OtherReceive
    Function SaveTrans(ByVal customclass As Parameter.OtherReceive, ByVal strFile As String) As String
    Function OtherDisburseAdd(ByVal customclass As Parameter.OtherReceive, ByVal strFile As String) As Parameter.OtherReceive
End Interface
