

Public Interface IAgreementTransfer
    Function AssetDocPaging(ByVal customclass As Parameter.DChange) As Parameter.DChange
    Function GetCust(ByVal customclass As Parameter.DChange) As Parameter.DChange
    Sub AgreementTransferReq(ByVal oCustomClass As Parameter.DChange)
    Function GetListExecAT(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
    Function GetATTC(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
    Function GetATTC2(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange
    Sub ATExec(ByVal oCustomClass As Parameter.DChange)
    Function PrintEndorsAgreementTransfer(ByVal customClass As Parameter.DChange) As String

End Interface
