
Public Interface IPDCMultiAgreement
    Function ListAgreementCustomer(ByVal oCustomClass As Parameter.PDCMultiAgreement) As Parameter.PDCMultiAgreement
    Function AgreementInformation(ByVal oCustomClass As Parameter.PDCMultiAgreement) As DataTable
    Sub SavePDCMultiAgreement(ByVal oCustomClass As Parameter.PDCMultiAgreement)
    Sub SaveEditPDCMultiAgreement(ByVal oCustomClass As Parameter.PDCMultiAgreement)
    Function GetEditPDCMultiAgreement(ByVal oCustomClass As Parameter.PDCMultiAgreement) As Parameter.PDCMultiAgreement
End Interface
