

Public Interface IPDCRInquiry
    Function ListPDCRInquiry(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
    Function ListPDCRInquiryV(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
    Function ListPDCRInquiryDet(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
    Function ListPDCInquiryDetail(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
    Function ListPDCInquiryListDetail(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
    Function ListPDCInquiryStatus(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
    Function ListPDCInqReport(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
    Function ListPDCInqStatusReport(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
    Function InqPDCIncomplete(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
    Function PDCInquiryDetailwithGiroSeqNo(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
End Interface
