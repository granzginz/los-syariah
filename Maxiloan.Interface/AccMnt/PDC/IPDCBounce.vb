

Public Interface IPDCBounce
    Function PDCBounceList(ByVal oCustomClass As Parameter.PDCBounce) As Parameter.PDCBounce
    Sub ProcesPDCBounce(ByVal oCustomClass As Parameter.PDCBounce)
    Function PDCBounceView(ByVal oCustomClass As Parameter.PDCBounce) As Parameter.PDCBounce
    Function PrintPDCBounce(ByVal oCustomClass As Parameter.PDCBounce) As DataSet
End Interface
