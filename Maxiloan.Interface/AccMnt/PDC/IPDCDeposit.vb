

Public Interface IPDCDeposit
    Function ListPDCDeposit(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
    Function SavePDCDeposit(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
    Function CheckDupPDCStatus(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
End Interface
