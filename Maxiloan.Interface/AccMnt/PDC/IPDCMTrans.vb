

Public Interface IPDCMTrans
    Function ListPDCMTrans(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
    Function GetTablePDCTrans(ByVal customclass As Parameter.PDCReceive, ByVal strFile As String) As Parameter.PDCReceive
    Function SavePDCTrans(ByVal customclass As Parameter.PDCReceive, ByVal strFile As String) As String
    Function SavePDCEdit(ByVal oCustomClass As Parameter.PDCReceive, ByVal strFile As String) As String
    Function GetLastPDCDetail(ByVal oCustomClass As Parameter.PDCReceive, ByVal strFile As String) As Parameter.PDCReceive
    Function GetTablePDCEdit(ByVal oCustomClass As Parameter.PDCReceive, ByVal strFile As String) As Parameter.PDCReceive
    Function PrintReceiptNo(ByVal strConnection As String, ByVal PDCReceiptNo As String, ByVal BranchID As String) As DataSet
    Function GetPDCEditListMultiAgrementController(ByVal oCustomClass As Parameter.PDCReceive) As Parameter.PDCReceive

    Function SavePDCReceiveBp(ByVal strConnection As String, ByVal oCustomClass As Parameter.PDCReceive) As String
End Interface
