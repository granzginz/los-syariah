

Public Interface IPDCReceive
    ' Function AddPDCReceive(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
    Function ListPDCReceive(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
    Function GetHoliday(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive
    Function GetTablePDC(ByVal customclass As Parameter.PDCReceive, ByVal strFile As String) As Parameter.PDCReceive
    Function SavePDCReceive(ByVal customclass As Parameter.PDCReceive, ByVal strFile As String) As String
    Function PrintReceiptNo(ByVal strConnection As String, ByVal PDCReceiptNo As String, ByVal BranchID As String) As DataSet

    Function SavePDCReceiveBp(ByVal strConnection As String, ByVal oCustomClass As Parameter.PDCReceive) As String
End Interface
