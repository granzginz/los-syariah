
Public Interface IInstallmentDrawdown

    Function DrawdownApprovalList(ByVal customclass As Parameter.Drawdown) As Parameter.Drawdown
    Function GetCboUserAprPCAll(ByVal customclass As Parameter.Drawdown) As Parameter.Drawdown
    Function GetCboUserApproval(ByVal customclass As Parameter.Drawdown) As Parameter.Drawdown
    Function ApproveSaveToNextPerson(ByVal customclass As Parameter.Drawdown) As String
    Function ApproveSave(ByVal customclass As Parameter.Drawdown) As String
    Function ApproveisFinal(ByVal customclass As Parameter.Drawdown) As Parameter.Drawdown
    Function ApproveIsValidApproval(ByVal customclass As Parameter.Drawdown) As Parameter.Drawdown

End Interface
