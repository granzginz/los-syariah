﻿Public Interface IReferal
    Function GetReferal(ByVal oCustomClass As Parameter.Referal) As Parameter.Referal
    Function GetReferalEdit(ByVal oCustomClass As Parameter.Referal) As Parameter.Referal
    Function GetReferalReport(ByVal oCustomClass As Parameter.Referal) As Parameter.Referal
    Function GetSector(ByVal oCustomClass As Parameter.Referal) As Parameter.Referal

    Function ReferalSaveAdd(ByVal oCustomClass As Parameter.Referal) As String
    Sub ReferalSaveEdit(ByVal oCustomClass As Parameter.Referal)
    Function ReferalDelete(ByVal oCustomClass As Parameter.Referal) As String
End Interface