
Public Interface IAgreementMasterTransaction
    Function GetAgreementMasterTransaction(ByVal oCustomClass As Parameter.AgreementMasterTransaction) As Parameter.AgreementMasterTransaction
    Function GetAgreementMasterTransactionList(ByVal oCustomClass As Parameter.AgreementMasterTransaction) As Parameter.AgreementMasterTransaction
    Function AgreementMasterTransactionSaveAdd(ByVal oCustomClass As Parameter.AgreementMasterTransaction) As String
    Sub AgreementMasterTransactionSaveEdit(ByVal oCustomClass As Parameter.AgreementMasterTransaction)
    Function AgreementMasterTransactionDelete(ByVal oCustomClass As Parameter.AgreementMasterTransaction) As String
End Interface
