Public Interface IPaymentAllocation
    Function GetPaymentAllocation(ByVal oCustomClass As Parameter.PaymentAllocation) As Parameter.PaymentAllocation
    Function GetPaymentAllocationID(ByVal strConnnection As String, ByVal ID As String) As Parameter.PaymentAllocation
    Function GetPaymentAllocationReport(ByVal oCustomClass As Parameter.PaymentAllocation) As Parameter.PaymentAllocation

    Sub PaymentAllocationAdd(ByVal customClass As Parameter.PaymentAllocation)
    Sub PaymentAllocationEdt(ByVal customClass As Parameter.PaymentAllocation)
    Sub PaymentAllocationDelete(ByVal customClass As Parameter.PaymentAllocation)
End Interface
