
Public Interface IJournalScheme
    Function GetJournalScheme(ByVal customClass As Parameter.JournalScheme) As Parameter.JournalScheme
    Function GetJournalSchemeAddDtl(ByVal customClass As Parameter.JournalScheme) As Parameter.JournalScheme
    Function GetJournalSchemeEditHdr(ByVal customClass As Parameter.JournalScheme) As Parameter.JournalScheme
    Function GetJournalSchemeEditDtl(ByVal customClass As Parameter.JournalScheme) As Parameter.JournalScheme
    Function GetJournalSchemeCopyFrom(ByVal customClass As Parameter.JournalScheme) As Parameter.JournalScheme
    Function GetJournalSchemeReport(ByVal customClass As Parameter.JournalScheme) As Parameter.JournalScheme

    Function JournalSchemeSaveAdd(ByVal customClass As Parameter.JournalScheme) As String
    Sub JournalSchemeSaveEdit(ByVal customClass As Parameter.JournalScheme)
    Sub JournalSchemeDelete(ByVal customClass As Parameter.JournalScheme)
End Interface
