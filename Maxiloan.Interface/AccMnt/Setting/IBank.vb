
Public Interface IBank
    Function ListBankMaster(ByVal customclass As Parameter.BankMaster) As Parameter.BankMaster
    Function ListBankAccount(ByVal customclass As Parameter.BankAccount) As Parameter.BankAccount

    Function ReportBankMaster(ByVal customclass As Parameter.BankMaster) As Parameter.BankMaster
    Function ReportBankAccount(ByVal customclass As Parameter.BankAccount) As Parameter.BankAccount

    Function AddBankMaster(ByVal customclass As Parameter.BankMaster) As String
    Function UpdateBankMaster(ByVal customclass As Parameter.BankMaster) As String
    Function DeleteBankMaster(ByVal customclass As Parameter.BankMaster) As String

    Function AddBankAccount(ByVal customclass As Parameter.BankAccount, _
                                    ByVal oClassAddress As Parameter.Address, _
                                    ByVal oClassPersonal As Parameter.Personal) As String
    Function UpdateBankAccount(ByVal customclass As Parameter.BankAccount, _
                                    ByVal oClassAddress As Parameter.Address, _
                                    ByVal oClassPersonal As Parameter.Personal) As String
    Function UpdateBankAccountH(ByVal customclass As Parameter.BankAccount) As String

    Function DeleteBankAccount(ByVal customclass As Parameter.BankAccount) As String

    Function BankAccountInformasi(ByVal strconnection As String, ByVal strBankAccountID As String, ByVal strBranchid As String) As DataTable
    Function BankAccountListByType(ByVal oCustom As Parameter.BankAccount) As DataTable
End Interface
