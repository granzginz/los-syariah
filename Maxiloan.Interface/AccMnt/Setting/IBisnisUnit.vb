﻿Public Interface IBisnisUnit
    Function GetBisnisUnit(ByVal oCustomClass As Parameter.BisnisUnit) As Parameter.BisnisUnit
    Function GetBisnisUnitEdit(ByVal oCustomClass As Parameter.BisnisUnit) As Parameter.BisnisUnit
    Function GetBisnisUnitReport(ByVal oCustomClass As Parameter.BisnisUnit) As Parameter.BisnisUnit
    Function GetSector(ByVal oCustomClass As Parameter.BisnisUnit) As Parameter.BisnisUnit

    Function BisnisUnitSaveAdd(ByVal oCustomClass As Parameter.BisnisUnit) As String
    Sub BisnisUnitSaveEdit(ByVal oCustomClass As Parameter.BisnisUnit)
    Function BisnisUnitDelete(ByVal oCustomClass As Parameter.BisnisUnit) As String
End Interface