
Public Interface IBankBranchMaster
    Function GetBankBranchMaster(ByVal oCustomClass As Parameter.BankBranchMaster) As Parameter.BankBranchMaster
    Function GetBankBranchMasterReport(ByVal oCustomClass As Parameter.BankBranchMaster) As Parameter.BankBranchMaster    
    Function GetBankBranchMasterList(ByVal oCustomClass As Parameter.BankBranchMaster) As Parameter.BankBranchMaster
    Function BankBranchMasterSaveAdd(ByVal oCustomClass As Parameter.BankBranchMaster) As String
    Sub BankBranchMasterSaveEdit(ByVal oCustomClass As Parameter.BankBranchMaster)
    Function BankBranchMasterDelete(ByVal oCustomClass As Parameter.BankBranchMaster) As String
End Interface
