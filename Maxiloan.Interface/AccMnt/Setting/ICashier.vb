
Public Interface ICashier
    Function ListCashier(ByVal oCustomClass As Parameter.Cashier) As Parameter.Cashier
    Sub CashierAdd(ByVal oCustomClass As Parameter.Cashier)
    Sub CashierEdit(ByVal oCustomClass As Parameter.Cashier)
    Sub CashierDelete(ByVal oCustomClass As Parameter.Cashier)
    Function CashierReport(ByVal customclass As Parameter.Cashier) As Parameter.Cashier
End Interface
