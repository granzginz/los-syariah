

Public Interface IPaymentInfo
    Function GetPaymentInfo(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase
    Function GetFullPrepaymentInfo(ByVal oCustomClass As Parameter.FullPrepay) As Parameter.FullPrepay
    Function GetFullPrePaymentInfoNormal(ByVal customclass As Parameter.FullPrepay) As Parameter.FullPrepay
    Function GetFullPrePaymentInfoOL(ByVal oCustomClass As Parameter.FullPrepay) As Parameter.FullPrepay
    Function GetPaymentInfoOL(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase
    Function GetIsPaymentHaveOtor(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase
    Function GetPaymentInfoFactAndMDKJ(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase
    Function GetFullPrepaymentInfoFactAndMDKJ(ByVal oCustomClass As Parameter.FullPrepay) As Parameter.FullPrepay
    Function GetFullPrepaymentInfoView(ByVal oCustomClass As Parameter.FullPrepay) As Parameter.FullPrepay
End Interface
