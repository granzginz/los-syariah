
Public Interface ICheckCashier
    Function CheckCashier(ByVal strConnection As String, ByVal LoginID As String, ByVal BranchID As String, _
                            ByVal BusinessDate As Date) As Boolean
End Interface
