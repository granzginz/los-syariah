
Public Interface IAgreementList
    Function ListAgreement(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
    Function ListAgreementView(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
    Function ReportListAgreement(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
    Function PrepayRequestList(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
    Function AgreementIdentifikasiPembayaran(ByVal customclass As Parameter.AgreementList, ByVal dtTable As DataTable) As Parameter.AgreementList
    Function UcAgreementList(ByVal oCustomClass As Parameter.AccMntBase) As DataTable
    Function AgreementListDrawdown(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
    Function AgreementListDisburseFactoring(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
    Function ListAgreementFactoring(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
    Function ListAgreementFactoringView(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
    Function ListRCVPPH(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
    Function ListAgreementModalKerja(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
    Function ListAgreementModalKerjaView(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
    Function ListAgreementLinkAge(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
    Function UcAgreementListFactoring(ByVal oCustomClass As Parameter.AccMntBase) As DataTable
    Function UcAgreementListMdKj(ByVal oCustomClass As Parameter.AccMntBase) As DataTable
    Function UcAgreementListInv(ByVal oCustomClass As Parameter.AccMntBase) As DataTable
    'penambahan reversal factoring & modal kerja
    Function ListAgreementFACT(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
    Function ListAgreementMDKJ(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
    'penambahan alokasi prepaid
    Function AgreementListPrepaid(ByVal oCustomClass As Parameter.AgreementList) As Parameter.AgreementList
    Function PrepaidAllocation(ByVal oCustomClass2 As Parameter.InstallRcv) As Parameter.InstallRcv
    Function ListAgreementnew(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
End Interface
