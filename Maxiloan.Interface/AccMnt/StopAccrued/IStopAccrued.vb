
Public Interface IStopAccrued
    Sub StopAccruedRequest(ByVal oCustomClass As Parameter.StopAccrued)
    Sub StopAccruedReversal(ByVal oCustomClass As Parameter.StopAccrued)
    Function StopAccruedGetPastDueDays(ByVal oCustomClass As Parameter.StopAccrued) As Parameter.StopAccrued
    Function StopAccruedView(ByVal oCustomClass As Parameter.StopAccrued) As Parameter.StopAccrued
    Function StopAccruedInquiry(ByVal oCustomClass As Parameter.StopAccrued) As Parameter.StopAccrued
    Function StopAccruedReversalList(ByVal oCustomClass As Parameter.StopAccrued) As Parameter.StopAccrued
End Interface
