


Public Interface IRefundReq
    Function ListRefundReq(ByVal customclass As Parameter.Refund) As Parameter.Refund
    Function ListRefundReqCust(ByVal customclass As Parameter.Refund) As Parameter.Refund
    Function SaveRefundReq(ByVal customclass As Parameter.Refund) As Parameter.Refund
    Function SaveRefundReqH(ByVal customclass As Parameter.Refund) As Parameter.Refund

End Interface
