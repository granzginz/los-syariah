

Public Interface IRefundReqAdv

    '============= Request ==================
    Function ListRefundReqAdv(ByVal customclass As Parameter.Refund) As Parameter.Refund
    Function SelectedRefundReqAdv(ByVal customclass As Parameter.Refund) As Parameter.Refund
    Function GetProduct(ByVal customclass As Parameter.Refund) As Parameter.Refund
    'Function GetRequestNo(ByVal customclass As Parameter.Refund) As String
    Function SaveRefundReqAdv(ByVal customclass As Parameter.Refund, ByVal oData1 As DataTable) As Parameter.Refund

    '============= Inquiry ==================
    Function ListRefundInqAdv(ByVal customclass As Parameter.Refund) As Parameter.Refund
    Function ListRefundInqAdvByOSRefund(ByVal customclass As Parameter.Refund) As Parameter.Refund
    Function ListingReportByRefund(ByVal customclass As Parameter.Refund) As Parameter.Refund
    Function ListingReportByOSRefund(ByVal customclass As Parameter.Refund) As Parameter.Refund
    Function ViewRefundAdvance1(ByVal customclass As Parameter.Refund) As Parameter.Refund
    Function ViewRefundAdvance2(ByVal customclass As Parameter.Refund) As Parameter.Refund
    Function ViewRefundDetail(ByVal customclass As Parameter.Refund) As Parameter.Refund
End Interface
