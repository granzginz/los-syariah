

Public Interface IRefundInq
    Function ListRefundReport(ByVal customclass As Parameter.Refund) As Parameter.Refund
    Function ViewRefund(ByVal customclass As Parameter.Refund) As Parameter.Refund
    Function ViewRefundApproval(ByVal customclass As Parameter.Refund) As Parameter.Refund
End Interface
