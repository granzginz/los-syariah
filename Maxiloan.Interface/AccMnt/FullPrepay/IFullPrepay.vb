
Public Interface IFullPrepay
    Sub FullPrepayRequest(ByVal oCustomClass As Parameter.FullPrepay)
    Sub FullPrepayCancel(ByVal oCustomClass As Parameter.FullPrepay)
    Sub FullPrepayExecution(ByVal oCustomClass As Parameter.FullPrepay)
    Function FullPrepayInfo(ByVal oCustomClass As Parameter.FullPrepay) As Parameter.FullPrepay
    Function InqPrepayment(ByVal ocustomclass As Parameter.FullPrepay) As Parameter.FullPrepay
    Function PrintTrial(ByVal ocustomclass As Parameter.FullPrepay) As DataSet
    Function PrintTrialOL(ByVal ocustomclass As Parameter.FullPrepay) As DataSet
    Sub FullPrepayExecutionOL(ByVal oCustomClass As Parameter.FullPrepay)
    Sub FullPrepayRequestOL(ByVal oCustomClass As Parameter.FullPrepay)
End Interface
