
Public Interface IPaymentReversal
    Function PaymentReversalList(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal
    Function PaymentReversalListOtor(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal

    Sub ProcessPaymentReversal(ByVal oCustomClass As Parameter.PaymentReversal)
    Sub ProcessPaymentReversalOtor(ByVal oCustomClass As Parameter.PaymentReversal)
    Sub ProcessReversalOtorFactoring(ByVal oCustomClass As Parameter.PaymentReversal)
    Sub ProcessReversalOtorModalKerja(ByVal oCustomClass As Parameter.PaymentReversal)

    Function ReversalLogList(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal
    Sub ProcessPaymentReversalOL(ByVal oCustomClass As Parameter.PaymentReversal)
	Function PaymentReversalListOL(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal
	Function PaymentReversalListFactoring(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal
	Sub ProcessPaymentReversalFactoring(ByVal oCustomClass As Parameter.PaymentReversal)
	Function PaymentReversalListModalKerja(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal
    Sub ProcessPaymentReversalModalKerja(ByVal oCustomClass As Parameter.PaymentReversal)
    Sub ProcessReversalTolakanBGOtor(ByVal oCustomClass As Parameter.PaymentReversal)
End Interface
