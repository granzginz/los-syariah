
Public Interface IDisburseFactoring

    Function DisburseApprovalList(ByVal customclass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    Function GetCboUserAprPCAll(ByVal customclass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    Function GetCboUserApproval(ByVal customclass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    Function ApproveSaveToNextPerson(ByVal customclass As Parameter.FactoringInvoice) As String
    Function ApproveSave(ByVal customclass As Parameter.FactoringInvoice) As String
    Function ApproveisFinal(ByVal customclass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    Function ApproveIsValidApproval(ByVal customclass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice

End Interface
