
Public Interface IInvoiceOL
    Function CetakInvoiceOLPaging(ByVal customClass As Parameter.InvoiceOL) As Parameter.InvoiceOL
    Function CetakInvoiceOLDetail(ByVal customClass As Parameter.InvoiceOL) As DataTable
    Function CetakInvoiceOLSave(ByVal oCustomClass As Parameter.InvoiceOL) As String
    Function ReportKwitansi(ByVal customClass As Parameter.InvoiceOL) As DataTable
    Function InvoiceOLPaging(ByVal customClass As Parameter.InvoiceOL) As Parameter.InvoiceOL
    Function InvoiceOLDetail(ByVal customClass As Parameter.InvoiceOL) As DataTable
    Function InvoiceOLExecute(ByVal oCustomClass As Parameter.InvoiceOL) As String
    Function GetInstallRCVPrint(ByVal customClass As Parameter.InvoiceOL) As DataTable
End Interface
