
Public Interface IInstallRcv
    Function InstallmentReceiveAdvIns(ByVal oCustomClass As Parameter.InstallRcv) As Integer
    Function InstallmentReceive(ByVal oCustomClass As Parameter.InstallRcv) As Integer
    Function IsMaxBackDate(ByVal strConnection As String, ByVal ValueDate As Date, ByVal BusinessDate As Date) As Boolean
    Function IsValidLastPayment(ByVal strConnection As String, ByVal ApplicationID As String, ByVal ValueDate As Date) As Boolean
    Overloads Function PrintTTU(ByVal strConnection As String, ByVal ApplicationID As String, ByVal BranchID As String, ByVal BusinessDate As Date, ByVal LoginID As String) As DataSet
    Overloads Function PrintTTU(ByVal strConnection As String, ByVal ApplicationID As String, ByVal branchID As String, ByVal BusinessDate As Date, ByVal HistorySequenceNo As Integer, ByVal LoginID As String) As DataSet
    Overloads Function PrintTTU(ByVal strConnection As String, _
                            ByVal ApplicationID As String, _
                            ByVal BranchID As String, _
                            ByVal BusinessDate As Date, ByVal HistorySequenceNo As Integer, ByVal LoginID As String, ByVal InsSeqNo As Integer) As DataSet
    Function IsLastPayment(ByVal oCustomClass As Parameter.InstallRcv) As Boolean
    Function ListKwitansiInstallment(ByVal oCustomClass As Parameter.InstallRcv) As DataSet
    Function SavePrintKwitansiInstallment(ByVal oCustomClass As Parameter.InstallRcv) As Parameter.InstallRcv
    Function ReportKwitansiInstallment(ByVal oCustomClass As Parameter.InstallRcv) As DataSet
    Function PrintInstallmentReceive(ByVal strConnection As String, ByVal ApplicationID As String, ByVal BranchID As String, ByVal BusinessDate As Date, ByVal LoginID As String) As DataSet
    Function ReportKwitansiInstallment2(ByVal oCustomClass As Parameter.InstallRcv) As DataSet
    Function ReportKwitansiInstallmentWithVoucherNo(ByVal customclass As Parameter.InstallRcv) As DataSet
    Function InstallRcvInstallmentSchedulePaging(ByVal oCustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging

    Function AdvanceInstallmet(ByVal CustomCLass As Parameter.InstallRcv) As Parameter.InstallRcv
    Function AlokasiAdvanceInstallmet(ByVal CustomClass As Parameter.InstallRcv) As String

    Function GetSP(ByVal customclass As Parameter.InstallRcv) As Parameter.InstallRcv
    Function ReportKwitansiInstallmentAdvIns(ByVal oCustomClass As Parameter.InstallRcv) As DataSet
    Function InstallRCVKorAngsSave(ByVal oCustomClass As Parameter.InstallRcv) As Integer
    Function InstallRCVTemp(ByVal oCustomClass As Parameter.InstallRcv) As Integer
    Function TransaksiPending(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
    Function DetailTransaction(ByVal customClass As Parameter.InstallRcv) As DataTable

    Function InstallRCVBATemp(ByVal oCustomClass As Parameter.InstallRcv) As Integer
    Function InstallRCVBAFactoring(ByVal oCustomClass As Parameter.InstallRcv) As Integer
    Function InstallRCVPPH(ByVal oCustomClass As Parameter.InstallRcv) As Integer
    Sub SaveOtorisasiPPH(ByVal oCustomClass As Parameter.InstallRcv)
    Sub RejectOtorisasiPPH(ByVal oCustomClass As Parameter.InstallRcv)

    Function InstallRCVBAModalKerja(ByVal oCustomClass As Parameter.InstallRcv) As Integer
    Function GetGeneralPaging(ByVal CustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
    Sub SaveOtorisasi(ByVal oCustomClass As Parameter.InstallRcv)
    Sub RejectOtorisasi(ByVal oCustomClass As Parameter.InstallRcv)
    Function InstallRCVCollOtor(ByVal oCustomClass As Parameter.InstallRcv) As Integer
    Sub EditOtorisasi(ByVal oCustomClass As Parameter.InstallRcv)


    Function DoUpload3rdFile(cnn As String, rows As Parameter.InstallRcv3rd, sesId As String) As String
    Function GetUploadFilePage(cnn As String, currentPage As Integer, pageSize As Integer, sesId As String, vtype As Parameter.EnViaType) As Parameter.InstallRcv3rd
    Function GetUploadFilePage(cnn As String, currentPage As Integer, pageSize As Integer, trDate As DateTime, vtype As Parameter.EnViaType) As Parameter.InstallRcv3rd
    Function PostingInstallmentUpload(cnn As String, compid As String, branchid As String, valuedate As DateTime, bsnDate As DateTime, login As String, vtype As Parameter.EnViaType) As String
    Function GeneratePaymentGatewayFile(cnn As String, processDate As DateTime, dir As String) As Boolean
    Function GetPaymentGatewayFile(cnn As String, processDate As DateTime) As DataTable
    Function DoPostingInstallmentUpload(cnn As String, ho As String, processDate As DateTime, businessDate As DateTime, companyId As String, vtype As Parameter.EnViaType, listKey As IList(Of String)) As String

    Function DoUploadAgreementVirtualAccount(cnn As String, rows As Parameter.AgreementVirtualAcc) As String
    Function InstallmentReceiveMobile(ByVal oCustomClass As Parameter.InstallRcv) As Integer
    Function GetComboDepartemen(ByVal oCustomClass As Parameter.InstallRcv) As DataTable
    Function GetComboCabang(ByVal oCustomClass As Parameter.InstallRcv) As DataTable
    Sub AlokasiPembNonARSaveEdit(ByVal oCustomClass As Parameter.InstallRcv)

    Function InstallKoreksiModalKerja(ByVal oCustomClass As Parameter.InstallRcv) As Integer
    Function InstallKoreksiFactoring(ByVal oCustomClass As Parameter.InstallRcv) As Integer
    Sub AlokasiPembTolakanBGSaveEdit(ByVal oCustomClass As Parameter.InstallRcv)
End Interface
