
Public Interface IRescheduling
    Function SaveReschedulingData(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable,
          ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.FinancialData
    Function GetMinDueDate(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function ViewRescheduling(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function ViewReschedulingApproval(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Sub ReschedulingCancel(ByVal ocustomClass As Parameter.FinancialData)
    Sub ReschedulingExecute(ByVal ocustomClass As Parameter.FinancialData)
    Function ReschedulingPrintTrial(ByVal oCustomClass As Parameter.FinancialData) As System.Data.DataSet
    Function ReschedulingProduct(ByVal strConnection As String, ByVal BranchId As String) As DataTable
    Function SaveReschedulingIRR(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable,
            ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.FinancialData
    Function ViewReschedulingIRR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function SaveReschedulingStepUpStepDown(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable,
               ByVal oData2 As DataTable, ByVal oData3 As DataTable, ByVal oData4 As DataTable) As Parameter.FinancialData
    Function SaveReschedulingEP(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable,
            ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.FinancialData
    Function GetEffectiveDate(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function SaveReschedulingStepUpStepDownFactAndMDKJ(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable,
               ByVal oData2 As DataTable, ByVal oData3 As DataTable, ByVal oData4 As DataTable) As Parameter.FinancialData
    Function ViewRestrcutFactAndMU(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Function ViewApprovalRestrcutFactAndMU(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Sub RestructFactAndMUExecute(ByVal ocustomClass As Parameter.FinancialData)
    Function GetMinDueDateFactAndMDKJ(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
    Sub ReschedulingCancelFactAndMDKJ(ByVal ocustomClass As Parameter.FinancialData)
End Interface


