
Public Interface IPaymentHistory
    Function ListPaymentHistory(ByVal customclass As Parameter.PaymentHistory) As Parameter.PaymentHistory
	Function ListPaymentHistoryDetail(ByVal customclass As Parameter.PaymentHistory) As Parameter.PaymentHistory
    Function ListPaymentHistoryDetailOtor(ByVal customclass As Parameter.PaymentHistory) As Parameter.PaymentHistory
	Function ListPaymentHistoryDetailFactoring(ByVal customclass As Parameter.PaymentHistory) As Parameter.PaymentHistory
	Function ListPaymentHistoryDetailModalKerja(ByVal customclass As Parameter.PaymentHistory) As Parameter.PaymentHistory
    Function ReversalTransaksiLainnyaSave(ByVal m_customclass As Parameter.PaymentHistory) As Parameter.PaymentHistory
    Function ReversalPembayaranTDPSave(ByVal m_customclass As Parameter.PaymentHistory) As Parameter.PaymentHistory
End Interface
