
Public Interface ISuspendAllocationGeneral

    Function GetSuspendAllocationGeneral(ByVal oCustomClass As Parameter.SuspendAllocationGeneral) As Parameter.SuspendAllocationGeneral
    Function GetSuspendAllocationGeneralList(ByVal oCustomClass As Parameter.SuspendAllocationGeneral) As Parameter.SuspendAllocationGeneral
    Sub SuspendAllocationGeneralSaveEdit(ByVal oCustomClass As Parameter.SuspendAllocationGeneral)
    Function GetComboDepartemen(ByVal oCustomClass As Parameter.SuspendAllocationGeneral) As DataTable
    Function GetComboCabang(ByVal oCustomClass As Parameter.SuspendAllocationGeneral) As DataTable
    Function GetIsBlock(ByVal oCustomClass As Parameter.SuspendAllocationGeneral) As Parameter.SuspendAllocationGeneral
    Function GetSuspendIsBlockList(ByVal oCustomClass As Parameter.SuspendAllocationGeneral) As Parameter.SuspendAllocationGeneral
    Sub ReleaseBlokirSuspend(ByVal oCustomClass As Parameter.SuspendAllocationGeneral)
End Interface
