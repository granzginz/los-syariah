

Public Interface ISuspendAllocation
    Sub SuspendAllocationSave(ByVal oCustomClass As Parameter.InstallRcv)
    Function SuspendDetail(oCustom As Parameter.SuspendReceive) As Parameter.SuspendReceive
    Sub SuspendSplit(oCustomClass As Parameter.SuspendReceive)
    'penambahan alokasi suspend fact & mdkj
    Sub SuspendAllocationSaveFACT(ByVal oCustomClass As Parameter.InstallRcv)
    Sub SuspendAllocationSaveMDKJ(ByVal oCustomClass As Parameter.InstallRcv)
    Function TitipanDetail(oCustom As Parameter.SuspendReceive) As Parameter.SuspendReceive
    Sub TitipanSplit(oCustomClass As Parameter.SuspendReceive)
End Interface
