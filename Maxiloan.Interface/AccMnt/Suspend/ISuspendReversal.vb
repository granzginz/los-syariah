
Public Interface ISuspendReversal
    Function SuspendReversalList(ByVal customclass As Parameter.SuspendReceive) As Parameter.SuspendReceive
    Function SuspendReverse(ByVal customclass As Parameter.SuspendReceive) As Parameter.SuspendReceive
    Sub SaveSuspendReverse(ByVal customclass As Parameter.SuspendReceive)
    Function SplitTitipanList(ByVal customclass As Parameter.SuspendReceive) As Parameter.SuspendReceive
End Interface
