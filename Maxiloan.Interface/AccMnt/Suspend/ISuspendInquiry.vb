

Public Interface ISuspendInquiry
    Function SuspendInquiryList(ByVal customclass As Parameter.SuspendReceive) As Parameter.SuspendReceive
    Function SuspendInqReport(ByVal customclass As Parameter.SuspendReceive) As Parameter.SuspendReceive
    Function GetBankAccountAll(ByVal oCustomClass As Parameter.SuspendReceive) As DataTable
End Interface
