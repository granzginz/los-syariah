

Public Interface ISuspendReceive
    Sub SaveSuspendReceive(ByVal customclass As Parameter.SuspendReceive)
    Sub SaveSuspendTransactionImplementasi(ByVal customclass As Parameter.SuspendReceive)
    Sub SaveSuspendOtorisasi(ByVal customclass As Parameter.SuspendReceive)
    Sub SaveReversalOtorisasi(ByVal customclass As Parameter.SuspendReceive)
End Interface
