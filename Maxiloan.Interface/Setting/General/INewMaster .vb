
Public Interface INewMaster
    Function GetMaster(ByVal customClass As Parameter.NewMaster) As Parameter.NewMaster
    Function GetMasterReport(ByVal customClass As Parameter.NewMaster) As Parameter.NewMaster
    Function GetKeyWord(ByVal customClass As Parameter.NewMaster) As Parameter.NewMaster

    Function MasterSaveAdd(ByVal customClass As Parameter.NewMaster) As String
    Sub MasterSaveEdit(ByVal customClass As Parameter.NewMaster)
    Function MasterDelete(ByVal customClass As Parameter.NewMaster) As String
End Interface
