

Public Interface IEmployeePosition
    Function EmployeePositionList(ByVal oEmpPos As Parameter.EmployeePosition) As Parameter.EmployeePosition
    Function EmployeePositionReport(ByVal oEmpPos As Parameter.EmployeePosition) As DataTable
    Sub EmployeePositionAdd(ByVal oEmpPos As Parameter.EmployeePosition)
    Sub EmployeePositionEdit(ByVal oEmpPos As Parameter.EmployeePosition)
    Sub EmployeePositionDelete(ByVal oEmpPos As Parameter.EmployeePosition)
End Interface
