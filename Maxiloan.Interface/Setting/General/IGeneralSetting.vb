

Public Interface IGeneralSetting
    Function GetGeneralSetting(ByVal customClass As Parameter.GeneralSetting) As Parameter.GeneralSetting
    Function GeneralSettingSaveEdit(ByVal customClass As Parameter.GeneralSetting) As Boolean
    Function GetGeneralSettingReport(ByVal customClass As Parameter.GeneralSetting) As Parameter.GeneralSetting
    Function GetGeneralSettingByID(ByVal oCustomClass As Parameter.GeneralSetting) As Parameter.GeneralSetting
    Function GetGeneralSettingValue(oClass As Parameter.GeneralSetting) As String
End Interface

