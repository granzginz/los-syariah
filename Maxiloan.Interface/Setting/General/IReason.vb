
Public Interface IReason
    Sub ReasonAdd(ByVal customclass As Parameter.Reason)
    Sub ReasonEdit(ByVal customclass As Parameter.Reason)
    Sub ReasonDelete(ByVal customclass As Parameter.Reason)
    Function ReasonView(ByVal customclass As Parameter.Reason) As Parameter.Reason
    Function GetReasonTypeCombo(ByVal customclass As Parameter.Reason) As DataTable
End Interface
