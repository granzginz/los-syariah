
Public Interface IVendor
    Function GetVendor(ByVal oCustomClass As Parameter.Vendor) As Parameter.Vendor
    Function GetVendorList(ByVal oCustomClass As Parameter.Vendor) As Parameter.Vendor
    Function VendorSaveAdd(ByVal oCustomClass As Parameter.Vendor) As String
    Sub VendorSaveEdit(ByVal oCustomClass As Parameter.Vendor)
    Function VendorDelete(ByVal oCustomClass As Parameter.Vendor) As String
End Interface
