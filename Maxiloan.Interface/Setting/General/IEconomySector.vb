
Public Interface IEconomySector
    Function GetEconomySector(ByVal customClass As Parameter.EconomySector) As Parameter.EconomySector
    Function GetEconomySectorReport(ByVal customClass As Parameter.EconomySector) As Parameter.EconomySector

    Function EconomySectorSaveAdd(ByVal customClass As Parameter.EconomySector) As String
    Sub EconomySectorSaveEdit(ByVal customClass As Parameter.EconomySector)
    Function EconomySectorDelete(ByVal customClass As Parameter.EconomySector) As String
End Interface
