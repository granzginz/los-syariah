
Public Interface INegara
    Function GetNegara(ByVal oCustomClass As Parameter.Negara) As Parameter.Negara
    Function GetNegaraList(ByVal oCustomClass As Parameter.Negara) As Parameter.Negara
    Function GetNegaraCombo(ByVal customclass As Parameter.Negara) As DataTable
End Interface
