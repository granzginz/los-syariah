
Public Interface IDatiII
    Function GetDati(ByVal oCustomClass As Parameter.Dati) As Parameter.Dati
    Function GetDatiReport(ByVal oCustomClass As Parameter.Dati) As Parameter.Dati
    Function GetDatiList(ByVal oCustomClass As Parameter.Dati) As Parameter.Dati
    Function DatiSaveAdd(ByVal oCustomClass As Parameter.Dati) As String
    Sub DatiSaveEdit(ByVal oCustomClass As Parameter.Dati)
    Function DatiDelete(ByVal oCustomClass As Parameter.Dati) As String
End Interface
