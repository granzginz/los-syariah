
Public Interface IZipCode
    Function GetMasterZipCode(ByVal oMasterZipCode As Parameter.MasterZipCode) As Parameter.MasterZipCode
    Function GetZipCodeIdReport(ByVal customClass As Parameter.MasterZipCode) As Parameter.MasterZipCode
    Function MasterZipCodeAdd(ByVal customClass As Parameter.MasterZipCode) As String
    Function MasterZipCodeDelete(ByVal customClass As Parameter.MasterZipCode) As String
End Interface