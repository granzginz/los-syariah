﻿
Public Interface IMailType
    Function GetMailType(ByVal oCustomClass As Parameter.MailType) As Parameter.MailType
    Function GetMailTypeEdit(ByVal oCustomClass As Parameter.MailType) As Parameter.MailType
    Function GetMailTypeReport(ByVal oCustomClass As Parameter.MailType) As Parameter.MailType
    Function GetSector(ByVal oCustomClass As Parameter.MailType) As Parameter.MailType

    Function MailTypeSaveAdd(ByVal oCustomClass As Parameter.MailType) As String
    Sub MailTypeSaveEdit(ByVal oCustomClass As Parameter.MailType)
    Function MailTypeDelete(ByVal oCustomClass As Parameter.MailType) As String
End Interface
