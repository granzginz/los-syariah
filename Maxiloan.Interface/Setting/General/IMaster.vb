
Public Interface IMaster
    Function GetMaster(ByVal customClass As Parameter.Master) As Parameter.Master
    Function GetMasterReport(ByVal customClass As Parameter.Master) As Parameter.Master
    Function GetKeyWord(ByVal customClass As Parameter.Master) As Parameter.Master

    Function MasterSaveAdd(ByVal customClass As Parameter.Master) As String
    Sub MasterSaveEdit(ByVal customClass As Parameter.Master)
    Function MasterDelete(ByVal customClass As Parameter.Master) As String
End Interface
