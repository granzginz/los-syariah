
Public Interface IPropinsi
    Function GetPropinsi(ByVal oCustomClass As Parameter.Propinsi) As Parameter.Propinsi
    Function GetPropinsiList(ByVal oCustomClass As Parameter.Propinsi) As Parameter.Propinsi
    Function GetPropinsiCombo(ByVal customclass As Parameter.Propinsi) As DataTable
    Function ProvinceSaveAdd(ByVal ocustomClass As Parameter.Propinsi) As String
    Sub ProvinceSaveEdit(ByVal ocustomClass As Parameter.Propinsi)
End Interface
