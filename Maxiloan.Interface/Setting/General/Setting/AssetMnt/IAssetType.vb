
Public Interface IAssetType
#Region "AssetType"
    Function GetAssetType(ByVal customClass As Parameter.AssetType) As Parameter.AssetType
    Function GetAssetTypeEdit(ByVal customClass As Parameter.AssetType) As Parameter.AssetType
    Function GetAssetTypeReport(ByVal customClass As Parameter.AssetType) As Parameter.AssetType
    Function AssetTypeSaveAdd(ByVal customClass As Parameter.AssetType) As String
    Function AssetTypeDelete(ByVal customClass As Parameter.AssetType) As String
    Sub AssetTypeSaveEdit(ByVal customClass As Parameter.AssetType)

#End Region

#Region "AssetTypeAttribute"
    Function GetAssetTypeAttribute(ByVal customClass As Parameter.AssetTypeAttribute) As Parameter.AssetTypeAttribute
    Function GetAssetTypeAttributeEdit(ByVal customClass As Parameter.AssetTypeAttribute) As Parameter.AssetTypeAttribute
    Function GetAssetTypeAttributeReport(ByVal customClass As Parameter.AssetTypeAttribute) As Parameter.AssetTypeAttribute
    Function AssetTypeAttributeSaveAdd(ByVal customClass As Parameter.AssetTypeAttribute) As String
    Sub AssetTypeAttributeSaveEdit(ByVal customClass As Parameter.AssetTypeAttribute)
    Function AssetTypeAttributeDelete(ByVal customClass As Parameter.AssetTypeAttribute) As String
#End Region

#Region "AssetTypeCategory"
    Function GetAssetTypeCategory(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory
    Function GetAssetTypeCategoryEdit(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory
    Function GetAssetTypeCategoryInsRate(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory
    Function GetAssetTypeCategoryReport(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory
    Function AssetTypeCategorySaveAdd(ByVal customClass As Parameter.AssetTypeCategory) As String
    Sub AssetTypeCategorySaveEdit(ByVal customClass As Parameter.AssetTypeCategory)
    Function AssetTypeCategoryDelete(ByVal customClass As Parameter.AssetTypeCategory) As String
    Function GetAssetTypeCategoryInsRateforSpecificAssetType(ByVal customClass As Parameter.AssetTypeCategory) As Parameter.AssetTypeCategory
#End Region

#Region "AssetTypeDocumentList"
    Function GetAssetTypeDocumentList(ByVal customClass As Parameter.AssetTypeDocumentList) As Parameter.AssetTypeDocumentList
    Function GetAssetTypeDocumentListEdit(ByVal customClass As Parameter.AssetTypeDocumentList) As Parameter.AssetTypeDocumentList
    Function GetAssetTypeDocumentListReport(ByVal customClass As Parameter.AssetTypeDocumentList) As Parameter.AssetTypeDocumentList
    Function AssetTypeDocumentListSaveAdd(ByVal customClass As Parameter.AssetTypeDocumentList) As String
    Function AssetTypeDocumentListSaveEdit(ByVal customClass As Parameter.AssetTypeDocumentList) As String
    Function AssetTypeDocumentListDelete(ByVal customClass As Parameter.AssetTypeDocumentList) As String
#End Region

#Region "AssetTypeOrigination"
    Function GetAssetTypeOrigination(ByVal customClass As Parameter.AssetTypeOrigination) As Parameter.AssetTypeOrigination
    Function GetAssetTypeOriginationEdit(ByVal customClass As Parameter.AssetTypeOrigination) As Parameter.AssetTypeOrigination
    Function GetAssetTypeOriginationReport(ByVal customClass As Parameter.AssetTypeOrigination) As Parameter.AssetTypeOrigination
    Function AssetTypeOriginationSaveAdd(ByVal customClass As Parameter.AssetTypeOrigination) As String
    Sub AssetTypeOriginationSaveEdit(ByVal customClass As Parameter.AssetTypeOrigination)
    Function AssetTypeOriginationDelete(ByVal customClass As Parameter.AssetTypeOrigination) As String
#End Region

#Region "AssetTypeScheme"
    Function GetAssetTypeScheme(ByVal customClass As Parameter.AssetTypeScheme) As Parameter.AssetTypeScheme
    Function GetAssetTypeSchemeEdit(ByVal customClass As Parameter.AssetTypeScheme) As Parameter.AssetTypeScheme
    Function GetAssetTypeSchemeReport(ByVal customClass As Parameter.AssetTypeScheme) As Parameter.AssetTypeScheme
    Function AssetTypeSchemeSaveAdd(ByVal customClass As Parameter.AssetTypeScheme) As String
    Sub AssetTypeSchemeSaveEdit(ByVal customClass As Parameter.AssetTypeScheme)
    Function AssetTypeSchemeDelete(ByVal customClass As Parameter.AssetTypeScheme) As String
    Function AssetTypeSchemeAdd(ByVal customClass As Parameter.AssetTypeScheme) As String
#End Region

#Region "AssetMaster"
    Function GetMaxLevel(ByVal strAssetTypeID As String, ByVal strConnection As String) As Integer
#End Region

End Interface
