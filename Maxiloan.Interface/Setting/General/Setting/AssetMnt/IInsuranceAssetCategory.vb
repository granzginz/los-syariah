

Public Interface IInsuranceAssetCategory
    Function GetInsuranceAssetCategory(ByVal oInsuranceAssetCategory As Parameter.InsuranceAssetCategory) As Parameter.InsuranceAssetCategory
    Function GetInsuranceAssetCategoryReport(ByVal customClass As Parameter.InsuranceAssetCategory) As Parameter.InsuranceAssetCategory
    Function InsuranceAssetCategoryAdd(ByVal customClass As Parameter.InsuranceAssetCategory) As String
    Function InsuranceAssetCategoryUpdate(ByVal customClass As Parameter.InsuranceAssetCategory) As String
    Function InsuranceAssetCategoryDelete(ByVal customClass As Parameter.InsuranceAssetCategory) As String
End Interface

