
Public Interface IAssetMasterPrice
    Function GetAssetMasterPrice(ByVal oCustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
    Function GetAssetMaster(ByVal oCustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
    Function GetAssetMasterPriceReport(ByVal oCustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
    Function GetAssetMasterPriceList(ByVal oCustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
    Function AssetMasterPriceSaveAdd(ByVal oCustomClass As Parameter.AssetMasterPrice) As String
    Sub AssetMasterPriceSaveEdit(ByVal oCustomClass As Parameter.AssetMasterPrice)
    Function AssetMasterPriceDelete(ByVal oCustomClass As Parameter.AssetMasterPrice) As String
    Function GetAssetTypeCombo(ByVal oCustomClass As Parameter.AssetMasterPrice) As DataTable
    Function GetAssetCodeCombo(ByVal oCustomClass As Parameter.AssetMasterPrice) As DataTable
    Function GetAllAssetCode(ByVal oCustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
    Function GetAssetMasterByAssetCode(ByVal oCustomClass As Parameter.AssetMasterPrice) As Parameter.AssetMasterPrice
End Interface
