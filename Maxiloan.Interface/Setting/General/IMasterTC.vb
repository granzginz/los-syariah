

Public Interface IMasterTC

    Function MasterTCList(ByVal oTC As Parameter.MasterTC) As Parameter.MasterTC
    Function MasterTCByID(ByVal oTC As Parameter.MasterTC) As Parameter.MasterTC
    Function MasterTCReport(ByVal oTC As Parameter.MasterTC) As Parameter.MasterTC

    Sub MasterTCAdd(ByVal oTC As Parameter.MasterTC)
    Sub MasterTCEdit(ByVal oTC As Parameter.MasterTC)
    Sub MasterTCDelete(ByVal oTC As Parameter.MasterTC)


    Function TCCLList(ByVal oTC As Parameter.MasterTC) As Parameter.MasterTC
    Function TCCLByID(ByVal oTC As Parameter.MasterTC) As Parameter.MasterTC
    Function TCCLReport(ByVal oTC As Parameter.MasterTC) As Parameter.MasterTC

    Sub TCCLAdd(ByVal oTC As Parameter.MasterTC)
    Sub TCCLEdit(ByVal oTC As Parameter.MasterTC)
    Sub TCCLDelete(ByVal oTC As Parameter.MasterTC)


End Interface
