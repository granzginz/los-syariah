
Public Interface IIndustryType
    Function GetIndustryType(ByVal oCustomClass As Parameter.IndustryType) As Parameter.IndustryType
    Function GetIndustryTypeEdit(ByVal oCustomClass As Parameter.IndustryType) As Parameter.IndustryType
    Function GetIndustryTypeReport(ByVal oCustomClass As Parameter.IndustryType) As Parameter.IndustryType
    Function GetSector(ByVal oCustomClass As Parameter.IndustryType) As Parameter.IndustryType

    Function IndustryTypeSaveAdd(ByVal oCustomClass As Parameter.IndustryType) As String
    Sub IndustryTypeSaveEdit(ByVal oCustomClass As Parameter.IndustryType)
    Function IndustryTypeDelete(ByVal oCustomClass As Parameter.IndustryType) As String
End Interface
