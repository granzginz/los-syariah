﻿Public Interface IAuthorizedSigner

    Function GetAuthorizedSigner(ByVal oCustomClass As Parameter.AuthorizedSigner) As Parameter.AuthorizedSigner
    Function GetAuthorizedSignerList(ByVal oCustomClass As Parameter.AuthorizedSigner) As Parameter.AuthorizedSigner
    Function AuthorizedSignerSaveAdd(ByVal oCustomClass As Parameter.AuthorizedSigner) As String
    Sub AuthorizedSignerSaveEdit(ByVal oCustomClass As Parameter.AuthorizedSigner)
    Function AuthorizedSignerDelete(ByVal oCustomClass As Parameter.AuthorizedSigner) As String

End Interface


