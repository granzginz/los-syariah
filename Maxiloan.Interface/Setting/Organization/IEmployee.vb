

Public Interface IEmployee

    Function EmployeeList(ByVal oEmployee As Parameter.Employee) As Parameter.Employee
    Function EmployeeByID(ByVal oEmployee As Parameter.Employee) As Parameter.Employee
    Function EmployeeReport(ByVal oEmployee As Parameter.Employee) As DataTable

    Function GetCombo(ByVal strCon As String, ByVal strCombo As String, ByVal BranchID As String) As DataTable

    Sub EmployeeAdd(ByVal oEmployee As Parameter.Employee, ByVal oAddress As Parameter.Address, ByVal oPerson As Parameter.Personal)
    Sub EmployeeEdit(ByVal oEmployee As Parameter.Employee, ByVal oAddress As Parameter.Address, ByVal oPerson As Parameter.Personal)
    Sub EmployeeDelete(ByVal oEmployee As Parameter.Employee)
    Sub EmployeePindahCabang(ByVal oEmployee As Parameter.Employee)
End Interface
