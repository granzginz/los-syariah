

Public Interface IArea
    Function ListArea(ByVal customclass As Parameter.Area) As Parameter.Area
    Function SaveArea(ByVal customclass As Parameter.Area) As String
    Function UpdateArea(ByVal customclass As Parameter.Area) As String
    Function DeleteArea(ByVal customclass As Parameter.Area) As String
    Function AreaInfo(ByVal customclass As Parameter.Area) As Parameter.Area
    Function ReportArea(ByVal customclass As Parameter.Area) As Parameter.Area
End Interface
