

Public Interface ICompany
    Function CompanyList(ByVal co As Parameter.Company) As Parameter.Company
    Function CompanyListByID(ByVal co As Parameter.Company) As Parameter.Company

    Function CompanyAdd(ByVal customClass As Parameter.Company, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As String
    Sub CompanyEdit(ByVal customClass As Parameter.Company, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
    Sub CompanyDelete(ByVal customclass As Parameter.Company)
    Function CompanyReport(ByVal co As Parameter.Company) As Parameter.Company

    Function CompanyBODList(ByVal co As Parameter.CompanyBOD) As Parameter.CompanyBOD
    Function CompanyBODListByID(ByVal co As Parameter.CompanyBOD) As Parameter.CompanyBOD
    Function GetComboInputType(ByVal strcon As String) As DataTable

    Function CompanyBODAdd(ByVal customClass As Parameter.CompanyBOD, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As String
    Sub CompanyBODEdit(ByVal customClass As Parameter.CompanyBOD, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
    Sub CompanyBODDelete(ByVal customclass As Parameter.CompanyBOD)
    Function CompanyBODReport(ByVal co As Parameter.CompanyBOD) As Parameter.CompanyBOD


    Function CompanyCommisionerList(ByVal co As Parameter.CompanyCommisioner) As Parameter.CompanyCommisioner
    Function CompanyCommisionerListByID(ByVal co As Parameter.CompanyCommisioner) As Parameter.CompanyCommisioner

    Function CompanyCommisionerAdd(ByVal customClass As Parameter.CompanyCommisioner, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As String
    Sub CompanyCommisionerEdit(ByVal customClass As Parameter.CompanyCommisioner, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
    Sub CompanyCommisionerDelete(ByVal customclass As Parameter.CompanyCommisioner)
    Function CompanyCommisionerReport(ByVal co As Parameter.CompanyCommisioner) As Parameter.CompanyCommisioner

End Interface
