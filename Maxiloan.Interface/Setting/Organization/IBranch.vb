Public Interface IBranch
    Function BranchList(ByVal oBranch As Parameter.Branch) As Parameter.Branch
    Function BranchByID(ByVal oBranch As Parameter.Branch) As Parameter.Branch
    Function BranchReport(ByVal oBranch As Parameter.Branch) As Parameter.Branch

    Sub BranchAdd(ByVal obranch As Parameter.Branch, ByVal oAddress As Parameter.Address, ByVal operson As Parameter.Personal)
    Sub BranchEdit(ByVal obranch As Parameter.Branch, ByVal oAddress As Parameter.Address, ByVal operson As Parameter.Personal)
    Sub BranchDelete(ByVal obranch As Parameter.Branch)
    Function BranchBudgetListing(ByVal customclass As Parameter.Branch) As Parameter.Branch
    Function BranchBudgetPrint(ByVal customclass As Parameter.Branch) As Parameter.Branch
    Function BranchForecastListing(ByVal customclass As Parameter.Branch) As Parameter.Branch
    Function BranchForecastPrint(ByVal customclass As Parameter.Branch) As Parameter.Branch
    Function GetCombo(ByVal strcon As String, ByVal strCombo As String) As DataTable
End Interface
