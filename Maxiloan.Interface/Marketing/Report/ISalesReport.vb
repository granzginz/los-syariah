

Public Interface ISalesReport
#Region "RptDailySalesByAO"
    Function GetAOSupervisor(ByVal customclass As Parameter.Sales) As DataTable
    Function GetAO(ByVal customclass As Parameter.Sales) As DataTable
    Function ViewRptDailySalesByAO(ByVal customclass As Parameter.Sales) As Parameter.Sales
#End Region
#Region "RptMonthlySalesByBranch"
    Function GetArea(ByVal ocustomclass As Parameter.Sales) As DataTable
#End Region
#Region "RptDailySalesByCA"
    Function GetCASupervisor(ByVal customclass As Parameter.Sales) As DataTable
    Function GetCA(ByVal customclass As Parameter.Sales) As DataTable
#End Region
#Region "RptDailySalesByBrand"
    Function GetSupplier(ByVal ocustomclass As Parameter.Sales) As DataTable
    Function GetBrand(ByVal ocustomclass As Parameter.Sales) As DataTable
#End Region
#Region "PercentDP"
    Function GetPercentDP(ByVal ocustomclass As Parameter.Sales) As DataTable
#End Region
#Region "PercentEffective"
    Function GetPercentEffective(ByVal ocustomclass As Parameter.Sales) As DataTable
#End Region
#Region "AmountFinance"
    Function GetAmountFinance(ByVal ocustomclass As Parameter.Sales) As DataTable
#End Region
#Region "InstallmentAmount"
    Function GetInstallmentAmount(ByVal ocustomclass As Parameter.Sales) As DataTable
#End Region
#Region "SalesPerPeriod"
    Function GetProduct(ByVal ocustomclass As Parameter.Sales) As DataTable
    Function InqSalesPerPeriod(ByVal ocustomclass As Parameter.Sales) As Parameter.Sales
#End Region
#Region "AgingStatus"
    Function InqAgingStatus(ByVal ocustomclass As Parameter.Sales) As Parameter.Sales
#End Region
    Function GetProductWhere(ByVal ocustomclass As Parameter.Sales) As DataTable
#Region "ApplicationPerPeriodINQ"
    Function GetProductBranch(ByVal ocustomclass As Parameter.Sales) As DataTable
    Function GetAssetCode(ByVal ocustomclass As Parameter.Sales) As DataTable
#End Region

    Function DailySalesCMOCrosstab(ByVal customclass As Parameter.Sales) As Parameter.Sales
    Function DailySalesSupplierCrosstab(ByVal customclass As Parameter.Sales) As Parameter.Sales

End Interface
