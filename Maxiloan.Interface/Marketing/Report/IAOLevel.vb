
Public Interface IAOLevel
    Function AOLevelEvaluation(ByVal customclass As Parameter.AOLevelReport) As Parameter.AOLevelReport
    Function GetAO(ByVal customclass As Parameter.AOLevelReport) As DataTable
    Function GetAOSupervisor(ByVal customclass As Parameter.AOLevelReport) As DataTable
End Interface
