
Public Interface IStatementOfAO
    Function ReportView(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
    Function GetAO(ByVal customclass As Parameter.StatementOfAO) As DataTable
    Function SubReportApproved(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
    Function SubReportFunding(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
    Function SubReportGrossYield(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
    Function SubReportBucket(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
    Function SubReportBucketTotal(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
    Function SubReportReppossess(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
    Function SubReportBPKB(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
    Function MainReport(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
    Function SubReportBPKBOverdue(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
    Function MainReportBranch(ByVal customclass As Parameter.StatementOfAO) As Parameter.StatementOfAO
End Interface
