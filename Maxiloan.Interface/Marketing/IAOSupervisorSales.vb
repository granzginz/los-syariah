Public Interface IAOSupervisorSales
    Function AOSupervisorListing(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
    Function AOSupervisorBudgetListing(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
    Function AOSupervisorBudgetView(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
    Function AOSupervisorBudgetPrint(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
    Function AOSupervisorForecastListing(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
    Function AOSupervisorForecastView(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
    Function AOSupervisorForecastPrint(ByVal customclass As Parameter.AOSupervisorSales) As Parameter.AOSupervisorSales
End Interface
