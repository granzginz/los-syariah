Public Interface IAOIndirectSales
    Function AOIndirectListing(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
    Function AOBudgetListing(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
    Function AOBudgetView(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
    Function AOBudgetPrint(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
    Function AOForecastListing(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
    Function AOForecastView(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
    Function AOForecastPrint(ByVal customclass As Parameter.AOIndirectSales) As Parameter.AOIndirectSales
End Interface
