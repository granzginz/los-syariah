
Imports Maxiloan.Parameter

Public Interface IProspectAssignAppraiser

    Function GetProspect(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
    Function ProspectSaveAdd(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
    Function ProspectSaveAsset(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
    Function ProspectSaveDemografi(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
    Function ProspectSaveFinancial(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
    Function GetViewProspect(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
    Function GetCbo(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
    Function GetCboGeneral(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
    Function DoProceeded(ByVal cnn As String, prospectId As String) As Boolean
    Function DoProspectCreaditScoring(cnn As String, branchId As String, prospectAppId As String, loginId As String, bnsDate As Date) As String
    Function DispositionCreditRpt(cnn As String, prospectAppIds As String(), tp As String) As DataSet
    Function GetProspectScorePolicyResult(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
    Function DoProspectDecision(cnn As String, prospectAppId As String, appr As Integer)
    Function MasterDataReviewRpt(cnn As String, prospectAppIds As String(), tp As String) As DataSet
    Function GetInitial(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
    Function InitialSaveAdd(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
    Function DoBIChecking(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
    Function DataSurveySaveAdd(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
    Function DataSurveySaveEdit(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
    Function ProspectReturnUpdate(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As String
    Function ProspectLogSave(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
    Function GetInqProspect(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
    Function GetApprovalprospect(ByVal oCustomClass As Parameter.ProspectAssignAppraiser, ByVal strApproval As String)

    Function DoProspectCreaditScoringProceed(cnn As String, branchId As String, prospectAppId As String, loginId As String, bnsDate As Date, proceed As Boolean) As String

    Function SaveProspectAssignAppraisal(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
    Function GetDataAppraisal(ByVal oCustomClass As Parameter.ProspectAssignAppraiser) As Parameter.ProspectAssignAppraiser
End Interface
