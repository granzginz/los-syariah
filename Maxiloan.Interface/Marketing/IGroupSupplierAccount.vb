
Public Interface IGroupSupplierAccount
    Function GetGroupSupplierAccount(ByVal oCustomClass As Parameter.GroupSupplierAccount) As Parameter.GroupSupplierAccount
    Function GetGroupSupplierAccountReport(ByVal oCustomClass As Parameter.GroupSupplierAccount) As Parameter.GroupSupplierAccount    
    Function GetGroupSupplierAccountList(ByVal oCustomClass As Parameter.GroupSupplierAccount) As Parameter.GroupSupplierAccount
    Function GroupSupplierAccountSaveAdd(ByVal oCustomClass As Parameter.GroupSupplierAccount) As String
    Sub GroupSupplierAccountSaveEdit(ByVal oCustomClass As Parameter.GroupSupplierAccount)
    Function GroupSupplierAccountDelete(ByVal oCustomClass As Parameter.GroupSupplierAccount) As String    
End Interface
