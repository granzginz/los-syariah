
Public Interface IMktNewApp
    Function GetProfession(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp
    Function GetPersonalHomeStatus(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp
    Function GetPersonalIdType(ByVal oCustomClass As Parameter.MktNewApp) As Parameter.MktNewApp
    Function MktNewAppPaging(ByVal oCustomClass As Parameter.MktNewApp) As Parameter.MktNewApp
    Function MktNewAppView(ByVal oCustomClass As Parameter.MktNewApp) As Parameter.MktNewApp
    Sub Generate_XML(ByVal pStrFile As String, ByVal oCustomClass As Parameter.MktNewApp)
    Function BindCustomer1_002(ByVal oCustomer As Parameter.MktNewApp) As Parameter.MktNewApp
    Function BindCustomer2_002(ByVal oCustomer As Parameter.MktNewApp) As Parameter.MktNewApp
    Function BindCustomerC1_002(ByVal oCustomer As Parameter.MktNewApp) As Parameter.MktNewApp
    Function BindCustomerC2_002(ByVal oCustomer As Parameter.MktNewApp) As Parameter.MktNewApp
    Sub MktNewAppSave(ByVal oCustomClass As Parameter.MktNewApp)
    Sub MktNewAppEdit(ByVal oCustomClass As Parameter.MktNewApp)
    Function GetInsuranceCo(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp
    Function GetAssetDescription(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp
    Function GetIndustryType(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp
    Function GetCompanyStatus(ByVal ocustomclass As Parameter.MktNewApp) As Parameter.MktNewApp
    Sub MktNewAppSaveCompany(ByVal oCustomClass As Parameter.MktNewApp)
    Function MktNewAppViewCompany(ByVal oCustomClass As Parameter.MktNewApp) As Parameter.MktNewApp
    Sub MktNewAppEditCompany(ByVal oCustomClass As Parameter.MktNewApp)
End Interface
