
Imports Maxiloan.Parameter

Public Interface IProspectResultAppraiser

    Function GetProspect(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
    Function ProspectSaveAdd(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
    Function ProspectSaveAsset(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
    Function ProspectSaveDemografi(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
    Function ProspectSaveFinancial(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
    Function GetViewProspect(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
    Function GetCbo(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
    Function GetCboGeneral(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
    Function DoProceeded(ByVal cnn As String, prospectId As String) As Boolean
    Function DoProspectCreaditScoring(cnn As String, branchId As String, prospectAppId As String, loginId As String, bnsDate As Date) As String
    Function DispositionCreditRpt(cnn As String, prospectAppIds As String(), tp As String) As DataSet
    Function GetProspectScorePolicyResult(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
    Function DoProspectDecision(cnn As String, prospectAppId As String, appr As Integer)
    Function MasterDataReviewRpt(cnn As String, prospectAppIds As String(), tp As String) As DataSet
    Function GetInitial(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
    Function InitialSaveAdd(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
    Function DoBIChecking(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
    Function DataSurveySaveAdd(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
    Function DataSurveySaveEdit(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
    Function ProspectReturnUpdate(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As String
    Function ProspectLogSave(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
    Function GetInqProspect(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser
    Function GetApprovalprospect(ByVal oCustomClass As Parameter.ProspectResultAppraiser, ByVal strApproval As String)

    Function DoProspectCreaditScoringProceed(cnn As String, branchId As String, prospectAppId As String, loginId As String, bnsDate As Date, proceed As Boolean) As String

    Function SaveProspectAssignAppraisal(ByVal oCustomClass As Parameter.ProspectResultAppraiser) As Parameter.ProspectResultAppraiser

End Interface
