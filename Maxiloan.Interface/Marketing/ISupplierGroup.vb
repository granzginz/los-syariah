
Public Interface ISupplierGroup
    Function GetSupplierGroup(ByVal oCustomClass As Parameter.SupplierGroup) As Parameter.SupplierGroup
    Function GetSupplierGroupReport(ByVal oCustomClass As Parameter.SupplierGroup) As Parameter.SupplierGroup
    Function GetSupplierGroupList(ByVal oCustomClass As Parameter.SupplierGroup) As Parameter.SupplierGroup
    Function SupplierGroupSaveAdd(ByVal oCustomClass As Parameter.SupplierGroup) As String
    Sub SupplierGroupSaveEdit(ByVal oCustomClass As Parameter.SupplierGroup)
    Function SupplierGroupDelete(ByVal oCustomClass As Parameter.SupplierGroup) As String
    Function GetJobPositionCombo(ByVal oCustomClass As Parameter.SupplierGroup) As DataTable
End Interface
