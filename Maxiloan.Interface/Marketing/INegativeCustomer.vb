

Public Interface INegativeCustomer
    Function ListNegativeCustomer(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
    Function NegativeCustomerAdd(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
    Function NegativeCustomerView(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
    Function NegativeCustomerEdit(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
    Function ListIDType(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
    Function ListDataSource(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
    Function NegativeCustomerRpt(ByVal customclass As Parameter.NegativeCustomer) As Parameter.NegativeCustomer
    Function GetApprovalNumberForHistory(ByVal data As Parameter.NegativeCustomer) As String
    Function DoUploadNegativeCsv(cnn As String, BusinessDate As DateTime, BranchId As String, params As DataTable) As String
End Interface
