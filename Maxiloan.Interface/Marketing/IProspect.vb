
Imports Maxiloan.Parameter

Public Interface IProspect

    Function GetProspect(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect
    Function ProspectSaveAdd(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect
    Function ProspectSaveAsset(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect
    Function ProspectSaveDemografi(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect
    Function ProspectSaveFinancial(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect
    Function GetViewProspect(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect
    Function GetCbo(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect
    Function GetCboGeneral(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect
    Function DoProceeded(ByVal cnn As String, prospectId As String) As Boolean
    Function DoProspectCreaditScoring(cnn As String, branchId As String, prospectAppId As String, loginId As String, bnsDate As Date) As String
    Function DispositionCreditRpt(cnn As String, prospectAppIds As String(), tp As String) As DataSet
    Function GetProspectScorePolicyResult(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect
    Function DoProspectDecision(cnn As String, prospectAppId As String, appr As Integer)
    Function MasterDataReviewRpt(cnn As String, prospectAppIds As String(), tp As String) As DataSet
    Function GetInitial(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect
    Function InitialSaveAdd(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect
    Function DoBIChecking(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect
    Function DataSurveySaveAdd(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect
    Function DataSurveySaveEdit(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect
    Function ProspectReturnUpdate(ByVal oCustomClass As Parameter.Prospect) As String
    Function ProspectLogSave(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect
    Function GetInqProspect(ByVal oCustomClass As Parameter.Prospect) As Parameter.Prospect
    Function GetApprovalprospect(ByVal oCustomClass As Parameter.Prospect, ByVal strApproval As String)

    Function DoProspectCreaditScoringProceed(cnn As String, branchId As String, prospectAppId As String, loginId As String, bnsDate As Date, proceed As Boolean) As String

End Interface
