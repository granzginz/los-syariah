﻿Public Interface IAcuanBungaIntern
    Function GetListing(ByVal oCustom As Parameter.PAcuanBungaIntern) As Parameter.PAcuanBungaIntern
    Function SaveUpdate(ByVal oCustome As Parameter.PAcuanBungaIntern) As String
    Function Delete(ByVal oCustome As Parameter.PAcuanBungaIntern) As String
    Function GetListingUCL(ByVal oCustom As Parameter.PAcuanBungaIntern, ByVal Tipe As String) As DataTable
    Function UpdateAssetAcuan(ByVal oCustome As Parameter.PAcuanBungaIntern, ByVal tipe As String) As String
End Interface
