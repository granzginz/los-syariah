
Public Interface ICredit
#Region "ComponentScoringCard"
    Function GetCredit(ByVal customClass As Parameter.Credit) As Parameter.Credit
    Function GetCreditReport(ByVal customClass As Parameter.Credit) As Parameter.Credit
    Function CreditEdit(ByVal customClass As Parameter.Credit) As Parameter.Credit
    Function CreditSaveAdd(ByVal customClass As Parameter.Credit) As String
    Function CreditDelete(ByVal customClass As Parameter.Credit) As String
    Sub CreditSaveEdit(ByVal customClass As Parameter.Credit)
#End Region
End Interface