

Public Interface IIncentiveCard
    Function GetViewByID(ByVal oCustomClass As Parameter.IncentiveCard) As Parameter.IncentiveCard
    Sub SupplierINCTVHeaderAdd(ByVal oCustomClass As Parameter.IncentiveCard)
    Sub SupplierINCTVHeaderEdit(ByVal oCustomClass As Parameter.IncentiveCard)
    Sub SupplierINCTVHeaderDelete(ByVal oCustomClass As Parameter.IncentiveCard)
    Function GetComboSupplierIncentiveCard(ByVal oCustomClass As Parameter.IncentiveCard) As DataTable
    Sub SupplierINCTVDetailUnitAdd(ByVal oCustomClass As Parameter.IncentiveCard)
    Sub SupplierINCTVDetailUnitEdit(ByVal oCustomClass As Parameter.IncentiveCard)
    Sub SupplierINCTVDetailAFAdd(ByVal oCustomClass As Parameter.IncentiveCard)
    Sub SupplierINCTVDetailAFEdit(ByVal oCustomClass As Parameter.IncentiveCard)
    Sub SupplierINCTVDailyDAdd(ByVal oCustomClass As Parameter.IncentiveCard)
    Sub SupplierINCTVUpdateIncentiveRestAmount(ByVal oCustomClass As Parameter.IncentiveCard)
    Sub SupplierINCTVUpdateIncentiveRestEmployeeAmount(ByVal oCustomClass As Parameter.IncentiveCard)
    Sub SupplierINCTVIncentiveRestAmountExecution(ByVal oCustomClass As Parameter.IncentiveCard)
    Sub SupplierINCTVEmployeeIncentiveRestAmountExecution(ByVal oCustomClass As Parameter.IncentiveCard)
    Sub SupplierINCTVDailyDEdit(ByVal oCustomClass As Parameter.IncentiveCard)
End Interface
