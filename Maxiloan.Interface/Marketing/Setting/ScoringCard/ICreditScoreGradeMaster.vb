
Public Interface ICreditScoreGradeMaster
    Function GetCreditScoreGradeMasterList(ByVal oCustomClass As Parameter.CreditScoreGradeMaster) As List(Of Parameter.CreditScoreGradeMaster)
    Function CreditScoreGradeMasterSaveAdd(ByVal oCustomClass As Parameter.CreditScoreGradeMaster) As String
    Sub CreditScoreGradeMasterSaveEdit(ByVal oCustomClass As Parameter.CreditScoreGradeMaster)
    Function CreditScoreGradeMasterDelete(ByVal oCustomClass As Parameter.CreditScoreGradeMaster) As String
End Interface
