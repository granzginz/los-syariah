

Public Interface ICreditScoringMain

    '===================== Bagian Main ================================
    Function GetCreditScoringMain(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
    Function GetCreditScoringMainReport(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
    Function CreditScoringMainEdit(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
    Function CreditScoringMainAdd(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
    Function CreditScoringMainSaveAdd(ByVal customClass As Parameter.CreditScoringMain) As String
    Function CreditScoringMainDelete(ByVal customClass As Parameter.CreditScoringMain) As String
    Sub CreditScoringMainSaveEdit(ByVal customClass As Parameter.CreditScoringMain)

    '===================== Bagian View ================================
    Function CreditScoringMainView(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain

    '===================== Bagian Edit Content ================================
    Function GetEditContentPaging(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
    Function EditContentEdit(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
    Function EditContentSaveAdd(ByVal customClass As Parameter.CreditScoringMain) As String
    Function EditContentDelete(ByVal customClass As Parameter.CreditScoringMain) As String
    Sub EditContentSaveEdit(ByVal customClass As Parameter.CreditScoringMain)

    Function GetEditContentPaging_Table(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
    Function EditContentEdit_Table(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
    Sub EditContentSaveEdit_Table(ByVal customClass As Parameter.CreditScoringMain)

    '--Cut Off Score --'
    Function CreditScoringCutOff(ByVal customClass As Parameter.CreditScoringMain) As Parameter.CreditScoringMain
    Function CreditScoringCutOffAdd(ByVal customClass As Parameter.CreditScoringMain) As String
    Function CreditScoringCutOffEdit(ByVal customClass As Parameter.CreditScoringMain) As String
    Function CreditScoringCutOffDelete(ByVal customClass As Parameter.CreditScoringMain) As String
End Interface
