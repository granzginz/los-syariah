

Public Interface IProduct
    Function ProductPaging(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductEdit(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductView(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductReport(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductSaveAdd(ByVal customClass As Parameter.Product) As String
    Sub ProductSaveEdit(ByVal customClass As Parameter.Product)

    Function Get_Combo_AssetType(ByVal customClass As Parameter.Product) As Parameter.Product
    Function Get_Combo_ScoreSchemeMaster(ByVal customClass As Parameter.Product) As Parameter.Product
    Function Get_Combo_CreditScoreSchemeMaster(ByVal customClass As Parameter.Product) As Parameter.Product
    Function Get_Combo_JournalScheme(ByVal customClass As Parameter.Product) As Parameter.Product
    Function Get_Combo_ApprovalTypeScheme(ByVal customClass As Parameter.Product) As Parameter.Product

    Function Get_Combo_Term_n_Condition(ByVal customClass As Parameter.Product) As Parameter.Product
    Function Get_Combo_Term_n_Condition_Brc(ByVal customClass As Parameter.Product) As Parameter.Product

    Function ProductTCPaging(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductTCEdit(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductTCReport(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductTCSaveAdd(ByVal customClass As Parameter.Product) As String
    Function ProductTCDelete(ByVal customClass As Parameter.Product) As String
    Sub ProductTCSaveEdit(ByVal customClass As Parameter.Product)

    Function GetBranch(ByVal customClass As Parameter.Product) As Parameter.Product
    Function GetBranchAll(ByVal customClass As Parameter.Product) As Parameter.Product

    Function ProductBranchHOPaging(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductBranchHOEdit(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductBranchHOReport(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductBranchHOSaveAdd(ByVal customClass As Parameter.Product) As String
    Sub ProductBranchHOSaveEdit(ByVal customClass As Parameter.Product)
    Sub CopyProduct(ByVal customClass As Parameter.CopyProduct)

    Function ProductBranchPaging(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductBranchEdit(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductBranchReport(ByVal customClass As Parameter.Product) As Parameter.Product
    Sub ProductBranchSaveEdit(ByVal customClass As Parameter.Product)

    Function ProductBranchTCPaging(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductBranchTCEdit(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductBranchTCReport(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductBranchTCSaveAdd(ByVal customClass As Parameter.Product) As String
    Function ProductBranchTCDelete(ByVal customClass As Parameter.Product) As String
    Sub ProductBranchTCSaveEdit(ByVal customClass As Parameter.Product)

    Function ProductOfferingPaging(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductOfferingEdit(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductOfferingAdd(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductOfferingView(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductOfferingReport(ByVal customClass As Parameter.Product) As Parameter.Product
    Function ProductOfferingSaveAdd(ByVal customClass As Parameter.Product) As String
    Function ProductOfferingDelete(ByVal customClass As Parameter.Product) As String
    Sub ProductOfferingSaveEdit(ByVal customClass As Parameter.Product)

    Function LoadKegiatanUsaha(cnn As String) As IList(Of Parameter.KegiatanUsaha)
    Function ProductByKUJP(ByVal customClass As Parameter.Product) As Parameter.Product
    Function DropDownListProduct(ByVal customClass As Parameter.Product) As Parameter.Product
    Function GetCboSkePemb(ByVal customClass As Parameter.Product) As Parameter.Product
    Function GetCboJenPemb(ByVal customClass As Parameter.Product) As Parameter.Product
End Interface
