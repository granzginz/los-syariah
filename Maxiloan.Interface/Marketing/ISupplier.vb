
Public Interface ISupplier
#Region "Supplier"
    Function GetSupplier(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function SupplierSaveAdd(ByVal oSupplier As Parameter.Supplier, _
     ByVal oAddress As Parameter.Address, ByVal oAddressNPWP As Parameter.Address) As Parameter.Supplier
    Function GetBranch(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Sub SupplierSaveBranch(ByVal oSupplier As Parameter.Supplier, ByVal oDataBranch As DataTable)
    Function DeleteSupplier(ByVal customClass As Parameter.Supplier) As String
    Function SupplierEdit(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Sub SupplierSaveEdit(ByVal oSupplier As Parameter.Supplier, _
      ByVal oAddress As Parameter.Address, ByVal oAddressNPWP As Parameter.Address)
    Function GetSupplierReport(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function GetSupplierID(ByVal customerClass As Parameter.Supplier) As Parameter.Supplier
    Function SupplierSaveAddPrivate(ByVal oSupplier As Parameter.Supplier, ByVal oAddress As Parameter.Address) As Parameter.Supplier
    Function GetProduk(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function GetNoPKS(ByVal customerClass As Parameter.Supplier) As Parameter.Supplier
    Function SupplierAccountSave2(ByVal oSupplier As Parameter.Supplier) As Parameter.Supplier
#End Region
#Region "SupplierBranch"
    Function GetSupplierBranch(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function GetSupplierBranchAdd(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function GetSupplierBranchIncentiveCard(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function GetSupplierBranchEdit(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Sub SupplierBranchSaveEdit(ByVal oSupplier As Parameter.Supplier)
    Function GetSupplierBranchDelete(ByVal customClass As Parameter.Supplier) As String
    Function GetSupplierEmployeePosition(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function GetSupplierBranchRefund(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Sub SupplierBranchRefundHeaderSaveEdit(ByVal customClass As Parameter.Supplier)
    Sub SupplierBranchRefundSaveEdit(ByVal customClass As Parameter.Supplier)

#End Region
#Region "SupplierOwner"
    Function GetSupplierOwner(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function GetSupplierOwnerIDType(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function SupplierOwnerSaveAdd(ByVal oSupplier As Parameter.Supplier, _
     ByVal oAddress As Parameter.Address) As Parameter.Supplier
    Function GetSupplierOwnerEdit(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function GetSupplierOwnerDetele(ByVal customClass As Parameter.Supplier) As String
    Function GetSupplierOwnerView(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    'Function GetSupplierOwnerID(ByVal customerClass As Parameter.Supplier) As String
#End Region
#Region "SupplierAccount"
    Function GetSupplierAccount(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    'Function GetSupplierAccountEdit(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function SupplierAccountSave(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function SupplierAccountDelete(ByVal customClass As Parameter.Supplier) As String
#End Region

    Function GetSupplierEmployee(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function SupplierEmployeeSave(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function SupplierEmployeeDelete(ByVal customClass As Parameter.Supplier) As String


#Region "SupplierSignature"
    Function GetSupplierSignature(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Sub SupplierSignatureSaveAdd(ByVal customClass As Parameter.Supplier)
    Sub SupplierSignatureSaveEdit(ByVal customClass As Parameter.Supplier)
    Function SupplierSignatureDelete(ByVal customClass As Parameter.Supplier) As String
    Function SupplierSignaturePath(ByVal customClass As Parameter.Supplier) As String
    Function SupplierSignatureGetSequenceNo(ByVal SupplierID As String, ByVal strConnection As String) As String
#End Region
#Region "SupplierBudget"
    Function AddSupplierBudget(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function EditSupplierBudget(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
#End Region

#Region "SupplierForecast"
    Function AddSupplierForecast(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function EditSupplierForecast(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
#End Region
#Region "SupplierView"
    Function SupplierView(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function SupplierViewBudgetForecast(ByVal customClass As Parameter.Supplier) As Parameter.Supplier

#End Region
#Region "SupplierLevelStatus"
    Function SupplierLevelStatus(ByVal strconnection As String) As DataTable
#End Region
#Region "GetSupplier Account bySupplierID"
    Function GetSupplierAccountbySupplierID(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
#End Region

#Region "SupplierRelasi"
    Function GetSupplierEmployeeSPV(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function GetSupplierEmployeeSales(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
    Function SaveSupplierEmployeeRelasi(ByVal customClass As Parameter.Supplier, ByVal tblEmployee As DataTable) As String
    Function GetSupplierEmployeeSPVBySales(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
#End Region
    ' Function GetProduk(ByVal customClass As Parameter.Supplier) As Parameter.Supplier
End Interface
