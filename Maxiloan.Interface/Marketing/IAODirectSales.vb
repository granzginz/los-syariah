
Public Interface IAODirectSales

    Function AOListing(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
    Function AOBudgetListing(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
    Sub AOBudgetAdd(ByVal customclass As Parameter.AODirectSales)
    Sub AOBudgetEdit(ByVal customclass As Parameter.AODirectSales)
    Function AOBudgetView(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
    Function AOForecastListing(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
    Sub AOForecastAdd(ByVal customclass As Parameter.AODirectSales)
    Sub AOForecastEdit(ByVal customclass As Parameter.AODirectSales)
    Sub AOBudgetSaveEdit(ByVal customclass As Parameter.AODirectSales)
    Function AOForecastView(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
    Function AOForecastPrint(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
    Function AOBudgetPrint(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
    Function AOBudgetList(ByVal customclass As Parameter.AODirectSales) As Parameter.AODirectSales
End Interface
