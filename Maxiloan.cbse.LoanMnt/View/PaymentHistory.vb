

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class PaymentHistory : Inherits ComponentBase
    Implements IPaymentHistory

    Public Function ListPaymentHistory(ByVal customClass As Parameter.PaymentHistory) As Parameter.PaymentHistory Implements [Interface].IPaymentHistory.ListPaymentHistory
        Dim DAPaymentHistory As New SQLEngine.LoanMnt.PaymentHistory
        Return DAPaymentHistory.ListPaymentHistory(customClass)
    End Function

    Public Function ListPaymentHistoryDetail(ByVal customclass As Parameter.PaymentHistory) As Parameter.PaymentHistory Implements [Interface].IPaymentHistory.ListPaymentHistoryDetail
        Dim DAPaymentHistory As New SQLEngine.LoanMnt.PaymentHistory
        Return DAPaymentHistory.ListPaymentHistoryDetail(customclass)
    End Function

    Public Function ListPaymentHistoryDetailOtor(ByVal customclass As Parameter.PaymentHistory) As Parameter.PaymentHistory Implements [Interface].IPaymentHistory.ListPaymentHistoryDetailOtor
        Dim DAPaymentHistory As New SQLEngine.LoanMnt.PaymentHistory
        Return DAPaymentHistory.ListPaymentHistoryDetailOtor(customclass)
    End Function

	Public Function ReversalTransaksiLainnyaSave(ByVal m_customclass As Parameter.PaymentHistory) As Parameter.PaymentHistory Implements [Interface].IPaymentHistory.ReversalTransaksiLainnyaSave
		Dim DAPaymentHistory As New SQLEngine.LoanMnt.PaymentHistory
		Return DAPaymentHistory.ReversalTransaksiLainnyaSave(m_customclass)
	End Function

	Public Function ListPaymentHistoryDetailFactoring(ByVal customclass As Parameter.PaymentHistory) As Parameter.PaymentHistory Implements [Interface].IPaymentHistory.ListPaymentHistoryDetailFactoring
		Dim DAPaymentHistory As New SQLEngine.LoanMnt.PaymentHistory
		Return DAPaymentHistory.ListPaymentHistoryDetailFactoring(customclass)
	End Function

    Public Function ListPaymentHistoryDetailModalKerja(ByVal customclass As Parameter.PaymentHistory) As Parameter.PaymentHistory Implements [Interface].IPaymentHistory.ListPaymentHistoryDetailModalKerja
        Dim DAPaymentHistory As New SQLEngine.LoanMnt.PaymentHistory
        Return DAPaymentHistory.ListPaymentHistoryDetailModalKerja(customclass)
    End Function
    Public Function ReversalPembayaranTDPSave(ByVal m_customclass As Parameter.PaymentHistory) As Parameter.PaymentHistory Implements [Interface].IPaymentHistory.ReversalPembayaranTDPSave
        Dim DAPaymentHistory As New SQLEngine.LoanMnt.PaymentHistory
        Return DAPaymentHistory.ReversalPembayaranTDPSave(m_customclass)
    End Function
End Class
