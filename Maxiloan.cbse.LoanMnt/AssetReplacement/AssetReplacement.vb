
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class AssetReplacement
    Implements IAssetReplacement
    Public Function AssetReplacementPaging(ByVal customClass As Parameter.AssetReplacement) As Parameter.AssetReplacement Implements IAssetReplacement.AssetReplacementPaging
        Dim DA As New SQLEngine.LoanMnt.AssetReplacement
        Return DA.AssetReplacementPaging(customClass)
    End Function
    Public Function GetAssetRepSerial(ByVal oCustomClass As Parameter.AssetReplacement) As Parameter.AssetReplacement Implements IAssetReplacement.GetAssetRepSerial
        Dim DA As New SQLEngine.LoanMnt.AssetReplacement
        Return DA.GetAssetRepSerial(oCustomClass)
    End Function

    Sub AssetReplacementSaveAdd(ByVal oCustomClass As Parameter.AssetReplacement, _
          ByVal oAddress As Parameter.Address, ByVal oData1 As DataTable, _
          ByVal oData2 As DataTable) Implements [Interface].IAssetReplacement.AssetReplacementSaveAdd
        Try
            Dim AssetDataDA As New SQLEngine.LoanMnt.AssetReplacement
            AssetDataDA.AssetReplacementSaveAdd(oCustomClass, oAddress, oData1, oData2)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Sub
    Public Function AssetRepApproval(ByVal oCustomClass As Parameter.AssetReplacement) As Parameter.AssetReplacement Implements IAssetReplacement.AssetRepApproval
        Dim DA As New SQLEngine.LoanMnt.AssetReplacement
        Return DA.AssetRepApproval(oCustomClass)
    End Function
    Public Function AssetInquiryReport(ByVal customclass As Parameter.AssetReplacement) As Parameter.AssetReplacement Implements IAssetReplacement.AssetInquiryReport
        Dim DA As New SQLEngine.LoanMnt.AssetReplacement
        Return DA.AssetInquiryReport(customclass)

    End Function
    Public Function ViewAssetReplInquiry(ByVal oCustomClass As Parameter.AssetReplacement) As Parameter.AssetReplacement Implements IAssetReplacement.ViewAssetReplInquiry
        Dim DA As New SQLEngine.LoanMnt.AssetReplacement
        Return DA.ViewAssetReplInquiry(oCustomClass)
    End Function
    Public Sub SaveAssetReplacementCancel(ByVal customClass As Parameter.AssetReplacement) Implements IAssetReplacement.SaveAssetReplacementCancel
        Dim AssetDataDA As New SQLEngine.LoanMnt.AssetReplacement
        AssetDataDA.SaveAssetReplacementCancel(customClass)
    End Sub
    Public Sub SaveAssetReplacementExecute(ByVal customClass As Parameter.AssetReplacement) Implements IAssetReplacement.SaveAssetReplacementExecute
        Dim AssetDataDA As New SQLEngine.LoanMnt.AssetReplacement
        AssetDataDA.SaveAssetReplacementExecute(customClass)
    End Sub
End Class
