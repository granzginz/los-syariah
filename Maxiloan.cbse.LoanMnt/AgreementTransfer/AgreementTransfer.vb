

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class AgreementTransfer : Inherits ComponentBase
    Implements IAgreementTransfer


    Public Function AssetDocPaging(ByVal customclass As Parameter.DChange) As Parameter.DChange Implements [Interface].IAgreementTransfer.AssetDocPaging
        Dim DAListDoc As New SQLEngine.LoanMnt.AgreementTransfer
        Return DAListDoc.AssetDocPaging(customclass)
    End Function

    Public Function GetCust(ByVal customclass As Parameter.DChange) As Parameter.DChange Implements [Interface].IAgreementTransfer.GetCust
        Dim DACust As New SQLEngine.LoanMnt.AgreementTransfer
        Return DACust.GetCust(customclass)
    End Function

    Public Sub AgreementTransferReq(ByVal oCustomClass As Parameter.DChange) Implements [Interface].IAgreementTransfer.AgreementTransferReq
        Dim AReq As New SQLEngine.LoanMnt.AgreementTransfer
        AReq.SaveATRequest(oCustomClass)
    End Sub

    Public Function GetListExecAT(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IAgreementTransfer.GetListExecAT
        Dim DAListE As New SQLEngine.LoanMnt.AgreementTransfer
        Return DAListE.GetListAT(oCustomClass)
    End Function

    Public Function GetATTC(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IAgreementTransfer.GetATTC
        Dim DAListE As New SQLEngine.LoanMnt.AgreementTransfer
        Return DAListE.GetListTC(oCustomClass)
    End Function

    Public Function GetATTC2(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IAgreementTransfer.GetATTC2
        Dim DAListE As New SQLEngine.LoanMnt.AgreementTransfer
        Return DAListE.GetListTC2(oCustomClass)
    End Function

    Public Sub ATExec(ByVal oCustomClass As Parameter.DChange) Implements [Interface].IAgreementTransfer.ATExec
        Dim ATEx As New SQLEngine.LoanMnt.AgreementTransfer
        ATEx.ATExec(oCustomClass)
    End Sub

    Public Function PrintEndorsAgreementTransfer(ByVal customClass As Parameter.DChange) As String Implements [Interface].IAgreementTransfer.PrintEndorsAgreementTransfer
        Dim DAListE As New SQLEngine.LoanMnt.AgreementTransfer
        Return DAListE.PrintEndorsAgreementTransfer(customClass)
    End Function
End Class
