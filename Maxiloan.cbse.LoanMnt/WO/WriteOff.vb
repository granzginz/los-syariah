
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class WriteOff
    Implements IWriteOff


    Public Function GetGeneralPagingWO(ByVal customClass As Parameter.WriteOff) As Parameter.WriteOff Implements [Interface].IWriteOff.GetGeneralPagingWO
        Dim DAWriteOff As New SQLEngine.LoanMnt.WriteOff
        Return DAWriteOff.GetGeneralPagingWO(customClass)
    End Function

    Public Function WriteOffGetSAAmount(ByVal oCustomClass As Parameter.WriteOff) As Parameter.WriteOff Implements [Interface].IWriteOff.WriteOffGetSAAmount
        Dim DAWriteOff As New SQLEngine.LoanMnt.WriteOff
        Return DAWriteOff.WriteOffGetSAAmount(oCustomClass)
    End Function
    Public Sub WORequest(ByVal oCustomClass As Parameter.WriteOff) Implements [Interface].IWriteOff.WORequest
        Dim DAWriteOff As New SQLEngine.LoanMnt.WriteOff
        DAWriteOff.WORequest(oCustomClass)

    End Sub
    Public Function WriteOffInquiry(ByVal oCustomClass As Parameter.WriteOff) As Parameter.WriteOff Implements [Interface].IWriteOff.WriteOffInquiry
        Dim DAPostingPayment As New SQLEngine.LoanMnt.WriteOff
        Return DAPostingPayment.WriteOffInquiry(oCustomClass)
    End Function
    Public Function WOInquiryReport(ByVal customclass As Parameter.WriteOff) As Parameter.WriteOff Implements [Interface].IWriteOff.WOInquiryReport
        Dim DA As New SQLEngine.LoanMnt.WriteOff
        Return DA.WOInquiryReport(customclass)
    End Function
    Public Function WriteOffView(ByVal oCustomClass As Parameter.WriteOff) As Parameter.WriteOff Implements [Interface].IWriteOff.WriteOffView
        Dim DA As New SQLEngine.LoanMnt.WriteOff
        Return DA.WriteOffView(oCustomClass)
    End Function
    Public Sub WriteOffEditPotensiTagih(ByVal oCustomClass As Parameter.WriteOff) Implements [Interface].IWriteOff.WriteOffEditPotensiTagih
        Try
            Dim DA As New SQLEngine.LoanMnt.WriteOff
            DA.WriteOffEditPotensiTagih(oCustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
End Class
