
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class LCWaived
    Implements ILCWaived

    Public Sub SaveLCWaived(ByVal customclass As Parameter.FullPrepay) Implements [Interface].ILCWaived.SaveLCWaived
        Dim DASaveLCWaived As New SQLEngine.LoanMnt.LCWaived
        DASaveLCWaived.SaveLCWaived(customclass)
    End Sub

    Public Function ListLCWaived(ByVal customclass As Parameter.FullPrepay) As Parameter.FullPrepay Implements [Interface].ILCWaived.ListLCWaived
        Dim DAListLCWaived As New SQLEngine.LoanMnt.LCWaived
        Return DAListLCWaived.ListLCWaived(customclass)
    End Function

    Public Function ViewLCWaived(ByVal customclass As Parameter.FullPrepay) As Parameter.FullPrepay Implements [Interface].ILCWaived.ViewLCWaived
        Dim DAListLCWaived As New SQLEngine.LoanMnt.LCWaived
        Return DAListLCWaived.ViewLCWaived(customclass)
    End Function
End Class
