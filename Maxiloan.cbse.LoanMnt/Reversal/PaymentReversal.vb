
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class PaymentReversal : Inherits ComponentBase
    Implements IPaymentReversal


    Public Function PaymentReversalList(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal Implements [Interface].IPaymentReversal.PaymentReversalList
        Dim PaymentReversalListDA As New SQLEngine.LoanMnt.PaymentReversal
        Return PaymentReversalListDA.PaymentReversalList(oCustomClass)

    End Function

    Public Function PaymentReversalListOtor(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal Implements [Interface].IPaymentReversal.PaymentReversalListOtor
        Dim PaymentReversalListDA As New SQLEngine.LoanMnt.PaymentReversal
        Return PaymentReversalListDA.PaymentReversalListOtor(oCustomClass)

    End Function

    Public Sub ProcessPaymentReversal(ByVal oCustomClass As Parameter.PaymentReversal) Implements [Interface].IPaymentReversal.ProcessPaymentReversal
        Dim ProcesPaymentReversalDA As New SQLEngine.LoanMnt.PaymentReversal
        ProcesPaymentReversalDA.ProcessPaymentReversal(oCustomClass)
    End Sub

    Public Sub ProcessPaymentReversalOtor(ByVal oCustomClass As Parameter.PaymentReversal) Implements [Interface].IPaymentReversal.ProcessPaymentReversalOtor
        Dim ProcesPaymentReversalDA As New SQLEngine.LoanMnt.PaymentReversal
        ProcesPaymentReversalDA.ProcessPaymentReversalOtor(oCustomClass)
    End Sub

    Public Sub ProcessReversalOtorFactoring(ByVal oCustomClass As Parameter.PaymentReversal) Implements [Interface].IPaymentReversal.ProcessReversalOtorFactoring
        Dim ProcesPaymentReversalDA As New SQLEngine.LoanMnt.PaymentReversal
        ProcesPaymentReversalDA.ProcessReversalOtorFactoring(oCustomClass)
    End Sub


    Public Sub ProcessReversalOtorModalKerja(ByVal oCustomClass As Parameter.PaymentReversal) Implements [Interface].IPaymentReversal.ProcessReversalOtorModalKerja
        Dim ProcesPaymentReversalDA As New SQLEngine.LoanMnt.PaymentReversal
        ProcesPaymentReversalDA.ProcessReversalOtorModalKerja(oCustomClass)
    End Sub

    Public Function ReversalLogList(oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal Implements [Interface].IPaymentReversal.ReversalLogList
        Dim ReversalLog As New SQLEngine.LoanMnt.PaymentReversal
        Return ReversalLog.ReversalLogList(oCustomClass)
    End Function

    Public Sub ProcessPaymentReversalOL(ByVal oCustomClass As Parameter.PaymentReversal) Implements [Interface].IPaymentReversal.ProcessPaymentReversalOL
        Dim ProcesPaymentReversalDA As New SQLEngine.LoanMnt.PaymentReversal
        ProcesPaymentReversalDA.ProcessPaymentReversalOL(oCustomClass)
    End Sub

	Public Function PaymentReversalListOL(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal Implements [Interface].IPaymentReversal.PaymentReversalListOL
		Dim PaymentReversalListDA As New SQLEngine.LoanMnt.PaymentReversal
		Return PaymentReversalListDA.PaymentReversalListOL(oCustomClass)
	End Function

	Public Function PaymentReversalListFactoring(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal Implements [Interface].IPaymentReversal.PaymentReversalListFactoring
		Dim PaymentReversalListDA As New SQLEngine.LoanMnt.PaymentReversal
		Return PaymentReversalListDA.PaymentReversalListFactoring(oCustomClass)
	End Function

	Public Sub ProcessPaymentReversalFactoring(ByVal oCustomClass As Parameter.PaymentReversal) Implements [Interface].IPaymentReversal.ProcessPaymentReversalFactoring
		Dim ProcesPaymentReversalDA As New SQLEngine.LoanMnt.PaymentReversal
		ProcesPaymentReversalDA.ProcessPaymentReversalFactoring(oCustomClass)
	End Sub

	Public Function PaymentReversalListModalKerja(ByVal oCustomClass As Parameter.PaymentReversal) As Parameter.PaymentReversal Implements [Interface].IPaymentReversal.PaymentReversalListModalKerja
		Dim PaymentReversalListDA As New SQLEngine.LoanMnt.PaymentReversal
		Return PaymentReversalListDA.PaymentReversalListModalKerja(oCustomClass)
	End Function

    Public Sub ProcessPaymentReversalModalKerja(ByVal oCustomClass As Parameter.PaymentReversal) Implements [Interface].IPaymentReversal.ProcessPaymentReversalModalKerja
        Dim ProcesPaymentReversalDA As New SQLEngine.LoanMnt.PaymentReversal
        ProcesPaymentReversalDA.ProcessPaymentReversalModalKerja(oCustomClass)
    End Sub
    Public Sub ProcessReversalTolakanBGOtor(ByVal oCustomClass As Parameter.PaymentReversal) Implements [Interface].IPaymentReversal.ProcessReversalTolakanBGOtor
        Dim ProcesPaymentReversalDA As New SQLEngine.LoanMnt.PaymentReversal
        ProcesPaymentReversalDA.ProcessReversalTolakanBGOtor(oCustomClass)
    End Sub

End Class
