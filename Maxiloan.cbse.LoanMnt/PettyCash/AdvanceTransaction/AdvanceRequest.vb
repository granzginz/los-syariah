
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class AdvanceRequest
    Implements IAdvanceRequest


    Private m_DAAdvanceRequest As SQLEngine.LoanMnt.AdvanceRequest

    Public Sub SaveAdvanceTransaction(ByVal oAdvanceRequest As Parameter.AdvanceRequest) Implements [Interface].IAdvanceRequest.SaveAdvanceTransaction
        Try
            m_DAAdvanceRequest.SaveAdvanceTransaction(oAdvanceRequest)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub New()
        m_DAAdvanceRequest = New SQLEngine.LoanMnt.AdvanceRequest
    End Sub

    Public Function GetPettyCashAdvanceList(ByVal oETAdvReq As Parameter.AdvanceRequest) As Parameter.AdvanceRequest Implements [Interface].IAdvanceRequest.GetPettyCashAdvanceList
        Try
            Return m_DAAdvanceRequest.GetPettyCashAdvanceList(oETAdvReq)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetAnAdvanceTransactionRecord(ByVal oETAdvReq As Parameter.AdvanceRequest) As Parameter.AdvanceRequest Implements [Interface].IAdvanceRequest.GetAnAdvanceTransactionRecord
        Try
            Return m_DAAdvanceRequest.GetAnAdvanceTransactionRecord(oETAdvReq)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetPettyCashAdvanceRpt(ByVal oETAdvReq As Parameter.AdvanceRequest) As Parameter.AdvanceRequest Implements [Interface].IAdvanceRequest.GetPettyCashAdvanceRpt
        Try
            Return m_DAAdvanceRequest.GetPettyCashAdvanceRpt(oETAdvReq)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub SaveAdvanceReturnTransaction(ByVal oAdvanceRequest As Parameter.AdvanceRequest) Implements [Interface].IAdvanceRequest.SaveAdvanceReturnTransaction
        Try
            m_DAAdvanceRequest.SaveAdvanceReturnTransaction(oAdvanceRequest)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Function CreateAdvanceTransactionDetail(cnn As String, advanceNo As String, atDetail As System.Collections.Generic.IList(Of Parameter.AdvanceDetail)) As String Implements [Interface].IAdvanceRequest.CreateAdvanceTransactionDetail
        Return (New SQLEngine.LoanMnt.AdvanceRequest).CreateAdvanceTransactionDetail(cnn, advanceNo, atDetail)
    End Function
End Class
