
#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region

Public Class PettyCashTrans : Inherits ComponentBase
    Implements IPettyCashTrans


#Region "Constanta"

    Private isError As Boolean
#End Region
#Region "GetStructPC"
    Public Function GetStructPC() As DataSet
        Dim lObjDataTable As New DataSet
        Dim dtTable As New DataTable("PC")

        lObjDataTable.Tables.Add(dtTable)
        lObjDataTable.Tables("PC").Columns.Add("TransactionID", GetType(String))
        lObjDataTable.Tables("PC").Columns.Add("Transaction", GetType(String))
        lObjDataTable.Tables("PC").Columns.Add("Description", GetType(String))
        lObjDataTable.Tables("PC").Columns.Add("ValueDate", GetType(String))
        lObjDataTable.Tables("PC").Columns.Add("Amount", GetType(Double))
        Return lObjDataTable
    End Function
#End Region
#Region "GetTablePC"


    Public Function GetTablePC(ByVal customclass As Parameter.PettyCash, ByVal strFile As String) As Parameter.PettyCash Implements [Interface].IPettyCashTrans.GetTablePC
        Dim CheckPCNo As DataRow()
        Dim lObjPC As New ArrayList

        Dim pstrFile As String
        Dim gStrPath As String

        Dim dsPC As New DataSet

        Dim DtPC As DataTable
        Dim DtPC2 As DataTable

        isError = False

        Dim CustomPC As New Parameter.PettyCash
        With customclass
            .isValidPC = True
            pstrFile = "C:\temp\" + strFile & "_M_PCTRANS"
            '=========bila hapus xml =================
            If .HpsXml = "1" Then
                If File.Exists(pstrFile & ".xsd") Then
                    File.Delete(pstrFile & ".xsd")
                End If
                If File.Exists(pstrFile & ".xml") Then
                    File.Delete(pstrFile & ".xml")
                End If
            Else
                If .FlagDelete = "1" Then
                    If Not File.Exists(pstrFile & ".xsd") Then
                        dsPC = GetStructPC()
                        dsPC.WriteXmlSchema(pstrFile & ".xsd")
                        dsPC.ReadXmlSchema(pstrFile & ".xsd")
                    End If
                    If Not File.Exists(pstrFile & ".xml") Then
                        dsPC = GetStructPC()
                        dsPC.WriteXml(pstrFile & ".xml")
                        dsPC.AcceptChanges()
                    Else
                        dsPC = GetStructPC()
                        dsPC.ReadXml(pstrFile & ".xml")
                        dsPC.AcceptChanges()
                    End If

                    dsPC = Generate_TableDelete(customclass, dsPC)
                    dsPC.WriteXml(pstrFile & ".xml")
                    dsPC.AcceptChanges()
                    customclass.ListPC = dsPC.Tables("PC")
                Else
                    If Not File.Exists(pstrFile & ".xsd") Then
                        dsPC = GetStructPC()
                        dsPC.WriteXmlSchema(pstrFile & ".xsd")
                        dsPC.ReadXmlSchema(pstrFile & ".xsd")
                    End If
                    If Not File.Exists(pstrFile & ".xml") Then
                        dsPC.WriteXml(pstrFile & ".xml")
                        dsPC.AcceptChanges()
                    Else
                        dsPC = GetStructPC()
                        dsPC.ReadXmlSchema(pstrFile & ".xsd")
                        dsPC.ReadXml(pstrFile & ".xml")
                        dsPC.AcceptChanges()
                        CheckPCNo = dsPC.Tables("PC").Select("TransactionID = '" & .TransactionID & "'")
                        If CheckPCNo.Length > 0 Then
                            .isValidPC = False
                            Return customclass
                        End If
                    End If
                    dsPC = Generate_Table(customclass, dsPC)


                    dsPC.WriteXml(pstrFile & ".xml")
                    dsPC.AcceptChanges()
                    customclass.ListPC = dsPC.Tables("PC")

                End If
            End If

        End With
        Return customclass
    End Function
#End Region
#Region "SavePCTrans"
    Public Function SavePCTrans(ByVal customclass As Parameter.PettyCash, ByVal strFile As String) As String Implements [Interface].IPettyCashTrans.SavePCTrans
        Dim DASavePCTrans As New SQLEngine.LoanMnt.PettyCashTrans
        With customclass
            Dim pstrFile As String = "C:\temp\" + strFile & "_M_PCTRANS"
            Dim dsNewPC As New DataSet
            dsNewPC.ReadXml(pstrFile & ".xml")
            Return DASavePCTrans.SavePCTrans(customclass, dsNewPC.Tables("PC"))
            .HpsXml = "1"
        End With
    End Function
#End Region
#Region "Generate_Table"
    Private Function Generate_Table(ByVal CustomClass As Parameter.PettyCash, ByVal dsPC As DataSet) As DataSet

        Dim lObjDataRow As DataRow
        With CustomClass

            lObjDataRow = dsPC.Tables("PC").NewRow()
            lObjDataRow("TransactionID") = .TransactionID
            lObjDataRow("Transaction") = .TransactionName
            lObjDataRow("Description") = .Description
            lObjDataRow("ValueDate") = .ValueDate
            lObjDataRow("Amount") = .Amount
            dsPC.Tables("PC").Rows.Add(lObjDataRow)

        End With
        Return dsPC
    End Function
#End Region
#Region "Generate_TableDelete"

    Private Function Generate_TableDelete(ByVal CustomClass As Parameter.PettyCash, ByVal dsPC As DataSet) As DataSet
        Dim NewDataSetDel As New DataSet
        With CustomClass
            dsPC.Tables("PC").Rows.RemoveAt(CInt(.IndexDelete))
            Return dsPC
        End With
    End Function
#End Region
#Region "SavePCReimburse"

    Public Function SavePCReimburse(ByVal customclass As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCashTrans.SavePCReimburse
        Dim DASavePCReimburse As New SQLEngine.LoanMnt.PettyCashTrans
        Return DASavePCReimburse.PCReimburseSave(customclass)
    End Function


#End Region
#Region "PCReimburseReconcile"

    Public Function PCReimburseReconcile(ByVal customClass As Parameter.PettyCashReconcile) As Parameter.PettyCashReconcile Implements [Interface].IPettyCashTrans.PCReimburseReconcile
        Dim DA As New SQLEngine.LoanMnt.PCReimburse
        DA.PCReimburseReconcile(customClass)
    End Function

#End Region

    Public Function PCreimburseStatusUpdate(ByVal customClass As Parameter.PettyCash) As String Implements [Interface].IPettyCashTrans.PCreimburseStatusUpdate
        Dim DA As New SQLEngine.LoanMnt.PCReimburse

        Return DA.PCreimburseStatusUpdate(customClass)
    End Function

    Public Function PCreimburseStatusUpdateACC(ByVal customClass As Parameter.PettyCash) As String Implements [Interface].IPettyCashTrans.PCreimburseStatusUpdateACC
        Dim DA As New SQLEngine.LoanMnt.PCReimburse

        Return DA.PCreimburseStatusUpdateACC(customClass)
    End Function
    Public Function PCreimburseStatusUpdateACCReject(ByVal customClass As Parameter.PettyCash) As String Implements [Interface].IPettyCashTrans.PCreimburseStatusUpdateACCReject
        Dim DA As New SQLEngine.LoanMnt.PCReimburse

        Return DA.PCreimburseStatusUpdateACCReject(customClass)
    End Function

    Public Function GetPettyCashFormKuning(ByVal customClass As IList(Of String), cnn As String) As DataSet Implements [Interface].IPettyCashTrans.GetPettyCashFormKuning
        Dim DASavePCReimburse As New SQLEngine.LoanMnt.PCReimburse
        Return DASavePCReimburse.GetPettyCashFormKuning(customClass, cnn)
    End Function

    Public Function PCreimburseStatusReject(customclass As Parameter.PettyCash) As String Implements [Interface].IPettyCashTrans.PCreimburseStatusReject
        Dim DAPCreimburseStatusReject As New SQLEngine.LoanMnt.PCReimburse
        Return DAPCreimburseStatusReject.PCreimburseStatusReject(customclass)
    End Function
    Public Function PCreimburseCOAUpdate(customclass As Parameter.PettyCash) As String Implements [Interface].IPettyCashTrans.PCreimburseCOAUpdate
        Dim DAPCreimburseCOAUpdate As New SQLEngine.LoanMnt.PCReimburse
        Return DAPCreimburseCOAUpdate.PCreimburseCOAUpdate(customclass)
    End Function
End Class
