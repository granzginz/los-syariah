Imports Maxiloan.Interface
Imports Maxiloan.Parameter

Public Class PettyCash
    Implements IPettyCash


    Public Function GetARecord(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.GetARecord
        Dim oDAPettyCash As SQLEngine.LoanMnt.PettyCash
        Try
            oDAPettyCash = New SQLEngine.LoanMnt.PettyCash
            Return oDAPettyCash.GetARecord(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetPagingTable(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.GetPagingTable
        Dim oDAPettyCash As SQLEngine.LoanMnt.PettyCash
        Try
            oDAPettyCash = New SQLEngine.LoanMnt.PettyCash
            Return oDAPettyCash.GetPagingTable(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetARecordAndDetailTable(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.GetARecordAndDetailTable
        Dim oDAPettyCash As SQLEngine.LoanMnt.PettyCash
        Try
            oDAPettyCash = New SQLEngine.LoanMnt.PettyCash
            Return oDAPettyCash.GetARecordAndDetailTable(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetARecordAndDetailTableACC(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.GetARecordAndDetailTableACC
        Dim oDAPettyCash As SQLEngine.LoanMnt.PettyCash
        Try
            oDAPettyCash = New SQLEngine.LoanMnt.PettyCash
            Return oDAPettyCash.GetARecordAndDetailTableACC(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetDetailTable(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.GetDetailTable
        Dim oDAPettyCash As SQLEngine.LoanMnt.PettyCash
        Try
            oDAPettyCash = New SQLEngine.LoanMnt.PettyCash
            Return oDAPettyCash.GetDetailTable(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetReportDataSet(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.GetReportDataSet
        Dim oDAPettyCash As SQLEngine.LoanMnt.PettyCash
        Try
            oDAPettyCash = New SQLEngine.LoanMnt.PettyCash
            Return oDAPettyCash.GetReportDataSet(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function InqPCReimburse(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.InqPCReimburse
        Dim daInqPCReimburse As New SQLEngine.LoanMnt.PettyCash
        Return daInqPCReimburse.InqPCReimburse(ocustomclass)
    End Function

    Public Function InqPCReimburseACC(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.InqPCReimburseACC
        Dim daInqPCReimburse As New SQLEngine.LoanMnt.PettyCash
        Return daInqPCReimburse.InqPCReimburseACC(ocustomclass)
    End Function

    Public Function getViewPCReimburseLabel(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.getViewPCReimburseLabel
        Dim daViewPCReimburseLabel As New SQLEngine.LoanMnt.PettyCash
        Return daViewPCReimburseLabel.getViewPCReimburseLabel(ocustomclass)
    End Function

    Public Function GetViewPCReimburseGrid(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.GetViewPCReimburseGrid
        Dim daViewPCReimburseGrid As New SQLEngine.LoanMnt.PettyCash
        Return daViewPCReimburseGrid.GetViewPCReimburseGrid(ocustomclass)
    End Function
    Public Function GetViewPCReimburseGridACC(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.GetViewPCReimburseGridACC
        Dim daViewPCReimburseGrid As New SQLEngine.LoanMnt.PettyCash
        Return daViewPCReimburseGrid.GetViewPCReimburseGridACC(ocustomclass)
    End Function

    Public Sub SavePettyCashReversal(ByVal oET As Parameter.PettyCash) Implements [Interface].IPettyCash.SavePettyCashReversal
        Dim oDAPettyCash As SQLEngine.LoanMnt.PettyCash
        Try
            oDAPettyCash = New SQLEngine.LoanMnt.PettyCash
            oDAPettyCash.SavePettyCashReversal(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function GetPettyCashVoucher(ByVal oCustomClass As Parameter.PettyCash) As System.Data.DataSet Implements [Interface].IPettyCash.GetPettyCashVoucher
        Dim oSQLE As New SQLEngine.LoanMnt.PettyCash
        Return oSQLE.GetPettyCashVoucher(oCustomClass)
    End Function

    Public Sub EditCOAPettyCash(oET As Parameter.PettyCash) Implements [Interface].IPettyCash.EditCOAPettyCash
        Dim oDAPettyCash As SQLEngine.LoanMnt.PettyCash
        Try
            oDAPettyCash = New SQLEngine.LoanMnt.PettyCash
            oDAPettyCash.EditCOAPettyCash(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function AprPCReimburse(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.AprPCReimburse
        Dim daInqPCReimburse As New SQLEngine.LoanMnt.PCReimburse
        Return daInqPCReimburse.AprPCReimburse(ocustomclass)
    End Function

    Public Function AprPCReimburseisFinal(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.AprPCReimburseisFinal
        Dim daInqPCReimburse As New SQLEngine.LoanMnt.PCReimburse
        Return daInqPCReimburse.AprPCReimburseisFinal(ocustomclass)
    End Function

    Public Function AprPCReimburseIsValidApproval(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.AprPCReimburseIsValidApproval
        Dim daInqPCReimburse As New SQLEngine.LoanMnt.PCReimburse
        Return daInqPCReimburse.AprPCReimburseIsValidApproval(ocustomclass)
    End Function

    Public Function GetCboUserAprPC(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.GetCboUserAprPC
        Dim daInqPCReimburse As New SQLEngine.LoanMnt.PCReimburse
        Return daInqPCReimburse.GetCboUserAprPC(ocustomclass)
    End Function

    Public Function PCAprReimburseSave(ByVal ocustomclass As Parameter.PettyCash) As String Implements [Interface].IPettyCash.PCAprReimburseSave
        Dim daInqPCReimburse As New SQLEngine.LoanMnt.PCReimburse
        Return daInqPCReimburse.PCAprReimburseSave(ocustomclass)
    End Function

    Public Function PCAprReimburseSaveToNextPerson(ByVal ocustomclass As Parameter.PettyCash) As String Implements [Interface].IPettyCash.PCAprReimburseSaveToNextPerson
        Dim daInqPCReimburse As New SQLEngine.LoanMnt.PCReimburse
        Return daInqPCReimburse.PCAprReimburseSaveToNextPerson(ocustomclass)
    End Function

    Public Function GetCboUserAprPCAll(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.GetCboUserAprPCAll
        Dim daInqPCReimburse As New SQLEngine.LoanMnt.PCReimburse
        Return daInqPCReimburse.GetCboUserAprPCAll(ocustomclass)
    End Function

    Public Function GetARecordAndDetailTableWithSeq(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.GetARecordAndDetailTableWithSeq
        Dim oDAPettyCash As SQLEngine.LoanMnt.PettyCash
        Try
            oDAPettyCash = New SQLEngine.LoanMnt.PettyCash
            Return oDAPettyCash.GetARecordAndDetailTableWithSeq(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetARecordAndDetailTableACCWithSeq(ByVal oET As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.GetARecordAndDetailTableACCWithSeq
        Dim oDAPettyCash As SQLEngine.LoanMnt.PettyCash
        Try
            oDAPettyCash = New SQLEngine.LoanMnt.PettyCash
            Return oDAPettyCash.GetARecordAndDetailTableACCWithSeq(oET)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetViewPCReimburseGridHistoryReject(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.GetViewPCReimburseGridHistoryReject
        Dim daViewPCReimburseGrid As New SQLEngine.LoanMnt.PettyCash
        Return daViewPCReimburseGrid.GetViewPCReimburseGridHistoryReject(ocustomclass)
    End Function

    Public Function GetViewPCReimburseGridHistoryApprovel(ByVal ocustomclass As Parameter.PettyCash) As Parameter.PettyCash Implements [Interface].IPettyCash.GetViewPCReimburseGridHistoryApprovel
        Dim daViewPCReimburseGrid As New SQLEngine.LoanMnt.PettyCash
        Return daViewPCReimburseGrid.GetViewPCReimburseGridHistoryApprovel(ocustomclass)
    End Function


End Class
