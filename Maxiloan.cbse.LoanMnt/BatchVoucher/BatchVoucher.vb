

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class BatchVoucher : Inherits ComponentBase
    Implements IBatchVoucher

    Public Sub BatchVoucherAdd(ByVal oCustomClass As Parameter.BatchVoucher) Implements [Interface].IBatchVoucher.BatchVoucherAdd
        Dim DABAtchVocher As New SQLEngine.LoanMnt.BatchVoucher
        DABAtchVocher.BatchVoucherAdd(oCustomClass)
    End Sub

    Public Sub BatchVoucherDelete(ByVal oCustomClass As Parameter.BatchVoucher) Implements [Interface].IBatchVoucher.BatchVoucherDelete
        Dim DABAtchVocher As New SQLEngine.LoanMnt.BatchVoucher
        DABAtchVocher.BatchVoucherDelete(oCustomClass)
    End Sub

    Public Sub BatchVoucherEdit(ByVal oCustomClass As Parameter.BatchVoucher) Implements [Interface].IBatchVoucher.BatchVoucherEdit
        Dim DABAtchVocher As New SQLEngine.LoanMnt.BatchVoucher
        DABAtchVocher.BatchVoucherEdit(oCustomClass)
    End Sub
    Public Sub BatchVoucherPosted(ByVal oCustomClass As Parameter.BatchVoucher) Implements [Interface].IBatchVoucher.BatchVoucherPosted
        Dim DABAtchVocher As New SQLEngine.LoanMnt.BatchVoucher
        DABAtchVocher.BatchVoucherPosted(oCustomClass)
    End Sub
    Public Function BatchVoucherPaging(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher Implements [Interface].IBatchVoucher.BatchVoucherPaging
        Dim DABAtchVocher As New SQLEngine.LoanMnt.BatchVoucher
        Return DABAtchVocher.BatchVoucherPaging(oCustomClass)
    End Function

    Public Function BatchVoucherView(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher Implements [Interface].IBatchVoucher.BatchVoucherView
        Dim DABAtchVocher As New SQLEngine.LoanMnt.BatchVoucher
        Return DABAtchVocher.BatchVoucherView(oCustomClass)
    End Function

    Public Function CashBankBatchVoucherPaging(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher Implements [Interface].IBatchVoucher.CashBankBatchVoucherPaging
        Dim DABAtchVocher As New SQLEngine.LoanMnt.BatchVoucher
        Return DABAtchVocher.CashBankBatchVoucherPaging(oCustomClass)
    End Function

    Public Sub CashBankBatchVoucherAdd(ByVal oCustomClass As Parameter.BatchVoucher) Implements [Interface].IBatchVoucher.CashBankBatchVoucherAdd
        Dim DABAtchVocher As New SQLEngine.LoanMnt.BatchVoucher
        DABAtchVocher.CashBankBatchVoucherAdd(oCustomClass)
    End Sub

    Public Sub CashBankBatchVoucherEdit(ByVal oCustomClass As Parameter.BatchVoucher) Implements [Interface].IBatchVoucher.CashBankBatchVoucherEdit
        Dim DABAtchVocher As New SQLEngine.LoanMnt.BatchVoucher
        DABAtchVocher.CashBankBatchVoucherEdit(oCustomClass)
    End Sub
    Public Sub CashBankBatchVoucherDelete(ByVal oCustomClass As Parameter.BatchVoucher) Implements [Interface].IBatchVoucher.CashBankBatchVoucherDelete
        Dim DABAtchVocher As New SQLEngine.LoanMnt.BatchVoucher
        DABAtchVocher.CashBankBatchVoucherDelete(oCustomClass)
    End Sub
    Public Function CashBankBatchVoucherView(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher Implements [Interface].IBatchVoucher.CashBankBatchVoucherView
        Dim DABAtchVocher As New SQLEngine.LoanMnt.BatchVoucher
        Return DABAtchVocher.CashBankBatchVoucherView(oCustomClass)
    End Function

    Public Function getTransactionBatchUpload(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher Implements [Interface].IBatchVoucher.getTransactionBatchUpload
        Dim DABAtchVocher As New SQLEngine.LoanMnt.BatchVoucher
        Return DABAtchVocher.getTransactionBatchUpload(oCustomClass)
    End Function

    Public Sub saveSingleBatch(ByVal oCustomClass As Parameter.BatchVoucher) Implements [Interface].IBatchVoucher.saveSingleBatch
        Dim DABAtchVocher As New SQLEngine.LoanMnt.BatchVoucher
        DABAtchVocher.saveSingleBatch(oCustomClass)
    End Sub

    Public Sub DeleteUploadedBatch(ByVal oCustomClass As Parameter.BatchVoucher) Implements [Interface].IBatchVoucher.DeleteUploadedBatch
        Dim DABAtchVocher As New SQLEngine.LoanMnt.BatchVoucher
        DABAtchVocher.DeleteUploadedBatch(oCustomClass)
    End Sub

    Public Function getCompleteBatch(ByVal oCustomClass As Parameter.BatchVoucher) As Parameter.BatchVoucher Implements [Interface].IBatchVoucher.getCompleteBatch
        Dim DABAtchVocher As New SQLEngine.LoanMnt.BatchVoucher
        Return DABAtchVocher.getCompleteBatch(oCustomClass)
    End Function
End Class
