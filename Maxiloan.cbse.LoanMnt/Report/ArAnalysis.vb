
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class ArAnalysis : Inherits ComponentBase
    Implements IArAnalysis

    Public Function ArAnalysisReport(ByVal oCustomClass As Parameter.ArAnalysis) As DataSet Implements [Interface].IArAnalysis.ArAnalysisReport
        Dim oArAnalysisReport As New SQLEngine.LoanMnt.ArAnalysis
        Return oArAnalysisReport.ArAnalysisReport(oCustomClass)
    End Function
    Public Function TerminationReport(ByVal customclass As Parameter.ArAnalysis) As Parameter.ArAnalysis Implements [Interface].IArAnalysis.TerminationReport
        Dim DA As New SQLEngine.LoanMnt.ArAnalysis
        Return DA.TerminationReport(customclass)
    End Function
End Class
