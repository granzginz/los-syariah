
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan
Imports Maxiloan.SQLEngine.LoanMnt
Imports Maxiloan.Parameter


#End Region
Public Class InstallmentDrawdown : Inherits ComponentBase
    Implements IInstallmentDrawdown


    Public Function InstallmentDrawdownApprovalList(ByVal customclass As Parameter.Drawdown) As Parameter.Drawdown Implements [Interface].IInstallmentDrawdown.DrawdownApprovalList
        Dim DataDA As New SQLEngine.LoanMnt.InstallmentDrawdown
        Return DataDA.DrawdownApprovalList(customclass)
    End Function

    Public Function GetCboUserAprPCAll(ByVal customclass As Parameter.Drawdown) As Parameter.Drawdown Implements [Interface].IInstallmentDrawdown.GetCboUserAprPCAll
        Dim DataDA As New SQLEngine.LoanMnt.InstallmentDrawdown
        Return DataDA.GetCboUserAprPCAll(customclass)
    End Function

    Public Function GetCboUserApproval(ByVal customclass As Parameter.Drawdown) As Parameter.Drawdown Implements [Interface].IInstallmentDrawdown.GetCboUserApproval
        Dim DataDA As New SQLEngine.LoanMnt.InstallmentDrawdown
        Return DataDA.GetCboUserApproval(customclass)
    End Function

    Public Function ApproveSaveToNextPerson(ByVal customclass As Parameter.Drawdown) As String Implements [Interface].IInstallmentDrawdown.ApproveSaveToNextPerson
        Dim DataDA As New SQLEngine.LoanMnt.InstallmentDrawdown
        Return DataDA.ApproveSaveToNextPerson(customclass)
    End Function

    Public Function ApproveSave(ByVal customclass As Parameter.Drawdown) As String Implements [Interface].IInstallmentDrawdown.ApproveSave
        Dim DataDA As New SQLEngine.LoanMnt.InstallmentDrawdown
        Return DataDA.ApproveSave(customclass)
    End Function

    Public Function ApproveisFinal(ByVal customclass As Parameter.Drawdown) As Parameter.Drawdown Implements [Interface].IInstallmentDrawdown.ApproveisFinal
        Dim DataDA As New SQLEngine.LoanMnt.InstallmentDrawdown
        Return DataDA.ApproveisFinal(customclass)
    End Function

    Public Function ApproveIsValidApproval(ByVal customclass As Parameter.Drawdown) As Parameter.Drawdown Implements [Interface].IInstallmentDrawdown.ApproveIsValidApproval
        Dim DataDA As New SQLEngine.LoanMnt.InstallmentDrawdown
        Return DataDA.ApproveIsValidApproval(customclass)
    End Function


End Class
