
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class InventorySelling
    Implements IInvSelling


    Public Function InvSellingReceiveViewRepo(ByVal customclass As Parameter.InvSelling) As Parameter.InvSelling Implements [Interface].IInvSelling.InvSellingReceiveViewRepo
        Dim DAInvSellingReceive As New SQLEngine.LoanMnt.InventorySelling
        Return DAInvSellingReceive.InvSellingReceiveViewRepo(customclass)
    End Function

    Public Function GetInvSellingReceive(ByVal customClass As Parameter.InvSelling) As Parameter.InvSelling Implements [Interface].IInvSelling.GetInvSellingReceive
        Dim DAInvSellingReceive As New SQLEngine.LoanMnt.InventorySelling
        Return DAInvSellingReceive.GetInvSellingReceive(customClass)

    End Function

    Public Function GetInvSellingReceiveApproval(ByVal customClass As Parameter.InvSelling) As Parameter.InvSelling Implements [Interface].IInvSelling.GetInvSellingReceiveApproval
        Dim DAInvSellingReceive As New SQLEngine.LoanMnt.InventorySelling
        Return DAInvSellingReceive.GetInvSellingReceiveApproval(customClass)

    End Function

    Public Function InvSellingReceiveView(ByVal customclass As Parameter.InvSelling) As Parameter.InvSelling Implements [Interface].IInvSelling.InvSellingReceiveView
        Dim DAInvSellingReceive As New SQLEngine.LoanMnt.InventorySelling
        Return DAInvSellingReceive.InvSellingReceiveView(customclass)

    End Function

    Public Function SavingInventorySellingReceive(oCustomClass As Parameter.InvSelling) As String Implements [Interface].IInvSelling.SavingInventorySellingReceive
        Dim DAInvSellingReceive As New SQLEngine.LoanMnt.InventorySelling
        Return DAInvSellingReceive.SavingInventorySellingReceive(oCustomClass)
    End Function

    Public Sub SavingInventorySellingExec(oCustomClass As Parameter.InvSelling) Implements [Interface].IInvSelling.SavingInventorySellingExec
        Dim DAInvSellingReceive As New SQLEngine.LoanMnt.InventorySelling
        DAInvSellingReceive.SavingInventorySellingExec(oCustomClass)
    End Sub

    Public Function SavingInventorySellingReceiveApproval(ByVal oCustomClass As Parameter.InvSelling) As String Implements [Interface].IInvSelling.SavingInventorySellingReceiveApproval
        Dim DAInvSellingReceive As New SQLEngine.LoanMnt.InventorySelling
        Return DAInvSellingReceive.SavingInventorySellingReceiveApproval(oCustomClass)
    End Function
End Class
