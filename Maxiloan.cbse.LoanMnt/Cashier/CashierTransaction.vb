
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class CashierTransaction
    Implements ICashierTransaction
    Public Function ListCashierHistory(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction Implements ICashierTransaction.ListCashierHistory
        Dim DAListCashierHistory As New SQLEngine.LoanMnt.CashierTransaction
        Return DAListCashierHistory.ListCashierTransactionHistory(oCustomClass)
    End Function

    Public Sub CashierOpen(ByVal oCustomClass As Parameter.CashierTransaction) Implements ICashierTransaction.CashierOpen
        Dim DACashierOpen As New SQLEngine.LoanMnt.CashierTransaction
        DACashierOpen.CashierOpen(oCustomClass)
    End Sub

    Public Sub CashierClose(ByVal oCustomClass As Parameter.CashierTransaction) Implements ICashierTransaction.CashierClose
        Dim DACashierClose As New SQLEngine.LoanMnt.CashierTransaction
        DACashierClose.CashierClose(oCustomClass)
    End Sub

    Public Function CashierCloseList(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction Implements ICashierTransaction.CashierCloseList
        Dim DACashierCloseList As New SQLEngine.LoanMnt.CashierTransaction
        Return DACashierCloseList.CashierCloseList(oCustomClass)
    End Function

    Public Function CHCloseList(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction Implements ICashierTransaction.CHCloseList
        Dim DACHCloseList As New SQLEngine.LoanMnt.CashierTransaction
        Return DACHCloseList.CHCloseList(oCustomClass)
    End Function

    Public Sub HeadCashierClose(ByVal oCustomClass As Parameter.CashierTransaction) Implements ICashierTransaction.HeadCashierClose
        Dim DACHCloseList As New SQLEngine.LoanMnt.CashierTransaction
        DACHCloseList.HeadCashierClose(oCustomClass)
    End Sub

    Public Function CheckHeadCashier(ByVal oCustomclass As Parameter.CashierTransaction) As Boolean Implements ICashierTransaction.CheckHeadCashier
        Dim DACHCloseList As New SQLEngine.LoanMnt.CashierTransaction
        Return DACHCloseList.CheckHeadCashier(oCustomclass)
    End Function

    Public Function CheckAllCashierClose(ByVal oCustomclass As Parameter.CashierTransaction) As Boolean Implements ICashierTransaction.CheckAllCashierClose
        Dim DACHCloseList As New SQLEngine.LoanMnt.CashierTransaction
        Return DACHCloseList.CheckAllCashierClose(oCustomclass)
    End Function

    Public Function GetCashier(ByVal ocustomclass As Parameter.CashierTransaction) As System.Data.DataTable Implements [Interface].ICashierTransaction.GetCashier
        Dim DAGetCashier As New SQLEngine.LoanMnt.CashierTransaction
        Return DAGetCashier.GetCashier(ocustomclass)
    End Function

    Public Sub HeadCashierBatchProcess(ByVal ocustomclass As Parameter.CashierTransaction) Implements [Interface].ICashierTransaction.HeadCashierBatchProcess
        Dim DACHCloseList As New SQLEngine.LoanMnt.CashierTransaction
        DACHCloseList.HeadCashierBatchProcess(ocustomclass)
    End Sub

    Public Function GLInterfaceDetail(ByVal ocustomclass As Parameter.CashierTransaction) As System.Data.DataTable Implements [Interface].ICashierTransaction.GLInterfaceDetail
        Dim DACHCloseList As New SQLEngine.LoanMnt.CashierTransaction
        Return DACHCloseList.GLInterfaceDetail(ocustomclass)
    End Function

    Public Function GLInterfaceHeader(ByVal ocustomclass As Parameter.CashierTransaction) As System.Data.DataTable Implements [Interface].ICashierTransaction.GLInterfaceHeader
        Dim DACHCloseList As New SQLEngine.LoanMnt.CashierTransaction
        Return DACHCloseList.GLInterfaceHeader(ocustomclass)
    End Function

    Public Function FileAllocationGLInterface(ByVal strConnection As String) As String Implements [Interface].ICashierTransaction.FileAllocationGLInterface
        Dim DACHCloseList As New SQLEngine.LoanMnt.CashierTransaction
        Return DACHCloseList.FileAllocationGLInterface(strConnection)
    End Function

    Public Function GetTransaction(ByVal customclass As Parameter.CashierTransaction) As System.Data.DataTable Implements [Interface].ICashierTransaction.GetTransaction
        Dim DAGetTrans As New SQLEngine.LoanMnt.CashierTransaction
        Return DAGetTrans.GetTransaction(customclass)
    End Function

    Public Function GetOutProc(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction Implements [Interface].ICashierTransaction.GetOutProc
        Dim DAGetOutProct As New SQLEngine.LoanMnt.CashierTransaction
        Return DAGetOutProct.GetOutProc(oCustomClass)
    End Function

    Public Function GetListOutProc(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction Implements [Interface].ICashierTransaction.GetListOutProc
        Dim DAGetListOutProct As New SQLEngine.LoanMnt.CashierTransaction
        Return DAGetListOutProct.GetListOutProc(oCustomClass)
    End Function

    Public Function getCashBankBalance(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction Implements [Interface].ICashierTransaction.getCashBankBalance
        Dim oSQLE As New SQLEngine.LoanMnt.CashierTransaction
        Return oSQLE.getCashBankBalance(oCustomClass)
    End Function

    Public Function getCashBankBalanceHistory(ByVal oCustomClass As Parameter.CashierTransaction) As Parameter.CashierTransaction Implements [Interface].ICashierTransaction.getCashBankBalanceHistory
        Dim oSQLE As New SQLEngine.LoanMnt.CashierTransaction
        Return oSQLE.getCashBankBalanceHistory(oCustomClass)
    End Function

End Class
