
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class InsClaimReceive
    Implements IInsClaimReceive

    Public Function InsClaimInfo(ByVal oCustomClass As Parameter.InsClaimReceive) As Parameter.InsClaimReceive Implements [Interface].IInsClaimReceive.InsClaimInfo
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InsClaimReceive
        Return DAPostingPayment.InsClaimInfo(oCustomClass)
    End Function

    Public Function SaveInsClaimReceive(ByVal ocustomClass As Parameter.InsClaimReceive) As Parameter.InsClaimReceive Implements [Interface].IInsClaimReceive.SaveInsClaimReceive
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InsClaimReceive
        Return DAPostingPayment.SaveInsClaimReceive(ocustomClass)
    End Function


End Class
