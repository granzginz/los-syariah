
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class DChange
    Implements IDChange

#Region "Request"
    Public Function GetList(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetList
        Dim DAEx As New SQLEngine.LoanMnt.DChange
        Return DAEx.GetList(oCustomClass)
    End Function

    Public Function GetListAmor(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListAmor
        Dim DAList As New SQLEngine.LoanMnt.DChange
        Return DAList.GetListAmor(oCustomClass)
    End Function

    Public Sub DueDateChangeRequest(ByVal oCustomClass As Parameter.DChange) Implements [Interface].IDChange.DueDateChangeRequest
        Dim DReq As New SQLEngine.LoanMnt.DChange
        DReq.SaveChangeRequest(oCustomClass)
    End Sub

    Public Function GetMaxDate(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetMaxDate
        Dim DAList As New SQLEngine.LoanMnt.DChange
        Return DAList.GetDueDate(oCustomClass)
    End Function

#End Region

#Region "Execute"

    Public Function GetListExec(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListExec
        Dim DAListE As New SQLEngine.LoanMnt.DChange
        Return DAListE.GetListExec(oCustomClass)
    End Function

    Public Sub DueDateChangeExec(ByVal oCustomClass As Parameter.DChange) Implements [Interface].IDChange.DueDateChangeExec
        Dim DExec As New SQLEngine.LoanMnt.DChange
        DExec.SaveChangeExec(oCustomClass)
    End Sub
#End Region

#Region "Modal Kerja"
	Public Function GetListModalKerja(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListModalKerja
		Dim DAEx As New SQLEngine.LoanMnt.DChange
		Return DAEx.GetListModalKerja(oCustomClass)
	End Function

	Public Function GetListAmorModalKerja(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListAmorModalKerja
		Dim DAList As New SQLEngine.LoanMnt.DChange
		Return DAList.GetListAmorModalKerja(oCustomClass)
	End Function

	Public Function GetListAmorModalKerjaFromTable(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListAmorModalKerjaFromTable
		Dim DAList As New SQLEngine.LoanMnt.DChange
		Return DAList.GetListAmorModalKerjaFromTable(oCustomClass)
	End Function

	Public Function CreateAmorModalKerja(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.CreateAmorModalKerja
		Dim DAList As New SQLEngine.LoanMnt.DChange
		Return DAList.CreateAmorModalKerja(oCustomClass)
	End Function

	Public Sub MDKJDueDateChangeRequest(ByVal oCustomClass As Parameter.DChange) Implements [Interface].IDChange.MDKJDueDateChangeRequest
		Dim DReq As New SQLEngine.LoanMnt.DChange
		DReq.MDKJSaveChangeRequest(oCustomClass)
	End Sub

	'Public Function GetMaxDate(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetMaxDate
	'	Dim DAList As New SQLEngine.LoanMnt.DChange
	'	Return DAList.GetDueDate(oCustomClass)
	'End Function

	Public Function GetListExecMDKJ(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListExecMDKJ
		Dim DAListE As New SQLEngine.LoanMnt.DChange
		Return DAListE.GetListExecMDKJ(oCustomClass)
	End Function

    Public Sub DueDateChangeExecMDKJ(ByVal oCustomClass As Parameter.DChange) Implements [Interface].IDChange.DueDateChangeExecMDKJ
        Dim DExec As New SQLEngine.LoanMnt.DChange
        DExec.SaveChangeExecMDKJ(oCustomClass)
    End Sub

    Public Function GetListAmorMDKJ(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListAmorMDKJ
        Dim DAList As New SQLEngine.LoanMnt.DChange
        Return DAList.GetListAmorMDKJ(oCustomClass)
    End Function
#End Region

#Region "Factoring"
    Public Function GetListFactoring(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListFactoring
		Dim DAEx As New SQLEngine.LoanMnt.DChange
		Return DAEx.GetListFactoring(oCustomClass)
	End Function

	Public Function GetListAmorFactoring(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListAmorFactoring
		Dim DAList As New SQLEngine.LoanMnt.DChange
		Return DAList.GetListAmorFactoring(oCustomClass)
	End Function

	Public Function CreateAmorFactoring(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.CreateAmorFactoring
		Dim DAList As New SQLEngine.LoanMnt.DChange
		Return DAList.CreateAmorFactoring(oCustomClass)
	End Function

	Public Function GetListAmorFactoringFromTable(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListAmorFactoringFromTable
		Dim DAList As New SQLEngine.LoanMnt.DChange
		Return DAList.GetListAmorFactoringFromTable(oCustomClass)
	End Function

	Public Function GetListInvoiceFactoring(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListInvoiceFactoring
		Dim DAList As New SQLEngine.LoanMnt.DChange
		Return DAList.GetListInvoiceFactoring(oCustomClass)
	End Function

	Public Sub DueDateChangeRequestFactoring(ByVal oCustomClass As Parameter.DChange) Implements [Interface].IDChange.DueDateChangeRequestFactoring
		Dim DReq As New SQLEngine.LoanMnt.DChange
		DReq.SaveChangeRequestFactoring(oCustomClass)
	End Sub

    Public Function GetListExecFactoring(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListExecFactoring
        Dim DAListE As New SQLEngine.LoanMnt.DChange
        Return DAListE.GetListExecFactoring(oCustomClass)
    End Function

    Public Function GetListExecFactoringApproval(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListExecFactoringApproval
        Dim DAListE As New SQLEngine.LoanMnt.DChange
        Return DAListE.GetListExecFactoringApproval(oCustomClass)
    End Function

    Public Sub DueDateChangeExecFactoring(ByVal oCustomClass As Parameter.DChange) Implements [Interface].IDChange.DueDateChangeExecFactoring
        Dim DExec As New SQLEngine.LoanMnt.DChange
        DExec.SaveChangeExecFactoring(oCustomClass)
    End Sub

    Public Function GetListAmorFACT(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListAmorFACT
        Dim DAList As New SQLEngine.LoanMnt.DChange
        Return DAList.GetListAmorFACT(oCustomClass)
    End Function

    Public Function GetListFactoringView(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListFactoringView
        Dim DAEx As New SQLEngine.LoanMnt.DChange
        Return DAEx.GetListFactoringView(oCustomClass)
    End Function
    Public Function GetListFactoringViewApproval(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListFactoringViewApproval
        Dim DAEx As New SQLEngine.LoanMnt.DChange
        Return DAEx.GetListFactoringViewApproval(oCustomClass)
    End Function
#End Region

    Public Function GetListFactAndMU(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListFactAndMU
        Dim DAEx As New SQLEngine.LoanMnt.DChange
        Return DAEx.GetListFactAndMU(oCustomClass)
    End Function

    Public Function GetListReschedulingApproval(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListReschedulingApproval
        Dim DAEx As New SQLEngine.LoanMnt.DChange
        Return DAEx.GetListReschedulingApproval(oCustomClass)
    End Function

    Public Function GetListInvoiceFactoringView(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListInvoiceFactoringView
        Dim DAList As New SQLEngine.LoanMnt.DChange
        Return DAList.GetListInvoiceFactoringView(oCustomClass)
    End Function

    Public Function GetListAmorFactoringView(ByVal oCustomClass As Parameter.DChange) As Parameter.DChange Implements [Interface].IDChange.GetListAmorFactoringView
        Dim DAList As New SQLEngine.LoanMnt.DChange
        Return DAList.GetListAmorFactoringView(oCustomClass)
    End Function
End Class
