

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class AChangeWOP : Inherits ComponentBase
    Implements IAChangeWOP


    Public Function GetListWOP(ByVal oCustomClass As Parameter.AChangeWOP) As Parameter.AChangeWOP Implements [Interface].IAChangeWOP.GetListWOP
        Dim DAWOP As New SQLEngine.LoanMnt.AChangeWOP
        Return DAWOP.GetListWOP(oCustomClass)
    End Function

    Public Sub WOPChange(ByVal oCustomClass As Parameter.AChangeWOP) Implements [Interface].IAChangeWOP.WOPChange
        Dim WOP As New SQLEngine.LoanMnt.AChangeWOP
        WOP.WOPChange(oCustomClass)
    End Sub
End Class
