

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class InstallReceive
    Implements IInstallRcv

    Public Function InstallmentReceive(ByVal oCustomClass As Parameter.InstallRcv) As Integer Implements [Interface].IInstallRcv.InstallmentReceive
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        Return DAPostingPayment.SavingPostingPayment(oCustomClass)
    End Function

    Public Function IsMaxBackDate(ByVal strConnection As String, ByVal ValueDate As Date, ByVal BusinessDate As Date) As Boolean Implements [Interface].IInstallRcv.IsMaxBackDate
        Dim DAMaxBackDate As New SQLEngine.LoanMnt.InstallRcv
        If DateDiff(DateInterval.Day, ValueDate, BusinessDate) < DAMaxBackDate.MaxBackDate(strConnection) Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function IsValidLastPayment(ByVal strConnection As String, ByVal ApplicationID As String, ByVal ValueDate As Date) As Boolean Implements [Interface].IInstallRcv.IsValidLastPayment
        Dim DAIsValidLastPayment As New SQLEngine.LoanMnt.InstallRcv
        Return DAIsValidLastPayment.IsValidLastPayment(strConnection, ApplicationID, ValueDate)
    End Function

    Public Function IsLastPayment(ByVal oCustomClass As Parameter.InstallRcv) As Boolean Implements [Interface].IInstallRcv.IsLastPayment
        Dim DAPrintTTU As New SQLEngine.LoanMnt.InstallRcv
        Return DAPrintTTU.IsLastPayment(oCustomClass)
    End Function

    Public Function ListKwitansiInstallment(ByVal oCustomClass As Parameter.InstallRcv) As System.Data.DataSet Implements [Interface].IInstallRcv.ListKwitansiInstallment
        Dim SQLE As New SQLEngine.LoanMnt.InstallRcv
        Return SQLE.ListKwitansiInstallment(oCustomClass)
    End Function

    Public Function SavePrintKwitansiInstallment(ByVal oCustomClass As Parameter.InstallRcv) As Parameter.InstallRcv Implements [Interface].IInstallRcv.SavePrintKwitansiInstallment
        Dim SQLE As New SQLEngine.LoanMnt.InstallRcv
        Return SQLE.SavePrintKwitansiInstallment(oCustomClass)
    End Function

    Public Function ReportKwitansiInstallment(ByVal oCustomClass As Parameter.InstallRcv) As System.Data.DataSet Implements [Interface].IInstallRcv.ReportKwitansiInstallment
        Dim SQLE As New SQLEngine.LoanMnt.InstallRcv
        Return SQLE.ReportKwitansiInstallment(oCustomClass)
    End Function

    Public Overloads Function PrintTTU(ByVal strConnection As String, ByVal ApplicationID As String, ByVal branchID As String, ByVal BusinessDate As Date, ByVal HistorySequenceNo As Integer, ByVal LoginID As String) As System.Data.DataSet Implements [Interface].IInstallRcv.PrintTTU
        Dim DAPrintTTU As New SQLEngine.LoanMnt.InstallRcv
        Return DAPrintTTU.PrintTTU(strConnection, ApplicationID, branchID, BusinessDate, HistorySequenceNo, LoginID)
    End Function

    Public Overloads Function PrintTTU(ByVal strConnection As String, ByVal ApplicationID As String, ByVal BranchID As String, ByVal BusinessDate As Date, ByVal LoginID As String) As System.Data.DataSet Implements [Interface].IInstallRcv.PrintTTU
        Dim DAPrintTTU As New SQLEngine.LoanMnt.InstallRcv
        Return DAPrintTTU.PrintTTU(strConnection, ApplicationID, BranchID, BusinessDate, LoginID)
    End Function

    Public Overloads Function PrintInstallmentReceive(ByVal strConnection As String, ByVal ApplicationID As String, ByVal BranchID As String, ByVal BusinessDate As Date, ByVal LoginID As String) As System.Data.DataSet Implements [Interface].IInstallRcv.PrintInstallmentReceive
        Dim DAPrintTTU As New SQLEngine.LoanMnt.InstallRcv
        Return DAPrintTTU.PrintInstallmentReceive(strConnection, ApplicationID, BranchID, BusinessDate, LoginID)
    End Function

    Public Function ReportKwitansiInstallment2(ByVal oCustomClass As Parameter.InstallRcv) As System.Data.DataSet Implements [Interface].IInstallRcv.ReportKwitansiInstallment2
        Dim SQLE As New SQLEngine.LoanMnt.InstallRcv
        Return SQLE.ReportKwitansiInstallment2(oCustomClass)
    End Function
    Public Function ReportKwitansiInstallmentWithVoucherNo(customclass As Parameter.InstallRcv) As System.Data.DataSet Implements [Interface].IInstallRcv.ReportKwitansiInstallmentWithVoucherNo
        Dim SQLE As New SQLEngine.LoanMnt.InstallRcv
        Return SQLE.ReportKwitansiInstallmentWithVoucherNo(customclass)
    End Function
    Public Function InstallRcvInstallmentSchedulePaging(oCustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging Implements [Interface].IInstallRcv.InstallRcvInstallmentSchedulePaging
        Dim SQLE As New SQLEngine.LoanMnt.InstallRcv
        Return SQLE.InstallRcvInstallmentSchedulePaging(oCustomClass)
    End Function


    Public Function AdvanceInstallmet(CustomCLass As Parameter.InstallRcv) As Parameter.InstallRcv Implements [Interface].IInstallRcv.AdvanceInstallmet
        Dim SQLE As New SQLEngine.LoanMnt.InstallRcv
        Return SQLE.AdvanceInstallmet(CustomCLass)
    End Function

    Public Function AlokasiAdvanceInstallmet(CustomClass As Parameter.InstallRcv) As String Implements [Interface].IInstallRcv.AlokasiAdvanceInstallmet
        Dim SQLE As New SQLEngine.LoanMnt.InstallRcv
        Return SQLE.AlokasiAdvanceInstallmet(CustomClass)
    End Function
    Public Function GetSP(customclass As Parameter.InstallRcv) As Parameter.InstallRcv Implements [Interface].IInstallRcv.GetSP
        Dim SQLE As New SQLEngine.LoanMnt.InstallRcv
        Return SQLE.GetSP(customclass)
    End Function

    Public Function InstallmentReceiveAdvIns(oCustomClass As Parameter.InstallRcv) As Integer Implements [Interface].IInstallRcv.InstallmentReceiveAdvIns
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        Return DAPostingPayment.InstallmentReceiveAdvIns(oCustomClass)
    End Function

    Public Function ReportKwitansiInstallmentAdvIns(oCustomClass As Parameter.InstallRcv) As System.Data.DataSet Implements [Interface].IInstallRcv.ReportKwitansiInstallmentAdvIns
        Dim SQLE As New SQLEngine.LoanMnt.InstallRcv
        Return SQLE.ReportKwitansiInstallmentAdvIns(oCustomClass)
    End Function

    Public Function InstallRCVKorAngsSave(oCustomClass As Parameter.InstallRcv) As Integer Implements [Interface].IInstallRcv.InstallRCVKorAngsSave
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        Return DAPostingPayment.InstallRCVKorAngsSave(oCustomClass)
    End Function


    Public Function InstallRCVTemp(oCustomClass As Parameter.InstallRcv) As Integer Implements [Interface].IInstallRcv.InstallRCVTemp
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        Return DAPostingPayment.InstallRCVTemp(oCustomClass)
    End Function
    Public Function InstallRCVBAFactoring(oCustomClass As Parameter.InstallRcv) As Integer Implements [Interface].IInstallRcv.InstallRCVBAFactoring
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        Return DAPostingPayment.InstallRCVBAFactoring(oCustomClass)
    End Function
    Public Function InstallRCVPPH(oCustomClass As Parameter.InstallRcv) As Integer Implements [Interface].IInstallRcv.InstallRCVPPH
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        Return DAPostingPayment.InstallRCVPPH(oCustomClass)
    End Function

    Public Sub SaveOtorisasiPPH(oCustomClass As Parameter.InstallRcv) Implements [Interface].IInstallRcv.SaveOtorisasiPPH
        Dim SaveOtorisasiPPH As New SQLEngine.LoanMnt.InstallRcv
        SaveOtorisasiPPH.SaveOtorisasiPPH(oCustomClass)
    End Sub
    Public Sub RejectOtorisasiPPH(oCustomClass As Parameter.InstallRcv) Implements [Interface].IInstallRcv.RejectOtorisasiPPH
        Dim RejectOtorisasiPPH As New SQLEngine.LoanMnt.InstallRcv
        RejectOtorisasiPPH.RejectOtorisasiPPH(oCustomClass)
    End Sub

    Public Function InstallRCVBAModalKerja(oCustomClass As Parameter.InstallRcv) As Integer Implements [Interface].IInstallRcv.InstallRCVBAModalKerja
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        Return DAPostingPayment.InstallRCVBAModalKerja(oCustomClass)
    End Function
    Public Function TransaksiPending(customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging Implements [Interface].IInstallRcv.TransaksiPending
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        Return DAPostingPayment.TransaksiPending(customClass)
    End Function

    Public Function DetailTransaction(customClass As Parameter.InstallRcv) As System.Data.DataTable Implements [Interface].IInstallRcv.DetailTransaction
        Dim DtlTransaction As New SQLEngine.LoanMnt.InstallRcv
        Return DtlTransaction.DetailTransaction(customClass)
    End Function
    Public Function InstallRCVBATemp(oCustomClass As Parameter.InstallRcv) As Integer Implements [Interface].IInstallRcv.InstallRCVBATemp
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        Return DAPostingPayment.InstallRCVBATemp(oCustomClass)
    End Function

    Public Function GetGeneralPaging(CustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging Implements [Interface].IInstallRcv.GetGeneralPaging
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        Return DAPostingPayment.GetGeneralPaging(CustomClass)

    End Function

    Public Sub RejectOtorisasi(oCustomClass As Parameter.InstallRcv) Implements [Interface].IInstallRcv.RejectOtorisasi
        Dim RejectOtor As New SQLEngine.LoanMnt.InstallRcv
        RejectOtor.RejectOtorisasi(oCustomClass)
    End Sub

    Public Sub SaveOtorisasi(oCustomClass As Parameter.InstallRcv) Implements [Interface].IInstallRcv.SaveOtorisasi
        Dim SaveOtorisasi As New SQLEngine.LoanMnt.InstallRcv
        SaveOtorisasi.SaveOtorisasi(oCustomClass)
    End Sub

    Public Overloads Function PrintTTU(strConnection As String, ApplicationID As String, BranchID As String, BusinessDate As Date, HistorySequenceNo As Integer, LoginID As String, InsSeqNo As Integer) As System.Data.DataSet Implements [Interface].IInstallRcv.PrintTTU
        Dim DAPrintTTU As New SQLEngine.LoanMnt.InstallRcv
        Return DAPrintTTU.PrintTTU(strConnection, ApplicationID, BranchID, BusinessDate, HistorySequenceNo, LoginID, InsSeqNo)
    End Function

    Public Function InstallRCVCollOtor(oCustomClass As Parameter.InstallRcv) As Integer Implements [Interface].IInstallRcv.InstallRCVCollOtor
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        Return DAPostingPayment.InstallRCVCollOtor(oCustomClass)
    End Function

    Public Sub EditOtorisasi(oCustomClass As Parameter.InstallRcv) Implements [Interface].IInstallRcv.EditOtorisasi
        Dim EditOtor As New SQLEngine.LoanMnt.InstallRcv
        EditOtor.EditOtorisasi(oCustomClass)
    End Sub


    Public Function DoUpload3rdFile(cnn As String, rows As Parameter.InstallRcv3rd, sesId As String) As String Implements [Interface].IInstallRcv.DoUpload3rdFile
        Dim _installRcv3rd As New SQLEngine.LoanMnt.InstallRcv3rdDA

        Select Case rows.ViaType
            Case Parameter.EnViaType.Indomaret
                Return _installRcv3rd.DoUploadFileInstallment(cnn, rows, sesId, "SPINSTALLRCVUPLOADINDOMARET")
            Case Parameter.EnViaType.POS
                Return _installRcv3rd.DoUploadFileInstallment(cnn, rows, sesId, "spInstallRcvUploadPOS")
            Case Parameter.EnViaType.Finnet
                Return _installRcv3rd.DoUploadFileInstallment(cnn, rows, sesId, "spInstallRcvUploadFIN")
            Case Parameter.EnViaType.Permata
                Return _installRcv3rd.DoUploadFileInstallment(cnn, rows, sesId, "spInstallRcvUploadPermata")
            Case Parameter.EnViaType.BCA
                Return _installRcv3rd.DoUploadFileInstallment(cnn, rows, sesId, "spInstallRcvUploadBCA")
            Case Else
                Throw New Exception(rows.ViaType.ToString + " Belum ada prosesnya")
        End Select
 
        Throw New Exception(rows.ViaType.ToString + " Belum ada prosesnya")
    End Function

    Public Function GetUploadFilePage(cnn As String, currentPage As Integer, pageSize As Integer, sesId As String, vtype As Parameter.EnViaType) As Parameter.InstallRcv3rd Implements [Interface].IInstallRcv.GetUploadFilePage
        Dim _installRcv3rd As New SQLEngine.LoanMnt.InstallRcv3rdDA
        Dim sp_name = ""
        Select Case vtype
            Case Parameter.EnViaType.Indomaret
                sp_name = "spTempINDOMARETpage"
            Case Parameter.EnViaType.POS
                sp_name = "spTempPOSpage"
            Case Parameter.EnViaType.Finnet
                sp_name = "spTempFINpage"
            Case Parameter.EnViaType.Permata
                sp_name = "spTempPermatapage"
            Case Parameter.EnViaType.BCA
                sp_name = "spTempBCApage"
            Case Else
                Throw New Exception(vtype.ToString + " Belum ada prosesnya")
        End Select
        Return _installRcv3rd.GetUploadFilePage(cnn, currentPage, pageSize, sesId, sp_name)
    End Function


    Public Function GetUploadFilePage(cnn As String, currentPage As Integer, pageSize As Integer, tedate As DateTime, vtype As Parameter.EnViaType) As Parameter.InstallRcv3rd Implements [Interface].IInstallRcv.GetUploadFilePage
        Dim _installRcv3rd As New SQLEngine.LoanMnt.InstallRcv3rdDA
        Dim sp_name = ""
        Select Case vtype
            Case Parameter.EnViaType.Indomaret
                sp_name = "spListPostIndomaretpage"
            Case Parameter.EnViaType.POS
                sp_name = "spListPostPOSpage"
            Case Parameter.EnViaType.Finnet
                sp_name = "spListPostFINpage"
            Case Parameter.EnViaType.Permata
                sp_name = "spListPostPermatapage"
            Case Parameter.EnViaType.BCA
                sp_name = "spListPostBCApage"
            Case Else
                Throw New Exception(vtype.ToString + " Belum ada prosesnya")
        End Select
        Return _installRcv3rd.GetUploadFilePage(cnn, currentPage, pageSize, tedate, sp_name) 
    End Function


    Public Function PostingInstallmentUpload(cnn As String, compid As String, branchid As String, valuedate As DateTime, bsnDate As DateTime, login As String, vtype As Parameter.EnViaType) As String Implements [Interface].IInstallRcv.PostingInstallmentUpload
        Dim _installRcv3rd As New SQLEngine.LoanMnt.InstallRcv3rdDA
        Dim sp_name = ""

        Select Case vtype
            Case Parameter.EnViaType.Indomaret
                sp_name = "spPostingInstallmentIndomaret"
            Case Parameter.EnViaType.POS
                sp_name = "spPostingInstallmentPos"
            Case Parameter.EnViaType.Finnet
                sp_name = "spPostingInstallmentFinnet"
            Case Parameter.EnViaType.Permata
                sp_name = "spPostingInstallmentPermata"
            Case Parameter.EnViaType.BCA
                sp_name = "spPostingInstallmentBCA"
            Case Else
                Throw New Exception(vtype.ToString + " Belum ada prosesnya")
        End Select
        Return _installRcv3rd.PostingInstallmentUpload(cnn, compid, branchid, valuedate, bsnDate, login, sp_name)
    End Function
     

    Public Function GeneratePaymentGatewayFile(cnn As String, processDate As DateTime, dir As String) As Boolean Implements [Interface].IInstallRcv.GeneratePaymentGatewayFile

        Dim _installRcv3rd As New SQLEngine.LoanMnt.InstallRcv3rdDA
        Dim result = _installRcv3rd.GeneratePaymentGatewayFile(cnn, processDate)
        result.FileDirectory = dir
        Try

            result.BuildUploadFiles()
            _installRcv3rd.PaymentGatewayAgreementLog(cnn, result)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function GetPaymentGatewayFile(cnn As String, processDate As DateTime) As DataTable Implements [Interface].IInstallRcv.GetPaymentGatewayFile
        Dim _installRcv3rd As New SQLEngine.LoanMnt.InstallRcv3rdDA
        Return _installRcv3rd.GetPaymentGatewayAgreementLog(cnn, processDate)
    End Function


    Public Function DoPostingInstallmentUpload(cnn As String, branchho As String, processDate As DateTime, businessDate As DateTime, companyId As String, vtype As Parameter.EnViaType, listKey As IList(Of String)) As String Implements [Interface].IInstallRcv.DoPostingInstallmentUpload
        Dim _installRcv3rd As New SQLEngine.LoanMnt.InstallRcv3rdDA
        Return _installRcv3rd.DoPostingInstallmentUpload(cnn, branchho, processDate, businessDate, companyId, vtype, listKey)
    End Function


    Public Function DoUploadAgreementVirtualAccount(cnn As String, rows As Parameter.AgreementVirtualAcc) As String Implements [Interface].IInstallRcv.DoUploadAgreementVirtualAccount
        Dim _installRcv3rd As New SQLEngine.LoanMnt.InstallRcv3rdDA
        Try
            Return _installRcv3rd.DoUploadAgreementVirtualAccount(cnn, rows)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try 
    End Function

    Public Function InstallmentReceiveMobile(ByVal oCustomClass As Parameter.InstallRcv) As Integer Implements [Interface].IInstallRcv.InstallmentReceiveMobile
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        Return DAPostingPayment.SavingPostingPaymentMobile(oCustomClass)
    End Function

    Public Function GetComboCabang(ByVal oCustomClass As Parameter.InstallRcv) As System.Data.DataTable Implements [Interface].IInstallRcv.GetComboCabang
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        Return DAPostingPayment.GetComboCabang(oCustomClass)
    End Function

    Public Function GetComboDepartemen(ByVal oCustomClass As Parameter.InstallRcv) As System.Data.DataTable Implements [Interface].IInstallRcv.GetComboDepartemen
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        Return DAPostingPayment.GetComboDepartemen(oCustomClass)
    End Function

    Public Sub AlokasiPembNonARSaveEdit(ByVal oCustomClass As Parameter.InstallRcv) Implements [Interface].IInstallRcv.AlokasiPembNonARSaveEdit
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        DAPostingPayment.AlokasiPembNonARSaveEdit(oCustomClass)
    End Sub

    Public Function InstallKoreksiModalKerja(ByVal oCustomClass As Parameter.InstallRcv) As Integer Implements [Interface].IInstallRcv.InstallKoreksiModalKerja
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        Return DAPostingPayment.InstallKoreksiModalKerja(oCustomClass)
    End Function
    Public Function InstallKoreksiFactoring(ByVal oCustomClass As Parameter.InstallRcv) As Integer Implements [Interface].IInstallRcv.InstallKoreksiFactoring
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        Return DAPostingPayment.InstallKoreksiFactoring(oCustomClass)
    End Function
    Public Sub AlokasiPembTolakanBGSaveEdit(ByVal oCustomClass As Parameter.InstallRcv) Implements [Interface].IInstallRcv.AlokasiPembTolakanBGSaveEdit
        Dim DAPostingPayment As New SQLEngine.LoanMnt.InstallRcv
        DAPostingPayment.AlokasiPembTolakanBGSaveEdit(oCustomClass)
    End Sub
End Class
