#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class InvoiceOL
    Implements IInvoiceOL

    Public Function CetakInvoiceOLPaging(ByVal customClass As Parameter.InvoiceOL) As Parameter.InvoiceOL Implements [Interface].IInvoiceOL.CetakInvoiceOLPaging
        Dim DA As New SQLEngine.LoanMnt.InvoiceOL
        Return DA.CetakInvoiceOLPaging(customClass)
    End Function

    Public Function CetakInvoiceOLDetail(ByVal customClass As Parameter.InvoiceOL) As DataTable Implements [Interface].IInvoiceOL.CetakInvoiceOLDetail
        Dim DA As New SQLEngine.LoanMnt.InvoiceOL
        Return DA.CetakInvoiceOLDetail(customClass)
    End Function

    Public Function CetakInvoiceOLSave(ByVal oCustomClass As Parameter.InvoiceOL) As String Implements [Interface].IInvoiceOL.CetakInvoiceOLSave
        Dim DA As New SQLEngine.LoanMnt.InvoiceOL
        Return DA.CetakInvoiceOLSave(oCustomClass)
    End Function

    Public Function ReportKwitansi(ByVal customClass As Parameter.InvoiceOL) As DataTable Implements [Interface].IInvoiceOL.ReportKwitansi
        Dim DA As New SQLEngine.LoanMnt.InvoiceOL
        Return DA.ReportKwitansi(customClass)
    End Function

    Public Function InvoiceOLPaging(ByVal customClass As Parameter.InvoiceOL) As Parameter.InvoiceOL Implements [Interface].IInvoiceOL.InvoiceOLPaging
        Dim DA As New SQLEngine.LoanMnt.InvoiceOL
        Return DA.InvoiceOLPaging(customClass)
    End Function

    Public Function InvoiceOLDetail(ByVal customClass As Parameter.InvoiceOL) As DataTable Implements [Interface].IInvoiceOL.InvoiceOLDetail
        Dim DA As New SQLEngine.LoanMnt.InvoiceOL
        Return DA.InvoiceOLDetail(customClass)
    End Function

    Public Function InvoiceOLExecute(ByVal oCustomClass As Parameter.InvoiceOL) As String Implements [Interface].IInvoiceOL.InvoiceOLExecute
        Dim DA As New SQLEngine.LoanMnt.InvoiceOL
        Return DA.InvoiceOLExecute(oCustomClass)
    End Function

    Public Function GetInstallRCVPrint(ByVal customClass As Parameter.InvoiceOL) As DataTable Implements [Interface].IInvoiceOL.GetInstallRCVPrint
        Dim DA As New SQLEngine.LoanMnt.InvoiceOL
        Return DA.GetInstallRCVPrint(customClass)
    End Function

End Class
