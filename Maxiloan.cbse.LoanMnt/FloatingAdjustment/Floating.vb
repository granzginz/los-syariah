
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class Floating
    Implements IFloating
    Public Function ReschedulingProduct(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements IFloating.ReschedulingProduct
        Dim DA As New SQLEngine.LoanMnt.Floating
        Return DA.ReschedulingProduct(oCustomClass)
    End Function
    Public Function LastAdjustment(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements IFloating.LastAdjustment
        Dim DA As New SQLEngine.LoanMnt.Floating
        Return DA.LastAdjustment(oCustomClass)
    End Function
    Public Function SaveFloatingData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements IFloating.SaveFloatingData
        Dim DA As New SQLEngine.LoanMnt.Floating
        Return DA.SaveFloatingData(oCustomClass)
    End Function
    Public Function GetFloatingData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements IFloating.GetFloatingData
        Dim DA As New SQLEngine.LoanMnt.Floating
        Return DA.GetFloatingData(oCustomClass)
    End Function
    Public Function FloatingPrintTrial(ByVal oCustomClass As Parameter.FinancialData) As System.Data.DataSet Implements IFloating.FloatingPrintTrial
        Dim DA As New SQLEngine.LoanMnt.Floating
        Return DA.FloatingPrintTrial(oCustomClass)
    End Function
End Class
