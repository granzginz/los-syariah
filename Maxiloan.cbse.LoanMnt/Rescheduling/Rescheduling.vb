
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class Rescheduling
    Implements IRescheduling
    Public Function SaveReschedulingData(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable, _
          ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.FinancialData Implements IRescheduling.SaveReschedulingData
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        Return DA.SaveReschedulingData(oCustomClass, oData1, oData2, oData3)
    End Function
    Public Function GetMinDueDate(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements IRescheduling.GetMinDueDate
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        Return DA.GetMinDueDate(oCustomClass)
    End Function
    Public Function ViewRescheduling(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements IRescheduling.ViewRescheduling
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        Return DA.ViewRescheduling(oCustomClass)
    End Function
    Public Function ViewReschedulingApproval(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements IRescheduling.ViewReschedulingApproval
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        Return DA.ViewReschedulingApproval(oCustomClass)
    End Function
    Public Sub ReschedulingCancel(ByVal ocustomClass As Parameter.FinancialData) Implements IRescheduling.ReschedulingCancel
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        DA.ReschedulingCancel(ocustomClass)
    End Sub
    Public Sub ReschedulingExecute(ByVal ocustomClass As Parameter.FinancialData) Implements IRescheduling.ReschedulingExecute
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        DA.ReschedulingExecute(ocustomClass)
    End Sub
    Public Function ReschedulingPrintTrial(ByVal oCustomClass As Parameter.FinancialData) As System.Data.DataSet Implements IRescheduling.ReschedulingPrintTrial
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        Return DA.ReschedulingPrintTrial(oCustomClass)
    End Function
    Public Function ReschedulingProduct(ByVal strConnection As String, ByVal BranchId As String) As DataTable Implements IRescheduling.ReschedulingProduct
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        Return DA.ReschedulingProduct(strConnection, BranchId)
    End Function
    Public Function SaveReschedulingIRR(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable, _
             ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.FinancialData Implements IRescheduling.SaveReschedulingIRR
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        Return DA.SaveReschedulingIRR(oCustomClass, oData1, oData2, oData3)
    End Function
    Public Function ViewReschedulingIRR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements IRescheduling.ViewReschedulingIRR
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        Return DA.ViewReschedulingIRR(oCustomClass)
    End Function
    Public Function SaveReschedulingStepUpStepDown(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable, _
               ByVal oData2 As DataTable, ByVal oData3 As DataTable, ByVal oData4 As DataTable) As Parameter.FinancialData Implements IRescheduling.SaveReschedulingStepUpStepDown
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        Return DA.SaveReschedulingStepUpStepDown(oCustomClass, oData1, oData2, oData3, oData4)
    End Function
    Public Function SaveReschedulingEP(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable, _
             ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.FinancialData Implements IRescheduling.SaveReschedulingEP
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        Return DA.SaveReschedulingEP(oCustomClass, oData1, oData2, oData3)
    End Function
    Public Function GetEffectiveDate(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements IRescheduling.GetEffectiveDate
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        Return DA.GetEffectiveDate(oCustomClass)
    End Function
    Public Function SaveReschedulingStepUpStepDownFactAndMDKJ(ByVal oCustomClass As Parameter.FinancialData, ByVal oData1 As DataTable,
               ByVal oData2 As DataTable, ByVal oData3 As DataTable, ByVal oData4 As DataTable) As Parameter.FinancialData Implements IRescheduling.SaveReschedulingStepUpStepDownFactAndMDKJ
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        Return DA.SaveReschedulingStepUpStepDownFactAndMDKJ(oCustomClass, oData1, oData2, oData3, oData4)
    End Function
    Public Function ViewRestrcutFactAndMU(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements IRescheduling.ViewRestrcutFactAndMU
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        Return DA.ViewRestrcutFactAndMU(oCustomClass)
    End Function
    Public Function ViewApprovalRestrcutFactAndMU(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements IRescheduling.ViewApprovalRestrcutFactAndMU
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        Return DA.ViewApprovalRestrcutFactAndMU(oCustomClass)
    End Function
    Public Sub RestructFactAndMUExecute(ByVal ocustomClass As Parameter.FinancialData) Implements IRescheduling.RestructFactAndMUExecute
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        DA.RestructFactAndMUExecute(ocustomClass)
    End Sub
    Public Function GetMinDueDateFactAndMDKJ(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData Implements IRescheduling.GetMinDueDateFactAndMDKJ
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        Return DA.GetMinDueDateFactAndMDKJ(oCustomClass)
    End Function
    Public Sub ReschedulingCancelFactAndMDKJ(ByVal ocustomClass As Parameter.FinancialData) Implements IRescheduling.ReschedulingCancelFactAndMDKJ
        Dim DA As New SQLEngine.LoanMnt.Rescheduling
        DA.ReschedulingCancelFactAndMDKJ(ocustomClass)
    End Sub
End Class
