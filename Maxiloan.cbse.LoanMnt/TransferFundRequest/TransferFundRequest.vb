
#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class TransferFundRequest : Inherits ComponentBase
    Implements ITransferFundRequest
#Region "Get_RequestNo"
    Public Function Get_RequestNo(ByVal customClass As Parameter.TransferFundRequest) As String Implements [Interface].ITransferFundRequest.Get_RequestNo
        Try
            Dim PRDA As New SQLEngine.LoanMnt.TransferFundRequest
            Return PRDA.Get_RequestNo(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
#End Region

    Public Function SavePR(ByVal customclass As Parameter.TransferFundRequest) As String Implements [Interface].ITransferFundRequest.SavePR
        Dim PRDA As New SQLEngine.LoanMnt.TransferFundRequest
        Return PRDA.SavePR(customclass)
    End Function
    Public Function CekPR(ByVal customclass As Parameter.TransferFundRequest) As String Implements [Interface].ITransferFundRequest.CekPR
        Dim PRDA As New SQLEngine.LoanMnt.TransferFundRequest
        Return PRDA.CekPayment(customclass)
    End Function

#Region "GetStructTransferFundRequest"
    Public Function GetStructTransferFundRequest() As DataSet
        Dim lObjDataTable As New DataSet
        Dim dtTable As New DataTable("TransferFundRequest")

        lObjDataTable.Tables.Add(dtTable)

        lObjDataTable.Tables("TransferFundRequest").Columns.Add("PaymentAllocationID", System.Type.GetType("System.String"))
        lObjDataTable.Tables("TransferFundRequest").Columns.Add("PaymentAllocationName", System.Type.GetType("System.String"))
        lObjDataTable.Tables("TransferFundRequest").Columns.Add("Desc", System.Type.GetType("System.String"))
        lObjDataTable.Tables("TransferFundRequest").Columns.Add("Amount", System.Type.GetType("System.String"))
        Return lObjDataTable
    End Function
#End Region

#Region "Generate_Table"
    Private Function Generate_Table(ByVal CustomClass As Parameter.TransferFundRequest, ByVal dsTransferFundRequest As DataSet) As DataSet
        Dim lObjDataRow As DataRow
        Dim strZero As String
        Dim lCounter As Integer
        Dim SerialNoGiroNo As Int64
        Dim FrequentDueDate As Date
        'strZero = ""
        With CustomClass
            lObjDataRow = dsTransferFundRequest.Tables("TransferFundRequest").NewRow()
            lObjDataRow("PaymentAllocationID") = .PaymentAllocationID
            lObjDataRow("PaymentAllocationName") = .PaymentAllDescription
            lObjDataRow("Desc") = .Description
            lObjDataRow("Amount") = .Amount
            dsTransferFundRequest.Tables("TransferFundRequest").Rows.Add(lObjDataRow)
        End With
        Return dsTransferFundRequest
    End Function
#End Region

#Region "Generate_TableDelete"
    Private Function Generate_TableDelete(ByVal CustomClass As Parameter.TransferFundRequest, ByVal dsTransferFundRequest As DataSet) As DataSet
        With CustomClass
            dsTransferFundRequest.Tables("TransferFundRequest").Rows.RemoveAt(CInt(.IndexDelete))
            Return dsTransferFundRequest
        End With
    End Function
#End Region

#Region "GetTableTransaction"
    Public Function GetTableTransaction(ByVal customclass As Parameter.TransferFundRequest, ByVal strFile As String) As Parameter.TransferFundRequest Implements [Interface].ITransferFundRequest.GetTableTransaction
        Dim CheckTransferFundRequestNo As DataRow()
        Dim lObjHoliday As New Hashtable
        Dim pstrFile As String
        Dim gStrPath As String
        Dim dsTransferFundRequest As New DataSet


        'isError = False

        With customclass
            pstrFile = "C:\temp\" + strFile & "_TransferFundRequest"
            If .hpsxml = "1" Then
                If File.Exists(pstrFile & ".xsd") Then
                    File.Delete(pstrFile & ".xsd")
                End If
                If File.Exists(pstrFile & ".xml") Then
                    File.Delete(pstrFile & ".xml")
                End If
            Else
                If .flagdelete = "1" Then
                    If Not File.Exists(pstrFile & ".xsd") Then
                        dsTransferFundRequest = GetStructTransferFundRequest()
                        dsTransferFundRequest.WriteXmlSchema(pstrFile & ".xsd")
                        dsTransferFundRequest.ReadXmlSchema(pstrFile & ".xsd")
                    End If
                    If Not File.Exists(pstrFile & ".xml") Then
                        dsTransferFundRequest.WriteXml(pstrFile & ".xml")
                        dsTransferFundRequest.AcceptChanges()
                    Else
                        dsTransferFundRequest.ReadXmlSchema(pstrFile & ".xsd")
                        dsTransferFundRequest.ReadXml(pstrFile & ".xml")
                        dsTransferFundRequest.AcceptChanges()
                    End If

                    dsTransferFundRequest = Generate_TableDelete(customclass, dsTransferFundRequest)
                    dsTransferFundRequest.WriteXml(pstrFile & ".xml")
                    dsTransferFundRequest.AcceptChanges()
                    customclass.ListData = dsTransferFundRequest.Tables("TransferFundRequest")
                Else
                    If Not File.Exists(pstrFile & ".xsd") Then
                        dsTransferFundRequest = GetStructTransferFundRequest()
                        dsTransferFundRequest.WriteXmlSchema(pstrFile & ".xsd")
                        dsTransferFundRequest.ReadXmlSchema(pstrFile & ".xsd")
                    End If
                    If Not File.Exists(pstrFile & ".xml") Then
                        dsTransferFundRequest.WriteXml(pstrFile & ".xml")
                        dsTransferFundRequest.AcceptChanges()
                    Else
                        dsTransferFundRequest.ReadXmlSchema(pstrFile & ".xsd")
                        dsTransferFundRequest.ReadXml(pstrFile & ".xml")
                        dsTransferFundRequest.AcceptChanges()
                        CheckTransferFundRequestNo = dsTransferFundRequest.Tables("TransferFundRequest").Select("PaymentAllocationID = '" & .PaymentAllocationID & "'")
                        If CheckTransferFundRequestNo.Length > 0 Then
                            .IsValid = False
                            Return customclass
                        End If
                    End If
                    dsTransferFundRequest = Generate_Table(customclass, dsTransferFundRequest)
                    dsTransferFundRequest.WriteXml(pstrFile & ".xml")
                    dsTransferFundRequest.AcceptChanges()
                    customclass.ListData = dsTransferFundRequest.Tables("TransferFundRequest")
                End If
            End If
        End With
        Return customclass
    End Function
#End Region
    Public Function PRCOAUpdate(ByVal customClass As Parameter.TransferFundRequest) As String Implements [Interface].ITransferFundRequest.PRCOAUpdate
        Dim DA As New SQLEngine.LoanMnt.TransferFundRequest
        Return DA.PRCOAUpdate(customClass)
    End Function
    Public Function GetTransferFundRequestPaging(ByVal customClass As Parameter.TransferFundRequest) As Parameter.TransferFundRequest Implements [Interface].ITransferFundRequest.GetTransferFundRequestPaging
        Try
            Dim PRDA As New SQLEngine.LoanMnt.TransferFundRequest
            Return PRDA.GetTransferFundRequestPaging(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetTransferFundRequestHeaderAndDetail(ByVal customClass As Parameter.TransferFundRequest) As Parameter.TransferFundRequest Implements [Interface].ITransferFundRequest.GetTransferFundRequestHeaderAndDetail
        Try
            Dim PRDA As New SQLEngine.LoanMnt.TransferFundRequest
            Return PRDA.GetTransferFundRequestHeaderAndDetail(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetTransferFundRequestDataset(ByVal customClass As Parameter.TransferFundRequest) As Parameter.TransferFundRequest Implements [Interface].ITransferFundRequest.GetTransferFundRequestDataset
        Try
            Dim PRDA As New SQLEngine.LoanMnt.TransferFundRequest
            Return PRDA.GetTransferFundRequestDataset(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub saveApprovalTransferFundRequest(ByVal oClass As Parameter.TransferFundRequest, ByVal strApproval As String) Implements [Interface].ITransferFundRequest.saveApprovalTransferFundRequest
        Dim PRDA As New SQLEngine.LoanMnt.TransferFundRequest
        PRDA.saveApprovalTransferFundRequest(oClass, strApproval)
    End Sub


    Public Function GetTransferFundRequestFormKuning(ByVal customClass As IList(Of String), cnn As String) As DataSet Implements [Interface].ITransferFundRequest.GetTransferFundRequestFormKuning
        Dim DASavePCReimburse As New SQLEngine.LoanMnt.TransferFundRequest
        Return DASavePCReimburse.GetTransferFundRequestFormKuning(customClass, cnn)
    End Function

    Public Sub PayReqStatusReject(oClass As Parameter.TransferFundRequest, strApproval As String) Implements [Interface].ITransferFundRequest.PayReqStatusReject
        Dim PRDA As New SQLEngine.LoanMnt.TransferFundRequest
        PRDA.PayReqStatusReject(oClass, strApproval)
    End Sub

    Public Function GetPaymentVoucherByRequestNoPrint(ocustom As Parameter.TransferFundRequest) As Parameter.TransferFundRequest Implements ITransferFundRequest.GetPaymentVoucherByRequestNoPrint
        Dim PRDA As New SQLEngine.LoanMnt.TransferFundRequest
        Return PRDA.GetPaymentVoucherByRequestNoPrint(ocustom)
    End Function

    Public Function GetBankAccountOther(oCustomClass As Parameter.TransferFundRequest) As Parameter.TransferFundRequest Implements ITransferFundRequest.GetBankAccountOther
        Dim PRDA As New SQLEngine.LoanMnt.TransferFundRequest
        Return PRDA.GetBankAccountOther(oCustomClass)
    End Function

    Public Function UpdatePR(customclass As Parameter.TransferFundRequest) As String Implements ITransferFundRequest.UpdatePR
        Dim PRDA As New SQLEngine.LoanMnt.TransferFundRequest
        Return PRDA.UpdatePR(customclass)
    End Function
End Class
