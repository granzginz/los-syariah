

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class Bank : Inherits ComponentBase
    Implements IBank


#Region "Bank Master"

    Public Function ListBankMaster(ByVal customclass As Parameter.BankMaster) As Parameter.BankMaster Implements [Interface].IBank.ListBankMaster
        Dim DAListBankMaster As New SQLEngine.LoanMnt.BankMaster
        Return DAListBankMaster.ListBankMaster(customclass)
    End Function

    Public Function AddBankMaster(ByVal customclass As Parameter.BankMaster) As String Implements [Interface].IBank.AddBankMaster
        Dim DAAddBankMaster As New SQLEngine.LoanMnt.BankMaster
        Return DAAddBankMaster.AddBankMaster(customclass)
    End Function

    Public Function DeleteBankMaster(ByVal customclass As Parameter.BankMaster) As String Implements [Interface].IBank.DeleteBankMaster
        Dim DADeleteBankMaster As New SQLEngine.LoanMnt.BankMaster
        Return DADeleteBankMaster.DeleteBankMaster(customclass)
    End Function

    Public Function UpdateBankMaster(ByVal customclass As Parameter.BankMaster) As String Implements [Interface].IBank.UpdateBankMaster
        Dim DAUpdateBankMaster As New SQLEngine.LoanMnt.BankMaster
        Return DAUpdateBankMaster.UpdateBankMaster(customclass)
    End Function

    Public Function ReportBankMaster(ByVal customclass As Parameter.BankMaster) As Parameter.BankMaster Implements [Interface].IBank.ReportBankMaster
        Dim DAPrintBankMaster As New SQLEngine.LoanMnt.BankMaster
        Return DAPrintBankMaster.ReportBankMaster(customclass)
    End Function


#End Region

#Region "Bank Account"
    Public Function AddBankAccount(ByVal customclass As Parameter.BankAccount, _
                                    ByVal oClassAddress As Parameter.Address, _
                                    ByVal oClassPersonal As Parameter.Personal) As String Implements [Interface].IBank.AddBankAccount
        Dim DAAddBankAccount As New SQLEngine.LoanMnt.BankAccount
        Return DAAddBankAccount.AddBankAccount(customclass, oClassAddress, oClassPersonal)
    End Function

    Public Function ListBankAccount(ByVal customclass As Parameter.BankAccount) As Parameter.BankAccount Implements [Interface].IBank.ListBankAccount
        Dim DAListBankAccount As New SQLEngine.LoanMnt.BankAccount
        Return DAListBankAccount.ListBankAccount(customclass)
    End Function

    Public Function ReportBankAccount(ByVal customclass As Parameter.BankAccount) As Parameter.BankAccount Implements [Interface].IBank.ReportBankAccount
        Dim DAPrintBankAccount As New SQLEngine.LoanMnt.BankAccount
        Return DAPrintBankAccount.ReportBankAccount(customclass)
    End Function

    Public Function UpdateBankAccount(ByVal customclass As Parameter.BankAccount, _
                                    ByVal oClassAddress As Parameter.Address, _
                                    ByVal oClassPersonal As Parameter.Personal) As String Implements [Interface].IBank.UpdateBankAccount
        Dim DAUpdateBankAccount As New SQLEngine.LoanMnt.BankAccount
        Return DAUpdateBankAccount.UpdateBankAccount(customclass, oClassAddress, oClassPersonal)
    End Function

    Public Function UpdateBankAccountH(ByVal customclass As Parameter.BankAccount) As String Implements [Interface].IBank.UpdateBankAccountH
        Dim DAUpdateBankAccount As New SQLEngine.LoanMnt.BankAccount
        Return DAUpdateBankAccount.UpdateBankAccountH(customclass)
    End Function

    Public Function DeleteBankAccount(ByVal customclass As Parameter.BankAccount) As String Implements [Interface].IBank.DeleteBankAccount
        Dim DADeleteBankAccount As New SQLEngine.LoanMnt.BankAccount
        Return DADeleteBankAccount.DeleteBankAccount(customclass)
    End Function

    Public Function BankAccountInformasi(ByVal strConnection As String, _
                                        ByVal strBankAccountID As String, _
                                        ByVal strBranchid As String) As DataTable Implements [Interface].IBank.BankAccountInformasi
        Dim DABankAccountInformasi As New SQLEngine.LoanMnt.BankAccount
        Return DABankAccountInformasi.BankAccountInformasi(strConnection, strBankAccountID, strBranchid)
    End Function
#End Region

    Public Function BankAccountListByType(ByVal oCustom As Parameter.BankAccount) As System.Data.DataTable Implements [Interface].IBank.BankAccountListByType
        Dim DABankAccountInformasi As New SQLEngine.LoanMnt.BankAccount
        Return DABankAccountInformasi.BankAccountListByType(oCustom)
    End Function
End Class
