

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class JournalScheme : Inherits ComponentBase
    Implements IJournalScheme

    Public Function GetJournalScheme(ByVal customClass As Parameter.JournalScheme) As Parameter.JournalScheme Implements [Interface].IJournalScheme.GetJournalScheme
        Dim JournalSchemeDA As New SQLEngine.LoanMnt.JournalScheme
        Return JournalSchemeDA.GetJournalScheme(customClass)
    End Function
    Public Function GetJournalSchemeReport(ByVal customClass As Parameter.JournalScheme) As Parameter.JournalScheme Implements [Interface].IJournalScheme.GetJournalSchemeReport

        Dim JournalSchemeDA As New SQLEngine.LoanMnt.JournalScheme
        Return JournalSchemeDA.GetJournalSchemeReport(customClass)
    End Function

    Public Function GetJournalSchemeAddDtl(ByVal customClass As Parameter.JournalScheme) As Parameter.JournalScheme Implements [Interface].IJournalScheme.GetJournalSchemeAddDtl
        Dim JournalSchemeDA As New SQLEngine.LoanMnt.JournalScheme
        Return JournalSchemeDA.GetJournalSchemeAddDtl(customClass)


    End Function

    Public Function GetJournalSchemeCopyFrom(ByVal customClass As Parameter.JournalScheme) As Parameter.JournalScheme Implements [Interface].IJournalScheme.GetJournalSchemeCopyFrom

        Dim JournalSchemeDA As New SQLEngine.LoanMnt.JournalScheme
        Return JournalSchemeDA.GetJournalSchemeCopyFrom(customClass)
    End Function

    Public Function GetJournalSchemeEditHdr(ByVal customClass As Parameter.JournalScheme) As Parameter.JournalScheme Implements [Interface].IJournalScheme.GetJournalSchemeEditHdr

        Dim JournalSchemeDA As New SQLEngine.LoanMnt.JournalScheme
        Return JournalSchemeDA.GetJournalSchemeEditHdr(customClass)
    End Function

    Public Function GetJournalSchemeEditDtl(ByVal customClass As Parameter.JournalScheme) As Parameter.JournalScheme Implements [Interface].IJournalScheme.GetJournalSchemeEditDtl
        Dim JournalSchemeDA As New SQLEngine.LoanMnt.JournalScheme
        Return JournalSchemeDA.GetJournalSchemeEditDtl(customClass)
    End Function
    Public Sub JournalSchemeDelete(ByVal customClass As Parameter.JournalScheme) Implements [Interface].IJournalScheme.JournalSchemeDelete
        Dim JournalSchemeDA As New SQLEngine.LoanMnt.JournalScheme
        JournalSchemeDA.JournalSchemeDelete(customClass)

    End Sub

    Public Function JournalSchemeSaveAdd(ByVal customClass As Parameter.JournalScheme) As String Implements [Interface].IJournalScheme.JournalSchemeSaveAdd
        Dim JournalSchemeDA As New SQLEngine.LoanMnt.JournalScheme
        Return JournalSchemeDA.JournalSchemeSaveAdd(customClass)
    End Function

    Public Sub JournalSchemeSaveEdit(ByVal customClass As Parameter.JournalScheme) Implements [Interface].IJournalScheme.JournalSchemeSaveEdit
        Dim JournalSchemeDA As New SQLEngine.LoanMnt.JournalScheme
        JournalSchemeDA.JournalSchemeSaveEdit(customClass)
    End Sub
End Class
