#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class Cashier
    Implements ICashier
    Public Function ListCashier(ByVal customclass As Parameter.Cashier) As Parameter.Cashier Implements [Interface].ICashier.ListCashier
        Dim DAListCashier As New SQLEngine.LoanMnt.Cashier
        Return DAListCashier.ListCashier(customclass)
    End Function

    Public Sub CashierAdd(ByVal customclass As Parameter.Cashier) Implements [Interface].ICashier.CashierAdd
        Dim DAAddCashier As New SQLEngine.LoanMnt.Cashier
        DAAddCashier.CashierAdd(customclass)
    End Sub

    Public Sub CashierDelete(ByVal customclass As Parameter.Cashier) Implements [Interface].ICashier.CashierDelete
        Dim DADeleteCashier As New SQLEngine.LoanMnt.Cashier
        DADeleteCashier.CashierDelete(customclass)
    End Sub

    Public Sub CashierEdit(ByVal customclass As Parameter.Cashier) Implements [Interface].ICashier.CashierEdit
        Dim DAUpdateCashier As New SQLEngine.LoanMnt.Cashier
        DAUpdateCashier.CashierEdit(customclass)
    End Sub

    Public Function CashierReport(ByVal customclass As Parameter.Cashier) As Parameter.Cashier Implements [Interface].ICashier.CashierReport
        Dim DAPrintCashier As New SQLEngine.LoanMnt.Cashier
        Return DAPrintCashier.CashierReport(customclass)
    End Function
End Class
