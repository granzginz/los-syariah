﻿#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class AgreementMasterTransaction : Inherits ComponentBase
    Implements IAgreementMasterTransaction

    Dim oSQLE As New SQLEngine.LoanMnt.AgreementMasterTransaction

    Public Function AgreementMasterTransactionDelete(ByVal oCustomClass As Parameter.AgreementMasterTransaction) As String Implements [Interface].IAgreementMasterTransaction.AgreementMasterTransactionDelete
        Return oSQLE.AgreementMasterTransactionDelete(oCustomClass)
    End Function

    Public Function AgreementMasterTransactionSaveAdd(ByVal oCustomClass As Parameter.AgreementMasterTransaction) As String Implements [Interface].IAgreementMasterTransaction.AgreementMasterTransactionSaveAdd
        Return oSQLE.AgreementMasterTransactionSaveAdd(oCustomClass)
    End Function

    Public Sub AgreementMasterTransactionSaveEdit(ByVal oCustomClass As Parameter.AgreementMasterTransaction) Implements [Interface].IAgreementMasterTransaction.AgreementMasterTransactionSaveEdit
        oSQLE.AgreementMasterTransactionSaveEdit(oCustomClass)
    End Sub

    Public Function GetAgreementMasterTransaction(ByVal oCustomClass As Parameter.AgreementMasterTransaction) As Parameter.AgreementMasterTransaction Implements [Interface].IAgreementMasterTransaction.GetAgreementMasterTransaction
        Return oSQLE.GetAgreementMasterTransaction(oCustomClass)
    End Function

    Public Function GetAgreementMasterTransactionList(ByVal oCustomClass As Parameter.AgreementMasterTransaction) As Parameter.AgreementMasterTransaction Implements [Interface].IAgreementMasterTransaction.GetAgreementMasterTransactionList
        Return oSQLE.GetAgreementMasterTransactionList(oCustomClass)
    End Function
End Class
