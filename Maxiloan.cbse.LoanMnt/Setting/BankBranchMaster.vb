﻿#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class BankBranchMaster : Inherits ComponentBase
    Implements IBankBranchMaster

    Dim oSQLE As New SQLEngine.LoanMnt.BankBranchMaster

    Public Function BankBranchMasterDelete(ByVal oCustomClass As Parameter.BankBranchMaster) As String Implements [Interface].IBankBranchMaster.BankBranchMasterDelete
        Return oSQLE.BankBranchMasterDelete(oCustomClass)
    End Function

    Public Function BankBranchMasterSaveAdd(ByVal oCustomClass As Parameter.BankBranchMaster) As String Implements [Interface].IBankBranchMaster.BankBranchMasterSaveAdd
        Return oSQLE.BankBranchMasterSaveAdd(oCustomClass)
    End Function

    Public Sub BankBranchMasterSaveEdit(ByVal oCustomClass As Parameter.BankBranchMaster) Implements [Interface].IBankBranchMaster.BankBranchMasterSaveEdit
        oSQLE.BankBranchMasterSaveEdit(oCustomClass)
    End Sub

    Public Function GetBankBranchMaster(ByVal oCustomClass As Parameter.BankBranchMaster) As Parameter.BankBranchMaster Implements [Interface].IBankBranchMaster.GetBankBranchMaster
        Return oSQLE.GetBankBranchMaster(oCustomClass)
    End Function

    Public Function GetBankBranchMasterList(ByVal oCustomClass As Parameter.BankBranchMaster) As Parameter.BankBranchMaster Implements [Interface].IBankBranchMaster.GetBankBranchMasterList
        Return oSQLE.GetBankBranchMasterList(oCustomClass)
    End Function

    Public Function GetBankBranchMasterReport(ByVal oCustomClass As Parameter.BankBranchMaster) As Parameter.BankBranchMaster Implements [Interface].IBankBranchMaster.GetBankBranchMasterReport
        Return oSQLE.GetBankBranchMasterReport(oCustomClass)
    End Function
End Class
