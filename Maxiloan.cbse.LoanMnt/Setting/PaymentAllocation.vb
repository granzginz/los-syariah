Imports Maxiloan.cbse
Imports Maxiloan.Interface


Public Class PaymentAllocation : Inherits ComponentBase
    Implements IPaymentAllocation

    Public Function GetPaymentAllocation(ByVal oCustomClass As Parameter.PaymentAllocation) As Parameter.PaymentAllocation Implements [Interface].IPaymentAllocation.GetPaymentAllocation
        Dim PaymentAllocationDA As New SQLEngine.LoanMnt.PaymentAllocation
        Return PaymentAllocationDA.GetPaymentAllocation(oCustomClass)
    End Function

    Public Function GetPaymentAllocationId(ByVal strConnection As String, ByVal id As String) As Parameter.PaymentAllocation Implements [Interface].IPaymentAllocation.GetPaymentAllocationID
        Dim PaymentAllocationDA As New SQLEngine.LoanMnt.PaymentAllocation
        Return PaymentAllocationDA.GetPaymentAllocationId(strConnection, id)
    End Function

    Public Sub PaymentAllocationAdd(ByVal customClass As Parameter.PaymentAllocation) Implements IPaymentAllocation.PaymentAllocationAdd

        Dim PaymentAllocationDA As New SQLEngine.LoanMnt.PaymentAllocation
        PaymentAllocationDA.PaymentAllocationAdd(customClass)
    End Sub

    Public Sub PaymentAllocationEdt(ByVal customClass As Parameter.PaymentAllocation) Implements IPaymentAllocation.PaymentAllocationEdt
        Dim PaymentAllocationDA As New SQLEngine.LoanMnt.PaymentAllocation
        PaymentAllocationDA.PaymentAllocationEdt(customClass)
    End Sub

    Public Sub PaymentAllocationDelete(ByVal customClass As Parameter.PaymentAllocation) Implements IPaymentAllocation.PaymentAllocationDelete
        Dim PaymentAllocationDA As New SQLEngine.LoanMnt.PaymentAllocation
        PaymentAllocationDA.PaymentAllocationDelete(customClass)
    End Sub
    Public Function GetPaymentAllocationReport(ByVal oCustomClass As Parameter.PaymentAllocation) As Parameter.PaymentAllocation Implements [Interface].IPaymentAllocation.GetPaymentAllocationReport
        Dim PaymentAllocationDA As New SQLEngine.LoanMnt.PaymentAllocation
        Return PaymentAllocationDA.GetPaymentAllocationReport(oCustomClass)
    End Function
End Class
