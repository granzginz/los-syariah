﻿

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class Referal : Inherits ComponentBase
    Implements IReferal

    Public Function GetReferal(ByVal oCustomClass As Parameter.Referal) As Parameter.Referal Implements [Interface].IReferal.GetReferal
        Try
            Dim ReferalDA As New SQLEngine.LoanMnt.Referal
            Return ReferalDA.GetReferal(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetReferalReport(ByVal oCustomClass As Parameter.Referal) As Parameter.Referal Implements [Interface].IReferal.GetReferalReport
        Try
            Dim ReferalDA As New SQLEngine.LoanMnt.Referal
            Return ReferalDA.GetReferalReport(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetSector(ByVal oCustomClass As Parameter.Referal) As Parameter.Referal Implements [Interface].IReferal.GetSector
        Try
            Dim ReferalDA As New SQLEngine.LoanMnt.Referal
            Return ReferalDA.GetSector(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetReferalEdit(ByVal oCustomClass As Parameter.Referal) As Parameter.Referal Implements [Interface].IReferal.GetReferalEdit
        Try
            Dim ReferalDA As New SQLEngine.LoanMnt.Referal
            Return ReferalDA.GetReferalEdit(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function ReferalSaveAdd(ByVal oCustomClass As Parameter.Referal) As String Implements IReferal.ReferalSaveAdd
        'Trace.WriteIf(MethodTrace.Enabled, "Enter")
        Try
            Dim ReferalDA As New SQLEngine.LoanMnt.Referal
            Return ReferalDA.ReferalSaveAdd(oCustomClass)
            'Throw busEx
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            'Throw ex
        End Try
    End Function

    Public Sub ReferalSaveEdit(ByVal oCustomClass As Parameter.Referal) Implements IReferal.ReferalSaveEdit
        Try
            Dim ReferalDA As New SQLEngine.LoanMnt.Referal
            ReferalDA.ReferalSaveEdit(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when saving Update Referal", ex)
        End Try
    End Sub

    Public Function ReferalDelete(ByVal oCustomClass As Parameter.Referal) As String Implements IReferal.ReferalDelete
        Try
            Dim ReferalDA As New SQLEngine.LoanMnt.Referal
            Return ReferalDA.ReferalDelete(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when Delete Referal", ex)
        End Try
    End Function
End Class

