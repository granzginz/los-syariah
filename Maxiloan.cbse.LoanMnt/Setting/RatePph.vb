

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region

Public Class RatePph : Inherits ComponentBase
    Implements IRatePph

    Public Function AddRatePphMaster(customclass As RatePphMaster) As String Implements IRatePph.AddRatePphMaster
        Dim DAListRatePphMaster As New SQLEngine.LoanMnt.RatePphMaster
        Return DAListRatePphMaster.AddRatePphMaster(customclass)
    End Function

    Public Function DeleteRatePphMaster(customclass As RatePphMaster) As String Implements IRatePph.DeleteRatePphMaster
        Dim DADeleteRatePphMaster As New SQLEngine.LoanMnt.RatePphMaster
        Return DADeleteRatePphMaster.DeleteRatePphMaster(customclass)
    End Function

    Public Function ListRatePphMaster(customclass As RatePphMaster) As RatePphMaster Implements IRatePph.ListRatePphMaster
        Dim DAListRatePphMaster As New SQLEngine.LoanMnt.RatePphMaster
        Return DAListRatePphMaster.ListRatePphMaster(customclass)
    End Function

    Public Function UpdateRatePphMaster(customclass As RatePphMaster) As String Implements IRatePph.UpdateRatePphMaster
        Dim DAUpdateRatePphMaster As New SQLEngine.LoanMnt.RatePphMaster
        Return DAUpdateRatePphMaster.UpdateRatePphMaster(customclass)
    End Function
End Class
