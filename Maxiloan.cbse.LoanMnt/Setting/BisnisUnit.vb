﻿

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class BisnisUnit : Inherits ComponentBase
    Implements IBisnisUnit

    Public Function GetBisnisUnit(ByVal oCustomClass As Parameter.BisnisUnit) As Parameter.BisnisUnit Implements [Interface].IBisnisUnit.GetBisnisUnit
        Try
            Dim BisnisUnitDA As New SQLEngine.LoanMnt.BisnisUnit
            Return BisnisUnitDA.GetBisnisUnit(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetBisnisUnitReport(ByVal oCustomClass As Parameter.BisnisUnit) As Parameter.BisnisUnit Implements [Interface].IBisnisUnit.GetBisnisUnitReport
        Try
            Dim BisnisUnitDA As New SQLEngine.LoanMnt.BisnisUnit
            Return BisnisUnitDA.GetBisnisUnitReport(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetSector(ByVal oCustomClass As Parameter.BisnisUnit) As Parameter.BisnisUnit Implements [Interface].IBisnisUnit.GetSector
        Try
            Dim BisnisUnitDA As New SQLEngine.LoanMnt.BisnisUnit
            Return BisnisUnitDA.GetSector(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetBisnisUnitEdit(ByVal oCustomClass As Parameter.BisnisUnit) As Parameter.BisnisUnit Implements [Interface].IBisnisUnit.GetBisnisUnitEdit
        Try
            Dim BisnisUnitDA As New SQLEngine.LoanMnt.BisnisUnit
            Return BisnisUnitDA.GetBisnisUnitEdit(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function BisnisUnitSaveAdd(ByVal oCustomClass As Parameter.BisnisUnit) As String Implements IBisnisUnit.BisnisUnitSaveAdd
        'Trace.WriteIf(MethodTrace.Enabled, "Enter")
        Try
            Dim BisnisUnitDA As New SQLEngine.LoanMnt.BisnisUnit
            Return BisnisUnitDA.BisnisUnitSaveAdd(oCustomClass)
            'Throw busEx
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            'Throw ex
        End Try
    End Function

    Public Sub BisnisUnitSaveEdit(ByVal oCustomClass As Parameter.BisnisUnit) Implements IBisnisUnit.BisnisUnitSaveEdit
        Try
            Dim BisnisUnitDA As New SQLEngine.LoanMnt.BisnisUnit
            BisnisUnitDA.BisnisUnitSaveEdit(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when saving Update BisnisUnit", ex)
        End Try
    End Sub

    Public Function BisnisUnitDelete(ByVal oCustomClass As Parameter.BisnisUnit) As String Implements IBisnisUnit.BisnisUnitDelete
        Try
            Dim BisnisUnitDA As New SQLEngine.LoanMnt.BisnisUnit
            Return BisnisUnitDA.BisnisUnitDelete(oCustomClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when Delete BisnisUnit", ex)
        End Try
    End Function
End Class

