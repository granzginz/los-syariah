
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class ADASetting : Inherits ComponentBase
    Implements IADASetting

    Public Function UpdateADASetting(ByVal oCustomClass As Parameter.ADASetting) As Boolean Implements [Interface].IADASetting.UpdateADASetting
        Dim DA As New SQLEngine.LoanMnt.ADASetting
        Return DA.UpdateADASetting(oCustomClass)
    End Function

End Class
