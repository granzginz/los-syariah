
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class AgreementList
    Implements IAgreementList
    Public Function ListAgreement(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IAgreementList.ListAgreement
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.ListAgreement(customclass)
    End Function

    Public Function ListAgreementView(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IAgreementList.ListAgreementView
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.ListAgreementView(customclass)
    End Function

    Public Function ListAgreementFactoring(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IAgreementList.ListAgreementFactoring
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.ListAgreementFactoring(customclass)
    End Function

    Public Function ListAgreementFactoringView(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IAgreementList.ListAgreementFactoringView
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.ListAgreementFactoringView(customclass)
    End Function

    Public Function ListRCVPPH(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IAgreementList.ListRCVPPH
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.ListRCVPPH(customclass)
    End Function

    Public Function ListAgreementModalKerja(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IAgreementList.ListAgreementModalKerja
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.ListAgreementModalKerja(customclass)
    End Function

    Public Function ListAgreementModalKerjaView(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IAgreementList.ListAgreementModalKerjaView
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.ListAgreementModalKerjaView(customclass)
    End Function

    Public Function ListAgreementLinkAge(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IAgreementList.ListAgreementLinkAge
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.ListAgreementLinkage(customclass)
    End Function

    Public Function ReportListAgreement(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IAgreementList.ReportListAgreement
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.ListAgreement(customclass)
    End Function

    Public Function PrepayRequestList(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IAgreementList.PrepayRequestList
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.PrepayRequestList(customclass)
    End Function

    Public Function AgreementIdentifikasiPembayaran(customclass As Parameter.AgreementList, dtTable As System.Data.DataTable) As Parameter.AgreementList Implements [Interface].IAgreementList.AgreementIdentifikasiPembayaran
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.AgreementIdentifikasiPembayaran(customclass, dtTable)
    End Function

    Public Function UcAgreementList(ByVal oCustomClass As Parameter.AccMntBase) As DataTable Implements [Interface].IAgreementList.UcAgreementList
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.UcAgreementList(oCustomClass)
    End Function

    Public Function AgreementListDrawdown(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IAgreementList.AgreementListDrawdown
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.AgreementListDrawdown(customclass)
    End Function

    Public Function AgreementListDisburseFactoring(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IAgreementList.AgreementListDisburseFactoring
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.AgreementListDisburseFactoring(customclass)
    End Function

    Public Function UcAgreementListFactoring(ByVal oCustomClass As Parameter.AccMntBase) As DataTable Implements [Interface].IAgreementList.UcAgreementListFactoring
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.UcAgreementListFactoring(oCustomClass)
    End Function

    Public Function UcAgreementListMdKj(ByVal oCustomClass As Parameter.AccMntBase) As DataTable Implements [Interface].IAgreementList.UcAgreementListMdKj
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.UcAgreementListMdKj(oCustomClass)
    End Function

    Public Function UcAgreementListInv(ByVal oCustomClass As Parameter.AccMntBase) As DataTable Implements [Interface].IAgreementList.UcAgreementListInv
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.UcAgreementListInv(oCustomClass)
    End Function

    'Penambahan reversal factoring dan modal kerja
    Public Function ListAgreementFACT(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IAgreementList.ListAgreementFACT
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.ListAgreementFACT(customclass)
    End Function

    Public Function ListAgreementMDKJ(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IAgreementList.ListAgreementMDKJ
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.ListAgreementMDKJ(customclass)
    End Function

    Public Function AgreementListPrepaid(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IAgreementList.AgreementListPrepaid
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.AgreementListPrepaid(customclass)
    End Function

    Public Function PrepaidAllocation(ByVal oCustomClass2 As Parameter.InstallRcv) As Parameter.InstallRcv Implements [Interface].IAgreementList.PrepaidAllocation
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.PrepaidAllocation(oCustomClass2)
    End Function

    Public Function ListAgreementnew(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList Implements [Interface].IAgreementList.ListAgreementnew
        Dim DAListAgreement As New SQLEngine.LoanMnt.AgreementList
        Return DAListAgreement.ListAgreementnew(customclass)
    End Function
End Class
