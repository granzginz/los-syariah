
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class Exposure
    Implements IExposure
    Public Function GetAgreeExposure(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase Implements [Interface].IExposure.GetAgreeExposure
        Dim DAEx As New SQLEngine.LoanMnt.Exposure
        Return DAEx.GetAgreeEx(oCustomClass)
    End Function

    Public Function GetListAgree(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase Implements [Interface].IExposure.GetListAgree
        Dim DAList As New SQLEngine.LoanMnt.Exposure
        Return DAList.ListAgree(oCustomClass)
    End Function

    Public Function GetCustExposure(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase Implements [Interface].IExposure.GetCustExposure
        Dim DAEx As New SQLEngine.LoanMnt.Exposure
        Return DAEx.GetCustomerEx(oCustomClass)
    End Function
End Class
