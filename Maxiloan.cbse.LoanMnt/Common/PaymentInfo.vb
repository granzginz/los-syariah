
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class PaymentInfo
    Implements IPaymentInfo


    Public Function GetPaymentInfo(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase Implements [Interface].IPaymentInfo.GetPaymentInfo
        Dim DAPaymentInfo As New SQLEngine.LoanMnt.PaymentInfo
        Dim oClassPaymentInfo As New Parameter.AccMntBase
        'Dim InstallmentDue As Double
        'Dim LCInstallment As Double

        oClassPaymentInfo = DAPaymentInfo.GetPaymentInfo(oCustomClass)
        With oClassPaymentInfo
            .AmountToBePaid = .InstallmentDue + .LcInstallment + .InstallmentCollFee + _
                                .InsuranceDue + .InsuranceCollFee + .LcInsurance + _
                                .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense + _
                                .RepossessionFee

        End With
        Return oClassPaymentInfo
    End Function

    Public Function GetFullPrePaymentInfo(ByVal oCustomClass As Parameter.FullPrepay) As Parameter.FullPrepay Implements [Interface].IPaymentInfo.GetFullPrepaymentInfo
        Dim DAPaymentInfo As New SQLEngine.LoanMnt.PaymentInfo
        Dim oClassPaymentInfo As New Parameter.FullPrepay
        'Dim InstallmentDue As Double
        'Dim LCInstallment As Double

        oClassPaymentInfo = DAPaymentInfo.GetFullPrePaymentInfo(oCustomClass)
        With oClassPaymentInfo
            .AmountToBePaid = .InstallmentDue + .LcInstallment + .InstallmentCollFee + _
                                .InsuranceDue + .InsuranceCollFee + .LcInsurance + _
                                .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense + _
                                .RepossessionFee

        End With
        Return oClassPaymentInfo
    End Function

    Public Function GetFullPrePaymentInfoNormal(ByVal customclass As Parameter.FullPrepay) As Parameter.FullPrepay Implements [Interface].IPaymentInfo.GetFullPrePaymentInfoNormal
        Dim DAPaymentInfo As New SQLEngine.LoanMnt.PaymentInfo
        Dim oClassPaymentInfo As New Parameter.FullPrepay
        oClassPaymentInfo = DAPaymentInfo.GetFullPrePaymentInfoNormal(customclass)
        Return oClassPaymentInfo
    End Function

    Public Function GetFullPrepaymentInfoOL(ByVal oCustomClass As Parameter.FullPrepay) As Parameter.FullPrepay Implements [Interface].IPaymentInfo.GetFullPrePaymentInfoOL
        Dim DAPaymentInfo As New SQLEngine.LoanMnt.PaymentInfo
        Dim oClassPaymentInfo As New Parameter.FullPrepay
        'Dim InstallmentDue As Double
        'Dim LCInstallment As Double

        oClassPaymentInfo = DAPaymentInfo.GetFullPrePaymentInfoOL(oCustomClass)
        With oClassPaymentInfo
            .AmountToBePaid = .InstallmentDue + .LcInstallment + .InstallmentCollFee + _
                                .InsuranceDue + .InsuranceCollFee + .LcInsurance + _
                                .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense + _
                                .RepossessionFee

        End With
        Return oClassPaymentInfo
    End Function

    Public Function GetPaymentInfoOL(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase Implements [Interface].IPaymentInfo.GetPaymentInfoOL
        Dim DAPaymentInfo As New SQLEngine.LoanMnt.PaymentInfo
        Dim oClassPaymentInfo As New Parameter.AccMntBase
        'Dim InstallmentDue As Double
        'Dim LCInstallment As Double

        oClassPaymentInfo = DAPaymentInfo.GetPaymentInfoOL(oCustomClass)
        With oClassPaymentInfo
            .AmountToBePaid = .InstallmentDue + .LcInstallment + .InstallmentCollFee + _
                                .InsuranceDue + .InsuranceCollFee + .LcInsurance + _
                                .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense + _
                                .RepossessionFee

        End With
        Return oClassPaymentInfo
    End Function

    Public Function GetIsPaymentHaveOtor(ByVal customclass As Parameter.AccMntBase) As Parameter.AccMntBase Implements [Interface].IPaymentInfo.GetIsPaymentHaveOtor
        Dim DAPaymentInfo As New SQLEngine.LoanMnt.PaymentInfo
        Dim oClassPaymentInfo As New Parameter.AccMntBase
        oClassPaymentInfo = DAPaymentInfo.GetIsPaymentHaveOtor(customclass)
        Return oClassPaymentInfo
    End Function

    Public Function GetPaymentInfoFactAndMDKJ(ByVal oCustomClass As Parameter.AccMntBase) As Parameter.AccMntBase Implements [Interface].IPaymentInfo.GetPaymentInfoFactAndMDKJ
        Dim DAPaymentInfo As New SQLEngine.LoanMnt.PaymentInfo
        Dim oClassPaymentInfo As New Parameter.AccMntBase

        oClassPaymentInfo = DAPaymentInfo.GetPaymentInfoFactAndMDKJ(oCustomClass)
        With oClassPaymentInfo
            .AmountToBePaid = .InstallmentDue + .LcInstallment + .InstallmentCollFee +
                                .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                                .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                                .RepossessionFee

        End With
        Return oClassPaymentInfo
    End Function

    Public Function GetFullPrepaymentInfoFactAndMDKJ(ByVal oCustomClass As Parameter.FullPrepay) As Parameter.FullPrepay Implements [Interface].IPaymentInfo.GetFullPrepaymentInfoFactAndMDKJ
        Dim DAPaymentInfo As New SQLEngine.LoanMnt.PaymentInfo
        Dim oClassPaymentInfo As New Parameter.FullPrepay

        oClassPaymentInfo = DAPaymentInfo.GetFullPrepaymentInfoFactAndMDKJ(oCustomClass)
        With oClassPaymentInfo
            .AmountToBePaid = .InstallmentDue + .LcInstallment + .InstallmentCollFee +
                                .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                                .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                                .RepossessionFee

        End With
        Return oClassPaymentInfo
    End Function

    Public Function GetFullPrepaymentInfoView(ByVal oCustomClass As Parameter.FullPrepay) As Parameter.FullPrepay Implements [Interface].IPaymentInfo.GetFullPrepaymentInfoView
        Dim DAPaymentInfo As New SQLEngine.LoanMnt.PaymentInfo
        Dim oClassPaymentInfo As New Parameter.FullPrepay

        oClassPaymentInfo = DAPaymentInfo.GetFullPrepaymentInfoView(oCustomClass)
        With oClassPaymentInfo
            .AmountToBePaid = .InstallmentDue + .LcInstallment + .InstallmentCollFee +
                                .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                                .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                                .RepossessionFee

        End With
        Return oClassPaymentInfo
    End Function

End Class
