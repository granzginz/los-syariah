
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class CheckCashier
    Implements ICheckCashier

    Public Function CheckCashier(ByVal strConnection As String, ByVal LoginID As String, _
                ByVal BranchID As String, ByVal BusinessDate As Date) As Boolean Implements [Interface].ICheckCashier.CheckCashier
        Dim DACheckCashier As New SQLEngine.LoanMnt.CheckCashier
        Return DACheckCashier.CheckCashier(strConnection, LoginID, BranchID, BusinessDate)
    End Function
End Class
