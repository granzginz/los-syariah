
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class StopAccrued
    Implements IStopAccrued

    Public Sub StopAccruedRequest(ByVal oCustomClass As Parameter.StopAccrued) Implements [Interface].IStopAccrued.StopAccruedRequest
        Dim DAPostingPayment As New SQLEngine.LoanMnt.StopAccrued
        DAPostingPayment.StopAccruedRequest(oCustomClass)
    End Sub
    Public Sub StopAccruedReversal(ByVal oCustomClass As Parameter.StopAccrued) Implements [Interface].IStopAccrued.StopAccruedReversal
        Dim DAPostingPayment As New SQLEngine.LoanMnt.StopAccrued
        DAPostingPayment.StopAccruedReversal(oCustomClass)
    End Sub

    Public Function StopAccruedGetPastDueDays(ByVal oCustomClass As Parameter.StopAccrued) As Parameter.StopAccrued Implements [Interface].IStopAccrued.StopAccruedGetPastDueDays
        Dim DAPostingPayment As New SQLEngine.LoanMnt.StopAccrued
        Return DAPostingPayment.StopAccruedGetPastDueDays(oCustomClass)
    End Function

    Public Function StopAccruedView(ByVal oCustomClass As Parameter.StopAccrued) As Parameter.StopAccrued Implements [Interface].IStopAccrued.StopAccruedView
        Dim DAPostingPayment As New SQLEngine.LoanMnt.StopAccrued
        Return DAPostingPayment.StopAccruedView(oCustomClass)
    End Function
    Public Function StopAccruedInquiry(ByVal oCustomClass As Parameter.StopAccrued) As Parameter.StopAccrued Implements [Interface].IStopAccrued.StopAccruedInquiry
        Dim DAPostingPayment As New SQLEngine.LoanMnt.StopAccrued
        Return DAPostingPayment.StopAccruedInquiry(oCustomClass)
    End Function
    Public Function StopAccruedReversalList(ByVal oCustomClass As Parameter.StopAccrued) As Parameter.StopAccrued Implements [Interface].IStopAccrued.StopAccruedReversalList
        Dim DA As New SQLEngine.LoanMnt.StopAccrued
        Return DA.StopAccruedReversalList(oCustomClass)
    End Function
End Class
