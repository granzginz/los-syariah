

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class OtherReceive : Inherits ComponentBase
    Implements IOtherReceive

    Private isError As Boolean

    Public Function GetStructTrans() As DataSet
        Dim lObjDataTable As New DataSet
        Dim dtTable As New DataTable("Receive")
        lObjDataTable.Tables.Add(dtTable)
        lObjDataTable.Tables("Receive").Columns.Add("PaymentAllocationID", System.Type.GetType("System.String"))
        lObjDataTable.Tables("Receive").Columns.Add("PaymentAllocationName", System.Type.GetType("System.String"))
        lObjDataTable.Tables("Receive").Columns.Add("Description", System.Type.GetType("System.String"))
        lObjDataTable.Tables("Receive").Columns.Add("Amount", System.Type.GetType("System.String"))
        lObjDataTable.Tables("Receive").Columns.Add("DepartmentID", System.Type.GetType("System.String"))
        lObjDataTable.Tables("Receive").Columns.Add("DepartmentName", System.Type.GetType("System.String"))
        Return lObjDataTable
    End Function

    Public Function GetTableTrans(ByVal customclass As Parameter.OtherReceive, ByVal strFile As String) As Parameter.OtherReceive Implements [Interface].IOtherReceive.GetTableTrans
        Dim CheckTransNo As DataRow()
        Dim lObjPDC As New ArrayList
        Dim pstrFile As String
        Dim gStrPath As String
        Dim dsOReceive As New DataSet

        'isError = False

        Dim CustomPDC As New Parameter.OtherReceive
        With customclass
            .IsValidTrans = True
            pstrFile = "C:\temp\" + strFile & "_O_Receive"
            If .hpsxml = "1" Then
                If File.Exists(pstrFile & ".xsd") Then
                    File.Delete(pstrFile & ".xsd")
                End If
                If File.Exists(pstrFile & ".xml") Then
                    File.Delete(pstrFile & ".xml")
                End If
            Else

                If .flagdelete = "1" Then
                    If Not File.Exists(pstrFile & ".xsd") Then
                        dsOReceive = GetStructTrans()
                        dsOReceive.WriteXmlSchema(pstrFile & ".xsd")
                        dsOReceive.ReadXmlSchema(pstrFile & ".xsd")
                    End If
                    If Not File.Exists(pstrFile & ".xml") Then
                        dsOReceive.WriteXml(pstrFile & ".xml")
                        dsOReceive.AcceptChanges()
                    Else
                        dsOReceive.ReadXmlSchema(pstrFile & ".xsd")
                        dsOReceive.ReadXml(pstrFile & ".xml")
                        dsOReceive.AcceptChanges()
                    End If

                    dsOReceive = Generate_TableDelete(customclass, dsOReceive)
                    dsOReceive.WriteXml(pstrFile & ".xml")
                    dsOReceive.AcceptChanges()
                    customclass.listReceive = dsOReceive.Tables("Receive")
                Else
                    If Not File.Exists(pstrFile & ".xsd") Then
                        dsOReceive = GetStructTrans()
                        dsOReceive.WriteXmlSchema(pstrFile & ".xsd")
                        dsOReceive.ReadXmlSchema(pstrFile & ".xsd")
                    End If
                    If Not File.Exists(pstrFile & ".xml") Then
                        dsOReceive.WriteXml(pstrFile & ".xml")
                        dsOReceive.AcceptChanges()
                    Else
                        dsOReceive.ReadXmlSchema(pstrFile & ".xsd")
                        dsOReceive.ReadXml(pstrFile & ".xml")
                        dsOReceive.AcceptChanges()
                        CheckTransNo = dsOReceive.Tables("Receive").Select("PaymentAllocationID = '" & .PaymentAllocationID & "' and DepartmentID = '" & .DepartementID & "' ")
                        If CheckTransNo.Length > 0 Then
                            .IsValidTrans = False
                            Return customclass
                        End If
                    End If
                    dsOReceive = Generate_Table(customclass, dsOReceive)
                    dsOReceive.WriteXml(pstrFile & ".xml")
                    dsOReceive.AcceptChanges()
                    customclass.listReceive = dsOReceive.Tables("Receive")

                End If
            End If
        End With
        Return customclass
    End Function

    Private Function Generate_Table(ByVal CustomClass As Parameter.OtherReceive, ByVal dsOReceive As DataSet) As DataSet
        Dim lObjDataRow As DataRow
        Dim CheckPDCNo As DataRow()
        Dim strZero As String
        Dim lCounter As Integer
        Dim SerialNoGiroNo As Int64
        Dim StrPDCNo As String
        Dim FrequentDueDate As Date
        'strZero = ""
        With CustomClass
            lObjDataRow = dsOReceive.Tables("Receive").NewRow()
            lObjDataRow("PaymentAllocationID") = .PaymentAllocationID
            lObjDataRow("PaymentAllocationName") = .PaymentAllocationdesc
            lObjDataRow("Description") = .desc
            lObjDataRow("Amount") = .AmountTrans
            lObjDataRow("DepartmentID") = .DepartementID
            lObjDataRow("DepartmentName") = .DepartmentName

            dsOReceive.Tables("Receive").Rows.Add(lObjDataRow)
        End With
        Return dsOReceive
    End Function

    Private Function Generate_TableDelete(ByVal CustomClass As Parameter.OtherReceive, ByVal dsOReceive As DataSet) As DataSet

        With CustomClass
            dsOReceive.Tables("Receive").Rows.RemoveAt(CInt(.IndexDelete))
            Return dsOReceive
        End With
    End Function


    Public Function SaveTrans(ByVal customclass As Parameter.OtherReceive, ByVal strFile As String) As Parameter.OtherReceive Implements [Interface].IOtherReceive.SaveTrans
        Dim DASaveOreceiveTrans As New SQLEngine.LoanMnt.OtherReceive
        With customclass
            Dim pstrFile As String = "C:\temp\" + strFile & "_O_Receive"
            Dim dsNewOReceive As New DataSet
            dsNewOReceive.ReadXml(pstrFile & ".xml")
            Return DASaveOreceiveTrans.SaveOReceiveTrans(customclass, dsNewOReceive.Tables("Receive"))
            .hpsxml = "1"
        End With
    End Function
End Class
