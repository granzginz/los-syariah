


#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class TransferFundReconcile : Inherits ComponentBase

    Implements ITransferFundReconcile
    Public Function ListTransfer(ByVal customclass As Parameter.TransferAccount) As Parameter.TransferAccount Implements [Interface].ITransferFundReconcile.ListTransfer
        Dim DAListT As New SQLEngine.LoanMnt.TransferFundReconcile
        Return DAListT.ListTransfer(customclass)
    End Function

    Public Function SaveTransfer(ByVal customclass As Parameter.TransferAccount) As Parameter.TransferAccount Implements [Interface].ITransferFundReconcile.SaveTransfer
        Dim DAListT As New SQLEngine.LoanMnt.TransferFundReconcile
        Return DAListT.SaveTransfer(customclass)
    End Function
End Class
