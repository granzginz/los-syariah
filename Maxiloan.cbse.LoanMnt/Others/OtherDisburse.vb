

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class OtherDisburse : Inherits ComponentBase
    Implements IOtherDisburse

    Private isError As Boolean

    Public Function GetStructTrans() As DataSet
        Dim lObjDataTable As New DataSet
        Dim dtTable As New DataTable("Disburse")
        lObjDataTable.Tables.Add(dtTable)
        lObjDataTable.Tables("Disburse").Columns.Add("PaymentAllocationID", System.Type.GetType("System.String"))
        lObjDataTable.Tables("Disburse").Columns.Add("PaymentAllocationName", System.Type.GetType("System.String"))
        lObjDataTable.Tables("Disburse").Columns.Add("DepartmentID", System.Type.GetType("System.String"))
        lObjDataTable.Tables("Disburse").Columns.Add("DepartmentName", System.Type.GetType("System.String"))
        lObjDataTable.Tables("Disburse").Columns.Add("Description", System.Type.GetType("System.String"))
        lObjDataTable.Tables("Disburse").Columns.Add("Amount", System.Type.GetType("System.String"))
        Return lObjDataTable
    End Function

    Public Function GetTableTrans(ByVal customclass As Parameter.OtherReceive, ByVal strFile As String) As Parameter.OtherReceive Implements [Interface].IOtherDisburse.GetTableTrans
        Dim CheckTransNo As DataRow()
        Dim lObjPDC As New ArrayList
        Dim pstrFile As String
        Dim dsODisburse As New DataSet

        'isError = False

        Dim CustomPDC As New Parameter.OtherReceive
        With customclass
            .IsValidTrans = True
            pstrFile = "C:\temp\" + strFile & "_O_Disburse"
            If .hpsxml = "1" Then
                If File.Exists(pstrFile & ".xsd") Then
                    File.Delete(pstrFile & ".xsd")
                End If
                If File.Exists(pstrFile & ".xml") Then
                    File.Delete(pstrFile & ".xml")
                End If
            Else

                If .flagdelete = "1" Then
                    If Not File.Exists(pstrFile & ".xsd") Then
                        dsODisburse = GetStructTrans()
                        dsODisburse.WriteXmlSchema(pstrFile & ".xsd")
                        dsODisburse.ReadXmlSchema(pstrFile & ".xsd")
                    End If
                    If Not File.Exists(pstrFile & ".xml") Then
                        dsODisburse.WriteXml(pstrFile & ".xml")
                        dsODisburse.AcceptChanges()
                    Else
                        dsODisburse.ReadXmlSchema(pstrFile & ".xsd")
                        dsODisburse.ReadXml(pstrFile & ".xml")
                        dsODisburse.AcceptChanges()
                    End If

                    dsODisburse = Generate_TableDelete(customclass, dsODisburse)
                    dsODisburse.WriteXml(pstrFile & ".xml")
                    dsODisburse.AcceptChanges()
                    customclass.listReceive = dsODisburse.Tables("Disburse")
                Else



                    If Not File.Exists(pstrFile & ".xsd") Then
                        dsODisburse = GetStructTrans()
                        dsODisburse.WriteXmlSchema(pstrFile & ".xsd")
                        dsODisburse.ReadXmlSchema(pstrFile & ".xsd")
                    End If
                    If Not File.Exists(pstrFile & ".xml") Then
                        dsODisburse.WriteXml(pstrFile & ".xml")
                        dsODisburse.AcceptChanges()
                    Else
                        dsODisburse.ReadXmlSchema(pstrFile & ".xsd")
                        dsODisburse.ReadXml(pstrFile & ".xml")
                        dsODisburse.AcceptChanges()
                        CheckTransNo = dsODisburse.Tables("Disburse").Select("PaymentAllocationID = '" & .PaymentAllocationID & "' and DepartmentID = '" & .DepartementID & "' ")
                        If CheckTransNo.Length > 0 Then
                            .IsValidTrans = False
                            Return customclass
                        End If
                    End If
                    dsODisburse = Generate_Table(customclass, dsODisburse)
                    dsODisburse.WriteXml(pstrFile & ".xml")
                    dsODisburse.AcceptChanges()
                    customclass.listReceive = dsODisburse.Tables("Disburse")

                End If
            End If
        End With
        Return customclass
    End Function


    Private Function Generate_Table(ByVal CustomClass As Parameter.OtherReceive, ByVal dsODisburse As DataSet) As DataSet
        Dim lObjDataRow As DataRow
        With CustomClass
            lObjDataRow = dsODisburse.Tables("Disburse").NewRow()
            lObjDataRow("PaymentAllocationID") = .PaymentAllocationID
            lObjDataRow("PaymentAllocationName") = .PaymentAllocationdesc
            lObjDataRow("DepartmentID") = .DepartementID
            lObjDataRow("DepartmentName") = .DepartmentName
            lObjDataRow("Description") = .Desc
            lObjDataRow("Amount") = .AmountTrans
            dsODisburse.Tables("Disburse").Rows.Add(lObjDataRow)
        End With
        Return dsODisburse
    End Function

    Private Function Generate_TableDelete(ByVal CustomClass As Parameter.OtherReceive, ByVal dsODisburse As DataSet) As DataSet

        With CustomClass
            dsODisburse.Tables("Disburse").Rows.RemoveAt(CInt(.IndexDelete))
            Return dsODisburse
        End With
    End Function

    Public Function SaveTrans(ByVal customclass As Parameter.OtherReceive, ByVal strFile As String) As String Implements [Interface].IOtherDisburse.SaveTrans
        Dim DASaveOdsODisburseTrans As New SQLEngine.LoanMnt.OtherDisburse
        With customclass

            If strFile.Trim <> "" Then
                Dim pstrFile As String = "C:\temp\" + strFile & "_O_Disburse"
                Dim dsNewODisburse As New DataSet
                dsNewODisburse.ReadXml(pstrFile & ".xml")
                Return DASaveOdsODisburseTrans.SaveODisburseTrans(customclass, dsNewODisburse.Tables("Disburse"))
                If File.Exists(pstrFile & ".xsd") Then
                    File.Delete(pstrFile & ".xsd")
                End If
                If File.Exists(pstrFile & ".xml") Then
                    File.Delete(pstrFile & ".xml")
                End If
            Else
                Dim table As New DataTable
                Return DASaveOdsODisburseTrans.SaveODisburseTrans(customclass, table)
            End If

        End With
    End Function

    Public Function OtherDisburseAdd(ByVal customclass As Parameter.OtherReceive, ByVal strFile As String) As Parameter.OtherReceive Implements [Interface].IOtherDisburse.OtherDisburseAdd
        Dim oSQLE As New SQLEngine.LoanMnt.OtherDisburse
        With customclass
            If strFile.Trim <> "" Then
                Dim pstrFile As String = "C:\temp\" + strFile & "_O_Disburse"
                Dim dsNewODisburse As New DataSet
                dsNewODisburse.ReadXml(pstrFile & ".xml")
                Return oSQLE.OtherDisburseAdd(customclass, dsNewODisburse.Tables("Disburse"))
                If File.Exists(pstrFile & ".xsd") Then
                    File.Delete(pstrFile & ".xsd")
                End If
                If File.Exists(pstrFile & ".xml") Then
                    File.Delete(pstrFile & ".xml")
                End If
            Else
                Dim table As New DataTable
                Return oSQLE.OtherDisburseAdd(customclass, table)
            End If
            
        End With
    End Function
End Class
