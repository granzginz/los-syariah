
#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.SQLEngine.LoanMnt
Imports Maxiloan.Parameter

#End Region

Public Class OtherReceiveAgreementRelated : Inherits ComponentBase
    Implements IOtherReceiveAgreementRelated
    Private isError As Boolean
    Public Function GetStructTrans() As DataSet
        Dim lObjDataTable As New DataSet
        Dim dtTable As New DataTable("ReceiveAgreementRelated")
        lObjDataTable.Tables.Add(dtTable)
        lObjDataTable.Tables("ReceiveAgreementRelated").Columns.Add("PaymentAllocationID", System.Type.GetType("System.String"))
        lObjDataTable.Tables("ReceiveAgreementRelated").Columns.Add("PaymentAllocationName", System.Type.GetType("System.String"))
        lObjDataTable.Tables("ReceiveAgreementRelated").Columns.Add("DepartmentID", System.Type.GetType("System.String"))
        lObjDataTable.Tables("ReceiveAgreementRelated").Columns.Add("DepartmentName", System.Type.GetType("System.String"))
        lObjDataTable.Tables("ReceiveAgreementRelated").Columns.Add("Description", System.Type.GetType("System.String"))
        lObjDataTable.Tables("ReceiveAgreementRelated").Columns.Add("Amount", System.Type.GetType("System.String"))
        Return lObjDataTable
    End Function

    Public Function GetTableOtherReceiveAgreementRelated(ByVal customclass As Parameter.OtherReceiveAgreement, ByVal strFile As String) As Parameter.OtherReceiveAgreement Implements [Interface].IOtherReceiveAgreementRelated.GetTableOtherReceiveAgreementRelated
        Dim CheckTransNo As DataRow()
        Dim lObjPDC As New ArrayList
        Dim pstrFile As String
        Dim gStrPath As String
        Dim dsOReceive As New DataSet

        'isError = False

        Dim CustomPDC As New Parameter.OtherReceiveAgreement
        With customclass
            .IsValidTrans = True
            pstrFile = "C:\temp\" + strFile & "_O_ReceiveAgreementRelated"
            If .EraseXML = "1" Then
                If File.Exists(pstrFile & ".xsd") Then
                    File.Delete(pstrFile & ".xsd")
                End If
                If File.Exists(pstrFile & ".xml") Then
                    File.Delete(pstrFile & ".xml")
                End If
            Else

                If .flagdelete = "1" Then
                    If Not File.Exists(pstrFile & ".xsd") Then
                        dsOReceive = GetStructTrans()
                        dsOReceive.WriteXmlSchema(pstrFile & ".xsd")
                        dsOReceive.ReadXmlSchema(pstrFile & ".xsd")
                    End If
                    If Not File.Exists(pstrFile & ".xml") Then
                        dsOReceive.WriteXml(pstrFile & ".xml")
                        dsOReceive.AcceptChanges()
                    Else
                        dsOReceive.ReadXmlSchema(pstrFile & ".xsd")
                        dsOReceive.ReadXml(pstrFile & ".xml")
                        dsOReceive.AcceptChanges()
                    End If

                    dsOReceive = Generate_TableDelete(customclass, dsOReceive)
                    dsOReceive.WriteXml(pstrFile & ".xml")
                    dsOReceive.AcceptChanges()
                    customclass.listReceive = dsOReceive.Tables("ReceiveAgreementRelated")
                Else
                    If Not File.Exists(pstrFile & ".xsd") Then
                        dsOReceive = GetStructTrans()
                        dsOReceive.WriteXmlSchema(pstrFile & ".xsd")
                        dsOReceive.ReadXmlSchema(pstrFile & ".xsd")
                    End If
                    If Not File.Exists(pstrFile & ".xml") Then
                        dsOReceive.WriteXml(pstrFile & ".xml")
                        dsOReceive.AcceptChanges()
                    Else
                        dsOReceive.ReadXmlSchema(pstrFile & ".xsd")
                        dsOReceive.ReadXml(pstrFile & ".xml")
                        dsOReceive.AcceptChanges()
                        CheckTransNo = dsOReceive.Tables("ReceiveAgreementRelated").Select("PaymentAllocationID = '" & .PaymentAllocationID & "' and DepartmentID = '" & .DepartementID & "' ")
                        If CheckTransNo.Length > 0 Then
                            .IsValidTrans = False
                            Return customclass
                        End If
                    End If
                    dsOReceive = Generate_Table(customclass, dsOReceive)
                    dsOReceive.WriteXml(pstrFile & ".xml")
                    dsOReceive.AcceptChanges()
                    customclass.listReceive = dsOReceive.Tables("ReceiveAgreementRelated")

                End If
            End If
        End With
        Return customclass
    End Function


    Private Function Generate_Table(ByVal CustomClass As Parameter.OtherReceiveAgreement, ByVal dsOReceive As DataSet) As DataSet
        Dim lObjDataRow As DataRow
        Dim CheckPDCNo As DataRow()
        Dim strZero As String
        Dim lCounter As Integer
        Dim SerialNoGiroNo As Int64
        Dim StrPDCNo As String
        Dim FrequentDueDate As Date
        'strZero = ""
        With CustomClass
            lObjDataRow = dsOReceive.Tables("Receive").NewRow()
            lObjDataRow("PaymentAllocationID") = .PaymentAllocationID
            lObjDataRow("PaymentAllocationName") = .PaymentAllocationdesc
            lObjDataRow("DepartmentID") = .DepartementID
            lObjDataRow("DepartmentName") = .DepartmentName
            lObjDataRow("Description") = .Desc
            lObjDataRow("Amount") = .AmountTrans
            dsOReceive.Tables("Receive").Rows.Add(lObjDataRow)
        End With
        Return dsOReceive
    End Function

    Private Function Generate_TableDelete(ByVal CustomClass As Parameter.OtherReceiveAgreement, ByVal dsOReceive As DataSet) As DataSet
        With CustomClass
            dsOReceive.Tables("ReceiveAgreementRelated").Rows.RemoveAt(CInt(.IndexDelete))
            Return dsOReceive
        End With
    End Function

    Public Sub OtherReceiveAgreementRelatedSaved(ByVal customclass As Parameter.OtherReceiveAgreement) Implements [Interface].IOtherReceiveAgreementRelated.OtherReceiveAgreementRelatedSaved
        Dim DASaveOdsOReceiveTrans As New SQLEngine.LoanMnt.OtherReceiveAgreementRelated
        With customclass
            Dim pstrFile As String = "C:\temp\" + customclass.FileName & "_O_ReceiveAgreementRelated"
            Dim dsNewOReceive As New DataSet
            dsNewOReceive.ReadXml(pstrFile & ".xml")
            customclass.listReceive = dsNewOReceive.Tables("ReceiveAgreementRelated")
            DASaveOdsOReceiveTrans.OtherReceiveAgreementRelatedSaved(customclass)
            If File.Exists(pstrFile & ".xsd") Then
                File.Delete(pstrFile & ".xsd")
            End If
            If File.Exists(pstrFile & ".xml") Then
                File.Delete(pstrFile & ".xml")
            End If
        End With
    End Sub
End Class
