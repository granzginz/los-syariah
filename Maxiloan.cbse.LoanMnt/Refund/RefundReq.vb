

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class RefundReq : Inherits ComponentBase

    Implements IRefundReq



    Public Function ListRefundReq(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundReq.ListRefundReq
        Dim DAListRefundReq As New SQLEngine.LoanMnt.RefundReq
        Return DAListRefundReq.RefundReqList(customclass)
    End Function

    Public Function SaveRefundReq(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundReq.SaveRefundReq
        Dim DASaveRefundReq As New SQLEngine.LoanMnt.RefundReq
        Return DASaveRefundReq.SaveRefundReq(customclass)
    End Function

    Public Function SaveRefundReqH(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundReq.SaveRefundReqH
        Dim DASaveRefundReq As New SQLEngine.LoanMnt.RefundReq
        Return DASaveRefundReq.SaveRefundReqH(customclass)
    End Function

    Public Function ListRefundReqCust(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundReq.ListRefundReqCust
        Dim DASaveRefundReq As New SQLEngine.LoanMnt.RefundReq
        Return DASaveRefundReq.RefundReqListCust(customclass)
    End Function
End Class
