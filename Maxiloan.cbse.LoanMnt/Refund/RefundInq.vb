

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class RefundInq : Inherits ComponentBase

    Implements IRefundInq


    Public Function ListRefundReport(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundInq.ListRefundReport
        Dim DAListRefundReport As New SQLEngine.LoanMnt.RefundInq
        Return DAListRefundReport.ReportRefundInq(customclass)
    End Function

    Public Function ViewRefund(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundInq.ViewRefund
        Dim DAViewRefund As New SQLEngine.LoanMnt.RefundInq
        Return DAViewRefund.RefundView(customclass)
    End Function

    Public Function ViewRefundApproval(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundInq.ViewRefundApproval
        Dim DAViewRefund As New SQLEngine.LoanMnt.RefundInq
        Return DAViewRefund.RefundViewApproval(customclass)
    End Function
End Class
