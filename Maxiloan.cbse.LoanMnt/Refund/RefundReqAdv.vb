

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class RefundReqAdv : Inherits ComponentBase

    Implements IRefundReqAdv

    '=================== Request =============================
    Public Function ListRefundReqAdv(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundReqAdv.ListRefundReqAdv
        Dim DAListRefundReqAdv As New SQLEngine.LoanMnt.RefundReqAdv
        Return DAListRefundReqAdv.RefundReqListAdv(customclass)
    End Function
    Public Function SelectedRefundReqAdv(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundReqAdv.SelectedRefundReqAdv
        Dim DAListRefundReqAdv As New SQLEngine.LoanMnt.RefundReqAdv
        Return DAListRefundReqAdv.SelectedRefundReqAdv(customclass)
    End Function
    Public Function GetProduct(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundReqAdv.GetProduct
        Dim DAListRefundReqAdv As New SQLEngine.LoanMnt.RefundReqAdv
        Return DAListRefundReqAdv.GetProduct(customclass)
    End Function
    'Public Function GetRequestNo(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundReqAdv.GetRequestNo
    '    Dim DAListRefundReqAdv As New SQLEngine.LoanMnt.RefundReqAdv
    '    Return DAListRefundReqAdv.GetRequestNo(customclass)
    'End Function
    Public Function SaveRefundReqAdv(ByVal customclass As Parameter.Refund, ByVal oData1 As DataTable) As Parameter.Refund Implements [Interface].IRefundReqAdv.SaveRefundReqAdv
        Dim DAListRefundReqAdv As New SQLEngine.LoanMnt.RefundReqAdv
        Return DAListRefundReqAdv.SaveRefundReqAdv(customclass, oData1)
    End Function


    '================ Inquiry =================================
    Public Function ListRefundInqAdv(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundReqAdv.ListRefundInqAdv
        Dim DAListRefundReqAdv As New SQLEngine.LoanMnt.RefundReqAdv
        Return DAListRefundReqAdv.RefundInqListAdv(customclass)
    End Function
    Public Function ListRefundInqAdvByOSRefund(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundReqAdv.ListRefundInqAdvByOSRefund
        Dim DAListRefundReqAdv As New SQLEngine.LoanMnt.RefundReqAdv
        Return DAListRefundReqAdv.RefundInqListAdvByOSRefund(customclass)
    End Function
    Public Function ListingReportByRefund(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundReqAdv.ListingReportByRefund
        Dim DAListRefundReqAdv As New SQLEngine.LoanMnt.RefundReqAdv
        Return DAListRefundReqAdv.ListingReportByRefund(customclass)
    End Function
    Public Function ListingReportByOSRefund(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundReqAdv.ListingReportByOSRefund
        Dim DAListRefundReqAdv As New SQLEngine.LoanMnt.RefundReqAdv
        Return DAListRefundReqAdv.ListingReportByOSRefund(customclass)
    End Function
    Public Function ViewRefundAdvance1(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundReqAdv.ViewRefundAdvance1
        Dim DAListRefundReqAdv As New SQLEngine.LoanMnt.RefundReqAdv
        Return DAListRefundReqAdv.ViewRefundAdvance1(customclass)
    End Function
    Public Function ViewRefundAdvance2(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundReqAdv.ViewRefundAdvance2
        Dim DAListRefundReqAdv As New SQLEngine.LoanMnt.RefundReqAdv
        Return DAListRefundReqAdv.ViewRefundAdvance2(customclass)
    End Function
    Public Function ViewRefundDetail(ByVal customclass As Parameter.Refund) As Parameter.Refund Implements [Interface].IRefundReqAdv.ViewRefundDetail
        Dim DAListRefundReqAdv As New SQLEngine.LoanMnt.RefundReqAdv
        Return DAListRefundReqAdv.ViewRefundDetail(customclass)
    End Function
End Class
