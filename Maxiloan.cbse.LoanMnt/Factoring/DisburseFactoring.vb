
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan
Imports Maxiloan.SQLEngine.LoanMnt
Imports Maxiloan.Parameter


#End Region
Public Class DisburseFactoring : Inherits ComponentBase
    Implements IDisburseFactoring


    Public Function InstallmentFactoringApprovalList(ByVal customclass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IDisburseFactoring.DisburseApprovalList
        Dim DataDA As New SQLEngine.LoanMnt.DisburseFactoring
        Return DataDA.DisburseApprovalList(customclass)
    End Function

    Public Function GetCboUserAprPCAll(ByVal customclass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IDisburseFactoring.GetCboUserAprPCAll
        Dim DataDA As New SQLEngine.LoanMnt.DisburseFactoring
        Return DataDA.GetCboUserAprPCAll(customclass)
    End Function

    Public Function GetCboUserApproval(ByVal customclass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IDisburseFactoring.GetCboUserApproval
        Dim DataDA As New SQLEngine.LoanMnt.DisburseFactoring
        Return DataDA.GetCboUserApproval(customclass)
    End Function

    Public Function ApproveSaveToNextPerson(ByVal customclass As Parameter.FactoringInvoice) As String Implements [Interface].IDisburseFactoring.ApproveSaveToNextPerson
        Dim DataDA As New SQLEngine.LoanMnt.DisburseFactoring
        Return DataDA.ApproveSaveToNextPerson(customclass)
    End Function

    Public Function ApproveSave(ByVal customclass As Parameter.FactoringInvoice) As String Implements [Interface].IDisburseFactoring.ApproveSave
        Dim DataDA As New SQLEngine.LoanMnt.DisburseFactoring
        Return DataDA.ApproveSave(customclass)
    End Function

    Public Function ApproveisFinal(ByVal customclass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IDisburseFactoring.ApproveisFinal
        Dim DataDA As New SQLEngine.LoanMnt.DisburseFactoring
        Return DataDA.ApproveisFinal(customclass)
    End Function

    Public Function ApproveIsValidApproval(ByVal customclass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice Implements [Interface].IDisburseFactoring.ApproveIsValidApproval
        Dim DataDA As New SQLEngine.LoanMnt.DisburseFactoring
        Return DataDA.ApproveIsValidApproval(customclass)
    End Function


End Class
