
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class Fiducia
    Implements IFiducia

    Public Function GetNotary(ByVal customclass As Parameter.Fiducia) As Parameter.Fiducia Implements [Interface].IFiducia.GetNotary
        Dim DA As New SQLEngine.LoanMnt.Fiducia
        Return DA.GetNotary(customclass)
    End Function

    Public Function GetNotaryCharge(ByVal customclass As Parameter.Fiducia) As Parameter.Fiducia Implements [Interface].IFiducia.GetNotaryCharge
        Dim DA As New SQLEngine.LoanMnt.Fiducia
        Return DA.GetNotaryCharge(customclass)
    End Function


    Sub FiduciaAdd(ByVal customclass As Parameter.Fiducia) Implements [Interface].IFiducia.FiduciaAdd
        Dim DA As New SQLEngine.LoanMnt.Fiducia
        DA.FiduciaAdd(customclass)
    End Sub

    Sub FiduciaAktaReceive(ByVal customclass As Parameter.Fiducia) Implements [Interface].IFiducia.FiduciaAktaReceive
        Dim DA As New SQLEngine.LoanMnt.Fiducia
        DA.FiduciaAktaReceive(customclass)
    End Sub

    Sub FiduciaFarsial(ByVal customclass As Parameter.Fiducia) Implements [Interface].IFiducia.FiduciaFarsial
        Dim DA As New SQLEngine.LoanMnt.Fiducia
        DA.FiduciaFarsial(customclass)
    End Sub


    Sub NotaryChargeAdd(ByVal customclass As Parameter.Fiducia) Implements [Interface].IFiducia.NotaryChargeAdd
        Dim DA As New SQLEngine.LoanMnt.Fiducia
        DA.NotaryChargeAdd(customclass)
    End Sub

    Sub AddNotary(ByVal customclass As Parameter.Fiducia) Implements [Interface].IFiducia.AddNotary
        Dim DA As New SQLEngine.LoanMnt.Fiducia
        DA.AddNotary(customclass)
    End Sub

    Public Sub GenerateFiduciaRoyaRegisterNo(ByVal customclass As Parameter.Fiducia) Implements [Interface].IFiducia.GenerateFiduciaRoyaRegisterNo
        Dim DA As New SQLEngine.LoanMnt.Fiducia
        DA.GenerateFiduciaRoyaRegisterNo(customclass)
    End Sub
End Class
