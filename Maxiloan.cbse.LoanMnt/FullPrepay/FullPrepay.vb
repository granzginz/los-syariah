
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class FullPrepay
    Implements IFullPrepay

    Public Sub FullPrepayRequest(ByVal oCustomClass As Parameter.FullPrepay) Implements [Interface].IFullPrepay.FullPrepayRequest
        Dim DAPostingPayment As New SQLEngine.LoanMnt.FullPrepayRequest
        DAPostingPayment.SaveFullPrepayRequest(oCustomClass)
    End Sub

    Public Sub FullPrepayRequestOL(ByVal oCustomClass As Parameter.FullPrepay) Implements [Interface].IFullPrepay.FullPrepayRequestOL
        Dim DAPostingPayment As New SQLEngine.LoanMnt.FullPrepayRequest
        DAPostingPayment.SaveFullPrepayRequestOL(oCustomClass)
    End Sub

    Public Sub FullPrepayCancel(ByVal oCustomClass As Parameter.FullPrepay) Implements [Interface].IFullPrepay.FullPrepayCancel
        Dim DAPostingPayment As New SQLEngine.LoanMnt.FullPrepayExecution
        DAPostingPayment.SaveFullPrepayCancel(oCustomClass)
    End Sub

    Public Sub FullPrepayExecution(ByVal oCustomClass As Parameter.FullPrepay) Implements [Interface].IFullPrepay.FullPrepayExecution
        Dim DAPostingPayment As New SQLEngine.LoanMnt.FullPrepayExecution
        DAPostingPayment.SaveFullPrepayExecute(oCustomClass)
    End Sub

    Public Sub FullPrepayExecutionOL(ByVal oCustomClass As Parameter.FullPrepay) Implements [Interface].IFullPrepay.FullPrepayExecutionOL
        Dim DAPostingPayment As New SQLEngine.LoanMnt.FullPrepayExecution
        DAPostingPayment.SaveFullPrepayExecuteOL(oCustomClass)
    End Sub

    Public Function FullPrepayInfo(ByVal oCustomClass As Parameter.FullPrepay) As Parameter.FullPrepay Implements [Interface].IFullPrepay.FullPrepayInfo
        Dim DAPostingPayment As New SQLEngine.LoanMnt.FullPrepayInquiry
        Return DAPostingPayment.FullPrepayView(oCustomClass)
    End Function
    Public Function InqPrepayment(ByVal ocustomclass As Parameter.FullPrepay) As Parameter.FullPrepay Implements [Interface].IFullPrepay.InqPrepayment
        Dim DAPrepayment As New SQLEngine.LoanMnt.FullPrepayInquiry
        Return DAPrepayment.InqPrepayment(ocustomclass)
    End Function

    Public Function PrintTrial(ByVal ocustomclass As Parameter.FullPrepay) As System.Data.DataSet Implements [Interface].IFullPrepay.PrintTrial
        Dim DAPrepayment As New SQLEngine.LoanMnt.FullPrepayInquiry
        Return DAPrepayment.PrintTrial(ocustomclass)
    End Function

    Public Function PrintTrialOL(ByVal ocustomclass As Parameter.FullPrepay) As System.Data.DataSet Implements [Interface].IFullPrepay.PrintTrialOL
        Dim DAPrepayment As New SQLEngine.LoanMnt.FullPrepayInquiry
        Return DAPrepayment.PrintTrialOL(ocustomclass)
    End Function
End Class
