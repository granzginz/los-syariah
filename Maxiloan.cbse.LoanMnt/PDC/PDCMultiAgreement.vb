
#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class PDCMultiAgreement : Inherits ComponentBase
    Implements IPDCMultiAgreement

    Public Function ListAgreementCustomer(ByVal oCustomClass As Parameter.PDCMultiAgreement) As Parameter.PDCMultiAgreement Implements [Interface].IPDCMultiAgreement.ListAgreementCustomer
        Dim DAListPDCMultiAgreement As New SQLEngine.LoanMnt.PDCMultiAgreement
        Return DAListPDCMultiAgreement.ListAgreementCustomer(oCustomClass)
    End Function
    Public Function AgreementInformation(ByVal oCustomClass As Parameter.PDCMultiAgreement) As System.Data.DataTable Implements [Interface].IPDCMultiAgreement.AgreementInformation
        Dim DAListPDCMultiAgreement As New SQLEngine.LoanMnt.PDCMultiAgreement
        Return DAListPDCMultiAgreement.AgreementInformation(oCustomClass)
    End Function

    Public Sub SavePDCMultiAgreement(ByVal oCustomClass As Parameter.PDCMultiAgreement) Implements [Interface].IPDCMultiAgreement.SavePDCMultiAgreement
        Dim DAListPDCMultiAgreement As New SQLEngine.LoanMnt.PDCMultiAgreement
        DAListPDCMultiAgreement.SavePDCMultiAgreement(oCustomClass)
    End Sub

    Public Function GetEditPDCMultiAgreement(ByVal oCustomClass As Parameter.PDCMultiAgreement) As Parameter.PDCMultiAgreement Implements [Interface].IPDCMultiAgreement.GetEditPDCMultiAgreement
        Dim DAListPDCMultiAgreement As New SQLEngine.LoanMnt.PDCMultiAgreement
        Return DAListPDCMultiAgreement.GetEditPDCMultiAgreement(oCustomClass)
    End Function

    Public Sub SaveEditPDCMultiAgreement(ByVal oCustomClass As Parameter.PDCMultiAgreement) Implements [Interface].IPDCMultiAgreement.SaveEditPDCMultiAgreement
        Dim DAListPDCMultiAgreement As New SQLEngine.LoanMnt.PDCMultiAgreement
        DAListPDCMultiAgreement.SaveEditPDCMultiAgreement(oCustomClass)
    End Sub
End Class
