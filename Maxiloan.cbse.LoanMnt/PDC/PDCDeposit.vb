

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region


Public Class PDCDeposit : Inherits ComponentBase
    Implements IPDCDeposit




    Public Function ListPDCDeposit(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCDeposit.ListPDCDeposit
        Dim DAListPDCDeposit As New SQLEngine.LoanMnt.PDCDeposit
        Return DAListPDCDeposit.ListPDCDeposit(customclass)
    End Function

    Public Function SavePDCDeposit(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCDeposit.SavePDCDeposit
        Dim DASavePDCDeposit As New SQLEngine.LoanMnt.PDCDeposit
        Return DASavePDCDeposit.SavePDCDeposit(customclass)
    End Function

    Public Function Generate_Table(ByVal customclass As Parameter.PDCReceive, ByVal dsPDCDeposit As DataSet) As DataSet
        Dim lObjDataRow As DataRow
        Dim lCounter As Integer

        With customclass
            For lCounter = 0 To .NumPDC
                '.Columns.Add("ApplicationID", GetType(System.String))
                '.Columns.Add("AgreementNo", GetType(System.String))
                '.Columns.Add("Name", GetType(System.String))
                '.Columns.Add("BANKID", GetType(System.String))
                '.Columns.Add("GIRONO", GetType(System.String))
                '.Columns.Add("DueDate", GetType(System.DateTime))
                '.Columns.Add("PDCAmount", GetType(System.String))
                '.Columns.Add("IsInkaso", GetType(System.String))
                '.Columns.Add("IsCum", GetType(System.String))
                '.Columns.Add("Bounce", GetType(System.String))
                '.Columns.Add("BranchLocation", GetType(System.String))

                lObjDataRow = dsPDCDeposit.Tables("PDCDeposit").NewRow()
                lObjDataRow("ApplicationID") = .ApplicationID
                lObjDataRow("AgreementNo") = .Agreementno
                lObjDataRow("BANKID") = .BankID
                lObjDataRow("GIRONO") = .GiroNo
                lObjDataRow("DueDate") = .PDCDueDate
                lObjDataRow("PDCAmount") = .PDCAmount
                lObjDataRow("IsInkaso") = .IsInkaso
                lObjDataRow("IsCum") = .IsCumm
                lObjDataRow("Bounce") = .Bounce
                lObjDataRow("BranchLocation") = .BranchLocation
                dsPDCDeposit.Tables("PDCDeposit").Rows.Add(lObjDataRow)
            Next
        End With
        Return dsPDCDeposit

    End Function

    Public Function CheckDupPDCStatus(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCDeposit.CheckDupPDCStatus
        Dim DACheckDupPDCStatus As New SQLEngine.LoanMnt.PDCDeposit
        Return DACheckDupPDCStatus.SavePDCDeposit(customclass)
    End Function
End Class
