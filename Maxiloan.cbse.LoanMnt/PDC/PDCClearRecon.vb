

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class PDCClearRecon : Inherits ComponentBase
    Implements IPDCClearRecon
    Public Function ListPDCClearRecon(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCClearRecon.ListPDCClearRecon
        Dim DAListPDCDeposit As New SQLEngine.LoanMnt.PDCClearRecon
        Return DAListPDCDeposit.ListPDCClearRecon(customclass)
    End Function

    Public Function SavePDCClearRecon(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCClearRecon.SavePDCClearRecon
        Dim DAListPDCClearRecon As New SQLEngine.LoanMnt.PDCClearRecon
        Return DAListPDCClearRecon.SavePDCClearRecon(customclass)
    End Function

    Public Function ListPDCClearCRecon(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCClearRecon.ListPDCClearCRecon
        Dim DAListPDCClearCRecon As New SQLEngine.LoanMnt.PDCClearRecon
        Return DAListPDCClearCRecon.ListPDCClearCRecon(customclass)
    End Function

    Public Sub SavePDCClearCRecon(ByVal customclass As Parameter.PDCReceive) Implements [Interface].IPDCClearRecon.SavePDCClearCRecon
        Dim DAListPDCClearCRecon As New SQLEngine.LoanMnt.PDCClearRecon
        DAListPDCClearCRecon.SavePDCClearReconC(customclass)
    End Sub
End Class
