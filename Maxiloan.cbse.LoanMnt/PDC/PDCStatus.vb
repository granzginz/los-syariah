

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class PDCStatus : Inherits ComponentBase
    Implements IPDCStatus
    Public Function ListPDCStatus(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCStatus.ListPDCStatus
        Dim DAListPDCStatus As New SQLEngine.LoanMnt.PDCStatus
        Return DAListPDCStatus.ListPDCStatus(customclass)
    End Function

    Public Function ListPDCStatusDet(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCStatus.ListPDCStatusDet
        Dim DAListPDCStatusDet As New SQLEngine.LoanMnt.PDCStatus
        Return DAListPDCStatusDet.ListPDCStatusDet(customclass)
    End Function

    Public Sub SavePDCStatusDet(ByVal customclass As Parameter.PDCReceive) Implements [Interface].IPDCStatus.SavePDCStatusDet
        Dim DASavePDCStatusDet As New SQLEngine.LoanMnt.PDCStatus
        DASavePDCStatusDet.SavePDCStatusDet(customclass)
    End Sub
End Class
