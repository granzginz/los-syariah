
#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class PDCMTrans : Inherits ComponentBase
    Implements IPDCMTrans

    Private isError As Boolean

    Public Function GetStructPDC() As DataSet
        Dim lObjDataTable As New DataSet
        Dim dtTable As New DataTable("PDC")

        lObjDataTable.Tables.Add(dtTable)

        lObjDataTable.Tables("PDC").Columns.Add("PaymentAllocationID", System.Type.GetType("System.String"))
        lObjDataTable.Tables("PDC").Columns.Add("PaymentAllocationName", System.Type.GetType("System.String"))
        lObjDataTable.Tables("PDC").Columns.Add("Description", System.Type.GetType("System.String"))
        lObjDataTable.Tables("PDC").Columns.Add("Amount", System.Type.GetType("System.String"))
        Return lObjDataTable
    End Function

    Public Function ListPDCMTrans(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCMTrans.ListPDCMTrans
        Dim DAListPDCMTrans As New SQLEngine.LoanMnt.PDCMTrans
        Return DAListPDCMTrans.ListPDCMtrans(customclass)
    End Function

    Public Function GetTablePDC(ByVal customclass As Parameter.PDCReceive, ByVal strFile As String) As Parameter.PDCReceive Implements [Interface].IPDCMTrans.GetTablePDCTrans
        Dim CheckPDCNo As DataRow()
        Dim lObjHoliday As New Hashtable
        Dim lObjPDC As New ArrayList

        Dim pstrFile As String
        Dim gStrPath As String

        Dim dsPDC As New DataSet

        Dim DtPDC As DataTable
        Dim DtPDC2 As DataTable

        'isError = False

        Dim CustomPDC As New Parameter.PDCReceive
        With customclass
            .IsValidPDC = True
            pstrFile = "C:\temp\" + strFile & "_T_PDC"
            If .HpsXml = "1" Then
                If File.Exists(pstrFile & ".xsd") Then
                    File.Delete(pstrFile & ".xsd")
                End If
                If File.Exists(pstrFile & ".xml") Then
                    File.Delete(pstrFile & ".xml")
                End If
            Else

                If .FlagDelete = "1" Then
                    If Not File.Exists(pstrFile & ".xsd") Then
                        dsPDC = GetStructPDC()
                        dsPDC.WriteXmlSchema(pstrFile & ".xsd")
                        dsPDC.ReadXmlSchema(pstrFile & ".xsd")
                    End If
                    If Not File.Exists(pstrFile & ".xml") Then
                        dsPDC.WriteXml(pstrFile & ".xml")
                        dsPDC.AcceptChanges()
                    Else
                        dsPDC.ReadXmlSchema(pstrFile & ".xsd")
                        dsPDC.ReadXml(pstrFile & ".xml")
                        dsPDC.AcceptChanges()
                    End If

                    dsPDC = Generate_TableDelete(customclass, dsPDC)
                    dsPDC.WriteXml(pstrFile & ".xml")
                    dsPDC.AcceptChanges()
                    customclass.ListPDC = dsPDC.Tables("PDC")
                Else
                    CustomPDC = Is_ValidPDC(customclass)
                    If CustomPDC.IsValidPDC Then
                        If Not File.Exists(pstrFile & ".xsd") Then
                            dsPDC = GetStructPDC()
                            dsPDC.WriteXmlSchema(pstrFile & ".xsd")
                            dsPDC.ReadXmlSchema(pstrFile & ".xsd")
                        End If
                        If Not File.Exists(pstrFile & ".xml") Then
                            dsPDC.WriteXml(pstrFile & ".xml")
                            dsPDC.AcceptChanges()
                        Else
                            dsPDC.ReadXmlSchema(pstrFile & ".xsd")
                            dsPDC.ReadXml(pstrFile & ".xml")
                            dsPDC.AcceptChanges()
                            CheckPDCNo = dsPDC.Tables("PDC").Select("PaymentAllocationID = '" & .PaymentAllocationID & "'")
                            If CheckPDCNo.Length > 0 Then
                                .IsValidPDC = False
                                Return customclass
                            End If
                        End If
                        dsPDC = Generate_Table(customclass, dsPDC)
                        dsPDC.WriteXml(pstrFile & ".xml")
                        dsPDC.AcceptChanges()
                        customclass.ListPDC = dsPDC.Tables("PDC")
                    Else
                        customclass.IsValidPDC = False
                    End If
                End If
            End If
        End With
        Return customclass
    End Function

    Private Function Generate_Table(ByVal CustomClass As Parameter.PDCReceive, ByVal dsPDC As DataSet) As DataSet
        Dim lObjDataRow As DataRow
        Dim CheckPDCNo As DataRow()
        Dim strZero As String
        Dim lCounter As Integer
        Dim SerialNoGiroNo As Int64
        Dim StrPDCNo As String
        Dim FrequentDueDate As Date
        'strZero = ""
        With CustomClass
            lObjDataRow = dsPDC.Tables("PDC").NewRow()
            lObjDataRow("PaymentAllocationID") = .PaymentAllocationID
            lObjDataRow("PaymentAllocationName") = .PaymentAllocationName
            lObjDataRow("Description") = .Description
            lObjDataRow("Amount") = .TransAmount
            dsPDC.Tables("PDC").Rows.Add(lObjDataRow)
        End With
        Return dsPDC
    End Function

    Private Function Generate_TableDelete(ByVal CustomClass As Parameter.PDCReceive, ByVal dsPDC As DataSet) As DataSet

        With CustomClass
            dsPDC.Tables("PDC").Rows.RemoveAt(CInt(.IndexDelete))
            Return dsPDC
        End With
    End Function

    Public Function SavePDCTrans(ByVal customclass As Parameter.PDCReceive, ByVal strFile As String) As String Implements [Interface].IPDCMTrans.SavePDCTrans
        Dim DASavePDCMTrans As New SQLEngine.LoanMnt.PDCMTrans
        With customclass
            Dim pstrFile As String = "C:\temp\" + strFile & "_T_PDC"
            Dim dsNewPDC As New DataSet
            dsNewPDC.ReadXml(pstrFile & ".xml")
            Return DASavePDCMTrans.SavePDCMtrans(customclass, dsNewPDC.Tables("PDC"))
            .HpsXml = "1"
        End With
    End Function

#Region "Is_ValidPDC"
    Private Function Is_ValidPDC(ByVal CustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim lObjPDC As New ArrayList
        Dim lStrGiroNo As String = CustomClass.GiroNo
        Dim lStrGiroCheck As String = ""
        Dim lBytEnd As Integer = lStrGiroNo.Length - 1
        Dim lBytCounter As Integer
        Dim lStrPrefix As String = ""
        Dim lStrPDC As String = ""
        Dim lStrZero As String = ""
        Dim lStrCheck As String
        Dim lBlnValid As Boolean = True
        Dim lIntGiroNo As Int64

        For lBytCounter = 0 To lBytEnd
            If IsNumeric(lStrGiroNo.Chars(lBytCounter)) Then
                lStrPDC &= lStrGiroNo.Chars(lBytCounter)
            Else
                If lStrPDC <> "" Then CustomClass.IsValidPDC = False
                lStrPrefix &= lStrGiroNo.Chars(lBytCounter)
            End If
        Next
        If lStrPDC = "" Then CustomClass.IsValidPDC = False

        If lBlnValid Then
            'Check PDC
            lBytEnd = CustomClass.NumPDC
            lStrZero = lStrZero.PadLeft(lStrPDC.Length, CType("0", Char))
            For lBytCounter = 1 To lBytEnd
                lIntGiroNo = CLng(lStrPDC) + lBytCounter - 1
                If lIntGiroNo.ToString.Length >= lStrZero.Length Then
                    lStrCheck = lStrPrefix & lIntGiroNo
                Else
                    lStrCheck = lStrPrefix & lStrZero.Substring(0, lStrZero.Length - lIntGiroNo.ToString.Length) & lIntGiroNo
                End If
                If lObjPDC.Contains(lStrCheck.ToUpper) Then
                    CustomClass.IsValidPDC = False
                    Exit For
                Else
                    lStrGiroCheck &= CStr(IIf(lStrGiroCheck = "", "", ", ")) & "'" & lStrCheck & "'"
                End If
            Next
        End If

        Dim DAPdcCheckDuplikasi As New SQLEngine.LoanMnt.PDCReceive
        CustomClass.ListGiroNo = lStrGiroCheck
        If Not DAPdcCheckDuplikasi.CheckDuplikasiPDC(CustomClass) Then
            CustomClass.IsValidPDC = True
            CustomClass.ListGiroNo = lStrPDC
            CustomClass.PrefixPDC = lStrPrefix
        Else
            CustomClass.IsValidPDC = False
        End If
        Return CustomClass
    End Function
#End Region

    Public Function GetTablePDCEdit(ByVal customclass As Parameter.PDCReceive, ByVal strFile As String) As Parameter.PDCReceive Implements [Interface].IPDCMTrans.GetTablePDCEdit
        Dim CheckPDCNo As DataRow()
        Dim lObjHoliday As New Hashtable
        Dim lObjPDC As New ArrayList

        Dim pstrFile As String
        Dim gStrPath As String

        Dim dsPDC As New DataSet

        Dim DtPDC As DataTable
        Dim DtPDC2 As DataTable
        Dim CustomPDC As New Parameter.PDCReceive
        With customclass
            .IsValidPDC = True
            pstrFile = "C:\temp\" + strFile & "_T_PDC"
            If .HpsXml = "1" Then
                If File.Exists(pstrFile & ".xsd") Then
                    File.Delete(pstrFile & ".xsd")
                End If
                If File.Exists(pstrFile & ".xml") Then
                    File.Delete(pstrFile & ".xml")
                End If
            Else

                If .FlagDelete = "1" Then
                    If Not File.Exists(pstrFile & ".xsd") Then
                        dsPDC = GetStructPDC()
                        dsPDC.WriteXmlSchema(pstrFile & ".xsd")
                        dsPDC.ReadXmlSchema(pstrFile & ".xsd")
                    End If
                    If Not File.Exists(pstrFile & ".xml") Then
                        dsPDC.WriteXml(pstrFile & ".xml")
                        dsPDC.AcceptChanges()
                    Else
                        dsPDC.ReadXmlSchema(pstrFile & ".xsd")
                        dsPDC.ReadXml(pstrFile & ".xml")
                        dsPDC.AcceptChanges()
                    End If

                    dsPDC = Generate_TableDelete(customclass, dsPDC)
                    dsPDC.WriteXml(pstrFile & ".xml")
                    dsPDC.AcceptChanges()
                    customclass.ListPDC = dsPDC.Tables("PDC")
                Else
                    If Not File.Exists(pstrFile & ".xsd") Then
                        dsPDC = GetStructPDC()
                        dsPDC.WriteXmlSchema(pstrFile & ".xsd")
                        dsPDC.ReadXmlSchema(pstrFile & ".xsd")
                    End If
                    If Not File.Exists(pstrFile & ".xml") Then
                        dsPDC.WriteXml(pstrFile & ".xml")
                        dsPDC.AcceptChanges()
                    Else
                        dsPDC.ReadXmlSchema(pstrFile & ".xsd")
                        dsPDC.ReadXml(pstrFile & ".xml")
                        dsPDC.AcceptChanges()
                    End If
                    dsPDC = Generate_Table(customclass, dsPDC)
                    dsPDC.WriteXml(pstrFile & ".xml")
                    dsPDC.AcceptChanges()
                    customclass.ListPDC = dsPDC.Tables("PDC")
                End If
            End If
        End With
        Return customclass
    End Function
    Public Function SavePDCEdit(ByVal customclass As Parameter.PDCReceive, ByVal strFile As String) As String Implements [Interface].IPDCMTrans.SavePDCEdit
        Dim DASavePDCEdit As New SQLEngine.LoanMnt.PDCMTrans
        With customclass
            Dim pstrFile As String = "C:\temp\" + strFile & "_T_PDC"
            Dim dsNewPDC As New DataSet
            dsNewPDC.ReadXml(pstrFile & ".xml")
            Return DASavePDCEdit.SavePDCEdit(customclass, dsNewPDC.Tables("PDC"))
        End With
    End Function
    Public Function GetLastPDCDetail(ByVal customclass As Parameter.PDCReceive, ByVal strFile As String) As Parameter.PDCReceive Implements [Interface].IPDCMTrans.GetLastPDCDetail
        Dim i As Integer
        Dim DAGetLastPDCDetail As New SQLEngine.LoanMnt.PDCMTrans
        Dim dsPDC As New DataSet
        Dim lObjDataRow As DataRow

        Dim pstrFile As String = "C:\temp\" + strFile & "_T_PDC"
        With customclass
            Dim dsNewPDC As New DataSet
            If File.Exists(pstrFile & ".xsd") Then
                File.Delete(pstrFile & ".xsd")
            End If
            If File.Exists(pstrFile & ".xml") Then
                File.Delete(pstrFile & ".xml")
            End If
            customclass = DAGetLastPDCDetail.GetLastPDCDetail(customclass, dsNewPDC.Tables("PDC"))
        End With

        If Not File.Exists(pstrFile & ".xsd") Then
            dsPDC = GetStructPDC()
            dsPDC.WriteXmlSchema(pstrFile & ".xsd")
            dsPDC.ReadXmlSchema(pstrFile & ".xsd")
        End If
        If Not File.Exists(pstrFile & ".xml") Then
            dsPDC.WriteXml(pstrFile & ".xml")
            dsPDC.AcceptChanges()
        Else
            dsPDC.ReadXmlSchema(pstrFile & ".xsd")
            dsPDC.ReadXml(pstrFile & ".xml")
            dsPDC.AcceptChanges()
        End If

        If customclass.ListPDC.Rows.Count > 0 Then
            With customclass
                .PDCDueDate = CDate(.ListPDC.Rows(0).Item("PDCDueDate"))
                .BankPDCName = CType(.ListPDC.Rows(0).Item("BankName"), String)
                .IsInkaso = CType(.ListPDC.Rows(0).Item("IsInkaso"), Boolean)
                .PDCType = CType(.ListPDC.Rows(0).Item("PDCType"), String)
                .IsCumm = CType(.ListPDC.Rows(0).Item("IsCummulative"), Boolean)
                .ReceivedFrom = CType(.ListPDC.Rows(0).Item("ReceivedFrom"), String)
                .BankID = CType(.ListPDC.Rows(0).Item("BankId"), String)
                .AmountReceive = CType(.ListPDC.Rows(0).Item("Amount"), Double)
            End With

            For i = 0 To customclass.ListPDC.Rows.Count - 1
                With customclass
                    lObjDataRow = dsPDC.Tables("PDC").NewRow()
                    lObjDataRow("PaymentAllocationID") = CType(.ListPDC.Rows(i).Item("PaymentAllocationID"), String)
                    lObjDataRow("PaymentAllocationName") = CType(.ListPDC.Rows(i).Item("PaymentAllocationName"), String)
                    lObjDataRow("Description") = CType(.ListPDC.Rows(i).Item("Description"), String)
                    lObjDataRow("Amount") = CType(.ListPDC.Rows(i).Item("Amount"), Double)
                    dsPDC.Tables("PDC").Rows.Add(lObjDataRow)
                End With
                dsPDC.WriteXml(pstrFile & ".xml")
                dsPDC.AcceptChanges()
            Next
        End If
        customclass.ListPDC = dsPDC.Tables("PDC")
        Return customclass
    End Function
    Public Function GetPDCEditListMultiAgrementController(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCMTrans.GetPDCEditListMultiAgrementController
        Dim DAPDCEditListMultiAgrement As New SQLEngine.LoanMnt.PDCMTrans
        Return DAPDCEditListMultiAgrement.GetPDCEditListMultiAgrementController(customclass)
    End Function



    Public Function SavePDCReceiveBp(ByVal strConnection As String, ByVal customclass As Parameter.PDCReceive) As String Implements [Interface].IPDCMTrans.SavePDCReceiveBp
        Dim DASavePDCReceive As New SQLEngine.LoanMnt.PDCReceive
        Return DASavePDCReceive.SavePDCReceiveBp(strConnection, customclass)
    End Function

    Public Function PrintReceiptNo(ByVal strConnection As String, ByVal PDCReceiptNo As String, ByVal BranchID As String) As DataSet Implements [Interface].IPDCMTrans.PrintReceiptNo
        Dim DASavePDCEdit As New SQLEngine.LoanMnt.PDCMTrans
        Return DASavePDCEdit.PrintReceiptNo(strConnection, PDCReceiptNo, BranchID)
    End Function
End Class