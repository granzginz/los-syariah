

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class PDCRInquiry : Inherits ComponentBase
    Implements IPDCRInquiry
#Region "PDC RInquiry"


    Public Function ListPDCRInquiry(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCRInquiry.ListPDCRInquiry
        Dim DAListPDCRInquiry As New SQLEngine.LoanMnt.PDCRInquiry
        Return DAListPDCRInquiry.ListPDCRInquiry(customclass)
    End Function

    Public Function ListPDCRInquiryV(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCRInquiry.ListPDCRInquiryV
        Dim DAListPDCRInquiryV As New SQLEngine.LoanMnt.PDCRInquiry
        Return DAListPDCRInquiryV.ListPDCRInquiryV(customclass)
    End Function

    Public Function ListPDCRInquiryDet(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCRInquiry.ListPDCRInquiryDet
        Dim DAListPDCRInquiryDet As New SQLEngine.LoanMnt.PDCRInquiry
        Return DAListPDCRInquiryDet.ListPDCRInquiryDet(customclass)
    End Function

    Public Function ListPDCInqReport(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCRInquiry.ListPDCInqReport
        Dim DAListPDCInqReport As New SQLEngine.LoanMnt.PDCRInquiry
        Return DAListPDCInqReport.ReportPDCInq(customclass)
    End Function

#End Region
#Region "PDCDetail RInquiry"

    Public Function ListPDCInquiryDetail(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCRInquiry.ListPDCInquiryDetail
        Dim DAListPDCInquiryDetail As New SQLEngine.LoanMnt.PDCRInquiry
        Return DAListPDCInquiryDetail.ListPDCInquiryDetail(customclass)
    End Function


    Public Function ListPDCInquiryListDetail(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCRInquiry.ListPDCInquiryListDetail
        Dim DAListPDCInquiryListDetail As New SQLEngine.LoanMnt.PDCRInquiry
        Return DAListPDCInquiryListDetail.ListPDCInquiryListDetail(customclass)
    End Function
    Public Function PDCInquiryDetailwithGiroSeqNo(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCRInquiry.PDCInquiryDetailwithGiroSeqNo
        Dim DAListPDCInquiryListDetail As New SQLEngine.LoanMnt.PDCRInquiry
        Return DAListPDCInquiryListDetail.PDCInquiryDetailwithGiroSeqNo(customclass)
    End Function
#End Region
#Region "PDCInqStatus"
    Public Function ListPDCInquiryStatus(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCRInquiry.ListPDCInquiryStatus
        Dim DAListPDCInquiryStatus As New SQLEngine.LoanMnt.PDCRInquiry
        Return DAListPDCInquiryStatus.ListPDCInquiryStatus(customclass)
    End Function

    Public Function ListPDCInqStatusReport(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCRInquiry.ListPDCInqStatusReport
        Dim DAListPDCInqStatusReport As New SQLEngine.LoanMnt.PDCRInquiry
        Return DAListPDCInqStatusReport.ReportPDCInqStatus(customclass)
    End Function
#End Region
#Region "InqPDCIncomplete"
    Public Function InqPDCIncomplete(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCRInquiry.InqPDCIncomplete
        Dim DAInqPDCIncomplete As New SQLEngine.LoanMnt.PDCRInquiry
        Return DAInqPDCIncomplete.InqPDCIncomplete(customclass)
    End Function
#End Region


End Class
