

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class PDCReceive : Inherits ComponentBase

    Implements IPDCReceive

    Private isError As Boolean

    Public Function ListPDCReceive(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCReceive.ListPDCReceive
        Dim DAListPDCReceive As New SQLEngine.LoanMnt.PDCReceive
        Return DAListPDCReceive.ListPDCReceive(customclass)
    End Function

    Public Function GetStructPDC() As DataSet
        Dim lObjDataTable As New DataSet
        Dim dtTable As New DataTable("PDC")

        lObjDataTable.Tables.Add(dtTable)

        lObjDataTable.Tables("PDC").Columns.Add("Holiday", GetType(String))
        lObjDataTable.Tables("PDC").Columns.Add("BankPDC", GetType(String))
        lObjDataTable.Tables("PDC").Columns.Add("BankID", GetType(String))

        lObjDataTable.Tables("PDC").Columns.Add("PDCNo", GetType(String))
        lObjDataTable.Tables("PDC").Columns.Add("PDCAmount", GetType(String))
        lObjDataTable.Tables("PDC").Columns.Add("DueDate", GetType(DateTime))
        lObjDataTable.Tables("PDC").Columns.Add("IsInkaso", GetType(String))
        lObjDataTable.Tables("PDC").Columns.Add("IsCummulative", GetType(String))
        lObjDataTable.Tables("PDC").Columns.Add("Type", GetType(String))
        Return lObjDataTable
    End Function


    Public Function GetHoliday(ByVal customclass As Parameter.PDCReceive) As Parameter.PDCReceive Implements [Interface].IPDCReceive.GetHoliday
        Dim DAListHoliday As New SQLEngine.LoanMnt.PDCReceive
        Return DAListHoliday.GetHoliday(customclass)
    End Function

    Public Function GetTablePDC(ByVal customclass As Parameter.PDCReceive, ByVal strFile As String) As Parameter.PDCReceive Implements [Interface].IPDCReceive.GetTablePDC
        Dim CheckPDCNo As DataRow()
        Dim CheckBankPDCName As DataRow()
        Dim lObjHoliday As New Hashtable
        Dim lObjPDC As New ArrayList

        Dim pstrFile As String
        Dim gStrPath As String

        Dim dsPDC As New DataSet

        Dim DtPDC As DataTable
        Dim DtPDC2 As DataTable

        isError = False

        Dim CustomPDC As New Parameter.PDCReceive
        With customclass
            pstrFile = "C:\temp\" + strFile & .BranchId & "_M_PDC"
            '=========bila hapus xml =================
            If .HpsXml = "1" Then
                If File.Exists(pstrFile & ".xsd") Then
                    File.Delete(pstrFile & ".xsd")
                End If
                If File.Exists(pstrFile & ".xml") Then
                    File.Delete(pstrFile & ".xml")
                End If
            Else
                If .FlagDelete = "1" Then
                    If Not File.Exists(pstrFile & ".xsd") Then
                        dsPDC = GetStructPDC()
                        dsPDC.WriteXmlSchema(pstrFile & ".xsd")
                        dsPDC.ReadXmlSchema(pstrFile & ".xsd")
                    End If
                    If Not File.Exists(pstrFile & ".xml") Then
                        dsPDC = GetStructPDC()
                        dsPDC.WriteXml(pstrFile & ".xml")
                        dsPDC.AcceptChanges()
                    Else
                        dsPDC = GetStructPDC()
                        dsPDC.ReadXml(pstrFile & ".xml")
                        dsPDC.AcceptChanges()
                    End If

                    dsPDC = Generate_TableDelete(customclass, dsPDC)
                    dsPDC.WriteXml(pstrFile & ".xml")
                    dsPDC.AcceptChanges()
                    customclass.ListPDC = dsPDC.Tables("PDC")
                Else
                    CustomPDC = Is_ValidPDC(customclass)
                    If CustomPDC.IsValidPDC Then
                        If Not File.Exists(pstrFile & ".xsd") Then
                            dsPDC = GetStructPDC()
                            dsPDC.WriteXmlSchema(pstrFile & ".xsd")
                            dsPDC.ReadXmlSchema(pstrFile & ".xsd")
                        End If
                        If Not File.Exists(pstrFile & ".xml") Then
                            dsPDC.WriteXml(pstrFile & ".xml")
                            dsPDC.AcceptChanges()
                        Else
                            dsPDC = GetStructPDC()
                            dsPDC.ReadXmlSchema(pstrFile & ".xsd")
                            dsPDC.ReadXml(pstrFile & ".xml")
                            dsPDC.AcceptChanges()
                            CheckPDCNo = dsPDC.Tables("PDC").Select("PDCNo = '" & .GiroNo & "'")
                            CheckBankPDCName = dsPDC.Tables("PDC").Select("BankPDC = '" & .BankPDCName & "'")
                            If CheckPDCNo.Length > 0 And CheckBankPDCName.Length > 0 Then
                                .IsValidPDC = False
                                Return customclass
                            End If
                        End If
                        dsPDC = Generate_Table(customclass, dsPDC)
                        If Not isError Then
                            dsPDC.WriteXml(pstrFile & ".xml")
                            dsPDC.AcceptChanges()
                            customclass.ListPDC = dsPDC.Tables("PDC")
                        Else
                            Dim dsNewPDC As New DataSet
                            dsPDC.ReadXmlSchema(pstrFile & ".xsd")
                            dsNewPDC.ReadXml(pstrFile & ".xml")
                            customclass.ListPDC = dsNewPDC.Tables("PDC")
                            customclass.IsValidPDC = False
                        End If
                    Else
                        customclass.IsValidPDC = False
                    End If
                End If
            End If
        End With
        Return customclass
    End Function

#Region "IsValidPDC"
    Private Function Is_ValidPDC(ByVal CustomClass As Parameter.PDCReceive) As Parameter.PDCReceive
        Dim lObjPDC As New ArrayList
        Dim lStrGiroNo As String = CustomClass.GiroNo
        Dim lStrGiroCheck As String = ""
        Dim lBytEnd As Integer = lStrGiroNo.Length - 1
        Dim lBytCounter As Integer
        Dim lStrPrefix As String = ""
        Dim lStrPDC As String = ""
        Dim lStrZero As String = ""
        Dim lStrCheck As String
        Dim lBlnValid As Boolean = True
        Dim lIntGiroNo As Int64

        For lBytCounter = 0 To lBytEnd
            If IsNumeric(lStrGiroNo.Chars(lBytCounter)) Then
                lStrPDC &= lStrGiroNo.Chars(lBytCounter)
            Else
                If lStrPDC <> "" Then CustomClass.IsValidPDC = False
                lStrPrefix &= lStrGiroNo.Chars(lBytCounter)
            End If
        Next
        If lStrPDC = "" Then CustomClass.IsValidPDC = False

        If lBlnValid Then
            'Check PDC
            lBytEnd = CustomClass.NumPDC
            lStrZero = lStrZero.PadLeft(lStrPDC.Length, CType("0", Char))
            For lBytCounter = 1 To lBytEnd
                lIntGiroNo = CLng(lStrPDC) + lBytCounter - 1
                If lIntGiroNo.ToString.Length >= lStrZero.Length Then
                    lStrCheck = lStrPrefix & lIntGiroNo
                Else
                    lStrCheck = lStrPrefix & lStrZero.Substring(0, lStrZero.Length - lIntGiroNo.ToString.Length) & lIntGiroNo
                End If
                If lObjPDC.Contains(lStrCheck.ToUpper) Then
                    CustomClass.IsValidPDC = False
                    Exit For
                Else
                    lStrGiroCheck &= CStr(IIf(lStrGiroCheck = "", "", ", ")) & "'" & lStrCheck.Trim & "'"
                End If
            Next
        End If

        Dim DAPdcCheckDuplikasi As New SQLEngine.LoanMnt.PDCReceive
        CustomClass.ListGiroNo = lStrGiroCheck
        If Not DAPdcCheckDuplikasi.CheckDuplikasiPDC(CustomClass) Then
            CustomClass.IsValidPDC = True
            CustomClass.ListGiroNo = lStrPDC
            CustomClass.PrefixPDC = lStrPrefix
        Else
            CustomClass.IsValidPDC = False
        End If
        Return CustomClass
    End Function
#End Region

    Private Function Generate_Table(ByVal CustomClass As Parameter.PDCReceive, ByVal dsPDC As DataSet) As DataSet
        Dim lObjDataRow As DataRow
        Dim CheckPDCNo As DataRow()
        Dim CheckBankPDCName As DataRow()
        Dim strZero As String
        Dim lCounter As Integer
        Dim SerialNoGiroNo As Int64
        Dim StrPDCNo As String
        Dim FrequentDueDate As Date
        strZero = ""
        With CustomClass
            strZero = strZero.PadLeft(.ListGiroNo.Trim.Length, CType("0", Char))
            For lCounter = 1 To .NumPDC
                SerialNoGiroNo = CLng(.ListGiroNo) + lCounter - 1
                If SerialNoGiroNo.ToString.Length >= strZero.Length Then
                    StrPDCNo = .PrefixPDC & SerialNoGiroNo
                Else
                    StrPDCNo = .PrefixPDC & strZero.Substring(0, strZero.Length - SerialNoGiroNo.ToString.Length) & SerialNoGiroNo
                End If
                CheckPDCNo = dsPDC.Tables("PDC").Select("PDCNo = '" & StrPDCNo & "'")
                CheckBankPDCName = dsPDC.Tables("PDC").Select("BankID = '" & .BankPDC & "'")
                If CheckPDCNo.Length > 0 And CheckBankPDCName.Length > 0 Then
                    isError = True
                    Exit For
                End If
                FrequentDueDate = DateAdd("m", .PaymentFrequency * (lCounter - 1), .NextInstallmentDueDate)
                lObjDataRow = dsPDC.Tables("PDC").NewRow()
                lObjDataRow("Holiday") = IIf(GetHoliday(CustomClass).HolidayDate.ContainsKey(FrequentDueDate), "Yes", "No")
                lObjDataRow("BankPDC") = .BankPDCName
                lObjDataRow("BankID") = .BankPDC
                lObjDataRow("PDCNo") = StrPDCNo
                lObjDataRow("PDCAmount") = .PDCAmount
                lObjDataRow("DueDate") = FrequentDueDate

                lObjDataRow("IsInkaso") = .IsInkaso
                lObjDataRow("IsCummulative") = .IsCumm
                lObjDataRow("Type") = .PDCType
                'dsPDC.Tables("PDC").Rows. = FrequentDueDate
                dsPDC.Tables("PDC").Rows.Add(lObjDataRow)
            Next
        End With
        Return dsPDC
    End Function

    Private Function Generate_TableDelete(ByVal CustomClass As Parameter.PDCReceive, ByVal dsPDC As DataSet) As DataSet
        Dim NewDataSetDel As New DataSet
        With CustomClass
            dsPDC.Tables("PDC").Rows.RemoveAt(CInt(.IndexDelete))
            ' NewDataSetDel = Generate_Table(CustomClass, dsPDC)
            'Return NewDataSetDel
            Return dsPDC
        End With
    End Function

    Public Function SavePDCReceive(ByVal customclass As Parameter.PDCReceive, ByVal strFile As String) As String Implements [Interface].IPDCReceive.SavePDCReceive
        Dim DASavePDCReceive As New SQLEngine.LoanMnt.PDCReceive
        With customclass
            Dim pstrFile As String = "C:\temp\" + strFile & "_M_PDC"
            Dim dsNewPDC As New DataSet
            dsNewPDC.ReadXml(pstrFile & ".xml")
            Return DASavePDCReceive.SavePDCReceive(customclass, dsNewPDC.Tables("PDC"))
            .HpsXml = "1"
        End With
    End Function
    Public Function SavePDCReceiveBp(ByVal strConnection As String, ByVal customclass As Parameter.PDCReceive) As String Implements [Interface].IPDCReceive.SavePDCReceiveBp
        Dim DASavePDCReceive As New SQLEngine.LoanMnt.PDCReceive
        Return DASavePDCReceive.SavePDCReceiveBp(strConnection, customclass)
    End Function
    Public Function PrintReceiptNo(ByVal strConnection As String, ByVal PDCReceiptNo As String, ByVal BranchID As String) As DataSet Implements [Interface].IPDCReceive.PrintReceiptNo
        Dim DASavePDCEdit As New SQLEngine.LoanMnt.PDCReceive
        Return DASavePDCEdit.PrintReceiptNo(strConnection, PDCReceiptNo, BranchID)
    End Function
End Class
