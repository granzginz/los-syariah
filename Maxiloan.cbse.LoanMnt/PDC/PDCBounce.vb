
#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class PDCBounce : Inherits ComponentBase
    Implements IPDCBounce

    Public Function PDCBounceList(ByVal customclass As Parameter.PDCBounce) As Parameter.PDCBounce Implements [Interface].IPDCBounce.PDCBounceList
        Dim DAPDCBounceList As New SQLEngine.LoanMnt.PDCBounce
        Return DAPDCBounceList.PDCBounceList(customclass)
    End Function

    Public Sub ProcesPDCBounce(ByVal customclass As Parameter.PDCBounce) Implements [Interface].IPDCBounce.ProcesPDCBounce
        Dim DAPDCBounceList As New SQLEngine.LoanMnt.PDCBounce
        DAPDCBounceList.ProcesPDCBounce(customclass)
    End Sub
    Public Function PDCBounceView(ByVal customclass As Parameter.PDCBounce) As Parameter.PDCBounce Implements [Interface].IPDCBounce.PDCBounceView
        Dim DAPDCBounceList As New SQLEngine.LoanMnt.PDCBounce
        Return DAPDCBounceList.PDCBounceView(customclass)
    End Function
    Public Function PrintPDCBounce(ByVal oCustomClass As Parameter.PDCBounce) As DataSet Implements [Interface].IPDCBounce.PrintPDCBounce
        Dim DAPDCBounceList As New SQLEngine.LoanMnt.PDCBounce
        Return DAPDCBounceList.PrintPDCBounce(oCustomClass)
    End Function
End Class