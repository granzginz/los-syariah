
#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class PaymentRequest : Inherits ComponentBase
    Implements IPaymentRequest
#Region "Get_RequestNo"
    Public Function Get_RequestNo(ByVal customClass As Parameter.PaymentRequest) As String Implements [Interface].IPaymentRequest.Get_RequestNo
        Try
            Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
            Return PRDA.Get_RequestNo(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
#End Region

    Public Function SavePR(ByVal customclass As Parameter.PaymentRequest) As String Implements [Interface].IPaymentRequest.SavePR
        Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
        Return PRDA.SavePR(customclass)
    End Function
    Public Function CekPR(ByVal customclass As Parameter.PaymentRequest) As String Implements [Interface].IPaymentRequest.CekPR
        Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
        Return PRDA.CekPayment(customclass)
    End Function

#Region "GetStructPaymentRequest"
    Public Function GetStructPaymentRequest() As DataSet
        Dim lObjDataTable As New DataSet
        Dim dtTable As New DataTable("PaymentRequest")

        lObjDataTable.Tables.Add(dtTable)

        lObjDataTable.Tables("PaymentRequest").Columns.Add("PaymentAllocationID", System.Type.GetType("System.String"))
        lObjDataTable.Tables("PaymentRequest").Columns.Add("PaymentAllocationName", System.Type.GetType("System.String"))
        lObjDataTable.Tables("PaymentRequest").Columns.Add("Desc", System.Type.GetType("System.String"))
        lObjDataTable.Tables("PaymentRequest").Columns.Add("Amount", System.Type.GetType("System.String"))
        Return lObjDataTable
    End Function
#End Region

#Region "Generate_Table"
    Private Function Generate_Table(ByVal CustomClass As Parameter.PaymentRequest, ByVal dsPaymentRequest As DataSet) As DataSet
        Dim lObjDataRow As DataRow
        Dim strZero As String
        Dim lCounter As Integer
        Dim SerialNoGiroNo As Int64
        Dim FrequentDueDate As Date
        'strZero = ""
        With CustomClass
            lObjDataRow = dsPaymentRequest.Tables("PaymentRequest").NewRow()
            lObjDataRow("PaymentAllocationID") = .PaymentAllocationID
            lObjDataRow("PaymentAllocationName") = .PaymentAllDescription
            lObjDataRow("Desc") = .Description
            lObjDataRow("Amount") = .Amount
            dsPaymentRequest.Tables("PaymentRequest").Rows.Add(lObjDataRow)
        End With
        Return dsPaymentRequest
    End Function
#End Region

#Region "Generate_TableDelete"
    Private Function Generate_TableDelete(ByVal CustomClass As Parameter.PaymentRequest, ByVal dsPaymentRequest As DataSet) As DataSet
        With CustomClass
            dsPaymentRequest.Tables("PaymentRequest").Rows.RemoveAt(CInt(.IndexDelete))
            Return dsPaymentRequest
        End With
    End Function
#End Region

#Region "GetTableTransaction"
    Public Function GetTableTransaction(ByVal customclass As Parameter.PaymentRequest, ByVal strFile As String) As Parameter.PaymentRequest Implements [Interface].IPaymentRequest.GetTableTransaction
        Dim CheckPaymentRequestNo As DataRow()
        Dim lObjHoliday As New Hashtable
        Dim pstrFile As String
        Dim gStrPath As String
        Dim dsPaymentRequest As New DataSet


        'isError = False

        With customclass
            pstrFile = "C:\temp\" + strFile & "_PaymentRequest"
            If .hpsxml = "1" Then
                If File.Exists(pstrFile & ".xsd") Then
                    File.Delete(pstrFile & ".xsd")
                End If
                If File.Exists(pstrFile & ".xml") Then
                    File.Delete(pstrFile & ".xml")
                End If
            Else
                If .flagdelete = "1" Then
                    If Not File.Exists(pstrFile & ".xsd") Then
                        dsPaymentRequest = GetStructPaymentRequest()
                        dsPaymentRequest.WriteXmlSchema(pstrFile & ".xsd")
                        dsPaymentRequest.ReadXmlSchema(pstrFile & ".xsd")
                    End If
                    If Not File.Exists(pstrFile & ".xml") Then
                        dsPaymentRequest.WriteXml(pstrFile & ".xml")
                        dsPaymentRequest.AcceptChanges()
                    Else
                        dsPaymentRequest.ReadXmlSchema(pstrFile & ".xsd")
                        dsPaymentRequest.ReadXml(pstrFile & ".xml")
                        dsPaymentRequest.AcceptChanges()
                    End If

                    dsPaymentRequest = Generate_TableDelete(customclass, dsPaymentRequest)
                    dsPaymentRequest.WriteXml(pstrFile & ".xml")
                    dsPaymentRequest.AcceptChanges()
                    customclass.ListData = dsPaymentRequest.Tables("PaymentRequest")
                Else
                    If Not File.Exists(pstrFile & ".xsd") Then
                        dsPaymentRequest = GetStructPaymentRequest()
                        dsPaymentRequest.WriteXmlSchema(pstrFile & ".xsd")
                        dsPaymentRequest.ReadXmlSchema(pstrFile & ".xsd")
                    End If
                    If Not File.Exists(pstrFile & ".xml") Then
                        dsPaymentRequest.WriteXml(pstrFile & ".xml")
                        dsPaymentRequest.AcceptChanges()
                    Else
                        dsPaymentRequest.ReadXmlSchema(pstrFile & ".xsd")
                        dsPaymentRequest.ReadXml(pstrFile & ".xml")
                        dsPaymentRequest.AcceptChanges()
                        CheckPaymentRequestNo = dsPaymentRequest.Tables("PaymentRequest").Select("PaymentAllocationID = '" & .PaymentAllocationID & "'")
                        If CheckPaymentRequestNo.Length > 0 Then
                            .IsValid = False
                            Return customclass
                        End If
                    End If
                    dsPaymentRequest = Generate_Table(customclass, dsPaymentRequest)
                    dsPaymentRequest.WriteXml(pstrFile & ".xml")
                    dsPaymentRequest.AcceptChanges()
                    customclass.ListData = dsPaymentRequest.Tables("PaymentRequest")
                End If
            End If
        End With
        Return customclass
    End Function
#End Region
    Public Function PRCOAUpdate(ByVal customClass As Parameter.PaymentRequest) As String Implements [Interface].IPaymentRequest.PRCOAUpdate
        Dim DA As New SQLEngine.LoanMnt.PaymentRequest
        Return DA.PRCOAUpdate(customClass)
    End Function
    Public Function GetPaymentRequestPaging(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest Implements [Interface].IPaymentRequest.GetPaymentRequestPaging
        Try
            Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
            Return PRDA.GetPaymentRequestPaging(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPaymentReceivePaging(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest Implements [Interface].IPaymentRequest.GetPaymentReceivePaging
        Try
            Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
            Return PRDA.GetPaymentReceivePaging(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPaymentRequestPagingFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest Implements [Interface].IPaymentRequest.GetPaymentRequestPagingFA
        Try
            Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
            Return PRDA.GetPaymentRequestPagingFA(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPenjualanPagingFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest Implements [Interface].IPaymentRequest.GetPenjualanPagingFA
        Try
            Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
            Return PRDA.GetPenjualanPagingFA(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPenghapusanPagingFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest Implements [Interface].IPaymentRequest.GetPenghapusanPagingFA
        Try
            Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
            Return PRDA.GetPenghapusanPagingFA(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetPaymentRequestHeaderAndDetail(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest Implements [Interface].IPaymentRequest.GetPaymentRequestHeaderAndDetail
        Try
            Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
            Return PRDA.GetPaymentRequestHeaderAndDetail(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPaymentRequestHeaderAndDetailFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest Implements [Interface].IPaymentRequest.GetPaymentRequestHeaderAndDetailFA
        Try
            Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
            Return PRDA.GetPaymentRequestHeaderAndDetailFA(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPaymentReceiveHeaderAndDetailFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest Implements [Interface].IPaymentRequest.GetPaymentReceiveHeaderAndDetailFA
        Try
            Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
            Return PRDA.GetPaymentReceiveHeaderAndDetailFA(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPenjualanHeaderAndDetailFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest Implements [Interface].IPaymentRequest.GetPenjualanHeaderAndDetailFA
        Try
            Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
            Return PRDA.GetPenjualanHeaderAndDetailFA(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPenghapusanHeaderAndDetailFA(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest Implements [Interface].IPaymentRequest.GetPenghapusanHeaderAndDetailFA
        Try
            Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
            Return PRDA.GetPenghapusanHeaderAndDetailFA(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetPaymentRequestDataset(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest Implements [Interface].IPaymentRequest.GetPaymentRequestDataset
        Try
            Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
            Return PRDA.GetPaymentRequestDataset(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub saveApprovalPaymentRequest(ByVal oClass As Parameter.PaymentRequest, ByVal strApproval As String) Implements [Interface].IPaymentRequest.saveApprovalPaymentRequest
        Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
        PRDA.saveApprovalPaymentRequest(oClass, strApproval)
    End Sub

    Public Sub saveApprovalPaymentRequestFA(ByVal oClass As Parameter.PaymentRequest, ByVal strApproval As String) Implements [Interface].IPaymentRequest.saveApprovalPaymentRequestFA
        Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
        PRDA.saveApprovalPaymentRequestFA(oClass, strApproval)
    End Sub
    Public Sub saveApprovalPaymentReceiveFA(ByVal oClass As Parameter.PaymentRequest, ByVal strApproval As String) Implements [Interface].IPaymentRequest.saveApprovalPaymentReceiveFA
        Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
        PRDA.saveApprovalPaymentReceiveFA(oClass, strApproval)
    End Sub
    Public Sub saveApprovalPenjualanFA(ByVal oClass As Parameter.PaymentRequest, ByVal strApproval As String) Implements [Interface].IPaymentRequest.saveApprovalPenjualanFA
        Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
        PRDA.saveApprovalPenjualanFA(oClass, strApproval)
    End Sub
    Public Sub saveApprovalPenghapusanFA(ByVal oClass As Parameter.PaymentRequest, ByVal strApproval As String) Implements [Interface].IPaymentRequest.saveApprovalPenghapusanFA
        Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
        PRDA.saveApprovalPenghapusanFA(oClass, strApproval)
    End Sub

    Public Function GetPaymentRequestFormKuning(ByVal customClass As IList(Of String), cnn As String) As DataSet Implements [Interface].IPaymentRequest.GetPaymentRequestFormKuning
        Dim DASavePCReimburse As New SQLEngine.LoanMnt.PaymentRequest
        Return DASavePCReimburse.GetPaymentRequestFormKuning(customClass, cnn)
    End Function
    Public Sub PayReqStatusReject(oClass As Parameter.PaymentRequest, strApproval As String) Implements [Interface].IPaymentRequest.PayReqStatusReject
        Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
        PRDA.PayReqStatusReject(oClass, strApproval)
    End Sub

    Public Function GetPaymentVoucherByRequestNoPrint(ocustom As Parameter.PaymentRequest) As Parameter.PaymentRequest Implements IPaymentRequest.GetPaymentVoucherByRequestNoPrint
        Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
        Return PRDA.GetPaymentVoucherByRequestNoPrint(ocustom)
    End Function

    Public Function GetBankAccountOther(oCustomClass As Parameter.PaymentRequest) As Parameter.PaymentRequest Implements IPaymentRequest.GetBankAccountOther
        Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
        Return PRDA.GetBankAccountOther(oCustomClass)
    End Function

    Public Function UpdatePR(customclass As Parameter.PaymentRequest) As String Implements IPaymentRequest.UpdatePR
        Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
        Return PRDA.UpdatePR(customclass)
    End Function

    Public Function GetFAPaymentRequestDataSet(ByVal customClass As Parameter.PaymentRequest) As Parameter.PaymentRequest Implements [Interface].IPaymentRequest.GetFAPaymentRequestDataSet
        Dim PRDA As New SQLEngine.LoanMnt.PaymentRequest
        Return PRDA.GetFAPaymentRequestDataSet(customClass)
    End Function

End Class
