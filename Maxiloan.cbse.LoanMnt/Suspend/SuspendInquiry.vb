
#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region
Public Class SuspendInquiry : Inherits ComponentBase

    Implements ISuspendInquiry

    Public Function SuspendInquiryList(ByVal customclass As Parameter.SuspendReceive) As Parameter.SuspendReceive Implements [Interface].ISuspendInquiry.SuspendInquiryList
        Dim DASuspendInquiryList As New SQLEngine.LoanMnt.SuspendInquiry
        Return DASuspendInquiryList.SuspendInquiryList(customclass)
    End Function

    Public Function SuspendInqReport(ByVal customclass As Parameter.SuspendReceive) As Parameter.SuspendReceive Implements [Interface].ISuspendInquiry.SuspendInqReport
        Dim DASuspendInqReport As New SQLEngine.LoanMnt.SuspendInquiry
        Return DASuspendInqReport.ReportSuspendInq(customclass)
    End Function

    Public Function GetBankAccountAll(ByVal customclass As Parameter.SuspendReceive) As DataTable Implements [Interface].ISuspendInquiry.GetBankAccountAll
        Dim DAGetBankAccountAll As New SQLEngine.LoanMnt.SuspendInquiry
        Return DAGetBankAccountAll.GetBankAccountAll(customclass)
    End Function
End Class
