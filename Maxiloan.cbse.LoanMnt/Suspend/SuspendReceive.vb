
#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region
Public Class SuspendReceive : Inherits ComponentBase

    Implements ISuspendReceive

    Public Sub SaveSuspendReceive(ByVal customclass As Parameter.SuspendReceive) Implements [Interface].ISuspendReceive.SaveSuspendReceive
        Dim DASaveSuspendReceive As New SQLEngine.LoanMnt.SuspendReceive
        DASaveSuspendReceive.SaveSuspendReceive(customclass)
    End Sub

    Public Sub SaveSuspendTransactionImplementasi(customclass As Parameter.SuspendReceive) Implements [Interface].ISuspendReceive.SaveSuspendTransactionImplementasi
        Dim DASaveSuspendReceive As New SQLEngine.LoanMnt.SuspendReceive
        DASaveSuspendReceive.SaveSuspendTransactionImplementasi(customclass)
    End Sub

    Public Sub SaveSuspendOtorisasi(ByVal customclass As Parameter.SuspendReceive) Implements [Interface].ISuspendReceive.SaveSuspendOtorisasi
        Dim DASaveSuspendReceive As New SQLEngine.LoanMnt.SuspendReceive
        DASaveSuspendReceive.SaveSuspendOtorisasi(customclass)
    End Sub

    Public Sub SaveReversalOtorisasi(ByVal customclass As Parameter.SuspendReceive) Implements [Interface].ISuspendReceive.SaveReversalOtorisasi
        Dim DASaveSuspendReceive As New SQLEngine.LoanMnt.SuspendReceive
        DASaveSuspendReceive.SaveReversalOtorisasi(customclass)
    End Sub
End Class
