#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region
Public Class SuspendReversal : Inherits ComponentBase

    Implements ISuspendReversal
    Public Function SuspendReversalList(ByVal customclass As Parameter.SuspendReceive) As Parameter.SuspendReceive Implements [Interface].ISuspendReversal.SuspendReversalList
        Dim DASuspendReversalList As New SQLEngine.LoanMnt.SuspendReversal
        Return DASuspendReversalList.SuspendReversalList(customclass)
    End Function

    Public Sub SaveSuspendReverse(ByVal customclass As Parameter.SuspendReceive) Implements [Interface].ISuspendReversal.SaveSuspendReverse
        Dim DASaveSuspendReverse As New SQLEngine.LoanMnt.SuspendReversal
        DASaveSuspendReverse.SaveSuspendReverse(customclass)
    End Sub

    Public Function SuspendReverse(ByVal customclass As Parameter.SuspendReceive) As Parameter.SuspendReceive Implements [Interface].ISuspendReversal.SuspendReverse
        Dim DASuspendReverse As New SQLEngine.LoanMnt.SuspendReversal
        Return DASuspendReverse.SuspendReverse(customclass)
    End Function

    Public Function SlitTitipanList(ByVal customclass As Parameter.SuspendReceive) As Parameter.SuspendReceive Implements [Interface].ISuspendReversal.SplitTitipanList
        Dim DASlitTitipanList As New SQLEngine.LoanMnt.SuspendReversal
        Return DASlitTitipanList.SplitTitipanList(customclass)
    End Function
End Class
