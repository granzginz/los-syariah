﻿
#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class SuspendAllocationGeneral : Inherits ComponentBase
    Implements ISuspendAllocationGeneral

    Dim oSQLE As New SQLEngine.LoanMnt.SuspendAllocationGeneral

    Public Function GetSuspendAllocationGeneral(ByVal oCustomClass As Parameter.SuspendAllocationGeneral) As Parameter.SuspendAllocationGeneral Implements [Interface].ISuspendAllocationGeneral.GetSuspendAllocationGeneral
        Return oSQLE.GetSuspendAllocationGeneral(oCustomClass)
    End Function

    Public Function GetSuspendAllocationGeneralList(ByVal oCustomClass As Parameter.SuspendAllocationGeneral) As Parameter.SuspendAllocationGeneral Implements [Interface].ISuspendAllocationGeneral.GetSuspendAllocationGeneralList
        Return oSQLE.GetSuspendAllocationGeneralList(oCustomClass)
    End Function

    Public Sub SuspendAllocationGeneralSaveEdit(ByVal oCustomClass As Parameter.SuspendAllocationGeneral) Implements [Interface].ISuspendAllocationGeneral.SuspendAllocationGeneralSaveEdit
        oSQLE.SuspendAllocationGeneralSaveEdit(oCustomClass)
    End Sub

    Public Function GetComboCabang(ByVal oCustomClass As Parameter.SuspendAllocationGeneral) As System.Data.DataTable Implements [Interface].ISuspendAllocationGeneral.GetComboCabang
        Return oSQLE.GetComboCabang(oCustomClass)
    End Function

    Public Function GetComboDepartemen(ByVal oCustomClass As Parameter.SuspendAllocationGeneral) As System.Data.DataTable Implements [Interface].ISuspendAllocationGeneral.GetComboDepartemen
        Return oSQLE.GetComboDepartemen(oCustomClass)
    End Function

    Public Function GetIsBlock(ByVal oCustomClass As Parameter.SuspendAllocationGeneral) As Parameter.SuspendAllocationGeneral Implements [Interface].ISuspendAllocationGeneral.GetIsBlock
        Return oSQLE.GetIsBlock(oCustomClass)
    End Function

    Public Function GetSuspendIsBlockList(ByVal oCustomClass As Parameter.SuspendAllocationGeneral) As Parameter.SuspendAllocationGeneral Implements [Interface].ISuspendAllocationGeneral.GetSuspendIsBlockList
        Return oSQLE.GetSuspendIsBlockList(oCustomClass)
    End Function

    Public Sub ReleaseBlokirSuspend(ByVal oCustomClass As Parameter.SuspendAllocationGeneral) Implements [Interface].ISuspendAllocationGeneral.ReleaseBlokirSuspend
        oSQLE.ReleaseBlokirSuspend(oCustomClass)
    End Sub
End Class
