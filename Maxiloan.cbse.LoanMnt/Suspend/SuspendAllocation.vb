

#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class SuspendAllocation
    Implements ISuspendAllocation


    Public Sub SuspendAllocationSave(ByVal oCustomClass As Parameter.InstallRcv) Implements [Interface].ISuspendAllocation.SuspendAllocationSave
        Dim DASuspendAllocation As New SQLEngine.LoanMnt.SuspendAllocation
        DASuspendAllocation.SuspendAllocationSave(oCustomClass)
    End Sub

    Public Function SuspendDetail(oCustom As Parameter.SuspendReceive) As Parameter.SuspendReceive Implements [Interface].ISuspendAllocation.SuspendDetail
        Dim DASuspendAllocation As New SQLEngine.LoanMnt.SuspendAllocation
        Return DASuspendAllocation.SuspendDetail(oCustom)
    End Function

    Public Sub SuspendSplit(oCustomClass As Parameter.SuspendReceive) Implements [Interface].ISuspendAllocation.SuspendSplit
        Dim DASuspendAllocation As New SQLEngine.LoanMnt.SuspendAllocation
        DASuspendAllocation.SuspendSplit(oCustomClass)
    End Sub

    'penambahan alokasi suspend fact & mdkj
    Public Sub SuspendAllocationSaveFACT(ByVal oCustomClass As Parameter.InstallRcv) Implements [Interface].ISuspendAllocation.SuspendAllocationSaveFACT
        Dim DASuspendAllocation As New SQLEngine.LoanMnt.SuspendAllocation
        DASuspendAllocation.SuspendAllocationSaveFACT(oCustomClass)
    End Sub
    Public Sub SuspendAllocationSaveMDKJ(ByVal oCustomClass As Parameter.InstallRcv) Implements [Interface].ISuspendAllocation.SuspendAllocationSaveMDKJ
        Dim DASuspendAllocation As New SQLEngine.LoanMnt.SuspendAllocation
        DASuspendAllocation.SuspendAllocationSaveMDKJ(oCustomClass)
    End Sub

    Public Function TitipanDetail(oCustom As Parameter.SuspendReceive) As Parameter.SuspendReceive Implements [Interface].ISuspendAllocation.TitipanDetail
        Dim DATitipanDetail As New SQLEngine.LoanMnt.SuspendAllocation
        Return DATitipanDetail.TitipanDetail(oCustom)
    End Function
    Public Sub TitipanSplit(oCustomClass As Parameter.SuspendReceive) Implements [Interface].ISuspendAllocation.TitipanSplit
        Dim DATitipanSplit As New SQLEngine.LoanMnt.SuspendAllocation
        DATitipanSplit.TitipanSplit(oCustomClass)
    End Sub
End Class
