﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class DataJurnalCrossPeriode : Inherits ComponentBase
	Implements IDataJurnalCrossPeriode

    Public Function GetDataJurnal(ByVal oCustomClass As Parameter.DataJurnalCrossPeriode) As Parameter.DataJurnalCrossPeriode Implements [Interface].IDataJurnalCrossPeriode.GetDataJurnal
        Dim DA As New SQLEngine.GL.DataJurnalCrossPeriode
        Return DA.GetDataJurnal(oCustomClass)
    End Function
    Public Function GetTr_Nomor(ByVal oCustomClass As Parameter.DataJurnalCrossPeriode) As Parameter.DataJurnalCrossPeriode Implements [Interface].IDataJurnalCrossPeriode.GetTr_Nomor
        Dim DA As New SQLEngine.GL.DataJurnalCrossPeriode
        Return DA.GetTr_Nomor(oCustomClass)
    End Function
End Class
