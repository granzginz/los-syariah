﻿Imports Maxiloan.Interface

Public Class GlCurrency : Inherits ComponentBase
    Implements IGlCurrency

    Private ReadOnly _da As New SQLEngine.GL.GlCurrencyDataAccess
  

    Public Function GetGlCurrencies(ByVal cnn As String) As IList(Of Maxiloan.Parameter.GlCurrency) Implements IGlCurrency.GetGlCurrencies
        Return _da.GetGlCurrencies(cnn)
    End Function

    Public Function GetHomeCurrency(ByVal cnn As String) As Maxiloan.Parameter.GlCurrency Implements IGlCurrency.GetHomeCurrency
        Return _da.GetHomeCurrency(cnn)
    End Function

    Public Function GetDefaultCurrency(ByVal cnn As String) As Maxiloan.Parameter.GlCurrency Implements IGlCurrency.GetDefaultCurrency
        Return _da.GetDefaultCurrency(cnn)
    End Function
End Class
