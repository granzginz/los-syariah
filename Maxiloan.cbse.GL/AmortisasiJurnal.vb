﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class AmortisasiJurnal : Inherits ComponentBase
    Implements IAmortisasiJurnal

    Public Function GetData(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal Implements [Interface].IAmortisasiJurnal.GetData
        Dim DA As New SQLEngine.GL.AmortisasiJurnal
        Return DA.GetData(oCustomClass)
    End Function
    Public Function GetDataEdit(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal Implements [Interface].IAmortisasiJurnal.GetDataEdit
        Dim DA As New SQLEngine.GL.AmortisasiJurnal
        Return DA.GetDataEdit(oCustomClass)
    End Function
    Public Function GetDataDelete(ByVal oCustomClass As Parameter.AmortisasiJurnal) As String Implements [Interface].IAmortisasiJurnal.GetDataDelete
        Dim DA As New SQLEngine.GL.AmortisasiJurnal
        Return DA.GetDataDelete(oCustomClass)
    End Function
    Public Function SaveData(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal Implements [Interface].IAmortisasiJurnal.SaveData
        Dim DA As New SQLEngine.GL.AmortisasiJurnal
        Return DA.SaveData(oCustomClass)
    End Function
    Public Function SaveEditData(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal Implements [Interface].IAmortisasiJurnal.SaveEditData
        Dim DA As New SQLEngine.GL.AmortisasiJurnal
        Return DA.SaveEditData(oCustomClass)
    End Function
    'AmortisasiBiaya
    Public Function GetDataAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal Implements [Interface].IAmortisasiJurnal.GetDataAmortisasiBiaya
        Dim DA As New SQLEngine.GL.AmortisasiJurnal
        Return DA.GetDataAmortisasiBiaya(oCustomClass)
    End Function
    Public Function GetDataSchemeAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal Implements [Interface].IAmortisasiJurnal.GetDataSchemeAmortisasiBiaya
        Dim DA As New SQLEngine.GL.AmortisasiJurnal
        Return DA.GetDataSchemeAmortisasiBiaya(oCustomClass)
    End Function
    Public Function GetDataGridAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal Implements [Interface].IAmortisasiJurnal.GetDataGridAmortisasiBiaya
        Dim DA As New SQLEngine.GL.AmortisasiJurnal
        Return DA.GetDataGridAmortisasiBiaya(oCustomClass)
    End Function
    Public Function AmortisasiBiayaSave(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal Implements [Interface].IAmortisasiJurnal.AmortisasiBiayaSave
        Dim DA As New SQLEngine.GL.AmortisasiJurnal
        Return DA.AmortisasiBiayaSave(oCustomClass)
    End Function
    Public Function GenerateJournal(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal Implements [Interface].IAmortisasiJurnal.GenerateJournal
        Dim DA As New SQLEngine.GL.AmortisasiJurnal
        Return DA.GenerateJournal(oCustomClass)
    End Function
    Public Function GenerateJournalList(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal Implements [Interface].IAmortisasiJurnal.GenerateJournalList
        Dim DA As New SQLEngine.GL.AmortisasiJurnal
        Return DA.GenerateJournalList(oCustomClass)
    End Function

    Public Function GetApprovalAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal, ByVal strApproval As String) Implements [Interface].IAmortisasiJurnal.GetApprovalAmortisasiBiaya
        Dim DA As New SQLEngine.GL.AmortisasiJurnal
        Return DA.GetApprovalAmortisasiBiaya(oCustomClass, strApproval)
    End Function
    Public Function GetGridApprovalAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal Implements [Interface].IAmortisasiJurnal.GetGridApprovalAmortisasiBiaya
        Dim DA As New SQLEngine.GL.AmortisasiJurnal
        Return DA.GetGridApprovalAmortisasiBiaya(oCustomClass)
    End Function
    Public Function GetViewSchemeAmortisasiBiaya(ByVal oCustomClass As Parameter.AmortisasiJurnal) As Parameter.AmortisasiJurnal Implements [Interface].IAmortisasiJurnal.GetViewSchemeAmortisasiBiaya
        Dim DA As New SQLEngine.GL.AmortisasiJurnal
        Return DA.GetViewSchemeAmortisasiBiaya(oCustomClass)
    End Function

    Public Function Get_RequestNo(ByVal oCustomClass As Parameter.AmortisasiJurnal) As String Implements [Interface].IAmortisasiJurnal.Get_RequestNo
        Dim DA As New SQLEngine.GL.AmortisasiJurnal
        Return DA.Get_RequestNo(oCustomClass)
    End Function
End Class
