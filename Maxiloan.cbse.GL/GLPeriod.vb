﻿#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
#End Region
Public Class GLPeriod : Inherits ComponentBase
    Implements IGLPeriod

    Dim oSQLE As New SQLEngine.GL.GL_Period
    Public Function GLPeriodDelete(ByVal oCustomClass As Parameter.GlPeriod) As String Implements [Interface].IGLPeriod.GLPeriodDelete
        Return oSQLE.GLPeriodDelete(oCustomClass)
    End Function

    Public Function GLPeriodSaveAdd(ByVal oCustomClass As Parameter.GlPeriod) As String Implements [Interface].IGLPeriod.GLPeriodSaveAdd
        Return oSQLE.GLPeriodSaveAdd(oCustomClass)
    End Function

    Public Sub GLPeriodSaveEdit(ByVal oCustomClass As Parameter.GlPeriod) Implements [Interface].IGLPeriod.GLPeriodSaveEdit
        oSQLE.GLPeriodSaveEdit(oCustomClass)
    End Sub

    Public Function GetGLPeriod(ByVal oCustomClass As Parameter.GlPeriod) As Parameter.GlPeriod Implements [Interface].IGLPeriod.GetGLPeriod
        Return oSQLE.GetGLPeriod(oCustomClass)
    End Function

    Public Function GetGLPeriodList(ByVal oCustomClass As Parameter.GlPeriod) As Parameter.GlPeriod Implements [Interface].IGLPeriod.GetGLPeriodList
        Return oSQLE.GetGLPeriodList(oCustomClass)
    End Function
End Class
