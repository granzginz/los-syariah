﻿Imports Maxiloan.Parameter
Imports Maxiloan.Interface
Public Class BalanceSheet : Inherits ComponentBase
    Implements IBalanceSheet
    Private ReadOnly _da As New SQLEngine.GL.BalanceSheet

    Public Sub DeleteBSAccDetail(ByVal strCnn As String, ByVal rid As Integer, ByVal coa As String) Implements [Interface].IBalanceSheet.DeleteBSAccDetail
        _da.DeleteBSAccDetail(strCnn, rid, COA)
    End Sub

    Public Sub DeleteBSAccGroup(strCnn As String, coaId As Integer) Implements [Interface].IBalanceSheet.DeleteBSAccGroup
        _da.DeleteBSAccGroup(strCnn, coaId)
    End Sub

    Public Sub DeleteBSGroupDetail(strCnn As String, reportGroupId As Integer, groupId As Integer) Implements [Interface].IBalanceSheet.DeleteBSGroupDetail
        _da.DeleteBSGroupDetail(strCnn, reportGroupId, groupId)
    End Sub

    Public Sub DeleteData(strCnn As String, coaId As String) Implements [Interface].IBalanceSheet.DeleteData
        _da.DeleteData(strCnn, coaId)
    End Sub

    Public Function FindById(strCnn As String, rid As String, companyId As String) As Parameter.BalanceSheetSetup Implements [Interface].IBalanceSheet.FindById
        Return _da.FindById(strCnn, rid, companyId)
    End Function

    Public Function FindByIdBSAccDetail(strCnn As String, reportGroupId As Integer, coaId As String) As Parameter.BalanceSheetAccDetail Implements [Interface].IBalanceSheet.FindByIdBSAccDetail
        Return _da.FindByIdBSAccDetail(strCnn, reportGroupId, coaId)
    End Function

    Public Function FindByIdBSAccGroup(strCnn As String, reportGroupId As Integer) As Parameter.BalanceSheetAccGroup Implements [Interface].IBalanceSheet.FindByIdBSAccGroup
        Return _da.FindByIdBSAccGroup(strCnn, reportGroupId)
    End Function

    Public Function FindByIdBSGroupDetail(strCnn As String, reportGroupId As Integer, groupId As Integer) As Parameter.BalanceSheetGroupDetail Implements [Interface].IBalanceSheet.FindByIdBSGroupDetail
        Return _da.FindByIdBSGroupDetail(strCnn, reportGroupId, groupId)
    End Function

    Public Sub InsertBSAccDetail(strCnn As String, data As Parameter.BalanceSheetAccDetail) Implements [Interface].IBalanceSheet.InsertBSAccDetail
        _da.InsertBSAccDetail(strCnn, data)
    End Sub

    Public Sub InsertBSAccGroup(strCnn As String, data As Parameter.BalanceSheetAccGroup) Implements [Interface].IBalanceSheet.InsertBSAccGroup
        _da.InsertBSAccGroup(strCnn, data)
    End Sub

    Public Sub InsertBSGroupDetail(strCnn As String, data As Parameter.BalanceSheetGroupDetail) Implements [Interface].IBalanceSheet.InsertBSGroupDetail
        _da.InsertBSGroupDetail(strCnn, data)
    End Sub

    Public Sub InsertData(strCnn As String, data As Parameter.BalanceSheetSetup) Implements [Interface].IBalanceSheet.InsertData
        _da.InsertData(strCnn, data)
    End Sub

    Public Function SelectAllBSAccGroup(strCnn As String) As System.Data.DataTable Implements [Interface].IBalanceSheet.SelectAllBSAccGroup
        Return _da.SelectAllBSAccGroup(strCnn)
    End Function

    Public Function SelectAllBSGroupDetail(strCnn As String, ByVal RID As String) As System.Data.DataTable Implements [Interface].IBalanceSheet.SelectAllBSGroupDetail
        Return _da.SelectAllBSGroupDetail(strCnn, RID)
    End Function

    Public Function SelectAllCOA(strCnn As String) As System.Data.DataTable Implements [Interface].IBalanceSheet.SelectAllCOA
        Return _da.SelectAllCOA(strCnn)
    End Function

    Public Function SelectBSAccDetail(ByRef totRecord As Integer, strCnn As String, ParamArray param() As Object) As System.Collections.Generic.IList(Of Parameter.BalanceSheetAccDetail) Implements [Interface].IBalanceSheet.SelectBSAccDetail
        Return _da.SelectBSAccDetail(totRecord, strCnn, param)
    End Function

    Public Function SelectBSAccGroup(ByRef totRecord As Integer, strCnn As String, ParamArray param() As Object) As System.Collections.Generic.IList(Of Parameter.BalanceSheetAccGroup) Implements [Interface].IBalanceSheet.SelectBSAccGroup
        Return _da.SelectBSAccGroup(totRecord, strCnn, param)
    End Function

    Public Function SelectBSGroupDetail(ByRef totRecord As Integer, strCnn As String, ParamArray param() As Object) As System.Collections.Generic.IList(Of Parameter.BalanceSheetGroupDetail) Implements [Interface].IBalanceSheet.SelectBSGroupDetail
        Return _da.SelectBSGroupDetail(totRecord, strCnn, param)
    End Function

    Public Function SelectData(ByRef totRecord As Integer, strCnn As String, ParamArray param() As Object) As System.Collections.Generic.IList(Of Parameter.BalanceSheetSetup) Implements [Interface].IBalanceSheet.SelectData
        Return _da.SelectData(totRecord, strCnn, param)
    End Function

    Public Sub UpdateBSAccDetail(strCnn As String, data As Parameter.BalanceSheetAccDetail) Implements [Interface].IBalanceSheet.UpdateBSAccDetail
        _da.UpdateBSAccDetail(strCnn, data)
    End Sub

    Public Sub UpdateBSAccGroup(strCnn As String, data As Parameter.BalanceSheetAccGroup) Implements [Interface].IBalanceSheet.UpdateBSAccGroup
        _da.UpdateBSAccGroup(strCnn, data)
    End Sub

    Public Sub UpdateBSGroupDetail(strCnn As String, data As Parameter.BalanceSheetGroupDetail) Implements [Interface].IBalanceSheet.UpdateBSGroupDetail
        _da.UpdateBSGroupDetail(strCnn, data)
    End Sub

    Public Sub UpdateData(strCnn As String, data As Parameter.BalanceSheetSetup) Implements [Interface].IBalanceSheet.UpdateData
        _da.UpdateData(strCnn, data)
    End Sub

    Public Function GetAccountCOA(ByVal strConnection As String, ByVal ReportGroupId As String) As System.Data.DataTable Implements [Interface].IBalanceSheet.GetAccountCOA
        Return _da.GetAccountCOA(strConnection, ReportGroupId)
    End Function

End Class
