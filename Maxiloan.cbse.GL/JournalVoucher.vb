﻿Imports Maxiloan.Parameter
Imports Maxiloan.Interface


Public Class JournalVoucher : Inherits ComponentBase
    Implements IJournalVoucher

    Private ReadOnly _da As New SQLEngine.GL.JournalVoucher
    Private ReadOnly _dat As New SQLEngine.GL.JournalTransaction

    Public Function GetGlMasterSequence(ByVal strCnn As String, ByVal ParamArray param() As Object) As IList(Of ValueTextObject) Implements IJournalVoucher.GetGlMasterSequence
        Return _da.GetGlMasterSequence(strCnn, param)
    End Function


    Public Function DownloadToDBF(ByVal cnn As String, period1 As DateTime, period2 As DateTime, branch As String) As DataTable Implements IJournalVoucher.DownloadToDBF

        Return _da.DownloadToDBF(cnn, period1, period2, branch)
    End Function

    Public Function GetCompanyId(ByVal cnn As String) As String Implements IJournalVoucher.GetCompanyId
        Return _da.GetCompanyId(cnn)
    End Function

    Public Function GetJurnalVoucherByTrNo(ByVal cnn As String, ByVal transNo As String) As GlJournalVoucher Implements IJournalVoucher.GetJurnalVoucherByTrNo
        Return _da.GetJurnalVoucherByTrNo(cnn, transNo)
    End Function

    Public Function GlJournalAddUpdate(ByVal cnn As String, ByVal isAdd As Boolean, ByVal component As IList(Of GlJournalVoucher)) As String Implements IJournalVoucher.GlJournalAddUpdate
        Return _da.GlJournalAddUpdate(cnn, isAdd, component)
    End Function

    Public Function GlJournalHold(ByVal cnn As String, ByVal component As IList(Of GlJournalVoucher)) As String Implements IJournalVoucher.GlJournalHold
        Return _da.GlJournalHold(cnn, component)
    End Function

    Public Sub ValidateCsvComponen(ByVal cnn As String, ByVal component As IList(Of GlJournalVoucher)) Implements IJournalVoucher.ValidateCsvComponen
        _da.ValidateCsvComponen(cnn, component)
    End Sub

    Public Function DoUploadCsvGlJournals(ByVal cnn As String, ByVal component As IList(Of GlJournalVoucher)) As String Implements IJournalVoucher.DoUploadCsvGlJournals
        Return _da.DoUploadCsvGlJournals(cnn, component)
    End Function

    Public Function IsJournals(ByVal cnn As String, ByVal trDate As Date) As Boolean Implements IJournalVoucher.IsJournals
        Return _da.IsJournals(cnn, trDate)
    End Function

    Public Function GetGlJournalAvailablePost(ByRef totalrec As Integer, ByVal strCnn As String, ByVal ParamArray param As Object()) As GlJournalPostAvailable Implements IJournalVoucher.GetGlJournalAvailablePost
        Return _da.GetGlJournalAvailablePost(totalrec, strCnn, param)
    End Function
    Public Function DoDailyJournalPost(ByVal cnn As String, ByVal ParamArray param As Object()) As String Implements IJournalVoucher.DoDailyJournalPost
        Return _da.DoDailyJournalPost(cnn, param)
    End Function

    Public Function SelectInitialBudget(ByRef totRecord As Integer, ByVal strCnn As String, ByVal ParamArray param As Object()) As IList(Of InitialBudget) Implements IJournalVoucher.SelectInitialBudget
        Return _da.GetGlInitialBudget(totRecord, strCnn, param)
    End Function

    Public Function GetGlInitialFindOne(ByVal strCnn As String, ByVal param As InitialBudget) As InitialBudget Implements IJournalVoucher.GetGlInitialFindOne
        Return _da.GetGlInitialFindOne(strCnn, param)
    End Function

    Public Function GlInitialBudgetUpdate(ByVal cnn As String, ByVal component As IList(Of InitialBudget)) As String Implements IJournalVoucher.GlInitialBudgetUpdate
        Return _da.GlInitialBudgetUpdate(cnn, component)
    End Function

    Public Function GlInitialBudgetAdd(ByVal cnn As String, ByVal component As IList(Of InitialBudget)) As String Implements IJournalVoucher.GlInitialBudgetAdd
        Return _da.GlInitialBudgetAdd(cnn, component)
    End Function

    Public Function GlJournalApproval(ByVal cnn As String, ByVal param As JournalApproval, Optional ByVal approve As Boolean = False) As JournalApproval Implements IJournalVoucher.GlJournalApproval
        Return _da.GlJournalApproval(cnn, param, approve)
    End Function
    Public Function GlJournalApproval(ByVal cnn As String, ByVal component As IList(Of String)) As String Implements IJournalVoucher.GlJournalApproval
        Return _da.GlJournalApproval(cnn, component)
    End Function
    Public Function GlJournalVerify(ByVal cnn As String, ByVal component As IList(Of String)) As String Implements IJournalVoucher.GlJournalVerify
        Return _da.GlJournalVerify(cnn, component)
    End Function
    Function GlJournalPost(ByVal cnn As String, ByVal component As IList(Of String)) As String Implements IJournalVoucher.GlJournalPost
        Return _da.GlJournalPost(cnn, component)
    End Function

    Function DoJournalMonthlyClose(ByVal cnn As String, ByVal ParamArray param As Object()) As String Implements IJournalVoucher.DoJournalMonthlyClose
        Return _da.DoJournalMonthlyClose(cnn, param)
    End Function

    Function DoJournalYearlyClose(ByVal cnn As String, ByVal companyId As String, ByVal userId As String) As String Implements IJournalVoucher.DoJournalYearlyClose
        Return _da.DoJournalYearlyClose(cnn, companyId, userId)

    End Function
    Public Function GlJournalApprovalPage(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64) As IList(Of JournalApproval) Implements IJournalVoucher.GlJournalApprovalPage
        Return _da.GlJournalApprovalPage(cnn, param, pageSz, currPg, totalReq)
    End Function

    'Public Function GlGelMasVirifyUpload(ByVal cnn As String, ByVal component As GelMas) As String Implements IJournalVoucher.GlGelMasVirifyUpload
    '    Return _da.GlGelMasUpload(cnn, component)
    'End Function

    'Public Function GlGelMasUpload(ByVal cnn As String, ByVal component As GelMas) As String Implements IJournalVoucher.GlGelMasUpload
    '    Return _da.GlGelMasUpload(cnn, component, False, True)
    'End Function

    'Public Function GlGelMasUploadValidate(ByVal cnn As String, ByVal component As GelMas) As String Implements IJournalVoucher.GlGelMasUploadValidate
    '    Return _da.GlGelMasUpload(cnn, component, True)
    'End Function
    Function GLHistoryCOASelect(ByRef totalrec As Integer, ByVal strCnn As String, ByVal ParamArray param() As Object) As DataView Implements IJournalVoucher.GLHistoryCOASelect
        Return _da.GLHistoryCOASelect(totalrec, strCnn, param)
    End Function
    Public Function GLGelMASValidateCOASelect(ByRef totRecord As Integer, ByVal strCnn As String, ByVal ParamArray param As Object()) As IList(Of ValueTextObject) Implements IJournalVoucher.GLGelMASValidateCOASelect
        Return _da.GLGelMASValidateCOASelect(totRecord, strCnn, param)
    End Function

    Private ReadOnly _da1 As New SQLEngine.GL.GlYearPeriodDataAccess

    Public Function GenerateNewYearPeriods(ByVal cnn As String, ByVal year As GlPeriodYear) As String Implements IGlYearPeriod.GenerateNewYearPeriods
        Return _da1.NewYear(cnn, year)
    End Function

    Public Function SetCurrentGlPeriod(ByVal cnn As String, ByVal periodId As String) As String Implements IGlYearPeriod.SetCurrentGlPeriod
        Return _da1.SetCurrentGlPeriod(cnn, periodId)
    End Function
    'Taufik
    Public Function YearlyCloseValidate(ByVal cnn As String, ByVal companyId As String) As String Implements IGlYearPeriod.YearlyCloseValidate
        Return _da1.YearlyCloseValidate(cnn, companyId)
    End Function


    Public Function IsYearValid(ByVal cnn As String, ByVal year As Integer, ByVal companyId As String, ByVal branchid As String) As Boolean Implements IGlYearPeriod.IsYearValid
        Return _da1.IsYearValid(cnn, year, companyId, branchid)
    End Function


    Public Function GetGlJournalAvailablePostPage(ByVal cnn As String,
                                                  ByVal companyId As String,
                                                  ByVal branchId As String,
                                                  ByVal trDateFrom As DateTime,
                                                  ByVal trDateTo As DateTime,
                                                  pageSz As Integer, currPg As Integer, ByRef totalReq As Int64) As IList(Of JournalApproval) Implements IJournalVoucher.GetGlJournalAvailablePostPage
        Return _da.GetGlJournalAvailablePostPage(cnn, companyId, branchId, trDateFrom, trDateTo, pageSz, currPg, totalReq)
    End Function


    Function GlJournalVerifyPage(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64, ByVal wOpt As Integer, ByVal cmdwhere As String) As IList(Of JournalApproval) Implements IJournalVoucher.GlJournalVerifyPage
        Return _da.GlJournalVerifyPage(cnn, param, pageSz, currPg, totalReq, wOpt, cmdwhere)
    End Function

    Public Function GetHifrontDetails(cnn As String, dateProc As Date) As System.Text.StringBuilder Implements IJournalVoucher.GetHifrontDetails
        Return _da.GetHifrontDetails(cnn, dateProc)
    End Function


    Public Function GetJournalTransaction(oCustom As Parameter.JournalTransaction) As Parameter.JournalTransaction Implements [Interface].IJournalVoucher.GetJournalTransaction
        Return _dat.GetJournalTransaction(oCustom)
    End Function

    Private Function IGlYearPeriod_YearPeriods(cnn As String, companyid As String, branchid As String, year As Integer, month As Integer) As IList(Of Parameter.GlPeriod) Implements IGlYearPeriod.YearPeriods
        Return _da1.YearPeriods(cnn, companyid, branchid, year, month)
    End Function
    'Taufik
    'Private Function IGlYearPeriod_CurrentYearPeriod(cnn As String, companyid As String, branchid As String) As Parameter.GlPeriod Implements IGlYearPeriod.CurrentYearPeriod
    '    Return _da1.CurrentYearPeriod(cnn, companyid, branchid)
    'End Function
    Public Function GlJournalUnPostPage(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64) As IList(Of JournalApproval) Implements IJournalVoucher.GlJournalUnPostPage
        Return _da.GlJournalUnPostPage(cnn, param, pageSz, currPg, totalReq)
    End Function
    Public Function GlJournalUnPost(ByVal cnn As String, ByVal component As IList(Of String)) As String Implements IJournalVoucher.GlJournalUnPost
        Return _da.GlJournalUnPost(cnn, component)
    End Function

    Public Function GlJournalUnPostPageFilter(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64, ByVal wOpt As Integer, ByVal cmdwhere As String) As IList(Of JournalApproval) Implements IJournalVoucher.GlJournalUnPostPageFilter
        Return _da.GlJournalUnPostPageFilter(cnn, param, pageSz, currPg, totalReq, wOpt, cmdwhere)
    End Function

    Public Function GlJournalAddDelete(cnn As String, isAdd As Boolean, component As IList(Of GlJournalVoucher)) As String Implements IJournalVoucher.GlJournalAddDelete
        Return _da.GlJournalAddDelete(cnn, isAdd, component)
    End Function
    Function GlJournalPageNotSuccess(ByVal cnn As String, ByVal param As JournalApproval, pageSz As Integer, currPg As Integer, ByRef totalReq As Int64) As IList(Of JournalApproval) Implements IJournalVoucher.GlJournalPageNotSuccess
        Return _da.GlJournalPageNotSuccess(cnn, param, pageSz, currPg, totalReq)
    End Function
End Class
