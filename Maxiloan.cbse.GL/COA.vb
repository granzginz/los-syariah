﻿
Imports Maxiloan.Interface
Imports Maxiloan.Parameter

Public Class COA : Inherits ComponentBase
    Implements ICOA, IGlCurrency
    Private ReadOnly _da As New SQLEngine.GL.COA

    Public Function AutoComplateCoaId(strCnn As String, coaid As String) As List(Of String) Implements ICOA.AutoComplateCoaId
        Return _da.AutoComplateCoaId(strCnn, coaid)
    End Function

    Public Function SelectData(ByRef tRec As Integer, ByVal strCnn As String, ByVal fromLookup As Boolean, isLeaf As Boolean, ByVal ParamArray param As Object()) As IList(Of MasterAccountObject) Implements ICOA.SelectData
        Return _da.SelectData(tRec, strCnn, fromLookup, isLeaf, param)
    End Function
    Public Function SelectDataIsLeaf(ByRef tRec As Integer, ByVal strCnn As String, ByVal fromLookup As Boolean, isLeaf As Boolean, ByVal ParamArray param As Object()) As IList(Of MasterAccountObject) Implements ICOA.SelectDataIsLeaf
        Return _da.SelectDataIsLeaf(tRec, strCnn, fromLookup, isLeaf, param)
    End Function

    Public Sub DeleteData(ByVal strCnn As String, ByVal coaId As String, ByVal Addfree As String) Implements ICOA.DeleteData
        _da.DeleteData(strCnn, coaId, Addfree)
    End Sub

    Public Sub InsertData(ByVal strCnn As String, ByVal data As MasterAccountObject) Implements ICOA.InsertData
        _da.InsertData(strCnn, data)
    End Sub

    Public Sub UpdateData(ByVal strCnn As String, ByVal data As MasterAccountObject) Implements ICOA.UpdateData

        _da.UpdateData(strCnn, data)
    End Sub

    Public Function FindById(ByVal strCnn As String, ByVal coaId As String, ByVal branchId As String, ByVal companyId As String) As MasterAccountObject Implements ICOA.FindById
        Return _da.FindById(strCnn, coaId, branchId, companyId)
    End Function
    Public Function FindByDesc(ByVal strCnn As String, ByVal Description As String, ByVal branchId As String, ByVal companyId As String) As MasterAccountObject Implements ICOA.FindByDesc
        Return _da.FindByDesc(strCnn, Description, branchId, companyId)
    End Function

    Private ReadOnly _da1 As New SQLEngine.GL.GlCurrencyDataAccess


    Public Function GetGlCurrencies(ByVal cnn As String) As IList(Of Maxiloan.Parameter.GlCurrency) Implements IGlCurrency.GetGlCurrencies
        Return _da1.GetGlCurrencies(cnn)
    End Function

    Public Function GetHomeCurrency(ByVal cnn As String) As Maxiloan.Parameter.GlCurrency Implements IGlCurrency.GetHomeCurrency
        Return _da1.GetHomeCurrency(cnn)
    End Function

    Public Function GetDefaultCurrency(ByVal cnn As String) As Maxiloan.Parameter.GlCurrency Implements IGlCurrency.GetDefaultCurrency
        Return _da1.GetDefaultCurrency(cnn)
    End Function
    Public Function AutoComplateCoaDesc(strCnn As String, coaid As String) As List(Of String) Implements ICOA.AutoComplateCoaDesc
        Return _da.AutoComplateCoaDesc(strCnn, coaid)
    End Function
End Class
