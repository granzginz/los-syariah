
#Region "Imports"

Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
#End Region

Public Class Implementasi : Inherits Maxiloan.SQLEngine.DataAccessBase
    Private Const LIST_SELECT As String = "spPagingBankAccountCabang"
    Protected Const PARAM_BANKACCOUNTID As String = "@bankaccountid"
    Protected Const PARAM_REFERENCENO As String = "@referenceno"

    Private m_connection As SqlConnection

    Public Function GetSP(ByVal customclass As Parameter.Implementasi) As Parameter.Implementasi
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.Listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, customclass.SPName, params).Tables(0)
        Return customclass
    End Function
    Public Sub UpdateOutstandingPokok(ByVal oCustomClass As Parameter.Implementasi)
        Dim params(2) As SqlParameter

        Dim objTrans As SqlTransaction

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(1).Value = oCustomClass.BranchId
        params(2) = New SqlParameter("@OutstandingPrincipal", SqlDbType.Decimal)
        params(2).Value = oCustomClass.NilaiOSP

        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spImOutstandingPokokSave", params)
            objTrans.Commit()
        Catch exp As Exception
            objTrans.Rollback()
            WriteException("UpdateOutstandingPokok", "UpdateOutstandingPokok", exp.Message + exp.StackTrace)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Sub
    Public Sub UpdateBiayaTarik(ByVal oCustomClass As Parameter.Implementasi)
        Dim params(2) As SqlParameter

        Dim objTrans As SqlTransaction

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(1).Value = oCustomClass.BranchId
        params(2) = New SqlParameter("@BiayaTarik", SqlDbType.Decimal)
        params(2).Value = oCustomClass.BiayaTarik

        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spImBiayaTarikSave", params)
            objTrans.Commit()
        Catch exp As Exception
            objTrans.Rollback()
            WriteException("UpdateBiayaTarik", "UpdateBiayaTarik", exp.Message + exp.StackTrace)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Sub
    Public Sub UpdateNDT(ByVal oCustomClass As Parameter.Implementasi)
        Dim params(2) As SqlParameter

        Dim objTrans As SqlTransaction

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(1).Value = oCustomClass.BranchId
        params(2) = New SqlParameter("@NilaiNDT", SqlDbType.Decimal)
        params(2).Value = oCustomClass.NilaiNDT

        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spImNDTSave", params)
            objTrans.Commit()
        Catch exp As Exception
            objTrans.Rollback()
            WriteException("UpdateNDT", "UpdateNDT", exp.Message + exp.StackTrace)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Sub

    Public Function GetBankAccountCabang(oCustomClass As Object) As Parameter.Implementasi
        Dim oReturnValue As New Parameter.Implementasi
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(4).Value = oCustomClass.BranchId
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(5).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "GetCustomer", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.GetCustomer")
        End Try
    End Function

    Public Function GetGeneralPaging(ByVal CustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim oReturnValue As New Parameter.GeneralPaging
        Dim params(4) As SqlParameter

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = CustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = CustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = CustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = CustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, CustomClass.SpName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function GetPeriodJournal(ByVal customClass As Parameter.Implementasi) As Parameter.Implementasi
        Dim oDetail As Parameter.Implementasi
        Try
            customClass = GetPeriodJournalHeader(customClass)
            Return customClass
        Catch exp As Exception
            WriteException("Journal", "GetPeriodJournal", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetPeriodJournalHeader(ByVal oET As Parameter.Implementasi) As Parameter.Implementasi
        Dim params(2) As SqlParameter
        Dim objReader As SqlDataReader

        Try
            With oET

                params(0) = New SqlParameter("@Companyid", SqlDbType.Char, 100)
                params(0).Value = "001"

                params(1) = New SqlParameter("@year", SqlDbType.Int)
                params(1).Value = .Year

                params(2) = New SqlParameter("@month", SqlDbType.Int)
                params(2).Value = .Month
                objReader = SqlHelper.ExecuteReader(.strConnection, CommandType.StoredProcedure, "GetYearPeriodList_", params)
            End With

            If objReader.Read Then
                With oET
                    .PeriodStatus = CType(objReader("PeriodStatus"), Int64)
                    .PeriodEverClosed = CType(objReader("PeriodStatus"), Int64)
                End With
            End If
            objReader.Close()
            Return oET
        Catch exp As Exception
            WriteException("PaymentRequest", "GetPeriodJournal", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetBankAccountFunding(ByVal strConnection As String) As DataTable


        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))


        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, "spGetBankAccountFunding")

            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString

                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "spGetBankAccountBranch", exp.Message + exp.StackTrace)
        End Try

    End Function
    Public Function GetFundingCoyID(ByVal strConnection As String, FundingCoyID As String) As DataTable


        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))

        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(0).Value = FundingCoyID

        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, "spFundingComboContract", params)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString

                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "spGetBankAccountBranch", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function GetFundingBatch(ByVal strConnection As String, FundingContractNo As String) As DataTable


        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))

        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(0).Value = FundingContractNo

        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, "spFundingComboBatch", params)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString

                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "spGetBankAccountBranch", exp.Message + exp.StackTrace)
        End Try

    End Function
    Public Sub SavePembayaranTDP(ByVal oCustomClass As Parameter.Implementasi)
        Dim params(6) As SqlParameter

        Dim objTrans As SqlTransaction

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.ApplicationID
        params(1) = New SqlParameter("@Amount", SqlDbType.Decimal)
        params(1).Value = oCustomClass.amount
        params(2) = New SqlParameter("@Reffno", SqlDbType.VarChar, 20)
        params(2).Value = oCustomClass.ReffNo
        params(3) = New SqlParameter("@ValueDate", SqlDbType.Date)
        params(3).Value = oCustomClass.valutadate
        params(4) = New SqlParameter("@SourceRK", SqlDbType.VarChar, 15)
        params(4).Value = oCustomClass.SourceRK
        params(5) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 20)
        params(5).Value = oCustomClass.bankaccountid
        params(6) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(6).Value = oCustomClass.BranchId
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spSavePembayaranTDP", params)
            objTrans.Commit()
        Catch exp As Exception
            objTrans.Rollback()
            WriteException("spSavePembayaranTDP", "spSavePembayaranTDP", exp.Message + exp.StackTrace)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Sub
    Public Function GetBankAccountHOTransfer(ByVal strConnection As String, ByVal BankID As String) As DataTable


        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))

        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
        params(0).Value = BankID

        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, "spGetBankAccountHOTransfer", params)

            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString

                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "spGetBankAccountHOTransfer", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Sub SaveTerimaTDP(ByVal customclass As Parameter.TerimaTDP)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(8) {}
        Dim params1() As SqlParameter = New SqlParameter(4) {}

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            If customclass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = customclass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = customclass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = customclass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "K"
                SqlHelper.ExecuteScalar(objtrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                customclass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@bankaccountid", SqlDbType.VarChar, 10)
            params(1).Value = customclass.BankAccountID

            params(2) = New SqlParameter("@loginid", SqlDbType.VarChar, 12)
            params(2).Value = customclass.LoginId

            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = customclass.BusinessDate

            params(4) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(4).Value = customclass.AmountRec

            params(5) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(5).Value = customclass.ValueDate

            params(6) = New SqlParameter("@referenceNo", SqlDbType.Char, 20)
            params(6).Value = customclass.ReferenceNo

            params(7) = New SqlParameter("@Notes", SqlDbType.Char, 50)
            params(7).Value = customclass.description

            params(8) = New SqlParameter("@CoyID", SqlDbType.Char, 3)
            params(8).Value = customclass.CoyID


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spTerimaTDP", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("TerimaTDP", "SaveTerimaTDP", exp.Message + exp.StackTrace)
        End Try
    End Sub
    Public Sub SaveOtorisasiTDP(ByVal oEntities As Parameter.TerimaTDP)
        Dim objcon As New SqlConnection(oEntities.strConnection)
        Dim objtrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(1) {}


        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@TDPReceiveNo", SqlDbType.VarChar, 20)
            params(0).Value = oEntities.TDPReceiveNo

            params(1) = New SqlParameter("@loginid", SqlDbType.VarChar, 12)
            params(1).Value = oEntities.LoginId

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spOtorisasiTDP", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("OtorisasiTDP", "SaveOtorisasiTDP", exp.Message + exp.StackTrace)
        End Try
    End Sub
    Public Function TDPReversalList(ByVal oCustomClass As Parameter.TerimaTDP) As Parameter.TerimaTDP
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = oCustomClass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = oCustomClass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = oCustomClass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = oCustomClass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spTDPReversalPaging", params).Tables(0)
            oCustomClass.TotalRecord = CInt(params(4).Value)
            Return oCustomClass
        Catch exp As Exception
            WriteException("TDPReversal", "TDPReversalList", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ReverseTDP(ByVal oCustomClass As Parameter.TerimaTDP) As Parameter.TerimaTDP
        Dim objread As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@TDPReceiveNo", SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.TDPReceiveNo

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = oCustomClass.BranchId

            objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spTDPReversalList", params)
            If objread.Read Then
                With oCustomClass
                    .TDPReceiveNo = CType(objread("TDPReceiveNo"), String)
                    .ReferenceNo = CType(objread("TransferRefNo"), String)
                    .postingdate = CType(objread("postingdate"), Date)
                    .BankAccountID = CType(objread("bankaccountid"), String)
                    .BankAccountName = CType(objread("bankaccountname"), String)

                    .ValueDate = CType(objread("valuedate"), Date)
                    .AmountRec = CType(objread("amount"), Double)
                    .description = CType(objread("description"), String)
                    .StatusDate = CType(objread("statusdate"), Date)
                    .VoucherNo = CType(objread("VoucherNoRcv"), String)
                    .Agreementno = CType(objread("agreementno"), String)
                    .CustomerName = CType(objread("Customername"), String)
                    .TDPStatus = CType(objread("TDPReceiveStatus"), String)
                    .TDPStatusdesc = CType(objread("TDPstatusdesc"), String)

                End With
            End If
            objread.Close()
            Return oCustomClass
        Catch exp As Exception
            WriteException("ReversalTDP", "ReverseTDP", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Sub SaveTDPReverse(ByVal customclass As Parameter.TerimaTDP)
        Dim params() As SqlParameter = New SqlParameter(9) {}
        Dim params1() As SqlParameter = New SqlParameter(4) {}
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            If customclass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = customclass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = customclass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = customclass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"
                SqlHelper.ExecuteScalar(objtrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                customclass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter(PARAM_BANKACCOUNTID, SqlDbType.VarChar, 10)
            params(1).Value = customclass.BankAccountID

            params(2) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(2).Value = customclass.LoginId

            params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(3).Value = customclass.BusinessDate

            params(4) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(4).Value = customclass.AmountRec

            params(5) = New SqlParameter(PARAM_VALUEDATE, SqlDbType.DateTime)
            params(5).Value = customclass.ValueDate

            params(6) = New SqlParameter(PARAM_REFERENCENO, SqlDbType.Char, 20)
            params(6).Value = customclass.ReferenceNo

            params(7) = New SqlParameter("@Notes", SqlDbType.Char, 50)
            params(7).Value = customclass.description

            params(8) = New SqlParameter(PARAM_COMPANYID, SqlDbType.Char, 3)
            params(8).Value = customclass.CoyID

            params(9) = New SqlParameter("@TDPReceiveNo", SqlDbType.VarChar, 20)
            params(9).Value = customclass.TDPReceiveNo



            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spTDPReversal", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("TDP Reverse", "SaveTDPReverse", exp.Message + exp.StackTrace)
        End Try
    End Sub

    Public Sub TDPSplit(ByVal oCustomclass As Parameter.TerimaTDP)
        Dim objtrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(4) {}

            m_connection = New SqlConnection(oCustomclass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objtrans = m_connection.BeginTransaction

            params(0) = New SqlParameter("@Amount", SqlDbType.Int)
            params(0).Value = oCustomclass.AmountRec
            params(1) = New SqlParameter("@Desc", SqlDbType.VarChar, 100)
            params(1).Value = oCustomclass.description
            params(2) = New SqlParameter("@TDPReceiveNoRef", SqlDbType.VarChar, 20)
            params(2).Value = oCustomclass.TDPReceiveNo
            params(3) = New SqlParameter("@branchid", SqlDbType.Char, 3)
            params(3).Value = oCustomclass.BranchId
            params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(4).Value = oCustomclass.BusinessDate

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spTDPSplit", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("TDP Split", "TDPSplit", exp.Message + exp.StackTrace)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Sub

#Region "FixedAsset"
    Public Function PaymentRequestFAList(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
		Dim params() As SqlParameter = New SqlParameter(4) {}
		Try
			params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
			params(0).Value = oCustomClass.CurrentPage

			params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
			params(1).Value = oCustomClass.PageSize

			params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
			params(2).Value = oCustomClass.WhereCond

			params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
			params(3).Value = oCustomClass.SortBy

			params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
			params(4).Direction = ParameterDirection.Output

			oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spPaymentRequestFAPaging", params).Tables(0)
			oCustomClass.TotalRecord = CInt(params(4).Value)
			Return oCustomClass
		Catch exp As Exception
			WriteException("Payment Request Fixed Asset List", "Payment Request Fixed Asset List", exp.Message + exp.StackTrace)
		End Try
	End Function

	Public Function PaymentRequestFAOtor(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
		Dim params() As SqlParameter = New SqlParameter(4) {}
		Try
			params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
			params(0).Value = oCustomClass.CurrentPage

			params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
			params(1).Value = oCustomClass.PageSize

			params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
			params(2).Value = oCustomClass.WhereCond

			params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
			params(3).Value = oCustomClass.SortBy

			params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
			params(4).Direction = ParameterDirection.Output

			oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spPaymentRequestFAPagingOtor", params).Tables(0)
			oCustomClass.TotalRecord = CInt(params(4).Value)
			Return oCustomClass
		Catch exp As Exception
			WriteException("Payment Request Fixed Asset List", "Payment Request Fixed Asset List", exp.Message + exp.StackTrace)
		End Try
	End Function

	Public Function PaymentRequestFASave(ByVal oCustomClass As Parameter.TransFixedAsset)
        Dim params() As SqlParameter = New SqlParameter(14) {}
        Dim objcon As New SqlConnection(oCustomClass.strConnection)
		Dim objtrans As SqlTransaction = Nothing
		Dim ErrMessage As String = ""
		Try
			If objcon.State = ConnectionState.Closed Then objcon.Open()
			objtrans = objcon.BeginTransaction

			params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
			params(0).Value = oCustomClass.BranchId

			params(1) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10)
			params(1).Value = oCustomClass.BankAccountID

			params(2) = New SqlParameter("@LoginID", SqlDbType.VarChar, 12)
			params(2).Value = oCustomClass.LoginId

			params(3) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
			params(3).Value = oCustomClass.ValueDate

			params(4) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 30)
			params(4).Value = oCustomClass.ReferenceNo

			params(5) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
			params(5).Value = oCustomClass.InvoiceNo

			params(6) = New SqlParameter("@PaymentAllocationID", SqlDbType.VarChar, 20)
			params(6).Value = oCustomClass.PaymentAllocationID

			params(7) = New SqlParameter("@Post", SqlDbType.Char, 1)
			params(7).Value = oCustomClass.Post

			params(8) = New SqlParameter("@Amount", SqlDbType.Decimal)
			params(8).Value = oCustomClass.Amount

			params(9) = New SqlParameter("@RefDesc", SqlDbType.VarChar, 50)
			params(9).Value = oCustomClass.RefDesc

			params(10) = New SqlParameter("@VoucherDesc", SqlDbType.VarChar, 50)
			params(10).Value = oCustomClass.VoucherDesc

			params(11) = New SqlParameter("@Receiver", SqlDbType.VarChar, 50)
			params(11).Value = oCustomClass.ReceivedFrom

			params(12) = New SqlParameter("@Departement", SqlDbType.VarChar, 50)
            params(12).Value = oCustomClass.Departement

            params(13) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 20)
            params(13).Value = oCustomClass.ApprovalNo

            params(14) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(14).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spPaymentRequestFASave", params)
            oCustomClass.strError = CType(params(14).Value, String)
            If oCustomClass.strError <> "" Then
				objtrans.Rollback()
				Return oCustomClass.strError
			Else
				objtrans.Commit()
			End If

		Catch exp As Exception
			objtrans.Rollback()
			WriteException("Save payment Request", "PaymentRequestFASave", exp.Message + exp.StackTrace)
			Return Nothing
		End Try

	End Function

	Public Function PaymentRequestFAOtorSave(ByVal oCustomClass As Parameter.TransFixedAsset)
		Dim params() As SqlParameter = New SqlParameter(1) {}
		Dim objcon As New SqlConnection(oCustomClass.strConnection)
		Dim objtrans As SqlTransaction = Nothing
		Dim ErrMessage As String = ""
		Try
			If objcon.State = ConnectionState.Closed Then objcon.Open()
			objtrans = objcon.BeginTransaction

			params(0) = New SqlParameter("@LoginID", SqlDbType.VarChar, 12)
			params(0).Value = oCustomClass.LoginId

			params(1) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
			params(1).Value = oCustomClass.InvoiceNo

			SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spOtorisasiPaymentRequestFA", params)
			objtrans.Commit()
		Catch exp As Exception
			objtrans.Rollback()
			WriteException("Otor payment Request", "spOtorisasiPaymentRequestFA", exp.Message + exp.StackTrace)
			Return Nothing
		End Try

	End Function

	Public Function RequestReceiveFAList(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
		Dim params() As SqlParameter = New SqlParameter(4) {}
		Try
			params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
			params(0).Value = oCustomClass.CurrentPage

			params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
			params(1).Value = oCustomClass.PageSize

			params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
			params(2).Value = oCustomClass.WhereCond

			params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
			params(3).Value = oCustomClass.SortBy

			params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
			params(4).Direction = ParameterDirection.Output

			oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spRequestReceiveFAPaging", params).Tables(0)
			oCustomClass.TotalRecord = CInt(params(4).Value)
			Return oCustomClass
		Catch exp As Exception
			WriteException("Request Receive Fixed Asset List", "Request Receive Fixed Asset List", exp.Message + exp.StackTrace)
		End Try
	End Function

	Public Function RequestReceiveFAOtor(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
		Dim params() As SqlParameter = New SqlParameter(4) {}
		Try
			params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
			params(0).Value = oCustomClass.CurrentPage

			params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
			params(1).Value = oCustomClass.PageSize

			params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
			params(2).Value = oCustomClass.WhereCond

			params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
			params(3).Value = oCustomClass.SortBy

			params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
			params(4).Direction = ParameterDirection.Output

			oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spRequestReceiveFAPagingOtor", params).Tables(0)
			oCustomClass.TotalRecord = CInt(params(4).Value)
			Return oCustomClass
		Catch exp As Exception
			WriteException("Payment Request Fixed Asset List", "Payment Request Fixed Asset List", exp.Message + exp.StackTrace)
		End Try
	End Function

	Public Function RequestReceiveFASave(ByVal oCustomClass As Parameter.TransFixedAsset)
		Dim params() As SqlParameter = New SqlParameter(7) {}
		Dim objcon As New SqlConnection(oCustomClass.strConnection)
		Dim objtrans As SqlTransaction = Nothing
		Dim ErrMessage As String = ""
		Try
			If objcon.State = ConnectionState.Closed Then objcon.Open()
			objtrans = objcon.BeginTransaction

			params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
			params(0).Value = oCustomClass.BranchId

			params(1) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10)
			params(1).Value = oCustomClass.BankAccountID

			params(2) = New SqlParameter("@LoginID", SqlDbType.VarChar, 12)
			params(2).Value = oCustomClass.LoginId

			params(3) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
			params(3).Value = oCustomClass.ValueDate

			params(4) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 30)
			params(4).Value = oCustomClass.ReferenceNo

			params(5) = New SqlParameter("@Amount", SqlDbType.Decimal)
			params(5).Value = oCustomClass.Amount

			params(6) = New SqlParameter("@AktivaId", SqlDbType.VarChar, 100)
			params(6).Value = oCustomClass.AktivaId

			params(7) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
			params(7).Direction = ParameterDirection.Output

			SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spRequestReceiveFASave", params)
			oCustomClass.strError = CType(params(7).Value, String)
			If oCustomClass.strError <> "" Then
				objtrans.Rollback()
				Return oCustomClass.strError
			Else
				objtrans.Commit()
			End If

		Catch exp As Exception
			objtrans.Rollback()
			WriteException("Save payment Request", "RequestReceiveFASave", exp.Message + exp.StackTrace)
			Return Nothing
		End Try

	End Function

    Public Function RequestReceiveFAOtorSave(ByVal oCustomClass As Parameter.TransFixedAsset)
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim objcon As New SqlConnection(oCustomClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@LoginID", SqlDbType.VarChar, 12)
            params(0).Value = oCustomClass.LoginId

            params(1) = New SqlParameter("@AktivaId", SqlDbType.VarChar, 100)
            params(1).Value = oCustomClass.AktivaId

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spOtorisasiRequestReceiveFA", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Otor payment Request", "spOtorisasiRequestReceiveFA", exp.Message + exp.StackTrace)
            Return Nothing
        End Try

    End Function


    Public Function PenjualanFAList(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = oCustomClass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = oCustomClass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = oCustomClass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = oCustomClass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spPenjualanFAPaging", params).Tables(0)
            oCustomClass.TotalRecord = CInt(params(4).Value)
            Return oCustomClass
        Catch exp As Exception
            WriteException("Request Receive Fixed Asset List", "Request Receive Fixed Asset List", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function PenjualanFixedAssetListOtor(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = oCustomClass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = oCustomClass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = oCustomClass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = oCustomClass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spPenjualanFAPagingOtor", params).Tables(0)
            oCustomClass.TotalRecord = CInt(params(4).Value)
            Return oCustomClass
        Catch exp As Exception
            WriteException("Payment Request Fixed Asset List", "Payment Request Fixed Asset List", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function PenjualanFASave(ByVal oCustomClass As Parameter.TransFixedAsset)
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Dim objcon As New SqlConnection(oCustomClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId

            params(1) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10)
            params(1).Value = oCustomClass.BankAccountID

            params(2) = New SqlParameter("@LoginID", SqlDbType.VarChar, 12)
            params(2).Value = oCustomClass.LoginId

            params(3) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(3).Value = oCustomClass.ValueDate

            params(4) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 30)
            params(4).Value = oCustomClass.ReferenceNo

            params(5) = New SqlParameter("@Amount", SqlDbType.Decimal)
            params(5).Value = oCustomClass.Amount

            params(6) = New SqlParameter("@AktivaId", SqlDbType.VarChar, 100)
            params(6).Value = oCustomClass.AktivaId

            params(7) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(7).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spPenjualanFASave", params)
            oCustomClass.strError = CType(params(7).Value, String)
            If oCustomClass.strError <> "" Then
                objtrans.Rollback()
                Return oCustomClass.strError
            Else
                objtrans.Commit()
            End If

        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Save payment Request", "RequestReceiveFASave", exp.Message + exp.StackTrace)
            Return Nothing
        End Try

    End Function

    Public Function PenjualanFAOtorSave(ByVal oCustomClass As Parameter.TransFixedAsset)
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim objcon As New SqlConnection(oCustomClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@LoginID", SqlDbType.VarChar, 12)
            params(0).Value = oCustomClass.LoginId

            params(1) = New SqlParameter("@AktivaId", SqlDbType.VarChar, 100)
            params(1).Value = oCustomClass.AktivaId

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spOtorisasiPenjualanFA", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Otor payment Request", "spOtorisasiRequestReceiveFA", exp.Message + exp.StackTrace)
            Return Nothing
        End Try

    End Function


    Public Function PenghapusanFAList(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = oCustomClass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = oCustomClass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = oCustomClass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = oCustomClass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spPenghapusanFAPaging", params).Tables(0)
            oCustomClass.TotalRecord = CInt(params(4).Value)
            Return oCustomClass
        Catch exp As Exception
            WriteException("Request Receive Fixed Asset List", "Request Receive Fixed Asset List", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function PenghapusanFAOtor(ByVal oCustomClass As Parameter.TransFixedAsset) As Parameter.TransFixedAsset
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = oCustomClass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = oCustomClass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = oCustomClass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = oCustomClass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spPenghapusanFAPagingOtor", params).Tables(0)
            oCustomClass.TotalRecord = CInt(params(4).Value)
            Return oCustomClass
        Catch exp As Exception
            WriteException("Payment Request Fixed Asset List", "Payment Request Fixed Asset List", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function PenghapusanFASave(ByVal oCustomClass As Parameter.TransFixedAsset)
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Dim objcon As New SqlConnection(oCustomClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId

            params(1) = New SqlParameter("@InternalMemoNo", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.InternalMemoNo

            params(2) = New SqlParameter("@DeleteReason", SqlDbType.VarChar, 50)
            params(2).Value = oCustomClass.DeleteReason

            params(3) = New SqlParameter("@Notes", SqlDbType.VarChar, 50)
            params(3).Value = oCustomClass.Notes

            params(4) = New SqlParameter("@LoginID", SqlDbType.VarChar, 12)
            params(4).Value = oCustomClass.LoginId

            params(5) = New SqlParameter("@InvoiceDate", SqlDbType.Date)
            params(5).Value = oCustomClass.InvoiceDate

            params(6) = New SqlParameter("@AktivaId", SqlDbType.VarChar, 100)
            params(6).Value = oCustomClass.AktivaId

            params(7) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
            params(7).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spPenghapusanFASave", params)
            oCustomClass.strError = CType(params(7).Value, String)
            If oCustomClass.strError <> "" Then
                objtrans.Rollback()
                Return oCustomClass.strError
            Else
                objtrans.Commit()
            End If

        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Save payment Request", "RequestReceiveFASave", exp.Message + exp.StackTrace)
            Return Nothing
        End Try

    End Function

    Public Function PenghapusanFAOtorSave(ByVal oCustomClass As Parameter.TransFixedAsset)
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim objcon As New SqlConnection(oCustomClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@LoginID", SqlDbType.VarChar, 12)
            params(0).Value = oCustomClass.LoginId

            params(1) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 100)
            params(1).Value = oCustomClass.InvoiceNo

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spOtorisasiPenghapusanFA", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Otor payment Request", "spOtorisasiRequestReceiveFA", exp.Message + exp.StackTrace)
            Return Nothing
        End Try

    End Function

#End Region


    Public Function PaymentRequestAgreementUnitExpenseList(ByVal oCustomClass As Parameter.AgreementUnitExpense) As Parameter.AgreementUnitExpense
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = oCustomClass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = oCustomClass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = oCustomClass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = oCustomClass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spRequestReceiveAgreementUnitExpensePaging", params).Tables(0)
            oCustomClass.TotalRecord = CInt(params(4).Value)
            Return oCustomClass
        Catch exp As Exception
            WriteException("Request Receive Agreement Unit Expense List", "Request Receive Agreement Unit Expense List", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function CetakKartuPiutangPaging(ByVal oCustomClass As Parameter.CetakKartuPiutang) As Parameter.CetakKartuPiutang
        Dim oReturnValue As New Parameter.CetakKartuPiutang
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spPagingCetakKartuPiutang", params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub EksekusiSave(ByVal oCustomClass As Parameter.UploadChanneling)
        Dim oReturnValue As New Parameter.UploadChanneling
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Dim objTrans As SqlTransaction

        params(0) = New SqlParameter("@TransactionNo", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.TransactionNo

        'Try
        '    SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spEksekusiSave", params)

        'Catch ex As Exception
        '    Throw New Exception(ex.Message)
        'End Try


        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spEksekusiDelete", params)
            objTrans.Commit()
        Catch exp As Exception
            objTrans.Rollback()
            WriteException("UpdateNDT", "UpdateNDT", exp.Message + exp.StackTrace)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Sub

    Public Sub EksekusiDelete(ByVal oCustomClass As Parameter.UploadChanneling)
        Dim oReturnValue As New Parameter.UploadChanneling
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Dim objTrans As SqlTransaction

        params(0) = New SqlParameter("@TransactionNo", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.TransactionNo

        'Try
        '    SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spEksekusiDelete", params)

        'Catch ex As Exception
        '    Throw New Exception(ex.Message)
        'End Try
        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spEksekusiDelete", params)
            objTrans.Commit()
        Catch exp As Exception
            objTrans.Rollback()
            WriteException("UpdateNDT", "UpdateNDT", exp.Message + exp.StackTrace)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Sub
End Class



