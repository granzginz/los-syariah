﻿
#Region "Imports"

Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

#End Region

Public Class AgreementVirtualAccount : Inherits Maxiloan.SQLEngine.DataAccessBase



    Public Function GetVirtualAccount(strConn As String) As DataSet
        Dim conn As New SqlConnection(strConn)
        Dim ds As New DataSet
        Try
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select VaBankID ID, VaBankInitial Name from tblVirtualAccount")
            Return ds
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Function ToDataTable(data As IList(Of String)) As DataTable
        Dim dt = New DataTable()

        Dim strArray As String() = New String() {"id", "value1", "value2"}

        For Each s In strArray
            dt.Columns.Add(New DataColumn(s))
        Next

        For Each v As String In data
            Dim row = dt.NewRow()
            row("id") = v.Trim()
            row("value1") = ""
            row("value2") = ""
            dt.Rows.Add(row)
        Next
        Return dt

    End Function

    Public Function GenerateVitualAccount(ByVal cnn As String, vabankid As IList(Of String), ByVal oDataTable As DataTable) As IList(Of Parameter.GeneratedVaToExcellLog) 'String
        Dim sqlparams As IList(Of SqlParameter) = New List(Of SqlParameter)
        Dim param As SqlParameter

        Dim tableVaBank = ToDataTable(vabankid)
        sqlparams.Add(New SqlParameter("@vaBankID", SqlDbType.Structured) With {.Value = tableVaBank})
        sqlparams.Add(New SqlParameter("@agreementNo", SqlDbType.Structured) With {.Value = oDataTable})
        param = New SqlParameter("@errMsg", SqlDbType.VarChar, 500)
        param.Direction = ParameterDirection.Output
        sqlparams.Add(param)
        Dim dt As DataTable = SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, "sp_GenerateAgreementVa", sqlparams.ToArray).Tables(0)
        If (param.Value.ToString().Trim() <> "") Then
            Throw New Exception(param.Value.ToString().Trim())
        End If

        'Dim val = dt.AsEnumerable().Select(Function(r) New Parameter.GeneratedVaToExcellLog(
        '                                                                                        r.Field(Of Long)("nomor"),
        '                                                                                        r.Field(Of String)("nama"),
        '                                                                                        r.Field(Of String)("nomorvirtualbca"),
        '                                                                                        r.Field(Of String)("nomorvirtualbri"), r.Field(Of String)("nokontrak"),
        '                                                                                        r.Field(Of Date)("masaberlakudari"), r.Field(Of Date)("masaberlakusampai")))
        Dim val = dt.AsEnumerable().Select(Function(r) New Parameter.GeneratedVaToExcellLog(
                                                                                                r.Field(Of String)("NomorVA"),
                                                                                                r.Field(Of String)("NamaVA"),
                                                                                                r.Field(Of String)("IDVA"),
                                                                                                r.Field(Of String)("Code1"),
                                                                                                r.Field(Of String)("Code2"),
                                                                                                r.Field(Of String)("JenisInstruksi")))
        Return val.ToList()
    End Function

    Public Function GeneratePaymentGatewayFile(cnn As String, processid As String) As Parameter.AngsuranVirtualAccount
        Dim query = " select applicationid, contractnumber, replace(replace(nameofcustomer, '.', ''), ',','') AS nameofcustomer, dateoverdue, periodoftennor, installment, pinalty, vabankid, vabankinitial, vabankname, vabinno, branchid, kodepersh, OverwriteAddRemove , insDueDate from PaymentAgreementAccountLog with (nolock)
                      where id = @processId order by sequenceno "

        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            params.Add(New SqlParameter("@processId", SqlDbType.VarChar, 50) With {.Value = processid})

            Dim sqlConnection1 = New SqlConnection(cnn)
            Dim cmd = New SqlCommand()
            cmd.CommandTimeout = 0
            Dim reader As SqlDataReader

            cmd.CommandText = query
            cmd.CommandType = CommandType.Text
            cmd.Connection = sqlConnection1

            cmd.Parameters.AddRange(params.ToArray)
            sqlConnection1.Open()
            reader = cmd.ExecuteReader()

            Dim dt As DataTable = New DataTable()
            dt.Load(reader)

            sqlConnection1.Close()

            Dim vaAgent = GetVirtualAccountAgents(cnn)

            Dim val = dt.AsEnumerable().Select(Function(r) New Parameter.AngsuranVirtualAccountItem(
                                                   r.Field(Of String)("ApplicationId"),
                                                   r.Field(Of String)("ContractNumber"),
                                                   r.Field(Of String)("NameOfCustomer"),
                                                   r.Field(Of Date)("DateOverDue"),
                                                   r.Field(Of Integer)("PeriodOfTennor"),
                                                   r.Field(Of Decimal)("installment"),
                                                   r.Field(Of Decimal)("Pinalty"),
                                                   r.Field(Of String)("VaBankID"),
                                                   r.Field(Of String)("VaBankInitial"),
                                                   r.Field(Of String)("VaBankName"),
                                                   r.Field(Of String)("VaBinNo"),
                                                   r.Field(Of String)("BranchId"),
                                                   r.Field(Of String)("kodepersh"),
                                                   r.Field(Of String)("OverwriteAddRemove"),
                                                    r.Field(Of Date)("InsDueDate")
                                                   )).ToList()
            Return New Parameter.AngsuranVirtualAccount(val, vaAgent) With {.SettingId = processid}
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function


    Public Sub PaymentGatewayAgreementLog(cnn As String, param As Parameter.AngsuranVirtualAccount)
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try

            params.Add(New SqlParameter("@DateOfUpload", SqlDbType.Date) With {.Value = param.UploadDate})
            params.Add(New SqlParameter("@installmentcount", SqlDbType.Int) With {.Value = param.InstallmentCount})
            params.Add(New SqlParameter("@totalInsallment", SqlDbType.Decimal) With {.Value = param.TotalInstallment})
            params.Add(New SqlParameter("@fileDir", SqlDbType.VarChar, 100) With {.Value = param.FileDirectory})
            params.Add(New SqlParameter("@settingid", SqlDbType.VarChar, 50) With {.Value = param.SettingId})
            params.Add(New SqlParameter("@vaLogDetails", SqlDbType.Structured) With {.Value = param.DetailLogTable})
            Dim pr = New SqlParameter("@errMsg", SqlDbType.VarChar, 500)
            pr.Direction = ParameterDirection.Output
            params.Add(pr)
            SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, "spCreateVirtualAccountInsAgreementLog", params.ToArray)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Public Function GetVirtualAccountInsAgreementLog(cnn As String, processDate As String) As Parameter.VirtualAccountInsAgreementLog
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@settingid", SqlDbType.VarChar, 50) With {.Value = processDate})
        Dim dt As DataTable = SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, "spVirtualAccountInsAgreementLog", params.ToArray).Tables(0)

        If (dt.Rows.Count <= 0) Then
            Return Nothing
        End If

        Dim row = dt.Rows(0)
        Dim val = New Parameter.VirtualAccountInsAgreementLog(CType(row("Id"), Long), CType(row("DateOfUpload"), Date), CType(row("InstallmentCount"), Integer), CType(row("grandtotal"), Decimal), row("FileDirectory").ToString)
        If (val Is Nothing) Then
            Return Nothing
        End If
        val.VaAgreementLogDetails = dt.AsEnumerable.Select(Function(r) New Parameter.VirtualAccountAgreementDetailLog(0L, r.Field(Of String)("VaBankId"), r.Field(Of String)("FileName"), r.Field(Of Integer)("InstallmentCount"), r.Field(Of Decimal)("TotalInstallment"), r.Field(Of String)("VaBankName"))).ToList()
        Return val
    End Function


    Function GetVirtualAccountAgents(cnn As String) As IList(Of Parameter.VirtualAccountAgent)
        Try
            Dim query = "Select VaBankID , VaBankName, VaBankInitial, VaBinNo, VaContact, VaJabatan, VaContactHP, VaEmail, VaSettleBankAccID, VaSettleBankAccName, VaTransactionFee, VaFeeTanggungKonsumen, VaFeeTanggungMF, VaPPNFee, VaPph23Fee, CoaPpn, CoaPph23 from tblVirtualAccount"
            Dim dt As DataTable = SqlHelper.ExecuteDataset(cnn, CommandType.Text, query).Tables(0)
            Dim result = New List(Of Parameter.VirtualAccountAgent)
            Return dt.AsEnumerable().Select(Function(r) New Parameter.VirtualAccountAgent(
                                                r.Field(Of String)("vaBankID"),
                                                r.Field(Of String)("vaBankName"),
                                                r.Field(Of String)("vaBankInitial"),
                                                r.Field(Of String)("vaBinNo"),
                                                r.Field(Of String)("vaContact"),
                                                r.Field(Of String)("vaJabatan"),
                                                r.Field(Of String)("vaContactHP"),
                                                r.Field(Of String)("vaEmail"),
                                                r.Field(Of String)("vaSettleBankAccID"),
                                                r.Field(Of String)("vaSettleBankAccName"),
                                                r.Field(Of Decimal)("vaTransactionFee"),
                                                r.Field(Of Decimal)("vaFeeTanggungKonsumen"),
                                                r.Field(Of Decimal)("vaFeeTanggungMF"),
                                                r.Field(Of Decimal)("vaPPNFee"),
                                                r.Field(Of Decimal)("vaPph23Fee"),
                                                r.Field(Of String)("coaPpn"),
                                                r.Field(Of String)("coaPph23"))
                                            ).ToList()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function VaGenerateSettingLog(cnn As String, processDate() As DateTime) As String()

        Dim query = "Select convert(nvarchar(50), s.id) id,'0' jumlahTagihan, '0' totalTagihan   from  vageneratesetting s  where s.uploaddate = @uploaddate  and s.dendadate = @dendadate and s.id is not null group by s.id"
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        params.Add(New SqlParameter("@uploaddate", SqlDbType.Date) With {.Value = processDate(0)})
        params.Add(New SqlParameter("@dendaDate", SqlDbType.Date) With {.Value = processDate(1)})

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.Text, query, params.ToArray)

        If (reader.HasRows) Then
            If (reader Is Nothing) Then
                Return Nothing
            End If
            If (reader.IsClosed) Then
                Return Nothing
            End If

            Dim _cID = reader.GetOrdinal("id")
            Dim _jumT = reader.GetOrdinal("jumlahTagihan")
            Dim _totalT = reader.GetOrdinal("totalTagihan")

            While (reader.Read())
                Return New String() {IIf(reader.IsDBNull(_cID), String.Empty, reader.GetString(_cID)),
                                         IIf(reader.IsDBNull(_jumT), "0", reader.GetString(_jumT)),
                                         IIf(reader.IsDBNull(_totalT), "0", reader.GetString(_totalT))
                                         }
            End While
        End If
        Return Nothing
    End Function

End Class