﻿Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

Public Class SimulasiPerhitunganPembiayaan : Inherits Maxiloan.SQLEngine.DataAccessBase

    Public Function SimulasiPerhitunganPembiayaanPaging(ByVal oCustomClass As Parameter.SimulasiPerhitunganPembiayaan) As Parameter.SimulasiPerhitunganPembiayaan
        Dim oReturnValue As New Parameter.SimulasiPerhitunganPembiayaan
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spSimulasiPerhitunganPembiayaanPaging", params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GenerateSimulasiPerhitunganPembiayaan(ByVal oCustomClass As Parameter.SimulasiPerhitunganPembiayaan) As Parameter.SimulasiPerhitunganPembiayaan
        Dim oReturnValue As New Parameter.SimulasiPerhitunganPembiayaan
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.CustomerID

        params(1) = New SqlParameter("@AgreementNo", SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.AgreementNo

        params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(2).Value = oCustomClass.BranchId

        Try
            oReturnValue.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spSimulasiPerhitunganPembiayaan", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

        Return oReturnValue
    End Function

    Public Function CetakKartuPiutangList(ByVal oCustomClass As Parameter.CetakKartuPiutang) As Parameter.CetakKartuPiutang
        Dim oReturnValue As New Parameter.CetakKartuPiutang
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter("@AgreementUnitNo", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.AgreementUnitNo



        Try
            oReturnValue.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetDataCetakKartuPiutang", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

        Return oReturnValue
    End Function
End Class
