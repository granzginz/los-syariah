﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="am_login.aspx.vb" Inherits="Maxiloan.Webform.am_login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="icon" type="image/ico" href="favicon.ico"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="Include/LoginV2.css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ValidationSummary ID="VlsLogin" runat="server" ShowMessageBox="True" ShowSummary="False">
    </asp:ValidationSummary>
    <asp:RequiredFieldValidator ID="RfvUserNm" runat="server" ErrorMessage="User Name cannot be blank"
        ControlToValidate="txtUsrNm" Display="None"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RfvPwd" runat="server" ErrorMessage="Password cannot be blank"
        ControlToValidate="txtPwd" Display="None"></asp:RequiredFieldValidator>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <asp:RequiredFieldValidator ID="rfvGroupDB" runat="server" ErrorMessage="Group DB must be fill"
        ControlToValidate="SlcGroupDBID" Display="None"></asp:RequiredFieldValidator>
    <%--    <div id="loginbg">
        <img src="Images/loginbg03.png" alt="loginbg03.png" />        
    </div>--%>
    <div class="content" >
     <div class="logo_container img">
        <img src="Images/BG.png"   />
    </div> 


    <div class="loginbox">
        
        <asp:Panel ID="pnllogin" runat="server">
            <div id="blokLogo">
                <img src="Images/logo SulSelBar.png" alt="" />
            </div>
            <div id="loginContent">
          
            <asp:Panel ID="pnllogininfo" runat="server">
                <label>
                    Sistem
                </label>
                <asp:DropDownList ID="cboApplication" runat="server">
                </asp:DropDownList>
                <label>
                    User
                </label>
                <asp:TextBox ID="txtUsrNm" runat="server"></asp:TextBox>
                <label>
                    Password
                </label>
                <asp:TextBox ID="txtPwd" runat="server" CssClass="textBox" TextMode ="Password" ></asp:TextBox>
                <asp:Button ID="btnLogin1" runat="server" Text="Login" />
            </asp:Panel>
            <asp:Panel ID="pnlgroupdb" runat="server">
                <label>
                    Cabang
                </label>
                <asp:DropDownList ID="SlcGroupDBID" runat="server">
                </asp:DropDownList>
                <asp:Button ID="btnLogin" runat="server" Text="Login" />
            </asp:Panel>
            <asp:Label ID="lblDate" runat="server" cssClass="smallText black"></asp:Label>
            </div>
        </asp:Panel>
    </div>
        </div>
    
    </form>

    
</body>
</html>
