#Region "Imports"
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.SQLEngine.Workflow
#End Region

Public Class ApplicationCreation : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    Private Const spPaging As String = "spNewProspectPaging"
    Private Const spProspectSaveAdd As String = "spProspectSaveAdd"
    Private Const spProspectSaveAsset As String = "spProspectSaveAsset"
    Private Const spProspectSaveDemografi As String = "spProspectSaveDemografi"
    Private Const spProspectSaveFinancial As String = "spProspectSaveFinancial"
    Private Const spProspectView As String = "spProspectView"
    Private Const spFillCbo As String = "spAssetDataFillCbo"
    Private Const spPagingInitial As String = "spNewInitialPaging"
    Private Const spInitialSaveAdd As String = "spInitialSaveAdd"
    Private Const spProspectBIChecking As String = "spProspectBIChecking"
    Private Const spDataSurveySaveAdd As String = "spDataSurveySaveAdd"
    Private Const spProspectReturnUpdate As String = "spProspectReturnUpdate"
    Private Const spProspectSaveLog As String = "spProspectLog"
    Private Const spPagingInqProspect As String = "spInqProspectPaging"
    Private Const spDataSurveySaveEdit As String = "spDataSurveySaveEdit"
#End Region

#Region "Prospect Application"
    Public Function GetProspect(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
        Dim oReturnValue As New Parameter.AppCreation
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spPaging, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Prospect", "GetProspect", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.SalesMarketing.Prospect.GetProspect")
        End Try
    End Function


    Public Function ProspectSaveAdd(ByVal oApplication As Parameter.AppCreation) As Parameter.AppCreation

        Dim oReturnValue As New Parameter.AppCreation
        Dim conn As New SqlConnection(oApplication.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim ProspectAppID As String = ""


        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            transaction = conn.BeginTransaction
            params.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = oApplication.BranchId})
            params.Add(New SqlParameter("@CustomerID", SqlDbType.Char, 20) With {.Value = oApplication.CustomerID})
            params.Add(New SqlParameter("@CustomerType", SqlDbType.Char, 1) With {.Value = oApplication.CustomerType})
            params.Add(New SqlParameter("@ProspectAppDate", SqlDbType.VarChar, 10) With {.Value = oApplication.ProspectAppDate})
            params.Add(New SqlParameter("@Name", SqlDbType.VarChar, 50) With {.Value = oApplication.Name})
            params.Add(New SqlParameter("@BirthPlace", SqlDbType.VarChar, 20) With {.Value = oApplication.BirthPlace})
            params.Add(New SqlParameter("@BirthDate", SqlDbType.VarChar, 10) With {.Value = oApplication.BirthDate})
            params.Add(New SqlParameter("@Address", SqlDbType.VarChar, 100) With {.Value = oApplication.Address})
            params.Add(New SqlParameter("@RT", SqlDbType.Char, 3) With {.Value = oApplication.RT})
            params.Add(New SqlParameter("@RW", SqlDbType.Char, 3) With {.Value = oApplication.RW})
            params.Add(New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30) With {.Value = oApplication.Kelurahan})
            params.Add(New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30) With {.Value = oApplication.Kecamatan})
            params.Add(New SqlParameter("@City", SqlDbType.VarChar, 30) With {.Value = oApplication.City})
            params.Add(New SqlParameter("@ZipCode", SqlDbType.Char, 5) With {.Value = oApplication.ZipCode})
            params.Add(New SqlParameter("@AreaPhone1", SqlDbType.Char, 4) With {.Value = oApplication.AreaPhone1})
            params.Add(New SqlParameter("@Phone1", SqlDbType.Char, 10) With {.Value = oApplication.Phone1})
            params.Add(New SqlParameter("@AreaPhone2", SqlDbType.Char, 4) With {.Value = oApplication.AreaPhone2})
            params.Add(New SqlParameter("@Phone2", SqlDbType.Char, 10) With {.Value = oApplication.Phone2})
            params.Add(New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20) With {.Value = oApplication.MobilePhone})
            params.Add(New SqlParameter("@JarakSurvey", SqlDbType.Int) With {.Value = oApplication.JarakSurvey})
            params.Add(New SqlParameter("@IDType", SqlDbType.VarChar, 10) With {.Value = oApplication.IDType})
            params.Add(New SqlParameter("@IDNumber", SqlDbType.VarChar, 20) With {.Value = oApplication.IDNumber})
            params.Add(New SqlParameter("@IsReferenced", SqlDbType.Bit) With {.Value = oApplication.IsReferenced})
            params.Add(New SqlParameter("@PhoneReference", SqlDbType.VarChar, 20) With {.Value = oApplication.PhoneReference})
            params.Add(New SqlParameter("@StatusNasabah", SqlDbType.VarChar, 1) With {.Value = oApplication.StatusNasabah})
            Dim errprm = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            Dim prmPid = New SqlParameter("@ProspectAppID", SqlDbType.Char, 20) With {.Direction = ParameterDirection.Output}
            params.Add(errprm)
            params.Add(prmPid)
            params.Add(New SqlParameter("@businessdate", SqlDbType.DateTime) With {.Value = oApplication.BusinessDate})
            params.Add(New SqlParameter("@LoginID", SqlDbType.VarChar, 20) With {.Value = oApplication.LoginId})
            params.Add(New SqlParameter("@mode", SqlDbType.VarChar, 5) With {.Value = oApplication.Mode})
            params.Add(New SqlParameter("@OldProspectAppID", SqlDbType.Char, 20) With {.Value = oApplication.ProspectAppID})
            params.Add(New SqlParameter("@AreaFax", SqlDbType.Char, 4) With {.Value = oApplication.AreaFax})
            params.Add(New SqlParameter("@Fax", SqlDbType.Char, 10) With {.Value = oApplication.Fax})


            params.Add(New SqlParameter("@OfficeAreaPhone", SqlDbType.VarChar, 4) With {.Value = oApplication.OfficeAreaPhone})
            params.Add(New SqlParameter("@OfficePhone", SqlDbType.VarChar, 10) With {.Value = oApplication.OfficePhone})
            params.Add(New SqlParameter("@GuarantorAreaPhone", SqlDbType.Char, 4) With {.Value = oApplication.GuarantorAreaPhone})
            params.Add(New SqlParameter("@GuarantorPhone", SqlDbType.Char, 10) With {.Value = oApplication.GuarantorPhone})

            params.Add(New SqlParameter("@KetReference", SqlDbType.VarChar, 20) With {.Value = oApplication.KetReference})


            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spProspectSaveAdd, params.ToArray)

            ErrMessage = CType(errprm.Value, String)
            ProspectAppID = CType(prmPid.Value, String)

            If ErrMessage <> "" Then
                oReturnValue.ApplicationID = ""
                oReturnValue.Err = ErrMessage
                Return oReturnValue
                Exit Function
            End If

            transaction.Commit()
            oReturnValue.ProspectAppID = ProspectAppID

            oReturnValue.Err = ErrMessage
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Prospect", "ProspectSaveAdd", exp.Message + exp.StackTrace)

            oReturnValue.ProspectAppID = ProspectAppID
            oReturnValue.Err = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.SalesMarketing.Prospect.ProspectSaveAdd")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function ProspectSaveAsset(ByVal oApplication As Parameter.AppCreation) As Parameter.AppCreation

        Dim oReturnValue As New Parameter.AppCreation
        Dim conn As New SqlConnection(oApplication.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim ProspectAppID As String = ""

        Dim params() As SqlParameter = New SqlParameter(20) {}

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oApplication.BranchId
            params(1) = New SqlParameter("@ProspectAppID", SqlDbType.Char, 20)
            params(1).Value = oApplication.ProspectAppID
            'params(2) = New SqlParameter("@AssetCode", SqlDbType.Char, 50)
            'params(2).Value = oApplication.AssetCode
            params(2) = New SqlParameter("@UsedNew", SqlDbType.Char, 1)
            params(2).Value = oApplication.UsedNew
            params(3) = New SqlParameter("@ManufacturingYear", SqlDbType.Int)
            params(3).Value = oApplication.ManufacturingYear
            params(4) = New SqlParameter("@AssetUsage", SqlDbType.VarChar, 10)
            params(4).Value = oApplication.AssetUsage
            params(5) = New SqlParameter("@LicensePlate", SqlDbType.VarChar, 50)
            params(5).Value = oApplication.LicensePlate
            params(6) = New SqlParameter("@OTRPrice", SqlDbType.Decimal)
            params(6).Value = oApplication.OTRPrice
            params(7) = New SqlParameter("@DPAmount", SqlDbType.Decimal)
            params(7).Value = oApplication.DPAmount
            params(8) = New SqlParameter("@DPAmountTradeIn", SqlDbType.Decimal)
            params(8).Value = oApplication.DPAmountTradeIn
            params(9) = New SqlParameter("@NTF", SqlDbType.Decimal)
            params(9).Value = oApplication.NTF
            params(10) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
            params(10).Value = oApplication.InstallmentAmount
            params(11) = New SqlParameter("@Tenor", SqlDbType.Int)
            params(11).Value = oApplication.Tenor
            params(12) = New SqlParameter("@WayOfPayment", SqlDbType.VarChar, 2)
            params(12).Value = oApplication.WayOfPayment
            params(13) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(13).Direction = ParameterDirection.Output
            params(14) = New SqlParameter("@businessdate", SqlDbType.DateTime)
            params(14).Value = oApplication.BusinessDate
            params(15) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
            params(15).Value = oApplication.LoginId
            'params(17) = New SqlParameter("@ProductOfferingID", SqlDbType.VarChar, 10)
            'params(17).Value = oApplication.ProductOfferingID
            'params(18) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
            'params(18).Value = oApplication.ProductID
            params(16) = New SqlParameter("@AssetType", SqlDbType.VarChar, 50)
            params(16).Value = oApplication.AssetType
            params(17) = New SqlParameter("@AssetBrand", SqlDbType.VarChar, 50)
            params(17).Value = oApplication.AssetBrand
            params(18) = New SqlParameter("@Package", SqlDbType.Char, 1)
            params(18).Value = oApplication.Package
            'params(19) = New SqlParameter("@ProductOfferingID", SqlDbType.VarChar, 10)
            'params(19).Value = oApplication.ProductOfferingID
            'params(20) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
            'params(20).Value = oApplication.ProductID
            params(19) = New SqlParameter("@Sertifikat", SqlDbType.VarChar, 50)
            params(19).Value = oApplication.Sertifikat
            params(20) = New SqlParameter("@Asset", SqlDbType.Char, 10)
            params(20).Value = oApplication.Asset

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spProspectSaveAsset, params)

            'ErrMessage = CType(params(14).Value, String)
            'If ErrMessage <> "" Then
            '    oReturnValue.ApplicationID = ""
            '    oReturnValue.Err = ErrMessage
            '    Return oReturnValue
            '    Exit Function
            'End If

            transaction.Commit()
            oReturnValue.ProspectAppID = oApplication.ProspectAppID

            oReturnValue.Err = ErrMessage
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Prospect", "ProspectSaveAsset", exp.Message + exp.StackTrace)

            oReturnValue.ProspectAppID = oApplication.ProspectAppID
            oReturnValue.Err = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.SalesMarketing.Prospect.ProspectSaveAsset")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function ProspectSaveDemografi(ByVal oApplication As Parameter.AppCreation) As Parameter.AppCreation

        Dim oReturnValue As New Parameter.AppCreation
        Dim conn As New SqlConnection(oApplication.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim ProspectAppID As String = ""

        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            transaction = conn.BeginTransaction
            params.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = oApplication.BranchId})
            params.Add(New SqlParameter("@ProspectAppID", SqlDbType.Char, 20) With {.Value = oApplication.ProspectAppID})
            params.Add(New SqlParameter("@Gender", SqlDbType.Char, 1) With {.Value = oApplication.Gender})
            params.Add(New SqlParameter("@MaritalStatus", SqlDbType.Char, 10) With {.Value = oApplication.MaritalStatus})
            params.Add(New SqlParameter("@NumOfDependence", SqlDbType.Int) With {.Value = oApplication.NumOfDependence})
            params.Add(New SqlParameter("@Education", SqlDbType.VarChar, 10) With {.Value = oApplication.Education})
            params.Add(New SqlParameter("@HomeStatus", SqlDbType.VarChar, 10) With {.Value = oApplication.HomeStatus})
            params.Add(New SqlParameter("@StaySinceYear", SqlDbType.Int) With {.Value = oApplication.StaySinceYear})
            params.Add(New SqlParameter("@StaySinceMonth", SqlDbType.Int) With {.Value = oApplication.StaySinceMonth})
            params.Add(New SqlParameter("@EmploymentSinceYear", SqlDbType.Int) With {.Value = oApplication.EmploymentSinceYear})
            params.Add(New SqlParameter("@EmploymentSinceMonth", SqlDbType.Int) With {.Value = oApplication.EmploymentSinceMonth})
            params.Add(New SqlParameter("@MonthlyVariableIncome", SqlDbType.Decimal) With {.Value = oApplication.MonthlyVariableIncome})
            params.Add(New SqlParameter("@MonthlyFixedIncome", SqlDbType.Decimal) With {.Value = oApplication.MonthlyFixedIncome})
            params.Add(New SqlParameter("@ProfessionID", SqlDbType.VarChar, 10) With {.Value = oApplication.ProfessionID})
            params.Add(New SqlParameter("@KodeIndustri", SqlDbType.VarChar, 4) With {.Value = oApplication.KodeIndustri})
            params.Add(New SqlParameter("@KodeIndustriDetail", SqlDbType.VarChar, 4) With {.Value = oApplication.KodeIndustriDetail})
            params.Add(New SqlParameter("@ProfessionIDPasangan", SqlDbType.VarChar, 10) With {.Value = oApplication.ProfessionIDPasangan})
            params.Add(New SqlParameter("@KodeIndustriPasangan", SqlDbType.VarChar, 4) With {.Value = oApplication.KodeIndustriPasangan})
            params.Add(New SqlParameter("@KodeIndustriDetailPasangan", SqlDbType.VarChar, 4) With {.Value = oApplication.KodeIndustriDetailPasangan})

            Dim errPrm = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}

            params.Add(New SqlParameter("@businessdate", SqlDbType.DateTime) With {.Value = oApplication.BusinessDate})
            params.Add(New SqlParameter("@LoginID", SqlDbType.VarChar, 20) With {.Value = oApplication.LoginId})


            params.Add(New SqlParameter("@OccupationID", SqlDbType.VarChar, 10) With {.Value = oApplication.Occupation})
            params.Add(New SqlParameter("@NatureOfBusinessID", SqlDbType.VarChar, 10) With {.Value = oApplication.NatureOfBusiness})
            params.Add(New SqlParameter("@OccupationSpouseID", SqlDbType.VarChar, 10) With {.Value = oApplication.OccupationSpouse})
            params.Add(New SqlParameter("@NatureOfBusinessSpouseID", SqlDbType.VarChar, 10) With {.Value = oApplication.NatureOfBusinessSpouse})
            params.Add(errPrm)
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spProspectSaveDemografi, params.ToArray)

            ErrMessage = CType(errPrm.Value, String)
            If ErrMessage <> "" Then
                oReturnValue.ApplicationID = ""
                oReturnValue.Err = ErrMessage
                Return oReturnValue
                Exit Function
            End If

            transaction.Commit()
            oReturnValue.ProspectAppID = oApplication.ProspectAppID

            oReturnValue.Err = ErrMessage
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Prospect", "ProspectSaveDemografi", exp.Message + exp.StackTrace)

            oReturnValue.ProspectAppID = oApplication.ProspectAppID
            oReturnValue.Err = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.SalesMarketing.Prospect.ProspectSaveDemografi")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function ProspectSaveFinancial(ByVal oApplication As Parameter.AppCreation) As Parameter.AppCreation

        Dim oReturnValue As New Parameter.AppCreation
        Dim conn As New SqlConnection(oApplication.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim ProspectAppID As String = ""

        Dim params() As SqlParameter = New SqlParameter(13) {}

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oApplication.BranchId
            params(1) = New SqlParameter("@ProspectAppID", SqlDbType.Char, 20)
            params(1).Value = oApplication.ProspectAppID
            params(2) = New SqlParameter("@RekeningTabungan", SqlDbType.Bit)
            params(2).Value = oApplication.RekeningTabungan
            params(3) = New SqlParameter("@NoRekening", SqlDbType.Char, 50)
            params(3).Value = oApplication.NoRekening
            params(4) = New SqlParameter("@PengalamanKredit", SqlDbType.Bit)
            params(4).Value = oApplication.PengalamanKredit
            params(5) = New SqlParameter("@PengalamanKreditDari", SqlDbType.VarChar, 50)
            params(5).Value = oApplication.PengalamanKreditDari
            params(6) = New SqlParameter("@KepemilikanUnitLain", SqlDbType.Bit)
            params(6).Value = oApplication.KepemilikanUnitLain
            params(7) = New SqlParameter("@KepemilikanUnitLainTahun", SqlDbType.Int)
            params(7).Value = oApplication.KepemilikanUnitLainTahun
            params(8) = New SqlParameter("@KepemilikanUnitLainMerk", SqlDbType.VarChar, 50)
            params(8).Value = oApplication.KepemilikanUnitLainMerk
            params(9) = New SqlParameter("@KepemilikanUnitLainKategori", SqlDbType.VarChar, 50)
            params(9).Value = oApplication.KepemilikanUnitLainKategori
            params(10) = New SqlParameter("@KepemilikanUnitLainPenggunaan", SqlDbType.VarChar, 10)
            params(10).Value = oApplication.KepemilikanUnitLainPenggunaan
            params(11) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(11).Direction = ParameterDirection.Output
            params(12) = New SqlParameter("@businessdate", SqlDbType.DateTime)
            params(12).Value = oApplication.BusinessDate
            params(13) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
            params(13).Value = oApplication.LoginId

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spProspectSaveFinancial, params)

            ErrMessage = CType(params(11).Value, String)
            If ErrMessage <> "" Then
                oReturnValue.ApplicationID = ""
                oReturnValue.Err = ErrMessage
                Return oReturnValue
                Exit Function
            End If

            transaction.Commit()
            oReturnValue.ProspectAppID = oApplication.ProspectAppID

            oReturnValue.Err = ErrMessage
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Prospect", "ProspectSaveFinancial", exp.Message + exp.StackTrace)

            oReturnValue.ProspectAppID = oApplication.ProspectAppID
            oReturnValue.Err = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.SalesMarketing.Prospect.ProspectSaveFinancial")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function ProspectLogSave(ByVal oApplication As Parameter.AppCreation) As Parameter.AppCreation

        Dim oReturnValue As New Parameter.AppCreation
        Dim conn As New SqlConnection(oApplication.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim ProspectAppID As String = ""

        Dim params() As SqlParameter = New SqlParameter(6) {}

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oApplication.BranchId
            params(1) = New SqlParameter("@ProspectAppID", SqlDbType.Char, 20)
            params(1).Value = oApplication.ProspectAppID
            params(2) = New SqlParameter("@ActivityType", SqlDbType.Char, 3)
            params(2).Value = oApplication.ActivityType
            params(3) = New SqlParameter("@ActivityDateStart", SqlDbType.DateTime)
            params(3).Value = oApplication.ActivityDateStart
            params(4) = New SqlParameter("@ActivityDateEnd", SqlDbType.DateTime)
            params(4).Value = oApplication.ActivityDateEnd
            params(5) = New SqlParameter("@ActivityUser", SqlDbType.Char, 20)
            params(5).Value = oApplication.ActivityUser
            params(6) = New SqlParameter("@SequenceNo", SqlDbType.Int)
            params(6).Value = oApplication.ActivitySeqNo

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spProspectSaveLog, params)

            'ErrMessage = CType(params(11).Value, String)
            'If ErrMessage <> "" Then
            '    oReturnValue.ProspectAppID = ""
            '    oReturnValue.Err = ErrMessages
            '    Return oReturnValue
            '    Exit Function
            'End If

            transaction.Commit()
            oReturnValue.ProspectAppID = oApplication.ProspectAppID

            oReturnValue.Err = ErrMessage
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Prospect", "ProspectLogSave", exp.Message + exp.StackTrace)

            oReturnValue.ProspectAppID = oApplication.ProspectAppID
            oReturnValue.Err = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.SalesMarketing.Prospect.ProspectLogSave")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "View Prospect"
    Public Function GetViewProspect(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
        Dim oReturnValue As New Parameter.AppCreation
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ProspectAppID", SqlDbType.Char, 20)
            params(1).Value = oCustomClass.ProspectAppID

            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spProspectView, params).Tables(0)
            oReturnValue.Err = ""
            Return oReturnValue
        Catch exp As Exception
            WriteException("Prospect", "GetViewProspect", exp.Message + exp.StackTrace)
            oReturnValue.Err = "Error: " & exp.Message & ""
            Throw New Exception("Error On DataAccess.SalesMarketing.Prospect.GetViewProspect")
            Return oReturnValue
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
        End Try
    End Function
#End Region

    Public Function GetCbo(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
        Dim oReturnValue As New Parameter.AppCreation
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SalesMarketing.Prospect.GetCbo")
        End Try
    End Function

    Public Function GetCboGeneral(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
        Dim oreturnValue As New Parameter.AppCreation
        Try
            oreturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, oCustomClass.StoreProcedure).Tables(0)
            Return oreturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.SalesMarketing.Prospect.GetCboGeneral")
        End Try
    End Function

    Public Function DoProceeded(cnn As String, prospectAppId As String) As Boolean
        Try
            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
            params.Add(New SqlParameter("@ProspectAppID", SqlDbType.VarChar) With {.Value = prospectAppId})
            Dim errPrm = New SqlParameter("@errMsg", SqlDbType.VarChar, 20) With {.Direction = ParameterDirection.Output}
            params.Add(errPrm)
            SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, "spProspectProceeded", params.ToArray)
            Return CType(errPrm.Value, String) = ""
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Private lObjCommand As SqlCommand
    Private gObjCon As SqlConnection
    Private lObjDataReader As SqlDataReader
    Private lStrResultQuery As Object
    Private comCont As Parameter.ScoreComponentContent

    Function DoProspectDecision(cnn As String, prospectAppId As String, appr As Integer)
        Try
            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
            params.Add(New SqlParameter("@ProspectAppID", SqlDbType.VarChar) With {.Value = prospectAppId})
            params.Add(New SqlParameter("@Appr", SqlDbType.Int) With {.Value = appr})
            SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, "spUpdateDecisionScoring", params.ToArray)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return "OK"
    End Function


    Public Function DoProspectCreaditScoring(cnn As String, branchId As String, prospectAppId As String, loginId As String, bnsDate As Date) As String
        Try

            Dim _scoreResults As Parameter.ScoreResults = New Parameter.ScoreResults
            gObjCon = New SqlConnection(cnn)
            getProspectCreaditScoring(cnn, branchId, prospectAppId, _scoreResults)
            getScoreRejectPolicyScoring(cnn, branchId, prospectAppId, _scoreResults)

            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

            params.Add(New SqlParameter("@BranchId", SqlDbType.VarChar, 3) With {.Value = branchId})
            params.Add(New SqlParameter("@ProspectAppID", SqlDbType.VarChar) With {.Value = prospectAppId})

            params.Add(New SqlParameter("@ScoreSchemeID", SqlDbType.VarChar) With {.Value = _scoreResults.ScoreSchemeID})
            params.Add(New SqlParameter("@ScoreDate", SqlDbType.Date) With {.Value = bnsDate})
            params.Add(New SqlParameter("@CutOffScore", SqlDbType.Int) With {.Value = _scoreResults.CuttOff})
            params.Add(New SqlParameter("@loginId", SqlDbType.VarChar) With {.Value = loginId})

            params.Add(New SqlParameter("@scoreResult", SqlDbType.Structured) With {.Value = _scoreResults.ToDataTable})
            Dim errPrm = New SqlParameter("@errMsg", SqlDbType.VarChar, 500) With {.Direction = ParameterDirection.Output}
            params.Add(errPrm)

            SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, "spScoringDataEntrySave", params.ToArray)

            Return errPrm.Value.ToString

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Sub getProspectCreaditScoring(cnn As String, branchId As String, prospectAppId As String, _scoreResults As Parameter.ScoreResults)

        Dim par As String() = scoreSchemeId(cnn, prospectAppId)
        Dim _ScoreComponentContentList As IList(Of Parameter.ScoreComponentContent) = GetScoreComponentContents(cnn, par(1))
        Dim _ScoreSchemeComponentList As IList(Of Parameter.ScoreSchemeComponent) = GetScoreSchemeComponents(cnn, par(1), par(0))

        Dim _scoreResult As Parameter.ScoreResult

        _scoreResults.SetRejectApproveScore(IIf(par(0).Trim = "P", _ScoreSchemeComponentList(0).PersonalApprovedScore, _ScoreSchemeComponentList(0).CompanyApprovedScore), IIf(par(0).Trim = "P", _ScoreSchemeComponentList(0).PersonalRejectScore, _ScoreSchemeComponentList(0).CompanyRejectScore), _ScoreSchemeComponentList(0).CuttOffScore)
        _scoreResults.ScoreSchemeID = par(1)
        For Each item As Parameter.ScoreSchemeComponent In _ScoreSchemeComponentList

            _scoreResult = New Parameter.ScoreResult(branchId, prospectAppId, item.ScoreComponentID, item.Description, item.Weight, "CRDITSCORE", item.Description)


            lObjCommand = New SqlCommand(item.SQLCmd, gObjCon)

            If gObjCon.State = ConnectionState.Closed Then gObjCon.Open()

            lObjCommand.Parameters.AddWithValue("@BranchID", branchId)
            lObjCommand.Parameters.AddWithValue("@ApplicationID", prospectAppId)

            lObjDataReader = lObjCommand.ExecuteReader()

            lStrResultQuery = "-1"
            If (lObjDataReader.HasRows) Then
                lObjDataReader.Read()
                lStrResultQuery = IIf(IsDBNull(lObjDataReader(0)), "", Trim(lObjDataReader(0).ToString))
            End If


            If IsNumeric(lStrResultQuery) Then
                lStrResultQuery = CDbl(lStrResultQuery)
            End If

            lObjDataReader.Close()

            If item.CalculationType.Trim = "R" Then
                If Not IsNumeric(lStrResultQuery) Then lStrResultQuery = 0
                comCont = _ScoreComponentContentList.Where(Function(x) x.ScoreComponentID = item.ScoreComponentID And x.ValueFrom <= lStrResultQuery And x.ValueTo >= lStrResultQuery).SingleOrDefault()
            Else
                comCont = _ScoreComponentContentList.Where(Function(x) x.ScoreComponentID = item.ScoreComponentID And x.ValueContent = lStrResultQuery.ToString).SingleOrDefault()
            End If

            _scoreResult.ScoreValue = 0
            _scoreResult.ScoreStatus = ""
            _scoreResult.ContentSeqNo = 97
            _scoreResult.QueryResult = lStrResultQuery
            If Not (comCont Is Nothing) Then
                _scoreResult.ScoreStatus = comCont.ScoreStatus.ToString.Trim
                _scoreResult.ScoreValue = comCont.ScoreValue
                _scoreResult.ContentSeqNo = comCont.ContentSeqNo
            End If
            _scoreResults.Add(_scoreResult)
        Next

    End Sub

    Public Sub getScoreRejectPolicyScoring(cnn As String, branchId As String, prospectAppId As String, _RejectPolicyResults As Parameter.ScoreResults)

        Dim _RejectPolicyContentList As IList(Of Parameter.ScoreComponentContent) = GetScoreRejectPolicys(cnn)
        Dim _RejectPolicyComponentList As IList(Of Parameter.ScoreSchemeComponent) = GetScoreRejectPolicyComponents(cnn)

        Dim _RejectPolicyResult As Parameter.ScoreResult

        For Each item As Parameter.ScoreSchemeComponent In _RejectPolicyComponentList

            _RejectPolicyResult = New Parameter.ScoreResult(branchId, prospectAppId, item.ScoreComponentID, item.Description, item.Weight, "REJECTPOLICY", item.Description)

            lObjCommand = New SqlCommand(item.SQLCmd, gObjCon)
            If gObjCon.State = ConnectionState.Closed Then gObjCon.Open()

            lObjCommand.Parameters.AddWithValue("@BranchID", branchId)
            lObjCommand.Parameters.AddWithValue("@ProspectAppID", prospectAppId)

            lObjDataReader = lObjCommand.ExecuteReader()



            lStrResultQuery = "-1"
            If (lObjDataReader.HasRows) Then
                lObjDataReader.Read()
                lStrResultQuery = IIf(IsDBNull(lObjDataReader(0)), "", Trim(lObjDataReader(0).ToString))
            End If

            If IsNumeric(lStrResultQuery) Then
                lStrResultQuery = CDbl(lStrResultQuery)
            End If

            lObjDataReader.Close()

            If item.CalculationType.Trim = "R" Then
                If Not IsNumeric(lStrResultQuery) Then lStrResultQuery = 0
                comCont = _RejectPolicyContentList.Where(Function(x) x.ScoreComponentID = item.ScoreComponentID And x.ValueFrom <= lStrResultQuery And x.ValueTo >= lStrResultQuery).SingleOrDefault()
            Else
                comCont = _RejectPolicyContentList.Where(Function(x) x.ScoreComponentID = item.ScoreComponentID And x.ValueContent = lStrResultQuery.ToString).SingleOrDefault()
            End If

            _RejectPolicyResult.ScoreValue = 0
            _RejectPolicyResult.ScoreStatus = "N"
            _RejectPolicyResult.ContentSeqNo = 0
            _RejectPolicyResult.QueryResult = lStrResultQuery
            If Not (comCont Is Nothing) Then
                _RejectPolicyResult.ScoreStatus = comCont.ScoreStatus.ToString.Trim
                _RejectPolicyResult.ScoreValue = comCont.ScoreValue
            End If
            _RejectPolicyResults.Add(_RejectPolicyResult)
        Next

    End Sub

    Public Function DispositionCreditRpt(cnn As String, whereCond As String(), tp As String) As DataSet
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@prospectAppids", SqlDbType.Structured) With {.Value = ToProspectAppidDataTable(whereCond)})

        Return SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, IIf(tp = "A", "spDispositionCreditRpt", "spScoreCalculationRpt"), params.ToArray)
    End Function

    Function ToProspectAppidDataTable(data As IList(Of String)) As DataTable
        Dim dt = New DataTable()

        Dim strArray As String() = New String() {"id", "value1", "value2"}
        For Each s In strArray
            dt.Columns.Add(New DataColumn(s))
        Next

        For Each v In data
            Dim row = dt.NewRow()
            row("id") = v.Trim()
            row("value1") = ""
            row("value2") = ""
            dt.Rows.Add(row)
        Next
        Return dt
    End Function
    Function scoreSchemeId(ByVal cnn As String, ByVal prospId As String) As String()
        Dim query = "select top 1 p.customertype, isnull(CreditScoreSchemeID,'SC1') scoreSchemeId from prospect p left join   ProductOffering pof on p.productid=pof.productid and p.branchid=pof.branchid  where prospectAppId = @prospectAppId"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@prospectAppId", prospId)})
        Dim ctp As String = ""
        Dim ssId As String = ""
        If (reader.HasRows) Then
            If (reader Is Nothing) Then
                Throw New ArgumentNullException()
            End If
            If (reader.IsClosed) Then
                Throw New InvalidOperationException("This datareader cannot be read from.  It is closed.")
            End If

            Dim _cID = reader.GetOrdinal("customertype")
            Dim _desc = reader.GetOrdinal("scoreSchemeId")

            While (reader.Read())
                ctp = IIf(reader.IsDBNull(_cID), String.Empty, reader.GetString(_cID))
                ssId = IIf(reader.IsDBNull(_desc), "SC1", reader.GetString(_desc))
            End While
        End If

        Dim resul As String() = New String() {ctp, ssId}
        Return resul
    End Function


    Function GetScoreComponentContents(ByVal strCnn As String, ByVal scoreSchemeId As String) As IList(Of Parameter.ScoreComponentContent)
        Dim result As IList(Of Parameter.ScoreComponentContent) = New List(Of Parameter.ScoreComponentContent)
        Dim query = "SELECT  CreditScoreSchemeID,  CreditScoreComponentID, isnull(ValueContent,'-')ValueContent, ValueDescription,  ValueFrom, ValueTo, ScoreValue, ScoreStatus,ReportSegNo  ContentSeqNo, ScoreAbsolute FROM dbo.CreditScoreComponentContent WHERE  CreditScoreSchemeID = @ScoreSchemeID "
        Dim data As DataTable = SqlHelper.ExecuteDataset(strCnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@ScoreSchemeID", scoreSchemeId)}).Tables(0)
        Return (From item As DataRow In data.Rows Select New Parameter.ScoreComponentContent(
                item("CreditScoreSchemeID"),
                item("CreditScoreComponentID"),
                item("ValueContent"),
                item("ValueDescription"),
                item("ValueFrom"),
                item("ValueTo"),
                item("ScoreValue"),
                item("ScoreStatus"),
                item("ContentSeqNo"),
                item("ScoreAbsolute")
                )).ToList()
    End Function


    Function GetScoreSchemeComponents(ByVal strCnn As String, ByVal scoreSchemeId As String, scoringType As String) As IList(Of Parameter.ScoreSchemeComponent)
        Dim query = "select dbo.CreditScoreComponentMaster.Description,dbo.CreditScoreSchemeMaster.CustOffScore, dbo.CreditScoreSchemeMaster.PersonalApprovedScore, dbo.CreditScoreSchemeMaster.PersonalRejectScore, dbo.CreditScoreSchemeMaster.CompanyApprovedScore,  dbo.CreditScoreSchemeMaster.CompanyRejectScore, dbo.CreditScoreSchemeComponent.CreditScoreComponentID, dbo.CreditScoreSchemeComponent.Weight, dbo.CreditScoreComponentMaster.SQLCmd, dbo.CreditScoreComponentMaster.CalculationType FROM dbo.CreditScoreSchemeMaster " &
            "INNER JOIN  dbo.CreditScoreSchemeComponent ON dbo.CreditScoreSchemeMaster.CreditScoreSchemeID = dbo.CreditScoreSchemeComponent.CreditScoreSchemeID  INNER JOIN  dbo.CreditScoreComponentMaster ON  dbo.CreditScoreSchemeComponent.CreditScoreComponentID = dbo.CreditScoreComponentMaster.CreditScoreComponentID WHERE (dbo.CreditScoreSchemeMaster.CreditScoreSchemeID = @ScoreSchemeID ) AND (dbo.CreditScoreSchemeComponent.IsChecked = 1) AND (dbo.CreditScoreComponentMaster.ScoringType = @ScoringType) "

        Dim data As DataTable = SqlHelper.ExecuteDataset(strCnn, CommandType.Text, query, New SqlParameter() {New SqlParameter("@ScoreSchemeID", scoreSchemeId), New SqlParameter("@ScoringType", scoringType)}).Tables(0)
        Return (From item As DataRow In data.Rows Select New Parameter.ScoreSchemeComponent(
                item("Description"),
                  item("CustOffScore"),
                item("PersonalApprovedScore"),
                item("PersonalRejectScore"),
                item("CompanyApprovedScore"),
                item("CompanyRejectScore"),
                item("CreditScoreComponentID"),
                item("Weight"),
                item("SQLCmd"),
                item("CalculationType"))).ToList()
    End Function

    Function GetScoreRejectPolicys(cnn As String) As IList(Of Parameter.ScoreComponentContent)
        Dim query = " select 'SMMOBIL' ScoreSchemeID, item ScoreComponentID , ValueContent, ValueDescription,  ValueFrom, ValueTo, ScoreValue, ScoreStatus, 0 ContentSeqNo, ScoreAbsolute  from ScoreRejectPolicy "
        Dim data As DataTable = SqlHelper.ExecuteDataset(cnn, CommandType.Text, query).Tables(0)
        Return (From item As DataRow In data.Rows Select New Parameter.ScoreComponentContent(
                item("ScoreSchemeID"), item("ScoreComponentID"),
                item("ValueContent"), item("ValueDescription"),
                item("ValueFrom"), item("ValueTo"),
                item("ScoreValue"), item("ScoreStatus"), item("ContentSeqNo"), item("ScoreAbsolute")
                )).ToList()
    End Function

    Function GetScoreRejectPolicyComponents(ByVal strCnn As String) As IList(Of Parameter.ScoreSchemeComponent)
        Dim result As IList(Of Parameter.ScoreSchemeComponent) = New List(Of Parameter.ScoreSchemeComponent)
        Dim query = " select item , itemdescription , calculationtype,sqlcmd from ScoreRejectPolicyMaster  order by id"
        Dim data As DataTable = SqlHelper.ExecuteDataset(strCnn, CommandType.Text, query).Tables(0)
        Return (From item As DataRow In data.Rows Select New Parameter.ScoreSchemeComponent(
               item("itemdescription"),
               0, 0, 0, 0, 0, item("item"), 0,
               item("sqlcmd"),
               item("calculationtype"))).ToList()
    End Function


    Public Function GetProspectScorePolicyResult(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
        Dim oReturnValue As New Parameter.AppCreation
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spScorePolicyResultSumPaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Prospect", "GetProspect", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.SalesMarketing.Prospect.GetProspect")
        End Try
    End Function

    Public Function MasterDataReviewRpt(cnn As String, whereCond As String(), tp As String) As DataSet
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@prospectAppids", SqlDbType.Structured) With {.Value = ToProspectAppidDataTable(whereCond)})

        Return SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, IIf(tp = "C", "spMasterDataReviewFormRpt", "spScoreCalculationRpt"), params.ToArray)
    End Function

    Public Function InitialSaveAdd(ByVal oApplication As Parameter.AppCreation) As Parameter.AppCreation

        Dim oReturnValue As New Parameter.AppCreation
        Dim conn As New SqlConnection(oApplication.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim ProspectAppID As String = ""

        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            transaction = conn.BeginTransaction
            params.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = oApplication.BranchId})
            params.Add(New SqlParameter("@CustomerID", SqlDbType.Char, 20) With {.Value = oApplication.CustomerID})
            params.Add(New SqlParameter("@CustomerType", SqlDbType.Char, 1) With {.Value = oApplication.CustomerType})
            params.Add(New SqlParameter("@ProspectAppDate", SqlDbType.VarChar, 10) With {.Value = oApplication.ProspectAppDate})
            params.Add(New SqlParameter("@Name", SqlDbType.VarChar, 50) With {.Value = oApplication.Name})
            params.Add(New SqlParameter("@BirthPlace", SqlDbType.VarChar, 20) With {.Value = oApplication.BirthPlace})
            params.Add(New SqlParameter("@BirthDate", SqlDbType.VarChar, 10) With {.Value = oApplication.BirthDate})
            params.Add(New SqlParameter("@IDType", SqlDbType.VarChar, 10) With {.Value = oApplication.IDType})
            params.Add(New SqlParameter("@IDNumber", SqlDbType.VarChar, 20) With {.Value = oApplication.IDNumber})
            Dim errprm = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            Dim prmPid = New SqlParameter("@ProspectAppID", SqlDbType.Char, 20) With {.Direction = ParameterDirection.Output}
            params.Add(errprm)
            params.Add(prmPid)
            params.Add(New SqlParameter("@businessdate", SqlDbType.DateTime) With {.Value = oApplication.BusinessDate})
            params.Add(New SqlParameter("@LoginID", SqlDbType.VarChar, 20) With {.Value = oApplication.LoginId})
            params.Add(New SqlParameter("@mode", SqlDbType.VarChar, 5) With {.Value = oApplication.Mode})
            params.Add(New SqlParameter("@OldProspectAppID", SqlDbType.Char, 20) With {.Value = oApplication.ProspectAppID})

            params.Add(New SqlParameter("@OTRPrice", SqlDbType.Decimal) With {.Value = oApplication.OTRPrice})
            params.Add(New SqlParameter("@DPAmount", SqlDbType.Decimal) With {.Value = oApplication.DPAmount})
            params.Add(New SqlParameter("@DPAmountTradeIn", SqlDbType.Decimal) With {.Value = oApplication.DPAmountTradeIn})
            params.Add(New SqlParameter("@NTF", SqlDbType.Decimal) With {.Value = oApplication.NTF})
            params.Add(New SqlParameter("@InstallmentAmount", SqlDbType.Decimal) With {.Value = oApplication.InstallmentAmount})
            params.Add(New SqlParameter("@Tenor", SqlDbType.Int) With {.Value = oApplication.Tenor})
            params.Add(New SqlParameter("@AssetType", SqlDbType.VarChar, 50) With {.Value = oApplication.AssetType})
            params.Add(New SqlParameter("@AssetBrand", SqlDbType.VarChar, 50) With {.Value = oApplication.AssetBrand})
            params.Add(New SqlParameter("@ProspectSource", SqlDbType.Char, 10) With {.Value = oApplication.ProspectSource})
            params.Add(New SqlParameter("@SupplierID", SqlDbType.Char, 10) With {.Value = oApplication.SupplierID})
            params.Add(New SqlParameter("@IsRelatedBNI", SqlDbType.Bit) With {.Value = oApplication.IsRelatedBNI})
            params.Add(New SqlParameter("@FlatRate", SqlDbType.Decimal) With {.Value = oApplication.FlatRate})
            params.Add(New SqlParameter("@EfektifRate", SqlDbType.Decimal) With {.Value = oApplication.EfektifRate})
            params.Add(New SqlParameter("@TotalBunga", SqlDbType.Decimal) With {.Value = oApplication.TotalBunga})

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spInitialSaveAdd, params.ToArray)

            ErrMessage = CType(errprm.Value, String)
            ProspectAppID = CType(prmPid.Value, String)

            If ErrMessage <> "" Then
                oReturnValue.ApplicationID = ""
                oReturnValue.Err = ErrMessage
                Return oReturnValue
                Exit Function
            End If

            transaction.Commit()
            oReturnValue.ProspectAppID = ProspectAppID

            oReturnValue.Err = ErrMessage
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Initial", "InitialSaveAdd", exp.Message + exp.StackTrace)

            oReturnValue.ProspectAppID = ProspectAppID
            oReturnValue.Err = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.SalesMarketing.Initial.InitialSaveAdd")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function GetInitial(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
        Dim oReturnValue As New Parameter.AppCreation
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spPagingInitial, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Initial", "GetInitial", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.SalesMarketing.Initial.GetInitial")
        End Try
    End Function

    Public Function DoBIChecking(ByVal oApplication As Parameter.AppCreation) As Parameter.AppCreation
        Dim conn As New SqlConnection(oApplication.strConnection)
        Dim oReturnValue As New Parameter.AppCreation
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim ProspectAppID As String = ""

        Try
            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
            If conn.State = ConnectionState.Closed Then conn.Open()

            transaction = conn.BeginTransaction

            params.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = oApplication.BranchId})
            params.Add(New SqlParameter("@ProspectAppID", SqlDbType.Char, 20) With {.Value = oApplication.ProspectAppID})
            params.Add(New SqlParameter("@BICheckingStatus", SqlDbType.Int) With {.Value = oApplication.BICheckingStatus})
            params.Add(New SqlParameter("@ActivityDateStart", SqlDbType.DateTime) With {.Value = oApplication.ActivityDateStart})
            params.Add(New SqlParameter("@ActivityDateEnd", SqlDbType.DateTime) With {.Value = oApplication.ActivityDateEnd})
            params.Add(New SqlParameter("@ActivityUser", SqlDbType.Char, 20) With {.Value = oApplication.ActivityUser})
            params.Add(New SqlParameter("@ActivitySeqNo", SqlDbType.Int) With {.Value = oApplication.ActivitySeqNo})
            Dim errPrm = New SqlParameter("@errMsg", SqlDbType.Char, 20) With {.Direction = ParameterDirection.Output}
            params.Add(errPrm)
            params.Add(New SqlParameter("@SIDNotes", SqlDbType.Text) With {.Value = oApplication.SIDNotes})
            params.Add(New SqlParameter("@HubBankLeasing", SqlDbType.Text) With {.Value = oApplication.HubBankLeasing})

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spProspectBIChecking, params.ToArray)
            ErrMessage = CType(errPrm.Value, String).Trim

            If ErrMessage <> "" Then
                oReturnValue.Err = ErrMessage
                Return oReturnValue
                Exit Function
            End If
            transaction.Commit()
            oReturnValue.ProspectAppID = ProspectAppID

            oReturnValue.Err = ErrMessage
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Initial", "BIChecking", exp.Message + exp.StackTrace)

            oReturnValue.ProspectAppID = ProspectAppID
            oReturnValue.Err = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.SalesMarketing.Initial.BIChecking")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function DataSurveySaveAdd(ByVal oApplication As Parameter.AppCreation) As Parameter.AppCreation

        Dim oReturnValue As New Parameter.AppCreation
        Dim conn As New SqlConnection(oApplication.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim ProspectAppID As String = ""


        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            transaction = conn.BeginTransaction
            params.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = oApplication.BranchId})
            params.Add(New SqlParameter("@CustomerID", SqlDbType.Char, 20) With {.Value = oApplication.CustomerID})
            params.Add(New SqlParameter("@ProspectAppDate", SqlDbType.VarChar, 10) With {.Value = oApplication.ProspectAppDate})
            params.Add(New SqlParameter("@Name", SqlDbType.VarChar, 50) With {.Value = oApplication.Name})
            params.Add(New SqlParameter("@Address", SqlDbType.VarChar, 100) With {.Value = oApplication.Address})
            params.Add(New SqlParameter("@RT", SqlDbType.Char, 3) With {.Value = oApplication.RT})
            params.Add(New SqlParameter("@RW", SqlDbType.Char, 3) With {.Value = oApplication.RW})
            params.Add(New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30) With {.Value = oApplication.Kelurahan})
            params.Add(New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30) With {.Value = oApplication.Kecamatan})
            params.Add(New SqlParameter("@City", SqlDbType.VarChar, 30) With {.Value = oApplication.City})
            params.Add(New SqlParameter("@ZipCode", SqlDbType.Char, 5) With {.Value = oApplication.ZipCode})
            params.Add(New SqlParameter("@AreaPhone1", SqlDbType.Char, 4) With {.Value = oApplication.AreaPhone1})
            params.Add(New SqlParameter("@Phone1", SqlDbType.Char, 10) With {.Value = oApplication.Phone1})
            params.Add(New SqlParameter("@AreaPhone2", SqlDbType.Char, 4) With {.Value = oApplication.AreaPhone2})
            params.Add(New SqlParameter("@Phone2", SqlDbType.Char, 10) With {.Value = oApplication.Phone2})
            params.Add(New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20) With {.Value = oApplication.MobilePhone})
            Dim errprm = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            Dim prmPid = New SqlParameter("@ProspectAppID", SqlDbType.Char, 20) With {.Direction = ParameterDirection.Output}
            params.Add(errprm)
            params.Add(prmPid)
            params.Add(New SqlParameter("@businessdate", SqlDbType.DateTime) With {.Value = oApplication.BusinessDate})
            params.Add(New SqlParameter("@LoginID", SqlDbType.VarChar, 20) With {.Value = oApplication.LoginId})
            params.Add(New SqlParameter("@mode", SqlDbType.VarChar, 5) With {.Value = oApplication.Mode})
            params.Add(New SqlParameter("@OldProspectAppID", SqlDbType.Char, 20) With {.Value = oApplication.ProspectAppID})
            params.Add(New SqlParameter("@AreaFax", SqlDbType.Char, 4) With {.Value = oApplication.AreaFax})
            params.Add(New SqlParameter("@Fax", SqlDbType.Char, 10) With {.Value = oApplication.Fax})
            params.Add(New SqlParameter("@AOID", SqlDbType.VarChar, 20) With {.Value = oApplication.AOID})
            params.Add(New SqlParameter("@Provinsi", SqlDbType.VarChar, 50) With {.Value = oApplication.Provinsi})
            params.Add(New SqlParameter("@NoRumah", SqlDbType.VarChar, 50) With {.Value = oApplication.NoRumah})
            params.Add(New SqlParameter("@StreetName", SqlDbType.VarChar, 150) With {.Value = oApplication.StreetName})
            params.Add(New SqlParameter("@Country", SqlDbType.VarChar, 50) With {.Value = oApplication.Country})
            'params.Add(New SqlParameter("@SupplierID", SqlDbType.Char, 10) With {.Value = oApplication.SupplierID})
            params.Add(New SqlParameter("@ActivityDateStart", SqlDbType.DateTime) With {.Value = oApplication.ActivityDateStart})
            params.Add(New SqlParameter("@ActivityDateEnd", SqlDbType.DateTime) With {.Value = oApplication.ActivityDateEnd})
            params.Add(New SqlParameter("@ActivityUser", SqlDbType.Char, 20) With {.Value = oApplication.ActivityUser})
            params.Add(New SqlParameter("@ActivitySeqNo", SqlDbType.Int) With {.Value = oApplication.ActivitySeqNo})
            params.Add(New SqlParameter("@MotherName", SqlDbType.VarChar, 50) With {.Value = oApplication.MotherName})
            params.Add(New SqlParameter("@AppMgr", SqlDbType.Char, 20) With {.Value = getReferenceDBName()})

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spDataSurveySaveAdd, params.ToArray)

            ErrMessage = CType(errprm.Value, String)
            ProspectAppID = CType(prmPid.Value, String)

            If ErrMessage <> "" Then
                oReturnValue.ApplicationID = ""
                oReturnValue.Err = ErrMessage
                Return oReturnValue
                Exit Function
            End If

            transaction.Commit()
            oReturnValue.ProspectAppID = ProspectAppID

            oReturnValue.Err = ErrMessage
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Initial", "DataSurveySaveAdd", exp.Message + exp.StackTrace)

            oReturnValue.ProspectAppID = ProspectAppID
            oReturnValue.Err = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.SalesMarketing.Initial.DataSurveySaveAdd")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function DataSurveySaveEdit(ByVal oApplication As Parameter.AppCreation) As Parameter.AppCreation

        Dim oReturnValue As New Parameter.AppCreation
        Dim conn As New SqlConnection(oApplication.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim ProspectAppID As String = ""


        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            transaction = conn.BeginTransaction
            params.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = oApplication.BranchId})
            params.Add(New SqlParameter("@CustomerID", SqlDbType.Char, 20) With {.Value = oApplication.CustomerID})
            params.Add(New SqlParameter("@ProspectAppDate", SqlDbType.VarChar, 10) With {.Value = oApplication.ProspectAppDate})
            params.Add(New SqlParameter("@Name", SqlDbType.VarChar, 50) With {.Value = oApplication.Name})
            params.Add(New SqlParameter("@Address", SqlDbType.VarChar, 100) With {.Value = oApplication.Address})
            params.Add(New SqlParameter("@RT", SqlDbType.Char, 3) With {.Value = oApplication.RT})
            params.Add(New SqlParameter("@RW", SqlDbType.Char, 3) With {.Value = oApplication.RW})
            params.Add(New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30) With {.Value = oApplication.Kelurahan})
            params.Add(New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30) With {.Value = oApplication.Kecamatan})
            params.Add(New SqlParameter("@City", SqlDbType.VarChar, 30) With {.Value = oApplication.City})
            params.Add(New SqlParameter("@ZipCode", SqlDbType.Char, 5) With {.Value = oApplication.ZipCode})
            params.Add(New SqlParameter("@AreaPhone1", SqlDbType.Char, 4) With {.Value = oApplication.AreaPhone1})
            params.Add(New SqlParameter("@Phone1", SqlDbType.Char, 10) With {.Value = oApplication.Phone1})
            params.Add(New SqlParameter("@AreaPhone2", SqlDbType.Char, 4) With {.Value = oApplication.AreaPhone2})
            params.Add(New SqlParameter("@Phone2", SqlDbType.Char, 10) With {.Value = oApplication.Phone2})
            params.Add(New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20) With {.Value = oApplication.MobilePhone})
            Dim errprm = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            Dim prmPid = New SqlParameter("@ProspectAppID", SqlDbType.Char, 20) With {.Direction = ParameterDirection.Output}
            params.Add(errprm)
            params.Add(prmPid)
            params.Add(New SqlParameter("@businessdate", SqlDbType.DateTime) With {.Value = oApplication.BusinessDate})
            params.Add(New SqlParameter("@LoginID", SqlDbType.VarChar, 20) With {.Value = oApplication.LoginId})
            params.Add(New SqlParameter("@mode", SqlDbType.VarChar, 5) With {.Value = oApplication.Mode})
            params.Add(New SqlParameter("@OldProspectAppID", SqlDbType.Char, 20) With {.Value = oApplication.ProspectAppID})
            params.Add(New SqlParameter("@AreaFax", SqlDbType.Char, 4) With {.Value = oApplication.AreaFax})
            params.Add(New SqlParameter("@Fax", SqlDbType.Char, 10) With {.Value = oApplication.Fax})
            params.Add(New SqlParameter("@AOID", SqlDbType.VarChar, 20) With {.Value = oApplication.AOID})
            params.Add(New SqlParameter("@OldAOID", SqlDbType.VarChar, 20) With {.Value = oApplication.OldAOID})
            params.Add(New SqlParameter("@Provinsi", SqlDbType.VarChar, 50) With {.Value = oApplication.Provinsi})
            params.Add(New SqlParameter("@NoRumah", SqlDbType.VarChar, 50) With {.Value = oApplication.NoRumah})
            params.Add(New SqlParameter("@StreetName", SqlDbType.VarChar, 150) With {.Value = oApplication.StreetName})
            params.Add(New SqlParameter("@Country", SqlDbType.VarChar, 50) With {.Value = oApplication.Country})
            'params.Add(New SqlParameter("@SupplierID", SqlDbType.Char, 10) With {.Value = oApplication.SupplierID})
            params.Add(New SqlParameter("@ActivityDateStart", SqlDbType.DateTime) With {.Value = oApplication.ActivityDateStart})
            params.Add(New SqlParameter("@ActivityDateEnd", SqlDbType.DateTime) With {.Value = oApplication.ActivityDateEnd})
            params.Add(New SqlParameter("@ActivityUser", SqlDbType.Char, 20) With {.Value = oApplication.ActivityUser})
            params.Add(New SqlParameter("@ActivitySeqNo", SqlDbType.Int) With {.Value = oApplication.ActivitySeqNo})
            params.Add(New SqlParameter("@MotherName", SqlDbType.VarChar, 50) With {.Value = oApplication.MotherName})
            params.Add(New SqlParameter("@AppMgr", SqlDbType.Char, 20) With {.Value = getReferenceDBName()})

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spDataSurveySaveEdit, params.ToArray)

            ErrMessage = CType(errprm.Value, String)
            ProspectAppID = CType(prmPid.Value, String)

            If ErrMessage <> "" Then
                oReturnValue.ApplicationID = ""
                oReturnValue.Err = ErrMessage
                Return oReturnValue
                Exit Function
            End If

            transaction.Commit()
            oReturnValue.ProspectAppID = ProspectAppID

            oReturnValue.Err = ErrMessage
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Initial", "DataSurveySaveEdit", exp.Message + exp.StackTrace)

            oReturnValue.ProspectAppID = ProspectAppID
            oReturnValue.Err = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.SalesMarketing.Initial.DataSurveySaveEdit")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function ProspectReturnUpdate(ByVal oCustomClass As Parameter.AppCreation) As String
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction
        Dim oReturnValue As String
        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        Try
            Dim par() As SqlParameter = New SqlParameter(4) {}

            par(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par(0).Value = oCustomClass.BranchId
            par(1) = New SqlParameter("@ProspectAppID", SqlDbType.Char, 20)
            par(1).Value = oCustomClass.ProspectAppID
            par(2) = New SqlParameter("@Notes", SqlDbType.Text)
            par(2).Value = oCustomClass.Notes
            par(3) = New SqlParameter("@HubBankLeasing", SqlDbType.Text)
            par(3).Value = oCustomClass.HubBankLeasing
            par(4) = New SqlParameter("@errMsg", SqlDbType.Char, 20)
            par(4).Direction = ParameterDirection.Output

            oReturnValue = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, spProspectReturnUpdate, par)

            oTransaction.Commit()
            Return oReturnValue
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetInqProspect(ByVal oCustomClass As Parameter.AppCreation) As Parameter.AppCreation
        Dim oReturnValue As New Parameter.AppCreation
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spPagingInqProspect, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("InquiryProspect", "GetInqProspect", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.SalesMarketing.Inquiry.GetInqProspect")
        End Try
    End Function

    Public Function DoProspectCreaditScoringProceed(cnn As String, branchId As String, prospectAppId As String, loginId As String, bnsDate As Date, proceed As Boolean) As String
        Try

            Dim _scoreResults As Parameter.ScoreResults = New Parameter.ScoreResults
            gObjCon = New SqlConnection(cnn)
            getProspectCreaditScoring(cnn, branchId, prospectAppId, _scoreResults)
            getScoreRejectPolicyScoring(cnn, branchId, prospectAppId, _scoreResults)

            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

            params.Add(New SqlParameter("@BranchId", SqlDbType.VarChar, 3) With {.Value = branchId})
            params.Add(New SqlParameter("@ProspectAppID", SqlDbType.VarChar) With {.Value = prospectAppId})

            params.Add(New SqlParameter("@ScoreSchemeID", SqlDbType.VarChar) With {.Value = _scoreResults.ScoreSchemeID})
            params.Add(New SqlParameter("@ScoreDate", SqlDbType.Date) With {.Value = bnsDate})
            params.Add(New SqlParameter("@CutOffScore", SqlDbType.Int) With {.Value = _scoreResults.CuttOff})
            params.Add(New SqlParameter("@loginId", SqlDbType.VarChar) With {.Value = loginId})

            params.Add(New SqlParameter("@proceed", SqlDbType.Bit) With {.Value = proceed})

            params.Add(New SqlParameter("@scoreResult", SqlDbType.Structured) With {.Value = _scoreResults.ToDataTable})
            Dim errPrm = New SqlParameter("@errMsg", SqlDbType.VarChar, 500) With {.Direction = ParameterDirection.Output}
            params.Add(errPrm)

            SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, "spScoringDataEntrySaveProceed", params.ToArray)

            Return errPrm.Value.ToString

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetApprovalprospect(ByVal oCustomClass As Parameter.AppCreation, ByVal strApproval As String)
        'Dim oApprovalID As New SQLEngine.Workflow.Approval
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim transaction As SqlTransaction = Nothing

        Dim par() As SqlParameter = New SqlParameter(1) {}

        par(0) = New SqlParameter("@ProspectAppID", SqlDbType.Char, 100)
        par(0).Value = oCustomClass.ProspectAppID
        par(1) = New SqlParameter("@Approval", SqlDbType.Char, 3)
        par(1).Value = strApproval

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            If (strApproval.Trim = "R") Then
                oCustomClass.Approval.ApprovalTransaction = transaction
                'oClass.Approval.TransactionNo = CStr(params(2).Value)
                'Dim strApprovalNo = oApprovalID.RequestForApproval(oCustomClass.Approval)
            End If

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spRequestApprovalProspect", par)
            transaction.Commit()

        Catch ex As Exception
            transaction.Rollback()
            Throw New Exception(ex.Message)

        Finally
            If conn.State = ConnectionState.Open Then conn.Dispose()
            conn.Dispose()

        End Try
    End Function
End Class


