

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class PO : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spPOPaging"
    Private Const LIST_SAVE As String = "spPO_Save"
    Private Const LIST_KEPADA As String = "spPO_Kepada"
    Private Const LIST_DIKIRIMKE As String = "spPO_DikirimKe"
    Private Const LIST_ITEMFEE As String = "spPO_ItemFee"
    Private Const LIST_SUPPLIERACCOUNT As String = "spCbSupplierAccount"
    Private Const LIST_PONUMBER As String = "spGetNoTransaction"
    Private Const LIST_POSAVE2 As String = "spApplicationSaveEdit2"
    Private Const LIST_POSAVE3 As String = "spApplicationSaveEdit3"
    Private Const LIST_SUPPLIER As String = "spPOSUPPLIER"
    'modify by amri 9 Feb 2018
    Private Const LIST_IDKURS As String = "SP_GetIDKurs"
#End Region

#Region "POPaging"
    Public Function POPaging(ByVal customClass As Parameter.PO) As Parameter.PO
        Dim oReturnValue As New Parameter.PO

        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(4).Value = customClass.BranchId
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(5).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region

#Region "POSave"
    'Public Function POSave(ByVal customClass As Parameter.PO, ByVal oData1 As DataTable, ByVal oData2 As DataTable) As String
    '    Dim oReturnValue As New Parameter.PO
    '    Dim conn As New SqlConnection(customClass.strConnection)

    '    If conn.State = ConnectionState.Closed Then conn.Open()

    '    Dim transaction As SqlTransaction = conn.BeginTransaction
    '    Dim ErrMessage As String = ""
    '    Dim intLoop As Integer

    '    Dim params() As SqlParameter = New SqlParameter(14) {}
    '    Dim params1() As SqlParameter = New SqlParameter(8) {}
    '    Dim params2() As SqlParameter = New SqlParameter(7) {}

    '    Try
    '        params(0) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
    '        params(0).Value = customClass.BusinessDate
    '        params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
    '        params(1).Value = customClass.BranchId
    '        params(2) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
    '        params(2).Value = customClass.ApplicationID
    '        params(3) = New SqlParameter("@PONo", SqlDbType.VarChar, 50)
    '        params(3).Value = customClass.PONumber
    '        params(4) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
    '        params(4).Value = customClass.AgreementNo
    '        params(5) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
    '        params(5).Value = customClass.SupplierID
    '        params(6) = New SqlParameter("@SupplierIDKaroseri", SqlDbType.Char, 10)
    '        params(6).Value = customClass.SupplierIDKaroseri
    '        params(7) = New SqlParameter("@SplitPembayaran", SqlDbType.Bit)
    '        params(7).Value = customClass.SplitPembayaran
    '        params(8) = New SqlParameter("@TotalOTR", SqlDbType.Decimal)
    '        params(8).Value = customClass.TotalOTR
    '        params(9) = New SqlParameter("@HargaKaroseri", SqlDbType.Decimal)
    '        params(9).Value = customClass.HargaKaroseri
    '        params(10) = New SqlParameter("@Notes", SqlDbType.VarChar, 255)
    '        params(10).Value = customClass.Notes
    '        params(11) = New SqlParameter("@NotesKaroseri", SqlDbType.VarChar, 255)
    '        params(11).Value = customClass.NotesKaroseri
    '        params(12) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
    '        params(12).Value = customClass.ProductID
    '        params(13) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 10)
    '        params(13).Value = customClass.ProductOfferingID
    '        params(14) = New SqlParameter("@Err", SqlDbType.VarChar, 8000)
    '        params(14).Direction = ParameterDirection.Output

    '        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, LIST_SAVE, params)
    '        ErrMessage = CType(params(14).Value, String)

    '        If ErrMessage <> "" Then
    '            Throw New Exception(ErrMessage)
    '        End If

    '        If oData1.Rows.Count > 0 Then
    '            params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
    '            params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
    '            params1(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
    '            params1(3) = New SqlParameter("@PriorTo", SqlDbType.Char, 3)
    '            params1(4) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
    '            params1(5) = New SqlParameter("@IsMandatory", SqlDbType.Char, 1)
    '            params1(6) = New SqlParameter("@PromiseDate", SqlDbType.VarChar, 8)
    '            params1(7) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
    '            params1(8) = New SqlParameter("@Notes", SqlDbType.Text)

    '            For intLoop = 0 To oData1.Rows.Count - 1
    '                params1(0).Value = customClass.BranchId
    '                params1(1).Value = customClass.ApplicationID
    '                params1(2).Value = oData1.Rows(intLoop).Item("MasterTCID")
    '                params1(3).Value = oData1.Rows(intLoop).Item("PriorTo")
    '                params1(4).Value = oData1.Rows(intLoop).Item("IsChecked")
    '                params1(5).Value = oData1.Rows(intLoop).Item("IsMandatory")
    '                params1(6).Value = oData1.Rows(intLoop).Item("PromiseDate")
    '                params1(7).Value = customClass.BusinessDate
    '                params1(8).Value = oData1.Rows(intLoop).Item("Notes")

    '                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, LIST_POSAVE2, params1)
    '            Next
    '        End If

    '        If oData2.Rows.Count > 0 Then
    '            params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
    '            params2(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
    '            params2(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
    '            params2(3) = New SqlParameter("@AGTCCLSequenceNo", SqlDbType.SmallInt)
    '            params2(4) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
    '            params2(5) = New SqlParameter("@PromiseDate", SqlDbType.VarChar, 8)
    '            params2(6) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
    '            params2(7) = New SqlParameter("@Notes", SqlDbType.Text)

    '            For intLoop = 0 To oData2.Rows.Count - 1
    '                params2(0).Value = customClass.BranchId
    '                params2(1).Value = customClass.ApplicationID
    '                params2(2).Value = oData2.Rows(intLoop).Item("MasterTCID")
    '                params2(3).Value = CInt(oData2.Rows(intLoop).Item("AGTCCLSequenceNo"))
    '                params2(4).Value = oData2.Rows(intLoop).Item("IsChecked")
    '                params2(5).Value = oData2.Rows(intLoop).Item("PromiseDate")
    '                params2(6).Value = customClass.BusinessDate
    '                params2(7).Value = oData2.Rows(intLoop).Item("Notes")

    '                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, LIST_POSAVE3, params2)
    '            Next
    '        End If
    '        transaction.Commit()
    '        Return ErrMessage
    '    Catch exp As Exception
    '        transaction.Rollback()
    '        Throw New Exception(exp.Message)
    '        Return ErrMessage
    '    Finally
    '        If conn.State = ConnectionState.Open Then conn.Close()
    '        conn.Dispose()
    '    End Try
    'End Function
#End Region
#Region "Save"
    Public Function POSave(ByVal customClass As Parameter.PO) As String
        Dim oReturnValue As New Parameter.PO
        Dim conn As New SqlConnection(customClass.strConnection)

        If conn.State = ConnectionState.Closed Then conn.Open()

        ' Dalam Store Procedure sudah ada transaction rollback
        ' Dim transaction As SqlTransaction = conn.BeginTransaction
        Dim ErrMessage As String = ""

        Dim params() As SqlParameter = New SqlParameter(18) {}
        Dim params1() As SqlParameter = New SqlParameter(8) {}
        Dim params2() As SqlParameter = New SqlParameter(7) {}

        Try
            params(0) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(0).Value = customClass.BusinessDate
            params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(1).Value = customClass.BranchId
            params(2) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(2).Value = customClass.ApplicationID
            params(3) = New SqlParameter("@PONo", SqlDbType.VarChar, 50)
            params(3).Value = customClass.PONumber
            params(4) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
            params(4).Value = customClass.AgreementNo
            params(5) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(5).Value = customClass.SupplierID
            params(6) = New SqlParameter("@SupplierIDKaroseri", SqlDbType.Char, 10)
            params(6).Value = customClass.SupplierIDKaroseri
            params(7) = New SqlParameter("@SplitPembayaran", SqlDbType.Bit)
            params(7).Value = customClass.SplitPembayaran
            params(8) = New SqlParameter("@TotalOTR", SqlDbType.Decimal)
            params(8).Value = customClass.TotalOTR
            params(9) = New SqlParameter("@HargaKaroseri", SqlDbType.Decimal)
            params(9).Value = customClass.HargaKaroseri
            params(10) = New SqlParameter("@Notes", SqlDbType.VarChar, 255)
            params(10).Value = customClass.Notes
            params(11) = New SqlParameter("@NotesKaroseri", SqlDbType.VarChar, 255)
            params(11).Value = customClass.NotesKaroseri
            params(12) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
            params(12).Value = customClass.ProductID
            params(13) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 10)
            params(13).Value = customClass.ProductOfferingID
            'modify by amri 26/01/2018
            params(14) = New SqlParameter("@PengurangPO", SqlDbType.Decimal)
            params(14).Value = customClass.PengurangPO
            params(15) = New SqlParameter("@DescPengurangPO", SqlDbType.Char, 100)
            params(15).Value = customClass.DescriptionPengurangPO
            params(16) = New SqlParameter("IDMataUang", SqlDbType.Char, 3)
            params(16).Value = customClass.IDKurs
            params(17) = New SqlParameter("NilaiKurs", SqlDbType.Decimal)
            params(17).Value = customClass.NilaiKurs
            params(18) = New SqlParameter("@Err", SqlDbType.VarChar, 8000)
            params(18).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, LIST_SAVE, params)
            ErrMessage = CType(params(18).Value, String)

            If ErrMessage <> "" Then
                Throw New Exception(ErrMessage)
            Else
                Dim param3() As SqlParameter = New SqlParameter(1) {}

                param3(0) = New SqlParameter("@SyaratPO", SqlDbType.Structured)
                param3(0).Value = customClass.SyaratPO
                param3(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                param3(1).Value = customClass.ApplicationID

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "spSaveSyaratPO", param3)
            End If


            'transaction.Commit()
            Return ErrMessage
        Catch exp As Exception
            ' transaction.Rollback()
            Throw New Exception(exp.Message)
            Return ErrMessage
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "PO_Kepada"
    Public Function PO_Kepada(ByVal customClass As Parameter.PO) As Parameter.PO
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
        params(0).Value = customClass.SupplierID

        Try
            Dim oReturnValue As New Parameter.PO
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_KEPADA, params)
            If reader.Read Then
                oReturnValue.SupplierName = reader("SupplierName").ToString
                oReturnValue.SupplierAddress = reader("SupplierAddress").ToString
                oReturnValue.SupplierRTRW = reader("SupplierRTRW").ToString
                oReturnValue.SupplierKecamatan = reader("SupplierKecamatan").ToString
                oReturnValue.SupplierKelurahan = reader("SupplierKelurahan").ToString
                oReturnValue.SupplierCity = reader("SupplierCity").ToString
                oReturnValue.SupplierZipCode = reader("SupplierZipCode").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch exp As Exception
            WriteException("PO", "PO_Kepada", exp.Message + exp.StackTrace)
            Return Nothing
        End Try
    End Function
#End Region

#Region "PO_DikirimKe"
    Public Function PO_DikirimKe(ByVal customClass As Parameter.PO) As Parameter.PO
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customClass.ApplicationID

        Try
            Dim oReturnValue As New Parameter.PO
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_DIKIRIMKE, params)
            If reader.Read Then
                oReturnValue.CustomerName = reader("CustomerName").ToString
                oReturnValue.CustomerAddress = reader("CustomerAddress").ToString
                oReturnValue.CustomerKecamatan = reader("CustomerKecamatan").ToString
                oReturnValue.CustomerKelurahan = reader("CustomerKelurahan").ToString
                oReturnValue.CustomerCity = reader("CustomerCity").ToString
                oReturnValue.CustomerZipCode = reader("CustomerZipCode").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch exp As Exception
            WriteException("PO", "PO_DikirimKe", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "PO_ItemFee"
    Public Function PO_ItemFee(ByVal customClass As Parameter.PO) As Parameter.PO
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = customClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(1).Value = customClass.BranchId

        Try
            Dim oReturnValue As New Parameter.PO
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_ITEMFEE, params)
            If reader.Read Then
                oReturnValue.Description = reader("Description").ToString
                oReturnValue.TotalOTR = CDec(reader("TotalOTR"))
                oReturnValue.HargaKaroseri = CDec(reader("HargaKaroseri"))
                oReturnValue.UsedNew = reader("UsedNew").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch exp As Exception
            WriteException("PO", "PO_ItemFee", exp.Message + exp.StackTrace)
            Return Nothing
        End Try
    End Function
#End Region

#Region "Get_Combo_SupplierAccount"
    Public Function Get_Combo_SupplierAccount(ByVal customClass As Parameter.PO) As Parameter.PO
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
        params(0).Value = customClass.SupplierID
        params(1) = New SqlParameter("@DefaultAccount", SqlDbType.Bit)
        params(1).Value = customClass.DefaultBankAccount
        params(2) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
        params(2).Value = customClass.BusinessDate

        Dim oReturnValue As New Parameter.PO

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_SUPPLIERACCOUNT, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
            Return Nothing
        End Try
    End Function
#End Region

#Region "Get_PONumber"
    Public Function Get_PONumber(ByVal customClass As Parameter.PO) As String
        Dim Result As String
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@branchId", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@businessdate", SqlDbType.DateTime)
        params(1).Value = customClass.BusinessDate
        params(2) = New SqlParameter("@ID", SqlDbType.VarChar, 10)
        params(2).Value = customClass.ID
        params(3) = New SqlParameter("@sequenceNo", SqlDbType.VarChar, 20)
        params(3).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_PONUMBER, params)
            Result = CType(params(3).Value, String)
            If Result <> "" Then
                Return Result
            End If
            Return ""
        Catch exp As Exception
            WriteException("PO", "Get_PONumber", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "PO_Account"
    Public Function PO_Account(ByVal customClass As Parameter.PO) As Parameter.PO
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
        params(0).Value = customClass.SupplierID
        params(1) = New SqlParameter("@supplierAccID", SqlDbType.Int)
        params(1).Value = customClass.SupplierAccountID

        Try
            Dim oReturnValue As New Parameter.PO
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, LIST_SUPPLIER, params)
            If reader.Read Then
                oReturnValue.BankName = reader("BankName").ToString
                oReturnValue.SupplierBankID = reader("BankID").ToString
                oReturnValue.BankBranch = reader("BankBranch").ToString
                oReturnValue.AccountName = reader("AccountName").ToString
                oReturnValue.AccountNo = reader("AccountNo").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch exp As Exception
            WriteException("PO", "PO_Account", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "POExtendPaging"
    Public Function POExtendPaging(ByVal customClass As Parameter.PO) As Parameter.PO
        Dim oReturnValue As New Parameter.PO

        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(4).Value = customClass.BusinessDate
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spPOExtendPaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(5).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("PO", "POExtendPaging", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "POExtendSaveEdit"
    Public Sub POExtendSaveEdit(ByVal customClass As Parameter.PO)
        Dim ErrMessage As String = ""
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@POExtendCounter", SqlDbType.Int)
        params(0).Value = customClass.POExtendCounter

        params(1) = New SqlParameter("@POExpiredDate", SqlDbType.DateTime)
        params(1).Value = customClass.POExpiredDate

        params(2) = New SqlParameter("@IsExpired", SqlDbType.Bit)
        params(2).Value = customClass.IsExpired

        params(3) = New SqlParameter("@PONo", SqlDbType.VarChar, 50)
        params(3).Value = customClass.PONumber
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, "spPOExtendSave", params)

        Catch exp As Exception
            WriteException("PO", "POExtendSaveEdit", exp.Message + exp.StackTrace)
        End Try
    End Sub
#End Region

#Region "POViewExtend"
    Public Function POViewExtend(ByVal customClass As Parameter.PO) As Parameter.PO
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customClass.ApplicationID
        params(2) = New SqlParameter("@PONo", SqlDbType.Char, 50)
        params(2).Value = customClass.PONumber
        params(3) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
        params(3).Value = customClass.SupplierID

        Try
            Dim oReturnValue As New Parameter.PO
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, "spPOViewExtend", params)
            If reader.Read Then
                oReturnValue.Notes = reader("Notes").ToString
                oReturnValue.Account = reader("Account").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch exp As Exception
            WriteException("PO", "POViewExtend", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "POGetIDKurs"
    Public Function GetIDKurs(ByVal customclass As Parameter.PO) As Parameter.PO
        Dim oReturnValue As New Parameter.PO
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, LIST_IDKURS).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

#End Region



End Class
