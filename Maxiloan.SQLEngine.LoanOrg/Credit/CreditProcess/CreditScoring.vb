

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class CreditScoring : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spCreditScoringDataEntryPaging"
    Private Const LIST_UPDATE As String = "spCreditScoringDataEntrySaveEdit"
    Private Const LIST_UPDATE2 As String = "spCreditScoringDataEntrySaveEdit2"

#End Region

#Region "CreditScoringPaging"
    Public Function CreditScoringPaging(ByVal customClass As Parameter.CreditScoring) As Parameter.CreditScoring
        Dim oReturnValue As New Parameter.CreditScoring

        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(4).Value = customClass.BranchId
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(5).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("CreditScoring", "CreditScoringPaging", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region

#Region "CreditScoringSaveAdd"

    Private Sub SaveGridScoring(ByVal customClass As Parameter.CreditScoring, ByVal transaction As SqlTransaction)
        Dim params1() As SqlParameter = New SqlParameter(6) {}
        Dim dtSave As New DataTable

        params1(0) = New SqlParameter("@CreditScoreComponentID", SqlDbType.Char, 10)
        params1(1) = New SqlParameter("@ComponentContent", SqlDbType.VarChar, 50)
        params1(2) = New SqlParameter("@ScoreValue", SqlDbType.Decimal)
        params1(3) = New SqlParameter("@Weight", SqlDbType.Int)
        params1(4) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params1(5) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params1(6) = New SqlParameter("@ComponentValue", SqlDbType.VarChar, 50)

        dtSave = customClass.ListData

        For i = 0 To dtSave.Rows.Count - 1
            params1(0).Value = CStr(dtSave.Rows(i).Item("ID"))
            params1(1).Value = CStr(dtSave.Rows(i).Item("Description"))
            params1(2).Value = CDbl(dtSave.Rows(i).Item("ScoreValue"))
            params1(3).Value = CInt(dtSave.Rows(i).Item("Weight"))
            params1(4).Value = customClass.BranchId
            params1(5).Value = customClass.ApplicationID
            params1(6).Value = CDbl(dtSave.Rows(i).Item("Value"))
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, LIST_UPDATE2, params1)
        Next
    End Sub

    Public Function CreditScoringSaveAdd(ByVal customClass As Parameter.CreditScoring) As String
        Return CreditScoringSaveAdd(customClass, False)
        'Dim conn As New SqlConnection(customClass.strConnection)
        'Dim transaction As SqlTransaction = Nothing
        'Dim dtSave As New DataTable
        'Dim i As Integer
        'Dim params() As SqlParameter = New SqlParameter(6) {}
        'Dim params1() As SqlParameter = New SqlParameter(5) {}
        'Try
        '    If conn.State = ConnectionState.Closed Then conn.Open()
        '    transaction = conn.BeginTransaction

        '    params(0) = New SqlParameter("@ApplicationStep", SqlDbType.Char, 3)
        '    params(0).Value = customClass.ApplicationStep
        '    params(1) = New SqlParameter("@CreditScoringDate", SqlDbType.DateTime)
        '    params(1).Value = customClass.CreditScoringDate
        '    params(2) = New SqlParameter("@CreditScore", SqlDbType.Decimal)
        '    params(2).Value = customClass.CreditScore
        '    params(3) = New SqlParameter("@CreditScoringResult", SqlDbType.Char, 1)
        '    params(3).Value = customClass.CreditScoringResult
        '    params(4) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        '    params(4).Value = customClass.BranchId
        '    params(5) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        '    params(5).Value = customClass.ApplicationID
        '    params(6) = New SqlParameter("@CSResult_Temp", SqlDbType.Char, 1)
        '    params(6).Value = customClass.CSResult_Temp
        '    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, LIST_UPDATE, params)

        '    params1(0) = New SqlParameter("@CreditScoreComponentID", SqlDbType.Char, 10)
        '    params1(1) = New SqlParameter("@ComponentContent", SqlDbType.VarChar, 50)
        '    params1(2) = New SqlParameter("@ScoreValue", SqlDbType.Decimal)
        '    params1(3) = New SqlParameter("@Weight", SqlDbType.Int)
        '    params1(4) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        '    params1(5) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)

        '    dtSave = customClass.ListData

        '    For i = 0 To dtSave.Rows.Count - 1
        '        params1(0).Value = CStr(dtSave.Rows(i).Item("ID"))
        '        params1(1).Value = CStr(dtSave.Rows(i).Item("Description"))
        '        params1(2).Value = CDbl(dtSave.Rows(i).Item("ScoreValue"))
        '        params1(3).Value = CInt(dtSave.Rows(i).Item("Weight"))
        '        params1(4).Value = customClass.BranchId
        '        params1(5).Value = customClass.ApplicationID
        '        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, LIST_UPDATE2, params1)
        '    Next

        '    transaction.Commit()
        '    Return ""
        'Catch exp As Exception
        '    transaction.Rollback()
        '    WriteException("CreditScoring", "CreditScoringSaveAdd", exp.Message + exp.StackTrace)
        '    Throw New Exception(exp.Message)
        'Finally
        '    If conn.State = ConnectionState.Open Then conn.Close()
        '    conn.Dispose()
        'End Try
    End Function

    Public Function CreditScoringSaveAdd(ByVal customClass As Parameter.CreditScoring, ByVal isSavingGrid As Boolean) As String
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim dtSave As New DataTable
        Dim i As Integer
        Dim params() As SqlParameter = New SqlParameter(6) {}
        Dim params1() As SqlParameter = New SqlParameter(5) {}
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            params(0) = New SqlParameter("@ApplicationStep", SqlDbType.Char, 3)
            params(0).Value = customClass.ApplicationStep
            params(1) = New SqlParameter("@CreditScoringDate", SqlDbType.DateTime)
            params(1).Value = customClass.CreditScoringDate
            params(2) = New SqlParameter("@CreditScore", SqlDbType.Decimal)
            params(2).Value = customClass.CreditScore
            params(3) = New SqlParameter("@CreditScoringResult", SqlDbType.Char, 1)
            params(3).Value = customClass.CreditScoringResult
            params(4) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(4).Value = customClass.BranchId
            params(5) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(5).Value = customClass.ApplicationID
            params(6) = New SqlParameter("@CSResult_Temp", SqlDbType.Char, 1)
            params(6).Value = customClass.CSResult_Temp
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, LIST_UPDATE, params)

            If isSavingGrid Then
                SaveGridScoring(customClass, transaction)
            End If


            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()
            WriteException("CreditScoring", "CreditScoringSaveAdd", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    
    
#End Region


    Public Function DoCreaditScoringSave(cnn As String, loginId As String, branchid As String, AppId As String, scoreResult As Parameter.ScoreResults, bnsDate As Date) As String
        Try

 
            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

            params.Add(New SqlParameter("@BranchId", SqlDbType.VarChar, 3) With {.Value = branchid})
            params.Add(New SqlParameter("@ApplicationId", SqlDbType.VarChar) With {.Value = AppId})

            params.Add(New SqlParameter("@ScoreSchemeID", SqlDbType.VarChar) With {.Value = scoreResult.ScoreSchemeID})
            params.Add(New SqlParameter("@ScoreDate", SqlDbType.Date) With {.Value = bnsDate})
            params.Add(New SqlParameter("@CutOffScore", SqlDbType.Int) With {.Value = scoreResult.CuttOff})
            params.Add(New SqlParameter("@scoreResult", SqlDbType.Structured) With {.Value = scoreResult.ToDataTable})
            params.Add(New SqlParameter("@loginId", SqlDbType.VarChar) With {.Value = loginId})

            params.Add(New SqlParameter("@ApplicationStep", SqlDbType.VarChar, 3) With {.Value = scoreResult.ApplicationStep})
            params.Add(New SqlParameter("@CSResult_Temp", SqlDbType.VarChar, 1) With {.Value = scoreResult.CSResult_Temp})

            Dim errPrm = New SqlParameter("@errMsg", SqlDbType.VarChar, 500) With {.Direction = ParameterDirection.Output}
            params.Add(errPrm)

            SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, "spCreditScoringDataEntrySave", params.ToArray)

            Return errPrm.Value.ToString

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function



    Public Function GetCreditScorePolicyResult(ByVal oCustomClass As Parameter.CreditScoring) As Parameter.CreditScoring
        Dim oReturnValue As New Parameter.CreditScoring
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spCREDITScorePolicyResultSumPaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Prospect", "GetProspect", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.SalesMarketing.Prospect.GetProspect")
        End Try
    End Function


    Public Function crDispositionCreditRpt(cnn As String, whereCond As String(), tp As String) As System.Data.DataSet
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        params.Add(New SqlParameter("@Applicationids", SqlDbType.Structured) With {.Value = ToProspectAppidDataTable(whereCond)})
        Return SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, IIf(tp = "A", "spCreditDispositionCreditRpt", "spCreditScoreCalculationRpt"), params.ToArray)

    End Function
    Function ToProspectAppidDataTable(data As IList(Of String)) As DataTable
        Dim dt = New DataTable()

        Dim strArray As String() = New String() {"id", "value1", "value2"}
        For Each s In strArray
            dt.Columns.Add(New DataColumn(s))
        Next

        For Each v In data
            Dim row = dt.NewRow()
            row("id") = v.Trim()
            row("value1") = ""
            row("value2") = ""
            dt.Rows.Add(row)
        Next
        Return dt
    End Function

    Private gObjCon As SqlConnection
    Public Function DoCreditScoringProceed(cnn As String, branchId As String, ApplicationID As String, loginId As String, bnsDate As Date) As String
        Try

            Dim _scoreResults As Parameter.ScoreResults = New Parameter.ScoreResults
            Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

            params.Add(New SqlParameter("@BranchId", SqlDbType.VarChar, 3) With {.Value = branchId})
            params.Add(New SqlParameter("@ApplicationId", SqlDbType.VarChar) With {.Value = ApplicationID})

            params.Add(New SqlParameter("@ScoreSchemeID", SqlDbType.VarChar) With {.Value = _scoreResults.ScoreSchemeID})
            params.Add(New SqlParameter("@ScoreDate", SqlDbType.Date) With {.Value = bnsDate})
            params.Add(New SqlParameter("@CutOffScore", SqlDbType.Int) With {.Value = _scoreResults.CuttOff})
            params.Add(New SqlParameter("@scoreResult", SqlDbType.Structured) With {.Value = _scoreResults.ToDataTable})
            params.Add(New SqlParameter("@loginId", SqlDbType.VarChar) With {.Value = loginId})

            params.Add(New SqlParameter("@ApplicationStep", SqlDbType.VarChar, 3) With {.Value = _scoreResults.ApplicationStep})
            params.Add(New SqlParameter("@CSResult_Temp", SqlDbType.VarChar, 1) With {.Value = _scoreResults.CSResult_Temp})

            Dim errPrm = New SqlParameter("@errMsg", SqlDbType.VarChar, 500) With {.Direction = ParameterDirection.Output}
            params.Add(errPrm)

            SqlHelper.ExecuteNonQuery(cnn, CommandType.StoredProcedure, "spCreditScoringDataEntrySave", params.ToArray)

            Return errPrm.Value.ToString

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetApprovalCreditScoring(ByVal oCustomClass As Parameter.CreditScoring, ByVal strApproval As String)
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim transaction As SqlTransaction = Nothing

        Dim par() As SqlParameter = New SqlParameter(1) {}

        par(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 100)
        par(0).Value = oCustomClass.ApplicationID
        par(1) = New SqlParameter("@Approval", SqlDbType.Char, 3)
        par(1).Value = strApproval

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            If (strApproval.Trim = "R") Then
                oCustomClass.Approval.ApprovalTransaction = transaction
                'oClass.Approval.TransactionNo = CStr(params(2).Value)
                Dim strApprovalNo = oApprovalID.RequestForApproval(oCustomClass.Approval)
            End If

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spRequestApprovalCreditScoring", par)
            transaction.Commit()

        Catch ex As Exception
            transaction.Rollback()
            Throw New Exception(ex.Message)

        Finally
            If conn.State = ConnectionState.Open Then conn.Dispose()
            conn.Dispose()

        End Try
    End Function
End Class
