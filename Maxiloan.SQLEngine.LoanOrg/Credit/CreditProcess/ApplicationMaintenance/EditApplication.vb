
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan
#End Region

Public Class EditApplication : Inherits maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const spPaging As String = "spApplicationPaging"
    Private Const spCopyFrom As String = "spApplicationCopyFrom"
    Private Const spTC As String = "spApplicationTC"
    Private Const spTC2 As String = "spApplicationTC2"
    Private Const spShowDataAgreement As String = "spApplicationShowDataAgreement"
    Private Const spApplicationSaveAdd As String = "spApplicationSaveAdd"
    Private Const spApplicationSaveAdd2 As String = "spApplicationSaveAdd2"
    Private Const spApplicationSaveAdd3 As String = "spApplicationSaveAdd3"
    Private Const spApplicationSaveAdd4 As String = "spApplicationSaveAdd4"
    Private Const spApplicationSaveEdit As String = "spAApplicationSaveEdit"
    Private Const spApplicationSaveEdit2 As String = "spAApplicationSaveEdit2"
    Private Const spApplicationSaveEdit3 As String = "spAApplicationSaveEdit3"
    Private Const spApplicationSaveEdit4 As String = "spAApplicationSaveEdit4"
    Private Const spApplicationSaveEdit4_1 As String = "spApplicationSaveEdit4_1"
    Private Const spApplicationFee As String = "spApplicationFee"
    Private Const spApplicationEditPaging As String = "spApplicationEditPaging"
    Private Const spApplicationMaintenancePaging As String = "spAApplicationMaintenancePaging"
    Private Const spGetCountAppliationId As String = "spGetCountApplicationId"
#End Region

#Region "Application"
    Public Function GetApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spPaging, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("EditApplication", "GetApplication", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetApplication")
        End Try
    End Function
    Public Function GetCountAppliationId(ByVal oCustomClass As Parameter.Application) As Integer
        Dim objreader As SqlDataReader
        Dim value As Integer
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@CustomerId", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.CustomerId
        Try
            objreader = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, spGetCountAppliationId, params)
            If objreader.Read Then
                value = CInt(objreader("Total"))
            End If
            objreader.Close()
            Return value
        Catch exp As Exception
            WriteException("EditApplication", "GetCountAppliationId", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function GetCopyFrom(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerId
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spCopyFrom, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("EditApplication", "GetCopyFrom", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetCopyFrom")
        End Try
    End Function
    Public Function GetTC(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Try
            If oCustomClass.AddEdit = "Edit" Then
                Dim params() As SqlParameter = New SqlParameter(1) {}
                params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params(0).Value = oCustomClass.AppID
                params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                params(1).Value = Replace(oCustomClass.BranchId, "'", "")
                oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spApplicationTCEdit", params).Tables(0)
                'ElseIf oCustomClass.AddEdit = "GoLive" Then
                '    Dim params() As SqlParameter = New SqlParameter(1) {}
                '    params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                '    params(0).Value = oCustomClass.AppID
                '    params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                '    params(1).Value = Replace(oCustomClass.BranchId, "'", "")
                '    oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGoLiveTC", params).Tables(0)
                'Else
                '    Dim params() As SqlParameter = New SqlParameter(2) {}
                '    params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
                '    params(0).Value = oCustomClass.ProductID
                '    params(1) = New SqlParameter("@Branch", SqlDbType.VarChar, 3)
                '    params(1).Value = Replace(oCustomClass.BranchId, "'", "")
                '    params(2) = New SqlParameter("@Type", SqlDbType.Char, 1)
                '    params(2).Value = oCustomClass.Type
                '    oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spTC, params).Tables(0)
            End If
            Return oReturnValue
        Catch exp As Exception
            WriteException("EditApplication", "GetTC", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetTC")
        End Try
    End Function
    Public Function GetTC2(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Try
            If oCustomClass.AddEdit = "Edit" Then
                Dim params() As SqlParameter = New SqlParameter(1) {}
                params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params(0).Value = oCustomClass.AppID
                params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                params(1).Value = Replace(oCustomClass.BranchId, "'", "")
                oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spApplicationTC2Edit", params).Tables(0)
            ElseIf oCustomClass.AddEdit = "GoLive" Then
                Dim params() As SqlParameter = New SqlParameter(1) {}
                params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params(0).Value = oCustomClass.AppID
                params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                params(1).Value = Replace(oCustomClass.BranchId, "'", "")
                oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGoLiveTC2", params).Tables(0)
            Else
                Dim params() As SqlParameter = New SqlParameter(2) {}
                params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
                params(0).Value = oCustomClass.ProductID
                params(1) = New SqlParameter("@Branch", SqlDbType.VarChar, 3)
                params(1).Value = Replace(oCustomClass.BranchId, "'", "")
                params(2) = New SqlParameter("@Type", SqlDbType.Char, 1)
                params(2).Value = oCustomClass.Type
                oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spTC2, params).Tables(0)
            End If
            Return oReturnValue
        Catch exp As Exception
            WriteException("EditApplication", "GetTC2", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetTC2")
        End Try
    End Function
    Public Function GetAddress(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        'Dim oReturnValue As New Parameter.Application
        'Dim params() As SqlParameter = New SqlParameter(0) {}
        'Dim spName As String
        'If oCustomClass.Type = "P" Then
        '    spName = "spApplicationCopy"
        'Else
        '    spName = "spApplicationCompanyAddress"
        'End If
        'params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        'params(0).Value = oCustomClass.CustomerId
        'Try
        '    oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spName, params).Tables(0)
        '    Return oReturnValue
        'Catch exp as Exception
        '    Throw New Exception("Error On DataAccess.AccAcq.Application.GetAddress")
        'End Try

        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Dim params1() As SqlParameter = New SqlParameter(2) {}
        Dim spName As String

        If oCustomClass.Type = "P" Then
            spName = "spAApplicationEditAddress"
            params1(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params1(0).Value = oCustomClass.ApplicationID
            params1(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params1(1).Value = oCustomClass.BranchId
            params1(2) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params1(2).Value = oCustomClass.CustomerId
            Try
                oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spName, params1).Tables(0)
                Return oReturnValue
            Catch exp As Exception
                WriteException("EditApplication", "GetAddress(PersonalCustomer)", exp.Message + exp.StackTrace)
                Throw New Exception("Error On DataAccess.AccAcq.Application.GetAddress")
            End Try
        Else
            spName = "spApplicationCompanyAddress"
            params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params(0).Value = oCustomClass.CustomerId
            Try
                oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spName, params).Tables(0)
                Return oReturnValue
            Catch exp As Exception
                WriteException("EditApplication", "GetAddress(CompanyCustomer)", exp.Message + exp.StackTrace)
                Throw New Exception("Error On DataAccess.AccAcq.Application.GetAddress")
            End Try

        End If

    End Function
    Public Function GetShowDataAgreement(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.AgreementNo
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spShowDataAgreement, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("EditApplication", "GetShowDataAgreement", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetShowDataAgreement")
        End Try
    End Function

    Public Function ApplicationSaveAdd(ByVal oApplication As Parameter.Application, _
        ByVal oRefPersonal As Parameter.Personal, _
        ByVal oRefAddress As Parameter.Address, ByVal oMailingAddress As Parameter.Address, _
        ByVal oData1 As DataTable, ByVal oData2 As DataTable, ByVal oData3 As DataTable) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim conn As New SqlConnection(oApplication.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim ApplicationID As String = ""
        Dim intLoop As Integer

        Dim params() As SqlParameter = New SqlParameter(60) {}
        Dim params1() As SqlParameter = New SqlParameter(8) {}
        Dim params2() As SqlParameter = New SqlParameter(7) {}
        Dim params3() As SqlParameter = New SqlParameter(1) {}

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            transaction = conn.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oApplication.BranchId
            params(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params(1).Value = oApplication.CustomerId
            params(2) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
            params(2).Value = oApplication.ProductID
            params(3) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 10)
            params(3).Value = oApplication.ProductOffID
            params(4) = New SqlParameter("@IsNST", SqlDbType.Char, 1)
            params(4).Value = oApplication.IsNST
            params(5) = New SqlParameter("@ContractStatus", SqlDbType.Char, 3)
            params(5).Value = oApplication.ContractStatus
            params(6) = New SqlParameter("@DefaultStatus", SqlDbType.Char, 3)
            params(6).Value = oApplication.DefaultStatus
            params(7) = New SqlParameter("@ApplicationStep", SqlDbType.Char, 3)
            params(7).Value = oApplication.ApplicationStep
            params(8) = New SqlParameter("@NumOfAssetUnit", SqlDbType.SmallInt)
            params(8).Value = oApplication.NumOfAssetUnit
            params(9) = New SqlParameter("@InterestType", SqlDbType.Char, 2)
            params(9).Value = oApplication.InterestType
            params(10) = New SqlParameter("@InstallmentScheme", SqlDbType.Char, 2)
            params(10).Value = oApplication.InstallmentScheme
            params(11) = New SqlParameter("@GuarantorID", SqlDbType.Char, 10)
            params(11).Value = oApplication.GuarantorID
            params(12) = New SqlParameter("@SpouseID", SqlDbType.Char, 10)
            params(12).Value = oApplication.SpouseID
            params(13) = New SqlParameter("@WayOfPayment", SqlDbType.Char, 2)
            params(13).Value = oApplication.WayOfPayment
            params(14) = New SqlParameter("@AgreementDate", SqlDbType.VarChar, 8)
            params(14).Value = oApplication.AgreementDate
            params(15) = New SqlParameter("@SurveyDate", SqlDbType.VarChar, 8)
            params(15).Value = oApplication.SurveyDate
            params(16) = New SqlParameter("@ApplicationSource", SqlDbType.Char, 1)
            params(16).Value = oApplication.ApplicationSource
            params(17) = New SqlParameter("@PercentagePenalty", SqlDbType.Decimal)
            params(17).Value = IIf(oApplication.PercentagePenalty = "", 0, oApplication.PercentagePenalty)
            params(18) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
            params(18).Value = IIf(oApplication.AdminFee = "", 0, oApplication.AdminFee)
            params(19) = New SqlParameter("@FiduciaFee", SqlDbType.Decimal)
            params(19).Value = IIf(oApplication.FiduciaFee = "", 0, oApplication.FiduciaFee)
            params(20) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal)
            params(20).Value = IIf(oApplication.ProvisionFee = "", 0, oApplication.ProvisionFee)
            params(21) = New SqlParameter("@NotaryFee", SqlDbType.Decimal)
            params(21).Value = IIf(oApplication.NotaryFee = "", 0, oApplication.NotaryFee)
            params(22) = New SqlParameter("@SurveyFee", SqlDbType.Decimal)
            params(22).Value = IIf(oApplication.SurveyFee = "", 0, oApplication.SurveyFee)
            params(23) = New SqlParameter("@BBNFee", SqlDbType.Decimal)
            params(23).Value = IIf(oApplication.BBNFee = "", 0, oApplication.BBNFee)
            params(24) = New SqlParameter("@OtherFee", SqlDbType.Decimal)
            params(24).Value = IIf(oApplication.OtherFee = "", 0, oApplication.OtherFee)
            params(25) = New SqlParameter("@MailingFax", SqlDbType.Char, 10)
            params(25).Value = oMailingAddress.Fax
            params(26) = New SqlParameter("@Notes", SqlDbType.Text)
            params(26).Value = oApplication.Notes
            params(27) = New SqlParameter("@ReferenceName", SqlDbType.VarChar, 50)
            params(27).Value = IIf(oRefPersonal.PersonName <> "", Replace(oRefPersonal.PersonName, "'", "''"), oRefPersonal.PersonName)
            'params(27).Value = oRefPersonal.PersonName
            params(28) = New SqlParameter("@ReferenceJobTitle", SqlDbType.VarChar, 50)
            params(28).Value = oRefPersonal.PersonTitle
            params(29) = New SqlParameter("@ReferenceAddress", SqlDbType.VarChar, 100)
            params(29).Value = oRefAddress.Address
            params(30) = New SqlParameter("@ReferenceRT", SqlDbType.Char, 3)
            params(30).Value = oRefAddress.RT
            params(31) = New SqlParameter("@ReferenceRW", SqlDbType.Char, 3)
            params(31).Value = oRefAddress.RW
            params(32) = New SqlParameter("@ReferenceKelurahan", SqlDbType.VarChar, 30)
            params(32).Value = oRefAddress.Kelurahan
            params(33) = New SqlParameter("@ReferenceKecamatan", SqlDbType.VarChar, 30)
            params(33).Value = oRefAddress.Kecamatan
            params(34) = New SqlParameter("@ReferenceCity", SqlDbType.VarChar, 30)
            params(34).Value = oRefAddress.City
            params(35) = New SqlParameter("@ReferenceZipCode", SqlDbType.Char, 5)
            params(35).Value = oRefAddress.ZipCode
            params(36) = New SqlParameter("@ReferenceAreaPhone1", SqlDbType.Char, 4)
            params(36).Value = oRefAddress.AreaPhone1
            params(37) = New SqlParameter("@ReferencePhone1", SqlDbType.Char, 10)
            params(37).Value = oRefAddress.Phone1
            params(38) = New SqlParameter("@ReferenceAreaPhone2", SqlDbType.Char, 4)
            params(38).Value = oRefAddress.AreaPhone2
            params(39) = New SqlParameter("@ReferencePhone2", SqlDbType.Char, 10)
            params(39).Value = oRefAddress.Phone2
            params(40) = New SqlParameter("@ReferenceAreaFax", SqlDbType.Char, 4)
            params(40).Value = oRefAddress.AreaFax
            params(41) = New SqlParameter("@ReferenceFax", SqlDbType.Char, 10)
            params(41).Value = oRefAddress.Fax
            params(42) = New SqlParameter("@ReferenceMobilePhone", SqlDbType.VarChar, 20)
            params(42).Value = oRefPersonal.MobilePhone
            params(43) = New SqlParameter("@ReferenceEmail", SqlDbType.VarChar, 30)
            params(43).Value = oRefPersonal.Email
            params(44) = New SqlParameter("@ReferenceNotes", SqlDbType.Text)
            params(44).Value = oApplication.RefNotes
            params(45) = New SqlParameter("@MailingAddress", SqlDbType.VarChar, 100)
            params(45).Value = oMailingAddress.Address
            params(46) = New SqlParameter("@MailingRT", SqlDbType.Char, 3)
            params(46).Value = oMailingAddress.RT
            params(47) = New SqlParameter("@MailingRW", SqlDbType.Char, 3)
            params(47).Value = oMailingAddress.RW
            params(48) = New SqlParameter("@MailingKelurahan", SqlDbType.VarChar, 30)
            params(48).Value = oMailingAddress.Kelurahan
            params(49) = New SqlParameter("@MailingKecamatan", SqlDbType.VarChar, 30)
            params(49).Value = oMailingAddress.Kecamatan
            params(50) = New SqlParameter("@MailingCity", SqlDbType.VarChar, 30)
            params(50).Value = oMailingAddress.City
            params(51) = New SqlParameter("@MailingZipCode", SqlDbType.Char, 5)
            params(51).Value = oMailingAddress.ZipCode
            params(52) = New SqlParameter("@MailingAreaPhone1", SqlDbType.Char, 4)
            params(52).Value = oMailingAddress.AreaPhone1
            params(53) = New SqlParameter("@MailingPhone1", SqlDbType.Char, 10)
            params(53).Value = oMailingAddress.Phone1
            params(54) = New SqlParameter("@MailingAreaPhone2", SqlDbType.Char, 4)
            params(54).Value = oMailingAddress.AreaPhone2
            params(55) = New SqlParameter("@MailingPhone2", SqlDbType.Char, 10)
            params(55).Value = oMailingAddress.Phone2
            params(56) = New SqlParameter("@MailingAreaFax", SqlDbType.Char, 4)
            params(56).Value = oMailingAddress.AreaFax
            params(57) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(57).Direction = ParameterDirection.Output
            params(58) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(58).Direction = ParameterDirection.Output
            params(59) = New SqlParameter("@businessdate", SqlDbType.DateTime)
            params(59).Value = oApplication.BusinessDate
            params(60) = New SqlParameter("@isAdmCapitalized", SqlDbType.Bit)
            params(60).Value = oApplication.isAdmCapitalized

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveAdd, params)
            ErrMessage = CType(params(57).Value, String)
            ApplicationID = CType(params(58).Value, String)

            If ErrMessage <> "" Then
                oReturnValue.ApplicationID = ""
                oReturnValue.Err = ErrMessage
                Return oReturnValue
                Exit Function
            End If

            If oData1.Rows.Count > 0 Then
                params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params1(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params1(3) = New SqlParameter("@PriorTo", SqlDbType.Char, 3)
                params1(4) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
                params1(5) = New SqlParameter("@IsMandatory", SqlDbType.Char, 1)
                params1(6) = New SqlParameter("@PromiseDate", SqlDbType.VarChar, 8)
                params1(7) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params1(8) = New SqlParameter("@Notes", SqlDbType.Text)

                For intLoop = 0 To oData1.Rows.Count - 1
                    params1(0).Value = oApplication.BranchId
                    params1(1).Value = ApplicationID
                    params1(2).Value = oData1.Rows(intLoop).Item("MasterTCID")
                    params1(3).Value = oData1.Rows(intLoop).Item("PriorTo")
                    params1(4).Value = oData1.Rows(intLoop).Item("IsChecked")
                    params1(5).Value = oData1.Rows(intLoop).Item("IsMandatory")
                    params1(6).Value = oData1.Rows(intLoop).Item("PromiseDate")
                    params1(7).Value = oApplication.BusinessDate
                    params1(8).Value = oData1.Rows(intLoop).Item("Notes")

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveAdd2, params1)
                Next
            End If
            If oData2.Rows.Count > 0 Then
                params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params2(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params2(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params2(3) = New SqlParameter("@AGTCCLSequenceNo", SqlDbType.SmallInt)
                params2(4) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
                params2(5) = New SqlParameter("@PromiseDate", SqlDbType.VarChar, 8)
                params2(6) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params2(7) = New SqlParameter("@Notes", SqlDbType.Text)

                For intLoop = 0 To oData2.Rows.Count - 1
                    params2(0).Value = oApplication.BranchId
                    params2(1).Value = ApplicationID
                    params2(2).Value = oData2.Rows(intLoop).Item("MasterTCID")
                    params2(3).Value = CInt(oData2.Rows(intLoop).Item("AGTCCLSequenceNo"))
                    params2(4).Value = oData2.Rows(intLoop).Item("IsChecked")
                    params2(5).Value = oData2.Rows(intLoop).Item("PromiseDate")
                    params2(6).Value = oApplication.BusinessDate
                    params2(7).Value = oData2.Rows(intLoop).Item("Notes")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveAdd3, params2)
                Next
            End If

            If oData3.Rows.Count > 0 Then
                params3(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params3(1) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)

                For intLoop = 0 To oData3.Rows.Count - 1
                    params3(0).Value = ApplicationID
                    params3(1).Value = oData3.Rows(intLoop).Item("AgreementNo")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveAdd4, params3)
                Next
            End If

            transaction.Commit()
            oReturnValue.ApplicationID = ApplicationID
            oReturnValue.Err = ErrMessage
            Return oReturnValue

        Catch exp As Exception
            transaction.Rollback()
            WriteException("EditApplication", "ApplicationSaveAdd", exp.Message + exp.StackTrace)
            oReturnValue.ApplicationID = ApplicationID
            oReturnValue.Err = exp.Message
            Throw New Exception("Error On DataAccess.AccAcq.Customer.ApplicationSaveAdd")
            Return oReturnValue

        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
    Public Function GetFee(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = oCustomClass.ProductID
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = Replace(oCustomClass.BranchId, "'", "")
        params(2) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 10)
        params(2).Value = oCustomClass.ProductOffID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spApplicationFee, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("EditApplication", "GetFee", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetFee")
        End Try
    End Function
#End Region
#Region "View Application"
    Public Function GetViewApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim intLoop As Integer
        Dim conn As New SqlConnection(oCustomClass.strConnection)

        Dim transaction As SqlTransaction = Nothing
        Dim spViewApplication As String
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = oCustomClass.AppID
            For intLoop = 0 To 3
                Select Case intLoop
                    Case 0
                        spViewApplication = "spViewApplication" + "CD"
                        oReturnValue.DataCD = SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spViewApplication, params).Tables(0)
                    Case 1
                        spViewApplication = "spViewApplication" + "TC"
                        oReturnValue.DataTC = SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spViewApplication, params).Tables(0)
                    Case 2
                        spViewApplication = "spViewApplication" + "TC2"
                        oReturnValue.DataTC2 = SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spViewApplication, params).Tables(0)
                    Case 3
                        spViewApplication = "spViewApplication"
                        oReturnValue.ListData = SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spViewApplication, params).Tables(0)
                End Select
            Next
            transaction.Commit()
            oReturnValue.Err = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("EditApplication", "GetViewApplication", exp.Message + exp.StackTrace)
            oReturnValue.Err = "Error: " & exp.Message & ""
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetViewApplication")
            Return oReturnValue
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
        End Try
    End Function
#End Region
#Region "ApplicationMaintenance"
    Public Function GetApplicationMaintenance(ByVal customClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@WhereCond2", SqlDbType.VarChar)
        params(5).Value = customClass.WhereCond2
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spAApplicationMaintenancePaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("EditApplication", "GetApplicationMaintenance", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetApplicationMaintenance")
        End Try
    End Function
    Public Function GetCD(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = oCustomClass.AppID
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spAApplicationCDEdit", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("EditApplication", "GetCD", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetCD")
        End Try
    End Function
    Public Function GetApplicationEdit(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = oCustomClass.AppID
            params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(1).Value = Replace(oCustomClass.BranchId, "'", "")
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spAApplicationEdit", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("EditApplication", "GetApplicationEdit", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetApplicationEdit")
        End Try
    End Function

    Public Function ApplicationSaveEdit(ByVal oApplication As Parameter.Application, _
       ByVal oRefPersonal As Parameter.Personal, _
       ByVal oRefAddress As Parameter.Address, ByVal oMailingAddress As Parameter.Address, _
       ByVal oData1 As DataTable, ByVal oData2 As DataTable, ByVal oData3 As DataTable, _
       ByVal MyDataTable As DataTable) As Parameter.Application        
        Dim oReturnValue As New Parameter.Application
        Dim conn As New SqlConnection(oApplication.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim intLoop As Integer
        Dim params() As SqlParameter = New SqlParameter(59) {}
        Dim params1() As SqlParameter = New SqlParameter(8) {}
        Dim params1_1() As SqlParameter = New SqlParameter(2) {}
        Dim params2() As SqlParameter = New SqlParameter(7) {}
        Dim params2_1() As SqlParameter = New SqlParameter(3) {}        
        Dim params3() As SqlParameter = New SqlParameter(1) {}
        Dim params4() As SqlParameter = New SqlParameter(0) {}
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oApplication.BranchId
            params(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params(1).Value = oApplication.CustomerId
            params(2) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
            params(2).Value = oApplication.ProductID
            params(3) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 10)
            params(3).Value = oApplication.ProductOffID
            params(4) = New SqlParameter("@MailingPhone1", SqlDbType.Char, 10)
            params(4).Value = oMailingAddress.Phone1
            params(5) = New SqlParameter("@MailingAreaPhone2", SqlDbType.Char, 4)
            params(5).Value = oMailingAddress.AreaPhone2
            params(6) = New SqlParameter("@MailingPhone2", SqlDbType.Char, 10)
            params(6).Value = oMailingAddress.Phone2
            params(7) = New SqlParameter("@MailingAreaFax", SqlDbType.Char, 4)
            params(7).Value = oMailingAddress.AreaFax
            params(8) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(8).Value = oApplication.AppID
            params(9) = New SqlParameter("@InterestType", SqlDbType.Char, 2)
            params(9).Value = oApplication.InterestType
            params(10) = New SqlParameter("@InstallmentScheme", SqlDbType.Char, 2)
            params(10).Value = oApplication.InstallmentScheme
            params(11) = New SqlParameter("@GuarantorID", SqlDbType.Char, 10)
            params(11).Value = oApplication.GuarantorID
            params(12) = New SqlParameter("@SpouseID", SqlDbType.Char, 10)
            params(12).Value = oApplication.SpouseID
            params(13) = New SqlParameter("@WayOfPayment", SqlDbType.Char, 2)
            params(13).Value = oApplication.WayOfPayment
            params(14) = New SqlParameter("@AgreementDate", SqlDbType.VarChar, 8)
            params(14).Value = oApplication.AgreementDate
            params(15) = New SqlParameter("@SurveyDate", SqlDbType.VarChar, 8)
            params(15).Value = oApplication.SurveyDate
            params(16) = New SqlParameter("@ApplicationSource", SqlDbType.Char, 1)
            params(16).Value = oApplication.ApplicationSource
            params(17) = New SqlParameter("@PercentagePenalty", SqlDbType.Decimal)
            params(17).Value = IIf(oApplication.PercentagePenalty = "", 0, oApplication.PercentagePenalty)
            params(18) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
            params(18).Value = IIf(oApplication.AdminFee = "", 0, oApplication.AdminFee)
            params(19) = New SqlParameter("@FiduciaFee", SqlDbType.Decimal)
            params(19).Value = IIf(oApplication.FiduciaFee = "", 0, oApplication.FiduciaFee)
            params(20) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal)
            params(20).Value = IIf(oApplication.ProvisionFee = "", 0, oApplication.ProvisionFee)
            params(21) = New SqlParameter("@NotaryFee", SqlDbType.Decimal)
            params(21).Value = IIf(oApplication.NotaryFee = "", 0, oApplication.NotaryFee)
            params(22) = New SqlParameter("@SurveyFee", SqlDbType.Decimal)
            params(22).Value = IIf(oApplication.SurveyFee = "", 0, oApplication.SurveyFee)
            params(23) = New SqlParameter("@BBNFee", SqlDbType.Decimal)
            params(23).Value = IIf(oApplication.BBNFee = "", 0, oApplication.BBNFee)
            params(24) = New SqlParameter("@OtherFee", SqlDbType.Decimal)
            params(24).Value = IIf(oApplication.OtherFee = "", 0, oApplication.OtherFee)
            params(25) = New SqlParameter("@MailingFax", SqlDbType.Char, 10)
            params(25).Value = oMailingAddress.Fax
            params(26) = New SqlParameter("@Notes", SqlDbType.Text)
            params(26).Value = oApplication.Notes
            params(27) = New SqlParameter("@ReferenceName", SqlDbType.VarChar, 50)
            params(27).Value = IIf(oRefPersonal.PersonName <> "", Replace(oRefPersonal.PersonName, "'", "''"), oRefPersonal.PersonName)
            params(28) = New SqlParameter("@ReferenceJobTitle", SqlDbType.VarChar, 50)
            params(28).Value = oRefPersonal.PersonTitle
            params(29) = New SqlParameter("@ReferenceAddress", SqlDbType.VarChar, 100)
            params(29).Value = oRefAddress.Address
            params(30) = New SqlParameter("@ReferenceRT", SqlDbType.Char, 3)
            params(30).Value = oRefAddress.RT
            params(31) = New SqlParameter("@ReferenceRW", SqlDbType.Char, 3)
            params(31).Value = oRefAddress.RW
            params(32) = New SqlParameter("@ReferenceKelurahan", SqlDbType.VarChar, 30)
            params(32).Value = oRefAddress.Kelurahan
            params(33) = New SqlParameter("@ReferenceKecamatan", SqlDbType.VarChar, 30)
            params(33).Value = oRefAddress.Kecamatan
            params(34) = New SqlParameter("@ReferenceCity", SqlDbType.VarChar, 30)
            params(34).Value = oRefAddress.City
            params(35) = New SqlParameter("@ReferenceZipCode", SqlDbType.Char, 5)
            params(35).Value = oRefAddress.ZipCode
            params(36) = New SqlParameter("@ReferenceAreaPhone1", SqlDbType.Char, 4)
            params(36).Value = oRefAddress.AreaPhone1
            params(37) = New SqlParameter("@ReferencePhone1", SqlDbType.Char, 10)
            params(37).Value = oRefAddress.Phone1
            params(38) = New SqlParameter("@ReferenceAreaPhone2", SqlDbType.Char, 4)
            params(38).Value = oRefAddress.AreaPhone2
            params(39) = New SqlParameter("@ReferencePhone2", SqlDbType.Char, 10)
            params(39).Value = oRefAddress.Phone2
            params(40) = New SqlParameter("@ReferenceAreaFax", SqlDbType.Char, 4)
            params(40).Value = oRefAddress.AreaFax
            params(41) = New SqlParameter("@ReferenceFax", SqlDbType.Char, 10)
            params(41).Value = oRefAddress.Fax
            params(42) = New SqlParameter("@ReferenceMobilePhone", SqlDbType.VarChar, 20)
            params(42).Value = oRefPersonal.MobilePhone
            params(43) = New SqlParameter("@ReferenceEmail", SqlDbType.VarChar, 30)
            params(43).Value = oRefPersonal.Email
            params(44) = New SqlParameter("@ReferenceNotes", SqlDbType.Text)
            params(44).Value = oApplication.RefNotes
            params(45) = New SqlParameter("@MailingAddress", SqlDbType.VarChar, 100)
            params(45).Value = oMailingAddress.Address
            params(46) = New SqlParameter("@MailingRT", SqlDbType.Char, 3)
            params(46).Value = oMailingAddress.RT
            params(47) = New SqlParameter("@MailingRW", SqlDbType.Char, 3)
            params(47).Value = oMailingAddress.RW
            params(48) = New SqlParameter("@MailingKelurahan", SqlDbType.VarChar, 30)
            params(48).Value = oMailingAddress.Kelurahan
            params(49) = New SqlParameter("@MailingKecamatan", SqlDbType.VarChar, 30)
            params(49).Value = oMailingAddress.Kecamatan
            params(50) = New SqlParameter("@MailingCity", SqlDbType.VarChar, 30)
            params(50).Value = oMailingAddress.City
            params(51) = New SqlParameter("@MailingZipCode", SqlDbType.Char, 5)
            params(51).Value = oMailingAddress.ZipCode
            params(52) = New SqlParameter("@MailingAreaPhone1", SqlDbType.Char, 4)
            params(52).Value = oMailingAddress.AreaPhone1
            params(53) = New SqlParameter("@AdditionalAdminFee", SqlDbType.Decimal)
            params(53).Value = oApplication.AdditionalAdminFee
            params(54) = New SqlParameter("@StepUpStepDownType", SqlDbType.Char, 2)
            params(54).Value = IIf(oApplication.StepUpStepDownType <> "", oApplication.StepUpStepDownType, DBNull.Value)
            params(55) = New SqlParameter("@StatusFiducia", SqlDbType.Char, 2)
            params(55).Value = oApplication.StatusFiduciaFee
            params(56) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(56).Direction = ParameterDirection.Output
            params(57) = New SqlParameter("@isAdminFeeCredit", SqlDbType.Bit)
            params(57).Value = oApplication.IsAdminFeeCredit
            params(58) = New SqlParameter("@isProvisiCredit", SqlDbType.Bit)
            params(58).Value = oApplication.IsProvisiCredit
            params(59) = New SqlParameter("@isAdmCapitalized", SqlDbType.Bit)
            params(59).Value = oApplication.isAdmCapitalized

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveEdit, params)
            ErrMessage = CType(params(56).Value, String)


            If MyDataTable.Rows.Count > 0 Then
                params4(0) = New SqlParameter("@CrossDefaultApplicationID", SqlDbType.Char, 20)
                For intLoop = 0 To MyDataTable.Rows.Count - 1
                    params4(0).Value = MyDataTable.Rows(intLoop).Item("AgreementNo")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spAApplicationClearCD", params4)
                Next
            End If

            If oData2.Rows.Count > 0 Then
                params2_1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params2_1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params2_1(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params2_1(3) = New SqlParameter("@AGTCCLSequenceNo", SqlDbType.SmallInt)
                For intLoop = 0 To oData2.Rows.Count - 1
                    params2_1(0).Value = oApplication.BranchId
                    params2_1(1).Value = oApplication.AppID
                    params2_1(2).Value = oData2.Rows(intLoop).Item("MasterTCID")
                    params2_1(3).Value = CInt(oData2.Rows(intLoop).Item("AGTCCLSequenceNo"))
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spAApplicationSaveEdit3_1", params2_1)
                Next
            End If

            If oData1.Rows.Count > 0 Then
                params1_1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params1_1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params1_1(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                For intLoop = 0 To oData1.Rows.Count - 1
                    params1_1(0).Value = oApplication.BranchId
                    params1_1(1).Value = oApplication.AppID
                    params1_1(2).Value = oData1.Rows(intLoop).Item("MasterTCID")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spAApplicationSaveEdit2_1", params1_1)
                Next
            End If

            If oData1.Rows.Count > 0 Then
                params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params1(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params1(3) = New SqlParameter("@PriorTo", SqlDbType.Char, 3)
                params1(4) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
                params1(5) = New SqlParameter("@IsMandatory", SqlDbType.Char, 1)
                params1(6) = New SqlParameter("@PromiseDate", SqlDbType.VarChar, 8)
                params1(7) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params1(8) = New SqlParameter("@Notes", SqlDbType.Text)
                For intLoop = 0 To oData1.Rows.Count - 1
                    params1(0).Value = oApplication.BranchId
                    params1(1).Value = oApplication.AppID
                    params1(2).Value = oData1.Rows(intLoop).Item("MasterTCID")
                    params1(3).Value = oData1.Rows(intLoop).Item("PriorTo")
                    params1(4).Value = oData1.Rows(intLoop).Item("IsChecked")
                    params1(5).Value = oData1.Rows(intLoop).Item("IsMandatory")
                    params1(6).Value = oData1.Rows(intLoop).Item("PromiseDate")
                    params1(7).Value = oApplication.BusinessDate
                    params1(8).Value = oData1.Rows(intLoop).Item("Notes")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveEdit2, params1)
                Next
            End If

            If oData2.Rows.Count > 0 Then
                params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params2(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params2(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params2(3) = New SqlParameter("@AGTCCLSequenceNo", SqlDbType.SmallInt)
                params2(4) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
                params2(5) = New SqlParameter("@PromiseDate", SqlDbType.VarChar, 8)
                params2(6) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params2(7) = New SqlParameter("@Notes", SqlDbType.Text)
                For intLoop = 0 To oData2.Rows.Count - 1
                    params2(0).Value = oApplication.BranchId
                    params2(1).Value = oApplication.AppID
                    params2(2).Value = oData2.Rows(intLoop).Item("MasterTCID")
                    params2(3).Value = CInt(oData2.Rows(intLoop).Item("AGTCCLSequenceNo"))
                    params2(4).Value = oData2.Rows(intLoop).Item("IsChecked")
                    params2(5).Value = oData2.Rows(intLoop).Item("PromiseDate")
                    params2(6).Value = oApplication.BusinessDate
                    params2(7).Value = oData2.Rows(intLoop).Item("Notes")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveEdit3, params2)
                Next
            End If

            If oData3.Rows.Count > 0 Then
                'params3_1(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                'params3_1(0).Value = oApplication.AppID
                'SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveEdit4_1, params3_1)

                params3(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params3(1) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
                For intLoop = 0 To oData3.Rows.Count - 1
                    params3(0).Value = oApplication.AppID
                    params3(1).Value = oData3.Rows(intLoop).Item("AgreementNo")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveEdit4, params3)
                Next
            End If
            transaction.Commit()
            oReturnValue.Err = ErrMessage
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("EditApplication", "ApplicationSaveEdit", exp.Message + exp.StackTrace)
            oReturnValue.Err = exp.Message
            Throw New Exception("Error On DataAccess.AccAcq.Customer.ApplicationSaveEdit")
            Return oReturnValue
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

    '=========================== EDIT ASSET DATA ==============================

    Public Function EditAssetDataGetInfoAssetData(ByVal oCustumClass As Parameter.Application) As Parameter.Application

        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = oCustumClass.ApplicationID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustumClass.strConnection, CommandType.StoredProcedure, oCustumClass.SpName, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("EditApplication", "EditAssetDataGetInfoAssetData", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("EditApplication.aspx.vb", "EditAssetDataGetInfoAssetData", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
        End Try

    End Function

    '=========================== GET ASSET DATA AWAL==============================
    Public Function AssetDataGetInfoAssetDataAwal(ByVal oCustumClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = oCustumClass.ApplicationID
        params(1) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 30)
        params(1).Value = oCustumClass.ProductOffID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustumClass.strConnection, CommandType.StoredProcedure, oCustumClass.SpName, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("ApplicationAwal", "AssetDataGetInfoAssetDataAwal", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("EditApplication.aspx.vb", "AssetDataGetInfoAssetDataAwal", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
        End Try
    End Function

End Class