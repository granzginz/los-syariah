
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class RCA : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const spPaging As String = "spApplicationPaging"
    Private Const spApplicationSaveAdd As String = "spApplicationSaveAdd"
    Private Const spApplicationSaveEdit2 As String = "spApplicationSaveEdit2"
    Private Const spApplicationSaveEdit3 As String = "spApplicationSaveEdit3"
    Private m_connection As SqlConnection
#End Region

 

#Region "REQ"

    Private Function getRCAList(cnn As String, ByVal params As IList(Of SqlParameter)) As Parameter.RCA
        Dim oReturnValue As New Parameter.RCA
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, "spRCAPaging", params.ToArray).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("REQ", "GetRCA", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetRCA(ByVal customClass As Parameter.RCA) As Parameter.RCA
        Dim oReturnValue As New Parameter.RCA
        Dim params As New List(Of SqlParameter)

        params.Add(New SqlParameter("@CurrentPage", SqlDbType.Int) With {.Value = customClass.CurrentPage})
        params.Add(New SqlParameter("@PageSize", SqlDbType.Int) With {.Value = customClass.PageSize})
        params.Add(New SqlParameter("@WhereCond", SqlDbType.VarChar) With {.Value = customClass.WhereCond})
        params.Add(New SqlParameter("@SortBy", SqlDbType.VarChar) With {.Value = customClass.SortBy})
        params.Add(New SqlParameter("@TotalRecords", SqlDbType.Int) With {.Direction = ParameterDirection.Output})
        params.Add(New SqlParameter("@WhereCond2", SqlDbType.VarChar) With {.Value = customClass.WhereCond2})
        params.Add(New SqlParameter("@ApplicationStep", SqlDbType.VarChar) With {.Value = "SCO"})
        'params.Add(New SqlParameter("@ApplicationStep", SqlDbType.VarChar) With {.Value = "SVR"})
        Return getRCAList(customClass.strConnection, params)

    End Function


    Public Function GetApprovalBM(ByVal customClass As Parameter.RCA) As Parameter.RCA
        Dim oReturnValue As New Parameter.RCA
        Dim params As New List(Of SqlParameter)

        params.Add(New SqlParameter("@CurrentPage", SqlDbType.Int) With {.Value = customClass.CurrentPage})
        params.Add(New SqlParameter("@PageSize", SqlDbType.Int) With {.Value = customClass.PageSize})
        params.Add(New SqlParameter("@WhereCond", SqlDbType.VarChar) With {.Value = customClass.WhereCond})
        params.Add(New SqlParameter("@SortBy", SqlDbType.VarChar) With {.Value = customClass.SortBy})
        params.Add(New SqlParameter("@TotalRecords", SqlDbType.Int) With {.Direction = ParameterDirection.Output})
        params.Add(New SqlParameter("@WhereCond2", SqlDbType.VarChar) With {.Value = customClass.WhereCond2})
        params.Add(New SqlParameter("@ApplicationStep", SqlDbType.VarChar) With {.Value = "REQ"})

        Return getRCAList(customClass.strConnection, params)

    End Function


    Public Function GetCreditAssesmentList(ByVal customClass As Parameter.RCA) As Parameter.RCA
        Dim oReturnValue As New Parameter.RCA
        Dim params As New List(Of SqlParameter)

        params.Add(New SqlParameter("@CurrentPage", SqlDbType.Int) With {.Value = customClass.CurrentPage})
        params.Add(New SqlParameter("@PageSize", SqlDbType.Int) With {.Value = customClass.PageSize})
        params.Add(New SqlParameter("@WhereCond", SqlDbType.VarChar) With {.Value = customClass.WhereCond})
        params.Add(New SqlParameter("@SortBy", SqlDbType.VarChar) With {.Value = customClass.SortBy})
        params.Add(New SqlParameter("@TotalRecords", SqlDbType.Int) With {.Direction = ParameterDirection.Output})
        params.Add(New SqlParameter("@WhereCond2", SqlDbType.VarChar) With {.Value = customClass.WhereCond2})
        params.Add(New SqlParameter("@ApplicationStep", SqlDbType.VarChar) With {.Value = "ABM"})
        Return getRCAList(customClass.strConnection, params)

    End Function

    Public Function ApprovalBMSave(ByVal oRCA As Parameter.RCA, Optional isApr As Boolean = True) As Parameter.RCA

        Dim oReturnValue As New Parameter.RCA
        Dim oConnection As New SqlConnection(oRCA.strConnection)
        Dim oApprovalID As New SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim transaction As SqlTransaction = Nothing
        Dim params As New List(Of SqlParameter)
        'Dim params(7) As SqlParameter

        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            transaction = oConnection.BeginTransaction

            params.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = oRCA.BranchId})
            params.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20) With {.Value = oRCA.AppID})
            params.Add(New SqlParameter("@Notes", SqlDbType.Text) With {.Value = oRCA.Notes})
            params.Add(New SqlParameter("@Argumentasi", SqlDbType.Text) With {.Value = oRCA.Argumentasi})
            params.Add(New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50) With {.Value = "-"})
            params.Add(New SqlParameter("@BusinessDate", SqlDbType.DateTime) With {.Value = oRCA.BusinessDate})
            params.Add(New SqlParameter("@AgreementNo", SqlDbType.Char, 20) With {.Value = oRCA.AgreementNo})
            params.Add(New SqlParameter("@loginId", SqlDbType.Char, 20) With {.Value = oRCA.LoginId})



            Dim errPrm = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            params.Add(errPrm)

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spApprovalBMSaveAdd", params.ToArray)
            oReturnValue.Err = CType(errPrm.Value, String)

            transaction.Commit()
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("ABM", "ApprovalBMSave", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)

        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try

    End Function


    Public Function RCASave(ByVal oRCA As Parameter.RCA, ByVal oData1 As DataTable, _
     ByVal oData2 As DataTable) As Parameter.RCA
        Dim oReturnValue As New Parameter.RCA
        Dim oConnection As New SqlConnection(oRCA.strConnection)
        Dim oApprovalID As New SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim transaction As SqlTransaction = Nothing
        Dim intLoop As Integer

        Dim params(10) As SqlParameter
        Dim params1(8) As SqlParameter
        Dim params2(7) As SqlParameter

        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            transaction = oConnection.BeginTransaction
            '/*
            'With oEntitiesApproval
            '    .ApprovalTransaction = transaction
            '    .BranchId = oRCA.BranchId
            '    .SchemeID = oRCA.SchemeID '
            '    .RequestDate = oRCA.BusinessDate
            '    .TransactionNo = oRCA.AppID
            '    .ApprovalNote = oRCA.Notes
            '    .ApprovalValue = oRCA.RefundAmount
            '    .UserRequest = oRCA.LoginId
            '    .UserApproval = oRCA.RequestBy
            '    .AprovalType = Parameter.Approval.ETransactionType.RCA_Approval
            '    .Argumentasi = oRCA.Argumentasi
            'End With

            'oRCA.ApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)
            '*/
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oRCA.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = oRCA.AppID
            params(2) = New SqlParameter("@Notes", SqlDbType.Text)
            params(2).Value = oRCA.Notes
            params(3) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
            params(3).Value = "-" 'oRCA.ApprovalNo
            params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(4).Value = oRCA.BusinessDate

            params(5) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
            params(5).Value = oRCA.AgreementNo

            params(6) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(6).Direction = ParameterDirection.Output

            params(7) = New SqlParameter("@FundingCoyID", SqlDbType.Char, 20)
            params(7).Value = oRCA.FundingCoyId

            params(8) = New SqlParameter("@FundingContractNO", SqlDbType.Char, 20)
            params(8).Value = oRCA.FundingContractNo

            params(9) = New SqlParameter("@FundingBatchNO", SqlDbType.Char, 20)
            params(9).Value = oRCA.FundingBatchNo

            params(10) = New SqlParameter("@appmgr", SqlDbType.Char, 50)
            params(10).Value = getReferenceDBName()


            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spRCASaveAdd", params)
            oReturnValue.Err = CType(params(6).Value, String)

            If oReturnValue.Err = "" Then
                If oData1.Rows.Count > 0 Then
                    params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                    params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                    params1(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                    params1(3) = New SqlParameter("@PriorTo", SqlDbType.Char, 3)
                    params1(4) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
                    params1(5) = New SqlParameter("@IsMandatory", SqlDbType.Char, 1)
                    params1(6) = New SqlParameter("@PromiseDate", SqlDbType.VarChar, 8)
                    params1(7) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                    params1(8) = New SqlParameter("@Notes", SqlDbType.Text)
                    For intLoop = 0 To oData1.Rows.Count - 1
                        params1(0).Value = oRCA.BranchId
                        params1(1).Value = oRCA.AppID
                        params1(2).Value = oData1.Rows(intLoop).Item("MasterTCID")
                        params1(3).Value = oData1.Rows(intLoop).Item("PriorTo")
                        params1(4).Value = oData1.Rows(intLoop).Item("IsChecked")
                        params1(5).Value = oData1.Rows(intLoop).Item("IsMandatory")
                        params1(6).Value = oData1.Rows(intLoop).Item("PromiseDate")
                        params1(7).Value = oRCA.BusinessDate
                        params1(8).Value = oData1.Rows(intLoop).Item("Notes")
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveEdit2, params1)
                    Next
                End If
                If oData2.Rows.Count > 0 Then
                    params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                    params2(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                    params2(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                    params2(3) = New SqlParameter("@AGTCCLSequenceNo", SqlDbType.SmallInt)
                    params2(4) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
                    params2(5) = New SqlParameter("@PromiseDate", SqlDbType.VarChar, 8)
                    params2(6) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                    params2(7) = New SqlParameter("@Notes", SqlDbType.Text)
                    For intLoop = 0 To oData2.Rows.Count - 1
                        params2(0).Value = oRCA.BranchId
                        params2(1).Value = oRCA.AppID
                        params2(2).Value = oData2.Rows(intLoop).Item("MasterTCID")
                        params2(3).Value = CInt(oData2.Rows(intLoop).Item("AGTCCLSequenceNo"))
                        params2(4).Value = oData2.Rows(intLoop).Item("IsChecked")
                        params2(5).Value = oData2.Rows(intLoop).Item("PromiseDate")
                        params2(6).Value = oRCA.BusinessDate
                        params2(7).Value = oData2.Rows(intLoop).Item("Notes")
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveEdit3, params2)
                    Next
                End If
                transaction.Commit()
            End If
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("REQ", "RCASave", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
            'Return oReturnValue
            'Throw New Exception("Error On DataAccess.AccAcq.Customer.RCASave")
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Function
#End Region
End Class
