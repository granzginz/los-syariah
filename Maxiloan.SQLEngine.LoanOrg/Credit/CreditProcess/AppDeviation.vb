﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class AppDeviation : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    'Stored Procedure name        
    Private Const LIST_SELECT As String = "spAppDeviationPaging"
    Private Const LIST_SELECT_VIEW As String = "spAppDeviationView"
    Private Const LIST_UPDATE As String = "spAppDeviationPrint"

    Private Const LIST_SELECT_APPROVAL_PATH As String = "spGetApprovalPathTreeBySchemeID"
    Private Const LIST_SELECT_TREE_MEMBER As String = "spGetApprovalPathTreeMemberSchemeID"
#End Region

    Public Function GetAppDeviation(ByVal oCustomClass As Parameter.AppDeviation) As Parameter.AppDeviation
        Dim oReturnValue As New Parameter.AppDeviation
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(5).Value = oCustomClass.BranchId
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.CreditProses.GetAppDeviation")
        End Try
    End Function

    Public Function GetDeviationView(ByVal oCustomClass As Parameter.AppDeviation) As Parameter.AppDeviation
        Dim oReturnValue As New Parameter.AppDeviation
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.ApplicationId
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT_VIEW, params).Tables(0)            
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.CreditProses.GetDeviationView")
        End Try
    End Function

    Public Sub AppDeviationPrint(ByVal ocustomClass As Parameter.AppDeviation)
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.ApplicationId
        params(1) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
        params(1).Value = Now.Date

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.CreditProses.AppDeviationPrint")
        End Try
    End Sub

    Public Function GetApprovalPath(ByVal oCustomClass As Parameter.ApprovalScheme) As Parameter.ApprovalScheme
        Dim oReturnValue As New Parameter.ApprovalScheme
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.Char, 5)
        params(0).Value = oCustomClass.ApprovalSchemeID
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT_APPROVAL_PATH, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetApprovalPathTreeMember(ByVal oCustomClass As Parameter.ApprovalScheme) As Parameter.ApprovalScheme
        Dim oReturnValue As New Parameter.ApprovalScheme
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.Char, 5)
        params(0).Value = oCustomClass.ApprovalSchemeID
        params(1) = New SqlParameter("@ApprovalSeqNum", SqlDbType.Int)
        params(1).Value = oCustomClass.ApprovalSeqNum

        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT_TREE_MEMBER, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
