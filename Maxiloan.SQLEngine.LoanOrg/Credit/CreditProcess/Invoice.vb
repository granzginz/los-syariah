

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class Invoice : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spInvoicePaging"
    Private Const LIST_AGREEMENT As String = "spInvoiceListAgreement"
    Private Const LIST_INVOICEATPM As String = "spInvoiceATPMList"
    Private Const LIST_AGREEMENTINVATPM As String = "spInvoiceATPMListAgreement"
    Private Const LIST_MAXBACKDATE As String = "spInvoiceMaxBackDate"
    Private Const LIST_SUPPTOPDAYS As String = "spInvoiceSuppTopDays"
    Private Const SAVE As String = "spInvoiceSave"
    Private Const SAVEINVATM As String = "spInvoiceATPMSave"
    Private Const SAVE2 As String = "spInvoiceSave2"
    Private Const spGenGetInvATPM As String = "spGetInvATPM"
    Private Const spListRecATPM As String = "spRecieveATPMPagging"
    Private Const LIST_SELECT_VIEW As String = ""
    Private Const SpGetInvoiceNo As String = "SpGetInvoiceNo"
    Private m_connection As SqlConnection
#End Region

#Region "InvoicePaging"
    Public Function InvoicePaging(ByVal customClass As Parameter.Invoice) As Parameter.Invoice
        Dim oReturnValue As New Parameter.Invoice

        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(4).Value = customClass.BranchId
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(5).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Invoice", "InvoicePaging", exp.Message + exp.StackTrace)
            Return Nothing
        End Try
    End Function
#End Region

#Region "GetInvoiceAgreementList"
    Public Function GetInvoiceAgreementList(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice
        Dim oReturnValue As New Parameter.Invoice
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = oCustomClass.BranchId
        params(1) = New SqlParameter("@SupplierID", SqlDbType.VarChar, 10)
        params(1).Value = oCustomClass.SupplierID
        params(2) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(2).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_AGREEMENT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(2).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Invoice", "GetInvoiceAgreementList", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "GetInvoiceATPMList"
    Public Function GetInvoiceATPMList(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice
        Dim oReturnValue As New Parameter.Invoice
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = oCustomClass.BranchId
        params(1) = New SqlParameter("@SupplierID", SqlDbType.VarChar, 10)
        params(1).Value = oCustomClass.SupplierID
        params(2) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(2).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_INVOICEATPM, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(2).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Invoice", "GetInvoiceATPMList", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region
    Public Function GetInvoiceATPMView(ByVal oCustomClass As Parameter.AppDeviation) As Parameter.AppDeviation
        Dim oReturnValue As New Parameter.AppDeviation
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = oCustomClass.BranchId
        params(1) = New SqlParameter("@SupplierID", SqlDbType.VarChar, 10)
        params(1).Value = oCustomClass.SupplierId
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT_VIEW, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.CreditProses.GetDeviationView")
        End Try
    End Function

    Public Function GetInvoiceATPMAgreementList(ByVal oCustomClass As Parameter.Invoice) As Parameter.Invoice
        Dim oReturnValue As New Parameter.Invoice
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@WhereCond2", SqlDbType.VarChar)
        params(5).Value = oCustomClass.WhereCond2
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_AGREEMENTINVATPM, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.CreditProses.GetAppDeviation")
        End Try
    End Function
    Function ReceiveATPMList(ByVal customclass As Parameter.Invoice) As Parameter.Invoice
        Dim oReturnValue As New Parameter.Invoice
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            oReturnValue.ListATPM = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListRecATPM, params).Tables(0)
            oReturnValue.TotalRecord = CInt(params(4).Value)
            Return oReturnValue
        Catch exp As Exception
            WriteException("AgreementList", "ListAgreement", exp.Message + exp.StackTrace)
        End Try
    End Function
#Region "InvoiceATPMSave"
    Public Function InvoiceATPMSave(ByVal customClass As Parameter.Invoice) As String

        Dim objCon As New SqlConnection(customClass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim ErrMessage2 As String = ""
        Dim ErrMessage3 As String = ""
        Dim params() As SqlParameter = New SqlParameter(6) {}
        ' Dim params1() As SqlParameter = New SqlParameter(13) {}
        Dim oData1 As New DataTable
        Dim intLoop As Integer


        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            oData1 = customClass.ListData

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@SupplierATPM", SqlDbType.Char, 10)
            params(1).Value = customClass.SupplierATPM
            params(2) = New SqlParameter("@InvoiceNo", SqlDbType.Char, 20)
            params(2).Value = customClass.InvoiceNo
            params(3) = New SqlParameter("@InvoiceDate", SqlDbType.DateTime)
            params(3).Value = customClass.InvoiceDate
            params(4) = New SqlParameter("@TotPPN", SqlDbType.Decimal)
            params(4).Value = customClass.TotPPN
            params(5) = New SqlParameter("@TotPPh23", SqlDbType.Decimal)
            params(5).Value = customClass.TotPPh23
            params(6) = New SqlParameter("@TotalInvAmount", SqlDbType.Decimal)
            params(6).Value = customClass.TotalInvAmount

            ''sub
            'params(10) = New SqlParameter("@TotSubsidiBungaATPM", SqlDbType.Decimal)
            'params(10).Value = customClass.TotSubsidiBungaATPM
            'params(11) = New SqlParameter("@TotSubsidiAngsuranATPM", SqlDbType.Decimal)
            'params(11).Value = customClass.TotSubsidiAngsuranATPM
            'params(12) = New SqlParameter("@TotSubsidiUangmukaATPM", SqlDbType.Decimal)
            'params(12).Value = customClass.TotSubsidiUangmukaATPM

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SAVEINVATM, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Invoice", "InvoiceSave", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function


    Public Function InvoiceSuppSave(cnn As String, ByVal invoiceSupp As Parameter.InvoiceSupp) As String
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Dim objCon As New SqlConnection(cnn)
        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = invoiceSupp.BranchID})
            params.Add(New SqlParameter("@SupplierID", SqlDbType.Char, 10) With {.Value = invoiceSupp.SupplierID})
            params.Add(New SqlParameter("@InvoiceNo", SqlDbType.Char, 50) With {.Value = invoiceSupp.InvoiceNo})
            params.Add(New SqlParameter("@InvoiceDate", SqlDbType.DateTime) With {.Value = invoiceSupp.InvoiceDate})
            params.Add(New SqlParameter("@InvAmountOriginal", SqlDbType.Decimal) With {.Value = invoiceSupp.InvAmountOriginal + invoiceSupp.NilPelunasan})

            params.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20) With {.Value = invoiceSupp.ApplicationID})
            params.Add(New SqlParameter("@APType", SqlDbType.Char, 4) With {.Value = invoiceSupp.APType})
            params.Add(New SqlParameter("@APDueDate", SqlDbType.DateTime) With {.Value = invoiceSupp.APDueDate})
            params.Add(New SqlParameter("@SupplierBankid", SqlDbType.Char, 4) With {.Value = invoiceSupp.SuppBankId})
            params.Add(New SqlParameter("@IsGabungRefundSupplier", SqlDbType.Bit) With {.Value = invoiceSupp.IsGabungRefundSupplier})
            params.Add(New SqlParameter("@FirstInstallmentDate", SqlDbType.DateTime) With {.Value = invoiceSupp.FirstInstallmentDate})
            params.Add(New SqlParameter("@tpDeduction", SqlDbType.Structured) With {.Value = invoiceSupp.ToSuppDeductionDataTable})
            Dim errPrm = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            params.Add(errPrm)

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spInvoiceSuppSave", params.ToArray)

            If CType(errPrm.Value, String) <> "" Then
                Return CType(errPrm.Value, String)
            End If
            transaction.Commit()
            Return ""

        Catch exp As Exception
            transaction.Rollback()
            WriteException("Invoice", "InvoiceSave", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function
#End Region

    Public Function GetInvoiceNo(ByVal customclass As Parameter.Invoice) As Parameter.Invoice
        Dim oReturnValue As New Parameter.Invoice
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(0).Value = customclass.BusinessDate
        params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(1).Value = customclass.BranchId
        params(2) = New SqlParameter("@InvoiceNo", SqlDbType.Char, 20)
        params(2).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, SpGetInvoiceNo, params)
            oReturnValue.InvoiceNo = CType(params(2).Value, String)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Invoice.GetInvoiceNo")
        End Try
    End Function

    Public Sub SaveATPMOtorisasi(ByVal customclass As Parameter.Invoice)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(1) {}

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            'params(0) = New SqlParameter("@VoucherNo", SqlDbType.VarChar, 20)
            'params(0).Value = customclass.VoucherNO

            'params(1) = New SqlParameter("@loginid", SqlDbType.VarChar, 12)
            'params(1).Value = customclass.LoginId

            'SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spATPMOtorisasi", params)
            'objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("ATPMOtorisasi", "SavespATPMOtorisasi", exp.Message + exp.StackTrace)
        End Try
    End Sub

    Public Sub RejectOtorisasi(ByVal oCustomClass As Parameter.Invoice)
        Dim params(0) As SqlParameter
        Dim objTrans As SqlTransaction
        params(0) = New SqlParameter("@VoucherNO", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.VoucherNO

        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spOtorisasiRejectATPM", params)
            objTrans.Commit()
        Catch exp As Exception
            objTrans.Rollback()
            WriteException("spOtorisasiRejectATPM", "spOtorisasiRejectATPM", exp.Message + exp.StackTrace)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Sub

    Public Sub EditOtorisasi(ByVal oCustomClass As Parameter.Invoice)
        Dim params(0) As SqlParameter
        Dim objTrans As SqlTransaction
        params(0) = New SqlParameter("@VoucherNO", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.VoucherNO

        Try
            m_connection = New SqlConnection(oCustomClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spOtorisasiEditATPM", params)
            objTrans.Commit()
        Catch exp As Exception
            objTrans.Rollback()
            WriteException("spOtorisasiEditATPM", "spOtorisasiEditATPM", exp.Message + exp.StackTrace)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Sub
#Region "GetInvoiceMaxBackDate"
    Public Function GetInvoiceMaxBackDate(ByVal customClass As Parameter.Invoice) As Integer
        Dim oReturnValue As New Parameter.Invoice
        Dim TotalDay As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@TotalDay", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_MAXBACKDATE, params)
            TotalDay = CInt(params(0).Value)
            Return TotalDay
        Catch exp As Exception
            WriteException("Invoice", "GetInvoiceMaxBackDate", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "GetInvoiceSuppTopDays"
    Public Function GetInvoiceSuppTopDays(ByVal customClass As Parameter.Invoice) As Integer
        Dim oReturnValue As New Parameter.Invoice
        Dim TotalDay As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@TotalDay", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_SUPPTOPDAYS, params)
            TotalDay = CInt(params(0).Value)
            Return TotalDay
        Catch exp As Exception
            WriteException("Invoice", "GetInvoiceSuppTopDays", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "InvoiceSave"
    Public Function InvoiceSave(ByVal customClass As Parameter.Invoice) As String

        Dim objCon As New SqlConnection(customClass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim ErrMessage2 As String = ""
        Dim ErrMessage3 As String = ""
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Dim params1() As SqlParameter = New SqlParameter(13) {}
        Dim oData1 As New DataTable
        Dim intLoop As Integer


        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            oData1 = customClass.ListData

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(1).Value = customClass.SupplierID
            params(2) = New SqlParameter("@InvoiceNo", SqlDbType.Char, 20)
            params(2).Value = customClass.InvoiceNo
            params(3) = New SqlParameter("@InvoiceDate", SqlDbType.DateTime)
            params(3).Value = customClass.InvoiceDate
            params(4) = New SqlParameter("@InvoiceAmount", SqlDbType.Decimal)
            params(4).Value = customClass.InvoiceAmount
            params(5) = New SqlParameter("@Err", SqlDbType.VarChar, 10)
            params(5).Direction = ParameterDirection.Output
            params(6) = New SqlParameter("@InvoiceNoX", SqlDbType.Char, 20)
            params(6).Direction = ParameterDirection.Output
            params(7) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            params(7).Value = customClass.BusinessDate


            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SAVE, params)

            Dim strInvoiceNoX As String = params(6).Value

            For intLoop = 0 To oData1.Rows.Count - 1
                params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params1(0).Value = customClass.BranchId
                params1(1) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
                params1(1).Value = customClass.SupplierID
                params1(2) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params1(2).Value = oData1.Rows(intLoop)("AplicationID")
                params1(3) = New SqlParameter("@CustomerName", SqlDbType.VarChar, 50)
                params1(3).Value = oData1.Rows(intLoop)("CustomerName")
                params1(4) = New SqlParameter("@InvoiceNo", SqlDbType.Char, 20)
                'params1(4).Value = customClass.InvoiceNo
                params1(4).Value = strInvoiceNoX
                params1(5) = New SqlParameter("@InvoiceDate", SqlDbType.DateTime)
                params1(5).Value = customClass.InvoiceDate
                params1(6) = New SqlParameter("@InvoiceAmount", SqlDbType.Decimal)
                params1(6).Value = customClass.InvoiceAmount
                params1(7) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
                params1(7).Value = oData1.Rows(intLoop)("AgreementNo")
                params1(8) = New SqlParameter("@APAmount", SqlDbType.Decimal)
                params1(8).Value = oData1.Rows(intLoop)("APAmount")
                params1(9) = New SqlParameter("@DeliveryOrderDate", SqlDbType.DateTime)
                params1(9).Value = oData1.Rows(intLoop)("DeliveryOrderDate")
                params1(10) = New SqlParameter("@ApplicationStep", SqlDbType.Char, 3)
                params1(10).Value = oData1.Rows(intLoop)("ApplicationStep")
                params1(11) = New SqlParameter("@Err", SqlDbType.VarChar, 10)
                params1(11).Direction = ParameterDirection.Output
                params1(12) = New SqlParameter("@APType", SqlDbType.Char, 4)
                params1(12).Value = oData1.Rows(intLoop)("APType")
                params1(13) = New SqlParameter("@APDueDate", SqlDbType.SmallDateTime)
                params1(13).Value = customClass.APDueDate

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SAVE2, params1)
            Next

            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Invoice", "InvoiceSave", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)            
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function
 
#End Region



    Public Function LoadCombo(cnn As String, ty As String, Optional id As String = "") As IList(Of Parameter.CommonValueText)
        Dim str As String = ""

        Select Case (ty.ToUpper)
            Case "POTONGPENCAIRAN"
                str = "select ID as Value ,description as Text from TblPotongPencairan order by description"

            Case "SUPPBANK"
                str = String.Format("select supplierbankid as Value ,isnull(BankName,'') + ' - ' + ISNULL(BankBranchName,'') +' - '+ ISNULL(SupplierAccountNo,'') +' - '+ ISNULL(SupplierAccountName,'') as Text  from supplieraccount " + _
                    "left join bankmaster on supplieraccount.supplierbankid= bankmaster.bankId " + _
                    "left join bankbranchmaster on  bankbranchmaster.BankBranchid=SupplierBankBranchID  where supplierid = '{0}'  order by DefaultAccount desc", id)
        End Select


        Dim result = New List(Of Parameter.CommonValueText)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.Text, str)


        If (reader.HasRows) Then
            If (reader Is Nothing) Then
                Return Nothing
            End If
            If (reader.IsClosed) Then
                Return Nothing
            End If

            Dim _cID = reader.GetOrdinal("Value")
            Dim _desc = reader.GetOrdinal("Text")


            While (reader.Read())
                result.Add(New Parameter.CommonValueText(
                          IIf(reader.IsDBNull(_cID), String.Empty, reader.GetString(_cID)),
                          IIf(reader.IsDBNull(_desc), "-", reader.GetString(_desc).ToString())))
            End While

        End If
        Return result
    End Function


    Public Function GetNoInvoiceATPM(ByVal oInATPM As Parameter.Invoice) As Parameter.Invoice
        Dim oReturnValue As New Parameter.Invoice
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(0).Value = oInATPM.BusinessDate
        params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(1).Value = oInATPM.BranchId
        params(2) = New SqlParameter("@NoIATPM", SqlDbType.Char, 20)
        params(2).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(oInATPM.strConnection, CommandType.StoredProcedure, spGenGetInvATPM, params)
            oReturnValue.InvoiceNoATPM = CType(params(2).Value, String)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Marketing.Supplier.GetSupplierID")
        End Try
    End Function

End Class
