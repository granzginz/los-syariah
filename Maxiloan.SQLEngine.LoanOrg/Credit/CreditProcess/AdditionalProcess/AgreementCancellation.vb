
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class AgreementCancellation : Inherits maxiloan.SQLEngine.DataAccessBase
#Region "Constants"
    Private STR_SP_PROCESS_AGREEMENT_CANCELLATION As String = "spProcessAgreementCancellation"
#End Region

    Public Sub ProcessAgreementCancellation(ByVal oCustomClass As Parameter.AgreementCancellation)
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        If conn.State = ConnectionState.Closed Then conn.Open()
        Dim transaction As SqlTransaction = Nothing

        Dim params() As SqlParameter = New SqlParameter(4) {}

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            With oCustomClass
                params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                params(0).Value = oCustomClass.BranchId

                params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
                params(1).Value = oCustomClass.ApplicationID

                params(2) = New SqlParameter("@CancellationDate", SqlDbType.DateTime)
                params(2).Value = oCustomClass.Cancellationdate

                params(3) = New SqlParameter("@CancellationReasonID", SqlDbType.VarChar, 10)
                params(3).Value = oCustomClass.CancellationReasonID

                params(4) = New SqlParameter("@NotesOfCancellation", SqlDbType.Text)
                params(4).Value = oCustomClass.NotesOfCancellation
            End With
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, STR_SP_PROCESS_AGREEMENT_CANCELLATION, params)

            'copy application to new application ID
            If oCustomClass.isCopyApplication Then CopyApplication(transaction, oCustomClass)

            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("AgreementCancellation", "ProcessAgreementCancellation", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Sub

    Private Sub CopyApplication(ByVal trans As SqlTransaction, ByVal data As Parameter.AgreementCancellation)
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(0).Value = data.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(1).Value = data.ApplicationID

        params(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(2).Value = data.BusinessDate
        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "spCopyApplication", params)
    End Sub

End Class
