﻿Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

Public Class KonfirmasiDealer
    Public Function KonfirmasiDealerPaging(ByVal customClass As Parameter.KonfirmasiDealer) As Parameter.KonfirmasiDealer
        Dim oReturnValue As New Parameter.KonfirmasiDealer
        Dim params() As SqlParameter = New SqlParameter(5) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(4).Value = customClass.BranchId
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output

        Try
            oReturnValue.dsKonfirmasiDealer = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spKonfirmasiDealerPaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(5).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function ValidationSerialNo(ByVal customClass As Parameter.KonfirmasiDealer) As DataTable
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim dt As New DataTable

        params(0) = New SqlParameter("@applicationID", SqlDbType.VarChar)
        params(0).Value = customClass.ApplicationID
        params(1) = New SqlParameter("@SerialNo1", SqlDbType.VarChar, 50)
        params(1).Value = customClass.SerialNo1
        params(2) = New SqlParameter("@SerialNo2", SqlDbType.VarChar, 50)
        params(2).Value = customClass.SerialNo2
      

        Try
            dt = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spValidationSerialNo", params).Tables(0)
            Return dt
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Sub KonfirmasiDealerSave(ByVal CustomClass As Parameter.KonfirmasiDealer)
        Dim oConn As New SqlConnection(CustomClass.strConnection)

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        Dim Transaction As SqlTransaction = oConn.BeginTransaction

        Try
            Dim params() As SqlParameter = New SqlParameter(10) {}

            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = CustomClass.ApplicationID
            params(1) = New SqlParameter("@TanggalKonfirmasi", SqlDbType.SmallDateTime)
            params(1).Value = CustomClass.TanggalKonfirmasi
            params(2) = New SqlParameter("@SerialNo1", SqlDbType.VarChar, 50)
            params(2).Value = CustomClass.SerialNo1
            params(3) = New SqlParameter("@SerialNo2", SqlDbType.VarChar, 50)
            params(3).Value = CustomClass.SerialNo2
            params(4) = New SqlParameter("@TanggalKontrak", SqlDbType.SmallDateTime)
            params(4).Value = CustomClass.TanggalKontrak
            'params(5) = New SqlParameter("@TanggalEfektif", SqlDbType.SmallDateTime)
            'params(5).Value = CustomClass.TanggalEfektif
            params(5) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            params(5).Value = CustomClass.BusinessDate
            params(6) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(6).Value = CustomClass.BranchId
            params(7) = New SqlParameter("@Warna", SqlDbType.VarChar, 25)
            params(7).Value = CustomClass.Warna
            params(8) = New SqlParameter("@TahunKendaraan", SqlDbType.SmallInt)
            params(8).Value = CustomClass.TahunKendaraan
            params(9) = New SqlParameter("@NamaSTNK", SqlDbType.VarChar, 100)
            params(9).Value = CustomClass.NamaSTNK
            params(10) = New SqlParameter("@FirstInstallment", SqlDbType.Char, 2)
            params(10).Value = CustomClass.FirstInstallment


            SqlHelper.ExecuteNonQuery(Transaction, CommandType.StoredProcedure, "spKonfirmasiDealerSave", params)



            'Update installment schedule dueDate
            'Dim par() As SqlParameter = New SqlParameter(2) {}

            'par(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            'par(0).Value = CustomClass.ApplicationID
            'par(1) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            'par(1).Value = CustomClass.BusinessDate
            'par(2) = New SqlParameter("@TanggalEfektif", SqlDbType.SmallDateTime)
            'par(2).Value = CustomClass.TanggalEfektif

            'SqlHelper.ExecuteNonQuery(Transaction, CommandType.StoredProcedure, "spInstallmentSchDueDateUpdate", par)




            Transaction.Commit()
        Catch ex As Exception
            Transaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub
End Class
