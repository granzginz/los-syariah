

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class _DO : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spDOPaging"
    Private Const GET_PO_NUMBER As String = "spGetPONo"
    Private Const GET_DO_PROCESS As String = "spDOProcess"
    Private Const GET_DO_ASSETDOC As String = "spDOAssetDoc"
    Private Const GET_BACKDATE As String = "spDOCheckBackDate"
    Private Const GET_ATTRIBUTE As String = "spGetAttribute"
    Private Const SAVE_DO As String = "spDOSave"
    Private Const SAVE_DO1 As String = "spDOSave1"
    Private Const SAVE_DO2 As String = "spDOSave2"
    Private Const SAVE_DO3 As String = "spDOSave3"
    Private Const SAVE_DO4 As String = "spDOSave4"
    Private Const SAVE_DO5 As String = "spDOSave5"
    Private Const CHECK_SERIAL As String = "spDOCheckSerial"
    Private Const CHECK_ATTRIBUTE As String = "spDOCheckAttribute"
    Private Const CHECK_ASSETDOC As String = "spDOCheckAssetDoc"

#End Region

#Region "Check"
    Public Function CheckSerial(ByVal oCustomClass As Parameter._DO) As Parameter._DO
        Dim oReturnValue As New Parameter._DO
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
        params(0).Value = oCustomClass.AssetTypeID
        params(1) = New SqlParameter("@Serial1", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.SerialNo1
        params(2) = New SqlParameter("@Serial2", SqlDbType.VarChar, 50)
        params(2).Value = oCustomClass.SerialNo2
        params(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(3).Value = oCustomClass.BranchId
        params(4) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(4).Value = oCustomClass.ApplicationID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, CHECK_SERIAL, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("_DO", "CheckSerial", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function CheckAttribute(ByVal oCustomClass As Parameter._DO) As Parameter._DO
        Dim oReturnValue As New Parameter._DO
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
        params(0).Value = oCustomClass.AssetTypeID
        params(1) = New SqlParameter("@Content", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.Input
        params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(2).Value = oCustomClass.BranchId
        params(3) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(3).Value = oCustomClass.ApplicationID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, CHECK_ATTRIBUTE, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("_DO", "CheckAttribute", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function CheckAssetDoc(ByVal oCustomClass As Parameter._DO) As Parameter._DO
        Dim oReturnValue As New Parameter._DO
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
        params(0).Value = oCustomClass.AssetTypeID
        params(1) = New SqlParameter("@Content", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.Input
        params(2) = New SqlParameter("@AssetDocID", SqlDbType.VarChar, 50)
        params(2).Value = oCustomClass.AssetDocID
        params(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(3).Value = oCustomClass.BranchId
        params(4) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(4).Value = oCustomClass.ApplicationID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, CHECK_ASSETDOC, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("_DO", "CheckAssetDoc", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region

#Region "DOPaging"
    Public Function DOPaging(ByVal customClass As Parameter._DO) As Parameter._DO
        Dim oReturnValue As New Parameter._DO

        Dim params() As SqlParameter = New SqlParameter(6) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(4).Value = customClass.BranchId
        params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(5).Value = customClass.BusinessDate
        params(6) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(6).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(6).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("_DO", "DOPaging", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region

#Region "Get_PONumber"
    Public Function Get_PONumber(ByVal customClass As Parameter._DO) As String
        Dim Result As String
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customClass.ApplicationID
        params(2) = New SqlParameter("@PONumber", SqlDbType.VarChar, 50)
        params(2).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, GET_PO_NUMBER, params)
            Result = CType(params(2).Value, String)
            If Result <> "" Then
                Return Result
            End If
            Return ""
        Catch exp As Exception
            WriteException("_DO", "Get_PONumber", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region

#Region "Get_DOProcess"
    Public Function Get_DOProcess(ByVal customClass As Parameter._DO) As Parameter._DO
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = customClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(1).Value = customClass.BranchId

        Try
            Dim oReturnValue As New Parameter._DO
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, GET_DO_PROCESS, params)
            If reader.Read Then
                With oReturnValue
                    .SerialNo1Label = reader("SerialNo1Label").ToString
                    .SerialNo2Label = reader("SerialNo2Label").ToString
                    .SerialNo1 = reader("SerialNo1").ToString
                    .SerialNo2 = reader("SerialNo2").ToString
                    .UsedNew = reader("UsedNew").ToString
                    .AssetTypeID = reader("AssetTypeID").ToString
                    .ManufacturingYear = CInt(reader("ManufacturingYear"))
                    .OldOwnerAsset = reader("OldOwnerAsset").ToString
                    .OldOwnerAddress = reader("OldOwnerAddress").ToString
                    .OldOwnerRT = reader("OldOwnerRT").ToString
                    .OldOwnerRW = reader("OldOwnerRW").ToString
                    .OldOwnerKelurahan = reader("OldOwnerKelurahan").ToString
                    .OldOwnerKecamatan = reader("OldOwnerKecamatan").ToString
                    .OldOwnerCity = reader("OldOwnerCity").ToString
                    .OldOwnerZipCode = reader("OldOwnerZipCode").ToString
                    .TaxDate = CDate(reader("TaxDate"))
                    .Notes = reader("Notes").ToString
                    .Warna = reader("Warna").ToString
                    .Nopol = reader("NoPolisi").ToString
                    .CustomerType = reader("CustomerType").ToString
                    .AssetOrigination = reader("Origination").ToString
                    .TipeAplikasi = reader("TipeAplikasi").ToString
                End With


            End If
            reader.Close()
            Return oReturnValue
        Catch exp As Exception
            WriteException("_DO", "Get_DOProcess", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetTypeEdit")
        End Try
    End Function
#End Region

#Region "GetAttribute"
    Public Function GetAttribute(ByVal oCustomClass As Parameter._DO) As Parameter._DO
        Dim oReturnValue As New Parameter._DO
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = oCustomClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.ApplicationID

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, GET_ATTRIBUTE, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("_DO", "GetAttribute", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region

#Region "GetDO_AssetDoc"
    Public Function GetDO_AssetDoc(ByVal oCustomClass As Parameter._DO) As Parameter._DO
        Dim oReturnValue As New Parameter._DO
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = oCustomClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.ApplicationID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, GET_DO_ASSETDOC, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("_DO", "GetDO_AssetDoc", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region

#Region "GetDO_CheckBackDate"
    Public Function GetDO_CheckBackDate(ByVal customClass As Parameter._DO) As Integer
        Dim oReturnValue As New Parameter._DO
        Dim TotalDay As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@TotalDay", SqlDbType.VarChar, 50)
        params(0).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, GET_BACKDATE, params)
            TotalDay = CInt(params(0).Value)
            Return TotalDay
        Catch exp As Exception
            WriteException("_DO", "GetDO_CheckBackDate", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region

#Region "DOSave"
    Public Sub DOSave(ByVal customClass As Parameter._DO, ByVal oData1 As DataTable, ByVal oData2 As DataTable, ByVal oData3 As DataTable, ByVal oData4 As DataTable, oData5 As DataTable)
        Dim oReturnValue As New Parameter._DO
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim intLoop As Integer

        Dim params() As SqlParameter = New SqlParameter(18) {}
        Dim params1() As SqlParameter = New SqlParameter(8) {}
        Dim params2() As SqlParameter = New SqlParameter(7) {}
        Dim params3() As SqlParameter = New SqlParameter(3) {}
        Dim params4() As SqlParameter = New SqlParameter(8) {}
        Dim params5() As SqlParameter = New SqlParameter(6) {}

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(0).Value = customClass.BusinessDate
            params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(1).Value = customClass.BranchId
            params(2) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(2).Value = customClass.ApplicationID
            params(3) = New SqlParameter("@DeliveryDate", SqlDbType.DateTime)
            params(3).Value = customClass.DeliveryDate
            params(4) = New SqlParameter("@SerialNo1", SqlDbType.VarChar, 50)
            params(4).Value = customClass.SerialNo1
            params(5) = New SqlParameter("@SerialNo2", SqlDbType.VarChar, 50)
            params(5).Value = customClass.SerialNo2
            params(6) = New SqlParameter("@ManufacturingYear", SqlDbType.Int)
            params(6).Value = customClass.ManufacturingYear
            params(7) = New SqlParameter("@OwnerAsset", SqlDbType.VarChar, 50)
            params(7).Value = customClass.OldOwnerAsset
            params(8) = New SqlParameter("@OwnerAddress", SqlDbType.VarChar, 100)
            params(8).Value = customClass.OldOwnerAddress
            params(9) = New SqlParameter("@OwnerRT", SqlDbType.Char, 3)
            params(9).Value = customClass.OldOwnerRT
            params(10) = New SqlParameter("@OwnerRW", SqlDbType.Char, 3)
            params(10).Value = customClass.OldOwnerRW
            params(11) = New SqlParameter("@OwnerKelurahan", SqlDbType.VarChar, 30)
            params(11).Value = customClass.OldOwnerKelurahan
            params(12) = New SqlParameter("@OwnerKecamatan", SqlDbType.VarChar, 30)
            params(12).Value = customClass.OldOwnerKecamatan
            params(13) = New SqlParameter("@OwnerCity", SqlDbType.VarChar, 30)
            params(13).Value = customClass.OldOwnerCity
            params(14) = New SqlParameter("@OwnerZipCode", SqlDbType.Char, 5)
            params(14).Value = customClass.OldOwnerZipCode
            params(15) = New SqlParameter("@TaxDate", SqlDbType.DateTime)
            params(15).Value = customClass.TaxDate
            params(16) = New SqlParameter("@Notes", SqlDbType.VarChar, 255)
            params(16).Value = customClass.Notes
            params(17) = New SqlParameter("@PPN", SqlDbType.Decimal)
            params(17).Value = customClass.PPN
            params(18) = New SqlParameter("@BBN", SqlDbType.Decimal)
            params(18).Value = customClass.BBN
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SAVE_DO, params)

            ' save TC
            If oData1.Rows.Count > 0 Then
                params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params1(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params1(3) = New SqlParameter("@PriorTo", SqlDbType.Char, 3)
                params1(4) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
                params1(5) = New SqlParameter("@IsMandatory", SqlDbType.Char, 1)
                params1(6) = New SqlParameter("@PromiseDate", SqlDbType.VarChar, 8)
                params1(7) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params1(8) = New SqlParameter("@Notes", SqlDbType.Text)
                For intLoop = 0 To oData1.Rows.Count - 1
                    params1(0).Value = customClass.BranchId
                    params1(1).Value = customClass.ApplicationID
                    params1(2).Value = oData1.Rows(intLoop).Item("MasterTCID")
                    params1(3).Value = oData1.Rows(intLoop).Item("PriorTo")
                    params1(4).Value = oData1.Rows(intLoop).Item("IsChecked")
                    params1(5).Value = oData1.Rows(intLoop).Item("IsMandatory")
                    params1(6).Value = oData1.Rows(intLoop).Item("PromiseDate")
                    params1(7).Value = customClass.BusinessDate
                    params1(8).Value = oData1.Rows(intLoop).Item("Notes")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SAVE_DO1, params1)
                Next
            End If

            'save tc check list
            If oData2.Rows.Count > 0 Then
                params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params2(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params2(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params2(3) = New SqlParameter("@AGTCCLSequenceNo", SqlDbType.SmallInt)
                params2(4) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
                params2(5) = New SqlParameter("@PromiseDate", SqlDbType.VarChar, 8)
                params2(6) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params2(7) = New SqlParameter("@Notes", SqlDbType.Text)
                For intLoop = 0 To oData2.Rows.Count - 1
                    params2(0).Value = customClass.BranchId
                    params2(1).Value = customClass.ApplicationID
                    params2(2).Value = oData2.Rows(intLoop).Item("MasterTCID")
                    params2(3).Value = CInt(oData2.Rows(intLoop).Item("AGTCCLSequenceNo"))
                    params2(4).Value = oData2.Rows(intLoop).Item("IsChecked")
                    params2(5).Value = oData2.Rows(intLoop).Item("PromiseDate")
                    params2(6).Value = customClass.BusinessDate
                    params2(7).Value = oData2.Rows(intLoop).Item("Notes")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SAVE_DO2, params2)
                Next
            End If

            'save asset attribute
            If oData3.Rows.Count > 0 Then
                params3(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params3(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params3(2) = New SqlParameter("@AttributeID", SqlDbType.Char, 10)
                params3(3) = New SqlParameter("@AttributeContent", SqlDbType.VarChar, 50)
                For intLoop = 0 To oData3.Rows.Count - 1
                    params3(0).Value = customClass.BranchId
                    params3(1).Value = customClass.ApplicationID
                    params3(2).Value = oData3.Rows(intLoop).Item("AttributeID").ToString
                    params3(3).Value = oData3.Rows(intLoop).Item("AttributeContent").ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SAVE_DO3, params3)
                Next
            End If

            ' save asset document content
            If oData4.Rows.Count > 0 Then
                params4(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params4(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params4(2) = New SqlParameter("@AssetDocID", SqlDbType.Char, 10)
                params4(3) = New SqlParameter("@DocumentNo", SqlDbType.VarChar, 50)
                params4(4) = New SqlParameter("@IsMainDoc", SqlDbType.Char, 1)
                params4(5) = New SqlParameter("@IsDocExist", SqlDbType.Char, 1)
                params4(6) = New SqlParameter("@Notes", SqlDbType.Text)
                params4(7) = New SqlParameter("@AssetTypeId", SqlDbType.VarChar, 10)
                params4(8) = New SqlParameter("@PromiseDate", SqlDbType.Date)
                For intLoop = 0 To oData4.Rows.Count - 1
                    params4(0).Value = customClass.BranchId
                    params4(1).Value = customClass.ApplicationID
                    params4(2).Value = oData4.Rows(intLoop).Item("AssetDocID").ToString
                    params4(3).Value = oData4.Rows(intLoop).Item("DocumentNo").ToString
                    params4(4).Value = IIf(oData4.Rows(intLoop).Item("IsMainDoc").ToString = "True", "1", "0").ToString
                    params4(5).Value = oData4.Rows(intLoop).Item("IsDocExist").ToString
                    params4(6).Value = oData4.Rows(intLoop).Item("Notes").ToString
                    params4(7).Value = customClass.AssetTypeID
                    If Not oData4.Rows(intLoop).Item("PromiseDate").ToString = "" Then
                        params4(8).Value = Date.ParseExact(oData4.Rows(intLoop).Item("PromiseDate").ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                    Else
                        params4(8).Value = Date.ParseExact("01/01/1900", "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                    End If

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SAVE_DO4, params4)
                Next
            End If

            If oData5.Rows.Count > 0 Then
                params5(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params5(1) = New SqlParameter("@SyaratCairID", SqlDbType.VarChar, 10)
                params5(2) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
                params5(3) = New SqlParameter("@Notes", SqlDbType.Text)
                params5(4) = New SqlParameter("@PromiseDate", SqlDbType.Date)
                params5(5) = New SqlParameter("@UsrCr", SqlDbType.VarChar, 50)
                params5(6) = New SqlParameter("@DtmCr", SqlDbType.SmallDateTime)

                For intLoop = 0 To oData5.Rows.Count - 1
                    params5(0).Value = customClass.ApplicationID
                    params5(1).Value = oData5.Rows(intLoop).Item("IdSyaratCair").ToString
                    params5(2).Value = "1"
                    params5(3).Value = oData5.Rows(intLoop).Item("Notes").ToString
                    If Not oData5.Rows(intLoop).Item("PromiseDate").ToString = "" Then
                        params5(4).Value = Date.ParseExact(oData5.Rows(intLoop).Item("PromiseDate").ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                    Else
                        params5(4).Value = Date.ParseExact("01/01/1900", "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                    End If


                    params5(5).Value = customClass.LoginId
                    params5(6).Value = customClass.BusinessDate

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SAVE_DO5, params5)
                Next

            End If

            transaction.Commit()
        Catch exp As Exception
            WriteException("_DO", "DOSave", exp.Message + exp.StackTrace)
            transaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try

    End Sub
#End Region

#Region "Update Return"
    Public Sub ApplicationReturnUpdate(ByVal oCustomClass As Parameter._DO)
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        Try
            Dim par() As SqlParameter = New SqlParameter(3) {}

            par(0) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            par(0).Value = oCustomClass.BusinessDate
            par(1) = New SqlParameter("@AlasanReturn", SqlDbType.VarChar, 255)
            par(1).Value = oCustomClass.AlasanReturn
            par(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par(2).Value = oCustomClass.BranchId
            par(3) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            par(3).Value = oCustomClass.ApplicationID


            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spApplicationReturnUpdate", par)

            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub
#End Region

End Class
