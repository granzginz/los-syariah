


#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class NewAppInsuranceByCompany
    Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    'Private Const CHECK_PROSPECT As String = "spInsCheckProspectSelect"
    Private Const INSURANCEBYCOMPANYPAID_STEP_1 As String = "spInsbyCompanyPaidByCustomerSelect"
    Private Const INSURANCEBYCOMPANYPAID_STEP_2 As String = "spPagingInsNewAppInsByCompanyGridTenor"
    Private Const INSURANCEBYCOMPANYPAID_AGRICULTURE_STEP_1 As String = "spInsbyCompanyPaidByAgriculture"
    Private Const INSURANCEBYCOMPANYFACTORING As String = "spPagingInsNewAppInsFactoring"
    Private Const SQL_QUERY_FILL_INSURANCE_COYBRANCH As String = "select MaskAssBranchID AS ID ,MaskAssBranchName as Name from InsuranceComBranch  "
    Private Const SQL_QUERY_SELECT_COVERAGE_TYPE As String = "Select ID,Description as Name from tblCoverageType Order by ID "
    Private Const SQL_QUERY_SELECT_ASSET_TYPE As String = "Select a.ApplicationID as ApplicationID, b.AssetTypeID as AssetTypeID, a.Tenor as Tenor, sum(OTRPrice) as NilaiCover From Agreement a inner join AgreementAsset b on a.ApplicationID = b.ApplicationID and a.BranchId = b.BranchID  "
#End Region
    '================ KHUSUS ENTRY INSURANCE =================================
#Region "CheckProspect"

    Public Function CheckProspect(ByVal CustomClass As Maxiloan.Parameter.NewAppInsuranceByCompany) As Maxiloan.Parameter.NewAppInsuranceByCompany
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = CustomClass.ApplicationID.Trim

        Try
            Dim oReturnValue As New Maxiloan.Parameter.NewAppInsuranceByCompany
            reader = SqlHelper.ExecuteReader(CustomClass.strConnection, CommandType.StoredProcedure, CustomClass.SpName, params)
            If reader.Read Then
                oReturnValue.MaskAssID = reader("MaskAssID").ToString.Trim
                oReturnValue.InsuranceComBranchID = reader("MaskAssBranchID").ToString.Trim
                oReturnValue.InsuranceComBranchName = reader("MaskAssBranchName").ToString.Trim
            End If
            reader.Close()
            Return oReturnValue
        Catch exp As Exception
            WriteException("NewAppInsuranceByCompany", "CheckProspect", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("NewAppInsuranceByCompany-DA", "CheckProspect", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            Throw New Exception(exp.StackTrace)
        End Try
    End Function
#End Region
#Region "HandleDropDownOnDataBound"
    Public Function HandleDropDownOnDataBound(ByVal customClass As Maxiloan.Parameter.NewAppInsuranceByCompany) As DataTable
        Dim oReader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))
        Try
            oReader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.Text, SQL_QUERY_SELECT_COVERAGE_TYPE)

            While oReader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = oReader("ID").ToString
                dtrResult("Name") = oReader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            Return dttResult
        Catch exp As Exception
            WriteException("NewAppInsuranceByCompany", "HandleDropDownOnDataBound", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "FillInsuranceComBranch"

    Public Function FillInsuranceComBranch(ByVal customClass As Maxiloan.Parameter.NewAppInsuranceByCompany) As DataTable
        Dim oReader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))
        Try

            oReader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.Text, SQL_QUERY_FILL_INSURANCE_COYBRANCH & " Where BranchID = '" & customClass.BranchId.Trim & "'")

            While oReader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = oReader("ID").ToString
                dtrResult("Name") = oReader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            Return dttResult
        Catch exp As Exception
            WriteException("NewAppInsuranceByCompany", "FillInsuranceComBranch", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

#End Region
#Region "GetInsuranceEntryStep2List"

    Public Function GetInsuranceEntryStep2List(ByVal customClass As Maxiloan.Parameter.NewAppInsuranceByCompany) As Maxiloan.Parameter.NewAppInsuranceByCompany
        Dim oReturnValue As New Maxiloan.Parameter.NewAppInsuranceByCompany
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@JmlGrid", SqlDbType.Int)
        params(0).Value = customClass.JmlGrid
        params(1) = New SqlParameter("@BranchID", SqlDbType.Char)
        params(1).Value = customClass.BranchId.Trim
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, INSURANCEBYCOMPANYPAID_STEP_2, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("NewAppInsuranceByCompany", "GetInsuranceEntryStep2List", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.GetPremiumToCustomer")
        End Try
    End Function
#End Region
#Region "GetInsuranceEntryStep1List"

    Public Function GetInsuranceEntryStep1List(ByVal customClass As Maxiloan.Parameter.NewAppInsuranceByCompany) As Maxiloan.Parameter.NewAppInsuranceByCompany
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.ApplicationID.Trim

        Try
            Dim oReturnValue As New Maxiloan.Parameter.NewAppInsuranceByCompany

            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, INSURANCEBYCOMPANYPAID_STEP_1, params)
            If reader.Read Then
                With oReturnValue
                    .BranchId = reader("BranchId").ToString.Trim
                    .ApplicationID = reader("ApplicationID").ToString.Trim
                    .CustomerID = reader("CustomerID").ToString.Trim
                    .CustomerName = reader("CustomerName").ToString.Trim
                    .AssetUsageNewUsed = reader("UsedNew").ToString
                    .AssetMasterDescr = reader("AssetMasterDescr").ToString
                    .InsuranceAssetDescr = reader("InsuranceAssetTypeDescr").ToString
                    .AssetUsageDescr = reader("AssetUsageDescr").ToString
                    .InsAssetInsuredByName = reader("InsByDescr").ToString
                    .InsAssetPaidByName = reader("PaidByDescr").ToString
                    .TotalOTR = CType(reader("TotalOTR").ToString, Double)
                    .Tenor = CType(reader("Tenor").ToString, Double)
                    .Tenor2 = CType(reader("Tenor2").ToString, Double)
                    .MinimumTenor = CType(reader("MinimumTenor"), Int16)
                    .MaximumTenor = CType(reader("MaximumTenor"), Int16)
                    .PBMinimumTenor = CType(reader("PBMinimumTenor"), Int16)
                    .PBMaximumTenor = CType(reader("PBMaximumTenor"), Int16)
                    .PMinimumTenor = CType(reader("PMinimumTenor"), Int16)
                    .PMaximumTenor = CType(reader("PMaximumTenor"), Int16)
                    .InsAdminFee = CType(reader("InsAdminFee"), Double)
                    .InsStampDutyFee = CType(reader("InsStampDutyFee"), Double)
                    .InsAdminFeeBehaviour = reader("InsAdminFeePatern").ToString
                    .InsStampDutyFeeBehaviour = reader("InsStampDutyFeePatern").ToString
                    .InsuranceType = reader("InsRateCategory").ToString
                    .AssetUsageID = reader("AssetUsageID").ToString
                    .ManufacturingYear = reader("ManufacturingYear").ToString
                    .SesionID = reader("InsuranceRateCardID").ToString
                    .JenisAksesoris = reader("jenisAksesoris").ToString
                    .InsNotes = reader("InsNotes").ToString
                    .NilaiAksesoris = reader("NilaiAksesoris").ToString
                    .sellingRate = reader("SellingRate")
                    .BiayaPolis = reader("BiayaPolis")
                    .BiayaMaterai = reader("BiayaMaterai")
                    .PaidByCust = reader("PaidAmountByCust")
                    .IsBMSave = reader("IsBMSave")
                    .InsuranceComBranchID = reader("MaskAssBranchID").ToString.Trim
                    .DescWil = reader("DescWil")
                    .PerluasanPA = CType(reader("PerluasanPA"), Boolean)
                    .JaminanKredit = CType(reader("JaminanCredit"), Boolean)
                    .CreditProtection = CType(reader("CreditProtection"), Boolean)
                End With
            End If
            reader.Close()
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetInsuranceEntryStep1ListAgriculture(ByVal customClass As Maxiloan.Parameter.NewAppInsuranceByCompany) As Maxiloan.Parameter.NewAppInsuranceByCompany
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.ApplicationID.Trim

        Try
            Dim oReturnValue As New Maxiloan.Parameter.NewAppInsuranceByCompany

            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, INSURANCEBYCOMPANYPAID_AGRICULTURE_STEP_1, params).Tables(0)
            Return oReturnValue

        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function


    Public Function GetInsuranceEntryFactoring(ByVal customClass As Maxiloan.Parameter.NewAppInsuranceByCompany) As Maxiloan.Parameter.NewAppInsuranceByCompany
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.ApplicationID.Trim

        Try
            Dim oReturnValue As New Maxiloan.Parameter.NewAppInsuranceByCompany

            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, INSURANCEBYCOMPANYFACTORING, params)
            If reader.Read Then
                With oReturnValue
                    .BranchId = reader("BranchId").ToString.Trim
                    .ApplicationID = reader("ApplicationID").ToString.Trim
                    .InsuranceComBranchID = reader("MaskAssBranchID").ToString.Trim
                    .Premium = reader("MainPremiumToCust").ToString.Trim
                    .Rate = reader("SellingRate").ToString.Trim
                    .InsRateCategory = reader("InsRateCategory").ToString.Trim
                End With
            End If
            reader.Close()
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

#End Region


    '================ KHUSUS EDIT INSURANCE =================================

    Public Function EditGetInsuranceEntryStep1List(ByVal customClass As Maxiloan.Parameter.NewAppInsuranceByCompany) As Maxiloan.Parameter.NewAppInsuranceByCompany

        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.ApplicationID.Trim
        Try
            Dim oReturnValue As New Maxiloan.Parameter.NewAppInsuranceByCompany
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params)
            If reader.Read Then
                oReturnValue.BranchId = reader("BranchId").ToString.Trim
                oReturnValue.ApplicationID = reader("ApplicationID").ToString.Trim
                oReturnValue.CustomerID = reader("CustomerID").ToString.Trim
                oReturnValue.CustomerName = reader("CustomerName").ToString.Trim
                oReturnValue.AssetUsageNewUsed = reader("UsedNew").ToString
                oReturnValue.AssetMasterDescr = reader("AssetMasterDescr").ToString
                oReturnValue.InsuranceAssetDescr = reader("InsuranceAssetTypeDescr").ToString
                oReturnValue.AssetUsageDescr = reader("AssetUsageDescr").ToString
                oReturnValue.InsAssetInsuredByName = reader("InsByDescr").ToString
                oReturnValue.InsAssetPaidByName = reader("PaidByDescr").ToString
                oReturnValue.TotalOTR = CType(reader("TotalOTR").ToString, Double)
                oReturnValue.Tenor = CType(reader("Tenor").ToString, Double)
                oReturnValue.MinimumTenor = CType(reader("MinimumTenor"), Int16)
                oReturnValue.MaximumTenor = CType(reader("MaximumTenor"), Int16)
                oReturnValue.InsAdminFee = CType(reader("InsAdminFee").ToString, Double)
                oReturnValue.InsStampDutyFee = CType(reader("InsStampDutyFee").ToString, Double)
                oReturnValue.InsAdminFeeBehaviour = reader("InsAdminFeeBehaviour").ToString
                oReturnValue.InsStampDutyFeeBehaviour = reader("InsStampDutyFeeBehaviour").ToString
                oReturnValue.InsuranceType = reader("InsRateCategory").ToString
                oReturnValue.AssetUsageID = reader("AssetUsageID").ToString
                oReturnValue.ManufacturingYear = reader("ManufacturingYear").ToString
                oReturnValue.InsuranceComBranchID = reader("MaskAssBranchID").ToString
                oReturnValue.ApplicationTypeDescr = reader("ApplicationTypeDescr").ToString
                oReturnValue.PBMinimumTenor = CType(reader("PBMinimumTenor"), Int16)
                oReturnValue.PBMaximumTenor = CType(reader("PBMaximumTenor"), Int16)
                oReturnValue.PMaximumTenor = CType(reader("PMaximumTenor"), Int16)
                oReturnValue.PMinimumTenor = CType(reader("PMinimumTenor"), Int16)
                oReturnValue.RefundToSupplier = CType(reader("InsRefundToSupplier"), Double)
                oReturnValue.SesionID = reader("InsuranceRateCardID").ToString

            End If
            reader.Close()
            Return oReturnValue
        Catch exp As Exception
            WriteException("NewAppInsuranceByCompany", "EditGetInsuranceEntryStep1List", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
            ' Throw New Exception("Error On DataAccess.Marketing.AssetTypeAttribute.GetAssetTypeAttributeEdit")
        End Try

    End Function

    Public Function EditGetInsuranceEntryStep2List(ByVal customClass As Maxiloan.Parameter.NewAppInsuranceByCompany) As Maxiloan.Parameter.NewAppInsuranceByCompany
        Dim oReturnValue As New Maxiloan.Parameter.NewAppInsuranceByCompany
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@JmlGrid", SqlDbType.Int)
        params(0).Value = customClass.JmlGrid
        params(1) = New SqlParameter("@BranchID", SqlDbType.Char)
        params(1).Value = customClass.BranchId.Trim
        params(2) = New SqlParameter("@ApplicationID", SqlDbType.Char)
        params(2).Value = customClass.ApplicationID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("NewAppInsuranceByCompany", "EditGetInsuranceEntryStep2List", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.GetPremiumToCustomer")
        End Try

    End Function

    Public Function EditInsuranceSelect(ByVal customClass As Maxiloan.Parameter.NewAppInsuranceByCompany) As Maxiloan.Parameter.NewAppInsuranceByCompany
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.ApplicationID.Trim
        Try
            Dim oReturnValue As New Maxiloan.Parameter.NewAppInsuranceByCompany
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params)
            If reader.Read Then
                oReturnValue.BranchId = reader("BranchId").ToString.Trim
                oReturnValue.CustomerID = reader("CustomerID").ToString.Trim
                oReturnValue.CustomerName = reader("CustomerName").ToString.Trim
                oReturnValue.InsAssetInsuredBy = reader("InsAssetInsuredBy").ToString.Trim
                oReturnValue.InsAssetPaidBy = reader("InsAssetPaidBy").ToString.Trim
                oReturnValue.SupplierGroupID = reader("SupplierGroupID").ToString.Trim
                oReturnValue.InterestType = reader("InterestType").ToString.Trim
                oReturnValue.InstallmentScheme = reader("InstallmentScheme").ToString.Trim
                oReturnValue.NumberInstallment = CDbl(reader("NumberInstallment").ToString.Trim)
                oReturnValue.NumberAgreementAsset = CDbl(reader("NumberAgreementAsset").ToString.Trim)
                oReturnValue.AssetID = reader("AssetID").ToString.Trim
                oReturnValue.DateEntryApplicationData = CType(reader("DateEntryApplicationData"), Date)
                oReturnValue.DateEntryAssetData = CType(reader("DateEntryAssetData"), Date)
                oReturnValue.DateEntryInsuranceData = CType(reader("DateEntryInsuranceData"), Date)
                oReturnValue.DateEntryFinancialData = CType(reader("DateEntryFinancialData"), Date)
                oReturnValue.DateEntryIncentiveData = CType(reader("DateEntryIncentiveData"), Date)
            End If
            reader.Close()
            Return oReturnValue
        Catch exp As Exception
            WriteException("NewAppInsuranceByCompany", "EditInsuranceSelect", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function


    Public Function GetInsurancehComByBranch(ByVal customClass As Maxiloan.Parameter.NewAppInsuranceByCompany) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@MaskAssBranchID", SqlDbType.VarChar, 10)
        params(0).Value = customClass.InsuranceComBranchID
        Try
            Return SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spGetInsurancehComByBranch", params).Tables(0)
        Catch exp As Exception
            Dim err As New MaxiloanExceptions
            WriteException("NewAppInsuranceByCompany", "GetInsurancehComByBranch", exp.Message + exp.StackTrace)
            err.WriteLog("NewAppInsuranceByCompany", "GetInsurancehComByBranch", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
        End Try
    End Function


    Public Function GetAssetCategory(ByVal customClass As Maxiloan.Parameter.NewAppInsuranceByCompany) As DataTable
        Dim oReader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("AssetTypeID", GetType(String))
        dttResult.Columns.Add("Tenor", GetType(String))
        dttResult.Columns.Add("NilaiCover", GetType(Double))
        Try
            oReader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.Text, SQL_QUERY_SELECT_ASSET_TYPE & " Where a.ApplicationID = '" & customClass.ApplicationID.Trim & "' Group by a.ApplicationID, b.AssetTypeId, a.Tenor")

            While oReader.Read
                dtrResult = dttResult.NewRow
                dtrResult("AssetTypeID") = oReader("AssetTypeID").ToString
                dtrResult("Tenor") = oReader("Tenor").ToString
                dtrResult("NilaiCover") = oReader("NilaiCover").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            Return dttResult
        Catch exp As Exception
            WriteException("NewAppInsuranceMultiAsset", "GetInsuranceEntryStep1List", exp.Message + exp.StackTrace)
        End Try
        Return Nothing
    End Function

    Public Function GetInsuranceBranchNameRateCard(connString As String, applicationid As String, MaskAssBranchID As String, CoverageType As String, yearnum As Integer) As DataTable
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@MaskAssBranchID", SqlDbType.VarChar, 10)
        params(0).Value = MaskAssBranchID
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(1).Value = applicationid
        params(2) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 3)
        params(2).Value = CoverageType
        params(3) = New SqlParameter("@YearNum", SqlDbType.Int)
        params(3).Value = yearnum
        Try
            Return SqlHelper.ExecuteDataset(connString, CommandType.StoredProcedure, "spGetInsuranceBranchNameRateCard", params).Tables(0)
        Catch exp As Exception
            Dim err As New MaxiloanExceptions
            WriteException("NewAppInsuranceByCompany", "GetInsuranceBranchNameProduct", exp.Message + exp.StackTrace)
            err.WriteLog("NewAppInsuranceByCompany", "GetInsuranceBranchNameProduct", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
        End Try
        Return Nothing
    End Function

    Public Function GetInsurancehComByBranchProduct(ByVal customClass As Maxiloan.Parameter.InsCoBranch) As Parameter.InsCoBranch
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@MaskAssBranchID", SqlDbType.VarChar, 10)
        params(0).Value = customClass.InsCoBranchID
        params(1) = New SqlParameter("@ProductID", SqlDbType.VarChar, 3)
        params(1).Value = customClass.InsuranceProductID
        params(2) = New SqlParameter("@Tenor", SqlDbType.Int)
        params(2).Value = customClass.Tenor

        Try
            Dim oReturnValue As New Maxiloan.Parameter.InsCoBranch

            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, "spGetInsuranceCompanyByBranchByProduct", params)
            If reader.Read Then
                With oReturnValue
                    .InsCoBranchID = reader("MaskAssID").ToString.Trim
                    .InsuranceProductID = reader("ProductID").ToString.Trim
                    .Tenor = reader("Tenor").ToString.Trim
                    .InsuranceRateAdditional = reader("Rate").ToString
                    .BiayaPolisAdditional = reader("BiayaPolis").ToString
                End With
            End If
            reader.Close()
            Return oReturnValue
        Catch exp As Exception
            Dim err As New MaxiloanExceptions
            WriteException("NewAppInsuranceByCompany", "GetInsurancehComByBranchProduct", exp.Message + exp.StackTrace)
            err.WriteLog("NewAppInsuranceByCompany", "GetInsurancehComByBranchProduct", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            Return Nothing
        End Try
    End Function

    Public Function GetInsuranceComBranch(ByVal customClass As Parameter.NewAppInsuranceByCompany) As Parameter.NewAppInsuranceByCompany
        Dim oReturnValue As New Parameter.NewAppInsuranceByCompany
        Dim params(2) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = customClass.Table
        params(1) = New SqlParameter("ApplicationID", SqlDbType.VarChar, 20)
        params(1).Value = customClass.ApplicationID
        params(2) = New SqlParameter("InsuranceComBranchID", SqlDbType.VarChar, 20)
        params(2).Value = customClass.InsuranceComBranchID

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spInsuranceComBranchFillCbo", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("NewAppInsuranceByCompany", "spInsuranceComBranchFillCbo", exp.Message + exp.StackTrace)
            Throw New Exception("Error On spInsuranceComBranchFillCbo ")
        End Try
    End Function

End Class
