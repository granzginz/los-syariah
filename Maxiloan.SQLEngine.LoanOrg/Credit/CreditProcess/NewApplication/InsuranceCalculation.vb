

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region


Public Class InsuranceCalculation
    Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    Private Const SP_PROCESSNEWAPP_BY_COMPANY_SAVE_ADD_TEMPORARY As String = "spInsNewAppInsuranceByCompanySaveAddTemporary"
    Private Const SP_PROCESSNEWAPP_BY_COMPANY_SAVE_DETAIL_ADD_TEMPORARY As String = "spShadowInsNewAppInsuranceByCompanyDetailSaveAddTemporary1"
    Private Const SP_INSURANCE_NEW_APPLICATION_LAST_PROCESS As String = "spShadowInsNewAppInsuranceByCompanyLastProcess1"
#End Region

#Region "ProcessSaveDataInsuranceApplicationHeaderAndDetail"

    Public Sub ProcessSaveDataInsuranceApplicationHeaderAndDetail(ByVal oDataTableInsuranceAsset As DataTable, ByVal CustomClass As Parameter.InsuranceCalculation)

        Dim objConnection As New SqlConnection(CustomClass.strConnection)
        Dim objtrans As SqlTransaction

        Try

            Dim paramsHeader(5) As SqlParameter
            paramsHeader(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            paramsHeader(0).Value = CustomClass.BranchId
            paramsHeader(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            paramsHeader(1).Value = CustomClass.ApplicationID
            paramsHeader(2) = New SqlParameter("@InsuranceComBranchID", SqlDbType.Char, 10)
            paramsHeader(2).Value = CustomClass.InsuranceComBranchID
            paramsHeader(3) = New SqlParameter("@ApplicationType", SqlDbType.Char, 10)
            paramsHeader(3).Value = CustomClass.ApplicationTypeID
            paramsHeader(4) = New SqlParameter("@UsageId", SqlDbType.Char, 10)
            paramsHeader(4).Value = CustomClass.UsageID
            paramsHeader(5) = New SqlParameter("@UsedNew", SqlDbType.Char, 1)
            paramsHeader(5).Value = CustomClass.NewUsed

            SqlHelper.ExecuteNonQuery(CustomClass.strConnection, CommandType.StoredProcedure, SP_PROCESSNEWAPP_BY_COMPANY_SAVE_ADD_TEMPORARY, paramsHeader)

            If oDataTableInsuranceAsset.Rows.Count > 0 Then
                Dim paramsDetail(15) As SqlParameter
                paramsDetail(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
                paramsDetail(0).Value = CustomClass.BranchId
                paramsDetail(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                paramsDetail(1).Value = CustomClass.ApplicationID
                paramsDetail(2) = New SqlParameter("@YearNum", SqlDbType.Int)
                paramsDetail(2).Value = CustomClass.YearNum
                paramsDetail(3) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 10)
                paramsDetail(3).Value = CustomClass.CoverageType
                paramsDetail(4) = New SqlParameter("@ApplicationType", SqlDbType.VarChar, 10)
                paramsDetail(4).Value = CustomClass.ApplicationTypeID
                paramsDetail(5) = New SqlParameter("@AmountCoverage", SqlDbType.Decimal)
                paramsDetail(5).Value = CustomClass.AmountCoverage
                paramsDetail(6) = New SqlParameter("@InsuranceType", SqlDbType.VarChar, 2)
                paramsDetail(6).Value = CustomClass.InsuranceType
                paramsDetail(7) = New SqlParameter("@UsageID", SqlDbType.VarChar, 10)
                paramsDetail(7).Value = CustomClass.UsageID
                paramsDetail(8) = New SqlParameter("@NewUsed", SqlDbType.VarChar, 1)
                paramsDetail(8).Value = CustomClass.NewUsed
                paramsDetail(9) = New SqlParameter("@BoolSRCC", SqlDbType.Bit)
                paramsDetail(9).Value = CustomClass.BolSRCC
                paramsDetail(10) = New SqlParameter("@BolFlood", SqlDbType.Bit)
                paramsDetail(10).Value = CustomClass.BolFlood
                paramsDetail(11) = New SqlParameter("@PaidByCustStatus", SqlDbType.VarChar, 10)
                paramsDetail(11).Value = CustomClass.PaidByCustStatus
                paramsDetail(12) = New SqlParameter("@ManufacturingYear", SqlDbType.DateTime)
                paramsDetail(12).Value = CustomClass.DateMonthYearManufacturingYear
                paramsDetail(13) = New SqlParameter("@TPLAmountToCust", SqlDbType.Decimal)
                paramsDetail(13).Value = CustomClass.TPL
                paramsDetail(14) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                paramsDetail(14).Value = CustomClass.BusinessDate
                paramsDetail(15) = New SqlParameter("@IsPageSourceCompanyCustomer", SqlDbType.Bit)
                paramsDetail(15).Value = CustomClass.IsPageSourceCompanyCustomer

                SqlHelper.ExecuteNonQuery(CustomClass.strConnection, CommandType.StoredProcedure, SP_PROCESSNEWAPP_BY_COMPANY_SAVE_DETAIL_ADD_TEMPORARY, paramsDetail)
            End If

        Catch exp As Exception
            WriteException("InsuranceCalculation", "ProcessSaveDataInsuranceApplicationHeaderAndDetail", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsuranceCalculation.vb", "ProcessSaveDataInsuranceApplicationHeaderAndDetail", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            objtrans.Rollback()
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()

        End Try


    End Sub

#End Region

#Region "ProcessNewAppInsuranceByCompanySaveAddTemporary"

    Public Sub ProcessNewAppInsuranceByCompanySaveAddTemporary(ByVal customClass As Parameter.InsuranceCalculation)
        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            Dim params(5) As SqlParameter
            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customClass.ApplicationID
            params(2) = New SqlParameter("@MaskAssBranchID", SqlDbType.Char, 10)
            params(2).Value = customClass.InsuranceComBranchID
            params(3) = New SqlParameter("@ApplicationType", SqlDbType.Char, 10)
            params(3).Value = customClass.ApplicationTypeID
            params(4) = New SqlParameter("@UsageId", SqlDbType.Char, 10)
            params(4).Value = customClass.UsageID
            params(5) = New SqlParameter("@UsedNew", SqlDbType.Char, 1)
            params(5).Value = customClass.NewUsed

            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, SP_PROCESSNEWAPP_BY_COMPANY_SAVE_ADD_TEMPORARY, params)
            objtrans.Commit()
        Catch exp As Exception
            WriteException("InsuranceCalculation", "ProcessNewAppInsuranceByCompanySaveAddTemporary", exp.Message + exp.StackTrace)
            objtrans.Rollback()
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub
#End Region

#Region "ProcessNewAppInsuranceByCompanyDetailSaveAddTemporary"

    Public Sub ProcessNewAppInsuranceByCompanyDetailSaveAddTemporary(ByVal customClass As Parameter.InsuranceCalculation)
        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            Dim params(23) As SqlParameter

            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customClass.ApplicationID
            params(2) = New SqlParameter("@YearNum", SqlDbType.Int)
            params(2).Value = customClass.YearNum
            params(3) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 10)
            params(3).Value = customClass.CoverageType
            params(4) = New SqlParameter("@ApplicationType", SqlDbType.VarChar, 10)
            params(4).Value = customClass.ApplicationTypeID
            params(5) = New SqlParameter("@AmountCoverage", SqlDbType.Decimal)
            params(5).Value = customClass.AmountCoverage
            params(6) = New SqlParameter("@InsuranceType", SqlDbType.VarChar, 2)
            params(6).Value = customClass.InsuranceType
            params(7) = New SqlParameter("@UsageID", SqlDbType.VarChar, 10)
            params(7).Value = customClass.UsageID
            params(8) = New SqlParameter("@NewUsed", SqlDbType.VarChar, 1)
            params(8).Value = customClass.NewUsed
            params(9) = New SqlParameter("@BoolSRCC", SqlDbType.Bit)
            params(9).Value = customClass.BolSRCC
            params(10) = New SqlParameter("@BolFlood", SqlDbType.Bit)
            params(10).Value = customClass.BolFlood
            params(11) = New SqlParameter("@PaidByCustStatus", SqlDbType.VarChar, 10)
            params(11).Value = customClass.PaidByCustStatus
            params(12) = New SqlParameter("@ManufacturingYear", SqlDbType.DateTime)
            params(12).Value = customClass.DateMonthYearManufacturingYear
            params(13) = New SqlParameter("@TPLAmountToCust", SqlDbType.Decimal)
            params(13).Value = customClass.TPL
            params(14) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(14).Value = customClass.BusinessDate
            params(15) = New SqlParameter("@IsPageSourceCompanyCustomer", SqlDbType.Bit)
            params(15).Value = customClass.IsPageSourceCompanyCustomer
            params(16) = New SqlParameter("@PaidAmountByCust", SqlDbType.Decimal)
            params(16).Value = customClass.PaidAmountByCust
            params(17) = New SqlParameter("@DiscountToCust", SqlDbType.Decimal)
            params(17).Value = customClass.DiscountToCust
            params(18) = New SqlParameter("@BolRiot", SqlDbType.Bit)
            params(18).Value = customClass.BolRiot
            params(19) = New SqlParameter("@BolPA", SqlDbType.Bit)
            params(19).Value = customClass.BolPA
            params(20) = New SqlParameter("@YearNumRate", SqlDbType.Int)
            params(20).Value = customClass.YearNumRate
            params(21) = New SqlParameter("@IsPADriver", SqlDbType.Bit)
            params(21).Value = customClass.bolPADriver
            params(22) = New SqlParameter("@IsTPL", SqlDbType.Bit)
            params(22).Value = customClass.bolTPL
            params(23) = New SqlParameter("@MasterRateCardID", SqlDbType.VarChar, 50)
            params(23).Value = customClass.MasterRateCardID

            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, SP_PROCESSNEWAPP_BY_COMPANY_SAVE_DETAIL_ADD_TEMPORARY, params)
            objtrans.Commit()

        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub


    Public Sub ProcessAppInsuranceAgricultureDetailSave(ByVal customClass As Parameter.InsuranceCalculation)
        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            Dim params(24) As SqlParameter

            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customClass.ApplicationID
            params(2) = New SqlParameter("@YearNum", SqlDbType.Int)
            params(2).Value = customClass.YearNum
            params(3) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 10)
            params(3).Value = customClass.CoverageType
            params(4) = New SqlParameter("@ApplicationType", SqlDbType.VarChar, 10)
            params(4).Value = customClass.ApplicationTypeID
            params(5) = New SqlParameter("@AmountCoverage", SqlDbType.Decimal)
            params(5).Value = customClass.AmountCoverage
            params(6) = New SqlParameter("@InsuranceType", SqlDbType.VarChar, 2)
            params(6).Value = customClass.InsuranceType
            params(7) = New SqlParameter("@UsageID", SqlDbType.VarChar, 10)
            params(7).Value = customClass.UsageID
            params(8) = New SqlParameter("@NewUsed", SqlDbType.VarChar, 1)
            params(8).Value = customClass.NewUsed
            params(9) = New SqlParameter("@BoolSRCC", SqlDbType.Bit)
            params(9).Value = customClass.BolSRCC
            params(10) = New SqlParameter("@BolFlood", SqlDbType.Bit)
            params(10).Value = customClass.BolFlood
            params(11) = New SqlParameter("@PaidByCustStatus", SqlDbType.VarChar, 10)
            params(11).Value = customClass.PaidByCustStatus
            params(12) = New SqlParameter("@ManufacturingYear", SqlDbType.DateTime)
            params(12).Value = customClass.DateMonthYearManufacturingYear
            params(13) = New SqlParameter("@TPLAmountToCust", SqlDbType.Decimal)
            params(13).Value = customClass.TPL
            params(14) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(14).Value = customClass.BusinessDate
            params(15) = New SqlParameter("@IsPageSourceCompanyCustomer", SqlDbType.Bit)
            params(15).Value = customClass.IsPageSourceCompanyCustomer
            params(16) = New SqlParameter("@PaidAmountByCust", SqlDbType.Decimal)
            params(16).Value = customClass.PaidAmountByCust
            params(17) = New SqlParameter("@DiscountToCust", SqlDbType.Decimal)
            params(17).Value = customClass.DiscountToCust
            params(18) = New SqlParameter("@BolRiot", SqlDbType.Bit)
            params(18).Value = customClass.BolRiot
            params(19) = New SqlParameter("@BolPA", SqlDbType.Bit)
            params(19).Value = customClass.BolPA
            params(20) = New SqlParameter("@YearNumRate", SqlDbType.Int)
            params(20).Value = customClass.YearNumRate
            params(21) = New SqlParameter("@IsPADriver", SqlDbType.Bit)
            params(21).Value = customClass.bolPADriver
            params(22) = New SqlParameter("@IsTPL", SqlDbType.Bit)
            params(22).Value = customClass.bolTPL
            params(23) = New SqlParameter("@InsuranceRate", SqlDbType.Decimal)
            params(23).Value = customClass.InsuranceRate
            params(24) = New SqlParameter("@MainPremiumToCust", SqlDbType.Decimal)
            params(24).Value = customClass.MainPremiumToCust

            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, "spAppInsuranceAgricultureDetailSave", params)
            objtrans.Commit()

        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub
#End Region

#Region "ProcessSaveInsuranceApplicationLastProcess"
    Public Sub ProcessSaveInsuranceApplicationLastProcess(ByVal customClass As Parameter.InsuranceCalculationResult)
        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            Dim params(32) As SqlParameter

            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customClass.ApplicationID
            params(2) = New SqlParameter("@AmountCoverage", SqlDbType.Decimal)
            params(2).Value = customClass.AmountCoverage
            params(3) = New SqlParameter("@AdminFeeToCust", SqlDbType.Decimal)
            params(3).Value = customClass.AdminFeeToCust
            params(4) = New SqlParameter("@MeteraiFeeToCust", SqlDbType.Decimal)
            params(4).Value = customClass.MeteraiFeeToCust
            params(5) = New SqlParameter("@DiscToCustAmount", SqlDbType.Decimal)
            params(5).Value = customClass.DiscToCustAmount
            params(6) = New SqlParameter("@PaidAmountByCust", SqlDbType.Decimal)
            params(6).Value = customClass.PaidAmountByCust
            params(7) = New SqlParameter("@PremiumBaseForRefundSupp", SqlDbType.Decimal)
            params(7).Value = customClass.PremiumBaseForRefundSupp
            params(8) = New SqlParameter("@AccNotes", SqlDbType.Char, 500)
            params(8).Value = customClass.AccNotes
            params(9) = New SqlParameter("@InsNotes", SqlDbType.Char, 500)
            params(9).Value = customClass.InsNotes
            params(10) = New SqlParameter("@InsLength", SqlDbType.SmallInt)
            params(10).Value = customClass.InsLength
            params(11) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(11).Value = customClass.BusinessDate
            params(12) = New SqlParameter("@PremiumAmountByCust", SqlDbType.Decimal)
            params(12).Value = customClass.PremiumToCustAmount
            params(13) = New SqlParameter("@MaskAssBranchID", SqlDbType.Char, 20)
            params(13).Value = customClass.InsuranceComBranchID
            params(14) = New SqlParameter("@AdditionalCapitalized", SqlDbType.Decimal)
            params(14).Value = customClass.AdditionalCapitalized
            params(15) = New SqlParameter("@AdditonalInsurancePremium", SqlDbType.Decimal)
            params(15).Value = customClass.AdditionalInsurancePremium
            params(16) = New SqlParameter("@RefundToSupplier", SqlDbType.Decimal)
            params(16).Value = customClass.RefundToSupplier
            params(17) = New SqlParameter("@JenisAksesoris", SqlDbType.VarChar, 50)
            params(17).Value = customClass.JenisAksesoris
            params(18) = New SqlParameter("@NilaiAksesoris", SqlDbType.Decimal)
            params(18).Value = customClass.NilaiAksesoris
            params(19) = New SqlParameter("@SellingRate", SqlDbType.Decimal)
            params(19).Value = customClass.sellingRate
            params(20) = New SqlParameter("@BiayaPolis", SqlDbType.Decimal)
            params(20).Value = customClass.BiayaPolis
            params(21) = New SqlParameter("@LoginID", SqlDbType.VarChar, 50)
            params(21).Value = customClass.LoginId
            params(22) = New SqlParameter("@BiayaMaterai", SqlDbType.Decimal)
            params(22).Value = customClass.BiayaMaterai

            'params(22) = New SqlParameter("@PerluasanPA", SqlDbType.Bit)
            'params(22).Value = customClass.PerluasanPA

            'params(23) = New SqlParameter("@PaidCust", SqlDbType.Structured)
            'params(23).Value = customClass.DataTablePaid

            params(23) = New SqlParameter("@PremiCP", SqlDbType.Decimal)
            params(23).Value = customClass.PremiCP

            params(24) = New SqlParameter("@BiayaPolisCP", SqlDbType.Decimal)
            params(24).Value = customClass.BiayaPolisCP

            params(25) = New SqlParameter("@RateCP", SqlDbType.Decimal)
            params(25).Value = customClass.RateCP

            params(26) = New SqlParameter("@InsComBranchIDCP", SqlDbType.VarChar, 20)
            params(26).Value = customClass.InsComBranchIDCP

            params(27) = New SqlParameter("@CreditProtection", SqlDbType.Bit)
            params(27).Value = customClass.CreditProtection

            params(28) = New SqlParameter("@PremiAJK", SqlDbType.Decimal)
            params(28).Value = customClass.PremiAJK

            params(29) = New SqlParameter("@BiayaPolisAJK", SqlDbType.Decimal)
            params(29).Value = customClass.BiayaPolisAJK

            params(30) = New SqlParameter("@RateAJK", SqlDbType.Decimal)
            params(30).Value = customClass.RateAJK

            params(31) = New SqlParameter("@InsComBranchIDAJK", SqlDbType.VarChar, 20)
            params(31).Value = customClass.InsComBranchIDAJK

            params(32) = New SqlParameter("@JaminanKredit", SqlDbType.Bit)
            params(32).Value = customClass.JaminanKredit

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SP_INSURANCE_NEW_APPLICATION_LAST_PROCESS, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("InsuranceCalculation", "ProcessSaveInsuranceApplicationLastProcess", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsuranceCalculation.vb", "ProcessSaveInsuranceApplicationLastProcess", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Sub

    'koreksi asuransi
    Public Sub ProcessKoreksiAsuransi(ByVal customClass As Parameter.InsuranceCalculationResult)
        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            Dim params(4) As SqlParameter

            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customClass.ApplicationID
            params(2) = New SqlParameter("@InsNotes", SqlDbType.VarChar, 255)
            params(2).Value = customClass.InsNotes
            params(3) = New SqlParameter("@JenisAksesoris", SqlDbType.VarChar, 50)
            params(3).Value = customClass.JenisAksesoris
            params(4) = New SqlParameter("@alasanKoreksi", SqlDbType.VarChar, 200)
            params(4).Value = customClass.alasanKoreksi

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spKoreksiAsuransi", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("InsuranceCalculation", "ProcessSaveInsuranceApplicationLastProcess", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsuranceCalculation.vb", "ProcessSaveInsuranceApplicationLastProcess", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Sub
#End Region

#Region "GetDateEntryInsuranceData"

    Public Function GetDateEntryInsuranceData(ByVal customClass As Parameter.InsuranceCalculation) As Parameter.InsuranceCalculation
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 30)
        params(0).Value = customClass.ApplicationID
        Try
            Dim oReturnValue As New Parameter.InsuranceCalculation
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, "spGetDateEntryInsuranceData", params)
            If reader.Read Then
                oReturnValue.TotalRecord = CType(reader("Total"), Integer)
            End If
            reader.Close()
            Return oReturnValue
        Catch exp As Exception
            WriteException("InsuranceCalculation", "GetDateEntryInsuranceData", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsuranceCalculation.vb", "GetDateEntryInsuranceData", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
        End Try
    End Function
#End Region

    Public Function DisplaySelectedPremiumOnGrid(ByVal customClass As Parameter.InsuranceCalculation) As Parameter.InsuranceCalculation
        Dim oReturnValue As New Parameter.InsuranceCalculation
        Dim params(18) As SqlParameter

        params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@NumRows", SqlDbType.Int)
        params(1).Value = customClass.NumRows
        params(2) = New SqlParameter("@YearNumDtg", SqlDbType.VarChar, 100)
        params(2).Value = customClass.YearNumDtg
        params(3) = New SqlParameter("@CoverageTypeDtg", SqlDbType.VarChar, 100)
        params(3).Value = customClass.CoverageTypeDtg
        params(4) = New SqlParameter("@ApplicationType", SqlDbType.VarChar, 10)
        params(4).Value = customClass.ApplicationTypeID
        params(5) = New SqlParameter("@AmountCoverage", SqlDbType.Decimal)
        params(5).Value = customClass.AmountCoverage
        params(6) = New SqlParameter("@InsuranceType", SqlDbType.VarChar, 2)
        params(6).Value = customClass.InsuranceType
        params(7) = New SqlParameter("@UsageID", SqlDbType.VarChar, 10)
        params(7).Value = customClass.UsageID
        params(8) = New SqlParameter("@NewUsed", SqlDbType.VarChar, 1)
        params(8).Value = customClass.NewUsed
        params(9) = New SqlParameter("@BoolSRCCDtg", SqlDbType.VarChar, 100)
        params(9).Value = customClass.BolSRCCDtg
        params(10) = New SqlParameter("@BolFloodDtg", SqlDbType.VarChar, 100)
        params(10).Value = customClass.BolFloodDtg
        params(11) = New SqlParameter("@ManufacturingYear", SqlDbType.DateTime)
        params(11).Value = customClass.DateMonthYearManufacturingYear
        params(12) = New SqlParameter("@TPLAmountToCustDtg", SqlDbType.VarChar, 100)
        params(12).Value = customClass.TPLAmountToCustDtg
        params(13) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(13).Value = customClass.BusinessDate
        params(14) = New SqlParameter("@IsPageSourceCompanyCustomer", SqlDbType.Bit)
        params(14).Value = customClass.IsPageSourceCompanyCustomer
        params(15) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(15).Value = customClass.ApplicationID
        params(16) = New SqlParameter("@BolRiotDtg", SqlDbType.VarChar, 100)
        params(16).Value = customClass.BolRiotDtg
        params(17) = New SqlParameter("@BolPADtg", SqlDbType.VarChar, 100)
        params(17).Value = customClass.BolPADtg
        params(18) = New SqlParameter("@YearNumRateDtg", SqlDbType.VarChar, 100)
        params(18).Value = customClass.YearNumRateDtg

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, _
            "spShadowInsNewAppInsuranceByCompanyDetailPremiumAmount", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("InsuranceCalculation", "DisplaySelectedPremiumOnGrid", exp.Message + exp.StackTrace)
        End Try
    End Function



End Class
