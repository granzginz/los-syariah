

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region


Public Class NewAppInsuranceByCust
    Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    Private m_connection As SqlConnection
    Private Const SP_SELECT_INSURED_BY_CUSTOMER As String = "spInsNewAppInsuredByCustomerSelect"
    Private Const SP_SAVE_INSURED_BY_CUSTOMER As String = "SpNewAppInsuredByCustomerSave"

#End Region

#Region "GetInsuredByCustomer"

    Public Function GetInsuredByCustomer(ByVal customClass As Parameter.NewAppInsuranceByCust) As Parameter.NewAppInsuranceByCust

        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = customClass.ApplicationID.Trim

        Try
            Dim oReturnValue As New Parameter.NewAppInsuranceByCust
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, SP_SELECT_INSURED_BY_CUSTOMER, params)

            If reader.Read Then
                oReturnValue.BranchId = reader("BranchId").ToString.Trim
                oReturnValue.ApplicationID = reader("ApplicationID").ToString.Trim
                oReturnValue.CustomerName = reader("CustomerName").ToString.Trim
                oReturnValue.MinimumTenor = CType(reader("MinimumTenor").ToString, Int16)
                oReturnValue.MaximumTenor = CType(reader("MaximumTenor").ToString, Int16)
                oReturnValue.PBMaximumTenor = CType(reader("PBMaximumTenor").ToString, Int16)
                oReturnValue.PBMinimumTenor = CType(reader("PBMinimumTenor").ToString, Int16)
                oReturnValue.PMaximumTenor = CType(reader("PMaximumTenor").ToString, Int16)
                oReturnValue.PMinimumTenor = CType(reader("PMinimumTenor").ToString, Int16)
                oReturnValue.InsAssetInsuredByName = reader("InsuredByName").ToString
                oReturnValue.InsAssetPaidByName = reader("PaidByName").ToString
                oReturnValue.InterestType = reader("InterestType").ToString
                oReturnValue.InstallmentScheme = reader("InstallmentScheme").ToString
                oReturnValue.CustomerID = reader("CustomerID").ToString.Trim

            End If
            reader.Close()
            Return oReturnValue

        Catch exp As Exception
            WriteException("NewAppInsuranceByCust", "GetInsuredByCustomer", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region


#Region "ProcessSaveInsuranceByCustomer"

    Public Sub ProcessSaveInsuranceByCustomer(ByVal customClass As Parameter.NewAppInsuranceByCust)
        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            Dim params(10) As SqlParameter
            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customClass.ApplicationID
            params(2) = New SqlParameter("@InsuranceCompany", SqlDbType.Char, 50)
            params(2).Value = customClass.InsuranceCompanyName
            params(3) = New SqlParameter("@SumInsured", SqlDbType.Decimal)
            params(3).Value = customClass.SumInsured
            params(4) = New SqlParameter("@CoverageType", SqlDbType.Char, 3)
            params(4).Value = customClass.CoverageType.Trim
            params(5) = New SqlParameter("@PolicyNo", SqlDbType.Char, 25)
            params(5).Value = customClass.PolicyNo
            params(6) = New SqlParameter("@ExpiredDate", SqlDbType.DateTime)
            params(6).Value = customClass.ExpiredDate
            params(7) = New SqlParameter("@InsNotes", SqlDbType.Char, 500)
            params(7).Value = customClass.InsNotes
            params(8) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(8).Value = customClass.BusinessDate
            params(9) = New SqlParameter("@Tenor", SqlDbType.Float)
            params(9).Value = customClass.Tenor
            params(10) = New SqlParameter("@ApplicationType", SqlDbType.VarChar, 2)
            params(10).Value = customClass.ApplicationTypeDescr


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SP_SAVE_INSURED_BY_CUSTOMER, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("NewAppInsuranceByCust", "ProcessSaveInsuranceByCustomer", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub
#End Region

End Class
