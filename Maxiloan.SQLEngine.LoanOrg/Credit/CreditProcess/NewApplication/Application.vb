#Region "Imports"
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class Application : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    Private Const spPaging As String = "spApplicationPaging"
    Private Const spPagingFactoring As String = "spApplicationPagingFactoring"
    Private Const spPagingModalKerja As String = "spApplicationPagingModalKerja"
    Private Const spCopyFrom As String = "spApplicationCopyFrom"
    Private Const spTC As String = "spApplicationTC"
    Private Const spTC2 As String = "spApplicationTC2"
    Private Const spShowDataAgreement As String = "spApplicationShowDataAgreement"
    Private Const spApplicationSaveAdd As String = "spApplicationSaveAdd"
    Private Const spApplicationSaveAdd2 As String = "spApplicationSaveAdd2"
    Private Const spApplicationSaveAdd3 As String = "spApplicationSaveAdd3"
    Private Const spApplicationSaveAdd4 As String = "spApplicationSaveAdd4"
    Private Const spApplicationSaveEditNonFinancial As String = "spApplicationSaveEditNonFinancial"
    Private Const spApplicationSaveEdit As String = "spApplicationSaveEdit"
    Private Const spApplicationSaveEdit2 As String = "spApplicationSaveEdit2"
    Private Const spApplicationSaveEdit3 As String = "spApplicationSaveEdit3"
    Private Const spApplicationSaveEdit4 As String = "spApplicationSaveEdit4"
    Private Const spApplicationSaveEdit4_1 As String = "spApplicationSaveEdit4_1"
    Private Const spApplicationFee As String = "spApplicationFee"
    Private Const spApplicationEditPaging As String = "spApplicationEditPaging"
    Private Const spApplicationMaintenancePaging As String = "spApplicationMaintenancePaging"
    Private Const spGetCountAppliationId As String = "spGetCountApplicationId"
    Private Const spListProspect As String = "spListProspect"

    Private Const spSyaratCair As String = "spGetSyaratCair"
    Private Const spSyaratCairPO As String = "spGetSyaratCairPO"

    Private Const spCollAct As String = "spCollAct"
    Private Const spHistorySP As String = "spHistorySP"
    Private Const spCollActPrint As String = "spCollActPrint"
    Private Const spHistorySPPrint As String = "spHistorySPPrint"
#End Region

#Region "Application"
    Public Function GetDataProspectType(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@CustomerId", SqlDbType.VarChar)
            params(1).Value = oCustomClass.CustomerId

            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "SpGetDataProspectTypeCustomer", params).Tables(0)
            oReturnValue.Err = ""
            Return oReturnValue
        Catch exp As Exception
            WriteException("Prospect", "GetViewProspect", exp.Message + exp.StackTrace)
            oReturnValue.Err = "Error: " & exp.Message & ""
            Throw New Exception("Error On DataAccess.SalesMarketing.Prospect.GetViewProspect")
            Return oReturnValue
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
        End Try
    End Function

    Public Function GetApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(5) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@isRO", SqlDbType.Bit)
        params(5).Value = oCustomClass.isRO

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spPaging, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetApplication", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetApplication")
        End Try
    End Function

    Public Function GetApplicationFactoring(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(5) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@isRO", SqlDbType.Bit)
        params(5).Value = oCustomClass.isRO

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spPagingFactoring, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetApplication", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetApplication")
        End Try
    End Function
    Public Function GetApplicationModalKerja(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(5) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@isRO", SqlDbType.Bit)
        params(5).Value = oCustomClass.isRO

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spPagingModalKerja, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetApplication", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetApplication")
        End Try
    End Function
    Public Function GetCountAppliationId(ByVal oCustomClass As Parameter.Application) As Integer
        Dim objreader As SqlDataReader
        Dim value As Integer
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@CustomerId", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.CustomerId
        Try
            objreader = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, spGetCountAppliationId, params)
            If objreader.Read Then
                value = CInt(objreader("Total"))
            End If
            objreader.Close()
            Return value
        Catch exp As Exception
            WriteException("Application", "GetApplicationID", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function GetCopyFrom(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerId
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spCopyFrom, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetCopyFrom", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetCopyFrom")
        End Try
    End Function
    Public Function GetTC(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Try
            If oCustomClass.AddEdit = "Edit" Then
                Dim params() As SqlParameter = New SqlParameter(1) {}
                params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params(0).Value = oCustomClass.AppID
                params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                params(1).Value = Replace(oCustomClass.BranchId, "'", "")
                oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spApplicationTCEdit", params).Tables(0)
            ElseIf oCustomClass.AddEdit = "GoLive" Then
                Dim params() As SqlParameter = New SqlParameter(1) {}
                params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params(0).Value = oCustomClass.AppID
                params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                params(1).Value = Replace(oCustomClass.BranchId, "'", "")
                oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGoLiveTC", params).Tables(0)

                Dim params1() As SqlParameter = New SqlParameter(1) {}
                Dim objread As SqlDataReader
                params1(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params1(0).Value = oCustomClass.AppID
                params1(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                params1(1).Value = Replace(oCustomClass.BranchId, "'", "")
                objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spFundingGetFundingDataAgreement", params1)
                If objread.Read Then
                    With oReturnValue
                        .FundingCoyId = CStr(objread("FundingCoyId"))
                        .FundingContractNo = CStr(objread("FundingContractID"))
                        .FundingBatchNo = CStr(objread("FundingBatchID"))
                        .FundingPledgeStatus = CStr(objread("FundingPledgeStatus"))
                    End With
                End If
                objread.Close()
            Else
                Dim params() As SqlParameter = New SqlParameter(4) {}
                params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
                params(0).Value = oCustomClass.ProductID
                params(1) = New SqlParameter("@Branch", SqlDbType.VarChar, 3)
                params(1).Value = Replace(oCustomClass.BranchId, "'", "")
                params(2) = New SqlParameter("@Type", SqlDbType.Char, 1)
                params(2).Value = oCustomClass.Type
                params(3) = New SqlParameter("@Penjamin", SqlDbType.VarChar, 50)
                params(3).Value = oCustomClass.NamaPenjamin
                params(4) = New SqlParameter("@PCType", SqlDbType.Char, 1)
                params(4).Value = oCustomClass.PersonalCustomerType

                oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spTC, params).Tables(0)
            End If
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetTC", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetTC")
        End Try
    End Function
    Public Function GetTC2(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Try
            If oCustomClass.AddEdit = "Edit" Then
                Dim params() As SqlParameter = New SqlParameter(1) {}
                params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params(0).Value = oCustomClass.AppID
                params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                params(1).Value = Replace(oCustomClass.BranchId, "'", "")
                oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spApplicationTC2Edit", params).Tables(0)
            ElseIf oCustomClass.AddEdit = "GoLive" Then
                Dim params() As SqlParameter = New SqlParameter(1) {}
                params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params(0).Value = oCustomClass.AppID
                params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                params(1).Value = Replace(oCustomClass.BranchId, "'", "")
                oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGoLiveTC2", params).Tables(0)

                Dim params1() As SqlParameter = New SqlParameter(1) {}
                Dim objread As SqlDataReader
                params1(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params1(0).Value = oCustomClass.AppID
                params1(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
                params1(1).Value = Replace(oCustomClass.BranchId, "'", "")
                objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spFundingGetFundingDataAgreement", params1)
                If objread.Read Then
                    With oReturnValue
                        .FundingCoyId = CStr(objread("FundingCoyId"))
                        .FundingContractNo = CStr(objread("FundingContractID"))
                        .FundingBatchNo = CStr(objread("FundingBatchID"))
                        .FundingPledgeStatus = CStr(objread("FundingPledgeStatus"))
                    End With
                End If
                objread.Close()
            Else
                Dim params() As SqlParameter = New SqlParameter(2) {}
                params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
                params(0).Value = oCustomClass.ProductID
                params(1) = New SqlParameter("@Branch", SqlDbType.VarChar, 3)
                params(1).Value = Replace(oCustomClass.BranchId, "'", "")
                params(2) = New SqlParameter("@Type", SqlDbType.Char, 1)
                params(2).Value = oCustomClass.Type
                oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spTC2, params).Tables(0)
            End If
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetTC2", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetTC2")
        End Try
    End Function
    Public Function GetAddress(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Dim spName As String
        If oCustomClass.Type = "P" Then
            spName = "spApplicationCopy"
        Else
            spName = "spApplicationCompanyAddress"
        End If
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerId
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spName, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetAddress", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetAddress")
        End Try
    End Function
    Public Function GetShowDataAgreement(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.AgreementNo
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spShowDataAgreement, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetShowDataAgreement", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetShowDataAgreement")
        End Try
    End Function

    Public Function ApplicationSaveAdd(ByVal oApplication As Parameter.Application,
                                       ByVal oRefPersonal As Parameter.Personal,
                                       ByVal oRefAddress As Parameter.Address,
                                       ByVal oMailingAddress As Parameter.Address,
                                       ByVal oData1 As DataTable,
                                       ByVal oData2 As DataTable,
                                       ByVal oData3 As DataTable,
                                       ByVal oGuarantorPersonal As Parameter.Personal,
                                       ByVal oGuarantorAddress As Parameter.Address) As Parameter.Application

        Dim oReturnValue As New Parameter.Application
        Dim conn As New SqlConnection(oApplication.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim ApplicationID As String = ""
        Dim intLoop As Integer

        Dim params() As SqlParameter = New SqlParameter(100) {}
        Dim params1() As SqlParameter = New SqlParameter(8) {}
        Dim params2() As SqlParameter = New SqlParameter(7) {}
        Dim params3() As SqlParameter = New SqlParameter(1) {}

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oApplication.BranchId
            params(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params(1).Value = oApplication.CustomerId
            params(2) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
            params(2).Value = oApplication.ProductID
            params(3) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 10)
            params(3).Value = oApplication.ProductOffID
            params(4) = New SqlParameter("@IsNST", SqlDbType.Char, 1)
            params(4).Value = oApplication.IsNST
            params(5) = New SqlParameter("@ContractStatus", SqlDbType.Char, 3)
            params(5).Value = oApplication.ContractStatus
            params(6) = New SqlParameter("@DefaultStatus", SqlDbType.Char, 3)
            params(6).Value = oApplication.DefaultStatus
            params(7) = New SqlParameter("@ApplicationStep", SqlDbType.Char, 3)
            params(7).Value = oApplication.ApplicationStep
            params(8) = New SqlParameter("@NumOfAssetUnit", SqlDbType.SmallInt)
            params(8).Value = oApplication.NumOfAssetUnit
            params(9) = New SqlParameter("@InterestType", SqlDbType.Char, 2)
            params(9).Value = oApplication.InterestType
            params(10) = New SqlParameter("@InstallmentScheme", SqlDbType.Char, 2)
            params(10).Value = oApplication.InstallmentScheme
            params(11) = New SqlParameter("@BiayaPeriksaBPKB", SqlDbType.Decimal)
            params(11).Value = oApplication.BiayaPeriksaBPKB
            params(12) = New SqlParameter("@kodeDATI", SqlDbType.VarChar, 4)
            params(12).Value = oApplication.KodeDATI
            params(13) = New SqlParameter("@WayOfPayment", SqlDbType.Char, 2)
            params(13).Value = oApplication.WayOfPayment
            params(14) = New SqlParameter("@AgreementDate", SqlDbType.VarChar, 8)
            params(14).Value = oApplication.AgreementDate
            params(15) = New SqlParameter("@SurveyDate", SqlDbType.VarChar, 8)
            params(15).Value = oApplication.SurveyDate
            params(16) = New SqlParameter("@ApplicationSource", SqlDbType.Char, 1)
            params(16).Value = oApplication.ApplicationSource
            params(17) = New SqlParameter("@PercentagePenalty", SqlDbType.Decimal)
            params(17).Value = IIf(oApplication.PercentagePenalty = "", 0, oApplication.PercentagePenalty)
            params(18) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
            params(18).Value = IIf(oApplication.AdminFee = "", 0, oApplication.AdminFee)
            params(19) = New SqlParameter("@FiduciaFee", SqlDbType.Decimal)
            params(19).Value = IIf(oApplication.FiduciaFee = "", 0, oApplication.FiduciaFee)
            params(20) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal)
            params(20).Value = IIf(oApplication.ProvisionFee = "", 0, oApplication.ProvisionFee)
            params(21) = New SqlParameter("@NotaryFee", SqlDbType.Decimal)
            params(21).Value = IIf(oApplication.NotaryFee = "", 0, oApplication.NotaryFee)
            params(22) = New SqlParameter("@SurveyFee", SqlDbType.Decimal)
            params(22).Value = IIf(oApplication.SurveyFee = "", 0, oApplication.SurveyFee)
            params(23) = New SqlParameter("@BBNFee", SqlDbType.Decimal)
            params(23).Value = IIf(oApplication.BBNFee = "", 0, oApplication.BBNFee)
            params(24) = New SqlParameter("@OtherFee", SqlDbType.Decimal)
            params(24).Value = IIf(oApplication.OtherFee = "", 0, oApplication.OtherFee)
            params(25) = New SqlParameter("@MailingFax", SqlDbType.Char, 10)
            params(25).Value = oMailingAddress.Fax
            params(26) = New SqlParameter("@Notes", SqlDbType.Text)
            params(26).Value = oApplication.Notes
            params(27) = New SqlParameter("@ReferenceName", SqlDbType.VarChar, 50)
            params(27).Value = "" 'IIf(oRefPersonal.PersonName <> "", Replace(oRefPersonal.PersonName, "'", "''"), oRefPersonal.PersonName)
            params(28) = New SqlParameter("@ReferenceJobTitle", SqlDbType.VarChar, 50)
            params(28).Value = "" 'oRefPersonal.PersonTitle
            params(29) = New SqlParameter("@ReferenceAddress", SqlDbType.VarChar, 100)
            params(29).Value = "" 'oRefAddress.Address
            params(30) = New SqlParameter("@ReferenceRT", SqlDbType.Char, 3)
            params(30).Value = "" 'oRefAddress.RT
            params(31) = New SqlParameter("@ReferenceRW", SqlDbType.Char, 3)
            params(31).Value = "" 'oRefAddress.RW
            params(32) = New SqlParameter("@ReferenceKelurahan", SqlDbType.VarChar, 30)
            params(32).Value = "" 'oRefAddress.Kelurahan
            params(33) = New SqlParameter("@ReferenceKecamatan", SqlDbType.VarChar, 30)
            params(33).Value = "" 'oRefAddress.Kecamatan
            params(34) = New SqlParameter("@ReferenceCity", SqlDbType.VarChar, 30)
            params(34).Value = "" 'oRefAddress.City
            params(35) = New SqlParameter("@ReferenceZipCode", SqlDbType.Char, 5)
            params(35).Value = "" 'oRefAddress.ZipCode
            params(36) = New SqlParameter("@ReferenceAreaPhone1", SqlDbType.Char, 4)
            params(36).Value = "" 'oRefAddress.AreaPhone1
            params(37) = New SqlParameter("@ReferencePhone1", SqlDbType.Char, 10)
            params(37).Value = "" ' oRefAddress.Phone1
            params(38) = New SqlParameter("@ReferenceAreaPhone2", SqlDbType.Char, 4)
            params(38).Value = "" 'oRefAddress.AreaPhone2
            params(39) = New SqlParameter("@ReferencePhone2", SqlDbType.Char, 10)
            params(39).Value = "" 'oRefAddress.Phone2
            params(40) = New SqlParameter("@ReferenceAreaFax", SqlDbType.Char, 4)
            params(40).Value = "" 'oRefAddress.AreaFax
            params(41) = New SqlParameter("@ReferenceFax", SqlDbType.Char, 10)
            params(41).Value = "" 'oRefAddress.Fax
            params(42) = New SqlParameter("@ReferenceMobilePhone", SqlDbType.VarChar, 20)
            params(42).Value = "" 'oRefPersonal.MobilePhone
            params(43) = New SqlParameter("@ReferenceEmail", SqlDbType.VarChar, 30)
            params(43).Value = "" 'oRefPersonal.Email
            params(44) = New SqlParameter("@ReferenceNotes", SqlDbType.Text)
            params(44).Value = "" 'oApplication.RefNotes
            params(45) = New SqlParameter("@MailingAddress", SqlDbType.VarChar, 100)
            params(45).Value = oMailingAddress.Address
            params(46) = New SqlParameter("@MailingRT", SqlDbType.Char, 3)
            params(46).Value = oMailingAddress.RT
            params(47) = New SqlParameter("@MailingRW", SqlDbType.Char, 3)
            params(47).Value = oMailingAddress.RW
            params(48) = New SqlParameter("@MailingKelurahan", SqlDbType.VarChar, 30)
            params(48).Value = oMailingAddress.Kelurahan
            params(49) = New SqlParameter("@MailingKecamatan", SqlDbType.VarChar, 30)
            params(49).Value = oMailingAddress.Kecamatan
            params(50) = New SqlParameter("@MailingCity", SqlDbType.VarChar, 30)
            params(50).Value = oMailingAddress.City
            params(51) = New SqlParameter("@MailingZipCode", SqlDbType.Char, 5)
            params(51).Value = oMailingAddress.ZipCode
            params(52) = New SqlParameter("@MailingAreaPhone1", SqlDbType.Char, 4)
            params(52).Value = oMailingAddress.AreaPhone1
            params(53) = New SqlParameter("@MailingPhone1", SqlDbType.Char, 10)
            params(53).Value = oMailingAddress.Phone1
            params(54) = New SqlParameter("@MailingAreaPhone2", SqlDbType.Char, 4)
            params(54).Value = oMailingAddress.AreaPhone2
            params(55) = New SqlParameter("@MailingPhone2", SqlDbType.Char, 10)
            params(55).Value = oMailingAddress.Phone2
            params(56) = New SqlParameter("@MailingAreaFax", SqlDbType.Char, 4)
            params(56).Value = oMailingAddress.AreaFax
            params(57) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(57).Direction = ParameterDirection.Output
            params(58) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(58).Direction = ParameterDirection.Output
            params(59) = New SqlParameter("@businessdate", SqlDbType.DateTime)
            params(59).Value = oApplication.BusinessDate
            params(60) = New SqlParameter("@AdditionalAdminFee", SqlDbType.Decimal)
            params(60).Value = oApplication.AdditionalAdminFee
            params(61) = New SqlParameter("@StepUpStepDownType", SqlDbType.Char, 2)
            params(61).Value = IIf(oApplication.StepUpStepDownType <> "", oApplication.StepUpStepDownType, DBNull.Value)
            params(62) = New SqlParameter("@StatusFiducia", SqlDbType.Bit)
            params(62).Value = oApplication.StatusFiduciaFee
            params(63) = New SqlParameter("@ProspectAppID", SqlDbType.Char, 20)
            params(63).Value = oApplication.ProspectAppID
            params(64) = New SqlParameter("@IsAdminFeeCredit", SqlDbType.Bit)
            params(64).Value = oApplication.IsAdminFeeCredit
            params(65) = New SqlParameter("@IsProvisiCredit", SqlDbType.Bit)
            params(65).Value = oApplication.IsProvisiCredit
            params(66) = New SqlParameter("@JenisPembiayaan", SqlDbType.Char, 2)
            params(66).Value = oApplication.JenisPembiayaan
            params(67) = New SqlParameter("@PolaTransaksi", SqlDbType.Char, 2)
            params(67).Value = oApplication.PolaTransaksi
            params(68) = New SqlParameter("@AdminFeeGross", SqlDbType.Decimal)
            params(68).Value = oApplication.AdminFeeGross
            params(69) = New SqlParameter("@Refinancing", SqlDbType.Bit)
            params(69).Value = oApplication.Refinancing
            params(70) = New SqlParameter("@IsFleet", SqlDbType.Bit)
            params(70).Value = oApplication.IsFleet
            params(71) = New SqlParameter("@NoApplicationData", SqlDbType.Bit)
            params(71).Value = oApplication.NoApplicationData
            params(72) = New SqlParameter("@BiayaPolis", SqlDbType.Decimal)
            params(72).Value = IIf(oApplication.BiayaPolis = "", 0, oApplication.BiayaPolis)
            params(73) = New SqlParameter("@NoPJJ", SqlDbType.Char, 20)
            params(73).Value = oApplication.NoPJJ
            params(74) = New SqlParameter("@RefinancingApplicationID", SqlDbType.Char, 20)
            params(74).Value = oApplication.RefinancingApplicationID

            params(75) = New SqlParameter("@KegiatanUsaha", SqlDbType.Char, 1)
            params(75).Value = oApplication.KegiatanUsaha

            params(76) = New SqlParameter("@IsCOP", SqlDbType.Char, 1)
            params(76).Value = oApplication.IsCOP
            params(77) = New SqlParameter("@LiniBisnis", SqlDbType.Char, 10)
            params(77).Value = oApplication.LiniBisnis
            params(78) = New SqlParameter("@TipeLoan", SqlDbType.Char, 1)
            params(78).Value = oApplication.TipeLoan
            params(79) = New SqlParameter("@HakOpsi", SqlDbType.Bit)
            params(79).Value = oApplication.HakOpsi
            params(80) = New SqlParameter("@ApplicationModule", SqlDbType.Char, 10)
            params(80).Value = oApplication.ApplicationModule
            '<<<<<<< .mine
            params(81) = New SqlParameter("@HandlingFee", SqlDbType.Decimal)
            params(81).Value = IIf(oApplication.HandlingFee = "", 0, oApplication.HandlingFee)
            params(82) = New SqlParameter("@UnitBisnis", SqlDbType.Char, 25)
            params(82).Value = oApplication.UnitBisnis
            params(83) = New SqlParameter("@ReferalFee", SqlDbType.Decimal)
            params(83).Value = IIf(oApplication.ReferalFee = "", 0, oApplication.ReferalFee)
            params(84) = New SqlParameter("@Lokasi", SqlDbType.Char, 50)
            params(84).Value = oApplication.Lokasi
            params(85) = New SqlParameter("@PorsiInstitusi", SqlDbType.Decimal)
            params(85).Value = IIf(oApplication.PorsiInstitusi = "", 0, oApplication.PorsiInstitusi)
            params(86) = New SqlParameter("@NPP", SqlDbType.Char, 50)
            params(86).Value = oApplication.NPP
            params(87) = New SqlParameter("@PorsiPemberiReferal", SqlDbType.Decimal)
            params(87).Value = IIf(oApplication.PorsiPemberiReferal = "", 0, oApplication.PorsiPemberiReferal)
            params(88) = New SqlParameter("@NPWP", SqlDbType.Char, 50)
            params(88).Value = oApplication.NPWP
            params(89) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(89).Value = oApplication.Name
            'params(90) = New SqlParameter("@UsrUpd", SqlDbType.VarChar)
            'params(90).Value = oApplication.UsrUpd
            'params(91) = New SqlParameter("@DtmUpd", SqlDbType.Date)
            'params(91).Value = oApplication.DtmUpd

            params(90) = New SqlParameter("@FactoringPiutangDibiayai", SqlDbType.Decimal)
            params(90).Value = IIf(oApplication.FactoringPiutangDibiayai = Nothing, 0, oApplication.FactoringPiutangDibiayai)
            params(91) = New SqlParameter("@FactoringRetensi", SqlDbType.Decimal)
            params(91).Value = IIf(oApplication.FactoringRetensi = Nothing, 0, oApplication.FactoringRetensi)
            params(92) = New SqlParameter("@FactoringInterest", SqlDbType.Decimal)
            params(92).Value = IIf(oApplication.FactoringInterest = Nothing, 0, oApplication.FactoringInterest)
            params(93) = New SqlParameter("@FactoringPPN", SqlDbType.Decimal)
            params(93).Value = IIf(oApplication.FactoringPPN = Nothing, 0, oApplication.FactoringPPN)
            params(94) = New SqlParameter("@SupplierID", SqlDbType.Char, 20)
            params(94).Value = oApplication.SupplierID
            params(95) = New SqlParameter("@FactoringPencairanDate", SqlDbType.VarChar, 8)
            params(95).Value = oApplication.FactoringPencairanDate
            params(96) = New SqlParameter("@FactoringPaymentMethod", SqlDbType.Char, 1)
            params(96).Value = oApplication.FactoringPaymentMethod

            params(97) = New SqlParameter("@FacilityNo", SqlDbType.Char, 20)
            params(97).Value = oApplication.FacilityNo
            params(98) = New SqlParameter("@isAdmCapitalized", SqlDbType.Bit)
            params(98).Value = oApplication.isAdmCapitalized

            params(99) = New SqlParameter("@isUpAdmCapitalized", SqlDbType.Bit)
            params(99).Value = oApplication.isUpAdmCapitalized

            params(100) = New SqlParameter("@AOID", SqlDbType.Char, 20)
            params(100).Value = oApplication.AOID

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveAdd, params)

            ErrMessage = CType(params(57).Value, String)
            ApplicationID = CType(params(58).Value, String)

            'Jika application id sudah digenerate dahulu
            'ApplicationID = oApplication.AppID

            If ErrMessage <> "" Then
                oReturnValue.ApplicationID = ""
                oReturnValue.Err = ErrMessage
                Return oReturnValue
                Exit Function
            End If

            If oData1.Rows.Count > 0 Then
                params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params1(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params1(3) = New SqlParameter("@PriorTo", SqlDbType.Char, 3)
                params1(4) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
                params1(5) = New SqlParameter("@IsMandatory", SqlDbType.Char, 1)
                params1(6) = New SqlParameter("@PromiseDate", SqlDbType.VarChar, 8)
                params1(7) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params1(8) = New SqlParameter("@Notes", SqlDbType.Text)
                For intLoop = 0 To oData1.Rows.Count - 1
                    params1(0).Value = oApplication.BranchId
                    params1(1).Value = ApplicationID
                    params1(2).Value = oData1.Rows(intLoop).Item("MasterTCID")
                    params1(3).Value = oData1.Rows(intLoop).Item("PriorTo")
                    params1(4).Value = oData1.Rows(intLoop).Item("IsChecked")
                    params1(5).Value = oData1.Rows(intLoop).Item("IsMandatory")
                    params1(6).Value = oData1.Rows(intLoop).Item("PromiseDate")
                    params1(7).Value = oApplication.BusinessDate
                    params1(8).Value = oData1.Rows(intLoop).Item("Notes")

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveAdd2, params1)
                Next
            End If

            If oData2.Rows.Count > 0 Then
                params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params2(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params2(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params2(3) = New SqlParameter("@AGTCCLSequenceNo", SqlDbType.SmallInt)
                params2(4) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
                params2(5) = New SqlParameter("@PromiseDate", SqlDbType.VarChar, 8)
                params2(6) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params2(7) = New SqlParameter("@Notes", SqlDbType.Text)
                For intLoop = 0 To oData2.Rows.Count - 1
                    params2(0).Value = oApplication.BranchId
                    params2(1).Value = ApplicationID
                    params2(2).Value = oData2.Rows(intLoop).Item("MasterTCID")
                    params2(3).Value = CInt(oData2.Rows(intLoop).Item("AGTCCLSequenceNo"))
                    params2(4).Value = oData2.Rows(intLoop).Item("IsChecked")
                    params2(5).Value = oData2.Rows(intLoop).Item("PromiseDate")
                    params2(6).Value = oApplication.BusinessDate
                    params2(7).Value = oData2.Rows(intLoop).Item("Notes")

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveAdd3, params2)
                Next
            End If

            If oData3.Rows.Count > 0 Then
                params3(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params3(1) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
                For intLoop = 0 To oData3.Rows.Count - 1
                    params3(0).Value = ApplicationID
                    params3(1).Value = oData3.Rows(intLoop).Item("AgreementNo")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveAdd4, params3)
                Next
            End If

            'Dim parGuarantor() As SqlParameter = New SqlParameter(20) {}

            'parGuarantor(0) = New SqlParameter("@mode", SqlDbType.Bit)
            'parGuarantor(0).Value = 1
            'parGuarantor(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            'parGuarantor(1).Value = ApplicationID
            'parGuarantor(2) = New SqlParameter("@GuarantorName", SqlDbType.VarChar, 50)
            'parGuarantor(2).Value = oGuarantorPersonal.PersonName
            'parGuarantor(3) = New SqlParameter("@GuarantorJobTitle", SqlDbType.VarChar, 50)
            'parGuarantor(3).Value = oGuarantorPersonal.PersonTitle
            'parGuarantor(4) = New SqlParameter("@GuarantorAddress", SqlDbType.VarChar, 50)
            'parGuarantor(4).Value = oGuarantorAddress.Address
            'parGuarantor(5) = New SqlParameter("@GuarantorRT", SqlDbType.Char, 3)
            'parGuarantor(5).Value = oGuarantorAddress.RT
            'parGuarantor(6) = New SqlParameter("@GuarantorRW", SqlDbType.Char, 3)
            'parGuarantor(6).Value = oGuarantorAddress.RW
            'parGuarantor(7) = New SqlParameter("@GuarantorKelurahan", SqlDbType.VarChar, 30)
            'parGuarantor(7).Value = oGuarantorAddress.Kelurahan
            'parGuarantor(8) = New SqlParameter("@GuarantorKecamatan", SqlDbType.VarChar, 30)
            'parGuarantor(8).Value = oGuarantorAddress.Kecamatan
            'parGuarantor(9) = New SqlParameter("@GuarantorCity", SqlDbType.VarChar, 30)
            'parGuarantor(9).Value = oGuarantorAddress.City
            'parGuarantor(10) = New SqlParameter("@GuarantorZipCode", SqlDbType.Char, 5)
            'parGuarantor(10).Value = oGuarantorAddress.ZipCode
            'parGuarantor(11) = New SqlParameter("@GuarantorAreaPhone1", SqlDbType.Char, 4)
            'parGuarantor(11).Value = oGuarantorAddress.AreaPhone1
            'parGuarantor(12) = New SqlParameter("@GuarantorPhone1", SqlDbType.VarChar, 10)
            'parGuarantor(12).Value = oGuarantorAddress.Phone1
            'parGuarantor(13) = New SqlParameter("@GuarantorAreaPhone2", SqlDbType.Char, 4)
            'parGuarantor(13).Value = oGuarantorAddress.AreaPhone2
            'parGuarantor(14) = New SqlParameter("@GuarantorPhone2", SqlDbType.Char, 10)
            'parGuarantor(14).Value = oGuarantorAddress.Phone2
            'parGuarantor(15) = New SqlParameter("@GuarantorAreaFax", SqlDbType.Char, 4)
            'parGuarantor(15).Value = oGuarantorAddress.AreaFax
            'parGuarantor(15) = New SqlParameter("@GuarantorAreaFax", SqlDbType.Char, 4)
            'parGuarantor(15).Value = oGuarantorAddress.AreaFax
            'parGuarantor(16) = New SqlParameter("@GuarantorFax", SqlDbType.VarChar, 10)
            'parGuarantor(16).Value = oGuarantorAddress.Fax
            'parGuarantor(17) = New SqlParameter("@GuarantorMobilePhone", SqlDbType.VarChar, 20)
            'parGuarantor(17).Value = oGuarantorPersonal.MobilePhone
            'parGuarantor(18) = New SqlParameter("@GuarantorEmail", SqlDbType.VarChar, 30)
            'parGuarantor(18).Value = oGuarantorPersonal.Email
            'parGuarantor(19) = New SqlParameter("@GuarantorNotes", SqlDbType.Text)
            'parGuarantor(19).Value = oGuarantorPersonal.Catatan
            'parGuarantor(20) = New SqlParameter("@GuarantorPenghasilan", SqlDbType.Money)
            'parGuarantor(20).Value = oGuarantorPersonal.Penghasilan

            'SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spApplicationSaveAddGuarantor", parGuarantor)

            transaction.Commit()
            oReturnValue.ApplicationID = ApplicationID

            'Jika application id sudah digenerate dahulu
            'oReturnValue.ApplicationID = oApplication.AppID

            oReturnValue.Err = ErrMessage
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Application", "ApplicationSaveAdd", exp.Message + exp.StackTrace)

            oReturnValue.ApplicationID = ApplicationID

            'Jika application id sudah digenerate dahulu
            'oReturnValue.ApplicationID = oApplication.AppID

            oReturnValue.Err = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccAcq.Customer.ApplicationSaveAdd")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function GetFee(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
        params(0).Value = oCustomClass.ProductID
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = Replace(oCustomClass.BranchId, "'", "")
        params(2) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 10)
        params(2).Value = oCustomClass.ProductOffID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spApplicationFee, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetFee", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetFee")
        End Try
    End Function
#End Region

#Region "View Application"
    Public Function GetViewApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim intLoop As Integer
        Dim conn As New SqlConnection(oCustomClass.strConnection)

        Dim transaction As SqlTransaction = Nothing
        Dim spViewApplication As String
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = oCustomClass.AppID
            For intLoop = 0 To 3
                Select Case intLoop
                    Case 0
                        spViewApplication = "spViewApplication" + "CD"
                        oReturnValue.DataCD = SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spViewApplication, params).Tables(0)
                    Case 1
                        spViewApplication = "spViewApplication" + "TC"
                        oReturnValue.DataTC = SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spViewApplication, params).Tables(0)
                    Case 2
                        spViewApplication = "spViewApplication" + "TC2"
                        oReturnValue.DataTC2 = SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spViewApplication, params).Tables(0)
                    Case 3
                        spViewApplication = "spViewApplication"
                        oReturnValue.ListData = SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spViewApplication, params).Tables(0)
                End Select
            Next
            transaction.Commit()
            oReturnValue.Err = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Application", "GetViewApplication", exp.Message + exp.StackTrace)
            oReturnValue.Err = "Error: " & exp.Message & ""
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetViewApplication")
            Return oReturnValue
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
        End Try
    End Function
#End Region

#Region "ApplicationMaintenance"
    Public Function GetApplicationMaintenance(ByVal customClass As Parameter.Application) As Parameter.Application
        Try
            Dim oReturnValue As New Parameter.Application
            Dim params() As SqlParameter = New SqlParameter(5) {}
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customClass.CurrentPage
            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customClass.PageSize
            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
            params(2).Value = customClass.WhereCond
            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
            params(3).Value = customClass.SortBy
            params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output
            params(5) = New SqlParameter("@WhereCond2", SqlDbType.VarChar)
            params(5).Value = customClass.WhereCond2

            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spAApplicationMaintenancePaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetApplicationMaintenance", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("Application.VB - DA", "GetApplicationMaintenance", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            Throw New Exception(exp.StackTrace)
        End Try
    End Function
    Public Function GetCD(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = oCustomClass.AppID
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spApplicationCDEdit", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetCD", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetCD")
        End Try
    End Function
    Public Function GetApplicationEdit(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = oCustomClass.AppID
            params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(1).Value = Replace(oCustomClass.BranchId, "'", "")
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spApplicationEdit", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetApplicationEdit", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetApplicationEdit")
        End Try
    End Function

    Public Function GetApplicationEditPPH(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = oCustomClass.AppID
            params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(1).Value = Replace(oCustomClass.BranchId, "'", "")
            params(2) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
            params(2).Value = oCustomClass.InvoiceSeqNo
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spApplicationEditPPH", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetApplicationEditPPH", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetApplicationEditPPH")
        End Try
    End Function
    Public Function ApplicationSaveEdit(ByVal oApplication As Parameter.Application,
                                        ByVal oRefPersonal As Parameter.Personal,
                                        ByVal oRefAddress As Parameter.Address,
                                        ByVal oMailingAddress As Parameter.Address,
                                        ByVal oData1 As DataTable,
                                        ByVal oData2 As DataTable,
                                        ByVal oData3 As DataTable,
                                        ByVal oGuarantorPersonal As Parameter.Personal,
                                        ByVal oGuarantorAddress As Parameter.Address) As Parameter.Application
        Dim ErrMessage As String
        Dim oReturnValue As New Parameter.Application
        Dim conn As New SqlConnection(oApplication.strConnection)
        Dim transaction As SqlTransaction = Nothing

        Dim intLoop As Integer
        Dim params() As SqlParameter = New SqlParameter(91) {}

        Dim params1() As SqlParameter = New SqlParameter(8) {}
        Dim params2() As SqlParameter = New SqlParameter(7) {}
        Dim params3_1() As SqlParameter = New SqlParameter(0) {}
        Dim params3() As SqlParameter = New SqlParameter(1) {}
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oApplication.BranchId
            params(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params(1).Value = oApplication.CustomerId
            params(2) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
            params(2).Value = oApplication.ProductID
            params(3) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 10)
            params(3).Value = oApplication.ProductOffID
            params(4) = New SqlParameter("@MailingPhone1", SqlDbType.Char, 10)
            params(4).Value = oMailingAddress.Phone1
            params(5) = New SqlParameter("@MailingAreaPhone2", SqlDbType.Char, 4)
            params(5).Value = oMailingAddress.AreaPhone2
            params(6) = New SqlParameter("@MailingPhone2", SqlDbType.Char, 10)
            params(6).Value = oMailingAddress.Phone2
            params(7) = New SqlParameter("@MailingAreaFax", SqlDbType.Char, 4)
            params(7).Value = oMailingAddress.AreaFax
            params(8) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(8).Value = oApplication.ApplicationID
            params(9) = New SqlParameter("@InterestType", SqlDbType.Char, 2)
            params(9).Value = oApplication.InterestType
            params(10) = New SqlParameter("@InstallmentScheme", SqlDbType.Char, 2)
            params(10).Value = oApplication.InstallmentScheme
            params(11) = New SqlParameter("@BiayaPeriksaBPKB", SqlDbType.Decimal)
            params(11).Value = oApplication.BiayaPeriksaBPKB
            params(12) = New SqlParameter("@KodeDATI", SqlDbType.Char, 4)
            params(12).Value = oApplication.KodeDATI
            params(13) = New SqlParameter("@WayOfPayment", SqlDbType.Char, 2)
            params(13).Value = oApplication.WayOfPayment
            params(14) = New SqlParameter("@AgreementDate", SqlDbType.VarChar, 8)
            params(14).Value = oApplication.AgreementDate
            params(15) = New SqlParameter("@SurveyDate", SqlDbType.VarChar, 8)
            params(15).Value = oApplication.SurveyDate
            params(16) = New SqlParameter("@ApplicationSource", SqlDbType.Char, 1)
            params(16).Value = oApplication.ApplicationSource
            params(17) = New SqlParameter("@PercentagePenalty", SqlDbType.Decimal)
            params(17).Value = IIf(oApplication.PercentagePenalty = "", 0, oApplication.PercentagePenalty)
            params(18) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
            params(18).Value = IIf(oApplication.AdminFee = "", 0, oApplication.AdminFee)
            params(19) = New SqlParameter("@FiduciaFee", SqlDbType.Decimal)
            params(19).Value = IIf(oApplication.FiduciaFee = "", 0, oApplication.FiduciaFee)
            params(20) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal)
            params(20).Value = IIf(oApplication.ProvisionFee = "", 0, oApplication.ProvisionFee)
            params(21) = New SqlParameter("@NotaryFee", SqlDbType.Decimal)
            params(21).Value = IIf(oApplication.NotaryFee = "", 0, oApplication.NotaryFee)
            params(22) = New SqlParameter("@SurveyFee", SqlDbType.Decimal)
            params(22).Value = IIf(oApplication.SurveyFee = "", 0, oApplication.SurveyFee)
            params(23) = New SqlParameter("@BBNFee", SqlDbType.Decimal)
            params(23).Value = IIf(oApplication.BBNFee = "", 0, oApplication.BBNFee)
            params(24) = New SqlParameter("@OtherFee", SqlDbType.Decimal)
            params(24).Value = IIf(oApplication.OtherFee = "", 0, oApplication.OtherFee)
            params(25) = New SqlParameter("@MailingFax", SqlDbType.Char, 10)
            params(25).Value = oMailingAddress.Fax
            params(26) = New SqlParameter("@Notes", SqlDbType.Text)
            params(26).Value = oApplication.Notes
            params(27) = New SqlParameter("@ReferenceName", SqlDbType.VarChar, 50)
            params(27).Value = "" 'IIf(oRefPersonal.PersonName <> "", Replace(oRefPersonal.PersonName, "'", "''"), oRefPersonal.PersonName)
            params(28) = New SqlParameter("@ReferenceJobTitle", SqlDbType.VarChar, 50)
            params(28).Value = ""
            params(29) = New SqlParameter("@ReferenceAddress", SqlDbType.VarChar, 100)
            params(29).Value = oRefAddress.Address
            params(30) = New SqlParameter("@ReferenceRT", SqlDbType.Char, 3)
            params(30).Value = oRefAddress.RT
            params(31) = New SqlParameter("@ReferenceRW", SqlDbType.Char, 3)
            params(31).Value = oRefAddress.RW
            params(32) = New SqlParameter("@ReferenceKelurahan", SqlDbType.VarChar, 30)
            params(32).Value = oRefAddress.Kelurahan
            params(33) = New SqlParameter("@ReferenceKecamatan", SqlDbType.VarChar, 30)
            params(33).Value = oRefAddress.Kecamatan
            params(34) = New SqlParameter("@ReferenceCity", SqlDbType.VarChar, 30)
            params(34).Value = oRefAddress.City
            params(35) = New SqlParameter("@ReferenceZipCode", SqlDbType.Char, 5)
            params(35).Value = oRefAddress.ZipCode
            params(36) = New SqlParameter("@ReferenceAreaPhone1", SqlDbType.Char, 4)
            params(36).Value = oRefAddress.AreaPhone1
            params(37) = New SqlParameter("@ReferencePhone1", SqlDbType.Char, 10)
            params(37).Value = oRefAddress.Phone1
            params(38) = New SqlParameter("@ReferenceAreaPhone2", SqlDbType.Char, 4)
            params(38).Value = oRefAddress.AreaPhone2
            params(39) = New SqlParameter("@ReferencePhone2", SqlDbType.Char, 10)
            params(39).Value = oRefAddress.Phone2
            params(40) = New SqlParameter("@ReferenceAreaFax", SqlDbType.Char, 4)
            params(40).Value = oRefAddress.AreaFax
            params(41) = New SqlParameter("@ReferenceFax", SqlDbType.Char, 10)
            params(41).Value = oRefAddress.Fax
            params(42) = New SqlParameter("@ReferenceMobilePhone", SqlDbType.VarChar, 20)
            params(42).Value = oRefPersonal.MobilePhone
            params(43) = New SqlParameter("@ReferenceEmail", SqlDbType.VarChar, 30)
            params(43).Value = oRefPersonal.Email
            params(44) = New SqlParameter("@ReferenceNotes", SqlDbType.Text)
            params(44).Value = oApplication.RefNotes
            params(45) = New SqlParameter("@MailingAddress", SqlDbType.VarChar, 100)
            params(45).Value = oMailingAddress.Address
            params(46) = New SqlParameter("@MailingRT", SqlDbType.Char, 3)
            params(46).Value = oMailingAddress.RT
            params(47) = New SqlParameter("@MailingRW", SqlDbType.Char, 3)
            params(47).Value = oMailingAddress.RW
            params(48) = New SqlParameter("@MailingKelurahan", SqlDbType.VarChar, 30)
            params(48).Value = oMailingAddress.Kelurahan
            params(49) = New SqlParameter("@MailingKecamatan", SqlDbType.VarChar, 30)
            params(49).Value = oMailingAddress.Kecamatan
            params(50) = New SqlParameter("@MailingCity", SqlDbType.VarChar, 30)
            params(50).Value = oMailingAddress.City
            params(51) = New SqlParameter("@MailingZipCode", SqlDbType.Char, 5)
            params(51).Value = oMailingAddress.ZipCode
            params(52) = New SqlParameter("@MailingAreaPhone1", SqlDbType.Char, 4)
            params(52).Value = oMailingAddress.AreaPhone1
            params(53) = New SqlParameter("@AdditionalAdminFee", SqlDbType.Decimal)
            params(53).Value = oApplication.AdditionalAdminFee
            params(54) = New SqlParameter("@StepUpStepDownType", SqlDbType.Char, 2)
            params(54).Value = IIf(oApplication.StepUpStepDownType <> "", oApplication.StepUpStepDownType, DBNull.Value)
            params(55) = New SqlParameter("@StatusFiducia", SqlDbType.Bit)
            params(55).Value = oApplication.StatusFiduciaFee
            params(56) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(56).Direction = ParameterDirection.Output
            params(57) = New SqlParameter("@JenisPembiayaan", SqlDbType.Char, 2)
            params(57).Value = oApplication.JenisPembiayaan
            params(58) = New SqlParameter("@PolaTransaksi", SqlDbType.Char, 2)
            params(58).Value = oApplication.PolaTransaksi
            params(59) = New SqlParameter("@AdminFeeGross", SqlDbType.Decimal)
            params(59).Value = oApplication.AdminFeeGross
            params(60) = New SqlParameter("@Refinancing", SqlDbType.Bit)
            params(60).Value = oApplication.Refinancing
            params(61) = New SqlParameter("@IsFleet", SqlDbType.Bit)
            params(61).Value = oApplication.IsFleet
            params(62) = New SqlParameter("@NoApplicationData", SqlDbType.Bit)
            params(62).Value = oApplication.NoApplicationData
            params(63) = New SqlParameter("@BiayaPolis", SqlDbType.Decimal)
            params(63).Value = IIf(oApplication.BiayaPolis = "", 0, oApplication.BiayaPolis)
            params(64) = New SqlParameter("@NoPJJ", SqlDbType.Char, 20)
            params(64).Value = oApplication.NoPJJ
            params(65) = New SqlParameter("@RefinancingApplicationID", SqlDbType.Char, 20)
            params(65).Value = oApplication.RefinancingApplicationID
            params(66) = New SqlParameter("@KegiatanUsaha", SqlDbType.Char, 1)
            params(66).Value = oApplication.KegiatanUsaha
            params(67) = New SqlParameter("@IsCOP", SqlDbType.Char, 1)
            params(67).Value = oApplication.IsCOP
            params(68) = New SqlParameter("@LiniBisnis", SqlDbType.Char, 10)
            params(68).Value = oApplication.LiniBisnis
            params(69) = New SqlParameter("@TipeLoan", SqlDbType.Char, 1)
            params(69).Value = oApplication.TipeLoan
            params(70) = New SqlParameter("@HakOpsi", SqlDbType.Bit)
            params(70).Value = oApplication.HakOpsi
            params(71) = New SqlParameter("@ApplicationModule", SqlDbType.Char, 10)
            params(71).Value = oApplication.ApplicationModule
            params(72) = New SqlParameter("@HandlingFee", SqlDbType.Decimal)
            params(72).Value = IIf(oApplication.HandlingFee = "", 0, oApplication.HandlingFee)
            params(73) = New SqlParameter("@UnitBisnis", SqlDbType.Char, 25)
            params(73).Value = oApplication.UnitBisnis
            params(74) = New SqlParameter("@ReferalFee", SqlDbType.Decimal)
            params(74).Value = IIf(oApplication.ReferalFee = "", 0, oApplication.ReferalFee)
            params(75) = New SqlParameter("@Lokasi", SqlDbType.Char, 50)
            params(75).Value = oApplication.Lokasi
            params(76) = New SqlParameter("@PorsiInstitusi", SqlDbType.Decimal)
            params(76).Value = IIf(oApplication.PorsiInstitusi = "", 0, oApplication.PorsiInstitusi)
            params(77) = New SqlParameter("@NPP", SqlDbType.Char, 50)
            params(77).Value = oApplication.NPP
            params(78) = New SqlParameter("@PorsiPemberiReferal", SqlDbType.Decimal)
            params(78).Value = IIf(oApplication.PorsiPemberiReferal = "", 0, oApplication.PorsiPemberiReferal)
            params(79) = New SqlParameter("@NPWP", SqlDbType.Char, 50)
            params(79).Value = oApplication.NPWP
            params(80) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(80).Value = oApplication.Name
            'params(81) = New SqlParameter("@UsrUpd", SqlDbType.VarChar)
            'params(81).Value = oApplication.UsrUpd
            'params(82) = New SqlParameter("@DtmUpd", SqlDbType.Date)
            'params(82).Value = oApplication.DtmUpd

            params(81) = New SqlParameter("@FactoringPiutangDibiayai", SqlDbType.Decimal)
            params(81).Value = IIf(oApplication.FactoringPiutangDibiayai = Nothing, 0, oApplication.FactoringPiutangDibiayai)
            params(82) = New SqlParameter("@FactoringRetensi", SqlDbType.Decimal)
            params(82).Value = IIf(oApplication.FactoringRetensi = Nothing, 0, oApplication.FactoringRetensi)
            params(83) = New SqlParameter("@FactoringInterest", SqlDbType.Decimal)
            params(83).Value = IIf(oApplication.FactoringInterest = Nothing, 0, oApplication.FactoringInterest)
            params(84) = New SqlParameter("@FactoringPPN", SqlDbType.Decimal)
            params(84).Value = IIf(oApplication.FactoringPPN = Nothing, 0, oApplication.FactoringPPN)

            params(85) = New SqlParameter("@SupplierID", SqlDbType.Char, 20)
            params(85).Value = oApplication.SupplierID
            params(86) = New SqlParameter("@FactoringPencairanDate", SqlDbType.VarChar, 8)
            params(86).Value = oApplication.FactoringPencairanDate
            params(87) = New SqlParameter("@FactoringPaymentMethod", SqlDbType.Char, 1)
            params(87).Value = oApplication.FactoringPaymentMethod

            params(88) = New SqlParameter("@isAdmCapitalized", SqlDbType.Bit)
            params(88).Value = oApplication.isAdmCapitalized

            params(89) = New SqlParameter("@isUpAdmCapitalized", SqlDbType.Bit)
            params(89).Value = oApplication.isUpAdmCapitalized

            params(90) = New SqlParameter("@AOID", SqlDbType.Char, 20)
            params(90).Value = oApplication.AOID

            params(91) = New SqlParameter("@FacilityNo", SqlDbType.Char, 20)
            params(91).Value = oApplication.FacilityNo

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveEdit, params)
            ErrMessage = CType(IIf(IsDBNull(params(56).Value), "", params(56).Value), String)

            If oData1.Rows.Count > 0 Then
                params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params1(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params1(3) = New SqlParameter("@PriorTo", SqlDbType.Char, 3)
                params1(4) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
                params1(5) = New SqlParameter("@IsMandatory", SqlDbType.Char, 1)
                params1(6) = New SqlParameter("@PromiseDate", SqlDbType.VarChar, 8)
                params1(7) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params1(8) = New SqlParameter("@Notes", SqlDbType.Text)
                For intLoop = 0 To oData1.Rows.Count - 1
                    params1(0).Value = oApplication.BranchId
                    params1(1).Value = oApplication.ApplicationID
                    params1(2).Value = oData1.Rows(intLoop).Item("MasterTCID")
                    params1(3).Value = oData1.Rows(intLoop).Item("PriorTo")
                    params1(4).Value = oData1.Rows(intLoop).Item("IsChecked")
                    params1(5).Value = oData1.Rows(intLoop).Item("IsMandatory")
                    params1(6).Value = oData1.Rows(intLoop).Item("PromiseDate")
                    params1(7).Value = oApplication.BusinessDate
                    params1(8).Value = oData1.Rows(intLoop).Item("Notes")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveEdit2, params1)
                Next
            End If
            If oData2.Rows.Count > 0 Then
                params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params2(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params2(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params2(3) = New SqlParameter("@AGTCCLSequenceNo", SqlDbType.SmallInt)
                params2(4) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
                params2(5) = New SqlParameter("@PromiseDate", SqlDbType.VarChar, 8)
                params2(6) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params2(7) = New SqlParameter("@Notes", SqlDbType.Text)
                For intLoop = 0 To oData2.Rows.Count - 1
                    params2(0).Value = oApplication.BranchId
                    params2(1).Value = oApplication.ApplicationID
                    params2(2).Value = oData2.Rows(intLoop).Item("MasterTCID")
                    params2(3).Value = CInt(oData2.Rows(intLoop).Item("AGTCCLSequenceNo"))
                    params2(4).Value = oData2.Rows(intLoop).Item("IsChecked")
                    params2(5).Value = oData2.Rows(intLoop).Item("PromiseDate")
                    params2(6).Value = oApplication.BusinessDate
                    params2(7).Value = oData2.Rows(intLoop).Item("Notes")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveEdit3, params2)
                Next
            End If
            If oData3.Rows.Count > 0 Then
                params3_1(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params3_1(0).Value = oApplication.ApplicationID
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveEdit4_1, params3_1)

                params3(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params3(1) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
                For intLoop = 0 To oData3.Rows.Count - 1
                    params3(0).Value = oApplication.ApplicationID
                    params3(1).Value = oData3.Rows(intLoop).Item("AgreementNo")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveEdit4, params3)
                Next
            End If

            'Dim parGuarantor() As SqlParameter = New SqlParameter(20) {}

            'parGuarantor(0) = New SqlParameter("@mode", SqlDbType.Bit)
            'parGuarantor(0).Value = 0
            'parGuarantor(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            'parGuarantor(1).Value = oApplication.ApplicationID
            'parGuarantor(2) = New SqlParameter("@GuarantorName", SqlDbType.VarChar, 50)
            'parGuarantor(2).Value = oGuarantorPersonal.PersonName
            'parGuarantor(3) = New SqlParameter("@GuarantorJobTitle", SqlDbType.VarChar, 50)
            'parGuarantor(3).Value = oGuarantorPersonal.PersonTitle
            'parGuarantor(4) = New SqlParameter("@GuarantorAddress", SqlDbType.VarChar, 50)
            'parGuarantor(4).Value = oGuarantorAddress.Address
            'parGuarantor(5) = New SqlParameter("@GuarantorRT", SqlDbType.Char, 3)
            'parGuarantor(5).Value = oGuarantorAddress.RT
            'parGuarantor(6) = New SqlParameter("@GuarantorRW", SqlDbType.Char, 3)
            'parGuarantor(6).Value = oGuarantorAddress.RW
            'parGuarantor(7) = New SqlParameter("@GuarantorKelurahan", SqlDbType.VarChar, 30)
            'parGuarantor(7).Value = oGuarantorAddress.Kelurahan
            'parGuarantor(8) = New SqlParameter("@GuarantorKecamatan", SqlDbType.VarChar, 30)
            'parGuarantor(8).Value = oGuarantorAddress.Kecamatan
            'parGuarantor(9) = New SqlParameter("@GuarantorCity", SqlDbType.VarChar, 30)
            'parGuarantor(9).Value = oGuarantorAddress.City
            'parGuarantor(10) = New SqlParameter("@GuarantorZipCode", SqlDbType.Char, 5)
            'parGuarantor(10).Value = oGuarantorAddress.ZipCode
            'parGuarantor(11) = New SqlParameter("@GuarantorAreaPhone1", SqlDbType.Char, 4)
            'parGuarantor(11).Value = oGuarantorAddress.AreaPhone1
            'parGuarantor(12) = New SqlParameter("@GuarantorPhone1", SqlDbType.VarChar, 10)
            'parGuarantor(12).Value = oGuarantorAddress.Phone1
            'parGuarantor(13) = New SqlParameter("@GuarantorAreaPhone2", SqlDbType.Char, 4)
            'parGuarantor(13).Value = oGuarantorAddress.AreaPhone2
            'parGuarantor(14) = New SqlParameter("@GuarantorPhone2", SqlDbType.Char, 10)
            'parGuarantor(14).Value = oGuarantorAddress.Phone2
            'parGuarantor(15) = New SqlParameter("@GuarantorAreaFax", SqlDbType.Char, 4)
            'parGuarantor(15).Value = oGuarantorAddress.AreaFax
            'parGuarantor(16) = New SqlParameter("@GuarantorFax", SqlDbType.VarChar, 10)
            'parGuarantor(16).Value = oGuarantorAddress.Fax
            'parGuarantor(17) = New SqlParameter("@GuarantorMobilePhone", SqlDbType.VarChar, 20)
            'parGuarantor(17).Value = oGuarantorPersonal.MobilePhone
            'parGuarantor(18) = New SqlParameter("@GuarantorEmail", SqlDbType.VarChar, 30)
            'parGuarantor(18).Value = oGuarantorPersonal.Email
            'parGuarantor(19) = New SqlParameter("@GuarantorNotes", SqlDbType.Text)
            'parGuarantor(19).Value = oGuarantorPersonal.Catatan
            'parGuarantor(20) = New SqlParameter("@GuarantorPenghasilan", SqlDbType.Money)
            'parGuarantor(20).Value = oGuarantorPersonal.Penghasilan

            'SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spApplicationSaveAddGuarantor", parGuarantor)

            'alasan koreksi
            If oApplication.isKoreksi Then

                Dim parKoreksi() As SqlParameter = New SqlParameter(3) {}

                parKoreksi(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                parKoreksi(0).Value = oApplication.ApplicationID
                parKoreksi(1) = New SqlParameter("@BranchId", SqlDbType.VarChar, 50)
                parKoreksi(1).Value = oApplication.BranchId
                parKoreksi(2) = New SqlParameter("@CorrectionDesc", SqlDbType.VarChar, 200)
                parKoreksi(2).Value = oApplication.alasanKoreksi
                parKoreksi(3) = New SqlParameter("@Modul", SqlDbType.VarChar, 200)
                parKoreksi(3).Value = "APLIKASI"

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spAppCorrectionLogSave", parKoreksi)
            End If

            transaction.Commit()
            oReturnValue.Err = ErrMessage
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Application", "ApplicationSaveEdit", exp.Message + exp.StackTrace)
            oReturnValue.Err = exp.Message
            Throw New Exception("Error On DataAccess.AccAcq.Customer.ApplicationSaveEdit")
            Return oReturnValue
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function


    Public Function ApplicationSaveEditNonFinancial(ByVal oApplication As Parameter.Application,
                                        ByVal oRefPersonal As Parameter.Personal,
                                        ByVal oRefAddress As Parameter.Address,
                                        ByVal oMailingAddress As Parameter.Address,
                                        ByVal oData1 As DataTable,
                                        ByVal oData2 As DataTable,
                                        ByVal oData3 As DataTable) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim conn As New SqlConnection(oApplication.strConnection)
        Dim transaction As SqlTransaction = Nothing

        Dim intLoop As Integer
        Dim params() As SqlParameter = New SqlParameter(21) {}
        Dim params1() As SqlParameter = New SqlParameter(8) {}
        Dim params2() As SqlParameter = New SqlParameter(7) {}
        Dim params3_1() As SqlParameter = New SqlParameter(0) {}
        Dim params3() As SqlParameter = New SqlParameter(1) {}
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oApplication.BranchId
            params(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params(1).Value = oApplication.CustomerId
            params(2) = New SqlParameter("@MailingPhone1", SqlDbType.Char, 10)
            params(2).Value = oMailingAddress.Phone1
            params(3) = New SqlParameter("@MailingAreaPhone2", SqlDbType.Char, 4)
            params(3).Value = oMailingAddress.AreaPhone2
            params(4) = New SqlParameter("@MailingPhone2", SqlDbType.Char, 10)
            params(4).Value = oMailingAddress.Phone2
            params(5) = New SqlParameter("@MailingAreaFax", SqlDbType.Char, 4)
            params(5).Value = oMailingAddress.AreaFax
            params(6) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(6).Value = oApplication.ApplicationID
            params(7) = New SqlParameter("@KodeDATI", SqlDbType.Char, 4)
            params(7).Value = oApplication.KodeDATI
            params(8) = New SqlParameter("@MailingFax", SqlDbType.Char, 10)
            params(8).Value = oMailingAddress.Fax
            params(9) = New SqlParameter("@Notes", SqlDbType.Text)
            params(9).Value = oApplication.Notes
            params(10) = New SqlParameter("@MailingAddress", SqlDbType.VarChar, 100)
            params(10).Value = oMailingAddress.Address
            params(11) = New SqlParameter("@MailingRT", SqlDbType.Char, 3)
            params(11).Value = oMailingAddress.RT
            params(12) = New SqlParameter("@MailingRW", SqlDbType.Char, 3)
            params(12).Value = oMailingAddress.RW
            params(13) = New SqlParameter("@MailingKelurahan", SqlDbType.VarChar, 30)
            params(13).Value = oMailingAddress.Kelurahan
            params(14) = New SqlParameter("@MailingKecamatan", SqlDbType.VarChar, 30)
            params(14).Value = oMailingAddress.Kecamatan
            params(15) = New SqlParameter("@MailingCity", SqlDbType.VarChar, 30)
            params(15).Value = oMailingAddress.City
            params(16) = New SqlParameter("@MailingZipCode", SqlDbType.Char, 5)
            params(16).Value = oMailingAddress.ZipCode
            params(17) = New SqlParameter("@MailingAreaPhone1", SqlDbType.Char, 4)
            params(17).Value = oMailingAddress.AreaPhone1
            params(18) = New SqlParameter("@IsCOP", SqlDbType.Char, 1)
            params(18).Value = oApplication.IsCOP
            params(19) = New SqlParameter("@LiniBisnis", SqlDbType.Char, 10)
            params(19).Value = oApplication.LiniBisnis
            params(20) = New SqlParameter("@TipeLoan", SqlDbType.Char, 1)
            params(20).Value = oApplication.TipeLoan
            params(21) = New SqlParameter("@HakOpsi", SqlDbType.Bit)
            params(21).Value = oApplication.HakOpsi

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveEditNonFinancial, params)

            If oData1.Rows.Count > 0 Then
                params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params1(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params1(3) = New SqlParameter("@PriorTo", SqlDbType.Char, 3)
                params1(4) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
                params1(5) = New SqlParameter("@IsMandatory", SqlDbType.Char, 1)
                params1(6) = New SqlParameter("@PromiseDate", SqlDbType.VarChar, 8)
                params1(7) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params1(8) = New SqlParameter("@Notes", SqlDbType.Text)
                For intLoop = 0 To oData1.Rows.Count - 1
                    params1(0).Value = oApplication.BranchId
                    params1(1).Value = oApplication.ApplicationID
                    params1(2).Value = oData1.Rows(intLoop).Item("MasterTCID")
                    params1(3).Value = oData1.Rows(intLoop).Item("PriorTo")
                    params1(4).Value = oData1.Rows(intLoop).Item("IsChecked")
                    params1(5).Value = oData1.Rows(intLoop).Item("IsMandatory")
                    params1(6).Value = oData1.Rows(intLoop).Item("PromiseDate")
                    params1(7).Value = oApplication.BusinessDate
                    params1(8).Value = oData1.Rows(intLoop).Item("Notes")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveEdit2, params1)
                Next
            End If
            If oData2.Rows.Count > 0 Then
                params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params2(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params2(2) = New SqlParameter("@MasterTCID", SqlDbType.VarChar, 50)
                params2(3) = New SqlParameter("@AGTCCLSequenceNo", SqlDbType.SmallInt)
                params2(4) = New SqlParameter("@IsChecked", SqlDbType.Char, 1)
                params2(5) = New SqlParameter("@PromiseDate", SqlDbType.VarChar, 8)
                params2(6) = New SqlParameter("@CheckedDate", SqlDbType.DateTime)
                params2(7) = New SqlParameter("@Notes", SqlDbType.Text)
                For intLoop = 0 To oData2.Rows.Count - 1
                    params2(0).Value = oApplication.BranchId
                    params2(1).Value = oApplication.ApplicationID
                    params2(2).Value = oData2.Rows(intLoop).Item("MasterTCID")
                    params2(3).Value = CInt(oData2.Rows(intLoop).Item("AGTCCLSequenceNo"))
                    params2(4).Value = oData2.Rows(intLoop).Item("IsChecked")
                    params2(5).Value = oData2.Rows(intLoop).Item("PromiseDate")
                    params2(6).Value = oApplication.BusinessDate
                    params2(7).Value = oData2.Rows(intLoop).Item("Notes")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveEdit3, params2)
                Next
            End If
            If oData3.Rows.Count > 0 Then
                params3_1(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params3_1(0).Value = oApplication.ApplicationID
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveEdit4_1, params3_1)

                params3(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params3(1) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
                For intLoop = 0 To oData3.Rows.Count - 1
                    params3(0).Value = oApplication.ApplicationID
                    params3(1).Value = oData3.Rows(intLoop).Item("AgreementNo")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spApplicationSaveEdit4, params3)
                Next
            End If
            'alasan koreksi
            If oApplication.isKoreksi Then

                Dim parKoreksi() As SqlParameter = New SqlParameter(3) {}

                parKoreksi(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                parKoreksi(0).Value = oApplication.ApplicationID
                parKoreksi(1) = New SqlParameter("@BranchId", SqlDbType.VarChar, 50)
                parKoreksi(1).Value = oApplication.BranchId
                parKoreksi(2) = New SqlParameter("@CorrectionDesc", SqlDbType.VarChar, 200)
                parKoreksi(2).Value = oApplication.alasanKoreksi
                parKoreksi(3) = New SqlParameter("@Modul", SqlDbType.VarChar, 200)
                parKoreksi(3).Value = "APLIKASI"

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spAppCorrectionLogSave", parKoreksi)
            End If

            transaction.Commit()
            oReturnValue.Err = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Application", "ApplicationSaveEdit", exp.Message + exp.StackTrace)
            oReturnValue.Err = exp.Message
            Throw New Exception("Error On DataAccess.AccAcq.Customer.ApplicationSaveEdit")
            Return oReturnValue
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "GoLive"
    Public Function GetGoLive(ByVal customClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@WhereCond2", SqlDbType.VarChar)
        params(5).Value = customClass.WhereCond2
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spGoLivePaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetGoLive", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetGoLive")
        End Try
    End Function

    Public Function GetGoLiveBackDated(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGoLiveBackDated").Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetGoLiveBackDated", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetGoLiveBackDated")
        End Try
    End Function

    Public Function GoLiveSave(ByVal oGoLive As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim conn As New SqlConnection(oGoLive.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim params() As SqlParameter = New SqlParameter(1) {}

        'Try
        '    If conn.State = ConnectionState.Closed Then conn.Open()
        '    transaction = conn.BeginTransaction

        '    params(0) = New SqlParameter("@dt", SqlDbType.Structured)
        '    params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)

        '    params(0).Value = oGoLive.ListData
        '    params(1).Value = oGoLive.BusinessDate

        '    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spGenInstallmentDueDate", params)

        '    transaction.Commit()
        '    ErrMessage = ""
        '    oReturnValue.Err = ErrMessage
        'Catch ex As Exception
        '    transaction.Rollback()
        '    ErrMessage = ex.Message
        '    oReturnValue.Err = ErrMessage
        '    Throw New Exception(ex.Message)
        'End Try

        If ErrMessage = "" Then
            Try
                If conn.State = ConnectionState.Closed Then conn.Open()
                transaction = conn.BeginTransaction

                params(0) = New SqlParameter("@dt", SqlDbType.Structured)
                params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)

                params(0).Value = oGoLive.ListData
                params(1).Value = oGoLive.BusinessDate

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spGoLiveProcess", params)

                transaction.Commit()
                oReturnValue.Err = ErrMessage
                Return oReturnValue
            Catch exp As Exception
                transaction.Rollback()
                ErrMessage = exp.Message
                oReturnValue.Err = ErrMessage
                Throw New Exception(exp.Message)
                Return oReturnValue
            Finally
                If conn.State = ConnectionState.Open Then conn.Close()
                conn.Dispose()
            End Try
        Else
            Return oReturnValue
        End If

    End Function

    Public Function GoLiveCancel(ByVal oGoLive As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim conn As New SqlConnection(oGoLive.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim params() As SqlParameter = New SqlParameter(1) {}

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            params(0) = New SqlParameter("@dt", SqlDbType.Structured)
            params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)

            params(0).Value = oGoLive.ListData
            params(1).Value = oGoLive.BusinessDate

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spGoLiveCancel", params)

            transaction.Commit()
            oReturnValue.Err = ErrMessage
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception(exp.Message)
            Return oReturnValue
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function GetGoLiveCancelPaging(ByVal customClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(5) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@WhereCond2", SqlDbType.VarChar)
        params(5).Value = customClass.WhereCond2

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spGoLiveCancelPaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetGoLiveCancel", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function


    Public Function GoLiveSaveModalKerja(ByVal oGoLive As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim conn As New SqlConnection(oGoLive.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim params() As SqlParameter = New SqlParameter(1) {}


        If ErrMessage = "" Then
            Try
                If conn.State = ConnectionState.Closed Then conn.Open()
                transaction = conn.BeginTransaction

                params(0) = New SqlParameter("@dt", SqlDbType.Structured)
                params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)

                params(0).Value = oGoLive.ListData
                params(1).Value = oGoLive.BusinessDate

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spGoLiveProcessModalKerja", params)

                transaction.Commit()
                oReturnValue.Err = ErrMessage
                Return oReturnValue
            Catch exp As Exception
                transaction.Rollback()
                ErrMessage = exp.Message
                oReturnValue.Err = ErrMessage
                Throw New Exception(exp.Message)
                Return oReturnValue
            Finally
                If conn.State = ConnectionState.Open Then conn.Close()
                conn.Dispose()
            End Try
        Else
            Return oReturnValue
        End If

    End Function

    Public Function GoLiveSaveFactoring(ByVal oGoLive As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim conn As New SqlConnection(oGoLive.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim params() As SqlParameter = New SqlParameter(1) {}


        If ErrMessage = "" Then
            Try
                If conn.State = ConnectionState.Closed Then conn.Open()
                transaction = conn.BeginTransaction

                params(0) = New SqlParameter("@dt", SqlDbType.Structured)
                params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)

                params(0).Value = oGoLive.ListData
                params(1).Value = oGoLive.BusinessDate

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spGoLiveProcessFactoring", params)

                transaction.Commit()
                oReturnValue.Err = ErrMessage
                Return oReturnValue
            Catch exp As Exception
                transaction.Rollback()
                ErrMessage = exp.Message
                oReturnValue.Err = ErrMessage
                Throw New Exception(exp.Message)
                Return oReturnValue
            Finally
                If conn.State = ConnectionState.Open Then conn.Close()
                conn.Dispose()
            End Try
        Else
            Return oReturnValue
        End If

    End Function


#End Region
#Region "Prospect "
    Public Function GetShowDataProspect(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
        params(0).Value = oCustomClass.BranchId
        params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.Char, 100)
        params(1).Value = oCustomClass.SortBy

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, oCustomClass.SpName, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetShowDataProspect", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetShowDataProspect")
        End Try
    End Function
    Public Function GetDataAppIDProspect(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim objreader As SqlDataReader
        Dim ErrMessage As String = ""
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
        params(0).Value = oCustomClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.ApplicationID
        params(2) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
        params(2).Direction = ParameterDirection.Output
        objreader = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewProspect", params)
        ErrMessage = CType(params(2).Value, String)
        If ErrMessage <> "" Then
            oReturnValue.Err = ErrMessage
            Return oReturnValue
            Exit Function
        End If
        If objreader.Read Then
            With oReturnValue
                .ApplicationID = objreader("ApplicationID").ToString
                .AssetCode = objreader("AssetCode").ToString
                .OTR = CDbl(objreader("OTRPrice"))
                .DP = CDbl(objreader("DPAmount"))
                .Tenor = CInt(objreader("Tenor"))
                .ExistingPolicy = objreader("ExistingPolicyNo").ToString
                .InstallmentAmount = CDbl(objreader("InstallmentAmount"))
                .FirstInstallment = objreader("FirstInstallment").ToString
                .AssetDesc = objreader("Description").ToString
            End With
        End If
        objreader.Close()
        oReturnValue.Err = ErrMessage
        Return oReturnValue
    End Function
    Public Function GetCustomerProspect(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim objreader As SqlDataReader
        Dim ErrMessage As String = ""
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
        params(0).Value = oCustomClass.BranchId
        params(1) = New SqlParameter("@ProspectAppID", SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.ProspectAppID
        objreader = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spConsumerProspect", params)
        If objreader.Read Then
            With oReturnValue
                .ApplicationID = objreader("ApplicationID").ToString
                .Name = objreader("Name").ToString
                .Address = objreader("Address").ToString
                .Rt = objreader("Rt").ToString
                .Rw = objreader("Rw").ToString
                .Kelurahan = objreader("Kelurahan").ToString
                .Kecamatan = objreader("Kecamatan").ToString
                .City = objreader("City").ToString
                .ZipCode = objreader("ZipCode").ToString
                .AreaPhone1 = objreader("AreaPhone1").ToString
                .Phone1 = objreader("Phone1").ToString
                .CustomerType = objreader("CustomerType").ToString
                .IDType = objreader("IDType").ToString
                .IDNumber = objreader("IDNumber").ToString
                .Gender = objreader("Gender").ToString
                If .CustomerType.ToString.Trim = "P" Then
                    .BirthPlace = objreader("BirthPlace").ToString
                    .BirthDate = CDate(objreader("BirthDate"))
                End If
                .HomeStatus = objreader("HomeStatus").ToString
                .MobilePhone = objreader("MobilePhone").ToString
                .Email = objreader("Email").ToString
                .ProfessionID = objreader("ProfessionID").ToString
                .IndustryTypeID = objreader("IndustryTypeID").ToString
                .txtSearch = objreader("CompanyStatus").ToString
                .Education = objreader("Education").ToString
                .StaySinceYear = objreader("StaySinceYear").ToString
                .StaySinceMonth = objreader("StaySinceMonth").ToString
                .NumOfDependence = objreader("NumOfDependence").ToString
                .KodeIndustri = objreader("KodeIndustri").ToString
                .KodeIndustriDetail = objreader("KodeIndustriDetail").ToString
                .NamaIndustri = objreader("NameIndustri").ToString
                .NatureOfBusinessId = objreader("NatureOfBusinessId").ToString
                .OccupationId = objreader("OccupationId").ToString
                .MaritalStatus = objreader("MaritalStatus").ToString
                .EmploymentSinceMonth = objreader("EmploymentSinceMonth").ToString
                .EmploymentSinceYear = objreader("EmploymentSinceYear").ToString
                .LicensePlate = objreader("LicensePlate").ToString
                .Tenor = objreader("Tenor").ToString
            End With
        End If
        objreader.Close()
        oReturnValue.Err = ErrMessage
        Return oReturnValue
    End Function
    Public Function ViewProspectApplication(ByVal oCustomClass As Parameter.Application) As DataTable
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@WhereBy", SqlDbType.VarChar, 1000)
        params(0).Value = oCustomClass.WhereCond
        Try
            Return SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, oCustomClass.SpName, params).Tables(0)
        Catch exp As Exception
            WriteException("Application", "ViewProspectApplication", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetShowDataProspect")
        End Try
    End Function
#End Region

    Public Function GetViewMKKFasilitas(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim params() As SqlParameter = New SqlParameter(1) {}

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = oCustomClass.ApplicationID
            params(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params(1).Value = oCustomClass.CustomerId

            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewMKKFasilitasSaatIni", params).Tables(0)
            oReturnValue.Err = ""

            Return oReturnValue
        Catch exp As Exception
            oReturnValue.Err = exp.Message
            Throw New Exception(exp.Message)
            Return oReturnValue
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
        End Try
    End Function

    Public Function saveApplicationProductOffering(ByVal oApplication As Parameter.Application, ByVal oAssetData As Parameter.AssetData) As Parameter.Application
        Dim Params() As SqlParameter = New SqlParameter(12) {}
        Dim transaction As SqlTransaction = Nothing
        Dim Conn As New SqlConnection(oApplication.strConnection)
        Dim oReturnValue As New Parameter.Application
        Dim strApplicationID As String = ""
        Dim strProductOfferingID As String = ""
        Dim strProductOfferingDesc As String = ""
        Dim strProductID As String = ""

        Try
            If Conn.State = ConnectionState.Closed Then Conn.Open()

            transaction = Conn.BeginTransaction

            Params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            Params(0).Value = oApplication.BranchId

            Params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            Params(1).Direction = ParameterDirection.Output

            Params(2) = New SqlParameter("@AssetCode", SqlDbType.VarChar, 50)
            Params(2).Value = oAssetData.AssetCode

            Params(3) = New SqlParameter("@YearManufacturing", SqlDbType.SmallInt)
            Params(3).Value = oAssetData.ManufacturingYear

            Params(4) = New SqlParameter("@UsedNew", SqlDbType.Char, 1)
            Params(4).Value = oAssetData.UsedNew

            Params(5) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            Params(5).Value = oApplication.Tenor

            Params(6) = New SqlParameter("@FirstInstallment", SqlDbType.Char, 2)
            Params(6).Value = oApplication.FirstInstallment

            Params(7) = New SqlParameter("@TotalOtr", SqlDbType.Decimal)
            Params(7).Value = oApplication.OTR

            Params(8) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            Params(8).Value = oApplication.BusinessDate

            Params(9) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            Params(9).Value = oApplication.CustomerId

            Params(10) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 10)
            Params(10).Direction = ParameterDirection.Output

            Params(11) = New SqlParameter("@ProductOfferingDesc", SqlDbType.VarChar, 100)
            Params(11).Direction = ParameterDirection.Output

            Params(12) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
            Params(12).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spApplicationProductOffering", Params)

            strApplicationID = CType(Params(1).Value, String)
            strProductOfferingID = CType(Params(10).Value, String)
            strProductOfferingDesc = CType(Params(11).Value, String)
            strProductID = CType(Params(12).Value, String)

            transaction.Commit()
            oReturnValue.ApplicationID = strApplicationID
            oReturnValue.ProductOffID = strProductOfferingID
            oReturnValue.ProductOffIDDesc = strProductOfferingDesc
            oReturnValue.ProductID = strProductID
            Return oReturnValue

        Catch ex As Exception
            transaction.Rollback()
            Throw New Exception(ex.Message)
            Return oReturnValue
        Finally
            If Conn.State = ConnectionState.Open Then Conn.Close()
            Conn.Dispose()
        End Try

    End Function

    Public Function saveApplicationProductOfferingEdit(ByVal oApplication As Parameter.Application, ByVal oAssetData As Parameter.AssetData) As Parameter.Application
        Dim Params() As SqlParameter = New SqlParameter(12) {}
        Dim transaction As SqlTransaction = Nothing
        Dim Conn As New SqlConnection(oApplication.strConnection)
        Dim oReturnValue As New Parameter.Application
        Dim strProductOfferingID As String = ""
        Dim strProductOfferingDesc As String = ""
        Dim strProductID As String = ""

        Try
            If Conn.State = ConnectionState.Closed Then Conn.Open()

            transaction = Conn.BeginTransaction

            Params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            Params(0).Value = oApplication.BranchId

            Params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            Params(1).Value = oApplication.ApplicationID

            Params(2) = New SqlParameter("@AssetCode", SqlDbType.VarChar, 50)
            Params(2).Value = oAssetData.AssetCode

            Params(3) = New SqlParameter("@YearManufacturing", SqlDbType.SmallInt)
            Params(3).Value = oAssetData.ManufacturingYear

            Params(4) = New SqlParameter("@UsedNew", SqlDbType.Char, 1)
            Params(4).Value = oAssetData.UsedNew

            Params(5) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            Params(5).Value = oApplication.Tenor

            Params(6) = New SqlParameter("@FirstInstallment", SqlDbType.Char, 2)
            Params(6).Value = oApplication.FirstInstallment

            Params(7) = New SqlParameter("@TotalOtr", SqlDbType.Decimal)
            Params(7).Value = oApplication.OTR

            Params(8) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            Params(8).Value = oApplication.BusinessDate

            Params(9) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            Params(9).Value = oApplication.CustomerId

            Params(10) = New SqlParameter("@ProductOfferingID", SqlDbType.Char, 10)
            Params(10).Direction = ParameterDirection.Output

            Params(11) = New SqlParameter("@ProductOfferingDesc", SqlDbType.VarChar, 100)
            Params(11).Direction = ParameterDirection.Output

            Params(12) = New SqlParameter("@ProductID", SqlDbType.Char, 10)
            Params(12).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spApplicationProductOfferingEdit", Params)

            strProductOfferingID = CType(Params(10).Value, String)
            strProductOfferingDesc = CType(Params(11).Value, String)
            strProductID = CType(Params(12).Value, String)

            transaction.Commit()
            oReturnValue.ProductOffID = strProductOfferingID
            oReturnValue.ProductOffIDDesc = strProductOfferingDesc
            oReturnValue.ProductID = strProductID
            Return oReturnValue

        Catch ex As Exception
            transaction.Rollback()
            Throw New Exception(ex.Message)
            Return oReturnValue
        Finally
            If Conn.State = ConnectionState.Open Then Conn.Close()
            Conn.Dispose()
        End Try

    End Function
    Public Function GetSyaratCair(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@UseNew", SqlDbType.Char, 1)
        params(0).Value = oCustomClass.UseNew

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spSyaratCair, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetSyaratCair", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetSyaratCair")
        End Try
    End Function
    Public Function GetSyaratCairPO(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@UseNew", SqlDbType.Char, 1)
        params(0).Value = oCustomClass.UseNew

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spSyaratCairPO, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetSyaratCairPO", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetSyaratCairPO")
        End Try
    End Function

    Public Function DuplikasiApplication(ByVal oCustomClass As Parameter.Application) As String
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(0).Value = oCustomClass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(1).Value = oCustomClass.ApplicationID

        params(2) = New SqlParameter("@JumlahAplikasi", SqlDbType.SmallInt)
        params(2).Value = oCustomClass.JumlahAplikasi

        params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(3).Value = oCustomClass.BusinessDate

        params(4) = New SqlParameter("@Notes", SqlDbType.VarChar, 255)
        params(4).Value = oCustomClass.Notes

        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spDuplikasiApplication", params)
            Return ""
        Catch exp As Exception
            WriteException("Application", "DuplikasiApplication", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.DuplikasiApplication")
        End Try

    End Function

    Public Function GetApplicationHasilSurveydanKYC(ByVal customClass As Parameter.Application) As Parameter.Application
        Try
            Dim oReturnValue As New Parameter.Application
            Dim params() As SqlParameter = New SqlParameter(5) {}
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customClass.CurrentPage
            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customClass.PageSize
            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
            params(2).Value = customClass.WhereCond
            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
            params(3).Value = customClass.SortBy
            params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output
            params(5) = New SqlParameter("@WhereCond2", SqlDbType.VarChar)
            params(5).Value = customClass.WhereCond2

            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spApplicationHasilSurveydanKYCPaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetApplicationHasilSurveydanKYC", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("Application.VB - DA", "GetApplicationHasilSurveydanKYC", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            Throw New Exception(exp.StackTrace)
        End Try
    End Function


    Public Function ValidasiCollector(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim conn As New SqlConnection(oCustomClass.strConnection)

        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            params(0) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
            params(0).Value = oCustomClass.ZipCode
            params(1) = New SqlParameter("@Kelurahan", SqlDbType.Char, 50)
            params(1).Value = oCustomClass.Kelurahan

            oReturnValue.ListData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "spCheckCollector", params).Tables(0)

            oReturnValue.Err = ""
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "ValidasiCollector", exp.Message + exp.StackTrace)
            oReturnValue.Err = "Error: " & exp.Message & ""
            Throw New Exception("Error On DataAccess.AccAcq.Application.ValidasiCollector")
            Return oReturnValue
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
        End Try

    End Function

    Public Function ModalKerjaApplicationFinancialSave(ByVal ocustomClass As Parameter.Application) As String
        Dim ErrMessage As String = ""
        Dim params(10) As SqlParameter

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = ocustomClass.BranchId

        params(2) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
        params(2).Value = ocustomClass.InstallmentAmount
        params(3) = New SqlParameter("@Bunga", SqlDbType.Decimal)
        params(3).Value = ocustomClass.FactoringInterest
        params(4) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
        params(4).Value = ocustomClass.AdminFee
        params(5) = New SqlParameter("@NotaryFee", SqlDbType.Decimal)
        params(5).Value = ocustomClass.NotaryFee
        params(6) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal)
        params(6).Value = ocustomClass.ProvisionFee
        params(7) = New SqlParameter("@SurveyFee", SqlDbType.Decimal)
        params(7).Value = ocustomClass.SurveyFee
        params(8) = New SqlParameter("@Tenor", SqlDbType.Int)
        params(8).Value = ocustomClass.Tenor

        params(9) = New SqlParameter("@InstallmentScheme", SqlDbType.VarChar, 2)
        params(9).Value = ocustomClass.InstallmentScheme

        params(10) = New SqlParameter("@FacilityType", SqlDbType.VarChar, 1)
        params(10).Value = ocustomClass.FacilityType
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spModalKerjaApplicationFinancialSave", params)
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On FactoringInvoice.FactoringApplicationFinancialSave- " & ex.Message)
        End Try
    End Function

    Public Function GetGoLiveModalKerja(ByVal customClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@WhereCond2", SqlDbType.VarChar)
        params(5).Value = customClass.WhereCond2
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spGoLiveModalKerjaPaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetGoLiveModalKerja", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetGoLiveModalKerja")
        End Try
    End Function

    Public Function GetGoLiveFactoring(ByVal customClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@WhereCond2", SqlDbType.VarChar)
        params(5).Value = customClass.WhereCond2
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spGoLiveFactoringPaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "spGoLiveFactoringPaging", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetGoLiveFactoring")
        End Try
    End Function

#Region "ActivityLog"
    Public Function ActivityLogSave(ByVal oApplication As Parameter.Application) As Parameter.Application

        Dim oReturnValue As New Parameter.Application
        Dim conn As New SqlConnection(oApplication.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim ProspectAppID As String = ""

        Dim params() As SqlParameter = New SqlParameter(6) {}

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oApplication.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = oApplication.ApplicationID
            params(2) = New SqlParameter("@ActivityType", SqlDbType.Char, 3)
            params(2).Value = oApplication.ActivityType
            params(3) = New SqlParameter("@ActivityDateStart", SqlDbType.DateTime)
            params(3).Value = oApplication.ActivityDateStart
            params(4) = New SqlParameter("@ActivityDateEnd", SqlDbType.DateTime)
            params(4).Value = oApplication.ActivityDateEnd
            params(5) = New SqlParameter("@ActivityUser", SqlDbType.Char, 20)
            params(5).Value = oApplication.ActivityUser
            params(6) = New SqlParameter("@SequenceNo", SqlDbType.Int)
            params(6).Value = oApplication.ActivitySeqNo

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spActivityNewLog", params)

            'ErrMessage = CType(params(11).Value, String)
            'If ErrMessage <> "" Then
            '    oReturnValue.ProspectAppID = ""
            '    oReturnValue.Err = ErrMessages
            '    Return oReturnValue
            '    Exit Function
            'End If

            transaction.Commit()
            oReturnValue.ApplicationID = oApplication.ApplicationID

            oReturnValue.Err = ErrMessage
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Application", "ActivityLogSave", exp.Message + exp.StackTrace)

            oReturnValue.ApplicationID = oApplication.ApplicationID
            oReturnValue.Err = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.LoanOrg.RCA.ActivityLogSave")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "DisburseLog"
    Public Function DisburseLogSave(ByVal oApplication As Parameter.Application) As Parameter.Application

        Dim oReturnValue As New Parameter.Application
        Dim conn As New SqlConnection(oApplication.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim ProspectAppID As String = ""

        Dim params() As SqlParameter = New SqlParameter(6) {}

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oApplication.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = oApplication.ApplicationID
            params(2) = New SqlParameter("@ActivityType", SqlDbType.Char, 3)
            params(2).Value = oApplication.ActivityType
            params(3) = New SqlParameter("@ActivityDateStart", SqlDbType.DateTime)
            params(3).Value = oApplication.ActivityDateStart
            params(4) = New SqlParameter("@ActivityDateEnd", SqlDbType.DateTime)
            params(4).Value = oApplication.ActivityDateEnd
            params(5) = New SqlParameter("@ActivityUser", SqlDbType.Char, 20)
            params(5).Value = oApplication.ActivityUser
            params(6) = New SqlParameter("@SequenceNo", SqlDbType.Int)
            params(6).Value = oApplication.ActivitySeqNo

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spDisbursementLog", params)

            'ErrMessage = CType(params(11).Value, String)
            'If ErrMessage <> "" Then
            '    oReturnValue.ProspectAppID = ""
            '    oReturnValue.Err = ErrMessages
            '    Return oReturnValue
            '    Exit Function
            'End If

            transaction.Commit()
            oReturnValue.ApplicationID = oApplication.ApplicationID

            oReturnValue.Err = ErrMessage
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Application", "DisburseLogSave", exp.Message + exp.StackTrace)

            oReturnValue.ApplicationID = oApplication.ApplicationID
            oReturnValue.Err = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.CashMgt.Disburse.DisburseLogSave")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region
    Private Const Query_getNPP As String = "select employeeIncentiveCardID as ID, CardName as Name from EmployeeIncentiveCardH"

    Public Function getdataNPP(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim Query As String = ""
        Dim oReturnValue As New Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@NPP", SqlDbType.Char, 20)
            params(0).Value = oCustomClass.NPP
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetDataNPP", params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            WriteException("Application", "spGetDataNPP", ex.Message + ex.StackTrace)
            Throw New Exception("Error")
        End Try
    End Function


    Public Function InstallmentDrawDownSave(ByVal oDrawdown As Parameter.Drawdown) As String
        Dim Params() As SqlParameter = New SqlParameter(12) {}
        Dim transaction As SqlTransaction = Nothing
        Dim Conn As New SqlConnection(oDrawdown.strConnection)
        Dim oReturnValue As New Parameter.Drawdown
        Dim DrawdownNo As String

        Try
            If Conn.State = ConnectionState.Closed Then Conn.Open()

            transaction = Conn.BeginTransaction

            Params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            Params(0).Value = oDrawdown.BranchId

            Params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            Params(1).Value = oDrawdown.ApplicationID

            Params(2) = New SqlParameter("@DrawdownDate", SqlDbType.VarChar, 8)
            Params(2).Value = oDrawdown.DrawdownDate

            Params(3) = New SqlParameter("@DrawdownAmount", SqlDbType.Decimal)
            Params(3).Value = oDrawdown.DrawdownAmount

            Params(4) = New SqlParameter("@InterestPeriodFrom", SqlDbType.VarChar, 8)
            Params(4).Value = oDrawdown.InterestPeriodFrom

            Params(5) = New SqlParameter("@InterestPeriodTo", SqlDbType.VarChar, 8)
            Params(5).Value = oDrawdown.InterestPeriodTo

            Params(6) = New SqlParameter("@InterestPeriod", SqlDbType.Int)
            Params(6).Value = oDrawdown.InterestPeriod

            Params(7) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
            Params(7).Value = oDrawdown.InterestAmount

            Params(8) = New SqlParameter("@InterestType", SqlDbType.Char, 1)
            Params(8).Value = oDrawdown.InterestType

            Params(9) = New SqlParameter("@BiayaProvisi", SqlDbType.Decimal)
            Params(9).Value = oDrawdown.BiayaProvisi

            Params(10) = New SqlParameter("@LoginID", SqlDbType.Char, 20)
            Params(10).Value = oDrawdown.LoginId

            Params(11) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            Params(11).Value = oDrawdown.BusinessDate

            Params(12) = New SqlParameter("@DrawdownNo", SqlDbType.Char, 20)
            Params(12).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spApplicationDrawdownSave", Params)

            DrawdownNo = CType(Params(12).Value, String)

            Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
            Dim oEntitiesApproval As New Parameter.Approval

            With oEntitiesApproval
                .ApprovalTransaction = transaction
                .BranchId = oDrawdown.BranchId
                .SchemeID = "RCA5"
                .RequestDate = oDrawdown.BusinessDate
                .TransactionNo = DrawdownNo
                .ApprovalNote = ""

                .ApprovalValue = oDrawdown.DrawdownAmount

                .UserRequest = oDrawdown.LoginId
                .UserApproval = oDrawdown.UserApproval
                .AprovalType = Parameter.Approval.ETransactionType.RCA_Approval
                .Argumentasi = ""
            End With

            oDrawdown.ApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)
            Dim par As IList(Of SqlParameter) = New List(Of SqlParameter)

            par.Add(New SqlParameter("@DrawdownNo", SqlDbType.Char, 20) With {.Value = DrawdownNo})
            par.Add(New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50) With {.Value = oDrawdown.ApprovalNo})
            par.Add(New SqlParameter("@BusinessDate", SqlDbType.DateTime) With {.Value = oDrawdown.BusinessDate})
            Dim eerParams = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            par.Add(eerParams)
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spInstallmentDrawdownSaveAddProceed", par.ToArray)

            transaction.Commit()

            Return ""

        Catch ex As Exception
            transaction.Rollback()
            Throw New Exception(ex.Message)
            Return ex.Message
        Finally
            If Conn.State = ConnectionState.Open Then Conn.Close()
            Conn.Dispose()
        End Try

    End Function

    Public Function InstallmentDrawDownCheckRequest(ByVal odrawdown As Parameter.Drawdown) As Boolean
        Dim oReturnValue As New Parameter.Drawdown
        Dim conn As New SqlConnection(odrawdown.strConnection)
        Dim rtn As Boolean = False

        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = odrawdown.ApplicationID

            oReturnValue.listData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "spApplicationDrawdownCheckRequest", params).Tables(0)

            If oReturnValue.listData.Rows.Count > 0 Then
                rtn = False
            Else
                rtn = True
            End If

        Catch exp As Exception
            WriteException("Application", "InstallmentDrawDownCheckRequest", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.InstallmentDrawDownCheckRequest")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
        End Try

        Return rtn
    End Function

    Public Function InstallmentDrawDownList(ByVal odrawdown As Parameter.Drawdown) As Parameter.Drawdown
        Dim oReturnValue As New Parameter.Drawdown
        Dim conn As New SqlConnection(odrawdown.strConnection)
        Dim rtn As Boolean = False

        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = odrawdown.ApplicationID

            oReturnValue.listData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "spApplicationDrawdownList", params).Tables(0)

            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "InstallmentDrawDownList", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.InstallmentDrawDownList")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
        End Try

    End Function

    Public Function GetApplicationDeduction(ByVal customClass As Parameter.Application) As Parameter.Application
        Try
            Dim oReturnValue As New Parameter.Application
            Dim params() As SqlParameter = New SqlParameter(1) {}
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(0).Value = customClass.ApplicationID
            params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(1).Value = customClass.BranchId

            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spGetApplicationDeduction", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetApplicationDeduction", exp.Message + exp.StackTrace)
            Throw New Exception(exp.StackTrace)
        End Try
    End Function

    Public Function GetCustomerFacility(ByVal oCustomClass As Parameter.Application) As Integer
        Dim objreader As SqlDataReader
        Dim value As Integer
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@CustomerId", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.CustomerId
        Try
            objreader = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetCustomerFacility", params)
            If objreader.Read Then
                value = CInt(objreader("Total"))
            End If
            objreader.Close()
            Return value
        Catch exp As Exception
            WriteException("CustomerFacility", "GetCustomerFacility", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function WayOfPaymentList(ByVal customclass As Parameter.Application) As Parameter.Application
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@Type", SqlDbType.Char, 2)
            params(0).Value = customclass.WhereCond

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spListResultWayOfPayment", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("WayOfPaymentList", "spListResultWayOfPayment", exp.Message + exp.StackTrace)
        End Try
    End Function

End Class