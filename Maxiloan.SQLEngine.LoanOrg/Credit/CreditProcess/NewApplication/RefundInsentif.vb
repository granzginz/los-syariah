﻿Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

Public Class RefundInsentif
    Inherits Maxiloan.SQLEngine.DataAccessBase

    Private Const spSupplierINCTVDailyDAdd As String = "spSupplierINCTVDailyDAdd"
    Private Const spSupplierINCTVDailyDEdit As String = "spSupplierINCTVDailyDEdit"

    Public Function getDistribusiNilaiInsentif(ByVal oCustomClass As Parameter.RefundInsentif) As Parameter.RefundInsentif
        Dim oReturnValue As New Parameter.RefundInsentif
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(1).Value = oCustomClass.BranchId

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetDistribusiNilaiInsentif", params)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Sub SupplierINCTVDailyDAdd(ByVal customclass As Parameter.RefundInsentif)
        ' Dim params() As SqlParameter = New SqlParameter(6) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            'params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            'params(0).Value = customclass.BranchId
            'params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            'params(1).Value = customclass.ApplicationID
            'params(2) = New SqlParameter("@RefundToSupp", SqlDbType.Decimal)
            'params(2).Value = customclass.RefundPremi
            'params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            'params(3).Value = customclass.BusinessDate
            'params(4) = New SqlParameter("@RefundInterest", SqlDbType.Decimal)
            'params(4).Value = customclass.RefundInterest
            'params(5) = New SqlParameter("@NilaiInsentif", SqlDbType.Decimal)
            'params(5).Value = customclass.NilaiInsentif
            'params(6) = New SqlParameter("@IsInsentifPercent", SqlDbType.Bit)
            'params(6).Value = customclass.IsInsentifPercent
            'SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierINCTVDailyDAdd, params)

            'Dim params1() As SqlParameter = New SqlParameter(12) {}

            'For Each oRow As DataRow In customclass.DistribusiNilaiInsentif.Rows
            '    params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            '    params1(0).Value = customclass.BranchId
            '    params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            '    params1(1).Value = customclass.ApplicationID
            '    params1(2) = New SqlParameter("@SupplierID", SqlDbType.Char, 20)
            '    params1(2).Value = customclass.SupplierID
            '    params1(3) = New SqlParameter("@SupplierEmployeeID", SqlDbType.Char, 10)
            '    params1(3).Value = oRow("SupplierEmployeeID")
            '    params1(4) = New SqlParameter("@SupplierEmployeePosition", SqlDbType.Char, 2)
            '    params1(4).Value = oRow("SupplierEmployeePositionID")
            '    params1(5) = New SqlParameter("@SupplierEmployeeName", SqlDbType.VarChar, 50)
            '    params1(5).Value = oRow("SupplierEmployeeName")
            '    params1(6) = New SqlParameter("@IncentiveForRecepient", SqlDbType.Decimal)
            '    params1(6).Value = oRow("IncentiveForRecepient")
            '    params1(7) = New SqlParameter("@TransId", SqlDbType.Char, 3)
            '    params1(7).Value = oRow("TransId")
            '    params1(8) = New SqlParameter("@pph", SqlDbType.Char, 2)
            '    params1(8).Value = oRow("pph")
            '    params1(9) = New SqlParameter("@tarifPajak", SqlDbType.Decimal)
            '    params1(9).Value = oRow("tarifPajak")
            '    params1(10) = New SqlParameter("@nilaiPajak", SqlDbType.Decimal)
            '    params1(10).Value = oRow("nilaiPajak")
            '    params1(11) = New SqlParameter("@insentifNet", SqlDbType.Decimal)
            '    params1(11).Value = oRow("insentifNet")
            '    params1(12) = New SqlParameter("@NilaiAlokasi", SqlDbType.Decimal)
            '    params1(12).Value = CInt(oRow("NilaiAlokasi"))


            '    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSupplierINCTVDailyDEdit, params1)
            'Next


            Dim params2() As SqlParameter = New SqlParameter(2) {}

            params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params2(0).Value = customclass.BranchId
            params2(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params2(1).Value = customclass.ApplicationID
            params2(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params2(2).Value = customclass.BusinessDate

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spRefundInsentifSave2", params2)




            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub

    Public Function GetSPBy(ByVal ocustomclass As Parameter.RefundInsentif) As Parameter.RefundInsentif
        Dim oReturnValue As New Parameter.RefundInsentif
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = ocustomclass.WhereCond

        Try
            oReturnValue.ListDataTable = SqlHelper.ExecuteDataset(ocustomclass.strConnection, CommandType.StoredProcedure, ocustomclass.SPName, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("GetSPBy", "GetSPBy", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.RefundInsentif.GetSPBy")
        End Try
    End Function

    Public Function SupplierIncentiveDailyDSave(ByVal customclass As Parameter.RefundInsentif) As String
        Dim params() As SqlParameter = New SqlParameter(14) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim status As String = ""
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customclass.ApplicationID
            params(2) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(2).Value = customclass.SupplierID
            params(3) = New SqlParameter("@SupplierEmployeeID", SqlDbType.Char, 10)
            params(3).Value = customclass.SupplierEmployeeID
            params(4) = New SqlParameter("@SupplierEmployeePosition", SqlDbType.VarChar, 10)
            params(4).Value = customclass.SupplierEmployeePosition
            params(5) = New SqlParameter("@EmployeeName", SqlDbType.VarChar, 50)
            params(5).Value = customclass.EmployeeName
            params(6) = New SqlParameter("@pph", SqlDbType.Char, 3)
            params(6).Value = customclass.pph
            params(7) = New SqlParameter("@tarifPajak", SqlDbType.Decimal)
            params(7).Value = customclass.tarifPajak
            params(8) = New SqlParameter("@nilaiPajak", SqlDbType.Decimal)
            params(8).Value = customclass.nilaiPajak
            params(9) = New SqlParameter("@insentifNet", SqlDbType.Decimal)
            params(9).Value = customclass.insentifNet
            params(10) = New SqlParameter("@nilaiAlokasi", SqlDbType.Decimal)
            params(10).Value = customclass.nilaiAlokasi
            params(11) = New SqlParameter("@TransID", SqlDbType.Char, 3)
            params(11).Value = customclass.TransID
            params(12) = New SqlParameter("@PersenAlokasi", SqlDbType.Decimal)
            params(12).Value = customclass.nilaiAlokasiPersen
            params(13) = New SqlParameter("@status", SqlDbType.VarChar, 10)
            params(13).Direction = ParameterDirection.Output
            params(14) = New SqlParameter("@IsDiscountPPH", SqlDbType.VarChar, 5)
            params(14).Value = customclass.IsDiscountPPH

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spSupplierIncentiveDailyDSave", params)

            status = CType(params(13).Value, String)
            transaction.Commit()
            Return status

        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function

    Public Sub SupplierIncentiveDailyDDelete(ByVal customclass As Parameter.RefundInsentif)
        Dim params() As SqlParameter = New SqlParameter(4) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction = Nothing

        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customclass.ApplicationID
            params(2) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(2).Value = customclass.SupplierID
            params(3) = New SqlParameter("@TransID", SqlDbType.Char, 3)
            params(3).Value = customclass.TransID
            params(4) = New SqlParameter("@isInternal", SqlDbType.Char, 1)
            params(4).Value = customclass.isInternal

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spSupplierIncentiveDailyDDelete", params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub

    Public Sub AlokasiInsentifInternalDelete(ByVal customclass As Parameter.RefundInsentif)
        Dim params() As SqlParameter = New SqlParameter(2) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customclass.ApplicationID
            params(2) = New SqlParameter("@TransID", SqlDbType.Char, 3)
            params(2).Value = customclass.TransID

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spAlokasiInsentifInternalDelete", params)

            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub

    Public Sub AlokasiInsentifInternalSave(ByVal customclass As Parameter.RefundInsentif)
        Dim params() As SqlParameter = New SqlParameter(5) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customclass.ApplicationID
            params(2) = New SqlParameter("@TransID", SqlDbType.Char, 3)
            params(2).Value = customclass.TransID
            params(3) = New SqlParameter("@IDPenggunaan", SqlDbType.VarChar, 10)
            params(3).Value = customclass.PenggunaanID
            params(4) = New SqlParameter("@Persen", SqlDbType.Decimal)
            params(4).Value = customclass.nilaiAlokasiPersen
            params(5) = New SqlParameter("@Insentif", SqlDbType.Decimal)
            params(5).Value = customclass.nilaiAlokasi

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spAlokasiInsentifInternalSave", params)

            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Function UpdateDateEntryInsentif(ByVal customclass As Parameter.RefundInsentif) As String
        Dim params() As SqlParameter = New SqlParameter(2) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = customclass.ApplicationID
            params(1) = New SqlParameter("@BussinesDate", SqlDbType.SmallDateTime)
            params(1).Value = customclass.BusinessDate
            params(2) = New SqlParameter("@status", SqlDbType.VarChar, 10)
            params(2).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spUpdateDateEntryInsentif", params)

            transaction.Commit()

            Return CType(params(2).Value, String)

        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function

    Public Function GoLiveJournalDummy(ByVal oCustomClass As Parameter.RefundInsentif) As Parameter.RefundInsentif
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        Try
            Dim parJournal() As SqlParameter = New SqlParameter(2) {}

            parJournal(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            parJournal(0).Value = oCustomClass.ApplicationID
            parJournal(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            parJournal(1).Value = oCustomClass.BranchId
            parJournal(2) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            parJournal(2).Value = oCustomClass.BusinessDate

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGoliveCrJournalDummy", parJournal)
            Return oCustomClass
            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Function

    'Public Function SupplierIncentiveDailyDSaveFactoring(ByVal customclass As Parameter.RefundInsentif) As String
    '    Dim params() As SqlParameter = New SqlParameter(13) {}

    '    Dim objCon As New SqlConnection(customclass.strConnection)
    '    Dim transaction As SqlTransaction = Nothing
    '    Dim status As String = ""
    '    Try
    '        If objCon.State = ConnectionState.Closed Then objCon.Open()
    '        transaction = objCon.BeginTransaction

    '        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
    '        params(0).Value = customclass.BranchId
    '        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
    '        params(1).Value = customclass.ApplicationID
    '        params(2) = New SqlParameter("@EmployeeID", SqlDbType.Char, 10)
    '        params(2).Value = customclass.SupplierEmployeeID
    '        params(3) = New SqlParameter("@EmployeePosition", SqlDbType.VarChar, 10)
    '        params(3).Value = customclass.SupplierEmployeePosition
    '        params(4) = New SqlParameter("@EmployeeName", SqlDbType.VarChar, 50)
    '        params(4).Value = customclass.EmployeeName
    '        params(5) = New SqlParameter("@pph", SqlDbType.Char, 3)
    '        params(5).Value = customclass.pph
    '        params(6) = New SqlParameter("@tarifPajak", SqlDbType.Decimal)
    '        params(6).Value = customclass.tarifPajak
    '        params(7) = New SqlParameter("@nilaiPajak", SqlDbType.Decimal)
    '        params(7).Value = customclass.nilaiPajak
    '        params(8) = New SqlParameter("@insentifNet", SqlDbType.Decimal)
    '        params(8).Value = customclass.insentifNet
    '        params(9) = New SqlParameter("@nilaiAlokasi", SqlDbType.Decimal)
    '        params(9).Value = customclass.nilaiAlokasi
    '        params(10) = New SqlParameter("@TransID", SqlDbType.Char, 3)
    '        params(10).Value = customclass.TransID
    '        params(11) = New SqlParameter("@PersenAlokasi", SqlDbType.Decimal)
    '        params(11).Value = customclass.nilaiAlokasiPersen
    '        params(12) = New SqlParameter("@status", SqlDbType.VarChar, 10)
    '        params(12).Direction = ParameterDirection.Output
    '        params(13) = New SqlParameter("@IsDiscountPPH", SqlDbType.VarChar, 5)
    '        params(13).Value = customclass.IsDiscountPPH

    '        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spSupplierIncentiveDailyDSaveFactoring", params)

    '        status = CType(params(12).Value, String)
    '        transaction.Commit()
    '        Return status

    '    Catch exp As Exception
    '        transaction.Rollback()
    '        Throw New Exception(exp.Message)
    '    Finally
    '        If objCon.State = ConnectionState.Open Then objCon.Close()
    '        objCon.Dispose()
    '    End Try
    'End Function

    'Public Sub SupplierINCTVDailyDAddFACT(ByVal customclass As Parameter.RefundInsentif)
    '    Dim objCon As New SqlConnection(customclass.strConnection)
    '    Dim transaction As SqlTransaction = Nothing
    '    Try
    '        If objCon.State = ConnectionState.Closed Then objCon.Open()
    '        transaction = objCon.BeginTransaction

    '        Dim params2() As SqlParameter = New SqlParameter(2) {}

    '        params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
    '        params2(0).Value = customclass.BranchId
    '        params2(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
    '        params2(1).Value = customclass.ApplicationID
    '        params2(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
    '        params2(2).Value = customclass.BusinessDate

    '        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spRefundInsentifSave2FACT", params2)
    '        transaction.Commit()
    '    Catch exp As Exception
    '        transaction.Rollback()
    '        Throw New Exception(exp.Message)
    '    Finally
    '        If objCon.State = ConnectionState.Open Then objCon.Close()
    '        objCon.Dispose()
    '    End Try
    'End Sub
    'Public Sub SupplierINCTVDailyDAddMDKJ(ByVal customclass As Parameter.RefundInsentif)
    '    Dim objCon As New SqlConnection(customclass.strConnection)
    '    Dim transaction As SqlTransaction = Nothing
    '    Try
    '        If objCon.State = ConnectionState.Closed Then objCon.Open()
    '        transaction = objCon.BeginTransaction

    '        Dim params2() As SqlParameter = New SqlParameter(2) {}

    '        params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
    '        params2(0).Value = customclass.BranchId
    '        params2(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
    '        params2(1).Value = customclass.ApplicationID
    '        params2(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
    '        params2(2).Value = customclass.BusinessDate

    '        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spRefundInsentifSave2MDKJ", params2)
    '        transaction.Commit()
    '    Catch exp As Exception
    '        transaction.Rollback()
    '        Throw New Exception(exp.Message)
    '    Finally
    '        If objCon.State = ConnectionState.Open Then objCon.Close()
    '        objCon.Dispose()
    '    End Try
    'End Sub
End Class
