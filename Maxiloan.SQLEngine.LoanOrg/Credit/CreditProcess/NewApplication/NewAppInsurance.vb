

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region


Public Class NewAppInsurance
    Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    Private m_connection As SqlConnection
    Private LISTING_NEW_APP_INSURANCE As String = "spPagingInsNewApplication"
#End Region
    Public Function getNewAppInsuranceListing(ByVal customClass As Parameter.NewAppInsurance) As Parameter.NewAppInsurance
        Dim oReturnValue As New Parameter.NewAppInsurance

        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LISTING_NEW_APP_INSURANCE, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("NewAppInsurance", "getNewAppInsuranceListing", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.getNewAppInsuranceListing")
        End Try
    End Function


End Class
