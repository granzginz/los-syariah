﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class ModalKerjaInvoice : Inherits Maxiloan.SQLEngine.DataAccessBase

    Public Function GetInvoiceList(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
        Dim oReturnValue As New Parameter.ModalKerjaInvoice
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.ApplicationID
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spModalKerjaInvoiceList", params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On ModalKerjaInvoice.GetInvoiceList - " & ex.Message)
        End Try
    End Function
    Public Function GetInvoiceList2(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
        Dim reader As SqlDataReader
        Dim oReturnValue As New Parameter.ModalKerjaInvoice
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.ApplicationID
        Try
            reader = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spModalKerjaInvoiceList2", params)
            If reader.Read Then
                oReturnValue.InvoiceNo = reader("InvoiceNo").ToString
                oReturnValue.InvoiceDueDate = reader("InvoiceDueDate")
				oReturnValue.InvoiceSeqNo = Convert.ToInt32(reader("InvoiceSeqNo").ToString)
				oReturnValue.IsFinal = reader("isAnuitas")
			End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On ModalKerjaInvoice.GetInvoiceList2 - " & ex.Message)
        End Try
    End Function
    Public Function GetJatuhTempoList(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
        Dim oReturnValue As New Parameter.ModalKerjaInvoice
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.ApplicationID
        params(1) = New SqlParameter("@ValueDate", SqlDbType.Date)
        params(1).Value = oCustomClass.ValueDate

        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spModalKerjaJatuhTempoList", params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On ModalKerjaInvoice.GetJatuhTempoList - " & ex.Message)
        End Try
    End Function

    Public Function GetInvoiceDetail(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.ModalKerjaInvoice
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
        params(1).Value = ocustomClass.InvoiceSeqNo
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, "spModalKerjaInvoiceDetail", params)
            If reader.Read Then
                oReturnValue.ApplicationID = reader("ApplicationID").ToString
                oReturnValue.BranchId = reader("BranchID").ToString
                oReturnValue.InvoiceSeqNo = Convert.ToInt32(reader("InvoiceSeqNo").ToString)
                oReturnValue.InvoiceNo = reader("InvoiceNo").ToString
                'oReturnValue.InvoiceDate = Format(reader("InvoiceDate"), "dd/MM/yyyy")
                'oReturnValue.InvoiceDueDate = Format(reader("InvoiceDueDate"), "dd/MM/yyyy")
                oReturnValue.InvoiceDate = reader("InvoiceDate")
                oReturnValue.InvoiceDueDate = reader("InvoiceDueDate")
                oReturnValue.RetensiAmount = Convert.ToDecimal(reader("RetensiAmount").ToString)
                oReturnValue.InvoiceAmount = Convert.ToDecimal(reader("InvoiceAmount").ToString)
                oReturnValue.TotalPembiayaan = Convert.ToDecimal(reader("TotalPembiayaan").ToString)
                oReturnValue.BankNameTo = reader("BankNameTo").ToString
                oReturnValue.BankBranchTo = reader("BankBranchTo").ToString
                oReturnValue.AccountNameTo = reader("AccountNameTo").ToString
                oReturnValue.AccountNoTo = reader("AccountNoTo").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On ModalKerjaInvoice.GetInvoiceDetail - " & ex.Message)
        End Try
    End Function


    Public Function InvoiceSaveAdd(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
        Dim ErrMessage As String = ""
        Dim params(14) As SqlParameter

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = ocustomClass.BranchId
        params(2) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 20)
        params(2).Value = ocustomClass.InvoiceNo
        params(3) = New SqlParameter("@InvoiceDate", SqlDbType.DateTime)
        params(3).Value = ocustomClass.InvoiceDate
        params(4) = New SqlParameter("@InvoiceDueDate", SqlDbType.DateTime)
        params(4).Value = ocustomClass.InvoiceDueDate
        params(5) = New SqlParameter("@InvoiceAmount", SqlDbType.Decimal)
        params(5).Value = ocustomClass.InvoiceAmount
        params(6) = New SqlParameter("@RetensiAmount", SqlDbType.Decimal)
        params(6).Value = ocustomClass.RetensiAmount
        params(7) = New SqlParameter("@TotalPembiayaan", SqlDbType.Decimal)
        params(7).Value = ocustomClass.TotalPembiayaan
        params(8) = New SqlParameter("@AccountNameTo", SqlDbType.VarChar, 50)
        params(8).Value = ocustomClass.AccountNameTo
        params(9) = New SqlParameter("@UsrUpd", SqlDbType.VarChar, 20)
        params(9).Value = ocustomClass.UsrUpd
        params(10) = New SqlParameter("@AccountNoTo", SqlDbType.VarChar, 50)
        params(10).Value = ocustomClass.AccountNoTo
        params(11) = New SqlParameter("@BankNameTo", SqlDbType.VarChar, 50)
        params(11).Value = ocustomClass.BankNameTo
        params(12) = New SqlParameter("@BankBranchTo", SqlDbType.VarChar, 50)
        params(12).Value = ocustomClass.BankBranchTo

        params(13) = New SqlParameter("@Err", SqlDbType.VarChar, 2000)
        params(13).Direction = ParameterDirection.Output
        params(14) = New SqlParameter("@PiutangDibiayai", SqlDbType.Decimal)
        params(14).Value = ocustomClass.PiutangDibiayai
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spModalKerjaInvoiceSaveAdd", params)
            ErrMessage = CType(params(13).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On ModalKerjaInvoice.InvoiceSaveAdd - " & ex.Message)
        End Try
    End Function

    Public Function InvoiceSaveAdd2(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
        Dim ErrMessage As String = ""
		Dim params(6) As SqlParameter

		params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = ocustomClass.BranchId
        params(2) = New SqlParameter("@DueDate", SqlDbType.Date)
        params(2).Value = ocustomClass.DueDate
        params(3) = New SqlParameter("@InvoiceNoBatch", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.InvoiceNoBatch
        params(4) = New SqlParameter("@InvoiceDate", SqlDbType.DateTime)
		params(4).Value = ocustomClass.InvoiceDate
		params(5) = New SqlParameter("@isAnuitas", SqlDbType.Bit)
		params(5).Value = ocustomClass.IsFinal

		params(6) = New SqlParameter("@Err", SqlDbType.VarChar, 2000)
		params(6).Direction = ParameterDirection.Output
		Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spModalKerjaInvoiceSaveAdd2", params)
            ErrMessage = CType(params(5).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On ModalKerjaInvoice.InvoiceSaveAdd - " & ex.Message)
        End Try
    End Function

    Public Function InvoiceSaveEdit(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
        Dim ErrMessage As String = ""
        Dim params(14) As SqlParameter

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = ocustomClass.BranchId
        params(2) = New SqlParameter("@InvoiceNoBatch", SqlDbType.VarChar, 20)
        params(2).Value = ocustomClass.InvoiceNoBatch
        params(3) = New SqlParameter("@InvoiceDate", SqlDbType.DateTime)
        params(3).Value = ocustomClass.InvoiceDate
        params(4) = New SqlParameter("@InvoiceDueDate", SqlDbType.DateTime)
        params(4).Value = ocustomClass.DueDate
        params(5) = New SqlParameter("@InvoiceAmount", SqlDbType.Decimal)
        params(5).Value = ocustomClass.InvoiceAmount
        params(6) = New SqlParameter("@RetensiAmount", SqlDbType.Decimal)
        params(6).Value = ocustomClass.RetensiAmount
        params(7) = New SqlParameter("@TotalPembiayaan", SqlDbType.Decimal)
        params(7).Value = ocustomClass.TotalPembiayaan
        params(8) = New SqlParameter("@AccountNameTo", SqlDbType.VarChar, 50)
        params(8).Value = ocustomClass.AccountNameTo
        params(9) = New SqlParameter("@UsrUpd", SqlDbType.VarChar, 20)
        params(9).Value = ocustomClass.UsrUpd
        params(10) = New SqlParameter("@AccountNoTo", SqlDbType.VarChar, 50)
        params(10).Value = ocustomClass.AccountNoTo
        params(11) = New SqlParameter("@BankNameTo", SqlDbType.VarChar, 50)
        params(11).Value = ocustomClass.BankNameTo
        params(12) = New SqlParameter("@BankBranchTo", SqlDbType.VarChar, 50)
        params(12).Value = ocustomClass.BankBranchTo

        params(13) = New SqlParameter("@Err", SqlDbType.VarChar, 2000)
        params(13).Direction = ParameterDirection.Output

        params(14) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
        params(14).Value = ocustomClass.InvoiceSeqNo
        params(15) = New SqlParameter("@PiutangDibiayai", SqlDbType.Decimal)
        params(15).Value = ocustomClass.PiutangDibiayai
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spModalKerjaInvoiceSaveEdit", params)
            ErrMessage = CType(params(13).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On ModalKerjaInvoice.InvoiceSaveEdit - " & ex.Message)
        End Try
    End Function

    Public Function InvoiceSaveEdit2(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
        Dim ErrMessage As String = ""
		Dim params(5) As SqlParameter

		params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = ocustomClass.BranchId
        params(2) = New SqlParameter("@InvoiceNoBatch", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.InvoiceNoBatch
        params(3) = New SqlParameter("@DueDate", SqlDbType.DateTime)
		params(3).Value = ocustomClass.DueDate
		params(4) = New SqlParameter("@isAnuitas", SqlDbType.Bit)
		params(4).Value = ocustomClass.IsFinal

		params(5) = New SqlParameter("@Err", SqlDbType.VarChar, 2000)
		params(5).Direction = ParameterDirection.Output

		Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spModalKerjaInvoiceSaveEdit2", params)
            ErrMessage = CType(params(4).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On ModalKerjaInvoice.InvoiceSaveEdit2 - " & ex.Message)
        End Try
    End Function

    Public Function InvoiceDelete(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
        Dim ErrMessage As String = ""
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
        params(1).Value = ocustomClass.InvoiceSeqNo
        Try
            ErrMessage = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spModalKerjaInvoiceDelete", params)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

    Public Function InvoiceSaveApplication(ByVal ocustomClass As Parameter.Application, ByVal oDataAmortisasi As DataTable) As String
        Dim ErrMessage As String = ""
        Dim params(1) As SqlParameter
        Dim paramsAmortisasi(7) As SqlParameter
        Dim transaction As SqlTransaction = Nothing
        Dim Conn As New SqlConnection(ocustomClass.strConnection)

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = ocustomClass.BranchId

        If ocustomClass.cbanuitas = True Then
            If oDataAmortisasi.Rows.Count > 0 Then

                If Conn.State = ConnectionState.Closed Then Conn.Open()
                transaction = Conn.BeginTransaction

                paramsAmortisasi(0) = New SqlParameter("@No", SqlDbType.Int)
                paramsAmortisasi(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
                paramsAmortisasi(2) = New SqlParameter("@DueDate", SqlDbType.VarChar, 8)
                paramsAmortisasi(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                paramsAmortisasi(4) = New SqlParameter("@PrincipalAmount", SqlDbType.VarChar, 17)
                paramsAmortisasi(5) = New SqlParameter("@InterestAmount", SqlDbType.VarChar, 17)
                paramsAmortisasi(6) = New SqlParameter("@InterestRate", SqlDbType.Char, 2)
                paramsAmortisasi(7) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)

                For intLoopOmset = 0 To oDataAmortisasi.Rows.Count - 1
                    paramsAmortisasi(0).Value = oDataAmortisasi.Rows(intLoopOmset).Item("No")
                    paramsAmortisasi(1).Value = oDataAmortisasi.Rows(intLoopOmset).Item("ApplicationID")
                    paramsAmortisasi(2).Value = oDataAmortisasi.Rows(intLoopOmset).Item("DueDate")
                    paramsAmortisasi(3).Value = oDataAmortisasi.Rows(intLoopOmset).Item("BranchID")
                    paramsAmortisasi(4).Value = oDataAmortisasi.Rows(intLoopOmset).Item("PrincipalAmount")
                    paramsAmortisasi(5).Value = oDataAmortisasi.Rows(intLoopOmset).Item("InterestAmount")
                    paramsAmortisasi(6).Value = oDataAmortisasi.Rows(intLoopOmset).Item("InterestRate")
                    paramsAmortisasi(7).Value = oDataAmortisasi.Rows(intLoopOmset).Item("InvoiceNo")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spModalUsahaAmortisasiSave", paramsAmortisasi)
                Next

            Else
                paramsAmortisasi(0) = New SqlParameter("@No", SqlDbType.Int)
                paramsAmortisasi(0).Value = ""
                paramsAmortisasi(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
                paramsAmortisasi(1).Value = ""
                paramsAmortisasi(2) = New SqlParameter("@DueDate", SqlDbType.Date)
                paramsAmortisasi(2).Value = "1900-01-01"
                paramsAmortisasi(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                paramsAmortisasi(3).Value = ""
                paramsAmortisasi(4) = New SqlParameter("@PrincipalAmount", SqlDbType.VarChar, 17)
                paramsAmortisasi(4).Value = 0
                paramsAmortisasi(5) = New SqlParameter("@InterestAmount", SqlDbType.VarChar, 17)
                paramsAmortisasi(5).Value = 0
                paramsAmortisasi(6) = New SqlParameter("@InterestRate", SqlDbType.Char, 2)
                paramsAmortisasi(6).Value = 0
                paramsAmortisasi(7) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
                paramsAmortisasi(7).Value = ""
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spModalUsahaAmortisasiSave", paramsAmortisasi)
            End If

            transaction.Commit()
        End If

        Try
            ErrMessage = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spModalKerjaInvoiceSaveApplication", params)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On ModalKerjaInvoice.InvoiceSaveApplication - " & ex.Message)
        End Try
    End Function

    Public Function GetFactoringFee(ByVal ocustomClass As Parameter.Application) As Parameter.ModalKerjaInvoice
        Dim ErrMessage As String = ""
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.ModalKerjaInvoice

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = ocustomClass.BranchId

        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, "spModalKerjaGetFee", params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On ModalKerjaInvoice.GetFactoringFee - " & ex.Message)
        End Try

    End Function

    Public Function FactoringApplicationFinancialSave(ByVal ocustomClass As Parameter.Application) As String
        Dim ErrMessage As String = ""
        Dim params(15) As SqlParameter

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = ocustomClass.BranchId
        params(2) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
        params(2).Value = ocustomClass.InstallmentAmount
        params(3) = New SqlParameter("@FactoringPPNAmount", SqlDbType.Decimal)
        params(3).Value = ocustomClass.FactoringPPNAmount
        params(4) = New SqlParameter("@FactoringDiscountCharges", SqlDbType.Decimal)
        params(4).Value = ocustomClass.FactoringDiscountCharges
        params(5) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
        params(5).Value = ocustomClass.AdminFee
        params(6) = New SqlParameter("@NotaryFee", SqlDbType.Decimal)
        params(6).Value = ocustomClass.NotaryFee
        params(7) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal)
        params(7).Value = ocustomClass.ProvisionFee
        params(8) = New SqlParameter("@SurveyFee", SqlDbType.Decimal)
        params(8).Value = ocustomClass.SurveyFee
        params(9) = New SqlParameter("@HandlingFee", SqlDbType.Decimal)
        params(9).Value = ocustomClass.HandlingFee
        params(10) = New SqlParameter("@FactoringPolisAmount", SqlDbType.Decimal)
        params(10).Value = ocustomClass.FactoringPolisAmount
        params(11) = New SqlParameter("@FactoringPolis", SqlDbType.Decimal)
        params(11).Value = ocustomClass.FactoringPolis
        params(12) = New SqlParameter("@BiayaPolis", SqlDbType.Decimal)
        params(12).Value = ocustomClass.BiayaPolis
        params(13) = New SqlParameter("@PPN", SqlDbType.Decimal)
        params(13).Value = ocustomClass.FactoringPPN
        params(14) = New SqlParameter("@InsuranceComBranch", SqlDbType.NChar, 10)
        params(14).Value = ocustomClass.FactoringInsuranceComID
        params(15) = New SqlParameter("@InsRateCategory", SqlDbType.VarChar, 50)
        params(15).Value = ocustomClass.InsRateCategory

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spModalKerjaApplicationFinancialSave", params)
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On ModalKerjaInvoice.FactoringApplicationFinancialSave- " & ex.Message)
        End Try
    End Function

    Public Function ModalKerjaInvoiceDisburse(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As String
        Dim Params() As SqlParameter = New SqlParameter(4) {}
        Dim transaction As SqlTransaction = Nothing
        Dim Conn As New SqlConnection(ocustomClass.strConnection)
        Dim oReturnValue As New Parameter.Drawdown

        Try
            If Conn.State = ConnectionState.Closed Then Conn.Open()

            transaction = Conn.BeginTransaction
            Params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            Params(0).Value = ocustomClass.BranchId

            Params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            Params(1).Value = ocustomClass.ApplicationID

            Params(2) = New SqlParameter("@InvoiceSeqNo", SqlDbType.VarChar, 8)
            Params(2).Value = ocustomClass.InvoiceSeqNo

            Params(3) = New SqlParameter("@LoginID", SqlDbType.Char, 20)
            Params(3).Value = ocustomClass.LoginId

            Params(4) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            Params(4).Value = ocustomClass.BusinessDate

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spModalKerjaInvoiceDisburse", Params)

            Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
            Dim oEntitiesApproval As New Parameter.Approval

            With oEntitiesApproval
                .ApprovalTransaction = transaction
                .BranchId = ocustomClass.BranchId
                .SchemeID = "RCA5"
                .RequestDate = ocustomClass.BusinessDate
                .TransactionNo = ocustomClass.InvoiceNo
                .ApprovalNote = ""

                .ApprovalValue = ocustomClass.TotalPembiayaan

                .UserRequest = ocustomClass.LoginId
                .UserApproval = ocustomClass.UserApproval
                .AprovalType = Parameter.Approval.ETransactionType.RCA_Approval
                .Argumentasi = ""
            End With

            ocustomClass.ApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)
            Dim par As IList(Of SqlParameter) = New List(Of SqlParameter)

            par.Add(New SqlParameter("@InvoiceSeqNo", SqlDbType.Int) With {.Value = ocustomClass.InvoiceSeqNo})
            par.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20) With {.Value = ocustomClass.ApplicationID})
            par.Add(New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50) With {.Value = ocustomClass.ApprovalNo})
            par.Add(New SqlParameter("@BusinessDate", SqlDbType.DateTime) With {.Value = ocustomClass.BusinessDate})
            Dim eerParams = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            par.Add(eerParams)
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spModalKerjaInvoiceDisburseProceed", par.ToArray)

            transaction.Commit()

            Return ""

        Catch ex As Exception
            transaction.Rollback()
            Throw New Exception(ex.Message)
            Return ex.Message
        Finally
            If Conn.State = ConnectionState.Open Then Conn.Close()
            Conn.Dispose()
        End Try

    End Function

    Public Function GetDataByLastPaymentDate(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
        Dim reader As SqlDataReader
        Dim oReturnValue As New Parameter.ModalKerjaInvoice
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(1).Value = oCustomClass.BranchId
        Try
            reader = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetDataModalKerjaByLastPaymentDate", params)
            If reader.Read Then
                oReturnValue.ApplicationID = reader("ApplicationID").ToString
                oReturnValue.BranchId = reader("BranchID").ToString
                oReturnValue.InvoiceNo = reader("InvoiceNo").ToString
                oReturnValue.InvoiceSeqNo = Convert.ToInt32(reader("InvoiceSeqNo").ToString)
                oReturnValue.InvoiceDate = reader("InvoiceDate").ToString
				oReturnValue.InvoiceDueDate = reader("InvoiceDueDate").ToString
				oReturnValue.InvoiceAmount = Convert.ToDecimal(reader("InvoiceAmount").ToString)
                oReturnValue.RetensiAmount = Convert.ToDecimal(reader("RetensiAmount").ToString)
                oReturnValue.InterestAmount = Convert.ToDecimal(reader("InterestAmount").ToString)
                oReturnValue.TotalPembiayaan = Convert.ToDecimal(reader("TotalPembiayaan").ToString)
                oReturnValue.OutstandingPrincipal = CDbl(reader("OutstandingPrincipal").ToString)
				oReturnValue.InsSeqno = CInt(reader("InvoiceSeqNo").ToString)
                'oReturnValue.Interestpaid = CInt(reader("Interestpaid").ToString)
                oReturnValue.Interestpaid = Convert.ToDecimal(reader("Interestpaid").ToString)
                oReturnValue.DrawDownDate = reader("DrawDownDate").ToString
				oReturnValue.EffectiveRate = CInt(reader("effectiveRate").ToString)
				oReturnValue.DueDateInvoice = (reader("DueDateInvoice").ToString)
				oReturnValue.LastInterestPaymentDate = (reader("LastInterestPaymentDate").ToString)
                oReturnValue.InterestAmountInvoice = Convert.ToDecimal(reader("InterestAmountInvoice").ToString)
                oReturnValue.TotalAlokasi = Convert.ToDecimal(reader("TotalAlokasi").ToString)
                oReturnValue.RateDenda = reader("RateDenda").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On ModalKerjaInvoice.GetDataByLastPaymentDate - " & ex.Message)
        End Try
    End Function

    Public Function GetPaymentHistory(ByVal oCustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice

        Dim oReturnValue As New Parameter.ModalKerjaInvoice
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.ApplicationID

        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetPaymentHistoryList", params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On ModalKerjaInvoice.GetJatuhTempoList - " & ex.Message)
        End Try
    End Function

    Public Function GetInvoiceDetail2(ByVal ocustomClass As Parameter.ModalKerjaInvoice) As Parameter.ModalKerjaInvoice
        Dim params(2) As SqlParameter
        Dim oReturnValue As New Parameter.ModalKerjaInvoice
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
        params(1).Value = ocustomClass.InvoiceSeqNo
        params(2) = New SqlParameter("@ValueDate", SqlDbType.Date)
        params(2).Value = ocustomClass.ValueDate
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, "spModalKerjaInvoiceDetail2", params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On ModalKerjaInvoice.GetInvoiceDetail - " & ex.Message)
        End Try
    End Function
End Class
