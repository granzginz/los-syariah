#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class AssetData : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    'Stored Procedure name    
    Private Const spPaging As String = "spAssetDataPaging"
    Private Const spFillCboEmployee As String = "spAssetDataFillCboEmployee"
    'Private Const spFillCboEmployee As String = "spAssetDataFillCboEmployee_2"
    Private Const spFillCboEmployeeReport As String = "spReportAssetDataFillCboEmployee"
    Private Const spFillCbo As String = "spAssetDataFillCbo"
    Private Const spAttribute As String = "spAssetDataAttribute"
    Private Const spSerial As String = "spAssetDataSerial"
    Private Const SpAssetDataUsedNew As String = "SpAssetDataUsedNew"
    Private Const spAssetDataSaveAdd As String = "spAssetDataSaveAdd"
    Private Const spAssetDataSaveAdd2 As String = "spAssetDataSaveAdd2"
    Private Const spAssetDataSaveAdd3 As String = "spAssetDataSaveAdd3"
    Private Const spAssetDataSaveAdd4 As String = "spAssetDataSaveAdd4"
    Private Const spAssetDataGetAO As String = "spAssetDataGetAO"
    Private Const spAssetDataCheckSerial As String = "spAssetDataCheckSerial"
    Private Const spFillCboEmployee2 As String = "spAssetDataFillCboEmployee2"
    Private Const spCheckAttribute As String = "spAssetDataCheckAttribute"
    Private Const spAssetDataAssetDoc As String = "spAssetDataAssetDoc"
    Private Const spAssetDataAssetDocWhere As String = "spAssetDataAssetDocWhere"
    Private Const spAssetDataCheckAssetDoc As String = "spAssetDataCheckAssetDoc"
    Private Const spAssetDataGetAssetID_AppID As String = "spAssetDataGetAssetID"
    Private Const spAssetDataGetSupplierGroupID As String = "spAssetDataGetSupplierGroupID"

    Private Const spTolakanNonCustSaveAdd As String = "spTolakanNonCustSaveAdd"
    Private Const spTolakanNonCustSaveEdit As String = "spTolakanNonCustSaveEdit"
    Private Const spGetcboAlasanPenolakan As String = "spGetcboAlasanPenolakan"
    Private Const spGetcboDetailPenolakan As String = "spGetcboDetailPenolakan"
    Private Const spGetcboStatus As String = "spGetcboStatus"
    Private Const spTolakanNonCustPaging As String = "spTolakanNonCustPaging"
    Private Const spTolakanNonCustDelete As String = "spTolakanNonCustDelete"
    Private Const spTolakanNonCustEdit As String = "spTolakanNonCustEdit"

#End Region

#Region "Get"
    Public Function GetAssetData(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spPaging, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.GetAssetData")
        End Try
    End Function
    Public Function GetCboEmp(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim SPName As String
        If oCustomClass.Table = "BranchEmployee" Then
            SPName = spFillCboEmployee
        Else
            SPName = spFillCboEmployee2
        End If
        Dim oReturnValue As New Parameter.AssetData
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Where", SqlDbType.VarChar, 1000)
        params(0).Value = oCustomClass.Where
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, SPName, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.GetCboEmp")
        End Try
    End Function

    Public Function GetCboEmpReport(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim SPName As String
        If oCustomClass.Table = "BranchEmployee" Then
            SPName = spFillCboEmployeeReport
        Else
            SPName = spFillCboEmployeeReport
        End If
        Dim oReturnValue As New Parameter.AssetData
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Where", SqlDbType.VarChar, 1000)
        params(0).Value = oCustomClass.Where
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, SPName, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.GetCboEmp")
        End Try
    End Function
    Public Function GetCbo(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 70)
        params(0).Value = oCustomClass.Table
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spFillCbo, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.GetCbo")
        End Try
    End Function
    Public Function GetAttribute(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@AssetID", SqlDbType.Char, 10)
        params(0).Value = oCustomClass.AssetID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spAttribute, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.GetAttribute")
        End Try
    End Function
    Public Function GetSerial(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@AssetID", SqlDbType.Char, 10)
        params(0).Value = oCustomClass.AssetID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spSerial, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.GetSerial")
        End Try
    End Function
    Public Function GetUsedNew(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = oCustomClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = oCustomClass.AppID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, SpAssetDataUsedNew, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.GetUsedNew")
        End Try

    End Function
    Public Function GetAO(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = oCustomClass.BranchId
        params(1) = New SqlParameter("@Supplier", SqlDbType.Char, 10)
        params(1).Value = oCustomClass.SupplierID
        params(2) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(2).Value = oCustomClass.ApplicationId

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spAssetDataGetAO, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.GetAO")
        End Try
    End Function
    Public Function GetAssetDoc(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@AssetID", SqlDbType.VarChar, 10)
        params(0).Value = oCustomClass.AssetID
        params(1) = New SqlParameter("@CustomerType", SqlDbType.Char, 1)
        params(1).Value = oCustomClass.CustomerType
        params(2) = New SqlParameter("@Origination", SqlDbType.Char, 3)
        params(2).Value = oCustomClass.Origination
        params(3) = New SqlParameter("@WhereCond", SqlDbType.Char, 500)
        params(3).Value = oCustomClass.WhereCond
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spAssetDataAssetDoc, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.GetAssetDoc")
        End Try
    End Function
    Public Function GetAssetDocWhere(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@AssetID", SqlDbType.VarChar, 10)
        params(0).Value = oCustomClass.AssetID
        params(1) = New SqlParameter("@CustomerType", SqlDbType.Char, 1)
        params(1).Value = oCustomClass.CustomerType
        params(2) = New SqlParameter("@Origination", SqlDbType.Char, 3)
        params(2).Value = oCustomClass.Origination
        params(3) = New SqlParameter("@WhereCond", SqlDbType.Char, 500)
        params(3).Value = oCustomClass.WhereCond
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spAssetDataAssetDocWhere, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.GetAssetDocWhere")
        End Try
    End Function
    Public Function GetAssetID(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.CustomerID.Trim
        params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(1).Value = oCustomClass.BranchId
        params(2) = New SqlParameter("@ProductID", SqlDbType.VarChar, 10)
        params(2).Value = oCustomClass.ProductID
        params(3) = New SqlParameter("@ProductOfferingID", SqlDbType.VarChar, 10)
        params(3).Value = oCustomClass.ProductOfferingID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spAssetDataGetAssetID_AppID, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.GetAssetID")
        End Try
    End Function
    Public Function GetSupplierGroupID(ByVal oCustomClass As Parameter.AssetData) As String
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
        params(0).Value = oCustomClass.SupplierID
        params(1) = New SqlParameter("@SupplierGroupID", SqlDbType.Char, 1)
        params(1).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spAssetDataGetSupplierGroupID, params)
            Return CType(params(1).Value, String)
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.GetAssetID")
        End Try
    End Function
#End Region

#Region "Check"
    Public Function CheckSerial(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@AssetID", SqlDbType.Char, 10)
        params(0).Value = oCustomClass.AssetID
        params(1) = New SqlParameter("@Serial1", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.Serial1
        params(2) = New SqlParameter("@Serial2", SqlDbType.VarChar, 50)
        params(2).Value = oCustomClass.Serial2
        params(3) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(3).Value = oCustomClass.AppID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spAssetDataCheckSerial, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("AssetData", "CheckSerial", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.CheckSerial")
        End Try
    End Function
    Public Function CheckAttribute(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim params1() As SqlParameter = New SqlParameter(1) {}

        If oCustomClass.ApplicationId.Trim <> "" Then
            params(0) = New SqlParameter("@AssetID", SqlDbType.Char, 10)
            params(0).Value = oCustomClass.AssetID
            params(1) = New SqlParameter("@Content", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.Input
            params(2) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(2).Value = oCustomClass.ApplicationId
        Else
            params1(0) = New SqlParameter("@AssetID", SqlDbType.Char, 10)
            params1(0).Value = oCustomClass.AssetID
            params1(1) = New SqlParameter("@Content", SqlDbType.VarChar, 50)
            params1(1).Value = oCustomClass.Input
        End If




        Try
            If oCustomClass.ApplicationId.Trim <> "" Then
                oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spAssetDataCheckAttribute1", params).Tables(0)
            Else
                oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spCheckAttribute, params1).Tables(0)
            End If

            Return oReturnValue
        Catch exp As Exception
            WriteException("AssetData", "CheckAttribute", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.CheckAttribute")
        End Try
    End Function
    Public Function CheckAssetDoc(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData
        Dim params() As SqlParameter = New SqlParameter(3) {}

        params(0) = New SqlParameter("@AssetID", SqlDbType.Char, 10)
        params(0).Value = oCustomClass.AssetID
        params(1) = New SqlParameter("@Content", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.Input
        params(2) = New SqlParameter("@AssetDocID", SqlDbType.VarChar, 50)
        params(2).Value = oCustomClass.AssetDocID
        params(3) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(3).Value = oCustomClass.ApplicationId

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spAssetDataCheckAssetDoc, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("AssetData", "CheckAssetDoc", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.CheckAssetDoc")
        End Try
    End Function
#End Region

#Region "Save"
    Public Function AssetDataSaveAdd(ByVal oAssetData As Parameter.AssetData, _
                                     ByVal oAddress As Parameter.Address, _
                                     ByVal oData1 As DataTable, _
                                     ByVal oData2 As DataTable, _
                                     ByVal oData3 As DataTable) As Parameter.AssetData

        Dim oReturnValue As New Parameter.AssetData
        Dim m_connection As New SqlConnection(oAssetData.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim Message As String = ""
        Dim intLoop As Integer
        Dim params() As SqlParameter = New SqlParameter(62) {}
        Dim params1() As SqlParameter = New SqlParameter(3) {}
        Dim params2() As SqlParameter = New SqlParameter(4) {}
        Dim params3() As SqlParameter = New SqlParameter(10) {}

        Try
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            transaction = m_connection.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oAssetData.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = oAssetData.AppID
            params(2) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(2).Value = oAssetData.SupplierID
            params(3) = New SqlParameter("@OTRPrice", SqlDbType.Decimal)
            params(3).Value = oAssetData.OTR
            params(4) = New SqlParameter("@DPAmount", SqlDbType.Decimal)
            params(4).Value = oAssetData.DP
            params(5) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
            params(5).Value = oAssetData.AssetID
            params(6) = New SqlParameter("@AssetCode", SqlDbType.VarChar, 50)
            params(6).Value = oAssetData.AssetCode
            params(7) = New SqlParameter("@SerialNo1", SqlDbType.VarChar, 50)
            params(7).Value = oAssetData.Serial1
            params(8) = New SqlParameter("@SerialNo2", SqlDbType.VarChar, 50)
            params(8).Value = oAssetData.Serial2
            params(9) = New SqlParameter("@UsedNew", SqlDbType.Char, 1)
            params(9).Value = oAssetData.UsedNew
            params(10) = New SqlParameter("@AssetUsage", SqlDbType.VarChar, 10)
            params(10).Value = oAssetData.AssetUsage
            params(11) = New SqlParameter("@ManufacturingYear", SqlDbType.SmallInt)
            params(11).Value = oAssetData.ManufacturingYear
            params(12) = New SqlParameter("@OldOwnerAsset", SqlDbType.VarChar, 50)
            params(12).Value = oAssetData.OldOwnerAsset
            params(13) = New SqlParameter("@OldOwnerAddress", SqlDbType.VarChar, 100)
            params(13).Value = oAddress.Address
            params(14) = New SqlParameter("@OldOwnerRT", SqlDbType.Char, 3)
            params(14).Value = oAddress.RT
            params(15) = New SqlParameter("@OldOwnerRW", SqlDbType.Char, 3)
            params(15).Value = oAddress.RW
            params(16) = New SqlParameter("@OldOwnerKelurahan", SqlDbType.VarChar, 30)
            params(16).Value = oAddress.Kelurahan
            params(17) = New SqlParameter("@OldOwnerKecamatan", SqlDbType.VarChar, 30)
            params(17).Value = oAddress.Kecamatan
            params(18) = New SqlParameter("@OldOwnerCity", SqlDbType.VarChar, 30)
            params(18).Value = oAddress.City
            params(19) = New SqlParameter("@OldOwnerZipCode", SqlDbType.Char, 5)
            params(19).Value = oAddress.ZipCode
            params(20) = New SqlParameter("@TaxDate", SqlDbType.VarChar, 8)
            params(20).Value = oAssetData.TaxDate
            params(21) = New SqlParameter("@Notes", SqlDbType.Text)
            params(21).Value = oAssetData.Notes
            params(22) = New SqlParameter("@InsAssetInsuredBy", SqlDbType.VarChar, 10)
            params(22).Value = oAssetData.InsuredBy
            params(23) = New SqlParameter("@InsAssetPaidBy", SqlDbType.VarChar, 10)
            params(23).Value = oAssetData.PaidBy
            params(24) = New SqlParameter("@AOID", SqlDbType.Char, 20)
            params(24).Value = oAssetData.AOID
            params(25) = New SqlParameter("@CAID", SqlDbType.Char, 10)
            params(25).Value = oAssetData.CAID
            params(26) = New SqlParameter("@SurveyorID", SqlDbType.Char, 10)
            params(26).Value = oAssetData.SurveyorID
            params(27) = New SqlParameter("@SalesmanID", SqlDbType.Char, 10)
            params(27).Value = oAssetData.SalesmanID
            params(28) = New SqlParameter("@SalesSupervisorID", SqlDbType.Char, 10)
            params(28).Value = IIf(oAssetData.SalesSupervisorID = "Select One", DBNull.Value, oAssetData.SalesSupervisorID)
            params(29) = New SqlParameter("@SupplierAdminID", SqlDbType.Char, 10)
            params(29).Value = IIf(oAssetData.SupplierAdminID = "Select One", DBNull.Value, oAssetData.SupplierAdminID)
            params(30) = New SqlParameter("@IsIncentiveSupplier", SqlDbType.Char, 1)
            params(30).Value = oAssetData.IsIncentiveSupplier
            params(31) = New SqlParameter("@DateEntryAssetData", SqlDbType.DateTime)
            params(31).Value = oAssetData.DateEntryAssetData
            params(32) = New SqlParameter("@Err", SqlDbType.VarChar, 255)
            params(32).Direction = ParameterDirection.Output
            params(33) = New SqlParameter("@Message", SqlDbType.VarChar, 100)
            params(33).Direction = ParameterDirection.Output
            params(34) = New SqlParameter("@Flag", SqlDbType.VarChar, 20)
            params(34).Value = oAssetData.Flag
            params(35) = New SqlParameter("@Pemakai", SqlDbType.VarChar, 50)
            params(35).Value = oAssetData.Pemakai
            params(36) = New SqlParameter("@Lokasi", SqlDbType.VarChar, 50)
            params(36).Value = oAssetData.Lokasi
            params(37) = New SqlParameter("@HargaLaku", SqlDbType.Decimal)
            params(37).Value = oAssetData.HargaLaku
            params(38) = New SqlParameter("@SR1", SqlDbType.VarChar, 50)
            params(38).Value = oAssetData.SR1
            params(39) = New SqlParameter("@SR2", SqlDbType.VarChar, 50)
            params(39).Value = oAssetData.SR2
            params(40) = New SqlParameter("@HargaSR1", SqlDbType.Decimal)
            params(40).Value = oAssetData.HargaSR1
            params(41) = New SqlParameter("@HargaSR2", SqlDbType.Decimal)
            params(41).Value = oAssetData.HargaSR2
            params(42) = New SqlParameter("@splitPembayaran", SqlDbType.Bit)
            params(42).Value = oAssetData.SplitPembayaran
            params(43) = New SqlParameter("@UangMukaBayarDi", SqlDbType.Char, 1)
            params(43).Value = oAssetData.UangMukaBayarDi
            params(44) = New SqlParameter("@PencairanKe", SqlDbType.Char, 1)
            params(44).Value = oAssetData.PencairanKe
            params(45) = New SqlParameter("@SupplierIDKaroseri", SqlDbType.Char, 20)
            params(45).Value = IIf(oAssetData.SupplierIDKaroseri = "", DBNull.Value, oAssetData.SupplierIDKaroseri)
            params(46) = New SqlParameter("@HargaKaroseri", SqlDbType.Decimal)
            params(46).Value = oAssetData.HargaKaroseri
            params(47) = New SqlParameter("@PHJMB", SqlDbType.Decimal)
            params(47).Value = oAssetData.PHJMB
            params(48) = New SqlParameter("@StatusKendaraan", SqlDbType.Char, 1)
            params(48).Value = oAssetData.StatusKendaraan
            params(49) = New SqlParameter("@NamaBPKBSama", SqlDbType.Bit)
            params(49).Value = oAssetData.NamaBPKBSama
            params(50) = New SqlParameter("@IsBPKBPengganti", SqlDbType.Bit)
            params(50).Value = oAssetData.BPKBPengganti
            params(51) = New SqlParameter("@AlasanSTNKExpired", SqlDbType.VarChar, 255)
            params(51).Value = oAssetData.AlasanSTNKExpired
            params(52) = New SqlParameter("@OwnerAssetCompany", SqlDbType.Bit)
            params(52).Value = oAssetData.OwnerAssetCompany
            params(53) = New SqlParameter("@KondisiAsset", SqlDbType.Char, 1)
            params(53).Value = oAssetData.KondisiAsset
            params(54) = New SqlParameter("@GradeCode", SqlDbType.Char, 2)
            params(54).Value = oAssetData.GradeCode
            params(55) = New SqlParameter("@GradeValue", SqlDbType.Decimal)
            params(55).Value = oAssetData.GradeValue
            params(56) = New SqlParameter("@IsIzinTrayek", SqlDbType.Bit)
            params(56).Value = oAssetData.IsIzinTrayek
            params(57) = New SqlParameter("@TrayekAtasNama", SqlDbType.Char, 50)
            params(57).Value = oAssetData.TrayekAtasNama
            params(58) = New SqlParameter("@Jurusan", SqlDbType.Char, 50)
            params(58).Value = oAssetData.Jurusan
            params(59) = New SqlParameter("@BukuKeur", SqlDbType.Char, 50)
            params(59).Value = oAssetData.BukuKeur
            params(60) = New SqlParameter("@RVEstimateOL", SqlDbType.Decimal)
            params(60).Value = oAssetData.RVEstimateOL
            params(61) = New SqlParameter("@RVInterestOL", SqlDbType.Decimal)
            params(61).Value = oAssetData.RVInterestOL
            'modify nofi 04022019 
            params(62) = New SqlParameter("@DPKaroseriAmount", SqlDbType.Decimal)
            params(62).Value = oAssetData.DPKaroseriAmount

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spAssetDataSaveAdd, params)
            ErrMessage = CType(params(32).Value, String)
            Message = CType(params(33).Value, String)

            If ErrMessage <> "" Then Throw New Exception(ErrMessage)

            If oData1.Rows.Count > 0 Then
                params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params1(2) = New SqlParameter("@AssetLevel", SqlDbType.Int)
                params1(3) = New SqlParameter("@AssetCode", SqlDbType.VarChar, 50)
                For intLoop = 0 To oData1.Rows.Count - 1
                    params1(0).Value = oAssetData.BranchId
                    params1(1).Value = oAssetData.AppID
                    params1(2).Value = oData1.Rows(intLoop).Item("AssetLevel")
                    params1(3).Value = oData1.Rows(intLoop).Item("AssetCode").ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spAssetDataSaveAdd2, params1)
                Next
            End If
            If oData2.Rows.Count > 0 Then
                params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params2(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params2(2) = New SqlParameter("@AttributeID", SqlDbType.Char, 10)
                params2(3) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
                params2(4) = New SqlParameter("@AttributeContent", SqlDbType.VarChar, 50)
                For intLoop = 0 To oData2.Rows.Count - 1
                    params2(0).Value = oAssetData.BranchId
                    params2(1).Value = Trim(oAssetData.AppID)
                    params2(2).Value = Trim(oData2.Rows(intLoop).Item("AttributeID").ToString)
                    params2(3).Value = Trim(oAssetData.AssetID)
                    params2(4).Value = Trim(oData2.Rows(intLoop).Item("AttributeContent").ToString)
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spAssetDataSaveAdd3, params2)
                Next
            End If
            If oData3.Rows.Count > 0 Then
                params3(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params3(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params3(2) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
                params3(3) = New SqlParameter("@AssetDocID", SqlDbType.Char, 10)
                params3(4) = New SqlParameter("@DocumentNo", SqlDbType.VarChar, 50)
                params3(5) = New SqlParameter("@IsMainDoc", SqlDbType.Char, 1)
                params3(6) = New SqlParameter("@IsDocExist", SqlDbType.Char, 1)
                params3(7) = New SqlParameter("@Notes", SqlDbType.Text)
                params3(8) = New SqlParameter("@IsFollowUp", SqlDbType.Char, 1)
                params3(9) = New SqlParameter("@TglDokument", SqlDbType.Date)
                params3(10) = New SqlParameter("@TglJanji", SqlDbType.Date)
                For intLoop = 0 To oData3.Rows.Count - 1
                    params3(0).Value = oAssetData.BranchId
                    params3(1).Value = oAssetData.AppID
                    params3(2).Value = oAssetData.AssetID
                    params3(3).Value = oData3.Rows(intLoop).Item("AssetDocID").ToString
                    params3(4).Value = oData3.Rows(intLoop).Item("DocumentNo").ToString
                    params3(5).Value = IIf(oData3.Rows(intLoop).Item("IsMainDoc").ToString = "True", "1", "0").ToString
                    params3(6).Value = oData3.Rows(intLoop).Item("IsDocExist").ToString
                    params3(7).Value = oData3.Rows(intLoop).Item("Notes").ToString
                    params3(8).Value = oData3.Rows(intLoop).Item("IsFollowUp").ToString
                    params3(9).Value = oData3.Rows(intLoop).Item("TglDokument").ToString
                    params3(10).Value = oData3.Rows(intLoop).Item("TglJanji").ToString
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spAssetDataSaveAdd4, params3)
                Next
            End If

            'alasan koreksi
            If oAssetData.isKoreksi Then

                Dim parKoreksi() As SqlParameter = New SqlParameter(3) {}

                parKoreksi(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                parKoreksi(0).Value = oAssetData.ApplicationId
                parKoreksi(1) = New SqlParameter("@BranchId", SqlDbType.VarChar, 50)
                parKoreksi(1).Value = oAssetData.BranchId
                parKoreksi(2) = New SqlParameter("@CorrectionDesc", SqlDbType.VarChar, 200)
                parKoreksi(2).Value = oAssetData.alasanKoreksi
                parKoreksi(3) = New SqlParameter("@Modul", SqlDbType.VarChar, 200)
                parKoreksi(3).Value = "ASET"

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spAppCorrectionLogSave", parKoreksi)
            End If


            transaction.Commit()
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            If ErrMessage = "" Then ErrMessage = exp.Message
            oReturnValue.Output = ErrMessage
            oReturnValue.Message = Message
            Return oReturnValue
            Throw New Exception(ErrMessage)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try

    End Function
#End Region

#Region "View Asset Data"
    Public Function GetViewAssetData(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData
        Dim intLoop As Integer
        Dim spViewAssetData As String
        Dim AssettypeId As String
        Dim oData As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Dim params1() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.AppID
            For intLoop = 0 To 1
                Select Case intLoop
                    Case 0
                        spViewAssetData = "spViewAssetData"
                        oData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spViewAssetData, params).Tables(0)
                        If oData.Rows.Count > 0 Then
                            AssettypeId = oData.Rows(0).Item(31).ToString.Trim
                        End If
                        oReturnValue.ListData = oData
                    Case 1
                        spViewAssetData = "spViewAssetData" + "3"
                        oReturnValue.DataAssetdoc = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spViewAssetData, params).Tables(0)
                End Select
            Next
            params1(0) = New SqlParameter("@AssetTypeID", SqlDbType.VarChar, 10)
            params1(0).Value = AssettypeId
            params1(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params1(1).Value = oCustomClass.AppID
            spViewAssetData = "spViewAssetData" + "2"
            oReturnValue.DataAttribute = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spViewAssetData, params1).Tables(0)
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            WriteException("AssetData", "GetViewAssetData", exp.Message + exp.StackTrace)
            oReturnValue.Output = "Error: " & exp.Message & ""
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.GetViewAssetData")
        End Try
    End Function
#End Region

    Public Function EditAssetDataGetDefaultSupplierID(ByVal oCustumClass As Parameter.AssetData) As Parameter.AssetData
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = oCustumClass.AppID

        Try
            Dim oReturnValue As New Parameter.AssetData
            reader = SqlHelper.ExecuteReader(oCustumClass.strConnection, CommandType.StoredProcedure, oCustumClass.SpName, params)
            If reader.Read Then
                oReturnValue.SupplierID = reader("supplierid").ToString.Trim
                oReturnValue.SupplierName = reader("suppliername").ToString.Trim
                oReturnValue.NewAmountFee = CDbl(reader("NewAmountFee"))
                oReturnValue.UsedAmountFee = CDbl(reader("UsedAmountFee"))

            End If
            reader.Close()
            Return oReturnValue

        Catch exp As Exception
            WriteException("AssetData", "EditAssetDataGetDefaultSupplierID", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("AssetData-DA", "EditAssetDataGetDefaultSupplierID", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
        End Try
    End Function

    Public Function EditGetAttribute(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.AppID
        params(1) = New SqlParameter("@AssetID", SqlDbType.Char, 10)
        params(1).Value = oCustomClass.AssetID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, oCustomClass.SpName, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Dim err As New MaxiloanExceptions
            WriteException("AssetData", "EditGetAttribute", exp.Message + exp.StackTrace)
            err.WriteLog("AssetData-DA", "EditGetAssetRegistration", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.GetAttribute")
        End Try
    End Function

    Public Function EditGetAssetRegistration(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.AppID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, oCustomClass.SpName, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Dim err As New MaxiloanExceptions
            WriteException("AssetData", "EditGetAssetRegistration", exp.Message + exp.StackTrace)
            err.WriteLog("AssetData-DA", "EditGetAssetRegistration", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)

        End Try

    End Function

    Public Function EditAssetInsuranceEmployee(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.AppID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, oCustomClass.SpName, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("AssetData", "GetViewApplication", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("AssetData-DA", "EditAssetInsuranceEmployee", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
        End Try
    End Function

    Public Function EditAssetDataDocument(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData
        'Dim params() As SqlParameter = New SqlParameter(3) {}
        'params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        'params(0).Value = oCustomClass.AppID
        'params(1) = New SqlParameter("@AssetID", SqlDbType.Char, 10)
        'params(1).Value = oCustomClass.AssetID
        'params(2) = New SqlParameter("@CustomerType", SqlDbType.Char, 1)
        'params(2).Value = oCustomClass.CustomerType
        'params(3) = New SqlParameter("@Origination", SqlDbType.Char, 3)
        'params(3).Value = oCustomClass.Origination

        Dim params As SqlParameter = New SqlParameter

        params = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params.Value = oCustomClass.WhereCond

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, oCustomClass.SpName, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("AssetData", "EditAssetDataDocument", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("AssetData-DA", "EditAssetDataDocument", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.GetAttribute")
        End Try
    End Function


    Public Function GetTolakanNonCustPaging(ByVal oCustomClass As Parameter.TolakanNonCust) As Parameter.TolakanNonCust
        Dim oReturnValue As New Parameter.TolakanNonCust
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spTolakanNonCustPaging, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.GetAssetData")
        End Try
    End Function

    Public Function GetcboAlasanPenolakan(ByVal oCustomClass As Parameter.TolakanNonCust) As Parameter.TolakanNonCust
        Dim SPName As String = ""
        If oCustomClass.table = "Reason" Then
            SPName = spGetcboAlasanPenolakan
        ElseIf oCustomClass.table = "tblDetailPenolakan" Then
            SPName = spGetcboDetailPenolakan
        ElseIf oCustomClass.table = "tblMaritalStatus" Then
            SPName = spGetcboStatus
        End If
        Dim oReturnValue As New Parameter.TolakanNonCust
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Where", SqlDbType.VarChar, 1000)
        params(0).Value = oCustomClass.Where
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, SPName, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception("Error On DataAccess.AccAcq.AssetData.GetCboEmp")
        End Try
    End Function

    Public Function TolakanNonCustSaveAdd(ByVal oAssetData As Parameter.TolakanNonCust, _
                                          ByVal oAddress As Parameter.Address) As Parameter.TolakanNonCust

        Dim oReturnValue As New Parameter.TolakanNonCust
        Dim m_connection As New SqlConnection(oAssetData.strConnection)
        Dim ErrMessage As String = ""
        Dim params() As SqlParameter = New SqlParameter(27) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oAssetData.BranchId
            params(1) = New SqlParameter("@Nama", SqlDbType.VarChar, 50)
            params(1).Value = oAssetData.Nama
            params(2) = New SqlParameter("@NoKTP", SqlDbType.VarChar, 30)
            params(2).Value = oAssetData.NoKTP
            params(3) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
            params(3).Value = oAssetData.StatusPerkawinan
            params(4) = New SqlParameter("@TglLahir", SqlDbType.VarChar, 10)
            params(4).Value = oAssetData.BirthDate
            params(5) = New SqlParameter("@Pekerjaan", SqlDbType.VarChar, 50)
            params(5).Value = oAssetData.Pekerjaan
            params(6) = New SqlParameter("Address", SqlDbType.VarChar, 100)
            params(6).Value = oAddress.Address
            params(7) = New SqlParameter("RT", SqlDbType.Char, 3)
            params(7).Value = oAddress.RT
            params(8) = New SqlParameter("RW", SqlDbType.Char, 3)
            params(8).Value = oAddress.RW
            params(9) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
            params(9).Value = oAddress.Kelurahan
            params(10) = New SqlParameter("Kecamatan", SqlDbType.VarChar, 30)
            params(10).Value = oAddress.Kecamatan
            params(11) = New SqlParameter("@City", SqlDbType.VarChar, 30)
            params(11).Value = oAddress.City
            params(12) = New SqlParameter("ZipCode", SqlDbType.Char, 5)
            params(12).Value = oAddress.ZipCode
            params(13) = New SqlParameter("@Err", SqlDbType.VarChar, 255)
            params(13).Direction = ParameterDirection.Output
            params(14) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
            params(14).Value = oAddress.AreaPhone1
            params(15) = New SqlParameter("@Phone1", SqlDbType.Char, 10)
            params(15).Value = oAddress.Phone1
            params(16) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
            params(16).Value = oAddress.AreaPhone2
            params(17) = New SqlParameter("@Phone2", SqlDbType.Char, 10)
            params(17).Value = oAddress.Phone2
            params(18) = New SqlParameter("@AreaFax", SqlDbType.Char, 4)
            params(18).Value = oAddress.AreaFax
            params(19) = New SqlParameter("@Fax", SqlDbType.Char, 10)
            params(19).Value = oAddress.Fax

            params(20) = New SqlParameter("@AlasanPenolakan", SqlDbType.VarChar, 50)
            params(20).Value = oAssetData.AlasanPenolakan
            params(21) = New SqlParameter("@DetailPenolakan", SqlDbType.VarChar, 50)
            params(21).Value = oAssetData.DetailPenolakan
            params(22) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
            params(22).Value = oAssetData.SupplierID
            params(23) = New SqlParameter("@AssetCode", SqlDbType.VarChar, 50)
            params(23).Value = oAssetData.AssetCode
            params(24) = New SqlParameter("@jumlah", SqlDbType.SmallInt)
            params(24).Value = oAssetData.jumlah

            params(25) = New SqlParameter("@Keterangan", SqlDbType.VarChar, 100)
            params(25).Value = oAssetData.Keterangan
            params(26) = New SqlParameter("@BirthPlace", SqlDbType.VarChar, 20)
            params(26).Value = oAssetData.BirthPlace
            params(27) = New SqlParameter("@ID", SqlDbType.Int)
            params(27).Value = oAssetData.ID
            If oAssetData.AddEdit = "Add" Then
                SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, spTolakanNonCustSaveAdd, params)
            Else
                SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, spTolakanNonCustSaveEdit, params)
            End If

            ErrMessage = CType(params(13).Value, String)

            If ErrMessage <> "" Then Throw New Exception(ErrMessage)

            Return oReturnValue
        Catch exp As Exception

            If ErrMessage = "" Then ErrMessage = exp.Message
            oReturnValue.Output = ErrMessage
            Return oReturnValue
            Throw New Exception(ErrMessage)
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Function

  

    Public Function GetTolakanNonCustEdit(ByVal oCustomClass As Parameter.TolakanNonCust) As Parameter.TolakanNonCust
        Dim oReturnValue As New Parameter.TolakanNonCust
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@ID", SqlDbType.Int)
            params(0).Value = oCustomClass.ID
            params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(1).Value = Replace(oCustomClass.BranchId, "'", "")
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spTolakanNonCustEdit, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetApplicationEdit", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetTolakanNonCustEdit")
        End Try
    End Function

    Public Function GetTolakanNonCustDelete(ByVal customClass As Parameter.TolakanNonCust) As String
        Dim paramsx(1) As SqlParameter
        Dim err As Integer
        ' If customClass.FormID = "TolakanNonCust" Then
        paramsx(0) = New SqlParameter("@ID", SqlDbType.Int)
        paramsx(0).Value = customClass.ID
        paramsx(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        paramsx(1).Value = customClass.BranchId.Trim.ToString
        Try
            Err = SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, spTolakanNonCustDelete, paramsx)
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.TolakanNonCustDelete")
        End Try
        '  End If
    End Function

    Public Function ListReportTolakanNonCust(ByVal customclass As Parameter.TolakanNonCust) As Parameter.TolakanNonCust
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.ListReportTolakanNonCust = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spReportTolakanNonCust", params)
        Return customclass
    End Function

    Public Function GetGradeAsset(ByVal oCustomClass As Parameter.AssetData) As Parameter.AssetData
        Dim oReturnValue As New Parameter.AssetData

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetGradeAsset").Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Application", "GetGradeAsset", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Application.GetGradeAsset")
        End Try
    End Function
End Class