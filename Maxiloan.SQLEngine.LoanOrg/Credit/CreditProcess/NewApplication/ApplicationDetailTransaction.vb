﻿
Imports Maxiloan.Framework.SQLEngine
Imports System.Data.SqlClient

Public Class ApplicationDetailTransaction : Inherits Maxiloan.SQLEngine.DataAccessBase
    Public Function getApplicationDetailTransaction(ByVal oCustomClass As Parameter.ApplicationDetailTransaction) As Parameter.ApplicationDetailTransaction
        Dim par() As SqlParameter = New SqlParameter(1) {}

        Try
            par(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par(0).Value = oCustomClass.BranchId
            par(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            par(1).Value = oCustomClass.ApplicationID

            oCustomClass.ApplicationDetailTransactionDT = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetAgreementDetailTransaction", par).Tables(0)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub ApplicationDetailTransactionSave(ByVal oCustomClass As Parameter.ApplicationDetailTransaction)
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim Transaction As SqlTransaction = Nothing

        Try
            If oConn.State = ConnectionState.Closed Then oConn.Open()
            Transaction = oConn.BeginTransaction

            Dim par1() As SqlParameter = New SqlParameter(2) {}

            par1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            par1(2) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            par1(0).Value = oCustomClass.BranchId
            par1(1).Value = oCustomClass.ApplicationID
            par1(2).Value = oCustomClass.BusinessDate

            SqlHelper.ExecuteNonQuery(Transaction, CommandType.StoredProcedure, "spAgreementDetailTransactionSave", par1)

            Dim par2() As SqlParameter = New SqlParameter(6) {}

            par2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par2(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            par2(2) = New SqlParameter("@TransID", SqlDbType.Char, 20)
            par2(3) = New SqlParameter("@TransAmount", SqlDbType.Money)
            par2(4) = New SqlParameter("@FlagCustomer", SqlDbType.Char, 1)
            par2(5) = New SqlParameter("@FlagSupplier", SqlDbType.Char, 1)
            par2(6) = New SqlParameter("@Rate", SqlDbType.Decimal)

            For Each oRow As DataRow In oCustomClass.ApplicationDetailTransactionDT.Rows
                par2(0).Value = oCustomClass.BranchId
                par2(1).Value = oCustomClass.ApplicationID
                par2(2).Value = oRow("TransID")
                par2(3).Value = oRow("TransAmount")
                par2(4).Value = oRow("FlagCustomer")
                par2(5).Value = oRow("FlagSupplier")
                par2(6).Value = oRow("Rate")
                SqlHelper.ExecuteNonQuery(Transaction, CommandType.StoredProcedure, "spAgreementDetailTransactionSave2", par2)
            Next

            Transaction.Commit()

        Catch ex As Exception
            Transaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub


    Public Function getBungaNett(ByVal oCustomClass As Parameter.ApplicationDetailTransaction) As Parameter.ApplicationDetailTransaction
        Dim par() As SqlParameter = New SqlParameter(0) {}

        Try
            par(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            par(0).Value = oCustomClass.ApplicationID

            oCustomClass.Tabels = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetBungaNett", par)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function getBungaNettKPR(ByVal oCustomClass As Parameter.ApplicationDetailTransaction) As Parameter.ApplicationDetailTransaction
        Dim par() As SqlParameter = New SqlParameter(0) {}

        Try
            par(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            par(0).Value = oCustomClass.ApplicationID

            oCustomClass.Tabels = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetBungaNettKPR", par)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function getBungaNettOperatingLease(ByVal oCustomClass As Parameter.ApplicationDetailTransaction) As Parameter.ApplicationDetailTransaction
        Dim par() As SqlParameter = New SqlParameter(0) {}

        Try
            par(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            par(0).Value = oCustomClass.ApplicationID

            oCustomClass.Tabels = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetBungaNettOperatingLease", par)
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

End Class
