﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan
#End Region

Public Class FactoringInvoice : Inherits Maxiloan.SQLEngine.DataAccessBase

    Public Function GetInvoiceList(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim reader As SqlDataReader
        Dim oReturnValue As New Parameter.FactoringInvoice
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.ApplicationID


        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spFactoringInvoiceList", params).Tables(0)
            Return oReturnValue

            'reader = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spFactoringInvoiceList", params)
            'If reader.Read Then
            '    oReturnValue.ApplicationID = reader("ApplicationID").ToString
            '    oReturnValue.BranchId = reader("BranchID").ToString
            '    oReturnValue.InvoiceNo = reader("InvoiceNo").ToString
            '    oReturnValue.InvoiceSeqNo = Convert.ToInt32(reader("InvoiceSeqNo").ToString)
            '    oReturnValue.InvoiceDate = reader("InvoiceDate")
            '    oReturnValue.InvoiceDueDate = reader("InvoiceDueDate")
            '    oReturnValue.InvoiceAmount = Convert.ToDecimal(reader("InvoiceAmount").ToString)
            '    oReturnValue.RetensiAmount = Convert.ToDecimal(reader("RetensiAmount").ToString)
            '    oReturnValue.InterestAmount = Convert.ToDecimal(reader("InterestAmount").ToString)
            '    oReturnValue.TotalPembiayaan = Convert.ToDecimal(reader("TotalPembiayaan").ToString)
            '    oReturnValue.HariDenda = CInt(reader("telatHari").ToString)
            '    oReturnValue.AmountDenda = CDbl(reader("AmountDenda").ToString)
            '    oReturnValue.OUtstandingPrincipal = CDbl(reader("OutstandingPrincipal").ToString)
            'End If
            'reader.Close()
            'Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On FactoringInvoice.GetInvoiceList - " & ex.Message)
        End Try
    End Function

    Public Function GetInvoiceDetail(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.FactoringInvoice
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
        params(1).Value = ocustomClass.InvoiceSeqNo
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, "spFactoringInvoiceDetail", params)
            If reader.Read Then
                oReturnValue.ApplicationID = reader("ApplicationID").ToString
                oReturnValue.BranchId = reader("BranchID").ToString
                oReturnValue.InvoiceSeqNo = Convert.ToInt32(reader("InvoiceSeqNo").ToString)
                oReturnValue.InvoiceNo = reader("InvoiceNo").ToString
                oReturnValue.InvoiceDate = Format(reader("InvoiceDate"), "dd/MM/yyyy".ToString)
                oReturnValue.InvoiceDueDate = Format(reader("InvoiceDueDate"), "dd/MM/yyyy".ToString)
                oReturnValue.InvoiceAmount = Convert.ToDecimal(reader("InvoiceAmount").ToString)
                oReturnValue.RetensiAmount = Convert.ToDecimal(reader("RetensiAmount").ToString)
                oReturnValue.InterestAmount = Convert.ToDecimal(reader("InterestAmount").ToString)
                oReturnValue.TotalPembiayaan = Convert.ToDecimal(reader("TotalPembiayaan").ToString)
                oReturnValue.JangkaWaktu = Convert.ToInt32(reader("JangkaWaktu").ToString)
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On FactoringInvoice.GetInvoiceDetail - " & ex.Message)
        End Try
    End Function

    Public Function GetInvoiceDetail2(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim params(2) As SqlParameter
        Dim oReturnValue As New Parameter.FactoringInvoice
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
        params(1).Value = ocustomClass.InvoiceSeqNo
        params(2) = New SqlParameter("@valuedate", SqlDbType.Date)
        params(2).Value = ocustomClass.valuedate

        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, "spFactoringInvoiceDetail2", params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On FactoringInvoice.GetInvoiceDetail - " & ex.Message)
        End Try
    End Function


    Public Function GetPPH(ByVal ocustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.FactoringInvoice
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@SeqNo", SqlDbType.Int)
        params(1).Value = ocustomClass.InvoiceSeqNo
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, "spGetPPH", params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On FactoringInvoice.GetInvoiceDetail - " & ex.Message)
        End Try
    End Function

    Public Function InvoiceSaveAdd(ByVal ocustomClass As Parameter.FactoringInvoice) As String
        Dim ErrMessage As String = ""
        Dim params(11) As SqlParameter

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = ocustomClass.BranchId
        params(2) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.InvoiceNo
        params(3) = New SqlParameter("@InvoiceDate", SqlDbType.DateTime)
        params(3).Value = ocustomClass.InvoiceDate
        params(4) = New SqlParameter("@InvoiceDueDate", SqlDbType.DateTime)
        params(4).Value = ocustomClass.InvoiceDueDate
        params(5) = New SqlParameter("@InvoiceAmount", SqlDbType.Decimal)
        params(5).Value = ocustomClass.InvoiceAmount
        params(6) = New SqlParameter("@RetensiAmount", SqlDbType.Decimal)
        params(6).Value = ocustomClass.RetensiAmount
        params(7) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
        params(7).Value = ocustomClass.InterestAmount
        params(8) = New SqlParameter("@TotalPembiayaan", SqlDbType.Decimal)
        params(8).Value = ocustomClass.TotalPembiayaan
        params(9) = New SqlParameter("@JangkaWaktu", SqlDbType.Int)
        params(9).Value = ocustomClass.JangkaWaktu
        params(10) = New SqlParameter("@UsrUpd", SqlDbType.VarChar, 20)
        params(10).Value = ocustomClass.UsrUpd

        params(11) = New SqlParameter("@Err", SqlDbType.VarChar, 2000)
        params(11).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spFactoringInvoiceSaveAdd", params)
            ErrMessage = CType(params(11).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On FactoringInvoice.InvoiceSaveAdd - " & ex.Message)
        End Try
    End Function

    Public Function InvoiceSaveEdit(ByVal ocustomClass As Parameter.FactoringInvoice) As String
        Dim ErrMessage As String = ""
        Dim params(12) As SqlParameter

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = ocustomClass.BranchId
        params(2) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 20)
        params(2).Value = ocustomClass.InvoiceNo
        params(3) = New SqlParameter("@InvoiceDate", SqlDbType.VarChar, 8)
        params(3).Value = ocustomClass.InvoiceDate
        params(4) = New SqlParameter("@InvoiceDueDate", SqlDbType.VarChar, 8)
        params(4).Value = ocustomClass.InvoiceDueDate
        params(5) = New SqlParameter("@InvoiceAmount", SqlDbType.Decimal)
        params(5).Value = ocustomClass.InvoiceAmount
        params(6) = New SqlParameter("@RetensiAmount", SqlDbType.Decimal)
        params(6).Value = ocustomClass.RetensiAmount
        params(7) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
        params(7).Value = ocustomClass.InterestAmount
        params(8) = New SqlParameter("@TotalPembiayaan", SqlDbType.Decimal)
        params(8).Value = ocustomClass.TotalPembiayaan
        params(9) = New SqlParameter("@JangkaWaktu", SqlDbType.Int)
        params(9).Value = ocustomClass.JangkaWaktu
        params(10) = New SqlParameter("@UsrUpd", SqlDbType.VarChar, 20)
        params(10).Value = ocustomClass.UsrUpd
        params(11) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
        params(11).Value = ocustomClass.InvoiceSeqNo

        params(12) = New SqlParameter("@Err", SqlDbType.VarChar, 2000)
        params(12).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spFactoringInvoiceSaveEdit", params)
            ErrMessage = CType(params(12).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On FactoringInvoice.InvoiceSaveEdit - " & ex.Message)
        End Try
    End Function

    Public Function InvoiceDelete(ByVal ocustomClass As Parameter.FactoringInvoice) As String
        Dim ErrMessage As String = ""
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@InvoiceSeqNo", SqlDbType.Int)
        params(1).Value = ocustomClass.InvoiceSeqNo
        Try
            ErrMessage = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spFactoringInvoiceDelete", params)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function

    Public Function InvoiceSaveApplication(ByVal ocustomClass As Parameter.Application) As String
        Dim ErrMessage As String = ""
        Dim params(1) As SqlParameter

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = ocustomClass.BranchId

        Try
            ErrMessage = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spFactoringInvoiceSaveApplication", params)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On FactoringInvoice.InvoiceSaveApplication - " & ex.Message)
        End Try
    End Function

    Public Function GetFactoringFee(ByVal ocustomClass As Parameter.Application) As Parameter.FactoringInvoice
        Dim ErrMessage As String = ""
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.FactoringInvoice

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = ocustomClass.BranchId

        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, "spFactoringGetFee", params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On FactoringInvoice.GetFactoringFee - " & ex.Message)
        End Try

    End Function

    Public Function FactoringApplicationFinancialSave(ByVal ocustomClass As Parameter.Application) As String
        Dim ErrMessage As String = ""
        Dim params(15) As SqlParameter

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = ocustomClass.BranchId

        params(2) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
        params(2).Value = ocustomClass.InstallmentAmount
        params(3) = New SqlParameter("@FactoringPPNAmount", SqlDbType.Decimal)
        params(3).Value = ocustomClass.FactoringPPNAmount
        params(4) = New SqlParameter("@FactoringDiscountCharges", SqlDbType.Decimal)
        params(4).Value = ocustomClass.FactoringDiscountCharges
        params(5) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
        params(5).Value = ocustomClass.AdminFee
        params(6) = New SqlParameter("@NotaryFee", SqlDbType.Decimal)
        params(6).Value = ocustomClass.NotaryFee
        params(7) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal)
        params(7).Value = ocustomClass.ProvisionFee
        params(8) = New SqlParameter("@SurveyFee", SqlDbType.Decimal)
        params(8).Value = ocustomClass.SurveyFee
        params(9) = New SqlParameter("@HandlingFee", SqlDbType.Decimal)
        params(9).Value = ocustomClass.HandlingFee
        params(10) = New SqlParameter("@FactoringPolisAmount", SqlDbType.Decimal)
        params(10).Value = ocustomClass.FactoringPolisAmount
        params(11) = New SqlParameter("@FactoringPolis", SqlDbType.Decimal)
        params(11).Value = ocustomClass.FactoringPolis
        params(12) = New SqlParameter("@BiayaPolis", SqlDbType.Decimal)
        params(12).Value = ocustomClass.BiayaPolis
        params(13) = New SqlParameter("@PPN", SqlDbType.Decimal)
        params(13).Value = ocustomClass.FactoringPPN
        params(14) = New SqlParameter("@InsuranceComBranch", SqlDbType.NChar, 10)
        params(14).Value = ocustomClass.FactoringInsuranceComID
        params(15) = New SqlParameter("@InsRateCategory", SqlDbType.VarChar, 50)
        params(15).Value = ocustomClass.InsRateCategory

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spFactoringApplicationFinancialSave", params)
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On FactoringInvoice.FactoringApplicationFinancialSave- " & ex.Message)
        End Try
    End Function

    Public Function FactoringInvoiceDisburse(ByVal ocustomClass As Parameter.FactoringInvoice) As String
        Dim Params() As SqlParameter = New SqlParameter(4) {}
        Dim transaction As SqlTransaction = Nothing
        Dim Conn As New SqlConnection(ocustomClass.strConnection)
        Dim oReturnValue As New Parameter.Drawdown

        Try
            If Conn.State = ConnectionState.Closed Then Conn.Open()

            transaction = Conn.BeginTransaction
            Params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            Params(0).Value = ocustomClass.BranchId

            Params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            Params(1).Value = ocustomClass.ApplicationID

            Params(2) = New SqlParameter("@InvoiceSeqNo", SqlDbType.VarChar, 8)
            Params(2).Value = ocustomClass.InvoiceSeqNo

            Params(3) = New SqlParameter("@LoginID", SqlDbType.Char, 20)
            Params(3).Value = ocustomClass.LoginId

            Params(4) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            Params(4).Value = ocustomClass.BusinessDate

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFactoringInvoiceDisburse", Params)

            Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
            Dim oEntitiesApproval As New Parameter.Approval

            With oEntitiesApproval
                .ApprovalTransaction = transaction
                .BranchId = ocustomClass.BranchId
                .SchemeID = "RCA5"
                .RequestDate = ocustomClass.BusinessDate
                .TransactionNo = ocustomClass.InvoiceNo
                .ApprovalNote = ""

                .ApprovalValue = ocustomClass.TotalPembiayaan

                .UserRequest = ocustomClass.LoginId
                .UserApproval = ocustomClass.UserApproval
                .AprovalType = Parameter.Approval.ETransactionType.RCA_Approval
                .Argumentasi = ""
            End With

            ocustomClass.ApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)
            Dim par As IList(Of SqlParameter) = New List(Of SqlParameter)

            par.Add(New SqlParameter("@InvoiceSeqNo", SqlDbType.Int) With {.Value = ocustomClass.InvoiceSeqNo})
            par.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20) With {.Value = ocustomClass.ApplicationID})
            par.Add(New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50) With {.Value = ocustomClass.ApprovalNo})
            par.Add(New SqlParameter("@BusinessDate", SqlDbType.DateTime) With {.Value = ocustomClass.BusinessDate})
            Dim eerParams = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            par.Add(eerParams)
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFactoringInvoiceDisburseProceed", par.ToArray)

            transaction.Commit()

            Return ""

        Catch ex As Exception
            transaction.Rollback()
            Throw New Exception(ex.Message)
            Return ex.Message
        Finally
            If Conn.State = ConnectionState.Open Then Conn.Close()
            Conn.Dispose()
        End Try

    End Function

    Public Function RestruckFactoringPaging(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim params(4) As SqlParameter
        With oCustomClass
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = .SortBy
            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output
            'params(5) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 20)
            'params(5).Value = .AppID

        End With
        Try
            oCustomClass.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spRestruckFactoringPaging", params).Tables(0)
            oCustomClass.TotalRecord = CType(params(4).Value, Int16)
            Return oCustomClass
        Catch exp As Exception
            WriteException("Restruct", "spRestruckFactoringPaging", exp.Message + exp.StackTrace)
            Return Nothing
        End Try
    End Function
    Public Function GetRestructFactoringData(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim reader As SqlDataReader
        Dim params(2) As SqlParameter
        Dim oReturnValue As New Parameter.FactoringInvoice
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.ApplicationID
        params(1) = New SqlParameter("@AgreementNo", SqlDbType.Char, 30)
        params(1).Value = oCustomClass.AgreementNo
        params(2) = New SqlParameter("@InvoiceNo", SqlDbType.Char, 30)
        params(2).Value = oCustomClass.InvoiceNo
        Try
            reader = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spRestructFactoringData", params)
            If reader.Read Then
                oReturnValue.ApplicationID = reader("ApplicationID").ToString
                oReturnValue.BranchId = reader("BranchID").ToString
                oReturnValue.InvoiceNo = reader("InvoiceNo").ToString
                oReturnValue.InvoiceDate = reader("InvoiceDate").ToString
                oReturnValue.InvoiceDueDate = reader("InvoiceDueDate").ToString
                'oReturnValue.InvoiceAmount = Convert.ToDecimal(reader("InvoiceAmount").ToString)
                'oReturnValue.RetensiAmount = Convert.ToDecimal(reader("RetensiAmount").ToString)
                'oReturnValue.InterestAmount = Convert.ToDecimal(reader("InterestAmount").ToString)
                'oReturnValue.TotalPembiayaan = Convert.ToDecimal(reader("TotalPembiayaan").ToString)
                'oReturnValue.JangkaWaktu = Convert.ToInt32(reader("JangkaWaktu").ToString)
                oReturnValue.DrawDownDate = reader("DrawDownDate").ToString
                oReturnValue.EffectiveRate = Convert.ToDecimal(reader("EffectiveRate").ToString)
                oReturnValue.FixInterest = Convert.ToDecimal(reader("InterestFix").ToString)
                'oReturnValue.AccruedInterest = Convert.ToDecimal(reader("AccruedInterest").ToString)
                oReturnValue.Status = reader("Status").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On FactoringInvoice.spRestructFactoringData - " & ex.Message)
        End Try
    End Function

    Public Function RestructFactoringSave(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim ErrMessage As String = ""
        Dim params(3) As SqlParameter
        Dim oReturnValue As New Parameter.FactoringInvoice

        params(0) = New SqlParameter("@AgreementNo", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.AgreementNo
        params(1) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.InvoiceNo
        params(2) = New SqlParameter("@EffectiveRateNew", SqlDbType.Decimal)
        params(2).Value = oCustomClass.EffectiveRateNew
        params(3) = New SqlParameter("@InvoiceDueDateNew", SqlDbType.DateTime)
        params(3).Value = oCustomClass.InvoiceDueDateNew


        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spRestructFactoringSave", params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On FactoringInvoice.RestructFactoringSave " & ex.Message)
        End Try
    End Function
    Public Function GetInvoiceListPaid(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim reader As SqlDataReader
        Dim oReturnValue As New Parameter.FactoringInvoice
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.ApplicationID


        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spFactoringInvoiceListPaid", params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On FactoringInvoice.GetInvoiceList - " & ex.Message)
        End Try
    End Function

    Public Function GetInvoiceChangeDueDateList(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
        Dim reader As SqlDataReader
        Dim oReturnValue As New Parameter.FactoringInvoice
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.ApplicationID


        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spFactoringChangeDueDateList", params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On FactoringInvoice.GetInvoiceList - " & ex.Message)
        End Try
    End Function
    'Public Function GetFinancialData_002(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    '    Dim oReturnValue As New Parameter.FactoringInvoice
    '    Dim params() As SqlParameter = New SqlParameter(1) {}
    '    params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
    '    params(0).Value = oCustomClass.BranchId
    '    params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
    '    params(1).Value = oCustomClass.ApplicationID
    '    Try
    '        oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, oCustomClass.SpName, params).Tables(0)
    '        Return oReturnValue
    '    Catch exp As Exception
    '        WriteException("FinancialData", oCustomClass.SpName, exp.Message + exp.StackTrace)
    '        Dim err As New MaxiloanExceptions
    '        err.WriteLog("FinancialData - DA", oCustomClass.SpName, exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
    '        Throw New Exception(exp.StackTrace)
    '    End Try
    'End Function
    'Public Function SaveFinancialDataTab2End(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    '    Dim oReturnValue As New Parameter.FactoringInvoice
    '    Dim conn As New SqlConnection(oCustomClass.strConnection)

    '    Dim transaction As SqlTransaction = Nothing
    '    Try
    '        If conn.State = ConnectionState.Closed Then conn.Open()
    '        transaction = conn.BeginTransaction

    '        Dim params() As SqlParameter = New SqlParameter(2) {}

    '        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
    '        params(0).Value = oCustomClass.BranchId
    '        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
    '        params(1).Value = oCustomClass.ApplicationID
    '        params(2) = New SqlParameter("@BusDate", SqlDbType.DateTime)
    '        params(2).Value = oCustomClass.BusinessDate

    '        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFinancialDataSaveTab2End", params)

    '        transaction.Commit()
    '        oReturnValue.Output = ""
    '        Return oReturnValue
    '    Catch exp As Exception
    '        transaction.Rollback()
    '        oReturnValue.Output = exp.Message
    '        Return oReturnValue
    '        Throw New Exception(exp.Message)
    '    Finally
    '        If conn.State = ConnectionState.Open Then conn.Close()
    '        conn.Dispose()
    '    End Try
    'End Function
    'Public Function GetRefundPremiumToSupplier(ByVal data As Parameter.Supplier) As Parameter.Supplier
    '    Dim objCommand As New SqlCommand
    '    Dim objConnection As New SqlConnection(data.strConnection)

    '    Try
    '        If objConnection.State = ConnectionState.Closed Then objConnection.Open()
    '        objCommand.CommandType = CommandType.StoredProcedure
    '        objCommand.CommandText = "spSupplierINCTVGetRefundPremiumToSupplier"

    '        objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = data.AppID
    '        objCommand.Parameters.Add("@RefundToSupp", SqlDbType.Decimal).Direction = ParameterDirection.Output
    '        objCommand.Parameters.Add("@BranchID", SqlDbType.Char, 3).Direction = ParameterDirection.Output
    '        objCommand.Parameters.Add("@SupplierID", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output
    '        objCommand.Parameters.Add("@PremiumBaseforRefundSupp", SqlDbType.Decimal).Direction = ParameterDirection.Output
    '        objCommand.Parameters.Add("@RefundPremi", SqlDbType.Char, 10).Direction = ParameterDirection.Output
    '        objCommand.Parameters.Add("@RefundInterest", SqlDbType.Decimal).Direction = ParameterDirection.Output

    '        objCommand.Connection = objConnection
    '        objCommand.ExecuteNonQuery()

    '        data.SupplierID = CStr(IIf(IsDBNull(objCommand.Parameters("@SupplierID").Value), "", objCommand.Parameters("@SupplierID").Value))

    '        Return data
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    Finally
    '        If objConnection.State = ConnectionState.Open Then objConnection.Close()
    '        objConnection.Dispose()
    '        objCommand.Dispose()
    '    End Try

    'End Function
    'Public Function saveBungaNett(oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    '    Dim conn As New SqlConnection(oCustomClass.strConnection)

    '    Try
    '        If conn.State = ConnectionState.Closed Then conn.Open()

    '        Dim par() As SqlParameter = New SqlParameter(2) {}

    '        par(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
    '        par(1) = New SqlParameter("@BungaNettEff", SqlDbType.Decimal)
    '        par(2) = New SqlParameter("@BungaNettFlat", SqlDbType.Decimal)

    '        par(0).Value = oCustomClass.ApplicationID
    '        par(1).Value = oCustomClass.BungaNettEff
    '        par(2).Value = oCustomClass.BungaNettFlat

    '        SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "spBungaNettSave", par)

    '        Return oCustomClass
    '    Catch ex As Exception
    '        Dim err As New MaxiloanExceptions
    '        err.WriteLog(oCustomClass.LoginId & "|" & Date.Now.ToLongTimeString & "|FinancialData.vb", "saveBungaNett", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
    '        Return oCustomClass
    '    End Try
    'End Function
    'Public Function SaveFinancialDataTab2(ByVal oCustomClass As Parameter.FactoringInvoice) As Parameter.FactoringInvoice
    '    Dim oReturnValue As New Parameter.FactoringInvoice
    '    Dim conn As New SqlConnection(oCustomClass.strConnection)

    '    Dim transaction As SqlTransaction = Nothing
    '    Try
    '        If conn.State = ConnectionState.Closed Then conn.Open()
    '        transaction = conn.BeginTransaction

    '        Dim params() As SqlParameter = New SqlParameter(22) {}

    '        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
    '        params(0).Value = oCustomClass.BranchId

    '        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
    '        params(1).Value = oCustomClass.ApplicationID

    '        params(2) = New SqlParameter("@RefundProvisiPercent", SqlDbType.Decimal)
    '        params(2).Value = oCustomClass.RefundProvisiPercent

    '        params(3) = New SqlParameter("@RefundProvisiAmount", SqlDbType.Decimal)
    '        params(3).Value = oCustomClass.RefundProvisiAmount

    '        params(4) = New SqlParameter("@RefundBiayaLain", SqlDbType.Decimal)
    '        params(4).Value = oCustomClass.AlokasiInsentifBiayaLain

    '        params(5) = New SqlParameter("@RefundBiayaLainPercent", SqlDbType.Decimal)
    '        params(5).Value = oCustomClass.AlokasiInsentifBiayaLainPercent

    '        params(6) = New SqlParameter("@RateIRR", SqlDbType.Decimal)
    '        params(6).Value = oCustomClass.RateIRR

    '        params(7) = New SqlParameter("@ExcludeAmount", SqlDbType.Decimal)
    '        params(7).Value = oCustomClass.ExcludeAmount

    '        params(8) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
    '        params(8).Value = oCustomClass.AdminFee

    '        params(9) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal)
    '        params(9).Value = oCustomClass.ProvisionFee

    '        params(10) = New SqlParameter("@SubsidiBunga", SqlDbType.Decimal)
    '        params(10).Value = oCustomClass.SubsidiBunga

    '        params(11) = New SqlParameter("@IncomePremi", SqlDbType.Decimal)
    '        params(11).Value = oCustomClass.IncomePremi

    '        params(12) = New SqlParameter("@OtherFee", SqlDbType.Decimal)
    '        params(12).Value = oCustomClass.OtherFee

    '        params(13) = New SqlParameter("@IncentiveDealer", SqlDbType.Decimal)
    '        params(13).Value = oCustomClass.IncentiveDealer

    '        params(14) = New SqlParameter("@InsuranceRefund", SqlDbType.Decimal)
    '        params(14).Value = oCustomClass.InsuranceRefund

    '        params(15) = New SqlParameter("@ProvisionRefund", SqlDbType.Decimal)
    '        params(15).Value = oCustomClass.ProvisionRefund

    '        params(16) = New SqlParameter("@OtherRefund", SqlDbType.Decimal)
    '        params(16).Value = oCustomClass.OtherRefund

    '        params(17) = New SqlParameter("@RefundPremiCP", SqlDbType.Decimal)
    '        params(17).Value = oCustomClass.RefundCreditProtectionAmount

    '        params(18) = New SqlParameter("@RefundPremiCPPercent", SqlDbType.Decimal)
    '        params(18).Value = oCustomClass.RefundCreditProtectionPercent

    '        params(19) = New SqlParameter("@AdminFeeMf", SqlDbType.Decimal)
    '        params(19).Value = oCustomClass.AdminFeeMf

    '        params(20) = New SqlParameter("@OtherFeeMf", SqlDbType.Decimal)
    '        params(20).Value = oCustomClass.OtherFeeMf

    '        params(21) = New SqlParameter("@ProvisionFeeMf", SqlDbType.Decimal)
    '        params(21).Value = oCustomClass.ProvisionFeeMf

    '        params(22) = New SqlParameter("@PaidAmountByCustMF", SqlDbType.Decimal)
    '        params(22).Value = oCustomClass.AsuransiTunaiMF


    '        'params(31) = New SqlParameter("@SubsidiRisk", SqlDbType.Decimal)
    '        'params(31).Value = oCustomClass.SubsidiRisk
    '        'params(32) = New SqlParameter("@SubsidiRiskATPM", SqlDbType.Decimal)
    '        'params(32).Value = oCustomClass.SubsidiRiskATPM

    '        'params(35) = New SqlParameter("@RefundPremiJK", SqlDbType.Decimal)
    '        'params(35).Value = oCustomClass.RefundJaminanKreditAmount
    '        'params(36) = New SqlParameter("@RefundPremiJKPercent", SqlDbType.Decimal)
    '        'params(36).Value = oCustomClass.RefundJaminanKreditPercent
    '        'params(37) = New SqlParameter("@RefundUppingBunga", SqlDbType.Decimal)
    '        'params(37).Value = oCustomClass.RefundUppingBungaAmount
    '        'params(38) = New SqlParameter("@RefundUppingBungaPercent", SqlDbType.Decimal)
    '        'params(38).Value = oCustomClass.RefundUppingBungaPercent
    '        'params(39) = New SqlParameter("@SubsidiUppingBunga", SqlDbType.Decimal)
    '        'params(39).Value = oCustomClass.SubsidiDPUppingBungaAmount
    '        'params(40) = New SqlParameter("@SubsidiUppingBungaPercent", SqlDbType.Decimal)
    '        'params(40).Value = oCustomClass.SubsidiDPUppingBungaPercent

    '        'params(41) = New SqlParameter("@DownPaymentMf", SqlDbType.Decimal)
    '        'params(41).Value = oCustomClass.DownPaymentMf

    '        'params(43) = New SqlParameter("@FiduciaFeeMf", SqlDbType.Decimal)
    '        'params(43).Value = oCustomClass.FiduciaFeeMf
    '        'params(2) = New SqlParameter("@SubsidiBungaATPM", SqlDbType.Decimal)
    '        'params(2).Value = oCustomClass.SubsidiBungaATPM
    '        'params(3) = New SqlParameter("@SubsidiAngsuranATPM", SqlDbType.Decimal)
    '        'params(3).Value = oCustomClass.SubsidiAngsuranATPM

    '        'params(4) = New SqlParameter("@SubsidiUangMuka", SqlDbType.Decimal)
    '        'params(4).Value = oCustomClass.SubsidiUangMuka
    '        'params(5) = New SqlParameter("@SubsidiUangMukaATPM", SqlDbType.Decimal)
    '        'params(5).Value = oCustomClass.SubsidiUangMukaATPM
    '        'params(6) = New SqlParameter("@DealerDiscount", SqlDbType.Decimal)
    '        'params(6).Value = oCustomClass.DealerDiscount
    '        'params(7) = New SqlParameter("@BiayaMaintenance", SqlDbType.Decimal)
    '        'params(7).Value = oCustomClass.BiayaMaintenance
    '        'params(8) = New SqlParameter("@RefundBungaPercent", SqlDbType.Decimal)
    '        'params(8).Value = oCustomClass.RefundBungaPercent

    '        'params(9) = New SqlParameter("@RefundBungaAmount", SqlDbType.Decimal)
    '        'params(9).Value = oCustomClass.RefundBungaAmount
    '        'params(10) = New SqlParameter("@RefundPremiPercent", SqlDbType.Decimal)
    '        'params(10).Value = oCustomClass.RefundPremiPercent
    '        'params(11) = New SqlParameter("@RefundPremiAmount", SqlDbType.Decimal)
    '        'params(11).Value = oCustomClass.RefundPremiAmount

    '        'params(47) = New SqlParameter("@InstallmentAmountFirstMF", SqlDbType.Decimal)
    '        'params(47).Value = oCustomClass.AngsuranPertamaMf

    '        'params(48) = New SqlParameter("@DPKaroseriAmountMF", SqlDbType.Decimal)
    '        'params(48).Value = oCustomClass.DPKaroseriAmountMF

    '        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFinancialDataSaveTab2FACT", params)

    '        transaction.Commit()
    '        oReturnValue.Output = ""
    '        Return oReturnValue
    '    Catch exp As Exception
    '        transaction.Rollback()
    '        oReturnValue.Output = exp.Message
    '        Return oReturnValue
    '        Throw New Exception(exp.Message)
    '    Finally
    '        If conn.State = ConnectionState.Open Then conn.Close()
    '        conn.Dispose()
    '    End Try
    'End Function
End Class
