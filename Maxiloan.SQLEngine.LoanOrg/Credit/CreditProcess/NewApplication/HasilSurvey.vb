﻿Option Strict On
Imports Maxiloan.Framework.SQLEngine
Imports System.Data.SqlClient

Public Class HasilSurvey : Inherits Maxiloan.SQLEngine.DataAccessBase

    Public Function getHasilSurvey(ByVal ocustomClass As Parameter.HasilSurvey) As Parameter.HasilSurvey
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.HasilSurvey
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = ocustomClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = ocustomClass.ApplicationID
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, "spGetHasilSurvey", params)
            If reader.Read Then
                With oReturnValue
                    .DateEntryHasilSurvey = CDate(reader("DateEntryHasilSurvey").ToString)
                    .DateEntryCharacter = CDate(reader("DateEntryCharacter").ToString)
                    .DateEntryCapacity = CDate(reader("DateEntryCapacity").ToString)
                    .DateEntryCondition = CDate(reader("DateEntryCondition").ToString)
                    .DateEntryCapital = CDate(reader("DateEntryCapital").ToString)
                    .DateEntryCollateral = CDate(reader("DateEntryCollateral").ToString)
                    .DateEntryKYC = CDate(reader("DateEntryKYC").ToString)
                    .DateEntryCatatan = CDate(reader("DateEntryCatatan").ToString)
                    .AgreementSurveyDate = CDate(reader("SurveyDate").ToString)
                    .SurveyorID = reader("SurveyorID").ToString
                    .CAID = reader("CAID").ToString
                    .SaldoRataRata = CDbl(reader("SaldoRataRata").ToString)
                    .SaldoAwal = CDbl(reader("SaldoAwal").ToString)
                    .SaldoAkhir = CDbl(reader("SaldoAkhir").ToString)
                    .JumlahPemasukan = CDbl(reader("JumlahPemasukan").ToString)
                    .JumlahPengeluaran = CDbl(reader("JumlahPengeluaran").ToString)
                    .JumlahHariTransaksi = CInt(reader("JumlahHariTransaksi").ToString)
                    .SurveyorNotes = reader("SurveyorNotes").ToString
                    .JenisRekening = reader("BankAccountType").ToString
                    .LamaSurvey = CShort(reader("LamaSurvey"))
                    .JumlahKendaraan = CInt(reader("JumlahKendaraan").ToString)
                    .Garasi = CBool(reader("Garasi").ToString)
                    .PertamaKredit = CStr(reader("PertamaKredit").ToString)
                    .OrderKe = CInt(reader("OrderKe").ToString)
                    .AgreementSurveyConfirmationDate = CDate(reader("SurveyConfirmationDate"))
                    .creditScore = CDec(reader("creditScore"))
                    .creditScoringResult = reader("creditScoringResult").ToString
                    .AnalisaNasabah = reader("AnalisaNasabah").ToString
                    .AnalisaTempatTinggal = reader("AnalisaTempatTinggal").ToString
                    .AnalisaKendaraan = reader("AnalisaKendaraan").ToString
                    .isProceed = CBool(reader("isProceed").ToString)
                    .CaraSurvey = reader("CaraSurvey").ToString
                    .SurveyAreaPhone1 = reader("SurveyAreaPhone1").ToString
                    .SurveyPhone1 = reader("SurveyPhone1").ToString
                    .SurveyAreaPhone2 = reader("SurveyAreaPhone2").ToString
                    .SurveyPhone2 = reader("SurveyPhone2").ToString
                    .SurveyCellphone = reader("SurveyCellphone").ToString
                    .EmergencyPhoneContact = reader("EmergencyPhoneContact").ToString
                    .PihakYangTurutSurvey = reader("PihakYangTurutSurvey").ToString
                    .AlasanPihakYangTurutSurvey = reader("AlasanPihakYangTurutSurvey").ToString
                    .LokasiSurvey = reader("LokasiSurvey").ToString
                    .AlasanLokasiSurvey = reader("AlasanLokasiSurvey").ToString
                    .LokasiSurveySesuaiKTP = reader("LokasiSurveySesuaiKTP").ToString
                    .AlasanLokasiSurveySesuaiKTP = reader("AlasanLokasiSurveySesuaiKTP").ToString
                    .WaktuSurvey1 = CDate(reader("WaktuSurvey1").ToString)
                    .WaktuSurvey2 = CDate(reader("WaktuSurvey2").ToString)
                    .CLNama1 = reader("CLNama1").ToString
                    .CLDomisili1 = reader("CLDomisili1").ToString
                    .CLLamaTinggal1 = reader("CLLamaTinggal1").ToString
                    .CLLamaUsaha1 = reader("CLLamaUsaha1").ToString
                    .CLNama2 = reader("CLNama2").ToString
                    .CLDomisili2 = reader("CLDomisili2").ToString
                    .CLLamaTinggal2 = reader("CLLamaTinggal2").ToString
                    .CLLamaUsaha2 = reader("CLLamaUsaha2").ToString
                    .CLNama3 = reader("CLNama3").ToString
                    .CLDomisili3 = reader("CLDomisili3").ToString
                    .CLLamaTinggal3 = reader("CLLamaTinggal3").ToString
                    .CLLamaUsaha3 = reader("CLLamaUsaha3").ToString
                    .HasilCekLkgAspekSosial = reader("HasilCekLkgAspekSosial").ToString
                    .PengalamanKredit = reader("PengalamanKredit").ToString
                    .Pembayaran = reader("Pembayaran").ToString
                    .BuktiBayar = reader("BuktiBayar").ToString
                    .PekerjaanSekarang = reader("PekerjaanSekarang").ToString
                    .LamanyaPekerjaanSekarang = reader("LamanyaPekerjaanSekarang").ToString
                    .PekerjaanSebelumnya = reader("PekerjaanSebelumnya").ToString
                    .LamanyaPekerjaanSebelumnya = reader("LamanyaPekerjaanSebelumnya").ToString
                    .Pendidikan = reader("Pendidikan").ToString
                    .NamaYangMenemaniSurvey1 = reader("NamaYangMenemaniSurvey1").ToString
                    .Hubungan1 = reader("Hubungan1").ToString
                    .NamaYangMenemaniSurvey2 = reader("NamaYangMenemaniSurvey2").ToString
                    .Hubungan2 = reader("Hubungan2").ToString
                    .HubunganSalesDgnPemohon = reader("HubunganSalesDgnPemohon").ToString
                    .AnalisaCharacter = reader("AnalisaCharacter").ToString
                    .LamaUsaha = reader("LamaUsaha").ToString
                    .NamaUsaha = reader("NamaUsaha").ToString
                    .AlamatUsaha1 = reader("AlamatUsaha1").ToString
                    .AlamatUsaha2 = reader("AlamatUsaha2").ToString
                    .FotoTempatUsaha = reader("FotoTempatUsaha").ToString
                    .AlasanFotoTempatUsaha = reader("AlasanFotoTempatUsaha").ToString
                    .StockBarangDagangan = reader("StockBarangDagangan").ToString
                    .AlasanStockBarangDagangan = reader("AlasanStockBarangDagangan").ToString
                    .BonBuktiUsaha = reader("BonBuktiUsaha").ToString
                    .AlasanBonBuktiUsaha = reader("AlasanBonBuktiUsaha").ToString
                    .SKUNo = reader("SKUNo").ToString
                    .TanggalSKU = CDate(reader("TanggalSKU").ToString)
                    .SIUPNo = reader("SIUPNo").ToString
                    .TanggalSIUP = CDate(reader("TanggalSIUP").ToString)
                    .TDPNo = reader("TDPNo").ToString
                    .TanggalTDPNo = CDate(reader("TanggalTDPNo").ToString)
                    .SKDPNo = reader("SKDPNo").ToString
                    .TanggalSKDP = CDate(reader("TanggalSKDP").ToString)
                    .SlipGaji = reader("SlipGaji").ToString
                    .AlasanSlipGaji = reader("AlasanSlipGaji").ToString
                    .SKPenghasilan = reader("SKPenghasilan").ToString
                    .AlasanSKPenghasilan = reader("AlasanSKPenghasilan").ToString
                    .Jabatan = reader("Jabatan").ToString
                    .NPWPPribadi = reader("NPWPPribadi").ToString
                    .NPWPNo = reader("NPWPNo").ToString
                    .UraianUsahaPekerjaan = reader("UraianUsahaPekerjaan").ToString
                    .AlurUsaha = reader("AlurUsaha").ToString
                    .KapasitasKonsumen = reader("KapasitasKonsumen").ToString
                    .AnalisaCapacity = reader("AnalisaCapacity").ToString
                    .TotalPenghasilan1 = CDbl(reader("TotalPenghasilan1").ToString)
                    .TotalPenghasilan2 = CDbl(reader("TotalPenghasilan2").ToString)
                    .BiayaUsaha1 = CDbl(reader("BiayaUsaha1").ToString)
                    .BiayaUsaha2 = CDbl(reader("BiayaUsaha2").ToString)
                    .BiayaHidup1 = CDbl(reader("BiayaHidup1").ToString)
                    .BiayaHidup2 = CDbl(reader("BiayaHidup2").ToString)
                    .BiayaCicilan1 = CDbl(reader("BiayaCicilan1").ToString)
                    .BiayaCicilan2 = CDbl(reader("BiayaCicilan2").ToString)
                    .SisaSebelumTambahUnit1 = CDbl(reader("SisaSebelumTambahUnit1").ToString)
                    .SisaSebelumTambahUnit2 = CDbl(reader("SisaSebelumTambahUnit2").ToString)
                    .IncomeDariUnit1 = CDbl(reader("IncomeDariUnit1").ToString)
                    .IncomeDariUnit2 = CDbl(reader("IncomeDariUnit2").ToString)
                    .EstimasiAngsuran1 = CDbl(reader("EstimasiAngsuran1").ToString)
                    .EstimasiAngsuran2 = CDbl(reader("EstimasiAngsuran2").ToString)
                    .SisaPendapatan1 = CDbl(reader("SisaPendapatan1").ToString)
                    .SisaPendapatan2 = CDbl(reader("SisaPendapatan2").ToString)
                    .RekeningTabunganGio = reader("RekeningTabunganGio").ToString
                    .NamaBank = reader("NamaBank").ToString
                    .LaporanKeuangan = reader("LaporanKeuangan").ToString
                    .AlasanLaporanKeuangan = reader("AlasanLaporanKeuangan").ToString
                    .KreditdariBankLKNB = reader("KreditdariBankLKNB").ToString
                    .NamaInstitusi1 = reader("NamaInstitusi1").ToString
                    .Angsuran1 = CDbl(reader("Angsuran1").ToString)
                    .Sisa1 = CDbl(reader("Sisa1").ToString)
                    .NamaInstitusi2 = reader("NamaInstitusi2").ToString
                    .Angsuran2 = CDbl(reader("Angsuran2").ToString)
                    .Sisa2 = CDbl(reader("Sisa2").ToString)
                    .BuktiPembayaranAngsuran = reader("BuktiPembayaranAngsuran").ToString
                    .AlasanBuktiPembayaranAngsuran = reader("AlasanBuktiPembayaranAngsuran").ToString
                    .AnalisaCondition = reader("AnalisaCondition").ToString
                    .StatusKepemilikan = reader("StatusKepemilikan").ToString
                    .AtasNama = reader("AtasNama").ToString
                    .PBBAJBSHMatasNama = reader("PBBAJBSHMatasNama").ToString
                    .TahunTagihanPBBAJBSHM = reader("TahunTagihanPBBAJBSHM").ToString
                    .PBBAJBSHM = reader("PBBAJBSHM").ToString
                    .AlasanPBBAJBSHM = reader("AlasanPBBAJBSHM").ToString
                    .RekListrikAtasNama = reader("RekListrikAtasNama").ToString
                    .BulanTagihanRekListrik = reader("BulanTagihanRekListrik").ToString
                    .RekListrik = reader("RekListrik").ToString
                    .AlasanRekListrik = reader("AlasanRekListrik").ToString
                    .LuasTanah = reader("LuasTanah").ToString
                    .LuasBangunan = reader("LuasBangunan").ToString
                    .TaksiranNilaiJual = CDbl(reader("TaksiranNilaiJual").ToString)
                    .LamaTinggal = reader("LamaTinggal").ToString
                    .AssetLain = reader("AssetLain").ToString
                    .FotoRumah = reader("FotoRumah").ToString
                    .AlasanFotoRumah = reader("AlasanFotoRumah").ToString
                    .KondisiBangunan = reader("KondisiBangunan").ToString
                    .DayaListrik = reader("DayaListrik").ToString
                    .Hubling = reader("Hubling").ToString
                    .NoKTPPemohon = reader("NoKTPPemohon").ToString
                    .TanggalKTPPemohon = CDate(reader("TanggalKTPPemohon").ToString)
                    .KTPPemohon = reader("KTPPemohon").ToString
                    .AlasanKTPPemohon = reader("AlasanKTPPemohon").ToString
                    .NoKTPPasangan = reader("NoKTPPasangan").ToString
                    .TanggalNoKTPPasangan = CDate(reader("TanggalNoKTPPasangan").ToString)
                    .KTPPasangan = reader("KTPPasangan").ToString
                    .AlasanKTPPasangan = reader("AlasanKTPPasangan").ToString
                    .NoKK = reader("NoKK").ToString
                    .TanggalNoKartuKeluarga = CDate(reader("TanggalNoKartuKeluarga").ToString)
                    .BukuNikah = reader("BukuNikah").ToString
                    .AlasanBukuNikah = reader("AlasanBukuNikah").ToString
                    .JumlahTanggungan = reader("JumlahTanggungan").ToString
                    .AnalisaCapital = reader("AnalisaCapital").ToString
                    .KendaraanYangDiajukan = reader("KendaraanYangDiajukan").ToString
                    .AlasanPembelian = reader("AlasanPembelian").ToString
                    .LokasiPenggunaan = reader("LokasiPenggunaan").ToString
                    .PencocokanSTNKdenganFisik = reader("PencocokanSTNKdenganFisik").ToString
                    .BPKBDiKeluarkanOleh = reader("BPKBDiKeluarkanOleh").ToString
                    .JumlahAssetLain = reader("JumlahAssetLain").ToString
                    .JenisAssetLain = reader("JenisAssetLain").ToString
                    .StatusAssetLainLunas = reader("StatusAssetLainLunas").ToString
                    .StatusAssetLainTidakLunas = reader("StatusAssetLainTidakLunas").ToString
                    .BPKBNo = reader("BPKBNo").ToString
                    .NamaBPKB = reader("NamaBPKB").ToString
                    .BPKBDiperlihatkan = reader("BPKBDiperlihatkan").ToString
                    .AlasanBPKBDiperlihatkan = reader("AlasanBPKBDiperlihatkan").ToString
                    .AnalisaCollateral = reader("AnalisaCollateral").ToString
                    .KunjunganRumahBertemu = reader("KunjunganRumahBertemu").ToString
                    .KondisiRumah = reader("KondisiRumah").ToString
                    .PerabotanRumah = reader("PerabotanRumah").ToString
                    .GarasiMobil = reader("GarasiMobil").ToString
                    .GarasiUntukBerapaUnit = reader("GarasiUntukBerapaUnit").ToString
                    .AlasanGarasiUntukBerapaUnit = reader("AlasanGarasiUntukBerapaUnit").ToString
                    .LokasiRumah = reader("LokasiRumah").ToString
                    .JalanMasukDilalui = reader("JalanMasukDilalui").ToString
                    .KondisiJalanDepanRumah = reader("KondisiJalanDepanRumah").ToString
                    .KeadaanLingkungan = reader("KeadaanLingkungan").ToString
                    .KunjunganKantorBertemu = reader("KunjunganKantorBertemu").ToString
                    .JumlahKaryawan = reader("JumlahKaryawan").ToString
                    .LokasiTempatUsaha = reader("LokasiTempatUsaha").ToString
                    .Alasan = reader("Alasan").ToString
                    .AktivitasTempatUsaha = reader("AktivitasTempatUsaha").ToString
                    .AlasanAktivitasTempatUsaha = reader("AlasanAktivitasTempatUsaha").ToString
                    .TBOList = reader("TBOList").ToString
                    .UsulanLain = reader("UsulanLain").ToString
                    .AnalisaKYC = reader("AnalisaKYC").ToString
                    .AreaPhoneHome = reader("SurveyAreaPhone1").ToString
                    .PhoneHome = reader("SurveyPhone1").ToString
                    .AreaPhoneOffice = reader("SurveyAreaPhone2").ToString
                    .PhoneOffice = reader("SurveyPhone2").ToString
                    '.EmergencyPhoneContact = reader("EmergencyPhoneContact").ToString
                    '.MobilePhone = reader("SurveyCellphone").ToString
                    .Integritas = reader("Integritas").ToString
                    .KondisiKesehatan = reader("KondisiKesehatan").ToString
                    .JenisPekerjaan = reader("JenisPekerjaan").ToString
                    .RatioAngsuran = CDbl(reader("RatioAngsuran").ToString)
                    .IndustryRisk = reader("IndustryRisk").ToString
                    .UsiaPemohon = reader("UsiaPemohon").ToString
                    .LamaTinggal = reader("LamaTinggal").ToString
                    .DP = CDbl(reader("DP").ToString)
                    .ManufacturerCountry = reader("ManufacturerCountry").ToString
                    .AssetType = reader("AssetType").ToString
                    .UsiaKendaraan = reader("UsiaKendaraan").ToString
                    .NTF = CDbl(reader("NTF").ToString)
                End With
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub HasilSurveySave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        Try
            Dim par() As SqlParameter = New SqlParameter(14) {}

            par(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par(0).Value = oCustomClass.BranchId
            par(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            par(1).Value = oCustomClass.ApplicationID
            par(2) = New SqlParameter("@SurveyDate", SqlDbType.SmallDateTime)
            par(2).Value = oCustomClass.AgreementSurveyDate
            par(3) = New SqlParameter("@SurveyorID", SqlDbType.Char, 20)
            par(3).Value = oCustomClass.SurveyorID
            par(4) = New SqlParameter("@CAID", SqlDbType.Char, 20)
            par(4).Value = oCustomClass.CAID
            par(5) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            par(5).Value = oCustomClass.BusinessDate
            par(6) = New SqlParameter("@SurveyorNotes", SqlDbType.Text)
            par(6).Value = oCustomClass.SurveyorNotes
            par(7) = New SqlParameter("@LamaSurvey", SqlDbType.SmallInt)
            par(7).Value = oCustomClass.LamaSurvey
            par(8) = New SqlParameter("@JumlahKendaraan", SqlDbType.SmallInt)
            par(8).Value = oCustomClass.JumlahKendaraan
            par(9) = New SqlParameter("@Garasi", SqlDbType.Bit)
            par(9).Value = oCustomClass.Garasi
            par(10) = New SqlParameter("@PertamaKredit", SqlDbType.Char, 2)
            par(10).Value = oCustomClass.PertamaKredit
            par(11) = New SqlParameter("@OrderKe", SqlDbType.SmallInt)
            par(11).Value = oCustomClass.OrderKe

            par(12) = New SqlParameter("@AnalisaNasabah", SqlDbType.Text)
            par(12).Value = oCustomClass.AnalisaNasabah
            par(13) = New SqlParameter("@AnalisaTempatTinggal", SqlDbType.Text)
            par(13).Value = oCustomClass.AnalisaTempatTinggal
            par(14) = New SqlParameter("@AnalisaKendaraan", SqlDbType.Text)
            par(14).Value = oCustomClass.AnalisaKendaraan

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spHasilSurveySave", par)

            Dim parJournal() As SqlParameter = New SqlParameter(2) {}

            parJournal(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            parJournal(0).Value = oCustomClass.ApplicationID
            parJournal(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            parJournal(1).Value = oCustomClass.BranchId
            parJournal(2) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            parJournal(2).Value = oCustomClass.BusinessDate
                    
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGoliveCrJournalDummy", parJournal).Tables(0)

            'simpan koreksi
            If oCustomClass.isKoreksi Then

                Dim parKoreksi() As SqlParameter = New SqlParameter(3) {}

                parKoreksi(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                parKoreksi(0).Value = oCustomClass.ApplicationID
                parKoreksi(1) = New SqlParameter("@BranchId", SqlDbType.VarChar, 50)
                parKoreksi(1).Value = oCustomClass.BranchId
                parKoreksi(2) = New SqlParameter("@CorrectionDesc", SqlDbType.VarChar, 200)
                parKoreksi(2).Value = oCustomClass.alasanKoreksi
                parKoreksi(3) = New SqlParameter("@Modul", SqlDbType.VarChar, 200)
                parKoreksi(3).Value = "SURVEY"

                SqlHelper.ExecuteNonQuery(oTransaction, CommandType.StoredProcedure, "spAppCorrectionLogSave", parKoreksi)
            End If
            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Function GetApplicationReject(ByVal oCustomClass As Parameter.HasilSurvey) As Parameter.HasilSurvey
        Dim oReturnValue As New Parameter.HasilSurvey
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spApplicationRejectPaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub ApplicationRejectUpdate(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        Try
            Dim par() As SqlParameter = New SqlParameter(3) {}

            par(0) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            par(0).Value = oCustomClass.BusinessDate
            par(1) = New SqlParameter("@RejectionReasonID", SqlDbType.VarChar, 10)
            par(1).Value = oCustomClass.RejectionReasonID
            par(2) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            par(2).Value = oCustomClass.ApplicationID
            par(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par(3).Value = oCustomClass.BranchId

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spApplicationRejectUpdate", par)

            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub HasilSurveyJournalSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        Try
            Dim parJournal() As SqlParameter = New SqlParameter(2) {}

            parJournal(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            parJournal(0).Value = oCustomClass.ApplicationID
            parJournal(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            parJournal(1).Value = oCustomClass.BranchId
            parJournal(2) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            parJournal(2).Value = oCustomClass.BusinessDate

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGoliveCrJournalDummy", parJournal).Tables(0)

            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Public Sub Proceed(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction
        Dim par() As SqlParameter = New SqlParameter(2) {}

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        par(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        par(0).Value = oCustomClass.ApplicationID
        par(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
        par(1).Value = oCustomClass.BranchId
        par(2) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
        par(2).Value = oCustomClass.BusinessDate
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spProceedApplication", par)
            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub
    'Wira 20150824
    Public Sub HasilSurvey002Save(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        Try
            Dim par() As SqlParameter = New SqlParameter(13) {}

            par(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par(0).Value = oCustomClass.BranchId
            par(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            par(1).Value = oCustomClass.ApplicationID
            par(2) = New SqlParameter("@SurveyDate", SqlDbType.SmallDateTime)
            par(2).Value = oCustomClass.SurveyDt
            par(3) = New SqlParameter("@SurveyorID", SqlDbType.Char, 20)
            par(3).Value = oCustomClass.SurveyorID
            par(4) = New SqlParameter("@CAID", SqlDbType.Char, 20)
            par(4).Value = oCustomClass.CAID
            par(5) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            par(5).Value = oCustomClass.BusinessDate
            par(6) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
            par(6).Value = oCustomClass.CustomerIDSurvey
            par(7) = New SqlParameter("@CaraSurvey", SqlDbType.Char, 1)
            par(7).Value = oCustomClass.CaraSurvey
            par(8) = New SqlParameter("@SurveyAreaPhone1", SqlDbType.Char, 4)
            par(8).Value = oCustomClass.SurveyAreaPhone1
            par(9) = New SqlParameter("@SurveyPhone1", SqlDbType.Char, 10)
            par(9).Value = oCustomClass.SurveyPhone1
            par(10) = New SqlParameter("@SurveyAreaPhone2", SqlDbType.Char, 4)
            par(10).Value = oCustomClass.SurveyAreaPhone2
            par(11) = New SqlParameter("@SurveyPhone2", SqlDbType.Char, 10)
            par(11).Value = oCustomClass.SurveyPhone2
            par(12) = New SqlParameter("@SurveyCellphone", SqlDbType.Char, 15)
            par(12).Value = oCustomClass.SurveyCellphone
            par(13) = New SqlParameter("@EmergencyPhoneContact", SqlDbType.Char, 15)
            par(13).Value = oCustomClass.EmergencyPhoneContact

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spHasilSurvey_002Save", par)

            'Dim parJournal() As SqlParameter = New SqlParameter(2) {}

            'parJournal(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            'parJournal(0).Value = oCustomClass.ApplicationID
            'parJournal(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            'parJournal(1).Value = oCustomClass.BranchId
            'parJournal(2) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            'parJournal(2).Value = oCustomClass.BusinessDate

            'oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGoliveCrJournalDummy", parJournal).Tables(0)

            'simpan koreksi
            'If oCustomClass.isKoreksi Then

            '    Dim parKoreksi() As SqlParameter = New SqlParameter(3) {}

            '    parKoreksi(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            '    parKoreksi(0).Value = oCustomClass.ApplicationID
            '    parKoreksi(1) = New SqlParameter("@BranchId", SqlDbType.VarChar, 50)
            '    parKoreksi(1).Value = oCustomClass.BranchId
            '    parKoreksi(2) = New SqlParameter("@CorrectionDesc", SqlDbType.VarChar, 200)
            '    parKoreksi(2).Value = oCustomClass.alasanKoreksi
            '    parKoreksi(3) = New SqlParameter("@Modul", SqlDbType.VarChar, 200)
            '    parKoreksi(3).Value = "SURVEY"

            '    SqlHelper.ExecuteNonQuery(oTransaction, CommandType.StoredProcedure, "spAppCorrectionLogSave", parKoreksi)
            'End If
            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub HasilSurvey002CharacterSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        Try
            Dim par() As SqlParameter = New SqlParameter(26) {}

            par(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par(0).Value = oCustomClass.BranchId
            par(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            par(1).Value = oCustomClass.ApplicationID
            par(2) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
            par(2).Value = oCustomClass.CustomerIDSurvey
            par(3) = New SqlParameter("@PihakYangTurutSurvey", SqlDbType.VarChar, 10)
            par(3).Value = oCustomClass.PihakYangTurutSurvey
            par(4) = New SqlParameter("@AlasanPihakYangTurutSurvey", SqlDbType.VarChar, 50)
            par(4).Value = oCustomClass.AlasanPihakYangTurutSurvey
            par(5) = New SqlParameter("@LokasiSurvey", SqlDbType.Char, 1)
            par(5).Value = oCustomClass.LokasiSurvey
            par(6) = New SqlParameter("@AlasanLokasiSurvey", SqlDbType.VarChar, 50)
            par(6).Value = oCustomClass.AlasanLokasiSurvey
            par(7) = New SqlParameter("@LokasiSurveySesuaiKTP", SqlDbType.Char, 1)
            par(7).Value = oCustomClass.LokasiSurveySesuaiKTP
            par(8) = New SqlParameter("@AlasanLokasiSurveySesuaiKTP", SqlDbType.VarChar, 50)
            par(8).Value = oCustomClass.AlasanLokasiSurveySesuaiKTP
            par(9) = New SqlParameter("@WaktuSurvey1", SqlDbType.Date)
            par(9).Value = oCustomClass.WaktuSurvey1
            par(10) = New SqlParameter("@WaktuSurvey2", SqlDbType.Date)
            par(10).Value = oCustomClass.WaktuSurvey2
            par(11) = New SqlParameter("@HasilCekLkgAspekSosial", SqlDbType.VarChar, 15)
            par(11).Value = oCustomClass.HasilCekLkgAspekSosial
            par(12) = New SqlParameter("@PengalamanKredit", SqlDbType.Char, 1)
            par(12).Value = oCustomClass.PengalamanKredit
            par(13) = New SqlParameter("@Pembayaran", SqlDbType.VarChar, 20)
            par(13).Value = oCustomClass.Pembayaran
            par(14) = New SqlParameter("@BuktiBayar", SqlDbType.Char, 1)
            par(14).Value = oCustomClass.BuktiBayar
            par(15) = New SqlParameter("@PekerjaanSekarang", SqlDbType.VarChar, 50)
            par(15).Value = oCustomClass.PekerjaanSekarang
            par(16) = New SqlParameter("@LamanyaPekerjaanSekarang", SqlDbType.Char, 2)
            par(16).Value = oCustomClass.LamanyaPekerjaanSekarang
            par(17) = New SqlParameter("@PekerjaanSebelumnya", SqlDbType.VarChar, 50)
            par(17).Value = oCustomClass.PekerjaanSebelumnya
            par(18) = New SqlParameter("@LamanyaPekerjaanSebelumnya", SqlDbType.VarChar, 2)
            par(18).Value = oCustomClass.LamanyaPekerjaanSebelumnya
            par(19) = New SqlParameter("@Pendidikan", SqlDbType.VarChar, 10)
            par(19).Value = oCustomClass.Pendidikan
            par(20) = New SqlParameter("@NamaYangMenemaniSurvey1", SqlDbType.VarChar, 50)
            par(20).Value = oCustomClass.NamaYangMenemaniSurvey1
            par(21) = New SqlParameter("@Hubungan1", SqlDbType.VarChar, 50)
            par(21).Value = oCustomClass.Hubungan1
            par(22) = New SqlParameter("@NamaYangMenemaniSurvey2", SqlDbType.VarChar, 50)
            par(22).Value = oCustomClass.NamaYangMenemaniSurvey2
            par(23) = New SqlParameter("@Hubungan2", SqlDbType.VarChar, 50)
            par(23).Value = oCustomClass.Hubungan2
            par(24) = New SqlParameter("@HubunganSalesDgnPemohon", SqlDbType.VarChar, 10)
            par(24).Value = oCustomClass.HubunganSalesDgnPemohon
            par(25) = New SqlParameter("@AnalisaCharacter", SqlDbType.Text)
            par(25).Value = oCustomClass.AnalisaCharacter
            par(26) = New SqlParameter("@Integritas", SqlDbType.VarChar, 15)
            par(26).Value = oCustomClass.Integritas

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spHasilSurvey_002CharacterSave", par)

            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub HasilSurvey002CapacitySave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        Try
            Dim par() As SqlParameter = New SqlParameter(31) {}

            par(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par(0).Value = oCustomClass.BranchId
            par(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            par(1).Value = oCustomClass.ApplicationID
            par(2) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
            par(2).Value = oCustomClass.CustomerIDSurvey
            par(3) = New SqlParameter("@LamaUsaha", SqlDbType.Char, 2)
            par(3).Value = oCustomClass.LamaUsaha
            par(4) = New SqlParameter("@NamaUsaha", SqlDbType.VarChar, 50)
            par(4).Value = oCustomClass.NamaUsaha
            par(5) = New SqlParameter("@AlamatUsaha1", SqlDbType.VarChar, 50)
            par(5).Value = oCustomClass.AlamatUsaha1
            par(6) = New SqlParameter("@AlamatUsaha2", SqlDbType.Char, 1)
            par(6).Value = oCustomClass.AlamatUsaha2
            par(7) = New SqlParameter("@FotoTempatUsaha", SqlDbType.Char, 1)
            par(7).Value = oCustomClass.FotoTempatUsaha
            par(8) = New SqlParameter("@AlasanFotoTempatUsaha", SqlDbType.VarChar, 50)
            par(8).Value = oCustomClass.AlasanFotoTempatUsaha
            par(9) = New SqlParameter("@StockBarangDagangan", SqlDbType.Char, 1)
            par(9).Value = oCustomClass.StockBarangDagangan
            par(10) = New SqlParameter("@AlasanStockBarangDagangan", SqlDbType.VarChar, 50)
            par(10).Value = oCustomClass.AlasanStockBarangDagangan
            par(11) = New SqlParameter("@BonBuktiUsaha", SqlDbType.Char, 1)
            par(11).Value = oCustomClass.BonBuktiUsaha
            par(12) = New SqlParameter("@AlasanBonBuktiUsaha", SqlDbType.VarChar, 50)
            par(12).Value = oCustomClass.AlasanBonBuktiUsaha
            par(13) = New SqlParameter("@SKUNo", SqlDbType.VarChar, 35)
            par(13).Value = oCustomClass.SKUNo
            par(14) = New SqlParameter("@TanggalSKU", SqlDbType.Date)
            par(14).Value = oCustomClass.TanggalSKU
            par(15) = New SqlParameter("@SIUPNo", SqlDbType.VarChar, 35)
            par(15).Value = oCustomClass.SIUPNo
            par(16) = New SqlParameter("@TanggalSIUP", SqlDbType.Date)
            par(16).Value = oCustomClass.TanggalSIUP
            par(17) = New SqlParameter("@TDPNo", SqlDbType.VarChar, 35)
            par(17).Value = oCustomClass.TDPNo
            par(18) = New SqlParameter("@TanggalTDPNo", SqlDbType.Date)
            par(18).Value = oCustomClass.TanggalTDPNo
            par(19) = New SqlParameter("@SKDPNo", SqlDbType.VarChar, 35)
            par(19).Value = oCustomClass.SKDPNo
            par(20) = New SqlParameter("@TanggalSKDP", SqlDbType.Date)
            par(20).Value = oCustomClass.TanggalSKDP
            par(21) = New SqlParameter("@SlipGaji", SqlDbType.Char, 1)
            par(21).Value = oCustomClass.SlipGaji
            par(22) = New SqlParameter("@AlasanSlipGaji", SqlDbType.VarChar, 50)
            par(22).Value = oCustomClass.AlasanSlipGaji
            par(23) = New SqlParameter("@SKPenghasilan", SqlDbType.Char, 1)
            par(23).Value = oCustomClass.SKPenghasilan
            par(24) = New SqlParameter("@AlasanSKPenghasilan", SqlDbType.VarChar, 50)
            par(24).Value = oCustomClass.AlasanSKPenghasilan
            par(25) = New SqlParameter("@Jabatan", SqlDbType.VarChar, 20)
            par(25).Value = oCustomClass.Jabatan
            par(26) = New SqlParameter("@NPWPPribadi", SqlDbType.Char, 1)
            par(26).Value = oCustomClass.NPWPPribadi
            par(27) = New SqlParameter("@NPWPNo", SqlDbType.VarChar, 35)
            par(27).Value = oCustomClass.NPWPNo
            par(28) = New SqlParameter("@UraianUsahaPekerjaan", SqlDbType.Text)
            par(28).Value = oCustomClass.UraianUsahaPekerjaan
            par(29) = New SqlParameter("@AlurUsaha", SqlDbType.Text)
            par(29).Value = oCustomClass.AlurUsaha
            par(30) = New SqlParameter("@KapasitasKonsumen", SqlDbType.Text)
            par(30).Value = oCustomClass.KapasitasKonsumen
            par(31) = New SqlParameter("@AnalisaCapacity", SqlDbType.Text)
            par(31).Value = oCustomClass.AnalisaCapacity

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spHasilSurvey_002CapacitySave", par)

            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub HasilSurvey002ConditionSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        Try
            Dim par() As SqlParameter = New SqlParameter(34) {}

            par(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par(0).Value = oCustomClass.BranchId
            par(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            par(1).Value = oCustomClass.ApplicationID
            par(2) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
            par(2).Value = oCustomClass.CustomerIDSurvey
            par(3) = New SqlParameter("@TotalPenghasilan1", SqlDbType.Decimal)
            par(3).Value = oCustomClass.TotalPenghasilan1
            par(4) = New SqlParameter("@TotalPenghasilan2", SqlDbType.Decimal)
            par(4).Value = oCustomClass.TotalPenghasilan2
            par(5) = New SqlParameter("@BiayaUsaha1", SqlDbType.Decimal)
            par(5).Value = oCustomClass.BiayaUsaha1
            par(6) = New SqlParameter("@BiayaUsaha2", SqlDbType.Decimal)
            par(6).Value = oCustomClass.BiayaUsaha2
            par(7) = New SqlParameter("@BiayaHidup1", SqlDbType.Decimal)
            par(7).Value = oCustomClass.BiayaHidup1
            par(8) = New SqlParameter("@BiayaHidup2", SqlDbType.Decimal)
            par(8).Value = oCustomClass.BiayaHidup2
            par(9) = New SqlParameter("@BiayaCicilan1", SqlDbType.Decimal)
            par(9).Value = oCustomClass.BiayaCicilan1
            par(10) = New SqlParameter("@BiayaCicilan2", SqlDbType.Decimal)
            par(10).Value = oCustomClass.BiayaCicilan2
            par(11) = New SqlParameter("@SisaSebelumTambahUnit1", SqlDbType.Decimal)
            par(11).Value = oCustomClass.SisaSebelumTambahUnit1
            par(12) = New SqlParameter("@SisaSebelumTambahUnit2", SqlDbType.Decimal)
            par(12).Value = oCustomClass.SisaSebelumTambahUnit2
            par(13) = New SqlParameter("@IncomeDariUnit1", SqlDbType.Decimal)
            par(13).Value = oCustomClass.IncomeDariUnit1
            par(14) = New SqlParameter("@IncomeDariUnit2", SqlDbType.Decimal)
            par(14).Value = oCustomClass.IncomeDariUnit2
            par(15) = New SqlParameter("@EstimasiAngsuran1", SqlDbType.Decimal)
            par(15).Value = oCustomClass.EstimasiAngsuran1
            par(16) = New SqlParameter("@EstimasiAngsuran2", SqlDbType.Decimal)
            par(16).Value = oCustomClass.EstimasiAngsuran2
            par(17) = New SqlParameter("@SisaPendapatan1", SqlDbType.Decimal)
            par(17).Value = oCustomClass.SisaPendapatan1
            par(18) = New SqlParameter("@SisaPendapatan2", SqlDbType.Decimal)
            par(18).Value = oCustomClass.SisaPendapatan2
            par(19) = New SqlParameter("@RekeningTabunganGio", SqlDbType.Char, 1)
            par(19).Value = oCustomClass.RekeningTabunganGio
            par(20) = New SqlParameter("@NamaBank", SqlDbType.VarChar, 50)
            par(20).Value = oCustomClass.NamaBank
            par(21) = New SqlParameter("@LaporanKeuangan", SqlDbType.Char, 1)
            par(21).Value = oCustomClass.LaporanKeuangan
            par(22) = New SqlParameter("@AlasanLaporanKeuangan", SqlDbType.VarChar, 50)
            par(22).Value = oCustomClass.AlasanLaporanKeuangan
            par(23) = New SqlParameter("@KreditdariBankLKNB", SqlDbType.Char, 1)
            par(23).Value = oCustomClass.KreditdariBankLKNB
            par(24) = New SqlParameter("@NamaInstitusi1", SqlDbType.VarChar, 50)
            par(24).Value = oCustomClass.NamaInstitusi1
            par(25) = New SqlParameter("@Angsuran1", SqlDbType.Decimal)
            par(25).Value = oCustomClass.Angsuran1
            par(26) = New SqlParameter("@Sisa1", SqlDbType.Decimal)
            par(26).Value = oCustomClass.Sisa1
            par(27) = New SqlParameter("@NamaInstitusi2", SqlDbType.VarChar, 50)
            par(27).Value = oCustomClass.NamaInstitusi2
            par(28) = New SqlParameter("@Angsuran2", SqlDbType.Decimal)
            par(28).Value = oCustomClass.Angsuran2
            par(29) = New SqlParameter("@Sisa2", SqlDbType.Decimal)
            par(29).Value = oCustomClass.Sisa2
            par(30) = New SqlParameter("@BuktiPembayaranAngsuran", SqlDbType.Char, 1)
            par(30).Value = oCustomClass.BuktiPembayaranAngsuran
            par(31) = New SqlParameter("@AlasanBuktiPembayaranAngsuran", SqlDbType.VarChar, 50)
            par(31).Value = oCustomClass.AlasanBuktiPembayaranAngsuran
            par(32) = New SqlParameter("@AnalisaCondition", SqlDbType.Text)
            par(32).Value = oCustomClass.AnalisaCondition
            par(33) = New SqlParameter("@KondisiKesehatan", SqlDbType.VarChar, 15)
            par(33).Value = oCustomClass.KondisiKesehatan
            par(34) = New SqlParameter("@KondisiLingkungan", SqlDbType.VarChar, 15)
            par(34).Value = oCustomClass.KondisiLingkungan

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spHasilSurvey_002ConditionSave", par)

            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub HasilSurvey002CapitalSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        Try
            Dim par() As SqlParameter = New SqlParameter(35) {}

            par(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par(0).Value = oCustomClass.BranchId
            par(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            par(1).Value = oCustomClass.ApplicationID
            par(2) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
            par(2).Value = oCustomClass.CustomerIDSurvey
            par(3) = New SqlParameter("@StatusKepemilikan", SqlDbType.VarChar, 10)
            par(3).Value = oCustomClass.StatusKepemilikan
            par(4) = New SqlParameter("@AtasNama", SqlDbType.VarChar, 35)
            par(4).Value = oCustomClass.AtasNama
            par(5) = New SqlParameter("@PBBAJBSHMatasNama", SqlDbType.VarChar, 35)
            par(5).Value = oCustomClass.PBBAJBSHMatasNama
            par(6) = New SqlParameter("@TahunTagihanPBBAJBSHM", SqlDbType.Char, 4)
            par(6).Value = oCustomClass.TahunTagihanPBBAJBSHM
            par(7) = New SqlParameter("@PBBAJBSHM", SqlDbType.Char, 1)
            par(7).Value = oCustomClass.PBBAJBSHM
            par(8) = New SqlParameter("@AlasanPBBAJBSHM", SqlDbType.VarChar, 50)
            par(8).Value = oCustomClass.AlasanPBBAJBSHM
            par(9) = New SqlParameter("@RekListrikAtasNama", SqlDbType.VarChar, 35)
            par(9).Value = oCustomClass.RekListrikAtasNama
            par(10) = New SqlParameter("@BulanTagihanRekListrik", SqlDbType.Char, 2)
            par(10).Value = oCustomClass.BulanTagihanRekListrik
            par(11) = New SqlParameter("@RekListrik", SqlDbType.Char, 1)
            par(11).Value = oCustomClass.RekListrik
            par(12) = New SqlParameter("@AlasanRekListrik", SqlDbType.VarChar, 50)
            par(12).Value = oCustomClass.AlasanRekListrik
            par(13) = New SqlParameter("@LuasTanah", SqlDbType.Char, 4)
            par(13).Value = oCustomClass.LuasTanah
            par(14) = New SqlParameter("@LuasBangunan", SqlDbType.Char, 4)
            par(14).Value = oCustomClass.LuasBangunan
            par(15) = New SqlParameter("@TaksiranNilaiJual", SqlDbType.Decimal)
            par(15).Value = oCustomClass.TaksiranNilaiJual
            par(16) = New SqlParameter("@LamaTinggal", SqlDbType.Char, 2)
            par(16).Value = oCustomClass.LamaTinggal
            par(17) = New SqlParameter("@AssetLain", SqlDbType.VarChar, 50)
            par(17).Value = oCustomClass.AssetLain
            par(18) = New SqlParameter("@FotoRumah", SqlDbType.Char, 1)
            par(18).Value = oCustomClass.FotoRumah
            par(19) = New SqlParameter("@AlasanFotoRumah", SqlDbType.VarChar, 50)
            par(19).Value = oCustomClass.AlasanFotoRumah
            par(20) = New SqlParameter("@KondisiBangunan", SqlDbType.VarChar, 50)
            par(20).Value = oCustomClass.KondisiBangunan
            par(21) = New SqlParameter("@NoKTPPemohon", SqlDbType.VarChar, 25)
            par(21).Value = oCustomClass.NoKTPPemohon
            par(22) = New SqlParameter("@TanggalKTPPemohon", SqlDbType.Date)
            par(22).Value = oCustomClass.TanggalKTPPemohon
            par(23) = New SqlParameter("@KTPPemohon", SqlDbType.Char, 1)
            par(23).Value = oCustomClass.KTPPemohon
            par(24) = New SqlParameter("@AlasanKTPPemohon", SqlDbType.VarChar, 50)
            par(24).Value = oCustomClass.AlasanKTPPemohon
            par(25) = New SqlParameter("@NoKTPPasangan", SqlDbType.VarChar, 25)
            par(25).Value = oCustomClass.NoKTPPasangan
            par(26) = New SqlParameter("@TanggalNoKTPPasangan", SqlDbType.Date)
            par(26).Value = oCustomClass.TanggalNoKTPPasangan
            par(27) = New SqlParameter("@KTPPasangan", SqlDbType.Char, 1)
            par(27).Value = oCustomClass.KTPPasangan
            par(28) = New SqlParameter("@AlasanKTPPasangan", SqlDbType.VarChar, 50)
            par(28).Value = oCustomClass.AlasanKTPPasangan
            par(29) = New SqlParameter("@NoKK", SqlDbType.VarChar, 20)
            par(29).Value = oCustomClass.NoKK
            par(30) = New SqlParameter("@TanggalNoKartuKeluarga", SqlDbType.Date)
            par(30).Value = oCustomClass.TanggalNoKartuKeluarga
            par(31) = New SqlParameter("@BukuNikah", SqlDbType.Char, 1)
            par(31).Value = oCustomClass.BukuNikah
            par(32) = New SqlParameter("@AlasanBukuNikah", SqlDbType.VarChar, 50)
            par(32).Value = oCustomClass.AlasanBukuNikah
            par(33) = New SqlParameter("@JumlahTanggungan", SqlDbType.Char, 4)
            par(33).Value = oCustomClass.JumlahTanggungan
            par(34) = New SqlParameter("@AnalisaCapital", SqlDbType.Text)
            par(34).Value = oCustomClass.AnalisaCapital
            par(35) = New SqlParameter("@DayaListrik", SqlDbType.VarChar, 15)
            par(35).Value = oCustomClass.DayaListrik

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spHasilSurvey_002CapitalSave", par)

            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub HasilSurvey002CollateralSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        Try
            Dim par() As SqlParameter = New SqlParameter(16) {}

            par(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par(0).Value = oCustomClass.BranchId
            par(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            par(1).Value = oCustomClass.ApplicationID
            par(2) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
            par(2).Value = oCustomClass.CustomerIDSurvey
            par(3) = New SqlParameter("@KendaraanYangDiajukan", SqlDbType.VarChar, 35)
            par(3).Value = oCustomClass.KendaraanYangDiajukan
            par(4) = New SqlParameter("@AlasanPembelian", SqlDbType.VarChar, 50)
            par(4).Value = oCustomClass.AlasanPembelian
            par(5) = New SqlParameter("@LokasiPenggunaan", SqlDbType.VarChar, 50)
            par(5).Value = oCustomClass.LokasiPenggunaan
            par(6) = New SqlParameter("@PencocokanSTNKdenganFisik", SqlDbType.Char, 1)
            par(6).Value = oCustomClass.PencocokanSTNKdenganFisik
            par(7) = New SqlParameter("@BPKBDiKeluarkanOleh", SqlDbType.Char, 1)
            par(7).Value = oCustomClass.BPKBDiKeluarkanOleh
            par(8) = New SqlParameter("@JumlahAssetLain", SqlDbType.Char, 3)
            par(8).Value = oCustomClass.JumlahAssetLain
            par(9) = New SqlParameter("@JenisAssetLain", SqlDbType.VarChar, 35)
            par(9).Value = oCustomClass.JenisAssetLain
            par(10) = New SqlParameter("@StatusAssetLainLunas", SqlDbType.Char, 3)
            par(10).Value = oCustomClass.StatusAssetLainLunas
            par(11) = New SqlParameter("@StatusAssetLainTidakLunas", SqlDbType.Char, 3)
            par(11).Value = oCustomClass.StatusAssetLainTidakLunas
            par(12) = New SqlParameter("@BPKBNo", SqlDbType.VarChar, 50)
            par(12).Value = oCustomClass.BPKBNo
            par(13) = New SqlParameter("@NamaBPKB", SqlDbType.VarChar, 35)
            par(13).Value = oCustomClass.NamaBPKB
            par(14) = New SqlParameter("@BPKBDiperlihatkan", SqlDbType.Char, 1)
            par(14).Value = oCustomClass.BPKBDiperlihatkan
            par(15) = New SqlParameter("@AlasanBPKBDiperlihatkan", SqlDbType.VarChar, 50)
            par(15).Value = oCustomClass.AlasanBPKBDiperlihatkan
            par(16) = New SqlParameter("@AnalisaCollateral", SqlDbType.Text)
            par(16).Value = oCustomClass.AnalisaCollateral

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spHasilSurvey_002CollateralSave", par)

            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub HasilSurvey002CatatanSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        Try
            Dim par() As SqlParameter = New SqlParameter(20) {}

            par(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par(0).Value = oCustomClass.BranchId
            par(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            par(1).Value = oCustomClass.ApplicationID
            par(2) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
            par(2).Value = oCustomClass.CustomerIDSurvey
            par(3) = New SqlParameter("@KunjunganRumahBertemu", SqlDbType.VarChar, 50)
            par(3).Value = oCustomClass.KunjunganRumahBertemu
            par(4) = New SqlParameter("@KondisiRumah", SqlDbType.Char, 1)
            par(4).Value = oCustomClass.KondisiRumah
            par(5) = New SqlParameter("@PerabotanRumah", SqlDbType.Char, 1)
            par(5).Value = oCustomClass.PerabotanRumah
            par(6) = New SqlParameter("@GarasiMobil", SqlDbType.Char, 1)
            par(6).Value = oCustomClass.GarasiMobil
            par(7) = New SqlParameter("@GarasiUntukBerapaUnit", SqlDbType.Char, 2)
            par(7).Value = oCustomClass.GarasiUntukBerapaUnit
            par(8) = New SqlParameter("@AlasanGarasiUntukBerapaUnit", SqlDbType.VarChar, 50)
            par(8).Value = oCustomClass.AlasanGarasiUntukBerapaUnit
            par(9) = New SqlParameter("@LokasiRumah", SqlDbType.VarChar, 20)
            par(9).Value = oCustomClass.LokasiRumah
            par(10) = New SqlParameter("@JalanMasukDilalui", SqlDbType.VarChar, 20)
            par(10).Value = oCustomClass.JalanMasukDilalui
            par(11) = New SqlParameter("@KondisiJalanDepanRumah", SqlDbType.VarChar, 20)
            par(11).Value = oCustomClass.KondisiJalanDepanRumah
            par(12) = New SqlParameter("@KeadaanLingkungan", SqlDbType.VarChar, 20)
            par(12).Value = oCustomClass.KeadaanLingkungan
            par(13) = New SqlParameter("@KunjunganKantorBertemu", SqlDbType.VarChar, 50)
            par(13).Value = oCustomClass.KunjunganKantorBertemu
            par(14) = New SqlParameter("@JumlahKaryawan", SqlDbType.Char, 4)
            par(14).Value = oCustomClass.JumlahKaryawan
            par(15) = New SqlParameter("@LokasiTempatUsaha", SqlDbType.Char, 1)
            par(15).Value = oCustomClass.LokasiTempatUsaha
            par(16) = New SqlParameter("@Alasan", SqlDbType.VarChar, 50)
            par(16).Value = oCustomClass.Alasan
            par(17) = New SqlParameter("@AktivitasTempatUsaha", SqlDbType.Char, 1)
            par(17).Value = oCustomClass.AktivitasTempatUsaha
            par(18) = New SqlParameter("@AlasanAktivitasTempatUsaha", SqlDbType.VarChar, 50)
            par(18).Value = oCustomClass.AlasanAktivitasTempatUsaha
            par(19) = New SqlParameter("@TBOList", SqlDbType.VarChar, 50)
            par(19).Value = oCustomClass.TBOList
            par(20) = New SqlParameter("@UsulanLain", SqlDbType.VarChar, 50)
            par(20).Value = oCustomClass.UsulanLain

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spHasilSurvey_002CatatanSave", par)

            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub HasilSurvey002KYCSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        Try
            Dim par() As SqlParameter = New SqlParameter(3) {}

            par(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par(0).Value = oCustomClass.BranchId
            par(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            par(1).Value = oCustomClass.ApplicationID
            par(2) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
            par(2).Value = oCustomClass.CustomerIDSurvey
            par(3) = New SqlParameter("@AnalisaKYC", SqlDbType.Text)
            par(3).Value = oCustomClass.AnalisaKYC

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spHasilSurvey_002KYCSave", par)

            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub HasilSurvey002CharacterCLSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        Try
            Dim parCL() As SqlParameter = New SqlParameter(7) {}

            parCL(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            parCL(0).Value = oCustomClass.BranchId
            parCL(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            parCL(1).Value = oCustomClass.ApplicationID
            parCL(2) = New SqlParameter("@CustomerID", SqlDbType.VarChar, 20)
            parCL(2).Value = oCustomClass.CustomerIDSurvey
            parCL(3) = New SqlParameter("@CLNama1", SqlDbType.VarChar, 35)
            parCL(3).Value = oCustomClass.CLNama1
            parCL(4) = New SqlParameter("@CLDomisili1", SqlDbType.Char, 1)
            parCL(4).Value = oCustomClass.CLDomisili1
            parCL(5) = New SqlParameter("@CLLamaTinggal1", SqlDbType.Char, 2)
            parCL(5).Value = oCustomClass.CLLamaTinggal1
            parCL(6) = New SqlParameter("@CLLamaUsaha1", SqlDbType.Char, 2)
            parCL(6).Value = oCustomClass.CLLamaUsaha1
            parCL(7) = New SqlParameter("@CLStatus", SqlDbType.VarChar, 35)
            parCL(7).Value = oCustomClass.CLStatus


            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spHasilSurvey_002CharacterCLSave", parCL)

            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub HasilSurvey002AplStepSave(ByVal oCustomClass As Parameter.HasilSurvey)
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction
        Dim par() As SqlParameter = New SqlParameter(1) {}

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        par(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        par(0).Value = oCustomClass.ApplicationID
        par(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
        par(1).Value = oCustomClass.BranchId
        Try
            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spHasilSurvey_002AplStepSave", par)
            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Function getHasilMobileSurvey(ByVal ocustomClass As Parameter.HasilSurvey) As Parameter.HasilSurvey
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.HasilSurvey
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = ocustomClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = ocustomClass.ApplicationID

        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, "spGetHasilMobileSurvey", params)
            If reader.Read Then
                With oReturnValue
                    .ProspectAppID = reader("ProspectAppID").ToString
                    .AgreementSurveyDate = CDate(reader("SurveyDate").ToString)
                    .SurveyorID = reader("SurveyorID").ToString
                    .CAID = reader("CAID").ToString
                    .SaldoRataRata = CDbl(reader("SaldoRataRata").ToString)
                    .SaldoAwal = CDbl(reader("SaldoAwal").ToString)
                    .SaldoAkhir = CDbl(reader("SaldoAkhir").ToString)
                    .JumlahPemasukan = CDbl(reader("JumlahPemasukan").ToString)
                    .JumlahPengeluaran = CDbl(reader("JumlahPengeluaran").ToString)
                    .JumlahHariTransaksi = CInt(reader("JumlahHariTransaksi").ToString)
                    .SurveyorNotes = reader("SurveyorNotes").ToString
                    .JenisRekening = reader("BankAccountType").ToString
                    .LamaSurvey = CShort(reader("LamaSurvey"))
                    .JumlahKendaraan = CInt(reader("JumlahKendaraan").ToString)
                    .Garasi = CBool(reader("Garasi").ToString)
                    .PertamaKredit = CStr(reader("PertamaKredit").ToString)
                    .OrderKe = CInt(reader("OrderKe").ToString)
                    .AgreementSurveyConfirmationDate = CDate(reader("SurveyConfirmationDate"))
                    .creditScore = CDec(reader("creditScore"))
                    .creditScoringResult = reader("creditScoringResult").ToString
                    .AnalisaNasabah = reader("AnalisaNasabah").ToString
                    .AnalisaTempatTinggal = reader("AnalisaTempatTinggal").ToString
                    .AnalisaKendaraan = reader("AnalisaKendaraan").ToString
                    .isProceed = CBool(reader("isProceed").ToString)
                    .CaraSurvey = reader("CaraSurvey").ToString
                    .SurveyAreaPhone1 = reader("SurveyAreaPhone1").ToString
                    .SurveyPhone1 = reader("SurveyPhone1").ToString
                    .SurveyAreaPhone2 = reader("SurveyAreaPhone2").ToString
                    .SurveyPhone2 = reader("SurveyPhone2").ToString
                    .SurveyCellphone = reader("SurveyCellphone").ToString
                    .EmergencyPhoneContact = reader("EmergencyPhoneContact").ToString
                    .PihakYangTurutSurvey = reader("PihakYangTurutSurvey").ToString
                    .AlasanPihakYangTurutSurvey = reader("AlasanPihakYangTurutSurvey").ToString
                    .LokasiSurvey = reader("LokasiSurvey").ToString
                    .AlasanLokasiSurvey = reader("AlasanLokasiSurvey").ToString
                    .LokasiSurveySesuaiKTP = reader("LokasiSurveySesuaiKTP").ToString
                    .AlasanLokasiSurveySesuaiKTP = reader("AlasanLokasiSurveySesuaiKTP").ToString
                    .WaktuSurvey1 = CDate(reader("WaktuSurvey1").ToString)
                    .WaktuSurvey2 = CDate(reader("WaktuSurvey2").ToString)
                    .CLNama1 = reader("CLNama1").ToString
                    .CLDomisili1 = reader("CLDomisili1").ToString
                    .CLLamaTinggal1 = reader("CLLamaTinggal1").ToString
                    .CLLamaUsaha1 = reader("CLLamaUsaha1").ToString
                    .CLNama2 = reader("CLNama2").ToString
                    .CLDomisili2 = reader("CLDomisili2").ToString
                    .CLLamaTinggal2 = reader("CLLamaTinggal2").ToString
                    .CLLamaUsaha2 = reader("CLLamaUsaha2").ToString
                    .CLNama3 = reader("CLNama3").ToString
                    .CLDomisili3 = reader("CLDomisili3").ToString
                    .CLLamaTinggal3 = reader("CLLamaTinggal3").ToString
                    .CLLamaUsaha3 = reader("CLLamaUsaha3").ToString
                    .HasilCekLkgAspekSosial = reader("HasilCekLkgAspekSosial").ToString
                    .PengalamanKredit = reader("PengalamanKredit").ToString
                    .Pembayaran = reader("Pembayaran").ToString
                    .BuktiBayar = reader("BuktiBayar").ToString
                    .PekerjaanSekarang = reader("PekerjaanSekarang").ToString
                    .LamanyaPekerjaanSekarang = reader("LamanyaPekerjaanSekarang").ToString
                    .PekerjaanSebelumnya = reader("PekerjaanSebelumnya").ToString
                    .LamanyaPekerjaanSebelumnya = reader("LamanyaPekerjaanSebelumnya").ToString
                    .Pendidikan = reader("Pendidikan").ToString
                    .NamaYangMenemaniSurvey1 = reader("NamaYangMenemaniSurvey1").ToString
                    .Hubungan1 = reader("Hubungan1").ToString
                    .NamaYangMenemaniSurvey2 = reader("NamaYangMenemaniSurvey2").ToString
                    .Hubungan2 = reader("Hubungan2").ToString
                    .HubunganSalesDgnPemohon = reader("HubunganSalesDgnPemohon").ToString
                    .AnalisaCharacter = reader("AnalisaCharacter").ToString
                    .LamaUsaha = reader("LamaUsaha").ToString
                    .NamaUsaha = reader("NamaUsaha").ToString
                    .AlamatUsaha1 = reader("AlamatUsaha1").ToString
                    .AlamatUsaha2 = reader("AlamatUsaha2").ToString
                    .FotoTempatUsaha = reader("FotoTempatUsaha").ToString
                    .AlasanFotoTempatUsaha = reader("AlasanFotoTempatUsaha").ToString
                    .StockBarangDagangan = reader("StockBarangDagangan").ToString
                    .AlasanStockBarangDagangan = reader("AlasanStockBarangDagangan").ToString
                    .BonBuktiUsaha = reader("BonBuktiUsaha").ToString
                    .AlasanBonBuktiUsaha = reader("AlasanBonBuktiUsaha").ToString
                    .SKUNo = reader("SKUNo").ToString
                    .TanggalSKU = CDate(reader("TanggalSKU").ToString)
                    .SIUPNo = reader("SIUPNo").ToString
                    .TanggalSIUP = CDate(reader("TanggalSIUP").ToString)
                    .TDPNo = reader("TDPNo").ToString
                    .TanggalTDPNo = CDate(reader("TanggalTDPNo").ToString)
                    .SKDPNo = reader("SKDPNo").ToString
                    .TanggalSKDP = CDate(reader("TanggalSKDP").ToString)
                    .SlipGaji = reader("SlipGaji").ToString
                    .AlasanSlipGaji = reader("AlasanSlipGaji").ToString
                    .SKPenghasilan = reader("SKPenghasilan").ToString
                    .AlasanSKPenghasilan = reader("AlasanSKPenghasilan").ToString
                    .Jabatan = reader("Jabatan").ToString
                    .NPWPPribadi = reader("NPWPPribadi").ToString
                    .NPWPNo = reader("NPWPNo").ToString
                    .UraianUsahaPekerjaan = reader("UraianUsahaPekerjaan").ToString
                    .AlurUsaha = reader("AlurUsaha").ToString
                    .KapasitasKonsumen = reader("KapasitasKonsumen").ToString
                    .AnalisaCapacity = reader("AnalisaCapacity").ToString
                    .TotalPenghasilan1 = CDbl(reader("TotalPenghasilan1").ToString)
                    .TotalPenghasilan2 = CDbl(reader("TotalPenghasilan2").ToString)
                    .BiayaUsaha1 = CDbl(reader("BiayaUsaha1").ToString)
                    .BiayaUsaha2 = CDbl(reader("BiayaUsaha2").ToString)
                    .BiayaHidup1 = CDbl(reader("BiayaHidup1").ToString)
                    .BiayaHidup2 = CDbl(reader("BiayaHidup2").ToString)
                    .BiayaCicilan1 = CDbl(reader("BiayaCicilan1").ToString)
                    .BiayaCicilan2 = CDbl(reader("BiayaCicilan2").ToString)
                    .SisaSebelumTambahUnit1 = CDbl(reader("SisaSebelumTambahUnit1").ToString)
                    .SisaSebelumTambahUnit2 = CDbl(reader("SisaSebelumTambahUnit2").ToString)
                    .IncomeDariUnit1 = CDbl(reader("IncomeDariUnit1").ToString)
                    .IncomeDariUnit2 = CDbl(reader("IncomeDariUnit2").ToString)
                    .EstimasiAngsuran1 = CDbl(reader("EstimasiAngsuran1").ToString)
                    .EstimasiAngsuran2 = CDbl(reader("EstimasiAngsuran2").ToString)
                    .SisaPendapatan1 = CDbl(reader("SisaPendapatan1").ToString)
                    .SisaPendapatan2 = CDbl(reader("SisaPendapatan2").ToString)
                    .RekeningTabunganGio = reader("RekeningTabunganGio").ToString
                    .NamaBank = reader("NamaBank").ToString
                    .LaporanKeuangan = reader("LaporanKeuangan").ToString
                    .AlasanLaporanKeuangan = reader("AlasanLaporanKeuangan").ToString
                    .KreditdariBankLKNB = reader("KreditdariBankLKNB").ToString
                    .NamaInstitusi1 = reader("NamaInstitusi1").ToString
                    .Angsuran1 = CDbl(reader("Angsuran1").ToString)
                    .Sisa1 = CDbl(reader("Sisa1").ToString)
                    .NamaInstitusi2 = reader("NamaInstitusi2").ToString
                    .Angsuran2 = CDbl(reader("Angsuran2").ToString)
                    .Sisa2 = CDbl(reader("Sisa2").ToString)
                    .BuktiPembayaranAngsuran = reader("BuktiPembayaranAngsuran").ToString
                    .AlasanBuktiPembayaranAngsuran = reader("AlasanBuktiPembayaranAngsuran").ToString
                    .AnalisaCondition = reader("AnalisaCondition").ToString
                    .StatusKepemilikan = reader("StatusKepemilikan").ToString
                    .AtasNama = reader("AtasNama").ToString
                    .PBBAJBSHMatasNama = reader("PBBAJBSHMatasNama").ToString
                    .TahunTagihanPBBAJBSHM = reader("TahunTagihanPBBAJBSHM").ToString
                    .PBBAJBSHM = reader("PBBAJBSHM").ToString
                    .AlasanPBBAJBSHM = reader("AlasanPBBAJBSHM").ToString
                    .RekListrikAtasNama = reader("RekListrikAtasNama").ToString
                    .BulanTagihanRekListrik = reader("BulanTagihanRekListrik").ToString
                    .RekListrik = reader("RekListrik").ToString
                    .AlasanRekListrik = reader("AlasanRekListrik").ToString
                    .LuasTanah = reader("LuasTanah").ToString
                    .LuasBangunan = reader("LuasBangunan").ToString
                    .TaksiranNilaiJual = CDbl(reader("TaksiranNilaiJual").ToString)
                    .LamaTinggal = reader("LamaTinggal").ToString
                    .AssetLain = reader("AssetLain").ToString
                    .FotoRumah = reader("FotoRumah").ToString
                    .AlasanFotoRumah = reader("AlasanFotoRumah").ToString
                    .KondisiBangunan = reader("KondisiBangunan").ToString
                    .NoKTPPemohon = reader("NoKTPPemohon").ToString
                    .TanggalKTPPemohon = CDate(reader("TanggalKTPPemohon").ToString)
                    .KTPPemohon = reader("KTPPemohon").ToString
                    .AlasanKTPPemohon = reader("AlasanKTPPemohon").ToString
                    .NoKTPPasangan = reader("NoKTPPasangan").ToString
                    .TanggalNoKTPPasangan = CDate(reader("TanggalNoKTPPasangan").ToString)
                    .KTPPasangan = reader("KTPPasangan").ToString
                    .AlasanKTPPasangan = reader("AlasanKTPPasangan").ToString
                    .NoKK = reader("NoKK").ToString
                    .TanggalNoKartuKeluarga = CDate(reader("TanggalNoKartuKeluarga").ToString)
                    .BukuNikah = reader("BukuNikah").ToString
                    .AlasanBukuNikah = reader("AlasanBukuNikah").ToString
                    .JumlahTanggungan = reader("JumlahTanggungan").ToString
                    .AnalisaCapital = reader("AnalisaCapital").ToString
                    .KendaraanYangDiajukan = reader("KendaraanYangDiajukan").ToString
                    .AlasanPembelian = reader("AlasanPembelian").ToString
                    .LokasiPenggunaan = reader("LokasiPenggunaan").ToString
                    .PencocokanSTNKdenganFisik = reader("PencocokanSTNKdenganFisik").ToString
                    .BPKBDiKeluarkanOleh = reader("BPKBDiKeluarkanOleh").ToString
                    .JumlahAssetLain = reader("JumlahAssetLain").ToString
                    .JenisAssetLain = reader("JenisAssetLain").ToString
                    .StatusAssetLainLunas = reader("StatusAssetLainLunas").ToString
                    .StatusAssetLainTidakLunas = reader("StatusAssetLainTidakLunas").ToString
                    .BPKBNo = reader("BPKBNo").ToString
                    .NamaBPKB = reader("NamaBPKB").ToString
                    .BPKBDiperlihatkan = reader("BPKBDiperlihatkan").ToString
                    .AlasanBPKBDiperlihatkan = reader("AlasanBPKBDiperlihatkan").ToString
                    .AnalisaCollateral = reader("AnalisaCollateral").ToString
                    .KunjunganRumahBertemu = reader("KunjunganRumahBertemu").ToString
                    .KondisiRumah = reader("KondisiRumah").ToString
                    .PerabotanRumah = reader("PerabotanRumah").ToString
                    .GarasiMobil = reader("GarasiMobil").ToString
                    .GarasiUntukBerapaUnit = reader("GarasiUntukBerapaUnit").ToString
                    .AlasanGarasiUntukBerapaUnit = reader("AlasanGarasiUntukBerapaUnit").ToString
                    .LokasiRumah = reader("LokasiRumah").ToString
                    .JalanMasukDilalui = reader("JalanMasukDilalui").ToString
                    .KondisiJalanDepanRumah = reader("KondisiJalanDepanRumah").ToString
                    .KeadaanLingkungan = reader("KeadaanLingkungan").ToString
                    .KunjunganKantorBertemu = reader("KunjunganKantorBertemu").ToString
                    .JumlahKaryawan = reader("JumlahKaryawan").ToString
                    .LokasiTempatUsaha = reader("LokasiTempatUsaha").ToString
                    .Alasan = reader("Alasan").ToString
                    .AktivitasTempatUsaha = reader("AktivitasTempatUsaha").ToString
                    .AlasanAktivitasTempatUsaha = reader("AlasanAktivitasTempatUsaha").ToString
                    .TBOList = reader("TBOList").ToString
                    .UsulanLain = reader("UsulanLain").ToString
                    .AnalisaKYC = reader("AnalisaKYC").ToString
                    .AreaPhoneHome = reader("SurveyAreaPhone1").ToString
                    .PhoneHome = reader("SurveyPhone1").ToString
                    .AreaPhoneOffice = reader("SurveyAreaPhone2").ToString
                    .PhoneOffice = reader("SurveyPhone2").ToString
                    '.EmergencyPhoneContact = reader("EmergencyPhoneContact").ToString
                    '.MobilePhone = reader("SurveyCellphone").ToString
                    .LinkFotoRumah1 = reader("FotoRumah1").ToString
                    .LinkFotoRumah2 = reader("FotoRumah2").ToString
                    .LinkFotoRumah3 = reader("FotoRumah3").ToString
                    .LinkFotoTempatUsaha1 = reader("FotoTempatUsaha1").ToString
                    .LinkFotoTempatUsaha2 = reader("FotoTempatUsaha2").ToString
                    .LinkFotoTempatUsaha3 = reader("FotoTempatUsaha3").ToString
                    .CustomerType = reader("CustomerType").ToString
                    .DP = CDbl(reader("DP").ToString)
                    .ManufacturerCountry = reader("ManufacturerCountry").ToString
                    .AssetType = reader("AssetType").ToString
                    .UsiaKendaraan = reader("UsiaKendaraan").ToString
                    .JenisPekerjaan = reader("JenisPekerjaan").ToString
                    .RatioAngsuran = CDbl(reader("RatioAngsuran").ToString)
                    .Integritas = reader("Integritas").ToString
                    .KondisiKesehatan = reader("KondisiKesehatan").ToString
                    .IndustryRisk = reader("IndustryRisk").ToString
                    .UsiaPemohon = reader("UsiaPemohon").ToString
                End With
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function getHasilScore(ByVal scoringData As Parameter.HasilSurvey) As Parameter.HasilSurvey
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.HasilSurvey
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = scoringData.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = scoringData.ApplicationID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(scoringData.strConnection, CommandType.StoredProcedure, "spgetHasilScore", params).Tables(0)
            'If reader.Read Then
            '    With oReturnValue
            '        .Komponen = reader("Komponen").ToString
            '        .Klasifikasi = reader("Klasifikasi").ToString
            '        .Nilai = reader("Nilai").ToString
            '        .Bobot = reader("Bobot").ToString
            '        .Hasil = reader("Hasil").ToString
            '    End With
            'End If
            'reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
