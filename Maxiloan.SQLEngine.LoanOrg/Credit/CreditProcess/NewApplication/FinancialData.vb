#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class FinancialData : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region "KPR"


    Public Function SaveFinancialDataTab2EndKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)

        Dim transaction As SqlTransaction = Nothing
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(35) {}

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.ApplicationID
            params(2) = New SqlParameter("@PerusahaanAppraisal", SqlDbType.VarChar)
            params(2).Value = oCustomClass.PerusahaanAppraisal
            params(3) = New SqlParameter("@Appraisal", SqlDbType.VarChar)
            params(3).Value = oCustomClass.Appraisal
            params(4) = New SqlParameter("@Appriser", SqlDbType.VarChar)
            params(4).Value = oCustomClass.Appriser
            params(5) = New SqlParameter("@TangggalSurvey", SqlDbType.Date)
            params(5).Value = oCustomClass.TangggalSurvey
            params(6) = New SqlParameter("@jam", SqlDbType.VarChar)
            params(6).Value = oCustomClass.jam
            params(7) = New SqlParameter("@LuastanahApplikasi", SqlDbType.Int)
            params(7).Value = oCustomClass.LuastanahApplikasi
            params(8) = New SqlParameter("@PanjangXLebarTanahApplikasi", SqlDbType.VarChar)
            params(8).Value = oCustomClass.PanjangXLebarTanahApplikasi
            params(9) = New SqlParameter("@LuasTanahAppraisal", SqlDbType.Int)
            params(9).Value = oCustomClass.LuasTanahAppraisal
            params(10) = New SqlParameter("@HargaPerMTanah", SqlDbType.Decimal)
            params(10).Value = oCustomClass.HargaPerMTanah
            params(11) = New SqlParameter("@NilaiPasarTanah", SqlDbType.Decimal)
            params(11).Value = oCustomClass.NilaiPasarTanah
            params(12) = New SqlParameter("@LikuidasiTanah", SqlDbType.Int)
            params(12).Value = oCustomClass.LikuidasiTanah
            params(13) = New SqlParameter("@NilaiLikuidasiTanah", SqlDbType.Decimal)
            params(13).Value = oCustomClass.NilaiLikuidasiTanah
            params(14) = New SqlParameter("@NamaContact", SqlDbType.VarChar)
            params(14).Value = oCustomClass.NamaContact
            params(15) = New SqlParameter("@HubunganContact", SqlDbType.VarChar)
            params(15).Value = oCustomClass.HubunganContact
            params(16) = New SqlParameter("@NoTelp", SqlDbType.VarChar)
            params(16).Value = oCustomClass.NoTelp
            params(17) = New SqlParameter("@NoHP", SqlDbType.VarChar)
            params(17).Value = oCustomClass.NoHP
            params(18) = New SqlParameter("@LuasBangunanApplikasi", SqlDbType.VarChar)
            params(18).Value = oCustomClass.LuasBangunanApplikasi
            params(19) = New SqlParameter("@PanjangXLebarBangunanApplikasi", SqlDbType.VarChar)
            params(19).Value = oCustomClass.PanjangXLebarBangunanApplikasi
            params(20) = New SqlParameter("@LuasBangunan", SqlDbType.Int)
            params(20).Value = oCustomClass.LuasBangunan
            params(21) = New SqlParameter("@PanjangXLebarBangunan", SqlDbType.VarChar)
            params(21).Value = oCustomClass.PanjangXLebarBangunan
            params(22) = New SqlParameter("@Lantai", SqlDbType.Int)
            params(22).Value = oCustomClass.Lantai
            params(23) = New SqlParameter("@HargaPermBangunan", SqlDbType.Decimal)
            params(23).Value = oCustomClass.HargaPermBangunan
            params(24) = New SqlParameter("@NilaiLikuidasiBangunan", SqlDbType.Decimal)
            params(24).Value = oCustomClass.NilaiLikuidasiBangunan
            params(25) = New SqlParameter("@PanjangXLebarTanah", SqlDbType.VarChar)
            params(25).Value = oCustomClass.PanjangXLebarTanah

            params(26) = New SqlParameter("@NilaiPasarSurvey", SqlDbType.Decimal)
            params(26).Value = oCustomClass.NilaiPasarTtl
            params(27) = New SqlParameter("@SumberPembandingan", SqlDbType.VarChar)
            params(27).Value = oCustomClass.SumberPerbandingan
            params(28) = New SqlParameter("@HargaInternetSurvey", SqlDbType.Decimal)
            params(28).Value = oCustomClass.HargaInternetTtl
            params(29) = New SqlParameter("@SumberDataInternet", SqlDbType.VarChar)
            params(29).Value = oCustomClass.SumbardataInternet
            params(30) = New SqlParameter("@Keterangan", SqlDbType.VarChar)
            params(30).Value = oCustomClass.Keterangan
            params(31) = New SqlParameter("@NilaiPermSurvey", SqlDbType.Decimal)
            params(31).Value = oCustomClass.NilaiPermTtl
            params(32) = New SqlParameter("@LuasmSurvey", SqlDbType.Decimal)
            params(32).Value = oCustomClass.LuasMTtl
            params(33) = New SqlParameter("@TotalSurvey", SqlDbType.Decimal)
            params(33).Value = oCustomClass.TotalSurvey
            params(34) = New SqlParameter("@NilaiLikuidasiSurvey", SqlDbType.Decimal)
            params(34).Value = oCustomClass.NilaiLikuidasiTtl
            params(35) = New SqlParameter("@SelectedAppraisal", SqlDbType.VarChar)
            params(35).Value = oCustomClass.SelectedAppraisal

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFinancialDataSaveTab2EndKPR", params)


            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function


    Public Function DeleteAngsuranKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try

            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar)
            params(0).Value = oCustomClass.ApplicationID

            SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "SpDeleteKPR", params)

            Return oReturnValue
        Catch exp As Exception
            WriteException("HitungAngsuranKPR", "HitungAngsuranKPR", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.HitungAngsuranKPR")
        End Try
    End Function
    Public Function GetDataDrop(ByVal oCustomer As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomer.Table

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomer.strConnection, CommandType.StoredProcedure, "SpAppraisalCompany", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "CustomerPersonalAdd", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.CustomerPersonalAdd")
        End Try
    End Function
    Public Function HitungangsuranKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim params() As SqlParameter = New SqlParameter(7) {}
        Try
            params(0) = New SqlParameter("@Rate", SqlDbType.Float)
            params(0).Value = oCustomClass.RateKPR
            params(1) = New SqlParameter("@NTF", SqlDbType.Decimal)
            params(1).Value = oCustomClass.NTFKPR
            params(2) = New SqlParameter("@TenorAwal", SqlDbType.Int)
            params(2).Value = oCustomClass.TenorAwalKPR
            params(3) = New SqlParameter("@TenorAkhir", SqlDbType.Int)
            params(3).Value = oCustomClass.TenorAkhirKPR
            params(4) = New SqlParameter("@TotalTenor", SqlDbType.Int)
            params(4).Value = oCustomClass.TotalTenorKPR
            params(5) = New SqlParameter("@ApplicationID", SqlDbType.VarChar)
            params(5).Value = oCustomClass.ApplicationID
            params(6) = New SqlParameter("@RateType", SqlDbType.VarChar)
            params(6).Value = oCustomClass.Ratetype
            params(7) = New SqlParameter("@TermSeqNo", SqlDbType.Int)
            params(7).Value = oCustomClass.TermSeqNo

            SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "SpHitungAngsuranKPR", params)

            Return oReturnValue
        Catch exp As Exception
            WriteException("HitungAngsuranKPR", "HitungAngsuranKPR", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.HitungAngsuranKPR")
        End Try
    End Function
    Public Function SaveKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim params() As SqlParameter = New SqlParameter(6) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar)
            params(0).Value = oCustomClass.ApplicationID
            params(1) = New SqlParameter("@NotarisFee", SqlDbType.Decimal)
            params(1).Value = oCustomClass.NotarisFee
            params(2) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
            params(2).Value = oCustomClass.AdminFee
            params(3) = New SqlParameter("@APHTFee", SqlDbType.Decimal)
            params(3).Value = oCustomClass.APHTFee
            params(4) = New SqlParameter("@AppraisalTab", SqlDbType.VarChar)
            params(4).Value = oCustomClass.AppraisalTab
            params(5) = New SqlParameter("@BPHTBFee", SqlDbType.Decimal)
            params(5).Value = oCustomClass.BPHTBFee
            params(6) = New SqlParameter("@AppraisalFee", SqlDbType.Decimal)
            params(6).Value = oCustomClass.AppraisalFee

            SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spSaveKPR", params)

            Return oReturnValue
        Catch exp As Exception
            WriteException("GetDataGridKPR", "GetDataGridKPR", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.GetDataGridKPR")
        End Try
    End Function
    Public Function getDataKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar)
            params(0).Value = oCustomClass.ApplicationID

            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewAmortizationKPR", params).Tables(0)

            Return oReturnValue
        Catch exp As Exception
            WriteException("GetDataGridKPR", "GetDataGridKPR", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.GetDataGridKPR")
        End Try
    End Function
    Public Function getDataKPRHeader(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar)
            params(0).Value = oCustomClass.ApplicationID

            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewAmortizationKPRHeader", params).Tables(0)

            Return oReturnValue
        Catch exp As Exception
            WriteException("GetDataGridKPR", "GetDataGridKPR", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.GetDataGridKPR")
        End Try
    End Function
    Public Function GetDataGridKPR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar)
            params(0).Value = oCustomClass.ApplicationID

            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetDataGridKPR", params).Tables(0)

            Return oReturnValue
        Catch exp As Exception
            WriteException("GetDataGridKPR", "GetDataGridKPR", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.GetDataGridKPR")
        End Try
    End Function
#End Region
#Region " Private Const "
    'Stored Procedure name    
    Private Const spPaging As String = "spFinancialDataPaging"
    Private Const spFinancialDataSave As String = "spFinancialDataSaveSim02" '"spFinancialDataSave"
    Private Const spFinancialDataSaveOperatingLease As String = "spFinancialDataSaveOperatingLease"
    Private Const spFinancialDataSave2 As String = "spFinancialDataSave2"
    Private Const spFinancialDataSave3 As String = "spFinancialDataSave3"
    Private Const spSaveAmortisasi As String = "spSaveAmortisation"
    Private Const spSaveAmortisasiIRR As String = "spSaveAmortisationIRR"
    Private Const spListAmortisasiIRR As String = "spListAmortisationIRR"
    Private Const spSaveAmortisasiStepUpDown As String = "spSaveAmortisationStepUpDown"
    Private Const spSelectAmortisasiStepUpDown As String = "spSelectAmortisationStepUpDown"
    Private Const qrGetStepUpStepDownType As String = "select StepUpStepDownType from Agreement  where Branchid=@BranchID and Applicationid=@ApplicationID"
    Private Const spFinancialDataSaveTab2 As String = "spFinancialDataSaveTab2"
    Private Const spFinancialDataSaveTab2End As String = "spFinancialDataSaveTab2End"
    Private Const spFinancialDataSaveTab2OperatingLease As String = "spFinancialDataSaveTab2OperatingLease"
    Private Const spFinancialDataSaveTab2EndOperatingLease As String = "spFinancialDataSaveTab2EndOperatingLease"

#End Region

#Region "Financial Data"
    Public Function GetFinancialData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = oCustomClass.CurrentPage
            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = oCustomClass.PageSize
            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
            params(2).Value = oCustomClass.WhereCond
            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
            params(3).Value = oCustomClass.SortBy
            params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output

            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spPaging, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("FinancialData", "GetFinancialData", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.GetFinancialData")
        End Try
    End Function

    Public Function GetFinancialData_002(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
        params(1).Value = oCustomClass.ApplicationID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, oCustomClass.SpName, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("FinancialData", oCustomClass.SpName, exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("FinancialData - DA", oCustomClass.SpName, exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            Throw New Exception(exp.StackTrace)
        End Try
    End Function

    Public Function GetInstallmentSchedule(ByVal ocustomClass As Parameter.FinancialData) As DataTable
        Dim oReturnValue As New DataTable
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
        params(0).Value = ocustomClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
        params(1).Value = ocustomClass.AppID
        Try
            oReturnValue = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, "spGetInstallmentSchedule", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("FinancialData", "GetInstallmentSchedule", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("FinancialData - DA", "GetInstallmentSchedule", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            Throw New Exception(exp.StackTrace)
        End Try

    End Function

    Public Sub ClearingInstallmentSchedule(ByVal oCustomClass As Parameter.FinancialData)
        Dim objConnection As New SqlConnection(oCustomClass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            Dim params(0) As SqlParameter
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.AppID
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, oCustomClass.SpName, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("FinancialData", "ClearingInstallmentSchedule", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("FinancialData.vb", "ClearingInstallmentSchedule", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Public Function SaveFinancialData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)

        Dim transaction As SqlTransaction = Nothing
        Dim Temp_PrincipleAmount As Double = 0
        Dim Temp_InterestAmount As Double = 0
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(81) {}

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.AppID
            params(2) = New SqlParameter("@NTF", SqlDbType.Decimal)
            params(2).Value = oCustomClass.NTF
            params(3) = New SqlParameter("@EffectiveRate", SqlDbType.Decimal)
            params(3).Value = oCustomClass.EffectiveRate
            params(4) = New SqlParameter("@FlatRate", SqlDbType.Decimal)
            params(4).Value = oCustomClass.FlatRate
            params(5) = New SqlParameter("@SupplierRate", SqlDbType.Decimal)
            params(5).Value = oCustomClass.SupplierRate
            params(6) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params(6).Value = oCustomClass.PaymentFrequency
            params(7) = New SqlParameter("@FirstInstallment", SqlDbType.Char, 2)
            params(7).Value = oCustomClass.FirstInstallment
            params(8) = New SqlParameter("@NumOfInstallment", SqlDbType.SmallInt)
            params(8).Value = oCustomClass.NumOfInstallment
            params(9) = New SqlParameter("@GracePeriod", SqlDbType.TinyInt)
            params(9).Value = oCustomClass.GracePeriod
            params(10) = New SqlParameter("@GracePeriodType", SqlDbType.Char, 1)
            params(10).Value = oCustomClass.GracePeriodType
            params(11) = New SqlParameter("@BusDate", SqlDbType.DateTime)
            params(11).Value = oCustomClass.BusDate
            params(12) = New SqlParameter("@DiffRateAmount", SqlDbType.Decimal)
            params(12).Value = oCustomClass.DiffRate
            params(13) = New SqlParameter("@GrossYield", SqlDbType.Decimal)
            params(13).Value = oCustomClass.GrossYield
            params(14) = New SqlParameter("@OutstandingPrincipal", SqlDbType.Decimal)
            params(14).Value = Temp_PrincipleAmount
            params(15) = New SqlParameter("@OutstandingInterest", SqlDbType.Decimal)
            params(15).Value = Temp_InterestAmount
            params(16) = New SqlParameter("@FloatingPeriod", SqlDbType.Char, 1)
            params(16).Value = oCustomClass.FloatingPeriod
            params(17) = New SqlParameter("@Flag", SqlDbType.VarChar, 20)
            params(17).Value = oCustomClass.Flag
            params(18) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
            params(18).Value = oCustomClass.AdministrationFee
            params(19) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal)
            params(19).Value = oCustomClass.ProvisionFee
            params(20) = New SqlParameter("@PolaAngsuran", SqlDbType.VarChar, 10)
            params(20).Value = oCustomClass.PolaAngsuran
            params(21) = New SqlParameter("@TidakAngsur", SqlDbType.Decimal)
            params(21).Value = oCustomClass.TidakAngsur
            params(22) = New SqlParameter("@PotongDanaCair", SqlDbType.Bit)
            params(22).Value = oCustomClass.PotongDanaCair
            params(23) = New SqlParameter("@AngsuranBayarDealer", SqlDbType.Bit)
            params(23).Value = oCustomClass.AngsuranBayarDealer
            params(24) = New SqlParameter("@TotalOTRSupplier", SqlDbType.Decimal)
            params(24).Value = oCustomClass.TotalOTRSupplier
            params(25) = New SqlParameter("@DPSupplier", SqlDbType.Decimal)
            params(25).Value = oCustomClass.DPSupplier
            params(26) = New SqlParameter("@NTFSupplier", SqlDbType.Decimal)
            params(26).Value = oCustomClass.NTFSupplier
            params(27) = New SqlParameter("@TenorSupplier", SqlDbType.SmallInt)
            params(27).Value = oCustomClass.TenorSupplier
            params(28) = New SqlParameter("@FlatRateSupplier", SqlDbType.Decimal)
            params(28).Value = oCustomClass.FlatRateSupplier
            params(29) = New SqlParameter("@EffectiveRateSupplier", SqlDbType.Decimal)
            params(29).Value = oCustomClass.EffectiveRateSupplier
            params(30) = New SqlParameter("@TotalBunga", SqlDbType.Decimal)
            params(30).Value = oCustomClass.TotalBunga
            params(31) = New SqlParameter("@TotalBungaSupplier", SqlDbType.Decimal)
            params(31).Value = oCustomClass.TotalBungaSupplier
            params(32) = New SqlParameter("@NilaiKontrak", SqlDbType.Decimal)
            params(32).Value = oCustomClass.NilaiKontrak
            params(33) = New SqlParameter("@NilaiKontrakSupplier", SqlDbType.Decimal)
            params(33).Value = oCustomClass.NilaiKontrakSupplier
            params(34) = New SqlParameter("@InstallmentAmountSupplier", SqlDbType.Decimal)
            params(34).Value = oCustomClass.InstallmentAmountSupplier
            params(35) = New SqlParameter("@AngsuranTidakSamaSupplier", SqlDbType.Decimal)
            params(35).Value = oCustomClass.AngsuranTidakSamaSupplier
            params(36) = New SqlParameter("@InstallmentUnpaid", SqlDbType.Decimal)
            params(36).Value = oCustomClass.InstallmentUnpaid
            params(37) = New SqlParameter("@downpayment", SqlDbType.Decimal)
            params(37).Value = oCustomClass.DownPayment
            params(38) = New SqlParameter("@RefundBungaPercent", SqlDbType.Decimal)
            params(38).Value = oCustomClass.RefundBungaPercent
            params(39) = New SqlParameter("@RefundBungaAmount", SqlDbType.Money)
            params(39).Value = oCustomClass.RefundBungaAmount
            params(40) = New SqlParameter("@BungaNettEff", SqlDbType.Decimal)
            params(40).Value = oCustomClass.BungaNettEff
            params(41) = New SqlParameter("@BungaNettFlat", SqlDbType.Decimal)
            params(41).Value = oCustomClass.BungaNettFlat

            params(42) = New SqlParameter("@AlokasiInsentifRefundBunga", SqlDbType.Decimal)
            params(43) = New SqlParameter("@AlokasiInsentifRefundBungaPercent", SqlDbType.Decimal)
            params(44) = New SqlParameter("@TitipanRefundBunga", SqlDbType.Decimal)
            params(45) = New SqlParameter("@TitipanRefundBungaPercent", SqlDbType.Decimal)
            params(46) = New SqlParameter("@AlokasiInsentifPremi", SqlDbType.Decimal)
            params(47) = New SqlParameter("@AlokasiInsentifPremiPercent", SqlDbType.Decimal)
            params(48) = New SqlParameter("@AlokasiProgresifPremi", SqlDbType.Decimal)
            params(49) = New SqlParameter("@AlokasiProgresifPremiPercent", SqlDbType.Decimal)
            params(50) = New SqlParameter("@SubsidiBungaPremi", SqlDbType.Decimal)
            params(51) = New SqlParameter("@SubsidiBungaPremiPercent", SqlDbType.Decimal)
            params(52) = New SqlParameter("@PendapatanPremi", SqlDbType.Decimal)
            params(53) = New SqlParameter("@PendapatanPremiPercent", SqlDbType.Decimal)
            params(54) = New SqlParameter("@ProvisiPercent", SqlDbType.Decimal)
            params(55) = New SqlParameter("@AlokasiInsentifProvisi", SqlDbType.Decimal)
            params(56) = New SqlParameter("@AlokasiInsentifProvisiPercent", SqlDbType.Decimal)
            params(57) = New SqlParameter("@SubsidiBungaProvisi", SqlDbType.Decimal)
            params(58) = New SqlParameter("@SubsidiBungaProvisiPercent", SqlDbType.Decimal)
            params(59) = New SqlParameter("@TitipanProvisi", SqlDbType.Decimal)
            params(60) = New SqlParameter("@TitipanProvisiPercent", SqlDbType.Decimal)
            params(61) = New SqlParameter("@AlokasiInsentifBiayaLain", SqlDbType.Decimal)
            params(62) = New SqlParameter("@AlokasiInsentifBiayaLainPercent", SqlDbType.Decimal)
            params(63) = New SqlParameter("@SubsidiBungaBiayaLain", SqlDbType.Decimal)
            params(64) = New SqlParameter("@SubsidiBungaBiayaLainPercent", SqlDbType.Decimal)
            params(65) = New SqlParameter("@TitipanBiayaLain", SqlDbType.Decimal)
            params(66) = New SqlParameter("@TitipanBiayaLainPercent", SqlDbType.Decimal)
            params(67) = New SqlParameter("@SubsidiBungaDealer", SqlDbType.Decimal)
            params(68) = New SqlParameter("@SubsidiAngsuran", SqlDbType.Decimal)
            params(69) = New SqlParameter("@RefundAdminPercent", SqlDbType.Decimal)
            params(70) = New SqlParameter("@RefundAdminAmount", SqlDbType.Decimal)
            params(71) = New SqlParameter("@RefundPremiPercent", SqlDbType.Decimal)
            params(72) = New SqlParameter("@RefundPremiAmount", SqlDbType.Decimal)
            params(73) = New SqlParameter("@RefundProvisiPercent", SqlDbType.Decimal)
            params(74) = New SqlParameter("@RefundProvisiAmount", SqlDbType.Decimal)
            params(75) = New SqlParameter("@GabungRefundSupplier", SqlDbType.Bit)
            params(76) = New SqlParameter("@RateIRR", SqlDbType.Decimal)
            params(77) = New SqlParameter("@UppingBungaRate", SqlDbType.Decimal)
            params(78) = New SqlParameter("@UppingBungaAmount", SqlDbType.Decimal)
            params(79) = New SqlParameter("@UppingBungaFlatRate", SqlDbType.Decimal)
            params(80) = New SqlParameter("@GracePeriodAngsuran", SqlDbType.Int)

            params(42).Value = oCustomClass.AlokasiInsentifRefundBunga
            params(43).Value = oCustomClass.AlokasiInsentifRefundBungaPercent
            params(44).Value = oCustomClass.TitipanRefundBunga
            params(45).Value = oCustomClass.TitipanRefundBungaPercent
            params(46).Value = oCustomClass.AlokasiInsentifPremi
            params(47).Value = oCustomClass.AlokasiInsentifPremiPercent
            params(48).Value = oCustomClass.AlokasiProgresifPremi
            params(49).Value = oCustomClass.AlokasiProgresifPremiPercent
            params(50).Value = oCustomClass.SubsidiBungaPremi
            params(51).Value = oCustomClass.SubsidiBungaPremiPercent
            params(52).Value = oCustomClass.PendapatanPremi
            params(53).Value = oCustomClass.PendapatanPremiPercent
            params(54).Value = oCustomClass.ProvisiPercent
            params(55).Value = oCustomClass.AlokasiInsentifProvisi
            params(56).Value = oCustomClass.AlokasiInsentifProvisiPercent
            params(57).Value = oCustomClass.SubsidiBungaProvisi
            params(58).Value = oCustomClass.SubsidiBungaProvisiPercent
            params(59).Value = oCustomClass.TitipanProvisi
            params(60).Value = oCustomClass.TitipanProvisiPercent
            params(61).Value = oCustomClass.AlokasiInsentifBiayaLain
            params(62).Value = oCustomClass.AlokasiInsentifBiayaLainPercent
            params(63).Value = oCustomClass.SubsidiBungaBiayaLain
            params(64).Value = oCustomClass.SubsidiBungaBiayaLainPercent
            params(65).Value = oCustomClass.TitipanBiayaLain
            params(66).Value = oCustomClass.TitipanBiayaLainPercent
            params(67).Value = oCustomClass.SubsidiBungaDealer
            params(68).Value = oCustomClass.SubsidiAngsuran
            params(69).Value = oCustomClass.RefundAdminPercent
            params(70).Value = oCustomClass.RefundAdminAmount
            params(71).Value = oCustomClass.RefundPremiPercent
            params(72).Value = oCustomClass.RefundPremiAmount
            params(73).Value = oCustomClass.RefundProvisiPercent
            params(74).Value = oCustomClass.RefundProvisiAmount
            params(75).Value = oCustomClass.GabungRefundSupplier
            params(76).Value = oCustomClass.RateIRR
            params(77).Value = oCustomClass.UppingBungaRate
            params(78).Value = oCustomClass.UppingBungaAmount
            params(79).Value = oCustomClass.UppingBungaFlatRate
            params(80).Value = oCustomClass.GracePeriodAngsuran

            params(81) = New SqlParameter("@DPKaroseriAmount", SqlDbType.Decimal)
            params(81).Value = oCustomClass.DPKaroseriAmount

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFinancialDataSave, params)

            'ini kalo balon
            If oCustomClass.PolaAngsuran = "NOTARIIL" Then
                If oCustomClass.MydataSet.Tables("AmortTable").Rows.Count > 0 Then
                    Dim params1() As SqlParameter = New SqlParameter(12) {}
                    Dim intLoop As Integer
                    Dim output As String
                    params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                    params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                    params1(2) = New SqlParameter("@InsSeqNo", SqlDbType.SmallInt)
                    params1(3) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
                    params1(4) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
                    params1(5) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
                    params1(6) = New SqlParameter("@OutstandingPrincipal", SqlDbType.Decimal)
                    params1(7) = New SqlParameter("@OutstandingInterest", SqlDbType.Decimal)
                    params1(8) = New SqlParameter("@Output", SqlDbType.VarChar, 50)
                    params1(9) = New SqlParameter("@Flag", SqlDbType.VarChar, 20)
                    params1(10) = New SqlParameter("@RefundInterest", SqlDbType.Decimal)
                    params1(11) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
                    params1(12) = New SqlParameter("@Provisi", SqlDbType.Decimal)


                    For intLoop = 0 To oCustomClass.MydataSet.Tables("AmortTable").Rows.Count - 1
                        params1(0).Value = oCustomClass.BranchId
                        params1(1).Value = oCustomClass.AppID
                        params1(2).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("No")
                        params1(3).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("Installment")
                        params1(4).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("Principal")
                        params1(5).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("Interest")
                        params1(6).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("PrincBalance")
                        params1(7).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("PrincInterest")
                        params1(8).Direction = ParameterDirection.Output
                        params1(9).Value = oCustomClass.Flag
                        params1(10).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("RefundInterest")
                        params1(11).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("AdminFee")
                        params1(12).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("Provisi")
                        Temp_PrincipleAmount += CType(params1(4).Value, Double)
                        Temp_InterestAmount += CType(params1(5).Value, Double)

                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFinancialDataSave2, params1)

                        output = CStr(params1(8).Value)

                        If output <> "" Then
                            transaction.Rollback()
                            oReturnValue.Output = output
                            Return oReturnValue
                        End If
                    Next
                    '==============

                End If
            Else
                If oCustomClass.MydataSet.Tables("AmortTable").Rows.Count > 0 Then
                    Dim params2() As SqlParameter = New SqlParameter(8) {}
                    Dim intLoop As Integer
                    Dim output As String
                    params2(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                    params2(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                    params2(2) = New SqlParameter("@InsSeqNo", SqlDbType.SmallInt)
                    params2(3) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
                    params2(4) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
                    params2(5) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
                    params2(6) = New SqlParameter("@OutstandingPrincipal", SqlDbType.Decimal)
                    params2(7) = New SqlParameter("@OutstandingInterest", SqlDbType.Decimal)
                    params2(8) = New SqlParameter("@Output", SqlDbType.VarChar, 50)

                    For intLoop = 0 To oCustomClass.MydataSet.Tables("AmortTable").Rows.Count - 1
                        params2(0).Value = oCustomClass.BranchId
                        params2(1).Value = oCustomClass.AppID
                        params2(2).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("InsSeqNo")
                        params2(3).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("InstallmentAmount")
                        params2(4).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("PrincipalAmount")
                        params2(5).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("InterestAmount")
                        params2(6).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("OutStandingPrincipal")
                        params2(7).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("OutStandingInterest")
                        params2(8).Direction = ParameterDirection.Output
                        'Temp_PrincipleAmount += CType(params1(4).Value, Double)
                        'Temp_InterestAmount += CType(params1(5).Value, Double)

                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "sp_SaveAmortisasiFromUI", params2)

                        output = CStr(params2(8).Value)

                        If output <> "" Then
                            transaction.Rollback()
                            oReturnValue.Output = output
                            Return oReturnValue
                        End If
                    Next
                    '==============

                End If
                'Dim parInst() As SqlParameter = New SqlParameter(7) {}

                'parInst(0) = New SqlParameter("@Rate", SqlDbType.Float)
                'parInst(0).Value = oCustomClass.EffectiveRate

                'parInst(1) = New SqlParameter("@PrincipalAmount", SqlDbType.Float)
                'parInst(1).Value = oCustomClass.NTF

                'parInst(2) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
                'parInst(2).Value = oCustomClass.NumOfInstallment

                'parInst(3) = New SqlParameter("@DueDate", SqlDbType.SmallDateTime)
                'parInst(3).Value = oCustomClass.EffectiveDate

                'parInst(4) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
                'parInst(4).Value = oCustomClass.installmentamount

                'parInst(5) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
                'parInst(5).Value = oCustomClass.AppID

                'parInst(6) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
                'parInst(6).Value = oCustomClass.BranchId

                'parInst(7) = New SqlParameter("@Term", SqlDbType.SmallInt)
                'parInst(7).Value = oCustomClass.Term

                'Dim spInstallmentSchecule As String
                'If oCustomClass.FirstInstallment = "AR" Then
                '    spInstallmentSchecule = "spSimulasiAngsuran"
                'Else ' oCustomClass.FirstInstallment = "AD"
                '    spInstallmentSchecule = "spSimulasiAngsuranAD"
                'End If
                'SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spInstallmentSchecule, parInst)
            End If

            'Update installment schedule dueDate

            Dim par() As SqlParameter = New SqlParameter(2) {}

            par(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            par(0).Value = oCustomClass.AppID
            par(1) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            par(1).Value = oCustomClass.BusinessDate
            par(2) = New SqlParameter("@TanggalEfektif", SqlDbType.SmallDateTime)
            par(2).Value = oCustomClass.EffectiveDate

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spInstallmentSchDueDateUpdate", par)

            transaction.Commit()
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function SaveFinancialDataOperatingLease(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)

        Dim transaction As SqlTransaction = Nothing
        Dim Temp_PrincipleAmount As Double = 0
        Dim Temp_InterestAmount As Double = 0
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(82) {}

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.AppID
            params(2) = New SqlParameter("@NTF", SqlDbType.Decimal)
            params(2).Value = oCustomClass.NTF
            params(3) = New SqlParameter("@EffectiveRate", SqlDbType.Decimal)
            params(3).Value = oCustomClass.EffectiveRate
            params(4) = New SqlParameter("@FlatRate", SqlDbType.Decimal)
            params(4).Value = oCustomClass.FlatRate
            params(5) = New SqlParameter("@SupplierRate", SqlDbType.Decimal)
            params(5).Value = oCustomClass.SupplierRate
            params(6) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params(6).Value = oCustomClass.PaymentFrequency
            params(7) = New SqlParameter("@FirstInstallment", SqlDbType.Char, 2)
            params(7).Value = oCustomClass.FirstInstallment
            params(8) = New SqlParameter("@NumOfInstallment", SqlDbType.SmallInt)
            params(8).Value = oCustomClass.NumOfInstallment
            params(9) = New SqlParameter("@GracePeriod", SqlDbType.TinyInt)
            params(9).Value = oCustomClass.GracePeriod
            params(10) = New SqlParameter("@GracePeriodType", SqlDbType.Char, 1)
            params(10).Value = oCustomClass.GracePeriodType
            params(11) = New SqlParameter("@BusDate", SqlDbType.DateTime)
            params(11).Value = oCustomClass.BusDate
            params(12) = New SqlParameter("@DiffRateAmount", SqlDbType.Decimal)
            params(12).Value = oCustomClass.DiffRate
            params(13) = New SqlParameter("@GrossYield", SqlDbType.Decimal)
            params(13).Value = oCustomClass.GrossYield
            params(14) = New SqlParameter("@OutstandingPrincipal", SqlDbType.Decimal)
            params(14).Value = Temp_PrincipleAmount
            params(15) = New SqlParameter("@OutstandingInterest", SqlDbType.Decimal)
            params(15).Value = Temp_InterestAmount
            params(16) = New SqlParameter("@FloatingPeriod", SqlDbType.Char, 1)
            params(16).Value = oCustomClass.FloatingPeriod
            params(17) = New SqlParameter("@Flag", SqlDbType.VarChar, 20)
            params(17).Value = oCustomClass.Flag
            params(18) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
            params(18).Value = oCustomClass.AdministrationFee
            params(19) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal)
            params(19).Value = oCustomClass.ProvisionFee
            params(20) = New SqlParameter("@PolaAngsuran", SqlDbType.VarChar, 10)
            params(20).Value = oCustomClass.PolaAngsuran
            params(21) = New SqlParameter("@TidakAngsur", SqlDbType.Decimal)
            params(21).Value = oCustomClass.TidakAngsur
            params(22) = New SqlParameter("@PotongDanaCair", SqlDbType.Bit)
            params(22).Value = oCustomClass.PotongDanaCair
            params(23) = New SqlParameter("@AngsuranBayarDealer", SqlDbType.Bit)
            params(23).Value = oCustomClass.AngsuranBayarDealer
            params(24) = New SqlParameter("@TotalOTRSupplier", SqlDbType.Decimal)
            params(24).Value = oCustomClass.TotalOTRSupplier
            params(25) = New SqlParameter("@DPSupplier", SqlDbType.Decimal)
            params(25).Value = oCustomClass.DPSupplier
            params(26) = New SqlParameter("@NTFSupplier", SqlDbType.Decimal)
            params(26).Value = oCustomClass.NTFSupplier
            params(27) = New SqlParameter("@TenorSupplier", SqlDbType.SmallInt)
            params(27).Value = oCustomClass.TenorSupplier
            params(28) = New SqlParameter("@FlatRateSupplier", SqlDbType.Decimal)
            params(28).Value = oCustomClass.FlatRateSupplier
            params(29) = New SqlParameter("@EffectiveRateSupplier", SqlDbType.Decimal)
            params(29).Value = oCustomClass.EffectiveRateSupplier
            params(30) = New SqlParameter("@TotalBunga", SqlDbType.Decimal)
            params(30).Value = oCustomClass.TotalBunga
            params(31) = New SqlParameter("@TotalBungaSupplier", SqlDbType.Decimal)
            params(31).Value = oCustomClass.TotalBungaSupplier
            params(32) = New SqlParameter("@NilaiKontrak", SqlDbType.Decimal)
            params(32).Value = oCustomClass.NilaiKontrak
            params(33) = New SqlParameter("@NilaiKontrakSupplier", SqlDbType.Decimal)
            params(33).Value = oCustomClass.NilaiKontrakSupplier
            params(34) = New SqlParameter("@InstallmentAmountSupplier", SqlDbType.Decimal)
            params(34).Value = oCustomClass.InstallmentAmountSupplier
            params(35) = New SqlParameter("@AngsuranTidakSamaSupplier", SqlDbType.Decimal)
            params(35).Value = oCustomClass.AngsuranTidakSamaSupplier
            params(36) = New SqlParameter("@InstallmentUnpaid", SqlDbType.Decimal)
            params(36).Value = oCustomClass.InstallmentUnpaid
            params(37) = New SqlParameter("@downpayment", SqlDbType.Decimal)
            params(37).Value = oCustomClass.DownPayment
            params(38) = New SqlParameter("@RefundBungaPercent", SqlDbType.Decimal)
            params(38).Value = oCustomClass.RefundBungaPercent
            params(39) = New SqlParameter("@RefundBungaAmount", SqlDbType.Money)
            params(39).Value = oCustomClass.RefundBungaAmount
            params(40) = New SqlParameter("@BungaNettEff", SqlDbType.Decimal)
            params(40).Value = oCustomClass.BungaNettEff
            params(41) = New SqlParameter("@BungaNettFlat", SqlDbType.Decimal)
            params(41).Value = oCustomClass.BungaNettFlat

            params(42) = New SqlParameter("@AlokasiInsentifRefundBunga", SqlDbType.Decimal)
            params(43) = New SqlParameter("@AlokasiInsentifRefundBungaPercent", SqlDbType.Decimal)
            params(44) = New SqlParameter("@TitipanRefundBunga", SqlDbType.Decimal)
            params(45) = New SqlParameter("@TitipanRefundBungaPercent", SqlDbType.Decimal)
            params(46) = New SqlParameter("@AlokasiInsentifPremi", SqlDbType.Decimal)
            params(47) = New SqlParameter("@AlokasiInsentifPremiPercent", SqlDbType.Decimal)
            params(48) = New SqlParameter("@AlokasiProgresifPremi", SqlDbType.Decimal)
            params(49) = New SqlParameter("@AlokasiProgresifPremiPercent", SqlDbType.Decimal)
            params(50) = New SqlParameter("@SubsidiBungaPremi", SqlDbType.Decimal)
            params(51) = New SqlParameter("@SubsidiBungaPremiPercent", SqlDbType.Decimal)
            params(52) = New SqlParameter("@PendapatanPremi", SqlDbType.Decimal)
            params(53) = New SqlParameter("@PendapatanPremiPercent", SqlDbType.Decimal)
            params(54) = New SqlParameter("@ProvisiPercent", SqlDbType.Decimal)
            params(55) = New SqlParameter("@AlokasiInsentifProvisi", SqlDbType.Decimal)
            params(56) = New SqlParameter("@AlokasiInsentifProvisiPercent", SqlDbType.Decimal)
            params(57) = New SqlParameter("@SubsidiBungaProvisi", SqlDbType.Decimal)
            params(58) = New SqlParameter("@SubsidiBungaProvisiPercent", SqlDbType.Decimal)
            params(59) = New SqlParameter("@TitipanProvisi", SqlDbType.Decimal)
            params(60) = New SqlParameter("@TitipanProvisiPercent", SqlDbType.Decimal)
            params(61) = New SqlParameter("@AlokasiInsentifBiayaLain", SqlDbType.Decimal)
            params(62) = New SqlParameter("@AlokasiInsentifBiayaLainPercent", SqlDbType.Decimal)
            params(63) = New SqlParameter("@SubsidiBungaBiayaLain", SqlDbType.Decimal)
            params(64) = New SqlParameter("@SubsidiBungaBiayaLainPercent", SqlDbType.Decimal)
            params(65) = New SqlParameter("@TitipanBiayaLain", SqlDbType.Decimal)
            params(66) = New SqlParameter("@TitipanBiayaLainPercent", SqlDbType.Decimal)
            params(67) = New SqlParameter("@SubsidiBungaDealer", SqlDbType.Decimal)
            params(68) = New SqlParameter("@SubsidiAngsuran", SqlDbType.Decimal)
            params(69) = New SqlParameter("@RefundAdminPercent", SqlDbType.Decimal)
            params(70) = New SqlParameter("@RefundAdminAmount", SqlDbType.Decimal)
            params(71) = New SqlParameter("@RefundPremiPercent", SqlDbType.Decimal)
            params(72) = New SqlParameter("@RefundPremiAmount", SqlDbType.Decimal)
            params(73) = New SqlParameter("@RefundProvisiPercent", SqlDbType.Decimal)
            params(74) = New SqlParameter("@RefundProvisiAmount", SqlDbType.Decimal)
            params(75) = New SqlParameter("@GabungRefundSupplier", SqlDbType.Bit)
            params(76) = New SqlParameter("@RateIRR", SqlDbType.Decimal)

            params(77) = New SqlParameter("@InsuranceMonthlyOL", SqlDbType.Decimal)
            params(78) = New SqlParameter("@MaintenanceFeeOL", SqlDbType.Decimal)
            params(79) = New SqlParameter("@STNKFeeOL", SqlDbType.Decimal)
            params(80) = New SqlParameter("@AdminFeeOL", SqlDbType.Decimal)
            params(81) = New SqlParameter("@ProvisiOL", SqlDbType.Decimal)
            params(82) = New SqlParameter("@BasicLease", SqlDbType.Decimal)


            params(42).Value = oCustomClass.AlokasiInsentifRefundBunga
            params(43).Value = oCustomClass.AlokasiInsentifRefundBungaPercent
            params(44).Value = oCustomClass.TitipanRefundBunga
            params(45).Value = oCustomClass.TitipanRefundBungaPercent
            params(46).Value = oCustomClass.AlokasiInsentifPremi
            params(47).Value = oCustomClass.AlokasiInsentifPremiPercent
            params(48).Value = oCustomClass.AlokasiProgresifPremi
            params(49).Value = oCustomClass.AlokasiProgresifPremiPercent
            params(50).Value = oCustomClass.SubsidiBungaPremi
            params(51).Value = oCustomClass.SubsidiBungaPremiPercent
            params(52).Value = oCustomClass.PendapatanPremi
            params(53).Value = oCustomClass.PendapatanPremiPercent
            params(54).Value = oCustomClass.ProvisiPercent
            params(55).Value = oCustomClass.AlokasiInsentifProvisi
            params(56).Value = oCustomClass.AlokasiInsentifProvisiPercent
            params(57).Value = oCustomClass.SubsidiBungaProvisi
            params(58).Value = oCustomClass.SubsidiBungaProvisiPercent
            params(59).Value = oCustomClass.TitipanProvisi
            params(60).Value = oCustomClass.TitipanProvisiPercent
            params(61).Value = oCustomClass.AlokasiInsentifBiayaLain
            params(62).Value = oCustomClass.AlokasiInsentifBiayaLainPercent
            params(63).Value = oCustomClass.SubsidiBungaBiayaLain
            params(64).Value = oCustomClass.SubsidiBungaBiayaLainPercent
            params(65).Value = oCustomClass.TitipanBiayaLain
            params(66).Value = oCustomClass.TitipanBiayaLainPercent
            params(67).Value = oCustomClass.SubsidiBungaDealer
            params(68).Value = oCustomClass.SubsidiAngsuran
            params(69).Value = oCustomClass.RefundAdminPercent
            params(70).Value = oCustomClass.RefundAdminAmount
            params(71).Value = oCustomClass.RefundPremiPercent
            params(72).Value = oCustomClass.RefundPremiAmount
            params(73).Value = oCustomClass.RefundProvisiPercent
            params(74).Value = oCustomClass.RefundProvisiAmount
            params(75).Value = oCustomClass.GabungRefundSupplier
            params(76).Value = oCustomClass.RateIRR

            params(77).Value = oCustomClass.InsuranceMonthlyOL
            params(78).Value = oCustomClass.MaintenanceFeeOL
            params(79).Value = oCustomClass.STNKFeeOL
            params(80).Value = oCustomClass.AdminFeeOL
            params(81).Value = oCustomClass.ProvisiOL
            params(82).Value = oCustomClass.BasicLease

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFinancialDataSaveOperatingLease, params)

            'ini kalo balon
            If oCustomClass.PolaAngsuran = "NOTARIIL" Then
                If oCustomClass.MydataSet.Tables("AmortTable").Rows.Count > 0 Then
                    Dim params1() As SqlParameter = New SqlParameter(12) {}
                    Dim intLoop As Integer
                    Dim output As String
                    params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                    params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                    params1(2) = New SqlParameter("@InsSeqNo", SqlDbType.SmallInt)
                    params1(3) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
                    params1(4) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
                    params1(5) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
                    params1(6) = New SqlParameter("@OutstandingPrincipal", SqlDbType.Decimal)
                    params1(7) = New SqlParameter("@OutstandingInterest", SqlDbType.Decimal)
                    params1(8) = New SqlParameter("@Output", SqlDbType.VarChar, 50)
                    params1(9) = New SqlParameter("@Flag", SqlDbType.VarChar, 20)
                    params1(10) = New SqlParameter("@RefundInterest", SqlDbType.Decimal)
                    params1(11) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
                    params1(12) = New SqlParameter("@Provisi", SqlDbType.Decimal)


                    For intLoop = 0 To oCustomClass.MydataSet.Tables("AmortTable").Rows.Count - 1
                        params1(0).Value = oCustomClass.BranchId
                        params1(1).Value = oCustomClass.AppID
                        params1(2).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("No")
                        params1(3).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("Installment")
                        params1(4).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("Principal")
                        params1(5).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("Interest")
                        params1(6).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("PrincBalance")
                        params1(7).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("PrincInterest")
                        params1(8).Direction = ParameterDirection.Output
                        params1(9).Value = oCustomClass.Flag
                        params1(10).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("RefundInterest")
                        params1(11).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("AdminFee")
                        params1(12).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("Provisi")
                        Temp_PrincipleAmount += CType(params1(4).Value, Double)
                        Temp_InterestAmount += CType(params1(5).Value, Double)

                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFinancialDataSave2, params1)

                        output = CStr(params1(8).Value)

                        If output <> "" Then
                            transaction.Rollback()
                            oReturnValue.Output = output
                            Return oReturnValue
                        End If
                    Next
                    '==============

                End If
            Else
                Dim parInst() As SqlParameter = New SqlParameter(7) {}

                parInst(0) = New SqlParameter("@Rate", SqlDbType.Float)
                parInst(0).Value = oCustomClass.EffectiveRate

                parInst(1) = New SqlParameter("@PrincipalAmount", SqlDbType.Float)
                parInst(1).Value = oCustomClass.NTF

                parInst(2) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
                parInst(2).Value = oCustomClass.NumOfInstallment

                parInst(3) = New SqlParameter("@DueDate", SqlDbType.SmallDateTime)
                parInst(3).Value = oCustomClass.EffectiveDate

                parInst(4) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
                parInst(4).Value = oCustomClass.installmentamount

                parInst(5) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
                parInst(5).Value = oCustomClass.AppID

                parInst(6) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
                parInst(6).Value = oCustomClass.BranchId

                parInst(7) = New SqlParameter("@Term", SqlDbType.SmallInt)
                parInst(7).Value = oCustomClass.Term

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spSimulasiAngsuran", parInst)
            End If

            'Update installment schedule dueDate

            Dim par() As SqlParameter = New SqlParameter(2) {}

            par(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            par(0).Value = oCustomClass.AppID
            par(1) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            par(1).Value = oCustomClass.BusinessDate
            par(2) = New SqlParameter("@TanggalEfektif", SqlDbType.SmallDateTime)
            par(2).Value = oCustomClass.EffectiveDate

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spInstallmentSchDueDateUpdate", par)

            transaction.Commit()
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function SaveFinancialDataSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim Temp_PrincipleAmount As Double = 0
        Dim Temp_InterestAmount As Double = 0

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(18) {}

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.AppID
            params(2) = New SqlParameter("@NTF", SqlDbType.Decimal)
            params(2).Value = oCustomClass.NTF
            params(3) = New SqlParameter("@EffectiveRate", SqlDbType.Decimal)
            params(3).Value = oCustomClass.EffectiveRate
            params(4) = New SqlParameter("@FlatRate", SqlDbType.Decimal)
            params(4).Value = oCustomClass.FlatRate
            params(5) = New SqlParameter("@SupplierRate", SqlDbType.Decimal)
            params(5).Value = oCustomClass.SupplierRate
            params(6) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params(6).Value = oCustomClass.PaymentFrequency
            params(7) = New SqlParameter("@FirstInstallment", SqlDbType.Char, 2)
            params(7).Value = oCustomClass.FirstInstallment
            params(8) = New SqlParameter("@NumOfInstallment", SqlDbType.SmallInt)
            params(8).Value = oCustomClass.NumOfInstallment
            params(9) = New SqlParameter("@GracePeriod", SqlDbType.TinyInt)
            params(9).Value = oCustomClass.GracePeriod
            params(10) = New SqlParameter("@GracePeriodType", SqlDbType.Char, 1)
            params(10).Value = oCustomClass.GracePeriodType
            params(11) = New SqlParameter("@BusDate", SqlDbType.DateTime)
            params(11).Value = oCustomClass.BusDate
            params(12) = New SqlParameter("@DiffRateAmount", SqlDbType.Decimal)
            params(12).Value = oCustomClass.DiffRate
            params(13) = New SqlParameter("@GrossYield", SqlDbType.Decimal)
            params(13).Value = oCustomClass.GrossYield
            params(14) = New SqlParameter("@OutstandingPrincipal", SqlDbType.Decimal)
            params(14).Value = Temp_PrincipleAmount
            params(15) = New SqlParameter("@OutstandingInterest", SqlDbType.Decimal)
            params(15).Value = Temp_InterestAmount
            params(16) = New SqlParameter("@FloatingPeriod", SqlDbType.Char, 1)
            params(16).Value = oCustomClass.FloatingPeriod
            params(17) = New SqlParameter("@Flag", SqlDbType.VarChar, 20)
            params(17).Value = oCustomClass.Flag
            params(18) = New SqlParameter("@InterestType", SqlDbType.Char, 2)
            params(18).Value = oCustomClass.InterestType

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure,
            "spFinancialDataSaveSiml", params)

            If oCustomClass.MydataSet.Tables("AmortTable").Rows.Count > 0 Then
                Dim params1() As SqlParameter = New SqlParameter(9) {}
                Dim intLoop As Integer
                Dim output As String

                params1(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params1(2) = New SqlParameter("@InsSeqNo", SqlDbType.SmallInt)
                params1(3) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
                params1(4) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
                params1(5) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
                params1(6) = New SqlParameter("@OutstandingPrincipal", SqlDbType.Decimal)
                params1(7) = New SqlParameter("@OutstandingInterest", SqlDbType.Decimal)
                params1(8) = New SqlParameter("@Output", SqlDbType.VarChar, 50)
                params1(9) = New SqlParameter("@Flag", SqlDbType.VarChar, 20)

                For intLoop = 0 To oCustomClass.MydataSet.Tables("AmortTable").Rows.Count - 1
                    If intLoop > 0 Then
                        params1(0).Value = oCustomClass.BranchId
                        params1(1).Value = oCustomClass.AppID
                        params1(2).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("No")
                        params1(3).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("Installment")
                        params1(4).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("Principal")
                        params1(5).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("Interest")
                        params1(6).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("PrincBalance")
                        params1(7).Value = oCustomClass.MydataSet.Tables("AmortTable").Rows(intLoop).Item("PrincInterest")
                        params1(8).Direction = ParameterDirection.Output
                        params1(9).Value = oCustomClass.Flag

                        Temp_PrincipleAmount += CType(params1(4).Value, Double)
                        Temp_InterestAmount += CType(params1(5).Value, Double)

                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure,
                        "spFinancialDataSave2Siml", params1)
                        output = CStr(params1(8).Value)

                        If output <> "" Then
                            transaction.Rollback()
                            oReturnValue.Output = output
                            Return oReturnValue
                        End If
                    End If
                Next
            End If


            transaction.Commit()
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.SaveFinancialData")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

#End Region

#Region "View Financial Data"
    Public Function GetViewFinancialData(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim intLoop As Integer
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim spViewFinancialData As String
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = oCustomClass.AppID
            For intLoop = 0 To 3
                Select Case intLoop
                    Case 0
                        spViewFinancialData = "spViewFinancialData"
                        oReturnValue.Data1 = SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spViewFinancialData, params).Tables(0)
                    Case 1
                        spViewFinancialData = "spViewFinancialData" + "Amortization2"
                        oReturnValue.data2 = SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spViewFinancialData, params).Tables(0)
                    Case 2
                        spViewFinancialData = "spViewFinancialData" + "Amortization1"
                        oReturnValue.data3 = SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spViewFinancialData, params).Tables(0)
                End Select
            Next
            transaction.Commit()
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FinancialData", "GetViewFinancialData", exp.Message + exp.StackTrace)
            oReturnValue.Output = "Error: " & exp.Message & ""
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.GetViewFinancialData")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "GetPrincipleAmount"
    Public Function GetPrincipleAmount(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim dblNTF As Double
        Dim intNumInstallment As Integer
        Dim dblPrincipleAmt As Double
        Dim intGracePeriod As Integer
        dblNTF = oCustomClass.NTF
        intNumInstallment = oCustomClass.NumOfInstallment
        intGracePeriod = oCustomClass.GracePeriod

        If intGracePeriod = 0 Then
            dblPrincipleAmt = dblNTF / intNumInstallment
        Else
            dblPrincipleAmt = dblNTF / (intNumInstallment - intGracePeriod)
        End If

        oCustomClass.Principal = dblPrincipleAmt
        Return oCustomClass
    End Function
#End Region

#Region "Save Amortisasi -- Even Principle"
    Public Function SaveAmortisasi(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim params(14) As SqlParameter
        Dim transaction As SqlTransaction = Nothing

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.AppID
            params(2) = New SqlParameter("@NTF", SqlDbType.Decimal)
            params(2).Value = oCustomClass.NTF
            params(3) = New SqlParameter("@effRate", SqlDbType.Decimal)
            params(3).Value = oCustomClass.EffectiveRate
            params(4) = New SqlParameter("@SupplierRate", SqlDbType.Decimal)
            params(4).Value = oCustomClass.SupplierRate
            params(5) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params(5).Value = oCustomClass.PaymentFrequency
            params(6) = New SqlParameter("@InstallmentType", SqlDbType.Char, 2)
            params(6).Value = oCustomClass.FirstInstallment
            params(7) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(7).Value = oCustomClass.Tenor
            params(8) = New SqlParameter("@GracePeriod", SqlDbType.TinyInt)
            params(8).Value = oCustomClass.GracePeriod
            params(9) = New SqlParameter("@GracePeriodType", SqlDbType.Char, 1)
            params(9).Value = oCustomClass.GracePeriodType
            params(10) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(10).Value = oCustomClass.BusDate
            params(11) = New SqlParameter("@DiffRateAmount", SqlDbType.Decimal)
            params(11).Value = oCustomClass.DiffRate
            params(12) = New SqlParameter("@strerror", SqlDbType.Char, 50)
            params(12).Direction = ParameterDirection.Output
            params(13) = New SqlParameter("@Flag", SqlDbType.VarChar, 20)
            params(13).Value = oCustomClass.Flag
            params(14) = New SqlParameter("@RateIRR", SqlDbType.Decimal)
            params(14).Value = oCustomClass.RateIRR
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSaveAmortisasi, params)
            transaction.Commit()
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            'WriteException("FinancialData", "SaveAmortisasi", exp.Message + exp.StackTrace)
            oReturnValue.Output = exp.Message
            'Dim err As New MaxiloanExceptions
            'err.WriteLog("FinancialData.vb - DA", "SaveFinancialData", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.SaveAmortisasi")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function SaveAmortisasiSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim params(13) As SqlParameter
        Dim transaction As SqlTransaction = Nothing

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.AppID
            params(2) = New SqlParameter("@NTF", SqlDbType.Decimal)
            params(2).Value = oCustomClass.NTF
            params(3) = New SqlParameter("@effRate", SqlDbType.Decimal)
            params(3).Value = oCustomClass.EffectiveRate
            params(4) = New SqlParameter("@SupplierRate", SqlDbType.Decimal)
            params(4).Value = oCustomClass.SupplierRate
            params(5) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params(5).Value = oCustomClass.PaymentFrequency
            params(6) = New SqlParameter("@InstallmentType", SqlDbType.Char, 2)
            params(6).Value = oCustomClass.FirstInstallment
            params(7) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(7).Value = oCustomClass.Tenor
            params(8) = New SqlParameter("@GracePeriod", SqlDbType.TinyInt)
            params(8).Value = oCustomClass.GracePeriod
            params(9) = New SqlParameter("@GracePeriodType", SqlDbType.Char, 1)
            params(9).Value = oCustomClass.GracePeriodType
            params(10) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(10).Value = oCustomClass.BusDate
            params(11) = New SqlParameter("@DiffRateAmount", SqlDbType.Decimal)
            params(11).Value = oCustomClass.DiffRate
            params(12) = New SqlParameter("@strerror", SqlDbType.Char, 50)
            params(12).Direction = ParameterDirection.Output
            params(13) = New SqlParameter("@Flag", SqlDbType.VarChar, 20)
            params(13).Value = oCustomClass.Flag
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure,
            "spSaveAmortisationSiml", params)
            transaction.Commit()
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.SaveAmortisasi")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "GetStepUpStepDownType"
    Public Function GetStepUpStepDownType(ByVal oCustomClass As Parameter.FinancialData) As String
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim params(1) As SqlParameter
        Dim strSTtype As String
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.AppID
            strSTtype = CType(SqlHelper.ExecuteScalar(conn, CommandType.Text, qrGetStepUpStepDownType, params), String)
        Catch ex As Exception
            strSTtype = ""
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
        Return strSTtype
    End Function
#End Region

#Region "Save Amortisasi Irregular"
    Public Function SaveAmortisasiIRR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim params(15) As SqlParameter
        Dim paramsInsert(4) As SqlParameter
        Dim transaction As SqlTransaction = Nothing
        Dim objTable As New DataTable
        Dim objRow As DataRow
        Dim inc As Integer

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            objTable = oCustomClass.MydataSet.Tables("Installment")
            inc = 1
            For Each objRow In objTable.Rows
                paramsInsert(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
                paramsInsert(0).Value = oCustomClass.BranchId
                paramsInsert(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
                paramsInsert(1).Value = oCustomClass.AppID
                paramsInsert(2) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
                paramsInsert(2).Value = CDbl(objRow("Amount"))
                paramsInsert(3) = New SqlParameter("@InsSeqno", SqlDbType.Int)
                paramsInsert(3).Value = inc
                paramsInsert(4) = New SqlParameter("@Flag", SqlDbType.VarChar, 20)
                paramsInsert(4).Value = oCustomClass.Flag

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFinancialDataSave3, paramsInsert)
                inc = inc + 1
            Next

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.AppID
            params(2) = New SqlParameter("@NTF", SqlDbType.Decimal)
            params(2).Value = oCustomClass.NTF
            params(3) = New SqlParameter("@effRate", SqlDbType.Decimal)
            params(3).Value = oCustomClass.EffectiveRate
            params(4) = New SqlParameter("@SupplierRate", SqlDbType.Decimal)
            params(4).Value = oCustomClass.SupplierRate
            params(5) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params(5).Value = oCustomClass.PaymentFrequency
            params(6) = New SqlParameter("@InstallmentType", SqlDbType.Char, 2)
            params(6).Value = oCustomClass.FirstInstallment
            params(7) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(7).Value = oCustomClass.Tenor
            params(8) = New SqlParameter("@GracePeriod", SqlDbType.TinyInt)
            params(8).Value = oCustomClass.GracePeriod
            params(9) = New SqlParameter("@GracePeriodType", SqlDbType.Char, 1)
            params(9).Value = oCustomClass.GracePeriodType
            params(10) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(10).Value = oCustomClass.BusDate
            params(11) = New SqlParameter("@DiffRateAmount", SqlDbType.Decimal)
            params(11).Value = oCustomClass.DiffRate
            params(12) = New SqlParameter("@Save", SqlDbType.Int)
            params(12).Value = oCustomClass.IsSave
            params(13) = New SqlParameter("@strerror", SqlDbType.Char, 50)
            params(13).Direction = ParameterDirection.Output
            params(14) = New SqlParameter("@LastInstallment", SqlDbType.Char, 50)
            params(14).Direction = ParameterDirection.Output
            params(15) = New SqlParameter("@RateIRR", SqlDbType.Decimal)
            params(15).Value = oCustomClass.RateIRR

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spSaveAmortisasiIRR, params)
            transaction.Commit()
            If params(14).Value Is DBNull.Value Then
                oReturnValue.installmentamount = 0
            Else
                oReturnValue.installmentamount = CDbl(params(14).Value)
            End If

            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            'WriteException("FinancialData", "SaveAmortisasi", exp.Message + exp.StackTrace)
            oReturnValue.Output = exp.Message
            oReturnValue.installmentamount = 0
            'Dim err As New MaxiloanExceptions
            'err.WriteLog("FinancialData.vb - DA", "SaveFinancialData", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.SaveAmortisasi")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function ListAmortisasiIRR(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim params(1) As SqlParameter
        Dim transaction As SqlTransaction = Nothing

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.AppID

            oReturnValue.ListData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, spListAmortisasiIRR, params).Tables(0)

            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            oReturnValue.Output = exp.Message
            oReturnValue.installmentamount = 0
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.ListAmortisasiIRR")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function SaveAmortisasiIRRSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim params(14) As SqlParameter
        Dim paramsInsert(4) As SqlParameter
        Dim transaction As SqlTransaction = Nothing
        Dim objTable As New DataTable
        Dim objRow As DataRow
        Dim inc As Integer

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            objTable = oCustomClass.MydataSet.Tables("Installment")
            inc = 1
            For Each objRow In objTable.Rows
                paramsInsert(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
                paramsInsert(0).Value = oCustomClass.BranchId
                paramsInsert(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
                paramsInsert(1).Value = oCustomClass.AppID
                paramsInsert(2) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
                paramsInsert(2).Value = CDbl(objRow("Amount"))
                paramsInsert(3) = New SqlParameter("@InsSeqno", SqlDbType.Int)
                paramsInsert(3).Value = inc
                paramsInsert(4) = New SqlParameter("@Flag", SqlDbType.VarChar, 20)
                paramsInsert(4).Value = oCustomClass.Flag

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure,
                "spFinancialDataSave3Siml", paramsInsert)
                inc = inc + 1
            Next

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.AppID
            params(2) = New SqlParameter("@NTF", SqlDbType.Decimal)
            params(2).Value = oCustomClass.NTF
            params(3) = New SqlParameter("@effRate", SqlDbType.Decimal)
            params(3).Value = oCustomClass.EffectiveRate
            params(4) = New SqlParameter("@SupplierRate", SqlDbType.Decimal)
            params(4).Value = oCustomClass.SupplierRate
            params(5) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params(5).Value = oCustomClass.PaymentFrequency
            params(6) = New SqlParameter("@InstallmentType", SqlDbType.Char, 2)
            params(6).Value = oCustomClass.FirstInstallment
            params(7) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(7).Value = oCustomClass.Tenor
            params(8) = New SqlParameter("@GracePeriod", SqlDbType.TinyInt)
            params(8).Value = oCustomClass.GracePeriod
            params(9) = New SqlParameter("@GracePeriodType", SqlDbType.Char, 1)
            params(9).Value = oCustomClass.GracePeriodType
            params(10) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(10).Value = oCustomClass.BusDate
            params(11) = New SqlParameter("@DiffRateAmount", SqlDbType.Decimal)
            params(11).Value = oCustomClass.DiffRate
            params(12) = New SqlParameter("@Save", SqlDbType.Int)
            params(12).Value = oCustomClass.IsSave
            params(13) = New SqlParameter("@strerror", SqlDbType.Char, 50)
            params(13).Direction = ParameterDirection.Output
            params(14) = New SqlParameter("@LastInstallment", SqlDbType.Char, 50)
            params(14).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure,
            "spSaveAmortisasiIRRSiml", params)
            transaction.Commit()
            If params(14).Value Is DBNull.Value Then
                oReturnValue.installmentamount = 0
            Else
                oReturnValue.installmentamount = CDbl(params(14).Value)
            End If

            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            oReturnValue.installmentamount = 0
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.SaveAmortisasi")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "Save Amortisasi StepUpStepDown"
    Public Function SaveAmortisasiStepUpDown(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim params(15) As SqlParameter
        Dim paramsInsert(4) As SqlParameter
        Dim objTable As New DataTable
        Dim objRow As DataRow
        Dim inc As Integer

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            'transaction = conn.BeginTransaction

            objTable = oCustomClass.MydataSet.Tables("Installment")
            inc = 1
            For Each objRow In objTable.Rows
                paramsInsert(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
                paramsInsert(0).Value = oCustomClass.BranchId
                paramsInsert(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
                paramsInsert(1).Value = oCustomClass.AppID
                paramsInsert(2) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
                paramsInsert(2).Value = CDbl(objRow("Amount"))
                paramsInsert(3) = New SqlParameter("@InsSeqno", SqlDbType.Int)
                paramsInsert(3).Value = inc
                paramsInsert(4) = New SqlParameter("@Flag", SqlDbType.VarChar, 20)
                paramsInsert(4).Value = oCustomClass.Flag

                SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, spFinancialDataSave3, paramsInsert)
                inc = inc + 1
            Next

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.AppID
            params(2) = New SqlParameter("@NTF", SqlDbType.Decimal)
            params(2).Value = oCustomClass.NTF
            params(3) = New SqlParameter("@effRate", SqlDbType.Decimal)
            params(3).Value = oCustomClass.EffectiveRate
            params(4) = New SqlParameter("@SupplierRate", SqlDbType.Decimal)
            params(4).Value = oCustomClass.SupplierRate
            params(5) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params(5).Value = oCustomClass.PaymentFrequency
            params(6) = New SqlParameter("@InstallmentType", SqlDbType.Char, 2)
            params(6).Value = oCustomClass.FirstInstallment
            params(7) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(7).Value = oCustomClass.Tenor
            params(8) = New SqlParameter("@GracePeriod", SqlDbType.TinyInt)
            params(8).Value = oCustomClass.GracePeriod
            params(9) = New SqlParameter("@GracePeriodType", SqlDbType.Char, 1)
            params(9).Value = oCustomClass.GracePeriodType
            params(10) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(10).Value = oCustomClass.BusDate
            params(11) = New SqlParameter("@DiffRateAmount", SqlDbType.Decimal)
            params(11).Value = oCustomClass.DiffRate
            params(12) = New SqlParameter("@strerror", SqlDbType.Char, 50)
            params(12).Direction = ParameterDirection.Output
            params(13) = New SqlParameter("@InstAmount", SqlDbType.Decimal)
            params(13).Value = oCustomClass.installmentamount
            params(14) = New SqlParameter("@TotalBunga", SqlDbType.Decimal)
            params(14).Value = oCustomClass.TotalBunga
            params(15) = New SqlParameter("@RateIRR", SqlDbType.Decimal)
            params(15).Value = oCustomClass.RateIRR

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, spSaveAmortisasiStepUpDown, params)
            'transaction.Commit()
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            'transaction.Rollback()
            'WriteException("FinancialData", "SaveAmortisasi", exp.Message + exp.StackTrace)
            oReturnValue.Output = exp.Message
            'Dim err As New MaxiloanExceptions
            'err.WriteLog("FinancialData.vb - DA", "SaveFinancialData", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.SaveAmortisasi")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
    Public Function SaveAmortisasiStepUpDownSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim params(12) As SqlParameter
        Dim paramsInsert(4) As SqlParameter
        Dim objTable As New DataTable
        Dim objRow As DataRow
        Dim inc As Integer

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            objTable = oCustomClass.MydataSet.Tables("Installment")
            inc = 1
            For Each objRow In objTable.Rows
                paramsInsert(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
                paramsInsert(0).Value = oCustomClass.BranchId
                paramsInsert(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
                paramsInsert(1).Value = oCustomClass.AppID
                paramsInsert(2) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
                paramsInsert(2).Value = CDbl(objRow("Amount"))
                paramsInsert(3) = New SqlParameter("@InsSeqno", SqlDbType.Int)
                paramsInsert(3).Value = inc
                paramsInsert(4) = New SqlParameter("@Flag", SqlDbType.VarChar, 20)
                paramsInsert(4).Value = oCustomClass.Flag

                SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure,
                "spFinancialDataSave3Siml", paramsInsert)
                inc = inc + 1
            Next

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.AppID
            params(2) = New SqlParameter("@NTF", SqlDbType.Decimal)
            params(2).Value = oCustomClass.NTF
            params(3) = New SqlParameter("@effRate", SqlDbType.Decimal)
            params(3).Value = oCustomClass.EffectiveRate
            params(4) = New SqlParameter("@SupplierRate", SqlDbType.Decimal)
            params(4).Value = oCustomClass.SupplierRate
            params(5) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params(5).Value = oCustomClass.PaymentFrequency
            params(6) = New SqlParameter("@InstallmentType", SqlDbType.Char, 2)
            params(6).Value = oCustomClass.FirstInstallment
            params(7) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(7).Value = oCustomClass.Tenor
            params(8) = New SqlParameter("@GracePeriod", SqlDbType.TinyInt)
            params(8).Value = oCustomClass.GracePeriod
            params(9) = New SqlParameter("@GracePeriodType", SqlDbType.Char, 1)
            params(9).Value = oCustomClass.GracePeriodType
            params(10) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(10).Value = oCustomClass.BusDate
            params(11) = New SqlParameter("@DiffRateAmount", SqlDbType.Decimal)
            params(11).Value = oCustomClass.DiffRate
            params(12) = New SqlParameter("@strerror", SqlDbType.Char, 50)
            params(12).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure,
            "spSaveAmortisasiStepUpDownSiml", params)
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.SaveAmortisasi")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "List Amortisasi StepUpStepDown"
    Public Function ListAmortisasiStepUpDown(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim params(12) As SqlParameter
        Dim paramsInsert(4) As SqlParameter
        Dim transaction As SqlTransaction = Nothing
        Dim objTable As New DataTable
        Dim objRow As DataRow
        Dim inc As Integer

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            objTable = oCustomClass.MydataSet.Tables("Installment")
            inc = 1
            For Each objRow In objTable.Rows
                paramsInsert(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
                paramsInsert(0).Value = oCustomClass.BranchId
                paramsInsert(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
                paramsInsert(1).Value = oCustomClass.AppID
                paramsInsert(2) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
                paramsInsert(2).Value = CDbl(objRow("Amount"))
                paramsInsert(3) = New SqlParameter("@InsSeqno", SqlDbType.Int)
                paramsInsert(3).Value = inc
                paramsInsert(4) = New SqlParameter("@Flag", SqlDbType.VarChar, 20)
                paramsInsert(4).Value = oCustomClass.Flag

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFinancialDataSave3, paramsInsert)
                inc = inc + 1
            Next
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.AppID
            params(2) = New SqlParameter("@NTF", SqlDbType.Decimal)
            params(2).Value = oCustomClass.NTF
            params(3) = New SqlParameter("@effRate", SqlDbType.Decimal)
            params(3).Value = oCustomClass.EffectiveRate
            params(4) = New SqlParameter("@SupplierRate", SqlDbType.Decimal)
            params(4).Value = oCustomClass.SupplierRate
            params(5) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params(5).Value = oCustomClass.PaymentFrequency
            params(6) = New SqlParameter("@InstallmentType", SqlDbType.Char, 2)
            params(6).Value = oCustomClass.FirstInstallment
            params(7) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(7).Value = oCustomClass.Tenor
            params(8) = New SqlParameter("@GracePeriod", SqlDbType.TinyInt)
            params(8).Value = oCustomClass.GracePeriod
            params(9) = New SqlParameter("@GracePeriodType", SqlDbType.Char, 1)
            params(9).Value = oCustomClass.GracePeriodType
            params(10) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(10).Value = oCustomClass.BusDate
            params(11) = New SqlParameter("@DiffRateAmount", SqlDbType.Decimal)
            params(11).Value = oCustomClass.DiffRate
            params(12) = New SqlParameter("@strerror", SqlDbType.Char, 50)
            params(12).Direction = ParameterDirection.Output

            oReturnValue.ListData = SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, spSelectAmortisasiStepUpDown, params).Tables(0)
            transaction.Commit()
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception("Error On DataAccess.AccAcq.FinancialData.ListAmortisasiStepUpDown")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function ListAmortisasiStepUpDownSiml(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim params(12) As SqlParameter
        Dim paramsInsert(4) As SqlParameter
        Dim transaction As SqlTransaction = Nothing
        Dim objTable As New DataTable
        Dim objRow As DataRow
        Dim inc As Integer

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            objTable = oCustomClass.MydataSet.Tables("Installment")
            inc = 1
            For Each objRow In objTable.Rows
                paramsInsert(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
                paramsInsert(0).Value = oCustomClass.BranchId
                paramsInsert(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
                paramsInsert(1).Value = oCustomClass.AppID
                paramsInsert(2) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
                paramsInsert(2).Value = CDbl(objRow("Amount"))
                paramsInsert(3) = New SqlParameter("@InsSeqno", SqlDbType.Int)
                paramsInsert(3).Value = inc
                paramsInsert(4) = New SqlParameter("@Flag", SqlDbType.VarChar, 20)
                paramsInsert(4).Value = oCustomClass.Flag

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure,
                "spFinancialDataSave3Siml", paramsInsert)
                inc = inc + 1
            Next

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.AppID
            params(2) = New SqlParameter("@NTF", SqlDbType.Decimal)
            params(2).Value = oCustomClass.NTF
            params(3) = New SqlParameter("@effRate", SqlDbType.Decimal)
            params(3).Value = oCustomClass.EffectiveRate
            params(4) = New SqlParameter("@SupplierRate", SqlDbType.Decimal)
            params(4).Value = oCustomClass.SupplierRate
            params(5) = New SqlParameter("@PaymentFrequency", SqlDbType.Char, 1)
            params(5).Value = oCustomClass.PaymentFrequency
            params(6) = New SqlParameter("@InstallmentType", SqlDbType.Char, 2)
            params(6).Value = oCustomClass.FirstInstallment
            params(7) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(7).Value = oCustomClass.Tenor
            params(8) = New SqlParameter("@GracePeriod", SqlDbType.TinyInt)
            params(8).Value = oCustomClass.GracePeriod
            params(9) = New SqlParameter("@GracePeriodType", SqlDbType.Char, 1)
            params(9).Value = oCustomClass.GracePeriodType
            params(10) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(10).Value = oCustomClass.BusDate
            params(11) = New SqlParameter("@DiffRateAmount", SqlDbType.Decimal)
            params(11).Value = oCustomClass.DiffRate
            params(12) = New SqlParameter("@strerror", SqlDbType.Char, 50)
            params(12).Direction = ParameterDirection.Output

            oReturnValue.ListData = SqlHelper.ExecuteDataset(transaction,
            CommandType.StoredProcedure, "spSelectAmortisationStepUpDownSiml", params).Tables(0)
            transaction.Commit()
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

    Public Sub saveDefaultAdminFee(ByVal oCustomClass As Parameter.FinancialData)
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim params(2) As SqlParameter
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = oCustomClass.AppID
            params(1) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
            params(1).Value = oCustomClass.AdministrationFee
            params(2) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal)
            params(2).Value = oCustomClass.ProvisionFee
            SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "spSaveDefaultAdminFee", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Sub

    Public Sub saveDefaultAgentFee(ByVal oCustomClass As Parameter.FinancialData)
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = oCustomClass.AppID
            params(1) = New SqlParameter("@AgentFee", SqlDbType.Decimal)
            params(1).Value = oCustomClass.AgentFee
            SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "spSaveDefaultAgentFee", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Sub

    Public Function GetInstallmentSchSiml(ByVal oCustomClass As Parameter.FinancialData) As DataSet
        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim ds As New DataSet
        Try
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from _temp_installmentScheduleSiml order by InsSeqno")
            Return ds
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetRefundPremiumToSupplier(ByVal data As Parameter.Supplier) As Parameter.Supplier
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(data.strConnection)

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spSupplierINCTVGetRefundPremiumToSupplier"

            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = data.AppID
            objCommand.Parameters.Add("@RefundToSupp", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@BranchID", SqlDbType.Char, 3).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@SupplierID", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@PremiumBaseforRefundSupp", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@RefundPremi", SqlDbType.Char, 10).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@RefundInterest", SqlDbType.Decimal).Direction = ParameterDirection.Output

            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()

            data.SupplierID = CStr(IIf(IsDBNull(objCommand.Parameters("@SupplierID").Value), "", objCommand.Parameters("@SupplierID").Value))

            Return data
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try

    End Function

    Public Function saveBungaNett(oClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim conn As New SqlConnection(oClass.strConnection)

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()

            Dim par() As SqlParameter = New SqlParameter(2) {}

            par(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            par(1) = New SqlParameter("@BungaNettEff", SqlDbType.Decimal)
            par(2) = New SqlParameter("@BungaNettFlat", SqlDbType.Decimal)

            par(0).Value = oClass.ApplicationID
            par(1).Value = oClass.BungaNettEff
            par(2).Value = oClass.BungaNettFlat

            SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "spBungaNettSave", par)

            Return oClass
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog(oClass.LoginId & "|" & Date.Now.ToLongTimeString & "|FinancialData.vb", "saveBungaNett", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Return oClass
        End Try
    End Function

    Public Function SaveFinancialDataTab2(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)

        Dim transaction As SqlTransaction = Nothing
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(48) {}

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter("@SubsidiBungaATPM", SqlDbType.Decimal)
            params(2).Value = oCustomClass.SubsidiBungaATPM
            params(3) = New SqlParameter("@SubsidiAngsuranATPM", SqlDbType.Decimal)
            params(3).Value = oCustomClass.SubsidiAngsuranATPM

            params(4) = New SqlParameter("@SubsidiUangMuka", SqlDbType.Decimal)
            params(4).Value = oCustomClass.SubsidiUangMuka
            params(5) = New SqlParameter("@SubsidiUangMukaATPM", SqlDbType.Decimal)
            params(5).Value = oCustomClass.SubsidiUangMukaATPM
            params(6) = New SqlParameter("@DealerDiscount", SqlDbType.Decimal)
            params(6).Value = oCustomClass.DealerDiscount
            params(7) = New SqlParameter("@BiayaMaintenance", SqlDbType.Decimal)
            params(7).Value = oCustomClass.BiayaMaintenance
            params(8) = New SqlParameter("@RefundBungaPercent", SqlDbType.Decimal)
            params(8).Value = oCustomClass.RefundBungaPercent

            params(9) = New SqlParameter("@RefundBungaAmount", SqlDbType.Decimal)
            params(9).Value = oCustomClass.RefundBungaAmount
            params(10) = New SqlParameter("@RefundPremiPercent", SqlDbType.Decimal)
            params(10).Value = oCustomClass.RefundPremiPercent
            params(11) = New SqlParameter("@RefundPremiAmount", SqlDbType.Decimal)
            params(11).Value = oCustomClass.RefundPremiAmount

            params(12) = New SqlParameter("@RefundProvisiPercent", SqlDbType.Decimal)
            params(12).Value = oCustomClass.RefundProvisiPercent

            params(13) = New SqlParameter("@RefundProvisiAmount", SqlDbType.Decimal)
            params(13).Value = oCustomClass.RefundProvisiAmount
            params(14) = New SqlParameter("@SubsidiBungaDealer", SqlDbType.Decimal)
            params(14).Value = oCustomClass.SubsidiBungaDealer
            params(15) = New SqlParameter("@SubsidiAngsuran", SqlDbType.Decimal)
            params(15).Value = oCustomClass.SubsidiAngsuran
            params(16) = New SqlParameter("@RefundBiayaLain", SqlDbType.Decimal)
            params(16).Value = oCustomClass.AlokasiInsentifBiayaLain
            params(17) = New SqlParameter("@RefundBiayaLainPercent", SqlDbType.Decimal)
            params(17).Value = oCustomClass.AlokasiInsentifBiayaLainPercent
            params(18) = New SqlParameter("@RateIRR", SqlDbType.Decimal)
            params(18).Value = oCustomClass.RateIRR
            params(19) = New SqlParameter("@ExcludeAmount", SqlDbType.Decimal)
            params(19).Value = oCustomClass.ExcludeAmount
            params(20) = New SqlParameter("@SupplierID", SqlDbType.Char, 20)
            params(20).Value = oCustomClass.SupplierID
            params(21) = New SqlParameter("@SupplierName", SqlDbType.Char, 50)
            params(21).Value = oCustomClass.SupplierName
            params(22) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
            params(22).Value = oCustomClass.AdminFee
            params(23) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal)
            params(23).Value = oCustomClass.ProvisionFee
            params(24) = New SqlParameter("@SubsidiBunga", SqlDbType.Decimal)
            params(24).Value = oCustomClass.SubsidiBunga
            params(25) = New SqlParameter("@IncomePremi", SqlDbType.Decimal)
            params(25).Value = oCustomClass.IncomePremi
            params(26) = New SqlParameter("@OtherFee", SqlDbType.Decimal)
            params(26).Value = oCustomClass.OtherFee
            params(27) = New SqlParameter("@IncentiveDealer", SqlDbType.Decimal)
            params(27).Value = oCustomClass.IncentiveDealer
            params(28) = New SqlParameter("@InsuranceRefund", SqlDbType.Decimal)
            params(28).Value = oCustomClass.InsuranceRefund
            params(29) = New SqlParameter("@ProvisionRefund", SqlDbType.Decimal)
            params(29).Value = oCustomClass.ProvisionRefund
            params(30) = New SqlParameter("@OtherRefund", SqlDbType.Decimal)
            params(30).Value = oCustomClass.OtherRefund

            params(31) = New SqlParameter("@SubsidiRisk", SqlDbType.Decimal)
            params(31).Value = oCustomClass.SubsidiRisk
            params(32) = New SqlParameter("@SubsidiRiskATPM", SqlDbType.Decimal)
            params(32).Value = oCustomClass.SubsidiRiskATPM
            params(33) = New SqlParameter("@RefundPremiCP", SqlDbType.Decimal)
            params(33).Value = oCustomClass.RefundCreditProtectionAmount
            params(34) = New SqlParameter("@RefundPremiCPPercent", SqlDbType.Decimal)
            params(34).Value = oCustomClass.RefundCreditProtectionPercent
            params(35) = New SqlParameter("@RefundPremiJK", SqlDbType.Decimal)
            params(35).Value = oCustomClass.RefundJaminanKreditAmount
            params(36) = New SqlParameter("@RefundPremiJKPercent", SqlDbType.Decimal)
            params(36).Value = oCustomClass.RefundJaminanKreditPercent
            params(37) = New SqlParameter("@RefundUppingBunga", SqlDbType.Decimal)
            params(37).Value = oCustomClass.RefundUppingBungaAmount
            params(38) = New SqlParameter("@RefundUppingBungaPercent", SqlDbType.Decimal)
            params(38).Value = oCustomClass.RefundUppingBungaPercent
            params(39) = New SqlParameter("@SubsidiUppingBunga", SqlDbType.Decimal)
            params(39).Value = oCustomClass.SubsidiDPUppingBungaAmount
            params(40) = New SqlParameter("@SubsidiUppingBungaPercent", SqlDbType.Decimal)
            params(40).Value = oCustomClass.SubsidiDPUppingBungaPercent

            params(41) = New SqlParameter("@DownPaymentMf", SqlDbType.Decimal)
            params(41).Value = oCustomClass.DownPaymentMf

            params(42) = New SqlParameter("@AdminFeeMf", SqlDbType.Decimal)
            params(42).Value = oCustomClass.AdminFeeMf

            params(43) = New SqlParameter("@FiduciaFeeMf", SqlDbType.Decimal)
            params(43).Value = oCustomClass.FiduciaFeeMf

            params(44) = New SqlParameter("@OtherFeeMf", SqlDbType.Decimal)
            params(44).Value = oCustomClass.OtherFeeMf

            params(45) = New SqlParameter("@ProvisionFeeMf", SqlDbType.Decimal)
            params(45).Value = oCustomClass.ProvisionFeeMf

            params(46) = New SqlParameter("@PaidAmountByCustMF", SqlDbType.Decimal)
            params(46).Value = oCustomClass.AsuransiTunaiMF

            params(47) = New SqlParameter("@InstallmentAmountFirstMF", SqlDbType.Decimal)
            params(47).Value = oCustomClass.AngsuranPertamaMf

            params(48) = New SqlParameter("@DPKaroseriAmountMF", SqlDbType.Decimal)
            params(48).Value = oCustomClass.DPKaroseriAmountMF

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFinancialDataSaveTab2, params)

            transaction.Commit()
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function SaveFinancialDataTab2OperatingLease(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)

        Dim transaction As SqlTransaction = Nothing
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(30) {}

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter("@SubsidiBungaATPM", SqlDbType.Decimal)
            params(2).Value = oCustomClass.SubsidiBungaATPM
            params(3) = New SqlParameter("@SubsidiAngsuranATPM", SqlDbType.Decimal)
            params(3).Value = oCustomClass.SubsidiAngsuranATPM

            params(4) = New SqlParameter("@SubsidiUangMuka", SqlDbType.Decimal)
            params(4).Value = oCustomClass.SubsidiUangMuka
            params(5) = New SqlParameter("@SubsidiUangMukaATPM", SqlDbType.Decimal)
            params(5).Value = oCustomClass.SubsidiUangMukaATPM
            params(6) = New SqlParameter("@DealerDiscount", SqlDbType.Decimal)
            params(6).Value = oCustomClass.DealerDiscount
            params(7) = New SqlParameter("@BiayaMaintenance", SqlDbType.Decimal)
            params(7).Value = oCustomClass.BiayaMaintenance
            params(8) = New SqlParameter("@RefundBungaPercent", SqlDbType.Decimal)
            params(8).Value = oCustomClass.RefundBungaPercent

            params(9) = New SqlParameter("@RefundBungaAmount", SqlDbType.Decimal)
            params(9).Value = oCustomClass.RefundBungaAmount
            params(10) = New SqlParameter("@RefundPremiPercent", SqlDbType.Decimal)
            params(10).Value = oCustomClass.RefundPremiPercent
            params(11) = New SqlParameter("@RefundPremiAmount", SqlDbType.Decimal)
            params(11).Value = oCustomClass.RefundPremiAmount

            params(12) = New SqlParameter("@RefundProvisiPercent", SqlDbType.Decimal)
            params(12).Value = oCustomClass.RefundProvisiPercent

            params(13) = New SqlParameter("@RefundProvisiAmount", SqlDbType.Decimal)
            params(13).Value = oCustomClass.RefundProvisiAmount
            params(14) = New SqlParameter("@SubsidiBungaDealer", SqlDbType.Decimal)
            params(14).Value = oCustomClass.SubsidiBungaDealer
            params(15) = New SqlParameter("@SubsidiAngsuran", SqlDbType.Decimal)
            params(15).Value = oCustomClass.SubsidiAngsuran
            params(16) = New SqlParameter("@RefundBiayaLain", SqlDbType.Decimal)
            params(16).Value = oCustomClass.AlokasiInsentifBiayaLain
            params(17) = New SqlParameter("@RefundBiayaLainPercent", SqlDbType.Decimal)
            params(17).Value = oCustomClass.AlokasiInsentifBiayaLainPercent
            params(18) = New SqlParameter("@RateIRR", SqlDbType.Decimal)
            params(18).Value = oCustomClass.RateIRR
            params(19) = New SqlParameter("@ExcludeAmount", SqlDbType.Decimal)
            params(19).Value = oCustomClass.ExcludeAmount
            params(20) = New SqlParameter("@SupplierID", SqlDbType.Char, 20)
            params(20).Value = oCustomClass.SupplierID
            params(21) = New SqlParameter("@SupplierName", SqlDbType.Char, 50)
            params(21).Value = oCustomClass.SupplierName
            params(22) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
            params(22).Value = oCustomClass.AdminFee
            params(23) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal)
            params(23).Value = oCustomClass.ProvisionFee
            params(24) = New SqlParameter("@SubsidiBunga", SqlDbType.Decimal)
            params(24).Value = oCustomClass.SubsidiBunga
            params(25) = New SqlParameter("@IncomePremi", SqlDbType.Decimal)
            params(25).Value = oCustomClass.IncomePremi
            params(26) = New SqlParameter("@OtherFee", SqlDbType.Decimal)
            params(26).Value = oCustomClass.OtherFee
            params(27) = New SqlParameter("@IncentiveDealer", SqlDbType.Decimal)
            params(27).Value = oCustomClass.IncentiveDealer
            params(28) = New SqlParameter("@InsuranceRefund", SqlDbType.Decimal)
            params(28).Value = oCustomClass.InsuranceRefund
            params(29) = New SqlParameter("@ProvisionRefund", SqlDbType.Decimal)
            params(29).Value = oCustomClass.ProvisionRefund
            params(30) = New SqlParameter("@OtherRefund", SqlDbType.Decimal)
            params(30).Value = oCustomClass.OtherRefund

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFinancialDataSaveTab2OperatingLease, params)

            transaction.Commit()
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function SaveFinancialDataTab2End(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)

        Dim transaction As SqlTransaction = Nothing
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(2) {}

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.ApplicationID
            params(2) = New SqlParameter("@BusDate", SqlDbType.DateTime)
            params(2).Value = oCustomClass.BusinessDate

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFinancialDataSaveTab2End, params)

            transaction.Commit()
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function SaveFinancialDataTab2EndOperatingLease(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oReturnValue As New Parameter.FinancialData
        Dim conn As New SqlConnection(oCustomClass.strConnection)

        Dim transaction As SqlTransaction = Nothing
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(2) {}

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.ApplicationID
            params(2) = New SqlParameter("@BusDate", SqlDbType.DateTime)
            params(2).Value = oCustomClass.BusinessDate

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFinancialDataSaveTab2EndOperatingLease, params)

            transaction.Commit()
            oReturnValue.Output = ""
            Return oReturnValue
        Catch exp As Exception
            transaction.Rollback()
            oReturnValue.Output = exp.Message
            Return oReturnValue
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function GoLiveJournalDummyOperatingLease(ByVal oCustomClass As Parameter.FinancialData) As Parameter.FinancialData
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim oTransaction As SqlTransaction

        If oConn.State = ConnectionState.Closed Then oConn.Open()
        oTransaction = oConn.BeginTransaction

        Try
            Dim parJournal() As SqlParameter = New SqlParameter(2) {}

            parJournal(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            parJournal(0).Value = oCustomClass.ApplicationID
            parJournal(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 50)
            parJournal(1).Value = oCustomClass.BranchId
            parJournal(2) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
            parJournal(2).Value = oCustomClass.BusinessDate

            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGoliveCrJournalDummyOperatingLease", parJournal).Tables(0)
            Return oCustomClass
            oTransaction.Commit()
        Catch ex As Exception
            oTransaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Function


End Class
