

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InsuranceCalculationResult
    Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    Private Const DISPLAY_RESULT_INSURANCE_CALCULATION As String = "spInsDisplayResultInsCalculation"
    Private Const DISPLAY_RESULT_GRID_INSURANCE_CALCULATION As String = "spInsDisplayResultGridInsCalculation"
#End Region

#Region "DisplayResultOnGrid"
    Public Function DisplayResultOnGrid(ByVal customClass As Parameter.InsuranceCalculationResult) As Parameter.InsuranceCalculationResult
        Dim oReturnValue As New Parameter.InsuranceCalculationResult
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId.Trim
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customClass.ApplicationID.Trim
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, DISPLAY_RESULT_GRID_INSURANCE_CALCULATION, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("InsuranceCalculationResult", "DisplayResultOnGrid", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function DisplayResultOnGridAdditional(ByVal customClass As Parameter.InsuranceCalculationResult, ByVal InsuranceProductID As String) As Parameter.InsuranceCalculationResult
        Dim oReturnValue As New Parameter.InsuranceCalculationResult
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId.Trim
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customClass.ApplicationID.Trim
        params(2) = New SqlParameter("@InsuranceProductID", SqlDbType.Char, 3)
        params(2).Value = InsuranceProductID
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spInsDisplayResultGridInsCalculationAdditional", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("InsuranceCalculationResult", "DisplayResultOnGrid", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "DisplayResultInsCalculationStep1"

    Public Function DisplayResultInsCalculationStep1(ByVal customClass As Parameter.InsuranceCalculationResult) As Parameter.InsuranceCalculationResult
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId.Trim
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customClass.ApplicationID.Trim
        Try
            Dim oReturnValue As New Parameter.InsuranceCalculationResult
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, DISPLAY_RESULT_INSURANCE_CALCULATION, params)
            If reader.Read Then
                oReturnValue.TotalSRCPremiumToCust = CType(reader("TotalSRCPremiumToCust"), Double)
                oReturnValue.TotalTPLPremiumToCust = CType(reader("TotalTPLPremiumToCust"), Double)
                oReturnValue.TotalRiotPremiumToCust = CType(reader("TotalRiotPremiumToCust"), Double)
                oReturnValue.TotalPAPremiumToCust = CType(reader("TotalPAPremiumToCust"), Double)
                oReturnValue.TotalFloodPremiumToCust = CType(reader("TotalFloodPremiumToCust"), Double)
                oReturnValue.TotalLoadingFeeToCust = CType(reader("TotalLoadingFeeToCust"), Double)
                oReturnValue.TotalStdPremium = CType(reader("TotalStdPremium"), Double)
                oReturnValue.TotalPremiumToCustBeforeDiscount = CType(reader("TotalPremiumToCustBeforeDiscount"), Double)
                oReturnValue.TotalPremiumToCustAfterDiscount = CType(reader("TotalPremiumToCustAfterDiscount"), Double)
                oReturnValue.PremiumBaseRefundShowroom = CType(reader("PremiumBaseRefundShowroom"), Double)
                oReturnValue.InterestType = reader("InterestType").ToString.Trim
                oReturnValue.InstallmentScheme = reader("InstallmentScheme").ToString.Trim
                oReturnValue.CustomerID = reader("CustomerID").ToString.Trim
                oReturnValue.InsNotes = reader("InsNotes").ToString.Trim
                oReturnValue.AccNotes = reader("AccNotes").ToString.Trim
                oReturnValue.TotalPaidAmountByCust = CType(reader("TotalPaidAmountByCust"), Double)
                oReturnValue.TotalDiscountToCust = CType(reader("TotalDiscountToCust"), Double)
                oReturnValue.AdminFee = CType(reader("AdminFee"), Double)

            End If
            reader.Close()
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
            WriteException("InsuranceCalculationResult", "DisplayResultInsCalculationStep1", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region



End Class
