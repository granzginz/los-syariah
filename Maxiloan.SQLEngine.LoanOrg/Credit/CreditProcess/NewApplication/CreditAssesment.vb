﻿Option Strict On
Imports Maxiloan.Framework.SQLEngine
Imports System.Data.SqlClient

Public Class CreditAssesment : Inherits Maxiloan.SQLEngine.DataAccessBase
    Public Function getCreditAssesment(ByVal ocustomClass As Parameter.CreditAssesment) As Parameter.CreditAssesment
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.CreditAssesment
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = ocustomClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = ocustomClass.ApplicationID
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, "spGetCreditAssesment", params)
            If reader.Read Then
                With oReturnValue
                    .ApplicationID = reader("ApplicationID").ToString
                    .TeleponRumah = reader("HomePhone").ToString
                    .TeleponKantor = reader("OfficePhone").ToString
                    .TeleponEC = reader("ECPhone").ToString
                    .TeleponPenjamin = reader("GuaranteePhone").ToString
                    .TanggalTelepon = CDate(reader("PhoneDate").ToString)
                    .TeleponRumahArea = reader("HomePhoneArea").ToString
                    .TeleponKantorArea = reader("OfficePhoneArea").ToString
                    .TeleponPenjaminArea = reader("GuaranteePhoneArea").ToString
                    .TeleponECArea = reader("ECPhoneArea").ToString
                    .NoHp = reader("NoHP").ToString
                    .CreditAnalyst = reader("CreditAnalyst").ToString
                    .SaldoAwal = CDbl(reader("SaldoAwal").ToString)
                    .SaldoAkhir = CDbl(reader("SaldoAkhir").ToString)
                    .SaldoRataRata = CDbl(reader("SaldoRataRata").ToString)
                    .JumlahPengeluaran = CDbl(reader("JumlahPengeluaran").ToString)
                    .JumlahPemasukan = CDbl(reader("JumlahPemasukan").ToString)
                    .JumlahHariTransaksi = CInt(reader("JumlahHariTransaksi").ToString)
                    .JenisRekening = reader("BankAccountType").ToString
                    .SurveyorNotes = reader("SurveyorNotes").ToString
                    .NotesBank = reader("NotesBank").ToString


                    .RequestBy = reader("RequestBy").ToString
                    .AgreementNo = reader("AgreementNo").ToString
                    '.SaldoRataRata = CDbl(reader("SaldoRataRata").ToString)
                    '.SaldoAwal = CDbl(reader("SaldoAwal").ToString)
                    '.SaldoAkhir = CDbl(reader("SaldoAkhir").ToString)
                    '.JumlahPemasukan = CDbl(reader("JumlahPemasukan").ToString)
                    '.JumlahPengeluaran = CDbl(reader("JumlahPengeluaran").ToString)
                    '.JumlahHariTransaksi = CInt(reader("JumlahHariTransaksi").ToString)

                    '.JenisRekening = reader("BankAccountType").ToString
                End With
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub CreditAssesmentSave(ByVal oCustomClass As Parameter.CreditAssesment)
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim transaction As SqlTransaction
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Try
            If oConn.State = ConnectionState.Closed Then oConn.Open()
            transaction = oConn.BeginTransaction

            'With oEntitiesApproval
            '    .ApprovalTransaction = transaction
            '    .BranchId = oCustomClass.BranchId
            '    .SchemeID = oCustomClass.SchemeID
            '    .RequestDate = oCustomClass.BusinessDate
            '    .TransactionNo = oCustomClass.ApplicationID
            '    .ApprovalNote = oCustomClass.Notes
            '    .ApprovalValue = CDbl(oCustomClass.RefundAmount)
            '    .UserRequest = oCustomClass.LoginId
            '    .UserApproval = oCustomClass.RequestBy
            '    .AprovalType = Parameter.Approval.ETransactionType.RCA_Approval
            '    .Argumentasi = oCustomClass.Argumentasi
            'End With

            'oCustomClass.ApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

            Dim par As IList(Of SqlParameter) = New List(Of SqlParameter)

            par.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = oCustomClass.BranchId})
            par.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20) With {.Value = oCustomClass.ApplicationID})
 
            par.Add(New SqlParameter("@HomePhone", SqlDbType.VarChar, 20) With {.Value = oCustomClass.TeleponRumah})
            par.Add(New SqlParameter("@OfficePhone", SqlDbType.VarChar, 20) With {.Value = oCustomClass.TeleponKantor})
            par.Add(New SqlParameter("@ECPhone", SqlDbType.VarChar, 20) With {.Value = oCustomClass.TeleponEC})
            par.Add(New SqlParameter("@GuaranteePhone", SqlDbType.VarChar, 20) With {.Value = oCustomClass.TeleponPenjamin})
            par.Add(New SqlParameter("@PhoneDate", SqlDbType.Date) With {.Value = oCustomClass.TanggalTelepon})
            par.Add(New SqlParameter("@CreditAnalyst", SqlDbType.VarChar, 20) With {.Value = oCustomClass.CreditAnalyst})
            par.Add(New SqlParameter("@SurveyorNotes", SqlDbType.Text) With {.Value = oCustomClass.SurveyorNotes}) 
            par.Add(New SqlParameter("@HomePhoneArea", SqlDbType.VarChar, 10) With {.Value = oCustomClass.TeleponRumahArea})
            par.Add(New SqlParameter("@OfficePhoneArea", SqlDbType.VarChar, 10) With {.Value = oCustomClass.TeleponKantorArea})
            par.Add(New SqlParameter("@ECPhoneArea", SqlDbType.VarChar, 10) With {.Value = oCustomClass.TeleponECArea})
            par.Add(New SqlParameter("@GuaranteePhoneArea", SqlDbType.VarChar, 10) With {.Value = oCustomClass.TeleponPenjaminArea})
            par.Add(New SqlParameter("@NoHP", SqlDbType.VarChar, 20) With {.Value = oCustomClass.NoHp})

            ''tabrekening bank
            par.Add(New SqlParameter("@SaldoRataRata", SqlDbType.Money) With {.Value = oCustomClass.SaldoRataRata})
            par.Add(New SqlParameter("@SaldoAwal", SqlDbType.Money) With {.Value = oCustomClass.SaldoAwal})
            par.Add(New SqlParameter("@SaldoAkhir", SqlDbType.Money) With {.Value = oCustomClass.SaldoAkhir})
            par.Add(New SqlParameter("@JumlahPemasukan", SqlDbType.Money) With {.Value = oCustomClass.JumlahPemasukan})
            par.Add(New SqlParameter("@JumlahPengeluaran", SqlDbType.Money) With {.Value = oCustomClass.JumlahPengeluaran})
            par.Add(New SqlParameter("@JumlahHariTransaksi", SqlDbType.SmallInt) With {.Value = oCustomClass.JumlahHariTransaksi})
            par.Add(New SqlParameter("@NotesBank", SqlDbType.Text) With {.Value = oCustomClass.NotesBank})
            par.Add(New SqlParameter("@BankAccountType", SqlDbType.VarChar, 10) With {.Value = oCustomClass.JenisRekening})

            par.Add(New SqlParameter("@UserApproval", SqlDbType.Text) With {.Value = oCustomClass.RequestBy})
            par.Add(New SqlParameter("@NoKontrak", SqlDbType.VarChar, 10) With {.Value = oCustomClass.AgreementNo})

            par.Add(New SqlParameter("@Tab", SqlDbType.VarChar, 20) With {.Value = oCustomClass.part})


            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spCreditAssesmentSave", par.ToArray)

        Catch ex As Exception
            '  transaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Function getApprovalValue(cnn As String, appId As String, branchid As String) As Double

        Dim par As IList(Of SqlParameter) = New List(Of SqlParameter)
        par.Add(New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20) With {.Value = appId})
        par.Add(New SqlParameter("@Branchid", SqlDbType.VarChar, 10) With {.Value = branchid})
       
        Dim reader = SqlHelper.ExecuteReader(cnn, CommandType.StoredProcedure, "GetAgreementAgregate", par.ToArray)
        If reader.Read Then
            Return CType(reader("AggrAmount"), Double)
        End If

        Return 0
          
    End Function

    Function getApprovalSchemeID(cnn As String, appId As String) As String
        Dim par As IList(Of SqlParameter) = New List(Of SqlParameter)
        par.Add(New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20) With {.Value = appId})
        Dim reader = SqlHelper.ExecuteReader(cnn, CommandType.StoredProcedure, "GetAgreementSchemeID", par.ToArray)
        If reader.Read Then
            Return CType(reader("SchemeID"), String)
        End If

        Return ""
    End Function

    Public Function Proceed(ByVal oCustomClass As Parameter.CreditAssesment) As String
        Dim oConn As New SqlConnection(oCustomClass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim par As IList(Of SqlParameter) = New List(Of SqlParameter)


        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Try
            If oConn.State = ConnectionState.Closed Then oConn.Open()
            transaction = oConn.BeginTransaction

            Dim approvalValue = getApprovalValue(oCustomClass.strConnection, oCustomClass.ApplicationID, oCustomClass.BranchId)

            With oEntitiesApproval
                .ApprovalTransaction = transaction
                .BranchId = oCustomClass.BranchId
                .SchemeID = oCustomClass.SchemeID
                .RequestDate = oCustomClass.BusinessDate
                .TransactionNo = oCustomClass.ApplicationID
                .ApprovalNote = oCustomClass.NotesBank

                .ApprovalValue = approvalValue

                .UserRequest = oCustomClass.LoginId
                .UserApproval = oCustomClass.RequestBy
                .AprovalType = Parameter.Approval.ETransactionType.RCA_Approval
                .Argumentasi = oCustomClass.NotesBank
            End With

            oCustomClass.ApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

            par.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = oCustomClass.BranchId})
            par.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20) With {.Value = oCustomClass.ApplicationID})
            par.Add(New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50) With {.Value = oCustomClass.ApprovalNo})
            par.Add(New SqlParameter("@BusinessDate", SqlDbType.DateTime) With {.Value = oCustomClass.BusinessDate})
            Dim eerParams = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            par.Add(eerParams)
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "sp_CreditAssessment_proceed", par.ToArray) 
            transaction.Commit()
            Return CType(eerParams.Value, String)
        Catch ex As Exception
            transaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
