﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class CustomerCases : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spCustomerCasesPaging"
    Private Const LIST_BY_ID As String = "spCustomerCasesView"
    Private Const LIST_ADD As String = "spCustomerCasesSaveAdd"
    Private Const LIST_UPDATE As String = "spCustomerCasesSaveEdit"
    Private Const LIST_DELETE As String = "spCustomerCasesDelete"
    Private Const LIST_SELECT_ALL_CASES As String = "spGetCasesAll"
#End Region

    Public Function GetCustomerCases(ByVal oCustomClass As Parameter.CustomerCases) As Parameter.CustomerCases
        Dim oReturnValue As New Parameter.CustomerCases
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerCases.GetCustomerCases")
        End Try
    End Function

    Public Function GetCustomerCasesList(ByVal ocustomClass As Parameter.CustomerCases) As Parameter.CustomerCases
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.CustomerCases
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CustomerID
        params(1) = New SqlParameter("@CaseID", SqlDbType.SmallInt)
        params(1).Value = ocustomClass.CaseID
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then                
                oReturnValue.CustomerID = reader("CustomerID").ToString
                oReturnValue.CaseID = CInt(reader("CaseID").ToString)
                oReturnValue.ReferenceNo = reader("ReferenceNo").ToString
                oReturnValue.ReferenceDate = CDate(reader("ReferenceDate").ToString)
                oReturnValue.Notes = reader("Notes").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerCases.GetCustomerCasesList")
        End Try
    End Function

    Public Function CustomerCasesSaveAdd(ByVal ocustomClass As Parameter.CustomerCases) As String
        Dim ErrMessage As String = ""
        Dim params(7) As SqlParameter
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CustomerIDEdit
        params(1) = New SqlParameter("@CaseID", SqlDbType.SmallInt)
        params(1).Value = ocustomClass.CaseIDEdit
        params(2) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.ReferenceNo
        params(3) = New SqlParameter("@ReferenceDate", SqlDbType.SmallDateTime)
        params(3).Value = ocustomClass.ReferenceDate
        params(4) = New SqlParameter("@Notes", SqlDbType.VarChar, 255)
        params(4).Value = ocustomClass.Notes
        params(5) = New SqlParameter("@LoginId", SqlDbType.VarChar, 20)
        params(5).Value = ocustomClass.LoginId
        params(6) = New SqlParameter("@businessdate", SqlDbType.DateTime)
        params(6).Value = ocustomClass.BusinessDate
        params(7) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(7).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(7).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerCases.CustomerCasesSaveAdd")
        End Try
    End Function

    Public Sub CustomerCasesSaveEdit(ByVal ocustomClass As Parameter.CustomerCases)
        Dim params(8) As SqlParameter
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CustomerID
        params(1) = New SqlParameter("@CaseID", SqlDbType.SmallInt)
        params(1).Value = ocustomClass.CaseID
        params(2) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.ReferenceNo
        params(3) = New SqlParameter("@ReferenceDate", SqlDbType.SmallDateTime)
        params(3).Value = ocustomClass.ReferenceDate
        params(4) = New SqlParameter("@Notes", SqlDbType.VarChar, 255)
        params(4).Value = ocustomClass.Notes
        params(5) = New SqlParameter("@LoginId", SqlDbType.VarChar, 20)
        params(5).Value = ocustomClass.LoginId
        params(6) = New SqlParameter("@businessdate", SqlDbType.DateTime)
        params(6).Value = ocustomClass.BusinessDate
        params(7) = New SqlParameter("@CustomerIDEdit", SqlDbType.Char, 20)
        params(7).Value = ocustomClass.CustomerIDEdit
        params(8) = New SqlParameter("@CaseIDEdit", SqlDbType.SmallInt)
        params(8).Value = ocustomClass.CaseIDEdit
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerCases.CustomerCasesSaveEdit")
        End Try
    End Sub

    Public Function CustomerCasesDelete(ByVal ocustomClass As Parameter.CustomerCases) As String
        Dim Err As Integer
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.CustomerCases
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CustomerID
        params(1) = New SqlParameter("@CaseID", SqlDbType.SmallInt)
        params(1).Value = ocustomClass.CaseID
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            'Throw New Exception("Error On DataAccess.Setting.CustomerCases.CustomerCasesDelete")
        End Try
    End Function

    Public Function GetComboCases(ByVal customclass As Parameter.CustomerCases) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, LIST_SELECT_ALL_CASES).Tables(0)
    End Function
End Class
