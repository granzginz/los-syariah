﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class CustomerGroup : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    Private Const LIST_SELECT As String = "spPagingCustomerGroup"
    Private Const LIST_SELECT_LIST_CUSTOMER As String = "spCustomerGroupListCustomer"
    Private Const LIST_BY_ID As String = "spCustomerGroupList"
    Private Const LIST_ADD As String = "spCustomerGroupSaveAdd"
    Private Const LIST_UPDATE As String = "spCustomerGroupSaveEdit"
    Private Const LIST_DELETE As String = "spCustomerGroupDelete"
    Private Const LIST_REPORT As String = "spCustomerGroupReport"
#End Region

    Public Function GetCustomerGroup(ByVal oCustomClass As Parameter.CustomerGroup) As Parameter.CustomerGroup
        Dim oReturnValue As New Parameter.CustomerGroup
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerGroup.GetCustomerGroup")
        End Try
    End Function

    Public Function GetCustomerGroupReport(ByVal oCustomClass As Parameter.CustomerGroup) As Parameter.CustomerGroup
        Dim oReturnValue As New Parameter.CustomerGroup
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CmdWhere", SqlDbType.VarChar, 1000)
        params(0).Value = oCustomClass.WhereCond
        Try
            oReturnValue.ListdataReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerGroup.GetCustomerGroupReport")
        End Try
    End Function

    Public Function GetCustomerGroupList(ByVal ocustomClass As Parameter.CustomerGroup) As Parameter.CustomerGroup
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.CustomerGroup
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CustomerGroupID
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.CustomerGroupID = reader("CustomerGroupID").ToString
                oReturnValue.CustomerGroupNm = reader("CustomerGroupNm").ToString
                oReturnValue.Plafond = Convert.ToDecimal(reader("Plafond").ToString)
                oReturnValue.Catatan = reader("Catatan").ToString
                oReturnValue.Oustanding = Convert.ToDecimal(reader("Oustanding").ToString)
                oReturnValue.Kontrak = reader("Kontrak").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerGroup.GetCustomerGroupList")
        End Try
    End Function

    Public Function GetCustomerGroupListCustomer(ByVal oCustomClass As Parameter.CustomerGroup) As Parameter.CustomerGroup
        Dim oReturnValue As New Parameter.CustomerGroup
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(0).Value = oCustomClass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(1).Value = oCustomClass.SortBy
        params(2) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(2).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT_LIST_CUSTOMER, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(2).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerGroup.GetCustomerGroupListCustomer")
        End Try
    End Function

    Public Function CustomerGroupSaveAdd(ByVal ocustomClass As Parameter.CustomerGroup) As String
        Dim ErrMessage As String = ""
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CustomerGroupNm", SqlDbType.VarChar, 50)
        params(0).Value = ocustomClass.CustomerGroupNm
        params(1) = New SqlParameter("@Plafond", SqlDbType.Decimal)
        params(1).Value = ocustomClass.Plafond
        params(2) = New SqlParameter("@Catatan", SqlDbType.VarChar, 250)
        params(2).Value = ocustomClass.Catatan
        params(3) = New SqlParameter("@BranchName", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.BranchName
        params(4) = New SqlParameter("@businessdate", SqlDbType.DateTime)
        params(4).Value = ocustomClass.BusinessDate
        params(5) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(5).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(5).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerGroup.CustomerGroupSaveAdd")
        End Try
    End Function

    Public Sub CustomerGroupSaveEdit(ByVal ocustomClass As Parameter.CustomerGroup)
        Dim params(3) As SqlParameter

        params(0) = New SqlParameter("@CustomerGroupID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CustomerGroupID
        params(1) = New SqlParameter("@CustomerGroupNm", SqlDbType.VarChar, 250)
        params(1).Value = ocustomClass.CustomerGroupNm
        params(2) = New SqlParameter("@Plafond", SqlDbType.Decimal)
        params(2).Value = ocustomClass.Plafond
        params(3) = New SqlParameter("@Catatan", SqlDbType.Text)
        params(3).Value = ocustomClass.Catatan

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerGroup.CustomerGroupSaveEdit")
        End Try
    End Sub

    Public Function CustomerGroupDelete(ByVal ocustomClass As Parameter.CustomerGroup) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CustomerGroupID
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"            
        End Try
    End Function
End Class
