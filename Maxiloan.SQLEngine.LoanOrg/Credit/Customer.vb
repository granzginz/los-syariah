
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter

#End Region

Public Class Customer : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    Private Const LISTCUSTOMERFRPROSPECT As String = "spCustomerFromProspectPaging"
    Private Const LIST_SELECT As String = "spCustomerPaging"
    Private Const LIST_REPORT As String = "spCustomerReport"
    Private Const spPersonalAddFillCbo As String = "spCustomerPersonalAddFillCbo"
    Private Const spPersonalGetIDType As String = "spCustomerPersonalGetIDType"
    Private Const spCompanyGetIDType As String = "spCustomerCompanyGetIDType"
    Private Const spPersonalSaveAdd As String = "spCustomerPersonalSaveAdd"
    Private Const spPersonalSaveAdd2 As String = "spCustomerPersonalSaveAdd2"
    Private Const spPersonalSaveAdd3 As String = "spCustomerPersonalSaveAdd3"
    Private Const spCompanySaveAdd As String = "spCustomerCompanySaveAdd"
    Private Const spCompanySaveAdd2 As String = "spCustomerCompanySaveAdd2"
    Private Const spCompanySaveAdd3 As String = "spCustomerCompanySaveAdd3"
    Private Const spPersonalSaveEdit As String = "spCustomerPersonalSaveEdit"
    Private Const spPersonalSaveEdit2 As String = "spCustomerPersonalSaveEdit2"
    Private Const spPersonalSaveEdit3 As String = "spCustomerPersonalSaveEdit3"
    Private Const spPersonalSaveEdit4 As String = "spCustomerPersonalOLSaveEdit"
    Private Const spCompanySaveEdit As String = "spCustomerCompanySaveEdit"
    Private Const spCompanySaveEdit2 As String = "spCustomerCompanySaveEdit2"
    Private Const spCompanySaveEdit3 As String = "spCustomerCompanySaveEdit3"
    Private Const spBindCustomer1_002 As String = "spCustomer002Bind1"
    Private Const spBindCustomer2_002 As String = "spCustomer002Bind2"
    Private Const spBindCustomer1_002Personal As String = "spCustomer002Bind1Personal"
    Private Const spBindCustomer2_002Personal As String = "spCustomer002Bind2Personal"
    Private Const spBindCustomerC1_002 As String = "spCustomer002BindC1"
    Private Const spBindCustomerC2_002 As String = "spCustomer002BindC2"
    Private Const spCustomerCompanyEdit As String = "spCustomerCompanyEdit"
    Private Const spCustomerCompanyEdit2 As String = "spCustomerCompanyEdit2"
    Private Const spCustomerCompanyEdit3 As String = "spCustomerCompanyEdit3"
    Private Const spCustomerPersonalEdit As String = "spCustomerPersonalEdit"
    Private Const spCustomerPersonalEdit2 As String = "spCustomerPersonalEdit2"
    Private Const spCustomerPersonalEdit3 As String = "spCustomerPersonalEdit3"

    Private Const VIEW_CUSTOMER As String = "spViewCustomerDetail"
    Private Const VIEW_EMP_OMSET As String = "spViewCustomerEmpOmset"
    Private Const VIEW_ENT_OMSET As String = "spViewCustomerEntOmset"
    Private Const VIEW_CUS_FAMILY As String = "spViewCustomerFamily"

    Private Const VIEW_COMPANY_CUSTOMER As String = "spViewCompanyCustomer"
    Private Const VIEW_COMPANY_CUSTOMER_MANAGEMENT As String = "spViewCompanyCustomerShareHolders"
    Private Const VIEW_COMPANY_CUSTOMER_DOCUMENT As String = "spViewCompanyCustomerDocument"
    Private Const QUERY_CHECK_CUSTOMERTYPE As String = "select customertype from customer"
    Private Const VIEW_COMPANY_CUSTOMER_AGREEMENT As String = "spAgreementListCompRpt"
    Private Const CHECK_PERSONAL_DOCUMENT As String = "spCheckPersonalCustomerDocument"
#End Region

#Region "General"
    Function customerPagingParam(ByVal oCustomClass As Parameter.Customer, ByRef totalRow As SqlParameter) As IList(Of SqlParameter)
        Dim params = New List(Of SqlParameter)

        params.Add(New SqlParameter("@CurrentPage", SqlDbType.Int) With {.Value = oCustomClass.CurrentPage})
        params.Add(New SqlParameter("@PageSize", SqlDbType.Int) With {.Value = oCustomClass.PageSize})
        params.Add(New SqlParameter("@WhereCond", SqlDbType.VarChar) With {.Value = oCustomClass.WhereCond})
        params.Add(New SqlParameter("@SortBy", SqlDbType.VarChar) With {.Value = oCustomClass.SortBy})
        totalRow = New SqlParameter("@TotalRecords", SqlDbType.Int) With {.Direction = ParameterDirection.Output}
        params.Add(totalRow)
        Return params
    End Function

    Public Function GetCustomer(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer
        'Dim oReturnValue As New Parameter.Customer
        'Dim params(4) As SqlParameter
        'params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        'params(0).Value = oCustomClass.CurrentPage
        'params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        'params(1).Value = oCustomClass.PageSize
        'params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        'params(2).Value = oCustomClass.WhereCond
        'params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        'params(3).Value = oCustomClass.SortBy
        'params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        'params(4).Direction = ParameterDirection.Output
        Dim totalRow = New SqlParameter
        Dim params = customerPagingParam(oCustomClass, totalRow)
        Try
            oCustomClass.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params.ToArray).Tables(0)
            oCustomClass.totalrecords = CType(params(4).Value, Int64)
            Return oCustomClass
        Catch exp As Exception
            WriteException("Customer", "GetCustomer", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.GetCustomer")
        End Try
    End Function
    Public Function GetCustomerFromProspect(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer
        Dim totalRow = New SqlParameter
        Dim params = customerPagingParam(oCustomClass, totalRow)
        Try
            oCustomClass.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LISTCUSTOMERFRPROSPECT, params.ToArray).Tables(0)
            oCustomClass.totalrecords = CType(params(4).Value, Int64)
            Return oCustomClass
        Catch exp As Exception
            WriteException("Customer", "GetCustomerFromProspect", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.GetCustomerFromProspect")
        End Try
    End Function



    Public Function GetCustomerEdit(ByVal oCustomClass As Parameter.Customer, ByVal level As String) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer
        Dim params(0) As SqlParameter
        Dim spName As String
        Dim custType As String = ""

        If oCustomClass.CustomerType Is Nothing Then
            custType = oCustomClass.PersonalCustomerType
        Else
            custType = oCustomClass.CustomerType
        End If

        Select Case custType.ToUpper
            Case "P"
                If level = "1" Then
                    spName = spCustomerPersonalEdit
                ElseIf level = "2" Then
                    spName = spCustomerPersonalEdit2
                ElseIf level = "3" Then
                    spName = "spCustomerPersonalIdentitasEdit"
                ElseIf level = "4" Then
                    spName = "spCustomerPersonalPekerjaanEdit"
                ElseIf level = "5" Then
                    spName = "spCustomerPersonalKeuanganEdit"
                ElseIf level = "6" Then
                    spName = "spCustomerPersonalPasanganEdit"
                ElseIf level = "65" Then
                    spName = "spCustomerPersonalGuarantorEdit"
                ElseIf level = "7" Then
                    spName = "spCustomerPersonalEmergencyEdit"
                ElseIf level = "8" Then
                    spName = "spCustomerPersonalKeluargaEdit"
                Else
                    spName = spCustomerPersonalEdit3
                End If

            Case "PP"
                spName = "SPPROSPECTPERSONALEDIT"

            Case Else
                If level = "1" Then
                    spName = spCustomerCompanyEdit
                ElseIf level = "2" Then
                    spName = spCustomerCompanyEdit2
                Else
                    spName = spCustomerCompanyEdit3
                End If


        End Select

        'If custType.ToUpper = "P" Then
        '    If level = "1" Then
        '        spName = spCustomerPersonalEdit
        '    ElseIf level = "2" Then
        '        spName = spCustomerPersonalEdit2
        '    ElseIf level = "3" Then
        '        spName = "spCustomerPersonalIdentitasEdit"
        '    ElseIf level = "4" Then
        '        spName = "spCustomerPersonalPekerjaanEdit"
        '    ElseIf level = "5" Then
        '        spName = "spCustomerPersonalKeuanganEdit"
        '    ElseIf level = "6" Then
        '        spName = "spCustomerPersonalPasanganEdit"
        '    ElseIf level = "65" Then
        '        spName = "spCustomerPersonalGuarantorEdit"
        '    ElseIf level = "7" Then
        '        spName = "spCustomerPersonalEmergencyEdit"
        '    ElseIf level = "8" Then
        '        spName = "spCustomerPersonalKeluargaEdit"
        '    Else
        '        spName = spCustomerPersonalEdit3
        '    End If
        'Else
        '    If level = "1" Then
        '        spName = spCustomerCompanyEdit
        '    ElseIf level = "2" Then
        '        spName = spCustomerCompanyEdit2
        '    Else
        '        spName = spCustomerCompanyEdit3
        '    End If
        'End If
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerID
        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, spName, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "GetCustomerEdit", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.GetCustomerEdit")
        End Try
    End Function
    Public Function CustomerPersonalGetIDType(ByVal oCustomer As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer
        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomer.strConnection, CommandType.StoredProcedure, spPersonalGetIDType).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "CustomerPersonalGetIDType", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.CustomerPersonalGetIDType")
        End Try
    End Function

    Public Function CustomerCompanyGetIDType(ByVal oCustomer As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer
        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomer.strConnection, CommandType.StoredProcedure, spCompanyGetIDType).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "CustomerCompanyGetIDType", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.CustomerCompanyGetIDType")
        End Try
    End Function

    Public Function CustomerPersonalAdd(ByVal oCustomer As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(0).Value = oCustomer.Table

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomer.strConnection, CommandType.StoredProcedure, spPersonalAddFillCbo, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "CustomerPersonalAdd", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.CustomerPersonalAdd")
        End Try
    End Function
#End Region

#Region "Personal"
    Public Function BindCustomer1_002(ByVal oCustomer As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer
        Dim params(3) As SqlParameter
        Dim oArr As Array = CType(oCustomer.Name.Split(CChar(" ")), Array)
        Try
            params(0) = New SqlParameter("@Birthdate", SqlDbType.VarChar, 10)
            params(0).Value = oCustomer.BirthDate
            params(1) = New SqlParameter("@IDType", SqlDbType.VarChar, 10)
            params(1).Value = oCustomer.IDType
            params(2) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 25)
            params(2).Value = oCustomer.IDNumber
            params(3) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(3).Value = Replace(oCustomer.Name, "'", "''")

            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomer.strConnection, CommandType.StoredProcedure, spBindCustomer1_002Personal, params).Tables(0)

            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "BindCustomer1_002", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.BindCustomer1_002")
        End Try
    End Function

    Public Function BindCustomer1_002_withSI(ByVal oCustomer As Parameter.Customer, ByVal SIData As Parameter.CustomerSI) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer
        Dim params(7) As SqlParameter
        Dim oArr As Array = CType(oCustomer.Name.Split(CChar(" ")), Array)
        Try
            params(0) = New SqlParameter("@Birthdate", SqlDbType.VarChar, 8)
            params(0).Value = oCustomer.BirthDate
            params(1) = New SqlParameter("@IDType", SqlDbType.VarChar, 10)
            params(1).Value = oCustomer.IDType
            params(2) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 25)
            params(2).Value = oCustomer.IDNumber
            params(3) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(3).Value = Replace(oCustomer.Name, "'", "''")
            params(4) = New SqlParameter("@MotherName", SqlDbType.VarChar, 50)
            params(4).Value = oCustomer.MotherName
            params(5) = New SqlParameter("@NoKK", SqlDbType.VarChar, 25)
            params(5).Value = oCustomer.NoKK
            params(6) = New SqlParameter("@NamaSuamiIstri", SqlDbType.VarChar, 50)
            params(6).Value = SIData.NamaSuamiIstri
            params(7) = New SqlParameter("@MaritalStatus", SqlDbType.VarChar, 10)
            params(7).Value = oCustomer.MaritalStatus

            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomer.strConnection, CommandType.StoredProcedure, spBindCustomer1_002, params).Tables(0)

            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "BindCustomer1_002", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.BindCustomer1_002")
        End Try
    End Function

    Public Function BindCustomer2_002(ByVal oCustomer As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer
        Dim params(3) As SqlParameter
        Try
            params(0) = New SqlParameter("@Birthdate", SqlDbType.VarChar, 10)
            params(0).Value = oCustomer.BirthDate
            params(1) = New SqlParameter("@IDType", SqlDbType.VarChar, 10)
            params(1).Value = oCustomer.IDType
            params(2) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 25)
            params(2).Value = oCustomer.IDNumber
            params(3) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(3).Value = Replace(oCustomer.Name, "'", "''")
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomer.strConnection, CommandType.StoredProcedure, spBindCustomer2_002Personal, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "BindCustomer2_002", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.BindCustomer2_002")
        End Try
    End Function

    Public Function PersonalSaveAdd(ByVal oCustomer As Parameter.Customer,
                                    ByVal oLegalAddress As Parameter.Address,
                                    ByVal oResAddress As Parameter.Address,
                                    ByVal oJDAddress As Parameter.Address,
                                    ByVal oDataOmset As DataTable,
                                    ByVal oDataFamily As DataTable,
                                    ByVal oPCEX As Parameter.CustomerEX,
                                    ByVal oPCSI As Parameter.CustomerSI,
                                    ByVal oPCSIAddress As Parameter.Address,
                                    ByVal oPCER As Parameter.CustomerER,
                                    ByVal oPCERAddress As Parameter.Address) As Parameter.Customer
        Dim conn As New SqlConnection(oCustomer.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim CustomerID As String = ""
        Dim intLoopOmset As Integer
        Dim paramsOmset(3) As SqlParameter
        Dim paramsFamily(5) As SqlParameter
        Dim paramsPCEX(11) As SqlParameter
        Dim paramsPCSI(36) As SqlParameter
        Dim paramsPCER(14) As SqlParameter
        Dim oReturn As New Parameter.Customer
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            Dim params(107) As SqlParameter

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 5)
            params(0).Value = oCustomer.BranchId
            params(1) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(1).Value = Replace(oCustomer.Name, "'", "''")
            params(2) = New SqlParameter("@CustomerType", SqlDbType.Char, 1)
            params(2).Value = oCustomer.CustomerType
            params(3) = New SqlParameter("@CustomerGroupID", SqlDbType.Char, 20)
            params(3).Value = oCustomer.CustomerGroupID
            params(4) = New SqlParameter("@IDType", SqlDbType.VarChar, 10)
            params(4).Value = oCustomer.IDType
            params(5) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 25)
            params(5).Value = oCustomer.IDNumber
            params(6) = New SqlParameter("@Gender", SqlDbType.Char, 1)
            params(6).Value = oCustomer.Gender
            params(7) = New SqlParameter("@BirthPlace", SqlDbType.VarChar, 20)
            params(7).Value = oCustomer.BirthPlace
            params(8) = New SqlParameter("@BirthDate", SqlDbType.VarChar, 8)
            params(8).Value = oCustomer.BirthDate
            params(9) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20)
            params(9).Value = oCustomer.MobilePhone
            params(10) = New SqlParameter("@EMail", SqlDbType.VarChar, 30)
            params(10).Value = oCustomer.Email
            params(11) = New SqlParameter("@Religion", SqlDbType.VarChar, 10)
            params(11).Value = IIf(oCustomer.Religion = "Select One", "", oCustomer.Religion)
            params(12) = New SqlParameter("@MaritalStatus", SqlDbType.VarChar, 10)
            params(12).Value = IIf(oCustomer.MaritalStatus = "Select One", "", oCustomer.MaritalStatus)
            params(13) = New SqlParameter("@NumOfDependence", SqlDbType.SmallInt)
            params(13).Value = CInt(IIf(oCustomer.NumOfDependence = "", "0", oCustomer.NumOfDependence))
            params(14) = New SqlParameter("@PersonalNPWP", SqlDbType.VarChar, 25)
            params(14).Value = oCustomer.PersonalNPWP
            params(15) = New SqlParameter("@NoKK", SqlDbType.VarChar, 20)
            params(15).Value = oCustomer.NoKK
            params(16) = New SqlParameter("@Education", SqlDbType.VarChar, 10)
            params(16).Value = IIf(oCustomer.Education = "Select One", "", oCustomer.Education)
            params(17) = New SqlParameter("@ProfessionID", SqlDbType.VarChar, 10)
            params(17).Value = IIf(oCustomer.ProfessionID = "Select One", "", oCustomer.ProfessionID)
            params(18) = New SqlParameter("@Nationality", SqlDbType.Char, 3)
            params(18).Value = IIf(oCustomer.Nationality = "Select One", "", oCustomer.Nationality)
            params(19) = New SqlParameter("@WNACountry", SqlDbType.VarChar, 50)
            params(19).Value = oCustomer.WNACountry
            params(20) = New SqlParameter("@HomeStatus", SqlDbType.VarChar, 10)
            params(20).Value = IIf(oCustomer.HomeStatus = "Select One", "", oCustomer.HomeStatus)
            params(21) = New SqlParameter("@RentFinishDate", SqlDbType.VarChar, 8)
            params(21).Value = oCustomer.RentFinishDate
            params(22) = New SqlParameter("@HomeLocation", SqlDbType.VarChar, 10)
            params(22).Value = IIf(oCustomer.HomeLocation = "Select One", "", oCustomer.HomeLocation)
            params(23) = New SqlParameter("@HomePrice", SqlDbType.Decimal)
            params(23).Value = CDec(IIf(oCustomer.HomePrice = "", "0", oCustomer.HomePrice))
            params(24) = New SqlParameter("@StaySinceYear", SqlDbType.SmallInt)
            params(24).Value = CInt(IIf(oCustomer.StaySinceYear = "", "0", oCustomer.StaySinceYear))
            params(25) = New SqlParameter("@LegalAddress", SqlDbType.VarChar, 100)
            params(25).Value = oLegalAddress.Address
            params(26) = New SqlParameter("@LegalRT", SqlDbType.Char, 3)
            params(26).Value = oLegalAddress.RT
            params(27) = New SqlParameter("@LegalRW", SqlDbType.Char, 3)
            params(27).Value = oLegalAddress.RW
            params(28) = New SqlParameter("@LegalKelurahan", SqlDbType.VarChar, 30)
            params(28).Value = oLegalAddress.Kelurahan
            params(29) = New SqlParameter("@LegalKecamatan", SqlDbType.VarChar, 30)
            params(29).Value = oLegalAddress.Kecamatan
            params(30) = New SqlParameter("@LegalCity", SqlDbType.VarChar, 30)
            params(30).Value = oLegalAddress.City
            params(31) = New SqlParameter("@LegalZipCode", SqlDbType.Char, 5)
            params(31).Value = oLegalAddress.ZipCode
            params(32) = New SqlParameter("@LegalAreaPhone1", SqlDbType.Char, 4)
            params(32).Value = oLegalAddress.AreaPhone1
            params(33) = New SqlParameter("@LegalPhone1", SqlDbType.Char, 10)
            params(33).Value = oLegalAddress.Phone1
            params(34) = New SqlParameter("@LegalAreaPhone2", SqlDbType.Char, 4)
            params(34).Value = oLegalAddress.AreaPhone2
            params(35) = New SqlParameter("@LegalPhone2", SqlDbType.Char, 10)
            params(35).Value = oLegalAddress.Phone2
            params(36) = New SqlParameter("@LegalAreaFax", SqlDbType.Char, 4)
            params(36).Value = oLegalAddress.AreaFax
            params(37) = New SqlParameter("@LegalFax", SqlDbType.Char, 10)
            params(37).Value = oLegalAddress.Fax
            params(38) = New SqlParameter("@ResidenceAddress", SqlDbType.VarChar, 100)
            params(38).Value = oResAddress.Address
            params(39) = New SqlParameter("@ResidenceRT", SqlDbType.Char, 3)
            params(39).Value = oResAddress.RT
            params(40) = New SqlParameter("@ResidenceRW", SqlDbType.Char, 3)
            params(40).Value = oResAddress.RW
            params(41) = New SqlParameter("@ResidenceKelurahan", SqlDbType.VarChar, 30)
            params(41).Value = oResAddress.Kelurahan
            params(42) = New SqlParameter("@ResidenceKecamatan", SqlDbType.VarChar, 30)
            params(42).Value = oResAddress.Kecamatan
            params(43) = New SqlParameter("@ResidenceCity", SqlDbType.VarChar, 30)
            params(43).Value = oResAddress.City
            params(44) = New SqlParameter("@ResidenceZipCode", SqlDbType.Char, 5)
            params(44).Value = oResAddress.ZipCode
            params(45) = New SqlParameter("@ResidenceAreaPhone1", SqlDbType.Char, 4)
            params(45).Value = oResAddress.AreaPhone1
            params(46) = New SqlParameter("@ResidencePhone1", SqlDbType.Char, 10)
            params(46).Value = oResAddress.Phone1
            params(47) = New SqlParameter("@ResidenceAreaPhone2", SqlDbType.Char, 4)
            params(47).Value = oResAddress.AreaPhone2
            params(48) = New SqlParameter("@ResidencePhone2", SqlDbType.Char, 10)
            params(48).Value = oResAddress.Phone2
            params(49) = New SqlParameter("@ResidenceAreaFax", SqlDbType.Char, 4)
            params(49).Value = oResAddress.AreaFax
            params(50) = New SqlParameter("@ResidenceFax", SqlDbType.Char, 10)
            params(50).Value = oResAddress.Fax
            params(51) = New SqlParameter("@JobTypeID", SqlDbType.VarChar, 10)
            params(51).Value = IIf(oCustomer.JobTypeID = "Select One", "", oCustomer.JobTypeID)
            params(52) = New SqlParameter("@JobPos", SqlDbType.VarChar, 10)
            params(52).Value = IIf(oCustomer.JobPos = "Select One", "", oCustomer.JobPos)
            params(53) = New SqlParameter("@CompanyName", SqlDbType.VarChar, 50)
            params(53).Value = oCustomer.CompanyName
            params(54) = New SqlParameter("@IndustryTypeID", SqlDbType.Char, 10)
            params(54).Value = IIf(oCustomer.IndustryTypeID = "Select One", "", oCustomer.IndustryTypeID)
            params(55) = New SqlParameter("@CompanyAddress", SqlDbType.VarChar, 100)
            params(55).Value = oJDAddress.Address
            params(56) = New SqlParameter("@CompanyRT", SqlDbType.Char, 3)
            params(56).Value = oJDAddress.RT
            params(57) = New SqlParameter("@CompanyRW", SqlDbType.Char, 3)
            params(57).Value = oJDAddress.RW
            params(58) = New SqlParameter("@CompanyKelurahan", SqlDbType.VarChar, 30)
            params(58).Value = oJDAddress.Kelurahan
            params(59) = New SqlParameter("@CompanyKecamatan", SqlDbType.VarChar, 30)
            params(59).Value = oJDAddress.Kecamatan
            params(60) = New SqlParameter("@CompanyCity", SqlDbType.VarChar, 30)
            params(60).Value = oJDAddress.City
            params(61) = New SqlParameter("@CompanyZipCode", SqlDbType.Char, 5)
            params(61).Value = oJDAddress.ZipCode
            params(62) = New SqlParameter("@CompanyAreaPhone1", SqlDbType.Char, 4)
            params(62).Value = oJDAddress.AreaPhone1
            params(63) = New SqlParameter("@CompanyPhone1", SqlDbType.Char, 10)
            params(63).Value = oJDAddress.Phone1
            params(64) = New SqlParameter("@CompanyAreaPhone2", SqlDbType.Char, 4)
            params(64).Value = oJDAddress.AreaPhone2
            params(65) = New SqlParameter("@CompanyPhone2", SqlDbType.Char, 10)
            params(65).Value = oJDAddress.Phone2
            params(66) = New SqlParameter("@CompanyAreaFax", SqlDbType.Char, 4)
            params(66).Value = oJDAddress.AreaFax
            params(67) = New SqlParameter("@CompanyFax", SqlDbType.Char, 10)
            params(67).Value = oJDAddress.Fax
            params(68) = New SqlParameter("@CompanyJobTitle", SqlDbType.VarChar, 50)
            params(68).Value = oCustomer.CompanyJobTitle
            params(69) = New SqlParameter("@EmploymentSinceYear", SqlDbType.SmallInt)
            params(69).Value = CInt(IIf(oCustomer.EmploymentSinceYear = "", "0", oCustomer.EmploymentSinceYear))
            params(70) = New SqlParameter("@MonthlyFixedIncome", SqlDbType.Decimal)
            params(70).Value = CDec(IIf(oCustomer.MonthlyFixedIncome = "", "0", oCustomer.MonthlyFixedIncome))
            params(71) = New SqlParameter("@MonthlyVariableIncome", SqlDbType.Decimal)
            params(71).Value = CDec(IIf(oCustomer.MonthlyVariableIncome = "", "0", oCustomer.MonthlyVariableIncome))
            params(72) = New SqlParameter("@OtherBusinessName", SqlDbType.VarChar, 50)
            params(72).Value = oCustomer.OtherBusinessName
            params(73) = New SqlParameter("@OtherBusinessType", SqlDbType.VarChar, 50)
            params(73).Value = oCustomer.OtherBusinessType
            params(74) = New SqlParameter("@OtherBusinessIndustryTypeID", SqlDbType.Char, 10)
            params(74).Value = IIf(oCustomer.OtherBusinessIndustryTypeID = "Select One", "", oCustomer.OtherBusinessIndustryTypeID)
            params(75) = New SqlParameter("@OtherBusinessJobTitle", SqlDbType.VarChar, 50)
            params(75).Value = oCustomer.OtherBusinessJobTitle
            params(76) = New SqlParameter("@OtherBusinessSinceYear", SqlDbType.SmallInt)
            params(76).Value = CInt(IIf(oCustomer.OtherBusinessSinceYear = "", "0", oCustomer.OtherBusinessSinceYear))
            params(77) = New SqlParameter("@Notes", SqlDbType.Text)
            params(77).Value = oCustomer.Notes
            params(78) = New SqlParameter("@PersonalCustomerType", SqlDbType.Char, 1)
            params(78).Value = oCustomer.PersonalCustomerType
            params(79) = New SqlParameter("@SpouseIncome", SqlDbType.Decimal)
            params(79).Value = CDec(IIf(oCustomer.SpouseIncome = "", "0", oCustomer.SpouseIncome))
            params(80) = New SqlParameter("@AvgBalanceAccount", SqlDbType.Decimal)
            params(80).Value = CDec(IIf(oCustomer.AvgBalanceAccount = "", "0", oCustomer.AvgBalanceAccount))
            params(81) = New SqlParameter("@BankAccountType", SqlDbType.VarChar, 10)
            params(81).Value = IIf(oCustomer.BankAccountType = "Select One", "", oCustomer.BankAccountType)
            params(82) = New SqlParameter("@AverageDebitTransaction", SqlDbType.Decimal)
            params(82).Value = CDec(IIf(oCustomer.AverageDebitTransaction = "", "0", oCustomer.AverageDebitTransaction))
            params(83) = New SqlParameter("@AverageCreditTransaction", SqlDbType.Decimal)
            params(83).Value = CDec(IIf(oCustomer.AverageCreditTransaction = "", "0", oCustomer.AverageCreditTransaction))
            params(84) = New SqlParameter("@AverageBalance", SqlDbType.Decimal)
            params(84).Value = CDec(IIf(oCustomer.AverageBalance = "", "0", oCustomer.AverageBalance))
            params(85) = New SqlParameter("@Deposito", SqlDbType.Decimal)
            params(85).Value = CDec(IIf(oCustomer.Deposito = "", "0", oCustomer.Deposito))
            params(86) = New SqlParameter("@LivingCostAmount", SqlDbType.Decimal)
            params(86).Value = CDec(IIf(oCustomer.LivingCostAmount = "", "0", oCustomer.LivingCostAmount))
            params(87) = New SqlParameter("@OtherLoanInstallment", SqlDbType.Decimal)
            params(87).Value = CDec(IIf(oCustomer.OtherLoanInstallment = "", "0", oCustomer.OtherLoanInstallment))
            params(88) = New SqlParameter("@AdditionalCollateralType", SqlDbType.VarChar, 50)
            params(88).Value = oCustomer.AdditionalCollateralType
            params(89) = New SqlParameter("@AdditionalCollateralAmount", SqlDbType.Decimal)
            params(89).Value = CDec(IIf(oCustomer.AdditionalCollateralAmount = "", "0", oCustomer.AdditionalCollateralAmount))
            params(90) = New SqlParameter("@CreditCardID", SqlDbType.VarChar, 10)
            params(90).Value = IIf(oCustomer.CreditCardID = "Select One", "", oCustomer.CreditCardID)
            params(91) = New SqlParameter("@CreditCardType", SqlDbType.Char, 1)
            params(91).Value = oCustomer.CreditCardType
            params(92) = New SqlParameter("@NumOfCreditCard", SqlDbType.SmallInt)
            params(92).Value = CInt(IIf(oCustomer.NumOfCreditCard = "", "0", oCustomer.NumOfCreditCard))
            params(93) = New SqlParameter("@BankID", SqlDbType.Char, 5)
            params(93).Value = IIf(oCustomer.BankID = "0", "", oCustomer.BankID)
            params(94) = New SqlParameter("@BankBranch", SqlDbType.VarChar, 50)
            params(94).Value = oCustomer.BankBranch
            params(95) = New SqlParameter("@AccountNo", SqlDbType.Char, 20)
            params(95).Value = oCustomer.AccountNo
            params(96) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
            params(96).Value = IIf(oCustomer.AccountName <> "", Replace(oCustomer.AccountName, "'", "''"), oCustomer.AccountName)
            params(97) = New SqlParameter("@Reference", SqlDbType.VarChar, 10)
            params(97).Value = IIf(oCustomer.Reference = "Select One", "", oCustomer.Reference)
            params(98) = New SqlParameter("@IsApplyCarLoanBefore", SqlDbType.Bit)
            params(98).Value = CInt(IIf(oCustomer.IsApplyCarLoanBefore = "0", 0, 1))
            params(99) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(99).Direction = ParameterDirection.Output
            params(100) = New SqlParameter("@CustID", SqlDbType.Char, 20)
            params(100).Direction = ParameterDirection.Output
            params(101) = New SqlParameter("@businessdate", SqlDbType.DateTime)
            params(101).Value = oCustomer.BusinessDate
            params(102) = New SqlParameter("@ProspectAppID", SqlDbType.Char, 20)
            params(102).Value = oCustomer.ProspectAppID
            params(103) = New SqlParameter("@MotherName", SqlDbType.VarChar, 50)
            params(103).Value = oCustomer.MotherName
            params(104) = New SqlParameter("@Gelar", SqlDbType.VarChar, 50)
            params(104).Value = oCustomer.Gelar
            params(105) = New SqlParameter("@IDExpiredDate", SqlDbType.Date)
            params(105).Value = oCustomer.IDExpiredDate
            params(106) = New SqlParameter("@KondisiRumah", SqlDbType.VarChar, 20)
            params(106).Value = oCustomer.KondisiRumah
            params(107) = New SqlParameter("@GelarBelakang", SqlDbType.VarChar, 50)
            params(107).Value = oCustomer.GelarBelakang
            'params(108) = New SqlParameter("@IsOLS", SqlDbType.Bit)
            'params(108).Value = oCustomer.IsOLS

            Dim oReturnValue As New Parameter.Customer

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spPersonalSaveAdd, params)
            ErrMessage = CType(params(99).Value, String)
            CustomerID = CType(params(100).Value, String)

            If ErrMessage <> "" Then
                oReturn.CustomerID = CustomerID
                oReturn.Err = ErrMessage
                Return oReturn
                Exit Function
            End If

            If oDataOmset.Rows.Count > 0 Then
                paramsOmset(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                paramsOmset(1) = New SqlParameter("@OmsetYear", SqlDbType.SmallInt)
                paramsOmset(2) = New SqlParameter("@OmsetMonth", SqlDbType.SmallInt)
                paramsOmset(3) = New SqlParameter("@Omset", SqlDbType.Decimal)
                For intLoopOmset = 0 To oDataOmset.Rows.Count - 1
                    paramsOmset(0).Value = CustomerID
                    paramsOmset(1).Value = oDataOmset.Rows(intLoopOmset).Item("Year")
                    paramsOmset(2).Value = oDataOmset.Rows(intLoopOmset).Item("Month")
                    paramsOmset(3).Value = oDataOmset.Rows(intLoopOmset).Item("Omset")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spPersonalSaveAdd2, paramsOmset)
                Next
            End If
            If oDataFamily.Rows.Count > 0 Then
                paramsFamily(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                paramsFamily(1) = New SqlParameter("@FamilySeqNo", SqlDbType.SmallInt)
                paramsFamily(2) = New SqlParameter("@FamilyName", SqlDbType.VarChar, 50)
                paramsFamily(3) = New SqlParameter("@FamilyIDNumber", SqlDbType.VarChar, 25)
                paramsFamily(4) = New SqlParameter("@FamilyBirthDate", SqlDbType.VarChar, 8)
                paramsFamily(5) = New SqlParameter("@FamilyRelation", SqlDbType.VarChar, 10)
                For intLoopOmset = 0 To oDataFamily.Rows.Count - 1
                    paramsFamily(0).Value = CustomerID
                    paramsFamily(1).Value = oDataFamily.Rows(intLoopOmset).Item("FamilySeqNo")
                    paramsFamily(2).Value = IIf(oDataFamily.Rows(intLoopOmset).Item("FamilyName").ToString <> "", Replace(oDataFamily.Rows(intLoopOmset).Item("FamilyName").ToString, "'", "''"), oDataFamily.Rows(intLoopOmset).Item("FamilyName").ToString)
                    paramsFamily(3).Value = oDataFamily.Rows(intLoopOmset).Item("FamilyIDNumber")
                    paramsFamily(4).Value = oDataFamily.Rows(intLoopOmset).Item("FamilyBirthDate")
                    paramsFamily(5).Value = oDataFamily.Rows(intLoopOmset).Item("FamilyRelation")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spPersonalSaveAdd3, paramsFamily)
                Next
            End If

            paramsPCEX(0) = New SqlParameter("@Mode", SqlDbType.Bit)
            paramsPCEX(0).Value = 1
            paramsPCEX(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            paramsPCEX(1).Value = CustomerID
            paramsPCEX(2) = New SqlParameter("@SPTTahunan", SqlDbType.Bit)
            paramsPCEX(2).Value = oPCEX.SPTTahunan
            paramsPCEX(3) = New SqlParameter("@TinggalSejakBulan", SqlDbType.TinyInt)
            paramsPCEX(3).Value = oPCEX.TinggalSejakBulan
            paramsPCEX(4) = New SqlParameter("@KeteranganUsaha", SqlDbType.VarChar, 50)
            paramsPCEX(4).Value = oPCEX.KeteranganUsaha
            paramsPCEX(5) = New SqlParameter("@KategoryPers", SqlDbType.Char, 1)
            paramsPCEX(5).Value = oPCEX.KategoryPers
            paramsPCEX(6) = New SqlParameter("@KondisiKantor", SqlDbType.Char, 1)
            paramsPCEX(6).Value = oPCEX.KondisiKantor
            paramsPCEX(7) = New SqlParameter("@KaryawanSejakBulan", SqlDbType.TinyInt)
            paramsPCEX(7).Value = oPCEX.KaryawanSejakBulan
            paramsPCEX(8) = New SqlParameter("@SaldoAwal", SqlDbType.Money)
            paramsPCEX(8).Value = oPCEX.SaldoAwal
            paramsPCEX(9) = New SqlParameter("@SaldoAkhir", SqlDbType.Money)
            paramsPCEX(9).Value = oPCEX.SaldoAkhir
            paramsPCEX(10) = New SqlParameter("@JumlahHariTransaksi", SqlDbType.TinyInt)
            paramsPCEX(10).Value = oPCEX.JumlahHariTransaksi
            paramsPCEX(11) = New SqlParameter("@TotalLamaKerja", SqlDbType.TinyInt)
            paramsPCEX(11).Value = oPCEX.TotalLamaKerja
            'paramsPCEX(11) = New SqlParameter("@JumlahKendaraan", SqlDbType.TinyInt)
            'paramsPCEX(11).Value = oPCEX.JumlahKendaraan
            'paramsPCEX(12) = New SqlParameter("@Garasi", SqlDbType.Bit)
            'paramsPCEX(12).Value = oPCEX.Garasi
            'paramsPCEX(13) = New SqlParameter("@PertamaKredit", SqlDbType.Char, 2)
            'paramsPCEX(13).Value = oPCEX.PertamaKredit
            'paramsPCEX(14) = New SqlParameter("@OrderKe", SqlDbType.TinyInt)
            'paramsPCEX(14).Value = oPCEX.OrderKe

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPersonalCustomerEXAddEdit", paramsPCEX)

            paramsPCSI(0) = New SqlParameter("@Mode", SqlDbType.Bit)
            paramsPCSI(0).Value = 1
            paramsPCSI(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            paramsPCSI(1).Value = CustomerID
            paramsPCSI(2) = New SqlParameter("@NamaSuamiIstri", SqlDbType.VarChar, 50)
            paramsPCSI(2).Value = oPCSI.NamaSuamiIstri
            paramsPCSI(3) = New SqlParameter("@JenisPekerjaanID", SqlDbType.VarChar, 10)
            paramsPCSI(3).Value = oPCSI.JenisPekerjaanID
            paramsPCSI(4) = New SqlParameter("@NamaPers", SqlDbType.VarChar, 50)
            paramsPCSI(4).Value = oPCSI.NamaPers
            paramsPCSI(5) = New SqlParameter("@JenisIndustriID", SqlDbType.VarChar, 10)
            paramsPCSI(5).Value = oPCSI.JenisIndustriID
            paramsPCSI(6) = New SqlParameter("@KeteranganUsaha", SqlDbType.VarChar, 50)
            paramsPCSI(6).Value = oPCSI.KeteranganUsaha
            paramsPCSI(7) = New SqlParameter("@Alamat", SqlDbType.VarChar, 50)
            paramsPCSI(7).Value = oPCSIAddress.Address
            paramsPCSI(8) = New SqlParameter("@RT", SqlDbType.Char, 3)
            paramsPCSI(8).Value = oPCSIAddress.RT
            paramsPCSI(9) = New SqlParameter("@RW", SqlDbType.Char, 3)
            paramsPCSI(9).Value = oPCSIAddress.RW
            paramsPCSI(10) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 50)
            paramsPCSI(10).Value = oPCSIAddress.Kelurahan
            paramsPCSI(11) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 50)
            paramsPCSI(11).Value = oPCSIAddress.Kecamatan
            paramsPCSI(12) = New SqlParameter("@Kota", SqlDbType.VarChar, 50)
            paramsPCSI(12).Value = oPCSIAddress.City
            paramsPCSI(13) = New SqlParameter("@KodePos", SqlDbType.Char, 5)
            paramsPCSI(13).Value = oPCSIAddress.ZipCode
            paramsPCSI(14) = New SqlParameter("@KaryawanSejak", SqlDbType.TinyInt)
            paramsPCSI(14).Value = oPCSI.KaryawanSejak
            paramsPCSI(15) = New SqlParameter("@KaryawanSejakTahun", SqlDbType.Char, 4)
            paramsPCSI(15).Value = oPCSI.KaryawanSejakTahun
            paramsPCSI(16) = New SqlParameter("@PenghasilanTetap", SqlDbType.Decimal)
            paramsPCSI(16).Value = oPCSI.PenghasilanTetap
            paramsPCSI(17) = New SqlParameter("@PenghasilanTambahan", SqlDbType.Decimal)
            paramsPCSI(17).Value = oPCSI.PenghasilanTambahan
            paramsPCSI(18) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
            paramsPCSI(18).Value = oPCSIAddress.AreaPhone1
            paramsPCSI(19) = New SqlParameter("@Phone1", SqlDbType.Char, 10)
            paramsPCSI(19).Value = oPCSIAddress.Phone1
            paramsPCSI(20) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
            paramsPCSI(20).Value = oPCSIAddress.AreaPhone2
            paramsPCSI(21) = New SqlParameter("@Phone2", SqlDbType.Char, 10)
            paramsPCSI(21).Value = oPCSIAddress.Phone2
            paramsPCSI(22) = New SqlParameter("@AreaFax", SqlDbType.Char, 4)
            paramsPCSI(22).Value = oPCSIAddress.AreaFax
            paramsPCSI(23) = New SqlParameter("@Fax", SqlDbType.Char, 10)
            paramsPCSI(23).Value = oPCSIAddress.Fax

            'alamat KTP pasangan
            paramsPCSI(24) = New SqlParameter("@KtpAlamat", SqlDbType.VarChar, 50)
            paramsPCSI(24).Value = oPCSI.AlamatKTP.Address
            paramsPCSI(25) = New SqlParameter("@KtpRT", SqlDbType.Char, 3)
            paramsPCSI(25).Value = oPCSI.AlamatKTP.RT
            paramsPCSI(26) = New SqlParameter("@KtpRW", SqlDbType.Char, 3)
            paramsPCSI(26).Value = oPCSI.AlamatKTP.RW
            paramsPCSI(27) = New SqlParameter("@KtpKelurahan", SqlDbType.VarChar, 50)
            paramsPCSI(27).Value = oPCSI.AlamatKTP.Kelurahan
            paramsPCSI(28) = New SqlParameter("@KtpKecamatan", SqlDbType.VarChar, 50)
            paramsPCSI(28).Value = oPCSI.AlamatKTP.Kecamatan
            paramsPCSI(29) = New SqlParameter("@KtpKota", SqlDbType.VarChar, 50)
            paramsPCSI(29).Value = oPCSI.AlamatKTP.City
            paramsPCSI(30) = New SqlParameter("@KtpKodePos", SqlDbType.Char, 5)
            paramsPCSI(30).Value = oPCSI.AlamatKTP.ZipCode
            paramsPCSI(31) = New SqlParameter("@KtpAreaPhone1", SqlDbType.Char, 4)
            paramsPCSI(31).Value = oPCSI.AlamatKTP.AreaPhone1
            paramsPCSI(32) = New SqlParameter("@KtpPhone1", SqlDbType.Char, 10)
            paramsPCSI(32).Value = oPCSI.AlamatKTP.Phone1
            paramsPCSI(33) = New SqlParameter("@KtpAreaPhone2", SqlDbType.Char, 4)
            paramsPCSI(33).Value = oPCSI.AlamatKTP.AreaPhone2
            paramsPCSI(34) = New SqlParameter("@KtpPhone2", SqlDbType.Char, 10)
            paramsPCSI(34).Value = oPCSI.AlamatKTP.Phone2
            paramsPCSI(35) = New SqlParameter("@KtpAreaFax", SqlDbType.Char, 4)
            paramsPCSI(35).Value = oPCSI.AlamatKTP.AreaFax
            paramsPCSI(36) = New SqlParameter("@KtpFax", SqlDbType.Char, 10)
            paramsPCSI(36).Value = oPCSI.AlamatKTP.Fax

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPersonalCustomerSIAddEdit", paramsPCSI)

            paramsPCER(0) = New SqlParameter("@Mode", SqlDbType.Bit)
            paramsPCER(0).Value = 1
            paramsPCER(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            paramsPCER(1).Value = CustomerID
            paramsPCER(2) = New SqlParameter("@Nama", SqlDbType.VarChar, 50)
            paramsPCER(2).Value = oPCER.Nama
            paramsPCER(3) = New SqlParameter("@Hubungan", SqlDbType.VarChar, 50)
            paramsPCER(3).Value = oPCER.Hubungan
            paramsPCER(4) = New SqlParameter("@Alamat", SqlDbType.VarChar, 50)
            paramsPCER(4).Value = oPCERAddress.Address
            paramsPCER(5) = New SqlParameter("@RT", SqlDbType.Char, 3)
            paramsPCER(5).Value = oPCERAddress.RT
            paramsPCER(6) = New SqlParameter("@RW", SqlDbType.Char, 3)
            paramsPCER(6).Value = oPCERAddress.RW
            paramsPCER(7) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 50)
            paramsPCER(7).Value = oPCERAddress.Kelurahan
            paramsPCER(8) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 50)
            paramsPCER(8).Value = oPCERAddress.Kecamatan
            paramsPCER(9) = New SqlParameter("@Kota", SqlDbType.VarChar, 50)
            paramsPCER(9).Value = oPCERAddress.City
            paramsPCER(10) = New SqlParameter("@KodePos", SqlDbType.Char, 5)
            paramsPCER(10).Value = oPCERAddress.ZipCode
            paramsPCER(11) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
            paramsPCER(11).Value = oPCERAddress.AreaPhone1
            paramsPCER(12) = New SqlParameter("@Phone1", SqlDbType.VarChar, 15)
            paramsPCER(12).Value = oPCERAddress.Phone1
            paramsPCER(13) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 15)
            paramsPCER(13).Value = oPCER.MobilePhone
            paramsPCER(14) = New SqlParameter("@Email", SqlDbType.VarChar, 50)
            paramsPCER(14).Value = oPCER.Email

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPersonalCustomerERAddEdit", paramsPCER)

            transaction.Commit()
            oReturn.CustomerID = CustomerID
            oReturn.Err = ""
            Return oReturn
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Customer", "PersonalSaveAdd", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.PersonalSaveAdd")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Sub PersonalCustomerIdentitasParams(params As IList(Of SqlParameter), oCustomer As Parameter.Customer, oLegalAddress As Parameter.Address, oResAddress As Parameter.Address)
        params.Add(New SqlParameter("@BranchID", SqlDbType.VarChar, 5) With {.Value = oCustomer.BranchId})
        params.Add(New SqlParameter("@Name", SqlDbType.VarChar, 50) With {.Value = Replace(oCustomer.Name, "'", "''")})
        params.Add(New SqlParameter("@CustomerType", SqlDbType.Char, 1) With {.Value = oCustomer.CustomerType})
        params.Add(New SqlParameter("@CustomerGroupID", SqlDbType.Char, 20) With {.Value = oCustomer.CustomerGroupID})
        params.Add(New SqlParameter("@IDType", SqlDbType.VarChar, 10) With {.Value = oCustomer.IDType})
        params.Add(New SqlParameter("@IDNumber", SqlDbType.VarChar, 25) With {.Value = oCustomer.IDNumber})
        params.Add(New SqlParameter("@Gender", SqlDbType.Char, 1) With {.Value = oCustomer.Gender})
        params.Add(New SqlParameter("@BirthPlace", SqlDbType.VarChar, 20) With {.Value = oCustomer.BirthPlace})
        params.Add(New SqlParameter("@BirthDate", SqlDbType.VarChar, 8) With {.Value = oCustomer.BirthDate})
        params.Add(New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20) With {.Value = oCustomer.MobilePhone})
        params.Add(New SqlParameter("@EMail", SqlDbType.VarChar, 30) With {.Value = oCustomer.Email})
        params.Add(New SqlParameter("@Religion", SqlDbType.VarChar, 10) With {.Value = IIf(oCustomer.Religion = "Select One", "", oCustomer.Religion)})
        params.Add(New SqlParameter("@MaritalStatus", SqlDbType.VarChar, 10) With {.Value = IIf(oCustomer.MaritalStatus = "Select One", "", oCustomer.MaritalStatus)})
        params.Add(New SqlParameter("@NumOfDependence", SqlDbType.SmallInt) With {.Value = CInt(IIf(oCustomer.NumOfDependence = "", "0", oCustomer.NumOfDependence))})
        params.Add(New SqlParameter("@PersonalNPWP", SqlDbType.VarChar, 25) With {.Value = oCustomer.PersonalNPWP})
        params.Add(New SqlParameter("@NoKK", SqlDbType.VarChar, 20) With {.Value = oCustomer.NoKK})
        params.Add(New SqlParameter("@Education", SqlDbType.VarChar, 10) With {.Value = IIf(oCustomer.Education = "Select One", "", oCustomer.Education)})
        params.Add(New SqlParameter("@IDExpiredDate", SqlDbType.Date) With {.Value = oCustomer.IDExpiredDate})
        params.Add(New SqlParameter("@Nationality", SqlDbType.Char, 3) With {.Value = IIf(oCustomer.Nationality = "Select One", "", oCustomer.Nationality)})
        params.Add(New SqlParameter("@WNACountry", SqlDbType.VarChar, 50) With {.Value = oCustomer.WNACountry})
        params.Add(New SqlParameter("@HomeStatus", SqlDbType.VarChar, 10) With {.Value = IIf(oCustomer.HomeStatus = "Select One", "", oCustomer.HomeStatus)})
        params.Add(New SqlParameter("@RentFinishDate", SqlDbType.VarChar, 8) With {.Value = oCustomer.RentFinishDate})
        params.Add(New SqlParameter("@HomeLocation", SqlDbType.VarChar, 10) With {.Value = IIf(oCustomer.HomeLocation = "Select One", "", oCustomer.HomeLocation)})
        params.Add(New SqlParameter("@HomePrice", SqlDbType.Decimal) With {.Value = CDec(IIf(oCustomer.HomePrice = "", "0", oCustomer.HomePrice))})
        params.Add(New SqlParameter("@StaySinceYear", SqlDbType.SmallInt) With {.Value = CInt(IIf(oCustomer.StaySinceYear = "", "0", oCustomer.StaySinceYear))})
        params.Add(New SqlParameter("@LegalAddress", SqlDbType.VarChar, 100) With {.Value = oLegalAddress.Address})
        params.Add(New SqlParameter("@LegalRT", SqlDbType.Char, 3) With {.Value = oLegalAddress.RT})
        params.Add(New SqlParameter("@LegalRW", SqlDbType.Char, 3) With {.Value = oLegalAddress.RW})
        params.Add(New SqlParameter("@LegalKelurahan", SqlDbType.VarChar, 30) With {.Value = oLegalAddress.Kelurahan})
        params.Add(New SqlParameter("@LegalKecamatan", SqlDbType.VarChar, 30) With {.Value = oLegalAddress.Kecamatan})
        params.Add(New SqlParameter("@LegalCity", SqlDbType.VarChar, 30) With {.Value = oLegalAddress.City})
        params.Add(New SqlParameter("@LegalZipCode", SqlDbType.Char, 5) With {.Value = oLegalAddress.ZipCode})
        params.Add(New SqlParameter("@LegalAreaPhone1", SqlDbType.Char, 4) With {.Value = oLegalAddress.AreaPhone1})
        params.Add(New SqlParameter("@LegalPhone1", SqlDbType.Char, 10) With {.Value = oLegalAddress.Phone1})
        params.Add(New SqlParameter("@LegalAreaPhone2", SqlDbType.Char, 4) With {.Value = oLegalAddress.AreaPhone2})
        params.Add(New SqlParameter("@LegalPhone2", SqlDbType.Char, 10) With {.Value = oLegalAddress.Phone2})
        params.Add(New SqlParameter("@LegalAreaFax", SqlDbType.Char, 4) With {.Value = oLegalAddress.AreaFax})
        params.Add(New SqlParameter("@LegalFax", SqlDbType.Char, 10) With {.Value = oLegalAddress.Fax})
        params.Add(New SqlParameter("@ResidenceAddress", SqlDbType.VarChar, 100) With {.Value = oResAddress.Address})
        params.Add(New SqlParameter("@ResidenceRT", SqlDbType.Char, 3) With {.Value = oResAddress.RT})
        params.Add(New SqlParameter("@ResidenceRW", SqlDbType.Char, 3) With {.Value = oResAddress.RW})
        params.Add(New SqlParameter("@ResidenceKelurahan", SqlDbType.VarChar, 30) With {.Value = oResAddress.Kelurahan})
        params.Add(New SqlParameter("@ResidenceKecamatan", SqlDbType.VarChar, 30) With {.Value = oResAddress.Kecamatan})
        params.Add(New SqlParameter("@ResidenceCity", SqlDbType.VarChar, 30) With {.Value = oResAddress.City})
        params.Add(New SqlParameter("@ResidenceZipCode", SqlDbType.Char, 5) With {.Value = oResAddress.ZipCode})
        params.Add(New SqlParameter("@ResidenceAreaPhone1", SqlDbType.Char, 4) With {.Value = oResAddress.AreaPhone1})
        params.Add(New SqlParameter("@ResidencePhone1", SqlDbType.Char, 10) With {.Value = oResAddress.Phone1})
        params.Add(New SqlParameter("@ResidenceAreaPhone2", SqlDbType.Char, 4) With {.Value = oResAddress.AreaPhone2})
        params.Add(New SqlParameter("@ResidencePhone2", SqlDbType.Char, 10) With {.Value = oResAddress.Phone2})
        params.Add(New SqlParameter("@ResidenceAreaFax", SqlDbType.Char, 4) With {.Value = oResAddress.AreaFax})
        params.Add(New SqlParameter("@ResidenceFax", SqlDbType.Char, 10) With {.Value = oResAddress.Fax})
        params.Add(New SqlParameter("@PersonalCustomerType", SqlDbType.Char, 1) With {.Value = oCustomer.PersonalCustomerType})
        params.Add(New SqlParameter("@businessdate", SqlDbType.DateTime) With {.Value = oCustomer.BusinessDate})
        params.Add(New SqlParameter("@MotherName", SqlDbType.VarChar, 50) With {.Value = oCustomer.MotherName})
        params.Add(New SqlParameter("@Gelar", SqlDbType.VarChar, 50) With {.Value = oCustomer.Gelar})
        params.Add(New SqlParameter("@KondisiRumah", SqlDbType.VarChar, 20) With {.Value = oCustomer.KondisiRumah})
        params.Add(New SqlParameter("@GelarBelakang", SqlDbType.VarChar, 50) With {.Value = oCustomer.GelarBelakang})

        params.Add(New SqlParameter("@MobilePhone1", SqlDbType.VarChar, 20) With {.Value = oCustomer.MobilePhone1})

        params.Add(New SqlParameter("@KegiatanUsaha", SqlDbType.VarChar, 1) With {.Value = oCustomer.KegiatanUsaha})
        params.Add(New SqlParameter("@JenisPembiayaan", SqlDbType.VarChar, 10) With {.Value = oCustomer.JenisPembiayaan})
        params.Add(New SqlParameter("@IsOLS", SqlDbType.Bit) With {.Value = oCustomer.IsOLS})
    End Sub

    Public Function PersonalCustomerIdentitasAdd(ByVal oCustomer As Parameter.Customer,
                                    ByVal oLegalAddress As Parameter.Address,
                                    ByVal oResAddress As Parameter.Address,
                                    ByVal oPCEX As Parameter.CustomerEX) As Parameter.Customer

        Dim conn As New SqlConnection(oCustomer.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim CustomerID As String = ""
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        'Dim params(59) As SqlParameter
        Dim paramsPCEX(3) As SqlParameter

        Dim oReturn As New Parameter.Customer
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            PersonalCustomerIdentitasParams(params, oCustomer, oLegalAddress, oResAddress)

            params.Add(New SqlParameter("@NamaSuamiIstri", SqlDbType.VarChar, 50) With {.Value = oCustomer.NamaSuamiIstri})
            Dim errPrm = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            params.Add(errPrm)

            Dim cusidPrm = New SqlParameter("@CustID", SqlDbType.Char, 20) With {.Direction = ParameterDirection.Output}
            params.Add(cusidPrm)

            params.Add(New SqlParameter("@ProspectAppID", SqlDbType.VarChar, 20) With {.Value = oCustomer.ProspectAppID})

            Dim oReturnValue As New Parameter.Customer

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCustomerPersonalIdentitasSaveAdd", params.ToArray())

            ErrMessage = CType(errPrm.Value, String)
            CustomerID = CType(cusidPrm.Value, String)

            If ErrMessage <> "" Then
                oReturn.CustomerID = CustomerID
                oReturn.Err = ErrMessage
                Return oReturn
                Exit Function
            End If

            paramsPCEX(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            paramsPCEX(0).Value = CustomerID
            paramsPCEX(1) = New SqlParameter("@SPTTahunan", SqlDbType.Bit)
            paramsPCEX(1).Value = oPCEX.SPTTahunan
            paramsPCEX(2) = New SqlParameter("@TinggalSejakBulan", SqlDbType.TinyInt)
            paramsPCEX(2).Value = oPCEX.TinggalSejakBulan
            paramsPCEX(3) = New SqlParameter("@KeteranganUsaha", SqlDbType.VarChar, 50)
            paramsPCEX(3).Value = oPCEX.KeteranganUsaha

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPersonalCustomerEXIdentitasAddEdit", paramsPCEX)

            transaction.Commit()
            oReturn.CustomerID = CustomerID
            oReturn.Err = ""
            Return oReturn
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Customer", "PersonalSaveAdd", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.PersonalSaveAdd")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
    Public Function PersonalCustomerIdentitasSaveEdit(ByVal oCustomer As Parameter.Customer,
                                   ByVal oLegalAddress As Parameter.Address,
                                   ByVal oResAddress As Parameter.Address,
                                   ByVal oPCEX As Parameter.CustomerEX) As String

        Dim conn As New SqlConnection(oCustomer.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""

        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Dim paramsPCEX(11) As SqlParameter

        Dim oReturn As New Parameter.Customer
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            PersonalCustomerIdentitasParams(params, oCustomer, oLegalAddress, oResAddress)
            params.Add(New SqlParameter("@CustomerID", SqlDbType.VarChar, 20) With {.Value = oCustomer.CustomerID})
            Dim oReturnValue As New Parameter.Customer

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCustomerPersonalIdentitasSaveEdit", params.ToArray())

            paramsPCEX(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            paramsPCEX(0).Value = IIf(IsDBNull(oCustomer.CustomerID), "", oCustomer.CustomerID)
            paramsPCEX(1) = New SqlParameter("@SPTTahunan", SqlDbType.Bit)
            paramsPCEX(1).Value = IIf(IsDBNull(oPCEX.SPTTahunan), 1, oPCEX.SPTTahunan)
            paramsPCEX(2) = New SqlParameter("@TinggalSejakBulan", SqlDbType.TinyInt)
            paramsPCEX(2).Value = IIf(IsDBNull(oPCEX.TinggalSejakBulan), 1, oPCEX.TinggalSejakBulan)
            paramsPCEX(3) = New SqlParameter("@KeteranganUsaha", SqlDbType.VarChar, 50)
            paramsPCEX(3).Value = IIf(IsDBNull(oPCEX.KeteranganUsaha), "", oPCEX.KeteranganUsaha)
            paramsPCEX(4) = New SqlParameter("@KategoryPers", SqlDbType.Char, 1)
            paramsPCEX(4).Value = IIf(IsDBNull(oPCEX.KategoryPers), "", oPCEX.KategoryPers)
            paramsPCEX(5) = New SqlParameter("@KondisiKantor", SqlDbType.Char, 1)
            paramsPCEX(5).Value = IIf(IsDBNull(oPCEX.KondisiKantor), "", oPCEX.KondisiKantor)
            paramsPCEX(6) = New SqlParameter("@KaryawanSejakBulan", SqlDbType.TinyInt)
            paramsPCEX(6).Value = IIf(IsDBNull(oPCEX.KaryawanSejakBulan), 0, oPCEX.KaryawanSejakBulan)
            paramsPCEX(7) = New SqlParameter("@SaldoAwal", SqlDbType.Money)
            paramsPCEX(7).Value = IIf(IsDBNull(oPCEX.SaldoAwal), 0, oPCEX.SaldoAwal)
            paramsPCEX(8) = New SqlParameter("@SaldoAkhir", SqlDbType.Money)
            paramsPCEX(8).Value = IIf(IsDBNull(oPCEX.SaldoAkhir), 0, oPCEX.SaldoAkhir)
            paramsPCEX(9) = New SqlParameter("@JumlahHariTransaksi", SqlDbType.TinyInt)
            paramsPCEX(9).Value = IIf(IsDBNull(oPCEX.JumlahHariTransaksi), 0, oPCEX.JumlahHariTransaksi)
            paramsPCEX(10) = New SqlParameter("@TotalLamaKerja", SqlDbType.TinyInt)
			paramsPCEX(10).Value = IIf(IsDBNull(oPCEX.TotalLamaKerja), 0, oPCEX.TotalLamaKerja)
			paramsPCEX(11) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
			paramsPCEX(11).Value = IIf(IsDBNull(oCustomer.Name), "", oCustomer.Name)
			'paramsPCEX(11) = New SqlParameter("@JumlahKendaraan", SqlDbType.TinyInt)
			'paramsPCEX(11).Value = oPCEX.JumlahKendaraan
			'paramsPCEX(12) = New SqlParameter("@Garasi", SqlDbType.Bit)
			'paramsPCEX(12).Value = oPCEX.Garasi
			'paramsPCEX(13) = New SqlParameter("@PertamaKredit", SqlDbType.Char, 2)
			'paramsPCEX(13).Value = oPCEX.PertamaKredit
			'paramsPCEX(14) = New SqlParameter("@OrderKe", SqlDbType.TinyInt)
			'paramsPCEX(14).Value = oPCEX.OrderKe

			SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPersonalCustomerEXAllAddEdit", paramsPCEX)

            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Customer", "PersonalSaveEdit", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.PersonalSaveEdit")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function PersonalSaveEdit(ByVal oCustomer As Parameter.Customer,
                                     ByVal oLegalAddress As Parameter.Address,
                                     ByVal oResAddress As Parameter.Address,
                                     ByVal oJDAddress As Parameter.Address,
                                     ByVal oDataOmset As DataTable,
                                     ByVal oDataFamily As DataTable,
                                     ByVal oPCEX As Parameter.CustomerEX,
                                     ByVal oPCSI As Parameter.CustomerSI,
                                     ByVal oPCSIAddress As Parameter.Address,
                                     ByVal oPCER As Parameter.CustomerER,
                                     ByVal oPCERAddress As Parameter.Address) As String
        Dim conn As New SqlConnection(oCustomer.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim intLoopOmset As Integer
        Dim paramsOmset(4) As SqlParameter
        Dim paramsFamily(6) As SqlParameter
        Dim paramsPCEX(11) As SqlParameter
        Dim paramsPCSI(36) As SqlParameter
        Dim paramsPCER(14) As SqlParameter

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            Dim params(97) As SqlParameter

            params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params(0).Value = oCustomer.CustomerID
            params(1) = New SqlParameter("@Notes", SqlDbType.Text)
            params(1).Value = oCustomer.Notes
            params(2) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
            params(2).Value = IIf(oCustomer.AccountName <> "", Replace(oCustomer.AccountName, "'", "''"), oCustomer.AccountName)
            params(3) = New SqlParameter("@CustomerGroupID", SqlDbType.Char, 20)
            params(3).Value = oCustomer.CustomerGroupID
            params(4) = New SqlParameter("@PersonalCustomerType", SqlDbType.Char, 1)
            params(4).Value = oCustomer.PersonalCustomerType
            params(5) = New SqlParameter("@BankBranch", SqlDbType.VarChar, 50)
            params(5).Value = oCustomer.BankBranch
            params(6) = New SqlParameter("@AccountNo", SqlDbType.Char, 20)
            params(6).Value = oCustomer.AccountNo
            params(7) = New SqlParameter("@Reference", SqlDbType.VarChar, 10)
            params(7).Value = IIf(oCustomer.Reference = "Select One", "", oCustomer.Reference)
            params(8) = New SqlParameter("@IsApplyCarLoanBefore", SqlDbType.Bit)
            params(8).Value = CInt(IIf(oCustomer.IsApplyCarLoanBefore = "0", 0, 1))
            params(9) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20)
            params(9).Value = oCustomer.MobilePhone
            params(10) = New SqlParameter("@EMail", SqlDbType.VarChar, 30)
            params(10).Value = oCustomer.Email
            params(11) = New SqlParameter("@Religion", SqlDbType.VarChar, 10)
            params(11).Value = IIf(oCustomer.Religion = "Select One", "", oCustomer.Religion)
            params(12) = New SqlParameter("@MaritalStatus", SqlDbType.VarChar, 10)
            params(12).Value = IIf(oCustomer.MaritalStatus = "Select One", "", oCustomer.MaritalStatus)
            params(13) = New SqlParameter("@NumOfDependence", SqlDbType.SmallInt)
            params(13).Value = CInt(IIf(oCustomer.NumOfDependence = "", "0", oCustomer.NumOfDependence))
            params(14) = New SqlParameter("@PersonalNPWP", SqlDbType.VarChar, 25)
            params(14).Value = oCustomer.PersonalNPWP
            params(15) = New SqlParameter("@NoKK", SqlDbType.VarChar, 20)
            params(15).Value = oCustomer.NoKK
            params(16) = New SqlParameter("@Education", SqlDbType.VarChar, 10)
            params(16).Value = IIf(oCustomer.Education = "Select One", "", oCustomer.Education)
            params(17) = New SqlParameter("@ProfessionID", SqlDbType.VarChar, 10)
            params(17).Value = IIf(oCustomer.ProfessionID = "Select One", "", oCustomer.ProfessionID)
            params(18) = New SqlParameter("@Nationality", SqlDbType.Char, 3)
            params(18).Value = IIf(oCustomer.Nationality = "Select One", "", oCustomer.Nationality)
            params(19) = New SqlParameter("@WNACountry", SqlDbType.VarChar, 50)
            params(19).Value = oCustomer.WNACountry
            params(20) = New SqlParameter("@HomeStatus", SqlDbType.VarChar, 10)
            params(20).Value = IIf(oCustomer.HomeStatus = "Select One", "", oCustomer.HomeStatus)
            params(21) = New SqlParameter("@RentFinishDate", SqlDbType.VarChar, 8)
            params(21).Value = oCustomer.RentFinishDate
            params(22) = New SqlParameter("@HomeLocation", SqlDbType.VarChar, 10)
            params(22).Value = IIf(oCustomer.HomeLocation = "Select One", "", oCustomer.HomeLocation)
            params(23) = New SqlParameter("@HomePrice", SqlDbType.Decimal)
            params(23).Value = CDec(IIf(oCustomer.HomePrice = "", "0", oCustomer.HomePrice))
            params(24) = New SqlParameter("@StaySinceYear", SqlDbType.SmallInt)
            params(24).Value = CInt(IIf(oCustomer.StaySinceYear = "", "0", oCustomer.StaySinceYear))
            params(25) = New SqlParameter("@LegalAddress", SqlDbType.VarChar, 100)
            params(25).Value = oLegalAddress.Address
            params(26) = New SqlParameter("@LegalRT", SqlDbType.Char, 3)
            params(26).Value = oLegalAddress.RT
            params(27) = New SqlParameter("@LegalRW", SqlDbType.Char, 3)
            params(27).Value = oLegalAddress.RW
            params(28) = New SqlParameter("@LegalKelurahan", SqlDbType.VarChar, 30)
            params(28).Value = oLegalAddress.Kelurahan
            params(29) = New SqlParameter("@LegalKecamatan", SqlDbType.VarChar, 30)
            params(29).Value = oLegalAddress.Kecamatan
            params(30) = New SqlParameter("@LegalCity", SqlDbType.VarChar, 30)
            params(30).Value = oLegalAddress.City
            params(31) = New SqlParameter("@LegalZipCode", SqlDbType.Char, 5)
            params(31).Value = oLegalAddress.ZipCode
            params(32) = New SqlParameter("@LegalAreaPhone1", SqlDbType.Char, 4)
            params(32).Value = oLegalAddress.AreaPhone1
            params(33) = New SqlParameter("@LegalPhone1", SqlDbType.Char, 10)
            params(33).Value = oLegalAddress.Phone1
            params(34) = New SqlParameter("@LegalAreaPhone2", SqlDbType.Char, 4)
            params(34).Value = oLegalAddress.AreaPhone2
            params(35) = New SqlParameter("@LegalPhone2", SqlDbType.Char, 10)
            params(35).Value = oLegalAddress.Phone2
            params(36) = New SqlParameter("@LegalAreaFax", SqlDbType.Char, 4)
            params(36).Value = oLegalAddress.AreaFax
            params(37) = New SqlParameter("@LegalFax", SqlDbType.Char, 10)
            params(37).Value = oLegalAddress.Fax
            params(38) = New SqlParameter("@ResidenceAddress", SqlDbType.VarChar, 100)
            params(38).Value = oResAddress.Address
            params(39) = New SqlParameter("@ResidenceRT", SqlDbType.Char, 3)
            params(39).Value = oResAddress.RT
            params(40) = New SqlParameter("@ResidenceRW", SqlDbType.Char, 3)
            params(40).Value = oResAddress.RW
            params(41) = New SqlParameter("@ResidenceKelurahan", SqlDbType.VarChar, 30)
            params(41).Value = oResAddress.Kelurahan
            params(42) = New SqlParameter("@ResidenceKecamatan", SqlDbType.VarChar, 30)
            params(42).Value = oResAddress.Kecamatan
            params(43) = New SqlParameter("@ResidenceCity", SqlDbType.VarChar, 30)
            params(43).Value = oResAddress.City
            params(44) = New SqlParameter("@ResidenceZipCode", SqlDbType.Char, 5)
            params(44).Value = oResAddress.ZipCode
            params(45) = New SqlParameter("@ResidenceAreaPhone1", SqlDbType.Char, 4)
            params(45).Value = oResAddress.AreaPhone1
            params(46) = New SqlParameter("@ResidencePhone1", SqlDbType.Char, 10)
            params(46).Value = oResAddress.Phone1
            params(47) = New SqlParameter("@ResidenceAreaPhone2", SqlDbType.Char, 4)
            params(47).Value = oResAddress.AreaPhone2
            params(48) = New SqlParameter("@ResidencePhone2", SqlDbType.Char, 10)
            params(48).Value = oResAddress.Phone2
            params(49) = New SqlParameter("@ResidenceAreaFax", SqlDbType.Char, 4)
            params(49).Value = oResAddress.AreaFax
            params(50) = New SqlParameter("@ResidenceFax", SqlDbType.Char, 10)
            params(50).Value = oResAddress.Fax
            params(51) = New SqlParameter("@JobTypeID", SqlDbType.VarChar, 10)
            params(51).Value = IIf(oCustomer.JobTypeID = "Select One", "", oCustomer.JobTypeID)
            params(52) = New SqlParameter("@JobPos", SqlDbType.VarChar, 10)
            params(52).Value = IIf(oCustomer.JobPos = "Select One", "", oCustomer.JobPos)
            params(53) = New SqlParameter("@CompanyName", SqlDbType.VarChar, 50)
            params(53).Value = oCustomer.CompanyName
            params(54) = New SqlParameter("@IndustryTypeID", SqlDbType.Char, 10)
            params(54).Value = IIf(oCustomer.IndustryTypeID = "Select One", "", oCustomer.IndustryTypeID)
            params(55) = New SqlParameter("@CompanyAddress", SqlDbType.VarChar, 100)
            params(55).Value = oJDAddress.Address
            params(56) = New SqlParameter("@CompanyRT", SqlDbType.Char, 3)
            params(56).Value = oJDAddress.RT
            params(57) = New SqlParameter("@CompanyRW", SqlDbType.Char, 3)
            params(57).Value = oJDAddress.RW
            params(58) = New SqlParameter("@CompanyKelurahan", SqlDbType.VarChar, 30)
            params(58).Value = oJDAddress.Kelurahan
            params(59) = New SqlParameter("@CompanyKecamatan", SqlDbType.VarChar, 30)
            params(59).Value = oJDAddress.Kecamatan
            params(60) = New SqlParameter("@CompanyCity", SqlDbType.VarChar, 30)
            params(60).Value = oJDAddress.City
            params(61) = New SqlParameter("@CompanyZipCode", SqlDbType.Char, 5)
            params(61).Value = oJDAddress.ZipCode
            params(62) = New SqlParameter("@CompanyAreaPhone1", SqlDbType.Char, 4)
            params(62).Value = oJDAddress.AreaPhone1
            params(63) = New SqlParameter("@CompanyPhone1", SqlDbType.Char, 10)
            params(63).Value = oJDAddress.Phone1
            params(64) = New SqlParameter("@CompanyAreaPhone2", SqlDbType.Char, 4)
            params(64).Value = oJDAddress.AreaPhone2
            params(65) = New SqlParameter("@CompanyPhone2", SqlDbType.Char, 10)
            params(65).Value = oJDAddress.Phone2
            params(66) = New SqlParameter("@CompanyAreaFax", SqlDbType.Char, 4)
            params(66).Value = oJDAddress.AreaFax
            params(67) = New SqlParameter("@CompanyFax", SqlDbType.Char, 10)
            params(67).Value = oJDAddress.Fax
            params(68) = New SqlParameter("@CompanyJobTitle", SqlDbType.VarChar, 50)
            params(68).Value = oCustomer.CompanyJobTitle
            params(69) = New SqlParameter("@EmploymentSinceYear", SqlDbType.SmallInt)
            params(69).Value = CInt(IIf(oCustomer.EmploymentSinceYear = "", "0", oCustomer.EmploymentSinceYear))
            params(70) = New SqlParameter("@MonthlyFixedIncome", SqlDbType.Decimal)
            params(70).Value = CDec(IIf(oCustomer.MonthlyFixedIncome = "", "0", oCustomer.MonthlyFixedIncome))
            params(71) = New SqlParameter("@MonthlyVariableIncome", SqlDbType.Decimal)
            params(71).Value = CDec(IIf(oCustomer.MonthlyVariableIncome = "", "0", oCustomer.MonthlyVariableIncome))
            params(72) = New SqlParameter("@OtherBusinessName", SqlDbType.VarChar, 50)
            params(72).Value = oCustomer.OtherBusinessName
            params(73) = New SqlParameter("@OtherBusinessType", SqlDbType.VarChar, 50)
            params(73).Value = oCustomer.OtherBusinessType
            params(74) = New SqlParameter("@OtherBusinessIndustryTypeID", SqlDbType.Char, 10)
            params(74).Value = IIf(oCustomer.OtherBusinessIndustryTypeID = "Select One", "", oCustomer.OtherBusinessIndustryTypeID)
            params(75) = New SqlParameter("@OtherBusinessJobTitle", SqlDbType.VarChar, 50)
            params(75).Value = oCustomer.OtherBusinessJobTitle
            params(76) = New SqlParameter("@OtherBusinessSinceYear", SqlDbType.SmallInt)
            params(76).Value = CInt(IIf(oCustomer.OtherBusinessSinceYear = "", "0", oCustomer.OtherBusinessSinceYear))
            params(77) = New SqlParameter("@NumOfCreditCard", SqlDbType.SmallInt)
            params(77).Value = CInt(IIf(oCustomer.NumOfCreditCard = "", "0", oCustomer.NumOfCreditCard))
            params(78) = New SqlParameter("@BankID", SqlDbType.Char, 5)
            params(78).Value = IIf(oCustomer.BankID = "0", "", oCustomer.BankID)
            params(79) = New SqlParameter("@SpouseIncome", SqlDbType.Decimal)
            params(79).Value = CDec(IIf(oCustomer.SpouseIncome = "", "0", oCustomer.SpouseIncome))
            params(80) = New SqlParameter("@AvgBalanceAccount", SqlDbType.Decimal)
            params(80).Value = CDec(IIf(oCustomer.AvgBalanceAccount = "", "0", oCustomer.AvgBalanceAccount))
            params(81) = New SqlParameter("@BankAccountType", SqlDbType.VarChar, 10)
            params(81).Value = IIf(oCustomer.BankAccountType = "Select One", "", oCustomer.BankAccountType)
            params(82) = New SqlParameter("@AverageDebitTransaction", SqlDbType.Decimal)
            params(82).Value = CDec(IIf(oCustomer.AverageDebitTransaction = "", "0", oCustomer.AverageDebitTransaction))
            params(83) = New SqlParameter("@AverageCreditTransaction", SqlDbType.Decimal)
            params(83).Value = CDec(IIf(oCustomer.AverageCreditTransaction = "", "0", oCustomer.AverageCreditTransaction))
            params(84) = New SqlParameter("@AverageBalance", SqlDbType.Decimal)
            params(84).Value = CDec(IIf(oCustomer.AverageBalance = "", "0", oCustomer.AverageBalance))
            params(85) = New SqlParameter("@Deposito", SqlDbType.Decimal)
            params(85).Value = CDec(IIf(oCustomer.Deposito = "", "0", oCustomer.Deposito))
            params(86) = New SqlParameter("@LivingCostAmount", SqlDbType.Decimal)
            params(86).Value = CDec(IIf(oCustomer.LivingCostAmount = "", "0", oCustomer.LivingCostAmount))
            params(87) = New SqlParameter("@OtherLoanInstallment", SqlDbType.Decimal)
            params(87).Value = CDec(IIf(oCustomer.OtherLoanInstallment = "", "0", oCustomer.OtherLoanInstallment))
            params(88) = New SqlParameter("@AdditionalCollateralType", SqlDbType.VarChar, 50)
            params(88).Value = oCustomer.AdditionalCollateralType
            params(89) = New SqlParameter("@AdditionalCollateralAmount", SqlDbType.Decimal)
            params(89).Value = CDec(IIf(oCustomer.AdditionalCollateralAmount = "", "0", oCustomer.AdditionalCollateralAmount))
            params(90) = New SqlParameter("@CreditCardID", SqlDbType.VarChar, 10)
            params(90).Value = IIf(oCustomer.CreditCardID = "Select One", "", oCustomer.CreditCardID)
            params(91) = New SqlParameter("@CreditCardType", SqlDbType.Char, 1)
            params(91).Value = oCustomer.CreditCardType
            params(92) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(92).Value = oCustomer.Name
            params(93) = New SqlParameter("@MotherName", SqlDbType.VarChar, 50)
            params(93).Value = oCustomer.MotherName
            params(94) = New SqlParameter("@Gelar", SqlDbType.VarChar, 50)
            params(94).Value = oCustomer.Gelar
            params(95) = New SqlParameter("@IDExpiredDate", SqlDbType.SmallDateTime)
            params(95).Value = oCustomer.IDExpiredDate
            params(96) = New SqlParameter("@GelarBelakang", SqlDbType.VarChar, 50)
            params(96).Value = oCustomer.GelarBelakang
            params(97) = New SqlParameter("@KondisiRumah", SqlDbType.VarChar, 20)
            params(97).Value = oCustomer.KondisiRumah
            'params(98) = New SqlParameter("@IsOLS", SqlDbType.Bit)
            'params(98).Value = oCustomer.IsOLS


            Dim oReturnValue As New Parameter.Customer
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spPersonalSaveEdit, params)
            Dim Delete As String = "1"

            If oDataOmset.Rows.Count > 0 Then
                paramsOmset(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                paramsOmset(1) = New SqlParameter("@OmsetYear", SqlDbType.SmallInt)
                paramsOmset(2) = New SqlParameter("@OmsetMonth", SqlDbType.SmallInt)
                paramsOmset(3) = New SqlParameter("@Omset", SqlDbType.Decimal)
                paramsOmset(4) = New SqlParameter("@Delete", SqlDbType.Char, 1)
                For intLoopOmset = 0 To oDataOmset.Rows.Count - 1
                    paramsOmset(0).Value = oCustomer.CustomerID
                    paramsOmset(1).Value = oDataOmset.Rows(intLoopOmset).Item("Year")
                    paramsOmset(2).Value = oDataOmset.Rows(intLoopOmset).Item("Month")
                    paramsOmset(3).Value = oDataOmset.Rows(intLoopOmset).Item("Omset")
                    paramsOmset(4).Value = Delete
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spPersonalSaveEdit2, paramsOmset)
                    Delete = "0"
                Next
            Else
                paramsOmset(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                paramsOmset(0).Value = oCustomer.CustomerID
                paramsOmset(1) = New SqlParameter("@OmsetYear", SqlDbType.SmallInt)
                paramsOmset(1).Value = 0
                paramsOmset(2) = New SqlParameter("@OmsetMonth", SqlDbType.SmallInt)
                paramsOmset(2).Value = 0
                paramsOmset(3) = New SqlParameter("@Omset", SqlDbType.Decimal)
                paramsOmset(3).Value = 0
                paramsOmset(4) = New SqlParameter("@Delete", SqlDbType.Char, 1)
                paramsOmset(4).Value = "3"
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spPersonalSaveEdit2, paramsOmset)
            End If
            If oDataFamily.Rows.Count > 0 Then
                Delete = "1"
                paramsFamily(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                paramsFamily(1) = New SqlParameter("@FamilySeqNo", SqlDbType.SmallInt)
                paramsFamily(2) = New SqlParameter("@FamilyName", SqlDbType.VarChar, 50)
                paramsFamily(3) = New SqlParameter("@FamilyIDNumber", SqlDbType.VarChar, 25)
                paramsFamily(4) = New SqlParameter("@FamilyBirthDate", SqlDbType.VarChar, 8)
                paramsFamily(5) = New SqlParameter("@FamilyRelation", SqlDbType.VarChar, 10)
                paramsFamily(6) = New SqlParameter("@Delete", SqlDbType.Char, 1)
                For intLoopOmset = 0 To oDataFamily.Rows.Count - 1
                    paramsFamily(0).Value = oCustomer.CustomerID
                    paramsFamily(1).Value = CInt(oDataFamily.Rows(intLoopOmset).Item("FamilySeqNo"))
                    paramsFamily(2).Value = IIf(oDataFamily.Rows(intLoopOmset).Item("FamilyName").ToString.Trim <> "", Replace(oDataFamily.Rows(intLoopOmset).Item("FamilyName").ToString.Trim, "'", "''"), oDataFamily.Rows(intLoopOmset).Item("FamilyName").ToString.Trim)
                    paramsFamily(3).Value = oDataFamily.Rows(intLoopOmset).Item("FamilyIDNumber")
                    paramsFamily(4).Value = oDataFamily.Rows(intLoopOmset).Item("FamilyBirthDate")
                    paramsFamily(5).Value = oDataFamily.Rows(intLoopOmset).Item("FamilyRelation")
                    paramsFamily(6).Value = Delete
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spPersonalSaveEdit3, paramsFamily)
                    Delete = "0"
                Next
            Else
                paramsFamily(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                paramsFamily(0).Value = oCustomer.CustomerID
                paramsFamily(1) = New SqlParameter("@FamilySeqNo", SqlDbType.SmallInt)
                paramsFamily(1).Value = 0
                paramsFamily(2) = New SqlParameter("@FamilyName", SqlDbType.VarChar, 50)
                paramsFamily(2).Value = ""
                paramsFamily(3) = New SqlParameter("@FamilyIDNumber", SqlDbType.VarChar, 25)
                paramsFamily(3).Value = ""
                paramsFamily(4) = New SqlParameter("@FamilyBirthDate", SqlDbType.VarChar, 8)
                paramsFamily(4).Value = ""
                paramsFamily(5) = New SqlParameter("@FamilyRelation", SqlDbType.VarChar, 10)
                paramsFamily(5).Value = ""
                paramsFamily(6) = New SqlParameter("@Delete", SqlDbType.Char, 1)
                paramsFamily(6).Value = "3"
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spPersonalSaveEdit3, paramsFamily)
            End If


            paramsPCEX(0) = New SqlParameter("@Mode", SqlDbType.Bit)
            paramsPCEX(0).Value = 0
            paramsPCEX(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            paramsPCEX(1).Value = oCustomer.CustomerID
            paramsPCEX(2) = New SqlParameter("@SPTTahunan", SqlDbType.Bit)
            paramsPCEX(2).Value = oPCEX.SPTTahunan
            paramsPCEX(3) = New SqlParameter("@TinggalSejakBulan", SqlDbType.TinyInt)
            paramsPCEX(3).Value = oPCEX.TinggalSejakBulan
            paramsPCEX(4) = New SqlParameter("@KeteranganUsaha", SqlDbType.VarChar, 50)
            paramsPCEX(4).Value = oPCEX.KeteranganUsaha
            paramsPCEX(5) = New SqlParameter("@KategoryPers", SqlDbType.Char, 1)
            paramsPCEX(5).Value = oPCEX.KategoryPers
            paramsPCEX(6) = New SqlParameter("@KondisiKantor", SqlDbType.Char, 1)
            paramsPCEX(6).Value = oPCEX.KondisiKantor
            paramsPCEX(7) = New SqlParameter("@KaryawanSejakBulan", SqlDbType.TinyInt)
            paramsPCEX(7).Value = oPCEX.KaryawanSejakBulan
            paramsPCEX(8) = New SqlParameter("@SaldoAwal", SqlDbType.Money)
            paramsPCEX(8).Value = oPCEX.SaldoAwal
            paramsPCEX(9) = New SqlParameter("@SaldoAkhir", SqlDbType.Money)
            paramsPCEX(9).Value = oPCEX.SaldoAkhir
            paramsPCEX(10) = New SqlParameter("@JumlahHariTransaksi", SqlDbType.TinyInt)
            paramsPCEX(10).Value = oPCEX.JumlahHariTransaksi
            paramsPCEX(11) = New SqlParameter("@TotalLamaKerja", SqlDbType.TinyInt)
            paramsPCEX(11).Value = oPCEX.TotalLamaKerja
            'paramsPCEX(11) = New SqlParameter("@JumlahKendaraan", SqlDbType.TinyInt)
            'paramsPCEX(11).Value = oPCEX.JumlahKendaraan
            'paramsPCEX(12) = New SqlParameter("@Garasi", SqlDbType.Bit)
            'paramsPCEX(12).Value = oPCEX.Garasi
            'paramsPCEX(13) = New SqlParameter("@PertamaKredit", SqlDbType.Char, 2)
            'paramsPCEX(13).Value = oPCEX.PertamaKredit
            'paramsPCEX(14) = New SqlParameter("@OrderKe", SqlDbType.TinyInt)
            'paramsPCEX(14).Value = oPCEX.OrderKe

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPersonalCustomerEXAddEdit", paramsPCEX)

            paramsPCSI(0) = New SqlParameter("@Mode", SqlDbType.Bit)
            paramsPCSI(0).Value = 0
            paramsPCSI(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            paramsPCSI(1).Value = oCustomer.CustomerID
            paramsPCSI(2) = New SqlParameter("@NamaSuamiIstri", SqlDbType.VarChar, 50)
            paramsPCSI(2).Value = oPCSI.NamaSuamiIstri
            paramsPCSI(3) = New SqlParameter("@JenisPekerjaanID", SqlDbType.VarChar, 10)
            paramsPCSI(3).Value = oPCSI.JenisPekerjaanID
            paramsPCSI(4) = New SqlParameter("@NamaPers", SqlDbType.VarChar, 50)
            paramsPCSI(4).Value = oPCSI.NamaPers
            paramsPCSI(5) = New SqlParameter("@JenisIndustriID", SqlDbType.VarChar, 10)
            paramsPCSI(5).Value = oPCSI.JenisIndustriID
            paramsPCSI(6) = New SqlParameter("@KeteranganUsaha", SqlDbType.VarChar, 50)
            paramsPCSI(6).Value = oPCSI.KeteranganUsaha
            paramsPCSI(7) = New SqlParameter("@Alamat", SqlDbType.VarChar, 50)
            paramsPCSI(7).Value = oPCSIAddress.Address
            paramsPCSI(8) = New SqlParameter("@RT", SqlDbType.Char, 3)
            paramsPCSI(8).Value = oPCSIAddress.RT
            paramsPCSI(9) = New SqlParameter("@RW", SqlDbType.Char, 3)
            paramsPCSI(9).Value = oPCSIAddress.RW
            paramsPCSI(10) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 50)
            paramsPCSI(10).Value = oPCSIAddress.Kelurahan
            paramsPCSI(11) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 50)
            paramsPCSI(11).Value = oPCSIAddress.Kecamatan
            paramsPCSI(12) = New SqlParameter("@Kota", SqlDbType.VarChar, 50)
            paramsPCSI(12).Value = oPCSIAddress.City
            paramsPCSI(13) = New SqlParameter("@KodePos", SqlDbType.Char, 5)
            paramsPCSI(13).Value = oPCSIAddress.ZipCode
            paramsPCSI(14) = New SqlParameter("@KaryawanSejak", SqlDbType.TinyInt)
            paramsPCSI(14).Value = oPCSI.KaryawanSejak
            paramsPCSI(15) = New SqlParameter("@KaryawanSejakTahun", SqlDbType.Char, 4)
            paramsPCSI(15).Value = oPCSI.KaryawanSejakTahun
            paramsPCSI(16) = New SqlParameter("@PenghasilanTetap", SqlDbType.Decimal)
            paramsPCSI(16).Value = oPCSI.PenghasilanTetap
            paramsPCSI(17) = New SqlParameter("@PenghasilanTambahan", SqlDbType.Decimal)
            paramsPCSI(17).Value = oPCSI.PenghasilanTambahan
            paramsPCSI(18) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
            paramsPCSI(18).Value = oPCSIAddress.AreaPhone1
            paramsPCSI(19) = New SqlParameter("@Phone1", SqlDbType.Char, 10)
            paramsPCSI(19).Value = oPCSIAddress.Phone1
            paramsPCSI(20) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
            paramsPCSI(20).Value = oPCSIAddress.AreaPhone2
            paramsPCSI(21) = New SqlParameter("@Phone2", SqlDbType.Char, 10)
            paramsPCSI(21).Value = oPCSIAddress.Phone2
            paramsPCSI(22) = New SqlParameter("@AreaFax", SqlDbType.Char, 4)
            paramsPCSI(22).Value = oPCSIAddress.AreaFax
            paramsPCSI(23) = New SqlParameter("@Fax", SqlDbType.Char, 10)
            paramsPCSI(23).Value = oPCSIAddress.Fax

            'alamat KTP pasangan
            paramsPCSI(24) = New SqlParameter("@KtpAlamat", SqlDbType.VarChar, 50)
            paramsPCSI(24).Value = oPCSI.AlamatKTP.Address
            paramsPCSI(25) = New SqlParameter("@KtpRT", SqlDbType.Char, 3)
            paramsPCSI(25).Value = oPCSI.AlamatKTP.RT
            paramsPCSI(26) = New SqlParameter("@KtpRW", SqlDbType.Char, 3)
            paramsPCSI(26).Value = oPCSI.AlamatKTP.RW
            paramsPCSI(27) = New SqlParameter("@KtpKelurahan", SqlDbType.VarChar, 50)
            paramsPCSI(27).Value = oPCSI.AlamatKTP.Kelurahan
            paramsPCSI(28) = New SqlParameter("@KtpKecamatan", SqlDbType.VarChar, 50)
            paramsPCSI(28).Value = oPCSI.AlamatKTP.Kecamatan
            paramsPCSI(29) = New SqlParameter("@KtpKota", SqlDbType.VarChar, 50)
            paramsPCSI(29).Value = oPCSI.AlamatKTP.City
            paramsPCSI(30) = New SqlParameter("@KtpKodePos", SqlDbType.Char, 5)
            paramsPCSI(30).Value = oPCSI.AlamatKTP.ZipCode
            paramsPCSI(31) = New SqlParameter("@KtpAreaPhone1", SqlDbType.Char, 4)
            paramsPCSI(31).Value = oPCSI.AlamatKTP.AreaPhone1
            paramsPCSI(32) = New SqlParameter("@KtpPhone1", SqlDbType.Char, 10)
            paramsPCSI(32).Value = oPCSI.AlamatKTP.Phone1
            paramsPCSI(33) = New SqlParameter("@KtpAreaPhone2", SqlDbType.Char, 4)
            paramsPCSI(33).Value = oPCSI.AlamatKTP.AreaPhone2
            paramsPCSI(34) = New SqlParameter("@KtpPhone2", SqlDbType.Char, 10)
            paramsPCSI(34).Value = oPCSI.AlamatKTP.Phone2
            paramsPCSI(35) = New SqlParameter("@KtpAreaFax", SqlDbType.Char, 4)
            paramsPCSI(35).Value = oPCSI.AlamatKTP.AreaFax
            paramsPCSI(36) = New SqlParameter("@KtpFax", SqlDbType.Char, 10)
            paramsPCSI(36).Value = oPCSI.AlamatKTP.Fax


            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPersonalCustomerSIAddEdit", paramsPCSI)

            paramsPCER(0) = New SqlParameter("@Mode", SqlDbType.Bit)
            paramsPCER(0).Value = 0
            paramsPCER(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            paramsPCER(1).Value = oCustomer.CustomerID
            paramsPCER(2) = New SqlParameter("@Nama", SqlDbType.VarChar, 50)
            paramsPCER(2).Value = oPCER.Nama
            paramsPCER(3) = New SqlParameter("@Hubungan", SqlDbType.VarChar, 50)
            paramsPCER(3).Value = oPCER.Hubungan
            paramsPCER(4) = New SqlParameter("@Alamat", SqlDbType.VarChar, 50)
            paramsPCER(4).Value = oPCERAddress.Address
            paramsPCER(5) = New SqlParameter("@RT", SqlDbType.Char, 3)
            paramsPCER(5).Value = oPCERAddress.RT
            paramsPCER(6) = New SqlParameter("@RW", SqlDbType.Char, 3)
            paramsPCER(6).Value = oPCERAddress.RW
            paramsPCER(7) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 50)
            paramsPCER(7).Value = oPCERAddress.Kelurahan
            paramsPCER(8) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 50)
            paramsPCER(8).Value = oPCERAddress.Kecamatan
            paramsPCER(9) = New SqlParameter("@Kota", SqlDbType.VarChar, 50)
            paramsPCER(9).Value = oPCERAddress.City
            paramsPCER(10) = New SqlParameter("@KodePos", SqlDbType.Char, 5)
            paramsPCER(10).Value = oPCERAddress.ZipCode
            paramsPCER(11) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
            paramsPCER(11).Value = oPCERAddress.AreaPhone1
            paramsPCER(12) = New SqlParameter("@Phone1", SqlDbType.VarChar, 15)
            paramsPCER(12).Value = oPCERAddress.Phone1
            paramsPCER(13) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 15)
            paramsPCER(13).Value = oPCER.MobilePhone
            paramsPCER(14) = New SqlParameter("@Email", SqlDbType.VarChar, 50)
            paramsPCER(14).Value = oPCER.Email

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPersonalCustomerERAddEdit", paramsPCER)

            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Customer", "PersonalSaveEdit", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function PersonalCustomerPekerjaanSaveEdit(ByVal oCustomer As Parameter.Customer,
                                     ByVal oJDAddress As Parameter.Address,
                                     ByVal oPCEX As Parameter.CustomerEX) As String
        Dim conn As New SqlConnection(oCustomer.strConnection)
        Dim transaction As SqlTransaction = Nothing

		Dim paramsPCEX(11) As SqlParameter
		Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)

        'Dim params(22) As SqlParameter

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            params.Add(New SqlParameter("@CustomerID", SqlDbType.Char, 20) With {.Value = oCustomer.CustomerID})
            params.Add(New SqlParameter("@ProfessionID", SqlDbType.VarChar, 10) With {.Value = IIf(oCustomer.ProfessionID = "Select One", "", oCustomer.ProfessionID)})
            params.Add(New SqlParameter("@JobTypeID", SqlDbType.VarChar, 10) With {.Value = IIf(oCustomer.JobTypeID = "Select One", "", oCustomer.JobTypeID)})
            params.Add(New SqlParameter("@JobPos", SqlDbType.VarChar, 10) With {.Value = IIf(oCustomer.JobPos = "Select One", "", oCustomer.JobPos)})
            params.Add(New SqlParameter("@CompanyName", SqlDbType.VarChar, 50) With {.Value = oCustomer.CompanyName})
            params.Add(New SqlParameter("@IndustryTypeID", SqlDbType.Char, 10) With {.Value = IIf(oCustomer.IndustryTypeID = "Select One", "", oCustomer.IndustryTypeID)})
            params.Add(New SqlParameter("@CompanyAddress", SqlDbType.VarChar, 100) With {.Value = oJDAddress.Address})
            params.Add(New SqlParameter("@CompanyRT", SqlDbType.Char, 3) With {.Value = oJDAddress.RT})
            params.Add(New SqlParameter("@CompanyRW", SqlDbType.Char, 3) With {.Value = oJDAddress.RW})
            params.Add(New SqlParameter("@CompanyKelurahan", SqlDbType.VarChar, 30) With {.Value = oJDAddress.Kelurahan})
            params.Add(New SqlParameter("@CompanyKecamatan", SqlDbType.VarChar, 30) With {.Value = oJDAddress.Kecamatan})
            params.Add(New SqlParameter("@CompanyCity", SqlDbType.VarChar, 30) With {.Value = oJDAddress.City})
            params.Add(New SqlParameter("@CompanyZipCode", SqlDbType.Char, 5) With {.Value = oJDAddress.ZipCode})
            params.Add(New SqlParameter("@CompanyAreaPhone1", SqlDbType.Char, 4) With {.Value = oJDAddress.AreaPhone1})
            params.Add(New SqlParameter("@CompanyPhone1", SqlDbType.Char, 10) With {.Value = oJDAddress.Phone1})
            params.Add(New SqlParameter("@CompanyAreaPhone2", SqlDbType.Char, 4) With {.Value = oJDAddress.AreaPhone2})
            params.Add(New SqlParameter("@CompanyPhone2", SqlDbType.Char, 10) With {.Value = oJDAddress.Phone2})
            params.Add(New SqlParameter("@CompanyAreaFax", SqlDbType.Char, 4) With {.Value = oJDAddress.AreaFax})
            params.Add(New SqlParameter("@CompanyFax", SqlDbType.Char, 10) With {.Value = oJDAddress.Fax})
            params.Add(New SqlParameter("@CompanyJobTitle", SqlDbType.VarChar, 50) With {.Value = oCustomer.CompanyJobTitle})
            params.Add(New SqlParameter("@EmploymentSinceYear", SqlDbType.SmallInt) With {.Value = CInt(IIf(oCustomer.EmploymentSinceYear = "", "0", oCustomer.EmploymentSinceYear))})
            params.Add(New SqlParameter("@OtherBusinessName", SqlDbType.Char, 50) With {.Value = oCustomer.OtherBusinessName})
            params.Add(New SqlParameter("@PersonalCustomerType", SqlDbType.Char, 1) With {.Value = oCustomer.PersonalCustomerType})
            params.Add(New SqlParameter("@JobsKantorOperasiSejak", SqlDbType.Int) With {.Value = oCustomer.JobsKantorOperasiSejak})
            params.Add(New SqlParameter("@JobsKantorNPWP", SqlDbType.Char, 25) With {.Value = oCustomer.JobsKantorNPWP})

            params.Add(New SqlParameter("@NatureOfBusinessId", SqlDbType.Char, 20) With {.Value = oCustomer.NatureOfBusinessId})
            params.Add(New SqlParameter("@OccupationID", SqlDbType.Char, 20) With {.Value = oCustomer.OccupationID})
            params.Add(New SqlParameter("@IsRelatedBNI", SqlDbType.Bit) With {.Value = oCustomer.IsRelatedBNI})
            params.Add(New SqlParameter("@IsEmployee", SqlDbType.Bit) With {.Value = oCustomer.IsEmployee})

            params.Add(New SqlParameter("@IndustryRisk", SqlDbType.VarChar, 15) With {.Value = oCustomer.IndustryRisk})

            Dim oReturnValue As New Parameter.Customer
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCustomerPersonalPekerjaanSaveEdit", params.ToArray())

            paramsPCEX(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            paramsPCEX(0).Value = IIf(IsDBNull(oCustomer.CustomerID), "", oCustomer.CustomerID)
            paramsPCEX(1) = New SqlParameter("@SPTTahunan", SqlDbType.Bit)
            paramsPCEX(1).Value = IIf(IsDBNull(oPCEX.SPTTahunan), 1, oPCEX.SPTTahunan)
            paramsPCEX(2) = New SqlParameter("@TinggalSejakBulan", SqlDbType.TinyInt)
            paramsPCEX(2).Value = IIf(IsDBNull(oPCEX.TinggalSejakBulan), 1, oPCEX.TinggalSejakBulan)
            paramsPCEX(3) = New SqlParameter("@KeteranganUsaha", SqlDbType.VarChar, 50)
            paramsPCEX(3).Value = IIf(IsDBNull(oPCEX.KeteranganUsaha), "", oPCEX.KeteranganUsaha)
            paramsPCEX(4) = New SqlParameter("@KategoryPers", SqlDbType.Char, 1)
            paramsPCEX(4).Value = IIf(IsDBNull(oPCEX.KategoryPers), "", oPCEX.KategoryPers)
            paramsPCEX(5) = New SqlParameter("@KondisiKantor", SqlDbType.Char, 1)
            paramsPCEX(5).Value = IIf(IsDBNull(oPCEX.KondisiKantor), "", oPCEX.KondisiKantor)
            paramsPCEX(6) = New SqlParameter("@KaryawanSejakBulan", SqlDbType.TinyInt)
            paramsPCEX(6).Value = IIf(IsDBNull(oPCEX.KaryawanSejakBulan), 0, oPCEX.KaryawanSejakBulan)
            paramsPCEX(7) = New SqlParameter("@SaldoAwal", SqlDbType.Money)
            paramsPCEX(7).Value = IIf(IsDBNull(oPCEX.SaldoAwal), 0, oPCEX.SaldoAwal)
            paramsPCEX(8) = New SqlParameter("@SaldoAkhir", SqlDbType.Money)
            paramsPCEX(8).Value = IIf(IsDBNull(oPCEX.SaldoAkhir), 0, oPCEX.SaldoAkhir)
            paramsPCEX(9) = New SqlParameter("@JumlahHariTransaksi", SqlDbType.TinyInt)
            paramsPCEX(9).Value = IIf(IsDBNull(oPCEX.JumlahHariTransaksi), 0, oPCEX.JumlahHariTransaksi)
            paramsPCEX(10) = New SqlParameter("@TotalLamaKerja", SqlDbType.TinyInt)
			paramsPCEX(10).Value = IIf(IsDBNull(oPCEX.TotalLamaKerja), 0, oPCEX.TotalLamaKerja)
			paramsPCEX(11) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
			If oCustomer.Name = Nothing Then
				paramsPCEX(11).Value = ""
			Else
				paramsPCEX(11).Value = IIf(IsDBNull(oCustomer.Name), "", oCustomer.Name)
			End If

			'paramsPCEX(11) = New SqlParameter("@JumlahKendaraan", SqlDbType.TinyInt)
			'paramsPCEX(11).Value = oPCEX.JumlahKendaraan
			'paramsPCEX(12) = New SqlParameter("@Garasi", SqlDbType.Bit)
			'paramsPCEX(12).Value = oPCEX.Garasi
			'paramsPCEX(13) = New SqlParameter("@PertamaKredit", SqlDbType.Char, 2)
			'paramsPCEX(13).Value = oPCEX.PertamaKredit
			'paramsPCEX(14) = New SqlParameter("@OrderKe", SqlDbType.TinyInt)
			'paramsPCEX(14).Value = oPCEX.OrderKe

			SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPersonalCustomerEXAllAddEdit", paramsPCEX)

            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Customer", "PersonalSaveEdit", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
    Public Function PersonalCustomerKeuanganSaveEdit(ByVal oCustomer As Parameter.Customer) As String
        Dim conn As New SqlConnection(oCustomer.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        'Dim params(11) As SqlParameter

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            params.Add(New SqlParameter("@CustomerID", SqlDbType.Char, 20) With {.Value = oCustomer.CustomerID})
            params.Add(New SqlParameter("@AccountName", SqlDbType.VarChar, 50) With {.Value = IIf(oCustomer.AccountName <> "", Replace(oCustomer.AccountName, "'", "''"), oCustomer.AccountName)})
            params.Add(New SqlParameter("@BankBranch", SqlDbType.VarChar, 50) With {.Value = oCustomer.BankBranch})
            params.Add(New SqlParameter("@AccountNo", SqlDbType.Char, 20) With {.Value = oCustomer.AccountNo})
            params.Add(New SqlParameter("@MonthlyFixedIncome", SqlDbType.Decimal) With {.Value = CDec(IIf(oCustomer.MonthlyFixedIncome = "", "0", oCustomer.MonthlyFixedIncome))})
            params.Add(New SqlParameter("@MonthlyVariableIncome", SqlDbType.Decimal) With {.Value = CDec(IIf(oCustomer.MonthlyVariableIncome = "", "0", oCustomer.MonthlyVariableIncome))})
            params.Add(New SqlParameter("@BankID", SqlDbType.Char, 5) With {.Value = IIf(oCustomer.BankID = "0", "", oCustomer.BankID)})
            params.Add(New SqlParameter("@Deposito", SqlDbType.Decimal) With {.Value = CDec(IIf(oCustomer.Deposito = "", "0", oCustomer.Deposito))})
            params.Add(New SqlParameter("@LivingCostAmount", SqlDbType.Decimal) With {.Value = CDec(IIf(oCustomer.LivingCostAmount = "", "0", oCustomer.LivingCostAmount))})

            'params.Add(New SqlParameter("@AdditionalCollateralType", SqlDbType.VarChar, 50) With {.Value = oCustomer.AdditionalCollateralType})
            'params.Add(New SqlParameter("@AdditionalCollateralAmount", SqlDbType.Decimal) With {.Value = CDec(IIf(oCustomer.AdditionalCollateralAmount = "", "0", oCustomer.AdditionalCollateralAmount))})

            params.Add(New SqlParameter("@BankBranchId", SqlDbType.Int) With {.Value = oCustomer.BankBranchId})

            params.Add(New SqlParameter("@PenghasilanTetapPasangan", SqlDbType.Decimal) With {.Value = CDec(oCustomer.PenghasilanTetapPasangan.ToString())})
            params.Add(New SqlParameter("@PenghasilanTambahanPasangan", SqlDbType.Decimal) With {.Value = CDec(oCustomer.PenghasilanTambahanPasangan.ToString())})
            params.Add(New SqlParameter("@PeghasilanTetapPenjamin", SqlDbType.Decimal) With {.Value = CDec(oCustomer.PenghasilanTetapPenjamin.ToString())})
            params.Add(New SqlParameter("@PeghasilanTambahanPenjamin", SqlDbType.Decimal) With {.Value = CDec(oCustomer.PenghasilanTambahanPenjamin.ToString())})
            params.Add(New SqlParameter("@AngsuranLainnya", SqlDbType.Decimal) With {.Value = CDec(oCustomer.AngsuranLainnya.ToString())})
            params.Add(New SqlParameter("@RataRataPenghasilanPerTahun", SqlDbType.Char, 100) With {.Value = oCustomer.RataRataPenghasilanPerTahun})
            params.Add(New SqlParameter("@SumberPembiayaan", SqlDbType.Char, 50) With {.Value = oCustomer.KodeSumberPenghasilan})


            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCustomerPersonalKeuanganSaveEdit", params.ToArray())
            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Customer", "PersonalSaveEdit", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
    Public Function PersonalCustomerPasanganSaveEdit(ByVal oCustomer As Parameter.Customer,
                                     ByVal oPCSI As Parameter.CustomerSI,
                                     ByVal oPCSIAddress As Parameter.Address) As String
        Dim conn As New SqlConnection(oCustomer.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim paramsPCSI As IList(Of SqlParameter) = New List(Of SqlParameter)

        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            params.Add(New SqlParameter("@CustomerID", SqlDbType.Char, 20) With {.Value = oCustomer.CustomerID})
            params.Add(New SqlParameter("@MonthlyFixedIncome", SqlDbType.Decimal) With {.Value = CDec(IIf(oCustomer.MonthlyFixedIncome = "", "0", oCustomer.MonthlyFixedIncome))})
            params.Add(New SqlParameter("@MonthlyVariableIncome", SqlDbType.Decimal) With {.Value = CDec(IIf(oCustomer.MonthlyVariableIncome = "", "0", oCustomer.MonthlyVariableIncome))})
            params.Add(New SqlParameter("@OtherBusinessName", SqlDbType.VarChar, 50) With {.Value = oCustomer.OtherBusinessName})

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCustomerPersonalPasanganSaveEdit", params.ToArray())

            PenjaminParams(paramsPCSI, oCustomer, oPCSI, oPCSIAddress)

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPersonalCustomerSIAddEdit", paramsPCSI.ToArray())

            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Customer", "PersonalSaveEdit", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Sub PenjaminParams(paramsPCSI As IList(Of SqlParameter), oCustomer As Parameter.Customer, ByVal oPCSI As Parameter.CustomerSI, ByVal oPCSIAddress As Parameter.Address)

        paramsPCSI.Add(New SqlParameter("@CustomerID", SqlDbType.Char, 20) With {.Value = oCustomer.CustomerID})
        paramsPCSI.Add(New SqlParameter("@NamaSuamiIstri", SqlDbType.VarChar, 50) With {.Value = oPCSI.NamaSuamiIstri})
        paramsPCSI.Add(New SqlParameter("@JenisPekerjaanID", SqlDbType.VarChar, 10) With {.Value = oPCSI.JenisPekerjaanID})
        paramsPCSI.Add(New SqlParameter("@NamaPers", SqlDbType.VarChar, 50) With {.Value = oPCSI.NamaPers})
        paramsPCSI.Add(New SqlParameter("@JenisIndustriID", SqlDbType.VarChar, 10) With {.Value = oPCSI.JenisIndustriID})
        paramsPCSI.Add(New SqlParameter("@KeteranganUsaha", SqlDbType.VarChar, 50) With {.Value = oPCSI.KeteranganUsaha})
        paramsPCSI.Add(New SqlParameter("@Alamat", SqlDbType.VarChar, 50) With {.Value = oPCSIAddress.Address})
        paramsPCSI.Add(New SqlParameter("@RT", SqlDbType.Char, 3) With {.Value = oPCSIAddress.RT})
        paramsPCSI.Add(New SqlParameter("@RW", SqlDbType.Char, 3) With {.Value = oPCSIAddress.RW})
        paramsPCSI.Add(New SqlParameter("@Kelurahan", SqlDbType.VarChar, 50) With {.Value = oPCSIAddress.Kelurahan})
        paramsPCSI.Add(New SqlParameter("@Kecamatan", SqlDbType.VarChar, 50) With {.Value = oPCSIAddress.Kecamatan})
        paramsPCSI.Add(New SqlParameter("@Kota", SqlDbType.VarChar, 50) With {.Value = oPCSIAddress.City})
        paramsPCSI.Add(New SqlParameter("@KodePos", SqlDbType.Char, 5) With {.Value = oPCSIAddress.ZipCode})
        paramsPCSI.Add(New SqlParameter("@KaryawanSejak", SqlDbType.TinyInt) With {.Value = oPCSI.KaryawanSejak})
        paramsPCSI.Add(New SqlParameter("@KaryawanSejakTahun", SqlDbType.Char, 4) With {.Value = oPCSI.KaryawanSejakTahun})
        paramsPCSI.Add(New SqlParameter("@PenghasilanTetap", SqlDbType.Decimal) With {.Value = oPCSI.PenghasilanTetap})
        paramsPCSI.Add(New SqlParameter("@PenghasilanTambahan", SqlDbType.Decimal) With {.Value = oPCSI.PenghasilanTambahan})
        paramsPCSI.Add(New SqlParameter("@AreaPhone1", SqlDbType.Char, 4) With {.Value = oPCSIAddress.AreaPhone1})
        paramsPCSI.Add(New SqlParameter("@Phone1", SqlDbType.Char, 10) With {.Value = oPCSIAddress.Phone1})
        paramsPCSI.Add(New SqlParameter("@AreaPhone2", SqlDbType.Char, 4) With {.Value = oPCSIAddress.AreaPhone2})
        paramsPCSI.Add(New SqlParameter("@Phone2", SqlDbType.Char, 10) With {.Value = oPCSIAddress.Phone2})
        paramsPCSI.Add(New SqlParameter("@AreaFax", SqlDbType.Char, 4) With {.Value = oPCSIAddress.AreaFax})
        paramsPCSI.Add(New SqlParameter("@Fax", SqlDbType.Char, 10) With {.Value = oPCSIAddress.Fax})


        paramsPCSI.Add(New SqlParameter("@TempatLahir", SqlDbType.Char, 20) With {.Value = oPCSI.TempatLahirSI})
        paramsPCSI.Add(New SqlParameter("@TglLahir", SqlDbType.DateTime) With {.Value = oPCSI.TglLahirSI})
        paramsPCSI.Add(New SqlParameter("@NoDokument", SqlDbType.Char, 50) With {.Value = oPCSI.NoDokumentSI})
        paramsPCSI.Add(New SqlParameter("@MasaBerlakuKTP", SqlDbType.DateTime) With {.Value = oPCSI.MasaBerlakuKTPSI})
        paramsPCSI.Add(New SqlParameter("@BeroperasiSejak", SqlDbType.Int) With {.Value = oPCSI.BeroperasiSejakSI})
        paramsPCSI.Add(New SqlParameter("@MasaKerja", SqlDbType.Int) With {.Value = oPCSI.MasaKerjaSI})
        paramsPCSI.Add(New SqlParameter("@UsahaLainnya", SqlDbType.Char, 50) With {.Value = oPCSI.UsahaLainnyaSI})
        paramsPCSI.Add(New SqlParameter("@JenisDokumen", SqlDbType.Char, 3) With {.Value = oPCSI.JenisDokumenSI})

        paramsPCSI.Add(New SqlParameter("@Seq", SqlDbType.Int) With {.Value = oPCSI.Seq})

        'alamat KTasangan
        paramsPCSI.Add(New SqlParameter("@KtpAlamat", SqlDbType.VarChar, 50) With {.Value = oPCSI.AlamatKTP.Address})
        paramsPCSI.Add(New SqlParameter("@KtpRT", SqlDbType.Char, 3) With {.Value = oPCSI.AlamatKTP.RT})
        paramsPCSI.Add(New SqlParameter("@KtpRW", SqlDbType.Char, 3) With {.Value = oPCSI.AlamatKTP.RW})
        paramsPCSI.Add(New SqlParameter("@KtpKelurahan", SqlDbType.VarChar, 50) With {.Value = oPCSI.AlamatKTP.Kelurahan})
        paramsPCSI.Add(New SqlParameter("@KtpKecamatan", SqlDbType.VarChar, 50) With {.Value = oPCSI.AlamatKTP.Kecamatan})
        paramsPCSI.Add(New SqlParameter("@KtpKota", SqlDbType.VarChar, 50) With {.Value = oPCSI.AlamatKTP.City})
        paramsPCSI.Add(New SqlParameter("@KtpKodePos", SqlDbType.Char, 5) With {.Value = oPCSI.AlamatKTP.ZipCode})
        paramsPCSI.Add(New SqlParameter("@KtpAreaPhone1", SqlDbType.Char, 4) With {.Value = oPCSI.AlamatKTP.AreaPhone1})
        paramsPCSI.Add(New SqlParameter("@KtpPhone1", SqlDbType.Char, 10) With {.Value = oPCSI.AlamatKTP.Phone1})
        paramsPCSI.Add(New SqlParameter("@KtpAreaPhone2", SqlDbType.Char, 4) With {.Value = oPCSI.AlamatKTP.AreaPhone2})
        paramsPCSI.Add(New SqlParameter("@KtpPhone2", SqlDbType.Char, 10) With {.Value = oPCSI.AlamatKTP.Phone2})
        paramsPCSI.Add(New SqlParameter("@KtpAreaFax", SqlDbType.Char, 4) With {.Value = oPCSI.AlamatKTP.AreaFax})
        paramsPCSI.Add(New SqlParameter("@KtpFax", SqlDbType.Char, 10) With {.Value = oPCSI.AlamatKTP.Fax})

        paramsPCSI.Add(New SqlParameter("@KtpHP", SqlDbType.Char, 15) With {.Value = oPCSI.AlamatKTP.MobilePhone})

        paramsPCSI.Add(New SqlParameter("@OccupationId", SqlDbType.Char, 20) With {.Value = oPCSI.OccupationID})
    End Sub

    Public Function PersonalCustomerGuarantorSaveEdit(ByVal oCustomer As Parameter.Customer,
                                   ByVal oPCSI As Parameter.CustomerSI,
                                   ByVal oPCSIAddress As Parameter.Address) As String
        Dim conn As New SqlConnection(oCustomer.strConnection)
        Dim transaction As SqlTransaction = Nothing



        If (oCustomer.OPCSI Is Nothing) Then Throw New Exception("Data Penjamin tidak ditemukan")
        If (oCustomer.OPCSI.Count <= 0) Then Throw New Exception("Data Penjamin tidak ditemukan")

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            For Each item In oCustomer.OPCSI
                Dim paramsPCSI As IList(Of SqlParameter) = New List(Of SqlParameter)
                PenjaminParams(paramsPCSI, oCustomer, item, item.PCSIAddress)
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPersonalCustomerGuarantorAddEdit", paramsPCSI.ToArray())
            Next

            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Customer", "PersonalSaveEdit", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function


    Public Function PersonalCustomerEmergencySaveEdit(ByVal oCustomer As Parameter.Customer,
                                     ByVal oPCER As Parameter.CustomerER,
                                     ByVal oPCERAddress As Parameter.Address) As String
        Dim conn As New SqlConnection(oCustomer.strConnection)
        Dim transaction As SqlTransaction = Nothing

        Dim paramsPCER(18) As SqlParameter

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            paramsPCER(0) = New SqlParameter("@Mode", SqlDbType.Bit)
            paramsPCER(0).Value = 0
            paramsPCER(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            paramsPCER(1).Value = oCustomer.CustomerID
            paramsPCER(2) = New SqlParameter("@Nama", SqlDbType.VarChar, 50)
            paramsPCER(2).Value = oPCER.Nama
            paramsPCER(3) = New SqlParameter("@Hubungan", SqlDbType.VarChar, 50)
            paramsPCER(3).Value = oPCER.Hubungan
            paramsPCER(4) = New SqlParameter("@Alamat", SqlDbType.VarChar, 50)
            paramsPCER(4).Value = oPCERAddress.Address
            paramsPCER(5) = New SqlParameter("@RT", SqlDbType.Char, 3)
            paramsPCER(5).Value = oPCERAddress.RT
            paramsPCER(6) = New SqlParameter("@RW", SqlDbType.Char, 3)
            paramsPCER(6).Value = oPCERAddress.RW
            paramsPCER(7) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 50)
            paramsPCER(7).Value = oPCERAddress.Kelurahan
            paramsPCER(8) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 50)
            paramsPCER(8).Value = oPCERAddress.Kecamatan
            paramsPCER(9) = New SqlParameter("@Kota", SqlDbType.VarChar, 50)
            paramsPCER(9).Value = oPCERAddress.City
            paramsPCER(10) = New SqlParameter("@KodePos", SqlDbType.Char, 5)
            paramsPCER(10).Value = oPCERAddress.ZipCode
            paramsPCER(11) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
            paramsPCER(11).Value = oPCERAddress.AreaPhone1
            paramsPCER(12) = New SqlParameter("@Phone1", SqlDbType.VarChar, 15)
            paramsPCER(12).Value = oPCERAddress.Phone1
            paramsPCER(13) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 15)
            paramsPCER(13).Value = oPCER.MobilePhone
            paramsPCER(14) = New SqlParameter("@Email", SqlDbType.VarChar, 50)
            paramsPCER(14).Value = oPCER.Email
            paramsPCER(15) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
            paramsPCER(15).Value = oPCERAddress.AreaPhone2
            paramsPCER(16) = New SqlParameter("@Phone2", SqlDbType.VarChar, 15)
            paramsPCER(16).Value = oPCERAddress.Phone2

            paramsPCER(17) = New SqlParameter("@AreaFax", SqlDbType.Char, 4)
            paramsPCER(17).Value = oPCERAddress.AreaFax
            paramsPCER(18) = New SqlParameter("@Fax", SqlDbType.VarChar, 15)
            paramsPCER(18).Value = oPCERAddress.Fax
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spPersonalCustomerERAddEdit", paramsPCER)
            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Customer", "PersonalSaveEdit", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
    Public Function PersonalCustomerKeluargaSaveEdit(ByVal oCustomer As Parameter.Customer,
                                     ByVal oDataFamily As DataTable) As String
        Dim conn As New SqlConnection(oCustomer.strConnection)
        Dim transaction As SqlTransaction = Nothing

        Dim paramsFamily(6) As SqlParameter
        Dim paramsFamilyD(0) As SqlParameter

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            Dim Delete As String = "1"

            If oDataFamily.Rows.Count > 0 Then
                Delete = "1"
                paramsFamily(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                paramsFamily(1) = New SqlParameter("@FamilySeqNo", SqlDbType.SmallInt)
                paramsFamily(2) = New SqlParameter("@FamilyName", SqlDbType.VarChar, 50)
                paramsFamily(3) = New SqlParameter("@FamilyIDNumber", SqlDbType.VarChar, 25)
                paramsFamily(4) = New SqlParameter("@FamilyBirthDate", SqlDbType.VarChar, 8)
                paramsFamily(5) = New SqlParameter("@FamilyRelation", SqlDbType.VarChar, 10)
                paramsFamily(6) = New SqlParameter("@Delete", SqlDbType.Char, 1)

                paramsFamilyD(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                paramsFamilyD(0).Value = oCustomer.CustomerID
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCustomerPersonalSaveEdit4", paramsFamilyD)

                For intLoopOmset = 0 To oDataFamily.Rows.Count - 1
                    paramsFamily(0).Value = oCustomer.CustomerID
                    paramsFamily(1).Value = CInt(oDataFamily.Rows(intLoopOmset).Item("FamilySeqNo"))
                    paramsFamily(2).Value = IIf(oDataFamily.Rows(intLoopOmset).Item("FamilyName").ToString.Trim <> "", Replace(oDataFamily.Rows(intLoopOmset).Item("FamilyName").ToString.Trim, "'", "''"), oDataFamily.Rows(intLoopOmset).Item("FamilyName").ToString.Trim)
                    paramsFamily(3).Value = oDataFamily.Rows(intLoopOmset).Item("FamilyIDNumber")
                    paramsFamily(4).Value = oDataFamily.Rows(intLoopOmset).Item("FamilyBirthDate")
                    paramsFamily(5).Value = oDataFamily.Rows(intLoopOmset).Item("FamilyRelation")
                    paramsFamily(6).Value = Delete
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spPersonalSaveEdit3, paramsFamily)
                    'Delete = "0"
                Next

            Else
                paramsFamily(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                paramsFamily(0).Value = oCustomer.CustomerID
                paramsFamily(1) = New SqlParameter("@FamilySeqNo", SqlDbType.SmallInt)
                paramsFamily(1).Value = 0
                paramsFamily(2) = New SqlParameter("@FamilyName", SqlDbType.VarChar, 50)
                paramsFamily(2).Value = ""
                paramsFamily(3) = New SqlParameter("@FamilyIDNumber", SqlDbType.VarChar, 25)
                paramsFamily(3).Value = ""
                paramsFamily(4) = New SqlParameter("@FamilyBirthDate", SqlDbType.VarChar, 8)
                paramsFamily(4).Value = ""
                paramsFamily(5) = New SqlParameter("@FamilyRelation", SqlDbType.VarChar, 10)
                paramsFamily(5).Value = ""
                paramsFamily(6) = New SqlParameter("@Delete", SqlDbType.Char, 1)
                paramsFamily(6).Value = "3"
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spPersonalSaveEdit3, paramsFamily)
            End If

            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Customer", "PersonalSaveEdit", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    'PersonalCustomerOL''
    'Public Function PersonalCustomerOLSaveEdit(ByVal oCustomer As Parameter.Customer, _
    '                               ByVal oDataPersonalCustomerOL As DataTable) As String
    '    Dim conn As New SqlConnection(oCustomer.strConnection)
    '    Dim transaction As SqlTransaction = Nothing

    '    Dim paramsPersonalCustomerOL(8) As SqlParameter

    '    Try
    '        If conn.State = ConnectionState.Closed Then conn.Open()
    '        transaction = conn.BeginTransaction

    '        Dim Delete As String = "1"

    '        If oDataPersonalCustomerOL.Rows.Count > 0 Then
    '            Delete = "1"
    '            paramsPersonalCustomerOL(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
    '            paramsPersonalCustomerOL(1) = New SqlParameter("@PersonalCustomerOLSeqNo", SqlDbType.SmallInt)
    '            paramsPersonalCustomerOL(2) = New SqlParameter("@NamaLembagaKeuangan", SqlDbType.VarChar, 50)
    '            paramsPersonalCustomerOL(3) = New SqlParameter("@JenisPinjaman", SqlDbType.VarChar, 50)
    '            paramsPersonalCustomerOL(4) = New SqlParameter("@JumlahPinjaman", SqlDbType.SmallInt)
    '            paramsPersonalCustomerOL(5) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
    '            paramsPersonalCustomerOL(6) = New SqlParameter("@SisaPokok", SqlDbType.SmallInt)
    '            paramsPersonalCustomerOL(7) = New SqlParameter("@Angsuran", SqlDbType.SmallInt)
    '            paramsPersonalCustomerOL(8) = New SqlParameter("@Delete", SqlDbType.Char, 1)
    '            For intLoopOmset = 0 To oDataPersonalCustomerOL.Rows.Count - 1
    '                paramsPersonalCustomerOL(0).Value = oCustomer.CustomerID
    '                paramsPersonalCustomerOL(1).Value = CInt(oDataPersonalCustomerOL.Rows(intLoopOmset).Item("PersonalCustomerOLSeqNo"))
    '                paramsPersonalCustomerOL(2).Value = IIf(oDataPersonalCustomerOL.Rows(intLoopOmset).Item("NamaLembagaKeuangan").ToString.Trim <> "", Replace(oDataPersonalCustomerOL.Rows(intLoopOmset).Item("NamaLembagaKeuangan").ToString.Trim, "'", "''"), oDataPersonalCustomerOL.Rows(intLoopOmset).Item("NamaLembagaKeuangan").ToString.Trim)
    '                paramsPersonalCustomerOL(3).Value = oDataPersonalCustomerOL.Rows(intLoopOmset).Item("JenisPinjaman")
    '                paramsPersonalCustomerOL(4).Value = oDataPersonalCustomerOL.Rows(intLoopOmset).Item("JumlahPinjaman")
    '                paramsPersonalCustomerOL(5).Value = oDataPersonalCustomerOL.Rows(intLoopOmset).Item("Tenor")
    '                paramsPersonalCustomerOL(6).Value = oDataPersonalCustomerOL.Rows(intLoopOmset).Item("SisaPokok")
    '                paramsPersonalCustomerOL(7).Value = oDataPersonalCustomerOL.Rows(intLoopOmset).Item("Angsuran")
    '                paramsPersonalCustomerOL(8).Value = Delete
    '                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spPersonalSaveEdit4, paramsPersonalCustomerOL)
    '                Delete = "0"
    '            Next
    '        Else
    '            paramsPersonalCustomerOL(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
    '            paramsPersonalCustomerOL(0).Value = oCustomer.CustomerID
    '            paramsPersonalCustomerOL(1) = New SqlParameter("@PersonalCustomerOLSeqNo", SqlDbType.SmallInt)
    '            paramsPersonalCustomerOL(1).Value = 0
    '            paramsPersonalCustomerOL(2) = New SqlParameter("@NamaLembagaKeuangan", SqlDbType.VarChar, 50)
    '            paramsPersonalCustomerOL(2).Value = ""
    '            paramsPersonalCustomerOL(3) = New SqlParameter("@JenisPinjaman", SqlDbType.VarChar, 50)
    '            paramsPersonalCustomerOL(3).Value = ""
    '            paramsPersonalCustomerOL(4) = New SqlParameter("@JumlahPinjaman", SqlDbType.SmallInt)
    '            paramsPersonalCustomerOL(4).Value = 0
    '            paramsPersonalCustomerOL(5) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
    '            paramsPersonalCustomerOL(5).Value = 0
    '            paramsPersonalCustomerOL(6) = New SqlParameter("@SisaPokok", SqlDbType.SmallInt)
    '            paramsPersonalCustomerOL(6).Value = 0
    '            paramsPersonalCustomerOL(7) = New SqlParameter("@Angsuran", SqlDbType.SmallInt)
    '            paramsPersonalCustomerOL(7).Value = 0
    '            paramsPersonalCustomerOL(8) = New SqlParameter("@Delete", SqlDbType.Char, 1)
    '            paramsPersonalCustomerOL(8).Value = "3"
    '            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spPersonalSaveEdit4, paramsPersonalCustomerOL)
    '        End If

    '        transaction.Commit()
    '        Return ""
    '    Catch exp As Exception
    '        transaction.Rollback()
    '        WriteException("Customer", "PersonalCustomerOL", exp.Message + exp.StackTrace)
    '        Throw New Exception(exp.Message)
    '    Finally
    '        If conn.State = ConnectionState.Open Then conn.Close()
    '        conn.Dispose()
    '    End Try
    'End Function
#End Region

#Region "Company"
    Public Function BindCustomerC1_002(ByVal oCustomer As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@NPWP", SqlDbType.VarChar, 25)
            params(0).Value = oCustomer.PersonalNPWP
            params(1) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(1).Value = Replace(oCustomer.Name, "'", "''")
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomer.strConnection, CommandType.StoredProcedure, spBindCustomerC1_002, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "BindCustomerC1_002", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.BindCustomerC1_002")
        End Try
    End Function

    Public Function BindCustomerC2_002(ByVal oCustomer As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@NPWP", SqlDbType.VarChar, 25)
            params(0).Value = oCustomer.PersonalNPWP
            params(1) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(1).Value = Replace(oCustomer.Name, "'", "''")
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomer.strConnection, CommandType.StoredProcedure, spBindCustomerC2_002, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "BindCustomerC2_002", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.BindCustomerC2_002")
        End Try
    End Function

    Public Function CompanySaveAdd(ByVal oCustomer As Parameter.Customer,
                                   ByVal oAddress As Parameter.Address,
                                   ByVal oWarehouseAddress As Parameter.Address,
                                   ByVal oData1 As DataTable,
                                   ByVal oData2 As DataTable,
                                   ByVal CompanyCustomerDPEx As Parameter.CompanyCustomerDPEx,
                                   ByVal CompanyCustomerPJ As Parameter.CompanyCustomerPJ,
                                   ByVal CompanyCustomerPJAddress As Parameter.Address,
                                   ByVal companySIUPAddress As Parameter.Address
                                   ) As Parameter.Customer
        Dim oReturn As New Parameter.Customer
        Dim conn As New SqlConnection(oCustomer.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim CustomerID As String = ""
        Dim intLoopOmset As Integer
        Dim params1(8) As SqlParameter
        Dim params2(5) As SqlParameter
        Dim params(62) As SqlParameter
        Dim params3(7) As SqlParameter
        Dim params4(15) As SqlParameter

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 5)
            params(0).Value = oCustomer.BranchId
            params(1) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(1).Value = Replace(oCustomer.Name, "'", "''")
            params(2) = New SqlParameter("@CustomerType", SqlDbType.Char, 1)
            params(2).Value = oCustomer.CustomerType
            params(3) = New SqlParameter("@CompanyType", SqlDbType.VarChar, 10)
            params(3).Value = oCustomer.CompanyType
            params(4) = New SqlParameter("@NPWP", SqlDbType.VarChar, 25)
            params(4).Value = oCustomer.PersonalNPWP
            params(5) = New SqlParameter("@NumberOfEmployees", SqlDbType.SmallInt)
            params(5).Value = CInt(IIf(oCustomer.NumEmployees = "", "0", oCustomer.NumEmployees))
            params(6) = New SqlParameter("@YearOfEstablished", SqlDbType.SmallInt)
            params(6).Value = CInt(IIf(oCustomer.YearEstablished = "", "0", oCustomer.YearEstablished))
            params(7) = New SqlParameter("@IndustryTypeID", SqlDbType.Char, 10)
            params(7).Value = oCustomer.IndustryTypeID
            params(8) = New SqlParameter("@CustomerGroupID", SqlDbType.Char, 20)
            params(8).Value = oCustomer.CustomerGroupID
            params(9) = New SqlParameter("@CompanyAddress", SqlDbType.VarChar, 100)
            params(9).Value = oAddress.Address
            params(10) = New SqlParameter("@CompanyRT", SqlDbType.Char, 3)
            params(10).Value = oAddress.RT
            params(11) = New SqlParameter("@CompanyRW", SqlDbType.Char, 3)
            params(11).Value = oAddress.RW
            params(12) = New SqlParameter("@CompanyKelurahan", SqlDbType.VarChar, 30)
            params(12).Value = oAddress.Kelurahan
            params(13) = New SqlParameter("@CompanyKecamatan", SqlDbType.VarChar, 30)
            params(13).Value = oAddress.Kecamatan
            params(14) = New SqlParameter("@CompanyCity", SqlDbType.VarChar, 30)
            params(14).Value = oAddress.City
            params(15) = New SqlParameter("@CompanyZipCode", SqlDbType.Char, 5)
            params(15).Value = oAddress.ZipCode
            params(16) = New SqlParameter("@CompanyAreaPhone1", SqlDbType.Char, 4)
            params(16).Value = oAddress.AreaPhone1
            params(17) = New SqlParameter("@CompanyPhone1", SqlDbType.Char, 10)
            params(17).Value = oAddress.Phone1
            params(18) = New SqlParameter("@CompanyAreaPhone2", SqlDbType.Char, 4)
            params(18).Value = oAddress.AreaPhone2
            params(19) = New SqlParameter("@CompanyPhone2", SqlDbType.Char, 10)
            params(19).Value = oAddress.Phone2
            params(20) = New SqlParameter("@CompanyAreaFax", SqlDbType.Char, 4)
            params(20).Value = oAddress.AreaFax
            params(21) = New SqlParameter("@CompanyFax", SqlDbType.Char, 10)
            params(21).Value = oAddress.Fax
            params(22) = New SqlParameter("@CurrentRatio", SqlDbType.Decimal)
            params(22).Value = CDec(IIf(oCustomer.Ratio = "", "0", oCustomer.Ratio))
            params(23) = New SqlParameter("@ROI", SqlDbType.Decimal)
            params(23).Value = CDec(IIf(oCustomer.ROI = "", "0", oCustomer.ROI))
            params(24) = New SqlParameter("@DER", SqlDbType.Decimal)
            params(24).Value = CDec(IIf(oCustomer.DER = "", "0", oCustomer.DER))

            params(25) = New SqlParameter("@OrderKe", SqlDbType.SmallInt)
            params(25).Value = oCustomer.OrderKe
            params(26) = New SqlParameter("@OmsetBulanan", SqlDbType.Money)
            params(26).Value = oCustomer.OmsetBulanan
            params(27) = New SqlParameter("@BiayaBulanan", SqlDbType.Money)
            params(27).Value = oCustomer.BiayaBulanan

            params(28) = New SqlParameter("@AverageBalance", SqlDbType.Decimal)
            params(28).Value = CDec(IIf(oCustomer.AverageBalance = "", "0", oCustomer.AverageBalance))
            params(29) = New SqlParameter("@Deposito", SqlDbType.Decimal)
            params(29).Value = CDec(IIf(oCustomer.Deposito = "", "0", oCustomer.Deposito))
            params(30) = New SqlParameter("@AdditionalCollateralType", SqlDbType.VarChar, 50)
            params(30).Value = oCustomer.AdditionalCollateralType
            params(31) = New SqlParameter("@AdditionalCollateralAmount", SqlDbType.Decimal)
            params(31).Value = CDec(IIf(oCustomer.AdditionalCollateralAmount = "", "0", oCustomer.AdditionalCollateralAmount))
            params(32) = New SqlParameter("@CompanyStatus", SqlDbType.VarChar, 10)
            params(32).Value = IIf(oCustomer.BusPlaceStatus = "Select One", "", oCustomer.BusPlaceStatus)
            params(33) = New SqlParameter("@CompanyStatusSinceYear ", SqlDbType.SmallInt)
            params(33).Value = CInt(IIf(oCustomer.BusPlaceSinceYear = "", "0", oCustomer.BusPlaceSinceYear))
            params(34) = New SqlParameter("@RentFinishDate ", SqlDbType.VarChar, 8)
            params(34).Value = oCustomer.RentFinishDate

            params(35) = New SqlParameter("@BankID", SqlDbType.Char, 5)
            params(35).Value = IIf(oCustomer.BankID = "0", "", oCustomer.BankID)
            params(36) = New SqlParameter("@BankBranch", SqlDbType.VarChar, 50)
            params(36).Value = oCustomer.BankBranch
            params(37) = New SqlParameter("@AccountNo", SqlDbType.Char, 20)
            params(37).Value = oCustomer.AccountNo
            params(38) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
            params(38).Value = IIf(oCustomer.AccountName <> "", Replace(oCustomer.AccountName, "'", "''"), oCustomer.AccountName)
            params(39) = New SqlParameter("@Reference", SqlDbType.VarChar, 10)
            params(39).Value = IIf(oCustomer.Reference = "Select One", "", oCustomer.Reference)
            params(40) = New SqlParameter("@PertamaKredit", SqlDbType.Char, 2)
            params(40).Value = oCustomer.PertamaKredit
            params(41) = New SqlParameter("@Notes", SqlDbType.Text)
            params(41).Value = oCustomer.Notes
            params(42) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(42).Direction = ParameterDirection.Output
            params(43) = New SqlParameter("@CustID", SqlDbType.Char, 20)
            params(43).Direction = ParameterDirection.Output
            params(44) = New SqlParameter("@businessdate", SqlDbType.DateTime)
            params(44).Value = oCustomer.BusinessDate
            params(45) = New SqlParameter("@ProspectAppID", SqlDbType.Char, 20)
            params(45).Value = oCustomer.ProspectAppID
            params(46) = New SqlParameter("@JumlahKendaraan", SqlDbType.SmallInt)
            params(46).Value = oCustomer.JumlahKendaraan
            params(47) = New SqlParameter("@Garasi", SqlDbType.Bit)
            params(47).Value = oCustomer.Garasi

            With companySIUPAddress
                params(48) = New SqlParameter("@CompanyAddress_SIUP", SqlDbType.VarChar, 100)
                params(48).Value = .Address
                params(49) = New SqlParameter("@CompanyRT_SIUP", SqlDbType.Char, 3)
                params(49).Value = .RT
                params(50) = New SqlParameter("@CompanyRW_SIUP", SqlDbType.Char, 3)
                params(50).Value = .RW
                params(51) = New SqlParameter("@CompanyKelurahan_SIUP", SqlDbType.VarChar, 30)
                params(51).Value = .Kelurahan
                params(52) = New SqlParameter("@CompanyKecamatan_SIUP", SqlDbType.VarChar, 30)
                params(52).Value = .Kecamatan
                params(53) = New SqlParameter("@CompanyCity_SIUP", SqlDbType.VarChar, 30)
                params(53).Value = .City
                params(54) = New SqlParameter("@CompanyZipCode_SIUP", SqlDbType.Char, 5)
                params(54).Value = .ZipCode
                params(55) = New SqlParameter("@CompanyAreaPhone1_SIUP", SqlDbType.Char, 4)
                params(55).Value = .AreaPhone1
                params(56) = New SqlParameter("@CompanyPhone1_SIUP", SqlDbType.Char, 10)
                params(56).Value = .Phone1
                params(57) = New SqlParameter("@CompanyAreaPhone2_SIUP", SqlDbType.Char, 4)
                params(57).Value = .AreaPhone2
                params(58) = New SqlParameter("@CompanyPhone2_SIUP", SqlDbType.Char, 10)
                params(58).Value = .Phone2
                params(59) = New SqlParameter("@CompanyAreaFax_SIUP", SqlDbType.Char, 4)
                params(59).Value = .AreaFax
                params(60) = New SqlParameter("@CompanyFax_SIUP", SqlDbType.Char, 10)
                params(60).Value = .Fax
            End With
            params(61) = New SqlParameter("@ResikoUsaha", SqlDbType.SmallInt)
            params(61).Value = oCustomer.ResikoUsaha
            params(62) = New SqlParameter("@BankBranchId", SqlDbType.Int)
            params(62).Value = oCustomer.BankBranchId
            'params(63) = New SqlParameter("@IsOLS", SqlDbType.Bit)
            'params(63).Value = oCustomer.IsOLS

            Dim oReturnValue As New Parameter.Customer

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spCompanySaveAdd, params)
            ErrMessage = CType(params(42).Value, String)
            CustomerID = CType(params(43).Value, String)

            If ErrMessage <> "" Then
                oReturn.Err = ErrMessage
                oReturn.CustomerID = CustomerID
                Throw New Exception(ErrMessage)
            End If

            If oData2.Rows.Count > 0 Then
                params2(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                params2(1) = New SqlParameter("@CLDSeqNo", SqlDbType.SmallInt)
                params2(2) = New SqlParameter("@DocumentType", SqlDbType.VarChar, 10)
                params2(3) = New SqlParameter("@DocumentNo", SqlDbType.VarChar, 50)
                params2(4) = New SqlParameter("@DocumentDate", SqlDbType.VarChar, 8)
                params2(5) = New SqlParameter("@DocumentNotes", SqlDbType.Text)
                For intLoopOmset = 0 To oData2.Rows.Count - 1
                    params2(0).Value = CustomerID
                    params2(1).Value = oData2.Rows(intLoopOmset).Item("lblNo2")
                    params2(2).Value = oData2.Rows(intLoopOmset).Item("cboType2")
                    params2(3).Value = oData2.Rows(intLoopOmset).Item("txtDocNo2")
                    params2(4).Value = oData2.Rows(intLoopOmset).Item("uscDate2")
                    params2(5).Value = oData2.Rows(intLoopOmset).Item("txtNotes2")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spCompanySaveAdd2, params2)
                Next
            End If
            If oData1.Rows.Count > 0 Then
                params1(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                params1(1) = New SqlParameter("@CSHSeqNo", SqlDbType.SmallInt)
                params1(2) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
                params1(3) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 30)
                params1(4) = New SqlParameter("@JobPositionID", SqlDbType.VarChar, 10)
                params1(5) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
                params1(6) = New SqlParameter("@Phone", SqlDbType.Char, 10)
                params1(7) = New SqlParameter("@SharePercentage", SqlDbType.Decimal)
                params1(8) = New SqlParameter("@Gender", SqlDbType.Char, 1)
                For intLoopOmset = 0 To oData1.Rows.Count - 1
                    params1(0).Value = CustomerID
                    params1(1).Value = oData1.Rows(intLoopOmset).Item("No")
                    params1(2).Value = IIf(oData1.Rows(intLoopOmset).Item("Name").ToString <> "", Replace(oData1.Rows(intLoopOmset).Item("Name").ToString, "'", "''"), oData1.Rows(intLoopOmset).Item("Name").ToString)
                    params1(3).Value = oData1.Rows(intLoopOmset).Item("IDNumber")
                    params1(4).Value = oData1.Rows(intLoopOmset).Item("Position")
                    params1(5).Value = oData1.Rows(intLoopOmset).Item("Address")
                    params1(6).Value = oData1.Rows(intLoopOmset).Item("Phone")
                    params1(7).Value = oData1.Rows(intLoopOmset).Item("Share")
                    params1(8).Value = oData1.Rows(intLoopOmset).Item("Gender")
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spCompanySaveAdd3, params1)
                Next
            End If
            params3(0) = New SqlParameter("@Mode", SqlDbType.Bit)
            params3(0).Value = 1
            params3(1) = New SqlParameter("@KeteranganUsaha", SqlDbType.VarChar, 50)
            params3(1).Value = CompanyCustomerDPEx.KeteranganUsaha
            params3(2) = New SqlParameter("@StatusUsaha", SqlDbType.Char, 2)
            params3(2).Value = CompanyCustomerDPEx.StatusUsaha
            params3(3) = New SqlParameter("@KategoriPerusahaan", SqlDbType.Char, 1)
            params3(3).Value = CompanyCustomerDPEx.KategoriPerusahaan
            params3(4) = New SqlParameter("@KondisiKantor", SqlDbType.Char, 1)
            params3(4).Value = CompanyCustomerDPEx.KondisiKantor
            params3(5) = New SqlParameter("@KondisiLingkungan", SqlDbType.Char, 1)
            params3(5).Value = CompanyCustomerDPEx.KondisiLingkungan
            params3(6) = New SqlParameter("@NamaWebsite", SqlDbType.VarChar, 50)
            params3(6).Value = CompanyCustomerDPEx.NamaWebsite
            params3(7) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params3(7).Value = CustomerID
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCompanyCustomerDPExtendedAddEdit", params3)
            params4(0) = New SqlParameter("@Mode", SqlDbType.Char, 1)
            params4(0).Value = 1
            params4(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params4(1).Value = CustomerID
            params4(2) = New SqlParameter("@Nama", SqlDbType.VarChar, 50)
            params4(2).Value = CompanyCustomerPJ.Nama
            params4(3) = New SqlParameter("@Jabatan", SqlDbType.VarChar, 10)
            params4(3).Value = CompanyCustomerPJ.Jabatan
            params4(4) = New SqlParameter("@NPWP", SqlDbType.VarChar, 50)
            params4(4).Value = CompanyCustomerPJ.NPWP
            params4(5) = New SqlParameter("@Alamat", SqlDbType.VarChar, 50)
            params4(5).Value = CompanyCustomerPJAddress.Address
            params4(6) = New SqlParameter("@RT", SqlDbType.Char, 3)
            params4(6).Value = CompanyCustomerPJAddress.RT
            params4(7) = New SqlParameter("@RW", SqlDbType.Char, 3)
            params4(7).Value = oAddress.RW
            params4(8) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 50)
            params4(8).Value = oAddress.Kelurahan
            params4(9) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 50)
            params4(9).Value = oAddress.Kecamatan
            params4(10) = New SqlParameter("@Kota", SqlDbType.VarChar, 50)
            params4(10).Value = oAddress.City
            params4(11) = New SqlParameter("@KodePos", SqlDbType.Char, 5)
            params4(11).Value = oAddress.ZipCode
            params4(12) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
            params4(12).Value = oAddress.AreaPhone1
            params4(13) = New SqlParameter("@Phone1", SqlDbType.VarChar, 15)
            params4(13).Value = oAddress.Phone1
            params4(14) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 15)
            params4(14).Value = CompanyCustomerPJ.NoHP
            params4(15) = New SqlParameter("@Email", SqlDbType.VarChar, 15)
            params4(15).Value = CompanyCustomerPJ.Email
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCompanyCustomerPJAddEdit", params4)
            transaction.Commit()
            oReturn.Err = ""
            oReturn.CustomerID = CustomerID
            Return oReturn
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Customer", "CompanySaveAdd", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.CompanySaveAdd")
        Finally
            If conn.State = ConnectionState.Closed Then conn.Open()
            conn.Dispose()
        End Try
    End Function

    Public Function CompanyDataPerusahaanSaveAdd(ByVal oCustomer As Parameter.Customer,
                                   ByVal oAddress As Parameter.Address,
                                   ByVal oWarehouseAddress As Parameter.Address,
                                   ByVal CompanyCustomerDPEx As Parameter.CompanyCustomerDPEx,
                                   ByVal companySIUPAddress As Parameter.Address
                                   ) As Parameter.Customer
        Dim oReturn As New Parameter.Customer
        Dim conn As New SqlConnection(oCustomer.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim CustomerID As String = ""
        'Dim params1(8) As SqlParameter
        'Dim params2(5) As SqlParameter
        Dim params(65) As SqlParameter
        'Dim params3(7) As SqlParameter
        'Dim params4(15) As SqlParameter

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 5)
            params(0).Value = oCustomer.BranchId
            params(1) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(1).Value = Replace(oCustomer.Name, "'", "''")
            params(2) = New SqlParameter("@CustomerType", SqlDbType.Char, 1)
            params(2).Value = oCustomer.CustomerType
            params(3) = New SqlParameter("@CompanyType", SqlDbType.VarChar, 10)
            params(3).Value = oCustomer.CompanyType
            params(4) = New SqlParameter("@NPWP", SqlDbType.VarChar, 25)
            params(4).Value = oCustomer.PersonalNPWP
            params(5) = New SqlParameter("@NumberOfEmployees", SqlDbType.SmallInt)
            params(5).Value = CInt(IIf(oCustomer.NumEmployees = "", "0", oCustomer.NumEmployees))
            params(6) = New SqlParameter("@YearOfEstablished", SqlDbType.SmallInt)
            params(6).Value = CInt(IIf(oCustomer.YearEstablished = "", "0", oCustomer.YearEstablished))
            params(7) = New SqlParameter("@IndustryTypeID", SqlDbType.Char, 10)
            params(7).Value = oCustomer.IndustryTypeID
            params(8) = New SqlParameter("@CustomerGroupID", SqlDbType.Char, 20)
            params(8).Value = oCustomer.CustomerGroupID
            params(9) = New SqlParameter("@CompanyAddress", SqlDbType.VarChar, 100)
            params(9).Value = oAddress.Address
            params(10) = New SqlParameter("@CompanyRT", SqlDbType.Char, 3)
            params(10).Value = oAddress.RT
            params(11) = New SqlParameter("@CompanyRW", SqlDbType.Char, 3)
            params(11).Value = oAddress.RW
            params(12) = New SqlParameter("@CompanyKelurahan", SqlDbType.VarChar, 30)
            params(12).Value = oAddress.Kelurahan
            params(13) = New SqlParameter("@CompanyKecamatan", SqlDbType.VarChar, 30)
            params(13).Value = oAddress.Kecamatan
            params(14) = New SqlParameter("@CompanyCity", SqlDbType.VarChar, 30)
            params(14).Value = oAddress.City
            params(15) = New SqlParameter("@CompanyZipCode", SqlDbType.Char, 5)
            params(15).Value = oAddress.ZipCode
            params(16) = New SqlParameter("@CompanyAreaPhone1", SqlDbType.Char, 4)
            params(16).Value = oAddress.AreaPhone1
            params(17) = New SqlParameter("@CompanyPhone1", SqlDbType.Char, 10)
            params(17).Value = oAddress.Phone1
            params(18) = New SqlParameter("@CompanyAreaPhone2", SqlDbType.Char, 4)
            params(18).Value = oAddress.AreaPhone2
            params(19) = New SqlParameter("@CompanyPhone2", SqlDbType.Char, 10)
            params(19).Value = oAddress.Phone2
            params(20) = New SqlParameter("@CompanyAreaFax", SqlDbType.Char, 4)
            params(20).Value = oAddress.AreaFax
            params(21) = New SqlParameter("@CompanyFax", SqlDbType.Char, 10)
            params(21).Value = oAddress.Fax
            params(22) = New SqlParameter("@CurrentRatio", SqlDbType.Decimal)
            params(22).Value = CDec(IIf(oCustomer.Ratio = "", "0", oCustomer.Ratio))
            params(23) = New SqlParameter("@ROI", SqlDbType.Decimal)
            params(23).Value = CDec(IIf(oCustomer.ROI = "", "0", oCustomer.ROI))
            params(24) = New SqlParameter("@DER", SqlDbType.Decimal)
            params(24).Value = CDec(IIf(oCustomer.DER = "", "0", oCustomer.DER))

            params(25) = New SqlParameter("@OrderKe", SqlDbType.SmallInt)
            params(25).Value = oCustomer.OrderKe
            params(26) = New SqlParameter("@OmsetBulanan", SqlDbType.Money)
            params(26).Value = oCustomer.OmsetBulanan
            params(27) = New SqlParameter("@BiayaBulanan", SqlDbType.Money)
            params(27).Value = oCustomer.BiayaBulanan

            params(28) = New SqlParameter("@AverageBalance", SqlDbType.Decimal)
            params(28).Value = CDec(IIf(oCustomer.AverageBalance = "", "0", oCustomer.AverageBalance))
            params(29) = New SqlParameter("@Deposito", SqlDbType.Decimal)
            params(29).Value = CDec(IIf(oCustomer.Deposito = "", "0", oCustomer.Deposito))
            params(30) = New SqlParameter("@AdditionalCollateralType", SqlDbType.VarChar, 50)
            params(30).Value = oCustomer.AdditionalCollateralType
            params(31) = New SqlParameter("@AdditionalCollateralAmount", SqlDbType.Decimal)
            params(31).Value = CDec(IIf(oCustomer.AdditionalCollateralAmount = "", "0", oCustomer.AdditionalCollateralAmount))
            params(32) = New SqlParameter("@CompanyStatus", SqlDbType.VarChar, 10)
            params(32).Value = IIf(oCustomer.BusPlaceStatus = "Select One", "", oCustomer.BusPlaceStatus)
            params(33) = New SqlParameter("@CompanyStatusSinceYear ", SqlDbType.SmallInt)
            params(33).Value = CInt(IIf(oCustomer.BusPlaceSinceYear = "", "0", oCustomer.BusPlaceSinceYear))
            params(34) = New SqlParameter("@RentFinishDate ", SqlDbType.VarChar, 8)
            params(34).Value = oCustomer.RentFinishDate

            params(35) = New SqlParameter("@BankID", SqlDbType.Char, 5)
            params(35).Value = IIf(oCustomer.BankID = "0", "", oCustomer.BankID)
            params(36) = New SqlParameter("@BankBranch", SqlDbType.VarChar, 50)
            params(36).Value = oCustomer.BankBranch
            params(37) = New SqlParameter("@AccountNo", SqlDbType.Char, 20)
            params(37).Value = oCustomer.AccountNo
            params(38) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
            params(38).Value = IIf(oCustomer.AccountName <> "", Replace(oCustomer.AccountName, "'", "''"), oCustomer.AccountName)
            params(39) = New SqlParameter("@Reference", SqlDbType.VarChar, 10)
            params(39).Value = IIf(oCustomer.Reference = "Select One", "", oCustomer.Reference)
            params(40) = New SqlParameter("@PertamaKredit", SqlDbType.Char, 2)
            params(40).Value = oCustomer.PertamaKredit
            params(41) = New SqlParameter("@Notes", SqlDbType.Text)
            params(41).Value = oCustomer.Notes
            params(42) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(42).Direction = ParameterDirection.Output
            params(43) = New SqlParameter("@CustID", SqlDbType.Char, 20)
            params(43).Direction = ParameterDirection.Output
            params(44) = New SqlParameter("@businessdate", SqlDbType.DateTime)
            params(44).Value = oCustomer.BusinessDate
            params(45) = New SqlParameter("@ProspectAppID", SqlDbType.Char, 20)
            params(45).Value = oCustomer.ProspectAppID
            params(46) = New SqlParameter("@JumlahKendaraan", SqlDbType.SmallInt)
            params(46).Value = oCustomer.JumlahKendaraan
            params(47) = New SqlParameter("@Garasi", SqlDbType.Bit)
            params(47).Value = oCustomer.Garasi
            params(48) = New SqlParameter("@IndustryRisk", SqlDbType.VarChar, 15)
            params(48).Value = oCustomer.IndustryRisk

            With companySIUPAddress
                params(49) = New SqlParameter("@CompanyAddress_SIUP", SqlDbType.VarChar, 100)
                params(49).Value = .Address
                params(50) = New SqlParameter("@CompanyRT_SIUP", SqlDbType.Char, 3)
                params(50).Value = .RT
                params(51) = New SqlParameter("@CompanyRW_SIUP", SqlDbType.Char, 3)
                params(51).Value = .RW
                params(52) = New SqlParameter("@CompanyKelurahan_SIUP", SqlDbType.VarChar, 30)
                params(52).Value = .Kelurahan
                params(53) = New SqlParameter("@CompanyKecamatan_SIUP", SqlDbType.VarChar, 30)
                params(53).Value = .Kecamatan
                params(54) = New SqlParameter("@CompanyCity_SIUP", SqlDbType.VarChar, 30)
                params(54).Value = .City
                params(55) = New SqlParameter("@CompanyZipCode_SIUP", SqlDbType.Char, 5)
                params(55).Value = .ZipCode
                params(56) = New SqlParameter("@CompanyAreaPhone1_SIUP", SqlDbType.Char, 4)
                params(56).Value = .AreaPhone1
                params(57) = New SqlParameter("@CompanyPhone1_SIUP", SqlDbType.Char, 10)
                params(57).Value = .Phone1
                params(58) = New SqlParameter("@CompanyAreaPhone2_SIUP", SqlDbType.Char, 4)
                params(58).Value = .AreaPhone2
                params(59) = New SqlParameter("@CompanyPhone2_SIUP", SqlDbType.Char, 10)
                params(59).Value = .Phone2
                params(60) = New SqlParameter("@CompanyAreaFax_SIUP", SqlDbType.Char, 4)
                params(60).Value = .AreaFax
                params(61) = New SqlParameter("@CompanyFax_SIUP", SqlDbType.Char, 10)
                params(61).Value = .Fax
            End With

            params(62) = New SqlParameter("@ResikoUsaha", SqlDbType.SmallInt)
            params(62).Value = oCustomer.ResikoUsaha
            params(63) = New SqlParameter("@BankBranchId", SqlDbType.Int)
            params(63).Value = oCustomer.BankBranchId
            params(64) = New SqlParameter("@IsOLS", SqlDbType.Bit)
            params(64).Value = oCustomer.IsOLS
            params(65) = New SqlParameter("@IsBUMN", SqlDbType.Bit)
            params(65).Value = oCustomer.IsBUMN

            Dim oReturnValue As New Parameter.Customer

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spCompanySaveAdd, params)
            ErrMessage = CType(params(42).Value, String)
            CustomerID = CType(params(43).Value, String)

            If ErrMessage <> "" Then
                oReturn.Err = ErrMessage
                oReturn.CustomerID = CustomerID
                Throw New Exception(ErrMessage)
            End If

            Dim params3 As IList(Of SqlParameter) = New List(Of SqlParameter)

            params3.Add(New SqlParameter("@Mode", SqlDbType.Bit) With {.Value = 0})
            params3.Add(New SqlParameter("@KeteranganUsaha", SqlDbType.VarChar, 50) With {.Value = CompanyCustomerDPEx.KeteranganUsaha})
            params3.Add(New SqlParameter("@StatusUsaha", SqlDbType.Char, 2) With {.Value = CompanyCustomerDPEx.StatusUsaha})
            params3.Add(New SqlParameter("@KategoriPerusahaan", SqlDbType.Char, 1) With {.Value = CompanyCustomerDPEx.KategoriPerusahaan})
            params3.Add(New SqlParameter("@KondisiKantor", SqlDbType.Char, 1) With {.Value = CompanyCustomerDPEx.KondisiKantor})
            params3.Add(New SqlParameter("@KondisiLingkungan", SqlDbType.Char, 1) With {.Value = CompanyCustomerDPEx.KondisiLingkungan})
            params3.Add(New SqlParameter("@NamaWebsite", SqlDbType.VarChar, 50) With {.Value = CompanyCustomerDPEx.NamaWebsite})
            params3.Add(New SqlParameter("@CustomerID", SqlDbType.Char, 20) With {.Value = CustomerID})
            params3.Add(New SqlParameter("@JenisPembiayaan", SqlDbType.Char, 10) With {.Value = CompanyCustomerDPEx.JenisPembiayaan})

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCompanyCustomerDPExtendedAddEdit", params3.ToArray())

            transaction.Commit()
            oReturn.Err = ""
            oReturn.CustomerID = CustomerID
            Return oReturn
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Customer", "CompanySaveDataPerusahaanAdd", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.CompanySaveDataPerusahaanAdd")
        Finally
            If conn.State = ConnectionState.Closed Then conn.Open()
            conn.Dispose()
        End Try
    End Function

    Public Function CompanySaveEdit(ByVal oCustomer As Parameter.Customer,
                                    ByVal oAddress As Parameter.Address,
                                    ByVal oWarehouseAddress As Parameter.Address,
                                    ByVal oData1 As DataTable,
                                    ByVal oData2 As DataTable,
                                    ByVal CompanyCustomerDPEx As Parameter.CompanyCustomerDPEx,
                                    ByVal CompanyCustomerPJ As Parameter.CompanyCustomerPJ,
                                    ByVal CompanyCustomerPJAddress As Parameter.Address,
                                    ByVal companySIUPAddress As Parameter.Address) As String
        Dim conn As New SqlConnection(oCustomer.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim intLoopOmset As Integer
        Dim params1(9) As SqlParameter
        Dim params2(6) As SqlParameter
        Dim params(57) As SqlParameter
        Dim params3(7) As SqlParameter
        Dim params4(15) As SqlParameter
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params(0).Value = oCustomer.CustomerID
            params(1) = New SqlParameter("@PertamaKredit", SqlDbType.Char, 2)
            params(1).Value = oCustomer.PertamaKredit
            params(2) = New SqlParameter("@Notes", SqlDbType.Text)
            params(2).Value = oCustomer.Notes
            params(3) = New SqlParameter("@CompanyType", SqlDbType.VarChar, 10)
            params(3).Value = oCustomer.CompanyType
            params(4) = New SqlParameter("@Reference", SqlDbType.VarChar, 10)
            params(4).Value = IIf(oCustomer.Reference = "Select One", "", oCustomer.Reference)
            params(5) = New SqlParameter("@NumberOfEmployees", SqlDbType.SmallInt)
            params(5).Value = CInt(IIf(oCustomer.NumEmployees = "", "0", oCustomer.NumEmployees))
            params(6) = New SqlParameter("@YearOfEstablished", SqlDbType.SmallInt)
            params(6).Value = CInt(IIf(oCustomer.YearEstablished = "", "0", oCustomer.YearEstablished))
            params(7) = New SqlParameter("@IndustryTypeID", SqlDbType.Char, 10)
            params(7).Value = oCustomer.IndustryTypeID
            params(8) = New SqlParameter("@CustomerGroupID", SqlDbType.Char, 20)
            params(8).Value = oCustomer.CustomerGroupID
            params(9) = New SqlParameter("@CompanyAddress", SqlDbType.VarChar, 100)
            params(9).Value = oAddress.Address
            params(10) = New SqlParameter("@CompanyRT", SqlDbType.Char, 3)
            params(10).Value = oAddress.RT
            params(11) = New SqlParameter("@CompanyRW", SqlDbType.Char, 3)
            params(11).Value = oAddress.RW
            params(12) = New SqlParameter("@CompanyKelurahan", SqlDbType.VarChar, 30)
            params(12).Value = oAddress.Kelurahan
            params(13) = New SqlParameter("@CompanyKecamatan", SqlDbType.VarChar, 30)
            params(13).Value = oAddress.Kecamatan
            params(14) = New SqlParameter("@CompanyCity", SqlDbType.VarChar, 30)
            params(14).Value = oAddress.City
            params(15) = New SqlParameter("@CompanyZipCode", SqlDbType.Char, 5)
            params(15).Value = oAddress.ZipCode
            params(16) = New SqlParameter("@CompanyAreaPhone1", SqlDbType.Char, 4)
            params(16).Value = oAddress.AreaPhone1
            params(17) = New SqlParameter("@CompanyPhone1", SqlDbType.Char, 10)
            params(17).Value = oAddress.Phone1
            params(18) = New SqlParameter("@CompanyAreaPhone2", SqlDbType.Char, 4)
            params(18).Value = oAddress.AreaPhone2
            params(19) = New SqlParameter("@CompanyPhone2", SqlDbType.Char, 10)
            params(19).Value = oAddress.Phone2
            params(20) = New SqlParameter("@CompanyAreaFax", SqlDbType.Char, 4)
            params(20).Value = oAddress.AreaFax
            params(21) = New SqlParameter("@CompanyFax", SqlDbType.Char, 10)
            params(21).Value = oAddress.Fax
            params(22) = New SqlParameter("@CurrentRatio", SqlDbType.Decimal)
            params(22).Value = oCustomer.Ratio
            params(23) = New SqlParameter("@ROI", SqlDbType.Decimal)
            params(23).Value = oCustomer.ROI
            params(24) = New SqlParameter("@DER", SqlDbType.Decimal)
            params(24).Value = oCustomer.DER

            params(25) = New SqlParameter("@OrderKe", SqlDbType.SmallInt)
            params(25).Value = oCustomer.OrderKe
            params(26) = New SqlParameter("@OmsetBulanan", SqlDbType.Money)
            params(26).Value = oCustomer.OmsetBulanan
            params(27) = New SqlParameter("@BiayaBulanan", SqlDbType.Money)
            params(27).Value = oCustomer.BiayaBulanan

            params(28) = New SqlParameter("@AverageBalance", SqlDbType.Decimal)
            params(28).Value = CDec(IIf(oCustomer.AverageBalance = "", "0", oCustomer.AverageBalance))
            params(29) = New SqlParameter("@Deposito", SqlDbType.Decimal)
            params(29).Value = CDec(IIf(oCustomer.Deposito = "", "0", oCustomer.Deposito))
            params(30) = New SqlParameter("@AdditionalCollateralType", SqlDbType.VarChar, 50)
            params(30).Value = oCustomer.AdditionalCollateralType
            params(31) = New SqlParameter("@AdditionalCollateralAmount", SqlDbType.Decimal)
            params(31).Value = CDec(IIf(oCustomer.AdditionalCollateralAmount = "", "0", oCustomer.AdditionalCollateralAmount))
            params(32) = New SqlParameter("@CompanyStatus", SqlDbType.VarChar, 10)
            params(32).Value = IIf(oCustomer.BusPlaceStatus = "Select One", "", oCustomer.BusPlaceStatus)
            params(33) = New SqlParameter("@CompanyStatusSinceYear ", SqlDbType.SmallInt)
            params(33).Value = IIf(oCustomer.BusPlaceSinceYear = "", "0", oCustomer.BusPlaceSinceYear)
            params(34) = New SqlParameter("@RentFinishDate ", SqlDbType.VarChar, 8)
            params(34).Value = oCustomer.RentFinishDate

            params(35) = New SqlParameter("@BankID", SqlDbType.Char, 5)
            params(35).Value = IIf(oCustomer.BankID = "0", "", oCustomer.BankID)
            params(36) = New SqlParameter("@BankBranch", SqlDbType.VarChar, 50)
            params(36).Value = oCustomer.BankBranch
            params(37) = New SqlParameter("@AccountNo", SqlDbType.Char, 20)
            params(37).Value = oCustomer.AccountNo
            params(38) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
            params(38).Value = IIf(oCustomer.AccountName <> "", Replace(oCustomer.AccountName, "'", "''"), oCustomer.AccountName)
            params(39) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(39).Value = oCustomer.Name
            params(40) = New SqlParameter("@JumlahKendaraan", SqlDbType.SmallInt)
            params(40).Value = oCustomer.JumlahKendaraan
            params(41) = New SqlParameter("@Garasi", SqlDbType.Bit)
            params(41).Value = oCustomer.Garasi
            params(42) = New SqlParameter("@IndustryRisk", SqlDbType.VarChar, 15)
            params(42).Value = oCustomer.IndustryRisk

            With companySIUPAddress
                params(43) = New SqlParameter("@CompanyAddress_SIUP", SqlDbType.VarChar, 100)
                params(43).Value = .Address
                params(44) = New SqlParameter("@CompanyRT_SIUP", SqlDbType.Char, 3)
                params(44).Value = .RT
                params(45) = New SqlParameter("@CompanyRW_SIUP", SqlDbType.Char, 3)
                params(45).Value = .RW
                params(46) = New SqlParameter("@CompanyKelurahan_SIUP", SqlDbType.VarChar, 30)
                params(46).Value = .Kelurahan
                params(47) = New SqlParameter("@CompanyKecamatan_SIUP", SqlDbType.VarChar, 30)
                params(47).Value = .Kecamatan
                params(48) = New SqlParameter("@CompanyCity_SIUP", SqlDbType.VarChar, 30)
                params(48).Value = .City
                params(49) = New SqlParameter("@CompanyZipCode_SIUP", SqlDbType.Char, 5)
                params(49).Value = .ZipCode
                params(50) = New SqlParameter("@CompanyAreaPhone1_SIUP", SqlDbType.Char, 4)
                params(50).Value = .AreaPhone1
                params(51) = New SqlParameter("@CompanyPhone1_SIUP", SqlDbType.Char, 10)
                params(51).Value = .Phone1
                params(52) = New SqlParameter("@CompanyAreaPhone2_SIUP", SqlDbType.Char, 4)
                params(52).Value = .AreaPhone2
                params(53) = New SqlParameter("@CompanyPhone2_SIUP", SqlDbType.Char, 10)
                params(53).Value = .Phone2
                params(54) = New SqlParameter("@CompanyAreaFax_SIUP", SqlDbType.Char, 4)
                params(54).Value = .AreaFax
                params(55) = New SqlParameter("@CompanyFax_SIUP", SqlDbType.Char, 10)
                params(55).Value = .Fax
            End With

            params(56) = New SqlParameter("@ResikoUsaha", SqlDbType.SmallInt)
            params(56).Value = oCustomer.ResikoUsaha
            params(57) = New SqlParameter("@BankBranchId", SqlDbType.Int)
            params(57).Value = oCustomer.BankBranchId
            'params(57) = New SqlParameter("@IsOLS", SqlDbType.Bit)
            'params(57).Value = oCustomer.IsOLS


            Dim oReturnValue As New Parameter.Customer
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spCompanySaveEdit, params)

            Dim Delete As String = "1"
            If oData2.Rows.Count > 0 Then
                params2(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                params2(1) = New SqlParameter("@CLDSeqNo", SqlDbType.SmallInt)
                params2(2) = New SqlParameter("@DocumentType", SqlDbType.VarChar, 10)
                params2(3) = New SqlParameter("@DocumentNo", SqlDbType.VarChar, 50)
                params2(4) = New SqlParameter("@DocumentDate", SqlDbType.VarChar, 8)
                params2(5) = New SqlParameter("@DocumentNotes", SqlDbType.Text)
                params2(6) = New SqlParameter("@Delete", SqlDbType.Char, 1)
                For intLoopOmset = 0 To oData2.Rows.Count - 1
                    params2(0).Value = oCustomer.CustomerID
                    params2(1).Value = CInt(oData2.Rows(intLoopOmset).Item("lblNo2"))
                    params2(2).Value = oData2.Rows(intLoopOmset).Item("cboType2")
                    params2(3).Value = oData2.Rows(intLoopOmset).Item("txtDocNo2")
                    params2(4).Value = oData2.Rows(intLoopOmset).Item("uscDate2")
                    params2(5).Value = oData2.Rows(intLoopOmset).Item("txtNotes2")
                    params2(6).Value = Delete
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spCompanySaveEdit2, params2)
                    Delete = "0"
                Next
            Else
                params2(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                params2(0).Value = oCustomer.CustomerID
                params2(1) = New SqlParameter("@CLDSeqNo", SqlDbType.SmallInt)
                params2(1).Value = 0
                params2(2) = New SqlParameter("@DocumentType", SqlDbType.VarChar, 10)
                params2(2).Value = ""
                params2(3) = New SqlParameter("@DocumentNo", SqlDbType.VarChar, 25)
                params2(3).Value = ""
                params2(4) = New SqlParameter("@DocumentDate", SqlDbType.VarChar, 8)
                params2(4).Value = ""
                params2(5) = New SqlParameter("@DocumentNotes", SqlDbType.Text)
                params2(5).Value = ""
                params2(6) = New SqlParameter("@Delete", SqlDbType.Char, 1)
                params2(6).Value = "3"

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spCompanySaveEdit2, params2)

            End If
            If oData1.Rows.Count > 0 Then
                Delete = "1"
                params1(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                params1(1) = New SqlParameter("@CSHSeqNo", SqlDbType.SmallInt)
                params1(2) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
                params1(3) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 30)
                params1(4) = New SqlParameter("@JobPositionID", SqlDbType.VarChar, 10)
                params1(5) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
                params1(6) = New SqlParameter("@Phone", SqlDbType.Char, 10)
                params1(7) = New SqlParameter("@SharePercentage", SqlDbType.Decimal)
                params1(8) = New SqlParameter("@Delete", SqlDbType.Char, 1)
                params1(9) = New SqlParameter("@Gender", SqlDbType.Char, 1)

                For intLoopOmset = 0 To oData1.Rows.Count - 1
                    params1(0).Value = oCustomer.CustomerID
                    params1(1).Value = CInt(oData1.Rows(intLoopOmset).Item("No"))
                    params1(2).Value = IIf(oData1.Rows(intLoopOmset).Item("Name").ToString <> "", Replace(oData1.Rows(intLoopOmset).Item("Name").ToString, "'", "''"), oData1.Rows(intLoopOmset).Item("Name").ToString)
                    params1(3).Value = oData1.Rows(intLoopOmset).Item("IDNumber")
                    params1(4).Value = oData1.Rows(intLoopOmset).Item("Position")
                    params1(5).Value = oData1.Rows(intLoopOmset).Item("Address")
                    params1(6).Value = oData1.Rows(intLoopOmset).Item("Phone")
                    params1(7).Value = CDec(oData1.Rows(intLoopOmset).Item("Share"))
                    params1(8).Value = Delete
                    params1(9).Value = oData1.Rows(intLoopOmset).Item("Gender")

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spCompanySaveEdit3, params1)
                    Delete = "0"
                Next
            Else
                params1(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                params1(0).Value = oCustomer.CustomerID
                params1(1) = New SqlParameter("@CSHSeqNo", SqlDbType.SmallInt)
                params1(1).Value = 0
                params1(2) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
                params1(2).Value = ""
                params1(3) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 30)
                params1(3).Value = ""
                params1(4) = New SqlParameter("@JobPositionID", SqlDbType.VarChar, 10)
                params1(4).Value = ""
                params1(5) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
                params1(5).Value = ""
                params1(6) = New SqlParameter("@Phone", SqlDbType.Char, 10)
                params1(6).Value = ""
                params1(7) = New SqlParameter("@SharePercentage", SqlDbType.Decimal)
                params1(7).Value = 0
                params1(8) = New SqlParameter("@Delete", SqlDbType.Char, 1)
                params1(8).Value = "3"
                params1(9) = New SqlParameter("@Gender", SqlDbType.Char, 1)
                params1(9).Value = ""

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spCompanySaveEdit3, params1)
            End If

            params3(0) = New SqlParameter("@Mode", SqlDbType.Bit)
            params3(0).Value = 0
            params3(1) = New SqlParameter("@KeteranganUsaha", SqlDbType.VarChar, 50)
            params3(1).Value = CompanyCustomerDPEx.KeteranganUsaha
            params3(2) = New SqlParameter("@StatusUsaha", SqlDbType.Char, 2)
            params3(2).Value = CompanyCustomerDPEx.StatusUsaha
            params3(3) = New SqlParameter("@KategoriPerusahaan", SqlDbType.Char, 1)
            params3(3).Value = CompanyCustomerDPEx.KategoriPerusahaan
            params3(4) = New SqlParameter("@KondisiKantor", SqlDbType.Char, 1)
            params3(4).Value = CompanyCustomerDPEx.KondisiKantor
            params3(5) = New SqlParameter("@KondisiLingkungan", SqlDbType.Char, 1)
            params3(5).Value = CompanyCustomerDPEx.KondisiLingkungan
            params3(6) = New SqlParameter("@NamaWebsite", SqlDbType.VarChar, 50)
            params3(6).Value = CompanyCustomerDPEx.NamaWebsite
            params3(7) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params3(7).Value = oCustomer.CustomerID

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCompanyCustomerDPExtendedAddEdit", params3)

            params4(0) = New SqlParameter("@Mode", SqlDbType.Char, 1)
            params4(0).Value = 0
            params4(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params4(1).Value = oCustomer.CustomerID
            params4(2) = New SqlParameter("@Nama", SqlDbType.VarChar, 50)
            params4(2).Value = CompanyCustomerPJ.Nama
            params4(3) = New SqlParameter("@Jabatan", SqlDbType.VarChar, 10)
            params4(3).Value = CompanyCustomerPJ.Jabatan
            params4(4) = New SqlParameter("@NPWP", SqlDbType.VarChar, 50)
            params4(4).Value = CompanyCustomerPJ.NPWP
            params4(5) = New SqlParameter("@Alamat", SqlDbType.VarChar, 50)
            params4(5).Value = CompanyCustomerPJAddress.Address
            params4(6) = New SqlParameter("@RT", SqlDbType.Char, 3)
            params4(6).Value = CompanyCustomerPJAddress.RT
            params4(7) = New SqlParameter("@RW", SqlDbType.Char, 3)
            params4(7).Value = oAddress.RW
            params4(8) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 50)
            params4(8).Value = oAddress.Kelurahan
            params4(9) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 50)
            params4(9).Value = oAddress.Kecamatan
            params4(10) = New SqlParameter("@Kota", SqlDbType.VarChar, 50)
            params4(10).Value = oAddress.City
            params4(11) = New SqlParameter("@KodePos", SqlDbType.Char, 5)
            params4(11).Value = oAddress.ZipCode
            params4(12) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
            params4(12).Value = oAddress.AreaPhone1
            params4(13) = New SqlParameter("@Phone1", SqlDbType.VarChar, 15)
            params4(13).Value = oAddress.Phone1
            params4(14) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 15)
            params4(14).Value = CompanyCustomerPJ.NoHP
            params4(15) = New SqlParameter("@Email", SqlDbType.VarChar, 15)
            params4(15).Value = CompanyCustomerPJ.Email

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCompanyCustomerPJAddEdit", params4)
            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Closed Then conn.Open()
            conn.Dispose()
        End Try
    End Function

    Public Function CompanyDataPerusahaanSaveEdit(ByVal oCustomer As Parameter.Customer,
                                    ByVal oAddress As Parameter.Address,
                                    ByVal oWarehouseAddress As Parameter.Address,
                                    ByVal CompanyCustomerDPEx As Parameter.CompanyCustomerDPEx,
                                    ByVal companySIUPAddress As Parameter.Address) As String
        Dim conn As New SqlConnection(oCustomer.strConnection)

        Dim transaction As SqlTransaction = Nothing
        Dim params(59) As SqlParameter

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params(0).Value = oCustomer.CustomerID
            params(1) = New SqlParameter("@PertamaKredit", SqlDbType.Char, 2)
            params(1).Value = oCustomer.PertamaKredit
            params(2) = New SqlParameter("@Notes", SqlDbType.Text)
            params(2).Value = oCustomer.Notes
            params(3) = New SqlParameter("@CompanyType", SqlDbType.VarChar, 10)
            params(3).Value = oCustomer.CompanyType
            params(4) = New SqlParameter("@Reference", SqlDbType.VarChar, 10)
            params(4).Value = IIf(oCustomer.Reference = "Select One", "", oCustomer.Reference)
            params(5) = New SqlParameter("@NumberOfEmployees", SqlDbType.SmallInt)
            params(5).Value = CInt(IIf(oCustomer.NumEmployees = "", "0", oCustomer.NumEmployees))
            params(6) = New SqlParameter("@YearOfEstablished", SqlDbType.SmallInt)
            params(6).Value = CInt(IIf(oCustomer.YearEstablished = "", "0", oCustomer.YearEstablished))
            params(7) = New SqlParameter("@IndustryTypeID", SqlDbType.Char, 10)
            params(7).Value = oCustomer.IndustryTypeID
            params(8) = New SqlParameter("@CustomerGroupID", SqlDbType.Char, 20)
            params(8).Value = oCustomer.CustomerGroupID
            params(9) = New SqlParameter("@CompanyAddress", SqlDbType.VarChar, 100)
            params(9).Value = oAddress.Address
            params(10) = New SqlParameter("@CompanyRT", SqlDbType.Char, 3)
            params(10).Value = oAddress.RT
            params(11) = New SqlParameter("@CompanyRW", SqlDbType.Char, 3)
            params(11).Value = oAddress.RW
            params(12) = New SqlParameter("@CompanyKelurahan", SqlDbType.VarChar, 30)
            params(12).Value = oAddress.Kelurahan
            params(13) = New SqlParameter("@CompanyKecamatan", SqlDbType.VarChar, 30)
            params(13).Value = oAddress.Kecamatan
            params(14) = New SqlParameter("@CompanyCity", SqlDbType.VarChar, 30)
            params(14).Value = oAddress.City
            params(15) = New SqlParameter("@CompanyZipCode", SqlDbType.Char, 5)
            params(15).Value = oAddress.ZipCode
            params(16) = New SqlParameter("@CompanyAreaPhone1", SqlDbType.Char, 4)
            params(16).Value = oAddress.AreaPhone1
            params(17) = New SqlParameter("@CompanyPhone1", SqlDbType.Char, 10)
            params(17).Value = oAddress.Phone1
            params(18) = New SqlParameter("@CompanyAreaPhone2", SqlDbType.Char, 4)
            params(18).Value = oAddress.AreaPhone2
            params(19) = New SqlParameter("@CompanyPhone2", SqlDbType.Char, 10)
            params(19).Value = oAddress.Phone2
            params(20) = New SqlParameter("@CompanyAreaFax", SqlDbType.Char, 4)
            params(20).Value = oAddress.AreaFax
            params(21) = New SqlParameter("@CompanyFax", SqlDbType.Char, 10)
            params(21).Value = oAddress.Fax
            params(22) = New SqlParameter("@CurrentRatio", SqlDbType.Decimal)
            params(22).Value = CDec(IIf(oCustomer.Ratio = "", "0", oCustomer.Ratio))
            params(23) = New SqlParameter("@ROI", SqlDbType.Decimal)
            params(23).Value = CDec(IIf(oCustomer.ROI = "", "0", oCustomer.ROI))
            params(24) = New SqlParameter("@DER", SqlDbType.Decimal)
            params(24).Value = CDec(IIf(oCustomer.DER = "", "0", oCustomer.DER))

            params(25) = New SqlParameter("@OrderKe", SqlDbType.SmallInt)
            params(25).Value = oCustomer.OrderKe
            params(26) = New SqlParameter("@OmsetBulanan", SqlDbType.Money)
            params(26).Value = oCustomer.OmsetBulanan
            params(27) = New SqlParameter("@BiayaBulanan", SqlDbType.Money)
            params(27).Value = oCustomer.BiayaBulanan

            params(28) = New SqlParameter("@AverageBalance", SqlDbType.Decimal)
            params(28).Value = CDec(IIf(oCustomer.AverageBalance = "", "0", oCustomer.AverageBalance))
            params(29) = New SqlParameter("@Deposito", SqlDbType.Decimal)
            params(29).Value = CDec(IIf(oCustomer.Deposito = "", "0", oCustomer.Deposito))
            params(30) = New SqlParameter("@AdditionalCollateralType", SqlDbType.VarChar, 50)
            params(30).Value = oCustomer.AdditionalCollateralType
            params(31) = New SqlParameter("@AdditionalCollateralAmount", SqlDbType.Decimal)
            params(31).Value = CDec(IIf(oCustomer.AdditionalCollateralAmount = "", "0", oCustomer.AdditionalCollateralAmount))
            params(32) = New SqlParameter("@CompanyStatus", SqlDbType.VarChar, 10)
            params(32).Value = IIf(oCustomer.BusPlaceStatus = "Select One", "", oCustomer.BusPlaceStatus)
            params(33) = New SqlParameter("@CompanyStatusSinceYear ", SqlDbType.SmallInt)
            params(33).Value = IIf(oCustomer.BusPlaceSinceYear = "", "0", oCustomer.BusPlaceSinceYear)
            params(34) = New SqlParameter("@RentFinishDate ", SqlDbType.VarChar, 8)
            params(34).Value = oCustomer.RentFinishDate

            params(35) = New SqlParameter("@BankID", SqlDbType.Char, 5)
            params(35).Value = IIf(oCustomer.BankID = "0", "", oCustomer.BankID)
            params(36) = New SqlParameter("@BankBranch", SqlDbType.VarChar, 50)
            params(36).Value = oCustomer.BankBranch
            params(37) = New SqlParameter("@AccountNo", SqlDbType.Char, 20)
            params(37).Value = oCustomer.AccountNo
            params(38) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
            params(38).Value = IIf(oCustomer.AccountName <> "", Replace(oCustomer.AccountName, "'", "''"), oCustomer.AccountName)
            params(39) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
            params(39).Value = oCustomer.Name
            params(40) = New SqlParameter("@JumlahKendaraan", SqlDbType.SmallInt)
            params(40).Value = oCustomer.JumlahKendaraan
            params(41) = New SqlParameter("@Garasi", SqlDbType.Bit)
            params(41).Value = oCustomer.Garasi
            params(42) = New SqlParameter("@IndustryRisk", SqlDbType.VarChar, 15)
            params(42).Value = oCustomer.IndustryRisk

            With companySIUPAddress
                params(43) = New SqlParameter("@CompanyAddress_SIUP", SqlDbType.VarChar, 100)
                params(43).Value = .Address
                params(44) = New SqlParameter("@CompanyRT_SIUP", SqlDbType.Char, 3)
                params(44).Value = .RT
                params(45) = New SqlParameter("@CompanyRW_SIUP", SqlDbType.Char, 3)
                params(45).Value = .RW
                params(46) = New SqlParameter("@CompanyKelurahan_SIUP", SqlDbType.VarChar, 30)
                params(46).Value = .Kelurahan
                params(47) = New SqlParameter("@CompanyKecamatan_SIUP", SqlDbType.VarChar, 30)
                params(47).Value = .Kecamatan
                params(48) = New SqlParameter("@CompanyCity_SIUP", SqlDbType.VarChar, 30)
                params(48).Value = .City
                params(49) = New SqlParameter("@CompanyZipCode_SIUP", SqlDbType.Char, 5)
                params(49).Value = .ZipCode
                params(50) = New SqlParameter("@CompanyAreaPhone1_SIUP", SqlDbType.Char, 4)
                params(50).Value = .AreaPhone1
                params(51) = New SqlParameter("@CompanyPhone1_SIUP", SqlDbType.Char, 10)
                params(51).Value = .Phone1
                params(52) = New SqlParameter("@CompanyAreaPhone2_SIUP", SqlDbType.Char, 4)
                params(52).Value = .AreaPhone2
                params(53) = New SqlParameter("@CompanyPhone2_SIUP", SqlDbType.Char, 10)
                params(53).Value = .Phone2
                params(54) = New SqlParameter("@CompanyAreaFax_SIUP", SqlDbType.Char, 4)
                params(54).Value = .AreaFax
                params(55) = New SqlParameter("@CompanyFax_SIUP", SqlDbType.Char, 10)
                params(55).Value = .Fax
            End With

            params(56) = New SqlParameter("@ResikoUsaha", SqlDbType.SmallInt)
            params(56).Value = oCustomer.ResikoUsaha
            params(57) = New SqlParameter("@BankBranchId", SqlDbType.Int)
            params(57).Value = oCustomer.BankBranchId
            params(58) = New SqlParameter("@IsOLS", SqlDbType.Bit)
            params(58).Value = oCustomer.IsOLS
            params(59) = New SqlParameter("@IsBUMN", SqlDbType.Bit)
            params(59).Value = oCustomer.IsBUMN

            Dim oReturnValue As New Parameter.Customer
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spCompanySaveEdit, params)

            Dim params3 As IList(Of SqlParameter) = New List(Of SqlParameter)
            params3.Add(New SqlParameter("@Mode", SqlDbType.Bit) With {.Value = 0})
            params3.Add(New SqlParameter("@KeteranganUsaha", SqlDbType.VarChar, 50) With {.Value = CompanyCustomerDPEx.KeteranganUsaha})
            params3.Add(New SqlParameter("@StatusUsaha", SqlDbType.Char, 2) With {.Value = CompanyCustomerDPEx.StatusUsaha})
            params3.Add(New SqlParameter("@KategoriPerusahaan", SqlDbType.Char, 1) With {.Value = CompanyCustomerDPEx.KategoriPerusahaan})
            params3.Add(New SqlParameter("@KondisiKantor", SqlDbType.Char, 1) With {.Value = CompanyCustomerDPEx.KondisiKantor})
            params3.Add(New SqlParameter("@KondisiLingkungan", SqlDbType.Char, 1) With {.Value = CompanyCustomerDPEx.KondisiLingkungan})
            params3.Add(New SqlParameter("@NamaWebsite", SqlDbType.VarChar, 50) With {.Value = CompanyCustomerDPEx.NamaWebsite})
            params3.Add(New SqlParameter("@CustomerID", SqlDbType.Char, 20) With {.Value = oCustomer.CustomerID})
            params3.Add(New SqlParameter("@JenisPembiayaan", SqlDbType.Char, 20) With {.Value = CompanyCustomerDPEx.JenisPembiayaan})

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCompanyCustomerDPExtendedAddEdit", params3.ToArray())
            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Closed Then conn.Open()
            conn.Dispose()
        End Try
    End Function

    Public Function CompanyKeuanganSaveEdit(ByVal oCustomer As Parameter.Customer) As String
        Dim conn As New SqlConnection(oCustomer.strConnection)
        Dim transaction As SqlTransaction = Nothing
        'Dim params(14) As SqlParameter
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            params.Add(New SqlParameter("@CustomerID", SqlDbType.Char, 20) With {.Value = oCustomer.CustomerID})
            params.Add(New SqlParameter("@OmsetBulanan", SqlDbType.Money) With {.Value = oCustomer.OmsetBulanan})
            params.Add(New SqlParameter("@BiayaBulanan", SqlDbType.Money) With {.Value = oCustomer.BiayaBulanan})
            params.Add(New SqlParameter("@CurrentRatio", SqlDbType.Decimal) With {.Value = oCustomer.Ratio})
            params.Add(New SqlParameter("@ROI", SqlDbType.Decimal) With {.Value = oCustomer.ROI})
            params.Add(New SqlParameter("@DER", SqlDbType.Decimal) With {.Value = oCustomer.DER})
            params.Add(New SqlParameter("@Deposito", SqlDbType.Decimal) With {.Value = CDec(IIf(oCustomer.Deposito = "", "0", oCustomer.Deposito))})
            'params.Add(New SqlParameter("@AdditionalCollateralType", SqlDbType.VarChar, 50) With {.Value = oCustomer.AdditionalCollateralType})
            'params.Add(New SqlParameter("@AdditionalCollateralAmount", SqlDbType.Decimal) With {.Value = CDec(IIf(oCustomer.AdditionalCollateralAmount = "", "0", oCustomer.AdditionalCollateralAmount))})
            params.Add(New SqlParameter("@BankBranchId", SqlDbType.Int) With {.Value = oCustomer.BankBranchId})
            params.Add(New SqlParameter("@BankID", SqlDbType.Char, 5) With {.Value = IIf(oCustomer.BankID = "0", "", oCustomer.BankID)})
            params.Add(New SqlParameter("@BankBranch", SqlDbType.VarChar, 50) With {.Value = oCustomer.BankBranch})
            params.Add(New SqlParameter("@AccountNo", SqlDbType.Char, 20) With {.Value = oCustomer.AccountNo})
            params.Add(New SqlParameter("@AccountName", SqlDbType.VarChar, 50) With {.Value = IIf(oCustomer.AccountName <> "", Replace(oCustomer.AccountName, "'", "''"), oCustomer.AccountName)})
            params.Add(New SqlParameter("@ResikoUsaha", SqlDbType.SmallInt) With {.Value = oCustomer.ResikoUsaha})
            params.Add(New SqlParameter("@ResikoLS", SqlDbType.SmallInt) With {.Value = oCustomer.ResikoLS})
            Dim oReturnValue As New Parameter.Customer
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCustomerCompanyKeuanganSaveEdit", params.ToArray())

            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Closed Then conn.Open()
            conn.Dispose()
        End Try
    End Function

    Public Function CompanyLegalitasSaveEdit(ByVal oCustomer As Parameter.Customer,
                                    ByVal oData2 As DataTable) As String
        Dim conn As New SqlConnection(oCustomer.strConnection)
        Dim transaction As SqlTransaction = Nothing

        Dim params2(7) As SqlParameter

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            Dim Delete As String = "1"
            If oData2.Rows.Count > 0 Then
                params2(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                params2(1) = New SqlParameter("@CLDSeqNo", SqlDbType.SmallInt)
                params2(2) = New SqlParameter("@DocumentType", SqlDbType.VarChar, 10)
                params2(3) = New SqlParameter("@DocumentNo", SqlDbType.VarChar, 50)
                params2(4) = New SqlParameter("@DocumentDate", SqlDbType.VarChar, 8)
                params2(5) = New SqlParameter("@JTDate", SqlDbType.VarChar, 8)
                params2(6) = New SqlParameter("@DocumentNotes", SqlDbType.Text)
                params2(7) = New SqlParameter("@Delete", SqlDbType.Char, 1)
                For intLoopOmset = 0 To oData2.Rows.Count - 1
                    params2(0).Value = oCustomer.CustomerID
                    params2(1).Value = CInt(oData2.Rows(intLoopOmset).Item("lblNo2"))
                    params2(2).Value = oData2.Rows(intLoopOmset).Item("cboType2")
                    params2(3).Value = oData2.Rows(intLoopOmset).Item("txtDocNo2")
                    params2(4).Value = oData2.Rows(intLoopOmset).Item("uscDate2")
                    params2(5).Value = oData2.Rows(intLoopOmset).Item("uscDate3")
                    params2(6).Value = oData2.Rows(intLoopOmset).Item("txtNotes2")
                    params2(7).Value = Delete
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spCompanySaveEdit2, params2)
                    Delete = "0"
                Next
            Else
                params2(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                params2(0).Value = oCustomer.CustomerID
                params2(1) = New SqlParameter("@CLDSeqNo", SqlDbType.SmallInt)
                params2(1).Value = 0
                params2(2) = New SqlParameter("@DocumentType", SqlDbType.VarChar, 10)
                params2(2).Value = ""
                params2(3) = New SqlParameter("@DocumentNo", SqlDbType.VarChar, 25)
                params2(3).Value = ""
                params2(4) = New SqlParameter("@DocumentDate", SqlDbType.VarChar, 8)
                params2(4).Value = ""
                params2(5) = New SqlParameter("@DocumentDate", SqlDbType.VarChar, 8)
                params2(5).Value = ""
                params2(6) = New SqlParameter("@DocumentNotes", SqlDbType.Text)
                params2(6).Value = ""
                params2(7) = New SqlParameter("@Delete", SqlDbType.Char, 1)
                params2(7).Value = "3"

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spCompanySaveEdit2, params2)

            End If

            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Closed Then conn.Open()
            conn.Dispose()
        End Try
    End Function

    Public Function CompanyManagementSaveEdit(ByVal oCustomer As Parameter.Customer,
                                    ByVal oData1 As DataTable,
                                    ByVal CompanyCustomerPJ As Parameter.CompanyCustomerPJ,
                                    ByVal CompanyCustomerPJAddress As Parameter.Address) As String
        Dim conn As New SqlConnection(oCustomer.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim intLoopOmset As Integer
        Dim params1(10) As SqlParameter
        Dim params4(15) As SqlParameter
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            Dim Delete As String = "1"

            If oData1.Rows.Count > 0 Then
                Delete = "1"
                params1(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                params1(1) = New SqlParameter("@CSHSeqNo", SqlDbType.SmallInt)
                params1(2) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
                params1(3) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 30)
                params1(4) = New SqlParameter("@JobPositionID", SqlDbType.VarChar, 10)
                params1(5) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
                params1(6) = New SqlParameter("@Phone", SqlDbType.Char, 10)
                params1(7) = New SqlParameter("@SharePercentage", SqlDbType.Decimal)
                params1(8) = New SqlParameter("@Delete", SqlDbType.Char, 1)
                params1(9) = New SqlParameter("@Gender", SqlDbType.Char, 1)
                params1(10) = New SqlParameter("@DefaultApproval", SqlDbType.Bit)

                For intLoopOmset = 0 To oData1.Rows.Count - 1
                    params1(0).Value = oCustomer.CustomerID
                    params1(1).Value = CInt(oData1.Rows(intLoopOmset).Item("No"))
                    params1(2).Value = IIf(oData1.Rows(intLoopOmset).Item("Name").ToString <> "", Replace(oData1.Rows(intLoopOmset).Item("Name").ToString, "'", "''"), oData1.Rows(intLoopOmset).Item("Name").ToString)
                    params1(3).Value = oData1.Rows(intLoopOmset).Item("IDNumber")
                    params1(4).Value = oData1.Rows(intLoopOmset).Item("Position")
                    params1(5).Value = oData1.Rows(intLoopOmset).Item("Address")
                    params1(6).Value = oData1.Rows(intLoopOmset).Item("Phone")
                    params1(7).Value = CDec(oData1.Rows(intLoopOmset).Item("Share"))
                    params1(8).Value = Delete
                    params1(9).Value = oData1.Rows(intLoopOmset).Item("Gender")
                    params1(10).Value = CBool(oData1.Rows(intLoopOmset).Item("Persetujuan"))

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spCompanySaveEdit3, params1)
                    Delete = "0"
                Next
            Else
                params1(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                params1(0).Value = oCustomer.CustomerID
                params1(1) = New SqlParameter("@CSHSeqNo", SqlDbType.SmallInt)
                params1(1).Value = 0
                params1(2) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
                params1(2).Value = ""
                params1(3) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 30)
                params1(3).Value = ""
                params1(4) = New SqlParameter("@JobPositionID", SqlDbType.VarChar, 10)
                params1(4).Value = ""
                params1(5) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
                params1(5).Value = ""
                params1(6) = New SqlParameter("@Phone", SqlDbType.Char, 10)
                params1(6).Value = ""
                params1(7) = New SqlParameter("@SharePercentage", SqlDbType.Decimal)
                params1(7).Value = 0
                params1(8) = New SqlParameter("@Delete", SqlDbType.Char, 1)
                params1(8).Value = "3"
                params1(9) = New SqlParameter("@Gender", SqlDbType.Char, 1)
                params1(9).Value = ""

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spCompanySaveEdit3, params1)
            End If

            params4(0) = New SqlParameter("@Mode", SqlDbType.Char, 1)
            params4(0).Value = 0
            params4(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params4(1).Value = oCustomer.CustomerID
            params4(2) = New SqlParameter("@Nama", SqlDbType.VarChar, 50)
            params4(2).Value = CompanyCustomerPJ.Nama
            params4(3) = New SqlParameter("@Jabatan", SqlDbType.VarChar, 10)
            params4(3).Value = CompanyCustomerPJ.Jabatan
            params4(4) = New SqlParameter("@NPWP", SqlDbType.VarChar, 50)
            params4(4).Value = CompanyCustomerPJ.NPWP
            params4(5) = New SqlParameter("@Alamat", SqlDbType.VarChar, 50)
            params4(5).Value = CompanyCustomerPJAddress.Address
            params4(6) = New SqlParameter("@RT", SqlDbType.Char, 3)
            params4(6).Value = CompanyCustomerPJAddress.RT
            params4(7) = New SqlParameter("@RW", SqlDbType.Char, 3)
            params4(7).Value = CompanyCustomerPJAddress.RW
            params4(8) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 50)
            params4(8).Value = CompanyCustomerPJAddress.Kelurahan
            params4(9) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 50)
            params4(9).Value = CompanyCustomerPJAddress.Kecamatan
            params4(10) = New SqlParameter("@Kota", SqlDbType.VarChar, 50)
            params4(10).Value = CompanyCustomerPJAddress.City
            params4(11) = New SqlParameter("@KodePos", SqlDbType.Char, 5)
            params4(11).Value = CompanyCustomerPJAddress.ZipCode
            params4(12) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
            params4(12).Value = CompanyCustomerPJAddress.AreaPhone1
            params4(13) = New SqlParameter("@Phone1", SqlDbType.VarChar, 15)
            params4(13).Value = CompanyCustomerPJAddress.Phone1
            params4(14) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 15)
            params4(14).Value = CompanyCustomerPJ.NoHP
            params4(15) = New SqlParameter("@Email", SqlDbType.VarChar, 15)
            params4(15).Value = CompanyCustomerPJ.Email

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCompanyManagementSaveEdit", params4)
            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Closed Then conn.Open()
            conn.Dispose()
        End Try
    End Function

    Public Function GetCompanyCustomerEC(cnn As String, oCustomClass As Parameter.CompanyCustomerEC) As Parameter.CompanyCustomerEC

        Dim params = New SqlParameter("@CustomerID", SqlDbType.Char, 20) With {.Value = oCustomClass.CustomerID}
        Dim operty = SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, "spCompanyCustomerECEdit", params).Tables(0)
        Dim oreturn = New Parameter.CompanyCustomerEC
        oreturn.BuildProperties(operty.Rows(0))

        Return oreturn
    End Function

    Public Function CompanyCustomerECSaveEdit(cnn As String, oCustomer As Parameter.CompanyCustomerEC) As String
        Dim conn As New SqlConnection(cnn)
        Dim transaction As SqlTransaction = Nothing

        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction


            params.Add(New SqlParameter("@CustomerID", SqlDbType.Char, 20) With {.Value = oCustomer.CustomerID})
            params.Add(New SqlParameter("@Nama", SqlDbType.Char, 50) With {.Value = oCustomer.Name})
            params.Add(New SqlParameter("@Hubungan", SqlDbType.Char, 20) With {.Value = oCustomer.Relation})
            params.Add(New SqlParameter("@Alamat", SqlDbType.Char, 50) With {.Value = oCustomer.Alamat.Address})
            params.Add(New SqlParameter("@RT", SqlDbType.Char, 3) With {.Value = oCustomer.Alamat.RT})
            params.Add(New SqlParameter("@RW", SqlDbType.Char, 3) With {.Value = oCustomer.Alamat.RW})
            params.Add(New SqlParameter("@Kelurahan", SqlDbType.Char, 50) With {.Value = oCustomer.Alamat.Kelurahan})
            params.Add(New SqlParameter("@Kecamatan", SqlDbType.Char, 50) With {.Value = oCustomer.Alamat.Kecamatan})
            params.Add(New SqlParameter("@Kota", SqlDbType.Char, 50) With {.Value = oCustomer.Alamat.City})
            params.Add(New SqlParameter("@KodePos", SqlDbType.Char, 5) With {.Value = oCustomer.Alamat.ZipCode})
            params.Add(New SqlParameter("@AreaPhone1", SqlDbType.Char, 4) With {.Value = oCustomer.Alamat.AreaPhone1})
            params.Add(New SqlParameter("@Phone1", SqlDbType.Char, 15) With {.Value = oCustomer.Alamat.Phone1})
            params.Add(New SqlParameter("@AreaPhone2", SqlDbType.Char, 4) With {.Value = oCustomer.Alamat.AreaPhone2})
            params.Add(New SqlParameter("@Phone2", SqlDbType.Char, 15) With {.Value = oCustomer.Alamat.Phone2})
            params.Add(New SqlParameter("@AreaFax", SqlDbType.Char, 4) With {.Value = oCustomer.Alamat.AreaFax})
            params.Add(New SqlParameter("@Fax", SqlDbType.Char, 15) With {.Value = oCustomer.Alamat.Fax})
            params.Add(New SqlParameter("@MobilePhone", SqlDbType.Char, 15) With {.Value = oCustomer.HP})
            params.Add(New SqlParameter("@Email", SqlDbType.Char, 15) With {.Value = oCustomer.Email})

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCompanyECSaveEdit", params.ToArray())
            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Closed Then conn.Open()
            conn.Dispose()
        End Try
    End Function

    Public Function GetCompanyCustomerGuarantor(cnn As String, customerid As String) As IList(Of Parameter.CompanyCustomerGuarantor)
        Dim params = New SqlParameter("@CustomerId", SqlDbType.Char, 25) With {.Value = customerid}
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.StoredProcedure, "spCustomerCompanyGuarantorEdit", params)

        Dim result = New List(Of Parameter.CompanyCustomerGuarantor)
        Dim _read As Boolean = False

        If (reader.HasRows) Then
            While (reader.Read())
                Dim row = Parameter.CompanyCustomerGuarantor.BuildProperties(reader)
                If Not row Is Nothing Then
                    row.CustomerId = customerid
                    result.Add(row)
                End If
            End While
        End If
        Return result
    End Function


    Public Function CompanyCustomerGuarantorSaveEdit(cnn As String, customerid As String, ccGuarantors As IList(Of Parameter.CompanyCustomerGuarantor)) As String
        Dim transaction As SqlTransaction = Nothing
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@ccGuarantor", SqlDbType.Structured) With {.Value = Parameter.CompanyCustomerGuarantor.ToDataTable(ccGuarantors)})
        params.Add(New SqlParameter("@CustomerId", SqlDbType.VarChar, 20) With {.Value = customerid})
        Dim errPrm = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
        params.Add(errPrm)

        Dim conn As New SqlConnection(cnn)
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCustomerCompanyGuarantorSaveEdit", params.ToArray())
            If CType(errPrm.Value, String) <> "" Then
                Return CType(errPrm.Value, String)
            End If
            transaction.Commit()
            Return ""
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Customer", "CompanyCustomerGuarantorSaveEdit", exp.Message + exp.StackTrace)
            Throw New Exception("CompanyCustomerGuarantorSaveEdit")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
#End Region

#Region "View"

    Public Function GetCustomerType(ByVal oCustomClass As Parameter.Customer) As String
        Dim oReturnValue As New Parameter.Customer
        Dim QUERY_CUSTOMERTYPE As String
        QUERY_CUSTOMERTYPE = QUERY_CHECK_CUSTOMERTYPE & " where CustomerID='" & oCustomClass.CustomerID & "'"
        Dim CustType As String
        Try
            CustType = CStr(SqlHelper.ExecuteScalar(oCustomClass.strConnection, CommandType.Text, QUERY_CUSTOMERTYPE))
            Return CustType
        Catch exp As Exception
            WriteException("Customer", "GetCustomerType", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetViewCustomer(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer

        Dim params(1) As SqlParameter

        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerID
        params(1) = New SqlParameter("@CustomerType", SqlDbType.Char, 1)
        params(1).Value = oCustomClass.CustomerType

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, VIEW_CUSTOMER, params).Tables(0)

            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "GetViewCustomer", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.GetViewCustomer")
        End Try
    End Function


    Public Function GetViewCustomerEmpOmset(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerID

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, VIEW_EMP_OMSET, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "GetViewCustomerEmpOmset", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetViewCustomerEntOmset(ByVal oCustomClass As Parameter.Customer) As DataTable
        Dim oReturnValue As New Parameter.Customer

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerID

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, VIEW_EMP_OMSET, params).Tables(0)
            Return oReturnValue.listdata
        Catch exp As Exception
            WriteException("Customer", "GetViewCustomerEntOmset", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetViewCustomerFamily(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerID

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, VIEW_CUS_FAMILY, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "GetViewCustomerFamily", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetViewCompanyCustomer(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerID

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, VIEW_COMPANY_CUSTOMER, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "GetViewCompanyCustomer", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetViewCompanyCustomerManagement(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerID

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, VIEW_COMPANY_CUSTOMER_MANAGEMENT, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "GetViewCompanyCustomerManagement", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetViewCompanyCustomerDocument(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerID

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, VIEW_COMPANY_CUSTOMER_DOCUMENT, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "GetViewCompanyCustomerDocument", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function GetAgreementListCompRpt(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerID

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, VIEW_COMPANY_CUSTOMER_AGREEMENT, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "GetAgreementListCompRpt", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetAgreementListCompRpt_Summary(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerID

        Try
            oReturnValue.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, VIEW_COMPANY_CUSTOMER_AGREEMENT, params)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "GetAgreementListCompRpt_Summary", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetAgreementListSummary1(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerID

        Try
            oReturnValue.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spAgreementListSummary1", params)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "GetAgreementListSummary1", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function GetAgreementListSummary2(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerID

        Try
            oReturnValue.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spAgreementListSummary2", params)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "GetAgreementListSummary2", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function GetViewCustomer_Summary(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerID

        Try
            oReturnValue.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, VIEW_CUSTOMER, params)

            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "GetViewCustomer_Summary", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.Customer.GetViewCustomer")
        End Try
    End Function

    Public Function GetViewCustomerEmpOmset_Summary(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerID

        Try
            oReturnValue.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, VIEW_EMP_OMSET, params)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "GetViewCustomerEmpOmset_Summary", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetViewCustomerEntOmset_Summary(ByVal oCustomClass As Parameter.Customer) As DataTable
        Dim oReturnValue As New Parameter.Customer

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerID

        Try
            oReturnValue.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, VIEW_EMP_OMSET, params)
            Return oReturnValue.listdata
        Catch exp As Exception
            WriteException("Customer", "GetViewCustomerEntOmset_Summary", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetViewCustomerFamily_Summary(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer
        Dim oReturnValue As New Parameter.Customer

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.CustomerID

        Try
            oReturnValue.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, VIEW_CUS_FAMILY, params)
            Return oReturnValue
        Catch exp As Exception
            WriteException("Customer", "GetViewCustomerFamily_Summary", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region

    Public Function GetTotalCustomer(ByVal strCustomerID As String, ByVal strConnection As String) As Integer
        Dim returnvalue As Integer

        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = strCustomerID.Trim

        Try
            returnvalue = CType(SqlHelper.ExecuteScalar(strConnection, CommandType.StoredProcedure, "spGetTotalCustomer", params), Integer)
            Return returnvalue
        Catch exp As Exception
            WriteException("Customer", "GetTotalCustomer", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function


    Public Function LoadCombo(cnn As String, ty As String) As IList(Of Parameter.CommonValueText)
        Dim str As String = ""

        Select Case (ty.ToUpper)
            Case "FAMILY"
                str = "select ID as Value ,description as Text from tblfamily order by description"

            Case "JENISPEMBIAYAAN"
                str = "select JENISPEMBIAYAANID as Value ,description as Text from JenisPembiayaan order by Description "

            Case "EMPLOYEEPOSITION"
                str = "select EmployeePositionID  as Value, EmployeePosition as Text   from TblEmployeePosition order by EmployeePosition "


            Case "IDTYPE"
                str = "select ID as Value, Description  as Text from tblidtype order by Description"
        End Select



        Dim result = New List(Of Parameter.CommonValueText)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.Text, str)


        If (reader.HasRows) Then
            If (reader Is Nothing) Then
                Return Nothing
            End If
            If (reader.IsClosed) Then
                Return Nothing
            End If

            Dim _cID = reader.GetOrdinal("Value")
            Dim _desc = reader.GetOrdinal("Text")


            While (reader.Read())
                result.Add(New Parameter.CommonValueText(
                          IIf(reader.IsDBNull(_cID), String.Empty, reader.GetString(_cID)),
                          IIf(reader.IsDBNull(_desc), "-", reader.GetString(_desc).ToString())))
            End While

        End If
        Return result
    End Function
    Public Function GetPersonalOL(cnn As String, custId As String) As IList(Of PinjamanLain)
        Dim result = New List(Of Parameter.PinjamanLain)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(cnn, CommandType.StoredProcedure, "sP_GetPersonalOL", New SqlParameter("@CustomerID", SqlDbType.VarChar, 20) With {.Value = custId})

        If (reader.HasRows) Then
            If (reader Is Nothing) Then
                Return Nothing
            End If
            If (reader.IsClosed) Then
                Return Nothing
            End If
            Dim _angsuran = reader.GetOrdinal("Angsuran")
            Dim _jenisPinjaman = reader.GetOrdinal("JenisPinjaman")
            Dim _JumlahPinjaman = reader.GetOrdinal("JumlahPinjaman")

            Dim _Tenor = reader.GetOrdinal("Tenor")
            Dim _PersonalCustomerOLSeqNo = reader.GetOrdinal("PersonalCustomerOLSeqNo")
            Dim _NamaLembagaKeuangan = reader.GetOrdinal("NamaLembagaKeuangan")
            Dim _SisaPokok = reader.GetOrdinal("SisaPokok")
            Dim no As Integer = 1
            While (reader.Read())
                result.Add(New Parameter.PinjamanLain With {.No = no,
                                                            .Name = IIf(reader.IsDBNull(_NamaLembagaKeuangan), String.Empty, reader.GetString(_NamaLembagaKeuangan)),
                                                            .Angsuran = IIf(reader.IsDBNull(_angsuran), 0, reader.GetDouble(_angsuran)),
                                                            .CustID = custId,
                                                            .JenisPinjaman = IIf(reader.IsDBNull(_jenisPinjaman), String.Empty, reader.GetString(_jenisPinjaman)),
                .JumlahPinjaman = IIf(reader.IsDBNull(_JumlahPinjaman), 0, reader.GetDouble(_JumlahPinjaman)),
                .Tenor = IIf(reader.IsDBNull(_Tenor), 0, reader.GetDecimal(_Tenor)),
                .SeqNo = IIf(reader.IsDBNull(_PersonalCustomerOLSeqNo), 0, reader.GetValue(_PersonalCustomerOLSeqNo)),
                .SisaPokok = IIf(reader.IsDBNull(_SisaPokok), 0, reader.GetDouble(_SisaPokok))})
                no += 1
            End While

        End If
        Return result
    End Function
    Public Function PersonalCustomerOLSaveEdit(cnn As String, custid As String, data As IList(Of PinjamanLain)) As String
        ' Return ""

        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Dim objCon As New SqlConnection(cnn)
        Dim transaction As SqlTransaction = Nothing
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params.Add(New SqlParameter("@PersonalCustOL", SqlDbType.Structured) With {.Value = (PinjamanLain.ToDataTable(data))})
            params.Add(New SqlParameter("@CustomerID", SqlDbType.VarChar, 50) With {.Value = custid})
            Dim errPrm = New SqlParameter("@errMsg", SqlDbType.VarChar, 2048) With {.Direction = ParameterDirection.Output}
            params.Add(errPrm)
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCustomerPersonalOLSaveEdit", params.ToArray)
            If CType(errPrm.Value, String) <> "" Then
                Return CType(errPrm.Value, String)
            End If
            transaction.Commit()
            'Return ""

        Catch exp As Exception
            transaction.Rollback()
            WriteException("Invoice", "InvoiceSave", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Function

	Public Function CheckPersonalCustomerDocument(ByVal oCustomClass As Parameter.Customer) As Integer
		Dim returnvalue As Integer

		Dim params(0) As SqlParameter
		params(0) = New SqlParameter("@IDNumber", SqlDbType.Char, 25)
		params(0).Value = oCustomClass.IDNumber

		Try

			returnvalue = CType(SqlHelper.ExecuteScalar(oCustomClass.strConnection, CommandType.StoredProcedure, CHECK_PERSONAL_DOCUMENT, params), Integer)
			Return returnvalue
		Catch exp As Exception
			WriteException("Customer", "CheckPersonalCustomerDocument", exp.Message + exp.StackTrace)
			Throw New Exception(exp.Message)
		End Try
	End Function

	Public Function GetCompanyCustomerPK(cnn As String, oCustomClass As Parameter.CompanyCustomerPK) As Parameter.CompanyCustomerPK
		Dim params() As SqlParameter = New SqlParameter(1) {}
		Dim objcon As New SqlConnection(cnn)
		Dim objtrans As SqlTransaction = Nothing

		Try
			If objcon.State = ConnectionState.Closed Then objcon.Open()
			objtrans = objcon.BeginTransaction

			params(0) = New SqlParameter("@CustomerId", SqlDbType.VarChar, 20)
			params(0).Value = oCustomClass.CustomerId
			params(1) = New SqlParameter("@SeqNo", SqlDbType.BigInt)
			params(1).Value = oCustomClass.SeqNo

			oCustomClass.Listdata = SqlHelper.ExecuteDataset(objtrans, CommandType.StoredProcedure, "spGetCompanyCustomerPK", params).Tables(0)
			Return oCustomClass
		Catch exp As Exception
			WriteException("GetCompanyCustomerPK", "GetCompanyCustomerPK", exp.Message + exp.StackTrace)
			Return Nothing
		End Try
	End Function

	Public Function CompanyCustomerPKSaveAdd(ByVal oCustomClass As Parameter.CompanyCustomerPK) As Parameter.CompanyCustomerPK
		Dim oReturn As New Parameter.Customer
		Dim conn As New SqlConnection(oCustomClass.strConnection)
		Dim transaction As SqlTransaction = Nothing
		Dim ErrMessage As String = ""

		Dim params(29) As SqlParameter


		Try
			If conn.State = ConnectionState.Closed Then conn.Open()
			transaction = conn.BeginTransaction
			params(0) = New SqlParameter("@CustomerId", SqlDbType.VarChar, 20)
			params(0).Value = oCustomClass.CustomerId
			params(1) = New SqlParameter("@TglLaporanKeuangan ", SqlDbType.Date)
			params(1).Value = oCustomClass.TglLaporanKeuangan
			params(2) = New SqlParameter("@AssetIDR", SqlDbType.Money)
			params(2).Value = oCustomClass.AssetIDR
			params(3) = New SqlParameter("@AssetLancar", SqlDbType.Money)
			params(3).Value = oCustomClass.AssetLancar
			params(4) = New SqlParameter("@KasSetaraKasAsetLancar", SqlDbType.Money)
			params(4).Value = oCustomClass.KasSetaraKasAsetLancar
			params(5) = New SqlParameter("@PiutangUsahAsetLancar", SqlDbType.Money)
			params(5).Value = oCustomClass.PiutangUsahAsetLancar
			params(6) = New SqlParameter("@InvestasiLainnyaAsetLancar", SqlDbType.Money)
			params(6).Value = oCustomClass.InvestasiLainnyaAsetLancar
			params(7) = New SqlParameter("@AsetLancarLainnya", SqlDbType.Money)
			params(7).Value = oCustomClass.AsetLancarLainnya
			params(8) = New SqlParameter("@AsetTidakLancar", SqlDbType.Money)
			params(8).Value = oCustomClass.AsetTidakLancar
			params(9) = New SqlParameter("@PiutangUsahaAsetTidakLancar", SqlDbType.Money)
			params(9).Value = oCustomClass.PiutangUsahaAsetTidakLancar
			params(10) = New SqlParameter("@InvestasiLainnyaAsetTidakLancar", SqlDbType.Money)
			params(10).Value = oCustomClass.InvestasiLainnyaAsetTidakLancar
			params(11) = New SqlParameter("@AsetTidakLancarLainnya", SqlDbType.Money)
			params(11).Value = oCustomClass.AsetTidakLancarLainnya
			params(12) = New SqlParameter("@Liabilitas", SqlDbType.Money)
			params(12).Value = oCustomClass.Liabilitas
			params(13) = New SqlParameter("@LiabilitasJangkaPendek", SqlDbType.Money)
			params(13).Value = oCustomClass.LiabilitasJangkaPendek
			params(14) = New SqlParameter("@PinjamanJangkaPendek", SqlDbType.Money)
			params(14).Value = oCustomClass.PinjamanJangkaPendek
			params(15) = New SqlParameter("@UtangUsahaJangkaPendek", SqlDbType.Money)
			params(15).Value = oCustomClass.UtangUsahaJangkaPendek
			params(16) = New SqlParameter("@LiabilitasJangkaPendekLainnya", SqlDbType.Money)
			params(16).Value = oCustomClass.LiabilitasJangkaPendekLainnya
			params(17) = New SqlParameter("@LiabilitasJangkaPanjang", SqlDbType.Money)
			params(17).Value = oCustomClass.LiabilitasJangkaPanjang
			params(18) = New SqlParameter("@PinjamanJangkaPanjang", SqlDbType.Money)
			params(18).Value = oCustomClass.PinjamanJangkaPanjang
			params(19) = New SqlParameter("@UtangUsahaJangkaPanjang", SqlDbType.Money)
			params(19).Value = oCustomClass.UtangUsahaJangkaPanjang
			params(20) = New SqlParameter("@LiabilitasJangkaPanjangLainnya", SqlDbType.Money)
			params(20).Value = oCustomClass.LiabilitasJangkaPanjangLainnya
			params(21) = New SqlParameter("@Ekuitas", SqlDbType.Money)
			params(21).Value = oCustomClass.Ekuitas
			params(22) = New SqlParameter("@PendapatanUsahaOpr", SqlDbType.Money)
			params(22).Value = oCustomClass.PendapatanUsahaOpr
			params(23) = New SqlParameter("@BebanPokokPendapatanOpr", SqlDbType.Money)
			params(23).Value = oCustomClass.BebanPokokPendapatanOpr
			params(24) = New SqlParameter("@LabaRugiBruto", SqlDbType.Money)
			params(24).Value = oCustomClass.LabaRugiBruto
			params(25) = New SqlParameter("@PLLNonOpr", SqlDbType.Money)
			params(25).Value = oCustomClass.PLLNonOpr
			params(26) = New SqlParameter("@BebanLainLainNonOpr", SqlDbType.Money)
			params(26).Value = oCustomClass.BebanLainLainNonOpr
			params(27) = New SqlParameter("@LabaRugiSebelumPajak", SqlDbType.Money)
			params(27).Value = oCustomClass.LabaRugiSebelumPajak
			params(28) = New SqlParameter("@LabaRugiTahunBerjalan", SqlDbType.Money)
			params(28).Value = oCustomClass.LabaRugiTahunBerjalan

			params(29) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
			params(29).Direction = ParameterDirection.Output

			Dim oReturnValue As New Parameter.CompanyCustomerPK

			SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCompanyCustomerPKSaveAdd", params)
			ErrMessage = CType(params(29).Value, String)

			If ErrMessage <> "" Then
				oReturnValue.Err = ErrMessage
				Throw New Exception(ErrMessage)
			End If

			transaction.Commit()
			oReturnValue.Err = ""
			Return oReturnValue
		Catch exp As Exception
			transaction.Rollback()
			WriteException("Customer", "CompanyCustomerPKSaveAdd", exp.Message + exp.StackTrace)
			Throw New Exception("Error On DataAccess.AccAcq.Customer.CompanyCustomerPKSaveAdd")
		Finally
			If conn.State = ConnectionState.Closed Then conn.Open()
			conn.Dispose()
		End Try
	End Function

	Public Function CompanyCustomerPKSaveEdit(ByVal oCustomClass As Parameter.CompanyCustomerPK) As Parameter.CompanyCustomerPK
		Dim oReturn As New Parameter.Customer
		Dim conn As New SqlConnection(oCustomClass.strConnection)
		Dim transaction As SqlTransaction = Nothing
		Dim ErrMessage As String = ""

		Dim params(30) As SqlParameter


		Try
			If conn.State = ConnectionState.Closed Then conn.Open()
			transaction = conn.BeginTransaction

			params(30) = New SqlParameter("@SeqNo", SqlDbType.BigInt)
			params(30).Value = oCustomClass.SeqNo
			params(29) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
			params(29).Direction = ParameterDirection.Output
			params(28) = New SqlParameter("@CustomerId", SqlDbType.VarChar, 20)
			params(28).Value = oCustomClass.CustomerId
			params(27) = New SqlParameter("@TglLaporanKeuangan ", SqlDbType.Date)
			params(27).Value = oCustomClass.TglLaporanKeuangan
			params(26) = New SqlParameter("@AssetIDR", SqlDbType.Money)
			params(26).Value = oCustomClass.AssetIDR
			params(25) = New SqlParameter("@AssetLancar", SqlDbType.Money)
			params(25).Value = oCustomClass.AssetLancar
			params(24) = New SqlParameter("@KasSetaraKasAsetLancar", SqlDbType.Money)
			params(24).Value = oCustomClass.KasSetaraKasAsetLancar
			params(23) = New SqlParameter("@PiutangUsahAsetLancar", SqlDbType.Money)
			params(23).Value = oCustomClass.PiutangUsahAsetLancar
			params(22) = New SqlParameter("@InvestasiLainnyaAsetLancar", SqlDbType.Money)
			params(22).Value = oCustomClass.InvestasiLainnyaAsetLancar
			params(21) = New SqlParameter("@AsetLancarLainnya", SqlDbType.Money)
			params(21).Value = oCustomClass.AsetLancarLainnya
			params(20) = New SqlParameter("@AsetTidakLancar", SqlDbType.Money)
			params(20).Value = oCustomClass.AsetTidakLancar
			params(19) = New SqlParameter("@PiutangUsahaAsetTidakLancar", SqlDbType.Money)
			params(19).Value = oCustomClass.PiutangUsahaAsetTidakLancar
			params(18) = New SqlParameter("@InvestasiLainnyaAsetTidakLancar", SqlDbType.Money)
			params(18).Value = oCustomClass.InvestasiLainnyaAsetTidakLancar
			params(17) = New SqlParameter("@AsetTidakLancarLainnya", SqlDbType.Money)
			params(17).Value = oCustomClass.AsetTidakLancarLainnya
			params(16) = New SqlParameter("@Liabilitas", SqlDbType.Money)
			params(16).Value = oCustomClass.Liabilitas
			params(15) = New SqlParameter("@LiabilitasJangkaPendek", SqlDbType.Money)
			params(15).Value = oCustomClass.LiabilitasJangkaPendek
			params(14) = New SqlParameter("@PinjamanJangkaPendek", SqlDbType.Money)
			params(14).Value = oCustomClass.PinjamanJangkaPendek
			params(13) = New SqlParameter("@UtangUsahaJangkaPendek", SqlDbType.Money)
			params(13).Value = oCustomClass.UtangUsahaJangkaPendek
			params(12) = New SqlParameter("@LiabilitasJangkaPendekLainnya", SqlDbType.Money)
			params(12).Value = oCustomClass.LiabilitasJangkaPendekLainnya
			params(11) = New SqlParameter("@LiabilitasJangkaPanjang", SqlDbType.Money)
			params(11).Value = oCustomClass.LiabilitasJangkaPanjang
			params(10) = New SqlParameter("@PinjamanJangkaPanjang", SqlDbType.Money)
			params(10).Value = oCustomClass.PinjamanJangkaPanjang
			params(9) = New SqlParameter("@UtangUsahaJangkaPanjang", SqlDbType.Money)
			params(9).Value = oCustomClass.UtangUsahaJangkaPanjang
			params(8) = New SqlParameter("@LiabilitasJangkaPanjangLainnya", SqlDbType.Money)
			params(8).Value = oCustomClass.LiabilitasJangkaPanjangLainnya
			params(7) = New SqlParameter("@Ekuitas", SqlDbType.Money)
			params(7).Value = oCustomClass.Ekuitas
			params(6) = New SqlParameter("@PendapatanUsahaOpr", SqlDbType.Money)
			params(6).Value = oCustomClass.PendapatanUsahaOpr
			params(5) = New SqlParameter("@BebanPokokPendapatanOpr", SqlDbType.Money)
			params(5).Value = oCustomClass.BebanPokokPendapatanOpr
			params(4) = New SqlParameter("@LabaRugiBruto", SqlDbType.Money)
			params(4).Value = oCustomClass.LabaRugiBruto
			params(3) = New SqlParameter("@PLLNonOpr", SqlDbType.Money)
			params(3).Value = oCustomClass.PLLNonOpr
			params(2) = New SqlParameter("@BebanLainLainNonOpr", SqlDbType.Money)
			params(2).Value = oCustomClass.BebanLainLainNonOpr
			params(1) = New SqlParameter("@LabaRugiSebelumPajak", SqlDbType.Money)
			params(1).Value = oCustomClass.LabaRugiSebelumPajak
			params(0) = New SqlParameter("@LabaRugiTahunBerjalan", SqlDbType.Money)
			params(0).Value = oCustomClass.LabaRugiTahunBerjalan

			Dim oReturnValue As New Parameter.CompanyCustomerPK

			SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCompanyCustomerPKSaveEdit", params)
			ErrMessage = CType(params(29).Value, String)

			If ErrMessage <> "" Then
				oReturnValue.Err = ErrMessage
				Throw New Exception(ErrMessage)
			End If

			transaction.Commit()
			oReturnValue.Err = ""
			Return oReturnValue
		Catch exp As Exception
			transaction.Rollback()
			WriteException("Customer", "CompanyCustomerPKSaveEdit", exp.Message + exp.StackTrace)
			Throw New Exception("Error On DataAccess.AccAcq.Customer.CompanyCustomerPKSaveEdit")
		Finally
			If conn.State = ConnectionState.Closed Then conn.Open()
			conn.Dispose()
		End Try
	End Function
End Class
