

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class PPK : Inherits DataAccessBase

#Region "Constanta"
    Private Const spPPKListAgreement As String = "spListAgreementPPK"
    Private Const spSavePrint As String = "spSavePrintPPK"
    Private Const spSavePrintPO As String = "spSavePrintPO"
    Private Const spSavePrintSuratKuasa As String = "spSavePrintSuratKuasa"
    Private Const spSavePrintKwitansi As String = "spSavePrintKwitansi"
    Private Const spSavePrintSSB As String = "spSavePrintSlipSetoranBank"
    Private Const spSuratKuasaReport As String = "spPrintSuratKuasa"
    Private Const spPPKReport As String = "spPPKReport"
    Private Const spReportPrintPO As String = "spPrintPO"
    Private Const spReportPrintKwitansi As String = "spRptPrintKwitansi"
    Private Const spListAccount As String = "spListAccount"
    Private Const spListPrintSSB As String = "spPrintSlipSetoranBank"
    Private Const spGetAssetModel As String = "spGetAssetModel"
    Private Const spAgingReport As String = "spAgingReport"
    Private Const spGetTypeAsset As String = "spGetTypeAsset"
    Private Const spCreditScoringCustomer As String = "spCreditScoringCustomer"
    Private Const spCreditScoringCustomerEnt As String = "spCreditScoringCustomerEnt"
    Private Const spCreditScoringCustomerProf As String = "spCreditScoringCustomerProf"
    Private Const SP_VIEW_AMORTIZATION As String = "spViewAmortization"
    Protected Const PARAM_COLLECTORTYPE As String = "@CollectorType"
    Protected Const PARAM_MAILTRANSNO As String = "@MailTransNo"
    Protected Const PARAM_APPLICATIONID As String = "@ApplicationID"
    Protected Const PARAM_WHERECOND2 As String = "@wherecond2"
    Private Const spSavePrintPOKaroseri As String = "spSavePrintPOKaroseri"
    Private Const spReportPrintPOKaroseri As String = "spPrintPOKaroseri"
    Private Const spPPKListAgreementFL As String = "spListAgreementPPKFL"
    Private Const spSavePrintFL As String = "spSavePrintPPKFL"

#End Region
#Region "List Agreement"
    Public Function ListAgreement(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            customclass.ListPPK = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spPPKListAgreement, params).Tables(0)
            customclass.TotalRecord = CType(params(4).Value, Int64)
            Return customclass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function ListAgreementFL(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            customclass.ListPPK = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spPPKListAgreementFL, params).Tables(0)
            customclass.TotalRecord = CType(params(4).Value, Int64)
            Return customclass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

#End Region
#Region "Save Print"
    Public Function SavePrint(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction


            Dim params() As SqlParameter = New SqlParameter(5) {}
            oDataTable = customclass.ListPPK
            For intloop = 0 To oDataTable.Rows.Count - 1

                params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params(2).Value = customclass.BranchId

                params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                params(3).Value = customclass.BusinessDate

                params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                params(4).Value = customclass.LoginId

                params(5) = New SqlParameter("@TanggalKontrak", SqlDbType.DateTime)
                params(5).Value = customclass.TanggalKontrak

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePrint, params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function

    Public Function SavePrintFL(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction


            Dim params() As SqlParameter = New SqlParameter(5) {}
            oDataTable = customclass.ListPPK
            For intloop = 0 To oDataTable.Rows.Count - 1

                params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params(2).Value = customclass.BranchId

                params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                params(3).Value = customclass.BusinessDate

                params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                params(4).Value = customclass.LoginId

                params(5) = New SqlParameter("@TanggalKontrak", SqlDbType.DateTime)
                params(5).Value = customclass.TanggalKontrak

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePrintFL, params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function

#End Region
#Region "List PPK Report"
    Public Function ListPPKReport(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Dim SPString As String = ""

        If customclass.Status = "C" Then
            SPString = "spPPKCompanyReport"
        Else
            SPString = "spPPKReport"
        End If

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 1000)
        params(1).Value = customclass.MultiApplicationID
        params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(2).Value = customclass.BranchId
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 1000)
        params(3).Value = customclass.SortBy

        Try
            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPString, params)
        Catch ex As Exception
        End Try
        Return customclass
    End Function

    Public Function ListPPKReportFL(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Dim SPString As String = ""

        If customclass.Status = "C" Then
            SPString = "spPPKReportFL"
        Else
            SPString = "spPPKReportFL"
        End If

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 1000)
        params(1).Value = customclass.MultiApplicationID
        params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(2).Value = customclass.BranchId
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 1000)
        params(3).Value = customclass.SortBy

        Try
            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPString, params)
        Catch ex As Exception
        End Try
        Return customclass
    End Function
#End Region

    '============================= Print Surat Kuasa ==============================
#Region "Save Print Surat Kuasa"
    Public Function SavePrintSuratKuasa(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(4) {}
            oDataTable = customclass.ListPPK
            For intloop = 0 To oDataTable.Rows.Count - 1

                params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params(2).Value = customclass.BranchId

                params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                params(3).Value = customclass.BusinessDate

                params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                params(4).Value = customclass.LoginId

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePrintSuratKuasa, params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
#End Region
#Region "List Surat Kuasa Report"
    Public Function ListSuratKuasaReport(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
        params(0).Value = customclass.BusinessDate
        params(1) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(1).Value = customclass.WhereCond
        params(2) = New SqlParameter("@SortBy", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.SortBy

        customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spSuratKuasaReport, params)
        Return customclass
    End Function
#End Region

    '============================== Print PO ==============================
#Region "Save Print PO"
    Public Function SavePrintPO(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(4) {}
            oDataTable = customclass.ListPPK
            For intloop = 0 To oDataTable.Rows.Count - 1

                params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params(2).Value = customclass.BranchId

                params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                params(3).Value = customclass.BusinessDate

                params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                params(4).Value = customclass.LoginId

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePrintPO, params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
#End Region
#Region "List PO Report"
    Public Function ListReportPO(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond


        customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spReportPrintPO, params)
        Return customclass
    End Function
#End Region

    '================================= Print Kwitansi =============================
#Region "Save Print Kwitansi"
    Public Function SavePrintKwitansi(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(4) {}
            oDataTable = customclass.ListPPK
            For intloop = 0 To oDataTable.Rows.Count - 1

                params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params(2).Value = customclass.BranchId

                params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                params(3).Value = customclass.BusinessDate

                params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                params(4).Value = customclass.LoginId

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePrintKwitansi, params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
#End Region
#Region "List Print Kwitansi"
    Public Function ListPrintKwitansi(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(3) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@MultiApplicationID", SqlDbType.VarChar, 1000)
        params(1).Value = customclass.MultiApplicationID
        params(2) = New SqlParameter("@MultiAgreementNo", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.MultiAgreementNo
        params(3) = New SqlParameter("@Branch_ID", SqlDbType.Char, 3)
        params(3).Value = customclass.Branch_ID


        customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spReportPrintKwitansi, params)
        Return customclass
    End Function
#End Region

    '================================ Print Slip Setoran Bank ================
#Region "List Account"
    Public Function ListAccountByPayment(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@BankAccounttype", SqlDbType.VarChar)
        params(1).Value = customclass.BankAccountType
        params(2) = New SqlParameter("@BankPurpose", SqlDbType.VarChar)
        params(2).Value = customclass.BankPurpose
        customclass.ListPPK = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spListAccountByPayment", params).Tables(0)
        Return customclass
    End Function

    Public Function ListAccount(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        customclass.ListPPK = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListAccount, params).Tables(0)
        Return customclass
    End Function
#End Region
#Region "Save Slip Setoran Bank"
    Public Function SaveSlipSetoranBank(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(4) {}
            oDataTable = customclass.ListPPK
            For intloop = 0 To oDataTable.Rows.Count - 1

                params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params(2).Value = customclass.BranchId

                params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                params(3).Value = customclass.BusinessDate

                params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                params(4).Value = customclass.LoginId

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePrintSSB, params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
#End Region
#Region "List Print Slip Setoran Bank"
    Public Function ListPrintSSB(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond

        customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListPrintSSB, params)
        Return customclass
    End Function
#End Region
#Region "GetBankID"
    Public Function GetBankID(ByVal customclass As Parameter.PPK) As String
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 50)
        params(0).Value = customclass.AccountNo
        Try
            Return SqlHelper.ExecuteScalar(customclass.strConnection, CommandType.StoredProcedure, "spGetBankID", params).ToString
        Catch ex As Exception
        End Try
    End Function
#End Region

    '========================== ReportAgingByModel ================================
#Region "GetAssetModel"
    Public Function GetAssetModel(ByVal customclass As Parameter.PPK) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spGetAssetModel).Tables(0)
    End Function
#End Region
#Region "ViewReportAging"
    Public Function ViewReportAging(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objcommand As New SqlCommand
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objcommand.Connection = objcon
            objcommand.Parameters.Add(PARAM_WHERECOND, SqlDbType.VarChar, 1000).Value = customclass.WhereCond
            objcommand.Parameters.Add(PARAM_WHERECOND2, SqlDbType.VarChar, 8000).Value = customclass.WhereCond2
            objcommand.CommandType = CommandType.StoredProcedure
            objcommand.CommandText = spAgingReport
            objcommand.CommandTimeout = 100000000
            da = New SqlDataAdapter(objcommand)
            da.Fill(ds)
            customclass.ListAgreement = ds
            '            customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAgingReport, params)

            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
            objcommand.Parameters.Clear()
            objcommand.Dispose()
            da.Dispose()
        End Try
    End Function
#End Region
#Region "GetAssetType"
    Public Function GetAssetType(ByVal customclass As Parameter.PPK) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spGetTypeAsset).Tables(0)
    End Function
#End Region
#Region "GetCreditScoringCustomer"
    Public Function GetCreditScoringCustomer(ByVal customclass As Parameter.PPK) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCreditScoringCustomer).Tables(0)
    End Function
#End Region
#Region "GetCreditScoringCustomer for CustomerType Enterpreiner"
    Public Function GetCreditScoringCustomerEnt(ByVal customclass As Parameter.PPK) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCreditScoringCustomerEnt).Tables(0)
    End Function
#End Region
#Region "GetCreditScoringCustomer for CustomerType Professional"
    Public Function GetCreditScoringCustomerProf(ByVal customclass As Parameter.PPK) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCreditScoringCustomerProf).Tables(0)
    End Function
#End Region

    '============================ ListRptAmortization ===================================
#Region "ListRptAmortization"
    Public Function ListRptAmortization(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 20)
        params(0).Value = customclass.ApplicationID
        params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
        params(1).Value = customclass.BusinessDate
        customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SP_VIEW_AMORTIZATION, params)
        Return customclass
    End Function
#End Region

    '============================= Confirmation Letter Report ============================
#Region "ListReportConfLetter"
    Public Function ListReportConfLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond

        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spConfirmationLetterPrint", params)
        Return customclass
    End Function
#End Region
#Region "ListConfLetter"
    Public Function ListConfLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oReturnValue As New Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spConfirmationLetterList", params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region
#Region "ListWaadLetter"
    Public Function ListWaadLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oReturnValue As New Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spWaadLetterList", params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region
#Region "ListWakalahLetter"
    Public Function ListWakalahLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oReturnValue As New Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spWakalahLetterList", params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region
#Region "ListReportWakalahLetter"
    Public Function ListReportWakalahLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spWakalahLetterPrint", params)
        Return customclass
    End Function
#End Region

#Region "Save Print Confirmation Letter"
    Public Function SavePrintConfLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(4) {}
            oDataTable = customclass.ListConfLetter
            For intloop = 0 To oDataTable.Rows.Count - 1
                params(0) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(2).Value = customclass.BranchId

                params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params(3).Value = customclass.BusinessDate

                params(4) = New SqlParameter("@LoginID", SqlDbType.Char, 12)
                params(4).Value = customclass.LoginId
                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, "spConfirmationLetterSavePrint", params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
#End Region
#Region "Save Print Waad Letter"
    Public Function SavePrintWaadLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(4) {}
            oDataTable = customclass.ListConfLetter
            For intloop = 0 To oDataTable.Rows.Count - 1
                params(0) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(2).Value = customclass.BranchId

                params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params(3).Value = customclass.BusinessDate

                params(4) = New SqlParameter("@LoginID", SqlDbType.Char, 12)
                params(4).Value = customclass.LoginId
                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, "spWaadLetterSavePrint", params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
#End Region
#Region "Save Print Wakalah Letter"
    Public Function SavePrintWakalahLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(4) {}
            oDataTable = customclass.ListConfLetter
            For intloop = 0 To oDataTable.Rows.Count - 1
                params(0) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(2).Value = customclass.BranchId

                params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params(3).Value = customclass.BusinessDate

                params(4) = New SqlParameter("@LoginID", SqlDbType.Char, 12)
                params(4).Value = customclass.LoginId
                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, "spWakalahLetterSavePrint", params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
#End Region

    '=============================PKS==============================================
    '#Region "ListPKS"
    '    Public Function ListPKS(ByVal customclass As Parameter.PPK) As Parameter.PPK
    '        Dim oReturnValue As New Parameter.PPK
    '        Dim params() As SqlParameter = New SqlParameter(4) {}

    '        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
    '        params(0).Value = customclass.CurrentPage

    '        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
    '        params(1).Value = customclass.PageSize

    '        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
    '        params(2).Value = customclass.WhereCond

    '        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
    '        params(3).Value = customclass.SortBy

    '        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
    '        params(4).Direction = ParameterDirection.Output

    '        Try
    '            oReturnValue.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPernyataanKerjasamaSuppPrint", params).Tables(0)
    '            oReturnValue.TotalRecord = CType(params(4).Value, Int64)

    '            Return oReturnValue
    '        Catch ex As Exception
    '            Throw New Exception(ex.Message)
    '        End Try
    '    End Function
    '#End Region

    Public Function ListPKS(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.ListTandaTerimaDokumenReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPernyataanKerjasamaSuppPrint", params)
        Return customclass
    End Function

    '============================= Report Bon Hijau============================
#Region "ListBonHijau"
    Public Function ListBonHijau(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy

        customclass.ListBonHijau = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spBonHijauList", params).Tables(0)
        Return customclass
    End Function
#End Region
#Region "ListBonHijauReport"
    Public Function ListBonHijauReport(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@GroupSupplierID", SqlDbType.VarChar, 4000)
        params(0).Value = customclass.GroupSupplierID
        params(1) = New SqlParameter("@GroupInvoiceNo", SqlDbType.VarChar, 4000)
        params(1).Value = customclass.GroupInvoiceNo
        params(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(2).Value = customclass.BDate

        customclass.ListBonHijauReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spBonHijauReport", params)
        Return customclass
    End Function
#End Region
#Region "Save Print Bon Hijau"
    Public Function SavePrintBonHijau(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(4) {}
            oDataTable = customclass.ListBonHijau
            For intloop = 0 To oDataTable.Rows.Count - 1
                params(0) = New SqlParameter("@InvoiceNo", SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("InvoiceNo")

                params(1) = New SqlParameter("@SupplierID", SqlDbType.Char, 10)
                params(1).Value = oDataTable.Rows(intloop)("SupplierID")

                params(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params(2).Value = customclass.BusinessDate

                params(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(3).Value = customclass.BranchId

                params(4) = New SqlParameter("@LoginID", SqlDbType.Char, 12)
                params(4).Value = customclass.LoginId
                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, "spBonHijauSavePrint", params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
#End Region

    '============================= Tanda Terima Dokumen Report ============================
#Region "ListReportTandaTerimaDokumen"
    Public Function ListReportTandaTerimaDokumen(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.ListTandaTerimaDokumenReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spTandaTerimaDokumenPrint", params)
        Return customclass
    End Function
#End Region


    Public Function ListReportPernyataanNPWP(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.ListTandaTerimaDokumenReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPernyataanNPWPPrint", params)
        Return customclass
    End Function
    Public Function ListReportSuratPernyataanNasabah(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.ListTandaTerimaDokumenReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPernyataanNasabah", params)
        Return customclass
    End Function

    Public Function ListReportPernyataanBedaTandaTangan(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.ListTandaTerimaDokumenReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPernyataanBedaTandaTanganPrint", params)
        Return customclass
    End Function

    Public Function ListReportSuratPenolakan(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.ListTandaTerimaDokumenReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSuratPenolakanPrint", params)
        Return customclass
    End Function

    Public Function ListReportBastk(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.ListTandaTerimaDokumenReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spRptBastk", params)
        Return customclass
    End Function

    Public Function ListReportSptransfer(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.ListTandaTerimaDokumenReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPermohonantransferPrint", params)
        Return customclass
    End Function

    Public Function ListReportGesekanRangkaMesin(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        customclass.ListTandaTerimaDokumenReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGesekanRangkaMesinPrint", params)
        Return customclass
    End Function


#Region "ListTandaTerimaDokumen"
    Public Function ListTandaTerimaDokumen(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy

        customclass.ListTandaTerimaDokumen = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spTandaTerimaDokumenList", params).Tables(0)
        Return customclass
    End Function
#End Region

    Public Function ListPernyataanNPWP(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        customclass.ListTandaTerimaDokumen = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPernyataanNPWPList", params).Tables(0)
        Return customclass
    End Function

    Public Function ListPernyataanBedaTandaTangan(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        customclass.ListTandaTerimaDokumen = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPernyataanBedaTandaTanganList", params).Tables(0)
        Return customclass
    End Function

    Public Function ListSuratPenolakan(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        customclass.ListTandaTerimaDokumen = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSuratPenolakanList", params).Tables(0)
        Return customclass
    End Function

    Public Function ListSuratPernyataanNasabah(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        customclass.ListTandaTerimaDokumen = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "[spSuratPernyataanNasabahList]", params).Tables(0)
        Return customclass
    End Function

    Public Function ListBastk(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        customclass.ListTandaTerimaDokumen = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spBastkList", params).Tables(0)
        Return customclass
    End Function

    Public Function ListSuratPermohonanTransfer(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        customclass.ListTandaTerimaDokumen = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPermohonanTransferList", params).Tables(0)
        Return customclass
    End Function

    Public Function ListGesekanRangkaMesin(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        customclass.ListTandaTerimaDokumen = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGesekanRangkaMesinList", params).Tables(0)
        Return customclass
    End Function


    '============================= Kelengkapan Data Report ============================
#Region "ListReportKelengkapanData"
    Public Function ListReportKelengkapanData(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Dim SP As String = ""

        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond

        If customclass.Status = "C" Then
            If customclass.WhereCond2 = "1" Then
                SP = "spKelengkapanDataReportCompany"
            ElseIf customclass.WhereCond2 = "2" Then
                SP = "spKelengkapanDataSubReportCompany"
            ElseIf customclass.WhereCond2 = "4" Then
                SP = "spKelengkapanDataDokumen"
            Else
                SP = "spKelengkapanDataSubReportCompany3"
            End If
        Else
            If customclass.WhereCond2 = "1" Then
                SP = "spKelengkapanDataReport"
            ElseIf customclass.WhereCond2 = "2" Then
                SP = "spKelengkapanDataSubReport"
            ElseIf customclass.WhereCond2 = "4" Then
                SP = "spKelengkapanDataDokumen"
            Else
                SP = "spKelengkapanDataSubReport3"
            End If
        End If

        customclass.ListKelengkapanDataReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SP, params)
        Return customclass
    End Function
#End Region
#Region "ListKelengkapanData"
    Public Function ListKelengkapanData(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy

        customclass.ListKelengkapanData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spKelengkapanData", params).Tables(0)
        Return customclass
    End Function
#End Region

    '============================= Listing Surat Kontrak ============================
#Region "ListReportListingSuratKontrak"
    Public Function ListReportListingSuratKontrak(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond

        customclass.ListListingSuratKontrakReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spListingSuratKontrakReport", params)
        Return customclass
    End Function
#End Region
#Region "ListListingSuratKontrak"
    Public Function ListListingSuratKontrak(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy

        customclass.ListListingSuratKontrak = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spListingSuratKontrak", params).Tables(0)
        Return customclass
    End Function
#End Region
#Region "GetCMO"
    Public Function GetCMO(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.Branch_ID
        customclass.ListListingSuratKontrakReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGetCMO", params)
        Return customclass
    End Function
#End Region

    '===================== Print Report OM================================
#Region "ListReportOM"
    Public Function ListReportOM(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@Month", SqlDbType.VarChar, 12)
        params(0).Value = customclass.Month
        params(1) = New SqlParameter("@Year", SqlDbType.Char, 4)
        params(1).Value = customclass.Year

        customclass.ListReportOM = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spReportOM", params)
        Return customclass
    End Function
#End Region

    Public Function ListPrintBASTK(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(3) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@MultiApplicationID", SqlDbType.VarChar, 1000)
        params(1).Value = customclass.MultiApplicationID
        params(2) = New SqlParameter("@MultiAgreementNo", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.MultiAgreementNo
        params(3) = New SqlParameter("@Branch_ID", SqlDbType.Char, 3)
        params(3).Value = customclass.Branch_ID

        customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, _
        "spRptBASTK", params)
        Return customclass
    End Function

    '============================= Persetujuan Pengalihan Kreditor Report ============================
#Region "ListReportPPKreditor"
    Public Function ListReportPPKreditor(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond

        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPPKreditorPrint", params)
        Return customclass
    End Function
#End Region
#Region "ListPPKreditor"
    Public Function ListPPKreditor(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oReturnValue As New Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPPKreditorList", params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region
#Region "Save Print PPKreditor"
    Public Function SavePrintPPKreditor(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(5) {}
            oDataTable = customclass.ListPPKreditor
            For intloop = 0 To oDataTable.Rows.Count - 1

                params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                params(2).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(3) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params(3).Value = customclass.BranchId

                params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                params(4).Value = customclass.BusinessDate

                params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                params(5).Value = customclass.LoginId

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, "spPPKreditorSavePrint", params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
#End Region

    '============================= Welcome LetterReport ============================
#Region "ListReportWLetter"
    Public Function ListReportWLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.ApplicationID

        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spWelcomeLetterPrint", params)
        Return customclass
    End Function
#End Region
#Region "ListWLetter"
    Public Function ListWLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oReturnValue As New Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spWelcomeLetterList", params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region
#Region "Save Print WLetter"
    Public Function SavePrintWLetter(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(5) {}
            oDataTable = customclass.ListWLetter
            For intloop = 0 To oDataTable.Rows.Count - 1

                params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                params(2).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(3) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params(3).Value = customclass.BranchId

                params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                params(4).Value = customclass.BusinessDate

                params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                params(5).Value = customclass.LoginId

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, "spWelcomeLetterSavePrint", params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
#End Region
#Region "Save Print WLetter"
    Public Function SavePrintSuratPernyataanNasabah(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(5) {}
            oDataTable = customclass.ListWLetter
            For intloop = 0 To oDataTable.Rows.Count - 1

                params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                params(2).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(3) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params(3).Value = customclass.BranchId

                params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                params(4).Value = customclass.BusinessDate

                params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                params(5).Value = customclass.LoginId

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, "spSuratPernyataanNasabahSavePrint", params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
#End Region
#Region "ListReportWLetterSubReport"
    Public Function ListReportWLetterSubRpt(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customclass.ApplicationID

        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spWelcomeLetterSubRpt", params)
        Return customclass
    End Function
#End Region

    '============================= Report Surat Kuasa Ekseskusi dan Penjualan Obyek Jaminan Fidusia======================
#Region "ListReportSKEPO"
    Public Function ListReportSKEPO(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond

        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSKEPOPrint", params)
        Return customclass
    End Function
#End Region
#Region "ListSKEPO"
    Public Function ListSKEPO(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oReturnValue As New Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSKEPOList", params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region
#Region "Save Print SKEPO"
    Public Function SavePrintSKEPO(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(5) {}
            oDataTable = customclass.ListSKEPO
            For intloop = 0 To oDataTable.Rows.Count - 1

                params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                params(2).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(3) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params(3).Value = customclass.BranchId

                params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                params(4).Value = customclass.BusinessDate

                params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                params(5).Value = customclass.LoginId

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, "spSKEPOSavePrint", params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
#End Region

    '============================== Print PO Karoseri ==============================
#Region "Save Print PO Karoseri"
    Public Function SavePrintPOKaroseri(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(4) {}
            oDataTable = customclass.ListPPK
            For intloop = 0 To oDataTable.Rows.Count - 1

                params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params(2).Value = customclass.BranchId

                params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                params(3).Value = customclass.BusinessDate

                params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                params(4).Value = customclass.LoginId

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePrintPOKaroseri, params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
#End Region
#Region "List PO Karoseri Report"
    Public Function ListReportPOKaroseri(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond


        customclass.ListAgreement = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spReportPrintPOKaroseri, params)
        Return customclass
    End Function
#End Region


    '============================= SK Potong Gaji ============================
#Region "ListReportSKPotongGaji"
    Public Function ListReportSKPotongGaji(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond

        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSKPotongGajiPrint", params)
        Return customclass
    End Function
#End Region
#Region "ListSKPotongGaji"
    Public Function ListSKPotongGaji(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oReturnValue As New Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSKPotongGajiList", params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region
#Region "Save Print SKPotongGaji"
    Public Function SavePrintSKPotongGaji(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(5) {}
            oDataTable = customclass.ListWLetter
            For intloop = 0 To oDataTable.Rows.Count - 1

                params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                params(2).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(3) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params(3).Value = customclass.BranchId

                params(4) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                params(4).Value = customclass.BusinessDate

                params(5) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                params(5).Value = customclass.LoginId

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, "spSKPotongGajiSavePrint", params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
#End Region
#Region "ListReportSKPotongGajiSubReport"
    Public Function ListReportSKPotongGajiSubRpt(ByVal customclass As Parameter.PPK) As Parameter.PPK
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.BranchId

        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSKPotongGajiSubRpt", params)
        Return customclass
    End Function
#End Region

End Class
