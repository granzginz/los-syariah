Imports System.Data.sqlClient
Imports Maxiloan.Framework.SQLEngine

Public Class Class1SQLEngine
    Private Const spListAdminFeeAllocation As String = "spListAdminFeeAllocation"

    Public Function listAdminFeeAllocation(ByVal customClass As Parameter.Class1Parameter) As Parameter.Class1Parameter
        customClass.listAdminFeeAllocation = SqlHelper.ExecuteDataset(customClass.strConnection, _
        CommandType.StoredProcedure, spListAdminFeeAllocation)

        Return customClass
    End Function
End Class
