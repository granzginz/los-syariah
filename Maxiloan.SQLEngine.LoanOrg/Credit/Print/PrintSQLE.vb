Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

Public Class PrintSQLE : Inherits DataAccessBase
    Dim spGetAgentFeeReceipt As String = "spGetAgentFeeReceipt"    
    Dim spSaveAgentFeeReceiptPrint As String = "spSaveAgentFeeReceiptPrint"
    Dim spGetCreditFundingReceipt As String = "spGetCreditFundingReceipt"
    Dim spSaveCreditFundingReceiptPrint As String = "spSaveCreditFundingReceiptPrint"
    Dim spGetSuratKuasaFiducia As String = "spGetSuratKuasaFiducia"
    Dim spSaveSuratKuasaFidusiaPrint As String = "spSaveSuratKuasaFidusiaPrint"

    Protected Const PARAM_MAILTRANSNO As String = "@MailTransNo"
    Protected Const PARAM_APPLICATIONID As String = "@ApplicationID"

    Public Function execGetAgentFeeReceipt(ByVal customClass As Parameter.AgentFeeReceipt) As Parameter.AgentFeeReceipt
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
        params(1).Value = customClass.SortBy

        customClass.Dataset = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, _
        spGetAgentFeeReceipt, params)

        Return customClass
    End Function

    Public Function execSaveAgentFeeReceiptPrint(ByVal customClass As Parameter.AgentFeeReceipt) As Parameter.AgentFeeReceipt
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customClass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer

        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(4) {}

            oDataTable = customClass.Datatable

            For intloop = 0 To oDataTable.Rows.Count - 1
                params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params(2).Value = customClass.BranchId

                params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                params(3).Value = customClass.BusinessDate

                params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                params(4).Value = customClass.LoginId

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSaveAgentFeeReceiptPrint, params)
            Next

            oTrans.Commit()
            customClass.Hasil = True

            Return customClass

        Catch ex As Exception
            customClass.ErrorMessage = ex.Message
            customClass.Hasil = False
            oTrans.Rollback()

            Return customClass

        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function

    Public Function execGetCreditFundingReceipt(ByVal customClass As Parameter.CreditFundingReceipt) As Parameter.CreditFundingReceipt
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
        params(1).Value = customClass.SortBy

        customClass.Dataset = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, _
        spGetCreditFundingReceipt, params)

        Return customClass
    End Function

    Public Function execSaveCreditFundingReceiptPrint(ByVal customClass As Parameter.CreditFundingReceipt) As Parameter.CreditFundingReceipt
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customClass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer

        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(4) {}

            oDataTable = customClass.Datatable

            For intloop = 0 To oDataTable.Rows.Count - 1
                params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params(2).Value = customClass.BranchId

                params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                params(3).Value = customClass.BusinessDate

                params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                params(4).Value = customClass.LoginId

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSaveCreditFundingReceiptPrint, params)
            Next

            oTrans.Commit()
            customClass.Hasil = True

            Return customClass

        Catch ex As Exception
            customClass.ErrorMessage = ex.Message
            customClass.Hasil = False
            oTrans.Rollback()

            Return customClass

        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
    Public Function execGetSuratKuasaFiducia(ByVal customClass As Parameter.SuratKuasaFiducia) As Parameter.SuratKuasaFiducia
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim SPString As String = ""

        If customClass.SPType.Trim = "C" Then
            SPString = "spGetSuratKuasaFiduciaCompany"
        Else
            SPString = "spGetSuratKuasaFiducia"
        End If

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
        params(1).Value = customClass.SortBy

        customClass.Dataset = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, _
        SPString, params)

        Return customClass
    End Function


    Public Function execSaveSuratKuasaFidusiaPrint(ByVal customClass As Parameter.SuratKuasaFiducia) As Parameter.SuratKuasaFiducia
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customClass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer

        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(4) {}

            oDataTable = customClass.Datatable

            For intloop = 0 To oDataTable.Rows.Count - 1
                params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params(2).Value = customClass.BranchId

                params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                params(3).Value = customClass.BusinessDate

                params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                params(4).Value = customClass.LoginId

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, _
                spSaveSuratKuasaFidusiaPrint, params)
            Next

            oTrans.Commit()
            customClass.Hasil = True

            Return customClass

        Catch ex As Exception
            customClass.ErrorMessage = ex.Message
            customClass.Hasil = False
            oTrans.Rollback()

            Return customClass

        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function

    Public Function getMKK(ByVal customClass As Parameter.Common) As DataSet
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = customClass.AppID
        params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(1).Value = customClass.BusinessDate
        
        Try
            Return SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, _
                    "spViewMKKAll", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try                
    End Function


    Public Function getMKKReport(ByVal customClass As Parameter.MKK) As Parameter.MKK
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim params1() As SqlParameter = New SqlParameter(1) {}
        Dim params2() As SqlParameter = New SqlParameter(1) {}
        Dim oMKK As New Parameter.MKK

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = customClass.AppID
        params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(1).Value = customClass.BusinessDate

        params1(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params1(0).Value = customClass.AppID
        params1(1) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params1(1).Value = customClass.CustomerID

        params2(0) = New SqlParameter("@ApprovalNo", SqlDbType.Char, 50)
        params2(0).Value = customClass.ApprovalNo
        params2(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params2(1).Value = customClass.BusinessDate

        Try
            oMKK.ds1 = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, _
                    "spViewMKK", params)
            oMKK.ds2 = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, _
                    "spViewMKKFasilitasSaatIni", params1)
            oMKK.ds3 = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, _
                    "dbo.spApprovalHistoryList", params2)

            Return oMKK
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function listAgreementMailForPrint(ByVal CustomClass As Parameter.AgreementList) As DataTable
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = CustomClass.WhereCond
        params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
        params(1).Value = CustomClass.SortBy

        Try
            Return SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, "spListAgreementMailForPrint", params).Tables(0)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function saveAgreementMailForPrint(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer

        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()

            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(4) {}
            oDataTable = customclass.ListData

            For intloop = 0 To oDataTable.Rows.Count - 1
                params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("AgreementNo")

                params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                params(1).Value = oDataTable.Rows(intloop)("ApplicationID")

                params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                params(2).Value = customclass.BranchId

                params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                params(3).Value = customclass.BusinessDate

                params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                params(4).Value = customclass.LoginId

                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, customclass.SPName, params)
            Next

            oTrans.Commit()
            customclass.Hasil = 1

            Return customclass

        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function

    Public Function printAgreementMailForPrint(ByVal customclass As Parameter.AgreementList) As Parameter.AgreementList
        Dim params() As SqlParameter = New SqlParameter(3) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 1000)
        params(1).Value = customclass.ApplicationID
        params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(2).Value = customclass.BranchId
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 1000)
        params(3).Value = customclass.SortBy

        Try
            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, customclass.SPName, params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

        Return customclass
    End Function
End Class
