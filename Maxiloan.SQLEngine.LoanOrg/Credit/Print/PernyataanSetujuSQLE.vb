Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine


Public Class PernyataanSetujuSQLE : Inherits DataAccessBase
    Private Const spListPernyataanSetuju As String = "spListPernyataanSetuju"
    Private Const spListPernyataanBPKBdalamProses As String = "spListPernyataanBPKBdalamProses"
    Private Const spListSPKurangdokumen As String = "spListSPKurangdokumen"

    Private Const spSavePrintPernyataanSetuju As String = "spSavePrintPernyataanSetuju"
    Private Const spSavePrintBPKBInProses As String = "spSavePrintBPKBInProses"
    Private Const spSavePrintSPKurangdokumen As String = "spSavePrintSPKurangdokumen"

    Protected Const PARAM_MAILTRANSNO As String = "@MailTransNo"
    Protected Const PARAM_APPLICATIONID As String = "@ApplicationID"

    Public Function listPernyataanSetuju(ByVal customClass As Parameter.PernyataanSetuju) As Parameter.PernyataanSetuju
        Dim params() As SqlParameter = New SqlParameter(1) {}

        If customClass.FormID = "RPTBPKBINPROSES" Then
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customClass.WhereCond
            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(1).Value = customClass.SortBy
            customClass.listPernyataanSetuju = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, _
            spListPernyataanBPKBdalamProses, params)

        ElseIf customClass.FormID = "SPKurangdokumen" Then
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customClass.WhereCond
            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(1).Value = customClass.SortBy
            customClass.listPernyataanSetuju = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, _
            spListSPKurangdokumen, params)

        Else
            params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(0).Value = customClass.WhereCond
            params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(1).Value = customClass.SortBy

            customClass.listPernyataanSetuju = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, _
            spListPernyataanSetuju, params)
        End If

        Return customClass
    End Function

    Public Function savePrintPernyataanSetuju(ByVal customClass As Parameter.PernyataanSetuju) As Parameter.PernyataanSetuju
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customClass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer

        If customClass.FormID = "RPTBPKBINPROSES" Then
            Try
                If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
                oTrans = oCOnnection.BeginTransaction
                Dim params() As SqlParameter = New SqlParameter(4) {}
                oDataTable = customClass.dtPernyataanSetuju
                For intloop = 0 To oDataTable.Rows.Count - 1
                    params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                    params(0).Value = oDataTable.Rows(intloop)("AgreementNo")
                    params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                    params(1).Value = oDataTable.Rows(intloop)("ApplicationID")
                    params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                    params(2).Value = customClass.BranchId
                    params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                    params(3).Value = customClass.BusinessDate
                    params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                    params(4).Value = customClass.LoginId
                    SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePrintBPKBInProses, params)
                Next
                oTrans.Commit()
                customClass.Hasil = True
                Return customClass
            Catch ex As Exception
                customClass.ErrorMessage = ex.Message
                customClass.Hasil = False
                oTrans.Rollback()
                Return customClass
            Finally
                If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
                oCOnnection.Dispose()
            End Try

        ElseIf customClass.FormID = "SPKurangdokumen" Then
            Try
                If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
                oTrans = oCOnnection.BeginTransaction
                Dim params() As SqlParameter = New SqlParameter(4) {}
                oDataTable = customClass.dtPernyataanSetuju
                For intloop = 0 To oDataTable.Rows.Count - 1
                    params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                    params(0).Value = oDataTable.Rows(intloop)("AgreementNo")
                    params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                    params(1).Value = oDataTable.Rows(intloop)("ApplicationID")
                    params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                    params(2).Value = customClass.BranchId
                    params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                    params(3).Value = customClass.BusinessDate
                    params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                    params(4).Value = customClass.LoginId
                    SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePrintSPKurangdokumen, params)
                Next
                oTrans.Commit()
                customClass.Hasil = True
                Return customClass
            Catch ex As Exception
                customClass.ErrorMessage = ex.Message
                customClass.Hasil = False
                oTrans.Rollback()
                Return customClass
            Finally
                If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
                oCOnnection.Dispose()
            End Try

        Else
            Try
                If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
                oTrans = oCOnnection.BeginTransaction
                Dim params() As SqlParameter = New SqlParameter(4) {}
                oDataTable = customClass.dtPernyataanSetuju

                For intloop = 0 To oDataTable.Rows.Count - 1
                    params(0) = New SqlParameter(PARAM_MAILTRANSNO, SqlDbType.Char, 20)
                    params(0).Value = oDataTable.Rows(intloop)("AgreementNo")
                    params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.Char, 20)
                    params(1).Value = oDataTable.Rows(intloop)("ApplicationID")
                    params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
                    params(2).Value = customClass.BranchId
                    params(3) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
                    params(3).Value = customClass.BusinessDate
                    params(4) = New SqlParameter(PARAM_LOGINID, SqlDbType.Char, 12)
                    params(4).Value = customClass.LoginId
                    SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, spSavePrintPernyataanSetuju, params)
                Next
                oTrans.Commit()
                customClass.Hasil = True
                Return customClass
            Catch ex As Exception
                customClass.ErrorMessage = ex.Message
                customClass.Hasil = False
                oTrans.Rollback()
                Return customClass
            Finally
                If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
                oCOnnection.Dispose()
            End Try
        End If
    End Function
End Class
