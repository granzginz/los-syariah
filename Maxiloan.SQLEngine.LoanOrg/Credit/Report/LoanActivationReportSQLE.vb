Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

Public Class LoanActivationReportSQLE : Inherits DataAccessBase
    Private Const spGetLoanActivationList As String = "spGetLoanActivationList"

    Public Function getLoanActivationList(ByVal customClass As Parameter.LoanActivationReport) As Parameter.LoanActivationReport
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
        params(1).Value = customClass.SortBy

        customClass.Dataset = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, _
        spGetLoanActivationList, params)

        Return customClass
    End Function

    Public Function getDailySalesReportList(ByVal customClass As Parameter.LoanActivationReport) As Parameter.LoanActivationReport
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
        params(1).Value = customClass.SortBy
        params(2) = New SqlParameter("@date", SqlDbType.VarChar, 10)
        params(2).Value = customClass.SelectedDate

        customClass.Dataset = SqlHelper.ExecuteDataset(customClass.strConnection, _
        CommandType.StoredProcedure, "spGetDailySalesReportList", params)

        Return customClass
    End Function

    Public Function getAgreementDownloadList(ByVal customClass As Parameter.LoanActivationReport) As Parameter.LoanActivationReport
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
        params(1).Value = customClass.SortBy

        customClass.Dataset = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, _
        "spGetAgreementDownloadList", params)

        Return customClass
    End Function
End Class
