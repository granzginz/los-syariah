
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter

#End Region

Public Class CustomerFacility : Inherits Maxiloan.SQLEngine.DataAccessBase

    Function customerPagingParam(ByVal oCustomClass As Parameter.CustomerFacility, ByRef totalRow As SqlParameter) As IList(Of SqlParameter)
        Dim params = New List(Of SqlParameter)

        params.Add(New SqlParameter("@CurrentPage", SqlDbType.Int) With {.Value = oCustomClass.CurrentPage})
        params.Add(New SqlParameter("@PageSize", SqlDbType.Int) With {.Value = oCustomClass.PageSize})
        params.Add(New SqlParameter("@WhereCond", SqlDbType.VarChar) With {.Value = oCustomClass.WhereCond})
        params.Add(New SqlParameter("@SortBy", SqlDbType.VarChar) With {.Value = oCustomClass.SortBy})
        totalRow = New SqlParameter("@TotalRecords", SqlDbType.Int) With {.Direction = ParameterDirection.Output}
        params.Add(totalRow)
        Return params

    End Function

    Public Function GetFacilityList(ByVal oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim totalRow = New SqlParameter
        Dim params = customerPagingParam(oCustomClass, totalRow)
        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetCustomerFacilityList", params.ToArray).Tables(0)
            oCustomClass.TotalRecord = CType(params(4).Value, Int64)
            Return oCustomClass
        Catch exp As Exception
            WriteException("Customer", "GetFacilityList", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.CustomerFacility.GetFacilityList")
        End Try
    End Function

    Public Function GetFacilityDetail(ByVal oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim oReturnValue As New Parameter.Customer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@NoFasilitas", SqlDbType.VarChar)
        params(0).Value = oCustomClass.NoFasilitas
        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetCustomerFacilityDetail", params.ToArray).Tables(0)
            If (oCustomClass.ListData.Rows.Count > 0) Then
                oCustomClass.NoFasilitas = oCustomClass.ListData.Rows(0).Item("NoFasilitas")
                oCustomClass.NamaFasilitas = oCustomClass.ListData.Rows(0).Item("NamaFasilitas")
                oCustomClass.CustomerID = oCustomClass.ListData.Rows(0).Item("CustomerID")
                oCustomClass.CustomerName = oCustomClass.ListData.Rows(0).Item("CustomerName")
                oCustomClass.FacilityStartDate = Format(IIf(String.IsNullOrEmpty(oCustomClass.ListData.Rows(0).Item("StartDate").ToString), "", oCustomClass.ListData.Rows(0).Item("StartDate")), "dd/MM/yyyy")
                oCustomClass.FacilityMaturityDate = Format(IIf(String.IsNullOrEmpty(oCustomClass.ListData.Rows(0).Item("MaturityDate").ToString), "", oCustomClass.ListData.Rows(0).Item("MaturityDate")), "dd/MM/yyyy")
                oCustomClass.DrawDownStartDate = Format(IIf(String.IsNullOrEmpty(oCustomClass.ListData.Rows(0).Item("DrawDownStartDate").ToString), "", oCustomClass.ListData.Rows(0).Item("StartDate")), "dd/MM/yyyy")
                oCustomClass.DrawDownMaturityDate = Format(IIf(String.IsNullOrEmpty(oCustomClass.ListData.Rows(0).Item("DrawDownMaturityDate").ToString), "", oCustomClass.ListData.Rows(0).Item("MaturityDate")), "dd/MM/yyyy")
                oCustomClass.FasilitasAmount = oCustomClass.ListData.Rows(0).Item("FasilitasAmount")
                oCustomClass.FasilitasType = oCustomClass.ListData.Rows(0).Item("FasilitiasType")
                oCustomClass.EffectiveRate = oCustomClass.ListData.Rows(0).Item("EffectiveRate")
                oCustomClass.Tenor = oCustomClass.ListData.Rows(0).Item("Tenor")
                oCustomClass.AdminFee = oCustomClass.ListData.Rows(0).Item("AdminFee")
                oCustomClass.CommitmentFee = oCustomClass.ListData.Rows(0).Item("CommitmentFee")
                oCustomClass.ProvisionFee = oCustomClass.ListData.Rows(0).Item("ProvisionFee")
                oCustomClass.NotaryFee = oCustomClass.ListData.Rows(0).Item("NotaryFee")
                oCustomClass.FasilitasBalance = oCustomClass.ListData.Rows(0).Item("FasilitasBalance")
                oCustomClass.Status = oCustomClass.ListData.Rows(0).Item("Status")
                oCustomClass.ApprovalBy = IIf(IsDBNull(oCustomClass.ListData.Rows(0).Item("ApprovalBy")), "", oCustomClass.ListData.Rows(0).Item("ApprovalBy"))
                oCustomClass.ApprovalDate = IIf(IsDBNull(oCustomClass.ListData.Rows(0).Item("ApprovalDate")), "", oCustomClass.ListData.Rows(0).Item("ApprovalDate"))
                oCustomClass.MinimumPencairan = oCustomClass.ListData.Rows(0).Item("MinimumPencairan")
                oCustomClass.BranchId = oCustomClass.ListData.Rows(0).Item("BranchID")
                oCustomClass.JenisPembiayaan = oCustomClass.ListData.Rows(0).Item("JenisPembiayaan")
                oCustomClass.KegiatanUsaha = oCustomClass.ListData.Rows(0).Item("KegiatanUsaha")
                oCustomClass.InstallmentScheme = oCustomClass.ListData.Rows(0).Item("InstallmentScheme")
                oCustomClass.AvailableAmount = oCustomClass.ListData.Rows(0).Item("AvailableAmount")
                oCustomClass.Retensi = oCustomClass.ListData.Rows(0).Item("Retensi")
                oCustomClass.ApplicationModule = oCustomClass.ListData.Rows(0).Item("ApplicationModule")
                'oCustomClass.FactoringInterest = oCustomClass.ListData.Rows(0).Item("EffectiveRate")
                oCustomClass.LateChargeRate = oCustomClass.ListData.Rows(0).Item("LateChargeRate")
                oCustomClass.MPPA = oCustomClass.ListData.Rows(0).Item("MPPA")
                oCustomClass.MPPB = oCustomClass.ListData.Rows(0).Item("MPPB")
                oCustomClass.MPPC = oCustomClass.ListData.Rows(0).Item("MPPC")
                oCustomClass.MPPD = oCustomClass.ListData.Rows(0).Item("MPPD")
                oCustomClass.MPPE = oCustomClass.ListData.Rows(0).Item("MPPE")
                oCustomClass.ApprovalNo = oCustomClass.ListData.Rows(0).Item("ApprovalNo")
                oCustomClass.FacilityId = oCustomClass.ListData.Rows(0).Item("FacilityId")
            End If
            Return oCustomClass
        Catch exp As Exception
            WriteException("Customer", "GetFacilityDetail", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.CustomerFacility.GetFacilityDetail")
        End Try
    End Function

    Public Function GetAddendumFacilityDetail(ByVal oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim oReturnValue As New Parameter.Customer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@AddendumNo", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.NoFasilitasAddendum
        Try
            oCustomClass.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetCustomerAddendumFacilityDetail", params.ToArray).Tables(0)
            If (oCustomClass.ListData.Rows.Count > 0) Then
                oCustomClass.NoFasilitas = oCustomClass.ListData.Rows(0).Item("NoFasilitas")
                oCustomClass.NoAddendum = oCustomClass.ListData.Rows(0).Item("AddendumNo")
                oCustomClass.NamaFasilitas = oCustomClass.ListData.Rows(0).Item("NamaFasilitas")
                oCustomClass.CustomerID = oCustomClass.ListData.Rows(0).Item("CustomerID")
                oCustomClass.CustomerName = oCustomClass.ListData.Rows(0).Item("CustomerName")
                oCustomClass.FacilityStartDate = Format(IIf(String.IsNullOrEmpty(oCustomClass.ListData.Rows(0).Item("StartDate").ToString), "", oCustomClass.ListData.Rows(0).Item("StartDate")), "dd/MM/yyyy")
                oCustomClass.FacilityMaturityDate = Format(IIf(String.IsNullOrEmpty(oCustomClass.ListData.Rows(0).Item("MaturityDate").ToString), "", oCustomClass.ListData.Rows(0).Item("MaturityDate")), "dd/MM/yyyy")
                oCustomClass.DrawDownStartDate = Format(IIf(String.IsNullOrEmpty(oCustomClass.ListData.Rows(0).Item("DrawDownStartDate").ToString), "", oCustomClass.ListData.Rows(0).Item("StartDate")), "dd/MM/yyyy")
                oCustomClass.DrawDownMaturityDate = Format(IIf(String.IsNullOrEmpty(oCustomClass.ListData.Rows(0).Item("DrawDownMaturityDate").ToString), "", oCustomClass.ListData.Rows(0).Item("MaturityDate")), "dd/MM/yyyy")
                oCustomClass.FasilitasAmount = oCustomClass.ListData.Rows(0).Item("FasilitasAmount")
                oCustomClass.FasilitasType = oCustomClass.ListData.Rows(0).Item("FasilitiasType")
                oCustomClass.EffectiveRate = oCustomClass.ListData.Rows(0).Item("EffectiveRate")
                oCustomClass.Tenor = oCustomClass.ListData.Rows(0).Item("Tenor")
                oCustomClass.AdminFee = oCustomClass.ListData.Rows(0).Item("AdminFee")
                oCustomClass.CommitmentFee = oCustomClass.ListData.Rows(0).Item("CommitmentFee")
                oCustomClass.ProvisionFee = oCustomClass.ListData.Rows(0).Item("ProvisionFee")
                oCustomClass.NotaryFee = oCustomClass.ListData.Rows(0).Item("NotaryFee")
                oCustomClass.FasilitasBalance = oCustomClass.ListData.Rows(0).Item("FasilitasBalance")
                oCustomClass.Status = oCustomClass.ListData.Rows(0).Item("Status")
                oCustomClass.ApprovalBy = IIf(IsDBNull(oCustomClass.ListData.Rows(0).Item("ApprovalBy")), "", oCustomClass.ListData.Rows(0).Item("ApprovalBy"))
                oCustomClass.ApprovalDate = IIf(IsDBNull(oCustomClass.ListData.Rows(0).Item("ApprovalDate")), "", oCustomClass.ListData.Rows(0).Item("ApprovalDate"))
                oCustomClass.MinimumPencairan = oCustomClass.ListData.Rows(0).Item("MinimumPencairan")
                oCustomClass.BranchId = oCustomClass.ListData.Rows(0).Item("BranchID")
                oCustomClass.JenisPembiayaan = oCustomClass.ListData.Rows(0).Item("JenisPembiayaan")
                oCustomClass.KegiatanUsaha = oCustomClass.ListData.Rows(0).Item("KegiatanUsaha")
                oCustomClass.InstallmentScheme = oCustomClass.ListData.Rows(0).Item("InstallmentScheme")
                oCustomClass.AvailableAmount = oCustomClass.ListData.Rows(0).Item("AvailableAmount")
                oCustomClass.Retensi = oCustomClass.ListData.Rows(0).Item("Retensi")
                oCustomClass.ApplicationModule = oCustomClass.ListData.Rows(0).Item("ApplicationModule")
                'oCustomClass.FactoringInterest = oCustomClass.ListData.Rows(0).Item("EffectiveRate")
                oCustomClass.LateChargeRate = oCustomClass.ListData.Rows(0).Item("LateChargeRate")
                oCustomClass.MPPA = oCustomClass.ListData.Rows(0).Item("MPPA")
                oCustomClass.MPPB = oCustomClass.ListData.Rows(0).Item("MPPB")
                oCustomClass.MPPC = oCustomClass.ListData.Rows(0).Item("MPPC")
                oCustomClass.MPPD = oCustomClass.ListData.Rows(0).Item("MPPD")
                oCustomClass.MPPE = oCustomClass.ListData.Rows(0).Item("MPPE")
                oCustomClass.ApprovalNo = oCustomClass.ListData.Rows(0).Item("ApprovalNo")
                oCustomClass.FacilityId = oCustomClass.ListData.Rows(0).Item("FacilityId")
            End If
            Return oCustomClass
        Catch exp As Exception
            WriteException("Customer", "GetAddendumFacilityDetail", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.CustomerFacility.GetAddendumFacilityDetail")
        End Try
    End Function

    Public Function CustomerFacilitySaveAdd(ByVal oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility

        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim params(35) As SqlParameter
        Dim NoFasilitas As String
        Dim IDFasilitas As String

        Dim par As IList(Of SqlParameter) = New List(Of SqlParameter)
        Dim oReturn As New Parameter.CustomerFacility
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            params(0) = New SqlParameter("@NoFasilitas", SqlDbType.Char, 20)
            params(0).Value = oCustomClass.NoFasilitas
            params(1) = New SqlParameter("@NamaFasilitas", SqlDbType.Char, 50)
            params(1).Value = oCustomClass.NamaFasilitas
            params(2) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params(2).Value = oCustomClass.CustomerID
            params(3) = New SqlParameter("@FacilityStartDate", SqlDbType.Char, 8)
            params(3).Value = oCustomClass.FacilityStartDate
            params(4) = New SqlParameter("@FacilityMaturityDate", SqlDbType.Char, 8)
            params(4).Value = oCustomClass.FacilityMaturityDate
            params(5) = New SqlParameter("@KegiatanUsaha", SqlDbType.Char, 1)
            params(5).Value = oCustomClass.KegiatanUsaha
            params(6) = New SqlParameter("@JenisPembiayaan", SqlDbType.Char, 2)
            params(6).Value = oCustomClass.JenisPembiayaan
            params(7) = New SqlParameter("@InstallmentScheme", SqlDbType.Char, 2)
            params(7).Value = oCustomClass.InstallmentScheme
            params(8) = New SqlParameter("@DrawDownStartDate", SqlDbType.Char, 8)
            params(8).Value = oCustomClass.DrawDownStartDate
            params(9) = New SqlParameter("@DrawDownMaturityDate", SqlDbType.Char, 8)
            params(9).Value = oCustomClass.DrawDownMaturityDate
            params(10) = New SqlParameter("@FasilitasAmount", SqlDbType.Float)
            params(10).Value = oCustomClass.FasilitasAmount
            params(11) = New SqlParameter("@DrawDownAmount", SqlDbType.Float)
            params(11).Value = oCustomClass.DrawDownAmount
            params(12) = New SqlParameter("@AvailableAmount", SqlDbType.Float)
            params(12).Value = oCustomClass.AvailableAmount
            params(13) = New SqlParameter("@MinimumPencairan", SqlDbType.Float)
            params(13).Value = oCustomClass.MinimumPencairan
            params(14) = New SqlParameter("@FasilitiasType", SqlDbType.Char, 1)
            params(14).Value = oCustomClass.FasilitasType
            params(15) = New SqlParameter("@EffectiveRate", SqlDbType.Decimal)
            params(15).Value = oCustomClass.EffectiveRate
            params(16) = New SqlParameter("@FlatRate", SqlDbType.Decimal)
            params(16).Value = oCustomClass.FlatRate
            params(17) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(17).Value = oCustomClass.Tenor
            params(18) = New SqlParameter("@AdminFee", SqlDbType.Float)
            params(18).Value = oCustomClass.AdminFee
            params(19) = New SqlParameter("@CommitmentFee", SqlDbType.Float)
            params(19).Value = oCustomClass.CommitmentFee
            params(20) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal)
            params(20).Value = oCustomClass.ProvisionFee
            params(21) = New SqlParameter("@NotaryFee", SqlDbType.Float)
            params(21).Value = oCustomClass.NotaryFee
            params(22) = New SqlParameter("@HandlingFee", SqlDbType.Decimal)
            params(22).Value = oCustomClass.HandlingFee
            params(23) = New SqlParameter("@RateAsuransiKredit", SqlDbType.Decimal)
            params(23).Value = oCustomClass.RateAsuransiKredit
            params(24) = New SqlParameter("@BiayaPolis", SqlDbType.Float)
            params(24).Value = oCustomClass.BiayaPolis
            params(25) = New SqlParameter("@UsrUpd", SqlDbType.Char, 20)
            params(25).Value = oCustomClass.LoginId
            params(26) = New SqlParameter("@Status", SqlDbType.Char, 1)
            params(26).Value = oCustomClass.Status
            params(27) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(27).Value = oCustomClass.BranchId
            params(28) = New SqlParameter("@Retensi", SqlDbType.Decimal)
            params(28).Value = oCustomClass.Retensi
            params(29) = New SqlParameter("@lateChargeRate", SqlDbType.Decimal)
            params(29).Value = oCustomClass.LateChargePersen
            params(30) = New SqlParameter("@ApplicationModule", SqlDbType.VarChar, 4)
            params(30).Value = oCustomClass.ApplicationModule
            params(31) = New SqlParameter("@MPPA", SqlDbType.Text)
            params(31).Value = oCustomClass.MPPA
            params(32) = New SqlParameter("@MPPB", SqlDbType.Text)
            params(32).Value = oCustomClass.MPPB
            params(33) = New SqlParameter("@MPPC", SqlDbType.Text)
            params(33).Value = oCustomClass.MPPC
            params(34) = New SqlParameter("@MPPD", SqlDbType.Text)
            params(34).Value = oCustomClass.MPPD
            params(35) = New SqlParameter("@FacilityId", SqlDbType.Char, 20)
            params(35).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCustomerFacilitySaveAdd", params.ToArray())

            'NoFasilitas = CType(params(19).Value, String)
            NoFasilitas = oCustomClass.NoFasilitas
            IDFasilitas = CType(params(35).Value, String)

            With oEntitiesApproval
                .ApprovalTransaction = transaction
                .BranchId = oCustomClass.BranchId
                .SchemeID = "RCA5"
                .RequestDate = oCustomClass.BusinessDate
                '.TransactionNo = NoFasilitas
                .TransactionNo = IDFasilitas.Trim
                .ApprovalNote = ""
                .ApprovalValue = oCustomClass.FasilitasAmount

                .UserRequest = oCustomClass.LoginId
                .UserApproval = oCustomClass.NextPersonApproval
                .AprovalType = Parameter.Approval.ETransactionType.RCA_Approval
                .Argumentasi = ""
            End With

            oCustomClass.ApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

            par.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = oCustomClass.BranchId})
            par.Add(New SqlParameter("@NoFasilitas", SqlDbType.Char, 20) With {.Value = NoFasilitas})
            par.Add(New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50) With {.Value = oCustomClass.ApprovalNo})
            par.Add(New SqlParameter("@BusinessDate", SqlDbType.DateTime) With {.Value = oCustomClass.BusinessDate})
            Dim eerParams = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            par.Add(eerParams)
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCustomerFacilitySaveAddProceed", par.ToArray)


            transaction.Commit()
            Return oReturn
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Customer", "CustomerFacilitySaveAdd", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.CustomerFacility.spCustomerFacilitySaveAdd")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function



    Function getApprovalSchemeID(cnn As String, appId As String) As String
        Dim par As IList(Of SqlParameter) = New List(Of SqlParameter)
        par.Add(New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20) With {.Value = appId})
        Dim reader = SqlHelper.ExecuteReader(cnn, CommandType.StoredProcedure, "GetAgreementSchemeID", par.ToArray)
        If reader.Read Then
            Return CType(reader("SchemeID"), String)
        End If

        Return ""
    End Function

    Public Function CustomerFacilityApprovalList(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim params(5) As SqlParameter
        With customclass
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = .SortBy
            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output
            params(5) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 20)
            'params(5).Value = .AppID
            params(5).Value = getReferenceDBName()

        End With
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spCustomerFacilityApprovalList", params).Tables(0)
            customclass.TotalRecord = CType(params(4).Value, Int16)
            Return customclass
        Catch exp As Exception
            WriteException("CustomerFacility", "CustomerFacilityApprovalList", exp.Message + exp.StackTrace)
            Return Nothing
        End Try
    End Function

    Public Function GetCboUserAprPCAll(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim oReturnValue As New Parameter.CustomerFacility

        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@ApprType", SqlDbType.VarChar, 20)
        params(0).Value = customclass.ApprovalSchemeID
        params(1) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
        'params(1).Value = customclass.AppID
        params(1).Value = getReferenceDBName()

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spApprovalListUser", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("CustomerFacility", "GetCboUserAprPCAll", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetCboUserApproval(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim oReturnValue As New Parameter.CustomerFacility

        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
        'params(0).Value = customclass.AppID
        params(0).Value = getReferenceDBName()
        params(1) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
        params(1).Value = customclass.LoginId

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGetCboUserApproval", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("CustomerFacility", "GetCboUserApproval", exp.Message + exp.StackTrace)
        End Try
    End Function

    Function CustFacApproveSaveToNextPerson(ByVal customclass As Parameter.CustomerFacility) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction

        If objConnection.State = ConnectionState.Closed Then objConnection.Open()
        objtrans = objConnection.BeginTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(13) {}
            params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.VarChar, 10)
            params(0).Value = customclass.ApprovalSchemeID
            params(1) = New SqlParameter("@NoFasilitas", SqlDbType.VarChar, 20)
            params(1).Value = customclass.NoFasilitas
            params(2) = New SqlParameter("@ApprovalResult", SqlDbType.VarChar, 1)
            params(2).Value = customclass.ApprovalResult
            params(3) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
            params(3).Value = customclass.BranchId
            params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(4).Value = customclass.BusinessDate
            params(5) = New SqlParameter("@ApprovalDate", SqlDbType.DateTime)
            params(5).Value = customclass.ApprovalDate
            params(6) = New SqlParameter("@notes", SqlDbType.VarChar, 1000)
            params(6).Value = customclass.Notes
            params(7) = New SqlParameter("@SecurityCode", SqlDbType.VarChar, 50)
            params(7).Value = customclass.SecurityCode
            params(8) = New SqlParameter("@UserApproval", SqlDbType.VarChar, 255)
            params(8).Value = customclass.UserApproval
            params(9) = New SqlParameter("@UserSecurityCode", SqlDbType.VarChar, 255)
            params(9).Value = customclass.UserSecurityCode
            params(10) = New SqlParameter("@IsEverRejected", SqlDbType.Bit)
            params(10).Value = customclass.IsEverRejected
            params(11) = New SqlParameter("@LoginID", SqlDbType.VarChar, 255)
            params(11).Value = customclass.LoginId
            params(12) = New SqlParameter("@NextPersonApproval", SqlDbType.VarChar, 255)
            params(12).Value = customclass.NextPersonApproval
            params(13) = New SqlParameter("@ipaddress", SqlDbType.VarChar, 255)
            params(13).Value = ""

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spApprovalRequestToNextPersonCustomerFacility", params.ToArray)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("CustomerFacility", "CustFacApproveSaveToNextPerson", exp.Message + exp.StackTrace)
            Throw New Exception("ERROR: Can Not Process Aprrove Customer Facility. " + exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Public Function CustFacApproveSave(ByVal customclass As Parameter.CustomerFacility) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(11) {}
            params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.VarChar, 10)
            params(0).Value = customclass.ApprovalSchemeID
            params(1) = New SqlParameter("@NoFasilitas", SqlDbType.VarChar, 20)
            params(1).Value = customclass.NoFasilitas
            params(2) = New SqlParameter("@ApprovalResult", SqlDbType.VarChar, 1)
            params(2).Value = customclass.ApprovalResult
            params(3) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
            params(3).Value = customclass.BranchId
            params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(4).Value = customclass.BusinessDate
            params(5) = New SqlParameter("@ApprovalDate", SqlDbType.DateTime)
            params(5).Value = customclass.ApprovalDate
            params(6) = New SqlParameter("@notes", SqlDbType.VarChar, 1000)
            params(6).Value = customclass.Notes
            params(7) = New SqlParameter("@SecurityCode", SqlDbType.VarChar, 50)
            params(7).Value = customclass.SecurityCode
            params(8) = New SqlParameter("@UserApproval", SqlDbType.VarChar, 255)
            params(8).Value = customclass.UserApproval
            params(9) = New SqlParameter("@UserSecurityCode", SqlDbType.VarChar, 255)
            params(9).Value = customclass.UserSecurityCode
            params(10) = New SqlParameter("@IsEverRejected", SqlDbType.Bit)
            params(10).Value = customclass.IsEverRejected
            params(11) = New SqlParameter("@LoginID", SqlDbType.VarChar, 255)
            params(11).Value = customclass.LoginId

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spApprovalSaveFinalCustomerFacility", params.ToArray)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("CustomerFacility ", "CustFacApproveSave", exp.Message + exp.StackTrace)
            Throw New Exception("ERROR: Can Not Process Approve Customer Facility. " + exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Function

    Public Function CustFacApproveisFinal(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim objread As SqlDataReader
        Dim params(3) As SqlParameter
        With customclass
            params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.VarChar, 50)
            params(0).Value = .ApprovalSchemeID
            params(1) = New SqlParameter("@NoFasilitas", SqlDbType.VarChar, 20)
            params(1).Value = .NoFasilitas
            params(2) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
            params(2).Value = .LoginId
            params(3) = New SqlParameter("@IsReRequest", SqlDbType.Bit)
            params(3).Value = .IsReRequest

        End With
        Try
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spCustFacApprovalScreenSetLimit", params)

            If Not IsNothing(objread) Then
                With customclass
                    If objread.Read Then
                        .IsFinal = CType(objread("IsFinal"), String)
                        .NextPersonApproval = CType(objread("NextPersonApproval"), String)
                    End If
                    objread.Close()
                End With
            End If

            Return customclass
        Catch exp As Exception
            WriteException("CustomerFacility", "CustFacApproveisFinal", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function CustFacApproveIsValidApproval(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim objread As SqlDataReader
        Dim params(1) As SqlParameter
        With customclass
            params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.VarChar, 50)
            params(0).Value = .ApprovalSchemeID
            params(1) = New SqlParameter("@NoFasilitas", SqlDbType.VarChar, 20)
            params(1).Value = .NoFasilitas
        End With
        Try
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spCusFacApprovalScreenIsValidApproval", params)

            If Not IsNothing(objread) Then
                With customclass
                    If objread.Read Then
                        .UserApproval = CType(objread("UserApproval"), String)
                    End If
                    objread.Close()
                End With
            End If

            Return customclass

        Catch exp As Exception
            WriteException("CustomerFacility", "CustFacApproveIsValidApproval", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AddendumCustomerFacilitySaveAdd(ByVal oCustomClass As Parameter.CustomerFacility) As Parameter.CustomerFacility

        Dim conn As New SqlConnection(oCustomClass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim ErrMessage As String = ""
        Dim params(33) As SqlParameter
        Dim NoFasilitas As String

        Dim par As IList(Of SqlParameter) = New List(Of SqlParameter)
        Dim oReturn As New Parameter.CustomerFacility
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            params(0) = New SqlParameter("@NoFasilitas", SqlDbType.Char, 20)
            params(0).Value = oCustomClass.NoFasilitas
            params(1) = New SqlParameter("@NamaFasilitas", SqlDbType.Char, 50)
            params(1).Value = oCustomClass.NamaFasilitas
            params(2) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params(2).Value = oCustomClass.CustomerID
            params(3) = New SqlParameter("@FacilityStartDate", SqlDbType.Char, 8)
            params(3).Value = oCustomClass.FacilityStartDate
            params(4) = New SqlParameter("@FacilityMaturityDate", SqlDbType.Char, 8)
            params(4).Value = oCustomClass.FacilityMaturityDate
            params(5) = New SqlParameter("@KegiatanUsaha", SqlDbType.Char, 1)
            params(5).Value = oCustomClass.KegiatanUsaha
            params(6) = New SqlParameter("@JenisPembiayaan", SqlDbType.Char, 2)
            params(6).Value = oCustomClass.JenisPembiayaan
            params(7) = New SqlParameter("@InstallmentScheme", SqlDbType.Char, 2)
            params(7).Value = oCustomClass.InstallmentScheme
            params(8) = New SqlParameter("@DrawDownStartDate", SqlDbType.Char, 8)
            params(8).Value = oCustomClass.DrawDownStartDate
            params(9) = New SqlParameter("@DrawDownMaturityDate", SqlDbType.Char, 8)
            params(9).Value = oCustomClass.DrawDownMaturityDate
            params(10) = New SqlParameter("@FasilitasAmount", SqlDbType.Float)
            params(10).Value = oCustomClass.FasilitasAmount
            params(11) = New SqlParameter("@DrawDownAmount", SqlDbType.Float)
            params(11).Value = oCustomClass.DrawDownAmount
            params(12) = New SqlParameter("@AvailableAmount", SqlDbType.Float)
            params(12).Value = oCustomClass.AvailableAmount
            params(13) = New SqlParameter("@MinimumPencairan", SqlDbType.Float)
            params(13).Value = oCustomClass.MinimumPencairan
            params(14) = New SqlParameter("@FasilitiasType", SqlDbType.Char, 1)
            params(14).Value = oCustomClass.FasilitasType
            params(15) = New SqlParameter("@EffectiveRate", SqlDbType.Decimal)
            params(15).Value = oCustomClass.EffectiveRate
            params(16) = New SqlParameter("@FlatRate", SqlDbType.Decimal)
            params(16).Value = oCustomClass.FlatRate
            params(17) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(17).Value = oCustomClass.Tenor
            params(18) = New SqlParameter("@AdminFee", SqlDbType.Float)
            params(18).Value = oCustomClass.AdminFee
            params(19) = New SqlParameter("@CommitmentFee", SqlDbType.Float)
            params(19).Value = oCustomClass.CommitmentFee
            params(20) = New SqlParameter("@ProvisionFee", SqlDbType.Decimal)
            params(20).Value = oCustomClass.ProvisionFee
            params(21) = New SqlParameter("@NotaryFee", SqlDbType.Float)
            params(21).Value = oCustomClass.NotaryFee
            params(22) = New SqlParameter("@HandlingFee", SqlDbType.Decimal)
            params(22).Value = oCustomClass.HandlingFee
            params(23) = New SqlParameter("@RateAsuransiKredit", SqlDbType.Decimal)
            params(23).Value = oCustomClass.RateAsuransiKredit
            params(24) = New SqlParameter("@BiayaPolis", SqlDbType.Float)
            params(24).Value = oCustomClass.BiayaPolis
            params(25) = New SqlParameter("@UsrUpd", SqlDbType.Char, 20)
            params(25).Value = oCustomClass.LoginId
            params(26) = New SqlParameter("@Status", SqlDbType.Char, 1)
            params(26).Value = oCustomClass.Status
            params(27) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(27).Value = oCustomClass.BranchId
            params(28) = New SqlParameter("@Retensi", SqlDbType.Decimal)
            params(28).Value = oCustomClass.Retensi
            params(29) = New SqlParameter("@lateChargeRate", SqlDbType.Decimal)
            params(29).Value = oCustomClass.LateChargePersen
            params(30) = New SqlParameter("@ApplicationModule", SqlDbType.VarChar, 4)
            params(30).Value = oCustomClass.ApplicationModule
            params(31) = New SqlParameter("@MPPE", SqlDbType.Text)
            params(31).Value = oCustomClass.MPPE
            params(32) = New SqlParameter("@AddendumNo", SqlDbType.Char, 20)
            params(32).Value = oCustomClass.NoAddendum
            params(33) = New SqlParameter("@FacilityId", SqlDbType.Char, 20)
            params(33).Value = oCustomClass.FacilityId

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spAddendumCustomerFacilitySaveAdd", params.ToArray())

            'NoFasilitas = CType(params(19).Value, String)
            NoFasilitas = oCustomClass.FacilityId

            With oEntitiesApproval
                .ApprovalTransaction = transaction
                .BranchId = oCustomClass.BranchId
                .SchemeID = "AFAC"
                .RequestDate = oCustomClass.BusinessDate
                .TransactionNo = NoFasilitas
                .ApprovalNote = ""

                .ApprovalValue = oCustomClass.FasilitasAmount

                .UserRequest = oCustomClass.LoginId
                .UserApproval = oCustomClass.NextPersonApproval
                .AprovalType = Parameter.Approval.ETransactionType.RCA_Approval
                .Argumentasi = ""
            End With

            oCustomClass.ApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)

            par.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = oCustomClass.BranchId})
            par.Add(New SqlParameter("@NoFasilitas", SqlDbType.Char, 20) With {.Value = NoFasilitas})
            par.Add(New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50) With {.Value = oCustomClass.ApprovalNo})
            par.Add(New SqlParameter("@BusinessDate", SqlDbType.DateTime) With {.Value = oCustomClass.BusinessDate})
            Dim eerParams = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            par.Add(eerParams)
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spCustomerFacilitySaveAddProceed", par.ToArray)


            transaction.Commit()
            Return oReturn
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Customer", "CustomerFacilitySaveAdd", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.AccAcq.CustomerFacility.spCustomerFacilitySaveAdd")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Public Function AddendumCustomerFacilityApprovalList(ByVal customclass As Parameter.CustomerFacility) As Parameter.CustomerFacility
        Dim params(5) As SqlParameter
        With customclass
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = .SortBy
            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output
            params(5) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 20)
            'params(5).Value = .AppID
            params(5).Value = getReferenceDBName()

        End With
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spAddendumCustomerFacilityApprovalList", params).Tables(0)
            customclass.TotalRecord = CType(params(4).Value, Int16)
            Return customclass
        Catch exp As Exception
            WriteException("CustomerFacility", "AddendumCustomerFacilityApprovalList", exp.Message + exp.StackTrace)
            Return Nothing
        End Try
    End Function

    Public Function AddendumCustFacApproveSave(ByVal customclass As Parameter.CustomerFacility) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(14) {}
            params(0) = New SqlParameter("@ApprovalTypeID", SqlDbType.VarChar, 10)
            params(0).Value = customclass.ApprovalSchemeID
            params(1) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
            params(1).Value = customclass.ApprovalNo
            params(2) = New SqlParameter("@ApprovalResult", SqlDbType.VarChar, 1)
            params(2).Value = customclass.ApprovalResult
            params(3) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
            params(3).Value = customclass.BranchId
            params(4) = New SqlParameter("@BusinessDate", SqlDbType.VarChar, 10)
            params(4).Value = customclass.BusinessDate
            params(5) = New SqlParameter("@ApprovalDate", SqlDbType.DateTime)
            params(5).Value = customclass.ApprovalDate
            params(6) = New SqlParameter("@notes", SqlDbType.VarChar, 1000)
            params(6).Value = customclass.Notes
            params(7) = New SqlParameter("@SecurityCode", SqlDbType.VarChar, 50)
            params(7).Value = customclass.SecurityCode
            params(8) = New SqlParameter("@UserApproval", SqlDbType.VarChar, 255)
            params(8).Value = customclass.UserApproval
            params(9) = New SqlParameter("@UserSecurityCode", SqlDbType.VarChar, 255)
            params(9).Value = customclass.UserSecurityCode
            params(10) = New SqlParameter("@IsEverRejected", SqlDbType.Bit)
            params(10).Value = customclass.IsEverRejected
            params(11) = New SqlParameter("@SchemeId", SqlDbType.VarChar, 50)
            params(11).Value = customclass.ApprovalSchemeID
            params(12) = New SqlParameter("@LoginID", SqlDbType.VarChar, 255)
            params(12).Value = customclass.LoginId
            params(13) = New SqlParameter("@ipaddress", SqlDbType.VarChar, 255)
            params(13).Value = ""
            params(14) = New SqlParameter("@AgreementNo", SqlDbType.VarChar, 255)
            params(14).Value = ""

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spApprovalSaveFinal", params.ToArray)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Addendum CustomerFacility ", "AddendumCustFacApproveSave", exp.Message + exp.StackTrace)
            Throw New Exception("ERROR: Can Not Process Approve Addendum Customer Facility. " + exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Function

    Function AddendumCustFacApproveSaveToNextPerson(ByVal customclass As Parameter.CustomerFacility) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction

        If objConnection.State = ConnectionState.Closed Then objConnection.Open()
        objtrans = objConnection.BeginTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(13) {}
            params(0) = New SqlParameter("@ApprovalSchemeID", SqlDbType.VarChar, 10)
            params(0).Value = customclass.ApprovalSchemeID
            params(1) = New SqlParameter("@NoFasilitas", SqlDbType.VarChar, 20)
            params(1).Value = customclass.NoFasilitas
            params(2) = New SqlParameter("@ApprovalResult", SqlDbType.VarChar, 1)
            params(2).Value = customclass.ApprovalResult
            params(3) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
            params(3).Value = customclass.BranchId
            params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(4).Value = customclass.BusinessDate
            params(5) = New SqlParameter("@ApprovalDate", SqlDbType.DateTime)
            params(5).Value = customclass.ApprovalDate
            params(6) = New SqlParameter("@notes", SqlDbType.VarChar, 1000)
            params(6).Value = customclass.Notes
            params(7) = New SqlParameter("@SecurityCode", SqlDbType.VarChar, 50)
            params(7).Value = customclass.SecurityCode
            params(8) = New SqlParameter("@UserApproval", SqlDbType.VarChar, 255)
            params(8).Value = customclass.UserApproval
            params(9) = New SqlParameter("@UserSecurityCode", SqlDbType.VarChar, 255)
            params(9).Value = customclass.UserSecurityCode
            params(10) = New SqlParameter("@IsEverRejected", SqlDbType.Bit)
            params(10).Value = customclass.IsEverRejected
            params(11) = New SqlParameter("@LoginID", SqlDbType.VarChar, 255)
            params(11).Value = customclass.LoginId
            params(12) = New SqlParameter("@NextPersonApproval", SqlDbType.VarChar, 255)
            params(12).Value = customclass.NextPersonApproval
            params(13) = New SqlParameter("@ipaddress", SqlDbType.VarChar, 255)
            params(13).Value = ""

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spApprovalRequestToNextPersonAddendumCustomerFacility", params.ToArray)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("AddendumCustomerFacility", "AddendumCustFacApproveSaveToNextPerson", exp.Message + exp.StackTrace)
            Throw New Exception("ERROR: Can Not Process Aprrove Addendum Customer Facility. " + exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function
End Class
