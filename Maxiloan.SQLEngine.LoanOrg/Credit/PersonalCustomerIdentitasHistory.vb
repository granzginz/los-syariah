﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class PersonalCustomerIdentitasHistory : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "

#End Region


    Public Function PersonalCustomerIdentitasHistorySaveAdd(ByVal ocustomClass As Parameter.Customer) As String        
        Dim ErrMessage As String = ""
        Dim params(14) As SqlParameter

        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CustomerID
        params(1) = New SqlParameter("@Name", SqlDbType.VarChar, 50)
        params(1).Value = ocustomClass.Name
        params(2) = New SqlParameter("@IDType", SqlDbType.VarChar, 10)
        params(2).Value = ocustomClass.IDType
        params(3) = New SqlParameter("@IDNumber", SqlDbType.VarChar, 25)
        params(3).Value = ocustomClass.IDNumber
        params(4) = New SqlParameter("@BirthPlace", SqlDbType.VarChar, 20)
        params(4).Value = ocustomClass.BirthPlace
        params(5) = New SqlParameter("@MotherName", SqlDbType.VarChar, 50)
        params(5).Value = ocustomClass.MotherName
        params(6) = New SqlParameter("@NamaSuamiIstri", SqlDbType.VarChar, 50)
        params(6).Value = ocustomClass.NamaSuamiIstri
        params(7) = New SqlParameter("@NOKK", SqlDbType.VarChar, 20)
        params(7).Value = ocustomClass.NoKK
        params(8) = New SqlParameter("@Notes", SqlDbType.VarChar, 255)
        params(8).Value = ocustomClass.Notes
        params(9) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(9).Direction = ParameterDirection.Output
        params(10) = New SqlParameter("@BirthDate", SqlDbType.DateTime)
        params(10).Value = ocustomClass.TglLahir
        params(11) = New SqlParameter("@IDExpiredDate", SqlDbType.DateTime)
        params(11).Value = ocustomClass.IDExpiredDate
        params(12) = New SqlParameter("@MaritalStatus", SqlDbType.VarChar, 10)
        params(12).Value = ocustomClass.MaritalStatus
        params(13) = New SqlParameter("@PersonalNPWP", SqlDbType.VarChar, 25)
        params(13).Value = ocustomClass.PersonalNPWP
        params(14) = New SqlParameter("@Gender", SqlDbType.Char, 1)
        params(14).Value = ocustomClass.Gender

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, "spPersonalCustomerIdentitasHistorySaveAdd", params)
            ErrMessage = params(9).Value.ToString
            If ErrMessage <> "" Then
                Return ErrMessage
            Else
                Return ""
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


End Class
