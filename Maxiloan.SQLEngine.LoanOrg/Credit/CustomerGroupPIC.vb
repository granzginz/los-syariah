﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class CustomerGroupPIC : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spPagingCustomerGroupPIC"
    Private Const LIST_BY_ID As String = "spCustomerGroupPICList"
    Private Const LIST_BY_CUSTGROUP_ID As String = "spCustomerGroupPICListCUSTGROUPID"
    Private Const LIST_ADD As String = "spCustomerGroupPICSaveAdd"
    Private Const LIST_UPDATE As String = "spCustomerGroupPICSaveEdit"
    Private Const LIST_DELETE As String = "spCustomerGroupPICDelete"
    Private Const LIST_REPORT As String = "spCustomerGroupPICReport"
#End Region

    Public Function GetCustomerGroupPIC(ByVal oCustomClass As Parameter.CustomerGroupPIC) As Parameter.CustomerGroupPIC
        Dim oReturnValue As New Parameter.CustomerGroupPIC
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerGroupPIC.GetCustomerGroupPIC")
        End Try
    End Function

    Public Function GetCustomerGroupPICReport(ByVal oCustomClass As Parameter.CustomerGroupPIC) As Parameter.CustomerGroupPIC
        Dim oReturnValue As New Parameter.CustomerGroupPIC
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CmdWhere", SqlDbType.VarChar, 1000)
        params(0).Value = oCustomClass.WhereCond
        Try
            oReturnValue.ListdataReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerGroupPIC.GetCustomerGroupPICReport")
        End Try
    End Function

    Public Function GetCustomerGroupPICList(ByVal ocustomClass As Parameter.CustomerGroupPIC) As Parameter.CustomerGroupPIC
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.CustomerGroupPIC
        params(0) = New SqlParameter("@Id", SqlDbType.Int, 4)
        params(0).Value = ocustomClass.ID
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.ID = reader("ID").ToString
                oReturnValue.NamaPIC = reader("NamaPIC").ToString
                oReturnValue.Alamat = reader("Alamat").ToString
                oReturnValue.RT = reader("RT").ToString
                oReturnValue.RW = reader("RW").ToString
                oReturnValue.Kelurahan = reader("Kelurahan").ToString
                oReturnValue.Kecamatan = reader("Kecamatan").ToString
                oReturnValue.Kota = reader("Kota").ToString
                oReturnValue.KodePos = reader("KodePos").ToString
                oReturnValue.AreaPhone1 = reader("AreaPhone1").ToString
                oReturnValue.Phone1 = reader("Phone1").ToString
                oReturnValue.AreaPhone2 = reader("AreaPhone2").ToString
                oReturnValue.Phone2 = reader("Phone2").ToString
                oReturnValue.AreaFax = reader("AreaFax").ToString
                oReturnValue.Fax = reader("Fax").ToString
                oReturnValue.NoHP = reader("NoHP").ToString
                oReturnValue.CustomerGroupID = reader("CustomerGroupID").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerGroupPIC.GetCustomerGroupPICList")
        End Try
    End Function

    Public Function GetCustomerGroupPICListByCustGroupID(ByVal ocustomClass As Parameter.CustomerGroupPIC) As Parameter.CustomerGroupPIC
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.CustomerGroupPIC
        params(0) = New SqlParameter("@CustomerGroupID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CustomerGroupID
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_CUSTGROUP_ID, params)
            If reader.Read Then
                oReturnValue.ID = reader("ID").ToString
                oReturnValue.NamaPIC = reader("NamaPIC").ToString
                oReturnValue.Alamat = reader("Alamat").ToString
                oReturnValue.RT = reader("RT").ToString
                oReturnValue.RW = reader("RW").ToString
                oReturnValue.Kelurahan = reader("Kelurahan").ToString
                oReturnValue.Kecamatan = reader("Kecamatan").ToString
                oReturnValue.Kota = reader("Kota").ToString
                oReturnValue.KodePos = reader("KodePos").ToString
                oReturnValue.AreaPhone1 = reader("AreaPhone1").ToString
                oReturnValue.Phone1 = reader("Phone1").ToString
                oReturnValue.AreaPhone2 = reader("AreaPhone2").ToString
                oReturnValue.Phone2 = reader("Phone2").ToString
                oReturnValue.AreaFax = reader("AreaFax").ToString
                oReturnValue.Fax = reader("Fax").ToString
                oReturnValue.NoHP = reader("NoHP").ToString
                oReturnValue.CustomerGroupID = reader("CustomerGroupID").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerGroupPIC.GetCustomerGroupPICList")
        End Try
    End Function

    Public Function CustomerGroupPICSaveAdd(ByVal ocustomClass As Parameter.CustomerGroupPIC) As String
        Dim ErrMessage As String = ""
        Dim params(17) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int, 4)
        params(0).Value = Convert.ToInt32(ocustomClass.ID)
        params(1) = New SqlParameter("@NamaPIC", SqlDbType.VarChar, 50)
        params(1).Value = ocustomClass.NamaPIC
        params(2) = New SqlParameter("@Alamat", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.Alamat
        params(3) = New SqlParameter("@RT", SqlDbType.Char, 3)
        params(3).Value = ocustomClass.RT
        params(4) = New SqlParameter("@RW", SqlDbType.Char, 3)
        params(4).Value = ocustomClass.RW
        params(5) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 50)
        params(5).Value = ocustomClass.Kelurahan
        params(6) = New SqlParameter("@Kota", SqlDbType.VarChar, 50)
        params(6).Value = ocustomClass.Kota
        params(7) = New SqlParameter("@KodePos", SqlDbType.Char, 5)
        params(7).Value = ocustomClass.KodePos
        params(8) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
        params(8).Value = ocustomClass.AreaPhone1
        params(9) = New SqlParameter("@Phone1", SqlDbType.VarChar, 15)
        params(9).Value = ocustomClass.Phone1
        params(10) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
        params(10).Value = ocustomClass.AreaPhone2
        params(11) = New SqlParameter("@Phone2", SqlDbType.VarChar, 15)
        params(11).Value = ocustomClass.Phone2
        params(12) = New SqlParameter("@AreaFax", SqlDbType.Char, 4)
        params(12).Value = ocustomClass.AreaFax
        params(13) = New SqlParameter("@Fax", SqlDbType.VarChar, 15)
        params(13).Value = ocustomClass.Fax
        params(14) = New SqlParameter("@NoHP", SqlDbType.VarChar, 15)
        params(14).Value = ocustomClass.NoHP
        params(15) = New SqlParameter("@CustomerGroupID", SqlDbType.Char, 20)
        params(15).Value = ocustomClass.CustomerGroupID
        params(16) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 50)
        params(16).Value = ocustomClass.Kecamatan
        params(17) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(17).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(17).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerGroupPIC.CustomerGroupPICSaveAdd")
        End Try
    End Function

    Public Sub CustomerGroupPICSaveEdit(ByVal ocustomClass As Parameter.CustomerGroupPIC)
        Dim params(16) As SqlParameter
        params(0) = New SqlParameter("@ID", SqlDbType.Int, 4)
        params(0).Value = Convert.ToInt32(ocustomClass.ID)
        params(1) = New SqlParameter("@NamaPIC", SqlDbType.VarChar, 50)
        params(1).Value = ocustomClass.NamaPIC
        params(2) = New SqlParameter("@Alamat", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.Alamat
        params(3) = New SqlParameter("@RT", SqlDbType.Char, 3)
        params(3).Value = ocustomClass.RT
        params(4) = New SqlParameter("@RW", SqlDbType.Char, 3)
        params(4).Value = ocustomClass.RW
        params(5) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 50)
        params(5).Value = ocustomClass.Kelurahan
        params(6) = New SqlParameter("@Kota", SqlDbType.VarChar, 50)
        params(6).Value = ocustomClass.Kota
        params(7) = New SqlParameter("@KodePos", SqlDbType.Char, 5)
        params(7).Value = ocustomClass.KodePos
        params(8) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
        params(8).Value = ocustomClass.AreaPhone1
        params(9) = New SqlParameter("@Phone1", SqlDbType.VarChar, 15)
        params(9).Value = ocustomClass.Phone1
        params(10) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
        params(10).Value = ocustomClass.AreaPhone2
        params(11) = New SqlParameter("@Phone2", SqlDbType.VarChar, 15)
        params(11).Value = ocustomClass.Phone2
        params(12) = New SqlParameter("@AreaFax", SqlDbType.Char, 4)
        params(12).Value = ocustomClass.AreaFax
        params(13) = New SqlParameter("@Fax", SqlDbType.VarChar, 15)
        params(13).Value = ocustomClass.Fax
        params(14) = New SqlParameter("@NoHP", SqlDbType.VarChar, 15)
        params(14).Value = ocustomClass.NoHP
        params(15) = New SqlParameter("@CustomerGroupID", SqlDbType.Char, 20)
        params(15).Value = ocustomClass.CustomerGroupID
        params(16) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 50)
        params(16).Value = ocustomClass.Kecamatan
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.CustomerGroupPIC.CustomerGroupPICSaveEdit")
        End Try
    End Sub

    Public Function CustomerGroupPICDelete(ByVal ocustomClass As Parameter.CustomerGroupPIC) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Int, 4)
        params(0).Value = ocustomClass.ID
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            'Throw New Exception("Error On DataAccess.Setting.CustomerGroupPIC.CustomerGroupPICDelete")
        End Try
    End Function
End Class
