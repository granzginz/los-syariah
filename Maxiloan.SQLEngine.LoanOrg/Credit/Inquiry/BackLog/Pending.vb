
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region
Public Class Pending : Inherits Maxiloan.SQLEngine.DataAccessBase
    Dim m_connection As SqlConnection
    Public Sub New()
        MyBase.New()
        m_connection = New SqlConnection(GetReferenceDataConnectionString)
    End Sub
    Public Function GetPendingDataEntry(ByVal customClass As Parameter.Pending) As Parameter.Pending
        Dim oReturnValue As New Parameter.Pending
        Dim params() As SqlParameter = New SqlParameter(6) {}
        Dim spName As String
        spName = customClass.Table

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        params(5) = New SqlParameter("@WhereCond2", SqlDbType.VarChar)
        params(5).Value = customClass.WhereCond2

        params(6) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(6).Value = customClass.BusinessDate
        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.AccAcq.Pending.GetPendingDataEntry")
        End Try
    End Function
    Public Function GetAO(ByVal customclass As Parameter.Pending) As DataTable
        Dim oData As New DataTable
        Try
            Dim par() As SqlParameter = New SqlParameter(0) {}

            par(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            par(0).Value = customclass.BranchId

            oData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPendingDataEntryAO", par).Tables(0)
            Return oData
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.AccAcq.Pending.GetAO")
        End Try
    End Function
    Public Function GetSupplier(ByVal customclass As Parameter.Pending) As DataTable
        Dim oData As New DataTable
        Try
            oData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPendingDataEntrySupplier").Tables(0)
            Return oData
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.AccAcq.Pending.GetSupplier")
        End Try
    End Function
    Public Function GetApproved(ByVal customclass As Parameter.Pending) As DataTable
        Dim oData As New DataTable
        Try
            oData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPendingApprovalApprovedBy").Tables(0)
            Return oData
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.AccAcq.Pending.GetApproved")
        End Try
    End Function
    Public Function GetApplicationPendingInquiry(ByVal customClass As Parameter.Pending) As Parameter.Pending
        Dim oReturnValue As New Parameter.Pending
        Dim params() As SqlParameter = New SqlParameter(5) {}                

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(5).Value = customClass.BusinessDate
        Try
            oReturnValue.listdata = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spApplicationPendingInquiryPaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
