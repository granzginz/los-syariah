

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class SalesWithCondition : Inherits DataAccessBase

#Region "Constanta"
    Private Const spListSales As String = "spSalesWithConditionPaging"
#End Region

#Region "List Sales with Condition"
    Public Function ListSalesWithCOndition(ByVal customclass As Parameter.SalesWithCondition) As Parameter.SalesWithCondition
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        customclass.ListSales = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListSales, params).Tables(0)
        customclass.TotalRecord = CInt(params(4).Value)
        Return customclass
    End Function
#End Region




    Public Function ListTboInquiry(cnn As String, currentPage As Integer, PageSize As Integer, branchid As String, where As String) As Parameter.SalesWithCondition
        Dim params = New List(Of SqlParameter)
        Dim customclass As New Parameter.SalesWithCondition
        Try
            params.Add(New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int) With {.Value = currentPage})
            params.Add(New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int) With {.Value = PageSize})
            params.Add(New SqlParameter("@branchid", SqlDbType.VarChar, 1000) With {.Value = branchid})
            params.Add(New SqlParameter("@WhereStr", SqlDbType.VarChar, 200) With {.Value = where})

            customclass.ListSales = SqlHelper.ExecuteDataset(cnn, CommandType.StoredProcedure, "spAgreementTcTboInqPaging", params.ToArray()).Tables(0)
            If customclass.ListSales.Rows.Count > 0 Then
                customclass.TotalRecord = customclass.ListSales.Rows(0)("totalRow")
            Else
                customclass.TotalRecord = 0
            End If


        Catch exp As Exception
            WriteException("ListTboInquiry", "ListInquiry", exp.Message + exp.StackTrace)
        End Try

        Return customclass
    End Function
End Class
