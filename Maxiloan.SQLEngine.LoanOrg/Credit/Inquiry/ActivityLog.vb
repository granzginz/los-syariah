

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class ActivityLog : Inherits DataAccessBase

    Private Const spListActivityUser As String = "spListActivityUser"
    Private Const spListActivity As String = "spActivityLogPaging"



#Region "List Activity User"
    Public Function ListActivityUser(ByVal customclass As Parameter.ActivityLog) As Parameter.ActivityLog

        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond
        Try
            customclass.ListActivity = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListActivityUser, params).Tables(0)
            Return customclass
        Catch ex As Exception
            Throw New Exception(ex.Message)
            'Throw New Exception("Error On DataAccess.Marketing.AssetTypeCategory.GetAssetTypeCategoryEdit")
        End Try
    End Function
#End Region

#Region "List Activity Log"
    Public Function ListActivity(ByVal customclass As Parameter.ActivityLog) As Parameter.ActivityLog
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        customclass.ListActivity = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListActivity, params).Tables(0)
        customclass.TotalRecord = CInt(params(4).Value)
        Return customclass
    End Function
#End Region

#Region "AgreementCancel"
    Public Function AgreementCancelRpt(ByVal customclass As Parameter.ActivityLog) As Parameter.ActivityLog
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter("@BRANCHID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@status", SqlDbType.VarChar, 10)
            params(1).Value = customclass.status

            params(2) = New SqlParameter("@DateFrom", SqlDbType.Date)
            params(2).Value = customclass.DateFrom

            params(3) = New SqlParameter("@DateTo", SqlDbType.Date)
            params(3).Value = customclass.DateTo

            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPagingAgrCancelRPT", params)
            Return customclass
        Catch exp As Exception
            WriteException("RALPrinting", "RALListReport", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
End Class
