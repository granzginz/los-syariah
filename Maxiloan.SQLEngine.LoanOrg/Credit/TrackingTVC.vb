﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class TrackingTVC : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spTrackingTVCPaging"
    Private Const LIST_UPDATE As String = "spAgreementTrackingTVCSaveEdit"
    Private Const LIST_SELECT_ALL As String = "spTrackingTVCComboList"
    Private Const LIST_SELECT_BY_APPLICATIONID As String = "spTrackingTVCList"
#End Region

    Public Function GetApplication(ByVal oCustomClass As Parameter.Application) As Parameter.Application
        Dim oReturnValue As New Parameter.Application
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub ApplicationSaveEdit(ByVal ocustomClass As Parameter.Application)
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.ApplicationID
        'params(1) = New SqlParameter("@TglKonfirmasiDealer", SqlDbType.Date)
        'params(1).Value = ocustomClass.TglKonfirmasiDealer
        params(1) = New SqlParameter("@TglRealisasiTtdKontrak", SqlDbType.Date)
        params(1).Value = ocustomClass.TglRealisasiTtdKontrak
        params(2) = New SqlParameter("@TglJanjiTtdKontrak", SqlDbType.Date)
        params(2).Value = ocustomClass.TglJanjiTtdKontrak
        params(3) = New SqlParameter("@StatusTVCDesc", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.StatusTVCDesc

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Function GetCombo(ByVal customclass As Parameter.Application) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, LIST_SELECT_ALL).Tables(0)
    End Function

    Public Function GetApplicatioList(ByVal ocustomClass As Parameter.Application) As Parameter.Application
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.Application
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.ApplicationID

        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT_BY_APPLICATIONID, params)
            If reader.Read Then
                oReturnValue.ApplicationID = ocustomClass.ApplicationID
                'oReturnValue.TglKonfirmasiDealer = CDate(reader("TglKonfirmasiDealer").ToString)
                oReturnValue.TglRealisasiTtdKontrak = CDate(reader("TglRealisasiTtdKontrak").ToString)
                oReturnValue.TglJanjiTtdKontrak = CDate(reader("TglJanjiTtdKontrak").ToString)
                oReturnValue.StatusTVCDesc = reader("StatusTVCDesc").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
