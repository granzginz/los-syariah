﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class FundingCriteria : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name        
    Private Const LIST_SELECT As String = "spFundingCriteriaPaging"
    Private Const LIST_BY_ID As String = "spFundingCriteriaView"
    Private Const LIST_ADD As String = "spFundingCriteriaSaveAdd"
    Private Const LIST_UPDATE As String = "spFundingCriteriaSaveEdit"
    Private Const LIST_DELETE As String = "spFundingCriteriaDelete"
    Private Const LIST_EXEC As String = "spCriteriaUpdate"
    Private Const LIST_SAVE_REQ As String = "spReqCriteria"
    Private Const LIST_BY_SQL As String = "spSQLListCriteria"
#End Region

    Public Function GetFundingCriteria(ByVal oCustomClass As Parameter.FundingCriteria) As Parameter.FundingCriteria
        Dim oReturnValue As New Parameter.FundingCriteria
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.FundingCriteria.GetFundingCriteria")
        End Try
    End Function

    Public Function GetFundingCriteriaList(ByVal ocustomClass As Parameter.FundingCriteria) As Parameter.FundingCriteria
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.FundingCriteria
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CriteriaID
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.CriteriaID = ocustomClass.CriteriaID
                oReturnValue.CriteriaDescription = reader("CriteriaDescription").ToString
                oReturnValue.CriteriaOption = reader("CriteriaOption").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.FundingCriteria.GetFundingCriteriaList")
        End Try
    End Function

    Public Function FundingCriteriaSaveAdd(ByVal ocustomClass As Parameter.FundingCriteria) As String
        Dim ErrMessage As String = ""
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@CriteriaID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CriteriaIDEdit
        params(1) = New SqlParameter("@CriteriaDescription", SqlDbType.VarChar, 50)
        params(1).Value = ocustomClass.CriteriaDescription
        params(2) = New SqlParameter("@CriteriaOption", SqlDbType.Char, 3)
        params(2).Value = ocustomClass.CriteriaOption

        params(3) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(3).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(3).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.FundingCriteria.FundingCriteriaSaveAdd")
        End Try
    End Function

    Public Sub FundingCriteriaSaveEdit(ByVal ocustomClass As Parameter.FundingCriteria)
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@CriteriaID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CriteriaID
        params(1) = New SqlParameter("@CriteriaDescription", SqlDbType.VarChar, 50)
        params(1).Value = ocustomClass.CriteriaDescription
        params(2) = New SqlParameter("@CriteriaOption", SqlDbType.Char, 3)
        params(2).Value = ocustomClass.CriteriaOption
        params(3) = New SqlParameter("@CriteriaIDEdit", SqlDbType.Char, 20)
        params(3).Value = ocustomClass.CriteriaIDEdit
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.FundingCriteria.FundingCriteriaSaveEdit")
        End Try
    End Sub

    Public Function FundingCriteriaDelete(ByVal ocustomClass As Parameter.FundingCriteria) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CriteriaID
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            'Throw New Exception("Error On DataAccess.Setting.FundingCriteria.FundingCriteriaDelete")
        End Try
    End Function
    Public Sub ExecQuery(ByVal ocustomClass As Parameter.FundingCriteria)
        Dim params(2) As SqlParameter

        params(0) = New SqlParameter("@Query", SqlDbType.VarChar, 1000)
        params(0).Value = ocustomClass.Query

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = ocustomClass.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = ocustomClass.FundingContractNo


        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_EXEC, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.FundingCriteria.FundingCriteriaExec")
        End Try
    End Sub
    Public Sub SaveReq(ByVal ocustomClass As Parameter.FundingCriteria)
        Dim params(2) As SqlParameter

        params(0) = New SqlParameter("@Notes", SqlDbType.VarChar, 1000)
        params(0).Value = ocustomClass.Req

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = ocustomClass.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = ocustomClass.FundingContractNo


        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_SAVE_REQ, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.FundingCriteria.FundingCriteriaSaveReq")
        End Try
    End Sub
    Public Function GetSQLList(ByVal ocustomClass As Parameter.FundingCriteria) As Parameter.FundingCriteria
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.FundingCriteria
        params(0) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 100)
        params(0).Value = ocustomClass.FundingContractNo

        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_SQL, params)
            If reader.Read Then
                oReturnValue.BankID = reader("BankID").ToString
                oReturnValue.FundingCoyId = reader("FundingCoyId").ToString
                oReturnValue.FundingContractNo = reader("FundingContractNo").ToString
                oReturnValue.SQLString = reader("SQLString").ToString
                oReturnValue.Notes = reader("Notes").ToString
                oReturnValue.Label = reader("Label").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.ContactBank.GetSQLList")
        End Try
    End Function
End Class
