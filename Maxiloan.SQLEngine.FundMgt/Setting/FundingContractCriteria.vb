﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports System.Linq
#End Region

Public Class FundingContractCriteria : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name        
    Private Const LIST_BY_Criteria As String = "spFundingContractCriteriaViewByCriteria"
    Private Const LIST_BY_ID As String = "spFundingContractCriteriaViewBy"
    Private Const LIST As String = "spFundingContractCriteriaView"
    Private Const LIST_ADD As String = "spFundingContractCriteriaSaveAdd"
    Private Const LIST_UPDATE As String = "spFundingContractCriteriaSaveEdit"
    Private Const LIST_DELETE As String = "spFundingContractCriteriaDelete"    
#End Region

    Public Function GetFundingContractCriteriaList(ByVal ocustomClass As Parameter.FundingContractCriteria) As Parameter.FundingContractCriteria
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.FundingContractCriteria
        Dim listParameter As New List(Of Parameter.FundingContractCriteria)

        Try

            If ocustomClass.FundingContractNo <> Nothing Then
                params(0) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
                params(0).Value = ocustomClass.FundingContractNo
                params(1) = New SqlParameter("@BankID", SqlDbType.Char, 5)
                params(1).Value = ocustomClass.BankID
                
                reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
                While reader.Read
                    oReturnValue = New Parameter.FundingContractCriteria
                    oReturnValue.FundingContractNo = ocustomClass.FundingContractNo
                    oReturnValue.BankID = ocustomClass.BankID
                    oReturnValue.CriteriaID = reader("CriteriaID").ToString
                    oReturnValue.CriteriaValue1 = reader("CriteriaValue1").ToString
                    oReturnValue.CriteriaValue2 = reader("CriteriaValue2").ToString
                    oReturnValue.CriteriaValueID = reader("CriteriaValueID").ToString
                    ListParameter.Add(oReturnValue)
                End While
            End If
            

            oReturnValue.Listdata = SqlHelper.ExecuteDataset(ocustomClass.strConnection, CommandType.StoredProcedure, LIST).Tables(0)
            oReturnValue.Listdata.Columns.Add("ChekPilih", GetType(Boolean))
            oReturnValue.Listdata.Columns.Add("No", GetType(String))
            oReturnValue.Listdata.Columns.Add("CriteriaValue1", GetType(String))
            oReturnValue.Listdata.Columns.Add("CriteriaValue2", GetType(String))
            oReturnValue.Listdata.Columns.Add("CriteriaValueID", GetType(String))

            If ListParameter.Count > 0 Then
                For i As Integer = 0 To oReturnValue.Listdata.Rows.Count - 1
                    Dim dr As DataRow = oReturnValue.Listdata.Rows(i)
                    Dim R = From c In listParameter _
                            Where c.CriteriaID.Trim = dr(0).ToString.Trim _
                            Select c.CriteriaID.Trim, c.CriteriaValue1, c.CriteriaValue2, c.CriteriaValueID

                    If R.Count > 0 Then
                        oReturnValue.Listdata.Rows(i)("ChekPilih") = "True"
                        oReturnValue.Listdata.Rows(i)("CriteriaValue1") = R(0).CriteriaValue1
                        oReturnValue.Listdata.Rows(i)("CriteriaValue2") = R(0).CriteriaValue2
                        oReturnValue.Listdata.Rows(i)("CriteriaValueID") = R(0).CriteriaValueID

                    Else
                        oReturnValue.Listdata.Rows(i)("ChekPilih") = "False"
                        oReturnValue.Listdata.Rows(i)("CriteriaValue1") = ""
                        oReturnValue.Listdata.Rows(i)("CriteriaValue2") = ""
                        oReturnValue.Listdata.Rows(i)("CriteriaValueID") = ""
                    End If
                Next
            Else
                For i As Integer = 0 To oReturnValue.Listdata.Rows.Count - 1
                    oReturnValue.Listdata.Rows(i)("ChekPilih") = "False"
                    oReturnValue.Listdata.Rows(i)("CriteriaValue1") = ""
                    oReturnValue.Listdata.Rows(i)("CriteriaValue2") = ""
                    oReturnValue.Listdata.Rows(i)("CriteriaValueID") = ""
                Next
            End If

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetFundingContractCriteriaListColl(ByVal ocustomClass As Parameter.FundingContractCriteria) As List(Of Parameter.FundingContractCriteria)
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.FundingContractCriteria
        Dim listParameter As New List(Of Parameter.FundingContractCriteria)

        Try
            If ocustomClass.FundingContractNo <> Nothing Then
                params(0) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
                params(0).Value = ocustomClass.FundingContractNo
                params(1) = New SqlParameter("@BankID", SqlDbType.Char, 5)
                params(1).Value = ocustomClass.BankID

                reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
                While reader.Read
                    oReturnValue = New Parameter.FundingContractCriteria
                    oReturnValue.FundingContractNo = ocustomClass.FundingContractNo
                    oReturnValue.BankID = ocustomClass.BankID
                    oReturnValue.FundingCoyID = ocustomClass.FundingCoyID
                    oReturnValue.CriteriaID = reader("CriteriaID").ToString
                    oReturnValue.CriteriaValue1 = reader("CriteriaValue1").ToString
                    oReturnValue.CriteriaValue2 = reader("CriteriaValue2").ToString
                    oReturnValue.CriteriaValueID = reader("CriteriaValueID").ToString
                    listParameter.Add(oReturnValue)
                End While
            End If
            Return listParameter
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.FundingContractCriteria.GetFundingContractCriteriaList")
        End Try
    End Function

    Public Function FundingContractCriteriaSaveAdd(ByVal ocustomClass As Parameter.FundingContractCriteria) As String
        Dim ErrMessage As String = ""
        Dim params(7) As SqlParameter
        params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.FundingCoyID
        params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(1).Value = ocustomClass.FundingContractNo
        params(2) = New SqlParameter("@CriteriaID", SqlDbType.Char, 20)
        params(2).Value = ocustomClass.CriteriaID
        params(3) = New SqlParameter("@BankID", SqlDbType.Char, 5)
        params(3).Value = ocustomClass.BankID
        params(4) = New SqlParameter("@CriteriaValue1", SqlDbType.VarChar, 50)
        params(4).Value = ocustomClass.CriteriaValue1
        params(5) = New SqlParameter("@CriteriaValue2", SqlDbType.VarChar, 50)
        params(5).Value = ocustomClass.CriteriaValue2
        params(6) = New SqlParameter("@CriteriaValueID", SqlDbType.Char, 20)
        params(6).Value = ocustomClass.CriteriaValueID
        

        params(7) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(7).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(6).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub FundingContractCriteriaSaveEdit(ByVal ocustomClass As Parameter.FundingContractCriteria)
        Dim params(6) As SqlParameter
        params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.FundingCoyID
        params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(1).Value = ocustomClass.FundingContractNo
        params(2) = New SqlParameter("@BankID", SqlDbType.Char, 5)
        params(2).Value = ocustomClass.BankID
        params(3) = New SqlParameter("@CriteriaID", SqlDbType.Char, 20)
        params(3).Value = ocustomClass.CriteriaID
        params(4) = New SqlParameter("@CriteriaValue1", SqlDbType.VarChar, 50)
        params(4).Value = ocustomClass.CriteriaValue1
        params(5) = New SqlParameter("@CriteriaValue2", SqlDbType.VarChar, 50)
        params(5).Value = ocustomClass.CriteriaValue2
        params(6) = New SqlParameter("@CriteriaValueID", SqlDbType.Char, 20)
        params(6).Value = ocustomClass.CriteriaValueID

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Function FundingContractCriteriaDelete(ByVal ocustomClass As Parameter.FundingContractCriteria) As String
        Dim Err As Integer
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
        params(0).Value = ocustomClass.FundingCoyID
        params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(1).Value = ocustomClass.FundingContractNo
        params(2) = New SqlParameter("@BankID", SqlDbType.Char, 5)
        params(2).Value = ocustomClass.BankID
        params(3) = New SqlParameter("@CriteriaID", SqlDbType.Char, 20)
        params(3).Value = ocustomClass.CriteriaID
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            'Throw New Exception("Error On DataAccess.Setting.FundingContractCriteria.FundingContractCriteriaDelete")
        End Try
    End Function

    Public Function GetCombo(ByVal customclass As Parameter.FundingContractCriteria) As DataTable
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CriteriaID", SqlDbType.VarChar, 20)
        params(0).Value = customclass.CriteriaID
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, LIST_BY_Criteria, params).Tables(0)
    End Function
End Class
