﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class FundingCriteriaValue : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name        
    Private Const LIST_SELECT As String = "spFundingCriteriaValuePaging"
    Private Const LIST_BY_ID As String = "spFundingCriteriaValueView"
    Private Const LIST_ADD As String = "spFundingCriteriaValueSaveAdd"
    Private Const LIST_UPDATE As String = "spFundingCriteriaValueSaveEdit"
    Private Const LIST_DELETE As String = "spFundingCriteriaValueDelete"

#End Region

    Public Function GetFundingCriteriaValue(ByVal oCustomClass As Parameter.FundingCriteriaValue) As Parameter.FundingCriteriaValue
        Dim oReturnValue As New Parameter.FundingCriteriaValue
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.FundingCriteriaValue.GetFundingCriteriaValue")
        End Try
    End Function

    Public Function GetFundingCriteriaValueList(ByVal ocustomClass As Parameter.FundingCriteriaValue) As Parameter.FundingCriteriaValue
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.FundingCriteriaValue
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CriteriaValueID
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.CriteriaValueID = ocustomClass.CriteriaValueID
                oReturnValue.CriteriaID = reader("CriteriaID").ToString
                oReturnValue.CriteriaDescription = reader("CriteriaDescription").ToString
                oReturnValue.CriteriaValue = reader("CriteriaValue").ToString
                oReturnValue.SQLData = reader("SQLData").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.FundingCriteriaValue.GetFundingCriteriaValueList")
        End Try
    End Function

    Public Function FundingCriteriaValueSaveAdd(ByVal ocustomClass As Parameter.FundingCriteriaValue) As String
        Dim ErrMessage As String = ""
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CriteriaID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CriteriaID
        params(1) = New SqlParameter("@CriteriaValueID", SqlDbType.Char, 20)
        params(1).Value = ocustomClass.CriteriaValueIDEdit
        params(2) = New SqlParameter("@CriteriaDescription", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.CriteriaDescription
        params(3) = New SqlParameter("@CriteriaValue", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.CriteriaValue
        params(4) = New SqlParameter("@SQLData", SqlDbType.VarChar)
        params(4).Value = ocustomClass.SQLData

        params(5) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(5).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(5).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.FundingCriteriaValue.FundingCriteriaValueSaveAdd")
        End Try
    End Function

    Public Sub FundingCriteriaValueSaveEdit(ByVal ocustomClass As Parameter.FundingCriteriaValue)
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CriteriaID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CriteriaID
        params(1) = New SqlParameter("@CriteriaValueID", SqlDbType.Char, 20)
        params(1).Value = ocustomClass.CriteriaValueID
        params(2) = New SqlParameter("@CriteriaDescription", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.CriteriaDescription
        params(3) = New SqlParameter("@CriteriaValue", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.CriteriaValue
        params(4) = New SqlParameter("@SQLData", SqlDbType.VarChar)
        params(4).Value = ocustomClass.SQLData
        params(5) = New SqlParameter("@CriteriaValueIDEdit", SqlDbType.Char, 20)
        params(5).Value = ocustomClass.CriteriaValueIDEdit
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.FundingCriteriaValue.FundingCriteriaValueSaveEdit")
        End Try
    End Sub

    Public Function FundingCriteriaValueDelete(ByVal ocustomClass As Parameter.FundingCriteriaValue) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CriteriaValueID
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            'Throw New Exception("Error On DataAccess.Setting.FundingCriteriaValue.FundingCriteriaValueDelete")
        End Try
    End Function

End Class
