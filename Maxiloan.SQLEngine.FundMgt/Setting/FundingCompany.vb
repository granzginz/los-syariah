

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region


Public Class FundingCompany : Inherits DataAccessBase
    Private Const spFUNDINGCOMPANY_LIST As String = "spFUNDINGCOMPANYLIST"
    Private Const spFUNDINGCOMPANY_ADD As String = "spFUNDINGCOMPANYADD"
    Private Const spFUNDINGCOMPANY_BYID As String = "spFundingCompanyByID"
    Private Const spFUNDINGCOMPANY_EDIT As String = "spFundingCOMPANYEdit"
    Private Const spFUNDINGCOMPANY_DEL As String = "spFundingCOMPANYDelete"
    Private Const spFUNDINGCOMPANY_Report As String = "spFUNDINGCOMPANYReport"

    Private Const spFUNDINGCONTRACT_ADD As String = "spFUNDINGCONTRACTADD"
    Private Const spFUNDINGCONTRACT_BYID As String = "spFundingContractByID"
    Private Const spFUNDINGCONTRACT_EDIT As String = "spFundingCONTRACTEdit"
    Private Const spFUNDINGCONTRACT_DEL As String = "spFundingCONTRACTDelete"

    Private Const spFUNDINGCONTRACTPLAFONDBRANCH_ADD As String = "spFUNDINGCONTRACTPlafondBranchADD"
    Private Const spFUNDINGCONTRACTPLAFONDBRANCH_BYID As String = "spFundingContractPlafondBranchByID"
    Private Const spFUNDINGCONTRACTPLAFONDBRANCH_EDIT As String = "spFundingCONTRACTPlafondBranchEdit"
    Private Const spFUNDINGCONTRACTPLAFONDBRANCH_DEL As String = "spFundingCONTRACTPlafondbranchDelete"

    Private Const spFUNDINGCONTRACTDOC_ADD As String = "spFUNDINGCONTRACTDOCADD"
    Private Const spFUNDINGCONTRACTDOC_DEL As String = "spFundingCONTRACTDOCDelete"

    Private Const spFUNDINGCONTRACTBATCH_ADD As String = "spFUNDINGCONTRACTBATCHADD"
    Private Const spFUNDINGCONTRACTBATCHINS_ADD As String = "spFUNDINGCONTRACTBATCHINSADD"
    Private Const spFUNDINGCONTRACTBATCH_EDIT As String = "spFUNDINGCONTRACTBATCHEDIT"

    Private Const spFUNDINGUPDATEAGREEMENT As String = "spFundingUpdateAgreementSelected"
    Private Const spFUNDINGUPDATEAGREEMENTEXECUTION As String = "spFundingUpdateAgreementExecution"
    Private Const spFUNDINGUPDATEAGREEMENTSECONDEXECUTION As String = "spFUNDINGUPDATEAGREEMENTSECONDEXECUTION"

    Private Const SPFUNDINGDROWDOWNRECEIVE As String = "spFundingDrowdownReceive"
    Private Const spPaymentOutInstallmentProcess As String = "spFundingPaymentOutInstallmentProcess"

    Private Const spGETCOMBO_INPUTBANK As String = "select BankId, ShortName as BankName from BANKMASTER Order by ShortName asc"
    Private Const spFundingPrepaymentBPKBReplacingProcess As String = "spFundingPrepaymentBPKBReplacingProcess"
    Private Const spFundingPrepaymentPaymentOutProcess As String = "spFundingPrepaymentPaymentOutProcess"
    Private Const spFundingPaymentOutFeesProcess As String = "spFundingPaymentOutFeesProcess"
    Private Const spFundingAddFundingAgreementInst As String = "spFundingAddFundingAgreementInst"
    Private Const spFundingRescheduleAgreementInstallment As String = "spFundingRescheduleAgreementInstallment"
    Private Const spFundingUpdateContractBatchInst As String = "spFundingUpdateContractBatchInst"
    Private Const spFundingUncheckAgreement As String = "spFundingUncheckAgreement"
    Private Const spFundingChangeBatchDateProcess As String = "spFundingChangeBatchDateProcess"
    Private Const spFundingReschedulePrepaymentAgreementInstallment As String = "spFundingReschedulePrepaymentAgreementInstallment"

    Private Const SPFUNDINGCONTRACTNEGCOVMNT_GET As String = "spGetFundContractNegCov"
    Private Const SPFUNDINGCONTRACTNEGCOVMNT_UPDATE As String = "spUpdateFundContractNegCov"
    Private Const spGETCOMBO_INPUTBANKBEBAN As String = "spGetAPBankBebanBungaPaymentAllocationList"
    Private Const spGETCOMBO_INPUTBANKADMINISTRASI As String = "spGetAPBankBebanAdministrasiPaymentAllocationList"
    Private Const spFundPrintSave_ADD As String = "spFundPrintSave"
    Private Const LIST_SELECT As String = "spFundingInquiryAgreementPledgeWillFinishNew"


#Region "FundingCompany"

#Region "FundingCompanyList"
    Public Function ListFundingCompany(ByVal customclass As Parameter.FundingCompany) As Parameter.FundingCompany

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spFUNDINGCOMPANY_LIST, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
        Catch ex As Exception

        End Try

        Return customclass
    End Function
#End Region

#Region " FundingCompanyAdd"
    Public Sub FundingCompanyAdd(ByVal oCompany As Parameter.FundingCompany, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)

        Dim params(25) As SqlParameter

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyID

        params(2) = New SqlParameter("@FundingCoyName", SqlDbType.VarChar, 50)
        params(2).Value = oCompany.FundingCoyName

        params(3) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
        params(3).Value = oClassAddress.Address

        params(4) = New SqlParameter("@RT", SqlDbType.Char, 3)
        params(4).Value = oClassAddress.RT
        params(5) = New SqlParameter("@RW", SqlDbType.Char, 3)
        params(5).Value = oClassAddress.RW

        params(6) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
        params(6).Value = oClassAddress.Kelurahan
        params(7) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
        params(7).Value = oClassAddress.Kecamatan
        params(8) = New SqlParameter("@City", SqlDbType.VarChar, 30)
        params(8).Value = oClassAddress.City
        params(9) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
        params(9).Value = oClassAddress.ZipCode
        params(10) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
        params(10).Value = oClassAddress.AreaPhone1
        params(11) = New SqlParameter("@Phone1", SqlDbType.Char, 15)
        params(11).Value = oClassAddress.Phone1
        params(12) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
        params(12).Value = oClassAddress.AreaPhone2
        params(13) = New SqlParameter("@Phone2", SqlDbType.Char, 15)
        params(13).Value = oClassAddress.Phone2

        params(14) = New SqlParameter("@AreaFax", SqlDbType.VarChar, 4)
        params(14).Value = oClassAddress.AreaFax
        params(15) = New SqlParameter("@Fax", SqlDbType.VarChar, 10)
        params(15).Value = oClassAddress.Fax

        params(16) = New SqlParameter("@ContactName", SqlDbType.VarChar, 50)
        params(16).Value = oClassPersonal.PersonName
        params(17) = New SqlParameter("@ContactJobTitle", SqlDbType.VarChar, 50)
        params(17).Value = oClassPersonal.PersonTitle
        params(18) = New SqlParameter("@ContactEmail", SqlDbType.VarChar, 30)
        params(18).Value = oClassPersonal.Email
        params(19) = New SqlParameter("@ContactMobilePhone", SqlDbType.VarChar, 20)
        params(19).Value = oClassPersonal.MobilePhone

        params(20) = New SqlParameter("@FundingCoBankBranchAccount", SqlDbType.VarChar, 30)
        params(20).Value = oCompany.FundingCoBankBranchAccount
        params(21) = New SqlParameter("@FundingCoBankAccountName", SqlDbType.VarChar, 50)
        params(21).Value = oCompany.FundingCoBankAccountName
        params(22) = New SqlParameter("@InterestCalculationOption", SqlDbType.Int)
        params(22).Value = oCompany.InterestCalculationOption

        'COA Funding
        params(23) = New SqlParameter("@COAFunding", SqlDbType.Char)
        params(23).Value = oCompany.COAFunding
        params(24) = New SqlParameter("@COAIntExp", SqlDbType.Char)
        params(24).Value = oCompany.COAIntExp
        params(25) = New SqlParameter("@COAPrepaid", SqlDbType.Char)
        params(25).Value = oCompany.COAPrepaid

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCOMPANY_ADD, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("A record already exists with the same primary key !")
            'Throw New Exception(ex.Message & "1")
        End Try


    End Sub
#End Region

#Region "FundingCompanybyID"
    Public Function ListFundingCompanyByID(ByVal customclass As Parameter.FundingCompany) As Parameter.FundingCompany

        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(0).Value = customclass.FundingCoyID


        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spFUNDINGCOMPANY_BYID, params).Tables(0)
        Catch ex As Exception

        End Try

        Return customclass
    End Function
#End Region

#Region " FundingCompanyEdit"
    Public Sub FundingCompanyEdit(ByVal oCompany As Parameter.FundingCompany, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)

        Dim params(25) As SqlParameter

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyID

        params(2) = New SqlParameter("@FundingCoyName", SqlDbType.VarChar, 50)
        params(2).Value = oCompany.FundingCoyName

        params(3) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
        params(3).Value = oClassAddress.Address

        params(4) = New SqlParameter("@RT", SqlDbType.Char, 3)
        params(4).Value = oClassAddress.RT
        params(5) = New SqlParameter("@RW", SqlDbType.Char, 3)
        params(5).Value = oClassAddress.RW

        params(6) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
        params(6).Value = oClassAddress.Kelurahan
        params(7) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
        params(7).Value = oClassAddress.Kecamatan
        params(8) = New SqlParameter("@City", SqlDbType.VarChar, 30)
        params(8).Value = oClassAddress.City
        params(9) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
        params(9).Value = oClassAddress.ZipCode
        params(10) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
        params(10).Value = oClassAddress.AreaPhone1
        params(11) = New SqlParameter("@Phone1", SqlDbType.Char, 15)
        params(11).Value = oClassAddress.Phone1
        params(12) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
        params(12).Value = oClassAddress.AreaPhone2
        params(13) = New SqlParameter("@Phone2", SqlDbType.Char, 15)
        params(13).Value = oClassAddress.Phone2

        params(14) = New SqlParameter("@AreaFax", SqlDbType.VarChar, 4)
        params(14).Value = oClassAddress.AreaFax
        params(15) = New SqlParameter("@Fax", SqlDbType.VarChar, 10)
        params(15).Value = oClassAddress.Fax

        params(16) = New SqlParameter("@ContactName", SqlDbType.VarChar, 50)
        params(16).Value = oClassPersonal.PersonName
        params(17) = New SqlParameter("@ContactJobTitle", SqlDbType.VarChar, 50)
        params(17).Value = oClassPersonal.PersonTitle
        params(18) = New SqlParameter("@ContactEmail", SqlDbType.VarChar, 30)
        params(18).Value = oClassPersonal.Email
        params(19) = New SqlParameter("@ContactMobilePhone", SqlDbType.VarChar, 20)
        params(19).Value = oClassPersonal.MobilePhone

        params(20) = New SqlParameter("@FundingCoBankBranchAccount", SqlDbType.VarChar, 30)
        params(20).Value = oCompany.FundingCoBankBranchAccount
        params(21) = New SqlParameter("@FundingCoBankAccountName", SqlDbType.VarChar, 50)
        params(21).Value = oCompany.FundingCoBankAccountName
        params(22) = New SqlParameter("@InterestCalculationOption", SqlDbType.Int)
        params(22).Value = oCompany.InterestCalculationOption

        'COA Funding
        params(23) = New SqlParameter("@COAFunding", SqlDbType.Char)
        params(23).Value = oCompany.COAFunding
        params(24) = New SqlParameter("@COAIntExp", SqlDbType.Char)
        params(24).Value = oCompany.COAIntExp
        params(25) = New SqlParameter("@COAPrepaid", SqlDbType.Char)
        params(25).Value = oCompany.COAPrepaid

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCOMPANY_EDIT, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("A record already exists with the same primary key !")
            Throw New Exception("Update Failed!")
        End Try


    End Sub
#End Region

#Region "FundingCompanyDelete"
    Public Sub FundingCompanyDelete(ByVal oCompany As Parameter.FundingCompany)

        Dim params(0) As SqlParameter

        With oCompany

            params(0) = New SqlParameter("@FundingCoyId", SqlDbType.Char, 20)
            params(0).Value = .FundingCoyID

        End With

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCOMPANY_DEL, params)
        Catch ex As Exception
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        End Try
    End Sub
#End Region

#Region "FundingCompanyReport"
    Public Function ListFundingCompanyReport(ByVal customclass As Parameter.FundingCompany) As Parameter.FundingCompany


        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spFUNDINGCOMPANY_Report).Tables(0)

        Catch ex As Exception

        End Try

        Return customclass
    End Function
#End Region

#End Region

#Region "FundingContract"

#Region "FundingContractAdd"

    Public Sub FundingContractAdd(ByVal oCompany As Parameter.FundingContract)

        Dim params(57) As SqlParameter

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@ContractName", SqlDbType.VarChar, 50)
        params(3).Value = oCompany.ContractName

        params(4) = New SqlParameter("@FlagCS", SqlDbType.Char, 1)
        params(4).Value = oCompany.FlagCS

        params(5) = New SqlParameter("@CurrencyID", SqlDbType.Char, 3)
        params(5).Value = oCompany.CurrencyID

        params(6) = New SqlParameter("@PlafondAmount", SqlDbType.Decimal)
        params(6).Value = oCompany.PlafondAmount

        params(7) = New SqlParameter("@ContractDate", SqlDbType.DateTime)
        params(7).Value = oCompany.ContractDate

        params(8) = New SqlParameter("@PeriodFrom", SqlDbType.DateTime)
        params(8).Value = oCompany.PeriodFrom

        params(9) = New SqlParameter("@PeriodTo", SqlDbType.DateTime)
        params(9).Value = oCompany.PeriodTo

        params(10) = New SqlParameter("@FacilityType", SqlDbType.Char, 1)
        params(10).Value = oCompany.FacilityType

        params(11) = New SqlParameter("@FinalMaturityDate", SqlDbType.DateTime)
        params(11).Value = oCompany.FinalMaturityDate

        params(12) = New SqlParameter("@EvaluationDate", SqlDbType.DateTime)
        params(12).Value = oCompany.EvaluationDate

        params(13) = New SqlParameter("@RateToFundingCoy", SqlDbType.Decimal)
        params(13).Value = oCompany.RateToFundingCoy

        params(14) = New SqlParameter("@InterestType", SqlDbType.Char, 1)
        params(14).Value = oCompany.InterestType

        params(15) = New SqlParameter("@InterestNotes", SqlDbType.VarChar, 50)
        params(15).Value = oCompany.InterestNotes

        params(16) = New SqlParameter("@LCPerDay", SqlDbType.Decimal)
        params(16).Value = oCompany.LCPerDay

        params(17) = New SqlParameter("@LCGracePeriod", SqlDbType.Int)
        params(17).Value = oCompany.LCGracePeriod

        params(18) = New SqlParameter("@FlagCoBranding", SqlDbType.Bit)
        params(18).Value = oCompany.FlagCoBranding

        params(19) = New SqlParameter("@FundingCoyPortion", SqlDbType.Decimal)
        params(19).Value = oCompany.FundingCoyPortion

        params(20) = New SqlParameter("@PrepaymentPenalty", SqlDbType.Decimal)
        params(20).Value = oCompany.PrepaymentPenalty

        params(21) = New SqlParameter("@PaymentScheme", SqlDbType.Char, 1)
        params(21).Value = oCompany.PaymentScheme

        params(22) = New SqlParameter("@RecourseType", SqlDbType.Char, 1)
        params(22).Value = oCompany.RecourseType

        params(23) = New SqlParameter("@AcqActiveStatus", SqlDbType.Bit)
        params(23).Value = oCompany.AcqActiveStatus

        params(24) = New SqlParameter("@NegCovDate", SqlDbType.DateTime)
        params(24).Value = oCompany.NegCovDate

        params(25) = New SqlParameter("@CashHoldBack", SqlDbType.Decimal)
        params(25).Value = oCompany.CashHoldBack

        params(26) = New SqlParameter("@ContractStatus", SqlDbType.Char, 1)
        params(26).Value = oCompany.ContractStatus

        params(27) = New SqlParameter("@FloatingStart", SqlDbType.DateTime)
        params(27).Value = oCompany.FloatingStart

        params(28) = New SqlParameter("@FacilityKind", SqlDbType.Char, 5)
        params(28).Value = oCompany.FacilityKind

        params(29) = New SqlParameter("@SecurityCoveragePercentage", SqlDbType.Decimal)
        params(29).Value = oCompany.SecurityCoveragePercentage

        params(30) = New SqlParameter("@SecurityCoverageType", SqlDbType.Char, 1)
        params(30).Value = oCompany.SecurityCoverageType

        params(31) = New SqlParameter("@SecurityType", SqlDbType.VarChar, 4)
        params(31).Value = oCompany.SecurityType

        params(32) = New SqlParameter("@BalanceSheetStatus", SqlDbType.Char, 1)
        params(32).Value = oCompany.BalanceSheetStatus

        params(33) = New SqlParameter("@ProvisionFeeAmount", SqlDbType.Decimal)
        params(33).Value = oCompany.ProvisionFeeAmount

        params(34) = New SqlParameter("@AdminFeePerAccount", SqlDbType.Decimal)
        params(34).Value = oCompany.AdminFeePerAccount

        params(35) = New SqlParameter("@AdminFeeFacility", SqlDbType.Decimal)
        params(35).Value = oCompany.AdminFeeFacility

        params(36) = New SqlParameter("@AdminFeePerDrawDown", SqlDbType.Decimal)
        params(36).Value = oCompany.AdminFeePerDrawDown

        params(37) = New SqlParameter("@CommitmentFee", SqlDbType.Decimal)
        params(37).Value = oCompany.CommitmentFee

        params(38) = New SqlParameter("@MaximumDateForDD", SqlDbType.Decimal)
        params(38).Value = oCompany.MaximumDateForDD

        params(39) = New SqlParameter("@PrepaymentType", SqlDbType.Char, 1)
        params(39).Value = oCompany.PrepaymentType

        params(40) = New SqlParameter("@AssetDocLocation", SqlDbType.Char, 1)
        params(40).Value = oCompany.AssetDocLocation

        params(41) = New SqlParameter("@CommitmentStatus", SqlDbType.Bit)
        params(41).Value = oCompany.CommitmentStatus

        params(42) = New SqlParameter("@Mirroring", SqlDbType.Bit)
        params(42).Value = oCompany.Mirroring

        params(43) = New SqlParameter("@PersenPokokHutang", SqlDbType.Decimal)
        params(43).Value = oCompany.persenPokokHutang

        params(44) = New SqlParameter("@NotaryName", SqlDbType.VarChar, 50)
        params(44).Value = oCompany.NotarisName

        params(45) = New SqlParameter("@TglJatuhTempoSpesifik", SqlDbType.SmallDateTime)
        params(45).Value = IIf(oCompany.TglJatuhTempoSpesifik = "01/01/1900", DBNull.Value, oCompany.TglJatuhTempoSpesifik)

        params(46) = New SqlParameter("@JumlahHaridlmBulan", SqlDbType.Char, 1)
        params(46).Value = oCompany.JumlahHaridlmBulan

        params(47) = New SqlParameter("@MinimumPencairan", SqlDbType.Decimal)
        params(47).Value = oCompany.MinimumPencairan

        params(48) = New SqlParameter("@BatasAngsuran", SqlDbType.Int)
        params(48).Value = oCompany.BatasAngsuran

        params(49) = New SqlParameter("@CaraBayar", SqlDbType.Char, 10)
        params(49).Value = oCompany.CaraBayar

        'COA Funding
        params(50) = New SqlParameter("@COAFunding", SqlDbType.Char, 25)
        params(50).Value = oCompany.COAFunding
        params(51) = New SqlParameter("@COAIntExp", SqlDbType.Char, 25)
        params(51).Value = oCompany.COAIntExp
        params(52) = New SqlParameter("@COAPrepaid", SqlDbType.Char, 25)
        params(52).Value = oCompany.COAPrepaid

        params(53) = New SqlParameter("@Pinalty", SqlDbType.Decimal)
        params(53).Value = oCompany.Pinalty

        params(54) = New SqlParameter("@LamaDlmBulan", SqlDbType.Int)
        params(54).Value = oCompany.PeriodeBulan

        params(55) = New SqlParameter("@Req", SqlDbType.VarChar, 8000)
        params(55).Value = oCompany.Req

        params(56) = New SqlParameter("@CoaBebanBunga", SqlDbType.VarChar, 50)
        params(56).Value = oCompany.CoaBeban

        params(57) = New SqlParameter("@CoaBebanAdministrasi", SqlDbType.VarChar, 50)
        params(57).Value = oCompany.CoaAdmin

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACT_ADD, params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try


    End Sub

#End Region

#Region "FundingSontractbyID"
    Public Function ListFundingContractByID(ByVal customclass As Parameter.FundingContract) As Parameter.FundingContract

        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(0).Value = customclass.FundingContractNo
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACT_BYID, params).Tables(0)
        Catch ex As Exception

        End Try

        Return customclass
    End Function
#End Region

#Region " FundingContractEdit"
    Public Sub FundingContractEdit(ByVal oCompany As Parameter.FundingContract)

        Dim params(57) As SqlParameter

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@ContractName", SqlDbType.VarChar, 50)
        params(3).Value = oCompany.ContractName

        params(4) = New SqlParameter("@FlagCS", SqlDbType.Char, 1)
        params(4).Value = oCompany.FlagCS

        params(5) = New SqlParameter("@CurrencyID", SqlDbType.Char, 3)
        params(5).Value = oCompany.CurrencyID

        params(6) = New SqlParameter("@PlafondAmount", SqlDbType.Decimal)
        params(6).Value = oCompany.PlafondAmount

        params(7) = New SqlParameter("@ContractDate", SqlDbType.DateTime)
        params(7).Value = oCompany.ContractDate

        params(8) = New SqlParameter("@PeriodFrom", SqlDbType.DateTime)
        params(8).Value = oCompany.PeriodFrom

        params(9) = New SqlParameter("@PeriodTo", SqlDbType.DateTime)
        params(9).Value = oCompany.PeriodTo

        params(10) = New SqlParameter("@FacilityType", SqlDbType.Char, 1)
        params(10).Value = oCompany.FacilityType

        params(11) = New SqlParameter("@FinalMaturityDate", SqlDbType.DateTime)
        params(11).Value = oCompany.FinalMaturityDate

        params(12) = New SqlParameter("@EvaluationDate", SqlDbType.DateTime)
        params(12).Value = oCompany.EvaluationDate

        params(13) = New SqlParameter("@RateToFundingCoy", SqlDbType.Decimal)
        params(13).Value = oCompany.RateToFundingCoy

        params(14) = New SqlParameter("@InterestType", SqlDbType.Char, 1)
        params(14).Value = oCompany.InterestType

        params(15) = New SqlParameter("@InterestNotes", SqlDbType.VarChar, 50)
        params(15).Value = oCompany.InterestNotes

        params(16) = New SqlParameter("@LCPerDay", SqlDbType.Decimal)
        params(16).Value = oCompany.LCPerDay

        params(17) = New SqlParameter("@LCGracePeriod", SqlDbType.Int)
        params(17).Value = oCompany.LCGracePeriod

        params(18) = New SqlParameter("@FlagCoBranding", SqlDbType.Bit)
        params(18).Value = oCompany.FlagCoBranding

        params(19) = New SqlParameter("@FundingCoyPortion", SqlDbType.Decimal, 12)
        params(19).Value = oCompany.FundingCoyPortion

        params(20) = New SqlParameter("@PrepaymentPenalty", SqlDbType.Decimal)
        params(20).Value = oCompany.PrepaymentPenalty

        params(21) = New SqlParameter("@PaymentScheme", SqlDbType.Char, 1)
        params(21).Value = oCompany.PaymentScheme

        params(22) = New SqlParameter("@RecourseType", SqlDbType.Char, 1)
        params(22).Value = oCompany.RecourseType

        params(23) = New SqlParameter("@AcqActiveStatus", SqlDbType.Bit)
        params(23).Value = oCompany.AcqActiveStatus

        params(24) = New SqlParameter("@NegCovDate", SqlDbType.DateTime)
        params(24).Value = oCompany.NegCovDate

        params(25) = New SqlParameter("@CashHoldBack", SqlDbType.Decimal)
        params(25).Value = oCompany.CashHoldBack

        params(26) = New SqlParameter("@ContractStatus", SqlDbType.Char, 1)
        params(26).Value = oCompany.ContractStatus

        params(27) = New SqlParameter("@FloatingStart", SqlDbType.DateTime)
        params(27).Value = oCompany.FloatingStart

        params(28) = New SqlParameter("@FacilityKind", SqlDbType.Char, 5)
        params(28).Value = oCompany.FacilityKind

        params(29) = New SqlParameter("@SecurityCoveragePercentage", SqlDbType.Decimal)
        params(29).Value = oCompany.SecurityCoveragePercentage

        params(30) = New SqlParameter("@SecurityCoverageType", SqlDbType.Char, 1)
        params(30).Value = oCompany.SecurityCoverageType

        params(31) = New SqlParameter("@SecurityType", SqlDbType.VarChar, 4)
        params(31).Value = oCompany.SecurityType

        params(32) = New SqlParameter("@BalanceSheetStatus", SqlDbType.Char, 1)
        params(32).Value = oCompany.BalanceSheetStatus

        params(33) = New SqlParameter("@ProvisionFeeAmount", SqlDbType.Decimal)
        params(33).Value = oCompany.ProvisionFeeAmount

        params(34) = New SqlParameter("@AdminFeePerAccount", SqlDbType.Decimal)
        params(34).Value = oCompany.AdminFeePerAccount

        params(35) = New SqlParameter("@AdminFeeFacility", SqlDbType.Decimal)
        params(35).Value = oCompany.AdminFeeFacility

        params(36) = New SqlParameter("@AdminFeePerDrawDown", SqlDbType.Decimal)
        params(36).Value = oCompany.AdminFeePerDrawDown

        params(37) = New SqlParameter("@CommitmentFee", SqlDbType.Decimal)
        params(37).Value = oCompany.CommitmentFee

        params(38) = New SqlParameter("@MaximumDateForDD", SqlDbType.Decimal)
        params(38).Value = oCompany.MaximumDateForDD

        params(39) = New SqlParameter("@PrepaymentType", SqlDbType.Char, 1)
        params(39).Value = oCompany.PrepaymentType

        params(40) = New SqlParameter("@AssetDocLocation", SqlDbType.Char, 1)
        params(40).Value = oCompany.AssetDocLocation

        params(41) = New SqlParameter("@CommitmentStatus", SqlDbType.Bit)
        params(41).Value = oCompany.CommitmentStatus

        params(42) = New SqlParameter("@Mirroring", SqlDbType.Bit)
        params(42).Value = oCompany.Mirroring

        params(43) = New SqlParameter("@PersenPokokHutang", SqlDbType.Decimal)
        params(43).Value = oCompany.persenPokokHutang

        params(44) = New SqlParameter("@NotaryName", SqlDbType.VarChar, 50)
        params(44).Value = oCompany.NotarisName

        params(45) = New SqlParameter("@TglJatuhTempoSpesifik", SqlDbType.SmallDateTime)
        params(45).Value = IIf(oCompany.TglJatuhTempoSpesifik = "01/01/1900", DBNull.Value, oCompany.TglJatuhTempoSpesifik)

        params(46) = New SqlParameter("@JumlahHaridlmBulan", SqlDbType.Char, 1)
        params(46).Value = oCompany.JumlahHaridlmBulan

        params(47) = New SqlParameter("@MinimumPencairan", SqlDbType.Decimal)
        params(47).Value = oCompany.MinimumPencairan

        params(48) = New SqlParameter("@BatasAngsuran", SqlDbType.Int)
        params(48).Value = oCompany.BatasAngsuran

        params(49) = New SqlParameter("@CaraBayar", SqlDbType.Char, 10)
        params(49).Value = oCompany.CaraBayar

        params(50) = New SqlParameter("@COAFunding", SqlDbType.Char, 25)
        params(50).Value = oCompany.COAFunding

        params(51) = New SqlParameter("@COAIntExp", SqlDbType.Char, 25)
        params(51).Value = oCompany.COAIntExp

        params(52) = New SqlParameter("@COAPrepaid", SqlDbType.Char, 25)
        params(52).Value = oCompany.COAPrepaid

        params(53) = New SqlParameter("@Pinalty", SqlDbType.Decimal)
        params(53).Value = oCompany.Pinalty

        params(54) = New SqlParameter("@LamaDlmBulan", SqlDbType.Int)
        params(54).Value = oCompany.PeriodeBulan

        params(55) = New SqlParameter("@Req", SqlDbType.VarChar, 8000)
        params(55).Value = oCompany.Req

        params(56) = New SqlParameter("@CoaBebanBunga", SqlDbType.VarChar, 50)
        params(56).Value = oCompany.CoaBeban

        params(57) = New SqlParameter("@CoaBebanAdministrasi", SqlDbType.VarChar, 50)
        params(57).Value = oCompany.CoaAdmin

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACT_EDIT, params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Sub
#End Region

#Region "FundingContractDelete"
    Public Sub FundingContractDelete(ByVal oCompany As Parameter.FundingContract)

        Dim params(0) As SqlParameter

        With oCompany

            params(0) = New SqlParameter("@FundingContractNo", SqlDbType.Char, 20)
            params(0).Value = .FundingContractNo

        End With

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACT_DEL, params)
        Catch ex As Exception
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        End Try
    End Sub
#End Region

#End Region

#Region "FundingContractPlafondBranch"

#Region "FundingContractAdd"

    Public Sub FundingContractPlafondBranchAdd(ByVal oCompany As Parameter.FundingContractBranch)

        Dim params(7) As SqlParameter

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyID

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(3).Value = oCompany.BranchId

        params(4) = New SqlParameter("@PlafondAmount", SqlDbType.Decimal)
        params(4).Value = oCompany.PlafondAmount

        params(5) = New SqlParameter("@BookAmount", SqlDbType.Decimal)
        params(5).Value = oCompany.BookAmount

        params(6) = New SqlParameter("@OSAmount", SqlDbType.Decimal)
        params(6).Value = oCompany.OSAmount

        params(7) = New SqlParameter("@AcqStatus", SqlDbType.VarChar, 1)
        params(7).Value = oCompany.AcqStatus

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTPLAFONDBRANCH_ADD, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("A record already exists with the same primary key !")
            Throw New Exception(ex.Message)
        End Try


    End Sub

#End Region

#Region "FundingContractbyID"
    Public Function ListFundingContractPlafondBranchByID(ByVal customclass As Parameter.FundingContractBranch) As Parameter.FundingContractBranch

        Dim params(1) As SqlParameter

        params(0) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(0).Value = customclass.FundingContractNo

        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = customclass.BranchId


        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTPLAFONDBRANCH_BYID, params).Tables(0)
        Catch ex As Exception

        End Try

        Return customclass
    End Function
#End Region

#Region " FundingContractEdit"
    Public Sub FundingContractPlafondBranchEdit(ByVal oCompany As Parameter.FundingContractBranch)

        Dim params(7) As SqlParameter

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyID

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(3).Value = oCompany.BranchId

        params(4) = New SqlParameter("@PlafondAmount", SqlDbType.Decimal)
        params(4).Value = oCompany.PlafondAmount

        params(5) = New SqlParameter("@BookAmount", SqlDbType.Decimal)
        params(5).Value = oCompany.BookAmount

        params(6) = New SqlParameter("@OSAmount", SqlDbType.Decimal)
        params(6).Value = oCompany.OSAmount

        params(7) = New SqlParameter("@AcqStatus", SqlDbType.VarChar, 1)
        params(7).Value = oCompany.AcqStatus

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTPLAFONDBRANCH_EDIT, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("A record already exists with the same primary key !")
            Throw New Exception(ex.Message)
        End Try


    End Sub
#End Region

#Region "FundingContractDelete"
    Public Sub FundingContractPlafondBranchDelete(ByVal oCompany As Parameter.FundingContractBranch)

        Dim params(1) As SqlParameter

        With oCompany

            params(0) = New SqlParameter("@FundingContractNo", SqlDbType.Char, 20)
            params(0).Value = .FundingContractNo

            params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(1).Value = .BranchId

        End With

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTPLAFONDBRANCH_DEL, params)
        Catch ex As Exception
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        End Try
    End Sub
#End Region

#End Region

#Region "FundingContractDoc"

#Region "FundingContractDocAdd"

    Public Sub FundingContractDocAdd(ByVal oCompany As Parameter.FundingContract)

        Dim params(4) As SqlParameter

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@FundingDocId", SqlDbType.Int)
        params(3).Value = oCompany.FundingDocId

        params(4) = New SqlParameter("@DocumentNote", SqlDbType.VarChar, 50)
        params(4).Value = oCompany.DocumentNote


        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTDOC_ADD, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("A record already exists with the same primary key !")
            Throw New Exception(ex.Message)
        End Try


    End Sub

#End Region

#Region "FundingContractDocDelete"
    Public Sub FundingContractDocDelete(ByVal oCompany As Parameter.FundingContract)

        Dim params(0) As SqlParameter

        With oCompany

            params(0) = New SqlParameter("@FundingContractNo", SqlDbType.Char, 20)
            params(0).Value = .FundingContractNo

        End With

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTDOC_DEL, params)
        Catch ex As Exception
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        End Try
    End Sub
#End Region

#End Region


#Region "FundingContractBatchAdd"

    Public Sub FundingContractBatchAdd(ByVal oCompany As Parameter.FundingContractBatch)

        Dim params() As SqlParameter = New SqlParameter(33) {}

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(3).Value = oCompany.FundingBatchNo

        params(4) = New SqlParameter("@BatchDate", SqlDbType.DateTime)
        params(4).Value = oCompany.BatchDate

        params(5) = New SqlParameter("@PrincipalAmtToFunCoy", SqlDbType.Decimal)
        params(5).Value = oCompany.PrincipalAmtToFunCoy

        params(6) = New SqlParameter("@InterestRate", SqlDbType.Decimal)
        params(6).Value = oCompany.InterestRate

        params(7) = New SqlParameter("@InterestType", SqlDbType.Char, 1)
        params(7).Value = oCompany.InterestType

        params(8) = New SqlParameter("@FinalMaturityDate", SqlDbType.DateTime)
        params(8).Value = oCompany.FinalMaturityDate

        params(9) = New SqlParameter("@PaymentScheme", SqlDbType.Char, 1)
        params(9).Value = oCompany.PaymentScheme

        params(10) = New SqlParameter("@ProvisionFeeAmount", SqlDbType.Decimal)
        params(10).Value = oCompany.ProvisionFeeAmount

        params(11) = New SqlParameter("@AdminAmount", SqlDbType.Decimal)
        params(11).Value = oCompany.AdminAmount

        params(12) = New SqlParameter("@CurrencyID", SqlDbType.Char, 3)
        params(12).Value = oCompany.CurrencyID

        params(13) = New SqlParameter("@ExchangeRate", SqlDbType.Decimal)
        params(13).Value = oCompany.ExchangeRate

        params(14) = New SqlParameter("@Tenor", SqlDbType.Int)
        params(14).Value = oCompany.Tenor

        params(15) = New SqlParameter("@InstallmentPeriod", SqlDbType.Char, 1)
        params(15).Value = oCompany.InstallmentPeriod

        params(16) = New SqlParameter("@InstallmentScheme", SqlDbType.Char, 1)
        params(16).Value = oCompany.InstallmentScheme

        params(17) = New SqlParameter("@ProposeDate", SqlDbType.DateTime)
        params(17).Value = oCompany.ProposeDate

        params(18) = New SqlParameter("@AragingDate", SqlDbType.DateTime)
        params(18).Value = oCompany.AragingDate

        params(19) = New SqlParameter("@RealizedDate", SqlDbType.DateTime)
        params(19).Value = oCompany.RealizedDate

        params(20) = New SqlParameter("@InstallmentDueDate", SqlDbType.DateTime)
        params(20).Value = oCompany.InstallmentDueDate

        params(21) = New SqlParameter("@AccProposedNum", SqlDbType.Int)
        params(21).Value = oCompany.AccProposedNum

        params(22) = New SqlParameter("@AccRealizedNum", SqlDbType.Int)
        params(22).Value = oCompany.AccRealizedNum

        params(23) = New SqlParameter("@OSAmtToFunCoy", SqlDbType.Decimal)
        params(23).Value = oCompany.OSAmtToFunCoy

        params(24) = New SqlParameter("@AssetDocLocation", SqlDbType.Char, 1)
        params(24).Value = oCompany.AssetDocLocation

        params(25) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
        params(25).Value = oCompany.BankAccountID

        params(26) = New SqlParameter("@FirstInstallment", SqlDbType.Char, 2)
        params(26).Value = oCompany.FirstInstallment

        params(27) = New SqlParameter("@IsFundingNTF", SqlDbType.SmallInt)
        params(27).Value = oCompany.isFundingNTF

        params(28) = New SqlParameter("@perhitunganBunga", SqlDbType.VarChar, 50)
        params(28).Value = oCompany.perhitunganBunga

        params(29) = New SqlParameter("@CaraBayar", SqlDbType.Char, 10)
        params(29).Value = oCompany.CaraBayar

        params(30) = New SqlParameter("@PeriodeBulan", SqlDbType.Int)
        params(30).Value = oCompany.PeriodeBulan

        params(31) = New SqlParameter("@PeriodFrom", SqlDbType.Date)
        params(31).Value = oCompany.PeriodFrom

        params(32) = New SqlParameter("@PeriodTo", SqlDbType.Date)
        params(32).Value = oCompany.PeriodTo

        params(33) = New SqlParameter("@BatchRate", SqlDbType.Structured)
        params(33).Value = oCompany.ListData2

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTBATCH_ADD, params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Sub

#End Region

#Region "FundingContractBatchInsAdd"

    Public Sub FundingContractBatchInsAdd(ByVal oCompany As Parameter.FundingContractBatch)

        Dim params() As SqlParameter = New SqlParameter(11) {}

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(3).Value = oCompany.FundingBatchNo

        params(4) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(4).Value = oCompany.InsSecNo

        params(5) = New SqlParameter("@DueDate", SqlDbType.DateTime)
        params(5).Value = oCompany.DueDate

        params(6) = New SqlParameter("@PrincipalAmount", SqlDbType.Money)
        params(6).Value = Math.Round(oCompany.PrincipalAmount, 2)

        params(7) = New SqlParameter("@InterestAmount", SqlDbType.Money)
        params(7).Value = Math.Round(oCompany.InterestAmount, 2)

        params(8) = New SqlParameter("@PrincipalPaidAmount", SqlDbType.Money)
        params(8).Value = Math.Round(oCompany.PrincipalPaidAmount, 2)

        params(9) = New SqlParameter("@InterestPaidAmount", SqlDbType.Money)
        params(9).Value = Math.Round(oCompany.InterestPaidAmount, 2)

        params(10) = New SqlParameter("@OSPrincipalAmount", SqlDbType.Money)
        params(10).Value = Math.Round(oCompany.OSPrincipalAmount, 2)

        params(11) = New SqlParameter("@OSInterestAmount", SqlDbType.Money)
        params(11).Value = Math.Round(oCompany.OSInterestAmount, 2)


        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTBATCHINS_ADD, params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

#End Region

#Region " FundingContractBatchEdit"
    Public Sub FundingContractBatchEdit(ByVal oCompany As Parameter.FundingContractBatch)

        Dim params() As SqlParameter = New SqlParameter(34) {}

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(3).Value = oCompany.FundingBatchNo

        params(4) = New SqlParameter("@BatchDate", SqlDbType.DateTime)
        params(4).Value = oCompany.BatchDate

        params(5) = New SqlParameter("@PrincipalAmtToFunCoy", SqlDbType.Decimal)
        params(5).Value = oCompany.PrincipalAmtToFunCoy

        params(6) = New SqlParameter("@InterestRate", SqlDbType.Decimal)
        params(6).Value = oCompany.InterestRate

        params(7) = New SqlParameter("@InterestType", SqlDbType.Char, 1)
        params(7).Value = oCompany.InterestType

        params(8) = New SqlParameter("@FinalMaturityDate", SqlDbType.DateTime)
        params(8).Value = oCompany.FinalMaturityDate

        params(9) = New SqlParameter("@PaymentScheme", SqlDbType.Char, 1)
        params(9).Value = oCompany.PaymentScheme

        params(10) = New SqlParameter("@ProvisionFeeAmount", SqlDbType.Decimal)
        params(10).Value = oCompany.ProvisionFeeAmount

        params(11) = New SqlParameter("@AdminAmount", SqlDbType.Decimal)
        params(11).Value = oCompany.AdminAmount

        params(12) = New SqlParameter("@CurrencyID", SqlDbType.Char, 3)
        params(12).Value = oCompany.CurrencyID

        params(13) = New SqlParameter("@ExchangeRate", SqlDbType.Decimal)
        params(13).Value = oCompany.ExchangeRate

        params(14) = New SqlParameter("@Tenor", SqlDbType.Int)
        params(14).Value = oCompany.Tenor

        params(15) = New SqlParameter("@InstallmentPeriod", SqlDbType.Char, 1)
        params(15).Value = oCompany.InstallmentPeriod

        params(16) = New SqlParameter("@InstallmentScheme", SqlDbType.Char, 1)
        params(16).Value = oCompany.InstallmentScheme

        params(17) = New SqlParameter("@ProposeDate", SqlDbType.DateTime)
        params(17).Value = oCompany.ProposeDate

        params(18) = New SqlParameter("@AragingDate", SqlDbType.DateTime)
        params(18).Value = oCompany.AragingDate

        params(19) = New SqlParameter("@RealizedDate", SqlDbType.DateTime)
        params(19).Value = oCompany.RealizedDate

        params(20) = New SqlParameter("@InstallmentDueDate", SqlDbType.DateTime)
        params(20).Value = oCompany.InstallmentDueDate

        params(21) = New SqlParameter("@AccProposedNum", SqlDbType.Int)
        params(21).Value = oCompany.AccProposedNum

        params(22) = New SqlParameter("@AccRealizedNum", SqlDbType.Int)
        params(22).Value = oCompany.AccRealizedNum

        params(23) = New SqlParameter("@OSAmtToFunCoy", SqlDbType.Decimal)
        params(23).Value = oCompany.OSAmtToFunCoy

        params(24) = New SqlParameter("@AssetDocLocation", SqlDbType.Char, 1)
        params(24).Value = oCompany.AssetDocLocation

        params(25) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
        params(25).Value = oCompany.BankAccountID

        params(26) = New SqlParameter("@FirstInstallment", SqlDbType.Char, 2)
        params(26).Value = oCompany.FirstInstallment

        params(27) = New SqlParameter("@IsFUndingNTF", SqlDbType.SmallInt)
        params(27).Value = oCompany.isFundingNTF

        params(28) = New SqlParameter("@perhitunganBunga", SqlDbType.VarChar, 50)
        params(28).Value = oCompany.perhitunganBunga

        params(29) = New SqlParameter("@CaraBayar", SqlDbType.Char, 10)
        params(29).Value = oCompany.CaraBayar

        params(30) = New SqlParameter("@FundingBatchNoUsed", SqlDbType.VarChar, 20)
        params(30).Value = oCompany.FundingBatchNoUsed

        params(31) = New SqlParameter("@PeriodeBulan", SqlDbType.Int)
        params(31).Value = oCompany.PeriodeBulan

        params(32) = New SqlParameter("@PeriodFrom", SqlDbType.Date)
        params(32).Value = oCompany.PeriodFrom

        params(33) = New SqlParameter("@PeriodTo", SqlDbType.Date)
        params(33).Value = oCompany.PeriodTo

        params(34) = New SqlParameter("@BatchRate", SqlDbType.Structured)
        params(34).Value = oCompany.ListData2

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGCONTRACTBATCH_EDIT, params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try


    End Sub
#End Region

#Region "FundingUpdateAgreementSelected"

    Public Sub FundingUpdateAgreementSelected(ByVal oCompany As Parameter.FundingContractBatch)

        Dim params(7) As SqlParameter

        params(0) = New SqlParameter("@ValidateAmount", SqlDbType.Decimal)
        params(0).Value = oCompany.PlafondAmount

        params(1) = New SqlParameter("@SecurityType", SqlDbType.Char, 1)
        params(1).Value = oCompany.SecurityType

        params(2) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(2).Value = oCompany.BranchId

        params(3) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(3).Value = oCompany.BankId

        params(4) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(4).Value = oCompany.FundingCoyId

        params(5) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(5).Value = oCompany.FundingContractNo

        params(6) = New SqlParameter("@FundingBatchID", SqlDbType.VarChar, 20)
        params(6).Value = oCompany.FundingBatchNo

        params(7) = New SqlParameter("@FundingBatchDate", SqlDbType.DateTime)
        params(7).Value = oCompany.BatchDate

        'params(8) = New SqlParameter("@GoliveDate", SqlDbType.DateTime)
        'params(8).Value = oCompany.GoliveDate

        'params(9) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
        'params(9).Value = oCompany.Tenor

        'params(10) = New SqlParameter("@NetDP", SqlDbType.Decimal)
        'params(10).Value = oCompany.NetDP

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGUPDATEAGREEMENT, params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

#End Region

#Region "FundingUpdateAgreementExecution"

    Public Sub FundingUpdateAgreementExecution(ByVal oCompany As Parameter.FundingContractBatch)

        Dim params(3) As SqlParameter


        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@FundingBatchID", SqlDbType.VarChar, 20)
        params(3).Value = oCompany.FundingBatchNo


        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGUPDATEAGREEMENTEXECUTION, params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub FundingUpdateAgreementSecondExecution(ByVal oCompany As Parameter.FundingContractBatch)

        Dim params(3) As SqlParameter


        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@FundingBatchID", SqlDbType.VarChar, 20)
        params(3).Value = oCompany.FundingBatchNo


        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, spFUNDINGUPDATEAGREEMENTSECONDEXECUTION, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("A record already exists with the same primary key !")
            Throw New Exception(ex.Message)
        End Try
    End Sub


#End Region


    Public Sub FundingAgreementSelect(ByVal customclass As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(3) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(0).Value = customclass.FundingCoyId

            params(1) = New SqlParameter("@FundingContractNO", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingContractNo

            params(2) = New SqlParameter("@FundingBatchNO", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingBatchNo


            params(3) = New SqlParameter("@FundingAgreementSelect", SqlDbType.Structured)
            params(3).Value = customclass.Listdata

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFundingAgreementSelect", params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "FundingUncheckAgreement", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub

    Public Sub FundingAgreementSelectFromUpload(ByVal customclass As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(4) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(0).Value = customclass.FundingCoyId

            params(1) = New SqlParameter("@FundingContractNO", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingContractNo

            params(2) = New SqlParameter("@FundingBatchNO", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingBatchNo

            params(3) = New SqlParameter("@FundingAgreementSelect", SqlDbType.Structured)
            params(3).Value = customclass.Listdata

            params(4) = New SqlParameter("@IsExcel", SqlDbType.Bit)
            params(4).Value = customclass.IsExcel

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFundingAgreementSelect", params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "FundingUncheckAgreement", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub

#Region "FundingContractNegCovMnt"
    Public Sub FundingAgreementUploadUpdateExcel(ByVal customclass As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(5) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(0).Value = customclass.FundingCoyId

            params(1) = New SqlParameter("@FundingContractNO", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingContractNo

            params(2) = New SqlParameter("@FundingBatchNO", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingBatchNo

            params(3) = New SqlParameter("@DueDate", SqlDbType.DateTime)
            params(3).Value = customclass.DueDate

            params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(4).Value = customclass.BusinessDate

            params(5) = New SqlParameter("@FundingAgreementSelect", SqlDbType.Structured)
            params(5).Value = customclass.Listdata



            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFundingAgreementUpdateByExcel", params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "spFundingAgreementUpdateByExcel", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
#Region "Function FundingContractNegCovGet"
    'Create By : Parang, june 11st 2004
    Public Function FundingContractNegCovGet(ByVal CustomClass As Parameter.FundingContract) As Parameter.FundingContract
        Dim oParams(1) As SqlParameter
        oParams(0) = New SqlParameter("@fudingCoyID", SqlDbType.VarChar, 20)
        oParams(0).Value = CustomClass.FundingCoyId
        oParams(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        oParams(1).Value = CustomClass.FundingContractNo
        Try
            CustomClass.ListData = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, SPFUNDINGCONTRACTNEGCOVMNT_GET, oParams).Tables(0)
        Catch ex As Exception

        End Try

        Return CustomClass
    End Function
#End Region

#Region "Sub FundingContractNegCovUpdate"
    'Create By : Parang, june 11st 2004
    Public Sub FundingContractNegCovUpdate(ByVal strcon As String, ByVal setField As String, ByVal WhereBy As String)
        Dim oParams(1) As SqlParameter
        oParams(0) = New SqlParameter("@cmdSet", SqlDbType.VarChar, 100)
        oParams(0).Value = setField
        oParams(1) = New SqlParameter("@cmdWhere", SqlDbType.VarChar, 100)
        oParams(1).Value = WhereBy
        Try
            SqlHelper.ExecuteNonQuery(strcon, CommandType.StoredProcedure, SPFUNDINGCONTRACTNEGCOVMNT_UPDATE, oParams)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
#End Region

#End Region

#Region " MISC"
    Public Function GetComboInputBank(ByVal strcon As String) As DataTable

        Dim customClass As New Parameter.BankMaster

        Try
            customClass.ListData = SqlHelper.ExecuteDataset(strcon, CommandType.Text, spGETCOMBO_INPUTBANK).Tables(0)
        Catch ex As Exception

        End Try

        Return customClass.ListData
    End Function

#End Region
#Region "DrownDownReceived"
    Public Sub FundingDrowDownReceive(ByVal customclass As Parameter.FundingContractBatch)
        Dim params1() As SqlParameter = New SqlParameter(4) {}
        Dim params2() As SqlParameter = New SqlParameter(4) {}
        Dim params3() As SqlParameter = New SqlParameter(4) {}
        Dim params() As SqlParameter = New SqlParameter(18) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            If customclass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = customclass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = customclass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = customclass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"
                SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                customclass.ReferenceNo = CStr(params1(2).Value)
            End If
            'reference 2
            If customclass.ReferenceNo2 = "" And customclass.BankAccountID2 <> "0" Then
                ' ambil reference no otomatis jika no reference blank
                params2(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params2(0).Value = customclass.BranchId.Replace("'", "")
                params2(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params2(1).Value = customclass.BankAccountID
                params2(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params2(2).Direction = ParameterDirection.Output
                params2(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params2(3).Value = customclass.BusinessDate
                params2(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params2(4).Value = "M"
                SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "spGetNoTransactionKM", params2)
                customclass.ReferenceNo2 = CStr(params2(2).Value)
            End If
            'reference 3
            If customclass.ReferenceNo3 = "" And customclass.BankAccountID3 <> "0" Then
                ' ambil reference no otomatis jika no reference blank
                params3(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params3(0).Value = customclass.BranchId.Replace("'", "")
                params3(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params3(1).Value = customclass.BankAccountID
                params3(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params3(2).Direction = ParameterDirection.Output
                params3(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params3(3).Value = customclass.BusinessDate
                params3(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params3(4).Value = "M"
                SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "spGetNoTransactionKM", params3)
                customclass.ReferenceNo3 = CStr(params3(2).Value)
            End If

            params(0) = New SqlParameter("@BankID", SqlDbType.VarChar, 5)
            params(0).Value = customclass.BankId

            params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingCoyId

            params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingContractNo

            params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(3).Value = customclass.FundingBatchNo

            params(4) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(4).Value = customclass.ValueDate

            params(5) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
            params(5).Value = customclass.ReferenceNo

            params(6) = New SqlParameter("@ReferenceNo2", SqlDbType.VarChar, 20)
            params(6).Value = customclass.ReferenceNo2

            params(7) = New SqlParameter("@ReferenceNo3", SqlDbType.VarChar, 20)
            params(7).Value = customclass.ReferenceNo3

            params(8) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
            params(8).Value = customclass.BranchId

            params(9) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10)
            params(9).Value = customclass.BankAccountID

            params(10) = New SqlParameter("@BankAccountID2", SqlDbType.VarChar, 10)
            params(10).Value = customclass.BankAccountID2

            params(11) = New SqlParameter("@BankAccountID3", SqlDbType.VarChar, 10)
            params(11).Value = customclass.BankAccountID3

            params(12) = New SqlParameter("@LoginId", SqlDbType.VarChar, 12)
            params(12).Value = customclass.LoginId

            params(13) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(13).Value = customclass.BusinessDate

            params(14) = New SqlParameter("@companyID", SqlDbType.VarChar, 3)
            params(14).Value = customclass.CompanyID

            params(15) = New SqlParameter("@DrowDownAmount", SqlDbType.Decimal)
            params(15).Value = customclass.PrincipalAmtToFunCoy

            params(16) = New SqlParameter("@BankAccountAmount1", SqlDbType.Decimal)
            params(16).Value = customclass.AmtDrawDown1

            params(17) = New SqlParameter("@BankAccountAmount2", SqlDbType.Decimal)
            params(17).Value = customclass.AmtDrawDown2

            params(18) = New SqlParameter("@BankAccountAmount3", SqlDbType.Decimal)
            params(18).Value = customclass.AmtDrawDown3


            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, SPFUNDINGDROWDOWNRECEIVE, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "FundingDrowDownReceive", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
#End Region
#Region "InstallmentPaymentOut"



    Public Function GetGeneralEditView(ByVal customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(0).Value = customClass.FundingCoyId
        params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(1).Value = customClass.FundingContractNo
        params(2) = New SqlParameter("@FundingBatchNO", SqlDbType.VarChar, 20)
        params(2).Value = customClass.FundingBatchNo
        params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(3).Value = customClass.BusinessDate
        Try
            customClass.Listdata = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params).Tables(0)
            Return customClass
        Catch exp As MaxiloanExceptions
            Dim err As New MaxiloanExceptions
            err.WriteLog("FundingCompany", "GetGeneralEditView", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            WriteException("FundingCompany", "GetGeneralEditView", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Sub PaymentOutInstallmentProcess(ByVal customclass As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(18) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BankID", SqlDbType.VarChar, 5)
            params(0).Value = customclass.BankId
            params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingCoyId
            params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingContractNo
            params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(3).Value = customclass.FundingBatchNo
            params(4) = New SqlParameter("@PrincipalPaidAmount", SqlDbType.Decimal)
            params(4).Value = customclass.PrincipalPaidAmount
            params(5) = New SqlParameter("@InterestPaidAmount", SqlDbType.Decimal)
            params(5).Value = customclass.InterestPaidAmount
            params(6) = New SqlParameter("@PenaltyPaidAmount", SqlDbType.Decimal)
            params(6).Value = customclass.PenaltyPaidAmount
            params(7) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(7).Value = customclass.ValueDate
            params(8) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
            params(8).Value = customclass.ReferenceNo
            params(9) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(9).Value = customclass.BusinessDate
            params(10) = New SqlParameter("@bankAccountID", SqlDbType.VarChar, 10)
            params(10).Value = customclass.BankAccountID
            params(11) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(11).Value = customclass.BranchId
            params(12) = New SqlParameter("@LoginId", SqlDbType.VarChar, 12)
            params(12).Value = customclass.LoginId
            params(13) = New SqlParameter("@companyID", SqlDbType.VarChar, 3)
            params(13).Value = customclass.CompanyID
            params(14) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(14).Value = customclass.InsSecNo
            params(15) = New SqlParameter("@PPHPaid", SqlDbType.Decimal)
            params(15).Value = customclass.PPHPaid
            params(16) = New SqlParameter("@PrincipalMustPaid", SqlDbType.Decimal)
            params(16).Value = customclass.AmtDrawDown1
            params(17) = New SqlParameter("@InterestMustPaid", SqlDbType.Decimal)
            params(17).Value = customclass.AmtDrawDown2
            params(18) = New SqlParameter("@Note", SqlDbType.VarChar, 100)
            params(18).Value = customclass.Notes

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spPaymentOutInstallmentProcess, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "PaymentOutInstallmentProcess", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
#End Region
#Region "Prepayment"
    Public Function GetPrepaymentEditView(ByVal customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        If customClass.SpName = "spFundingPrepaymentPaymentOutView" Then
            Dim params() As SqlParameter = New SqlParameter(6) {}
            params(0) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
            params(0).Value = customClass.FundingCoyId
            params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(1).Value = customClass.FundingContractNo
            params(2) = New SqlParameter("@FundingBatchNO", SqlDbType.VarChar, 20)
            params(2).Value = customClass.FundingBatchNo
            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = customClass.BusinessDate
            params(4) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(4).Value = customClass.BranchIDApplication
            params(5) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(5).Value = customClass.ApplicationID
            params(6) = New SqlParameter("@AsOfDate", SqlDbType.DateTime)
            params(6).Value = customClass.PaymentOutAsOfDate
            Try
                customClass.Listdata = SqlHelper.ExecuteDataset(customClass.strConnection,
                CommandType.StoredProcedure, customClass.SpName, params).Tables(0)
                Return customClass
            Catch exp As MaxiloanExceptions
                Dim err As New MaxiloanExceptions
                err.WriteLog("FundingCompany", "GetPrepaymentEditView", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
                WriteException("FundingCompany", "GetPrepaymentEditView", exp.Message + exp.StackTrace)
            End Try
        Else
            Dim params() As SqlParameter = New SqlParameter(5) {}
            params(0) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
            params(0).Value = customClass.FundingCoyId
            params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(1).Value = customClass.FundingContractNo
            params(2) = New SqlParameter("@FundingBatchNO", SqlDbType.VarChar, 20)
            params(2).Value = customClass.FundingBatchNo
            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = customClass.BusinessDate
            params(4) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(4).Value = customClass.BranchIDApplication
            params(5) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(5).Value = customClass.ApplicationID
            Try
                customClass.Listdata = SqlHelper.ExecuteDataset(customClass.strConnection,
                CommandType.StoredProcedure, customClass.SpName, params).Tables(0)
                Return customClass
            Catch exp As MaxiloanExceptions
                Dim err As New MaxiloanExceptions
                err.WriteLog("FundingCompany", "GetPrepaymentEditView", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
                WriteException("FundingCompany", "GetPrepaymentEditView", exp.Message + exp.StackTrace)
            End Try
        End If
    End Function
    Public Sub PrepaymentBPKBReplacing(ByVal customclass As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(7) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@BankID", SqlDbType.VarChar, 5)
            params(0).Value = customclass.BankId
            params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingCoyId
            params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingContractNo
            params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(3).Value = customclass.FundingBatchNo
            params(4) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(4).Value = customclass.ApplicationID
            params(5) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(5).Value = customclass.BranchId
            params(6) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(6).Value = customclass.BusinessDate
            params(7) = New SqlParameter("@IsBatchPrepayment", SqlDbType.Bit)
            params(7).Value = customclass.IsBatchPrepayment
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFundingPrepaymentBPKBReplacingProcess, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "PrepaymentBPKBReplacing", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Sub PrepaymentPaymentOut(ByVal customclass As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(22) {}
        Dim params1() As SqlParameter = New SqlParameter(4) {}
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            If customclass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = customclass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = customclass.BankAccountID
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = customclass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "K"
                SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                customclass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter("@BankID", SqlDbType.VarChar, 5)
            params(0).Value = customclass.BankId
            params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingCoyId
            params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingContractNo
            params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(3).Value = customclass.FundingBatchNo
            params(4) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
            params(4).Value = customclass.ReferenceNo
            params(5) = New SqlParameter("@bankAccountID", SqlDbType.VarChar, 10)
            params(5).Value = customclass.BankAccountID
            params(6) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(6).Value = customclass.ApplicationID
            params(7) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(7).Value = customclass.BranchId
            params(8) = New SqlParameter("@LoginId", SqlDbType.VarChar, 12)
            params(8).Value = customclass.LoginId
            params(9) = New SqlParameter("@companyID", SqlDbType.VarChar, 3)
            params(9).Value = customclass.CompanyID
            params(10) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(10).Value = customclass.BusinessDate
            params(11) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(11).Value = customclass.ValueDate
            params(12) = New SqlParameter("@AccruedInterest", SqlDbType.Decimal)
            params(12).Value = customclass.AccruedInterest
            params(13) = New SqlParameter("@PrepaymentAmount", SqlDbType.Decimal)
            params(13).Value = customclass.PrepaymentAmount
            params(14) = New SqlParameter("@OSPrincipalUndue", SqlDbType.Decimal)
            params(14).Value = customclass.OSPrincipalAmount
            params(15) = New SqlParameter("@PrepaymentPaidAmount", SqlDbType.Decimal)
            params(15).Value = customclass.PrincipalPaidAmount
            params(16) = New SqlParameter("@PenaltyPaidAmount", SqlDbType.Decimal)
            params(16).Value = customclass.PenaltyPaidAmount
            params(17) = New SqlParameter("@PrepaymentPenalty", SqlDbType.Decimal)
            params(17).Value = customclass.PrepaymentPenalty
            params(18) = New SqlParameter("@BranchIDApplication", SqlDbType.Decimal)
            params(18).Value = customclass.BranchIDApplication
            params(19) = New SqlParameter("@InterestForThisMonth", SqlDbType.Decimal)
            params(19).Value = customclass.InterestForThisMonth
            params(20) = New SqlParameter("@IsBatchPrepayment", SqlDbType.Bit)
            params(20).Value = customclass.IsBatchPrepayment
            params(21) = New SqlParameter("@PrincipalMustPaid", SqlDbType.Decimal)
            params(21).Value = customclass.AmtDrawDown1
            params(22) = New SqlParameter("@InterestMustPaid", SqlDbType.Decimal)
            params(22).Value = customclass.AmtDrawDown2

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFundingPrepaymentPaymentOutProcess, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "PrepaymentPaymentOut", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
#End Region
#Region "PaymentOutFees"
    Public Sub PaymentOutFees(ByVal customclass As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(14) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BankID", SqlDbType.VarChar, 5)
            params(0).Value = customclass.BankId
            params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingCoyId
            params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingContractNo
            params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(3).Value = customclass.FundingBatchNo
            params(4) = New SqlParameter("@AdminFeeFacility", SqlDbType.Decimal)
            params(4).Value = customclass.AdminFeeFacility
            params(5) = New SqlParameter("@CommitmentFee", SqlDbType.Decimal)
            params(5).Value = customclass.CommitmentFee
            params(6) = New SqlParameter("@ProvisionFeeAmount", SqlDbType.Decimal)
            params(6).Value = customclass.ProvisionFeeAmount
            params(7) = New SqlParameter("@OtherFeeAmount", SqlDbType.Decimal)
            params(7).Value = customclass.OtherFeeAmount
            params(8) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
            params(8).Value = customclass.ReferenceNo
            params(9) = New SqlParameter("@bankAccountID", SqlDbType.VarChar, 10)
            params(9).Value = customclass.BankAccountID
            params(10) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(10).Value = customclass.BranchId
            params(11) = New SqlParameter("@LoginId", SqlDbType.VarChar, 12)
            params(11).Value = customclass.LoginId
            params(12) = New SqlParameter("@companyID", SqlDbType.VarChar, 3)
            params(12).Value = customclass.CompanyID
            params(13) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(13).Value = customclass.ValueDate
            params(14) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(14).Value = customclass.BusinessDate
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFundingPaymentOutFeesProcess, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "PaymentOutFees", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
#End Region
    Public Sub AddFundingAgreementInstallment(ByVal customclass As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(17) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID
            params(2) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(2).Value = customclass.InsSecNo
            params(3) = New SqlParameter("@DueDate", SqlDbType.DateTime)
            params(3).Value = customclass.DueDate
            params(4) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
            params(4).Value = customclass.PrincipalAmount
            params(5) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
            params(5).Value = customclass.InterestAmount
            params(6) = New SqlParameter("@PrincipalPaidAmount", SqlDbType.Decimal)
            params(6).Value = customclass.PrincipalPaidAmount
            params(7) = New SqlParameter("@InterestPaidAmount", SqlDbType.Decimal)
            params(7).Value = customclass.InterestPaidAmount
            params(8) = New SqlParameter("@OSPrincipalAmount", SqlDbType.Decimal)
            params(8).Value = customclass.OSPrincipalAmount
            params(9) = New SqlParameter("@OSInterestAmount", SqlDbType.Decimal)
            params(9).Value = customclass.OSInterestAmount
            params(10) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(10).Value = customclass.FundingCoyId
            params(11) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(11).Value = customclass.FundingContractNo
            params(12) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(12).Value = customclass.FundingBatchNo
            params(13) = New SqlParameter("@InsAmount", SqlDbType.Decimal)
            params(13).Value = customclass.PrepaymentAmount

            params(14) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(14).Value = customclass.Tenor
            params(15) = New SqlParameter("@InterestRate", SqlDbType.Decimal)
            params(15).Value = customclass.InterestRate
            params(16) = New SqlParameter("@InsPeriod", SqlDbType.SmallInt)
            params(16).Value = customclass.LCGracePeriod
            params(17) = New SqlParameter("@MonthPeriod", SqlDbType.SmallInt)
            params(17).Value = customclass.MaximumDateForDD

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFundingAddFundingAgreementInst, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "AddFundingAgreementInstallment", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub

    Public Sub AddFundingAgreementInstallmentKMK(ByVal customclass As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(8) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID

            params(2) = New SqlParameter("@DueDate", SqlDbType.DateTime)
            params(2).Value = customclass.DueDate
            params(3) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
            params(3).Value = customclass.PrincipalAmount

            params(4) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(4).Value = customclass.FundingCoyId
            params(5) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(5).Value = customclass.FundingContractNo
            params(6) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(6).Value = customclass.FundingBatchNo

            params(7) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(7).Value = customclass.Tenor
            params(8) = New SqlParameter("@InterestRate", SqlDbType.Decimal)
            params(8).Value = customclass.InterestRate

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFundingAddFundingAgreementInstKMK", params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub

    Public Sub FundingRescheduleAgreementInstallment(ByVal customclass As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(16) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID
            params(2) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(2).Value = customclass.InsSecNo
            params(3) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
            params(3).Value = customclass.PrincipalAmount
            params(4) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
            params(4).Value = customclass.InterestAmount
            params(5) = New SqlParameter("@PrincipalPaidAmount", SqlDbType.Decimal)
            params(5).Value = customclass.PrincipalPaidAmount
            params(6) = New SqlParameter("@InterestPaidAmount", SqlDbType.Decimal)
            params(6).Value = customclass.InterestPaidAmount
            params(7) = New SqlParameter("@OSPrincipalAmount", SqlDbType.Decimal)
            params(7).Value = customclass.OSPrincipalAmount
            params(8) = New SqlParameter("@OSInterestAmount", SqlDbType.Decimal)
            params(8).Value = customclass.OSInterestAmount
            params(9) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(9).Value = customclass.FundingCoyId
            params(10) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(10).Value = customclass.FundingContractNo
            params(11) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(11).Value = customclass.FundingBatchNo
            params(12) = New SqlParameter("@InsAmount", SqlDbType.Decimal)
            params(12).Value = customclass.PrepaymentAmount
            params(13) = New SqlParameter("@InterestRate", SqlDbType.Decimal)
            params(13).Value = customclass.InterestRate
            params(14) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(14).Value = customclass.Tenor
            params(15) = New SqlParameter("@InsPeriod", SqlDbType.SmallInt)
            params(15).Value = customclass.LCGracePeriod
            params(16) = New SqlParameter("@MonthPeriod", SqlDbType.SmallInt)
            params(16).Value = customclass.MaximumDateForDD

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFundingRescheduleAgreementInstallment, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "FundingRescheduleAgreementInstallment", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Sub FundingReschedulePrepaymentAgreementInstallment(ByVal customclass As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(16) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID
            params(2) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(2).Value = customclass.InsSecNo
            params(3) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
            params(3).Value = customclass.PrincipalAmount
            params(4) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
            params(4).Value = customclass.InterestAmount
            params(5) = New SqlParameter("@PrincipalPaidAmount", SqlDbType.Decimal)
            params(5).Value = customclass.PrincipalPaidAmount
            params(6) = New SqlParameter("@InterestPaidAmount", SqlDbType.Decimal)
            params(6).Value = customclass.InterestPaidAmount
            params(7) = New SqlParameter("@OSPrincipalAmount", SqlDbType.Decimal)
            params(7).Value = customclass.OSPrincipalAmount
            params(8) = New SqlParameter("@OSInterestAmount", SqlDbType.Decimal)
            params(8).Value = customclass.OSInterestAmount
            params(9) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(9).Value = customclass.FundingCoyId
            params(10) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(10).Value = customclass.FundingContractNo
            params(11) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(11).Value = customclass.FundingBatchNo
            params(12) = New SqlParameter("@InsAmount", SqlDbType.Decimal)
            params(12).Value = customclass.PrepaymentAmount
            params(13) = New SqlParameter("@InterestRate", SqlDbType.Decimal)
            params(13).Value = customclass.InterestRate

            params(14) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(14).Value = customclass.Tenor
            params(15) = New SqlParameter("@InsPeriod", SqlDbType.SmallInt)
            params(15).Value = customclass.LCGracePeriod
            params(16) = New SqlParameter("@MonthPeriod", SqlDbType.SmallInt)
            params(16).Value = customclass.MaximumDateForDD
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFundingReschedulePrepaymentAgreementInstallment, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "FundingReschedulePrepaymentAgreementInstallment", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Sub FundingUpdateContractBatchInst(ByVal oCompany As Parameter.FundingContractBatch)

        Dim params() As SqlParameter = New SqlParameter(11) {}
        Dim objCon As New SqlConnection(oCompany.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
            params(0).Value = oCompany.BankId

            params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
            params(1).Value = oCompany.FundingCoyId

            params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(2).Value = oCompany.FundingContractNo

            params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(3).Value = oCompany.FundingBatchNo

            params(4) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(4).Value = oCompany.InsSecNo

            params(5) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
            params(5).Value = oCompany.PrincipalAmount

            params(6) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
            params(6).Value = oCompany.InterestAmount

            params(7) = New SqlParameter("@PrincipalPaidAmount", SqlDbType.Decimal)
            params(7).Value = oCompany.PrincipalPaidAmount

            params(8) = New SqlParameter("@InterestPaidAmount", SqlDbType.Decimal)
            params(8).Value = oCompany.InterestPaidAmount

            params(9) = New SqlParameter("@OSPrincipalAmount", SqlDbType.Decimal)
            params(9).Value = oCompany.OSPrincipalAmount

            params(10) = New SqlParameter("@OSInterestAmount", SqlDbType.Decimal)
            params(10).Value = oCompany.OSInterestAmount

            params(11) = New SqlParameter("@InterestRate", SqlDbType.Decimal)
            params(11).Value = oCompany.InterestRate



            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spFundingUpdateContractBatchInst, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "FundingUpdateContractBatchInst", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub

    Public Sub AddFundingAgreementInstallmentDraft(ByVal customclass As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(17) {}
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationID
            params(2) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(2).Value = customclass.InsSecNo
            params(3) = New SqlParameter("@DueDate", SqlDbType.DateTime)
            params(3).Value = customclass.DueDate
            params(4) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
            params(4).Value = customclass.PrincipalAmount
            params(5) = New SqlParameter("@InterestAmount", SqlDbType.Decimal)
            params(5).Value = customclass.InterestAmount
            params(6) = New SqlParameter("@PrincipalPaidAmount", SqlDbType.Decimal)
            params(6).Value = customclass.PrincipalPaidAmount
            params(7) = New SqlParameter("@InterestPaidAmount", SqlDbType.Decimal)
            params(7).Value = customclass.InterestPaidAmount
            params(8) = New SqlParameter("@OSPrincipalAmount", SqlDbType.Decimal)
            params(8).Value = customclass.OSPrincipalAmount
            params(9) = New SqlParameter("@OSInterestAmount", SqlDbType.Decimal)
            params(9).Value = customclass.OSInterestAmount
            params(10) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(10).Value = customclass.FundingCoyId
            params(11) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(11).Value = customclass.FundingContractNo
            params(12) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(12).Value = customclass.FundingBatchNo
            params(13) = New SqlParameter("@InsAmount", SqlDbType.Decimal)
            params(13).Value = customclass.PrepaymentAmount

            params(14) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(14).Value = customclass.Tenor
            params(15) = New SqlParameter("@InterestRate", SqlDbType.Decimal)
            params(15).Value = customclass.InterestRate
            params(16) = New SqlParameter("@InsPeriod", SqlDbType.SmallInt)
            params(16).Value = customclass.LCGracePeriod
            params(17) = New SqlParameter("@MonthPeriod", SqlDbType.SmallInt)
            params(17).Value = customclass.MaximumDateForDD

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFundingAddFundingAgreementInstDraft", params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub

#Region "DueDateChangeProcess"
    Public Sub DueDateChangeSave(ByVal customClass As Parameter.FundingContractBatch)
        Dim oConnection As New SqlConnection(customClass.strConnection)
        Dim objTransaction As SqlTransaction
        If oConnection.State = ConnectionState.Closed Then oConnection.Open()
        objTransaction = oConnection.BeginTransaction
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Dim intLoopOmset As Integer
        Dim i As Integer = 0
        Try
            params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(1) = New SqlParameter("@FundingContractNO", SqlDbType.VarChar, 20)
            params(2) = New SqlParameter("@FundingBatchNO", SqlDbType.VarChar, 20)
            params(3) = New SqlParameter("@FacilityKind", SqlDbType.Char, 5)
            params(4) = New SqlParameter("@DueDate", SqlDbType.VarChar, 10)
            params(5) = New SqlParameter("@InsSeqNo", SqlDbType.Int)

            If customClass.Listdata.Rows.Count > 0 Then
                For intLoopOmset = 0 To customClass.Listdata.Rows.Count - 1
                    i = i + 1
                    params(0).Value = customClass.FundingCoyId
                    params(1).Value = customClass.FundingContractNo
                    params(2).Value = customClass.FundingBatchNo
                    params(3).Value = customClass.FacilityKind
                    params(4).Value = customClass.Listdata.Rows(intLoopOmset).Item("DueDate")
                    params(5).Value = CInt(customClass.Listdata.Rows(intLoopOmset).Item("InSeqNo"))
                    SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, spFundingChangeBatchDateProcess, params)
                Next
            End If
            objTransaction.Commit()
        Catch exp As Exception
            WriteException("DChange", "DueDateChangeSave", exp.Message + exp.StackTrace)
            objTransaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If oConnection.State = ConnectionState.Open Then oConnection.Close()
            oConnection.Dispose()
        End Try
    End Sub
#End Region

    Public Sub FundingContractBatchInsAddKMK(ByVal oCompany As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(7) {}

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(3).Value = oCompany.FundingBatchNo

        params(4) = New SqlParameter("@Interest", SqlDbType.Float)
        params(4).Value = oCompany.InterestRate

        params(5) = New SqlParameter("@Period", SqlDbType.Int)
        params(5).Value = oCompany.Tenor

        params(6) = New SqlParameter("@DrawAmount", SqlDbType.Money)
        params(6).Value = oCompany.PrincipalAmount

        params(7) = New SqlParameter("@Tanggal", SqlDbType.DateTime)
        params(7).Value = oCompany.DueDate

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, "spFundingContractBatchInsAddKMK", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub FundingContractBatchInsAdd2(ByVal oCompany As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(7) {}

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(3).Value = oCompany.FundingBatchNo

        params(4) = New SqlParameter("@Interest", SqlDbType.Float)
        params(4).Value = oCompany.InterestRate

        params(5) = New SqlParameter("@Period", SqlDbType.Int)
        params(5).Value = oCompany.Tenor

        params(6) = New SqlParameter("@DrawAmount", SqlDbType.Money)
        params(6).Value = oCompany.PrincipalAmount

        params(7) = New SqlParameter("@Tanggal", SqlDbType.DateTime)
        params(7).Value = oCompany.DueDate

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, "spFundingContractBatchInsAdd2", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Function FundingDraftSoftcopy(ByVal oCompany As Parameter.FundingContractBatch) As DataSet
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(0).Value = oCompany.FundingCoyId

        params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingContractNo

        params(2) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingBatchNo

        Try
            Return SqlHelper.ExecuteDataset(oCompany.strConnection, CommandType.StoredProcedure, "spGetFundingDraft", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub genFAIExec(ByVal oCompany As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(6) {}

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankId

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyId

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(3).Value = oCompany.FundingBatchNo

        params(4) = New SqlParameter("@Interest", SqlDbType.Float)
        params(4).Value = oCompany.InterestRate

        params(5) = New SqlParameter("@Period", SqlDbType.Int)
        params(5).Value = oCompany.Tenor

        params(6) = New SqlParameter("@Tanggal", SqlDbType.DateTime)
        params(6).Value = oCompany.DueDate

        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, "spFundingFAIEfektifExec", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub


    Public Function FundingAgreementSelection(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch

        Dim params() As SqlParameter = New SqlParameter(7) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.BigInt)
        params(4).Direction = ParameterDirection.Output

        params(5) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(5).Value = customclass.FundingCoyId

        params(6) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(6).Value = customclass.FundingContractNo

        params(7) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(7).Value = customclass.FundingBatchNo

        Try
            customclass.Listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spFundingAgreementSelectionList", params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
        Catch ex As Exception

        End Try

        Return customclass
    End Function

    Function FundingAgreementList(customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim params() As SqlParameter = New SqlParameter(8) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(3).Value = customClass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        params(5) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(5).Value = customClass.FundingCoyId

        params(6) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(6).Value = customClass.FundingContractNo

        params(7) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(7).Value = customClass.FundingBatchNo

        params(8) = New SqlParameter("@Filter", SqlDbType.Char, 1)
        params(8).Value = customClass.Filter

        Try
            customClass.Listdata = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spFundingAgreementList", params).Tables(0)
            customClass.TotalRecord = CInt(params(4).Value)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

        Return customClass
    End Function

#Region " FundingContractRate"

    Public Sub FundingContractRateEdit(ByVal oCompany As Parameter.FundingContractRate)

        Dim params(5) As SqlParameter

        params(0) = New SqlParameter("@BankId", SqlDbType.Char, 5)
        params(0).Value = oCompany.BankID

        params(1) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1).Value = oCompany.FundingCoyID

        params(2) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2).Value = oCompany.FundingContractNo

        params(3) = New SqlParameter("@TenorFrom", SqlDbType.SmallInt)
        params(3).Value = oCompany.TenorFrom

        params(4) = New SqlParameter("@TenorTo", SqlDbType.SmallInt)
        params(4).Value = oCompany.TenorTo

        params(5) = New SqlParameter("@Rate", SqlDbType.Decimal)
        params(5).Value = oCompany.FundingRate


        Try
            SqlHelper.ExecuteNonQuery(oCompany.strConnection, CommandType.StoredProcedure, "spFundingContractRateSaveEdit", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Sub
#End Region

    Public Sub FundingDrowDownReceiveCheckPeriod(ByVal customclass As Parameter.FundingContractBatch)

        Dim params(4) As SqlParameter

        With customclass

            params(0) = New SqlParameter("@FundingContractNo", SqlDbType.Char, 20)
            params(1) = New SqlParameter("@FundingCoyID", SqlDbType.Char, 20)
            params(2) = New SqlParameter("@FundingBatchNo", SqlDbType.Char, 20)
            params(3) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(4) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
            params(0).Value = .FundingContractNo
            params(1).Value = .FundingCoyId
            params(2).Value = .FundingBatchNo
            params(3).Value = .ValueDate
            params(4).Direction = ParameterDirection.Output

        End With

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spFundingContractReceiveCheckPeriod", params)
            customclass.ErrLabel = params(4).Value

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub FundingDrowDownReceiveCheckScheme(ByVal customclass As Parameter.FundingContractBatch)

        Dim params(3) As SqlParameter

        With customclass

            params(0) = New SqlParameter("@FundingContractNo", SqlDbType.Char, 20)
            params(1) = New SqlParameter("@FundingCoyID", SqlDbType.Char, 20)
            params(2) = New SqlParameter("@FundingBatchNo", SqlDbType.Char, 20)
            params(3) = New SqlParameter("@strscheme", SqlDbType.VarChar, 100)
            params(0).Value = .FundingContractNo
            params(1).Value = .FundingCoyId
            params(2).Value = .FundingBatchNo
            params(3).Direction = ParameterDirection.Output

        End With

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spFundingContractReceiveCheckPaymentScheme", params)
            customclass.Scheme = params(3).Value

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub FundingDrowDownReceiveDueDateRangeAdd(ByVal customclass As Parameter.FundingContractBatch)

        Dim params(5) As SqlParameter
        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction

        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
            params(0).Value = customclass.FundingCoyId

            params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingContractNo

            params(2) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingBatchNo

            params(3) = New SqlParameter("@DateFrom", SqlDbType.VarChar, 20)
            params(3).Value = customclass.ReferenceNo

            params(4) = New SqlParameter("@DateTo", SqlDbType.VarChar, 20)
            params(4).Value = customclass.ReferenceNo2

            params(5) = New SqlParameter("@DueDate", SqlDbType.VarChar, 20)
            params(5).Value = customclass.ReferenceNo3

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFundingDueDateByRangeAdd", params)
            transaction.Commit()

        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "FundingDrowDownReceive", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub

    Function FundingTotalSelected(customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(0).Value = customClass.FundingCoyId

        params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(1).Value = customClass.FundingContractNo

        params(2) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(2).Value = customClass.FundingBatchNo

        Try
            customClass.Listdata = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spFundingTotalAgreement", params).Tables(0)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

        Return customClass
    End Function

    Function FundingAgreementInstallmentView(customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oClassReturn As New Parameter.FundingContractBatch
        Dim dtSet As New DataSet

        Dim params() As SqlParameter = New SqlParameter(3) {}

        params(0) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(2) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(3) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)

        params(0).Value = customClass.FundingCoyId
        params(1).Value = customClass.FundingContractNo
        params(2).Value = customClass.FundingBatchNo
        params(3).Value = customClass.ApplicationID

        Try
            dtSet = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spFundingAgreementInstallmentView", params)
            oClassReturn.Listdata = dtSet.Tables(0)
            oClassReturn.ListData2 = dtSet.Tables(1)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

        Return oClassReturn
    End Function


    Public Function ListSPCReport(ByVal customclass As Parameter.FundingCompany) As DataSet
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim SPString As String = ""


        SPString = "spSPCPencairanReport"

        params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.FundingCoyID
        params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 1000)
        params(1).Value = customclass.FundingContractNo
        params(2) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.FundingBatchNo

        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPString, params)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try




    End Function
    Public Function FundPrintSave(ByVal ocustomClass As Parameter.FundingCompany) As String
        Dim ErrMessage As String = ""
        Dim params(5) As SqlParameter

        params(0) = New SqlParameter("@debetDate", SqlDbType.DateTime)
        params(0).Value = ocustomClass.debetDate

        params(1) = New SqlParameter("@PrintedBy", SqlDbType.VarChar, 100)
        params(1).Value = ocustomClass.PrintedBy

        params(2) = New SqlParameter("@Up", SqlDbType.VarChar, 100)
        params(2).Value = ocustomClass.Up

        params(3) = New SqlParameter("@Status", SqlDbType.VarChar, 1)
        params(3).Value = ocustomClass.Status

        params(4) = New SqlParameter("@NoReff", SqlDbType.VarChar, 100)
        params(4).Value = ocustomClass.NoReff

        params(5) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(5).Direction = ParameterDirection.Output


        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, spFundPrintSave_ADD, params)
            ErrMessage = CType(params(5).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.PrepayFundPrint.FundPrintSave")
        End Try
    End Function
    Public Function ListPrint(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oReturnValue As New Parameter.FundingContractBatch
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPaymentOutPrepayListPrint", params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function SavePrint(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(2) {}
            oDataTable = customclass.ListConfLetter
            For intloop = 0 To oDataTable.Rows.Count - 1
                params(0) = New SqlParameter("@PayOutReff", SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("NoReff")

                params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params(1).Value = customclass.BusinessDate

                params(2) = New SqlParameter("@LoginID", SqlDbType.Char, 12)
                params(2).Value = customclass.LoginId
                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, "spPaymentOutPrepayListSavePrint", params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
    Public Function ListReportPrepay(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond

        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPrepayOutPrint", params)
        Return customclass
    End Function
    'Public Function FundingAgreementJnFnc(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
    '    Dim params() As SqlParameter = New SqlParameter(0) {}
    '    params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 1000)
    '    params(0).Value = customclass.ApplicationID

    '    customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spFundingAgreementJnFnc", params)
    '    Return customclass
    'End Function

    Public Function FundingAgreementJnFnc(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        'Dim objconnection As New SqlConnection(customclass.strConnection)
        'Dim objtransaction As SqlTransaction = Nothing
        'Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        'Try
        '    If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        '    objtransaction = objconnection.BeginTransaction

        '    params.Add(New SqlParameter("@ApplicationID", SqlDbType.VarChar, 100) With {.Value = customclass.ApplicationID})

        '    SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spFundingAgreementJnFnc", params.ToArray)

        '    objtransaction.Commit()

        'Catch exp As Exception
        '    objtransaction.Rollback()
        '    WriteException("FundingContractBatch", "spFundingAgreementJnFnc", exp.Message + exp.StackTrace)
        '    Throw New Exception("Failed On")

        'Finally
        '    If objconnection.State = ConnectionState.Open Then objconnection.Close()
        '    objconnection.Dispose()
        'End Try

        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.ApplicationID

        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spFundingAgreementJnFnc", params)
        Return customclass
    End Function

    Function GetFundingInquiryAgreementPledgeWillFinis(oCustomClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oReturnValue As New Parameter.FundingContractBatch
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetComboInputBankBeban(ByVal strcon As String) As DataTable

        Dim customClass As New Parameter.BankMaster

        Try
            customClass.ListData = SqlHelper.ExecuteDataset(strcon, CommandType.Text, spGETCOMBO_INPUTBANKBEBAN).Tables(0)
        Catch ex As Exception

        End Try

        Return customClass.ListData
    End Function
    Public Function GetComboInputBankAdministrasi(ByVal strcon As String) As DataTable

        Dim customClass As New Parameter.BankMaster

        Try
            customClass.ListData = SqlHelper.ExecuteDataset(strcon, CommandType.Text, spGETCOMBO_INPUTBANKADMINISTRASI).Tables(0)
        Catch ex As Exception

        End Try

        Return customClass.ListData
    End Function

    Public Function ListPrintPaymentOut(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oReturnValue As New Parameter.FundingContractBatch
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPaymentOutListPrint", params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function SavePrintPaymentOut(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim oTrans As SqlTransaction
        Dim oCOnnection As New SqlConnection(customclass.strConnection)
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            If oCOnnection.State = ConnectionState.Closed Then oCOnnection.Open()
            oTrans = oCOnnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(2) {}
            oDataTable = customclass.ListConfLetter
            For intloop = 0 To oDataTable.Rows.Count - 1
                params(0) = New SqlParameter("@PayOutReff", SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("NoReff")

                params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params(1).Value = customclass.BusinessDate

                params(2) = New SqlParameter("@LoginID", SqlDbType.Char, 12)
                params(2).Value = customclass.LoginId
                SqlHelper.ExecuteNonQuery(oTrans, CommandType.StoredProcedure, "spPaymentOutListSavePrint", params)
            Next
            oTrans.Commit()
            customclass.hasil = 1
            Return customclass
        Catch ex As Exception
            oTrans.Rollback()
            customclass.hasil = 0
            Return customclass
        Finally
            If oCOnnection.State = ConnectionState.Open Then oCOnnection.Close()
            oCOnnection.Dispose()
        End Try
    End Function
    Public Function ListReportPaymentOut(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond

        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spFundingPaymentOutPrint", params)
        Return customclass
    End Function
    'Upload FundingBatchInstallment
    Public Sub FundingBatchInstallmentSelectFromUpload(ByVal customclass As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(4) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@BankId", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BankId

            params(1) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingCoyId

            params(2) = New SqlParameter("@FundingContractNO", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingContractNo

            params(3) = New SqlParameter("@FundingBatchNO", SqlDbType.VarChar, 20)
            params(3).Value = customclass.FundingBatchNo

            params(4) = New SqlParameter("@DataFundingBatchInstallmentUpload", SqlDbType.Structured)
            params(4).Value = customclass.Listdata

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spFundingBatchInstallmentUpload", params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "FundingUncheckAgreement", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub
    Public Function FundingBatchRate(ByVal oClass As Parameter.FundingContractRate) As Parameter.FundingContractRate
        Dim aClass As New Parameter.FundingContractRate
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim SPString As String = ""


        SPString = "spSPCPencairanReport"

        params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
        params(0).Value = oClass.FundingCoyID
        params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(1).Value = oClass.FundingContractNo
        params(2) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(2).Value = oClass.FUndingBatchNo

        Try
            aClass.Listdata = SqlHelper.ExecuteDataset(oClass.strConnection, CommandType.StoredProcedure, "spFundingBatchRate", params).Tables(0)
            Return aClass

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function


    Public Sub UpdateFundingAgreementSelect(ByVal customclass As Parameter.FundingContractBatch)
        Dim params() As SqlParameter = New SqlParameter(3) {}

        Dim objCon As New SqlConnection(customclass.strConnection)
        Dim transaction As SqlTransaction
        Try
            If objCon.State = ConnectionState.Closed Then objCon.Open()
            transaction = objCon.BeginTransaction

            params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(0).Value = customclass.FundingCoyId

            params(1) = New SqlParameter("@FundingContractNO", SqlDbType.VarChar, 20)
            params(1).Value = customclass.FundingContractNo

            params(2) = New SqlParameter("@FundingBatchNO", SqlDbType.VarChar, 20)
            params(2).Value = customclass.FundingBatchNo

            params(3) = New SqlParameter("@FundingAgreementSelect", SqlDbType.Structured)
            params(3).Value = customclass.Listdata

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spUpdateFundingAgreementSelect", params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("FundingCompany", "UpdateFundingAgreementSelect", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        Finally
            If objCon.State = ConnectionState.Open Then objCon.Close()
            objCon.Dispose()
        End Try
    End Sub


End Class
