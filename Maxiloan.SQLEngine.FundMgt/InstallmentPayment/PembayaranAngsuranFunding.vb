Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter

Public Class PembayaranAngsuranFunding : Inherits DataAccessBase

    Private m_connection As SqlConnection
    Public Function getListPembayaranAngsuran(ByVal customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim params() As SqlParameter = New SqlParameter(9) {}
        params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.FundingCoyID
        params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(1).Value = customClass.FundingContractNo
        params(2) = New SqlParameter("@InstallmentDueDate", SqlDbType.DateTime)
        params(2).Value = customClass.InstallmentDueDate
        params(3) = New SqlParameter("@JumlahKontrak", SqlDbType.SmallInt)
        params(3).Direction = ParameterDirection.Output
        params(4) = New SqlParameter("@JumlahAngsuran", SqlDbType.Money)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@JumlahPokok", SqlDbType.Money)
        params(5).Direction = ParameterDirection.Output
        params(6) = New SqlParameter("@JumlahBunga", SqlDbType.Money)
        params(6).Direction = ParameterDirection.Output
        params(7) = New SqlParameter("@RefundBunga", SqlDbType.Money)
        params(7).Direction = ParameterDirection.Output
        params(8) = New SqlParameter("@InstallmentDueDateTo", SqlDbType.DateTime)
        params(8).Value = customClass.InstallmentDueDateTo
        params(9) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(9).Value = customClass.FundingBatchNo
        'params(10) = New SqlParameter("@where", SqlDbType.VarChar, 1000)
        'params(10).Value = customClass.WhereCond
        Try
            customClass.ListPembayaranAngsuran = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure,
            "spGetListPembayaranAngsuranFunding", params)
            customClass.JumlahKontrak = CInt(params(3).Value)
            customClass.JumlahPokok = CDbl(params(5).Value)
            customClass.JumlahAngsuran = CDbl(params(4).Value)
            customClass.JumlahBunga = CDbl(params(6).Value)
            customClass.RefundBunga = CDbl(params(7).Value)
            Return customClass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetListData(ByVal strConnection As String, ByVal cmdWhere As String, ByVal currentPage As Integer, ByVal pagesize As Integer, ByVal SortBy As String) As Parameter.PembayaranAngsuranFunding
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = currentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = pagesize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = cmdWhere
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            Dim oReturnValue As New Parameter.PembayaranAngsuranFunding
            oReturnValue.ListDataVendor = SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, "spGetSupplierAccount", params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("LookUpTransaction", "GetListData", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Sub DisbursePembayaranAngsuranFunding(ByVal customClass As Parameter.DisbursePembayaranAngsuranFunding)
        Dim objTrans As SqlTransaction
        Try
            m_connection = New SqlConnection(customClass.strConnection)
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()
            objTrans = m_connection.BeginTransaction

            Dim params() As SqlParameter = New SqlParameter(13) {}
            Dim params1() As SqlParameter = New SqlParameter(4) {}

            If customClass.ReferenceNo = "" Then
                ' ambil reference no otomatis jika no reference blank
                params1(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
                params1(0).Value = customClass.BranchId.Replace("'", "")
                params1(1) = New SqlParameter("@BankAccountID", SqlDbType.Char, 10)
                params1(1).Value = customClass.BankAccountId
                params1(2) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
                params1(2).Direction = ParameterDirection.Output
                params1(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
                params1(3).Value = customClass.BusinessDate
                params1(4) = New SqlParameter("@Flag", SqlDbType.Char, 1)
                params1(4).Value = "M"
                SqlHelper.ExecuteScalar(objTrans, CommandType.StoredProcedure, "spGetNoTransactionKM", params1)
                customClass.ReferenceNo = CStr(params1(2).Value)
            End If

            params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(0).Value = customClass.FundingCoyID
            params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(1).Value = customClass.FundingContractNo
            params(2) = New SqlParameter("@PrincipalPaidAmount", SqlDbType.Decimal)
            params(2).Value = customClass.PrincipalPaidAmount
            params(3) = New SqlParameter("@InterestPaidAmount", SqlDbType.Decimal)
            params(3).Value = customClass.InterestPaidAmount
            params(4) = New SqlParameter("@PenaltyPaidAmount", SqlDbType.Decimal)
            params(4).Value = customClass.PenaltyPaidAmount
            params(5) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(5).Value = customClass.ValueDate
            params(6) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
            params(6).Value = customClass.ReferenceNo
            params(7) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(7).Value = customClass.BusinessDate
            params(8) = New SqlParameter("@bankAccountID", SqlDbType.VarChar, 10)
            params(8).Value = customClass.BankAccountId
            params(9) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
            params(9).Value = customClass.BranchId
            params(10) = New SqlParameter("@LoginId", SqlDbType.VarChar, 12)
            params(10).Value = customClass.LoginId
            params(11) = New SqlParameter("@companyID", SqlDbType.VarChar, 3)
            params(11).Value = customClass.CompanyID
            params(12) = New SqlParameter("@PPHPaid", SqlDbType.Decimal)
            params(12).Value = customClass.PPHPaid
            params(13) = New SqlParameter("@ApplicationID", SqlDbType.VarChar)
            params(13).Value = customClass.SelectedApplicationIDs

            SqlHelper.ExecuteNonQuery(objTrans, CommandType.StoredProcedure, "spPembayaranAngsuranFunding", params)
            objTrans.Commit()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()

        Catch ex As Exception
            objTrans.Rollback()
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Function getListBonHijauFundingDisburse(ByVal customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@ReferenceNo", SqlDbType.Char, 20)
        params(0).Value = customClass.ReferenceNoBonHijau
        Try
            customClass.ListBonHijau = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure,
            "spBonHijauFundingDisburse", params)
            Return customClass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function getFundingAngsJthTempoSummary(ByVal customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim params() As SqlParameter = New SqlParameter(8) {}

        params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.FundingCoyID
        params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(1).Value = customClass.FundingContractNo
        params(2) = New SqlParameter("@InstallmentDueDate", SqlDbType.DateTime)
        params(2).Value = customClass.InstallmentDueDate
        params(3) = New SqlParameter("@JumlahKontrak", SqlDbType.SmallInt)
        params(3).Direction = ParameterDirection.Output
        params(4) = New SqlParameter("@JumlahAngsuran", SqlDbType.Money)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@JumlahPokok", SqlDbType.Money)
        params(5).Direction = ParameterDirection.Output
        params(6) = New SqlParameter("@JumlahBunga", SqlDbType.Money)
        params(6).Direction = ParameterDirection.Output
        params(7) = New SqlParameter("@RefundBunga", SqlDbType.Money)
        params(7).Direction = ParameterDirection.Output
        params(8) = New SqlParameter("@InstallmentDueDateTo", SqlDbType.DateTime)
        params(8).Value = customClass.InstallmentDueDateTo

        Try
            SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spGetFundingAngsJthTempoSummary", params)
            customClass.JumlahKontrak = CInt(params(3).Value)
            customClass.JumlahPokok = CDbl(params(5).Value)
            customClass.JumlahAngsuran = CDbl(params(4).Value)
            customClass.JumlahBunga = CDbl(params(6).Value)
            customClass.RefundBunga = CDbl(params(7).Value)
            Return customClass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Sub disburseFundingAngsuranJthTempo(ByVal customClass As Parameter.DisbursePembayaranAngsuranFunding)
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim params() As SqlParameter = New SqlParameter(14) {}

        If conn.State = ConnectionState.Closed Then conn.Open()
        Dim trans As SqlTransaction = conn.BeginTransaction

        params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.FundingCoyID
        params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(1).Value = customClass.FundingContractNo
        params(2) = New SqlParameter("@PrincipalPaidAmount", SqlDbType.Decimal)
        params(2).Value = customClass.PrincipalPaidAmount
        params(3) = New SqlParameter("@InterestPaidAmount", SqlDbType.Decimal)
        params(3).Value = customClass.InterestPaidAmount
        params(4) = New SqlParameter("@PenaltyPaidAmount", SqlDbType.Decimal)
        params(4).Value = customClass.PenaltyPaidAmount
        params(5) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
        params(5).Value = customClass.ValueDate
        params(6) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
        params(6).Value = customClass.ReferenceNo
        params(7) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(7).Value = customClass.BusinessDate
        params(8) = New SqlParameter("@bankAccountID", SqlDbType.VarChar, 10)
        params(8).Value = customClass.BankAccountId
        params(9) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
        params(9).Value = customClass.BranchId
        params(10) = New SqlParameter("@LoginId", SqlDbType.VarChar, 12)
        params(10).Value = customClass.LoginId
        params(11) = New SqlParameter("@companyID", SqlDbType.VarChar, 3)
        params(11).Value = customClass.CompanyID
        params(12) = New SqlParameter("@PPHPaid", SqlDbType.Decimal)
        params(12).Value = customClass.PPHPaid
        params(13) = New SqlParameter("@InstallmentDueDate", SqlDbType.DateTime)
        params(13).Value = customClass.InstallmentDueDate
        params(14) = New SqlParameter("@InstallmentDueDateTo", SqlDbType.DateTime)
        params(14).Value = customClass.InstallmentDueDateTo

        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "spDisburseFundingAngsuranJthTempo", params)
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub prepaymentBatchProcess(ByVal customclass As Parameter.FundingContractBatch)
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Try
            params.Add(New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20) With {.Value = customclass.ReferenceNo})
            params.Add(New SqlParameter("@bankAccountID", SqlDbType.VarChar, 10) With {.Value = customclass.BankAccountID})
            params.Add(New SqlParameter("@AgreementNoTbl", SqlDbType.Structured) With {.Value = customclass.AgreementNoTbl})
            params.Add(New SqlParameter("@BranchID", SqlDbType.VarChar, 3) With {.Value = customclass.BranchId})
            params.Add(New SqlParameter("@LoginId", SqlDbType.VarChar, 12) With {.Value = customclass.LoginId})
            params.Add(New SqlParameter("@companyID", SqlDbType.VarChar, 3) With {.Value = customclass.CompanyID})
            params.Add(New SqlParameter("@BusinessDate", SqlDbType.DateTime) With {.Value = customclass.BusinessDate})
            params.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime) With {.Value = customclass.ValueDate})
            params.Add(New SqlParameter("@TotalPrepaymentAmount", SqlDbType.Decimal) With {.Value = customclass.TotalPrepaymentAmount})
            params.Add(New SqlParameter("@EffectiveDate", SqlDbType.DateTime) With {.Value = customclass.EffectiveDate})
            params.Add(New SqlParameter("@Catatan", SqlDbType.VarChar, 800) With {.Value = customclass.Catatan})
            Dim strerror = New SqlParameter("@errMsg", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            params.Add(strerror)

            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spFundingPrepaymentPaymentOutProcessNew", params.ToArray)

            If (strerror.Value.ToString().Trim <> "") Then
                Throw New Exception(strerror.Value.ToString().Trim)
            End If

        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Sub

    Function FundingPrepaymentReport(CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding

        Try
            CustomClass.ListData = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure,
            "spFundingPrepaymentReport")

            Return CustomClass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Function BPKBLoanReport(CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding

        Try
            CustomClass.ListData = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure,
            "spBPKBLoan")

            Return CustomClass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Function BPKBFotoCopyRequestReport(CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Try
            CustomClass.ListData = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure,
            "spBPKBFotoCopyRequest")

            Return CustomClass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Function BPKBPickupReport(CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Try
            CustomClass.ListData = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure,
            "spBPKBPickupReport")

            Return CustomClass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Function RequestCalculationFundingPrepaymentReport(CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Try
            CustomClass.ListData = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure,
            "spRequestCalculationFundingPrepaymentReport")

            Return CustomClass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Function TAFPrepaymentReport(CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Try
            CustomClass.ListData = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure,
            "spTAFPrepaymentReport")

            Return CustomClass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function getFundingpaymentAdvance(ByVal customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim params() As SqlParameter = New SqlParameter(7) {}
        params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.FundingCoyID
        params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(1).Value = customClass.FundingContractNo
        params(2) = New SqlParameter("@JumlahKontrak", SqlDbType.SmallInt)
        params(2).Direction = ParameterDirection.Output
        params(3) = New SqlParameter("@JumlahAngsuran", SqlDbType.Money)
        params(3).Direction = ParameterDirection.Output
        params(4) = New SqlParameter("@JumlahPokok", SqlDbType.Money)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@JumlahBunga", SqlDbType.Money)
        params(5).Direction = ParameterDirection.Output
        params(6) = New SqlParameter("@RefundBunga", SqlDbType.Money)
        params(6).Direction = ParameterDirection.Output
        params(7) = New SqlParameter("@FundingbatchNo", SqlDbType.VarChar, 20)
        params(7).Value = customClass.FundingBatchNo

        Try
            SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spGetFundingpaymentAdvance", params)
            customClass.JumlahKontrak = CInt(params(2).Value)
            customClass.JumlahPokok = CDbl(params(4).Value)
            customClass.JumlahAngsuran = CDbl(params(3).Value)
            customClass.JumlahBunga = CDbl(params(5).Value)
            customClass.RefundBunga = CDbl(params(6).Value)
            Return customClass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Function disburseFundingpaymentAdvance(customClass As Parameter.DisbursePembayaranAngsuranFunding) As Parameter.DisbursePembayaranAngsuranFunding
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim params() As SqlParameter = New SqlParameter(12) {}

        If conn.State = ConnectionState.Closed Then conn.Open()
        Dim trans As SqlTransaction = conn.BeginTransaction

        params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.FundingCoyID
        params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(1).Value = customClass.FundingContractNo
        params(2) = New SqlParameter("@PrincipalPaidAmount", SqlDbType.Decimal)
        params(2).Value = customClass.PrincipalPaidAmount
        params(3) = New SqlParameter("@InterestPaidAmount", SqlDbType.Decimal)
        params(3).Value = customClass.InterestPaidAmount
        params(4) = New SqlParameter("@PenaltyPaidAmount", SqlDbType.Decimal)
        params(4).Value = customClass.PenaltyPaidAmount
        params(5) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
        params(5).Value = customClass.ValueDate
        params(6) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
        params(6).Value = customClass.ReferenceNo
        params(7) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(7).Value = customClass.BusinessDate
        params(8) = New SqlParameter("@bankAccountID", SqlDbType.VarChar, 10)
        params(8).Value = customClass.BankAccountId
        params(9) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
        params(9).Value = customClass.BranchId
        params(10) = New SqlParameter("@LoginId", SqlDbType.VarChar, 12)
        params(10).Value = customClass.LoginId
        params(11) = New SqlParameter("@companyID", SqlDbType.VarChar, 3)
        params(11).Value = customClass.CompanyID
        params(12) = New SqlParameter("@PPHPaid", SqlDbType.Decimal)
        params(12).Value = customClass.PPHPaid
        params(13) = New SqlParameter("@FundingBatchNo", SqlDbType.DateTime)
        params(13).Value = customClass.FundingBatchNo

        Return customClass

        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "disburseFundingFundingPaymentAdvance", params)
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Function


    Function bayarBatchJthTempoList(customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim params() As SqlParameter = New SqlParameter(2) {}

        If conn.State = ConnectionState.Closed Then conn.Open()
        Dim trans As SqlTransaction = conn.BeginTransaction

        params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.FundingCoyID
        params(1) = New SqlParameter("@BankID", SqlDbType.VarChar, 20)
        params(1).Value = customClass.FundingBankID
        params(2) = New SqlParameter("@DueDate", SqlDbType.SmallDateTime)
        params(2).Value = customClass.InstallmentDueDate

        Try
            customClass.ListData = SqlHelper.ExecuteDataset(trans, CommandType.StoredProcedure, "spFundingBayarBatchJthTempoList", params)
            trans.Commit()
            Return customClass
        Catch ex As Exception
            trans.Rollback()
            Throw New Exception(ex.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function

    Function disburseBatchPerjtTempo(customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim params() As SqlParameter = New SqlParameter(9) {}

        If conn.State = ConnectionState.Closed Then conn.Open()
        Dim trans As SqlTransaction = conn.BeginTransaction

        params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.FundingCoyID
        params(1) = New SqlParameter("@BankID", SqlDbType.VarChar, 20)
        params(1).Value = customClass.FundingBankID
        params(2) = New SqlParameter("@DueDate", SqlDbType.SmallDateTime)
        params(2).Value = customClass.InstallmentDueDate
        params(3) = New SqlParameter("@ReferenceNo", SqlDbType.VarChar, 20)
        params(3).Value = customClass.referenceno
        params(4) = New SqlParameter("@Valuedate", SqlDbType.SmallDateTime)
        params(4).Value = customClass.valuedate
        params(5) = New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime)
        params(5).Value = customClass.BusinessDate
        params(6) = New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10)
        params(6).Value = customClass.bankaccountid
        params(7) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
        params(7).Value = customClass.notes
        params(8) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(8).Value = customClass.BranchId
        params(9) = New SqlParameter("@LoginID", SqlDbType.VarChar, 12)
        params(9).Value = customClass.LoginId

        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "spDisburseBatchPerjtTempo", params)
            trans.Commit()
            Return customClass
        Catch ex As Exception
            trans.Rollback()
            Throw New Exception(ex.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
    Function AppendSelectionAgreementBatch(ByVal customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim params() As SqlParameter = New SqlParameter(9) {}

        If conn.State = ConnectionState.Closed Then conn.Open()
        Dim trans As SqlTransaction = conn.BeginTransaction

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.ApplicationID
        params(1) = New SqlParameter("@Rate", SqlDbType.Decimal)
        params(1).Value = customClass.InterestRate
        params(2) = New SqlParameter("@PrincipalAmount", SqlDbType.Decimal)
        params(2).Value = customClass.PrincipalAmtToFunCoy
        params(3) = New SqlParameter("@Tenor", SqlDbType.Int)
        params(3).Value = customClass.Tenor
        params(4) = New SqlParameter("@DueDate", SqlDbType.SmallDateTime)
        params(4).Value = customClass.DueDate
        params(5) = New SqlParameter("@InstallmentAmount", SqlDbType.Decimal)
        params(5).Value = customClass.InstallmentAmount
        params(6) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
        params(6).Value = customClass.FundingCoyId
        params(7) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(7).Value = customClass.FundingContractNo
        params(8) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(8).Value = customClass.FundingBatchNo
        params(9) = New SqlParameter("@LastPayment", SqlDbType.SmallDateTime)
        params(9).Value = customClass.BusinessDate

        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "spAppandSelectionFundingJF", params)
            trans.Commit()
            Return customClass
        Catch ex As Exception
            trans.Rollback()
            Throw New Exception(ex.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
    Function RemoveSelectionAgreementBatch(ByVal oCustom As Parameter.FundingContractBatch) As Parameter.FundingContractBatch
        Dim conn As New SqlConnection(oCustom.strConnection)
        Dim params() As SqlParameter = New SqlParameter(0) {}

        If conn.State = ConnectionState.Closed Then conn.Open()
        Dim trans As SqlTransaction = conn.BeginTransaction
        params(0) = New SqlParameter("@dt", SqlDbType.Structured)
        params(0).Value = oCustom.Listdata

        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "spDeleteSelectionFundingJF", params)
            trans.Commit()
            Return oCustom
        Catch ex As Exception
            trans.Rollback()
            Throw New Exception(ex.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
    Function InstallmentScheduleTL(customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim params() As SqlParameter = New SqlParameter(2) {}

        If conn.State = ConnectionState.Closed Then conn.Open()
        Dim trans As SqlTransaction = conn.BeginTransaction

        params(0) = New SqlParameter("@FundingCoyId", SqlDbType.VarChar, 20)
        params(0).Value = customClass.FundingCoyID
        params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
        params(1).Value = customClass.FundingContractNo
        params(2) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
        params(2).Value = customClass.FundingBatchNo

        Try
            customClass.ListData = SqlHelper.ExecuteDataset(trans, CommandType.StoredProcedure, "spFundingInstallmentScheduleTL", params)
            trans.Commit()
            Return customClass
        Catch ex As Exception
            trans.Rollback()
            Throw New Exception(ex.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Function
End Class
