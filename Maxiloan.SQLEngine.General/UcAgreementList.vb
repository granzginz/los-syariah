
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Public Class UcAgreementList : Inherits DataAccessBase
    Private Const SQLEXECSP_UCAGREEMENT_LIST As String = "spUcAgreementList"
    Private Const PARAM_CUSTOMERID As String = "@CustomerID"
    Public Function UcAgreementList(ByVal customclass As Parameter.AccMntBase) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("Nomor", GetType(String))
        dttResult.Columns.Add("ApplicationID", GetType(String))
        dttResult.Columns.Add("AgreementNo", GetType(String))
        dttResult.Columns.Add("AgreementDate", GetType(String))
        dttResult.Columns.Add("InstallmentAmount", GetType(String))
        dttResult.Columns.Add("ApplicationStep", GetType(String))
        dttResult.Columns.Add("ContractStatus", GetType(String))

        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter(PARAM_CUSTOMERID, SqlDbType.VarChar, 20)
        params(0).Value = customclass.CustomerID

        Try
            reader = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, SQLEXECSP_UCAGREEMENT_LIST, params)

            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("Nomor") = reader("Nomor").ToString
                dtrResult("ApplicationID") = reader("ApplicationID").ToString
                dtrResult("AgreementNo") = reader("AgreementNo").ToString
                If IsDBNull(reader("AgreementDate")) Then
                    dtrResult("AgreementDate") = ""
                Else
                    dtrResult("AgreementDate") = Format(reader("AgreementDate"), "dd/M/yyyy")
                End If
                'dtrResult("AgreementDate") = IIf(IsDBNull(reader("AgreementDate")), "", Format(reader("AgreementDate"), "dd/M/yyyy"))
                dtrResult("InstallmentAmount") = reader("InstallmentAmount").ToString
                dtrResult("ApplicationStep") = reader("ApplicationStep").ToString
                dtrResult("ContractStatus") = reader("ContractStatus").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("UcAgreementList", "UcAgreementList", exp.Message + exp.StackTrace)
        End Try
    End Function

End Class
