﻿

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region     '

Public Class Login : Inherits DataAccessBase

#Region "Constanta"
    Private Const GETCHECKBRANCH As String = "spCheckBranch"
    Private Const GETCHECKMULTIBRANCH As String = "spCheckMultiBranch"
    Private Const GETAPPLICATION As String = "spGetLoginApplication"
    Private Const SPGETBUSINESSDATE As String = "spGetBusinessDate"
    Private Const SPGETUSER As String = "spGetUser"
    Private Const SPGETEMPLOYEE As String = "spGetEmployee"
    Private Const SPSETSESSION As String = "spSetSession"
    Private Const SPGETGROUPDB As String = "spGetGroupDB"

    Private Const GETLISTTREE As String = "spListTreeMenu"
    Private Const UPDATELISTTREE As String = "spUpdateTreeMenu"
    Private Const SPWRITEXMLFILE As String = "spWriteToFileXML"
    Private Const SPWRITEXMLFORUSER As String = "spWriteXmlforUser"
    Private Const SPCOUNTWRONGPASSWORD As String = "spCountWrongPwd"
    Private Const SPSELECTPWDSETTING As String = "spSelectPWDSetting"
    Private Const SPISPWDEXPIRED As String = "spIsPwdExpired"
    Private Const SPSELECTPASSWORD As String = "spSelectPassword"

    Private Const PARAM_APPLICATIONID As String = "@applicationid"
    Private Const PARAM_EMPLOEYEID As String = "@employeeid"
    Private Const PARAM_GROUPDBID As String = "@groupdbid"
    Private Const PARAM_ISEOD As String = "@iseod"
    Private Const PARAM_ISCLOSED As String = "@isclosed"
    Private Const PARAM_SERVERNAME As String = "@servername"
    Private Const PARAM_DBNAME As String = "@dbname"
    Private Const PARAM_FULLNAME As String = "@fullname"
    Private Const PARAM_SQLPASSWORD As String = "@sqlpassword"
    Private Const PARAM_BRANCHNAME As String = "@branchname"
    Private Const PARAM_ISUPDATE As String = "@isupdate"
    Private Const PARAM_ISMULTIBRANCH As String = "@ismultibranch"
    Private Const PARAM_ISHOBRANCH As String = "@ishobranch"
    Private Const PARAM_COUNTNUMBER As String = "@CountNumber"
    Private Const PARAM_ISRESET As String = "@IsReset"
    Private Const SPCOMPANYADDRESS As String = "spCompanyAddressList"
    Private Const PARAM_RESULT As String = "@Result"

    Private m_connection As SqlConnection
#End Region

    Public Sub New()
        MyBase.New()

        'access of shared member is not evaluated
        'm_connection = New SqlConnection(Me.GetReferenceDataConnectionString)

        m_connection = New SqlConnection(GetReferenceDataConnectionString)
    End Sub

#Region "AM_LOGIN"

    Public Function ListPasswordHistory(ByVal oCustomClass As Parameter.Login) As Parameter.Login        
        Dim params() As SqlParameter = New SqlParameter(0) {}

        Try
            params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(0).Value = oCustomClass.LoginId

            oCustomClass = Me.PasswordSetting()
            oCustomClass.ListPassword = SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, SPSELECTPASSWORD, params).Tables(0)
            Return oCustomClass
        Catch exp As Exception
            Return Nothing
            WriteException("Login", "CountWrongPwd", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function IsPasswordExpired(ByVal oCustomClass As Parameter.Login) As Boolean    
        Dim params() As SqlParameter = New SqlParameter(2) {}

        Try
            params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(0).Value = oCustomClass.LoginId
            params(1) = New SqlParameter(PARAM_BUSINESSDATE, SqlDbType.DateTime)
            params(1).Value = oCustomClass.BusinessDate
            params(2) = New SqlParameter(PARAM_RESULT, SqlDbType.Bit)
            params(2).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, SPISPWDEXPIRED, params)

            Return CType(params(2).Value, Boolean)
        Catch exp As Exception
            Return False
            WriteException("Login", "CountWrongPwd", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function PasswordSetting() As Parameter.Login
        Dim oCustomClass As New Parameter.Login
        Dim objread As SqlDataReader

        Try
            objread = SqlHelper.ExecuteReader(m_connection, CommandType.StoredProcedure, SPSELECTPWDSETTING)
            While objread.Read
                With oCustomClass
                    .PwdHistory = CType(objread("PwdHistory"), Integer)
                    .PwdLength = CType(objread("PwdLength"), Integer)
                    .PwdFailed = CType(objread("PwdFailed"), Integer)
                    .PwdExpired = CType(objread("PwdExpired"), Integer)
                End With
            End While
            objread.Close()

            Return oCustomClass
        Catch exp As Exception
            Return oCustomClass
            WriteException("Login", "CheckBranch", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function CheckBranch(ByVal oCustomClass As Parameter.Login) As Parameter.Login
        Dim oConn As New SqlConnection(oCustomClass.strConnection)


        Dim params() As SqlParameter = New SqlParameter(0) {}
        Dim objread As SqlDataReader


        Try

            If oConn.State = ConnectionState.Closed Or oConn.State = ConnectionState.Broken Then oConn.Open()

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = oCustomClass.BranchId



            'objread = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, GETCHECKBRANCH, params)
            objread = SqlHelper.ExecuteReader(oConn, CommandType.StoredProcedure, GETCHECKBRANCH, params)
            oCustomClass.BranchId = ""

            While objread.Read
                With oCustomClass
                    .IsClosed = CBool(objread("isclosed"))
                    .IsEod = CBool(objread("iseod"))
                    .BusinessDate = CDate(objread("BusinessDate"))
                    .BranchId = .BranchId & CStr(IIf(.BranchId = "", "", ",")) & "'" & CStr(objread("branchid")) & "'"
                    .IsHoBranch = CBool(objread("ishobranch"))
                End With
            End While
            objread.Close()

            Return oCustomClass
        Catch exp As Exception
            Return oCustomClass
            WriteException("Login", "CheckBranch", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function CheckMultiBranch(ByVal oCustomClass As Parameter.Login) As Parameter.Login
        Dim params() As SqlParameter = New SqlParameter(4) {}

        Try
            params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(0).Value = oCustomClass.LoginId

            params(1) = New SqlParameter(PARAM_APPID, SqlDbType.VarChar, 10)
            params(1).Value = oCustomClass.Applicationid

            params(2) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(2).Direction = ParameterDirection.Output

            params(3) = New SqlParameter(PARAM_BRANCHNAME, SqlDbType.VarChar, 50)
            params(3).Direction = ParameterDirection.Output

            params(4) = New SqlParameter(PARAM_ISMULTIBRANCH, SqlDbType.Bit)
            params(4).Direction = ParameterDirection.Output

            With oCustomClass
                .ListMultiBranch = SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, GETCHECKMULTIBRANCH, params).Tables(0)
                .IsMultiBranch = CBool(params(4).Value)
                .BranchId = CStr(params(2).Value)
                .BranchName = CStr(params(3).Value)
            End With
            m_connection.Dispose()
            Return oCustomClass
        Catch exp As Exception
            Return oCustomClass
            WriteException("Login", "CheckMultiBranch", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetLoginData() As Parameter.Login
        Dim oCustomClass As New Parameter.Login

        Try
            Dim par() As SqlParameter = New SqlParameter(0) {}

            par(0) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
            par(0).Value = getReferenceDBName()


            oCustomClass.LoginApplication = SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, GETAPPLICATION, par).Tables(0)
            m_connection.Dispose()
            Return oCustomClass
        Catch exp As Exception
            Return oCustomClass
            WriteException("Login", "GetLoginData", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetBusinessDate(ByVal strConnection As String, ByVal branchid As String) As Date
        Dim params() As SqlParameter = New SqlParameter(0) {}

        Try
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 2)
            params(0).Value = branchid
            Return CType(SqlHelper.ExecuteScalar(strConnection, CommandType.StoredProcedure, SPGETBUSINESSDATE, params), Date)
        Catch exp As Exception
            Return Now
            WriteException("Login", "GetBusinessDate", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetUser(ByVal oCustomClass As Parameter.Login) As Parameter.Login
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}

        Try
            params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(0).Value = oCustomClass.LoginId

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 10)
            params(1).Value = oCustomClass.Applicationid

            reader = SqlHelper.ExecuteReader(m_connection, CommandType.StoredProcedure, SPGETUSER, params)

            If reader.Read Then
                With oCustomClass
                    .LoginId = CType(reader("loginid"), String).Trim
                    .Password = CType(reader("password"), String).Trim
                    .IsActive = CType(reader("Active"), Boolean)
                End With
            Else
                oCustomClass.LoginId = ""
                oCustomClass.Password = ""
            End If
            m_connection.Dispose()
            Return oCustomClass
        Catch exp As Exception
            Return oCustomClass
            WriteException("Login", "GetUser", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetEmployee(ByVal oCustomClass As Parameter.Login) As String        
        Dim params() As SqlParameter = New SqlParameter(1) {}

        Try
            params(0) = New SqlParameter(PARAM_EMPLOEYEID, SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.LoginId

            params(1) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
            params(1).Value = getReferenceDBName()

            Dim strLogin As String

            strLogin = SqlHelper.ExecuteScalar(oCustomClass.strConnection, CommandType.StoredProcedure, SPGETEMPLOYEE, params).ToString

            Return strLogin
        Catch exp As Exception
            Return ""
            WriteException("Login", "GetEmployee", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function CountWrongPwd(ByVal oCustomClass As Parameter.Login) As Int16        
        Dim params() As SqlParameter = New SqlParameter(2) {}

        Try
            params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(0).Value = oCustomClass.LoginId
            params(1) = New SqlParameter(PARAM_ISRESET, SqlDbType.Bit)
            params(1).Value = oCustomClass.IsReset
            params(2) = New SqlParameter(PARAM_COUNTNUMBER, SqlDbType.Int)
            params(2).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, SPCOUNTWRONGPASSWORD, params)

            Return CType(params(2).Value, Int16)
        Catch exp As Exception
            Return 0
            WriteException("Login", "CountWrongPwd", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function SetSession(ByVal oCustomClass As Parameter.Login) As Parameter.Login
        Dim params() As SqlParameter = New SqlParameter(16) {}


        Try
            If m_connection.State = ConnectionState.Closed Or m_connection.State = ConnectionState.Broken Then m_connection.Open()

            params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(0).Value = oCustomClass.LoginId

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 10)
            params(1).Value = oCustomClass.Applicationid

            params(2) = New SqlParameter(PARAM_GROUPDBID, SqlDbType.VarChar, 3)
            params(2).Value = oCustomClass.GroupDBID

            params(3) = New SqlParameter(PARAM_SERVERNAME, SqlDbType.VarChar, 50)
            params(3).Direction = ParameterDirection.Output

            params(4) = New SqlParameter(PARAM_DBNAME, SqlDbType.VarChar, 50)
            params(4).Direction = ParameterDirection.Output

            params(5) = New SqlParameter(PARAM_FULLNAME, SqlDbType.VarChar, 50)
            params(5).Direction = ParameterDirection.Output

            params(6) = New SqlParameter(PARAM_SQLPASSWORD, SqlDbType.VarChar, 20)
            params(6).Direction = ParameterDirection.Output

            params(7) = New SqlParameter("@ServerNameRS", SqlDbType.VarChar, 50)
            params(7).Direction = ParameterDirection.Output

            params(8) = New SqlParameter("@DatabaseNameRS", SqlDbType.VarChar, 50)
            params(8).Direction = ParameterDirection.Output

            params(9) = New SqlParameter("@VirtualDirectoryRS", SqlDbType.VarChar, 50)
            params(9).Direction = ParameterDirection.Output

            params(10) = New SqlParameter("@ReportManagerRS", SqlDbType.VarChar, 50)
            params(10).Direction = ParameterDirection.Output

            params(11) = New SqlParameter("@UIDRS", SqlDbType.VarChar, 50)
            params(11).Direction = ParameterDirection.Output

            params(12) = New SqlParameter("@PassRS", SqlDbType.VarChar, 50)
            params(12).Direction = ParameterDirection.Output

            params(13) = New SqlParameter("@CoyID", SqlDbType.Char, 3)
            params(13).Direction = ParameterDirection.Output

            params(14) = New SqlParameter("@CompanyName", SqlDbType.VarChar, 50)
            params(14).Direction = ParameterDirection.Output

            params(15) = New SqlParameter("@EmpPos", SqlDbType.VarChar, 10)
            params(15).Direction = ParameterDirection.Output

            params(16) = New SqlParameter("@BranchCity", SqlDbType.VarChar, 50)
            params(16).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, SPSETSESSION, params)

            With oCustomClass
                .ServerName = CType(params(3).Value, String).Trim
                .DBName = CType(params(4).Value, String).Trim
                .FullName = CType(params(5).Value, String).Trim
                .Password = CType(params(6).Value, String).Trim
                .ServerNameRS = params(7).Value.ToString.Trim
                .DatabaseNameRS = params(8).Value.ToString.Trim
                .VirtualDirectoryRS = params(9).Value.ToString.Trim
                .ReportManagerRS = params(10).Value.ToString.Trim
                .UIDRS = params(11).Value.ToString.Trim
                .PassRS = params(12).Value.ToString.Trim
                .CoyID = params(13).Value.ToString.Trim
                .CompanyID = params(13).Value.ToString.Trim
                .CompanyName = params(14).Value.ToString.Trim
                .ApplicationManagerDB = getReferenceDBName()
                .PassKeyPrefix = GetPassKeyPrefix()
                .EmpPos = params(15).Value.ToString.Trim
                .BranchCity = params(16).Value.ToString.Trim
                .PoolSize = getPoolSize()
            End With

            Return oCustomClass
        Catch exp As Exception
            Return oCustomClass
            WriteException("Login", "SetSession", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "AM_TREE"
    Public Function ListTreeMenu(ByVal oCustomClass As Parameter.Login) As Boolean
        Dim params() As SqlParameter = New SqlParameter(2) {}        

        Try
            params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(0).Value = oCustomClass.LoginId

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 10)
            params(1).Value = oCustomClass.Applicationid

            params(2) = New SqlParameter(PARAM_ISUPDATE, SqlDbType.Bit)
            params(2).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, GETLISTTREE, params)
            Return CType(params(2).Value, Boolean)
        Catch exp As Exception
            Return False
            WriteException("Login", "ListTreeMenu", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function UpdateTreeMenu(ByVal oCustomClass As Parameter.Login) As String
        Dim dsListTreeMenu As New DataSet
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(0).Value = oCustomClass.LoginId

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 10)
            params(1).Value = oCustomClass.Applicationid

            params(2) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(2).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, UPDATELISTTREE, params)

            Return CType(params(2).Value, String)
        Catch exp As Exception
            Return ""
            WriteException("Login", "UpdateTreeMenu", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function WriteFileXML(ByVal oCustomClass As Parameter.Login) As DataTable
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 10)
            params(0).Value = oCustomClass.Applicationid

            Return (SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, SPWRITEXMLFILE, params).Tables(0))
        Catch exp As Exception
            Return Nothing
            WriteException("Login", "WriteFileXML", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function WriteXMLForUser(ByVal oCustomClass As Parameter.Login) As DataTable
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(0).Value = oCustomClass.LoginId

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 10)
            params(1).Value = oCustomClass.Applicationid

            params(2) = New SqlParameter(PARAM_GROUPDBID, SqlDbType.VarChar, 3)
            params(2).Value = oCustomClass.GroupDBID

            Return (SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, SPWRITEXMLFORUSER, params).Tables(0))
        Catch exp As Exception
            Return Nothing
            WriteException("Login", "WriteXMLForUser", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

    Public Function GetCompanyAddress(ByVal strConnection As String, ByVal StrCompanyID As String) As String
        Dim params() As SqlParameter = New SqlParameter(0) {}        
        Try
            params(0) = New SqlParameter(PARAM_COMPANYID, SqlDbType.Char, 3)
            params(0).Value = StrCompanyID
            Return CStr(SqlHelper.ExecuteScalar(strConnection, CommandType.StoredProcedure, SPCOMPANYADDRESS, params))
        Catch exp As Exception
            Return Nothing
            WriteException("Login", "GetCompanyAddress", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Sub KillUser(ByVal oClass As Parameter.Login)
        Dim oConn As New SqlConnection(oClass.strConnection)
        Dim par() As SqlParameter = New SqlParameter(1) {}

        Try
            If oConn.State = ConnectionState.Closed Or oConn.State = ConnectionState.Broken Then oConn.Open()

            par(0) = New SqlParameter("@LoginID", SqlDbType.VarChar, 50)
            par(0).Value = oClass.LoginId
            par(1) = New SqlParameter("@DBName", SqlDbType.VarChar, 50)
            par(1).Value = oClass.DBName

            'SqlHelper.ExecuteNonQuery(oClass.strConnection, CommandType.StoredProcedure, "spKillUser", par)

            SqlHelper.ExecuteNonQuery(oConn, CommandType.StoredProcedure, "spKillUser", par)

        Catch ex As Exception

        Finally
            If oConn.State = ConnectionState.Open Then oConn.Close()
            oConn.Dispose()
        End Try


    End Sub

    Public Sub setloghistory(ByVal oClass As Parameter.Login)
        Dim oConn As New SqlConnection(oClass.strConnection)
        Dim par() As SqlParameter = New SqlParameter(3) {}

        Try
            If oConn.State = ConnectionState.Closed Or oConn.State = ConnectionState.Broken Then oConn.Open()

            par(0) = New SqlParameter("@branchid", SqlDbType.Char, 3)
            par(0).Value = oClass.BranchId
            par(1) = New SqlParameter("@LoginID", SqlDbType.Char, 12)
            par(1).Value = oClass.LoginId
            par(2) = New SqlParameter("@activityid", SqlDbType.SmallInt)
            par(2).Value = oClass.activityid
            par(3) = New SqlParameter("@formid", SqlDbType.VarChar, 20)
            par(3).Value = oClass.FormID

            SqlHelper.ExecuteNonQuery(oConn, CommandType.StoredProcedure, "spsetloghistory", par)

        Catch ex As Exception

        Finally
            If oConn.State = ConnectionState.Open Then oConn.Close()
            oConn.Dispose()
        End Try


    End Sub
End Class

