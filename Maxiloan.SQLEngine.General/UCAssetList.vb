
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

Public Class UcAssetList : Inherits DataAccessBase
    Private Const SP_UCAsset_LIST As String = "spUcAssetList"
    Private Const PARAM_APPLICATIONID As String = "@ApplicationID"

    Public Function UCAssetList(ByVal customclass As Parameter.AccMntBase) As DataTable
        Dim params(0) As SqlParameter
        With customclass
            params(0) = New SqlParameter("@PARAM_APPLICATIONID", SqlDbType.Char, 20)
            params(0).Value = .ApplicationID
        End With
        Try
            Dim oAssetList As New Parameter.AccMntBase
            oAssetList.listdata = SqlHelper.ExecuteDataset(oAssetList.strConnection, CommandType.StoredProcedure, SP_UCAsset_LIST, params).Tables(0)
            Return oAssetList.listdata
        Catch exp As Exception
            WriteException("UcAssetList", "UCAssetList", exp.Message + exp.StackTrace)
        End Try
    End Function

End Class

