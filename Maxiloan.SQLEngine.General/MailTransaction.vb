
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.General
#End Region

Public Class MailTransaction : Inherits DataAccessBase
#Region "Constanta"
    Private Const SP_INSERT_TO_MAILTRANS As String = "spInsRenewalLetterInsertMail"
#End Region
#Region "PrintMailTransaction"

    Public Function PrintMailTransaction(ByVal customClass As Parameter.MailTransaction) As Parameter.MailTransaction

        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            Dim params(8) As SqlParameter
            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20)
            params(1).Value = customClass.ApplicationID
            params(2) = New SqlParameter("@MailTypeId", SqlDbType.VarChar, 10)
            params(2).Value = customClass.MailTypeID
            params(3) = New SqlParameter("@MailTransNo", SqlDbType.VarChar, 50)
            params(3).Value = customClass.MailTransNo
            params(4) = New SqlParameter("@MailDateCreate", SqlDbType.DateTime)
            params(4).Value = customClass.MailDateCreate
            params(5) = New SqlParameter("@MailUserCreate", SqlDbType.VarChar, 50)
            params(5).Value = customClass.MailUserCreate
            params(6) = New SqlParameter("@MailDatePrint", SqlDbType.DateTime)
            params(6).Value = customClass.MailDatePrint
            params(7) = New SqlParameter("@MailPrintedNum", SqlDbType.Int)
            params(7).Value = customClass.MailPrintedNum
            params(8) = New SqlParameter("@MailUserPrint", SqlDbType.VarChar, 50)
            params(8).Value = customClass.MailUserPrint

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SP_INSERT_TO_MAILTRANS, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Dim err As New MaxiloanExceptions
            err.WriteLog("MailTransaction.vb", "PrintMailTransaction - DataAccess.Common", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            Throw New Exception(exp.Message)
            WriteException("MailTransaction", "PrintMailTransaction", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Function

#End Region
End Class
