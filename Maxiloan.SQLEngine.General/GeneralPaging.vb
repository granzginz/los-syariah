#Region "Imports "
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.General
Imports Maxiloan.Exceptions
#End Region

Public Class GeneralPaging : Inherits DataAccessBase
    Private Const SP_GET_INS_BRANCH_ALL As String = "spGetInsuranceCompanyBranchALL"
    Private Const spARMutation As String = "spARMutationReport"
    Private Const SPAPREPORT As String = "spAPReport"
    Private Const SPAPTOINSREPORT As String = "spAP2InsReport"
    Private Const SPSUSPENDREPORT As String = "spSuspendReport"
    Private Const SPPREPAIDREPORT As String = "spPrepaidReport"
    Private Const SPBIQUALITYREPORT As String = "spBIQualityReport"
    Private Const spPrepaymentTerminationReport As String = "spPrepaymentTerminationReport"
    Private Const spAssetRepossessionOrderTerminationReport As String = "spAssetRepossessionOrderTerminationReport"


#Region "GetReportWithParameterWhereCond"

    Public Function GetReportWithParameterWhereCond(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@WhereBy", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        Try
            customClass.ListDataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params)
            Return customClass
        Catch exp As Exception
            WriteException("GeneralPaging", "GetReportWithParameterWhereCond", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "GetReportWithTwoParameter"

    Public Function GetReportWithTwoParameter(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging

        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@WhereBy", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(1).Value = customClass.BusinessDate
        Try
            customClass.ListDataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params)
            Return customClass
        Catch exp As Exception
            WriteException("GeneralPaging", "GetReportWithTwoParameter", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "GetReportWithParameterWhereAndSort"

    Public Function GetReportWithParameterWhereAndSort(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging

        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@WhereBy", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
        params(1).Value = customClass.SortBy

        Try
            customClass.ListDataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params)
            Return customClass
        Catch exp As Exception
            WriteException("GeneralPaging", "GetReportWithParameterWhereAndSort", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "GetRecordWithoutParameter"
    Public Function GetRecordWithoutParameter(ByVal strConnection As String) As DataTable

        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("BranchID", GetType(String))
        dttResult.Columns.Add("ChildID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))

        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SP_GET_INS_BRANCH_ALL)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("BranchID") = reader("BranchID").ToString
                dtrResult("ChildID") = reader("ChildID").ToString
                dtrResult("Name") = reader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("GeneralPaging", "GetRecordWithoutParameter", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetRecordWithoutParameterAll(ByVal oCustom As Parameter.GeneralPaging) As DataTable
        Try
            Return SqlHelper.ExecuteDataset(oCustom.strConnection, CommandType.StoredProcedure, oCustom.SpName).Tables(0)
        Catch exp As Exception
            WriteException("GeneralPaging", "GetRecordWithoutParameter", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "GetGeneralPaging"

    Public Function ARMutationReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
        Dim params(1) As SqlParameter
        Try

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@ARDate", SqlDbType.Char)
            params(1).Value = customclass.ARDate
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spARMutation, params)
        Catch exp As Exception
            WriteException("ARMutation", "ARMutationReport", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function APMutationReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
        Dim params(2) As SqlParameter
        Try

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@APDate", SqlDbType.Char)
            params(1).Value = customclass.APDate
            params(2) = New SqlParameter("@APType", SqlDbType.Char)
            params(2).Value = customclass.WhereCond
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, customclass.SpName, params)
        Catch exp As Exception
            WriteException("APReport", "APReport", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function APToInsReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
        Dim params(1) As SqlParameter
        Try

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@APDate", SqlDbType.Char)
            params(1).Value = customclass.APDate
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPAPTOINSREPORT, params)
        Catch exp As Exception
            WriteException("APToInsReport", "APReport", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function SuspendReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
        Dim params(1) As SqlParameter
        Try

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@SuspenDate", SqlDbType.Char)
            params(1).Value = customclass.SuspendDate
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPSUSPENDREPORT, params)
        Catch exp As Exception
            WriteException("APToInsReport", "APReport", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function PrepaidReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
        Dim params(1) As SqlParameter
        Try

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@APDate", SqlDbType.Char)
            params(1).Value = customclass.APDate
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPPREPAIDREPORT, params)
        Catch exp As Exception
            WriteException("PrepaidReport", "PrepaidReport", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function BIQualityReport(ByVal customclass As Parameter.GeneralPaging) As DataSet
        Dim params(0) As SqlParameter
        Try
            params(0) = New SqlParameter("@Period", SqlDbType.Char)
            params(0).Value = customclass.APDate
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPBIQUALITYREPORT, params)
        Catch exp As Exception
            WriteException("BIQualityReport", "BIQualityReport", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetGeneralPaging(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim oReturnValue As New Parameter.GeneralPaging


        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output


        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try


    End Function

    Public Function GetReportWithBranchAndstrDate(ByVal customclass As Parameter.GeneralPaging) As DataSet
        Dim params(1) As SqlParameter
        Try

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@strDate", SqlDbType.Char, 10)
            params(1).Value = customclass.strDate
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, customclass.SpName, params)
        Catch exp As Exception
            WriteException("GetReportWithBranchAndstrDate", "GetReportWithBranchAndstrDate", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetReportWithBranch_strDate_Where(ByVal customclass As Parameter.GeneralPaging) As DataSet
        Dim params(2) As SqlParameter
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId
            params(1) = New SqlParameter("@strDate", SqlDbType.Char, 10)
            params(1).Value = customclass.strDate
            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, customclass.SpName, params)
        Catch exp As Exception
            WriteException("GetReportWithBranch_strDate_Where", "GetReportWithBranch_strDate_Where", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

    Public Function DeleteGeneral(ByVal customclass As Parameter.Common) As Boolean
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter("@TableDelete", SqlDbType.VarChar, 20)
        params(0).Value = customclass.tableName

        params(1) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(1).Value = customclass.WhereCond

        params(2) = New SqlParameter("@saveStatus", SqlDbType.Bit)
        params(2).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "SpDeleteGeneral", params)
            Return CType(params(2).Value, Boolean)
        Catch exp As MaxiloanExceptions
            WriteException("DeleteGeneral", "DeleteGeneral", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("GeneralPaging.vb", "DeleteGeneral", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
        End Try
    End Function
    Public Function GetGeneralPagingWithTwoWhereCondition(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim oReturnValue As New Parameter.GeneralPaging
        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond1
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@WhereCond2", SqlDbType.VarChar, 1000)
        params(5).Value = customClass.WhereCond2
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As MaxiloanExceptions
            Dim err As New MaxiloanExceptions
            err.WriteLog("GeneralPaging", "GetGeneralPagingWithTwoWhereCondition", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            WriteException("GeneralPaging", "GetGeneralPagingWithTwoWhereCondition", exp.Message + exp.StackTrace)
        End Try
    End Function


#Region "GetBranchCombo"

    Public Function GetBranchCombo(ByVal oClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 200)
        params(0).Value = oClass.BranchId

        Try
            Dim oList As New Parameter.InsCoAllocationDetailList
            oList.ListData = SqlHelper.ExecuteDataset(oClass.strConnection, CommandType.StoredProcedure, "spgetbranch", params).Tables(0)
            Return oList

        Catch exp As Exception
            WriteException("GeneralPaging", "GetBranchCombo", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "GetInsuranceBranchCombo"

    Public Function GetInsuranceBranchCombo(ByVal oClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 200)
        params(0).Value = oClass.BranchId

        Try
            Dim oList As New Parameter.InsCoAllocationDetailList

            oList.ListData = SqlHelper.ExecuteDataset(oClass.strConnection, CommandType.StoredProcedure, "spGetInsuranceCompanyBranchALL", params).Tables(0)
            Return oList

        Catch exp As Exception
            WriteException("GeneralPaging", "GetInsuranceBranchCombo", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "GetGeneralSP"
    Public Function GetGeneralSP(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim oReturnValue As New Parameter.GeneralPaging
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@WhereCond1", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond1
        params(1) = New SqlParameter("@WhereCond2", SqlDbType.VarChar, 1000)
        params(1).Value = customClass.WhereCond2
        params(2) = New SqlParameter("@WhereCond3", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond3
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("GeneralPaging", "GetGeneralSP", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "GetReportWithThreeParameterWhereCond"
    Public Function GetReportWithThreeParameterWhereCond(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging

        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@WhereBy", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter("@WhereCond2", SqlDbType.VarChar, 1000)
        params(1).Value = customClass.WhereCond2
        params(2) = New SqlParameter("@WhereCond3", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond3
        Try
            customClass.ListDataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params)
            Return customClass
        Catch exp As Exception
            WriteException("GeneralPaging", "GetReportWithThreeParameterWhereCond", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "GetReportWithFiveParameterWhereCond"
    Public Function GetReportWithFiveParameterWhereCond(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@WhereBy", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter("@WhereCond1", SqlDbType.VarChar, 5)
        params(1).Value = customClass.WhereCond1
        params(2) = New SqlParameter("@WhereCond2", SqlDbType.VarChar, 5)
        params(2).Value = customClass.WhereCond2
        params(3) = New SqlParameter("@WhereCond3", SqlDbType.Char, 4)
        params(3).Value = customClass.WhereCond3
        params(4) = New SqlParameter("@WhereCond4", SqlDbType.Char, 4)
        params(4).Value = customClass.WhereCond4
        Try
            customClass.ListDataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params)
            Return customClass
        Catch exp As Exception
            WriteException("GeneralPaging", "GetReportWithThreeParameterWhereCond", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "GetReportWithParamApplicationID"
    Public Function GetReportWithParamApplicationID(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.ApplicationID
        Try
            customClass.ListDataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params)
            Return customClass
        Catch exp As Exception
            WriteException("GeneralPaging", "GetReportWithParamApplicationID", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("", "", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
        End Try
    End Function
#End Region
#Region "PrepaymentTerminationReport"
    Public Function PrepaymentTerminationReport(ByVal customclass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@Branch", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@PODateFrom", SqlDbType.Char, 10)
        params(1).Value = customclass.PODateFrom

        params(2) = New SqlParameter("@PODateTo", SqlDbType.Char, 10)
        params(2).Value = customclass.PODateTo

        Try
            customclass.ListDataReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spPrepaymentTerminationReport, params)
            Return customclass

        Catch exp As Exception
            WriteException("AssetDocument", "AssetDocumentReport", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "AssetRepossessionOrderTerminationReport"
    Public Function AssetRepossessionOrderTerminationReport(ByVal customclass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@Branch", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@PODateFrom", SqlDbType.Char, 10)
        params(1).Value = customclass.PODateFrom

        params(2) = New SqlParameter("@PODateTo", SqlDbType.Char, 10)
        params(2).Value = customclass.PODateTo

        Try
            customclass.ListDataReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAssetRepossessionOrderTerminationReport, params)
            Return customclass

        Catch exp As Exception
            WriteException("AssetDocument", "AssetDocumentReport", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "GetReportWithBranchAndTwoPeriod"
    Public Function GetReportWithBranchAndTwoPeriod(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging

        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@DateFrom", SqlDbType.DateTime)
        params(1).Value = customClass.DateFrom
        params(2) = New SqlParameter("@DateTo", SqlDbType.DateTime)
        params(2).Value = customClass.DateTo
        Try
            customClass.ListDataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, customClass.SpName, params)
            Return customClass
        Catch exp As Exception
            WriteException("GeneralPaging", "GetReportWithTwoParameter", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
    Public Function GetDailyTransReport(ByVal customClass As Parameter.GeneralPaging) As Parameter.GeneralPaging        
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@WhereBy", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(1).Value = customClass.BusinessDate
        params(2) = New SqlParameter("@isBusinessDate", SqlDbType.Bit)
        params(2).Value = customClass.isBusinessDate
        Try
            customClass.ListDataReport = SqlHelper.ExecuteDataset(customClass.strConnection, _
            CommandType.StoredProcedure, customClass.SpName, params)
            Return customClass
        Catch exp As Exception
            WriteException("GeneralPaging", "GetDailyTransReport", _
            exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
