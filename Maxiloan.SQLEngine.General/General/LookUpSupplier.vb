

#Region " Imports "
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class LookUpSupplier : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SUPPLIER As String = "spSupplierList"
    Private Const LIST_SUPPLIER_BRANCH As String = "spSupplierBranchList"
    Private Const LIST_SUPPLIER_MULTI As String = "spSupplierListMulti"
    Private Const LIST_SUPPLIER_MULTI_CONFIRM As String = "spSupplierListMultiConfirm"
#End Region

#Region " GetListData "
    Public Function GetListData(ByVal oEntities As Parameter.LookUpSupplier) As Parameter.LookUpSupplier
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oEntities.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oEntities.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = oEntities.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
        params(3).Value = oEntities.SortBy
        params(4) = New SqlParameter("@ApplicationId", SqlDbType.Char, 20)
        params(4).Value = oEntities.ApplicationId
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output

        Try
            Dim oReturnValue As New Parameter.LookUpSupplier

            oReturnValue.ListData = SqlHelper.ExecuteDataset(oEntities.strConnection, CommandType.StoredProcedure, LIST_SUPPLIER, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(5).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("LookUpSupplier", "GetListData", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region

#Region " GetListDataSupplierBranch "
    Public Function GetListDataSupplierBranch(ByVal oEntities As Parameter.LookUpSupplier) As Parameter.LookUpSupplier
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oEntities.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oEntities.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = oEntities.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
        params(3).Value = oEntities.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            Dim oReturnValue As New Parameter.LookUpSupplier

            oReturnValue.ListData = SqlHelper.ExecuteDataset(oEntities.strConnection, CommandType.StoredProcedure, LIST_SUPPLIER_BRANCH, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("LookUpSupplierBranch", "GetListData", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region

#Region " GetListDataMulti "
    Public Function GetListDataMulti(ByVal oEntities As Parameter.LookUpSupplier) As Parameter.LookUpSupplier
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oEntities.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oEntities.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = oEntities.WhereCond
        params(3) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(3).Value = oEntities.BranchId
        params(4) = New SqlParameter("@AssetUsedNew", SqlDbType.Char, 1)
        params(4).Value = oEntities.AssetUsedNew
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output
        Try
            Dim oReturnValue As New Parameter.LookUpSupplier

            oReturnValue.ListData = SqlHelper.ExecuteDataset(oEntities.strConnection, CommandType.StoredProcedure, LIST_SUPPLIER_MULTI, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(5).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("LookUpSupplier", "GetListDataMulti", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region

#Region " GetListDataMultiConfirm "
    Public Function GetListDataMultiConfirm(ByVal oEntities As Parameter.LookUpSupplier) As Parameter.LookUpSupplier
        Dim params(6) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oEntities.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oEntities.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = oEntities.WhereCond
        params(3) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(3).Value = oEntities.BranchId
        params(4) = New SqlParameter("@AssetUsedNew", SqlDbType.Char, 1)
        params(4).Value = oEntities.AssetUsedNew
        params(5) = New SqlParameter("@TotalSupplierID", SqlDbType.VarChar, 8000)
        params(5).Value = oEntities.TotalSupplierID
        params(6) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(6).Direction = ParameterDirection.Output
        Try
            Dim oReturnValue As New Parameter.LookUpSupplier

            oReturnValue.ListData = SqlHelper.ExecuteDataset(oEntities.strConnection, CommandType.StoredProcedure, LIST_SUPPLIER_MULTI_CONFIRM, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(6).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("LookUpSupplier", "GetListDataMultiConfirm", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region
End Class
