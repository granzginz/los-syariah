

#Region " Imports "
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class LookUpCustomer : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_CUSTOMER As String = "spCustomerList"
#End Region
#Region " GetListData "
    Public Function GetListData(ByVal oEntities As Parameter.LookUpCustomer) As Parameter.LookUpCustomer
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oEntities.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oEntities.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = oEntities.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
        params(3).Value = oEntities.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Decimal)
        params(4).Direction = ParameterDirection.Output

        Try
            Dim oReturnValue As New Parameter.LookUpCustomer

            oReturnValue.ListData = SqlHelper.ExecuteDataset(oEntities.strConnection, CommandType.StoredProcedure, LIST_CUSTOMER, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Decimal)
            Return oReturnValue
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
        End Try

    End Function
#End Region
End Class
