
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

Public Class LookUpTransaction : Inherits DataAccessBase
#Region " Const "

    Private Const LIST_SELECT As String = "spPagingTransaction"
    Private Const GET_PROCESSID As String = "spPagingTransactionGetDetail"
#End Region

#Region " GetListData "
    Public Function GetListData(ByVal strConnection As String, ByVal cmdWhere As String, ByVal currentPage As Integer, ByVal pagesize As Integer, ByVal SortBy As String) As Parameter.LookUpTransaction
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = currentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = pagesize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = cmdWhere
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            Dim oReturnValue As New Parameter.LookUpTransaction
            oReturnValue.ListData = SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("LookUpTransaction", "GetListData", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region " GetProcessID "
    Public Function GetProcessID(ByVal strConnection As String, ByVal TransactionID As String) As String
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@TransactionID", SqlDbType.VarChar)
        params(0).Value = TransactionID

        Try
            Dim strReturnValue As String
            strReturnValue = CType(SqlHelper.ExecuteScalar(strConnection, CommandType.StoredProcedure, GET_PROCESSID, params), String)
            Return strReturnValue
        Catch exp As Exception
            WriteException("LookUpTransaction", "GetProcessID", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

End Class
