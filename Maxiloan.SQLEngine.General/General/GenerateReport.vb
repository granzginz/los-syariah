

#Region " Imports "
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region
Public Class GenerateReport : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_ASSET As String = "spAssetList"
    Private m_connection As SqlConnection

#End Region

#Region " GetListData "
    Public Function ListReport(ByVal oEntities As Parameter.GenerateReport) As DataSet
        Dim params(5) As SqlParameter

        Try
            Return CType(SqlHelper.ExecuteDataset(oEntities.strConnection, CommandType.StoredProcedure, oEntities.StoreProcedure), DataSet)
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch exp As Exception
            WriteException("GenerateReport", "ListReport", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region
End Class
