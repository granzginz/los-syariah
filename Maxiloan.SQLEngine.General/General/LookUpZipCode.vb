

Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

Public Class LookUpZipCode : Inherits DataAccessBase

#Region " Const "
    Private Const LIST_SELECT As String = "spPagingZipCode"
#End Region
#Region "GetCitySearch"
    Public Function GetCitySearch(ByVal strConnection As String) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("City", GetType(String))

        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.Text, "Select rtrim(City) as City from Kelurahan group by City order by City ")
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("City") = reader("City").ToString
                'insert DataRow to DataTable
                dttResult.Rows.Add(dtrResult)
            End While
            Return dttResult
        Catch exp As Exception
            WriteException("LookUpZipCode", "GetCitySearch", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region " GetListData "
    Public Function GetListData(ByVal strconnection As String, ByVal cmdWhere As String, ByVal currentPage As Integer, ByVal pagesize As Integer, ByVal SortBy As String) As Parameter.LookUpZipCode
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = currentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = pagesize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = cmdWhere
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            Dim oReturnValue As New Parameter.LookUpZipCode
            oReturnValue.ListData = SqlHelper.ExecuteDataset(strconnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            WriteException("LookUpZipCode", "GetListData", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region


End Class
