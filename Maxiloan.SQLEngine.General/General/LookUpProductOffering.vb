#Region " Imports "
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class LookUpProductOffering : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "     
    Private Const LIST_PRODUCT_BRANCH As String = "spProductOfferingSelect"
    Private Const LIST_PRODUCT_OFFERING As String = "spProductOfferingList"
#End Region

#Region "GetProductBranch"
    Public Function GetProductBranch(ByVal branchid As String, ByVal strconnection As String) As DataTable        
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 3)
        params(0).Value = branchid
        Try
            Return SqlHelper.ExecuteDataset(strconnection, CommandType.StoredProcedure, LIST_PRODUCT_BRANCH, params).Tables(0)
        Catch exp As Exception            
            Throw New Exception(exp.Message)
            Return Nothing
        End Try
    End Function
#End Region

#Region " GetListData "
    Public Function GetListData(ByVal oEntities As Parameter.LookUpProductOffering) As Parameter.LookUpProductOffering
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oEntities.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oEntities.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = oEntities.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
        params(3).Value = oEntities.SortBy
        params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(4).Value = oEntities.BusDate
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output
        Try
            Dim oReturnValue As New Parameter.LookUpProductOffering
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oEntities.strConnection, CommandType.StoredProcedure, LIST_PRODUCT_OFFERING, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(5).Value, Int64)
            Return oReturnValue
        Catch exp As Exception
            Throw New Exception(exp.Message)
            Return Nothing
        End Try
    End Function
#End Region

End Class
