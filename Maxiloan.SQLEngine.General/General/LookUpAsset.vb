#Region " Imports "
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class LookUpAsset : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "    
    Private Const LIST_ASSET As String = "spAssetList"
    Private Const LIST_ASSET_MULTI As String = "spAssetCodeMulti"
    Private Const LIST_ASSET_MULTI_CONFIRM As String = "spAssetCodeMultiConfirm"
#End Region

#Region " GetListData "
    Public Function GetListData(ByVal oEntities As Parameter.LookUpAsset) As Parameter.LookUpAsset
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oEntities.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oEntities.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = oEntities.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
        params(3).Value = oEntities.SortBy
        params(4) = New SqlParameter("@ApplicationId", SqlDbType.Char, 20)
        params(4).Value = oEntities.ApplicationId
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output
        Try
            Dim oReturnValue As New Parameter.LookUpAsset
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oEntities.strConnection, CommandType.StoredProcedure, LIST_ASSET, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(5).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        End Try
    End Function
#End Region

#Region " GetListDataMulti "
    Public Function GetListDataMulti(ByVal oEntities As Parameter.LookUpAsset) As Parameter.LookUpAsset
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oEntities.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oEntities.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = oEntities.WhereCond
        params(3) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(3).Value = oEntities.BranchId
        params(4) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
        params(4).Value = oEntities.AssetTypeID
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output
        Try
            Dim oReturnValue As New Parameter.LookUpAsset
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oEntities.strConnection, CommandType.StoredProcedure, LIST_ASSET_MULTI, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(5).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        End Try
    End Function
#End Region

#Region " GetListDataMultiConfirm "
    Public Function GetListDataMultiConfirm(ByVal oEntities As Parameter.LookUpAsset) As Parameter.LookUpAsset
        Dim params(6) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oEntities.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oEntities.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = oEntities.WhereCond
        params(3) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(3).Value = oEntities.BranchId
        params(4) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
        params(4).Value = oEntities.AssetTypeID
        params(5) = New SqlParameter("@TotalAsset", SqlDbType.VarChar, 8000)
        params(5).Value = oEntities.TotalAsset
        params(6) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(6).Direction = ParameterDirection.Output
        Try
            Dim oReturnValue As New Parameter.LookUpAsset
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oEntities.strConnection, CommandType.StoredProcedure, LIST_ASSET_MULTI_CONFIRM, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(6).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region
End Class
