﻿Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

Public Class ReportingService : Inherits DataAccessBase

    Public Function getCatalogRS(ByVal oClass As Parameter.CatalogRS) As Parameter.CatalogRS
        Dim objConnection As New SqlConnection(oClass.strConnection)
        Dim oPar() As SqlParameter = New SqlParameter(2) {}
        Dim oData As New DataSet

        If objConnection.State = ConnectionState.Closed Then objConnection.Open()

        Try
            oPar(0) = New SqlParameter("@FormID", SqlDbType.VarChar, 20)
            oPar(0).Value = oClass.FormID.ToString
            oPar(1) = New SqlParameter("@ReportPath", SqlDbType.VarChar, 50)
            oPar(1).Direction = ParameterDirection.Output
            oPar(2) = New SqlParameter("@ReportName", SqlDbType.VarChar, 50)
            oPar(2).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objConnection, CommandType.StoredProcedure, "spGetCatalogRS", oPar)

            oClass.ReportPath = CType(oPar(1).Value, String)
            oClass.ReportName = CType(oPar(2).Value, String)

            Return oClass
        Catch ex As Exception            
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
