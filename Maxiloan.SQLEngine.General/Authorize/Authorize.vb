﻿
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region
Public Class Authorize : Inherits DataAccessBase

#Region "Constanta"
    Private Const SPCHECKFEATURE As String = "spFeatureCheck"
    Private Const SPCHECKFORM As String = "spFormCheck"


    Private Const PARAM_APPLICATIONID As String = "@applicationid"
    Private m_connection As SqlConnection
#End Region

    Public Function CheckForm(ByVal oCustomClass As Parameter.Common) As Boolean
        Dim params() As SqlParameter = New SqlParameter(4) {}


        Try
            params(0) = New SqlParameter(PARAM_FORMID, SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.FormID

            params(1) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(1).Value = oCustomClass.LoginId

            params(2) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 10)
            params(2).Value = oCustomClass.AppID

            params(3) = New SqlParameter(PARAM_ISAUTHORIZE, SqlDbType.Bit)
            params(3).Direction = ParameterDirection.Output

            params(4) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
            params(4).Value = getReferenceDBName()


            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SPCHECKFORM, params)

            Return CBool(params(3).Value)
        Catch exp As Exception
            WriteException("Authorize", "CheckForm", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function CheckFeature(ByVal oCustomClass As Parameter.Common) As Boolean
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_FEATUREID, SqlDbType.VarChar, 5)
            params(0).Value = oCustomClass.FeatureID

            params(1) = New SqlParameter(PARAM_FORMID, SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.FormID

            params(2) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(2).Value = oCustomClass.LoginId

            params(3) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 10)
            params(3).Value = oCustomClass.AppID

            params(4) = New SqlParameter(PARAM_ISAUTHORIZE, SqlDbType.Bit)
            params(4).Direction = ParameterDirection.Output

            params(5) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
            params(5).Value = getReferenceDBName()

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SPCHECKFEATURE, params)
            Return CBool(params(4).Value)
        Catch exp As Exception
            WriteException("Authorize", "CheckFeature", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
