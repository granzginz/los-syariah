
#Region "Imports "

Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.General
Imports Maxiloan.Exceptions
#End Region

Public Class DataUserControl : Inherits DataAccessBase

#Region " Const "

    Private Const SQLQUERY_APPLICATION_TYPE As String = "select rtrim(ID) as ID,Description as Name from tblapplicationtype"
    Private Const SQLQUERY_COPYRATE_PREMIUM_RATE_FROM_BRANCH As String = "select BranchID as ID ,BranchFullName as Name from vCopyStandardPremiumRateFromBranch"
    Private Const SQLQUERY_COPYRATE_FROM_BRANCH As String = "select distinct BranchID as ID ,BranchFullName as Name from vCopyRateFromBranch"
    Private Const SQLQUERY_COPYRATE_FROM_INSCOBRANCH As String = "select distinct insurancecoybranchid as ID,br.branchfullName as Name from InsuranceCoyBranch left join dbo.branch br on br.branchid = insurancecoybranch.branchid"
    Private Const SQLQUERY_COVERAGETYPE As String = "select id,description As Name from tblcoveragetype"
    Private Const SQLEXECSP_MASTER_ALL_COLLECTORTYPE_CL As String = "spGetCollectorTypeCL"
    Private Const SQLQUERY_MASTER_BANK As String = "Select rtrim(BankId) as ID, ShortName as Name from dbo.BankMaster where IsActive = 1 ORDER BY ShortName"
    Private Const SQLEXECSP_MASTER_BRANCH As String = "spGetBranch"
    Private Const SQLEXECSP_MASTER_BRANCH2 As String = "spGetBranch2"
    Private Const SQLEXECSP_GET_HOBranch As String = "spGetHoBranch"
    Private Const SQLEXECSP_CASHIERLIST As String = "spCashierList"
    Private Const SQLEXECSP_BANKACCOUNT As String = "spGetBankAccount"
    Private Const SQLEXECSP_REASON As String = "spGetReason"
    Private Const SQLEXECSP_MASTER_BRANCH_COLLECTION As String = "spGetBranchCollection"
    Private Const SQLEXECSP_MASTER_ALL_BRANCH_COLLECTION As String = "spGetAllBranchCollection"
    Private Const SQLEXECSP_MASTER_BRANCH_ALL As String = "spGetBranchAll"
    Private Const spGetReportCollectorType_CL As String = "spGetReportCollectorType_CL"
    Private Const spGetReportCollectorType_D As String = "spGetReportCollectorType_D"    
    Private Const spGetReportKasir As String = "spGetReportKasir"
    Private Const spGetReportNamaKonsumen As String = "spGetReportNamaKonsumen"
    Private Const spGetInsuranceCompanyBranchALL As String = "spGetInsuranceCompanyBranchALL"
    Private Const spGetReportTransactionType As String = "spGetReportTransactionType"
    Private Const spGetReportNoKontrak As String = "spGetReportNoKontrak"
    Private Const spGetReportNoAplikasi As String = "spGetReportNoAplikasi"
    Private Const spGetReportComboContract As String = "spGetReportComboContract"


    Private Const SQLEXECSP_BGNumber As String = "spGetBGNumber"
    Private Const SQLEXECSP_MASTER_INSURANCE_BRANCH As String = "spGetInsuranceCompanyBranch"
    Private Const SQLEXECSP_BRANCHHO As String = "spGetbranchHO"
    Private Const SQLEXECSP_BANKACCOUNTBRANCH As String = "spGetBankAccountBranch"
    Private Const SQLEXECSP_GET_ASSETTYPE As String = "spGetAssetType"
    Private Const SQLEXECSP_GET_PRODUCTBRANCH As String = "spProspectAppGetProductBranch"

    Private Const GET_SERIALNO1 As String = "Select SerialNo1Label from AssetType where AssetTypeID=@AssetTypeID"
    Private Const GET_SERIALNO2 As String = "Select SerialNo2Label from AssetType where AssetTypeID=@AssetTypeID"
    Private Const GET_GENERALSETTING As String = "Select GSValue from GeneralSetting where GSID=@GSID"

    Private Const spViewEmp As String = "spViewEmployee"
    Private Const spViewProd As String = "spViewproduct"

    Private Const PARAM_ISHEADCASHIER As String = "@IsHeadCashier"
    Private Const PARAM_BANKTYPE As String = "@banktype"
    Private Const PARAM_BANKPURPOSE As String = "@bankpurpose"

    Private Const Collector_SPVAct_CBO As String = "spGetCollectorSPVAct"

    Private Const spViewBankAccount As String = "spViewBankAccount"

    Private Const Query_CBODepartement As String = "select departementID as id, DepartementName as name from departement"

#End Region

#Region "GetCoverageType"

    Public Function GetCoverageType(ByVal strConnection As String) As DataTable
        Dim oReader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))
        Try
            oReader = SqlHelper.ExecuteReader(strConnection, CommandType.Text, SQLQUERY_COVERAGETYPE)
            While oReader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = oReader("ID").ToString
                dtrResult("Name") = oReader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetCoverageType", exp.Message + exp.StackTrace)
        End Try

    End Function

#End Region

#Region "GetApplicationType"

    Public Function GetApplicationType(ByVal strConnection As String) As DataTable
        Dim oReader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))

        Try
            oReader = SqlHelper.ExecuteReader(strConnection, CommandType.Text, SQLQUERY_APPLICATION_TYPE)

            While oReader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = oReader("ID").ToString
                dtrResult("Name") = oReader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While

            Return dttResult

        Catch exp As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("", "", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            WriteException("DataUserControl", "GetCoverageType", exp.Message + exp.StackTrace)
        End Try

    End Function

#End Region

#Region "GetBankName"

    Public Function GetBankName(ByVal strConnection As String) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))

        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.Text, SQLQUERY_MASTER_BANK)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                'insert DataRow to DataTable
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetCoverageType", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region

#Region "GetBranchName"

    Public Function GetBranchName(ByVal strConnection As String, ByVal strBranch As String) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))
        dttResult.Columns.Add("areaid", GetType(String))

        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 200)
        params(0).Value = strBranch
        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_MASTER_BRANCH, params)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dtrResult("areaid") = reader("areaid").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetBranchName", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "GetBranchName2"

    Public Function GetBranchName2(ByVal strConnection As String, ByVal strBranch As String) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))
        dttResult.Columns.Add("areaid", GetType(String))

        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 200)
        params(0).Value = strBranch
        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_MASTER_BRANCH2, params)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dtrResult("areaid") = reader("areaid").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetBranchName2", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "GetSupplierName"
    Public Function GetSupplierName(ByVal strConnection As String) As DataTable
        Dim reader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("SupplierID", GetType(String))
        dttResult.Columns.Add("SupplierName", GetType(String))

        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, "spGetSupplierName")
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("SupplierID") = reader("SupplierID").ToString
                dtrResult("SupplierName") = reader("SupplierName").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetSupplierName", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "GetHOBranch"
    Public Function GetHOBranch(ByVal strConnection As String) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))

        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_GET_HOBranch)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetHOBranch", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "GetInsuranceBranchName"

    Public Overloads Function GetInsuranceBranchName(ByVal strConnection As String, ByVal strBranch As String) As DataTable

        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))

        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3000)
        params(0).Value = strBranch
        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_MASTER_INSURANCE_BRANCH, params)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception

            WriteException("DataUserControl", "GetInsuranceBranchName", exp.Message + exp.StackTrace)
            Return Nothing
        End Try
    End Function


    Public Overloads Function GetInsuranceBranchName(ByVal strConnection As String, ByVal strBranch As String, ByVal strLoginID As String) As DataTable

        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))

        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar)
        params(0).Value = strBranch
        params(1) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar)
        params(1).Value = strLoginID

        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_MASTER_INSURANCE_BRANCH, params)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            Return Nothing
            WriteException("DataUserControl", "GetInsuranceBranchName", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region

#Region "GetNotary"

    Public Function GetMasterNotary(ByVal strConnection As String) As DataTable

        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))

        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, "spGetMasterNotary")
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetMasterNotary", exp.Message + exp.StackTrace)
        End Try

    End Function

#End Region

#Region "GetBranchCollectionName"



    Public Function GetBranchCollectionName(ByVal strConnection As String, ByVal strBranch As String) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            dttResult.Columns.Add("ID", GetType(String))
            dttResult.Columns.Add("Name", GetType(String))


            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 2000)
            params(0).Value = strBranch

            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_MASTER_BRANCH_COLLECTION, params)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetBranchCollectionName", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "GetAllBranchCollectionName"



    Public Function GetAllBranchCollectionName(ByVal strConnection As String) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        Try
            dttResult.Columns.Add("ID", GetType(String))
            dttResult.Columns.Add("Name", GetType(String))




            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_MASTER_ALL_BRANCH_COLLECTION)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetAllBranchCollectionName", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "GetAllCollectorTypeCL"



    Public Function GetAllCollectorTypeCL(ByVal strConnection As String) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        Try
            dttResult.Columns.Add("CollectorID", GetType(String))
            dttResult.Columns.Add("Name", GetType(String))




            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_MASTER_ALL_COLLECTORTYPE_CL)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("CollectorID") = reader("CollectorID").ToString
                dtrResult("Name") = reader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetAllCollectorTypeCL", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "getBranchfromCopyRate"


    Public Function getBranchfromCopyRate(ByVal strConnection As String, ByVal strBranchSource As String, ByVal strBranch As String) As DataTable
        Dim oReader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow
        Dim sqlQuery As String
        Try

            Select Case strBranchSource
                Case CommonVariableHelper.BRANCH_SOURCE_FOR_CUSTOMER
                    sqlQuery = SQLQUERY_COPYRATE_FROM_BRANCH
                Case CommonVariableHelper.BRANCH_SOURCE_FOR_PREMIUM_RATE
                    sqlQuery = SQLQUERY_COPYRATE_PREMIUM_RATE_FROM_BRANCH + " where BranchID <> '" + strBranch + "'"
                Case Else  'Punya PETRA (jangan dirubah !)
                    sqlQuery = SQLQUERY_COPYRATE_FROM_INSCOBRANCH + " where InsuranceCoyID ='" + strBranchSource + "'"
            End Select

            'If strBranchSource = Maxiloan.General.CommonVariableHelper.BRANCH_SOURCE_FOR_CUSTOMER Then
            '    sqlQuery = SQLQUERY_COPYRATE_FROM_BRANCH
            'Else

            '   sqlQuery = SQLQUERY_COPYRATE_PREMIUM_RATE_FROM_BRANCH
            'End If


            dttResult.Columns.Add("ID", GetType(String))
            dttResult.Columns.Add("Name", GetType(String))

            oReader = SqlHelper.ExecuteReader(strConnection, CommandType.Text, sqlQuery)
            While oReader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = oReader("ID").ToString
                dtrResult("Name") = oReader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "getBranchfromCopyRate", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "BranchCashierList"
    Public Function BranchCashierList(ByVal strConnection As String, ByVal strBranch As String) As DataTable
        Dim reader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))

        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 1000)
        params(0).Value = strBranch
        params(1) = New SqlParameter(PARAM_ISHEADCASHIER, SqlDbType.Bit)
        params(1).Value = False

        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_CASHIERLIST, params)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "BranchCashierList", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "BranchHeadCashierList"
    Public Function BranchHeadCashierList(ByVal strConnection As String, ByVal strBranch As String) As DataTable
        Dim reader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow
        Try
            dttResult.Columns.Add("ID", GetType(String))
            dttResult.Columns.Add("Name", GetType(String))

            Dim params() As SqlParameter = New SqlParameter(1) {}

            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 1000)
            params(0).Value = strBranch
            params(1) = New SqlParameter(PARAM_ISHEADCASHIER, SqlDbType.Bit)
            params(1).Value = True


            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_CASHIERLIST, params)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "BranchHeadCashierList", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "GetBankAccount"
    Public Function GetBankAccount(ByVal strConnection As String, ByVal strBranch As String, _
                                ByVal strBankType As String, ByVal strBankPurpose As String) As DataTable
        Dim reader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            dttResult.Columns.Add("ID", GetType(String))
            dttResult.Columns.Add("Name", GetType(String))
            dttResult.Columns.Add("Type", GetType(String))
            dttResult.Columns.Add("IsGenerateTextAvailable", GetType(Boolean))
            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 1000)
            params(0).Value = strBranch.Replace("'", "")
            params(1) = New SqlParameter(PARAM_BANKTYPE, SqlDbType.Char, 1)
            params(1).Value = CStr(IIf(strBankType Is Nothing, "", strBankType)).Trim
            params(2) = New SqlParameter(PARAM_BANKPURPOSE, SqlDbType.VarChar, 2)
            params(2).Value = CStr(IIf(strBankPurpose Is Nothing, "", strBankPurpose)).Trim

            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_BANKACCOUNT, params)

            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dtrResult("Type") = reader("Type").ToString
                dtrResult("IsGenerateTextAvailable") = reader("IsGenerateTextAvailable").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetBankAccount", exp.Message + exp.StackTrace)
            Return Nothing
        End Try
    End Function
#End Region

#Region "GetBranchAll"

    Public Function GetBranchAll(ByVal strConnection As String) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))

        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_MASTER_BRANCH_ALL)

            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetBranchAll", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region


    Public Function GetCollectorReportLapHasilKunColl(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim params(0) As SqlParameter
        Dim oCollectorList As New Parameter.ControlsRS
        Try
            With oClass
                params(0) = New SqlParameter("@Cabang", SqlDbType.Char, 3)
                params(0).Value = .BranchId

            End With
            oCollectorList.ListData = SqlHelper.ExecuteDataset(oClass.strConnection, CommandType.StoredProcedure, spGetReportCollectorType_CL, params).Tables(0)
            Return oCollectorList
        Catch exp As Exception
            WriteException("DataUserControl", "GetCollectorReportLapHasilKunColl", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Function GetCollectorReportLapKegDeskColl(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim params(0) As SqlParameter
        Dim oCollectorList As New Parameter.ControlsRS
        Try
            With oClass
                params(0) = New SqlParameter("@Cabang", SqlDbType.Char, 3)
                params(0).Value = .BranchId

            End With
            oCollectorList.ListData = SqlHelper.ExecuteDataset(oClass.strConnection, CommandType.StoredProcedure, spGetReportCollectorType_D, params).Tables(0)
            Return oCollectorList
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetReportKasir(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim params(0) As SqlParameter
        Dim oCollectorList As New Parameter.ControlsRS
        Try
            With oClass
                params(0) = New SqlParameter("@Cabang", SqlDbType.Char, 3)
                params(0).Value = .BranchId

            End With
            oCollectorList.ListData = SqlHelper.ExecuteDataset(oClass.strConnection, CommandType.StoredProcedure, spGetReportKasir, params).Tables(0)
            Return oCollectorList
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function GetReportNamaKonsumen(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim params(1) As SqlParameter
        Dim oCollectorList As New Parameter.ControlsRS
        Try
            With oClass
                params(0) = New SqlParameter("@Kontrak", SqlDbType.VarChar, 20)
                params(0).Value = .Kontrak
                params(1) = New SqlParameter("@NoBatch", SqlDbType.VarChar, 30)
                params(1).Value = .NoBatch
            End With
            oCollectorList.ListData = SqlHelper.ExecuteDataset(oClass.strConnection, CommandType.StoredProcedure, spGetReportNamaKonsumen, params).Tables(0)
            Return oCollectorList
        Catch exp As Exception
            WriteException("DataUserControl", "GetReportKasir", exp.Message + exp.StackTrace)
        End Try
    End Function


#Region "GetInsuranceCompanyBranchALL"
    Public Function GetInsuranceCompanyBranchALL(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim params(0) As SqlParameter
        Dim oCollectorList As New Parameter.ControlsRS
        Try
            With oClass
                params(0) = New SqlParameter("@branchid", SqlDbType.VarChar, 20)
                params(0).Value = .BranchId

            End With
            oCollectorList.ListData = SqlHelper.ExecuteDataset(oClass.strConnection, CommandType.StoredProcedure, spGetInsuranceCompanyBranchALL, params).Tables(0)
            Return oCollectorList
        Catch exp As Exception
            WriteException("DataUserControl", "GetReportKasir", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region



    Public Function GetReportTransactionType(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim oCollectorList As New Parameter.ControlsRS
        Try
            oCollectorList.ListData = SqlHelper.ExecuteDataset(oClass.strConnection, CommandType.StoredProcedure, spGetReportTransactionType).Tables(0)
            Return oCollectorList
        Catch exp As Exception
            WriteException("DataUserControl", "GetReportTransactionType", exp.Message + exp.StackTrace)
        End Try
    End Function



    Public Function GetReportNoKontrak(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim params(0) As SqlParameter
        Dim oCollectorList As New Parameter.ControlsRS
        Try
            With oClass
                params(0) = New SqlParameter("@Cabang", SqlDbType.VarChar, 20)
                params(0).Value = .BranchId

            End With
            oCollectorList.ListData = SqlHelper.ExecuteDataset(oClass.strConnection, CommandType.StoredProcedure, spGetReportNoKontrak, params).Tables(0)
            Return oCollectorList
        Catch exp As Exception
            WriteException("DataUserControl", "GetReportTransactionType", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetReportNoAplikasi(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim params(0) As SqlParameter
        Dim oCollectorList As New Parameter.ControlsRS
        Try
            params(0) = New SqlParameter("@Cabang", SqlDbType.Char, 3)
            params(0).Value = oClass.BranchId

            oCollectorList.ListData = SqlHelper.ExecuteDataset(oClass.strConnection, CommandType.StoredProcedure, spGetReportNoAplikasi, params).Tables(0)
            Return oCollectorList
        Catch exp As Exception
            WriteException("DataUserControl", "GetReportTransactionType", exp.Message + exp.StackTrace)
        End Try
    End Function




    Public Function GetReportComboContract(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        Dim params(0) As SqlParameter
        Dim oCollectorList As New Parameter.ControlsRS
        Try
            With oClass
                params(0) = New SqlParameter("@Bank", SqlDbType.VarChar, 20)
                params(0).Value = .CoyID

            End With
            oCollectorList.ListData = SqlHelper.ExecuteDataset(oClass.strConnection, CommandType.StoredProcedure, spGetReportComboContract, params).Tables(0)
            Return oCollectorList
        Catch exp As Exception
            WriteException("DataUserControl", "GetReportTransactionType", exp.Message + exp.StackTrace)
        End Try
    End Function

#Region "GetReason"


    Public Function GetReason(ByVal strConnection As String, ByVal strReasonID As String) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow
        Try
            dttResult.Columns.Add("ID", GetType(String))
            dttResult.Columns.Add("Name", GetType(String))

            Dim params() As SqlParameter = New SqlParameter(0) {}

            params(0) = New SqlParameter(PARAM_REASONTYPEID, SqlDbType.VarChar, 5)
            params(0).Value = strReasonID.Trim

            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_REASON, params)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetReason", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "Get BGNumber"
    Public Function GetBGNumber(ByVal strConnection As String, ByVal strWhere As String) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))

        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Where", SqlDbType.VarChar, 100)
        params(0).Value = strWhere

        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_BGNumber, params)

            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetBGNumber", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "Get branchHO"
    Public Function GetBranchHO(ByVal strConnection As String, ByVal strWhere As String) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))

        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@Where", SqlDbType.VarChar, 100)
        params(0).Value = strWhere

        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_BRANCHHO, params)

            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetBranchHO", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "GetBankAccountbranch"
    Public Function GetBankAccountbranch(ByVal strConnection As String, ByVal strBranch As String) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        Try
            dttResult.Columns.Add("ID", GetType(String))
            dttResult.Columns.Add("Name", GetType(String))

            Dim params() As SqlParameter = New SqlParameter(0) {}
            params(0) = New SqlParameter("@BRANCHID", SqlDbType.VarChar, 10)
            params(0).Value = strBranch

            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_BANKACCOUNTBRANCH, params)

            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetBankAccountbranch", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "get CollectorCombo for Supervisor Activity"

    Public Function GetCollectorComboForSupervisorActivity(ByVal oCollector As Parameter.Collector) As Parameter.Collector
        Dim params(2) As SqlParameter
        Dim oCollectorList As New Parameter.Collector
        Try
            With oCollector
                params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
                params(0).Value = .CGID
                params(1) = New SqlParameter("@SupervisorID", SqlDbType.Char, 10)
                params(1).Value = .Supervisor
                params(2) = New SqlParameter("@WhereCollectorType", SqlDbType.VarChar, 100)
                params(2).Value = .WhereCollectorType
            End With
            oCollectorList.ListData = SqlHelper.ExecuteDataset(oCollector.strConnection, CommandType.StoredProcedure, Collector_SPVAct_CBO, params).Tables(0)
            Return oCollectorList
        Catch exp As Exception
            WriteException("DataUserControl", "GetCollectorComboForSupervisorActivity", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "SuspendTransaction"
    Public Function ViewBankAccount(ByVal StrConnection As String) As DataTable
        Try
            Return SqlHelper.ExecuteDataset(StrConnection, CommandType.StoredProcedure, spViewBankAccount).Tables(0)
        Catch exp As Exception
            WriteException("DataUserControl", "ViewBankAccount", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "Get Departement"
    Public Function GetDepartement(ByVal strconn As String) As DataTable
        Try
            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDataset(strconn, CommandType.Text, Query_CBODepartement).Tables(0)
            Return dt
        Catch exp As Exception
            WriteException("DataUserControl", "GetDepartement", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "Get BankMaster"
    Public Function GetBankMaster(ByVal strconn As String) As DataTable
        Try
            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDataset(strconn, CommandType.Text, "spGetBankMaster").Tables(0)
            Return dt
        Catch exp As Exception
            WriteException("DataUserControl", "ViewEmployee", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "View Employee"
    Public Function ViewEmployee(ByVal StrConnection As String) As DataTable
        Try
            Dim par() As SqlParameter = New SqlParameter(0) {}

            par(0) = New SqlParameter("@APPMGR", SqlDbType.VarChar, 50)
            par(0).Value = getReferenceDBName()


            Return SqlHelper.ExecuteDataset(StrConnection, CommandType.StoredProcedure, spViewEmp, par).Tables(0)
        Catch exp As Exception
            WriteException("DataUserControl", "ViewEmployee", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "View Product"
    Public Function ViewProduct(ByVal StrConnection As String) As DataTable
        Try
            Return SqlHelper.ExecuteDataset(StrConnection, CommandType.StoredProcedure, spViewProd).Tables(0)
        Catch exp As Exception
            WriteException("DataUserControl", "ViewProduct", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "View Supplier"
    Public Function ViewSupplier(ByVal StrConnection As String) As DataTable
        Try
            Return SqlHelper.ExecuteDataset(StrConnection, CommandType.StoredProcedure, "spViewSupplier").Tables(0)
        Catch exp As Exception
            WriteException("DataUserControl", "ViewSupplier", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "View Area"
    Public Function ViewArea(ByVal StrConnection As String) As DataTable
        Try
            Return SqlHelper.ExecuteDataset(StrConnection, CommandType.StoredProcedure, "spViewArea").Tables(0)
        Catch exp As Exception
            WriteException("DataUserControl", "ViewArea", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "GetAgingList"
    Public Function GetAgingList(ByVal customclass As Parameter.Sales) As Parameter.Sales
        Dim objread As SqlDataReader
        Try
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, "spAgingList")
            If objread.Read Then
                With customclass
                    .sdate = CType(objread("AgingDate"), Date)
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            WriteException("DataUserControl", "GetAgingList", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "GetAssetType"

    Public Function GetAssetType(ByVal strConnection As String) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("AssetTypeID", GetType(String))
        dttResult.Columns.Add("Description", GetType(String))

        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_GET_ASSETTYPE)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("AssetTypeID") = reader("AssetTypeID").ToString
                dtrResult("Description") = reader("Description").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetAssetType", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "Get GeneralSetting"
    Public Function GetGeneralSetting(ByVal strConnection As String, ByVal Entities As Parameter.GeneralSetting) As String
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            With Entities
                params(0) = New SqlParameter("@GSID", SqlDbType.Char, 50)
                params(0).Value = .GSID
            End With
            Return DirectCast(SqlHelper.ExecuteScalar(strConnection, CommandType.Text, GET_GENERALSETTING, params), String)
        Catch exp As Exception
            WriteException("GeneralSetting", "GetGeneralSetting", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "get SerialNo1Label and SerialNo2Label"

    Public Function GetSerialNo1Label(ByVal strConnection As String, ByVal Entities As Parameter.DocumentChangeLocation) As String
        Dim params(0) As SqlParameter
        Try
            With Entities
                params(0) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
                params(0).Value = .AssetTypeID
            End With
            Return DirectCast(SqlHelper.ExecuteScalar(strConnection, CommandType.Text, GET_SERIALNO1, params), String)
        Catch exp As Exception
            WriteException("DataUserControl", "GetSerialNo1Label", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function GetSerialNo2Label(ByVal strConnection As String, ByVal Entities As Parameter.DocumentChangeLocation) As String
        Dim params(0) As SqlParameter
        Try
            With Entities
                params(0) = New SqlParameter("@AssetTypeID", SqlDbType.Char, 10)
                params(0).Value = .AssetTypeID
            End With
            Return DirectCast(SqlHelper.ExecuteScalar(strConnection, CommandType.Text, GET_SERIALNO2, params), String)
        Catch exp As Exception
            WriteException("DataUserControl", "GetSerialNo1Label", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "GetProductBranch"

    Public Function GetProductBranch(ByVal strConnection As String, ByVal strBranch As String) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ProductID", GetType(String))
        dttResult.Columns.Add("Description", GetType(String))

        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 200)
        params(0).Value = strBranch
        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.StoredProcedure, SQLEXECSP_GET_PRODUCTBRANCH, params)
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ProductID") = reader("ProductID").ToString
                dtrResult("Description") = reader("Description").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetProductBranch", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

    Public Function GetAPBank(ByVal strConnection As String) As DataTable
        Dim reader As SqlDataReader

        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))

        Try
            reader = SqlHelper.ExecuteReader(strConnection, CommandType.Text, _
            "spGetAPBankPaymentAllocationList")
            While reader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = reader("ID").ToString
                dtrResult("Name") = reader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While

            reader.Close()
            Return dttResult
        Catch exp As Exception
            WriteException("DataUserControl", "GetAPBank", exp.Message + exp.StackTrace)
        End Try

    End Function

    Public Function popCollector(ByVal strConnection As String, ByVal strCGID As String, ByVal strCollectorType As String) As DataTable
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@CGID", SqlDbType.Char, 3)
            params(0).Value = strCGID
            params(1) = New SqlParameter("@CollectorType", SqlDbType.Char, 3)
            params(1).Value = strCollectorType

            Return SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, "spGetCollectorCombo", params).Tables(0)
        Catch exp As Exception
            Return Nothing
            WriteException("CollZipCode", "GetCollectorCombo", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function viewCustomerDetailShort(ByVal oCustomClass As Parameter.Customer) As Parameter.Customer
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            With oCustomClass
                params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
                params(0).Value = .CustomerID
                oCustomClass.listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spViewCustomerDetailShort", params).Tables(0)
            End With
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
        Return oCustomClass
    End Function

    Public Function getKotaKabupaten(ByVal oCustomClass As Parameter.Dati) As Parameter.Dati
        Dim par() As SqlParameter = New SqlParameter(0) {}
        Try
            par(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 8000)
            par(0).Value = oCustomClass.WhereCond
            oCustomClass.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetDati", par).Tables(0)
        Catch ex As Exception
            Throw New Exception(ex.Message)            
        End Try
        Return oCustomClass
    End Function

    Public Function getAgreementGuarantor(ByVal oCustomClass As Parameter.AgreementGuarantor) As Parameter.AgreementGuarantor
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.AgreementGuarantor
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 20)
        params(0).Value = oCustomClass.ApplicationID
        Try
            reader = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetAgreementGuarantorView", params)
            If reader.Read Then
                oReturnValue.ApplicationID = oCustomClass.ApplicationID
                oReturnValue.ApplicationID = reader("ApplicationID").ToString
                oReturnValue.GuarantorName = reader("GuarantorName").ToString
                oReturnValue.GuarantorJobTitle = reader("GuarantorJobTitle").ToString
                oReturnValue.GuarantorAddress = reader("GuarantorAddress").ToString
                oReturnValue.GuarantorRT = reader("GuarantorRT").ToString
                oReturnValue.GuarantorRW = reader("GuarantorRW").ToString
                oReturnValue.GuarantorKelurahan = reader("GuarantorKelurahan").ToString
                oReturnValue.GuarantorKecamatan = reader("GuarantorKecamatan").ToString
                oReturnValue.GuarantorCity = reader("GuarantorCity").ToString
                oReturnValue.GuarantorZipCode = reader("GuarantorZipCode").ToString
                oReturnValue.GuarantorAreaPhone1 = reader("GuarantorAreaPhone1").ToString
                oReturnValue.GuarantorPhone1 = reader("GuarantorPhone1").ToString
                oReturnValue.GuarantorAreaPhone2 = reader("GuarantorAreaPhone2").ToString
                oReturnValue.GuarantorPhone2 = reader("GuarantorPhone2").ToString
                oReturnValue.GuarantorAreaFax = reader("GuarantorAreaFax").ToString
                oReturnValue.GuarantorFax = reader("GuarantorFax").ToString
                oReturnValue.GuarantorMobilePhone = reader("GuarantorMobilePhone").ToString
                oReturnValue.GuarantorEmail = reader("GuarantorEmail").ToString
                oReturnValue.GuarantorNotes = reader("GuarantorNotes").ToString
                oReturnValue.GuarantorPenghasilan = CDec(reader("GuarantorPenghasilan").ToString)
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.getAgreementGuarantor")
        End Try
    End Function
End Class
