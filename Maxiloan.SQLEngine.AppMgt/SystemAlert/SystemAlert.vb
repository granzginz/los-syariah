
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class SystemAlert : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region "Constanta"
    Private Const PARAM_ALERTID As String = "@AlertID"
    Private Const PARAM_ALERTMESSAGE As String = "@AlertMessage"
    Private Const PARAM_NUMBERSQLCMD As String = "@NumberSQLCmd"
    Private Const PARAM_DETAILSQLCMD As String = "@DetailSqlCmd"
    Private Const PARAM_KEYCOLUMNNAME As String = "@KeyColumnName"
    Private Const PARAM_ISDEFAULTFORM As String = "@ISDefaultForm"
    Private Const PARAM_LISTFORMID As String = "@ListFormID"
    Private Const PARAM_LISTFORMPARAMETER As String = "@ListFormParameter"
    Private Const PARAM_VIEWFORMID As String = "@ViewFormID"
    Private Const PARAM_VIEWFORMPARAMETER As String = "@ViewFormParameter"
    Private Const PARAM_EDITFORMID As String = "@EditFormID"
    Private Const PARAM_EDITFORMPARAMETER As String = "@EditFormParameter"
    Private Const PARAM_GROUPALERTID As String = "@GroupAlertID"
    Private Const PARAM_GROUPALERTNAME As String = "@GroupAlertName"
    Private Const PARAM_GROUPALLALERTID As String = "@GroupAllAlertID"

#Region "System Alert"
    Private Const SPSYSTEMALERTADD As String = "spSystemAlertAdd"
    Private Const SPSYSTEMALERTEDIT As String = "spSystemAlertEdit"
    Private Const SPSYSTEMALERTDELETE As String = "spSystemAlertDelete"
    Private Const SPSYSTEMALERTVIEW As String = "spSystemAlertView"
    Private Const SPGETGROUPALERT As String = "spGetGroupAlert"
#End Region

#Region "User Alert"
    Private Const SPUSERALERTUPDATE As String = "spUserAlertUpdate"
    Private Const SPUSERALERTPAGING As String = "spUserAlertPaging"
#End Region

#Region "Group Alert"
    Private Const SPGROUPALERTADD As String = "spGroupAlertAdd"
    Private Const SPGROUPALERTEDIT As String = "spGroupAlertEdit"
    Private Const SPGROUPALERTDELETE As String = "spGroupAlertDelete"
    Private Const SPGROUPALERTVIEW As String = "spGroupAlertView"
#End Region
#End Region

#Region "System Alert"
    Public Sub SystemAlertAdd(ByVal customclass As Parameter.SystemAlert)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(12) {}

            params(0) = New SqlParameter(PARAM_ALERTID, SqlDbType.VarChar, 20)
            params(0).Value = customclass.AlertID

            params(1) = New SqlParameter(PARAM_ALERTMESSAGE, SqlDbType.VarChar, 50)
            params(1).Value = customclass.AlertMessage

            params(2) = New SqlParameter(PARAM_NUMBERSQLCMD, SqlDbType.VarChar, 3000)
            params(2).Value = customclass.NumberSQLCmd

            params(3) = New SqlParameter(PARAM_DETAILSQLCMD, SqlDbType.VarChar, 3000)
            params(3).Value = customclass.NumberSQLCmd

            params(4) = New SqlParameter(PARAM_KEYCOLUMNNAME, SqlDbType.VarChar, 100)
            params(4).Value = customclass.KeyColumnName

            params(5) = New SqlParameter(PARAM_ISDEFAULTFORM, SqlDbType.Bit)
            params(5).Value = customclass.IsDefaultForm

            params(6) = New SqlParameter(PARAM_LISTFORMID, SqlDbType.VarChar, 500)
            params(6).Value = customclass.ListFormID

            params(7) = New SqlParameter(PARAM_LISTFORMPARAMETER, SqlDbType.VarChar, 1000)
            params(7).Value = customclass.KeyColumnName

            params(8) = New SqlParameter(PARAM_VIEWFORMID, SqlDbType.VarChar, 5000)
            params(8).Value = customclass.ViewFormID

            params(9) = New SqlParameter(PARAM_VIEWFORMPARAMETER, SqlDbType.VarChar, 1000)
            params(9).Value = customclass.ViewFormParameter

            params(10) = New SqlParameter(PARAM_EDITFORMID, SqlDbType.VarChar, 500)
            params(10).Value = customclass.EditFormID

            params(11) = New SqlParameter(PARAM_EDITFORMPARAMETER, SqlDbType.VarChar, 1000)
            params(11).Value = customclass.EditFormParameter

            params(12) = New SqlParameter(PARAM_GROUPALERTID, SqlDbType.VarChar, 5)
            params(12).Value = customclass.GroupAlertID
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPSYSTEMALERTADD, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try

    End Sub

    Public Sub SystemAlertDelete(ByVal customclass As Parameter.SystemAlert)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(0) {}

            params(0) = New SqlParameter(PARAM_ALERTID, SqlDbType.VarChar, 20)
            params(0).Value = customclass.AlertID

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPSYSTEMALERTDELETE, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try

    End Sub

    Public Sub SystemAlertEdit(ByVal customclass As Parameter.SystemAlert)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(12) {}

            params(0) = New SqlParameter(PARAM_ALERTID, SqlDbType.VarChar, 20)
            params(0).Value = customclass.AlertID

            params(1) = New SqlParameter(PARAM_ALERTMESSAGE, SqlDbType.VarChar, 50)
            params(1).Value = customclass.AlertMessage

            params(2) = New SqlParameter(PARAM_NUMBERSQLCMD, SqlDbType.VarChar, 3000)
            params(2).Value = customclass.NumberSQLCmd

            params(3) = New SqlParameter(PARAM_DETAILSQLCMD, SqlDbType.VarChar, 3000)
            params(3).Value = customclass.NumberSQLCmd

            params(4) = New SqlParameter(PARAM_KEYCOLUMNNAME, SqlDbType.VarChar, 100)
            params(4).Value = customclass.KeyColumnName

            params(5) = New SqlParameter(PARAM_ISDEFAULTFORM, SqlDbType.Bit)
            params(5).Value = customclass.IsDefaultForm

            params(6) = New SqlParameter(PARAM_LISTFORMID, SqlDbType.VarChar, 500)
            params(6).Value = customclass.ListFormID

            params(7) = New SqlParameter(PARAM_LISTFORMPARAMETER, SqlDbType.VarChar, 1000)
            params(7).Value = customclass.KeyColumnName

            params(8) = New SqlParameter(PARAM_VIEWFORMID, SqlDbType.VarChar, 5000)
            params(8).Value = customclass.ViewFormID

            params(9) = New SqlParameter(PARAM_VIEWFORMPARAMETER, SqlDbType.VarChar, 1000)
            params(9).Value = customclass.ViewFormParameter

            params(10) = New SqlParameter(PARAM_EDITFORMID, SqlDbType.VarChar, 500)
            params(10).Value = customclass.EditFormID

            params(11) = New SqlParameter(PARAM_EDITFORMPARAMETER, SqlDbType.VarChar, 1000)
            params(11).Value = customclass.EditFormParameter

            params(12) = New SqlParameter(PARAM_GROUPALERTID, SqlDbType.VarChar, 5)
            params(12).Value = customclass.GroupAlertID
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPSYSTEMALERTEDIT, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try

    End Sub

    Public Function SystemAlertView(ByVal customclass As Parameter.SystemAlert) As Parameter.SystemAlert
        Dim objread As SqlDataReader
        Try
            Dim params() As SqlParameter = New SqlParameter(0) {}

            params(0) = New SqlParameter(PARAM_ALERTID, SqlDbType.VarChar, 20)
            params(0).Value = customclass.AlertID

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, SPSYSTEMALERTVIEW, params)
            If objread.Read Then
                With customclass
                    .AlertID = objread("AlertID").ToString.Trim
                    .AlertMessage = objread("AlertMessage").ToString.Trim
                    .NumberSQLCmd = objread("NumberSQLCmd").ToString.Trim
                    .DetailSQLCmd = objread("DetailSQLCmd").ToString.Trim
                    .KeyColumnName = objread("KeyColumnName").ToString.Trim
                    .IsDefaultForm = CBool(objread("IsDefaultForm"))
                    .ListFormID = objread("ListFormID").ToString.Trim
                    .ListFormParameter = objread("ListFormParameter").ToString.Trim
                    .ViewFormID = objread("ViewFormID").ToString.Trim
                    .ViewFormParameter = objread("ViewFormParameter").ToString.Trim
                    .EditFormID = objread("EditFormID").ToString.Trim
                    .EditFormParameter = objread("EditFormParameter").ToString.Trim
                    .GroupAlertID = objread("GroupAlertID").ToString.Trim
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function
    Public Function GetGroupAlert(ByVal customclass As Parameter.SystemAlert) As DataTable
        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPGETGROUPALERT).Tables(0)
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region
#Region "User Alert"
    Public Sub UserAlertUpdate(ByVal customclass As Parameter.SystemAlert)
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim objconnection As New SqlConnection(customclass.strConnection)
        Dim objTransaction As SqlTransaction = Nothing
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objtransaction = objconnection.BeginTransaction
            params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(0).Value = customclass.LoginId

            params(1) = New SqlParameter(PARAM_GROUPALLALERTID, SqlDbType.VarChar, 8000)
            params(1).Value = customclass.GroupAllAlertID

            params(2) = New SqlParameter(PARAM_GROUPALERTID, SqlDbType.VarChar, 8000)
            params(2).Value = customclass.GroupAlertID

            SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, SPUSERALERTUPDATE, params)
            objtransaction.Commit()
        Catch exp As Exception
            objtransaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
    End Sub

    Public Function UserAlertPaging(ByVal customclass As Parameter.SystemAlert) As Parameter.SystemAlert
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(0).Value = customclass.LoginId

            params(1) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(1).Value = customclass.CurrentPage

            params(2) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(2).Value = customclass.PageSize

            params(3) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(3).Value = customclass.WhereCond

            params(4) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(4).Value = customclass.SortBy

            params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output

            customclass.ListUserAlert = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPUSERALERTPAGING, params).Tables(0)
            customclass.TotalRecord = CInt(params(5).Value)
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function


#End Region

#Region "Group Alert"
    Public Sub GroupAlertAdd(ByVal customclass As Parameter.SystemAlert)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(1) {}

            params(0) = New SqlParameter(PARAM_GROUPALERTID, SqlDbType.VarChar, 5)
            params(0).Value = customclass.GroupAlertID

            params(1) = New SqlParameter(PARAM_GROUPALERTNAME, SqlDbType.VarChar, 30)
            params(1).Value = customclass.GroupAlertName
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPGROUPALERTADD, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try

    End Sub

    Public Sub GroupAlertDelete(ByVal customclass As Parameter.SystemAlert)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(0) {}

            params(0) = New SqlParameter(PARAM_GROUPALERTID, SqlDbType.VarChar, 20)
            params(0).Value = customclass.GroupAlertID

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPGROUPALERTDELETE, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try

    End Sub

    Public Sub GroupAlertEdit(ByVal customclass As Parameter.SystemAlert)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(1) {}

            params(0) = New SqlParameter(PARAM_GROUPALERTID, SqlDbType.VarChar, 5)
            params(0).Value = customclass.GroupAlertID

            params(1) = New SqlParameter(PARAM_GROUPALERTNAME, SqlDbType.VarChar, 30)
            params(1).Value = customclass.GroupAlertName
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPGROUPALERTEDIT, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try

    End Sub

    Public Function GroupAlertView(ByVal customclass As Parameter.SystemAlert) As Parameter.SystemAlert
        Dim objread As SqlDataReader
        Try
            Dim params() As SqlParameter = New SqlParameter(0) {}

            params(0) = New SqlParameter(PARAM_GROUPALERTID, SqlDbType.VarChar, 5)
            params(0).Value = customclass.GroupAlertID

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, SPGROUPALERTVIEW, params)
            If objread.Read Then
                With customclass
                    .GroupAlertID = objread("GroupAlertID").ToString.Trim
                    .GroupAlertName = objread("GroupAlertName").ToString.Trim
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region
End Class
