

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class MasterForm : Inherits Maxiloan.SQLEngine.DataAccessBase
    Private Const spListMasterForm As String = "spFormMasterPaging"
    Private Const spListMasterApplication As String = "spGetMsApplication"

    Private Const spFormMasterAdd As String = "spFormMasterAdd"
    Private Const spFormMasterUpdate As String = "spFormMasterUpdate"
    Private Const spFormMasterDelete As String = "spFormMasterDelete"
    Private Const spFormMasterShow As String = "spFormMasterShow"

    Private Const PARAM_FORMNAME As String = "@formname"
    Private Const PARAM_FORMFILENAME As String = "@formfilename"
    Private Const PARAM_APPLICATIONID As String = "@applicationid"
    Private Const PARAM_CALLBYMENU As String = "@callbymenu"
    Private Const PARAM_SUBSYSTEM As String = "@subsystem"
    Private Const PARAM_ICON As String = "@icon"


    Private m_connection As SqlConnection
    Public Sub New()
        MyBase.New()
        m_connection = New SqlConnection(GetReferenceDataConnectionString)
    End Sub

    Public Function ListMasterForm(ByVal customclass As Parameter.MasterForm) As Parameter.MasterForm

        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        customclass.ListMasterForm = SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, spListMasterForm, params).Tables(0)
        customclass.ListMasterApplication = SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, spListMasterApplication).Tables(0)
        customclass.TotalRecord = CInt(params(4).Value)

        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return customclass
    End Function

    Public Function AddMasterForm(ByVal customclass As Parameter.MasterForm) As String

        Dim params() As SqlParameter = New SqlParameter(7) {}

        params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 10)
        params(0).Value = customclass.Applicationid

        params(1) = New SqlParameter(PARAM_FORMID, SqlDbType.VarChar, 20)
        params(1).Value = customclass.FormID

        params(2) = New SqlParameter(PARAM_FORMNAME, SqlDbType.VarChar, 50)
        params(2).Value = customclass.FormName

        params(3) = New SqlParameter(PARAM_FORMFILENAME, SqlDbType.VarChar, 200)
        params(3).Value = customclass.FormFileName

        params(4) = New SqlParameter(PARAM_CALLBYMENU, SqlDbType.Bit)
        params(4).Value = customclass.CallByMenu

        params(5) = New SqlParameter(PARAM_SUBSYSTEM, SqlDbType.VarChar, 5)
        params(5).Value = customclass.SubSystem

        params(6) = New SqlParameter(PARAM_ICON, SqlDbType.VarChar, 50)
        params(6).Value = customclass.Icon

        params(7) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
        params(7).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, spFormMasterAdd, params)

        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return CType(params(7).Value, String)
    End Function

    Public Function UpdateMasterForm(ByVal customclass As Parameter.MasterForm) As String

        Dim params() As SqlParameter = New SqlParameter(7) {}

        params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 10)
        params(0).Value = customclass.Applicationid

        params(1) = New SqlParameter(PARAM_FORMID, SqlDbType.VarChar, 20)
        params(1).Value = customclass.FormID

        params(2) = New SqlParameter(PARAM_FORMNAME, SqlDbType.VarChar, 50)
        params(2).Value = customclass.FormName

        params(3) = New SqlParameter(PARAM_FORMFILENAME, SqlDbType.VarChar, 200)
        params(3).Value = customclass.FormFileName

        params(4) = New SqlParameter(PARAM_CALLBYMENU, SqlDbType.Bit)
        params(4).Value = customclass.CallByMenu

        params(5) = New SqlParameter(PARAM_SUBSYSTEM, SqlDbType.VarChar, 5)
        params(5).Value = customclass.SubSystem

        params(6) = New SqlParameter(PARAM_ICON, SqlDbType.VarChar, 50)
        params(6).Value = customclass.Icon

        params(7) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
        params(7).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, spFormMasterUpdate, params)

        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return CType(params(7).Value, String)
    End Function

    Public Function DeleteMasterForm(ByVal customclass As Parameter.MasterForm) As String

        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 10)
        params(0).Value = customclass.Applicationid

        params(1) = New SqlParameter(PARAM_FORMID, SqlDbType.VarChar, 20)
        params(1).Value = customclass.FormID

        params(2) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
        params(2).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, spFormMasterDelete, params)

        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return CType(params(2).Value, String)
    End Function

    Public Function ShowMasterForm(ByVal customclass As Parameter.MasterForm) As Parameter.MasterForm
        Dim objread As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 10)
        params(0).Value = customclass.Applicationid

        params(1) = New SqlParameter(PARAM_FORMID, SqlDbType.VarChar, 20)
        params(1).Value = customclass.FormID

        objread = SqlHelper.ExecuteReader(m_connection, CommandType.StoredProcedure, spFormMasterShow, params)
        If objread.Read Then
            With customclass
                .FormID = CType(objread("formid"), String)
                .FormName = CType(objread("formname"), String)
                .FormFileName = CType(objread("formfilename"), String)
                .CallByMenu = CType(objread("callbymenu"), Boolean)
                .SubSystem = CType(objread("subsystem"), String)
                .Icon = CType(objread("icon"), String)
            End With
        Else
            With customclass
                .FormID = ""
                .FormName = ""
                .FormFileName = ""
                .CallByMenu = False
                .SubSystem = ""
                .Icon = ""
            End With
        End If
        objread.Close()
        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return customclass
    End Function
End Class
