
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class MasterNews : Inherits Maxiloan.SQLEngine.DataAccessBase

    Private Const PARAM_NEWSYEAR As String = "@NewsYear"
    Private Const PARAM_NEWSNUMBER As String = "@NewsNumber"
    Private Const PARAM_POSTINGDATE As String = "@PostingDate"
    Private Const PARAM_AUTHORBY As String = "@Authorby"
    Private Const PARAM_TITLE As String = "@Title"
    Private Const PARAM_ABSTRACT As String = "@Abstract"
    Private Const PARAM_DETAILNEWS As String = "@DetailNews"
    Private Const PARAM_IMAGENEWS As String = "@ImageNews"

    Private Const SPNEWSADD As String = "spNewsAdd"
    Private Const SPNEWSEDIT As String = "spNewsEdit"
    Private Const SPNEWSDELETE As String = "spNewsDelete"
    Private Const SPNEWSVIEW As String = "spNewsView"

    Private Const SPNEWSDETAILUPDATE As String = "spNewsDetailUpdate"
    Private Const SPNEWSDETAILVIEW As String = "spNewsDetailView"

#Region "Master News"
    Public Sub MasterNewsAdd(ByVal customclass As Parameter.News)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(4) {}

            params(0) = New SqlParameter(PARAM_NEWSYEAR, SqlDbType.VarChar, 20)
            params(0).Value = customclass.NewsYear

            params(1) = New SqlParameter(PARAM_POSTINGDATE, SqlDbType.DateTime)
            params(1).Value = customclass.PostingDate

            params(2) = New SqlParameter(PARAM_AUTHORBY, SqlDbType.VarChar, 30)
            params(2).Value = customclass.AuthorBy

            params(3) = New SqlParameter(PARAM_TITLE, SqlDbType.VarChar, 250)
            params(3).Value = customclass.Title

            params(4) = New SqlParameter(PARAM_ABSTRACT, SqlDbType.VarChar, 4000)
            params(4).Value = customclass.Abstract

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPNEWSADD, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        End Try
    End Sub

    Public Sub MasterNewsDelete(ByVal customclass As Parameter.News)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(1) {}

            params(0) = New SqlParameter(PARAM_NEWSYEAR, SqlDbType.VarChar, 20)
            params(0).Value = customclass.NewsYear

            params(1) = New SqlParameter(PARAM_NEWSNUMBER, SqlDbType.VarChar, 50)
            params(1).Value = customclass.NewsNumber

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPNEWSDELETE, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        End Try
    End Sub

    Public Sub MasterNewsEdit(ByVal customclass As Parameter.News)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(5) {}

            params(0) = New SqlParameter(PARAM_NEWSYEAR, SqlDbType.VarChar, 4)
            params(0).Value = customclass.NewsYear

            params(1) = New SqlParameter(PARAM_NEWSNUMBER, SqlDbType.Int)
            params(1).Value = customclass.NewsNumber

            params(2) = New SqlParameter(PARAM_POSTINGDATE, SqlDbType.DateTime)
            params(2).Value = customclass.PostingDate

            params(3) = New SqlParameter(PARAM_AUTHORBY, SqlDbType.VarChar, 30)
            params(3).Value = customclass.AuthorBy

            params(4) = New SqlParameter(PARAM_TITLE, SqlDbType.VarChar, 250)
            params(4).Value = customclass.Title

            params(5) = New SqlParameter(PARAM_ABSTRACT, SqlDbType.VarChar, 4000)
            params(5).Value = customclass.Abstract


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPNEWSEDIT, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        End Try
    End Sub

    Public Function MasterNewsView(ByVal customclass As Parameter.News) As Parameter.News
        Dim objread As SqlDataReader
        Try
            Dim params() As SqlParameter = New SqlParameter(1) {}
            params(0) = New SqlParameter(PARAM_NEWSYEAR, SqlDbType.VarChar, 4)
            params(0).Value = customclass.NewsYear

            params(1) = New SqlParameter(PARAM_NEWSNUMBER, SqlDbType.Int)
            params(1).Value = customclass.NewsNumber
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, SPNEWSVIEW, params)
            If objread.Read Then
                With customclass
                    .PostingDate = CType(objread("PostingDate"), DateTime)
                    .Title = objread("Title").ToString.Trim
                    .Abstract = objread("Abstract").ToString.Trim
                    .AuthorBy = objread("AuthorBy").ToString.Trim
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region

#Region "News Detail"
    Public Function NewsDetailView(ByVal customclass As Parameter.News) As Parameter.News
        Dim objread As SqlDataReader
        Try
            Dim params() As SqlParameter = New SqlParameter(1) {}
            params(0) = New SqlParameter(PARAM_NEWSYEAR, SqlDbType.VarChar, 4)
            params(0).Value = customclass.NewsYear

            params(1) = New SqlParameter(PARAM_NEWSNUMBER, SqlDbType.Int)
            params(1).Value = customclass.NewsNumber
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, SPNEWSDETAILVIEW, params)
            If objread.Read Then
                With customclass
                    .NewsDetail = IIf(IsDBNull(objread("DetailNews")), "", objread("DetailNews")).ToString.Trim
                    .ImageNews = IIf(IsDBNull(objread("ImageNews")), "", objread("ImageNews")).ToString.Trim
                    .NewsYear = IIf(IsDBNull(objread("NewsYear")), "", objread("NewsYear")).ToString.Trim
                    .NewsNumber = CInt(IIf(IsDBNull(objread("NewsYear")), "", objread("NewsYear")))
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Sub NewsDetailUpdate(ByVal customclass As Parameter.News)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(3) {}
            params(0) = New SqlParameter(PARAM_NEWSYEAR, SqlDbType.VarChar, 4)
            params(0).Value = customclass.NewsYear

            params(1) = New SqlParameter(PARAM_NEWSNUMBER, SqlDbType.Int)
            params(1).Value = customclass.NewsNumber

            params(2) = New SqlParameter(PARAM_DETAILNEWS, SqlDbType.Text)
            params(2).Value = customclass.NewsDetail

            params(3) = New SqlParameter(PARAM_IMAGENEWS, SqlDbType.VarChar, 200)
            params(3).Value = customclass.ImageNews
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPNEWSDETAILUPDATE, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        End Try
    End Sub
#End Region
End Class
