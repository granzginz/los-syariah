

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class GroupUser : Inherits Maxiloan.SQLEngine.DataAccessBase
    Private Const spGroupUserList As String = "spGroupUserPaging"
    Private Const spGroupUserAdd As String = "spGroupUserAdd"
    Private Const spGroupUserDelete As String = "spGroupUserDelete"
    Private Const spListMasterApplication As String = "spGetMSApplication"
    Private Const spGroupUserUpdate As String = "spGroupUserUpdate"

    Private Const PARAM_GROUPID As String = "@groupid"
    Private Const PARAM_GROUPNAME As String = "@groupname"
    Private Const PARAM_APLLICATIONID As String = "@applicationid"

    Private m_connection As SqlConnection

    Public Sub New()
        MyBase.New()
        m_connection = New SqlConnection(GetReferenceDataConnectionString)
    End Sub

    Public Function ListGroupUser(ByVal customclass As Parameter.GroupUser) As Parameter.GroupUser
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        customclass.ListGroupUser = SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, spGroupUserList, params).Tables(0)
        customclass.ListMasterApplication = SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, spListMasterApplication).Tables(0)
        customclass.TotalRecord = CInt(params(4).Value)

        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return customclass
    End Function

    Public Function AddGroupUser(ByVal customclass As Parameter.GroupUser) As String
        Dim params() As SqlParameter = New SqlParameter(3) {}

        params(0) = New SqlParameter(PARAM_APLLICATIONID, SqlDbType.VarChar, 10)
        params(0).Value = customclass.ApplicationId

        params(1) = New SqlParameter(PARAM_GROUPID, SqlDbType.VarChar, 5)
        params(1).Value = customclass.GroupID

        params(2) = New SqlParameter(PARAM_GROUPNAME, SqlDbType.Char, 50)
        params(2).Value = customclass.GroupName

        params(3) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
        params(3).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, spGroupUserAdd, params)

        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return CStr(params(3).Value)
    End Function

    Public Function UpdateGroupUser(ByVal customclass As Parameter.GroupUser) As String
        Dim params() As SqlParameter = New SqlParameter(3) {}

        params(0) = New SqlParameter(PARAM_APLLICATIONID, SqlDbType.VarChar, 10)
        params(0).Value = customclass.ApplicationId

        params(1) = New SqlParameter(PARAM_GROUPID, SqlDbType.VarChar, 5)
        params(1).Value = customclass.GroupID

        params(2) = New SqlParameter(PARAM_GROUPNAME, SqlDbType.Char, 50)
        params(2).Value = customclass.GroupName

        params(3) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
        params(3).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, spGroupUserUpdate, params)

        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return CStr(params(3).Value)
    End Function

    Public Function DeleteGroupUser(ByVal customclass As Parameter.GroupUser) As String        
        Dim params() As SqlParameter = New SqlParameter(2) {}        

        params(0) = New SqlParameter(PARAM_APLLICATIONID, SqlDbType.VarChar, 10)
        params(0).Value = customclass.ApplicationId

        params(1) = New SqlParameter(PARAM_GROUPID, SqlDbType.VarChar, 5)
        params(1).Value = customclass.GroupID

        params(2) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
        params(2).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, spGroupUserDelete, params)

        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return CStr(params(2).Value)
    End Function

End Class
