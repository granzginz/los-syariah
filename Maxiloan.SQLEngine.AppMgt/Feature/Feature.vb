
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class Feature : Inherits Maxiloan.SQLEngine.DataAccessBase
    Private Const spListMasterForm As String = "spFeaturePaging"
    Private Const spListFeatureUser As String = "spFeatureUserPaging"
    Private Const spUpdateFeatureUser As String = "spFeatureUserUpdate"

    Private Const PARAM_FORMNAME As String = "@formname"
    Private Const PARAM_FORMFILENAME As String = "@formfilename"
    Private Const PARAM_APPLICATIONID As String = "@applicationid"
    Private Const PARAM_CALLBYMENU As String = "@callbymenu"
    Private Const PARAM_SUBSYSTEM As String = "@subsystem"
    Private Const PARAM_ICON As String = "@icon"

    Private m_connection As SqlConnection
    Public Sub New()
        MyBase.New()
        m_connection = New SqlConnection(GetReferenceDataConnectionString)
    End Sub
    Public Function ListMasterForm(ByVal customclass As Parameter.Feature) As Parameter.Feature

        Dim params() As SqlParameter = New SqlParameter(6) {}
        Try
            params(0) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 10)
            params(0).Value = customclass.AppID

            params(1) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 10)
            params(1).Value = customclass.LoginId

            params(2) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(2).Value = customclass.CurrentPage

            params(3) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(3).Value = customclass.PageSize

            params(4) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(4).Value = customclass.WhereCond

            params(5) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(5).Value = customclass.SortBy

            params(6) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(6).Direction = ParameterDirection.Output

            customclass.ListMasterForm = SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, spListMasterForm, params).Tables(0)
            customclass.TotalRecord = CInt(params(6).Value)

            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
            Return customclass
        Catch exp As Exception
            WriteException("Feature", "ListMasterForm", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListFeatureUser(ByVal customclass As Parameter.Feature) As Parameter.Feature

        Dim params() As SqlParameter = New SqlParameter(3) {}
        Try
            params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 10)
            params(0).Value = customclass.LoginId

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 10)
            params(1).Value = customclass.AppID

            params(2) = New SqlParameter(PARAM_FORMID, SqlDbType.VarChar, 20)
            params(2).Value = customclass.FormID

            params(3) = New SqlParameter(PARAM_FORMFILENAME, SqlDbType.VarChar, 50)
            params(3).Direction = ParameterDirection.Output

            customclass.ListFeatureUser = SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, spListFeatureUser, params).Tables(0)
            customclass.FormName = CStr(params(3).Value)

            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
            Return customclass
        Catch exp As Exception
            WriteException("Feature", "ListFeatureUser", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function UpdateFeatureUser(ByVal customclass As Parameter.Feature) As String

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 10)
            params(0).Value = customclass.LoginId

            params(1) = New SqlParameter(PARAM_APPLICATIONID, SqlDbType.VarChar, 10)
            params(1).Value = customclass.AppID

            params(2) = New SqlParameter(PARAM_FORMID, SqlDbType.VarChar, 20)
            params(2).Value = customclass.FormID

            params(3) = New SqlParameter(PARAM_FEATUREID, SqlDbType.VarChar, 1000)
            params(3).Value = customclass.FeatureID

            params(4) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 50)
            params(4).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, spUpdateFeatureUser, params)

            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
            Return CStr(params(4).Value)
        Catch exp As Exception
            WriteException("Feature", "UpdateFeatureUser", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
