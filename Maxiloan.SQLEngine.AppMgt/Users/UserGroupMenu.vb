
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.General
#End Region

Public Class UserGroupMenu : Inherits maxiloan.SQLEngine.DataAccessBase
    Private m_connection As SqlConnection
    Private Const spListGroupMenu As String = "spUserGroupMenuPaging"
    Private Const spUserGroupMenuUpdate As String = "spUserGroupMenuUpdate"


    Private Const PARAM_APP As String = "@appid"
    Private Const PARAM_GROUPDBID As String = "@groupdbid"

    Private Const PARAM_GROUPDBALL As String = "@groupdball"
    Private Const PARAM_GROUPMENUID As String = "@groupmenuid"
    Private Const PARAM_GROUPMENUITEM As String = "@groupmenuiditem"
    Public Sub New()
        MyBase.New()
        m_connection = New SqlConnection(GetReferenceDataConnectionString)
    End Sub

    Public Function ListUserGroupMenu(ByVal customclass As Parameter.UserGroupMenu) As Parameter.UserGroupMenu
        Dim params() As SqlParameter = New SqlParameter(7) {}

        params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
        params(0).Value = customclass.LoginId

        params(1) = New SqlParameter(PARAM_APP, SqlDbType.VarChar, 30)
        params(1).Value = customclass.ApplicationId

        params(2) = New SqlParameter(PARAM_GROUPDBID, SqlDbType.VarChar, 500)
        params(2).Value = customclass.GroupDBID

        params(3) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(3).Value = customclass.CurrentPage

        params(4) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(4).Value = customclass.PageSize

        params(5) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(5).Value = customclass.WhereCond

        params(6) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(6).Value = customclass.SortBy

        params(7) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(7).Direction = ParameterDirection.Output

        customclass.ListUserGroupDb = SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, spListGroupMenu, params).Tables(0)
        customclass.TotalRecord = CInt(params(7).Value)
        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return customclass
    End Function

    Public Function UpdateUserGroupMenu(ByVal customclass As Parameter.UserGroupMenu) As String
        Dim params() As SqlParameter = New SqlParameter(6) {}
        params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
        params(0).Value = customclass.LoginId

        params(1) = New SqlParameter(PARAM_APP, SqlDbType.VarChar, 30)
        params(1).Value = customclass.ApplicationId

        params(2) = New SqlParameter(PARAM_GROUPDBID, SqlDbType.VarChar, 3)
        params(2).Value = customclass.GroupDBID

        params(3) = New SqlParameter(PARAM_GROUPDBALL, SqlDbType.VarChar, 500)
        params(3).Value = customclass.GroupDBAll

        params(4) = New SqlParameter(PARAM_GROUPMENUID, SqlDbType.VarChar, 500)
        params(4).Value = customclass.GroupDBMenu

        params(5) = New SqlParameter(PARAM_GROUPMENUITEM, SqlDbType.Int)
        params(5).Value = customclass.GroupDBMenuItem

        params(6) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 1)
        params(6).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, spUserGroupMenuUpdate, params)

        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return CStr(params(6).Value)
    End Function
End Class
