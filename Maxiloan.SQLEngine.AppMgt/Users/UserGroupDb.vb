

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.General
#End Region

Public Class UserGroupDb : Inherits maxiloan.SQLEngine.DataAccessBase
    Private Const spListGroupDb As String = "spUserGroupDataPaging"
    Private Const spUserGroupDBUpdate As String = "spUserGroupDataUpdate"


    Private Const PARAM_APP As String = "@appid"
    Private Const PARAM_GROUPDBID As String = "@groupdbid"
    Private Const PARAM_GROUPDBALL As String = "@groupdball"
    Private Const PARAM_GROUPDBIDITEM As String = "@groupdbiditem"

    Private m_connection As SqlConnection

    Public Sub New()
        MyBase.New()
        m_connection = New SqlConnection(GetReferenceDataConnectionString)
    End Sub

    Public Function ListUserGroupDb(ByVal customclass As Parameter.UserGroupDB) As Parameter.UserGroupDB
        Dim params() As SqlParameter = New SqlParameter(6) {}        

        params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
        params(0).Value = customclass.LoginId

        params(1) = New SqlParameter(PARAM_APP, SqlDbType.VarChar, 30)
        params(1).Value = customclass.ApplicationId

        params(2) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(2).Value = customclass.CurrentPage

        params(3) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(3).Value = customclass.PageSize

        params(4) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(4).Value = customclass.WhereCond

        params(5) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(5).Value = customclass.SortBy

        params(6) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(6).Direction = ParameterDirection.Output

        customclass.ListUserGroupDb = SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, spListGroupDb, params).Tables(0)
        customclass.TotalRecord = CInt(params(6).Value)
        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return customclass
    End Function

    Public Function UpdateUserGroupDB(ByVal customclass As Parameter.UserGroupDB) As String
        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
        params(0).Value = customclass.LoginId

        params(1) = New SqlParameter(PARAM_APP, SqlDbType.VarChar, 30)
        params(1).Value = customclass.ApplicationId

        params(2) = New SqlParameter(PARAM_GROUPDBID, SqlDbType.VarChar, 500)
        params(2).Value = customclass.GroupDBID

        params(3) = New SqlParameter(PARAM_GROUPDBALL, SqlDbType.VarChar, 500)
        params(3).Value = customclass.GroubDBAll

        params(4) = New SqlParameter(PARAM_GROUPDBIDITEM, SqlDbType.Int)
        params(4).Value = customclass.GroupDBIDItem

        params(5) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
        params(5).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, spUserGroupDBUpdate, params)

        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return CStr(params(4).Value)
    End Function
End Class
