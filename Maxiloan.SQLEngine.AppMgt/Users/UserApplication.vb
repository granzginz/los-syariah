
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.General

#End Region

Public Class UserApplication : Inherits maxiloan.SQLEngine.DataAccessBase
    Private Const spListApplication As String = "spUserApplicationPaging"
    Private Const spListMasterApplication As String = "spGetMsApplication"
    Private Const spUserApplicationDelete As String = "spUserApplicationDelete"
    Private Const spUserApplicationUpdate As String = "spUserApplicationUpdate"
    Private Const spUserApplicationAdd As String = "spUserApplicationAdd"
    Private Const PARAM_APP As String = "@applicationid"
    Private Const PARAM_ISACTIVE As String = "@isactive"
    Private Const PARAM_ORDER As String = "@order"

    Private m_connection As SqlConnection

    Public Sub New()
        MyBase.New()
        m_connection = New SqlConnection(GetReferenceDataConnectionString)
    End Sub

    Public Function ListUserApplication(ByVal customclass As Parameter.UserApplication) As Parameter.UserApplication       
        Dim params() As SqlParameter = New SqlParameter(5) {}        

        params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
        params(0).Value = customclass.LoginId

        params(1) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(1).Value = customclass.CurrentPage

        params(2) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(2).Value = customclass.PageSize

        params(3) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(3).Value = customclass.WhereCond

        params(4) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(4).Value = customclass.SortBy

        params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(5).Direction = ParameterDirection.Output

        customclass.ListUserApplication = SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, spListApplication, params).Tables(0)
        customclass.ListMasterApplication = SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, spListMasterApplication).Tables(0)
        customclass.TotalRecord = CInt(params(5).Value)

        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return customclass
    End Function

    Public Function UpdateUserApplication(ByVal customclass As Parameter.UserApplication) As String
        Dim params() As SqlParameter = New SqlParameter(4) {}


        params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
        params(0).Value = customclass.LoginId

        params(1) = New SqlParameter(PARAM_APP, SqlDbType.VarChar, 30)
        params(1).Value = customclass.ApplicationId

        params(2) = New SqlParameter(PARAM_ISACTIVE, SqlDbType.Bit)
        params(2).Value = customclass.isActive

        params(3) = New SqlParameter(PARAM_ORDER, SqlDbType.Decimal)
        params(3).Value = customclass.Order

        params(4) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
        params(4).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, spUserApplicationUpdate, params)

        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return CStr(params(4).Value)
    End Function

    Public Function DeleteUserApplication(ByVal customclass As Parameter.UserApplication) As String       
        Dim params() As SqlParameter = New SqlParameter(2) {}        

        params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
        params(0).Value = customclass.LoginId

        params(1) = New SqlParameter(PARAM_APP, SqlDbType.VarChar, 30)
        params(1).Value = customclass.ApplicationId

        params(2) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
        params(2).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, spUserApplicationDelete, params)

        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return CStr(params(1).Value)
    End Function

    Public Function AddUserApplication(ByVal customclass As Parameter.UserApplication) As String
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
        params(0).Value = customclass.LoginId

        params(1) = New SqlParameter(PARAM_APP, SqlDbType.VarChar, 30)
        params(1).Value = customclass.ApplicationId

        params(2) = New SqlParameter(PARAM_ISACTIVE, SqlDbType.Bit)
        params(2).Value = customclass.isActive

        params(3) = New SqlParameter(PARAM_ORDER, SqlDbType.Decimal)
        params(3).Value = customclass.Order

        params(4) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
        params(4).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, spUserApplicationAdd, params)

        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return CStr(params(4).Value)
    End Function
End Class
