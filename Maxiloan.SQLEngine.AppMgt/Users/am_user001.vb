

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.General
#End Region

Public Class am_user001 : Inherits maxiloan.SQLEngine.DataAccessBase
    Private Const spListUser As String = "spUserListPaging"
    Private Const spListEmployee As String = "spEmployeePaging"
    Private Const spUserListAdd As String = "spUserListAdd"
    Private Const spUserListDelete As String = "spUserListDelete"
    Private Const spUserListUpdate As String = "spUserListUpdate"

    Private Const PARAM_FULLNAME As String = "@fullname"
    Private Const PARAM_EMPLOYEEID As String = "@employeeid"
    Private Const PARAM_USERLEVEL As String = "@userlevel"
    Private Const PARAM_PASSWORD As String = "@password"
    Private Const PARAM_ISACTIVE As String = "@isactive"
    Private Const PARAM_ISCONNECT As String = "@isconnect"
    Private Const PARAM_USERCREATE As String = "@usrcrt"
    Private Const PARAM_DBNAME As String = "@dbname"

    Private m_connection As SqlConnection

    Public Sub New()
        MyBase.New()
        'm_connection = New SqlConnection(Me.GetReferenceDataConnectionString)
        m_connection = New SqlConnection(GetReferenceDataConnectionString)
    End Sub

    Public Function ListUserList(ByVal customclass As Parameter.am_user001) As Parameter.am_user001

        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        customclass.ListUserList = SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, spListUser, params).Tables(0)
        customclass.TotalRecord = CInt(params(4).Value)

        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return customclass
    End Function

    Public Function AddUserList(ByVal customclass As Parameter.am_user001) As String
        Dim params() As SqlParameter = New SqlParameter(9) {}

        params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
        params(0).Value = customclass.LoginId

        params(1) = New SqlParameter(PARAM_EMPLOYEEID, SqlDbType.VarChar, 30)
        params(1).Value = customclass.EmployeeID

        params(2) = New SqlParameter(PARAM_FULLNAME, SqlDbType.VarChar, 30)
        params(2).Value = customclass.FullName

        params(3) = New SqlParameter(PARAM_USERLEVEL, SqlDbType.Char, 2)
        params(3).Value = customclass.UserLevel

        params(4) = New SqlParameter(PARAM_PASSWORD, SqlDbType.Char, 20)
        params(4).Value = customclass.Password

        params(5) = New SqlParameter(PARAM_ISACTIVE, SqlDbType.Bit)
        params(5).Value = customclass.isActive

        params(6) = New SqlParameter(PARAM_USERCREATE, SqlDbType.VarChar, 12)
        params(6).Value = customclass.UserCreate

        params(7) = New SqlParameter(PARAM_DBNAME, SqlDbType.VarChar, 10)
        params(7).Value = customclass.DBName

        params(8) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
        params(8).Direction = ParameterDirection.Output

        params(9) = New SqlParameter("@PassKeyPrefix", SqlDbType.VarChar, 8)
        params(9).Value = GetPassKeyPrefix()



        Try
            SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, spUserListAdd, params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try



        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If


        Return CStr(params(8).Value)
    End Function

    Public Function UpdateUserList(ByVal customclass As Parameter.am_user001) As String
        Dim params() As SqlParameter = New SqlParameter(8) {}

        params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
        params(0).Value = customclass.LoginId

        params(1) = New SqlParameter(PARAM_FULLNAME, SqlDbType.VarChar, 30)
        params(1).Value = customclass.FullName

        params(2) = New SqlParameter(PARAM_USERLEVEL, SqlDbType.Char, 2)
        params(2).Value = customclass.UserLevel

        params(3) = New SqlParameter(PARAM_PASSWORD, SqlDbType.Char, 20)
        params(3).Value = customclass.Password

        params(4) = New SqlParameter(PARAM_ISACTIVE, SqlDbType.Bit)
        params(4).Value = customclass.isActive

        params(5) = New SqlParameter(PARAM_ISCONNECT, SqlDbType.Bit)
        params(5).Value = customclass.isConnect

        params(6) = New SqlParameter(PARAM_USERCREATE, SqlDbType.VarChar, 12)
        params(6).Value = customclass.UserCreate

        params(7) = New SqlParameter(PARAM_DBNAME, SqlDbType.VarChar, 10)
        params(7).Value = customclass.DBName

        params(8) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
        params(8).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, spUserListUpdate, params)

        If Not IsNothing(m_connection) Then
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End If
        Return CStr(params(7).Value)
    End Function

    'Public Function DeleteUserList(ByVal customclass As Parameter.am_user001) As String
    '    Dim params() As SqlParameter = New SqlParameter(2) {}

    '    params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
    '    params(0).Value = customclass.LoginId
    '    params(1) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
    '    params(1).Direction = ParameterDirection.Output
    '    params(2) = New SqlParameter("@PasswordPrefix", SqlDbType.VarChar, 50)
    '    params(2).Value = GetPassKeyPrefix()



    '    SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, spUserListDelete, params)

    '    If Not IsNothing(m_connection) Then
    '        If m_connection.State = ConnectionState.Open Then m_connection.Close()
    '        m_connection.Dispose()
    '    End If
    '    Return CStr(params(1).Value)
    'End Function

    Public Function DeleteUserList(ByVal customclass As Parameter.am_user001) As Parameter.am_user001
        Try
            Dim params() As SqlParameter = New SqlParameter(2) {}

            params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(0).Value = customclass.LoginId
            params(1) = New SqlParameter(PARAM_STRERROR, SqlDbType.VarChar, 100)
            params(1).Direction = ParameterDirection.Output
            params(2) = New SqlParameter("@PasswordPrefix", SqlDbType.VarChar, 50)
            params(2).Value = GetPassKeyPrefix()



            customclass.ListUserGroupDB = SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, spUserListDelete, params).Tables(0)

            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If

            customclass.strError = params(1).Value.ToString
            Return customclass
        Catch ex As Exception
            customclass.strError = ex.Message
            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
            Return customclass
        End Try
       

    End Function

    Public Function ListEmployee(ByVal customclass As Parameter.am_user001) As Parameter.am_user001
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output
        Try

            If customclass.strConnection = "" Then
                customclass.ListEmployee = SqlHelper.ExecuteDataset(m_connection, CommandType.StoredProcedure, spListEmployee, params).Tables(0)
            Else
                customclass.ListEmployee = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListEmployee, params).Tables(0)
            End If

            customclass.TotalRecord = CInt(params(4).Value)

            If Not IsNothing(m_connection) Then
                If m_connection.State = ConnectionState.Open Then m_connection.Close()
                m_connection.Dispose()
            End If
            Return customclass
        Catch ex As Exception
            Throw
        End Try

    End Function


    
End Class
