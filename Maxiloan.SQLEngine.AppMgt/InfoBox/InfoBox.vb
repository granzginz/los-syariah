
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InfoBox : Inherits Maxiloan.SQLEngine.DataAccessBase
    Private Const PARAM_INFOBOXID As String = "@InfoBoxID"
    Private Const PARAM_INFOBOXTITLE As String = "@InfoBoxTitle"
    Private Const PARAM_ICON As String = "@Icon"
    Private Const PARAM_ISGENERAL As String = "@IsGeneral"
    Private Const PARAM_INFOBOXSIDE As String = "@InfoBoxSide"
    Private Const PARAM_INCLUDEFILENAME As String = "@IncludeFileName"
    Private Const PARAM_INFOBOXTYPE As String = "@InfoBoxType"
    Private Const PARAM_FONTCOLOR As String = "@FontColor"
    Private Const PARAM_BGCOLOR As String = "@BGColor"
    Private Const PARAM_ORDER As String = "@Order"

    Private Const PARAM_GROUPINFOBOXID As String = "@GroupInfoBoxID"
    Private Const PARAM_GROUPALLINFOBOXID As String = "@GroupAllInfoBoxID"

    Private Const SPINFOBOXADD As String = "spInfoBoxAdd"
    Private Const SPINFOBOXEDIT As String = "spInfoBoxEdit"
    Private Const SPINFOBOXDELETE As String = "spInfoBoxDelete"
    Private Const SPINFOBOXVIEW As String = "spInfoBoxView"

    Private Const SPUSERINFOBOXPAGING As String = "spUserInfoBoxPaging"
    Private Const SPUSERINFOBOXPUPDATE As String = "spUserInfoBoxUpdate"

#Region "Info Box"


    Public Sub InfoBoxAdd(ByVal customclass As Parameter.InfoBox)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(9) {}

            params(0) = New SqlParameter(PARAM_INFOBOXID, SqlDbType.VarChar, 5)
            params(0).Value = customclass.InfoBoxID

            params(1) = New SqlParameter(PARAM_INFOBOXTITLE, SqlDbType.VarChar, 30)
            params(1).Value = customclass.InfoBoxTitle

            params(2) = New SqlParameter(PARAM_ICON, SqlDbType.VarChar, 50)
            params(2).Value = customclass.Icon

            params(3) = New SqlParameter(PARAM_ISGENERAL, SqlDbType.Bit)
            params(3).Value = customclass.IsGeneral

            params(4) = New SqlParameter(PARAM_INFOBOXSIDE, SqlDbType.Char, 1)
            params(4).Value = customclass.InfoBoxSide

            params(5) = New SqlParameter(PARAM_INCLUDEFILENAME, SqlDbType.VarChar, 100)
            params(5).Value = customclass.IncludeFileName

            params(6) = New SqlParameter(PARAM_INFOBOXTYPE, SqlDbType.Char, 1)
            params(6).Value = customclass.InfoBoxType

            params(7) = New SqlParameter(PARAM_FONTCOLOR, SqlDbType.VarChar, 10)
            params(7).Value = customclass.FontColor

            params(8) = New SqlParameter(PARAM_BGCOLOR, SqlDbType.VarChar, 10)
            params(8).Value = customclass.BGColor

            params(9) = New SqlParameter(PARAM_ORDER, SqlDbType.Int)
            params(9).Value = customclass.Order

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPINFOBOXADD, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        End Try

    End Sub

    Public Sub InfoBoxDelete(ByVal customclass As Parameter.InfoBox)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(0) {}


            params(0) = New SqlParameter(PARAM_INFOBOXID, SqlDbType.VarChar, 5)
            params(0).Value = customclass.InfoBoxID


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPINFOBOXDELETE, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        End Try

    End Sub

    Public Sub InfoBoxEdit(ByVal customclass As Parameter.InfoBox)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(9) {}

            params(0) = New SqlParameter(PARAM_INFOBOXID, SqlDbType.VarChar, 5)
            params(0).Value = customclass.InfoBoxID

            params(1) = New SqlParameter(PARAM_INFOBOXTITLE, SqlDbType.VarChar, 30)
            params(1).Value = customclass.InfoBoxTitle

            params(2) = New SqlParameter(PARAM_ICON, SqlDbType.VarChar, 50)
            params(2).Value = customclass.Icon

            params(3) = New SqlParameter(PARAM_ISGENERAL, SqlDbType.Bit)
            params(3).Value = customclass.IsGeneral

            params(4) = New SqlParameter(PARAM_INFOBOXSIDE, SqlDbType.Char, 1)
            params(4).Value = customclass.InfoBoxSide

            params(5) = New SqlParameter(PARAM_INCLUDEFILENAME, SqlDbType.VarChar, 100)
            params(5).Value = customclass.IncludeFileName

            params(6) = New SqlParameter(PARAM_INFOBOXTYPE, SqlDbType.Char, 1)
            params(6).Value = customclass.InfoBoxType

            params(7) = New SqlParameter(PARAM_FONTCOLOR, SqlDbType.VarChar, 10)
            params(7).Value = customclass.FontColor

            params(8) = New SqlParameter(PARAM_BGCOLOR, SqlDbType.VarChar, 10)
            params(8).Value = customclass.BGColor

            params(9) = New SqlParameter(PARAM_ORDER, SqlDbType.Int)
            params(9).Value = customclass.Order
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPINFOBOXEDIT, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        End Try

    End Sub

    Public Function InfoBoxView(ByVal customclass As Parameter.InfoBox) As Parameter.InfoBox
        Dim objread As SqlDataReader
        Try
            Dim params() As SqlParameter = New SqlParameter(0) {}

            params(0) = New SqlParameter(PARAM_INFOBOXID, SqlDbType.VarChar, 5)
            params(0).Value = customclass.InfoBoxID
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, SPINFOBOXVIEW, params)
            If objread.Read Then
                With customclass
                    .InfoBoxID = objread("InfoBoxID").ToString.Trim
                    .InfoBoxTitle = objread("InfoBoxTitle").ToString.Trim
                    .Icon = objread("Icon").ToString.Trim
                    .IsGeneral = CBool(objread("IsGeneral").ToString.Trim)
                    .InfoBoxSide = objread("InfoBoxSide").ToString.Trim
                    .IncludeFileName = CStr(objread("IncludeFileName"))
                    .InfoBoxType = objread("InfoBoxType").ToString.Trim
                    .FontColor = objread("FontColor").ToString.Trim
                    .BGColor = objread("BGColor").ToString.Trim
                    .Order = CInt(objread("Order_"))
                End With
            End If
            objread.Close()
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region
#Region "InfoBoxUser"
    Public Sub UserInfoBoxUpdate(ByVal customclass As Parameter.InfoBox)
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim objconnection As New SqlConnection(customclass.strConnection)
        Dim objTransaction As SqlTransaction = Nothing
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objtransaction = objconnection.BeginTransaction
            params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(0).Value = customclass.LoginId

            params(1) = New SqlParameter(PARAM_GROUPALLINFOBOXID, SqlDbType.VarChar, 8000)
            params(1).Value = customclass.GroupAllInfoBoxID

            params(2) = New SqlParameter(PARAM_GROUPINFOBOXID, SqlDbType.VarChar, 8000)
            params(2).Value = customclass.GroupInfoBoxID

            SqlHelper.ExecuteNonQuery(objtransaction, CommandType.StoredProcedure, SPUSERINFOBOXPUPDATE, params)
            objtransaction.Commit()
        Catch exp As Exception
            objtransaction.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
    End Sub

    Public Function UserInfoBoxPaging(ByVal customclass As Parameter.InfoBox) As Parameter.InfoBox
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_LOGINID, SqlDbType.VarChar, 12)
            params(0).Value = customclass.LoginId

            params(1) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(1).Value = customclass.CurrentPage

            params(2) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(2).Value = customclass.PageSize

            params(3) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(3).Value = customclass.WhereCond

            params(4) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(4).Value = customclass.SortBy

            params(5) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output

            customclass.ListUserInfoBox = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPUSERINFOBOXPAGING, params).Tables(0)
            customclass.TotalRecord = CInt(params(5).Value)
            Return customclass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function
#End Region

End Class
