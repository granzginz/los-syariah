﻿

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class AgunanLain : Inherits DataAccessBase
    Private Const spCollateralType_List As String = "spCollateralTypeList"
    Private Const spCollateralCust_List As String = "spCustomerCollateralList"
    Private Const spMITRA_BYID As String = "spMitraMultifinanceByID"
    Private Const spCollateralType_ADD As String = "spCollateralTypeAdd"
    Private Const spMITRA_EDIT As String = "spMitraMultifinanceEdit"
    Private Const spMITRA_DEL As String = "spMitraMultifinanceDelete"

    Private Const spRekMITRA_LIST As String = "spRekMitraMultifinanceList"
    Private Const spRekMITRA_BYID As String = "spRekMitraMultifinanceByID"
    Private Const spRekMITRA_ADD As String = "spRekMitraMultifinanceAdd"
    Private Const spRekMITRA_EDIT As String = "spRekMitraMultifinanceEdit"
    Private Const spRekMITRA_DEL As String = "spRekMitraMultifinanceDelete"
    Private Const spCollateralPledg_ADD As String = "spCollateralPledgingAdd"

#Region "MITRA MULTIFINANCE"

#Region "JenisAgunanList"
    Public Function JenisAgunanList(ByVal customclass As Parameter.AgunanLain) As Parameter.AgunanLain

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCollateralType_List, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("Jenis Collateral", "Jenis Collateral Paging", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "AgunanList"
    Public Function ListAgunan(ByVal customclass As Parameter.AgunanLain) As Parameter.AgunanLain

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCollateralCust_List, params).Tables(0)
            customclass.totalrecords = CType(params(4).Value, Int64)
            Return customclass
        Catch exp As Exception
            WriteException("Jenis Collateral", "Jenis Collateral Paging", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "MitrabyID"
    Public Function ListMitraByID(ByVal customclass As Parameter.AgunanLain) As Parameter.AgunanLain
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond

        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spMITRA_BYID, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("Mitra Multifinance", "ListMitraByID", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region " JenisAgunanAdd"
    Public Sub JenisAgunanAdd(ByVal oAgunan As Parameter.AgunanLain)

        Dim params(4) As SqlParameter
        Dim objcon As New SqlConnection(oAgunan.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@CollateralTypeID", SqlDbType.Char, 3)
            params(0).Value = oAgunan.JenisCollateral
            params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
            params(1).Value = oAgunan.Description
            params(2) = New SqlParameter("@Initial", SqlDbType.Char, 10)
            params(2).Value = oAgunan.Initial
            params(3) = New SqlParameter("@Status", SqlDbType.Bit)
            params(3).Value = oAgunan.Status
            params(4) = New SqlParameter("@UsrCrt", SqlDbType.Char, 20)
            params(4).Value = oAgunan.LoginId

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCollateralType_ADD, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Jenis Collateral", "Jenis Collateral Add", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region
#Region " JenisAgunanEdit"
	Public Sub JenisAgunanEdit(ByVal oAgunan As Parameter.AgunanLain)

		Dim params(4) As SqlParameter
		Dim objcon As New SqlConnection(oAgunan.strConnection)
		Dim objtrans As SqlTransaction = Nothing
		Try
			If objcon.State = ConnectionState.Closed Then objcon.Open()
			objtrans = objcon.BeginTransaction
			params(4) = New SqlParameter("@UsrCrt", SqlDbType.Char, 20)
			params(4).Value = oAgunan.LoginId
			params(3) = New SqlParameter("@Status", SqlDbType.Bit)
			params(3).Value = oAgunan.Status
			params(2) = New SqlParameter("@Initial", SqlDbType.Char, 10)
			params(2).Value = oAgunan.Initial
			params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
			params(1).Value = oAgunan.Description
			params(0) = New SqlParameter("@CollateralTypeID", SqlDbType.Char, 3)
			params(0).Value = oAgunan.JenisCollateral

			SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spCollateralTypeEdit", params)
			objtrans.Commit()
		Catch exp As Exception
			objtrans.Rollback()
			WriteException("Jenis Collateral", "Jenis Collateral Edit", exp.Message + exp.StackTrace)
			Throw New Exception("A record already exists with the same primary key !")
		Finally
			If objcon.State = ConnectionState.Open Then objcon.Close()
			objcon.Dispose()
		End Try
	End Sub
#End Region
#Region " JenisAgunanDelete"
	Public Function JenisAgunanDelete(ByVal oCustomClass As Parameter.AgunanLain) As String
		Dim Err As Integer
		Dim params(0) As SqlParameter
		params(0) = New SqlParameter("@CollateralTypeID", SqlDbType.Int)
		params(0).Value = oCustomClass.JenisCollateral
		Try
			Err = SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spCollateralTypeDelete", params)
			Return ""
		Catch ex As Exception
			Return "ERROR: Unable to delete the selected record. Record already used!"
		End Try
	End Function
#End Region
#Region " MitraEdit"
	Public Sub MitraEdit(ByVal oMitra As Parameter.AgunanLain, ByVal oClassAddress As Parameter.Address)
        Dim params(17) As SqlParameter
        Dim objcon As New SqlConnection(oMitra.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@MfCode", SqlDbType.Char, 10)
            params(0).Value = oMitra.MitraID
            params(1) = New SqlParameter("@FullName", SqlDbType.VarChar, 50)
            params(1).Value = oMitra.MitraFullName
            params(2) = New SqlParameter("@InitialName", SqlDbType.Char, 20)
            params(2).Value = oMitra.MitraInitialName
            params(3) = New SqlParameter("@Address", SqlDbType.Char, 100)
            params(3).Value = oClassAddress.Address
            params(4) = New SqlParameter("@RT", SqlDbType.Char, 3)
            params(4).Value = oClassAddress.RT
            params(5) = New SqlParameter("@RW", SqlDbType.Char, 3)
            params(5).Value = oClassAddress.RW
            params(6) = New SqlParameter("@VillageName", SqlDbType.VarChar, 50)
            params(6).Value = oClassAddress.Kelurahan
            params(7) = New SqlParameter("@District", SqlDbType.VarChar, 50)
            params(7).Value = oClassAddress.Kecamatan
            params(8) = New SqlParameter("@City", SqlDbType.VarChar, 50)
            params(8).Value = oClassAddress.City
            params(9) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
            params(9).Value = oClassAddress.ZipCode
            params(10) = New SqlParameter("@ProvinceName", SqlDbType.Char, 50)
            params(10).Value = oMitra.Provinsi
            params(11) = New SqlParameter("@Phone1", SqlDbType.Char, 20)
            params(11).Value = oMitra.Phone1
            params(12) = New SqlParameter("@Phone2", SqlDbType.Char, 20)
            params(12).Value = oMitra.Phone2
            params(13) = New SqlParameter("@Fax", SqlDbType.Char, 20)
            params(13).Value = oMitra.Fax
            params(14) = New SqlParameter("@Website", SqlDbType.VarChar, 50)
            params(14).Value = oMitra.WebSite
            params(15) = New SqlParameter("@JoinDate", SqlDbType.DateTime)
            params(15).Value = oMitra.JoinDate
            params(16) = New SqlParameter("@Notes", SqlDbType.VarChar, 500)
            params(16).Value = oMitra.Notes
            params(17) = New SqlParameter("@UsrCrt", SqlDbType.Char, 20)
            params(17).Value = oMitra.LoginId


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spMITRA_EDIT, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Mitra Multifinance", "MitraEdit", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region
#End Region
	Public Function GetCboCollateralType(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
		Dim oReturnValue As New Parameter.AgunanLain
		Dim params(0) As SqlParameter

		params(0) = New SqlParameter("@Initial", SqlDbType.VarChar, 50)
		params(0).Value = oCustomClass.Initial
		Try
			oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetCboCollateralType", params).Tables(0)
			Return oReturnValue
		Catch exp As Exception
			Throw New Exception("Error On DataAccess.SIPP.SIPP0000.GetCboBulandataSIPP")
		End Try
	End Function

	Public Function getDataLandBuildidng(ByVal ocustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        Dim oReturnValue As New Parameter.AgunanLain

        params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.CustomerID
        params(1) = New SqlParameter("@CollateralID", SqlDbType.Char, 20)
        params(1).Value = ocustomClass.CollateralID

        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, "spGetDataAgunanLainLandBuilding", params)
            If reader.Read Then
                With oReturnValue
                    .BranchId = reader("BranchID").ToString
					.CollateralTypeID = reader("CollateralTypeID").ToString
					.CollateralDescription = reader("CollateralDescription").ToString
                    .CollateralID = reader("CollateralID").ToString
                    .CollateralStatus = reader("CollateralStatus").ToString
                    .Currency = reader("Currency").ToString
                    .CollateralSeqNo = CInt(reader("CollateralSeqNo").ToString)
                    .PemilikCollateral = reader("PemilikCollateral").ToString
                    .AlamatPemilikCollateral = reader("AlamatPemilikCollateral").ToString
                    .StatusSertifikat = reader("StatusSertifikat").ToString
                    .JenisSertifikat = reader("JenisSertifikat").ToString
                    .ThnPenerbitanSertifikat = CDate(reader("ThnPenerbitanSertifikat").ToString)
                    .NoSertifikat = reader("NoSertifikat").ToString
                    .NoGambarSituasi = reader("NoGambarSituasi").ToString
                    .Developer = reader("Developer").ToString
                    .LuasTanah = reader("LuasTanah").ToString
                    .LuasBangunan = reader("LuasBangunan").ToString
                    .ProgresKonstruksi = reader("ProgresKonstruksi").ToString
                    .TglUpdKonstruksi = CDate(reader("TglUpdKonstruksi").ToString)
                    .Kecamatan = reader("Kecamatan").ToString
                    .Desa = reader("Desa").ToString
                    .Alamat = reader("Alamat").ToString
                    .SandiLokasi = reader("SandiLokasi").ToString
                    .MortgageType = reader("MortgageType").ToString
                    .PropertyType = reader("PropertyType").ToString
                    .NilaiPasarDiakui = CDbl(reader("NilaiPasarDiakui").ToString)
                    .TglPenilaian = CDate(reader("TglPenilaian").ToString)
                    .NilaiPledging = CDbl(reader("NilaiPledging").ToString)
                    .TglupdAgunan = CDate(reader("TglupdAgunan").ToString)
                    .PIBDTglPenilaian = CDate(reader("PIBDTglPenilaian").ToString)
                    .PIBDNilaiTanah = CDbl(reader("PIBDNilaiTanah").ToString)
                    .PIBDNilaiBangunan = CDbl(reader("PIBDNilaiBangunan").ToString)
                    .PIBDNilaiTanahBangunan = CDbl(reader("PIBDNilaiTanahBangunan").ToString)
                    .PIBFTglPenilaian = CDate(reader("PIBFTglPenilaian").ToString)
                    .PIBFNilaiTanah = CDbl(reader("PIBFNilaiTanah").ToString)
                    .PIBFNilaiBangunan = CDbl(reader("PIBFNilaiBangunan").ToString)
                    .PIBFNilaiTanahBangunan = CDbl(reader("PIBFNilaiTanahBangunan").ToString)
                    .PEBDTglPenilaian = CDate(reader("PEBDTglPenilaian").ToString)
                    .PEBDNilaiTanah = CDbl(reader("PEBDNilaiTanah").ToString)
                    .PEBDNilaiBangunan = CDbl(reader("PEBDNilaiBangunan").ToString)
                    .PEBDNilaiTanahBangunan = CDbl(reader("PEBDNilaiTanahBangunan").ToString)
                    .PEBFTglPenilaian = CDate(reader("PEBFTglPenilaian").ToString)
                    .PEBFNilaiTanah = CDbl(reader("PEBFNilaiTanah").ToString)
                    .PEBFNilaiBangunan = CDbl(reader("PEBFNilaiBangunan").ToString)
                    .PEBFNilaiTanahBangunan = CDbl(reader("PEBFNilaiTanahBangunan").ToString)
                    .PJNoSKMHT = reader("PJNoSKMHT").ToString
                    .PJTglSKMHT = CDate(reader("PJTglSKMHT").ToString)
                    .PJTglJatuhTempoSKMHT = CDate(reader("PJTglJatuhTempoSKMHT").ToString)
                    .PJNoAPHT = reader("PJNoAPHT").ToString
                    .PJTglAPHT = CDate(reader("PJTglAPHT").ToString)
                    .PJNoSHT = reader("PJNoSHT").ToString
                    .PJTglIssuedSHT = CDate(reader("PJTglIssuedSHT").ToString)
                    .PJTotalNilaiHT = CDbl(reader("PJTotalNilaiHT").ToString)
                    .PJRangkingHT = reader("PJRangkingHT").ToString
                    .PJJenisPengikatan = reader("PJJenisPengikatan").ToString
                    .PJNotaris = reader("PJNotaris").ToString
                    .PJLokasiPenyimpananDokumen = reader("PJLokasiPenyimpananDokumen").ToString
                    .PJKantorBPNWilayah = reader("PJKantorBPNWilayah").ToString
                    .BgnFisikJaminan = reader("BgnFisikJaminan").ToString
                    .BgnSuratIzin = reader("BgnSuratIzin").ToString
                    .BgnNoSuratIzin = reader("BgnNoSuratIzin").ToString
                    .BgnJenisSuratIzin = reader("BgnJenisSuratIzin").ToString
                    .BgnPeruntukanBangunan = reader("BgnPeruntukanBangunan").ToString
                    .BgnNoIzinLayakHuni = reader("BgnNoIzinLayakHuni").ToString
                    .BgnPBBTahunTerakhir = reader("BgnPBBTahunTerakhir").ToString
                    .BgnNilaiNJOP = CDbl(reader("BgnNilaiNJOP").ToString)
                    .BgnCetakBiru = reader("BgnCetakBiru").ToString
                    .BgnAktaBalikNama = reader("BgnAktaBalikNama").ToString
                    .BgnNoAkta = reader("BgnNoAkta").ToString
                    .BgnTglAkta = CDate(reader("BgnTglAkta").ToString)
                    .BgnNotaris = reader("BgnNotaris").ToString
                    .BgnParipasu = CInt(reader("BgnParipasu").ToString)
                    .BgnBangunanDiasuransikan = reader("BgnBangunanDiasuransikan").ToString
                End With
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

	Public Function getDataInvoice(ByVal ocustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
		Dim reader As SqlDataReader
		Dim params(1) As SqlParameter
		Dim oReturnValue As New Parameter.AgunanLain

		params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
		params(0).Value = ocustomClass.CustomerID
		params(1) = New SqlParameter("@CollateralID", SqlDbType.Char, 20)
		params(1).Value = ocustomClass.CollateralID

		Try
			reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, "spGetDataAgunanLainInvoice", params)
			If reader.Read Then
				With oReturnValue
					.BranchId = reader("BranchID").ToString
					.CollateralTypeID = reader("CollateralTypeID").ToString
					.CollateralDescription = reader("CollateralDescription").ToString
					.CollateralID = reader("CollateralID").ToString
					.CollateralStatus = reader("CollateralStatus").ToString
					.Currency = reader("Currency").ToString
					.CollateralSeqNo = CInt(reader("CollateralSeqNo").ToString)
					.PemilikCollateral = reader("PemilikCollateral").ToString
					.Objek = reader("Objek").ToString
					.NoInvoice = reader("NoInvoice").ToString
					.PenerbitInvoice = reader("PenerbitInvoice").ToString
					.PenerimaInvoice = reader("PenerimaInvoice").ToString
					.NilaiInvoice = CDbl(reader("NilaiInvoice").ToString)
					.TglKontrak = CDate(reader("TglKontrak").ToString)
					.TglInvoice = CDate(reader("TglInvoice").ToString)
				End With
			End If
			reader.Close()
			Return oReturnValue
		Catch ex As Exception
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function getDataBPKB(ByVal ocustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
		Dim reader As SqlDataReader
		Dim params(1) As SqlParameter
		Dim oReturnValue As New Parameter.AgunanLain

		params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
		params(0).Value = ocustomClass.CustomerID
		params(1) = New SqlParameter("@CollateralID", SqlDbType.Char, 20)
		params(1).Value = ocustomClass.CollateralID

		Try
			reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, "spGetDataAgunanLainBPKB", params)
			If reader.Read Then
				With oReturnValue
					.BranchId = reader("BranchID").ToString
					.CollateralTypeID = reader("CollateralTypeID").ToString
					.CollateralDescription = reader("CollateralDescription").ToString
					.CollateralID = reader("CollateralID").ToString
					.CollateralStatus = reader("CollateralStatus").ToString
					.Currency = reader("Currency").ToString
					.CollateralSeqNo = CInt(reader("CollateralSeqNo").ToString)
					.PemilikCollateral = reader("PemilikCollateral").ToString
					.NoRangka = reader("NoRangka").ToString
					.NoMesin = reader("NoMesin").ToString
					.NamaDiBPKB = reader("NamaDiBPKB").ToString
					.Merk = reader("Merk").ToString
					.Tipe = reader("Tipe").ToString
					.Warna = reader("Warna").ToString
					.NamaDealer = reader("NamaDealer").ToString
					.AlamatDealer = reader("AlamatDealer").ToString
					.TelponDealer = reader("TelponDealer").ToString
					.JTBPKB = CDate(reader("JTBPKB").ToString)
					.TglTerimaBPKB = CDate(reader("TglTerimaBPKB").ToString)
					.TglBPKB = CDate(reader("TglBPKB").ToString)
					.TglKeluarBPKBSementara = CDate(reader("TglKeluarBPKBSementara").ToString)
					.TglClose = CDate(reader("TglClose").ToString)
					.NoBPKB = reader("NoBPKB").ToString
					.NoPolisi = reader("NoPolisi").ToString
					.NoFaktur = reader("NoFaktur").ToString
					.NoNIK = reader("NoNIK").ToString
					.NoFormA = reader("NoFormA").ToString
					.AktaFidusia = reader("AktaFidusia").ToString
					.SertifikatFidusia = reader("SertifikatFidusia").ToString
					.Keterangan = reader("Keterangan").ToString
					.NilaiInvoice = CDbl(reader("NilaiBPKB").ToString)
				End With
			End If
			reader.Close()
			Return oReturnValue
		Catch ex As Exception
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function saveAddDataBPKB(ByVal oAgunan As Parameter.AgunanLain) As Parameter.AgunanLain

		Dim params(29) As SqlParameter
		Dim objcon As New SqlConnection(oAgunan.strConnection)
		Dim objtrans As SqlTransaction = Nothing
		Try
			If objcon.State = ConnectionState.Closed Then objcon.Open()
			objtrans = objcon.BeginTransaction

			params(0) = New SqlParameter("BranchID", SqlDbType.Char, 3)
			params(0).Value = oAgunan.BranchId
			params(1) = New SqlParameter("CollateralTypeID", SqlDbType.Char, 3)
			params(1).Value = oAgunan.CollateralTypeID
			params(2) = New SqlParameter("CollateralID", SqlDbType.Char, 20)
			params(2).Value = oAgunan.CollateralID
			params(3) = New SqlParameter("PemilikCollateral", SqlDbType.VarChar, 30)
			params(3).Value = oAgunan.PemilikCollateral
			params(4) = New SqlParameter("NoRangka", SqlDbType.VarChar, 50)
			params(4).Value = oAgunan.NoRangka
			params(5) = New SqlParameter("NoMesin", SqlDbType.VarChar, 50)
			params(5).Value = oAgunan.NoMesin
			params(6) = New SqlParameter("NamaDiBPKB", SqlDbType.VarChar, 50)
			params(6).Value = oAgunan.NamaDiBPKB
			params(7) = New SqlParameter("Merk", SqlDbType.VarChar, 30)
			params(7).Value = oAgunan.Merk
			params(8) = New SqlParameter("Tipe", SqlDbType.VarChar, 50)
			params(8).Value = oAgunan.Tipe
			params(9) = New SqlParameter("Warna", SqlDbType.VarChar, 50)
			params(9).Value = oAgunan.Warna
			params(10) = New SqlParameter("NamaDealer", SqlDbType.VarChar, 100)
			params(10).Value = oAgunan.NamaDealer
			params(11) = New SqlParameter("AlamatDealer", SqlDbType.VarChar, 100)
			params(11).Value = oAgunan.AlamatDealer
			params(12) = New SqlParameter("TelponDealer", SqlDbType.Char, 15)
			params(12).Value = oAgunan.TelponDealer

			params(13) = New SqlParameter("JTBPKB", SqlDbType.Date)
			If oAgunan.TglKeluarBPKBSementara <> "01/01/1900" Then
				params(13).Value = oAgunan.TglKeluarBPKBSementara
			Else
				params(13).Value = DBNull.Value
			End If

			params(14) = New SqlParameter("TglTerimaBPKB", SqlDbType.Date)
			If oAgunan.TglTerimaBPKB <> CDate("01/01/1900") Then
				params(14).Value = oAgunan.TglTerimaBPKB
			Else
				params(14).Value = DBNull.Value
			End If

			params(15) = New SqlParameter("NoBPKB", SqlDbType.VarChar, 30)
			params(15).Value = oAgunan.NoBPKB

			params(16) = New SqlParameter("TglBPKB", SqlDbType.Date)
			If oAgunan.TglBPKB <> CDate("01/01/1900") Then
				params(16).Value = oAgunan.TglBPKB
			Else
				params(16).Value = DBNull.Value
			End If

			params(17) = New SqlParameter("NoPolisi", SqlDbType.VarChar, 50)
			params(17).Value = oAgunan.NoPolisi
			params(18) = New SqlParameter("NoFaktur", SqlDbType.VarChar, 50)
			params(18).Value = oAgunan.NoFaktur
			params(19) = New SqlParameter("NoNIK", SqlDbType.VarChar, 50)
			params(19).Value = oAgunan.NoNIK
			params(20) = New SqlParameter("NoFormA", SqlDbType.VarChar, 50)
			params(20).Value = oAgunan.NoFormA
			params(21) = New SqlParameter("AktaFidusia", SqlDbType.VarChar, 50)
			params(21).Value = oAgunan.AktaFidusia
			params(22) = New SqlParameter("SertifikatFidusia", SqlDbType.VarChar, 50)
			params(22).Value = oAgunan.SertifikatFidusia

			params(23) = New SqlParameter("TglKeluarBPKBSementara", SqlDbType.Date)
			If oAgunan.TglKeluarBPKBSementara <> CDate("01/01/1900") Then
				params(23).Value = oAgunan.TglKeluarBPKBSementara
			Else
				params(23).Value = DBNull.Value
			End If

			params(24) = New SqlParameter("Keterangan", SqlDbType.VarChar, 100)
			params(24).Value = oAgunan.Keterangan

			params(25) = New SqlParameter("TglClose", SqlDbType.Date)
			If oAgunan.TglKeluarBPKBSementara <> CDate("01/01/1900") Then
				params(25).Value = oAgunan.TglKeluarBPKBSementara
			Else
				params(25).Value = DBNull.Value
			End If

			params(26) = New SqlParameter("UsrCrt", SqlDbType.Char, 20)
			params(26).Value = oAgunan.LoginId
			params(27) = New SqlParameter("CollateralStatus", SqlDbType.Char, 1)
			params(27).Value = oAgunan.CollateralStatus
			params(28) = New SqlParameter("Currency", SqlDbType.Char, 3)
			params(28).Value = oAgunan.Currency
			params(29) = New SqlParameter("NilaiBPKB", SqlDbType.Decimal)
			params(29).Value = oAgunan.NilaiInvoice

			SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spsaveAddDataBPKB", params)
			objtrans.Commit()
		Catch exp As Exception
			objtrans.Rollback()
			WriteException("Jenis Collateral", "Jenis Collateral Add", exp.Message + exp.StackTrace)
			Throw New Exception("A record already exists with the same primary key !")
		Finally
			If objcon.State = ConnectionState.Open Then objcon.Close()
			objcon.Dispose()
		End Try
	End Function

	Public Function saveEditDataBPKB(ByVal oAgunan As Parameter.AgunanLain) As Parameter.AgunanLain

		Dim params(27) As SqlParameter
		Dim objcon As New SqlConnection(oAgunan.strConnection)
		Dim objtrans As SqlTransaction = Nothing
		Try
			If objcon.State = ConnectionState.Closed Then objcon.Open()
			objtrans = objcon.BeginTransaction

			params(27) = New SqlParameter("NilaiBPKB", SqlDbType.Decimal)
			params(27).Value = oAgunan.NilaiInvoice

			params(26) = New SqlParameter("UsrUpd", SqlDbType.Char, 20)
			params(26).Value = oAgunan.LoginId

			params(25) = New SqlParameter("TglClose", SqlDbType.Date)
			If oAgunan.TglClose <> CDate("01/01/1900") Then
				params(25).Value = oAgunan.TglClose
			Else
				params(25).Value = DBNull.Value
			End If


			params(24) = New SqlParameter("Keterangan", SqlDbType.VarChar, 100)
			params(24).Value = oAgunan.Keterangan

			params(23) = New SqlParameter("TglKeluarBPKBSementara", SqlDbType.Date)
			If oAgunan.TglKeluarBPKBSementara <> CDate("01/01/1900") Then
				params(23).Value = oAgunan.TglKeluarBPKBSementara
			Else
				params(23).Value = DBNull.Value
			End If

			params(22) = New SqlParameter("SertifikatFidusia", SqlDbType.VarChar, 50)
			params(22).Value = oAgunan.SertifikatFidusia
			params(21) = New SqlParameter("AktaFidusia", SqlDbType.VarChar, 50)
			params(21).Value = oAgunan.AktaFidusia
			params(20) = New SqlParameter("NoFormA", SqlDbType.VarChar, 50)
			params(20).Value = oAgunan.NoFormA
			params(19) = New SqlParameter("NoNIK", SqlDbType.VarChar, 50)
			params(19).Value = oAgunan.NoNIK
			params(18) = New SqlParameter("NoFaktur", SqlDbType.VarChar, 50)
			params(18).Value = oAgunan.NoFaktur
			params(17) = New SqlParameter("NoPolisi", SqlDbType.VarChar, 50)
			params(17).Value = oAgunan.NoPolisi

			params(16) = New SqlParameter("TglBPKB", SqlDbType.Date)
			If oAgunan.TglBPKB <> CDate("01/01/1900") Then
				params(16).Value = oAgunan.TglBPKB
			Else
				params(16).Value = DBNull.Value
			End If

			params(15) = New SqlParameter("NoBPKB", SqlDbType.VarChar, 30)
			params(15).Value = oAgunan.NoBPKB

			params(14) = New SqlParameter("TglTerimaBPKB", SqlDbType.Date)
			If oAgunan.TglTerimaBPKB <> CDate("01/01/1900") Then
				params(14).Value = oAgunan.TglTerimaBPKB
			Else
				params(14).Value = DBNull.Value
			End If

			params(13) = New SqlParameter("JTBPKB", SqlDbType.Date)
			If oAgunan.JTBPKB <> CDate("01/01/1900") Then
				params(13).Value = oAgunan.JTBPKB
			Else
				params(13).Value = DBNull.Value
			End If

			params(12) = New SqlParameter("TelponDealer", SqlDbType.Char, 15)
			params(12).Value = oAgunan.TelponDealer
			params(11) = New SqlParameter("AlamatDealer", SqlDbType.VarChar, 100)
			params(11).Value = oAgunan.AlamatDealer
			params(10) = New SqlParameter("NamaDealer", SqlDbType.VarChar, 100)
			params(10).Value = oAgunan.NamaDealer
			params(9) = New SqlParameter("Warna", SqlDbType.VarChar, 50)
			params(9).Value = oAgunan.Warna
			params(8) = New SqlParameter("Tipe", SqlDbType.VarChar, 50)
			params(8).Value = oAgunan.Tipe
			params(7) = New SqlParameter("Merk", SqlDbType.VarChar, 30)
			params(7).Value = oAgunan.Merk
			params(6) = New SqlParameter("NamaDiBPKB", SqlDbType.VarChar, 50)
			params(6).Value = oAgunan.NamaDiBPKB
			params(5) = New SqlParameter("NoMesin", SqlDbType.VarChar, 50)
			params(5).Value = oAgunan.NoMesin
			params(4) = New SqlParameter("NoRangka", SqlDbType.VarChar, 50)
			params(4).Value = oAgunan.NoRangka
			params(3) = New SqlParameter("PemilikCollateral", SqlDbType.VarChar, 30)
			params(3).Value = oAgunan.PemilikCollateral
			params(2) = New SqlParameter("CollateralID", SqlDbType.Char, 20)
			params(2).Value = oAgunan.CollateralID
			params(1) = New SqlParameter("CollateralTypeID", SqlDbType.Char, 3)
			params(1).Value = oAgunan.CollateralTypeID
			params(0) = New SqlParameter("BranchID", SqlDbType.Char, 3)
			params(0).Value = oAgunan.BranchId

			SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spsaveEditDataBPKB", params)
			objtrans.Commit()
		Catch exp As Exception
			objtrans.Rollback()
			WriteException("Jenis Collateral", "Jenis Collateral Add", exp.Message + exp.StackTrace)
			Throw New Exception("Error Update!")
		Finally
			If objcon.State = ConnectionState.Open Then objcon.Close()
			objcon.Dispose()
		End Try
	End Function

	Public Function SaveEditDataInvoice(ByVal oAgunan As Parameter.AgunanLain) As Parameter.AgunanLain

		Dim params(11) As SqlParameter
		Dim objcon As New SqlConnection(oAgunan.strConnection)
		Dim objtrans As SqlTransaction = Nothing
		Try
			If objcon.State = ConnectionState.Closed Then objcon.Open()
			objtrans = objcon.BeginTransaction
			params(11) = New SqlParameter("UsrUpd", SqlDbType.Char, 20)
			params(11).Value = oAgunan.LoginId
			params(10) = New SqlParameter("TglKontrak", SqlDbType.Date)
			If oAgunan.TglKontrak <> CDate("01/01/1900") Then
				params(10).Value = oAgunan.TglKontrak
			Else
				params(10).Value = DBNull.Value
			End If

			params(9) = New SqlParameter("Objek", SqlDbType.VarChar, 100)
			params(9).Value = oAgunan.Objek
			params(8) = New SqlParameter("NoInvoice", SqlDbType.VarChar, 50)
			params(8).Value = oAgunan.NoInvoice
			params(7) = New SqlParameter("TglInvoice", SqlDbType.Date)
			If oAgunan.TglInvoice <> CDate("01/01/1900") Then
				params(7).Value = oAgunan.TglInvoice
			Else
				params(7).Value = DBNull.Value
			End If

			params(6) = New SqlParameter("PenerbitInvoice", SqlDbType.VarChar, 50)
			params(6).Value = oAgunan.PenerbitInvoice
			params(5) = New SqlParameter("PenerimaInvoice", SqlDbType.VarChar, 50)
			params(5).Value = oAgunan.PenerimaInvoice
			params(4) = New SqlParameter("NilaiInvoice", SqlDbType.Decimal)
			params(4).Value = oAgunan.NilaiInvoice
			params(3) = New SqlParameter("PemilikCollateral", SqlDbType.VarChar, 30)
			params(3).Value = oAgunan.PemilikCollateral
			params(2) = New SqlParameter("CollateralID", SqlDbType.Char, 20)
			params(2).Value = oAgunan.CollateralID
			params(1) = New SqlParameter("CollateralTypeID", SqlDbType.Char, 3)
			params(1).Value = oAgunan.CollateralTypeID
			params(0) = New SqlParameter("BranchID", SqlDbType.Char, 3)
			params(0).Value = oAgunan.BranchId

			SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spSaveEditDataInvoice", params)
			objtrans.Commit()
		Catch exp As Exception
			objtrans.Rollback()
			WriteException("Jenis Collateral", "Jenis Collateral Add", exp.Message + exp.StackTrace)
			Throw New Exception("Error Update!")
		Finally
			If objcon.State = ConnectionState.Open Then objcon.Close()
			objcon.Dispose()
		End Try
	End Function

	Public Function SaveAddDataInvoice(ByVal oAgunan As Parameter.AgunanLain) As Parameter.AgunanLain

		Dim params(13) As SqlParameter
		Dim objcon As New SqlConnection(oAgunan.strConnection)
		Dim objtrans As SqlTransaction = Nothing
		Try
			If objcon.State = ConnectionState.Closed Then objcon.Open()
			objtrans = objcon.BeginTransaction

			params(0) = New SqlParameter("BranchID", SqlDbType.Char, 3)
			params(0).Value = oAgunan.BranchId
			params(1) = New SqlParameter("CollateralTypeID", SqlDbType.Char, 3)
			params(1).Value = oAgunan.CollateralTypeID
			params(2) = New SqlParameter("CollateralID", SqlDbType.Char, 20)
			params(2).Value = oAgunan.CollateralID
			params(3) = New SqlParameter("PemilikCollateral", SqlDbType.VarChar, 30)
			params(3).Value = oAgunan.PemilikCollateral
			params(4) = New SqlParameter("NilaiInvoice", SqlDbType.Decimal)
			params(4).Value = oAgunan.NilaiInvoice
			params(5) = New SqlParameter("PenerimaInvoice", SqlDbType.VarChar, 50)
			params(5).Value = oAgunan.PenerimaInvoice
			params(6) = New SqlParameter("PenerbitInvoice", SqlDbType.VarChar, 50)
			params(6).Value = oAgunan.PenerbitInvoice
			params(7) = New SqlParameter("TglInvoice", SqlDbType.Date)
			If oAgunan.TglInvoice <> CDate("01/01/1900") Then
				params(7).Value = oAgunan.TglInvoice
			Else
				params(7).Value = DBNull.Value
			End If
			params(8) = New SqlParameter("NoInvoice", SqlDbType.VarChar, 50)
			params(8).Value = oAgunan.NoInvoice
			params(9) = New SqlParameter("Objek", SqlDbType.VarChar, 100)
			params(9).Value = oAgunan.Objek
			params(10) = New SqlParameter("TglKontrak", SqlDbType.Date)
			If oAgunan.TglKontrak <> CDate("01/01/1900") Then
				params(10).Value = oAgunan.TglKontrak
			Else
				params(10).Value = DBNull.Value
			End If
			params(11) = New SqlParameter("UsrCrt", SqlDbType.Char, 20)
			params(11).Value = oAgunan.LoginId
			params(12) = New SqlParameter("CollateralStatus", SqlDbType.Char, 1)
			params(12).Value = oAgunan.CollateralStatus
			params(13) = New SqlParameter("Currency", SqlDbType.Char, 3)
			params(13).Value = oAgunan.Currency

			SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spSaveAddDataInvoice", params)
			objtrans.Commit()
		Catch exp As Exception
			objtrans.Rollback()
			WriteException("Jenis Collateral", "Jenis Collateral Add", exp.Message + exp.StackTrace)
			Throw New Exception("Error Update!")
		Finally
			If objcon.State = ConnectionState.Open Then objcon.Close()
			objcon.Dispose()
		End Try
	End Function

	Public Function SaveEditDataLandBuilding(ByVal oAgunan As Parameter.AgunanLain) As Parameter.AgunanLain

        Dim params(71) As SqlParameter
        Dim objcon As New SqlConnection(oAgunan.strConnection)
		Dim objtrans As SqlTransaction = Nothing
		Try
			If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(71) = New SqlParameter("Currency", SqlDbType.Char, 3)
            params(71).Value = oAgunan.Currency
            params(70) = New SqlParameter("CollateralStatus", SqlDbType.Char, 1)
            params(70).Value = oAgunan.CollateralStatus

            params(69) = New SqlParameter("UsrUpd", SqlDbType.Char, 20)
			params(69).Value = oAgunan.LoginId

			params(68) = New SqlParameter("BgnBangunanDiasuransikan", SqlDbType.Char, 1)
			params(68).Value = oAgunan.BgnBangunanDiasuransikan
			params(67) = New SqlParameter("BgnParipasu", SqlDbType.Int)
			params(67).Value = oAgunan.BgnParipasu
			params(66) = New SqlParameter("BgnNotaris", SqlDbType.VarChar, 30)
			params(66).Value = oAgunan.BgnNotaris

			params(65) = New SqlParameter("BgnTglAkta", SqlDbType.Date)
			If oAgunan.BgnTglAkta <> CDate("01/01/1900") Then
				params(65).Value = oAgunan.BgnTglAkta
			Else
				params(65).Value = DBNull.Value
			End If

			params(64) = New SqlParameter("BgnNoAkta", SqlDbType.VarChar, 15)
			params(64).Value = oAgunan.BgnNoAkta
			params(63) = New SqlParameter("BgnAktaBalikNama", SqlDbType.VarChar, 15)
			params(63).Value = oAgunan.BgnAktaBalikNama
			params(62) = New SqlParameter("BgnCetakBiru", SqlDbType.Char, 1)
			params(62).Value = oAgunan.BgnCetakBiru
			params(61) = New SqlParameter("BgnNilaiNJOP", SqlDbType.Decimal)
			params(61).Value = oAgunan.BgnNilaiNJOP
			params(60) = New SqlParameter("BgnPBBTahunTerakhir", SqlDbType.Char, 4)
			params(60).Value = oAgunan.BgnPBBTahunTerakhir
			params(59) = New SqlParameter("BgnNoIzinLayakHuni", SqlDbType.VarChar, 20)
			params(59).Value = oAgunan.BgnNoIzinLayakHuni
			params(58) = New SqlParameter("BgnPeruntukanBangunan", SqlDbType.VarChar, 30)
			params(58).Value = oAgunan.BgnPeruntukanBangunan
			params(57) = New SqlParameter("BgnJenisSuratIzin", SqlDbType.Char, 1)
			params(57).Value = oAgunan.BgnJenisSuratIzin
			params(56) = New SqlParameter("BgnNoSuratIzin", SqlDbType.VarChar, 20)
			params(56).Value = oAgunan.BgnNoSuratIzin
			params(55) = New SqlParameter("BgnSuratIzin", SqlDbType.Char, 1)
			params(55).Value = oAgunan.BgnSuratIzin
			params(54) = New SqlParameter("BgnFisikJaminan", SqlDbType.VarChar, 30)
			params(54).Value = oAgunan.BgnFisikJaminan
			params(53) = New SqlParameter("PJKantorBPNWilayah", SqlDbType.VarChar, 30)
			params(53).Value = oAgunan.PJKantorBPNWilayah
			params(52) = New SqlParameter("PJLokasiPenyimpananDokumen", SqlDbType.VarChar, 30)
			params(52).Value = oAgunan.PJLokasiPenyimpananDokumen
			params(51) = New SqlParameter("PJNotaris", SqlDbType.VarChar, 30)
			params(51).Value = oAgunan.PJNotaris
			params(50) = New SqlParameter("PJJenisPengikatan", SqlDbType.VarChar, 15)
			params(50).Value = oAgunan.PJJenisPengikatan
			params(49) = New SqlParameter("PJRangkingHT", SqlDbType.Char, 3)
			params(49).Value = oAgunan.PJRangkingHT
			params(48) = New SqlParameter("PJTotalNilaiHT", SqlDbType.Decimal)
			params(48).Value = oAgunan.PJTotalNilaiHT

			params(47) = New SqlParameter("PJTglIssuedSHT", SqlDbType.Date)
			If oAgunan.PJTglIssuedSHT <> CDate("01/01/1900") Then
				params(47).Value = oAgunan.PJTglIssuedSHT
			Else
				params(47).Value = DBNull.Value
			End If

			params(46) = New SqlParameter("PJNoSHT", SqlDbType.VarChar, 15)
			params(46).Value = oAgunan.PJNoSHT

			params(45) = New SqlParameter("PJTglAPHT", SqlDbType.Date)
			If oAgunan.PJTglAPHT <> CDate("01/01/1900") Then
				params(45).Value = oAgunan.PJTglAPHT
			Else
				params(45).Value = DBNull.Value
			End If

			params(44) = New SqlParameter("PJNoAPHT", SqlDbType.VarChar, 15)
			params(44).Value = oAgunan.PJNoAPHT

			params(43) = New SqlParameter("PJTglJatuhTempoSKMHT", SqlDbType.Date)
			If oAgunan.PJTglJatuhTempoSKMHT <> CDate("01/01/1900") Then
				params(43).Value = oAgunan.PJTglJatuhTempoSKMHT
			Else
				params(43).Value = DBNull.Value
			End If

			params(42) = New SqlParameter("PJTglSKMHT", SqlDbType.Date)
			If oAgunan.PJTglSKMHT <> CDate("01/01/1900") Then
				params(42).Value = oAgunan.PJTglSKMHT
			Else
				params(42).Value = DBNull.Value
			End If

			params(41) = New SqlParameter("PJNoSKMHT", SqlDbType.VarChar, 15)
			params(41).Value = oAgunan.PJNoSKMHT
			params(40) = New SqlParameter("PEBFNilaiTanahBangunan", SqlDbType.Decimal)
			params(40).Value = oAgunan.PEBFNilaiTanahBangunan
			params(39) = New SqlParameter("PEBFNilaiBangunan", SqlDbType.Decimal)
			params(39).Value = oAgunan.PEBFNilaiBangunan
			params(38) = New SqlParameter("PEBFNilaiTanah", SqlDbType.Decimal)
			params(38).Value = oAgunan.PEBFNilaiTanah

			params(37) = New SqlParameter("PEBFTglPenilaian", SqlDbType.Date)
			If oAgunan.PIBFTglPenilaian <> CDate("01/01/1900") Then
				params(37).Value = oAgunan.PEBFTglPenilaian
			Else
				params(37).Value = DBNull.Value
			End If

			params(36) = New SqlParameter("PEBDNilaiTanahBangunan", SqlDbType.Decimal)
			params(36).Value = oAgunan.PEBDNilaiTanahBangunan
			params(35) = New SqlParameter("PEBDNilaiBangunan", SqlDbType.Decimal)
			params(35).Value = oAgunan.PEBDNilaiBangunan
			params(34) = New SqlParameter("PEBDNilaiTanah", SqlDbType.Decimal)
			params(34).Value = oAgunan.PEBDNilaiTanah

			params(33) = New SqlParameter("PEBDTglPenilaian", SqlDbType.Date)
			If oAgunan.PIBFTglPenilaian <> CDate("01/01/1900") Then
				params(33).Value = oAgunan.PEBDTglPenilaian
			Else
				params(33).Value = DBNull.Value
			End If

			params(32) = New SqlParameter("PIBFNilaiTanahBangunan", SqlDbType.Decimal)
			params(32).Value = oAgunan.PIBFNilaiTanahBangunan
			params(31) = New SqlParameter("PIBFNilaiBangunan", SqlDbType.Decimal)
			params(31).Value = oAgunan.PIBFNilaiBangunan
			params(30) = New SqlParameter("PIBFNilaiTanah", SqlDbType.Decimal)
			params(30).Value = oAgunan.PIBFNilaiTanah

			params(29) = New SqlParameter("PIBFTglPenilaian", SqlDbType.Date)
			If oAgunan.PIBFTglPenilaian <> CDate("01/01/1900") Then
				params(29).Value = oAgunan.PIBFTglPenilaian
			Else
				params(29).Value = DBNull.Value
			End If

			params(28) = New SqlParameter("PIBDNilaiTanahBangunan", SqlDbType.Decimal)
			params(28).Value = oAgunan.PIBDNilaiTanahBangunan
			params(27) = New SqlParameter("PIBDNilaiBangunan", SqlDbType.Decimal)
			params(27).Value = oAgunan.PIBDNilaiBangunan
			params(26) = New SqlParameter("PIBDNilaiTanah", SqlDbType.Decimal)
			params(26).Value = oAgunan.PIBDNilaiTanah

			params(25) = New SqlParameter("PIBDTglPenilaian", SqlDbType.Date)
			If oAgunan.PIBDTglPenilaian <> CDate("01/01/1900") Then
				params(25).Value = oAgunan.PIBDTglPenilaian
			Else
				params(25).Value = DBNull.Value
			End If

			params(24) = New SqlParameter("TglupdAgunan", SqlDbType.Date)
			If oAgunan.TglPenilaian <> CDate("01/01/1900") Then
				params(24).Value = oAgunan.TglupdAgunan
			Else
				params(24).Value = DBNull.Value
			End If

			params(23) = New SqlParameter("NilaiPledging", SqlDbType.Decimal)
			params(23).Value = oAgunan.NilaiPledging

			params(22) = New SqlParameter("TglPenilaian", SqlDbType.Date)
			If oAgunan.TglPenilaian <> CDate("01/01/1900") Then
				params(22).Value = oAgunan.TglPenilaian
			Else
				params(22).Value = DBNull.Value
			End If

			params(21) = New SqlParameter("NilaiPasarDiakui", SqlDbType.Decimal)
			params(21).Value = oAgunan.NilaiPasarDiakui
			params(20) = New SqlParameter("PropertyType", SqlDbType.Char, 3)
			params(20).Value = oAgunan.PropertyType
			params(19) = New SqlParameter("MortgageType", SqlDbType.Char, 3)
			params(19).Value = oAgunan.MortgageType
			params(18) = New SqlParameter("SandiLokasi", SqlDbType.Char, 3)
			params(18).Value = oAgunan.SandiLokasi
			params(17) = New SqlParameter("Alamat", SqlDbType.VarChar, 100)
			params(17).Value = oAgunan.Alamat
			params(16) = New SqlParameter("Desa", SqlDbType.VarChar, 30)
			params(16).Value = oAgunan.Desa
			params(15) = New SqlParameter("Kecamatan", SqlDbType.VarChar, 30)
			params(15).Value = oAgunan.Kecamatan

			params(14) = New SqlParameter("TglUpdKonstruksi", SqlDbType.DateTime)
			If oAgunan.TglUpdKonstruksi <> CDate("01/01/1900") Then
				params(14).Value = oAgunan.TglUpdKonstruksi
			Else
				params(14).Value = DBNull.Value
			End If

			params(13) = New SqlParameter("ProgresKonstruksi", SqlDbType.Int)
			If oAgunan.ProgresKonstruksi <> "" Then
				params(13).Value = oAgunan.ProgresKonstruksi
			Else
				params(13).Value = DBNull.Value
			End If

			params(12) = New SqlParameter("LuasBangunan", SqlDbType.Char, 6)
			params(12).Value = oAgunan.LuasBangunan
			params(11) = New SqlParameter("LuasTanah", SqlDbType.Char, 6)
			params(11).Value = oAgunan.LuasTanah
			params(10) = New SqlParameter("Developer", SqlDbType.VarChar, 30)
			params(10).Value = oAgunan.Developer
			params(9) = New SqlParameter("NoGambarSituasi", SqlDbType.VarChar, 15)
			params(9).Value = oAgunan.NoGambarSituasi
			params(8) = New SqlParameter("NoSertifikat", SqlDbType.VarChar, 50)
			params(8).Value = oAgunan.NoSertifikat
			params(7) = New SqlParameter("ThnPenerbitanSertifikat", SqlDbType.Date)
			params(7).Value = oAgunan.ThnPenerbitanSertifikat
			params(6) = New SqlParameter("JenisSertifikat", SqlDbType.Char, 4)
			params(6).Value = oAgunan.JenisSertifikat
			params(5) = New SqlParameter("StatusSertifikat", SqlDbType.Char, 1)
			params(5).Value = oAgunan.StatusSertifikat
			params(4) = New SqlParameter("AlamatPemilikCollateral", SqlDbType.VarChar, 100)
			params(4).Value = oAgunan.AlamatPemilikCollateral
			params(3) = New SqlParameter("PemilikCollateral", SqlDbType.VarChar, 30)
			params(3).Value = oAgunan.PemilikCollateral
			params(2) = New SqlParameter("CollateralID", SqlDbType.Char, 20)
			params(2).Value = oAgunan.CollateralID
			params(1) = New SqlParameter("CollateralTypeID", SqlDbType.Char, 3)
			params(1).Value = oAgunan.CollateralTypeID
			params(0) = New SqlParameter("BranchID", SqlDbType.Char, 3)
			params(0).Value = oAgunan.BranchId

			SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spSaveEditDataLandBuilding", params)
			objtrans.Commit()
		Catch exp As Exception
			objtrans.Rollback()
			WriteException("Jenis Collateral", "Jenis Collateral Add", exp.Message + exp.StackTrace)
			Throw New Exception("Error Update!")
		Finally
			If objcon.State = ConnectionState.Open Then objcon.Close()
			objcon.Dispose()
		End Try
	End Function

	Public Function SaveAddDataLandBuilding(ByVal oAgunan As Parameter.AgunanLain) As Parameter.AgunanLain

		Dim params(71) As SqlParameter
		Dim objcon As New SqlConnection(oAgunan.strConnection)
		Dim objtrans As SqlTransaction = Nothing
		Try
			If objcon.State = ConnectionState.Closed Then objcon.Open()
			objtrans = objcon.BeginTransaction

			params(0) = New SqlParameter("BranchID", SqlDbType.Char, 3)
			params(0).Value = oAgunan.BranchId
			params(1) = New SqlParameter("CollateralTypeID", SqlDbType.Char, 3)
			params(1).Value = oAgunan.CollateralTypeID
			params(2) = New SqlParameter("CollateralID", SqlDbType.Char, 20)
			params(2).Value = oAgunan.CollateralID
			params(3) = New SqlParameter("PemilikCollateral", SqlDbType.VarChar, 30)
			params(3).Value = oAgunan.PemilikCollateral
			params(4) = New SqlParameter("AlamatPemilikCollateral", SqlDbType.VarChar, 100)
			params(4).Value = oAgunan.AlamatPemilikCollateral
			params(5) = New SqlParameter("StatusSertifikat", SqlDbType.Char, 1)
			params(5).Value = oAgunan.StatusSertifikat
			params(6) = New SqlParameter("JenisSertifikat", SqlDbType.Char, 4)
			params(6).Value = oAgunan.JenisSertifikat

			params(7) = New SqlParameter("ThnPenerbitanSertifikat", SqlDbType.Date)
			If oAgunan.ThnPenerbitanSertifikat <> CDate("01/01/1900") Then
				params(7).Value = oAgunan.ThnPenerbitanSertifikat
			Else
				params(7).Value = DBNull.Value
			End If

			params(8) = New SqlParameter("NoSertifikat", SqlDbType.VarChar, 50)
			params(8).Value = oAgunan.NoSertifikat
			params(9) = New SqlParameter("NoGambarSituasi", SqlDbType.VarChar, 15)
			params(9).Value = oAgunan.NoGambarSituasi
			params(10) = New SqlParameter("Developer", SqlDbType.VarChar, 30)
			params(10).Value = oAgunan.Developer
			params(11) = New SqlParameter("LuasTanah", SqlDbType.Char, 6)
			params(11).Value = oAgunan.LuasTanah
			params(12) = New SqlParameter("LuasBangunan", SqlDbType.Char, 6)
			params(12).Value = oAgunan.LuasBangunan

			params(13) = New SqlParameter("ProgresKonstruksi", SqlDbType.Int)
			If oAgunan.ProgresKonstruksi <> "" Then
				params(13).Value = oAgunan.ProgresKonstruksi
			Else
				params(13).Value = DBNull.Value
			End If

			params(14) = New SqlParameter("TglUpdKonstruksi", SqlDbType.DateTime)
			If oAgunan.TglUpdKonstruksi <> CDate("01/01/1900") Then
				params(14).Value = oAgunan.TglUpdKonstruksi
			Else
				params(14).Value = DBNull.Value
			End If

			params(15) = New SqlParameter("Kecamatan", SqlDbType.VarChar, 30)
			params(15).Value = oAgunan.Kecamatan
			params(16) = New SqlParameter("Desa", SqlDbType.VarChar, 30)
			params(16).Value = oAgunan.Desa
			params(17) = New SqlParameter("Alamat", SqlDbType.VarChar, 100)
			params(17).Value = oAgunan.Alamat
			params(18) = New SqlParameter("SandiLokasi", SqlDbType.Char, 3)
			params(18).Value = oAgunan.SandiLokasi
			params(19) = New SqlParameter("MortgageType", SqlDbType.Char, 3)
			params(19).Value = oAgunan.MortgageType
			params(20) = New SqlParameter("PropertyType", SqlDbType.Char, 3)
			params(20).Value = oAgunan.PropertyType
			params(21) = New SqlParameter("NilaiPasarDiakui", SqlDbType.Decimal)
			params(21).Value = oAgunan.NilaiPasarDiakui

			params(22) = New SqlParameter("TglPenilaian", SqlDbType.Date)
			If oAgunan.TglPenilaian <> CDate("01/01/1900") Then
				params(22).Value = oAgunan.TglPenilaian
			Else
				params(22).Value = DBNull.Value
			End If

			params(23) = New SqlParameter("NilaiPledging", SqlDbType.Decimal)
			params(23).Value = oAgunan.NilaiPledging

			params(24) = New SqlParameter("TglupdAgunan", SqlDbType.Date)
			If oAgunan.TglPenilaian <> CDate("01/01/1900") Then
				params(24).Value = oAgunan.TglupdAgunan
			Else
				params(24).Value = DBNull.Value
			End If

			params(25) = New SqlParameter("PIBDTglPenilaian", SqlDbType.Date)
			If oAgunan.PIBDTglPenilaian <> CDate("01/01/1900") Then
				params(25).Value = oAgunan.PIBDTglPenilaian
			Else
				params(25).Value = DBNull.Value
			End If

			params(26) = New SqlParameter("PIBDNilaiTanah", SqlDbType.Decimal)
			params(26).Value = oAgunan.PIBDNilaiTanah
			params(27) = New SqlParameter("PIBDNilaiBangunan", SqlDbType.Decimal)
			params(27).Value = oAgunan.PIBDNilaiBangunan
			params(28) = New SqlParameter("PIBDNilaiTanahBangunan", SqlDbType.Decimal)
			params(28).Value = oAgunan.PIBDNilaiTanahBangunan

			params(29) = New SqlParameter("PIBFTglPenilaian", SqlDbType.Date)
			If oAgunan.PIBFTglPenilaian <> CDate("01/01/1900") Then
				params(29).Value = oAgunan.PIBFTglPenilaian
			Else
				params(29).Value = DBNull.Value
			End If

			params(30) = New SqlParameter("PIBFNilaiTanah", SqlDbType.Decimal)
			params(30).Value = oAgunan.PIBFNilaiTanah
			params(31) = New SqlParameter("PIBFNilaiBangunan", SqlDbType.Decimal)
			params(31).Value = oAgunan.PIBFNilaiBangunan
			params(32) = New SqlParameter("PIBFNilaiTanahBangunan", SqlDbType.Decimal)
			params(32).Value = oAgunan.PIBFNilaiTanahBangunan

			params(33) = New SqlParameter("PEBDTglPenilaian", SqlDbType.Date)
			If oAgunan.PIBFTglPenilaian <> CDate("01/01/1900") Then
				params(33).Value = oAgunan.PEBDTglPenilaian
			Else
				params(33).Value = DBNull.Value
			End If

			params(34) = New SqlParameter("PEBDNilaiTanah", SqlDbType.Decimal)
			params(34).Value = oAgunan.PEBDNilaiTanah
			params(35) = New SqlParameter("PEBDNilaiBangunan", SqlDbType.Decimal)
			params(35).Value = oAgunan.PEBDNilaiBangunan
			params(36) = New SqlParameter("PEBDNilaiTanahBangunan", SqlDbType.Decimal)
			params(36).Value = oAgunan.PEBDNilaiTanahBangunan

			params(37) = New SqlParameter("PEBFTglPenilaian", SqlDbType.Date)
			If oAgunan.PIBFTglPenilaian <> CDate("01/01/1900") Then
				params(37).Value = oAgunan.PEBFTglPenilaian
			Else
				params(37).Value = DBNull.Value
			End If

			params(38) = New SqlParameter("PEBFNilaiTanah", SqlDbType.Decimal)
			params(38).Value = oAgunan.PEBFNilaiTanah
			params(39) = New SqlParameter("PEBFNilaiBangunan", SqlDbType.Decimal)
			params(39).Value = oAgunan.PEBFNilaiBangunan
			params(40) = New SqlParameter("PEBFNilaiTanahBangunan", SqlDbType.Decimal)
			params(40).Value = oAgunan.PEBFNilaiTanahBangunan
			params(41) = New SqlParameter("PJNoSKMHT", SqlDbType.VarChar, 15)
			params(41).Value = oAgunan.PJNoSKMHT

			params(42) = New SqlParameter("PJTglSKMHT", SqlDbType.Date)
			If oAgunan.PJTglSKMHT <> CDate("01/01/1900") Then
				params(42).Value = oAgunan.PJTglSKMHT
			Else
				params(42).Value = DBNull.Value
			End If

			params(43) = New SqlParameter("PJTglJatuhTempoSKMHT", SqlDbType.Date)
			If oAgunan.PJTglJatuhTempoSKMHT <> CDate("01/01/1900") Then
				params(43).Value = oAgunan.PJTglJatuhTempoSKMHT
			Else
				params(43).Value = DBNull.Value
			End If

			params(44) = New SqlParameter("PJNoAPHT", SqlDbType.VarChar, 15)
			params(44).Value = oAgunan.PJNoAPHT

			params(45) = New SqlParameter("PJTglAPHT", SqlDbType.Date)
			If oAgunan.PJTglAPHT <> CDate("01/01/1900") Then
				params(45).Value = oAgunan.PJTglAPHT
			Else
				params(45).Value = DBNull.Value
			End If

			params(46) = New SqlParameter("PJNoSHT", SqlDbType.VarChar, 15)
			params(46).Value = oAgunan.PJNoSHT

			params(47) = New SqlParameter("PJTglIssuedSHT", SqlDbType.Date)
			If oAgunan.PJTglIssuedSHT <> CDate("01/01/1900") Then
				params(47).Value = oAgunan.PJTglIssuedSHT
			Else
				params(47).Value = DBNull.Value
			End If

			params(48) = New SqlParameter("PJTotalNilaiHT", SqlDbType.Decimal)
			params(48).Value = oAgunan.PJTotalNilaiHT
			params(49) = New SqlParameter("PJRangkingHT", SqlDbType.Char, 3)
			params(49).Value = oAgunan.PJRangkingHT
			params(50) = New SqlParameter("PJJenisPengikatan", SqlDbType.VarChar, 15)
			params(50).Value = oAgunan.PJJenisPengikatan
			params(51) = New SqlParameter("PJNotaris", SqlDbType.VarChar, 30)
			params(51).Value = oAgunan.PJNotaris
			params(52) = New SqlParameter("PJLokasiPenyimpananDokumen", SqlDbType.VarChar, 30)
			params(52).Value = oAgunan.PJLokasiPenyimpananDokumen
			params(53) = New SqlParameter("PJKantorBPNWilayah", SqlDbType.VarChar, 30)
			params(53).Value = oAgunan.PJKantorBPNWilayah
			params(54) = New SqlParameter("BgnFisikJaminan", SqlDbType.VarChar, 30)
			params(54).Value = oAgunan.BgnFisikJaminan
			params(55) = New SqlParameter("BgnSuratIzin", SqlDbType.Char, 1)
			params(55).Value = oAgunan.BgnSuratIzin
			params(56) = New SqlParameter("BgnNoSuratIzin", SqlDbType.VarChar, 20)
			params(56).Value = oAgunan.BgnNoSuratIzin
			params(57) = New SqlParameter("BgnJenisSuratIzin", SqlDbType.Char, 1)
			params(57).Value = oAgunan.BgnJenisSuratIzin
			params(58) = New SqlParameter("BgnPeruntukanBangunan", SqlDbType.VarChar, 30)
			params(58).Value = oAgunan.BgnPeruntukanBangunan
			params(59) = New SqlParameter("BgnNoIzinLayakHuni", SqlDbType.VarChar, 20)
			params(59).Value = oAgunan.BgnNoIzinLayakHuni
			params(60) = New SqlParameter("BgnPBBTahunTerakhir", SqlDbType.Char, 4)
			params(60).Value = oAgunan.BgnPBBTahunTerakhir
			params(61) = New SqlParameter("BgnNilaiNJOP", SqlDbType.Decimal)
			params(61).Value = oAgunan.BgnNilaiNJOP
			params(62) = New SqlParameter("BgnCetakBiru", SqlDbType.Char, 1)
			params(62).Value = oAgunan.BgnCetakBiru
			params(63) = New SqlParameter("BgnAktaBalikNama", SqlDbType.VarChar, 15)
			params(63).Value = oAgunan.BgnAktaBalikNama
			params(64) = New SqlParameter("BgnNoAkta", SqlDbType.VarChar, 15)
			params(64).Value = oAgunan.BgnNoAkta

			params(65) = New SqlParameter("BgnTglAkta", SqlDbType.Date)
			If oAgunan.BgnTglAkta <> CDate("01/01/1900") Then
				params(65).Value = oAgunan.BgnTglAkta
			Else
				params(65).Value = DBNull.Value
			End If

			params(66) = New SqlParameter("BgnNotaris", SqlDbType.VarChar, 30)
			params(66).Value = oAgunan.BgnNotaris
			params(67) = New SqlParameter("BgnParipasu", SqlDbType.Int)
			params(67).Value = oAgunan.BgnParipasu
			params(68) = New SqlParameter("BgnBangunanDiasuransikan", SqlDbType.Char, 1)
			params(68).Value = oAgunan.BgnBangunanDiasuransikan
			params(69) = New SqlParameter("UsrCrt", SqlDbType.Char, 20)
			params(69).Value = oAgunan.LoginId
			params(70) = New SqlParameter("CollateralStatus", SqlDbType.Char, 1)
			params(70).Value = oAgunan.CollateralStatus
			params(71) = New SqlParameter("Currency", SqlDbType.Char, 3)
			params(71).Value = oAgunan.Currency

			SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spSaveAddDataLandBuilding", params)
			objtrans.Commit()
		Catch exp As Exception
			objtrans.Rollback()
			WriteException("Jenis Collateral", "Jenis Collateral Add", exp.Message + exp.StackTrace)
			Throw New Exception("Save Error!")
		Finally
			If objcon.State = ConnectionState.Open Then objcon.Close()
			objcon.Dispose()
		End Try
	End Function

	Public Function AddCollateral(ByVal oAgunan As Parameter.AgunanLain) As Parameter.AgunanLain

		Dim params(4) As SqlParameter
		Dim objcon As New SqlConnection(oAgunan.strConnection)
		Dim objtrans As SqlTransaction = Nothing
		Try
			If objcon.State = ConnectionState.Closed Then objcon.Open()
			objtrans = objcon.BeginTransaction

			params(0) = New SqlParameter("CustomerID", SqlDbType.Char, 20)
			params(0).Value = oAgunan.CustomerID
			params(1) = New SqlParameter("CollateralID", SqlDbType.Char, 20)
			params(1).Value = oAgunan.CollateralID
			params(2) = New SqlParameter("CollateralAmount", SqlDbType.Decimal)
			params(2).Value = oAgunan.CollateralAmount
			params(3) = New SqlParameter("Status", SqlDbType.Char, 1)
			params(3).Value = oAgunan.Keterangan
			params(4) = New SqlParameter("UsrCrt", SqlDbType.VarChar, 20)
			params(4).Value = oAgunan.LoginId

			SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spAddCollateral", params)
			objtrans.Commit()
		Catch exp As Exception
			objtrans.Rollback()
			WriteException("Jenis Collateral", "Jenis Collateral Add", exp.Message + exp.StackTrace)
			Throw New Exception("Error Update!")
		Finally
			If objcon.State = ConnectionState.Open Then objcon.Close()
			objcon.Dispose()
		End Try
	End Function

	Public Function getAgunanNett(ByVal oCustomClass As Parameter.AgunanLain) As Parameter.AgunanLain
		Dim params() As SqlParameter = New SqlParameter(1) {}

		Try
			params(0) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
			params(0).Value = oCustomClass.CustomerID
			params(1) = New SqlParameter("@CollateralID", SqlDbType.Char, 20)
			params(1).Value = oCustomClass.CollateralID

			oCustomClass.Tabels = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spGetSummaryAgunan", params)
			Return oCustomClass
		Catch ex As Exception
			Throw New Exception(ex.Message)
		End Try
	End Function

#Region "GetCollateralId"
	Public Function GetCollateralId(ByVal customClass As Parameter.AgunanLain) As String
		Dim Result As String
		Dim params() As SqlParameter = New SqlParameter(2) {}
		params(0) = New SqlParameter("@branchId", SqlDbType.Char, 3)
		params(0).Value = customClass.BranchId
		params(1) = New SqlParameter("@ID", SqlDbType.VarChar, 10)
		params(1).Value = customClass.ID
		params(2) = New SqlParameter("@sequenceNo", SqlDbType.VarChar, 20)
		params(2).Direction = ParameterDirection.Output
		Try
			SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, "spGetNoCollateral", params)
			Result = CType(params(2).Value, String)
			'If Result <> CDate("01/01/1900") Then
			Return Result
			'End If
			'Return ""
		Catch exp As Exception
			WriteException("CLTR", "GetCollateralId", exp.Message + exp.StackTrace)
		End Try
	End Function
#End Region

#Region "PledgingAdd"
	Public Sub PledgingAdd(ByVal oAgunan As Parameter.AgunanLain)

        Dim params(8) As SqlParameter
        Dim objcon As New SqlConnection(oAgunan.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = oAgunan.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = oAgunan.ApplicationID
            params(2) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
            params(2).Value = oAgunan.AgreementNo
            params(3) = New SqlParameter("@CustomerID", SqlDbType.Char, 20)
            params(3).Value = oAgunan.CustomerID
            params(4) = New SqlParameter("@CollateralID", SqlDbType.Char, 20)
            params(4).Value = oAgunan.CollateralID
            params(5) = New SqlParameter("@PercentagePledged", SqlDbType.Decimal)
            params(5).Value = oAgunan.PledgingPercentage
            params(6) = New SqlParameter("@PledgedAmount", SqlDbType.Decimal)
            params(6).Value = oAgunan.PledgingAmount
            params(7) = New SqlParameter("@FlagLinkage", SqlDbType.Char, 1)
            params(7).Value = oAgunan.CollateralStatus
            params(8) = New SqlParameter("@UsrCrt", SqlDbType.Char, 20)
            params(8).Value = oAgunan.LoginId

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCollateralPledg_ADD, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Jenis Collateral", "Jenis Collateral Add", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

    Public Function GetDetailPledging(ByVal oAgunan As Parameter.AgunanLain) As Parameter.AgunanLain

        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@CollateralID", SqlDbType.Char, 20)
            params(0).Value = oAgunan.CollateralID

            oAgunan.ListData = SqlHelper.ExecuteDataset(oAgunan.strConnection, CommandType.StoredProcedure, "spCollateralPledgingDetail", params).Tables(0)
			Return oAgunan
		Catch exp As Exception
            WriteException("Jenis Collateral", "Jenis Collateral Paging", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
