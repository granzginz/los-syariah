

Public Class InsEndorsment : Inherits ComponentBase
    Implements [Interface].IInsEndorsment

    Public Function InsEndorsmentList(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment Implements [Interface].IInsEndorsment.InsEndorsmentList
        Dim DA As New SQLEngine.Insurance.InsEndorsment
        Return DA.InsEndorsmentList(customClass)
    End Function
    Public Function GetData(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment Implements [Interface].IInsEndorsment.GetData
        Dim DA As New SQLEngine.Insurance.InsEndorsment
        Return DA.GetData(customClass)
    End Function
    Public Function GetDataInsDetail(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment Implements [Interface].IInsEndorsment.GetDataInsDetail
        Dim DA As New SQLEngine.Insurance.InsEndorsment
        Return DA.GetDataInsDetail(customClass)
    End Function
    Public Function GetDataCbTPL(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment Implements [Interface].IInsEndorsment.GetDataCbTPL
        Dim DA As New SQLEngine.Insurance.InsEndorsment
        Return DA.GetDataCbTPL(customClass)
    End Function
    Public Function GetDataCbCoverage(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment Implements [Interface].IInsEndorsment.GetDataCbCoverage
        Dim DA As New SQLEngine.Insurance.InsEndorsment
        Return DA.GetDataCbCoverage(customClass)
    End Function

    Public Function GetEndorsCalculationResult(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment Implements [Interface].IInsEndorsment.GetEndorsCalculationResult
        Dim DA As New SQLEngine.Insurance.InsEndorsment
        Return DA.GetEndorsCalculationResult(customClass)
    End Function


    Public Function GetInsured_Coverage(ByVal oCustomClass As Parameter.InsEndorsment, ByVal oData1 As System.Data.DataTable) As Parameter.InsEndorsment Implements [Interface].IInsEndorsment.GetInsured_Coverage
        Dim DA As New SQLEngine.Insurance.InsEndorsment
        Return DA.GetInsured_Coverage(oCustomClass, oData1)
    End Function





    Public Function PrintForm(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment Implements [Interface].IInsEndorsment.PrintForm
        Dim DA As New SQLEngine.Insurance.InsEndorsment
        Return DA.PrintForm(customClass)
    End Function



    Public Function ViewData(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment Implements [Interface].IInsEndorsment.ViewData
        Dim DA As New SQLEngine.Insurance.InsEndorsment
        Return DA.ViewData(customClass)
    End Function



    Public Function SaveData(ByVal customClass As Parameter.InsEndorsment, ByVal result As System.Data.DataTable) As Parameter.InsEndorsment Implements [Interface].IInsEndorsment.SaveData
        Dim DA As New SQLEngine.Insurance.InsEndorsment
        Return DA.SaveData(customClass, result)
    End Function
End Class
