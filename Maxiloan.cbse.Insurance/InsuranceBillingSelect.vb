


Public Class InsuranceBillingSelect : Inherits ComponentBase
    Implements [Interface].IInsuranceBillingSelect

    Public Function CheckInvoiceNo(ByVal customClass As Parameter.InsCoAllocationDetailList) As String Implements [Interface].IInsuranceBillingSelect.CheckInvoiceNo
        Dim DA As New SQLEngine.Insurance.InsuranceBillingSelect
        Return DA.CheckInvoiceNo(customClass)
    End Function

    Public Sub SaveInsuranceBillingSelectDetail(ByVal customClass As Parameter.InsCoAllocationDetailList) Implements [Interface].IInsuranceBillingSelect.SaveInsuranceBillingSelectDetail
        Dim DA As New SQLEngine.Insurance.InsuranceBillingSelect
        DA.SaveInsuranceBillingSelectDetail(customClass)
    End Sub

    Public Sub SaveInsuranceBillingSave(customClass As Parameter.InsCoAllocationDetailList, customClassList As System.Collections.Generic.List(Of Parameter.InsCoAllocationDetailList)) Implements [Interface].IInsuranceBillingSelect.SaveInsuranceBillingSave
        Dim DA As New SQLEngine.Insurance.InsuranceBillingSelect
        DA.SaveInsuranceBillingSave(customClass, customClassList)
    End Sub
End Class
