
Public Class PolicyNumber : Inherits ComponentBase    
    Implements [Interface].IInsurancePolicyNumber


    Dim DA As New SQLEngine.Insurance.InsPolicyNumber

    Public Function GetInsPolicyNumber(ByVal customClass As Parameter.InsPolicyNumber) As Parameter.InsPolicyNumber Implements [Interface].IInsurancePolicyNumber.GetInsPolicyNumber
        Return DA.GetInsPolicyNumber(customClass)
    End Function

    Public Sub InsurancePolicyNumberSaveEdit(ByVal customClass As Parameter.InsPolicyNumber) Implements [Interface].IInsurancePolicyNumber.InsurancePolicyNumberSaveEdit
        DA.InsurancePolicyNumberSaveEdit(customClass)
    End Sub

    Public Function BankersClauseDSaveAdd(ByVal customClass As Parameter.BankersClause) As String Implements [Interface].IInsurancePolicyNumber.BankersClauseDSaveAdd
        Return DA.BankersClauseDSaveAdd(customClass)
    End Function

    Public Function BankersClauseHSaveAdd(ByVal customClass As Parameter.BankersClause) As String Implements [Interface].IInsurancePolicyNumber.BankersClauseHSaveAdd
        Return DA.BankersClauseHSaveAdd(customClass)
    End Function

    Public Function GetBankersClause(ByVal customClass As Parameter.BankersClause) As Parameter.BankersClause Implements [Interface].IInsurancePolicyNumber.GetBankersClause
        Return DA.GetBankersClause(customClass)
    End Function
    Public Function GetInsCompanyBranchByID(ByVal customClass As Parameter.BankersClause) As Parameter.BankersClause Implements [Interface].IInsurancePolicyNumber.GetInsCompanyBranchByID
        Return DA.GetInsCompanyBranchByID(customClass)
    End Function

    Public Function GetBankersClausePrint(ByVal customClass As Parameter.BankersClause) As System.Data.DataSet Implements [Interface].IInsurancePolicyNumber.GetBankersClausePrint
        Return DA.GetBankersClausePrint(customClass)
    End Function

    Public Function GetBankersClauseViewPrint(ByVal customClass As Parameter.BankersClause) As Parameter.BankersClause Implements [Interface].IInsurancePolicyNumber.GetBankersClauseViewPrint
        Return DA.GetBankersClauseViewPrint(customClass)
    End Function
End Class
