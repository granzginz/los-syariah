
Public Class InsNewCover
    Implements [Interface].IInsNewCover


    Public Function InsNewCoverView(ByVal customclass As Parameter.InsNewCover) As Parameter.InsNewCover Implements [Interface].IInsNewCover.InsNewCoverView
        Dim DAInsCoverView As New SQLEngine.Insurance.InsNewCover
        Return DAInsCoverView.InsNewCoverView(customclass)
    End Function


    Public Function InsNewCoverGetInsured(ByVal customclass As Parameter.InsNewCover) As Parameter.InsNewCover Implements [Interface].IInsNewCover.InsNewCoverGetInsured
        Dim DAInsCoverView As New SQLEngine.Insurance.InsNewCover
        Return DAInsCoverView.InsNewCoverGetInsured(customclass)
    End Function

    Public Function InsNewCoverGetPaid(ByVal customclass As Parameter.InsNewCover) As Parameter.InsNewCover Implements [Interface].IInsNewCover.InsNewCoverGetPaid
        Dim DAInsCoverView As New SQLEngine.Insurance.InsNewCover
        Return DAInsCoverView.InsNewCoverGetPaid(customclass)
    End Function



    Public Function FillInsuranceComBranch(ByVal customClass As Parameter.InsNewCover) As System.Data.DataTable Implements [Interface].IInsNewCover.FillInsuranceComBranch
        Dim DAInsCoverView As New SQLEngine.Insurance.InsNewCover
        Return DAInsCoverView.FillInsuranceComBranch(customClass)
    End Function

    Public Function GetInsuranceEntryStep2List(ByVal customClass As Parameter.InsNewCover) As Parameter.InsNewCover Implements [Interface].IInsNewCover.GetInsuranceEntryStep2List
        Dim DAInsCoverView As New SQLEngine.Insurance.InsNewCover
        Return DAInsCoverView.GetInsuranceEntryStep2List(customClass)
    End Function

    Public Function GetInsuranceEntryStep1List(ByVal customClass As Parameter.InsNewCover) As Parameter.InsNewCover Implements [Interface].IInsNewCover.GetInsuranceEntryStep1List
        Dim DAInsCoverView As New SQLEngine.Insurance.InsNewCover
        Return DAInsCoverView.GetInsuranceEntryStep1List(customClass)
    End Function
    Public Function CheckProspect(ByVal CustomClass As Parameter.InsNewCover) As Parameter.InsNewCover Implements [Interface].IInsNewCover.CheckProspect
        Dim DAInsCoverView As New SQLEngine.Insurance.InsNewCover
        Return DAInsCoverView.CheckProspect(CustomClass)
    End Function


    Public Sub ProcessSaveInsuranceApplicationLastProcess(ByVal customClass As Parameter.InsNewCover) Implements [Interface].IInsNewCover.ProcessSaveInsuranceApplicationLastProcess
        Dim DAInsCoverView As New SQLEngine.Insurance.InsNewCover
        DAInsCoverView.ProcessSaveInsuranceApplicationLastProcess(customClass)
    End Sub



    Public Sub ProcessSaveInsuranceByCustomer(ByVal customClass As Parameter.InsNewCover) Implements [Interface].IInsNewCover.ProcessSaveInsuranceByCustomer
        Dim DAInsCoverView As New SQLEngine.Insurance.InsNewCover
        DAInsCoverView.ProcessSaveInsuranceByCustomer(customClass)
    End Sub

    Public Function GetInsuredByCustomer(ByVal customClass As Parameter.InsNewCover) As Parameter.InsNewCover Implements [Interface].IInsNewCover.GetInsuredByCustomer
        Dim DAInsCoverView As New SQLEngine.Insurance.InsNewCover
        Return DAInsCoverView.GetInsuredByCustomer(customClass)
    End Function

    Public Sub InsNewCoverInsuredByCustDelete(ByVal customClass As Parameter.InsNewCover) Implements [Interface].IInsNewCover.InsNewCoverInsuredByCustDelete
        Dim DAInsCoverView As New SQLEngine.Insurance.InsNewCover
        DAInsCoverView.InsNewCoverInsuredByCustDelete(customClass)
    End Sub
End Class
