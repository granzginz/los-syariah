#Region "Imports"

Imports Maxiloan.cbse
Imports Maxiloan.cbse.Insurance
Imports Maxiloan.Interface

#End Region

Public Class InsuranceCompany
    Implements iInsuranceCompany


    Public Function InsCompanySave(ByVal customClass As Parameter.eInsuranceCompany, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As Parameter.eInsuranceCompany Implements [Interface].iInsuranceCompany.InsCompanySave
        Dim DA As New SQLEngine.Insurance.daInsuranceCompany
        Return DA.InsCompanySave(customClass, oClassAddress, oClassPersonal)
    End Function

    Public Sub InsCompanyDelete(ByVal customclass As Parameter.eInsuranceCompany) Implements [Interface].iInsuranceCompany.InsCompanyDelete
        Try
            Dim DA As New SQLEngine.Insurance.daInsuranceCompany
            DA.InsCompanyDelete(customclass)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Public Function GetInsCompanyByID(ByVal strConnection As String, ByVal InsCoID As String) As Entities.InsCoSelectionList Implements [Interface].IInsCoSelection.GetInsCompanyByID
    '    Dim DA As New DataAccess.Insurance.InsCoSelection
    '    Return DA.GetInsCompanyByID(strConnection, InsCoID)
    'End Function

    'Public Function GetInsCompanyBranchList(ByVal InsCoBranch As Entities.InsCoBranch) As Entities.InsCoSelectionList Implements [Interface].IInsCoSelection.GetInsCompanyBranchList
    '    Dim DA As New DataAccess.Insurance.InsCoSelection
    '    Return DA.GetInsCompanyBranchList(InsCoBranch)
    'End Function

    'Public Function GetAgreementList(ByVal oInsCo As Entities.InsCoSelectionList) As Entities.InsCoSelectionList Implements [Interface].IInsCoSelection.GetAgreementList

    'End Function

    'Public Function GetInsCompanyList(ByVal oInsCo As Entities.InsCoSelectionList) As Entities.InsCoSelectionList Implements [Interface].IInsCoSelection.GetInsCompanyList
    '    Dim DA As New DataAccess.Insurance.InsCoSelection
    '    Return DA.GetInsCompanyList(oInsCo)
    'End Function

    'Public Function GetInsCompanyListDataSet(ByVal oInsCo As Entities.InsCoSelectionList) As Entities.InsCoSelectionList Implements [Interface].IInsCoSelection.GetInsCompanyListDataSet
    '    Dim DA As New DataAccess.Insurance.InsCoSelection
    '    Return DA.GetInsCompanyListDataSet(oInsCo)
    'End Function

    Public Function InsCompanyBranchSave(ByVal customClass As Parameter.eInsuranceCompany, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal, ByVal oClassBankAccount As Parameter.BankAccount) As Parameter.eInsuranceCompany Implements [Interface].iInsuranceCompany.InsCompanyBranchSave
        Dim DA As New SQLEngine.Insurance.daInsuranceCompany
        Return DA.InsCompanyBranchSave(customClass, oClassAddress, oClassPersonal, oClassBankAccount)
    End Function

    Public Sub InsCompanyBranchDelete(ByVal customClass As Parameter.eInsuranceCompany) Implements [Interface].iInsuranceCompany.InsCompanyBranchDelete
        Try
            Dim DA As New SQLEngine.Insurance.daInsuranceCompany
            DA.InsCompanyBranchDelete(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    'Public Sub InsCompanyBranchEdit(ByVal customClass As Entities.InsCoBranch, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal, ByVal oClassBankAccount As Parameter.BankAccount) Implements [Interface].IInsCoSelection.InsCompanyBranchEdit
    '    Try
    '        Dim DA As New DataAccess.Insurance.InsCoSelection
    '        DA.InsCompanyBranchEdit(customClass, oClassAddress, oClassPersonal, oClassBankAccount)
    '    Catch ex As Exception
    '        'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
    '        Throw ex
    '    End Try
    'End Sub

    'Public Function GetInsCompanyBranchByID(ByVal InsCoBranch As Entities.InsCoBranch) As Entities.InsCoSelectionList Implements [Interface].IInsCoSelection.GetInsCompanyBranchByID
    '    Dim DA As New DataAccess.Insurance.InsCoSelection
    '    Return DA.GetInsCompanyBranchByID(InsCoBranch)
    'End Function


    'Public Function GetTPLList(ByVal inscoTPL As Entities.InsCoTPL) As Entities.InsCoSelectionList Implements [Interface].IInsCoSelection.GetTPLList
    '    Dim DA As New DataAccess.Insurance.InsCoSelection
    '    Return DA.GetTPLList(inscoTPL)
    'End Function


    'Public Function GetTPLByID(ByVal inscoTPL As Entities.InsCoTPL) As Entities.InsCoSelectionList Implements [Interface].IInsCoSelection.GetTPLByID
    '    Dim DA As New DataAccess.Insurance.InsCoSelection
    '    Return DA.GetTPLByID(inscoTPL)
    'End Function


    Public Function TPLSave(ByVal inscoTPL As Parameter.eInsuranceCompany) As Parameter.eInsuranceCompany Implements [Interface].iInsuranceCompany.TPLSave

        Dim DA As New SQLEngine.Insurance.daInsuranceCompany
        Return DA.TPLSave(inscoTPL)

    End Function

    'Public Sub TPLEdit(ByVal inscoTPL As Entities.InsCoTPL) Implements [Interface].IInsCoSelection.TPLEdit
    '    Try
    '        Dim DA As New DataAccess.Insurance.InsCoSelection
    '        DA.TPLEdit(inscoTPL)
    '    Catch ex As Exception
    '        'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
    '        Throw ex
    '    End Try
    'End Sub

    Public Sub TPLDelete(ByVal inscoTPL As Parameter.eInsuranceCompany) Implements [Interface].iInsuranceCompany.TPLDelete
        Try
            Dim DA As New SQLEngine.Insurance.daInsuranceCompany
            DA.TPLDelete(inscoTPL)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    'Public Function GetRateList(ByVal inscoRate As Entities.InsCoRate) As Entities.InsCoSelectionList Implements [Interface].IInsCoSelection.GetRateList
    '    Dim DA As New DataAccess.Insurance.InsCoSelection
    '    Return DA.GetRateList(inscoRate)
    'End Function

    'Public Function GetRateByID(ByVal inscoRate As Entities.InsCoRate) As Entities.InsCoSelectionList Implements [Interface].IInsCoSelection.GetRateByID
    '    Dim DA As New DataAccess.Insurance.InsCoSelection
    '    Return DA.GetRateByID(inscoRate)
    'End Function

    Public Function RateSave(ByVal inscoRate As Parameter.eInsuranceCompany) As Parameter.eInsuranceCompany Implements [Interface].iInsuranceCompany.RateSave
        Dim DA As New SQLEngine.Insurance.daInsuranceCompany
        Return DA.RateSave(inscoRate)
    End Function

    Public Sub RateDelete(ByVal inscoRate As Parameter.eInsuranceCompany) Implements [Interface].iInsuranceCompany.RateDelete
        Try
            Dim DA As New SQLEngine.Insurance.daInsuranceCompany
            DA.RateDelete(inscoRate)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    'Public Sub RateEdit(ByVal inscoRate As Entities.InsCoRate) Implements [Interface].IInsCoSelection.RateEdit
    '    Try
    '        Dim DA As New DataAccess.Insurance.InsCoSelection
    '        DA.RateEdit(inscoRate)
    '    Catch ex As Exception
    '        'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
    '        Throw ex
    '    End Try
    'End Sub


    'Public Function GetTPLReport(ByVal inscoTPL As Entities.InsCoTPL) As Entities.InsCoSelectionList Implements [Interface].IInsCoSelection.GetTPLReport
    '    Dim DA As New DataAccess.Insurance.InsCoSelection
    '    Return DA.GetTPLReport(inscoTPL)
    'End Function

    'Public Function GetRateReport(ByVal inscoRate As Entities.InsCoRate) As Entities.InsCoSelectionList Implements [Interface].IInsCoSelection.GetRateReport
    '    Dim DA As New DataAccess.Insurance.InsCoSelection
    '    Return DA.GetRateReport(inscoRate)
    'End Function

    'Public Function GetCoverageTypeCombo(ByVal InsCoBranch As Entities.InsCoBranch) As Entities.InsCoSelectionList Implements [Interface].IInsCoSelection.GetCoverageTypeCombo
    '    Dim DA As New DataAccess.Insurance.InsCoSelection
    '    Return DA.GetCoverageTypeCombo(InsCoBranch)
    'End Function

    'Public Function GetInsAssetTypeCombo(ByVal InsCoBranch As Entities.InsCoBranch) As Entities.InsCoSelectionList Implements [Interface].IInsCoSelection.GetInsAssetTypeCombo
    '    Dim DA As New DataAccess.Insurance.InsCoSelection
    '    Return DA.GetInsAssetTypeCombo(InsCoBranch)
    'End Function


    'Public Function GetAssetUsageCombo(ByVal InsCoBranch As Entities.InsCoBranch) As Entities.InsCoSelectionList Implements [Interface].IInsCoSelection.GetAssetUsageCombo
    '    Dim DA As New DataAccess.Insurance.InsCoSelection
    '    Return DA.GetAssetUsageCombo(InsCoBranch)
    'End Function

    Public Sub CopyInsCoBranchRate(ByVal inscoRate As Parameter.eInsuranceCompany) Implements [Interface].iInsuranceCompany.CopyInsCoBranchRate
        Try
            Dim DA As New SQLEngine.Insurance.daInsuranceCompany
            DA.CopyInsCoBranchRate(inscoRate)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function CoverageSave(ByVal inscoCoverage As Parameter.eInsuranceCompany) As Parameter.eInsuranceCompany Implements [Interface].iInsuranceCompany.CoverageSave
        Dim DA As New SQLEngine.Insurance.daInsuranceCompany
        Return DA.CoverageSave(inscoCoverage)
    End Function

    Public Function InsCompanyBranchSaveEdit(ByVal oClassEdit As Parameter.eInsuranceCompany) As Parameter.eInsuranceCompany Implements [Interface].iInsuranceCompany.InsCompanyBranchSaveEdit
        Dim DA As New SQLEngine.Insurance.daInsuranceCompany
        Return DA.InsCompanyBranchSaveEdit(oClassEdit)
    End Function

End Class
