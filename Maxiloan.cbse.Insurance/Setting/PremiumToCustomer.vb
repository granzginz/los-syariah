
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region
Public Class PremiumToCustomer : Inherits ComponentBase
    Implements IPremiumToCustomer


    Public Function ProcessCopyRateFromAnotherBranch(ByVal customClass As Parameter.PremiumToCustomer) As String Implements IPremiumToCustomer.ProcessCopyRateFromAnotherBranch
        Try
            Dim PremiumToCustomerDA As New SQLEngine.Insurance.PremiumToCustomer
            Return PremiumToCustomerDA.ProcessCopyRateFromAnotherBranch(customClass)
        Catch ex As Exception

        End Try

    End Function

    Public Function GetPremiumToCustomer(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer Implements [Interface].IPremiumToCustomer.GetPremiumToCustomer
        Try
            Dim PremiumToCustomerDA As New SQLEngine.Insurance.PremiumToCustomer
            Return PremiumToCustomerDA.GetPremiumToCustomer(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function GetPremiumToCustomerBranch(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer Implements [Interface].IPremiumToCustomer.GetPremiumToCustomerBranch
        Try
            Dim PremiumToCustomerDA As New SQLEngine.Insurance.PremiumToCustomer
            Return PremiumToCustomerDA.GetPremiumToCustomerBranch(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetPremiumToCustomerReport(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer Implements [Interface].IPremiumToCustomer.GetPremiumToCustomerReport
        Try
            Dim PremiumToCustomerDA As New SQLEngine.Insurance.PremiumToCustomer
            Return PremiumToCustomerDA.GetPremiumToCustomerReport(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetPremiumToCustomerInsType(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer Implements [Interface].IPremiumToCustomer.GetPremiumToCustomerInsType
        Try
            Dim PremiumToCustomerDA As New SQLEngine.Insurance.PremiumToCustomer
            Return PremiumToCustomerDA.GetPremiumToCustomerInsType(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function PremiumToCustomerTPLSaveAdd(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer Implements [Interface].IPremiumToCustomer.PremiumToCustomerTPLSaveAdd
        Try
            Dim PremiumToCustomerDA As New SQLEngine.Insurance.PremiumToCustomer
            Return PremiumToCustomerDA.PremiumToCustomerTPLSaveAdd(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Sub PremiumToCustomerTPLSaveEdit(ByVal customClass As Parameter.PremiumToCustomer) Implements IPremiumToCustomer.PremiumToCustomerTPLSaveEdit
        Try
            Dim PremiumToCustomerDA As New SQLEngine.Insurance.PremiumToCustomer
            PremiumToCustomerDA.PremiumToCustomerTPLSaveEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub PremiumToCustomerTPLDelete(ByVal customClass As Parameter.PremiumToCustomer) Implements IPremiumToCustomer.PremiumToCustomerTPLDelete
        Try
            Dim PremiumToCustomerDA As New SQLEngine.Insurance.PremiumToCustomer
            PremiumToCustomerDA.PremiumToCustomerTPLDelete(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when saving Update PremiumToCustomer", ex)

        End Try
    End Sub
    'Public Function GetPremiumToCustomerRateCustEdit(ByVal Branchid As String, ByVal InsType As String, ByVal YearNum As String) As Parameter.PremiumToCustomer Implements [Interface].IPremiumToCustomer.GetPremiumToCustomerRateCustEdit
    '    Try
    '        Dim PremiumToCustomerDA As New SQLEngine.Insurance.PremiumToCustomer
    '        Return PremiumToCustomerDA.GetPremiumToCustomerRateCustEdit(Branchid, InsType, YearNum)
    '    Catch ex As Exception
    '        'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
    '    End Try
    'End Function
    Public Function PremiumToCustomerRateCustSaveAdd___(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer Implements [Interface].IPremiumToCustomer.PremiumToCustomerRateCustSaveAdd
        Try
            Dim PremiumToCustomerDA As New SQLEngine.Insurance.PremiumToCustomer
            Return PremiumToCustomerDA.PremiumToCustomerRateCustSaveAdd(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Sub PremiumToCustomerRateCustSaveEdit(ByVal customClass As Parameter.PremiumToCustomer) Implements IPremiumToCustomer.PremiumToCustomerRateCustSaveEdit
        Try
            Dim PremiumToCustomerDA As New SQLEngine.Insurance.PremiumToCustomer
            PremiumToCustomerDA.PremiumToCustomerRateCustSaveEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub PremiumToCustomerRateCustDelete(ByVal customClass As Parameter.PremiumToCustomer) Implements IPremiumToCustomer.PremiumToCustomerRateCustDelete
        Try
            Dim PremiumToCustomerDA As New SQLEngine.Insurance.PremiumToCustomer
            PremiumToCustomerDA.PremiumToCustomerRateCustDelete(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw New Exception("Error when saving Update PremiumToCustomer", ex)

        End Try
    End Sub


End Class
