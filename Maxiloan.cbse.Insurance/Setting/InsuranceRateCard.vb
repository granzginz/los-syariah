#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.cbse
Imports Maxiloan.cbse.Insurance

#End Region

Public Class InsuranceRateCard
    Implements iInsuranceRateCard
    Public Function GetInsuranceRateCardHSave(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard Implements [Interface].iInsuranceRateCard.GetInsuranceRateCardHSave
        Dim DAGetHSave As New SQLEngine.Insurance.daInsuranceRateCard
        Return DAGetHSave.GetInsuranceRateCardHSave(oCustomClass)
    End Function
    Public Function GetInsuranceRateCardHDelete(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String Implements [Interface].iInsuranceRateCard.GetInsuranceRateCardHDelete
        Dim DAGetDDelete As New SQLEngine.Insurance.daInsuranceRateCard
        Return DAGetDDelete.GetInsuranceRateCardHDelete(oCustomClass)
    End Function
    Public Function GetInsuranceRateCardDSave(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard Implements [Interface].iInsuranceRateCard.GetInsuranceRateCardDSave
        Dim DAGetDSave As New SQLEngine.Insurance.daInsuranceRateCard
        Return DAGetDSave.GetInsuranceRateCardDSave(oCustomClass)
    End Function
    Public Function GetInsuranceRateCardDDelete(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String Implements [Interface].iInsuranceRateCard.GetInsuranceRateCardDDelete
        Dim DAGetDDelete As New SQLEngine.Insurance.daInsuranceRateCard
        Return DAGetDDelete.GetInsuranceRateCardDDelete(oCustomClass)
    End Function
    Public Function GetInsuranceRateCardDCopy(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String Implements [Interface].iInsuranceRateCard.GetInsuranceRateCardDCopy
        Dim DAGetDCopy As New SQLEngine.Insurance.daInsuranceRateCard
        Return DAGetDCopy.GetInsuranceRateCardDCopy(oCustomClass)
    End Function
    Public Function GetInsuranceMasterRateDSave(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard Implements [Interface].iInsuranceRateCard.GetInsuranceMasterRateDSave
        Dim DAGetMasterDSave As New SQLEngine.Insurance.daInsuranceRateCard
        Return DAGetMasterDSave.GetInsuranceMasterRateDSave(oCustomClass)
    End Function
    Public Function GetInsuranceMasterRateDDelete(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String Implements [Interface].iInsuranceRateCard.GetInsuranceMasterRateDDelete
        Dim DAGetMasterDDelete As New SQLEngine.Insurance.daInsuranceRateCard
        Return DAGetMasterDDelete.GetInsuranceMasterRateDDelete(oCustomClass)
    End Function
    Public Function GetNewInsuranceRateCardDSave(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard Implements [Interface].iInsuranceRateCard.GetNewInsuranceRateCardDSave
        Dim DAGetNewDSave As New SQLEngine.Insurance.daInsuranceRateCard
        Return DAGetNewDSave.GetNewInsuranceRateCardDSave(oCustomClass)
    End Function
    Public Function GetNewInsuranceRateCardDDelete(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String Implements [Interface].iInsuranceRateCard.GetNewInsuranceRateCardDDelete
        Dim DAGetNewDDelete As New SQLEngine.Insurance.daInsuranceRateCard
        Return DAGetNewDDelete.GetNewInsuranceRateCardDDelete(oCustomClass)
    End Function
    Public Function GetTPLToCustSave(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard Implements [Interface].iInsuranceRateCard.GetTPLToCustSave
        Dim DAGetTPLSave As New SQLEngine.Insurance.daInsuranceRateCard
        Return DAGetTPLSave.GetTPLToCustSave(oCustomClass)
    End Function
    Public Function GetTPLToCustDelete(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String Implements [Interface].iInsuranceRateCard.GetTPLToCustDelete
        Dim DAGetTPLDelete As New SQLEngine.Insurance.daInsuranceRateCard
        Return DAGetTPLDelete.GetTPLToCustDelete(oCustomClass)
    End Function
    Public Function GetOtherCoverage(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard Implements [Interface].iInsuranceRateCard.GetOtherCoverage
        Dim SaveOtherCoverage As New SQLEngine.Insurance.daInsuranceRateCard
        Return SaveOtherCoverage.GetOtherCoverage(oCustomClass)
    End Function
End Class
