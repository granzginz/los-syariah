#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.cbse
Imports Maxiloan.cbse.Insurance
Imports Maxiloan.Parameter

#End Region

Public Class InsCoRateAdditional
    Implements [Interface].IInsCoRateAdditional

    Public Function AddInsCoRateadditional(customclass As Parameter.InsCoRateAdditional) As String Implements [Interface].IInsCoRateAdditional.AddInsCoCPMaster
        Dim DAIInsCoCP As New SQLEngine.Insurance.InsCoRateAdditional
        Return DAIInsCoCP.AddInsCoRateadditional(customclass)
    End Function

    Public Function DeletInsCoRateadditional(customclass As Parameter.InsCoRateAdditional) As String Implements [Interface].IInsCoRateAdditional.DeletInsCoCPMaster
        Dim DAIInsCoCP As New SQLEngine.Insurance.InsCoRateAdditional
        Return DAIInsCoCP.DeleteInsCoRateadditional(customclass)
    End Function

    Public Function EditInsCoRateadditional(customclass As Parameter.InsCoRateAdditional) As String Implements [Interface].IInsCoRateAdditional.EditInsCoCPMaster
        Dim DAIInsCoCP As New SQLEngine.Insurance.InsCoRateAdditional
        Return DAIInsCoCP.EditInsCoRateadditional(customclass)
    End Function

    Public Function ListInsCoRateadditional(customclass As Parameter.InsCoRateAdditional) As Parameter.InsCoRateAdditional Implements [Interface].IInsCoRateAdditional.ListInsCoCPMaster
        Dim DAIInsCoCP As New SQLEngine.Insurance.InsCoRateAdditional
        Return DAIInsCoCP.ListInsCoRateadditional(customclass)
    End Function

End Class
