#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.cbse
Imports Maxiloan.cbse.Insurance
Imports Maxiloan.Parameter

#End Region

Public Class InsCoProduct
    Implements IInsCoProduct

    Public Function AddInsCoProductMaster(customclass As Parameter.InsCoProduct) As String Implements IInsCoProduct.AddInsCoProductMaster
        Dim DAInsCoProduct As New SQLEngine.Insurance.InsCoProduct
        Return DAInsCoProduct.AddInsCoProduct(customclass)
    End Function

    Public Function DeletInsCoProductMaster(customclass As Parameter.InsCoProduct) As String Implements IInsCoProduct.DeletInsCoProductMaster
        Dim DAInsCoProduct As New SQLEngine.Insurance.InsCoProduct
        Return DAInsCoProduct.DeleteInsCoProduct(customclass)
    End Function

    Public Function ListInsCoProductMaster(customclass As Parameter.InsCoProduct) As Parameter.InsCoProduct Implements IInsCoProduct.ListInsCoProductMaster
        Dim DAInsCoProduct As New SQLEngine.Insurance.InsCoProduct
        Return DAInsCoProduct.ListInsCoProduct(customclass)
    End Function

End Class
