

Public Class InsuranceStandardPremium : Inherits ComponentBase
    Implements [Interface].IInsuranceStandardPremium

    Public Function processCopyStandardPremiumRate(ByVal customClass As Parameter.InsuranceStandardPremium) As String Implements [Interface].IInsuranceStandardPremium.processCopyStandardPremiumRate
        Try
            Dim PremiumToCustomerDA As New SQLEngine.Insurance.InsuranceStandardPremium
            Return PremiumToCustomerDA.processCopyStandardPremiumRate(customClass)
        Catch ex As Exception
        End Try

    End Function

    Public Function getInsuranceStandardPremium(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium Implements [Interface].IInsuranceStandardPremium.getInsuranceStandardPremium
        Try
            Dim InsuranceStandardPremiumDA As New SQLEngine.Insurance.InsuranceStandardPremium
            Return InsuranceStandardPremiumDA.getInsuranceStandardPremium(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function InsuranceStandardPremiumSaveAdd(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium Implements [Interface].IInsuranceStandardPremium.InsuranceStandardPremiumSaveAdd
        Try
            Dim InsuranceStandardPremiumDA As New SQLEngine.Insurance.InsuranceStandardPremium
            Return InsuranceStandardPremiumDA.InsuranceStandardPremiumSaveAdd(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function
    Public Function InsuranceStandardPremiumSaveEdit(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium Implements [Interface].IInsuranceStandardPremium.InsuranceStandardPremiumSaveEdit
        Try
            Dim InsuranceStandardPremiumDA As New SQLEngine.Insurance.InsuranceStandardPremium
            InsuranceStandardPremiumDA.InsuranceStandardPremiumSaveEdit(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try

    End Function
    Public Function InsuranceStandardPremiumDelete(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium Implements [Interface].IInsuranceStandardPremium.InsuranceStandardPremiumDelete
        Try
            Dim InsuranceStandardPremiumDA As New SQLEngine.Insurance.InsuranceStandardPremium
            InsuranceStandardPremiumDA.InsuranceStandardPremiumDelete(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try

    End Function
    Public Function getInsuranceStandardPremiumReport(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium Implements [Interface].IInsuranceStandardPremium.getInsuranceStandardPremiumReport
        Try
            Dim InsuranceStandardPremiumDA As New SQLEngine.Insurance.InsuranceStandardPremium
            Return InsuranceStandardPremiumDA.GetInsuranceStandardPremiumReport(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

End Class
