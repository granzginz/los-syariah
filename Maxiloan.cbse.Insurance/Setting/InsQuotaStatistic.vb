
Public Class InsQuotaStatistic : Inherits ComponentBase
    Implements [Interface].IInsQuotaStatistic

    Public Function ProcessInsuranceQuotaYear(ByVal customClass As Parameter.InsQuotaStatistic) As String Implements [Interface].IInsQuotaStatistic.ProcessInsuranceQuotaYear
        Try
            Dim ProcessInsuranceQuotaYearDA As New SQLEngine.Insurance.InsQuotaStatistic
            ProcessInsuranceQuotaYearDA.ProcessInsuranceQuotaYear(customClass)
        Catch ex As Exception
        End Try
    End Function

    Public Function getInsuranceQuotaListing(ByVal customClass As Parameter.InsQuotaStatistic) As Parameter.InsQuotaStatistic Implements [Interface].IInsQuotaStatistic.getInsuranceQuotaListing
        Try
            Dim getInsuranceQuotaListingDA As New SQLEngine.Insurance.InsQuotaStatistic
            Return getInsuranceQuotaListingDA.getInsuranceQuotaListing(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function UpdateInsuranceQuotaYear(ByVal customClass As Parameter.InsQuotaStatistic) As String Implements [Interface].IInsQuotaStatistic.UpdateInsuranceQuotaYear
        Try
            Dim UpdateInsuranceQuotaYearDA As New SQLEngine.Insurance.InsQuotaStatistic
            UpdateInsuranceQuotaYearDA.UpdateInsuranceQuotaYear(customClass)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
        End Try
    End Function

    Public Function GetInsuranceQuotaReport(ByVal CustomClass As Parameter.InsQuotaStatistic) As Parameter.InsQuotaStatistic Implements [Interface].IInsQuotaStatistic.GetInsuranceQuotaReport
        Try
            Dim DA As New SQLEngine.Insurance.InsQuotaStatistic
            Return DA.GetInsuranceQuotaReport(CustomClass)
        Catch ex As Exception
            '
        End Try
    End Function

End Class
