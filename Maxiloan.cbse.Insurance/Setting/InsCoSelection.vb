Imports Maxiloan.cbse
Imports Maxiloan.cbse.Insurance
Imports Maxiloan.Interface



Public Class InsCoSelection : Inherits ComponentBase
    Implements IInsCoSelection


    Public Function InsCompanyAdd(ByVal customClass As Parameter.InsCo, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As String Implements [Interface].IInsCoSelection.InsCompanyAdd
        Try
            Dim DA As New SQLEngine.Insurance.InsCoSelection
            Return DA.InsCompanyAdd(customClass, oClassAddress, oClassPersonal)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

    Public Sub InsCompanyDelete(ByVal customclass As Parameter.InsCo) Implements [Interface].IInsCoSelection.InsCompanyDelete
        Try
            Dim DA As New SQLEngine.Insurance.InsCoSelection
            DA.InsCompanyDelete(customclass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub InsCompanyEdit(ByVal customClass As Parameter.InsCo, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) Implements [Interface].IInsCoSelection.InsCompanyEdit
        Try
            Dim DA As New SQLEngine.Insurance.InsCoSelection
            DA.InsCompanyEdit(customClass, oClassAddress, oClassPersonal)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function GetInsCompanyByID(ByVal strConnection As String, ByVal InsCoID As String) As Parameter.InsCoSelectionList Implements [Interface].IInsCoSelection.GetInsCompanyByID
        Dim DA As New SQLEngine.Insurance.InsCoSelection
        Return DA.GetInsCompanyByID(strConnection, InsCoID)
    End Function

    Public Function GetInsCompanyBranchList(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList Implements [Interface].IInsCoSelection.GetInsCompanyBranchList
        Dim DA As New SQLEngine.Insurance.InsCoSelection
        Return DA.GetInsCompanyBranchList(InsCoBranch)
    End Function

    Public Function GetAgreementList(ByVal oInsCo As Parameter.InsCoSelectionList) As Parameter.InsCoSelectionList Implements [Interface].IInsCoSelection.GetAgreementList

    End Function

    Public Function GetInsCompanyList(ByVal oInsCo As Parameter.InsCoSelectionList) As Parameter.InsCoSelectionList Implements [Interface].IInsCoSelection.GetInsCompanyList
        Dim DA As New SQLEngine.Insurance.InsCoSelection
        Return DA.GetInsCompanyList(oInsCo)
    End Function

    Public Function GetInsCompanyListDataSet(ByVal oInsCo As Parameter.InsCoSelectionList) As Parameter.InsCoSelectionList Implements [Interface].IInsCoSelection.GetInsCompanyListDataSet
        Dim DA As New SQLEngine.Insurance.InsCoSelection
        Return DA.GetInsCompanyListDataSet(oInsCo)
    End Function

    Public Sub InsCompanyBranchAdd(ByVal customClass As Parameter.InsCoBranch, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal, ByVal oClassBankAccount As Parameter.BankAccount) Implements [Interface].IInsCoSelection.InsCompanyBranchAdd
        Try
            Dim DA As New SQLEngine.Insurance.InsCoSelection
            DA.InsCompanyBranchAdd(customClass, oClassAddress, oClassPersonal, oClassBankAccount)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub InsCompanyBranchDelete(ByVal customClass As Parameter.InsCoBranch) Implements [Interface].IInsCoSelection.InsCompanyBranchDelete
        Try
            Dim DA As New SQLEngine.Insurance.InsCoSelection
            DA.InsCompanyBranchDelete(customClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub InsCompanyBranchEdit(ByVal customClass As Parameter.InsCoBranch, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal, ByVal oClassBankAccount As Parameter.BankAccount) Implements [Interface].IInsCoSelection.InsCompanyBranchEdit
        Try
            Dim DA As New SQLEngine.Insurance.InsCoSelection
            DA.InsCompanyBranchEdit(customClass, oClassAddress, oClassPersonal, oClassBankAccount)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function GetInsCompanyBranchByID(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList Implements [Interface].IInsCoSelection.GetInsCompanyBranchByID
        Dim DA As New SQLEngine.Insurance.InsCoSelection
        Return DA.GetInsCompanyBranchByID(InsCoBranch)
    End Function


    Public Function GetTPLList(ByVal inscoTPL As Parameter.InsCoTPL) As Parameter.InsCoSelectionList Implements [Interface].IInsCoSelection.GetTPLList
        Dim DA As New SQLEngine.Insurance.InsCoSelection
        Return DA.GetTPLList(inscoTPL)
    End Function


    Public Function GetTPLByID(ByVal inscoTPL As Parameter.InsCoTPL) As Parameter.InsCoSelectionList Implements [Interface].IInsCoSelection.GetTPLByID
        Dim DA As New SQLEngine.Insurance.InsCoSelection
        Return DA.GetTPLByID(inscoTPL)
    End Function


    Public Sub TPLAdd(ByVal inscoTPL As Parameter.InsCoTPL) Implements [Interface].IInsCoSelection.TPLAdd
        Try
            Dim DA As New SQLEngine.Insurance.InsCoSelection
            DA.TPLAdd(inscoTPL)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub TPLEdit(ByVal inscoTPL As Parameter.InsCoTPL) Implements [Interface].IInsCoSelection.TPLEdit
        Try
            Dim DA As New SQLEngine.Insurance.InsCoSelection
            DA.TPLEdit(inscoTPL)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub TPLDelete(ByVal inscoTPL As Parameter.InsCoTPL) Implements [Interface].IInsCoSelection.TPLDelete
        Try
            Dim DA As New SQLEngine.Insurance.InsCoSelection
            DA.TPLDelete(inscoTPL)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub


    Public Function GetRateList(ByVal inscoRate As Parameter.InsCoRate) As Parameter.InsCoSelectionList Implements [Interface].IInsCoSelection.GetRateList
        Dim DA As New SQLEngine.Insurance.InsCoSelection
        Return DA.GetRateList(inscoRate)
    End Function

    Public Function GetRateByID(ByVal inscoRate As Parameter.InsCoRate) As Parameter.InsCoSelectionList Implements [Interface].IInsCoSelection.GetRateByID
        Dim DA As New SQLEngine.Insurance.InsCoSelection
        Return DA.GetRateByID(inscoRate)
    End Function

    Public Sub RateAdd(ByVal inscoRate As Parameter.InsCoRate) Implements [Interface].IInsCoSelection.RateAdd
        Try
            Dim DA As New SQLEngine.Insurance.InsCoSelection
            DA.RateAdd(inscoRate)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub RateDelete(ByVal inscoRate As Parameter.InsCoRate) Implements [Interface].IInsCoSelection.RateDelete
        Try
            Dim DA As New SQLEngine.Insurance.InsCoSelection
            DA.RateDelete(inscoRate)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub RateEdit(ByVal inscoRate As Parameter.InsCoRate) Implements [Interface].IInsCoSelection.RateEdit
        Try
            Dim DA As New SQLEngine.Insurance.InsCoSelection
            DA.RateEdit(inscoRate)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub


    Public Function GetTPLReport(ByVal inscoTPL As Parameter.InsCoTPL) As Parameter.InsCoSelectionList Implements [Interface].IInsCoSelection.GetTPLReport
        Dim DA As New SQLEngine.Insurance.InsCoSelection
        Return DA.GetTPLReport(inscoTPL)
    End Function

    Public Function GetRateReport(ByVal inscoRate As Parameter.InsCoRate) As Parameter.InsCoSelectionList Implements [Interface].IInsCoSelection.GetRateReport
        Dim DA As New SQLEngine.Insurance.InsCoSelection
        Return DA.GetRateReport(inscoRate)
    End Function

    Public Function GetCoverageTypeCombo(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList Implements [Interface].IInsCoSelection.GetCoverageTypeCombo
        Dim DA As New SQLEngine.Insurance.InsCoSelection
        Return DA.GetCoverageTypeCombo(InsCoBranch)
    End Function

    Public Function GetInsAssetTypeCombo(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList Implements [Interface].IInsCoSelection.GetInsAssetTypeCombo
        Dim DA As New SQLEngine.Insurance.InsCoSelection
        Return DA.GetInsAssetTypeCombo(InsCoBranch)
    End Function


    Public Function GetAssetUsageCombo(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList Implements [Interface].IInsCoSelection.GetAssetUsageCombo
        Dim DA As New SQLEngine.Insurance.InsCoSelection
        Return DA.GetAssetUsageCombo(InsCoBranch)
    End Function

    Public Sub CopyInsCoBranchRate(ByVal inscoRate As Parameter.InsCoRate) Implements [Interface].IInsCoSelection.CopyInsCoBranchRate
        Try
            Dim DA As New SQLEngine.Insurance.InsCoSelection
            DA.CopyInsCoBranchRate(inscoRate)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function GetInsuranceComByProduct(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList Implements [Interface].IInsCoSelection.GetInsuranceComByProduct
        Try
            Dim DA As New SQLEngine.Insurance.InsCoSelection
            Return DA.GetInsuranceComByProduct(InsCoBranch)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

End Class
