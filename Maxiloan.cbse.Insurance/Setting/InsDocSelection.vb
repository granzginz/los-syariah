
Imports Maxiloan.cbse
Imports Maxiloan.cbse.Insurance
Imports Maxiloan.Interface

Public Class InsDocSelection : Inherits ComponentBase
    Implements IInsDocSelection

#Region "Private Const"
    Private _DA As SQLEngine.Insurance.InsDocSelection
#End Region

    Public Function GetInsDocList(ByVal oInsDoc As Parameter.InsDocSelectionList) As Parameter.InsDocSelectionList Implements [Interface].IInsDocSelection.GetInsDocList
        Try
            Return _DA.GetInsDocList(oInsDoc)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub AddInsDoc(ByVal oInsDoc As Parameter.InsDocSelectionList) Implements [Interface].IInsDocSelection.AddInsDoc
        Try
            _DA.AddInsDoc(oInsDoc)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub DeleteInsDoc(ByVal oInsDoc As Parameter.InsDocSelectionList) Implements [Interface].IInsDocSelection.DeleteInsDoc
        Try
            _DA.DeleteInsDoc(oInsDoc)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub UpdateInsDoc(ByVal oInsDoc As Parameter.InsDocSelectionList) Implements [Interface].IInsDocSelection.UpdateInsDoc
        Try
            _DA.UpdateInsDoc(oInsDoc)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub New()
        _DA = New SQLEngine.Insurance.InsDocSelection
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Public Function GetInsDocById(ByVal oInsDoc As Parameter.InsDocSelectionList) As Parameter.InsDocSelectionList Implements [Interface].IInsDocSelection.GetInsDocById
        Try
            Return _DA.GetInsDocById(oInsDoc)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetInsDocRpt(ByVal oInsDoc As Parameter.InsDocSelectionList) As Parameter.InsDocSelectionList Implements [Interface].IInsDocSelection.GetInsDocRpt
        Try
            Return _DA.GetInsDocRpt(oInsDoc)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
