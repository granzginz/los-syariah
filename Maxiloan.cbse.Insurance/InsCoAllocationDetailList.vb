Imports Maxiloan.Interface

Public Class InsCoAllocationDetailList : Inherits ComponentBase    
    Implements IInsCoAllocationDetailList


    Public Function GetListByAgreementNo(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList Implements [Interface].IInsCoAllocationDetailList.GetListByAgreementNo

        Dim DA As New SQLEngine.Insurance.InsCoAllocationDetailList
        Return DA.GetListByAgreementNo(customClass)


    End Function
    Public Function GetListByApplicationId(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList Implements [Interface].IInsCoAllocationDetailList.GetListByApplicationId

        Dim DA As New SQLEngine.Insurance.InsCoAllocationDetailList
        Return DA.GetListByApplicationId(customClass)


    End Function


    Public Function GetGridInsuranceStatistic(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList Implements [Interface].IInsCoAllocationDetailList.GetGridInsuranceStatistic

        Dim DA As New SQLEngine.Insurance.InsCoAllocationDetailList
        Return DA.GetGridInsuranceStatistic(customClass)

    End Function

    Public Function GetGridTenor(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList Implements [Interface].IInsCoAllocationDetailList.GetGridTenor

        Dim DA As New SQLEngine.Insurance.InsCoAllocationDetailList
        Return DA.GetGridTenor(customClass)

    End Function

    Public Function GetGridInsCoPremium(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList Implements [Interface].IInsCoAllocationDetailList.GetGridInsCoPremium

        Dim DA As New SQLEngine.Insurance.InsCoAllocationDetailList
        Return DA.GetGridInsCoPremium(customClass)

    End Function
    Function GetGridInsCoPremiumView(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList Implements [Interface].IInsCoAllocationDetailList.GetGridInsCoPremiumView
        Dim DA As New SQLEngine.Insurance.InsCoAllocationDetailList
        Return DA.GetGridInsCoPremiumView(customClass)
    End Function

    Public Function SaveInsuranceCompanySelectionLastProcess(ByVal customClass As Parameter.InsCoAllocationDetailList) As String Implements [Interface].IInsCoAllocationDetailList.SaveInsuranceCompanySelectionLastProcess
        Dim DA As New SQLEngine.Insurance.InsCoAllocationDetailList
        Return DA.SaveInsuranceCompanySelectionLastProcess(customClass)
    End Function

    Public Function PenutupanAsuransiManualUpdate(ByVal customClass As Parameter.InsCoAllocationDetailList) As String Implements [Interface].IInsCoAllocationDetailList.PenutupanAsuransiManualUpdate
        Dim DA As New SQLEngine.Insurance.InsCoAllocationDetailList
        Return DA.PenutupanAsuransiManualUpdate(customClass)
    End Function



    Public Sub InitMaskAssComponent(customClass As Parameter.MaskAssCalcEng) Implements [Interface].IInsCoAllocationDetailList.InitMaskAssComponent
        Dim DA As New SQLEngine.Insurance.InsCoAllocationDetailList
        DA.InitMaskAssComponent(customClass)
    End Sub

    Public Function CalcKoreksiMaskapaiAss(customClass As Parameter.MaskAssCalcEng) As DataSet Implements [Interface].IInsCoAllocationDetailList.CalcKoreksiMaskapaiAss
        Dim DA As New SQLEngine.Insurance.InsCoAllocationDetailList
        Return DA.CalcKoreksiMaskapaiAss(customClass)
    End Function

    Public Function KoreksiMaskapaiAss(customClass As Parameter.MaskAssCalcEng) As String Implements [Interface].IInsCoAllocationDetailList.KoreksiMaskapaiAss
        Dim DA As New SQLEngine.Insurance.InsCoAllocationDetailList
        Return DA.KoreksiMaskapaiAss(customClass)
    End Function
End Class
