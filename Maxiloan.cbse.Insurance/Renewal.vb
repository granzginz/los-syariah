

#Region "Imports"
Imports Maxiloan.Exceptions
#End Region

Public Class Renewal : Inherits ComponentBase
    Implements [Interface].IRenewal

    Public Function GetRenewalDetailTop(ByVal customClass As Parameter.Renewal) As Parameter.Renewal Implements [Interface].IRenewal.GetRenewalDetailTop
        Dim DA As New SQLEngine.Insurance.Renewal
        Return DA.GetRenewalDetailTop(customClass)
    End Function

    Public Function GetRenewalNewCoverage(ByVal customClass As Parameter.Renewal) As Parameter.Renewal Implements [Interface].IRenewal.GetRenewalNewCoverage
        Dim DA As New SQLEngine.Insurance.Renewal
        Return DA.GetRenewalNewCoverage(customClass)
    End Function

    Public Function CalculateAndSaveRenewal(ByVal customClass As Parameter.Renewal) As Parameter.Renewal Implements [Interface].IRenewal.CalculateAndSaveRenewal
        Dim DA As New SQLEngine.Insurance.Renewal
        Return DA.CalculateAndSaveRenewal(customClass)
    End Function

    Public Function GetCoverageRenewalGridInsCALCULATE(ByVal customClass As Parameter.Renewal) As Parameter.Renewal Implements [Interface].IRenewal.GetCoverageRenewalGridInsCALCULATE
        Dim DA As New SQLEngine.Insurance.Renewal
        Return DA.GetCoverageRenewalGridInsCALCULATE(customClass)
    End Function
End Class
