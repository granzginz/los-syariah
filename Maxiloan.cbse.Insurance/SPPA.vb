

Public Class SPPA : Inherits ComponentBase
    Implements [Interface].ISPPA

    Public Function GetListCreateSPPA(ByVal customClass As Parameter.SPPA) As Parameter.SPPA Implements [Interface].ISPPA.GetListCreateSPPA
        Dim DA As New SQLEngine.Insurance.SPPA
        Return DA.GetListCreateSPPA(customClass)
    End Function

    Public Function GenerateSPPA(ByVal customClass As Parameter.SPPA) As Parameter.SPPA Implements [Interface].ISPPA.GenerateSPPA
        Dim DA As New SQLEngine.Insurance.SPPA
        Return DA.GenerateSPPA(customClass)
    End Function

    Public Function PrintMailTransaction(ByVal customClass As Parameter.MailTransaction) As Parameter.SPPA Implements [Interface].ISPPA.PrintMailTransaction
        Dim DA As New SQLEngine.Insurance.SPPA
        Return DA.PrintMailTransaction(customClass)
    End Function

    Public Function GetSPPAList(ByVal customClass As Parameter.SPPA) As Parameter.SPPA Implements [Interface].ISPPA.GetSPPAList
        Dim DA As New SQLEngine.Insurance.SPPA
        Return DA.GetSPPAList(customClass)
    End Function

    Public Function GetSPPAReport(ByVal customClass As Parameter.SPPA) As DataSet Implements [Interface].ISPPA.GetSPPAReport
        Dim DA As New SQLEngine.Insurance.SPPA
        Return DA.GetSPPAReport(customClass)
    End Function
    Public Function GetSPPAReportCP(ByVal customClass As Parameter.SPPA) As DataSet Implements [Interface].ISPPA.GetSPPAReportCP
        Dim DA As New SQLEngine.Insurance.SPPA
        Return DA.GetSPPAReportCP(customClass)
    End Function
    Public Function GetSPPAReportJK(ByVal customClass As Parameter.SPPA) As DataSet Implements [Interface].ISPPA.GetSPPAReportJK
        Dim DA As New SQLEngine.Insurance.SPPA
        Return DA.GetSPPAReportJK(customClass)
    End Function
End Class
