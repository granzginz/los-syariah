
Public Class InsTerminate
    Implements [Interface].IInsTerminate


    Public Function InsTerminateList(ByVal customclass As Parameter.InsTerminate) As Parameter.InsTerminate Implements [Interface].IInsTerminate.InsTerminateList
        Dim DAInsTerminateReq As New SQLEngine.Insurance.InsTerminate
        Return DAInsTerminateReq.InsTerminateList(customclass)
    End Function

    Public Function InsrequestPrint(ByVal customclass As Parameter.InsTerminate) As DataSet Implements [Interface].IInsTerminate.InsrequestPrint
        Dim DAInsTerminateReq As New SQLEngine.Insurance.InsTerminate
        Return DAInsTerminateReq.InsrequestPrint(customclass)
    End Function

    Public Function InsrequestPrintRefund(ByVal customclass As Parameter.InsTerminate) As DataSet Implements [Interface].IInsTerminate.InsrequestPrintRefund
        Dim DAInsTerminateReq As New SQLEngine.Insurance.InsTerminate
        Return DAInsTerminateReq.InsrequestPrintRefund(customclass)
    End Function

    Public Function InsTerminateDetail(ByVal customclass As Parameter.InsTerminate) As Parameter.InsTerminate Implements [Interface].IInsTerminate.InsTerminateDetail
        Dim DAInsTerminateReq As New SQLEngine.Insurance.InsTerminate
        Return DAInsTerminateReq.InsTerminateDetail(customclass)
    End Function

    Public Function InsCalculate(ByVal customclass As Parameter.InsTerminate) As Parameter.InsTerminate Implements [Interface].IInsTerminate.InsCalculate
        Dim DAInsTerminateReq As New SQLEngine.Insurance.InsTerminate
        Return DAInsTerminateReq.InsCalculate(customclass)
    End Function

    Public Function InsSaveCalculate(ByVal customclass As Parameter.InsTerminate) As Boolean Implements [Interface].IInsTerminate.InsSaveCalculate
        Dim DAInsTerminateReq As New SQLEngine.Insurance.InsTerminate
        Return DAInsTerminateReq.InsSaveCalculate(customclass)
    End Function

    Public Function InsRefundCalculate(ByVal customclass As Parameter.InsTerminate) As DataSet Implements [Interface].IInsTerminate.InsRefundCalculate
        Dim DAInsTerminateReq As New SQLEngine.Insurance.InsTerminate
        Return DAInsTerminateReq.InsRefundCalculate(customclass)
    End Function

    Public Sub InsInsertMail(ByVal customclass As Parameter.InsTerminate) Implements [Interface].IInsTerminate.InsInsertMail
        Dim DAInsTerminateReq As New SQLEngine.Insurance.InsTerminate
        DAInsTerminateReq.InsInsertMail(customclass)
    End Sub
    Public Sub InsInsertMailRefund(ByVal customclass As Parameter.InsTerminate) Implements [Interface].IInsTerminate.InsInsertMailRefund
        Dim DAInsTerminateReq As New SQLEngine.Insurance.InsTerminate
        DAInsTerminateReq.InsInsertMailRefund(customclass)
    End Sub

    Public Function InsApproveRejectCalculate(ByVal customclass As Parameter.InsTerminate) As String Implements [Interface].IInsTerminate.InsApproveRejectCalculate
        Dim DAInsTerminateReq As New SQLEngine.Insurance.InsTerminate
        Return DAInsTerminateReq.InsApproveRejectCalculate(customclass)
    End Function
End Class
