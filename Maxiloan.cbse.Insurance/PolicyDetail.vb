

Public Class PolicyDetail : Inherits ComponentBase
    Implements [Interface].IPolicyDetail


    Public Function GetPopUpEndorsmentHistory(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList Implements [Interface].IPolicyDetail.GetPopUpEndorsmentHistory
        Dim DA As New SQLEngine.Insurance.PolicyDetail
        Return DA.GetPopUpEndorsmentHistory(customClass)
    End Function


    Public Function GetPolicyDetail(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList Implements [Interface].IPolicyDetail.GetPolicyDetail
        Dim DA As New SQLEngine.Insurance.PolicyDetail
        Return DA.GetPolicyDetail(customClass)
    End Function

End Class
