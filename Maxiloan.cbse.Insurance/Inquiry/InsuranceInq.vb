
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class InsuranceInq : Inherits ComponentBase
    Implements IInsuranceInq
    Public Function InqInsInvoice(ByVal ocustomclass As Parameter.InsInqEntities) As Parameter.InsInqEntities Implements [Interface].IInsuranceInq.InqInsInvoice
        Dim daInqInsInvoice As New SQLEngine.Insurance.InsuranceInq
        Return daInqInsInvoice.InqInsInvoice(ocustomclass)
    End Function
    Public Function InqInsExpired(ByVal ocustomclass As Parameter.InsInqEntities) As Parameter.InsInqEntities Implements [Interface].IInsuranceInq.InqInsExpired
        Dim daInqInsExpired As New SQLEngine.Insurance.InsuranceInq
        Return daInqInsExpired.InqInsExpired(ocustomclass)
    End Function
    Public Function GetReportInsStatitistic(ByVal ocustomclass As Parameter.InsInqEntities) As Parameter.InsInqEntities Implements [Interface].IInsuranceInq.GetReportInsStatitistic
        Dim daInqInsExpired As New SQLEngine.Insurance.InsuranceInq
        Return daInqInsExpired.GetReportInsStatitistic(ocustomclass)
    End Function
End Class
