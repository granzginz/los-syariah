
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
#End Region

Public Class InsuranceDue : Inherits ComponentBase
    Implements IInsuranceDue


    Public Function InsuranceDuepaging1(ByVal oCustomclass As Parameter.InsuranceDue) As Parameter.InsuranceDue Implements [Interface].IInsuranceDue.InsuranceDuepaging
        Dim DAInsuranceDue As New SQLEngine.Insurance.InsuranceDue
        Return DAInsuranceDue.InsuranceDuepaging(oCustomclass)
    End Function

    Public Function InsuranceDueReport(ByVal oCustomclass As Parameter.InsuranceDue) As Parameter.InsuranceDue Implements [Interface].IInsuranceDue.InsuranceDueReport
        Dim DAInsuranceDue As New SQLEngine.Insurance.InsuranceDue
        Return DAInsuranceDue.InsuranceDueReport(oCustomclass)
    End Function
End Class
