
Public Class InsEndorsInq
    Implements [Interface].IInsEndorsInq

    Public Function InsInqEndorsDetail(ByVal customclass As Parameter.InsEndorsInq) As Parameter.InsEndorsInq Implements [Interface].IInsEndorsInq.InsInqEndorsDetail
        Dim DAInsEndorsInq As New SQLEngine.Insurance.InsEndorsInq
        Return DAInsEndorsInq.InsInqEndorsDetail(customclass)
    End Function

    Public Function InsInqEndorsList(ByVal customclass As Parameter.InsEndorsInq) As Parameter.InsEndorsInq Implements [Interface].IInsEndorsInq.InsInqEndorsList
        Dim DAInsEndorsInq As New SQLEngine.Insurance.InsEndorsInq
        Return DAInsEndorsInq.InsInqEndorsList(customclass)
    End Function
End Class
