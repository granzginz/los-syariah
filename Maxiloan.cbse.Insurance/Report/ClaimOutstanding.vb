
Public Class ClaimOutstanding
    Implements [Interface].IInsClaimOutstanding

    Public Function ViewClaimReport(ByVal customClass As Parameter.ClaimOutstanding) As Parameter.ClaimOutstanding Implements [Interface].IInsClaimOutstanding.ViewClaimReport
        Dim DAInsClaim As New SQLEngine.Insurance.ClaimOutstanding
        Return DAInsClaim.ViewClaimReport(customClass)
    End Function

    Public Function getInsuranceCoverageList(ByVal customClass As Parameter.ClaimOutstanding) As Parameter.ClaimOutstanding Implements [Interface].IInsClaimOutstanding.getInsuranceCoverageList
        Dim DAInsClaim As New SQLEngine.Insurance.ClaimOutstanding
        Return DAInsClaim.getInsuranceCoverageList(customClass)
    End Function

End Class
