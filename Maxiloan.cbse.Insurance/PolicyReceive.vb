

Public Class PolicyReceive : Inherits ComponentBase
    Implements [Interface].IPolicyReceive


    Public Function getInsurancePolicyReceive(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium Implements [Interface].IPolicyReceive.getInsurancePolicyReceive

        Dim DA As New SQLEngine.Insurance.PolicyReceive
        Return DA.getInsurancePolicyReceive(customClass)

    End Function

    Public Function getPolicyUpload(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium Implements [Interface].IPolicyReceive.getPolicyUpload
        Dim DA As New SQLEngine.Insurance.PolicyReceive
        Return DA.getPolicyUpload(customClass)
    End Function

    Public Sub savePolicyUpload(ByVal strConnection As String, ByVal strAgreementNo As String, ByVal strPolicyNumber As String, ByVal PolicyReceiveDate As Date, ByVal strReceiveBy As String) Implements [Interface].IPolicyReceive.savePolicyUpload
        Dim DA As New SQLEngine.Insurance.PolicyReceive
        DA.savePolicyUpload(strConnection, strAgreementNo, strPolicyNumber, PolicyReceiveDate, strReceiveBy)
    End Sub

    Public Sub savePolicyUploadXLS(ByVal customClass As Parameter.InsuranceStandardPremium) Implements [Interface].IPolicyReceive.savePolicyUploadXLS
        Dim DA As New SQLEngine.Insurance.PolicyReceive
        DA.savePolicyUploadXLS(customClass)
    End Sub

    Public Sub NotaAsuransiSave(oCustomer As Parameter.InsuranceStandardPremium) Implements [Interface].IPolicyReceive.NotaAsuransiSave
        Dim DA As New SQLEngine.Insurance.PolicyReceive
        DA.NotaAsuransiSave(oCustomer)
    End Sub
End Class
