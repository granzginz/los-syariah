Public Class AdvClaimReq
    Implements [Interface].IAdvClaimReq

    Public Function AdvClaimReqPaging(ByVal customClass As Parameter.AdvClaimReq) As Parameter.AdvClaimReq Implements [Interface].IAdvClaimReq.AdvClaimReqPaging
        Dim DAAdvClaimReq As New SQLEngine.Insurance.AdvClaimReq
        Return DAAdvClaimReq.AdvClaimReqPaging(customClass)
    End Function

    Public Function AdvanceClaimCostRequestView(ByVal customclass As Parameter.AdvClaimReq) As Parameter.AdvClaimReq Implements [Interface].IAdvClaimReq.AdvanceClaimCostRequestView
        Dim DAAdvClaimReq As New SQLEngine.Insurance.AdvClaimReq
        Return DAAdvClaimReq.AdvanceClaimCostRequestView(customclass)

    End Function

    Public Function AdvanceClaimCostRequestViewGrid(ByVal customClass As Parameter.AdvClaimReq) As Parameter.AdvClaimReq Implements [Interface].IAdvClaimReq.AdvanceClaimCostRequestViewGrid
        Dim DAAdvClaimReq As New SQLEngine.Insurance.AdvClaimReq
        Return DAAdvClaimReq.AdvanceClaimCostRequestViewGrid(customClass)
    End Function

    Public Sub AdvanceClaimCostRequestSave(ByVal customClass As Parameter.AdvClaimReq) Implements [Interface].IAdvClaimReq.AdvanceClaimCostRequestSave
        Dim DAAdvClaimReq As New SQLEngine.Insurance.AdvClaimReq
        DAAdvClaimReq.AdvanceClaimCostRequestSave(customClass)

    End Sub

    Public Function AdvanceClaimCostRequestViewApproval(ByVal customclass As Parameter.AdvClaimReq) As Parameter.AdvClaimReq Implements [Interface].IAdvClaimReq.AdvanceClaimCostRequestViewApproval
        Dim DAAdvClaimReq As New SQLEngine.Insurance.AdvClaimReq
        Return DAAdvClaimReq.AdvanceClaimCostRequestViewApproval(customclass)
    End Function
End Class
