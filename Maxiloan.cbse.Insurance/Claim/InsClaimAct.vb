Public Class InsClaimAct
    Implements [Interface].IInsClaimAct

#Region "Activity"

    Public Function ClaimActList(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct Implements [Interface].IInsClaimAct.ClaimActList
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.ClaimActList(customclass)
    End Function

    Public Function ClaimActDetail(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct Implements [Interface].IInsClaimAct.ClaimActDetail
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.ClaimActDetail(customclass)
    End Function

    Public Function ClaimActGetDoc(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct Implements [Interface].IInsClaimAct.ClaimActGetDoc
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.ClaimActGetDoc(customclass)
    End Function

    Public Function ClaimActsave(ByVal customclass As Parameter.InsClaimAct) As Boolean Implements [Interface].IInsClaimAct.ClaimActsave
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.ClaimActsave(customclass)
    End Function

    Public Function ClaimRejectsave(ByVal customclass As Parameter.InsClaimAct) As Boolean Implements [Interface].IInsClaimAct.ClaimRejectsave
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.ClaimRejectsave(customclass)
    End Function

    Public Function ClaimAcceptsave(ByVal customclass As Parameter.InsClaimAct) As Boolean Implements [Interface].IInsClaimAct.ClaimAcceptsave
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.ClaimAcceptsave(customclass)
    End Function
#End Region
#Region "setting"

    Public Function GetInsRateCategory(ByVal customclass As Parameter.InsClaimAct) As System.Data.DataSet Implements [Interface].IInsClaimAct.GetInsRateCategory
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.GetInsRateCategory(customclass)

    End Function
    Public Function SaveClaimDoc(ByVal customclass As Parameter.InsClaimAct) As Boolean Implements [Interface].IInsClaimAct.SaveClaimDoc
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.SaveClaimDoc(customclass)
    End Function
    Public Function DocclaimList(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct Implements [Interface].IInsClaimAct.DocclaimList
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.DocclaimList(customclass)
    End Function

    Public Function GetDocMaster(ByVal customclass As Parameter.InsClaimAct) As System.Data.DataSet Implements [Interface].IInsClaimAct.GetDocMaster
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.GetDocMaster(customclass)
    End Function
    Public Function DeleteClaimDoc(ByVal customclass As Parameter.InsClaimAct) As Boolean Implements [Interface].IInsClaimAct.DeleteClaimDoc
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.DeleteClaimDoc(customclass)
    End Function

    Public Function ClaimDocReport(ByVal customclass As Parameter.InsClaimAct) As System.Data.DataSet Implements [Interface].IInsClaimAct.ClaimDocReport
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.ClaimDocReport(customclass)
    End Function


#End Region
#Region "InsuranceBilling"

    Public Function InsuranceBillingReport(ByVal customclass As Parameter.InsClaimAct) As System.Data.DataSet Implements [Interface].IInsClaimAct.InsuranceBillingReport
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.InsuranceBillingReport(customclass)
    End Function
#End Region
#Region "ClaimInquery"
    Public Function GetInsCo(ByVal customclass As Parameter.InsClaimAct) As System.Data.DataSet Implements [Interface].IInsClaimAct.GetInsCo
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.GetInsCo(customclass)
    End Function
    Public Function ClaimInquiryList(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct Implements [Interface].IInsClaimAct.ClaimInquiryList
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.ClaimInquiryList(customclass)
    End Function
    Public Function GetClaimInqDet(ByVal customclass As Parameter.InsClaimAct) As System.Data.DataSet Implements [Interface].IInsClaimAct.GetClaimInqDet
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.GetClaimInqDet(customclass)
    End Function
    Public Function GetClaimActList(ByVal customclass As Parameter.InsClaimAct) As System.Data.DataSet Implements [Interface].IInsClaimAct.GetClaimActList
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.GetClaimActList(customclass)
    End Function
    Public Function ViewPolicy(ByVal customclass As Parameter.InsClaimAct) As System.Data.DataSet Implements [Interface].IInsClaimAct.ViewPolicy
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.ViewPolicy(customclass)
    End Function
    Public Function GetClaimActHistList(ByVal customclass As Parameter.InsClaimAct) As System.Data.DataSet Implements [Interface].IInsClaimAct.GetClaimActHistList
        Dim DA As New SQLEngine.Insurance.InsClaimAct
        Return DA.GetClaimActHistList(customclass)
    End Function
#End Region






End Class
