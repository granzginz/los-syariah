Public Class ClaimReq
    Implements [Interface].IClaimReq


    Public Function ClaimrequestList(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest Implements [Interface].IClaimReq.ClaimrequestList
        Dim DAClaimReq As New SQLEngine.Insurance.ClaimReq
        Return DAClaimReq.ClaimrequestList(customclass)
    End Function

    Public Function ClaimrequesDetail(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest Implements [Interface].IClaimReq.ClaimrequesDetail
        Dim DAClaimReq As New SQLEngine.Insurance.ClaimReq
        Return DAClaimReq.ClaimrequesDetail(customclass)
    End Function

    Public Function ClaimrequestCoverage(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest Implements [Interface].IClaimReq.ClaimrequestCoverage
        Dim DAClaimReq As New SQLEngine.Insurance.ClaimReq
        Return DAClaimReq.ClaimrequestCoverage(customclass)
    End Function

    Public Function ClaimrequestSave(ByVal customclass As Parameter.ClaimRequest) As Boolean Implements [Interface].IClaimReq.ClaimrequestSave
        Dim DAClaimReq As New SQLEngine.Insurance.ClaimReq
        Return DAClaimReq.ClaimrequestSave(customclass)
    End Function

    Public Function ClaimrequestGetClaimType(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest Implements [Interface].IClaimReq.ClaimrequestGetClaimType
        Dim DAClaimReq As New SQLEngine.Insurance.ClaimReq
        Return DAClaimReq.ClaimrequestGetClaimType(customclass)
    End Function

    Public Function ClaimrequestPrint(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest Implements [Interface].IClaimReq.ClaimrequestPrint
        Dim DAClaimReq As New SQLEngine.Insurance.ClaimReq
        Return DAClaimReq.ClaimrequestPrint(customclass)
    End Function
    Public Function ViewInsuraceClaimInq(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest Implements [Interface].IClaimReq.ViewInsuraceClaimInq
        Dim DAClaimReq As New SQLEngine.Insurance.ClaimReq
        Return DAClaimReq.ViewInsuraceClaimInq(customclass)
    End Function
    Public Function ClaimrequestEdit(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest Implements [Interface].IClaimReq.ClaimrequestEdit
        Dim DAClaimReq As New SQLEngine.Insurance.ClaimReq
        Return DAClaimReq.ClaimrequestEdit(customclass)
    End Function
    Public Function ClaimrequestListApproval(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct Implements [Interface].IClaimReq.ClaimrequestListApproval
        Dim DAClaimReq As New SQLEngine.Insurance.ClaimReq
        Return DAClaimReq.ClaimrequestListApproval(customclass)
    End Function

    Public Function ClaimrequestSaveApprove(ByVal customclass As Parameter.InsClaimAct) As Boolean Implements [Interface].IClaimReq.ClaimrequestSaveApprove
        Dim DAClaimReq As New SQLEngine.Insurance.ClaimReq
        Return DAClaimReq.ClaimrequestSaveApprove(customclass)
    End Function
End Class
