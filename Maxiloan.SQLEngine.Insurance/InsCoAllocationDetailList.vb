
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class InsCoAllocationDetailList : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region "Private Constanta"

    Private Const SP_GETAGREEMENT As String = "spInsCompanySelecetionGetListByAgreementNo"
    Private Const SP_GET_GRID_TENOR As String = "spInsCompanySelectionGetGridTenor"
    Private Const SP_GET_GRID_INSURANCE_STATISTIC As String = "spGridInsuranceStatistic"
    Private Const SP_GET_GRID_INSURANCE_COMPANY_REMIUM As String = "spInsGridInsCoPremium"

    Private Const SP_UPDATE_ASSETDETAIL As String = "spInsCoSelectionUpdateAssetDetail"

    Private Const SP_GETBYAPPID As String = "spInsCompanySelectionGetListByApplicationId"

#End Region
#Region "GetListByAgreementNo"


    'spInsCompanySelecetionGetListByAgreementNo
    Public Function GetListByAgreementNo(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
        params(0).Value = customClass.AgreementNo.Trim
        'params(1) = New SqlParameter("@DataExist", SqlDbType.Bit)
        'params(1).Direction = ParameterDirection.Output
        'params(2) = New SqlParameter("@OutPutCoverPeriodSelection", SqlDbType.Bit)
        'params(2).Direction = ParameterDirection.Output

        Try
            Dim oReturnValue As New Parameter.InsCoAllocationDetailList
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, SP_GETAGREEMENT, params)
            If reader.Read Then
                oReturnValue.BranchId = reader("BranchId").ToString.Trim
                oReturnValue.ApplicationID = reader("ApplicationID").ToString.Trim
                oReturnValue.CustomerID = reader("CustomerID").ToString.Trim
                oReturnValue.CustomerName = reader("CustomerName").ToString.Trim
                oReturnValue.TotalStdPremium = CType(reader("StdPremium").ToString.Trim, Double)
                oReturnValue.AssetTypeDescr = reader("AssetTypeDescr").ToString.Trim
                oReturnValue.GoLiveDate = CType(reader("GoLiveDate").ToString.Trim, Date)
                oReturnValue.StartDateRenewal = CType(reader("StartDateRenewal").ToString.Trim, Date)
                oReturnValue.InsuredByDescr = reader("InsuredByDescr").ToString.Trim
                oReturnValue.PaidByDescr = reader("PaidByDescr").ToString.Trim
                oReturnValue.AmountCoverage = CType(reader("AmountCoverage").ToString.Trim, Double)
                oReturnValue.MainPremiumToCust = CType(reader("MainPremiumToCust").ToString.Trim, Double)
                oReturnValue.PremiumAmountByCust = CType(reader("PremiumAmountByCust").ToString.Trim, Double)
                oReturnValue.SRCCToCust = CType(reader("SRCCToCust").ToString.Trim, Double)
                oReturnValue.TPLToCust = CType(reader("TPLToCust").ToString.Trim, Double)
                oReturnValue.FloodToCust = CType(reader("FloodToCust").ToString.Trim, Double)
                oReturnValue.LoadingFeeToCust = CType(reader("LoadingFeeToCust").ToString.Trim, Double)
                oReturnValue.AdminFeeToCust = CType(reader("AdminFeeToCust").ToString.Trim, Double)
                oReturnValue.MeteraiFeeToCust = CType(reader("MeteraiFeeToCust").ToString.Trim, Double)
                oReturnValue.AssetUsageDescr = reader("AssetUsageDescr").ToString.Trim
                oReturnValue.UsedNew = reader("UsedNew").ToString.Trim
                oReturnValue.FlagInsActivation = reader("FlagInsActivation").ToString.Trim
                oReturnValue.DataExist = CType(reader("DataExist").ToString.Trim, Boolean)
                oReturnValue.OutPutCoverPeriodSelection = CType(reader("OutPutCoverPeriodSelection").ToString.Trim, Boolean)
                oReturnValue.InsuranceComBranchID = reader("InsuranceComBranchID").ToString.Trim
                oReturnValue.MaturityDate = CType(reader("MaturityDate").ToString.Trim, Date)
                oReturnValue.MaxYearNum = CType(reader("MaxYearNum").ToString.Trim, Int16)

            End If
            '            oReturnValue.DataExist = CType(params(1).Value, Boolean)
            '           oReturnValue.OutPutCoverPeriodSelection = CType(params(2).Value, Boolean)
            reader.Close()
            Return oReturnValue

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsCoAllocationDetailList.vb", "GetListByAgreementNo", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)

        End Try



    End Function

#End Region

#Region "GetListByApplicationId"


    'spInsCompanySelectionGetListByApplicationId
    Public Function GetListByApplicationId(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId.Trim
        params(1) = New SqlParameter("@ApplicationId", SqlDbType.Char, 20)
        params(1).Value = customClass.ApplicationID.Trim

        Try
            Dim oReturnValue As New Parameter.InsCoAllocationDetailList
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, SP_GETBYAPPID, params)
            If reader.Read Then
                oReturnValue.BranchId = reader("BranchId").ToString.Trim
                oReturnValue.ApplicationID = reader("ApplicationID").ToString.Trim
                oReturnValue.AgreementNo = reader("AgreementNo").ToString.Trim
                oReturnValue.CustomerID = reader("CustomerID").ToString.Trim
                oReturnValue.CustomerName = reader("CustomerName").ToString.Trim
                oReturnValue.TotalStdPremium = CType(reader("StdPremium").ToString.Trim, Double)
                oReturnValue.AssetTypeDescr = reader("AssetTypeDescr").ToString.Trim
                oReturnValue.GoLiveDate = CType(reader("GoLiveDate").ToString.Trim, Date)
                oReturnValue.StartDateRenewal = CType(reader("StartDateRenewal").ToString.Trim, Date)
                oReturnValue.InsuredByDescr = reader("InsuredByDescr").ToString.Trim
                oReturnValue.PaidByDescr = reader("PaidByDescr").ToString.Trim
                oReturnValue.AmountCoverage = CType(reader("AmountCoverage").ToString.Trim, Double)
                oReturnValue.MainPremiumToCust = CType(reader("MainPremiumToCust").ToString.Trim, Double)
                oReturnValue.PremiumAmountByCust = CType(reader("PremiumAmountByCust").ToString.Trim, Double)
                oReturnValue.SRCCToCust = CType(reader("SRCCToCust").ToString.Trim, Double)
                oReturnValue.TPLToCust = CType(reader("TPLToCust").ToString.Trim, Double)
                oReturnValue.FloodToCust = CType(reader("FloodToCust").ToString.Trim, Double)
                oReturnValue.LoadingFeeToCust = CType(reader("LoadingFeeToCust").ToString.Trim, Double)
                oReturnValue.AdminFeeToCust = CType(reader("AdminFeeToCust").ToString.Trim, Double)
                oReturnValue.MeteraiFeeToCust = CType(reader("MeteraiFeeToCust").ToString.Trim, Double)
                oReturnValue.AssetUsageDescr = reader("AssetUsageDescr").ToString.Trim
                oReturnValue.UsedNew = reader("UsedNew").ToString.Trim
                oReturnValue.FlagInsActivation = reader("FlagInsActivation").ToString.Trim
                oReturnValue.DataExist = CType(reader("DataExist").ToString.Trim, Boolean)
                oReturnValue.OutPutCoverPeriodSelection = CType(reader("OutPutCoverPeriodSelection").ToString.Trim, Boolean)
                oReturnValue.InsuranceComBranchID = reader("InsuranceComBranchID").ToString.Trim
                oReturnValue.MaturityDate = CType(reader("MaturityDate").ToString.Trim, Date)
                oReturnValue.MaxYearNum = CType(reader("MaxYearNum").ToString.Trim, Int16)
                oReturnValue.Tenor = CType(reader("Tenor").ToString.Trim, Int16)
                oReturnValue.AssetYear = CType(IIf(IsDBNull(reader("ManufacturingYear")), "", reader("ManufacturingYear")), String)
                oReturnValue.EffectiveDate = CType(reader("EffectiveDate").ToString.Trim, Date)
                oReturnValue.ApplicationType = reader("ApplicationType").ToString.Trim
                oReturnValue.InsuranceType = reader("InsuranceType").ToString.Trim
                oReturnValue.UsageID = reader("UsageID").ToString.Trim

            End If
            reader.Close()
            Return oReturnValue

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsCoAllocationDetailList.vb", "GetListByAgreementNo", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)

        End Try



    End Function

#End Region
#Region "GetGridTenor"

    Public Function GetGridTenor(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList

        Dim oReturnValue As New Parameter.InsCoAllocationDetailList

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.ApplicationID

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, SP_GET_GRID_TENOR, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            '
        End Try

    End Function
#End Region
#Region "GetGridInsCoPremium"

    Public Function GetGridInsCoPremium(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList

        Dim oReturnValue As New Parameter.InsCoAllocationDetailList
        Dim params(15) As SqlParameter
        Dim dtSet As DataSet

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.ApplicationID
        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = customClass.BranchId
        params(2) = New SqlParameter("@CoverPeriodSelection", SqlDbType.VarChar, 3)
        params(2).Value = customClass.CoverPeriodSelection
        params(3) = New SqlParameter("@strWithTPL", SqlDbType.VarChar, 8000)
        params(3).Value = customClass.strWithTPL
        params(4) = New SqlParameter("@strWithTPLAmount", SqlDbType.VarChar, 8000)
        params(4).Value = customClass.strWithTPLAmount
        params(5) = New SqlParameter("@strWithFlood", SqlDbType.VarChar, 8000)
        params(5).Value = customClass.strWithFlood
        params(6) = New SqlParameter("@strWithEQVET", SqlDbType.VarChar, 8000)
        params(6).Value = customClass.strWithEQVET
        params(7) = New SqlParameter("@strWithSRCC", SqlDbType.VarChar, 8000)
        params(7).Value = customClass.strWithSRCC
        params(8) = New SqlParameter("@strWithTerrorism", SqlDbType.VarChar, 8000)
        params(8).Value = customClass.strWithTerrorism
        params(9) = New SqlParameter("@strWithPAAmount", SqlDbType.VarChar, 8000)
        params(9).Value = customClass.strWithPAAmount
        params(10) = New SqlParameter("@strWithPA", SqlDbType.VarChar, 8000)
        params(10).Value = customClass.strWithPA
        params(11) = New SqlParameter("@strWithPADriver", SqlDbType.VarChar, 8000)
        params(11).Value = customClass.strWithPADriver
        params(12) = New SqlParameter("@strCoverageType", SqlDbType.VarChar, 8000)
        params(12).Value = customClass.strCoverageType
        params(13) = New SqlParameter("@NumRows", SqlDbType.Int)
        params(13).Value = customClass.NumRows
        params(14) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
        params(14).Direction = ParameterDirection.Output
        params(15) = New SqlParameter("@strWithPADriverAmount", SqlDbType.VarChar, 8000)
        params(15).Value = customClass.strWithPADriverAmount

        Try
            dtSet = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, SP_GET_GRID_INSURANCE_COMPANY_REMIUM, params)

            oReturnValue.ListData = dtSet.Tables(0)
            oReturnValue.ListData2 = dtSet.Tables(1)

            oReturnValue.ErrStr = CType(params(14).Value, String)

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        End Try

    End Function


    Public Function GetGridInsCoPremiumView(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList

        Dim oReturnValue As New Parameter.InsCoAllocationDetailList
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Dim dtSet As DataSet

        params.Add(New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20) With {.Value = customClass.ApplicationID})
        params.Add(New SqlParameter("@BranchID", SqlDbType.VarChar, 3) With {.Value = customClass.BranchId})

        Dim prmErr = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
        params.Add(prmErr)
        Try
            dtSet = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spInsGridInsCoPremiumView", params.ToArray)

            oReturnValue.ListData = dtSet.Tables(0)
            oReturnValue.ListData2 = dtSet.Tables(1) 
            oReturnValue.ErrStr = CType(prmErr.Value, String) 
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        End Try
    End Function

#End Region
#Region "GetGridInsuranceStatistic"

    Public Function GetGridInsuranceStatistic(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList

        Dim oReturnValue As New Parameter.InsCoAllocationDetailList
        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter("@StatisticMonth", SqlDbType.Int)
        params(0).Value = customClass.StatMonth
        params(1) = New SqlParameter("@statisticyear", SqlDbType.Int)
        params(1).Value = customClass.StatYear
        params(2) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
        params(2).Value = customClass.BranchId


        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, SP_GET_GRID_INSURANCE_STATISTIC, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsCoAllocationDetailList.vb", "GetGridInsuranceStatistic", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)

        End Try


    End Function

#End Region
#Region "SaveInsuranceCompanySelectionLastProcess"

    Public Function SaveInsuranceCompanySelectionLastProcess(ByVal customClass As Parameter.InsCoAllocationDetailList) As String
        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        If customClass.spName = "" Then customClass.spName = "spInsCoSelectionUpdateAssetDetail"

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            Dim params(36) As SqlParameter

            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customClass.ApplicationID
            params(2) = New SqlParameter("@StartDate", SqlDbType.DateTime)
            params(2).Value = customClass.StartDate
            params(3) = New SqlParameter("@EndDateEntry", SqlDbType.DateTime)
            params(3).Value = customClass.EndDate
            params(4) = New SqlParameter("@InsuranceComBranchID", SqlDbType.VarChar)
            params(4).Value = customClass.InsuranceComBranchID
            params(5) = New SqlParameter("@MainPremium", SqlDbType.Decimal)
            params(5).Value = customClass.MainPremiumToInsco
            params(6) = New SqlParameter("@RSCCToInsco", SqlDbType.Decimal)
            params(6).Value = customClass.RSCCToInsco
            params(7) = New SqlParameter("@TPLToInsco", SqlDbType.Decimal)
            params(7).Value = customClass.TPLToInsco
            params(8) = New SqlParameter("@FloodToInsco", SqlDbType.Decimal)
            params(8).Value = customClass.FloodToInsco
            params(9) = New SqlParameter("@LoadingFeeToInsco", SqlDbType.Decimal)
            params(9).Value = customClass.LoadingFeeToInsco
            params(10) = New SqlParameter("@TotalPremiumToInsco", SqlDbType.Decimal)
            params(10).Value = customClass.TotalPremiumToInsco
            params(11) = New SqlParameter("@Adminfee", SqlDbType.Decimal)
            params(11).Value = customClass.AdminFee
            params(12) = New SqlParameter("@MeteraiFee", SqlDbType.Decimal)
            params(12).Value = customClass.MeteraiFee
            params(13) = New SqlParameter("@StatisticMonth", SqlDbType.Int)
            params(13).Value = customClass.StatMonth
            params(14) = New SqlParameter("@StatisticYear", SqlDbType.Int)
            params(14).Value = customClass.StatYear
            params(15) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(15).Value = customClass.BusinessDate
            params(16) = New SqlParameter("@CoyID", SqlDbType.Char, 20)
            params(16).Value = customClass.CoyID
            params(17) = New SqlParameter("@CoverPeriodSelection", SqlDbType.Char, 2)
            params(17).Value = customClass.CoverPeriodSelection
            params(18) = New SqlParameter("@ErrMessages", SqlDbType.Char, 100)
            params(18).Direction = ParameterDirection.Output
            params(19) = New SqlParameter("@IsInscoSelectOnEntriApp", SqlDbType.Bit)
            params(19).Value = IIf(IsNothing(customClass.IsInscoSelectOnEntriApp), 0, customClass.IsInscoSelectOnEntriApp)
            params(20) = New SqlParameter("@PAPassToInsco", SqlDbType.Money)
            params(20).Value = customClass.PAPassanger
            params(21) = New SqlParameter("@PADriverToInsco", SqlDbType.Money)
            params(21).Value = customClass.PADriver
            params(22) = New SqlParameter("@BiayaPolis", SqlDbType.Money)
            params(22).Value = customClass.BiayaPolis
            params(23) = New SqlParameter("@PaidAmountbyCust", SqlDbType.Money)
            params(23).Value = customClass.PaidAmountByCust
            params(24) = New SqlParameter("@TotalPremiManual", SqlDbType.Money)
            params(24).Value = customClass.TotalPremiManual

            params(25) = New SqlParameter("@PerluasanPA", SqlDbType.Bit)
            params(25).Value = customClass.PerluasanPA

            params(26) = New SqlParameter("@PaidCust", SqlDbType.Structured)
            params(26).Value = customClass.DataTablePaid

            params(27) = New SqlParameter("@InsAmountToNPV", SqlDbType.Money)
            params(27).Value = customClass.InsAmountToNPV

            params(28) = New SqlParameter("@PaidByCreditProtection", SqlDbType.VarChar, 10)
            params(28).Value = customClass.PaidByCreditProtection

            params(29) = New SqlParameter("@PremiAdditionalSignCreditProtection", SqlDbType.VarChar, 1)
            params(29).Value = customClass.PremiAdditionalSignCreditProtection

            params(30) = New SqlParameter("@PremiAdditionalAmountCreditProtection", SqlDbType.Money)
            params(30).Value = customClass.PremiAdditionalAmountCreditProtection

            params(31) = New SqlParameter("@PremiNettCreditProtection", SqlDbType.Money)
            params(31).Value = customClass.PremiNettCreditProtection

            params(32) = New SqlParameter("@PaidByJaminanCredit", SqlDbType.VarChar, 10)
            params(32).Value = customClass.PaidByJaminanCredit

            params(33) = New SqlParameter("@PremiAdditionalSignJaminanCredit", SqlDbType.VarChar, 1)
            params(33).Value = customClass.PremiAdditionalSignJaminanCredit

            params(34) = New SqlParameter("@PremiAdditionalAmountJaminanCredit", SqlDbType.Money)
            params(34).Value = customClass.PremiAdditionalAmountJaminanCredit

            params(35) = New SqlParameter("@PremiNettJaminanCredit", SqlDbType.Money)
            params(35).Value = customClass.PremiNettJaminanCredit

            params(36) = New SqlParameter("@TenorAngsuran", SqlDbType.Int)
            params(36).Value = customClass.Tenor2

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, customClass.spName, params)

            objtrans.Commit()
            Return CType(params(18).Value, String)

        Catch ex As Exception
            objtrans.Rollback()
            Dim err As New MaxiloanExceptions
            err.WriteLog("InscoAllocationDetailList.vb", "SaveInsuranceCompanySelectionLastProcess", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Function

#End Region


    Public Function PenutupanAsuransiManualUpdate(ByVal customClass As Parameter.InsCoAllocationDetailList) As String
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction



            params.Add(New SqlParameter("@BranchId", SqlDbType.Char, 3) With {.Value = customClass.BranchId})
            params.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20) With {.Value = customClass.ApplicationID})
            params.Add(New SqlParameter("@coverperiod ", SqlDbType.Char, 2) With {.Value = customClass.CoverPeriodSelection})
            params.Add(New SqlParameter("@StartDate", SqlDbType.DateTime) With {.Value = customClass.StartDate})
            params.Add(New SqlParameter("@enddate", SqlDbType.DateTime) With {.Value = customClass.StartDate.AddYears(1)})
             
           
            Dim param = New SqlParameter("@errMsg", SqlDbType.Char, 100) With {.Direction = ParameterDirection.Output}
            params.Add(param)


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spPenutupanAssuransiManual", params.ToArray)

            objtrans.Commit()
            Return CType(param.Value, String)

        Catch ex As Exception
            objtrans.Rollback()
            Dim err As New MaxiloanExceptions
            err.WriteLog("InscoAllocationDetailList.vb", "SaveInsuranceCompanySelectionLastProcess", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try


    End Function 

    Public Sub InitMaskAssComponent(customClass As Parameter.MaskAssCalcEng)

        Dim oReturnValue As New Parameter.InsCoAllocationDetailList
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Dim dtSet As DataSet

        params.Add(New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20) With {.Value = customClass.ApplicationId})
        params.Add(New SqlParameter("@BranchID", SqlDbType.VarChar, 3) With {.Value = customClass.BranchId})
         
        Try
            dtSet = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spInitMaskAssComponent", params.ToArray) 
            customClass.InitialValue(dtSet.Tables(1).Rows(0))
            customClass.InitMaskapai(dtSet.Tables(0))
            customClass.InitInsuranceCoverType(dtSet.Tables(2))
        Catch ex As Exception
            Throw New Exception(ex.Message) 
        End Try
    End Sub

    Public Function CalcKoreksiMaskapaiAss(customClass As Parameter.MaskAssCalcEng) As DataSet

        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Dim params0 As IList(Of SqlParameter) = New List(Of SqlParameter)
        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            Dim brn = New SqlParameter("@BranchId", SqlDbType.Char, 3) With {.Value = customClass.BranchId}
            Dim appid = New SqlParameter("@ApplicationID", SqlDbType.Char, 20) With {.Value = customClass.ApplicationId}

            params.Add(brn)
            params.Add(appid)
            params0.Add(brn)
            params0.Add(appid)

            params.Add(New SqlParameter("@ApplicationType", SqlDbType.Char, 10) With {.Value = customClass.ApplicationType})
            params.Add(New SqlParameter("@UsageID", SqlDbType.Char, 10) With {.Value = customClass.UsageID})
            params.Add(New SqlParameter("@NewUsed", SqlDbType.Char, 1) With {.Value = customClass.NewUsed})
            params.Add(New SqlParameter("@AmountCoverage", SqlDbType.Decimal) With {.Value = customClass.AmountCoverage})
            params.Add(New SqlParameter("@InsuranceType", SqlDbType.Char, 2) With {.Value = customClass.InsuranceType})
            params.Add(New SqlParameter("@BusinessDate", SqlDbType.Date) With {.Value = customClass.BusinessDate})
            params.Add(New SqlParameter("@ManufacturingYear", SqlDbType.Date) With {.Value = New Date(customClass.ManufacturingYear, 1, 1)})
            params.Add(New SqlParameter("@IsPageSourceCompanyCustomer", SqlDbType.Bit) With {.Value = customClass.IsPageSourceCompanyCustomer})
            params.Add(New SqlParameter("@DiscountToCust", SqlDbType.Decimal) With {.Value = customClass.DiscountToCust})
            params.Add(New SqlParameter("@PaidAmountByCust", SqlDbType.Decimal) With {.Value = customClass.PaidAmountByCust})
            params.Add(New SqlParameter("@MaskAssBranchID", SqlDbType.Char, 10) With {.Value = customClass.MaskAssBranchIDChoosed})
            params.Add(New SqlParameter("@insCover", SqlDbType.Structured) With {.Value = customClass.ToDataTable()})

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spShadowKoreksiInsuranceByCompanyDetailSaveAddTemporary", params.ToArray)
            objtrans.Commit()
            Dim prmErr = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            params0.Add(prmErr)
            Dim dtSet = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spInsGridInsCoPremiumKoreksiView", params0.ToArray)
            Return dtSet
        Catch ex As Exception
            objtrans.Rollback()
            Throw New Exception(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Public Function KoreksiMaskapaiAss(customClass As Parameter.MaskAssCalcEng) As String
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            params.Add(New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20) With {.Value = customClass.ApplicationId})
            params.Add(New SqlParameter("@CoverPeriod", SqlDbType.Char, 2) With {.Value = customClass.CoverPeriodKoreksi})
            params.Add(New SqlParameter("@StartDate", SqlDbType.Date) With {.Value = customClass.TglDariKoreksi})
            params.Add(New SqlParameter("@EndDate", SqlDbType.Date) With {.Value = customClass.TglSampaiKoreksi})
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spInsuranceKorekesiSave", params.ToArray)
            objtrans.Commit()

            Return "OK"
        Catch ex As Exception
            objtrans.Rollback()
            Throw New Exception(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
        



    End Function
End Class
