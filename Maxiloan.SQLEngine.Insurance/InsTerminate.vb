
#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region
Public Class InsTerminate : Inherits Maxiloan.SQLEngine.DataAccessBase

    'description : fungsi ini untuk menampilkan data customer yang mengajukan refund ke datagrid
    Public Function InsTerminateList(ByVal customclass As Parameter.InsTerminate) As Parameter.InsTerminate
        Dim data As New Parameter.InsTerminate
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize
        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond
        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy
        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output
        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spInsTerminateRequest", params).Tables(0)
            data.TotalRecord = CInt(params(4).Value)
            Return data
        Catch exp As Exception
            WriteException("InsTerminate", "Insurance Termination", exp.Message + exp.StackTrace)
        End Try

    End Function



    'description : fungsi ini untuk menampilkan data untuk dicetak ke form refund yang dikirimimkan ke insco
    Public Function InsrequestPrint(ByVal customclass As Parameter.InsTerminate) As DataSet
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(2).Value = customclass.AssetSequenceNo
        params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(3).Value = customclass.InsSequenceNo
        params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(4).Value = customclass.BusinessDate

        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spInsuranceRefundClaim", params)
        Catch exp As Exception
            WriteException("InsTerminate", "InsuranceTerminateRequestForm", exp.Message + exp.StackTrace)
        End Try
    End Function

    'description : fungsi ini untuk menampilkan data detil customer jika icon terminate diklik
    Public Function InsTerminateDetail(ByVal customclass As Parameter.InsTerminate) As Parameter.InsTerminate
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int, 3)
        params(2).Value = customclass.AssetSequenceNo
        params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int, 3)
        params(3).Value = customclass.InsSequenceNo
        params(4) = New SqlParameter("@TerminationDate", SqlDbType.DateTime)
        params(4).Value = customclass.TerminationDate

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spInsTerminateProcessForm", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("InsTerminate", "InsuranceTerminateDetail", exp.Message + exp.StackTrace)
        End Try
    End Function
    'description : fungsi ini untuk menampilkan data hasil perhitungan refund customer

    Public Function InsCalculate(ByVal customclass As Parameter.InsTerminate) As Parameter.InsTerminate
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int, 3)
        params(2).Value = customclass.AssetSequenceNo
        params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int, 3)
        params(3).Value = customclass.InsSequenceNo
        params(4) = New SqlParameter("@Businessdate", SqlDbType.DateTime)
        params(4).Value = customclass.TerminationDate
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spCalculateTerminate", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("InsTerminate", "InsuranceTerminateCalculateDetail", exp.Message + exp.StackTrace)
        End Try
    End Function

    'description : fungsi ini untuk menyimpan data hasil calculate
    Public Function InsSaveCalculate(ByVal customclass As Parameter.InsTerminate) As Boolean
        Dim params() As SqlParameter = New SqlParameter(12) {}
        Dim objtrans As SqlTransaction
        Dim objcon As New SqlConnection(customclass.strConnection)
        If objcon.State = ConnectionState.Closed Then objcon.Open()
        objtrans = objcon.BeginTransaction

        params(0) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
        params(0).Value = customclass.AgreementNo
        params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(1).Value = customclass.BranchId
        params(2) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(2).Value = customclass.ApplicationID
        params(3) = New SqlParameter("@AssetSeqNo", SqlDbType.Int, 3)
        params(3).Value = customclass.AssetSequenceNo
        params(4) = New SqlParameter("@InsSeqNo", SqlDbType.Int, 3)
        params(4).Value = customclass.InsSequenceNo
        params(5) = New SqlParameter("@CustomerName", SqlDbType.VarChar, 50)
        params(5).Value = customclass.CustomerName
        params(6) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(6).Value = customclass.TerminationDate
        params(7) = New SqlParameter("@RefundFromInsCo", SqlDbType.Decimal)
        params(7).Value = customclass.ClaimAmount
        params(8) = New SqlParameter("@RefundDeductionAmount", SqlDbType.Decimal)
        params(8).Value = customclass.RefundDeduct
        params(9) = New SqlParameter("@TotalRefund ", SqlDbType.Decimal)
        params(9).Value = customclass.TotalRefundTocust
        params(10) = New SqlParameter("@Notes", SqlDbType.Char, 50)
        params(10).Value = customclass.Notes
        params(11) = New SqlParameter("@ReasonID", SqlDbType.Char, 10)
        params(11).Value = customclass.ReasonID
        params(12) = New SqlParameter("@saveStatus", SqlDbType.Bit, 3)
        params(12).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spInsSaveCalculate", params)
            objtrans.Commit()
            Return CType(params(12).Value, Boolean)
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("InsTerminate", "InsuranceTerminateSaveCalculate", exp.Message + exp.StackTrace)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Function

    'description : fungsi ini untuk menyajikan data hasil perhitungan refund ke form
    Public Function InsRefundCalculate(ByVal customclass As Parameter.InsTerminate) As DataSet
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID
        params(2) = New SqlParameter("@AssetSeqNo ", SqlDbType.Int)
        params(2).Value = customclass.AssetSequenceNo
        params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(3).Value = customclass.InsSequenceNo
        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spInsuranceRefundCalculate", params)
        Catch exp As Exception
            WriteException("InsTerminate", "InsuranceTerminateCalculate", exp.Message + exp.StackTrace)
        End Try
    End Function

    'description : fungsi ini untuk menginsert data ke tabel Mail Transaction
    Public Sub InsInsertMail(ByVal customclass As Parameter.InsTerminate)
        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(2).Value = customclass.AssetSequenceNo
        params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(3).Value = customclass.InsSequenceNo
        params(4) = New SqlParameter("@LoginId", SqlDbType.VarChar, 12)
        params(4).Value = customclass.LoginId
        params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(5).Value = customclass.BusinessDate

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spInsInsertMail", params)
        Catch exp As Exception
            WriteException("InsTerminate", "InsuranceInsertMailTransaction", exp.Message + exp.StackTrace)
        End Try
    End Sub
    Public Function InsApproveRejectCalculate(ByVal customclass As Parameter.InsTerminate) As String
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
       
        params.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = customclass.BranchId})

        params.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20) With {.Value = customclass.ApplicationID})
        params.Add(New SqlParameter("@AssetSeqNo", SqlDbType.Int, 3) With {.Value = customclass.AssetSequenceNo})
        params.Add(New SqlParameter("@InsSeqNo", SqlDbType.Int, 3) With {.Value = customclass.InsSequenceNo})

        params.Add(New SqlParameter("@ClaimStatus", SqlDbType.Char, 1) With {.Value = customclass.ClaimStatus})
        params.Add(New SqlParameter("@Notes", SqlDbType.Char, 50) With {.Value = customclass.Notes})
        Dim errprm = New SqlParameter("@errMsg", SqlDbType.Char, 500) With {.Direction = ParameterDirection.Output}
        params.Add(errprm)

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spInsApproveRejectCalculate", params.ToArray)
           
            Return CType(errprm.Value, String)
        Catch exp As Exception 
            Throw New Exception(exp.Message) 
        End Try

    End Function
    Public Function InsrequestPrintRefund(ByVal customclass As Parameter.InsTerminate) As DataSet
        Dim params() As SqlParameter = New SqlParameter(2) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID
        'params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        'params(2).Value = customclass.AssetSequenceNo
        'params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        'params(3).Value = customclass.InsSequenceNo
        params(2) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(2).Value = customclass.BusinessDate

        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spInsuranceRefund", params)
        Catch exp As Exception
            WriteException("InsTerminate", "InsuranceTerminateRequestForm", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Sub InsInsertMailRefund(ByVal customclass As Parameter.InsTerminate)
        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(2).Value = customclass.AssetSequenceNo
        params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(3).Value = customclass.InsSequenceNo
        params(4) = New SqlParameter("@LoginId", SqlDbType.VarChar, 12)
        params(4).Value = customclass.LoginId
        params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(5).Value = customclass.BusinessDate

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spInsInsertMailRefund", params)
        Catch exp As Exception
            WriteException("InsTerminate", "InsuranceInsertMailTransaction", exp.Message + exp.StackTrace)
        End Try
    End Sub
End Class
