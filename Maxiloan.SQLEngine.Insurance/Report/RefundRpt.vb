
#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class RefundRpt : Inherits Maxiloan.SQLEngine.DataAccessBase
    Public Function ViewRefundReport(ByVal customClass As Parameter.RefundRpt) As Parameter.RefundRpt

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@pivchrWhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        Try
            customClass.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spRefundRpt", params)
            Return customClass
        Catch exp As Exception
            WriteException("Refund Report", "ViewRefundReport", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
