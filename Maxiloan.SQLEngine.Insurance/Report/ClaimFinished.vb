
#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region
Public Class ClaimFinished : Inherits maxiloan.SQLEngine.DataAccessBase
    Public Function ViewClaimFinishedReport(ByVal customClass As Parameter.ClaimFinished) As Parameter.ClaimFinished

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@pivchrWhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        Try
            customClass.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spClaimFinishedRpt", params)
            Return customClass
        Catch exp As Exception
            WriteException("Claim Outstanding Report", "ViewClaimOutstandingReport", exp.Message + exp.StackTrace)
        End Try
    End Function

End Class
