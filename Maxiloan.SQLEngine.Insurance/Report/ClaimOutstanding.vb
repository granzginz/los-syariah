
#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class ClaimOutstanding : Inherits Maxiloan.SQLEngine.DataAccessBase
    Public Function ViewClaimReport(ByVal customClass As Parameter.ClaimOutstanding) As Parameter.ClaimOutstanding

        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@pivchrWhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(1).Value = customClass.BusinessDate
        Try
            customClass.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spClaimRpt", params)
            Return customClass
        Catch exp As Exception
            WriteException("Claim Outstanding Report", "ViewClaimOutstandingReport", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function getInsuranceCoverageList(ByVal customClass As Parameter.ClaimOutstanding) As Parameter.ClaimOutstanding
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@wherecond", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        Try
            customClass.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, _
            CommandType.StoredProcedure, "spGetInsuranceAssetCoverage", params)
            Return customClass
        Catch exp As Exception
            WriteException("Insurance Coverage", "Insurance Coverage", exp.Message + exp.StackTrace)
        End Try
    End Function

End Class
