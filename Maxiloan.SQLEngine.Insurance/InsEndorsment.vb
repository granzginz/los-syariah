
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.General
Imports Maxiloan.Exceptions

#End Region

Public Class InsEndorsment : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    Private Const SP_INSENDORSMENT_LIST As String = "spInsEndorsmentRequest"
    Private Const SP_INSENDORSMENT_DATA As String = "spInsEndorsmentGetData"
    Private Const SP_INSENDORSMENT_DATADETAIL As String = "spInsEndorsmentGetData_InsDetail"
    Private Const SP_INSENDORSMENT_COVERAGE As String = "spInsEndorsmentGetDataCoverageType"
    Private Const SP_INSENDORSMENT_TPL As String = "spInsEndorsmentGetDataTPL"
    Private Const SP_INSENDORSMENT_CALCULATIONRESULT As String = "spGetAdminStampFee"
    Private Const SP_INSENDORSMENT_INSUREDCOVERAGE As String = "spInsEndorsmentInsuredCoverage"
#End Region
    'fungsi ini dipakai untuk menampilkan semua data customer yang mau melakukan endorsment
    Public Function InsEndorsmentList(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
        Dim oReturnValue As New Parameter.InsEndorsment
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, SP_INSENDORSMENT_LIST, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception

            'Dim err As New MaxiloanExceptions
            'err.WriteLog("InsEndorsment.aspx", "InsEndorsmentList", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try
    End Function

    'fungsi ini dipakai untuk menampilkan data detil customer jika diklik hyperlink endors
    Public Function GetData(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
        Dim oReturnValue As New Parameter.InsEndorsment
        Dim reader As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customClass.AppID
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(2).Value = customClass.AssetSeqNo
        params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(3).Value = customClass.InsSeqNo


        Try
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, SP_INSENDORSMENT_DATA, params)
            If reader.Read Then
                oReturnValue.MaskAssBranchID = reader("MaskAssBranchID").ToString
                oReturnValue.PolicyNumber = reader("PolicyNumber").ToString
                oReturnValue.ManufacturingYear = reader("ManufacturingYear").ToString
                oReturnValue.Usage = reader("Usage").ToString
                oReturnValue.AssetUsage = reader("AssetUsage").ToString
                oReturnValue.SerialNo1Label = reader("SerialNo1Label").ToString
                oReturnValue.SerialNo2Label = reader("SerialNo2Label").ToString
                oReturnValue.SerialNo1 = reader("SerialNo1").ToString
                oReturnValue.SerialNo2 = reader("SerialNo2").ToString
                oReturnValue.InsLength = reader("InsLength").ToString
                oReturnValue.InsNotes = reader("InsNotes").ToString
                oReturnValue.AccNotes = reader("AccNotes").ToString
                If reader("StartDate").ToString = Nothing Then
                    oReturnValue.StartDate = ""
                Else
                    oReturnValue.StartDate = CStr(reader("startdate"))
                End If
                If reader("EndDate").ToString = Nothing Then
                    oReturnValue.EndDate = ""
                Else
                    oReturnValue.EndDate = CType(reader("EndDate"), String)
                End If
                oReturnValue.ContractPrepaidAmount = CType(reader("ContractPrepaidAmount"), Decimal)
                oReturnValue.InsuranceType = reader("InsuranceType").ToString
                oReturnValue.UsedNew = reader("UsedNew").ToString
                oReturnValue.Tenor = reader("Tenor").ToString
                oReturnValue.MainPremiumToCust = CType(reader("MainPremiumToCust"), Decimal)
                oReturnValue.MainPremiumToInsCo = CType(reader("MainPremiumToInsco"), Decimal)
                oReturnValue.PremiumAmountByCust = CType(reader("PremiumAmountByCust"), Decimal)
                oReturnValue.PremiumAmountToInsCo = CType(reader("PremiumAmountToInsCo"), Decimal)
                oReturnValue.YearActive = reader("YearActive").ToString
                oReturnValue.AgreementNo = reader("AgreementNo").ToString
                oReturnValue.BranchId = reader("BranchID").ToString
                oReturnValue.AppID = reader("ApplicationId").ToString
                oReturnValue.Name = reader("Name").ToString
                oReturnValue.Description = reader("Description").ToString
                oReturnValue.ApplicationType = reader("ApplicationType").ToString
                oReturnValue.CustomerId = reader("CustomerID").ToString
                oReturnValue.SumInsured = CType(reader("SumInsured"), Decimal)
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsEndorsment.vb", "GetData", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try
    End Function

    'fungsi ini dipakai untuk mendapatkan adminfee dan stampdutyfee dari customer
    Public Function GetEndorsCalculationResult(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
        Dim oReturnValue_Result As New Parameter.InsEndorsment
        Dim reader As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customClass.AppID

        Try
            oReturnValue_Result.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, SP_INSENDORSMENT_CALCULATIONRESULT, params).Tables(0)


            Return oReturnValue_Result
        Catch ex As Exception
            'Dim err As New MaxiloanExceptions
            'err.WriteLog("InsEndorsment.aspx", "GetDataEndorsment", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try
    End Function

    'fungsi ini untuk menampilkan data asuransi cust ke datagrid agar dapat diendors
    Public Function GetDataInsDetail(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
        Dim oReturnValue As New Parameter.InsEndorsment
        Dim reader As SqlDataReader

        'Dim params(5) As SqlParameter
        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customClass.AppID
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(2).Value = customClass.AssetSeqNo
        params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(3).Value = customClass.InsSeqNo
        params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(4).Value = customClass.BusinessDate
        params(5) = New SqlParameter("@EndorsmentDate", SqlDbType.DateTime)
        params(5).Value = customClass.BDEndorsDate  ' Ini maksudnya EndorsmentDate

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, SP_INSENDORSMENT_DATADETAIL, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsEndorsment.aspx", "GetDataDetailEndorsment", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try
    End Function
    'fungsi ini untuk mendapatkan data tplamount dan tplpremium dari tabel
    Public Function GetDataCbTPL(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
        Dim oReturnValue As New Parameter.InsEndorsment
        Dim reader As SqlDataReader

        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, SP_INSENDORSMENT_TPL, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            'Dim err As New MaxiloanExceptions
            'err.WriteLog("InsEndorsment.aspx", "GetDataTPL", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try
    End Function
    'fungsi ini untuk mendapatkan semua tipe coveragetype dari tabel 

    Public Function GetDataCbCoverage(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
        Dim oReturnValue As New Parameter.InsEndorsment
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, SP_INSENDORSMENT_COVERAGE).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            'Dim err As New MaxiloanExceptions
            'err.WriteLog("InsEndorsment.aspx", "GetDataTPL", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try
    End Function
    'fungsi ini untuk melakukan perhitungan newpremi,additionalpremi yang ada berdasarkan perubahan yang dilakukan

    Public Function GetInsured_Coverage(ByVal oCustomClass As Parameter.InsEndorsment, ByVal oData1 As DataTable) As Parameter.InsEndorsment
        Dim oReturnValue As New Parameter.InsEndorsment
        Dim odata As New DataTable
        Dim row As datarow
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(20) {}
        Dim intloop As Integer = 0
        Dim datarow As datarow

        odata.Columns.Add("NewSRCCCust", GetType(Decimal)) ''untuk premi srcc baru
        odata.Columns.Add("NewFloodCust", GetType(Decimal)) ''untuk premi flood baru
        odata.Columns.Add("NewTPLCust", GetType(Decimal)) ''untuk premi tpl baru
        odata.Columns.Add("NewTPLAmountCust", GetType(Decimal)) ''untuk tplamount cust yg baru
        odata.Columns.Add("NewTPLAmountInsCo", GetType(Decimal)) ''untuk tplamount insco yg baru
        odata.Columns.Add("NewPremiCust", GetType(Decimal)) ''untuk new premi
        odata.Columns.Add("NewPremiInsco", GetType(Decimal)) ''untuk new premi insco
        odata.Columns.Add("NewSRCCInsco", GetType(Decimal)) ''untuk premi srcc insco yg baru
        odata.Columns.Add("NewFloodInsco", GetType(Decimal)) ''untuk premi flood insco yg baru
        odata.Columns.Add("NewTPLInsCo", GetType(Decimal)) ''untuk premi tpl insco yg baru
        odata.Columns.Add("NewCoverage", GetType(String))
        odata.Columns.Add("OldCoverage", GetType(String))
        odata.Columns.Add("Year", GetType(String))
        odata.Columns.Add("OldSRCC", GetType(String))
        odata.Columns.Add("NewSRCC", GetType(Boolean))
        odata.Columns.Add("OldFlood", GetType(String))
        odata.Columns.Add("NewFlood", GetType(Boolean))
        odata.Columns.Add("OldTPL", GetType(Decimal))
        odata.Columns.Add("NewTPLAmount", GetType(Decimal))        
        odata.Columns.Add("AddPremiCust", GetType(Decimal))
        odata.Columns.Add("AddPremiInsCo", GetType(Decimal))
        odata.Columns.Add("NewSumInsured", GetType(Decimal))
        odata.Columns.Add("SRCCPremiumToCust", GetType(Decimal)) ''untuk srccpremium yg lama
        odata.Columns.Add("SRCCPremiumToInsCo", GetType(Decimal)) ''untuk srccpremiuminsco yang lama
        odata.Columns.Add("TPLPremiumToCust", GetType(Decimal)) ''untuk tplpremi yang lama
        odata.Columns.Add("TPLPremiumToInsCo", GetType(Decimal)) ''untuk tplpremi insco yg lama        
        odata.Columns.Add("FloodPremiumToCust", GetType(Decimal)) ''untuk floodpremium yg lama
        odata.Columns.Add("FloodPremiumToInsco", GetType(Decimal)) ''untuk floodpremium insco yg lama
        odata.Columns.Add("ispaid", GetType(Boolean))
        odata.Columns.Add("DiscToCust", GetType(Decimal))
        odata.Columns.Add("OldPremiCust", GetType(Decimal)) ''untuk  refund main premi ke cust
        odata.Columns.Add("OldPremiInsco", GetType(Decimal)) ''untuk refund main premi ke insco
        odata.Columns.Add("OldSRCCInsco", GetType(Decimal)) 'untuk refund srcc ke insco
        odata.Columns.Add("OldFloodInsco", GetType(Decimal)) 'untuk refund flood ke insco
        odata.Columns.Add("OldTPLInsco", GetType(Decimal)) 'untuk refund TPL ke insco
        odata.Columns.Add("OldSRCCCust", GetType(Decimal)) 'untuk refund SRCC ke Cust
        odata.Columns.Add("OldFloodCust", GetType(Decimal)) 'untuk refund flood ke cust
        odata.Columns.Add("OldTPLCust", GetType(Decimal)) 'untuk refund TPL ke cust
        odata.Columns.Add("RefundDays", GetType(Decimal))
        odata.Columns.Add("PaidAmtByCust", GetType(Decimal))
        odata.Columns.Add("PaidAmtToInsCo", GetType(Decimal))
        odata.Columns.Add("PrevMainToCust", GetType(Decimal)) ''untuk main premium cust yang lama
        odata.Columns.Add("PrevMainToInsCo", GetType(Decimal)) ''untuk main premium insco yang lama
        odata.Columns.Add("ActiveYearNum", GetType(Integer))

        odata.Columns.Add("oldLoadingFeeToCust", GetType(Decimal))
        odata.Columns.Add("oldLoadingFeeToInsco", GetType(Decimal))
        odata.Columns.Add("NewLoadingFeeToInsco", GetType(Decimal))
        odata.Columns.Add("NewLoadingFeeToCustomer", GetType(Decimal))

        odata.Columns.Add("YearNumRate", GetType(Integer))

        If oData1.Rows.Count > 0 Then

            params(0) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 3)
            params(1) = New SqlParameter("@OldCoverageType", SqlDbType.VarChar, 3)
            params(2) = New SqlParameter("@YearNum", SqlDbType.Int)
            params(3) = New SqlParameter("@OldSRCC", SqlDbType.VarChar, 3)
            params(4) = New SqlParameter("@NewSRCC", SqlDbType.Bit)
            params(5) = New SqlParameter("@OldFlood", SqlDbType.VarChar, 3)
            params(6) = New SqlParameter("@NewFlood", SqlDbType.Bit)
            params(7) = New SqlParameter("@OldTPL", SqlDbType.Decimal)
            params(8) = New SqlParameter("@NewTPL", SqlDbType.Decimal)
            params(9) = New SqlParameter("@ApplicationType", SqlDbType.Char, 3)
            params(10) = New SqlParameter("@InsuranceType", SqlDbType.Char, 3)
            params(11) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(12) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(13) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
            params(14) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(15) = New SqlParameter("@NewSumInsured", SqlDbType.Decimal)
            params(16) = New SqlParameter("@Endorsmentdate", SqlDbType.DateTime)
            params(17) = New SqlParameter("@UsageID", SqlDbType.VarChar, 3)
            params(18) = New SqlParameter("@NewUsed", SqlDbType.VarChar, 3)
            params(19) = New SqlParameter("@NewTPLAmount", SqlDbType.Decimal)
            params(20) = New SqlParameter("@YearNumRate", SqlDbType.Int)

            For intloop = 0 To oData1.Rows.Count - 1
                params(0).Value = oData1.Rows(intloop).Item("CoverageType")
                params(1).Value = oData1.Rows(intloop).Item("OldCoverageType")
                params(2).Value = oData1.Rows(intloop).Item("Year")
                params(3).Value = oData1.Rows(intloop).Item("OldSRCC")
                params(4).Value = oData1.Rows(intloop).Item("NewSRCC")
                params(5).Value = oData1.Rows(intloop).Item("OldFlood")
                params(6).Value = oData1.Rows(intloop).Item("NewFlood")
                params(7).Value = oData1.Rows(intloop).Item("OldTPL")
                params(8).Value = oData1.Rows(intloop).Item("NewTPL")
                params(9).Value = oCustomClass.ApplicationType
                params(10).Value = oCustomClass.InsuranceType
                params(11).Value = oCustomClass.BranchId
                params(12).Value = oCustomClass.AppID
                params(13).Value = oCustomClass.AssetSeqNo
                params(14).Value = oCustomClass.InsSeqNo
                params(15).Value = oData1.Rows(intloop).Item("NewSumInsured")
                params(16).Value = oData1.Rows(intloop).Item("EndorsmentDate")
                params(17).Value = oCustomClass.AssetUsage
                params(18).Value = oCustomClass.UsedNew
                params(19).Value = oData1.Rows(intloop).Item("NewTPLAmount")
                params(20).Value = oData1.Rows(intloop).Item("YearNumRate")

                Try
                    reader = SqlHelper.ExecuteReader(oCustomClass.strConnection, CommandType.StoredProcedure, SP_INSENDORSMENT_INSUREDCOVERAGE, params)
                    If reader.Read Then
                        row = odata.NewRow
                        'data premi lama cust
                        row("PrevMainToCust") = CType(reader("PrevMainToCust"), Decimal) 'main premium yg lama
                        row("SRCCPremiumToCust") = CType(reader("PrevSRCCToCust"), Decimal) 'srcc premium yg lama
                        row("FloodPremiumToCust") = CType(reader("PrevFloodToCust"), Decimal) 'flood premium yg lama
                        row("TPLPremiumToCust") = CType(reader("PrevTPLToCust"), Decimal) 'tpl premium yg lama
                        row("DiscToCust") = CType(reader("DiscToCust"), Decimal)
                        row("PaidAmtByCust") = CType(reader("PaidAmtByCust"), Decimal)

                        'data premi lama insco
                        row("PrevMainToInsCo") = CType(reader("PrevMainToInsCo"), Decimal) 'main premium yg lama
                        row("SRCCPremiumToInsCo") = CType(reader("PrevSRCCToInsco"), Decimal) 'srcc premium yg lama
                        row("TPLPremiumToInsCo") = CType(reader("PrevTPLToInsco"), Decimal) 'tpl premium yg lama
                        row("FloodPremiumToInsco") = CType(reader("PrevFloodToInsco"), Decimal) 'flood premium yg lama
                        row("PaidAmtToInsCo") = CType(reader("PaidAmountToInsCo"), Decimal)

                        'data refund dan new premi untuk cust
                        row("Year") = CType(reader("Year"), String)
                        row("RefundDays") = CType(reader("RefundDays"), Decimal)
                        row("OldPremiCust") = CType(reader("RefundToCust"), Decimal)
                        row("OldSRCCCust") = CType(reader("RefundSRCCToCust"), Decimal)
                        row("OldFloodCust") = CType(reader("RefundFloodToCust"), Decimal)
                        row("OldTPLCust") = CType(reader("RefundTPLToCust"), Decimal)
                        row("NewSRCCCust") = CType(reader("NewSRCCCust"), Decimal)
                        row("NewFloodCust") = CType(reader("NewFloodCust"), Decimal)
                        row("NewTPLCust") = CType(reader("NewTPLCust"), Decimal)
                        row("NewTPLAmountCust") = CType(reader("NewTPLAmount"), Decimal)
                        row("NewPremiCust") = CType(reader("NewPremiCust"), Decimal)

                        'data refund dan new premi untuk insco
                        row("OldPremiInsco") = CType(reader("RefundToInsco"), Decimal)
                        row("OldSRCCInsco") = CType(reader("RefundSRCCToInsco"), Decimal)
                        row("OldFloodInsco") = CType(reader("RefundFloodToInsCo"), Decimal)
                        row("OldTPLInsco") = CType(reader("RefundTPLToInsCo"), Decimal)
                        row("NewPremiInsco") = CType(reader("NewPremiInsco"), Decimal)
                        row("NewSRCCInsco") = CType(reader("NewSRCCInsco"), Decimal)
                        row("NewFloodInsco") = CType(reader("NewFloodInsco"), Decimal)
                        row("NewTPLInsCo") = CType(reader("NewTPLInsCo"), Decimal)
                        row("NewTPLAmountInsCo") = CType(reader("NewTPLAmount"), Decimal)

                        'data pendukung lainnya
                        row("NewCoverage") = CType(reader("NewCoverage"), String)
                        row("OldCoverage") = CType(reader("OldCoverage"), String)
                        row("OldSRCC") = CType(reader("OldSRCC"), String)
                        row("NewSRCC") = CType(reader("NewSRCC"), Boolean)
                        row("OldFlood") = CType(reader("OldFlood"), String)
                        row("NewFlood") = CType(reader("NewFlood"), Boolean)
                        row("OldTPL") = CType(reader("OldTPL"), Decimal)
                        row("NewTPLAmount") = CType(reader("NewTPLAmount"), Decimal)                                                
                        row("AddPremiCust") = 0
                        row("AddPremiInsCo") = 0
                        row("NewSumInsured") = 0
                        row("ispaid") = CType(reader("ispaid"), Boolean)
                        row("ActiveYearNum") = CType(reader("ActiveYearnum"), Integer)

                        row("OldLoadingFeeToCust") = CType(reader("OldLoadingFeeToCust"), Decimal)
                        row("oldLoadingFeeToInsco") = CType(reader("oldLoadingFeeToInsco"), Decimal)
                        row("NewLoadingFeeToInsco") = CType(reader("NewLoadingFeeToInsco"), Decimal)
                        row("NewLoadingFeeToCustomer") = CType(reader("NewLoadingFeeToCustomer"), Decimal)
                        row("YearNumRate") = CType(reader("YearNumRate"), Integer)

                        odata.Rows.Add(row)
                    End If
                    reader.Close()

                Catch ex As Exception
                    Throw New Exception(ex.Message)
                Finally
                End Try
            Next
            oReturnValue.ListData = odata
            Return oReturnValue
        End If
    End Function


    'fungsi ini untuk memindahkan semua data ke insuranceassethistory,insuranceassetdetailhistory dan insert data ke tabel mailtransaction
    Public Function SaveData(ByVal customClass As Parameter.InsEndorsment, ByVal result As DataTable) As Parameter.InsEndorsment
        Dim ocustomclass As New Parameter.InsEndorsment
        Dim objcon As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction
        Dim params() As SqlParameter = New SqlParameter(6) {}

        If objcon.State = ConnectionState.Closed Then objcon.Open()

        objtrans = objcon.BeginTransaction

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customClass.AppID
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(2).Value = customClass.AssetSeqNo
        params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(3).Value = customClass.InsSeqNo
        params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(4).Value = customClass.BusinessDate
        params(5) = New SqlParameter("@Loginid", SqlDbType.VarChar, 50)
        params(5).Value = customClass.LoginId
        params(6) = New SqlParameter("@EndorsmentLetter", SqlDbType.VarChar, 25)
        params(6).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spInsEndorsmentSaveDetail", params)
            customClass.EndorsmentLetter = CType(params(6).Value, String)

            UpdateTable(customClass, result, objtrans)
            GetDiscAllocation(customClass, objtrans)
            UpdateTableSecond(customClass, result, objtrans)
            UpdateAsset(customClass, objtrans)
            JournalMaking(customClass, objtrans)

            objtrans.Commit()
            Return customClass
        Catch ex As Exception
            objtrans.Rollback()
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsEndorsment.vb", "SaveData", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try

    End Function

    'sub ini untuk update data ke tabel insuranceassetdetail
    Private Sub UpdateTable(ByVal ocustomClass As Parameter.InsEndorsment, ByVal result As DataTable, ByVal ptrans As SqlTransaction)
        Dim oReturnValue As New Parameter.InsEndorsment
        Dim odata As New DataTable
        Dim row As datarow
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(34) {}
        Dim intloop As Integer = 0
        Dim datarow As datarow

        If result.Rows.Count > 0 Then
            params(0) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 3)
            params(1) = New SqlParameter("@OldCoverageType", SqlDbType.VarChar, 3)
            params(2) = New SqlParameter("@YearNum", SqlDbType.Int)
            params(3) = New SqlParameter("@OldSRCC", SqlDbType.VarChar, 3)
            params(4) = New SqlParameter("@NewSRCC", SqlDbType.Bit)
            params(5) = New SqlParameter("@OldFlood", SqlDbType.VarChar, 3)
            params(6) = New SqlParameter("@NewFlood", SqlDbType.Bit)
            params(7) = New SqlParameter("@OldTPL", SqlDbType.Decimal)
            params(8) = New SqlParameter("@NewTPL", SqlDbType.Decimal)
            params(9) = New SqlParameter("@ApplicationType", SqlDbType.Char, 3)
            params(10) = New SqlParameter("@InsuranceType", SqlDbType.Char, 3)
            params(11) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(12) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(13) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
            params(14) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(15) = New SqlParameter("@NewSumInsured", SqlDbType.Decimal)
            params(16) = New SqlParameter("@UsageID", SqlDbType.VarChar, 3)
            params(17) = New SqlParameter("@NewUsed", SqlDbType.VarChar, 3)
            params(18) = New SqlParameter("@Discount", SqlDbType.Decimal)
            params(19) = New SqlParameter("@RefundToCust", SqlDbType.Decimal)
            params(20) = New SqlParameter("@AdditionalCust", SqlDbType.Decimal)
            params(21) = New SqlParameter("@SRCCCust", SqlDbType.Decimal)
            params(22) = New SqlParameter("@TPLPremiumCust", SqlDbType.Decimal)
            params(23) = New SqlParameter("@FloodCust", SqlDbType.Decimal)
            params(24) = New SqlParameter("@SRCCInsco", SqlDbType.Decimal)
            params(25) = New SqlParameter("@TPLPremiumInsco", SqlDbType.Decimal)
            params(26) = New SqlParameter("@FloodInsco", SqlDbType.Decimal)
            params(27) = New SqlParameter("@TPLAmountCust", SqlDbType.Decimal)
            params(28) = New SqlParameter("@TPLAmountInsco", SqlDbType.Decimal)    
            params(29) = New SqlParameter("@RefundToInsCo", SqlDbType.Decimal)
            params(30) = New SqlParameter("@AdditionalInsCo", SqlDbType.Decimal)
            params(31) = New SqlParameter("@NewMainPremiumToCust", SqlDbType.Decimal)
            params(32) = New SqlParameter("@NewMainPremiumToInsco", SqlDbType.Decimal)
            params(33) = New SqlParameter("@NewLoadingFeeToInsco", SqlDbType.Decimal)
            params(34) = New SqlParameter("@YearNumRate", SqlDbType.Int)

            For intloop = 0 To result.Rows.Count - 1
                params(0).Value = result.Rows(intloop).Item("NewCoverage")
                params(1).Value = result.Rows(intloop).Item("OldCoverage")
                params(2).Value = result.Rows(intloop).Item("Year")
                params(3).Value = result.Rows(intloop).Item("OldSRCC")
                params(4).Value = result.Rows(intloop).Item("NewSRCC")
                params(5).Value = result.Rows(intloop).Item("OldFlood")
                params(6).Value = result.Rows(intloop).Item("NewFlood")
                params(7).Value = result.Rows(intloop).Item("OldTPL")
                params(8).Value = result.Rows(intloop).Item("NewTPLAmount")
                params(9).Value = ocustomClass.ApplicationType
                params(10).Value = ocustomClass.InsuranceType
                params(11).Value = ocustomClass.BranchId
                params(12).Value = ocustomClass.AppID
                params(13).Value = ocustomClass.AssetSeqNo
                params(14).Value = ocustomClass.InsSeqNo
                params(15).Value = result.Rows(intloop).Item("NewSumInsured")
                params(16).Value = ocustomClass.AssetUsage
                params(17).Value = ocustomClass.UsedNew
                params(18).Value = ocustomClass.Discount
                params(19).Value = result.Rows(intloop).Item("OldPremiCust")
                params(20).Value = result.Rows(intloop).Item("AddPremiCust")
                params(21).Value = result.Rows(intloop).Item("NewSRCCCust")
                params(22).Value = result.Rows(intloop).Item("NewTPLCust")
                params(23).Value = result.Rows(intloop).Item("NewFloodCust")
                params(24).Value = result.Rows(intloop).Item("NewSRCCInsco")
                params(25).Value = result.Rows(intloop).Item("NewTPLInsCo")
                params(26).Value = result.Rows(intloop).Item("NewFloodInsco")
                params(27).Value = result.Rows(intloop).Item("NewTPLAmount")
                params(28).Value = result.Rows(intloop).Item("NewTPLAmount")            
                params(29).Value = result.Rows(intloop).Item("OldPremiInsco")
                params(30).Value = result.Rows(intloop).Item("AddPremiInsCo")
                params(31).Value = result.Rows(intloop).Item("NewPremiCust")
                params(32).Value = result.Rows(intloop).Item("NewPremiInsco")
                params(33).Value = result.Rows(intloop).Item("NewLoadingFeeToInsco")
                params(34).Value = result.Rows(intloop).Item("YearNumRate")

                SqlHelper.ExecuteNonQuery(ptrans, CommandType.StoredProcedure, "spInsEndorsmentUpdate", params)

            Next
        End If
    End Sub
    'sub ini untuk update data ke tabel insuranceassetdetail
    Private Sub UpdateTableSecond(ByVal ocustomClass As Parameter.InsEndorsment, ByVal result As DataTable, ByVal ptrans As SqlTransaction)
        Dim oReturnValue As New Parameter.InsEndorsment
        Dim odata As New DataTable
        Dim row As datarow
        Dim reader As SqlDataReader
        Dim params() As SqlParameter = New SqlParameter(32) {}
        Dim intloop As Integer = 0
        Dim datarow As datarow

        If result.Rows.Count > 0 Then
            params(0) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 3)            
            params(1) = New SqlParameter("@OldCoverageType", SqlDbType.VarChar, 3)            
            params(2) = New SqlParameter("@YearNum", SqlDbType.Int)
            params(3) = New SqlParameter("@OldSRCC", SqlDbType.VarChar, 3)
            params(4) = New SqlParameter("@NewSRCC", SqlDbType.Bit)
            params(5) = New SqlParameter("@OldFlood", SqlDbType.VarChar, 3)
            params(6) = New SqlParameter("@NewFlood", SqlDbType.Bit)
            params(7) = New SqlParameter("@OldTPL", SqlDbType.Decimal)
            params(8) = New SqlParameter("@NewTPL", SqlDbType.Decimal)
            params(9) = New SqlParameter("@ApplicationType", SqlDbType.Char, 3)
            params(10) = New SqlParameter("@InsuranceType", SqlDbType.Char, 3)
            params(11) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(12) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(13) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
            params(14) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
            params(15) = New SqlParameter("@NewSumInsured", SqlDbType.Decimal)            
            params(16) = New SqlParameter("@UsageID", SqlDbType.VarChar, 3)
            params(17) = New SqlParameter("@NewUsed", SqlDbType.VarChar, 3)
            params(18) = New SqlParameter("@Discount", SqlDbType.Decimal)
            params(19) = New SqlParameter("@RefundToCust", SqlDbType.Decimal)
            params(20) = New SqlParameter("@AdditionalCust", SqlDbType.Decimal)
            params(21) = New SqlParameter("@SRCCCust", SqlDbType.Decimal)
            params(22) = New SqlParameter("@TPLPremiumCust", SqlDbType.Decimal)
            params(23) = New SqlParameter("@FloodCust", SqlDbType.Decimal)
            params(24) = New SqlParameter("@SRCCInsco", SqlDbType.Decimal)
            params(25) = New SqlParameter("@TPLPremiumInsco", SqlDbType.Decimal)
            params(26) = New SqlParameter("@FloodInsco", SqlDbType.Decimal)
            params(27) = New SqlParameter("@TPLAmountCust", SqlDbType.Decimal)
            params(28) = New SqlParameter("@TPLAmountInsco", SqlDbType.Decimal)            
            params(29) = New SqlParameter("@RefundToInsCo", SqlDbType.Decimal)
            params(30) = New SqlParameter("@AdditionalInsCo", SqlDbType.Decimal)
            params(31) = New SqlParameter("@NewLoadingFeeToInsco", SqlDbType.Decimal)
            params(32) = New SqlParameter("@YearNumRate", SqlDbType.Int)

            For intloop = 0 To result.Rows.Count - 1
                params(0).Value = result.Rows(intloop).Item("NewCoverage")
                params(1).Value = result.Rows(intloop).Item("OldCoverage")
                params(2).Value = result.Rows(intloop).Item("Year")
                params(3).Value = result.Rows(intloop).Item("OldSRCC")
                params(4).Value = result.Rows(intloop).Item("NewSRCC")
                params(5).Value = result.Rows(intloop).Item("OldFlood")
                params(6).Value = result.Rows(intloop).Item("NewFlood")
                params(7).Value = result.Rows(intloop).Item("OldTPL")
                params(8).Value = result.Rows(intloop).Item("NewTPLAmount")
                params(9).Value = ocustomClass.ApplicationType
                params(10).Value = ocustomClass.InsuranceType
                params(11).Value = ocustomClass.BranchId
                params(12).Value = ocustomClass.AppID
                params(13).Value = ocustomClass.AssetSeqNo
                params(14).Value = ocustomClass.InsSeqNo
                params(15).Value = result.Rows(intloop).Item("NewSumInsured")                
                params(16).Value = ocustomClass.AssetUsage
                params(17).Value = ocustomClass.UsedNew
                params(18).Value = ocustomClass.Discount
                params(19).Value = result.Rows(intloop).Item("OldPremiCust")
                params(20).Value = result.Rows(intloop).Item("AddPremiCust")
                params(21).Value = result.Rows(intloop).Item("NewSRCCCust")
                params(22).Value = result.Rows(intloop).Item("NewTPLCust")
                params(23).Value = result.Rows(intloop).Item("NewFloodCust")
                params(24).Value = result.Rows(intloop).Item("NewSRCCInsco")
                params(25).Value = result.Rows(intloop).Item("NewTPLInsCo")
                params(26).Value = result.Rows(intloop).Item("NewFloodInsco")
                params(27).Value = result.Rows(intloop).Item("NewTPLAmount")
                params(28).Value = result.Rows(intloop).Item("NewTPLAmount")                
                params(29).Value = result.Rows(intloop).Item("OldPremiInsco")
                params(30).Value = result.Rows(intloop).Item("AddPremiInsCo")
                params(31).Value = result.Rows(intloop).Item("NewLoadingFeeToInsco")
                params(32).Value = result.Rows(intloop).Item("YearNumRate")

                SqlHelper.ExecuteNonQuery(ptrans, CommandType.StoredProcedure, "spInsEndorsmentUpdateSecond", params)
            Next
        End If

    End Sub
    'sub ini untuk melakukan alokasi disc cust
    Private Sub GetDiscAllocation(ByVal ocustomClass As Parameter.InsEndorsment, ByVal ptrans As SqlTransaction)
        Dim oReturnValue As New Parameter.InsEndorsment


        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@branchid", SqlDbType.Char, 3)
        params(0).Value = ocustomClass.BranchId
        params(1) = New SqlParameter("@applicationid", SqlDbType.Char, 20)
        params(1).Value = ocustomClass.AppID
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(2).Value = ocustomClass.AssetSeqNo
        params(3) = New SqlParameter("@InsSequenceNo", SqlDbType.Int)
        params(3).Value = ocustomClass.InsSeqNo
        params(4) = New SqlParameter("@AmountAllocation", SqlDbType.Decimal)
        params(4).Value = ocustomClass.Discount
        params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(5).Value = ocustomClass.BusinessDate

        SqlHelper.ExecuteNonQuery(ptrans, CommandType.StoredProcedure, ocustomClass.spName, params)

    End Sub
    'sub ini untuk melakukan update data ke tabel insuranceasset
    Private Sub UpdateAsset(ByVal ocustomclass As Parameter.InsEndorsment, ByVal ptrans As SqlTransaction)
        Dim oReturnValue As New Parameter.InsEndorsment
        Dim params(11) As SqlParameter

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = ocustomclass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = ocustomclass.AppID
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(2).Value = ocustomclass.AssetSeqNo
        params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(3).Value = ocustomclass.InsSeqNo
        params(4) = New SqlParameter("@EndorsmentLetter", SqlDbType.VarChar, 25)
        params(4).Value = ocustomclass.EndorsmentLetter
        params(5) = New SqlParameter("@AdditionalCust", SqlDbType.Decimal)
        params(5).Value = ocustomclass.AdditionalCust
        params(6) = New SqlParameter("@AdditionalInsCo", SqlDbType.Decimal)
        params(6).Value = ocustomclass.AdditionalInsco
        params(7) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(7).Value = ocustomclass.BusinessDate
        params(8) = New SqlParameter("@AccNotes", SqlDbType.VarChar, 500)
        params(8).Value = ocustomclass.AccNotes
        params(9) = New SqlParameter("@InsNotes", SqlDbType.VarChar, 500)
        params(9).Value = ocustomclass.InsNotes
        params(10) = New SqlParameter("@RefundToCust", SqlDbType.Decimal)
        params(10).Value = ocustomclass.RefundToCust
        params(11) = New SqlParameter("@RefundFromInsCo", SqlDbType.Decimal)
        params(11).Value = ocustomclass.RefundToInsCo

        SqlHelper.ExecuteNonQuery(ptrans, CommandType.StoredProcedure, "spInsEndorsmentUpdateAsset", params)
    End Sub

    'sub ini untuk melakukan proses createjournal,insert ke tabel accountpayable
    Private Sub JournalMaking(ByVal ocustomclass As Parameter.InsEndorsment, ByVal ptrans As SqlTransaction)
        Dim oReturnValue As New Parameter.InsEndorsment
        Dim params(5) As SqlParameter

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = ocustomclass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = ocustomclass.AppID
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(2).Value = ocustomclass.AssetSeqNo
        params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(3).Value = ocustomclass.InsSeqNo
        params(4) = New SqlParameter("@CoyId", SqlDbType.VarChar, 3)
        params(4).Value = ocustomclass.CoyID
        params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(5).Value = ocustomclass.BusinessDate


        SqlHelper.ExecuteNonQuery(ptrans, CommandType.StoredProcedure, "spInsEndorsmentJournalMaking", params)        
    End Sub

    'fungsi ini untuk menampilkan perhitungan additional jika diklik button print
    Public Function PrintForm(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
        Dim oReturnValue As New Parameter.InsEndorsment
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customClass.AppID
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(2).Value = customClass.AssetSeqNo
        params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(3).Value = customClass.InsSeqNo


        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spInsEndorsmentPrintForm", params).Tables(0)

            Return oReturnValue
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsEndorsment.vb", "PrintForm", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try
    End Function
    'fungsi ini untuk menampilkan data detil customer jika melakukan endorsment setelah save data dilakukan dan berhasil
    Public Function ViewData(ByVal customClass As Parameter.InsEndorsment) As Parameter.InsEndorsment
        Dim oReturnValue As New Parameter.InsEndorsment
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customClass.AppID
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(2).Value = customClass.AssetSeqNo
        params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(3).Value = customClass.InsSeqNo


        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spInsEndorsmentReportRequest", params).Tables(0)
            oReturnValue.DataDetil = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spInsEndorsmentViewEndorsmentData", params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsEndorsment.vb", "ViewData", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
