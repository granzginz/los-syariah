
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.General

#End Region

Public Class PolicyDetail
#Region "Private Constanta"

    Private Const SP_GETPOLICYDETAIL As String = "spInsViewPolicyDetail"
    Private Const SP_GET_ENDORSMENT_HEADER As String = "spGetOldInsuranceAssetHeader"

#End Region
#Region "GetPopUpEndorsmentHistory"

    Public Function GetPopUpEndorsmentHistory(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@WhereCond", SqlDbType.Char, 1000)
        params(0).Value = customClass.WhereCond

        Try
            Dim oReturnValue As New Parameter.InsCoAllocationDetailList
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, SP_GET_ENDORSMENT_HEADER, params)
            If reader.Read Then
                oReturnValue.BranchId = reader("BranchId").ToString.Trim
                oReturnValue.ApplicationID = reader("ApplicationID").ToString.Trim
                oReturnValue.BDEndorsDocNo = reader("BDEndorsDocNo").ToString.Trim
                oReturnValue.BDEndorsPolicyNo = reader("BDEndorsPolicyNo").ToString.Trim
                oReturnValue.BDEndorsDate = CType(reader("BDEndorsDate").ToString, Date)
                oReturnValue.PolicyNumber = reader("PolicyNumber").ToString.Trim

            End If

            reader.Close()
            Return oReturnValue

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("PolicyDetail.vb", "GetPopUpEndorsmentHistory", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try


    End Function

#End Region
#Region "GetPolicyDetail"


    Public Function GetPolicyDetail(ByVal customClass As Parameter.InsCoAllocationDetailList) As Parameter.InsCoAllocationDetailList
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@WhereCond", SqlDbType.Char, 1000)
        params(0).Value = customClass.WhereCond

        Try
            Dim oReturnValue As New Parameter.InsCoAllocationDetailList
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, SP_GETPOLICYDETAIL, params)
            If reader.Read Then
                oReturnValue.BranchId = reader("BranchId").ToString.Trim
                oReturnValue.ApplicationID = reader("ApplicationID").ToString.Trim
                oReturnValue.AgreementNo = reader("AgreementNo").ToString.Trim
                oReturnValue.CustomerID = reader("CustomerID").ToString.Trim
                oReturnValue.CustomerName = reader("CustomerName").ToString.Trim
                oReturnValue.AssetMasterDescr = reader("AssetMasterDescr").ToString.Trim
                oReturnValue.InsuranceComBranchName = reader("InsuranceComBranchName").ToString.Trim
                oReturnValue.InsuranceTypeDescr = reader("InsuranceTypeDescr").ToString.Trim
                oReturnValue.AccNotes = reader("AccNotes").ToString.Trim
                oReturnValue.InsNotes = reader("InsNotes").ToString.Trim
                oReturnValue.AssetUsageDescr = reader("AssetUsageDescr").ToString.Trim
                oReturnValue.UsedNew = reader("UsedNew").ToString.Trim
                oReturnValue.SPPANo = reader("SPPANo").ToString.Trim
                oReturnValue.SPPADate = CType(reader("SPPADate").ToString, Date)
                oReturnValue.PolicyNumber = reader("PolicyNumber").ToString.Trim
                oReturnValue.PolicyReceiveDate = CType(reader("PolicyReceiveDate").ToString, Date)
                oReturnValue.StartDate = CType(reader("StartDate").ToString, Date)
                oReturnValue.EndDate = CType(reader("EndDate").ToString, Date)
                oReturnValue.SumInsured = CType(reader("SumInsured").ToString, Double)
                oReturnValue.PremiumAmountByCust = CType(reader("PremiumAmountByCust").ToString, Double)
                oReturnValue.PaidAmountByCust = CType(reader("PaidAmountByCust").ToString, Double)
                oReturnValue.PremiumAmountToInsco = CType(reader("PremiumAmountToInsco").ToString, Double)
                oReturnValue.PaidAmountToInsco = CType(reader("PaidAmountToInsco").ToString, Double)

            End If

            reader.Close()
            Return oReturnValue

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("PolicyDetail.vb", "GetPolicyDetail", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            'Throw New Exception(ex.Message)

        End Try


    End Function

#End Region
End Class
