


#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region


Public Class InsCoAllocation : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    Private Const SP_SELECT_INSURANCE_COMPANY_SELECTION As String = "spInsCompanySelectionSelect"

#End Region

#Region "getInsuranceCompanySelection"

    Public Function getInsuranceCompanySelection(ByVal customClass As Parameter.InsCoAllocation) As Parameter.InsCoAllocation

        Dim oReturnValue As New Parameter.InsCoAllocation

        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, SP_SELECT_INSURANCE_COMPANY_SELECTION, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception

            Dim err As New MaxiloanExceptions
            err.WriteLog("InsCoAllication.aspx", "getInsuranceCompanySelection", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            'Throw New Exception(ex.Message)
        End Try

    End Function

#End Region


End Class
