
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.General
Imports Maxiloan.Exceptions

#End Region
Public Class InsPolicyNumber : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    Private Const GET_INSURANCE_POLICY_NUMBER As String = "spEditPolicyNumberPaging"
    Private Const INSURANCE_POLICY_NUMBER_SAVE_EDIT As String = "spInsPolicyNumberEdit"
    Private Const LIST_ADD_BANKERS_CLAUSE_D As String = "spBankersClauseDSaveAdd"
    Private Const LIST_ADD_BANKERS_CLAUSE_H As String = "spBankersClauseHSaveAdd"
    Private Const LIST_ADD_BANKERS_CLAUSE_PAGING As String = "spBankersClausePaging"
    Private Const LIST_INSCOMPANY_BRANCH As String = "spInsuranceCoyBranchByID"
    Private Const LIST_INSCOMPANY_BRANCH_VIEW_PRINT As String = "spBankersClauseViewPrint"
    Private Const LIST_INSCOMPANY_BRANCH_PRINT As String = "spBankersClausePrint"

#End Region

#Region "GetInsPolicyNumber"
    Public Function GetInsPolicyNumber(ByVal customClass As Parameter.InsPolicyNumber) As Parameter.InsPolicyNumber
        Dim oReturnValue As New Parameter.InsPolicyNumber

        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, GET_INSURANCE_POLICY_NUMBER, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.GetPremiumToCustomer")
        End Try

    End Function
#End Region

#Region "InsurancePolicyNumberSaveEdit"
    Public Sub InsurancePolicyNumberSaveEdit(ByVal customClass As Parameter.InsPolicyNumber)
        Dim objconnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction
        Dim params(8) As SqlParameter
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(1).Value = customClass.ApplicationId
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt)
        params(2).Value = customClass.AssetSeqId
        params(3) = New SqlParameter("@InsSequenceNo", SqlDbType.SmallInt)
        params(3).Value = customClass.InsSeqNo
        params(4) = New SqlParameter("@PolicyNumber", SqlDbType.VarChar, 25)
        params(4).Value = customClass.PolicyNumber
        params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(5).Value = customClass.BusinessDate
        params(6) = New SqlParameter("@LoginId", SqlDbType.VarChar, 20)
        params(6).Value = customClass.LoginId
        params(7) = New SqlParameter("@OldPolicyNumber", SqlDbType.VarChar, 25)
        params(7).Value = customClass.OldPolicyNumber
        params(8) = New SqlParameter("@YearNum", SqlDbType.VarChar, 3)
        params(8).Value = customClass.Yearnum

        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objtrans = objconnection.BeginTransaction

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, INSURANCE_POLICY_NUMBER_SAVE_EDIT, params)
            objtrans.Commit()
        Catch ex As Exception
            objtrans.Rollback()
            Throw New Exception(ex.Message)
            'Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.PremiumToCustomerRateCustSaveEdit")
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try

    End Sub
#End Region

#Region "BankersClause"
    Public Function BankersClauseDSaveAdd(ByVal customClass As Parameter.BankersClause) As String
        Dim ErrMessage As String = ""
        Dim objtrans As SqlTransaction
        Dim params(4) As SqlParameter

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(1).Value = customClass.ApplicationID
        params(2) = New SqlParameter("@NoClause", SqlDbType.VarChar, 50)
        params(2).Value = customClass.NoClause
        params(3) = New SqlParameter("@InsuranceCoyBranchID", SqlDbType.Char, 10)
        params(3).Value = customClass.InsuranceComBranchID
        params(4) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(4).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_ADD_BANKERS_CLAUSE_D, params)
            ErrMessage = CType(params(4).Value, String)
            Return ErrMessage
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function BankersClauseHSaveAdd(ByVal customClass As Parameter.BankersClause) As String
        Dim ErrMessage As String = ""
        Dim objtrans As SqlTransaction
        Dim params(5) As SqlParameter

        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@TglClause", SqlDbType.DateTime)
        params(1).Value = customClass.TglClause
        params(2) = New SqlParameter("@NoClause", SqlDbType.VarChar, 50)
        params(2).Value = customClass.NoClause
        params(3) = New SqlParameter("@InsuranceCoyBranchID", SqlDbType.Char, 10)
        params(3).Value = customClass.InsuranceComBranchID
        params(4) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@ContactPersonName", SqlDbType.VarChar, 50)
        params(5).Value = customClass.ContactPersonName

        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_ADD_BANKERS_CLAUSE_H, params)
            ErrMessage = CType(params(4).Value, String)
            Return ErrMessage
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetBankersClause(ByVal customClass As Parameter.BankersClause) As Parameter.BankersClause
        Dim oReturnValue As New Parameter.BankersClause

        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_ADD_BANKERS_CLAUSE_PAGING, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    Public Function GetInsCompanyBranchByID(ByVal customClass As Parameter.BankersClause) As Parameter.BankersClause
        Dim oReturnValue As New Parameter.BankersClause
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@InsuranceCoyBranchID", SqlDbType.Char, 10)
        params(0).Value = customClass.InsuranceComBranchID
        params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(1).Value = customClass.BranchId

        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_INSCOMPANY_BRANCH, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetBankersClauseViewPrint(ByVal customClass As Parameter.BankersClause) As Parameter.BankersClause
        Dim oReturnValue As New Parameter.BankersClause

        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_INSCOMPANY_BRANCH_VIEW_PRINT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    Public Function GetBankersClausePrint(ByVal customClass As Parameter.BankersClause) As DataSet
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@NoClause", SqlDbType.VarChar, 50)
        params(0).Value = customClass.NoClause
        params(1) = New SqlParameter("@InsuranceCoyBranchID", SqlDbType.Char, 10)
        params(1).Value = customClass.InsuranceComBranchID

        Try
            Return SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_INSCOMPANY_BRANCH_PRINT, params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region
End Class
