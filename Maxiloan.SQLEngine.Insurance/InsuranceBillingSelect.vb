
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.General

#End Region
Public Class InsuranceBillingSelect
#Region "Private Constanta"

    Private Const SP_INSURANCE_BILLING_SELECT As String = "spInsuranceBillingSave"
    Private Const SP_INSURANCE_BILLING_SELECT_DETAIL As String = "spInsuranceBillingDetailSave"
#End Region
#Region "CheckInvoiceNo"
    Public Function CheckInvoiceNo(ByVal customClass As Parameter.InsCoAllocationDetailList) As String
        Try
            Dim oReturnValue As Parameter.InsCoAllocation
            Dim params(2) As SqlParameter
            params(0) = New SqlParameter("@InvoiceNo", SqlDbType.Char, 20)
            params(0).Value = customClass.InvoiceNo
            params(1) = New SqlParameter("@MaskAssBranchID", SqlDbType.Char, 10)
            params(1).Value = customClass.InsuranceComBranchID
            params(2) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(2).Value = customClass.BranchId
            Dim hasil As String
            hasil = CStr(SqlHelper.ExecuteScalar(customClass.strConnection, CommandType.StoredProcedure, customClass.spName, params))
            Return hasil
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("", "", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.StackTrace)
        End Try
    End Function
#End Region
#Region "SaveInsuranceBillingSelectHeader"
    Public Function SaveInsuranceBillingSelectHeader(ByVal customClass As Parameter.InsCoAllocationDetailList, ByRef oSaveTransaction As SqlTransaction) As Boolean

        Try
            Dim params(9) As SqlParameter
            params(0) = New SqlParameter("@MaskAssBranchID", SqlDbType.Char, 20)
            params(0).Value = customClass.InsuranceComBranchID
            params(1) = New SqlParameter("@CoyID", SqlDbType.Char, 10)
            params(1).Value = customClass.CoyID
            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(2).Value = customClass.BranchId
            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = customClass.BusinessDate
            params(4) = New SqlParameter("@InvoiceNo", SqlDbType.Char, 30)
            params(4).Value = customClass.InvoiceNo
            params(5) = New SqlParameter("@InvoiceDate", SqlDbType.DateTime)
            params(5).Value = customClass.InvoiceDate
            params(6) = New SqlParameter("@TotalInvoiceAmount", SqlDbType.Decimal)
            params(6).Value = customClass.TotalInvoiceAmount
            params(7) = New SqlParameter("@TotalAdminFee", SqlDbType.Decimal)
            params(7).Value = customClass.TotalAdminFee
            params(8) = New SqlParameter("@TotalStampDutyFee", SqlDbType.Decimal)
            params(8).Value = customClass.TotalStampDutyFee
            params(9) = New SqlParameter("@LoginID", SqlDbType.Char, 50)
            params(9).Value = customClass.LoginId

            SqlHelper.ExecuteNonQuery(oSaveTransaction, CommandType.StoredProcedure, SP_INSURANCE_BILLING_SELECT, params)
            Return True
        Catch ex As Exception
            Return False
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsuranceBillingSelect.vb", "SaveInsuranceBillingSelectHeader", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
            'Finally
            '    If objConnection.State = ConnectionState.Open Then objConnection.Close()
            '    objConnection.Dispose()
        End Try

    End Function
#End Region
#Region "SaveInsuranceBillingSelectDetail"
    Public Sub SaveInsuranceBillingSelectDetail(ByVal customClass As Parameter.InsCoAllocationDetailList)
        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction
        Dim intloop As Integer
        Dim oReturnValue As New Parameter.InsCoAllocationDetailList
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            If Not SaveInsuranceBillingSelectHeader(customClass, objtrans) Then
                objtrans.Rollback()
                If objConnection.State = ConnectionState.Open Then objConnection.Close()
                objConnection.Dispose()
                Exit Sub
            End If

            Dim params(13) As SqlParameter
            params(0) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
            params(1) = New SqlParameter("@PolicyNumber", SqlDbType.Char, 20)
            params(2) = New SqlParameter("@PremiumEndorsmentToInsco", SqlDbType.Decimal)
            params(3) = New SqlParameter("@PremiumToBePaid", SqlDbType.Decimal)
            params(4) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(5) = New SqlParameter("@CoyId", SqlDbType.Char, 10)
            params(6) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(7) = New SqlParameter("@PartialAdminFee", SqlDbType.Decimal)
            params(8) = New SqlParameter("@PartialStampFee", SqlDbType.Decimal)
            params(9) = New SqlParameter("@InvoiceNo", SqlDbType.Char, 30)
            params(10) = New SqlParameter("@InvoiceDate", SqlDbType.DateTime)
            params(11) = New SqlParameter("@DueDateEntry", SqlDbType.DateTime)
            params(12) = New SqlParameter("@MaskAssBranchID", SqlDbType.Char, 10)
            params(13) = New SqlParameter("@AccountPayableNo", SqlDbType.Char, 20)
            For intloop = 0 To customClass.ListData.Rows.Count - 1
                With customClass.ListData.Rows(intloop)
                    params(0).Value = .Item("AgreementNo")
                    params(1).Value = .Item("PolicyNumber")
                    params(2).Value = .Item("PremiumEndorsmentToInsco")
                    params(3).Value = .Item("PremiumToBePaid")
                    params(4).Value = .Item("BranchId")
                    params(5).Value = .Item("CoyID")
                    params(6).Value = .Item("BusinessDate")
                    params(7).Value = .Item("AdminFee")
                    params(8).Value = .Item("MeteraiFee")
                    params(9).Value = .Item("InvoiceNo")
                    params(10).Value = .Item("InvoiceDate")
                    params(11).Value = .Item("DueDate")
                    params(12).Value = .Item("InsuranceComBranchID")
                    params(13).Value = .Item("AccountPayableNo")
                End With
                SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SP_INSURANCE_BILLING_SELECT_DETAIL, params)
            Next
            objtrans.Commit()

        Catch ex As Exception
            objtrans.Rollback()
            'If objConnection.State = ConnectionState.Open Then objConnection.Close()
            'objConnection.Dispose()

            Throw New Exception("Error On DataAccess.Insurance.InsuranceBillingSelect" + ex.Message.ToString)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Sub
#End Region
#Region "SAVE"
    Public Sub SaveInsuranceBillingSave(ByVal customClass As Parameter.InsCoAllocationDetailList, ByVal customClassList As List(Of Parameter.InsCoAllocationDetailList))

        Try
            Dim params(7) As SqlParameter
            Dim params1(11) As SqlParameter
            Dim InvoiceNoOut As String = ""

            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@InsuranceCoyId", SqlDbType.Char, 10)
            params(1).Value = customClass.MaskAssID
            params(2) = New SqlParameter("@InsuranceCoyBranchID", SqlDbType.Char, 10)
            params(2).Value = customClass.InsuranceComBranchID
            params(3) = New SqlParameter("@InvoiceNo", SqlDbType.Char, 20)
            params(3).Value = customClass.InvoiceNo
            params(4) = New SqlParameter("@InvoiceDate", SqlDbType.DateTime)
            params(4).Value = customClass.InvoiceDate
            params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(5).Value = customClass.BusinessDate
            params(6) = New SqlParameter("@TotalTagihanAll", SqlDbType.Decimal)
            params(6).Value = customClass.TotalTagihanAll
            params(7) = New SqlParameter("@InvoiceNoOut", SqlDbType.Char, 20)
            params(7).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, "spTerimaTagihanAsuransiSaveH", params)

            InvoiceNoOut = params(7).Value

            For index = 0 To customClassList.Count - 1
                Dim _custom = New Parameter.InsCoAllocationDetailList
                _custom = customClassList(index)

                params1(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
                params1(0).Value = customClass.BranchId
                params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                params1(1).Value = _custom.ApplicationID
                params1(2) = New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt)
                params1(2).Value = _custom.AssetSeqNo
                params1(3) = New SqlParameter("@InsSequenceNo", SqlDbType.SmallInt)
                params1(3).Value = _custom.InsSequenceNo
                params1(4) = New SqlParameter("@InsuranceCoyId", SqlDbType.Char, 10)
                params1(4).Value = customClass.MaskAssID
                params1(5) = New SqlParameter("@InsuranceCoyBranchID", SqlDbType.Char, 10)
                params1(5).Value = customClass.InsuranceComBranchID
                params1(6) = New SqlParameter("@InvoiceNo", SqlDbType.Char, 20)
                params1(6).Value = InvoiceNoOut
                params1(7) = New SqlParameter("@AccountPayableNo", SqlDbType.Char, 20)
                params1(7).Value = _custom.AccountPayableNo
                params1(8) = New SqlParameter("@InvoiceDate", SqlDbType.DateTime)
                params1(8).Value = customClass.InvoiceDate
                params1(9) = New SqlParameter("@DueDateEntry", SqlDbType.DateTime)
                params1(9).Value = customClass.DueDateEntry
                params1(10) = New SqlParameter("@TotalTagihanItem", SqlDbType.Decimal)
                params1(10).Value = _custom.TotalTagihanItem
                params1(11) = New SqlParameter("@PolicyNo", SqlDbType.VarChar, 25)
                params1(11).Value = _custom.PolicyNumber

                SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, "spTerimaTagihanAsuransiSaveD", params1)
            Next
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsuranceBillingSelect.vb", "SaveInsuranceBillingSave", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try

    End Sub
#End Region
End Class
