
#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class InsCoSelection : Inherits DataAccessBase

#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_INSCOMPANY As String = "spInsCoList"
    Private Const LIST_INSCOMPANYBYID As String = "spInsCoByID"
    Private Const INSCOMPANY_ADD As String = "spInsCoAdd"
    Private Const INSCOMPANY_EDT As String = "spInsCoEdit"
    Private Const INSCOMPANY_DEL As String = "spInsCoDelete"

    Private Const LIST_INSCOMPANY_BRANCH As String = "spInsCoBranchList"
    Private Const LIST_INSCOMPANY_BRANCH_BYID As String = "spInsCoBranchByID"
    Private Const INSCOMPANY_BRANCH_ADD As String = "spInsCoBranchAdd"
    Private Const INSCOMPANY_BRANCH_EDT As String = "spInsCoBranchEdit"
    Private Const INSCOMPANY_BRANCH_DEL As String = "spInsCoBranchDelete"

    Private Const LIST_RATE As String = "spInsCoBranchRateList"
    Private Const LIST_RATE_BYID As String = "spInsCoBranchRateByID"
    Private Const RATE_ADD As String = "spInsCoBranchRateAdd"
    Private Const RATE_EDT As String = "spInsCoBranchRateEdit"
    Private Const RATE_DEL As String = "spInsCoBranchRateDelete"

    Private Const LIST_TPL As String = "spInsCoBranchTPLList"
    Private Const LIST_TPL_BYID As String = "spInsCoBranchTPLByID"
    Private Const TPL_ADD As String = "spInsCoBranchTPLAdd"
    Private Const TPL_EDT As String = "spInsCoBranchTPLEdit"
    Private Const TPL_DEL As String = "spInsCoBranchTPLDelete"

    Private Const TPL_RPT As String = "spInsCoBranchTPLReport"
    Private Const RATE_RPT As String = "spInsCoBranchRateReport"

    Private Const CBO_ASSET As String = "spGetInsAssetType"
    Private Const CBO_COVERAGE As String = "spGetCoverageType"
    Private Const CBO_ASSETUSAGE As String = "spGetAssetUsage"

    Private Const RATE_INCOBRANCH_COPY As String = "spCopyRateFromAnotherInsCoBranch"
    Private Const INCOBRANCH_BYPRODUCT As String = "spGetInsuranceCompanyBranchByProduct"

#End Region


    Public Function GetInsCompanyList(ByVal oInsCo As Parameter.InsCoSelectionList) As Parameter.InsCoSelectionList

        Dim params(4) As SqlParameter

        With oInsCo
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = .SortBy
            params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output
        End With

        Try


            oInsCo.ListData = SqlHelper.ExecuteDataset(oInsCo.strConnection, CommandType.StoredProcedure, LIST_INSCOMPANY, params).Tables(0)
            oInsCo.TotRec = CType(params(4).Value, Int16)

            Return oInsCo
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception

            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

        End Try
    End Function

    Public Function GetInsCompanyByID(ByVal strConnection As String, ByVal InsCoID As String) As Parameter.InsCoSelectionList

        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
        params(0).Value = InsCoID


        Try
            Dim oReturnValue As New Parameter.InsCoSelectionList

            oReturnValue.ListData = SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, LIST_INSCOMPANYBYID, params).Tables(0)
            Return oReturnValue
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            WriteException(ex.Message)
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

        End Try
    End Function

    Public Function GetInsCompanyListDataSet(ByVal InsCo As Parameter.InsCoSelectionList) As Parameter.InsCoSelectionList


        Dim params(4) As SqlParameter

        With InsCo
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = .SortBy
            params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output

        End With

        Try
            Dim oReturnValue As New Parameter.InsCoSelectionList

            oReturnValue.ListDataSet = SqlHelper.ExecuteDataset(InsCo.strConnection, CommandType.StoredProcedure, LIST_INSCOMPANY, params)
            oReturnValue.TotRec = CType(params(4).Value, Int16)

            Return oReturnValue
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            WriteException(ex.Message)
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

        End Try
    End Function

#Region " InsCompanyAdd"
    Public Function InsCompanyAdd(ByVal customClass As Parameter.InsCo, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As String

        Dim params(18) As SqlParameter

        params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
        params(0).Value = customClass.InsCoID
        params(1) = New SqlParameter("@InsCoName", SqlDbType.VarChar, 50)
        params(1).Value = customClass.InsCoName

        params(2) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
        params(2).Value = oClassAddress.Address

        params(3) = New SqlParameter("@RT", SqlDbType.Char, 3)
        params(3).Value = oClassAddress.RT
        params(4) = New SqlParameter("@RW", SqlDbType.Char, 3)
        params(4).Value = oClassAddress.RW

        params(5) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
        params(5).Value = oClassAddress.Kelurahan
        params(6) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
        params(6).Value = oClassAddress.Kecamatan
        params(7) = New SqlParameter("@City", SqlDbType.VarChar, 30)
        params(7).Value = oClassAddress.City
        params(8) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
        params(8).Value = oClassAddress.ZipCode
        params(9) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
        params(9).Value = oClassAddress.AreaPhone1
        params(10) = New SqlParameter("@Phone1", SqlDbType.Char, 15)
        params(10).Value = oClassAddress.Phone1
        params(11) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
        params(11).Value = oClassAddress.AreaPhone2
        params(12) = New SqlParameter("@Phone2", SqlDbType.Char, 15)
        params(12).Value = oClassAddress.Phone2

        params(13) = New SqlParameter("@AreaFax", SqlDbType.VarChar, 4)
        params(13).Value = oClassAddress.AreaFax
        params(14) = New SqlParameter("@Fax", SqlDbType.VarChar, 10)
        params(14).Value = oClassAddress.Fax

        params(15) = New SqlParameter("@CPersonName", SqlDbType.VarChar, 50)
        params(15).Value = oClassPersonal.PersonName
        params(16) = New SqlParameter("@CPersonTitle", SqlDbType.VarChar, 50)
        params(16).Value = oClassPersonal.PersonTitle
        params(17) = New SqlParameter("@Email", SqlDbType.VarChar, 30)
        params(17).Value = oClassPersonal.Email
        params(18) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20)
        params(18).Value = oClassPersonal.MobilePhone

        Dim reader As SqlDataReader
        Try
            Dim oReturnValue As New Parameter.InsCo
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, INSCOMPANY_ADD, params)

        Catch ex As SqlException
            WriteException(ex.Message)
            Return "UnSuccess"


        End Try

    End Function
#End Region

#Region " InsCompanyEdit"
    Public Sub InsCompanyEdit(ByVal customClass As Parameter.InsCo, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)

        Dim params(19) As SqlParameter

        params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
        params(0).Value = customClass.InsCoID
        params(1) = New SqlParameter("@InsCoName", SqlDbType.VarChar, 50)
        params(1).Value = customClass.InsCoName

        params(2) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
        params(2).Value = oClassAddress.Address

        params(3) = New SqlParameter("@RT", SqlDbType.Char, 3)
        params(3).Value = oClassAddress.RT
        params(4) = New SqlParameter("@RW", SqlDbType.Char, 3)
        params(4).Value = oClassAddress.RW

        params(5) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
        params(5).Value = oClassAddress.Kelurahan
        params(6) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
        params(6).Value = oClassAddress.Kecamatan
        params(7) = New SqlParameter("@City", SqlDbType.VarChar, 30)
        params(7).Value = oClassAddress.City
        params(8) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
        params(8).Value = oClassAddress.ZipCode
        params(9) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
        params(9).Value = oClassAddress.AreaPhone1
        params(10) = New SqlParameter("@Phone1", SqlDbType.Char, 15)
        params(10).Value = oClassAddress.Phone1
        params(11) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
        params(11).Value = oClassAddress.AreaPhone2
        params(12) = New SqlParameter("@Phone2", SqlDbType.Char, 15)
        params(12).Value = oClassAddress.Phone2

        params(13) = New SqlParameter("@AreaFax", SqlDbType.VarChar, 4)
        params(13).Value = oClassAddress.AreaFax
        params(14) = New SqlParameter("@Fax", SqlDbType.VarChar, 10)
        params(14).Value = oClassAddress.Fax

        params(15) = New SqlParameter("@CPersonName", SqlDbType.VarChar, 50)
        params(15).Value = oClassPersonal.PersonName
        params(16) = New SqlParameter("@CPersonTitle", SqlDbType.VarChar, 50)
        params(16).Value = oClassPersonal.PersonTitle
        params(17) = New SqlParameter("@Email", SqlDbType.VarChar, 30)
        params(17).Value = oClassPersonal.Email
        params(18) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20)
        params(18).Value = oClassPersonal.MobilePhone

        params(19) = New SqlParameter("@DtmUpd", SqlDbType.DateTime)
        params(19).Value = Now()


        Dim reader As SqlDataReader
        Try
            Dim oReturnValue As New Parameter.InsCo
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, INSCOMPANY_EDT, params)

        Catch ex As Exception
            WriteException(ex.Message)
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("INFO: Unable to edit the selected record")

        End Try

    End Sub
#End Region

#Region " InsCompanyDelete"
    Public Sub InsCompanyDelete(ByVal customClass As Parameter.InsCo)

        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
        params(0).Value = customClass.InsCoID


        Dim reader As SqlDataReader
        Try
            Dim oReturnValue As New Parameter.InsCo
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, INSCOMPANY_DEL, params)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            WriteException(ex.Message)
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")

        End Try

    End Sub
#End Region

    Public Function GetInsCompanyBranchList(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList
        Dim params(5) As SqlParameter

        With InsCoBranch
            params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
            params(0).Value = .InsCoID
            params(1) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(1).Value = .CurrentPage
            params(2) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(2).Value = .PageSize
            params(3) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(3).Value = .WhereCond
            params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(4).Value = .SortBy
            params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
            params(5).Direction = ParameterDirection.Output
        End With

        Try
            Dim oReturnValue As New Parameter.InsCoSelectionList

            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoBranch.strConnection, CommandType.StoredProcedure, LIST_INSCOMPANY_BRANCH, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(5).Value, Int16)

            Return oReturnValue
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            WriteException(ex.Message)
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

        End Try
    End Function

    Public Function GetInsCompanyBranchByID(ByVal insCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList

        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
        params(0).Value = insCoBranch.InsCoID
        params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
        params(1).Value = insCoBranch.InsCoBranchID

        Try
            Dim oReturnValue As New Parameter.InsCoSelectionList

            oReturnValue.ListData = SqlHelper.ExecuteDataset(insCoBranch.strConnection, CommandType.StoredProcedure, LIST_INSCOMPANY_BRANCH_BYID, params).Tables(0)

            Return oReturnValue
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            WriteException(ex.Message)
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

        End Try
    End Function

#Region " InsCompanyBranchAdd"
    Public Sub InsCompanyBranchAdd(ByVal customClass As Parameter.InsCoBranch, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal, ByVal oClassBankAccount As Parameter.BankAccount)

        Dim params(28) As SqlParameter

        params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
        params(0).Value = customClass.InsCoID

        params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 10)
        params(1).Value = customClass.BranchId
        params(2) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
        params(2).Value = customClass.InsCoBranchID
        params(3) = New SqlParameter("@InsCoBranchName", SqlDbType.VarChar, 50)
        params(3).Value = customClass.InsCoBranchName


        params(4) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
        params(4).Value = oClassAddress.Address
        params(5) = New SqlParameter("@RT", SqlDbType.Char, 3)
        params(5).Value = oClassAddress.RT
        params(6) = New SqlParameter("@RW", SqlDbType.Char, 3)
        params(6).Value = oClassAddress.RW

        params(7) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
        params(7).Value = oClassAddress.Kelurahan
        params(8) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
        params(8).Value = oClassAddress.Kecamatan
        params(9) = New SqlParameter("@City", SqlDbType.VarChar, 30)
        params(9).Value = oClassAddress.City
        params(10) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
        params(10).Value = oClassAddress.ZipCode
        params(11) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
        params(11).Value = oClassAddress.AreaPhone1
        params(12) = New SqlParameter("@Phone1", SqlDbType.Char, 15)
        params(12).Value = oClassAddress.Phone1
        params(13) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
        params(13).Value = oClassAddress.AreaPhone2
        params(14) = New SqlParameter("@Phone2", SqlDbType.Char, 15)
        params(14).Value = oClassAddress.Phone2
        params(15) = New SqlParameter("@AreaFax", SqlDbType.VarChar, 4)
        params(15).Value = oClassAddress.AreaFax
        params(16) = New SqlParameter("@Fax", SqlDbType.VarChar, 10)
        params(16).Value = oClassAddress.Fax


        params(17) = New SqlParameter("@CPersonName", SqlDbType.VarChar, 50)
        params(17).Value = oClassPersonal.PersonName
        params(18) = New SqlParameter("@CPersonTitle", SqlDbType.VarChar, 50)
        params(18).Value = oClassPersonal.PersonTitle
        params(19) = New SqlParameter("@Email", SqlDbType.VarChar, 30)
        params(19).Value = oClassPersonal.Email
        params(20) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20)
        params(20).Value = oClassPersonal.MobilePhone

        params(21) = New SqlParameter("@BankName", SqlDbType.Char, 5)
        params(21).Value = oClassBankAccount.BankName
        params(22) = New SqlParameter("@BankBranch", SqlDbType.VarChar, 50)
        params(22).Value = oClassBankAccount.BankBranch
        params(23) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 25)
        params(23).Value = oClassBankAccount.BankAccountID
        params(24) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
        params(24).Value = oClassBankAccount.BankAccountName

        params(25) = New SqlParameter("@TermOfPayment", SqlDbType.Int)
        params(25).Value = customClass.TermOfPayment
        params(26) = New SqlParameter("@AdminFee", SqlDbType.Float)
        params(26).Value = customClass.AdminFee
        params(27) = New SqlParameter("@StampDutyFee", SqlDbType.Float)
        params(27).Value = customClass.StampDutyFee

        params(28) = New SqlParameter("@DtmUpd", SqlDbType.DateTime)
        params(28).Value = Now()


        Dim reader As SqlDataReader
        Try
            Dim oReturnValue As New Parameter.InsCo
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, INSCOMPANY_BRANCH_ADD, params)

        Catch ex As Exception
            WriteException(ex.Message)
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("A record already exist with the same primary key!")

        End Try

    End Sub
#End Region

#Region " InsCompanyBranchEdit"
    Public Sub InsCompanyBranchEdit(ByVal customClass As Parameter.InsCoBranch, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal, ByVal oClassBankAccount As Parameter.BankAccount)

        Dim params(28) As SqlParameter

        params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
        params(0).Value = customClass.InsCoID

        params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 10)
        params(1).Value = customClass.BranchId
        params(2) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
        params(2).Value = customClass.InsCoBranchID
        params(3) = New SqlParameter("@InsCoBranchName", SqlDbType.VarChar, 50)
        params(3).Value = customClass.InsCoBranchName


        params(4) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
        params(4).Value = oClassAddress.Address
        params(5) = New SqlParameter("@RT", SqlDbType.Char, 3)
        params(5).Value = oClassAddress.RT
        params(6) = New SqlParameter("@RW", SqlDbType.Char, 3)
        params(6).Value = oClassAddress.RW

        params(7) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
        params(7).Value = oClassAddress.Kelurahan
        params(8) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
        params(8).Value = oClassAddress.Kecamatan
        params(9) = New SqlParameter("@City", SqlDbType.VarChar, 30)
        params(9).Value = oClassAddress.City
        params(10) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
        params(10).Value = oClassAddress.ZipCode
        params(11) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
        params(11).Value = oClassAddress.AreaPhone1
        params(12) = New SqlParameter("@Phone1", SqlDbType.Char, 15)
        params(12).Value = oClassAddress.Phone1
        params(13) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
        params(13).Value = oClassAddress.AreaPhone2
        params(14) = New SqlParameter("@Phone2", SqlDbType.Char, 15)
        params(14).Value = oClassAddress.Phone2
        params(15) = New SqlParameter("@AreaFax", SqlDbType.VarChar, 4)
        params(15).Value = oClassAddress.AreaFax
        params(16) = New SqlParameter("@Fax", SqlDbType.VarChar, 10)
        params(16).Value = oClassAddress.Fax


        params(17) = New SqlParameter("@CPersonName", SqlDbType.VarChar, 50)
        params(17).Value = oClassPersonal.PersonName
        params(18) = New SqlParameter("@CPersonTitle", SqlDbType.VarChar, 50)
        params(18).Value = oClassPersonal.PersonTitle
        params(19) = New SqlParameter("@Email", SqlDbType.VarChar, 30)
        params(19).Value = oClassPersonal.Email
        params(20) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20)
        params(20).Value = oClassPersonal.MobilePhone

        params(21) = New SqlParameter("@BankName", SqlDbType.Char, 5)
        params(21).Value = oClassBankAccount.BankName
        params(22) = New SqlParameter("@BankBranch", SqlDbType.VarChar, 50)
        params(22).Value = oClassBankAccount.BankBranch
        params(23) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 25)
        params(23).Value = oClassBankAccount.BankAccountID
        params(24) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
        params(24).Value = oClassBankAccount.BankAccountName

        params(25) = New SqlParameter("@TermOfPayment", SqlDbType.Int)
        params(25).Value = customClass.TermOfPayment
        params(26) = New SqlParameter("@AdminFee", SqlDbType.Float)
        params(26).Value = customClass.AdminFee
        params(27) = New SqlParameter("@StampDutyFee", SqlDbType.Float)
        params(27).Value = customClass.StampDutyFee

        params(28) = New SqlParameter("@DtmUpd", SqlDbType.DateTime)
        params(28).Value = Now()


        Dim reader As SqlDataReader
        Try
            Dim oReturnValue As New Parameter.InsCo
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, INSCOMPANY_BRANCH_EDT, params)

        Catch ex As Exception
            WriteException(ex.Message)
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("INFO: Unable to edit the selected record")

        End Try

    End Sub
#End Region

#Region " InsCompanyBranchDelete"
    Public Sub InsCompanyBranchDelete(ByVal customClass As Parameter.InsCoBranch)

        Dim params(2) As SqlParameter

        params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
        params(0).Value = customClass.InsCoID
        params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
        params(1).Value = customClass.InsCoBranchID
        params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(2).Value = customClass.BranchId


        Dim reader As SqlDataReader
        Try
            Dim oReturnValue As New Parameter.InsCo
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, INSCOMPANY_BRANCH_DEL, params)

        Catch ex As Exception
            WriteException(ex.Message)
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")

        End Try

    End Sub
#End Region

    Public Function GetRateList(ByVal InsCoRate As Parameter.InsCoRate) As Parameter.InsCoSelectionList
        Dim params(7) As SqlParameter

        With InsCoRate
            params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
            params(0).Value = .InsCoID
            params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
            params(1).Value = .InsCoBranchID
            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(2).Value = .BranchId
            params(3) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(3).Value = .CurrentPage
            params(4) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(4).Value = .PageSize
            params(5) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(5).Value = .WhereCond
            params(6) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(6).Value = .SortBy

            params(7) = New SqlParameter("@TotalRecords", SqlDbType.Int)
            params(7).Direction = ParameterDirection.Output
        End With

        Try
            Dim oReturnValue As New Parameter.InsCoSelectionList

            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoRate.strConnection, CommandType.StoredProcedure, LIST_RATE, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(7).Value, Int16)

            Return oReturnValue
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            WriteException(ex.Message)
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

        End Try
    End Function

    Public Function GetRateByID(ByVal InsCoRate As Parameter.InsCoRate) As Parameter.InsCoSelectionList
        Dim params(7) As SqlParameter

        With InsCoRate
            params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
            params(0).Value = .InsCoID
            params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
            params(1).Value = .InsCoBranchID
            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(2).Value = .BranchId
            params(3) = New SqlParameter("@InsuranceType", SqlDbType.Char, 2)
            params(3).Value = .insAssettype
            params(4) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 10)
            params(4).Value = .CoverageType
            params(5) = New SqlParameter("@UsageID", SqlDbType.VarChar, 10)
            params(5).Value = .AssetUssage
            params(6) = New SqlParameter("@NewUsed", SqlDbType.Char, 1)
            params(6).Value = .newused
            params(7) = New SqlParameter("@YearNum", SqlDbType.VarChar, 4)
            params(7).Value = .yearnum


        End With

        Try
            Dim oReturnValue As New Parameter.InsCoSelectionList

            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoRate.strConnection, CommandType.StoredProcedure, LIST_RATE_BYID, params).Tables(0)

            Return oReturnValue
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            WriteException(ex.Message)
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

        End Try
    End Function

    Public Function GetTPLList(ByVal InsCoTPL As Parameter.InsCoTPL) As Parameter.InsCoSelectionList
        Dim params(7) As SqlParameter

        With InsCoTPL
            params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
            params(0).Value = .InsCoID
            params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
            params(1).Value = .InsCoBranchID
            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(2).Value = .BranchId

            params(3) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(3).Value = .CurrentPage
            params(4) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(4).Value = .PageSize
            params(5) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(5).Value = .WhereCond
            params(6) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(6).Value = .SortBy

            params(7) = New SqlParameter("@TotalRecords", SqlDbType.Int)
            params(7).Direction = ParameterDirection.Output
        End With

        Try
            Dim oReturnValue As New Parameter.InsCoSelectionList

            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoTPL.strConnection, CommandType.StoredProcedure, LIST_TPL, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(7).Value, Int16)

            Return oReturnValue
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            WriteException(ex.Message)
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

        End Try
    End Function

    Public Function GetTPLByID(ByVal InsCoTPL As Parameter.InsCoTPL) As Parameter.InsCoSelectionList
        Dim params(3) As SqlParameter

        With InsCoTPL
            params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
            params(0).Value = .InsCoID
            params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
            params(1).Value = .InsCoBranchID
            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(2).Value = .BranchId
            params(3) = New SqlParameter("@TPLAmount", SqlDbType.Float)
            params(3).Value = .tplamount
        End With

        Try
            Dim oReturnValue As New Parameter.InsCoSelectionList

            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoTPL.strConnection, CommandType.StoredProcedure, LIST_TPL_BYID, params).Tables(0)

            Return oReturnValue
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

        End Try
    End Function


#Region "GetTPLReport"
    Public Function GetTPLReport(ByVal InsCoTPL As Parameter.InsCoTPL) As Parameter.InsCoSelectionList
        Dim params(1) As SqlParameter

        With InsCoTPL

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = .BranchId
            params(1) = New SqlParameter("@InsuranceComBranchID", SqlDbType.VarChar, 20)
            params(1).Value = .InsCoBranchID

        End With

        Try
            Dim oReturnValue As New Parameter.InsCoSelectionList

            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoTPL.strConnection, CommandType.StoredProcedure, TPL_RPT, params).Tables(0)

            Return oReturnValue
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

        End Try
    End Function
#End Region


    Public Function GetRateReport(ByVal InsCoRate As Parameter.InsCoRate) As Parameter.InsCoSelectionList
        Dim params(1) As SqlParameter

        With InsCoRate

            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = .BranchId
            params(1) = New SqlParameter("@InsuranceComBranch", SqlDbType.Char, 10)
            params(1).Value = .InsCoBranchID

        End With

        Try
            Dim oReturnValue As New Parameter.InsCoSelectionList

            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoRate.strConnection, CommandType.StoredProcedure, RATE_RPT, params).Tables(0)

            Return oReturnValue
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

        End Try
    End Function


#Region " TPLAdd"
    Public Sub TPLAdd(ByVal InsCoTPL As Parameter.InsCoTPL)

        Dim params(4) As SqlParameter

        params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
        params(0).Value = InsCoTPL.InsCoID
        params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
        params(1).Value = InsCoTPL.InsCoBranchID
        params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 10)
        params(2).Value = InsCoTPL.BranchId
        params(3) = New SqlParameter("@TPLAmount", SqlDbType.Float)
        params(3).Value = InsCoTPL.tplamount
        params(4) = New SqlParameter("@TPLPremium", SqlDbType.Float)
        params(4).Value = InsCoTPL.TPLPremium


        Dim reader As SqlDataReader
        Try
            Dim oReturnValue As New Parameter.InsCo
            SqlHelper.ExecuteNonQuery(InsCoTPL.strConnection, CommandType.StoredProcedure, TPL_ADD, params)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("A record already exist with the same primary key!")

        End Try

    End Sub
#End Region

#Region " TPLEdit"
    Public Sub TPLEdit(ByVal InsCoTPL As Parameter.InsCoTPL)

        Dim params(4) As SqlParameter

        params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
        params(0).Value = InsCoTPL.InsCoID
        params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
        params(1).Value = InsCoTPL.InsCoBranchID
        params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 10)
        params(2).Value = InsCoTPL.BranchId
        params(3) = New SqlParameter("@TPLAmount", SqlDbType.Float)
        params(3).Value = InsCoTPL.tplamount
        params(4) = New SqlParameter("@TPLPremium", SqlDbType.Float)
        params(4).Value = InsCoTPL.TPLPremium


        Dim reader As SqlDataReader
        Try
            Dim oReturnValue As New Parameter.InsCo
            SqlHelper.ExecuteNonQuery(InsCoTPL.strConnection, CommandType.StoredProcedure, TPL_EDT, params)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("Error On DataAccess.InsCo.ActionAdd")

        End Try

    End Sub
#End Region

#Region " TPLDelete"
    Public Sub TPLDelete(ByVal InsCoTPL As Parameter.InsCoTPL)

        Dim params(3) As SqlParameter

        params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
        params(0).Value = InsCoTPL.InsCoID
        params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
        params(1).Value = InsCoTPL.InsCoBranchID
        params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 10)
        params(2).Value = InsCoTPL.BranchId
        params(3) = New SqlParameter("@TPLAmount", SqlDbType.Float)
        params(3).Value = InsCoTPL.tplamount


        Dim reader As SqlDataReader
        Try
            Dim oReturnValue As New Parameter.InsCoTPL
            SqlHelper.ExecuteNonQuery(InsCoTPL.strConnection, CommandType.StoredProcedure, TPL_DEL, params)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("ERROR: Unable to delete the selected record. Record already used !")

        End Try

    End Sub
#End Region

#Region " RateAdd"
    Public Sub RateAdd(ByVal InsCoRate As Parameter.InsCoRate)

        Dim params(12) As SqlParameter

        With InsCoRate
            params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
            params(0).Value = .InsCoID.Trim
            params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
            params(1).Value = .InsCoBranchID.Trim
            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(2).Value = .BranchId.Trim
            params(3) = New SqlParameter("@InsuranceAssetType", SqlDbType.Char, 2)
            params(3).Value = .insAssettype.Trim
            params(4) = New SqlParameter("@AssetUsage", SqlDbType.Char, 1)
            params(4).Value = .AssetUssage.Trim
            params(5) = New SqlParameter("@AssetNewUsed", SqlDbType.Char, 1)
            params(5).Value = .newused.Trim
            params(6) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 3)
            params(6).Value = .CoverageType.Trim
            params(7) = New SqlParameter("@YearNum", SqlDbType.VarChar, 4)
            params(7).Value = .yearnum
            params(8) = New SqlParameter("@rateInsCo", SqlDbType.Float)
            params(8).Value = .ratetoInsco
            params(9) = New SqlParameter("@SRCCRate", SqlDbType.Float)
            params(9).Value = .srccRate
            params(10) = New SqlParameter("@FloodRate", SqlDbType.Float)
            params(10).Value = .FloodRate
            params(11) = New SqlParameter("@LoadingRate", SqlDbType.Float)
            params(11).Value = .loadingrate
            params(12) = New SqlParameter("@SumInsured ", SqlDbType.Float)
            params(12).Value = .sumInsuranced

        End With

        Dim reader As SqlDataReader
        Try
            Dim oReturnValue As New Parameter.InsCo
            SqlHelper.ExecuteNonQuery(InsCoRate.strConnection, CommandType.StoredProcedure, RATE_ADD, params)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("A record already exist with the same primary key!")

        End Try

    End Sub
#End Region

#Region " RateEdit"
    Public Sub RateEdit(ByVal InsCoRate As Parameter.InsCoRate)


        Dim params(12) As SqlParameter

        With InsCoRate
            params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
            params(0).Value = .InsCoID.Trim
            params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
            params(1).Value = .InsCoBranchID.Trim
            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(2).Value = .BranchId.Trim
            params(3) = New SqlParameter("@InsuranceAssetType", SqlDbType.Char, 2)
            params(3).Value = .insAssettype.Trim
            params(4) = New SqlParameter("@AssetUsage", SqlDbType.Char, 1)
            params(4).Value = .AssetUssage.Trim
            params(5) = New SqlParameter("@AssetNewUsed", SqlDbType.Char, 1)
            params(5).Value = .newused.Trim
            params(6) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 3)
            params(6).Value = .CoverageType.Trim
            params(7) = New SqlParameter("@YearNum", SqlDbType.VarChar, 4)
            params(7).Value = .yearnum
            params(8) = New SqlParameter("@rateInsCo", SqlDbType.Decimal)
            params(8).Value = .ratetoInsco
            params(9) = New SqlParameter("@SRCCRate", SqlDbType.Decimal)
            params(9).Value = .srccRate
            params(10) = New SqlParameter("@FloodRate", SqlDbType.Decimal)
            params(10).Value = .FloodRate
            params(11) = New SqlParameter("@LoadingRate", SqlDbType.Decimal)
            params(11).Value = .loadingrate
            params(12) = New SqlParameter("@SumInsured ", SqlDbType.Decimal)
            params(12).Value = .sumInsuranced

        End With

        Dim reader As SqlDataReader
        Try
            Dim oReturnValue As New Parameter.InsCoRate
            SqlHelper.ExecuteNonQuery(InsCoRate.strConnection, CommandType.StoredProcedure, RATE_EDT, params)

        Catch ex As Exception

        End Try

    End Sub
#End Region

#Region " RateDelete"
    Public Sub RateDelete(ByVal InsCoRate As Parameter.InsCoRate)

        Dim params(7) As SqlParameter

        With InsCoRate
            params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
            params(0).Value = .InsCoID
            params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
            params(1).Value = .InsCoBranchID
            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 10)
            params(2).Value = .BranchId
            params(3) = New SqlParameter("@InsuranceAssetType", SqlDbType.VarChar, 2)
            params(3).Value = .insAssettype
            params(4) = New SqlParameter("@AssetUsage", SqlDbType.VarChar, 10)
            params(4).Value = .AssetUssage
            params(5) = New SqlParameter("@AssetNewUsed", SqlDbType.Char, 4)
            params(5).Value = .newused
            params(6) = New SqlParameter("@CoverageType", SqlDbType.Char, 10)
            params(6).Value = .CoverageType
            params(7) = New SqlParameter("@YearNum", SqlDbType.VarChar, 4)
            params(7).Value = .yearnum

        End With



        Dim reader As SqlDataReader
        Try
            Dim oReturnValue As New Parameter.InsCoRate
            SqlHelper.ExecuteNonQuery(InsCoRate.strConnection, CommandType.StoredProcedure, RATE_DEL, params)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("ERROR: Unable to delete the selected record. Record already used !")

        End Try

    End Sub
#End Region

#Region "CopyInsCoBranchRate"
    Public Sub CopyInsCoBranchRate(ByVal InsCoRate As Parameter.InsCoRate)
        Dim params(5) As SqlParameter

        With InsCoRate
            params(0) = New SqlParameter("@InsCoBranchIDFrom", SqlDbType.Char, 10)
            params(0).Value = .InsCoBranchIDFrom
            params(1) = New SqlParameter("@InsCoBranchIDTo", SqlDbType.Char, 10)
            params(1).Value = .InsCoBranchIDTo
            params(2) = New SqlParameter("@InsCoIDTo", SqlDbType.Char, 10)
            params(2).Value = .InsCoIDTo
            params(3) = New SqlParameter("@BranchIDTo", SqlDbType.Char, 3)
            params(3).Value = .BranchIDTo
            params(4) = New SqlParameter("@BranchIDFrom", SqlDbType.VarChar, 3)
            params(4).Value = .BranchIDFrom
            params(5) = New SqlParameter("@InsCoyIDFrom", SqlDbType.VarChar, 20)
            params(5).Value = .InsCoIDFrom

        End With

        Try
            SqlHelper.ExecuteNonQuery(InsCoRate.strConnection, CommandType.StoredProcedure, RATE_INCOBRANCH_COPY, params)
        Catch ex As Exception

        End Try

    End Sub
#End Region

#Region " Get The Combo Box"
    Public Function GetCoverageTypeCombo(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList

        Try
            Dim oReturnValue As New Parameter.InsCoSelectionList
          
            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoBranch.strConnection, CommandType.StoredProcedure, CBO_COVERAGE).Tables(0)

            Return oReturnValue
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

        End Try
    End Function

    Public Function GetInsAssetTypeCombo(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList

        Try
            Dim oReturnValue As New Parameter.InsCoSelectionList

            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoBranch.strConnection, CommandType.StoredProcedure, CBO_ASSET).Tables(0)

            Return oReturnValue
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

        End Try
    End Function

    Public Function GetAssetUsageCombo(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList

        Try
            Dim oReturnValue As New Parameter.InsCoSelectionList

            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoBranch.strConnection, CommandType.StoredProcedure, CBO_ASSETUSAGE).Tables(0)

            Return oReturnValue
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

        End Try
    End Function

    Public Function GetInsuranceComByProduct(ByVal InsCoBranch As Parameter.InsCoBranch) As Parameter.InsCoSelectionList
        Try
            Dim oReturnValue As New Parameter.InsCoSelectionList
            Dim params(2) As SqlParameter

            params(0) = New SqlParameter("@branchid", SqlDbType.Char, 3)
            params(0).Value = InsCoBranch.BranchId
            params(1) = New SqlParameter("@ProductID", SqlDbType.Char, 3)
            params(1).Value = InsCoBranch.InsuranceProductID
            params(2) = New SqlParameter("@LoginId", SqlDbType.Char, 50)
            params(2).Value = InsCoBranch.LoginId

            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoBranch.strConnection, CommandType.StoredProcedure, INCOBRANCH_BYPRODUCT, params).Tables(0)

            Return oReturnValue
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

        End Try
    End Function

#End Region
End Class
