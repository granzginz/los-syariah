#Region "Imports"

Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class daInsuranceCompany
    Private m_connection As SqlConnection

#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_INSCOMPANY As String = "spInsCoList"
    Private Const LIST_INSCOMPANYBYID As String = "spInsCoByID"
    Private Const INSCOMPANY_ADD As String = "spInsCoAdd"
    Private Const INSCOMPANY_EDT As String = "spInsCoEdit"
    Private Const INSCOMPANY_DEL As String = "spInsCoDelete"

    Private Const LIST_INSCOMPANY_BRANCH As String = "spInsCoBranchList"
    Private Const LIST_INSCOMPANY_BRANCH_BYID As String = "spInsCoBranchByID"
    Private Const INSCOMPANY_BRANCH_ADD As String = "spInsCoBranchAdd"
    Private Const INSCOMPANY_BRANCH_EDT As String = "spInsCoBranchEdit"
    Private Const INSCOMPANY_BRANCH_DEL As String = "spInsCoBranchDelete"

    Private Const RATE_DEL As String = "spShadowInsuranceCompanyBranchRateDelete"
    Private Const RATE_INCOBRANCH_COPY As String = "spShadowInsuranceCompanyBranchRateCopy"

    Private Const TPL_DEL As String = "spShadowInsuranceCompanyBranchTPLDelete"
    Private Const COVERAGE_SAVE As String = "spShadowInsuranceCompanyBranchCoverageSave"

    Private Const TPL_RPT As String = "spInsCoBranchTPLReport"
    Private Const RATE_RPT As String = "spInsCoBranchRateReport"

    Private Const CBO_ASSET As String = "spGetInsAssetType"
    Private Const CBO_COVERAGE As String = "spGetCoverageType"
    Private Const CBO_ASSETUSAGE As String = "spGetAssetUsage"


#End Region


    'Public Function GetInsCompanyList(ByVal oInsCo As Entities.InsCoSelectionList) As Entities.InsCoSelectionList

    '    Dim params(4) As SqlParameter

    '    With oInsCo
    '        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
    '        params(0).Value = .CurrentPage
    '        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
    '        params(1).Value = .PageSize
    '        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
    '        params(2).Value = .WhereCond
    '        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
    '        params(3).Value = .SortBy
    '        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
    '        params(4).Direction = ParameterDirection.Output
    '    End With

    '    Try


    '        oInsCo.ListData = SqlHelper.ExecuteDataset(oInsCo.strConnection, CommandType.StoredProcedure, LIST_INSCOMPANY, params).Tables(0)
    '        oInsCo.TotRec = CType(params(4).Value, Int16)

    '        Return oInsCo
    '        'Trace.WriteIf(MethodTrace.Enabled, "Exit")

    '    Catch ex As Exception

    '        'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

    '    End Try
    'End Function

    'Public Function GetInsCompanyByID(ByVal strConnection As String, ByVal InsCoID As String) As Entities.InsCoSelectionList

    '    Dim params() As SqlParameter = New SqlParameter(0) {}

    '    params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
    '    params(0).Value = InsCoID


    '    Try
    '        Dim oReturnValue As New Entities.InsCoSelectionList

    '        oReturnValue.ListData = SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, LIST_INSCOMPANYBYID, params).Tables(0)
    '        Return oReturnValue
    '        'Trace.WriteIf(MethodTrace.Enabled, "Exit")

    '    Catch ex As Exception
    '        writeexception(ex.Message)
    '        'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

    '    End Try
    'End Function

    'Public Function GetInsCompanyListDataSet(ByVal InsCo As Entities.InsCoSelectionList) As Entities.InsCoSelectionList


    '    Dim params(4) As SqlParameter

    '    With InsCo
    '        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
    '        params(0).Value = .CurrentPage
    '        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
    '        params(1).Value = .PageSize
    '        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
    '        params(2).Value = .WhereCond
    '        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
    '        params(3).Value = .SortBy
    '        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
    '        params(4).Direction = ParameterDirection.Output

    '    End With

    '    Try
    '        Dim oReturnValue As New Entities.InsCoSelectionList

    '        oReturnValue.ListDataSet = SqlHelper.ExecuteDataset(InsCo.strConnection, CommandType.StoredProcedure, LIST_INSCOMPANY, params)
    '        oReturnValue.TotRec = CType(params(4).Value, Int16)

    '        Return oReturnValue
    '        'Trace.WriteIf(MethodTrace.Enabled, "Exit")

    '    Catch ex As Exception
    '        writeexception(ex.Message)
    '        'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

    '    End Try
    'End Function

#Region " InsCompanySave"
    Public Function InsCompanySave(ByVal customClass As Parameter.eInsuranceCompany, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As Parameter.eInsuranceCompany

        Dim params() As SqlParameter = New SqlParameter(22) {}

        Dim objCon As New SqlConnection(customClass.strConnection)
        'Dim transaction As SqlTransaction
        Dim ErrMessage As String = ""
        Dim SP As String
        If customClass.Command = "A" Then
            SP = "spInsCoAdd"
        ElseIf customClass.Command = "E" Then
            SP = "spInsCoEdit"
        End If
        Try
            params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
            params(0).Value = customClass.InsCoID

            params(1) = New SqlParameter("@InsCoName", SqlDbType.VarChar, 50)
            params(1).Value = customClass.InsCoName

            params(2) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
            params(2).Value = oClassAddress.Address

            params(3) = New SqlParameter("@RT", SqlDbType.Char, 3)
            params(3).Value = oClassAddress.RT

            params(4) = New SqlParameter("@RW", SqlDbType.Char, 3)
            params(4).Value = oClassAddress.RW

            params(5) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
            params(5).Value = oClassAddress.Kelurahan

            params(6) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
            params(6).Value = oClassAddress.Kecamatan

            params(7) = New SqlParameter("@City", SqlDbType.VarChar, 30)
            params(7).Value = oClassAddress.City

            params(8) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
            params(8).Value = oClassAddress.ZipCode

            params(9) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
            params(9).Value = oClassAddress.AreaPhone1

            params(10) = New SqlParameter("@Phone1", SqlDbType.Char, 15)
            params(10).Value = oClassAddress.Phone1

            params(11) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
            params(11).Value = oClassAddress.AreaPhone2

            params(12) = New SqlParameter("@Phone2", SqlDbType.Char, 15)
            params(12).Value = oClassAddress.Phone2

            params(13) = New SqlParameter("@AreaFax", SqlDbType.VarChar, 4)
            params(13).Value = oClassAddress.AreaFax

            params(14) = New SqlParameter("@Fax", SqlDbType.VarChar, 10)
            params(14).Value = oClassAddress.Fax

            params(15) = New SqlParameter("@CPersonName", SqlDbType.VarChar, 50)
            params(15).Value = oClassPersonal.PersonName

            params(16) = New SqlParameter("@CPersonTitle", SqlDbType.VarChar, 50)
            params(16).Value = oClassPersonal.PersonTitle

            params(17) = New SqlParameter("@Email", SqlDbType.VarChar, 30)
            params(17).Value = oClassPersonal.Email

            params(18) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20)
            params(18).Value = oClassPersonal.MobilePhone

            params(19) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(19).Direction = ParameterDirection.Output

            params(20) = New SqlParameter("@DtmUpd", SqlDbType.DateTime)
            params(20).Value = Now()

            params(21) = New SqlParameter("@IsUtama", SqlDbType.Bit)
            params(21).Value = customClass.IsUtama

            params(22) = New SqlParameter("@AccountAP", SqlDbType.Char, 10)
            params(22).Value = customClass.AccountAP

            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, SP, params)

            ErrMessage = CType(params(19).Value, String)

            customClass.output = ErrMessage
            Return customClass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try

    End Function
#End Region

#Region " InsCompanyDelete"
    Public Sub InsCompanyDelete(ByVal customClass As Parameter.eInsuranceCompany)
        Dim params(0) As SqlParameter
        Dim paramsLoadRate(5) As SqlParameter
        Dim ErrMessage As String = ""

        Dim sp As String
        If customClass.FormID = "LOADINGRATE" Then
            sp = "spInsuranceLoadingRateDelete"
            Try
                With customClass
                    paramsLoadRate(0) = New SqlParameter("@MaskAssID", SqlDbType.Char, 10)
                    paramsLoadRate(0).Value = .InsCoID.Trim
                    paramsLoadRate(1) = New SqlParameter("@InsuranceComBranchID", SqlDbType.Char, 10)
                    paramsLoadRate(1).Value = .InsCoBranchID.Trim
                    paramsLoadRate(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                    paramsLoadRate(2).Value = .BranchId.Trim
                    paramsLoadRate(3) = New SqlParameter("@CategoryType", SqlDbType.VarChar, 10)
                    paramsLoadRate(3).Value = .CategoryType.Trim
                    paramsLoadRate(4) = New SqlParameter("@UsiaAsset", SqlDbType.SmallInt)
                    paramsLoadRate(4).Value = .UsiaAsset
                    paramsLoadRate(5) = New SqlParameter("@LoadingRate", SqlDbType.Decimal)
                    paramsLoadRate(5).Value = .LoadingRate
                    'paramsLoadRate(6) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
                    'paramsLoadRate(6).Direction = ParameterDirection.Output
                End With

                SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, sp, paramsLoadRate)
                '  ErrMessage = CType(params(6).Value, String)
                ' customClass.output = ErrMessage
                Return
            Catch ex As Exception
                Throw New Exception("Error On DataAccess.Insurance.daInsuranceCompany.RateSave")
            Finally
                If Not m_connection Is Nothing Then
                    m_connection.Close()
                End If
            End Try
        Else
            params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
            params(0).Value = customClass.InsCoID
            Dim reader As SqlDataReader
            Try
                Dim oReturnValue As New Parameter.eInsuranceCompany
                SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, INSCOMPANY_DEL, params)

            Catch ex As Exception
                'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
                'writeexception(ex.Message)
                Throw New Exception("INFO: Unable to delete the selected record. Record already used !")

            End Try
        End If
    End Sub
#End Region

    '    Public Function GetInsCompanyBranchList(ByVal InsCoBranch As Entities.InsCoBranch) As Entities.InsCoSelectionList
    '        Dim params(5) As SqlParameter

    '        With InsCoBranch
    '            params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
    '            params(0).Value = .InsCoID
    '            params(1) = New SqlParameter("@CurrentPage", SqlDbType.Int)
    '            params(1).Value = .CurrentPage
    '            params(2) = New SqlParameter("@PageSize", SqlDbType.Int)
    '            params(2).Value = .PageSize
    '            params(3) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
    '            params(3).Value = .WhereCond
    '            params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
    '            params(4).Value = .SortBy
    '            params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
    '            params(5).Direction = ParameterDirection.Output
    '        End With

    '        Try
    '            Dim oReturnValue As New Entities.InsCoSelectionList

    '            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoBranch.strConnection, CommandType.StoredProcedure, LIST_INSCOMPANY_BRANCH, params).Tables(0)
    '            oReturnValue.TotalRecord = CType(params(5).Value, Int16)

    '            Return oReturnValue
    '            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

    '        Catch ex As Exception
    '            writeexception(ex.Message)
    '            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

    '        End Try
    '    End Function

    '    Public Function GetInsCompanyBranchByID(ByVal insCoBranch As Entities.InsCoBranch) As Entities.InsCoSelectionList

    '        Dim params() As SqlParameter = New SqlParameter(2) {}

    '        params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
    '        params(0).Value = insCoBranch.InsCoID
    '        params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
    '        params(1).Value = insCoBranch.InsCoBranchID
    '        params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
    '        params(2).Value = insCoBranch.BranchId

    '        Try
    '            Dim oReturnValue As New Entities.InsCoSelectionList

    '            oReturnValue.ListData = SqlHelper.ExecuteDataset(insCoBranch.strConnection, CommandType.StoredProcedure, LIST_INSCOMPANY_BRANCH_BYID, params).Tables(0)

    '            Return oReturnValue
    '            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

    '        Catch ex As Exception
    '            writeexception(ex.Message)
    '            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

    '        End Try
    '    End Function

#Region " InsCompanyBranchSave"
    Public Function InsCompanyBranchSave(ByVal customClass As Parameter.eInsuranceCompany, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal, ByVal oClassBankAccount As Parameter.BankAccount) As Parameter.eInsuranceCompany
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        ' Dim params(39) As SqlParameter

        Dim objCon As New SqlConnection(customClass.strConnection)

        Dim ErrMessage As String = ""
        Dim SP As String
        If customClass.Command = "A" Then
            SP = "spInsCoBranchAdd"
        ElseIf customClass.Command = "E" Then
            SP = "spInsCoBranchEdit"
        End If
        Try
            params.Add(New SqlParameter("@MaskAssID", SqlDbType.Char, 10) With {.Value = customClass.MaskAssID})
            params.Add(New SqlParameter("@MaskAssBranchID", SqlDbType.Char, 10) With {.Value = customClass.MaskAssBranchID})
            params.Add(New SqlParameter("@BranchId", SqlDbType.Char, 3) With {.Value = customClass.BranchId})
            params.Add(New SqlParameter("@MaskAssBranchName", SqlDbType.VarChar, 50) With {.Value = customClass.MaskAssBranchName})
            params.Add(New SqlParameter("@Address", SqlDbType.VarChar, 100) With {.Value = oClassAddress.Address})
            params.Add(New SqlParameter("@RT", SqlDbType.Char, 3) With {.Value = oClassAddress.RT})
            params.Add(New SqlParameter("@RW", SqlDbType.Char, 3) With {.Value = oClassAddress.RW})
            params.Add(New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30) With {.Value = oClassAddress.Kelurahan})
            params.Add(New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30) With {.Value = oClassAddress.Kecamatan})
            params.Add(New SqlParameter("@City", SqlDbType.VarChar, 30) With {.Value = oClassAddress.City})
            params.Add(New SqlParameter("@ZipCode", SqlDbType.Char, 5) With {.Value = oClassAddress.ZipCode})
            params.Add(New SqlParameter("@AreaPhone1", SqlDbType.Char, 4) With {.Value = oClassAddress.AreaPhone1})
            params.Add(New SqlParameter("@Phone1", SqlDbType.Char, 15) With {.Value = oClassAddress.Phone1})
            params.Add(New SqlParameter("@AreaPhone2", SqlDbType.Char, 4) With {.Value = oClassAddress.AreaPhone2})
            params.Add(New SqlParameter("@Phone2", SqlDbType.Char, 15) With {.Value = oClassAddress.Phone2})
            params.Add(New SqlParameter("@AreaFax", SqlDbType.VarChar, 4) With {.Value = oClassAddress.AreaFax})
            params.Add(New SqlParameter("@Fax", SqlDbType.VarChar, 10) With {.Value = oClassAddress.Fax})


            params.Add(New SqlParameter("@CPersonName", SqlDbType.VarChar, 50) With {.Value = oClassPersonal.PersonName})
            params.Add(New SqlParameter("@CPersonTitle", SqlDbType.VarChar, 50) With {.Value = oClassPersonal.PersonTitle})
            params.Add(New SqlParameter("@Email", SqlDbType.VarChar, 30) With {.Value = oClassPersonal.Email})
            params.Add(New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20) With {.Value = oClassPersonal.MobilePhone})

            params.Add(New SqlParameter("@BankName", SqlDbType.Char, 5) With {.Value = oClassBankAccount.BankName})
            params.Add(New SqlParameter("@BankBranchID", SqlDbType.VarChar, 50) With {.Value = oClassBankAccount.BankBranchId})
            params.Add(New SqlParameter("@AccountNo", SqlDbType.VarChar, 25) With {.Value = oClassBankAccount.BankAccountID})
            params.Add(New SqlParameter("@AccountName", SqlDbType.VarChar, 50) With {.Value = oClassBankAccount.BankAccountName})

            params.Add(New SqlParameter("@TermOfPayment", SqlDbType.Int) With {.Value = customClass.TermOfPayment})
            params.Add(New SqlParameter("@AdminFee", SqlDbType.Float) With {.Value = customClass.AdminFee})
            params.Add(New SqlParameter("@StampDutyFee", SqlDbType.Float) With {.Value = customClass.StampDutyFee})

            params.Add(New SqlParameter("@DtmUpd", SqlDbType.DateTime) With {.Value = Now()})
            'params(29) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            'params(29).Direction = ParameterDirection.Output
            Dim errPrm = New SqlParameter("@Err", SqlDbType.VarChar, 100) With {.Direction = ParameterDirection.Output}
            params.Add(errPrm)
            params.Add(New SqlParameter("@LoadingFeeAllRisk", SqlDbType.SmallInt) With {.Value = customClass.LoadingFeeAllRisk})
            params.Add(New SqlParameter("@LoadingFeeTLO", SqlDbType.SmallInt) With {.Value = customClass.LoadingFeeTLO})

            params.Add(New SqlParameter("@Fee", SqlDbType.Decimal) With {.Value = customClass.Fee})
            params.Add(New SqlParameter("@Komisi", SqlDbType.Decimal) With {.Value = customClass.Komisi})
            params.Add(New SqlParameter("@PPh23", SqlDbType.Decimal) With {.Value = customClass.PPh23})
            params.Add(New SqlParameter("@PPN", SqlDbType.Decimal) With {.Value = customClass.PPN})
            params.Add(New SqlParameter("@PPh23CertifiedByIns", SqlDbType.Bit) With {.Value = customClass.PPh23CertifiedByIns})
            params.Add(New SqlParameter("@PPNCertifiedByIns", SqlDbType.Bit) With {.Value = customClass.PPNCertifiedByIns})
            params.Add(New SqlParameter("@AllRiskOptions", SqlDbType.VarChar) With {.Value = customClass.AllRiskOptions})
            params.Add(New SqlParameter("@TLOOptions", SqlDbType.VarChar) With {.Value = customClass.TLOOptions})
            params.Add(New SqlParameter("@Discount", SqlDbType.VarChar) With {.Value = customClass.Dist})
            params.Add(New SqlParameter("@BiayaOpex", SqlDbType.VarChar) With {.Value = customClass.BiayaOpex})
            params.Add(New SqlParameter("@NoClaimBonus", SqlDbType.VarChar) With {.Value = customClass.NoClaimBonus})
            params.Add(New SqlParameter("@PolisFeeCreditProtection", SqlDbType.Decimal) With {.Value = customClass.PolisFeeCp})
            params.Add(New SqlParameter("@PolisFeeJaminanKredit", SqlDbType.Decimal) With {.Value = customClass.PolisFeeJk})
                '   SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, SP, params)
                SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, SP, params.ToArray())
                ErrMessage = CType(errPrm.Value, String)

                customClass.output = ErrMessage
                Return customClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.daInsuranceCompany.InsCompanyBranchSave")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try

    End Function

   

#End Region
#Region "Save Edit"
    Public Function InsCompanyBranchSaveEdit(ByVal oClassEdit As Parameter.eInsuranceCompany) As Parameter.eInsuranceCompany
        Dim params() As SqlParameter = New SqlParameter(5) {}

        Dim objCon As New SqlConnection(oClassEdit.strConnection)
        Dim transaction As SqlTransaction
        Dim ErrMessage As String = ""

        Try
            params(0) = New SqlParameter("@InsCoId", SqlDbType.Char, 10)
            params(0).Value = oClassEdit.InsCoID.Trim
            params(1) = New SqlParameter("@strID", SqlDbType.VarChar, 5000)
            params(1).Value = oClassEdit.strID
            params(2) = New SqlParameter("@strBranch", SqlDbType.VarChar, 5000)
            params(2).Value = oClassEdit.strBranch
            params(3) = New SqlParameter("@strState", SqlDbType.VarChar, 5000)
            params(3).Value = oClassEdit.strState
            params(4) = New SqlParameter("@NumRows", SqlDbType.Int)
            params(4).Value = oClassEdit.NumRows
            params(5) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(5).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oClassEdit.strConnection, CommandType.StoredProcedure, "spShadowInsCoBranchEdit", params)

            ErrMessage = CType(params(5).Value, String)
            'If ErrMessage <> "" Then
            '    oReturnValue.Output = ErrMessage
            '    Return oReturnValue
            '    Exit Function
            'End If
            oClassEdit.output = ErrMessage
            Return oClassEdit
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.daInsuranceCompany.InsCompanyBranchSave")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
#End Region

    '#Region " InsCompanyBranchEdit"
    '    Public Sub InsCompanyBranchEdit(ByVal customClass As Entities.InsCoBranch, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal, ByVal oClassBankAccount As Parameter.BankAccount)

    '        Dim params(28) As SqlParameter

    '        params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
    '        params(0).Value = customClass.InsCoID

    '        params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 10)
    '        params(1).Value = customClass.BranchId
    '        params(2) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
    '        params(2).Value = customClass.InsCoBranchID
    '        params(3) = New SqlParameter("@InsCoBranchName", SqlDbType.VarChar, 50)
    '        params(3).Value = customClass.InsCoBranchName


    '        params(4) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
    '        params(4).Value = oClassAddress.Address
    '        params(5) = New SqlParameter("@RT", SqlDbType.Char, 3)
    '        params(5).Value = oClassAddress.RT
    '        params(6) = New SqlParameter("@RW", SqlDbType.Char, 3)
    '        params(6).Value = oClassAddress.RW

    '        params(7) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
    '        params(7).Value = oClassAddress.Kelurahan
    '        params(8) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
    '        params(8).Value = oClassAddress.Kecamatan
    '        params(9) = New SqlParameter("@City", SqlDbType.VarChar, 30)
    '        params(9).Value = oClassAddress.City
    '        params(10) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
    '        params(10).Value = oClassAddress.ZipCode
    '        params(11) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
    '        params(11).Value = oClassAddress.AreaPhone1
    '        params(12) = New SqlParameter("@Phone1", SqlDbType.Char, 15)
    '        params(12).Value = oClassAddress.Phone1
    '        params(13) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
    '        params(13).Value = oClassAddress.AreaPhone2
    '        params(14) = New SqlParameter("@Phone2", SqlDbType.Char, 15)
    '        params(14).Value = oClassAddress.Phone2
    '        params(15) = New SqlParameter("@AreaFax", SqlDbType.VarChar, 4)
    '        params(15).Value = oClassAddress.AreaFax
    '        params(16) = New SqlParameter("@Fax", SqlDbType.VarChar, 10)
    '        params(16).Value = oClassAddress.Fax


    '        params(17) = New SqlParameter("@CPersonName", SqlDbType.VarChar, 50)
    '        params(17).Value = oClassPersonal.PersonName
    '        params(18) = New SqlParameter("@CPersonTitle", SqlDbType.VarChar, 50)
    '        params(18).Value = oClassPersonal.PersonTitle
    '        params(19) = New SqlParameter("@Email", SqlDbType.VarChar, 30)
    '        params(19).Value = oClassPersonal.Email
    '        params(20) = New SqlParameter("@MobilePhone", SqlDbType.VarChar, 20)
    '        params(20).Value = oClassPersonal.MobilePhone

    '        params(21) = New SqlParameter("@BankName", SqlDbType.Char, 5)
    '        params(21).Value = oClassBankAccount.BankName
    '        params(22) = New SqlParameter("@BankBranch", SqlDbType.VarChar, 50)
    '        params(22).Value = oClassBankAccount.BankBranch
    '        params(23) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 25)
    '        params(23).Value = oClassBankAccount.BankAccountID
    '        params(24) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
    '        params(24).Value = oClassBankAccount.BankAccountName

    '        params(25) = New SqlParameter("@TermOfPayment", SqlDbType.Int)
    '        params(25).Value = customClass.TermOfPayment
    '        params(26) = New SqlParameter("@AdminFee", SqlDbType.Float)
    '        params(26).Value = customClass.AdminFee
    '        params(27) = New SqlParameter("@StampDutyFee", SqlDbType.Float)
    '        params(27).Value = customClass.StampDutyFee

    '        params(28) = New SqlParameter("@DtmUpd", SqlDbType.DateTime)
    '        params(28).Value = Now()


    '        Dim reader As SqlDataReader
    '        Try
    '            Dim oReturnValue As New Entities.InsCo
    '            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, INSCOMPANY_BRANCH_EDT, params)

    '        Catch ex As Exception
    '            writeexception(ex.Message)
    '            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
    '            Throw New Exception("INFO: Unable to edit the selected record")

    '        End Try

    '    End Sub
    '#End Region

#Region " InsCompanyBranchDelete"
    Public Sub InsCompanyBranchDelete(ByVal customClass As Parameter.eInsuranceCompany)

        Dim params(2) As SqlParameter

        params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
        params(0).Value = customClass.InsCoID
        params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
        params(1).Value = customClass.InsCoBranchID
        params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(2).Value = customClass.BranchId

        Dim reader As SqlDataReader
        Try
            Dim oReturnValue As New Parameter.eInsuranceCompany
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, INSCOMPANY_BRANCH_DEL, params)

        Catch ex As Exception
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")

        End Try
    End Sub
#End Region

#Region " RateSave"
    Public Function RateSave(ByVal InsCoRate As Parameter.eInsuranceCompany) As Parameter.eInsuranceCompany

        Dim params(10) As SqlParameter
        Dim paramsLoadingRate(6) As SqlParameter
        Dim paramsLoadingRateEdit(9) As SqlParameter

        Dim ErrMessage As String = ""
        Dim SP As String
        If InsCoRate.Command = "A" Then

            Try
                With InsCoRate
                    params(0) = New SqlParameter("@MaskAssID", SqlDbType.Char, 10)
                    params(0).Value = .InsCoID.Trim
                    params(1) = New SqlParameter("@MaskAssBranchID", SqlDbType.Char, 10)
                    params(1).Value = .InsCoBranchID.Trim
                    params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                    params(2).Value = .BranchId.Trim
                    params(3) = New SqlParameter("@InsuranceAssetType", SqlDbType.Char, 2)
                    params(3).Value = .insAssettype.Trim
                    params(4) = New SqlParameter("@AssetUsage", SqlDbType.Char, 1)
                    params(4).Value = .AssetUssage.Trim
                    params(5) = New SqlParameter("@AssetNewUsed", SqlDbType.Char, 1)
                    params(5).Value = .newused.Trim
                    params(6) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 3)
                    params(6).Value = .CoverageType.Trim
                    params(7) = New SqlParameter("@RateCardType", SqlDbType.VarChar, 50)
                    params(7).Value = .RateCardType
                    params(8) = New SqlParameter("@SumInsuredFrom", SqlDbType.Float)
                    params(8).Value = .SumInsuredFrom
                    params(9) = New SqlParameter("@SumInsuredTo", SqlDbType.Float)
                    params(9).Value = .SumInsuredTo
                    params(10) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
                    params(10).Direction = ParameterDirection.Output
                End With

                SqlHelper.ExecuteNonQuery(InsCoRate.strConnection, CommandType.StoredProcedure, "spShadowInsuranceCompanyBranchRateSaveAdd", params)
                ErrMessage = CType(params(10).Value, String)
                InsCoRate.output = ErrMessage
                Return InsCoRate
            Catch ex As Exception
                Throw New Exception("Error On DataAccess.Insurance.daInsuranceCompany.RateSave")
            Finally
                If Not m_connection Is Nothing Then
                    m_connection.Close()
                End If
            End Try

        ElseIf InsCoRate.Command = "E" Then


            Try
                With InsCoRate
                    params(0) = New SqlParameter("@MaskAssID", SqlDbType.Char, 10)
                    params(0).Value = .InsCoID.Trim
                    params(1) = New SqlParameter("@MaskAssBranchID", SqlDbType.Char, 10)
                    params(1).Value = .InsCoBranchID.Trim
                    params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                    params(2).Value = .BranchId.Trim
                    params(3) = New SqlParameter("@InsuranceAssetType", SqlDbType.Char, 2)
                    params(3).Value = .insAssettype.Trim
                    params(4) = New SqlParameter("@AssetUsage", SqlDbType.Char, 1)
                    params(4).Value = .AssetUssage.Trim
                    params(5) = New SqlParameter("@AssetNewUsed", SqlDbType.Char, 1)
                    params(5).Value = .newused.Trim
                    params(6) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 3)
                    params(6).Value = .CoverageType.Trim
                    params(7) = New SqlParameter("@RateCardType", SqlDbType.VarChar, 50)
                    params(7).Value = .RateCardType
                    params(8) = New SqlParameter("@SumInsuredFrom", SqlDbType.Float)
                    params(8).Value = .SumInsuredFrom
                    params(9) = New SqlParameter("@SumInsuredTo", SqlDbType.Float)
                    params(9).Value = .SumInsuredTo
                    params(10) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
                    params(10).Direction = ParameterDirection.Output
                End With

                SqlHelper.ExecuteNonQuery(InsCoRate.strConnection, CommandType.StoredProcedure, "spShadowInsuranceCompanyBranchRateSaveEdit", params)
                ErrMessage = CType(params(10).Value, String)
                InsCoRate.output = ErrMessage
                Return InsCoRate
            Catch ex As Exception
                Throw New Exception("Error On DataAccess.Insurance.daInsuranceCompany.RateSave")
            Finally
                If Not m_connection Is Nothing Then
                    m_connection.Close()
                End If
            End Try

        ElseIf InsCoRate.FormID = "LOADINGRATE" And InsCoRate.Command = "Add" Then
            SP = "spInsuranceLoadingRateSaveAdd"
            Try
                With InsCoRate
                    paramsLoadingRate(0) = New SqlParameter("@MaskAssID", SqlDbType.Char, 10)
                    paramsLoadingRate(0).Value = .InsCoID.Trim
                    paramsLoadingRate(1) = New SqlParameter("@MaskAssBranchID", SqlDbType.Char, 10)
                    paramsLoadingRate(1).Value = .InsCoBranchID.Trim
                    paramsLoadingRate(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                    paramsLoadingRate(2).Value = .BranchId.Trim
                    paramsLoadingRate(3) = New SqlParameter("@CategoryType", SqlDbType.VarChar, 10)
                    paramsLoadingRate(3).Value = .CategoryType.Trim
                    paramsLoadingRate(4) = New SqlParameter("@UsiaAsset", SqlDbType.SmallInt)
                    paramsLoadingRate(4).Value = .UsiaAsset
                    paramsLoadingRate(5) = New SqlParameter("@LoadingRate", SqlDbType.Decimal)
                    paramsLoadingRate(5).Value = .LoadingRate
                    paramsLoadingRate(6) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
                    paramsLoadingRate(6).Direction = ParameterDirection.Output
                End With

                SqlHelper.ExecuteNonQuery(InsCoRate.strConnection, CommandType.StoredProcedure, SP, paramsLoadingRate)
                ErrMessage = CType(paramsLoadingRate(6).Value, String)
                InsCoRate.output = ErrMessage
                Return InsCoRate
            Catch ex As Exception
                Throw New Exception("Error On DataAccess.Insurance.daInsuranceCompany.RateSave")
            Finally
                If Not m_connection Is Nothing Then
                    m_connection.Close()
                End If
            End Try

        ElseIf InsCoRate.FormID = "LOADINGRATE" And InsCoRate.Command = "Edit" Then
            SP = "spInsuranceLoadingRateSaveEdit"
            Try
                With InsCoRate
                    paramsLoadingRateEdit(0) = New SqlParameter("@MaskAssID", SqlDbType.Char, 10)
                    paramsLoadingRateEdit(0).Value = .InsCoID.Trim
                    paramsLoadingRateEdit(1) = New SqlParameter("@MaskAssBranchID", SqlDbType.Char, 10)
                    paramsLoadingRateEdit(1).Value = .InsCoBranchID.Trim
                    paramsLoadingRateEdit(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                    paramsLoadingRateEdit(2).Value = .BranchId.Trim
                    paramsLoadingRateEdit(3) = New SqlParameter("@CategoryType", SqlDbType.VarChar, 10)
                    paramsLoadingRateEdit(3).Value = .CategoryType.Trim
                    paramsLoadingRateEdit(4) = New SqlParameter("@UsiaAsset", SqlDbType.SmallInt)
                    paramsLoadingRateEdit(4).Value = .UsiaAsset
                    paramsLoadingRateEdit(5) = New SqlParameter("@LoadingRate", SqlDbType.Decimal)
                    paramsLoadingRateEdit(5).Value = .LoadingRate
                    paramsLoadingRateEdit(6) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
                    paramsLoadingRateEdit(6).Direction = ParameterDirection.Output

                    paramsLoadingRateEdit(7) = New SqlParameter("@CategoryTypeNew", SqlDbType.VarChar, 10)
                    paramsLoadingRateEdit(7).Value = .CategoryTypeNew.Trim
                    paramsLoadingRateEdit(8) = New SqlParameter("@UsiaAssetNew", SqlDbType.SmallInt)
                    paramsLoadingRateEdit(8).Value = .UsiaAssetNew
                    paramsLoadingRateEdit(9) = New SqlParameter("@LoadingRateNew", SqlDbType.Decimal)
                    paramsLoadingRateEdit(9).Value = .LoadingRateNew
                End With

                SqlHelper.ExecuteNonQuery(InsCoRate.strConnection, CommandType.StoredProcedure, SP, paramsLoadingRateEdit)
                ErrMessage = CType(paramsLoadingRateEdit(6).Value, String)
                InsCoRate.output = ErrMessage
                Return InsCoRate
            Catch ex As Exception
                Throw New Exception(ex.Message)
            Finally
                If Not m_connection Is Nothing Then
                    m_connection.Close()
                End If
            End Try
        End If
    End Function


#End Region

    '    Public Function GetRateList(ByVal InsCoRate As Entities.InsCoRate) As Entities.InsCoSelectionList
    '        Dim params(7) As SqlParameter

    '        With InsCoRate
    '            params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
    '            params(0).Value = .InsCoID
    '            params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
    '            params(1).Value = .InsCoBranchID
    '            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
    '            params(2).Value = .BranchId
    '            params(3) = New SqlParameter("@CurrentPage", SqlDbType.Int)
    '            params(3).Value = .CurrentPage
    '            params(4) = New SqlParameter("@PageSize", SqlDbType.Int)
    '            params(4).Value = .PageSize
    '            params(5) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
    '            params(5).Value = .WhereCond
    '            params(6) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
    '            params(6).Value = .SortBy

    '            params(7) = New SqlParameter("@TotalRecords", SqlDbType.Int)
    '            params(7).Direction = ParameterDirection.Output
    '        End With

    '        Try
    '            Dim oReturnValue As New Entities.InsCoSelectionList

    '            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoRate.strConnection, CommandType.StoredProcedure, LIST_RATE, params).Tables(0)
    '            oReturnValue.TotalRecord = CType(params(7).Value, Int16)

    '            Return oReturnValue
    '            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

    '        Catch ex As Exception
    '            writeexception(ex.Message)
    '            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

    '        End Try
    '    End Function

    '    Public Function GetRateByID(ByVal InsCoRate As Entities.InsCoRate) As Entities.InsCoSelectionList
    '        Dim params(7) As SqlParameter

    '        With InsCoRate
    '            params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
    '            params(0).Value = .InsCoID
    '            params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
    '            params(1).Value = .InsCoBranchID
    '            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
    '            params(2).Value = .BranchId
    '            params(3) = New SqlParameter("@InsuranceType", SqlDbType.Char, 2)
    '            params(3).Value = .insAssettype
    '            params(4) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 10)
    '            params(4).Value = .CoverageType
    '            params(5) = New SqlParameter("@UsageID", SqlDbType.VarChar, 10)
    '            params(5).Value = .AssetUssage
    '            params(6) = New SqlParameter("@NewUsed", SqlDbType.Char, 1)
    '            params(6).Value = .newused
    '            params(7) = New SqlParameter("@YearNum", SqlDbType.VarChar, 4)
    '            params(7).Value = .yearnum


    '        End With

    '        Try
    '            Dim oReturnValue As New Entities.InsCoSelectionList

    '            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoRate.strConnection, CommandType.StoredProcedure, LIST_RATE_BYID, params).Tables(0)

    '            Return oReturnValue
    '            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

    '        Catch ex As Exception
    '            writeexception(ex.Message)
    '            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

    '        End Try
    '    End Function

    '    Public Function GetTPLList(ByVal InsCoTPL As Entities.InsCoTPL) As Entities.InsCoSelectionList
    '        Dim params(7) As SqlParameter

    '        With InsCoTPL
    '            params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
    '            params(0).Value = .InsCoID
    '            params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
    '            params(1).Value = .InsCoBranchID
    '            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
    '            params(2).Value = .BranchId

    '            params(3) = New SqlParameter("@CurrentPage", SqlDbType.Int)
    '            params(3).Value = .CurrentPage
    '            params(4) = New SqlParameter("@PageSize", SqlDbType.Int)
    '            params(4).Value = .PageSize
    '            params(5) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
    '            params(5).Value = .WhereCond
    '            params(6) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
    '            params(6).Value = .SortBy

    '            params(7) = New SqlParameter("@TotalRecords", SqlDbType.Int)
    '            params(7).Direction = ParameterDirection.Output
    '        End With

    '        Try
    '            Dim oReturnValue As New Entities.InsCoSelectionList

    '            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoTPL.strConnection, CommandType.StoredProcedure, LIST_TPL, params).Tables(0)
    '            oReturnValue.TotalRecord = CType(params(7).Value, Int16)

    '            Return oReturnValue
    '            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

    '        Catch ex As Exception
    '            writeexception(ex.Message)
    '            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

    '        End Try
    '    End Function

    '    Public Function GetTPLByID(ByVal InsCoTPL As Entities.InsCoTPL) As Entities.InsCoSelectionList
    '        Dim params(3) As SqlParameter

    '        With InsCoTPL
    '            params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
    '            params(0).Value = .InsCoID
    '            params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
    '            params(1).Value = .InsCoBranchID
    '            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
    '            params(2).Value = .BranchId
    '            params(3) = New SqlParameter("@TPLAmount", SqlDbType.Float)
    '            params(3).Value = .tplamount
    '        End With

    '        Try
    '            Dim oReturnValue As New Entities.InsCoSelectionList

    '            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoTPL.strConnection, CommandType.StoredProcedure, LIST_TPL_BYID, params).Tables(0)

    '            Return oReturnValue
    '            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

    '        Catch ex As Exception
    '            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

    '        End Try
    '    End Function


    '#Region "GetTPLReport"
    '    Public Function GetTPLReport(ByVal InsCoTPL As Entities.InsCoTPL) As Entities.InsCoSelectionList
    '        Dim params(1) As SqlParameter

    '        With InsCoTPL

    '            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
    '            params(0).Value = .BranchId
    '            params(1) = New SqlParameter("@InsuranceComBranchID", SqlDbType.VarChar, 20)
    '            params(1).Value = .InsCoBranchID

    '        End With

    '        Try
    '            Dim oReturnValue As New Entities.InsCoSelectionList

    '            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoTPL.strConnection, CommandType.StoredProcedure, TPL_RPT, params).Tables(0)

    '            Return oReturnValue
    '            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

    '        Catch ex As Exception
    '            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

    '        End Try
    '    End Function
    '#End Region


    '    Public Function GetRateReport(ByVal InsCoRate As Entities.InsCoRate) As Entities.InsCoSelectionList
    '        Dim params(1) As SqlParameter

    '        With InsCoRate

    '            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
    '            params(0).Value = .BranchId
    '            params(1) = New SqlParameter("@InsuranceComBranch", SqlDbType.Char, 10)
    '            params(1).Value = .InsCoBranchID

    '        End With

    '        Try
    '            Dim oReturnValue As New Entities.InsCoSelectionList

    '            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoRate.strConnection, CommandType.StoredProcedure, RATE_RPT, params).Tables(0)

    '            Return oReturnValue
    '            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

    '        Catch ex As Exception
    '            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

    '        End Try
    '    End Function


#Region " TPLSave"
    Public Function TPLSave(ByVal InsCoTPL As Parameter.eInsuranceCompany) As Parameter.eInsuranceCompany

        Dim params(7) As SqlParameter

        Dim ErrMessage As String = ""
        Dim SP As String
        If InsCoTPL.Command = "A" Then
            SP = "spShadowInsuranceCompanyBranchTPLSaveAdd"
            Try

                params(0) = New SqlParameter("@MaskAssID", SqlDbType.Char, 10)
                params(0).Value = InsCoTPL.InsCoID
                params(1) = New SqlParameter("@MaskAssBranchID", SqlDbType.Char, 10)
                params(1).Value = InsCoTPL.InsCoBranchID
                params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 10)
                params(2).Value = InsCoTPL.BranchId
                params(3) = New SqlParameter("@TPLAmountFrom", SqlDbType.Float)
                params(3).Value = InsCoTPL.AmountFrom
                params(4) = New SqlParameter("@TPLAmountTo", SqlDbType.Float)
                params(4).Value = InsCoTPL.AmountTo
                params(5) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
                params(5).Direction = ParameterDirection.Output
                params(6) = New SqlParameter("@TPLRate", SqlDbType.Float)
                params(6).Value = InsCoTPL.Rate
                params(7) = New SqlParameter("@TPLInsRateCategory", SqlDbType.VarChar)
                params(7).Value = InsCoTPL.InsRateCategory

                SqlHelper.ExecuteNonQuery(InsCoTPL.strConnection, CommandType.StoredProcedure, SP, params)

                ErrMessage = CType(params(5).Value, String)

                InsCoTPL.output = ErrMessage
                Return InsCoTPL
            Catch ex As Exception
                Throw New Exception("Error On DataAccess.Insurance.daInsuranceCompany.TPLSave")
            Finally
                If Not m_connection Is Nothing Then
                    m_connection.Close()
                End If
            End Try
        ElseIf InsCoTPL.Command = "E" Then
            SP = "spShadowInsuranceCompanyBranchTPLSaveEdit"
        End If
        Try

            params(0) = New SqlParameter("@MaskAssID", SqlDbType.Char, 10)
            params(0).Value = InsCoTPL.InsCoID
            params(1) = New SqlParameter("@MaskAssBranchID", SqlDbType.Char, 10)
            params(1).Value = InsCoTPL.InsCoBranchID
            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 10)
            params(2).Value = InsCoTPL.BranchId
            params(3) = New SqlParameter("@TPLAmountFrom", SqlDbType.Float)
            params(3).Value = InsCoTPL.AmountFrom
            params(4) = New SqlParameter("@TPLAmountTo", SqlDbType.Float)
            params(4).Value = InsCoTPL.AmountTo
            params(5) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(5).Direction = ParameterDirection.Output
            params(6) = New SqlParameter("@TPLRate", SqlDbType.Float)
            params(6).Value = InsCoTPL.Rate
            params(7) = New SqlParameter("@TPLInsRateCategory", SqlDbType.VarChar)
            params(7).Value = InsCoTPL.InsRateCategory

            SqlHelper.ExecuteNonQuery(InsCoTPL.strConnection, CommandType.StoredProcedure, SP, params)

            ErrMessage = CType(params(5).Value, String)

            InsCoTPL.output = ErrMessage
            Return InsCoTPL
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.daInsuranceCompany.TPLSave")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try

    End Function
#End Region

    '#Region " TPLEdit"
    '    Public Sub TPLEdit(ByVal InsCoTPL As Entities.InsCoTPL)

    '        Dim params(4) As SqlParameter

    '        params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
    '        params(0).Value = InsCoTPL.InsCoID
    '        params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
    '        params(1).Value = InsCoTPL.InsCoBranchID
    '        params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 10)
    '        params(2).Value = InsCoTPL.BranchId
    '        params(3) = New SqlParameter("@TPLAmount", SqlDbType.Float)
    '        params(3).Value = InsCoTPL.tplamount
    '        params(4) = New SqlParameter("@TPLPremium", SqlDbType.Float)
    '        params(4).Value = InsCoTPL.TPLPremium


    '        Dim reader As SqlDataReader
    '        Try
    '            Dim oReturnValue As New Entities.InsCo
    '            SqlHelper.ExecuteNonQuery(InsCoTPL.strConnection, CommandType.StoredProcedure, TPL_EDT, params)

    '        Catch ex As Exception
    '            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
    '            Throw New Exception("Error On DataAccess.InsCo.ActionAdd")

    '        End Try

    '    End Sub
    '#End Region

#Region " TPLDelete"
    Public Sub TPLDelete(ByVal InsCoTPL As Parameter.eInsuranceCompany)

        Dim params(3) As SqlParameter

        params(0) = New SqlParameter("@InsCoID", SqlDbType.VarChar, 10)
        params(0).Value = InsCoTPL.InsCoID
        params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.VarChar, 10)
        params(1).Value = InsCoTPL.InsCoBranchID
        params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(2).Value = InsCoTPL.BranchId
        params(3) = New SqlParameter("@TPLAmount", SqlDbType.Float)
        params(3).Value = InsCoTPL.TPLAmount

        Dim reader As SqlDataReader
        Try
            Dim oReturnValue As New Parameter.eInsuranceCompany
            SqlHelper.ExecuteNonQuery(InsCoTPL.strConnection, CommandType.StoredProcedure, TPL_DEL, params)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("ERROR: Unable to delete the selected record. Record already used !")

        End Try

    End Sub
#End Region



    '#Region " RateEdit"
    '    Public Sub RateEdit(ByVal InsCoRate As Entities.InsCoRate)


    '        Dim params(12) As SqlParameter

    '        With InsCoRate
    '            params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
    '            params(0).Value = .InsCoID.Trim
    '            params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
    '            params(1).Value = .InsCoBranchID.Trim
    '            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
    '            params(2).Value = .BranchId.Trim
    '            params(3) = New SqlParameter("@InsuranceAssetType", SqlDbType.Char, 2)
    '            params(3).Value = .insAssettype.Trim
    '            params(4) = New SqlParameter("@AssetUsage", SqlDbType.Char, 1)
    '            params(4).Value = .AssetUssage.Trim
    '            params(5) = New SqlParameter("@AssetNewUsed", SqlDbType.Char, 1)
    '            params(5).Value = .newused.Trim
    '            params(6) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 3)
    '            params(6).Value = .CoverageType.Trim
    '            params(7) = New SqlParameter("@YearNum", SqlDbType.VarChar, 4)
    '            params(7).Value = .yearnum
    '            params(8) = New SqlParameter("@rateInsCo", SqlDbType.Decimal)
    '            params(8).Value = .ratetoInsco
    '            params(9) = New SqlParameter("@SRCCRate", SqlDbType.Decimal)
    '            params(9).Value = .srccRate
    '            params(10) = New SqlParameter("@FloodRate", SqlDbType.Decimal)
    '            params(10).Value = .FloodRate
    '            params(11) = New SqlParameter("@LoadingRate", SqlDbType.Decimal)
    '            params(11).Value = .loadingrate
    '            params(12) = New SqlParameter("@SumInsured ", SqlDbType.Decimal)
    '            params(12).Value = .sumInsuranced

    '        End With

    '        Dim reader As SqlDataReader
    '        Try
    '            Dim oReturnValue As New Entities.InsCoRate
    '            SqlHelper.ExecuteNonQuery(InsCoRate.strConnection, CommandType.StoredProcedure, RATE_EDT, params)

    '        Catch ex As Exception

    '        End Try

    '    End Sub
    '#End Region

#Region " RateDelete"
    Public Sub RateDelete(ByVal InsCoRate As Parameter.eInsuranceCompany)

        Dim params(7) As SqlParameter

        With InsCoRate
            params(0) = New SqlParameter("@InsCoID", SqlDbType.Char, 10)
            params(0).Value = .InsCoID
            params(1) = New SqlParameter("@InsCoBranchID", SqlDbType.Char, 10)
            params(1).Value = .InsCoBranchID
            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 10)
            params(2).Value = .BranchId
            params(3) = New SqlParameter("@InsuranceAssetType", SqlDbType.VarChar, 2)
            params(3).Value = .insAssettype
            params(4) = New SqlParameter("@AssetUsage", SqlDbType.VarChar, 10)
            params(4).Value = .AssetUssage
            params(5) = New SqlParameter("@AssetNewUsed", SqlDbType.Char, 4)
            params(5).Value = .newused
            params(6) = New SqlParameter("@CoverageType", SqlDbType.Char, 10)
            params(6).Value = .CoverageType
            params(7) = New SqlParameter("@RateCardType", SqlDbType.VarChar, 50)
            params(7).Value = .RateCardType

        End With



        Dim reader As SqlDataReader
        Try
            Dim oReturnValue As New Parameter.eInsuranceCompany
            SqlHelper.ExecuteNonQuery(InsCoRate.strConnection, CommandType.StoredProcedure, RATE_DEL, params)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("ERROR: Unable to delete the selected record. Record already used !")

        End Try

    End Sub
#End Region

#Region "CopyInsCoBranchRate"
    Public Sub CopyInsCoBranchRate(ByVal InsCoRate As Parameter.eInsuranceCompany)
        Dim params(4) As SqlParameter

        With InsCoRate
            params(0) = New SqlParameter("@InsCoBranchIDFrom", SqlDbType.VarChar, 10)
            params(0).Value = .InsCoBranchIDFrom.Trim
            params(1) = New SqlParameter("@InsCoBranchIDTo", SqlDbType.VarChar, 10)
            params(1).Value = .InsCoBranchIDTo.Trim
            params(2) = New SqlParameter("@InsCoID", SqlDbType.VarChar, 10)
            params(2).Value = .InsCoID.Trim
            params(3) = New SqlParameter("@BranchIDTo", SqlDbType.Char, 3)
            params(3).Value = .BranchIDTo
            params(4) = New SqlParameter("@BranchIDFrom", SqlDbType.Char, 3)
            params(4).Value = .BranchIDFrom

        End With

        Try
            SqlHelper.ExecuteNonQuery(InsCoRate.strConnection, CommandType.StoredProcedure, RATE_INCOBRANCH_COPY, params)
        Catch ex As Exception

        End Try

    End Sub
#End Region

    '#Region " Get The Combo Box"
    '    Public Function GetCoverageTypeCombo(ByVal InsCoBranch As Entities.InsCoBranch) As Entities.InsCoSelectionList

    '        Try
    '            Dim oReturnValue As New Entities.InsCoSelectionList

    '            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoBranch.strConnection, CommandType.StoredProcedure, CBO_COVERAGE).Tables(0)

    '            Return oReturnValue
    '            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

    '        Catch ex As Exception
    '            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

    '        End Try
    '    End Function

    '    Public Function GetInsAssetTypeCombo(ByVal InsCoBranch As Entities.InsCoBranch) As Entities.InsCoSelectionList

    '        Try
    '            Dim oReturnValue As New Entities.InsCoSelectionList

    '            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoBranch.strConnection, CommandType.StoredProcedure, CBO_ASSET).Tables(0)

    '            Return oReturnValue
    '            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

    '        Catch ex As Exception
    '            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

    '        End Try
    '    End Function

    '    Public Function GetAssetUsageCombo(ByVal InsCoBranch As Entities.InsCoBranch) As Entities.InsCoSelectionList

    '        Try
    '            Dim oReturnValue As New Entities.InsCoSelectionList

    '            oReturnValue.ListData = SqlHelper.ExecuteDataset(InsCoBranch.strConnection, CommandType.StoredProcedure, CBO_ASSETUSAGE).Tables(0)

    '            Return oReturnValue
    '            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

    '        Catch ex As Exception
    '            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)

    '        End Try
    '    End Function
    '#End Region

#Region " CoverageSave"
    Public Function CoverageSave(ByVal InsCoCoverage As Parameter.eInsuranceCompany) As Parameter.eInsuranceCompany

        Dim params(10) As SqlParameter

        Dim ErrMessage As String = ""

        Try

            params(0) = New SqlParameter("@MaskAssID", SqlDbType.Char, 10)
            params(0).Value = InsCoCoverage.InsCoID
            params(1) = New SqlParameter("@MaskAssBranchID", SqlDbType.Char, 10)
            params(1).Value = InsCoCoverage.InsCoBranchID
            params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 10)
            params(2).Value = InsCoCoverage.BranchId
            params(3) = New SqlParameter("@strCoverageID", SqlDbType.VarChar, 8000)
            params(3).Value = InsCoCoverage.strCoverageID
            params(4) = New SqlParameter("@strCoverageRate", SqlDbType.VarChar, 8000)
            params(4).Value = InsCoCoverage.strCoverageRate
            params(5) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(5).Direction = ParameterDirection.Output
            params(6) = New SqlParameter("@NumRows", SqlDbType.Int)
            params(6).Value = InsCoCoverage.RowCount
            params(7) = New SqlParameter("@strCoverageType", SqlDbType.VarChar, 8000)
            params(7).Value = InsCoCoverage.strCoverageType
            params(8) = New SqlParameter("@strInsuranceAreaId", SqlDbType.VarChar, 8000)
            params(8).Value = InsCoCoverage.strInsuranceAreaId
            params(9) = New SqlParameter("@strRateBottom", SqlDbType.VarChar, 8000)
            params(9).Value = InsCoCoverage.strRateBottom
            params(10) = New SqlParameter("@strRateTop", SqlDbType.VarChar, 8000)
            params(10).Value = InsCoCoverage.strRateTop

            SqlHelper.ExecuteNonQuery(InsCoCoverage.strConnection, CommandType.StoredProcedure, COVERAGE_SAVE, params)

            ErrMessage = CType(params(5).Value, String)

            InsCoCoverage.output = ErrMessage
            Return InsCoCoverage
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.daInsuranceCompany.CoverageSave")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try

    End Function
#End Region

End Class
