#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class daInsuranceRateCard
    Private m_connection As SqlConnection

    Public Function GetInsuranceRateCardHSave(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard

        Dim params() As SqlParameter = New SqlParameter(3) {}

        Dim objCon As New SqlConnection(oCustomClass.strConnection)
        Dim transaction As SqlTransaction
        Dim ErrMessage As String = ""
        Dim SP As String
        If oCustomClass.Command = "A" Then
            SP = "spShadowInsuranceRateCardHSaveAdd"
        ElseIf oCustomClass.Command = "A1" Then
            SP = "spShadowInsuranceMasterSellRateHSaveAdd"
        ElseIf oCustomClass.Command = "E1" Then
            SP = "spShadowInsuranceMasterSellRateHSaveEdit"
        ElseIf oCustomClass.Command = "A2" Then
            SP = "spShadowInsuranceMasterHPPRateHSaveAdd"
        ElseIf oCustomClass.Command = "E2" Then
            SP = "spShadowInsuranceMasterHPPRateHSaveEdit"
        ElseIf oCustomClass.Command = "A3" Then
            SP = "spShadowNewInsuranceRateCardHSaveAdd"
        ElseIf oCustomClass.Command = "E3" Then
            SP = "spShadowNewInsuranceRateCardHSaveEdit"
        Else
            SP = "spShadowInsuranceRateCardHSaveEdit"
        End If
        Try
            params(0) = New SqlParameter("@CardID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.CardID.Trim
            params(1) = New SqlParameter("@CardDesc", SqlDbType.VarChar, 100)
            params(1).Value = oCustomClass.CardDesc.Trim
            params(2) = New SqlParameter("@IsActive", SqlDbType.Bit)
            params(2).Value = oCustomClass.IsActive
            params(3) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(3).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SP, params)

            ErrMessage = CType(params(3).Value, String)
            'If ErrMessage <> "" Then
            '    oReturnValue.Output = ErrMessage
            '    Return oReturnValue
            '    Exit Function
            'End If
            oCustomClass.output = ErrMessage
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.daInsuranceRateCard.GetInsuranceRateCardHSave")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try

    End Function

    Public Function GetInsuranceRateCardDSave(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard

        Dim params() As SqlParameter = New SqlParameter(13) {}

        Dim objCon As New SqlConnection(oCustomClass.strConnection)
        Dim transaction As SqlTransaction
        Dim ErrMessage As String = ""
        Dim SP As String
        If oCustomClass.Command = "A" Then
            SP = "spShadowInsuranceRateCardDSaveAdd"
        Else
            SP = "spShadowInsuranceRateCardDSaveEdit"
        End If
        Try
            params(0) = New SqlParameter("@CardID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.CardID.Trim
            params(1) = New SqlParameter("@InsType", SqlDbType.VarChar, 2)
            params(1).Value = oCustomClass.InsType.Trim
            params(2) = New SqlParameter("@UsageID", SqlDbType.Char, 1)
            params(2).Value = oCustomClass.UsageID.Trim
            params(3) = New SqlParameter("@NewUsed", SqlDbType.VarChar, 1)
            params(3).Value = oCustomClass.NewUsed.Trim
            params(4) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 10)
            params(4).Value = oCustomClass.CoverageType.Trim
            params(5) = New SqlParameter("@YearNum ", SqlDbType.SmallInt)
            params(5).Value = oCustomClass.YearNum
            params(6) = New SqlParameter("@RateGroup", SqlDbType.Decimal, 9)
            params(6).Value = oCustomClass.RateGroup
            params(7) = New SqlParameter("@RateNonGroup", SqlDbType.Decimal, 9)
            params(7).Value = oCustomClass.RateNonGroup
            params(8) = New SqlParameter("@RateRepeatOrder", SqlDbType.Decimal, 9)
            params(8).Value = oCustomClass.RateRepeatOrder
            params(9) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(9).Direction = ParameterDirection.Output
            params(10) = New SqlParameter("@SRCCRate", SqlDbType.Decimal, 9)
            params(10).Value = oCustomClass.SRCCRate
            params(11) = New SqlParameter("@FloodRate", SqlDbType.Decimal, 9)
            params(11).Value = oCustomClass.FloodRate
            params(12) = New SqlParameter("@LoadingRate", SqlDbType.Decimal, 9)
            params(12).Value = oCustomClass.LoadingRate
            params(13) = New SqlParameter("@SumIns", SqlDbType.Decimal, 9)
            params(13).Value = oCustomClass.SumIns

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SP, params)

            ErrMessage = CType(params(9).Value, String)
            'If ErrMessage <> "" Then
            '    oReturnValue.Output = ErrMessage
            '    Return oReturnValue
            '    Exit Function
            'End If
            oCustomClass.output = ErrMessage
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.daInsuranceRateCard.GetInsuranceRateCardDSave")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try

    End Function

    Public Function GetInsuranceRateCardDDelete(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String
        Dim params() As SqlParameter = New SqlParameter(6) {}
        Dim ErrMessage As String = ""
        Try
            params(0) = New SqlParameter("@CardID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.CardID.Trim
            params(1) = New SqlParameter("@InsType", SqlDbType.VarChar, 2)
            params(1).Value = oCustomClass.InsType.Trim
            params(2) = New SqlParameter("@UsageID", SqlDbType.VarChar, 10)
            params(2).Value = oCustomClass.UsageID.Trim
            params(3) = New SqlParameter("@NewUsed", SqlDbType.Char, 1)
            params(3).Value = oCustomClass.NewUsed.Trim
            params(4) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 10)
            params(4).Value = oCustomClass.CoverageType.Trim
            params(5) = New SqlParameter("@YearNum", SqlDbType.Int)
            params(5).Value = oCustomClass.YearNum
            params(6) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(6).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spShadowInsuranceRateCardDDelete", params)
            Return CStr(params(6).Value)

        Catch exp As Exception
            Throw New Exception("Error On DataAccess.Insurance.daInsuranceRateCardD.GetInsuranceRateCardDDelete")
        End Try

    End Function

    Public Function GetInsuranceRateCardDCopy(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim ErrMessage As String = ""
        Dim SP As String
        If oCustomClass.Command = "C1" Then
            SP = "spShadowInsuranceMasterSellRateDCopy"
        ElseIf oCustomClass.Command = "C2" Then
            SP = "spShadowInsuranceMasterHPPRateDCopy"
        ElseIf oCustomClass.Command = "C3" Then
            SP = "spShadowNewInsuranceRateCardDCopy"
        ElseIf oCustomClass.Command = "C4" Then
            SP = "spShadowNewInsuranceStdRateCopy"
        Else
            SP = "spShadowInsuranceRateCardDCopy"
        End If
        Try
            params(0) = New SqlParameter("@CardIDDestination", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.CardID.Trim
            params(1) = New SqlParameter("@CardIDSource", SqlDbType.VarChar, 50)
            params(1).Value = oCustomClass.CardIDSource.Trim
            params(2) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(2).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SP, params)
            Return CStr(params(2).Value)

        Catch exp As Exception
            Throw New Exception("Error On DataAccess.Insurance.daInsuranceRateCardD.GetInsuranceRateCardDCopy")
        End Try

    End Function
    Public Function GetInsuranceMasterRateDSave(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard

        Dim params() As SqlParameter

        Dim objCon As New SqlConnection(oCustomClass.strConnection)
        Dim transaction As SqlTransaction
        Dim ErrMessage As String = ""
        Dim SP As String
        If oCustomClass.Command = "A" Then
            SP = "spShadowInsuranceMasterSellRateDSaveAdd"
        ElseIf oCustomClass.Command = "A2" Then
            SP = "spShadowInsuranceMasterHPPRateDSaveAdd"
        ElseIf oCustomClass.Command = "E2" Then
            SP = "spShadowInsuranceMasterHPPRateDSaveEdit"
        Else
            SP = "spShadowInsuranceMasterSellRateDSaveEdit"
        End If
        Try
            If oCustomClass.Command = "A2" Or oCustomClass.Command = "E2" Then
                params = New SqlParameter(13) {}
                params(8) = New SqlParameter("@InsuranceAreaId", SqlDbType.Char, 1)
                params(8).Value = oCustomClass.InsuranceAreaId
                params(9) = New SqlParameter("@OJKSumInsuredFrom", SqlDbType.Decimal)
                params(9).Value = oCustomClass.OJKSumInsuredFrom
                params(10) = New SqlParameter("@OJKSumInsuredTo", SqlDbType.Decimal)
                params(10).Value = oCustomClass.OJKSumInsuredTo
                params(11) = New SqlParameter("@RateBottom", SqlDbType.Decimal)
                params(11).Value = oCustomClass.RateBottom
                params(12) = New SqlParameter("@RateTop", SqlDbType.Decimal)
                params(12).Value = oCustomClass.RateTop
                params(13) = New SqlParameter("@SeqNo ", SqlDbType.Int)
                params(13).Value = oCustomClass.SeqNo
            Else
                params = New SqlParameter(7) {}
            End If

            params(0) = New SqlParameter("@CardID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.CardID.Trim
            params(1) = New SqlParameter("@YearNum ", SqlDbType.SmallInt)
            params(1).Value = oCustomClass.YearNum
            params(2) = New SqlParameter("@RateGroup", SqlDbType.Decimal, 9)
            params(2).Value = oCustomClass.RateGroup
            params(3) = New SqlParameter("@RateNonGroup", SqlDbType.Decimal, 9)
            params(3).Value = oCustomClass.RateNonGroup
            params(4) = New SqlParameter("@RateRepeatOrder", SqlDbType.Decimal, 9)
            params(4).Value = oCustomClass.RateRepeatOrder
            params(5) = New SqlParameter("@RateLoadingPenggunaan", SqlDbType.Decimal, 9)
            params(5).Value = oCustomClass.RateLoadingPenggunaan
            params(6) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(6).Direction = ParameterDirection.Output
            params(7) = New SqlParameter("@SumIns", SqlDbType.Decimal, 9)
            params(7).Value = oCustomClass.SumIns


            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SP, params)

            ErrMessage = CType(params(6).Value, String)
            'If ErrMessage <> "" Then
            '    oReturnValue.Output = ErrMessage
            '    Return oReturnValue
            '    Exit Function
            'End If
            oCustomClass.output = ErrMessage
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.daInsuranceRateCard.GetInsuranceMasterRateDSave")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try
    End Function
    Public Function GetInsuranceMasterRateDDelete(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Dim ErrMessage As String = ""
        Dim SP As String
        If oCustomClass.Command = "D" Then
            SP = "spShadowInsuranceMasterSellRateDDelete"
        ElseIf oCustomClass.Command = "D2" Then
            SP = "spShadowInsuranceMasterHPPRateDDelete"
        End If
        Try
            params(0) = New SqlParameter("@CardID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.CardID.Trim
            params(1) = New SqlParameter("@YearNum", SqlDbType.Int)
            params(1).Value = oCustomClass.YearNum
            'params(2) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            'params(2).Direction = ParameterDirection.Output
            params(2) = New SqlParameter("@InsuranceAreaId", SqlDbType.Char, 1)
            params(2).Value = oCustomClass.InsuranceAreaId.Trim
            params(3) = New SqlParameter("@OJKSumInsuredFrom", SqlDbType.Decimal)
            params(3).Value = oCustomClass.SumFrom
            params(4) = New SqlParameter("@OJKSumInsuredTo", SqlDbType.Decimal)
            params(4).Value = oCustomClass.SumTo
            params(5) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(5).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SP, params)
            'Return CStr(params(2).Value)
            Return CStr(params(5).Value)

        Catch exp As Exception
            Throw New Exception("Error On DataAccess.Insurance.daInsuranceRateCardD.GetInsuranceMasterRateDDelete")
        End Try

    End Function
    Public Function GetNewInsuranceRateCardDSave(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard

        Dim params() As SqlParameter = New SqlParameter(8) {}

        Dim objCon As New SqlConnection(oCustomClass.strConnection)
        Dim transaction As SqlTransaction
        Dim ErrMessage As String = ""
        Dim SP As String
        If oCustomClass.Command = "A" Then
            SP = "spShadowNewInsuranceRateCardDSaveAdd"
        ElseIf oCustomClass.Command = "A1" Then
            SP = "spShadowNewInsuranceStdRateSaveAdd"
        ElseIf oCustomClass.Command = "E1" Then
            SP = "spShadowNewInsuranceStdRateSaveEdit"
        ElseIf oCustomClass.Command = "D" Then
            SP = "spShadowNewInsuranceStdRateDelete"
        Else
            SP = "spShadowNewInsuranceRateCardDSaveEdit"
        End If
        Try
            params(0) = New SqlParameter("@CardID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.CardID.Trim
            params(1) = New SqlParameter("@InsType", SqlDbType.VarChar, 2)
            params(1).Value = oCustomClass.InsType.Trim
            params(2) = New SqlParameter("@UsageID", SqlDbType.Char, 1)
            params(2).Value = oCustomClass.UsageID.Trim
            params(3) = New SqlParameter("@NewUsed", SqlDbType.VarChar, 1)
            params(3).Value = oCustomClass.NewUsed.Trim
            params(4) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 10)
            params(4).Value = oCustomClass.CoverageType.Trim
            params(5) = New SqlParameter("@MasterRate", SqlDbType.VarChar, 50)
            params(5).Value = oCustomClass.MasterRate.Trim
            params(6) = New SqlParameter("@SumFrom", SqlDbType.Decimal)
            params(6).Value = oCustomClass.SumFrom
            params(7) = New SqlParameter("@SumTo", SqlDbType.Decimal)
            params(7).Value = oCustomClass.SumTo
            params(8) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(8).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SP, params)

            ErrMessage = CType(params(8).Value, String)
            'If ErrMessage <> "" Then
            '    oReturnValue.Output = ErrMessage
            '    Return oReturnValue
            '    Exit Function
            'End If
            oCustomClass.output = ErrMessage
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.daInsuranceRateCard.GetNewInsuranceRateCardDSave")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try

    End Function
    Public Function GetNewInsuranceRateCardDDelete(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String
        Dim params() As SqlParameter = New SqlParameter(8) {}
        Dim ErrMessage As String = ""
        Try
            params(0) = New SqlParameter("@CardID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.CardID.Trim
            params(1) = New SqlParameter("@InsType", SqlDbType.VarChar, 2)
            params(1).Value = oCustomClass.InsType.Trim
            params(2) = New SqlParameter("@UsageID", SqlDbType.VarChar, 10)
            params(2).Value = oCustomClass.UsageID.Trim
            params(3) = New SqlParameter("@NewUsed", SqlDbType.Char, 1)
            params(3).Value = oCustomClass.NewUsed.Trim
            params(4) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 10)
            params(4).Value = oCustomClass.CoverageType.Trim
            params(5) = New SqlParameter("@MasterRate", SqlDbType.VarChar, 50)
            params(5).Value = oCustomClass.MasterRate.Trim
            params(6) = New SqlParameter("@SumFrom", SqlDbType.Decimal)
            params(6).Value = oCustomClass.SumFrom
            params(7) = New SqlParameter("@SumTo", SqlDbType.Decimal)
            params(7).Value = oCustomClass.SumTo
            params(8) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(8).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spShadowNewInsuranceRateCardDDelete", params)
            Return CStr(params(8).Value)

        Catch exp As Exception
            Throw New Exception("Error On DataAccess.Insurance.daInsuranceRateCardD.GetInsuranceRateCardDDelete")
        End Try

    End Function
    Public Function GetTPLToCustSave(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard

        Dim params() As SqlParameter = New SqlParameter(3) {}

        Dim objCon As New SqlConnection(oCustomClass.strConnection)
        Dim transaction As SqlTransaction
        Dim ErrMessage As String = ""
        Dim SP As String
        If oCustomClass.Command = "A" Then
            SP = "spShadowNewInsurancePremiumTPLToCustSaveAdd"
        ElseIf oCustomClass.Command = "A1" Then
            SP = "spShadowNewInsuranceStdRateTPLSaveAdd"
        ElseIf oCustomClass.Command = "E1" Then
            SP = "spShadowNewInsuranceStdRateTPLSaveEdit"
        Else
            SP = "spShadowNewInsurancePremiumTPLToCustSaveEdit"
        End If
        Try
            params(0) = New SqlParameter("@CardID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.CardID.Trim
            params(1) = New SqlParameter("@TPLAmount", SqlDbType.Decimal)
            params(1).Value = oCustomClass.TPLAmount
            params(2) = New SqlParameter("@TPLPremium", SqlDbType.Decimal)
            params(2).Value = oCustomClass.TPLPremium
            params(3) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(3).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SP, params)

            ErrMessage = CType(params(3).Value, String)
            'If ErrMessage <> "" Then
            '    oReturnValue.Output = ErrMessage
            '    Return oReturnValue
            '    Exit Function
            'End If
            oCustomClass.output = ErrMessage
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.daInsuranceRateCard.GetTPLToCustSave")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try

    End Function
    Public Function GetTPLToCustDelete(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String
        Dim params() As SqlParameter = New SqlParameter(3) {}
        Dim ErrMessage As String = ""
        Dim SP As String
        If oCustomClass.Command = "D" Then
            SP = "spShadowNewInsurancePremiumTPLToCustDelete"
        Else
            SP = "spShadowNewInsuranceStdRateTPLDelete"
        End If
        Try
            params(0) = New SqlParameter("@CardID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.CardID.Trim
            params(1) = New SqlParameter("@TPLAmount", SqlDbType.Decimal)
            params(1).Value = oCustomClass.TPLAmount
            params(2) = New SqlParameter("@TPLPremium", SqlDbType.Decimal)
            params(2).Value = oCustomClass.TPLPremium
            params(3) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(3).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SP, params)
            Return CStr(params(3).Value)

        Catch exp As Exception
            Throw New Exception("Error On DataAccess.Insurance.daInsuranceRateCardD.GetTPLToCustDelete")
        End Try

    End Function
    Public Function GetOtherCoverage(ByVal oCustomClass As Parameter.eInsuranceRateCard) As Parameter.eInsuranceRateCard

        Dim params() As SqlParameter = New SqlParameter(3) {}

        Dim objCon As New SqlConnection(oCustomClass.strConnection)
        Dim transaction As SqlTransaction
        Dim ErrMessage As String = ""
        Dim SP As String
        If oCustomClass.Command = "A" Then
            SP = "spShadowOtherCoverageRateToCustSaveAdd"
        ElseIf oCustomClass.Command = "E" Then
            SP = "spShadowOtherCoverageRateToCustSaveEdit"
        ElseIf oCustomClass.Command = "A1" Then
            SP = "spShadowStdOtherCoverageRateSaveAdd"
        ElseIf oCustomClass.Command = "E1" Then
            SP = "spShadowStdOtherCoverageRateSaveEdit"
        ElseIf oCustomClass.Command = "D1" Then
            SP = "spShadowStdOtherCoverageRateDelete"
        Else
            SP = "spShadowOtherCoverageRateToCustDelete"
        End If
        Try
            params(0) = New SqlParameter("@CardID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.CardID.Trim
            params(1) = New SqlParameter("@OtherCoverage", SqlDbType.VarChar, 25)
            params(1).Value = oCustomClass.OtherCoverage
            params(2) = New SqlParameter("@CoverageRate", SqlDbType.Decimal, 9)
            params(2).Value = oCustomClass.CoverageRate
            params(3) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(3).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, SP, params)

            ErrMessage = CType(params(3).Value, String)
            oCustomClass.output = ErrMessage
            Return oCustomClass
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.daInsuranceRateCard.GetOtherCoverage")
        Finally
            If Not m_connection Is Nothing Then
                m_connection.Close()
            End If
        End Try

    End Function

    Public Function GetInsuranceRateCardHDelete(ByVal oCustomClass As Parameter.eInsuranceRateCard) As String
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim ErrMessage As String = ""
        Try
            params(0) = New SqlParameter("@CardID", SqlDbType.VarChar, 50)
            params(0).Value = oCustomClass.CardID.Trim
            params(1) = New SqlParameter("@Err", SqlDbType.VarChar, 100)
            params(1).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(oCustomClass.strConnection, CommandType.StoredProcedure, "spShadowInsuranceMasterHPPRateHDelete", params)
            Return CStr(params(1).Value)

        Catch exp As Exception
            Throw New Exception("Error On DataAccess.Insurance.daInsuranceRateCardD.GetInsuranceRateCardHDelete")
        End Try

    End Function
End Class
