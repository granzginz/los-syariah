
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.General
#End Region

Public Class InsuranceStandardPremium : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    Private Const LIST_INSURANCE_STANDARD_RATE As String = "spPagingInsStdRate"
    Private Const LIST_SAVEADD_INSURANCE_STANDARD_RATE As String = "spInsStdRateSaveAdd"
    Private Const LIST_SAVEEDIT_INSURANCE_STANDARD_RATE As String = "spInsStdRateSaveEdit"
    Private Const LIST_DELETE_INSURANCE_STANDARD_RATE As String = "spInsStdRateDelete"
    Private Const PROCESS_COPY_STANDARD_RATE As String = "spCopyStandardPremiumRateFromAnotherBranch"
    Private Const LIST_SELECT_BRANCH As String = "spPagingInsPremToCust"
    Private Const LIST_REPORT As String = "spReportInsStdRate"
    Private Const LIST_REPORT_INSURANCE_STANDARD As String = "spInsPremStandardRateReport"

#End Region

#Region "processCopyStandardPremiumRate"
    Public Function processCopyStandardPremiumRate(ByVal customClass As Parameter.InsuranceStandardPremium) As String
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@BranchDestination", SqlDbType.Char)
        params(0).Value = customClass.BranchDestination
        params(1) = New SqlParameter("@Branchsource", SqlDbType.Char)
        params(1).Value = customClass.BranchSource
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, PROCESS_COPY_STANDARD_RATE, params)
            Return CStr(params(1).Value)
        Catch ex As Exception
            Dim err As String = ex.Message
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.ProcessCopyRateFromAnotherBranch")
        End Try
    End Function
#End Region

#Region "getInsuranceStandardPremium"

    Public Function getInsuranceStandardPremium(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
        Dim oReturnValue As New Parameter.InsuranceStandardPremium
        Dim spName As String
        If customClass.Page = General.CommonVariableHelper.VIEW_BRANCH Then
            spName = LIST_SELECT_BRANCH
        Else
            spName = LIST_INSURANCE_STANDARD_RATE
        End If

        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.GetPremiumToCustomer")
        End Try

    End Function

#End Region

#Region "InsuranceStandardPremiumSaveAdd"
    Public Function InsuranceStandardPremiumSaveAdd(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
        Dim reader As SqlDataReader
        Dim oReturnValue As New Parameter.InsuranceStandardPremium
        Dim params(11) As SqlParameter
        params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@InsType", SqlDbType.VarChar, 2)
        params(1).Value = customClass.InsType
        params(2) = New SqlParameter("@UsageID", SqlDbType.Char, 1)
        params(2).Value = customClass.UsageID
        params(3) = New SqlParameter("@NewUsed", SqlDbType.Char, 1)
        params(3).Value = customClass.NewUsed
        params(4) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 3)
        params(4).Value = customClass.CoverageType
        params(5) = New SqlParameter("@YearNum", SqlDbType.SmallInt)
        params(5).Value = customClass.YearNum
        params(6) = New SqlParameter("@Rate", SqlDbType.Decimal)
        params(6).Value = customClass.Rate
        params(7) = New SqlParameter("@SRCCRate", SqlDbType.Decimal)
        params(7).Value = customClass.SRCCRate
        params(8) = New SqlParameter("@FloodRate", SqlDbType.Decimal)
        params(8).Value = customClass.FloodRate
        params(9) = New SqlParameter("@LoadingRate", SqlDbType.Decimal)
        params(9).Value = customClass.LoadingRate
        params(10) = New SqlParameter("@SumIns", SqlDbType.Decimal)
        params(10).Value = customClass.SumIns
        params(11) = New SqlParameter("@Error", SqlDbType.VarChar, 100)
        params(11).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_SAVEADD_INSURANCE_STANDARD_RATE, params)
            oReturnValue.Err = CType(params(11).Value, String)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.PremiumToCustomerRateCustSaveAdd")
        End Try

    End Function
#End Region

#Region "InsuranceStandardPremiumSaveEdit"


    Public Sub InsuranceStandardPremiumSaveEdit(ByVal customClass As Parameter.InsuranceStandardPremium)

        Dim params(10) As SqlParameter
        params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@InsType", SqlDbType.VarChar, 2)
        params(1).Value = customClass.InsType
        params(2) = New SqlParameter("@UsageID", SqlDbType.Char, 1)
        params(2).Value = customClass.UsageID
        params(3) = New SqlParameter("@NewUsed", SqlDbType.Char, 1)
        params(3).Value = customClass.NewUsed
        params(4) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 3)
        params(4).Value = customClass.CoverageType
        params(5) = New SqlParameter("@YearNum", SqlDbType.SmallInt)
        params(5).Value = customClass.YearNum
        params(6) = New SqlParameter("@Rate", SqlDbType.Decimal)
        params(6).Value = customClass.Rate
        params(7) = New SqlParameter("@SRCCRate", SqlDbType.Decimal)
        params(7).Value = customClass.SRCCRate
        params(8) = New SqlParameter("@FloodRate", SqlDbType.Decimal)
        params(8).Value = customClass.FloodRate
        params(9) = New SqlParameter("@LoadingRate", SqlDbType.Decimal)
        params(9).Value = customClass.LoadingRate
        params(10) = New SqlParameter("@SumIns", SqlDbType.Decimal)
        params(10).Value = customClass.SumIns
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_SAVEEDIT_INSURANCE_STANDARD_RATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.PremiumToCustomerRateCustSaveEdit")
        End Try

    End Sub
#End Region

#Region "InsuranceStandardPremiumDelete"


    Public Sub InsuranceStandardPremiumDelete(ByVal customClass As Parameter.InsuranceStandardPremium)
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@InsType", SqlDbType.VarChar, 2)
        params(1).Value = customClass.InsType
        params(2) = New SqlParameter("@UsageID", SqlDbType.Char, 1)
        params(2).Value = customClass.UsageID
        params(3) = New SqlParameter("@NewUsed", SqlDbType.Char, 1)
        params(3).Value = customClass.NewUsed
        params(4) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 15)
        params(4).Value = customClass.CoverageType
        params(5) = New SqlParameter("@YearNum", SqlDbType.SmallInt)
        params(5).Value = customClass.YearNum
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_DELETE_INSURANCE_STANDARD_RATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.PremiumToCustomerRateCustDelete")
        End Try
    End Sub
#End Region

#Region "GetInsuranceStandardPremiumReport"

    Public Function GetInsuranceStandardPremiumReport(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
        Dim oReturnValue As New Parameter.InsuranceStandardPremium
        Dim spName As String
        If customClass.PageSource = General.CommonVariableHelper.PAGE_SOURCE_REPORT_INSURANCE_STANDARD_PREMIUM Then
            spName = LIST_REPORT_INSURANCE_STANDARD
        Else
            spName = LIST_REPORT
        End If

        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId

        Try
            oReturnValue.ListDataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spName, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.GetPremiumToCustomerReport")
        End Try
    End Function
#End Region


End Class
