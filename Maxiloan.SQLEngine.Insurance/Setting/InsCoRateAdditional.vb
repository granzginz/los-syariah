#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InsCoRateAdditional
    Private Const spListInsCoRateAdditional As String = "spInsuranceRateAdditionalPaging"
    Private Const spInsCoRateAdditionalAdd As String = "spInsuranceRateAdditionalAdd"
    Private Const spInsCoRateAdditionalUpdate As String = "spInsuranceRateAdditionalUpdate"
    Private Const spInsCoRateAdditionalDelete As String = "spInsuranceRateAdditionalDelete"


    'Private Const PARAM_STATUS As String = "@isactive"

    Public Function ListInsCoRateadditional(ByVal customclass As Parameter.InsCoRateAdditional) As Parameter.InsCoRateAdditional
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            If customclass.SortBy.Contains("From") Or customclass.SortBy.Contains("To") Then
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                If customclass.SortBy.Contains("From") Then
                    params(3).Value = customclass.SortBy.Insert(0, "[").Insert(5, "]")
                Else
                    params(3).Value = customclass.SortBy.Insert(0, "[").Insert(3, "]")
                End If

            Else
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                params(3).Value = customclass.SortBy
            End If


            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListInsCoRateAdditional, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            'WriteException("InsCoRateAdditional", "ListInsCoProduct", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function AddInsCoRateadditional(ByVal customclass As Parameter.InsCoRateAdditional) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(7) {}
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            params(0) = New SqlParameter("@maskId", SqlDbType.Char, 10)
            params(0).Value = customclass.MaskAssId

            params(1) = New SqlParameter("@maskAssBranchID", SqlDbType.Char, 10)
            params(1).Value = customclass.InsCoBranchID

            params(2) = New SqlParameter("@productID", SqlDbType.Char, 3)
            params(2).Value = customclass.ProductID

            params(3) = New SqlParameter("@branchID", SqlDbType.Char, 3)
            params(3).Value = customclass.BranchId

            params(4) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(4).Value = customclass.Tenor

            params(5) = New SqlParameter("@rate", SqlDbType.Decimal)
            params(5).Value = customclass.Rate

            params(6) = New SqlParameter("@isActive", SqlDbType.Bit)
            params(6).Value = customclass.IsActive


            params(7) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
            params(7).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spInsCoRateAdditionalAdd, params)
            objtrans.Commit()
            Return CStr(params(7).Value)
        Catch exp As Exception
            Throw New Exception(exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function


    Public Function EditInsCoRateadditional(ByVal customclass As Parameter.InsCoRateAdditional) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(8) {}
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            params(0) = New SqlParameter("@maskId", SqlDbType.Char, 10)
            params(0).Value = customclass.MaskAssId

            params(1) = New SqlParameter("@maskAssBranchID", SqlDbType.Char, 10)
            params(1).Value = customclass.InsCoBranchID

            params(2) = New SqlParameter("@productID", SqlDbType.Char, 3)
            params(2).Value = customclass.ProductID

            params(3) = New SqlParameter("@branchID", SqlDbType.Char, 3)
            params(3).Value = customclass.BranchId

            params(4) = New SqlParameter("@Tenor", SqlDbType.SmallInt)
            params(4).Value = customclass.Tenor

            params(5) = New SqlParameter("@currrate", SqlDbType.Decimal)
            params(5).Value = customclass.CurrRate

            params(6) = New SqlParameter("@rate", SqlDbType.Decimal)
            params(6).Value = customclass.Rate

            params(7) = New SqlParameter("@isActive", SqlDbType.Bit)
            params(7).Value = customclass.IsActive


            params(8) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
            params(8).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spInsCoRateAdditionalUpdate, params)
            objtrans.Commit()
            Return CStr(params(8).Value)
        Catch exp As Exception
            Throw New Exception(exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function


    Public Function DeleteInsCoRateadditional(ByVal customclass As Parameter.InsCoRateAdditional) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(5) {}
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            params(0) = New SqlParameter("@maskId", SqlDbType.Char, 10)
            params(0).Value = customclass.MaskAssId

            params(1) = New SqlParameter("@maskAssBranchID", SqlDbType.Char, 10)
            params(1).Value = customclass.InsCoBranchID

            params(2) = New SqlParameter("@productID", SqlDbType.Char, 3)
            params(2).Value = customclass.ProductID

            params(3) = New SqlParameter("@branchID", SqlDbType.Char, 3)
            params(3).Value = customclass.BranchId

            params(4) = New SqlParameter("@tenor", SqlDbType.SmallInt)
            params(4).Value = customclass.Tenor

            params(5) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
            params(5).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spInsCoRateAdditionalDelete, params)
            objtrans.Commit()
            Return CStr(params(5).Value)
        Catch exp As Exception
            Throw New Exception(exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

End Class
