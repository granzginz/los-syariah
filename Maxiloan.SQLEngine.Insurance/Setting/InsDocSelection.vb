
#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class InsDocSelection : Inherits DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const SP_INS_DOC_PAGING As String = "spInsDocPaging"
    Private Const SP_INS_DOC_RPT As String = "spInsDocRpt"
    Private Const SP_INS_DOC_ADD As String = "spInsDocAdd"
    Private Const SP_INS_DOC_EDIT As String = "spInsDocEdit"
    Private Const SP_INS_DOC_DELETE As String = "spInsDocDelete"
    Private Const SP_INS_DOC_GET_BY_ID As String = "spInsDocGetById"

    '*** Field Names
    Private Const FLD_NM_INS_DOC_ID As String = "@InsDocId"
    Private Const FLD_NM_INS_DOC_DESC As String = "@InsDocDescription"
#End Region

#Region "QueryFunctions"
    Public Function GetInsDocList(ByVal oInsDoc As Parameter.InsDocSelectionList) As Parameter.InsDocSelectionList
        Dim Params(4) As SqlParameter

        With oInsDoc
            Params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            Params(0).Value = .CurrentPage
            Params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            Params(1).Value = .PageSize
            Params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            Params(2).Value = .WhereCond
            Params(3) = New SqlParameter("@TotalRecords", SqlDbType.Int)
            Params(3).Direction = ParameterDirection.Output
            Params(4) = New SqlParameter("@OrderBy", SqlDbType.VarChar, 100)
            Params(4).Value = .SortBy
        End With
        Try
            oInsDoc.ListData = SqlHelper.ExecuteDataset(oInsDoc.strConnection, CommandType.StoredProcedure, SP_INS_DOC_PAGING, Params).Tables(0)

            oInsDoc.TotalRecord = CType(Params(3).Value, Int16)

            Return oInsDoc
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetInsDocRpt(ByVal oInsDoc As Parameter.InsDocSelectionList) As Parameter.InsDocSelectionList
        Try
            oInsDoc.ListDataSet = SqlHelper.ExecuteDataset(oInsDoc.strConnection, CommandType.StoredProcedure, SP_INS_DOC_RPT)

            Return oInsDoc
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetInsDocById(ByVal oInsDoc As Parameter.InsDocSelectionList) As Parameter.InsDocSelectionList
        Dim Params(0) As SqlParameter

        With oInsDoc
            Params(0) = CreateInsDocIdParam()
            Params(0).Value = oInsDoc.InsDocId
        End With
        Try
            oInsDoc.ListDataSet = SqlHelper.ExecuteDataset(oInsDoc.strConnection, CommandType.StoredProcedure, SP_INS_DOC_GET_BY_ID, Params)

            Return oInsDoc
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region

#Region "HelperFunctions"
    Private Function CreateInsDocIdParam() As SqlParameter
        Return (New SqlParameter(FLD_NM_INS_DOC_ID, SqlDbType.Char, 10))
    End Function

    Private Function CreateInsDocDescParam() As SqlParameter
        Return (New SqlParameter(FLD_NM_INS_DOC_DESC, SqlDbType.VarChar, 50))
    End Function
#End Region

#Region "TableMaintenanceFunctions"
    Public Sub AddInsDoc(ByVal oInsDoc As Parameter.InsDocSelectionList)
        Dim Params(1) As SqlParameter

        With oInsDoc
            Params(0) = CreateInsDocIdParam()
            Params(0).Value = oInsDoc.InsDocId
            Params(1) = CreateInsDocDescParam()
            Params(1).Value = oInsDoc.InsDocDescription
        End With
        Try
            SqlHelper.ExecuteNonQuery(oInsDoc.strConnection, CommandType.StoredProcedure, SP_INS_DOC_ADD, Params)
        Catch ex As Exception
            Throw New Exception("INFO: Unable to add record. Record already exists. ERRMSG: " + ex.Message)
        End Try
    End Sub

    Public Sub UpdateInsDoc(ByVal oInsDoc As Parameter.InsDocSelectionList)
        Dim Params(1) As SqlParameter

        With oInsDoc
            Params(0) = CreateInsDocIdParam()
            Params(0).Value = oInsDoc.InsDocId
            Params(1) = CreateInsDocDescParam()
            Params(1).Value = oInsDoc.InsDocDescription
        End With
        Try
            SqlHelper.ExecuteNonQuery(oInsDoc.strConnection, CommandType.StoredProcedure, SP_INS_DOC_EDIT, Params)
        Catch ex As Exception
            Throw New Exception("INFO: Unable to edit the selected record. ERRMSG: " + ex.Message)
        End Try
    End Sub

    Public Sub DeleteInsDoc(ByVal oInsDoc As Parameter.InsDocSelectionList)
        Dim Params(0) As SqlParameter

        With oInsDoc
            Params(0) = CreateInsDocIdParam()
            Params(0).Value = oInsDoc.InsDocId
        End With
        Try
            SqlHelper.ExecuteNonQuery(oInsDoc.strConnection, CommandType.StoredProcedure, SP_INS_DOC_DELETE, Params)
        Catch ex As Exception
            Throw New Exception("INFO: Unable to delete the selected record. Record already used ! ERRMSG: " + ex.Message)
        End Try
    End Sub
#End Region
End Class
