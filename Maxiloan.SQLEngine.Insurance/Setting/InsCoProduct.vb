#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InsCoProduct
    Private Const spListInsCoProduct As String = "spInsCoProductPaging"
    Private Const spInsCoProductAdd As String = "spInsCoProductAdd"
    'Private Const spInsCoProductUpdate As String = "spInsCoProductUpdate"
    Private Const spInsCoProductDelete As String = "spInsCoProductDelete"


    'Private Const PARAM_STATUS As String = "@isactive"

    Public Function ListInsCoProduct(ByVal customclass As Parameter.InsCoProduct) As Parameter.InsCoProduct
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            If customclass.SortBy.Contains("From") Or customclass.SortBy.Contains("To") Then
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                If customclass.SortBy.Contains("From") Then
                    params(3).Value = customclass.SortBy.Insert(0, "[").Insert(5, "]")
                Else
                    params(3).Value = customclass.SortBy.Insert(0, "[").Insert(3, "]")
                End If

            Else
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
                params(3).Value = customclass.SortBy
            End If


            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListProduct = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spListInsCoProduct, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            'WriteException("InsCoProduct", "ListInsCoProduct", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function

    Public Function AddInsCoProduct(ByVal customclass As Parameter.InsCoProduct) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(4) {}
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            params(0) = New SqlParameter("@maskId", SqlDbType.VarChar, 10)
            params(0).Value = customclass.MaskAssID

            params(1) = New SqlParameter("@prodId", SqlDbType.VarChar, 3)
            params(1).Value = customclass.IDProd

            params(2) = New SqlParameter("@productName", SqlDbType.VarChar, 50)
            params(2).Value = customclass.ProdName

            params(3) = New SqlParameter("@active", SqlDbType.Bit)
            params(3).Value = customclass.IsActive


            params(4) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
            params(4).Direction = ParameterDirection.Output


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spInsCoProductAdd, params)
            objtrans.Commit()
            Return CStr(params(4).Value)
        Catch exp As Exception
            Throw New Exception(exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

    Public Function DeleteInsCoProduct(ByVal customclass As Parameter.InsCoProduct) As String
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            Dim params() As SqlParameter = New SqlParameter(2) {}
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            params(0) = New SqlParameter("@prodId", SqlDbType.VarChar, 3)
            params(0).Value = customclass.IDProd

            params(1) = New SqlParameter("@maskId", SqlDbType.VarChar, 10)
            params(1).Value = customclass.MaskAssID

            params(2) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
            params(2).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spInsCoProductDelete, params)
            objtrans.Commit()
            Return CStr(params(2).Value)
        Catch exp As Exception
            Throw New Exception(exp.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Function

End Class
