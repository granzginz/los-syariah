
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class PremiumToCustomer : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spPagingInsPremToCust"
    Private Const LIST_SELECT_RATE As String = "spPagingInsPremToCustRateCust"
    Private Const LIST_SELECT_TPL As String = "spPagingInsPremToCustTPL"
    'Private Const LIST_EDIT_RATE As String = "spInsPremToCustRateCustEdit"
    'Private Const LIST_EDIT_TPL As String = "spInsPremToCustTPLEdit"
    'Private Const LIST_ADD_RATE As String = "spPremiumToCustomerSaveAdd"
    'Private Const LIST_ADD_TPL As String = "spPremiumToCustomerSaveAdd"
    Private Const LIST_SAVEADD_RATE As String = "spInsPremToCustRateCustSaveAdd"
    Private Const LIST_SAVEADD_TPL As String = "spInsPremToCustTPLSaveAdd"
    Private Const LIST_SAVEEDIT_RATE As String = "spInsPremToCustRateCustSaveEdit"
    Private Const LIST_SAVEEDIT_TPL As String = "spInsPremToCustTPLSaveEdit"
    Private Const LIST_DELETE_RATE As String = "spInsPremToCustRateCustDelete"
    Private Const LIST_DELETE_TPL As String = "spInsPremToCustTPLDelete"
    Private Const LIST_BRANCH As String = "spInsPremToCustBranch"
    Private Const LIST_INSTYPE As String = "spInsPremToCustInsType"
    Private Const LIST_RATERPT As String = "spInsPremToCustRateCustReport"
    Private Const LIST_TPLRPT As String = "spInsPremToCustTPLReport"
    Private Const PROCESS_COPYRATE As String = "spCopyRateFromAnotherBranch"
#End Region

    Public Function ProcessCopyRateFromAnotherBranch(ByVal customClass As Parameter.PremiumToCustomer) As String


        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@BranchDestination", SqlDbType.Char)
        params(0).Value = customClass.BranchDestination
        params(1) = New SqlParameter("@Branchsource", SqlDbType.Char)
        params(1).Value = customClass.BranchSource
        Try

            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, PROCESS_COPYRATE, params)
            Return CStr(params(1).Value)

        Catch ex As Exception
            Dim err As String = ex.Message
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.ProcessCopyRateFromAnotherBranch")
        End Try

    End Function

    Public Function GetPremiumToCustomer(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer
        Dim oReturnValue As New Parameter.PremiumToCustomer
        Dim spName As String
        If customClass.Page = "PremiumToCustomer" Then
            spName = LIST_SELECT
        ElseIf customClass.Page = "PremiumToCustomerRateCust" Then
            spName = LIST_SELECT_RATE
        Else
            spName = LIST_SELECT_TPL
        End If
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.GetPremiumToCustomer")
        End Try
    End Function

    Public Function GetPremiumToCustomerReport(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer
        Dim oReturnValue As New Parameter.PremiumToCustomer
        Dim spName As String
        If customClass.Page = "RateCust" Then
            spName = LIST_RATERPT
        Else
            spName = LIST_TPLRPT
        End If
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        Try
            oReturnValue.ListDataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spName, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.GetPremiumToCustomerReport")
        End Try
    End Function

    Public Function GetPremiumToCustomerBranch(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer
        Dim oReturnValue As New Parameter.PremiumToCustomer
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_BRANCH).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.GetPremiumToCustomerBranch")
        End Try
    End Function

    Public Function GetPremiumToCustomerInsType(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer
        Dim oReturnValue As New Parameter.PremiumToCustomer
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, LIST_INSTYPE).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.GetPremiumToCustomerBranch")
        End Try
    End Function

#Region "TPL"
    Public Function PremiumToCustomerTPLSaveAdd(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer
        Dim reader As SqlDataReader
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@TPLAmount", SqlDbType.Int)
        params(1).Value = customClass.TPLAmount
        params(2) = New SqlParameter("@TPLPremium", SqlDbType.Int)
        params(2).Value = customClass.TPLPremium
        params(3) = New SqlParameter("@Error", SqlDbType.VarChar, 50)
        params(3).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_SAVEADD_TPL, params)
            Dim oReturnValue As New Parameter.PremiumToCustomer
            oReturnValue.Err = CType(params(3).Value, String)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.PremiumToCustomerTPLSaveAdd")
        End Try
    End Function
    Public Sub PremiumToCustomerTPLSaveEdit(ByVal customClass As Parameter.PremiumToCustomer)
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@TPLAmount", SqlDbType.Int)
        params(1).Value = customClass.TPLAmount
        params(2) = New SqlParameter("@TPLPremium", SqlDbType.Int)
        params(2).Value = customClass.TPLPremium
        Dim reader As SqlDataReader
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_SAVEEDIT_TPL, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.PremiumToCustomerTPLSaveEdit")
        End Try
    End Sub
    Public Sub PremiumToCustomerTPLDelete(ByVal customClass As Parameter.PremiumToCustomer)
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@TPLAmount", SqlDbType.Int)
        params(1).Value = customClass.TPLAmount
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_DELETE_TPL, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.PremiumToCustomerTPLDelete")
        End Try
    End Sub
#End Region

#Region "RateCust"
    Public Function PremiumToCustomerRateCustSaveAdd(ByVal customClass As Parameter.PremiumToCustomer) As Parameter.PremiumToCustomer
        Dim reader As SqlDataReader
        Dim oReturnValue As New Parameter.PremiumToCustomer
        Dim params(13) As SqlParameter
        params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@InsType", SqlDbType.VarChar, 2)
        params(1).Value = customClass.InsType
        params(2) = New SqlParameter("@UsageID", SqlDbType.Char, 1)
        params(2).Value = customClass.UsageID
        params(3) = New SqlParameter("@NewUsed", SqlDbType.Char, 1)
        params(3).Value = customClass.NewUsed
        params(4) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 3)
        params(4).Value = customClass.CoverageType
        params(5) = New SqlParameter("@YearNum", SqlDbType.SmallInt)
        params(5).Value = customClass.YearNum
        params(6) = New SqlParameter("@RateGroup", SqlDbType.Decimal)
        params(6).Value = customClass.RateGroup
        params(7) = New SqlParameter("@RateNonGroup", SqlDbType.Decimal)
        params(7).Value = customClass.RateNonGroup
        params(8) = New SqlParameter("@RateRepeatOrder", SqlDbType.Decimal)
        params(8).Value = customClass.RateRepeatOrder
        params(9) = New SqlParameter("@SRCCRate", SqlDbType.Decimal)
        params(9).Value = customClass.SRCCRate
        params(10) = New SqlParameter("@FloodRate", SqlDbType.Decimal)
        params(10).Value = customClass.FloodRate
        params(11) = New SqlParameter("@LoadingRate", SqlDbType.Decimal)
        params(11).Value = customClass.LoadingRate
        params(12) = New SqlParameter("@SumIns", SqlDbType.Decimal)
        params(12).Value = customClass.SumIns
        params(13) = New SqlParameter("@Error", SqlDbType.VarChar, 100)
        params(13).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_SAVEADD_RATE, params)
            oReturnValue.Err = CType(params(13).Value, String)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.PremiumToCustomerRateCustSaveAdd")
        End Try
    End Function
    Public Sub PremiumToCustomerRateCustSaveEdit(ByVal customClass As Parameter.PremiumToCustomer)
        Dim params(12) As SqlParameter
        params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@InsType", SqlDbType.VarChar, 2)
        params(1).Value = customClass.InsType
        params(2) = New SqlParameter("@UsageID", SqlDbType.Char, 1)
        params(2).Value = customClass.UsageID
        params(3) = New SqlParameter("@NewUsed", SqlDbType.Char, 1)
        params(3).Value = customClass.NewUsed
        params(4) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 3)
        params(4).Value = customClass.CoverageType
        params(5) = New SqlParameter("@YearNum", SqlDbType.SmallInt)
        params(5).Value = customClass.YearNum
        params(6) = New SqlParameter("@RateGroup", SqlDbType.Decimal)
        params(6).Value = customClass.RateGroup
        params(7) = New SqlParameter("@RateNonGroup", SqlDbType.Decimal)
        params(7).Value = customClass.RateNonGroup
        params(8) = New SqlParameter("@RateRepeatOrder", SqlDbType.Decimal)
        params(8).Value = customClass.RateRepeatOrder
        params(9) = New SqlParameter("@SRCCRate", SqlDbType.Decimal)
        params(9).Value = customClass.SRCCRate
        params(10) = New SqlParameter("@FloodRate", SqlDbType.Decimal)
        params(10).Value = customClass.FloodRate
        params(11) = New SqlParameter("@LoadingRate", SqlDbType.Decimal)
        params(11).Value = customClass.LoadingRate
        params(12) = New SqlParameter("@SumIns", SqlDbType.Decimal)
        params(12).Value = customClass.SumIns
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_SAVEEDIT_RATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.PremiumToCustomerRateCustSaveEdit")
        End Try
    End Sub
    Public Sub PremiumToCustomerRateCustDelete(ByVal customClass As Parameter.PremiumToCustomer)
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@InsType", SqlDbType.VarChar, 2)
        params(1).Value = customClass.InsType
        params(2) = New SqlParameter("@UsageID", SqlDbType.Char, 1)
        params(2).Value = customClass.UsageID
        params(3) = New SqlParameter("@NewUsed", SqlDbType.Char, 1)
        params(3).Value = customClass.NewUsed
        params(4) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 15)
        params(4).Value = customClass.CoverageType
        params(5) = New SqlParameter("@YearNum", SqlDbType.SmallInt)
        params(5).Value = customClass.YearNum
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_DELETE_RATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.PremiumToCustomerRateCustDelete")
        End Try
    End Sub
#End Region
End Class

