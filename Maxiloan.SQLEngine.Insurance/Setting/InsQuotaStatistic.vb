

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.General
#End Region


Public Class InsQuotaStatistic : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "

    Private LIST_INS_QUOTA_LISTING As String = "spPagingInsQuota"
    Private PROCESS_INS_QUOTA_YEAR As String = "spInsProcessQuotaYear"
    Private LIST_INS_QUOTA_DETAIL_MONTH_YEAR As String = "spPagingInsQuotaDetailMonthYear"
    Private UPDATE_INS_QUOTA As String = "spInsQuotaSaveEdit"
    Private GET_REPORT_INSURANCE_QUOTA As String = "SpInsuranceQuotaReport"


#End Region

#Region "getInsuranceQuotaListing"

    Public Function getInsuranceQuotaListing(ByVal customClass As Parameter.InsQuotaStatistic) As Parameter.InsQuotaStatistic
        Dim oReturnValue As New Parameter.InsQuotaStatistic
        Dim spname As String

        Select Case customClass.Page
            Case CommonVariableHelper.INS_QUOTA_LISTING
                spname = LIST_INS_QUOTA_LISTING
            Case CommonVariableHelper.INS_QUOTA_LISTING_DETAIL_MONTH_YEAR
                spname = LIST_INS_QUOTA_DETAIL_MONTH_YEAR
        End Select


        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spname, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.InsQuotaStatistic.getInsuranceQuotaListing")

        End Try

    End Function

#End Region

#Region "ProcessInsuranceQuotaYear"

    Public Sub ProcessInsuranceQuotaYear(ByVal customClass As Parameter.InsQuotaStatistic)

        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@MaskAssID", SqlDbType.Char, 10)
        params(0).Value = customClass.MaskAssID
        params(1) = New SqlParameter("@MaskAssBranchID", SqlDbType.Char, 10)
        params(1).Value = customClass.InsuranceComBranchID
        params(2) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(2).Value = customClass.BranchId
        params(3) = New SqlParameter("@year", SqlDbType.Char, 4)
        params(3).Value = customClass.StatisticYear
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, PROCESS_INS_QUOTA_YEAR, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.InsQuotaStatistic.ProcessInsuranceQuotaYear")

        End Try

    End Sub

#End Region

#Region "UpdateInsuranceQuotaYear"

    Public Sub UpdateInsuranceQuotaYear(ByVal customClass As Parameter.InsQuotaStatistic)
        Dim params() As SqlParameter = New SqlParameter(6) {}

        params(0) = New SqlParameter("@MaskAssID", SqlDbType.Char, 10)
        params(0).Value = customClass.MaskAssID
        params(1) = New SqlParameter("@MaskAssBranchID", SqlDbType.Char, 10)
        params(1).Value = customClass.InsuranceComBranchID
        params(2) = New SqlParameter("@BranchId", SqlDbType.VarChar, 3)
        params(2).Value = customClass.BranchId
        params(3) = New SqlParameter("@StatisticMonth", SqlDbType.SmallInt, 4)
        params(3).Value = customClass.StatisticMonth
        params(4) = New SqlParameter("@StatisticYear", SqlDbType.SmallInt, 4)
        params(4).Value = customClass.StatisticYear
        params(5) = New SqlParameter("@QuotaAmount", SqlDbType.Float, 4)
        params(5).Value = customClass.QuotaAmont
        params(6) = New SqlParameter("@QuotaPercentage", SqlDbType.Decimal)
        params(6).Value = customClass.QuotaPercentage

        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, UPDATE_INS_QUOTA, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.InsQuotaStatistic.ProcessInsuranceQuotaYear")

        End Try

    End Sub

#End Region

#Region "GetInsuranceQuotaReport"


    Public Function GetInsuranceQuotaReport(ByVal CustomClass As Parameter.InsQuotaStatistic) As Parameter.InsQuotaStatistic

        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = CustomClass.WhereCond

        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = CustomClass.SortBy

        CustomClass.ListReport = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, GET_REPORT_INSURANCE_QUOTA, params)
        Return CustomClass
    End Function

#End Region

End Class
