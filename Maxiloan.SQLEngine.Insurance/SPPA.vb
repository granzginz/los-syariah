#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.General
#End Region
Public Class SPPA
#Region "Private Constanta"

    Private Const SP_CREATESPPA As String = "spinslistcreatesppa"
    Private Const SP_GRIDTENOR_INSURANCE_ASSET_DETAIL As String = "spViewInsuranceTenorAssetDetail"
    Private Const SP_GRIDTENOR_INSURANCE_ASSET_DETAIL_AGRI As String = "spViewInsuranceTenorAssetDetailAgri"
    Private Const SP_GET_CLAIM_HISTORY As String = "spGetClaimHistory"
    Private Const SP_GET_ENDORSMENT_HISTORY As String = "spGetOldInsuranceAssetHeader"
    Private Const SP_GET_ENDORSMENT_HISTORY_DETAIL_POPUP As String = "spGetOldInsuranceAssetDetail"
    Private Const SP_GET_INS_BILL_SELECT As String = "spInsuranceBillingSelect"
    Private Const SP_GET_INS_BILL_SELECTCP As String = "spInsuranceBillingSelectCP"
    Private Const SP_GET_INS_BILL_SELECTJK As String = "spInsuranceBillingSelectJK"
    Private Const SP_GET_INS_COVERING_INQ As String = "spInsuranceCoveringInquiry"
    Private Const SP_GENERATE_SPPA As String = "spGenerateSPPA"
    'Private Const SP_GENERATE_SPPA As String = "spGenerateSPPA"
    Private Const SP_PRINT_SPPA_REPORT As String = "spSPPAPrint"
    Private Const SP_INSERT_TO_MAILTRANS As String = "spInsRenewalLetterInsertMail"
    Private Const PARAM_SPPANO As String = "@SPPANoBuatPrint"
    Private Const PARAM_INSURANCE_COY_BRANCH_ID As String = "@InsuranceComBranchID"

#End Region
#Region "ListCreateSPPA"

    Public Function GetListCreateSPPA(ByVal customClass As Parameter.SPPA) As Parameter.SPPA
        Dim oReturnValue As New Parameter.SPPA
        Dim SpName As String
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond

        Select Case customClass.PageSource
            Case CommonVariableHelper.PAGE_SOURCE_CREATE_SPPA
                SpName = SP_CREATESPPA
            Case CommonVariableHelper.PAGE_SOURCE_GRID_TENOR_VIEW_INSURANCE_ASSET_DETAIL
                SpName = SP_GRIDTENOR_INSURANCE_ASSET_DETAIL
            Case CommonVariableHelper.PAGE_SOURCE_GET_CLAIM_HISTORY
                SpName = SP_GET_CLAIM_HISTORY
            Case CommonVariableHelper.PAGE_SOURCE_GET_ENDORSMENT_HISTORY
                SpName = SP_GET_ENDORSMENT_HISTORY
            Case CommonVariableHelper.PAGE_SOURCE_GET_ENDORSMENT_HISTORY_DETAIL_POPUP
                SpName = SP_GET_ENDORSMENT_HISTORY_DETAIL_POPUP
            Case CommonVariableHelper.PAGE_SOURCE_GET_INSURANCE_BILLING_SELECT
                SpName = SP_GET_INS_BILL_SELECT
            Case CommonVariableHelper.PAGE_SOURCE_GET_INSURANCE_COVERING_INQUIRY
                SpName = SP_GET_INS_COVERING_INQ
            Case CommonVariableHelper.PAGE_SOURCE_PRINT_SPPA_REPORT
                SpName = SP_PRINT_SPPA_REPORT
            Case "PAGE_SOURCE_GRID_TENOR_VIEW_INSURANCE_ASSET_DETAIL_AGRI"
                SpName = SP_GRIDTENOR_INSURANCE_ASSET_DETAIL_AGRI
            Case "PAGE_SOURCE_GET_INSURANCE_BILLING_SELECTCP"
                SpName = SP_GET_INS_BILL_SELECTCP
            Case "PAGE_SOURCE_GET_INSURANCE_BILLING_SELECTJK"
                SpName = SP_GET_INS_BILL_SELECTJK
            Case Else
                SpName = SP_CREATESPPA
        End Select

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, SpName, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
#End Region
#Region "GenerateSPPA"

    Public Function GenerateSPPA(ByVal customClass As Parameter.SPPA) As Parameter.SPPA

        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            Dim params() As SqlParameter = New SqlParameter(5) {}
            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(1).Value = customClass.BusinessDate
            params(2) = New SqlParameter("@BulkofApplicationID", SqlDbType.VarChar, 8000)
            params(2).Value = customClass.BulkOfApplicationID
            params(3) = New SqlParameter(PARAM_INSURANCE_COY_BRANCH_ID, SqlDbType.VarChar, 100)
            params(3).Value = customClass.MaskAssBranchID
            params(4) = New SqlParameter("@ProductAsuransi", SqlDbType.VarChar, 100)
            params(4).Value = customClass.ProductAsuransi
            params(5) = New SqlParameter(PARAM_SPPANO, SqlDbType.VarChar, 50)
            params(5).Direction = ParameterDirection.Output

            'If customClass.ProductAsuransi = "CP" Then
            '    SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SP_GENERATE_SPPA, params)
            'ElseIf customClass.ProductAsuransi = "JK" Then
            '    SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SP_GENERATE_SPPA, params)
            'Else
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SP_GENERATE_SPPA, params)
            'End If
            objtrans.Commit()
            customClass.SPPANo = CStr(params(5).Value)
            Return customClass
        Catch ex As Exception
            objtrans.Rollback()
            Dim err As New MaxiloanExceptions
            err.WriteLog("SPPA.vb", "GenerateSPPA", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Function

#End Region
#Region "PrintMailTransaction"

    Public Function PrintMailTransaction(ByVal customClass As Parameter.MailTransaction) As Parameter.SPPA

        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction

            Dim params(8) As SqlParameter
            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@ApplicationId", SqlDbType.DateTime)
            params(1).Value = customClass.ApplicationID
            params(2) = New SqlParameter("@MailTypeId", SqlDbType.VarChar, 10)
            params(2).Value = customClass.MailTypeID
            params(3) = New SqlParameter("@MailTransNo", SqlDbType.VarChar, 50)
            params(3).Value = customClass.MailTransNo
            params(4) = New SqlParameter("@MailDateCreate", SqlDbType.DateTime)
            params(4).Value = customClass.MailDateCreate
            params(5) = New SqlParameter("@MailUserCreate", SqlDbType.VarChar, 50)
            params(5).Value = customClass.MailUserCreate
            params(6) = New SqlParameter("@MailDatePrint", SqlDbType.DateTime)
            params(6).Value = customClass.MailDatePrint
            params(7) = New SqlParameter("@MailPrintedNum", SqlDbType.Int)
            params(7).Value = customClass.MailPrintedNum
            params(8) = New SqlParameter("@MailUserPrint", SqlDbType.VarChar, 50)
            params(8).Value = customClass.MailUserPrint

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SP_INSERT_TO_MAILTRANS, params)
            objtrans.Commit()
        Catch ex As Exception
            objtrans.Rollback()
            Dim err As New MaxiloanExceptions
            err.WriteLog("SPPA.vb", "PrintMailTransaction", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Function

#End Region
#Region "Get SPPA List"
    Public Function GetSPPAList(ByVal customClass As Parameter.SPPA) As Parameter.SPPA
        Dim oReturnValue As New Parameter.SPPA
        Dim SpName As String
        Dim params(1) As SqlParameter

        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter("@ProductAsuransi", SqlDbType.VarChar, 50)
        params(1).Value = customClass.ProductAsuransi

        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, _
            CommandType.StoredProcedure, "spGetSPPA", params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetSPPAReport(ByVal customClass As Parameter.SPPA) As DataSet
        Dim oReturnValue As New Parameter.SPPA
        Dim params(1) As SqlParameter

        params(0) = New SqlParameter("@WhereBy", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond

        params(1) = New SqlParameter("@SPPANo", SqlDbType.VarChar, 50)
        params(1).Value = customClass.SPPANo
        Try
            
            Return SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spCreateSPPAReport", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetSPPAReportCP(ByVal customClass As Parameter.SPPA) As DataSet
        Dim oReturnValue As New Parameter.SPPA
        Dim params(1) As SqlParameter

        params(0) = New SqlParameter("@WhereBy", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond

        params(1) = New SqlParameter("@SPPANo", SqlDbType.VarChar, 50)
        params(1).Value = customClass.SPPANo
        Try
            'If customClass.ProductAsuransi.Trim = "CP" Then
            '    Return SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spCreateSPPAReportCP", params)
            'ElseIf customClass.ProductAsuransi.Trim = "JK" Then
            '    Return SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spCreateSPPAReportJK", params)
            'Else
            Return SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spCreateSPPAReportCP", params)

            'End If

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetSPPAReportJK(ByVal customClass As Parameter.SPPA) As DataSet
        Dim oReturnValue As New Parameter.SPPA
        Dim params(1) As SqlParameter

        params(0) = New SqlParameter("@WhereBy", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond

        params(1) = New SqlParameter("@SPPANo", SqlDbType.VarChar, 50)
        params(1).Value = customClass.SPPANo
        Try
            Return SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spCreateSPPAReportJK", params)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region
End Class
