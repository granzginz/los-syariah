
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.General

#End Region

Public Class PolicyReceive
    Public Function getInsurancePolicyReceive(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
        Dim oReturnValue As New Parameter.InsuranceStandardPremium
        Dim spName As String

        spName = "spPagingInsPolicyReceive"


        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@ProductInsurance", SqlDbType.VarChar, 2)
        params(5).Value = customClass.ProductInsurance
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spName, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("PolicyReceive.vb", "getInsurancePolicyReceive", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function getPolicyUpload(ByVal customClass As Parameter.InsuranceStandardPremium) As Parameter.InsuranceStandardPremium
        Dim oReturnValue As New Parameter.InsuranceStandardPremium
        Dim params(7) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        params(5) = New SqlParameter("@SheetName", SqlDbType.VarChar)
        params(5).Value = "Sheet1$"
        params(6) = New SqlParameter("@FilePath", SqlDbType.VarChar)
        params(6).Value = customClass.ExcelFilePath
        params(7) = New SqlParameter("@HDR", SqlDbType.VarChar)
        params(7).Value = "Yes"
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, _
                CommandType.StoredProcedure, "spGetPolicyRecUpload", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub savePolicyUpload(ByVal strConnection As String, ByVal strAgreementNo As String, ByVal strPolicyNumber As String, ByVal PolicyReceiveDate As Date, ByVal strReceiveBy As String)
        Dim oReturnValue As New Parameter.InsuranceStandardPremium
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@agreementNo", SqlDbType.Char, 20)
        params(0).Value = strAgreementNo
        params(1) = New SqlParameter("@PolicyNumber", SqlDbType.VarChar, 25)
        params(1).Value = strPolicyNumber
        params(2) = New SqlParameter("@PolicyReceiveDate", SqlDbType.SmallDateTime)
        params(2).Value = PolicyReceiveDate
        params(3) = New SqlParameter("@PolicyReceiveBy", SqlDbType.VarChar, 20)
        params(3).Value = strReceiveBy
        Try
            SqlHelper.ExecuteNonQuery(strConnection, _
                CommandType.StoredProcedure, "spPolicyUploadSave", params)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub savePolicyUploadXLS(ByVal customClass As Parameter.InsuranceStandardPremium)
        Dim params() As SqlParameter = New SqlParameter(13) {}
        Dim i As Integer
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim transaction As SqlTransaction = Nothing

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction

            'clear temporary
            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, "truncate table dbo.PolisUploaded")

            With customClass.dtPolis
                For i = 0 To .Rows.Count - 1
                    params(0) = New SqlParameter("@KONTRAK", SqlDbType.Char, 20)
                    params(0).Value = .Rows(i).Item("kontrak").ToString

                    params(1) = New SqlParameter("@SERTIFIKAT", SqlDbType.VarChar, 50)
                    params(1).Value = .Rows(i).Item("sertifikat").ToString

                    params(2) = New SqlParameter("@NAMA_TERTANGGUNG", SqlDbType.VarChar, 50)
                    params(2).Value = .Rows(i).Item("nama_tertanggung").ToString

                    params(3) = New SqlParameter("@MODEL", SqlDbType.VarChar, 50)
                    params(3).Value = .Rows(i).Item("model").ToString

                    params(4) = New SqlParameter("@MERK", SqlDbType.VarChar, 50)
                    params(4).Value = .Rows(i).Item("merk").ToString

                    params(5) = New SqlParameter("@TAHUN", SqlDbType.Char, 4)
                    params(5).Value = .Rows(i).Item("tahun").ToString

                    params(6) = New SqlParameter("@WARNA", SqlDbType.VarChar, 50)
                    params(6).Value = .Rows(i).Item("warna").ToString

                    params(7) = New SqlParameter("@NO_MESIN", SqlDbType.VarChar, 50)
                    params(7).Value = .Rows(i).Item("no_mesin").ToString

                    params(8) = New SqlParameter("@NO_RANGKA", SqlDbType.VarChar, 50)
                    params(8).Value = .Rows(i).Item("no_rangka").ToString

                    params(9) = New SqlParameter("@TSI", SqlDbType.Decimal)
                    params(9).Value = CDbl(.Rows(i).Item("tsi"))

                    params(10) = New SqlParameter("@PERIODE_START", SqlDbType.SmallDateTime)
                    params(10).Value = CDate(.Rows(i).Item("periode_start"))

                    params(11) = New SqlParameter("@PERIODE_END", SqlDbType.SmallDateTime)
                    params(11).Value = CDate(.Rows(i).Item("periode_end"))

                    params(12) = New SqlParameter("@TERMS", SqlDbType.Int)
                    params(12).Value = CInt(.Rows(i).Item("terms"))

                    params(13) = New SqlParameter("@PREMI", SqlDbType.Decimal)
                    params(13).Value = CDbl(.Rows(i).Item("premi"))

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spSavePolisUploadXLS", params)
                Next
            End With

            transaction.Commit()

        Catch ex As Exception
            transaction.Rollback()
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub NotaAsuransiSave(ByVal oCustomer As Parameter.InsuranceStandardPremium)
        Dim conn As New SqlConnection(oCustomer.strConnection)
        Dim transaction As SqlTransaction = Nothing

        Dim params(10) As SqlParameter
        Dim params1(14) As SqlParameter

        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction


            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = oCustomer.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = oCustomer.ApplicationID
            params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt)
            params(2).Value = oCustomer.AssetSeqNo
            params(3) = New SqlParameter("@InsSequenceNo", SqlDbType.SmallInt)
            params(3).Value = oCustomer.InsSequenceNo
            params(4) = New SqlParameter("@NoNotaAsuransi", SqlDbType.VarChar, 50)
            params(4).Value = oCustomer.NoNotaAsuransi
            params(5) = New SqlParameter("@TglNotaAsuransi", SqlDbType.DateTime)
            params(5).Value = oCustomer.TglNotaAsuransi            
            params(6) = New SqlParameter("@PayAmount", SqlDbType.Decimal)
            params(6).Value = oCustomer.PayAmount
            params(7) = New SqlParameter("@PolicyNumber", SqlDbType.VarChar, 25)
            params(7).Value = oCustomer.PolicyNumber
            params(8) = New SqlParameter("@PolicyReceiveDate", SqlDbType.DateTime)
            params(8).Value = oCustomer.PolicyReceiveDate
            params(9) = New SqlParameter("@UserID", SqlDbType.VarChar, 20)
            params(9).Value = oCustomer.UserID
            params(10) = New SqlParameter("ProductInsurance", SqlDbType.VarChar, 2)
            params(10).Value = oCustomer.ProductInsurance

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spInsuranceAssetSave", params)

            params1(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params1(0).Value = oCustomer.BranchId
            params1(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params1(1).Value = oCustomer.ApplicationID
            params1(2) = New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt)
            params1(2).Value = oCustomer.AssetSeqNo
            params1(3) = New SqlParameter("@InsSequenceNo", SqlDbType.SmallInt)
            params1(3).Value = oCustomer.InsSequenceNo
            params1(4) = New SqlParameter("@PremiGross", SqlDbType.Decimal)
            params1(4).Value = oCustomer.PremiGross
            params1(5) = New SqlParameter("@Komisi", SqlDbType.Decimal)
            params1(5).Value = oCustomer.Komisi
            params1(6) = New SqlParameter("@PremiNet", SqlDbType.Decimal)
            params1(6).Value = oCustomer.PremiNet
            params1(7) = New SqlParameter("@PPN", SqlDbType.Decimal)
            params1(7).Value = oCustomer.PPN
            params1(8) = New SqlParameter("@PPH", SqlDbType.Decimal)
            params1(8).Value = oCustomer.PPH
            params1(9) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
            params1(9).Value = oCustomer.AdminFee
            params1(10) = New SqlParameter("@MeteraiFee", SqlDbType.Decimal)
            params1(10).Value = oCustomer.MeteraiFee
            params1(11) = New SqlParameter("@BillAmount", SqlDbType.Decimal)
            params1(11).Value = oCustomer.BillAmount
            params1(12) = New SqlParameter("@ProductInsurance", SqlDbType.VarChar, 2)
            params1(12).Value = oCustomer.ProductInsurance
            params1(13) = New SqlParameter("@SelisihInsurancePayable", SqlDbType.Decimal)
            params1(13).Value = oCustomer.BeforeBalance
            params1(14) = New SqlParameter("@PaymentAllocationIDSelisih", SqlDbType.Char, 11)
            params1(14).Value = oCustomer.JournalCode

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "spNotaAsuransiSave", params1)

            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()            
            Throw New Exception(exp.Message)
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Sub
End Class
