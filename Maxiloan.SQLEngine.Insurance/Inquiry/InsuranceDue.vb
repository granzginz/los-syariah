
#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InsuranceDue : Inherits DataAccessBase
#Region "Constanta"
    Private Const INSURANCE_DUE_PAGING As String = "spInsuranceDuePaging"
    Private Const INSURANCE_DUE_REPORT As String = "spInsuranceDueReport"
#End Region
    Public Function InsuranceDuepaging(ByVal oCustomclass As Parameter.InsuranceDue) As Parameter.InsuranceDue
        Dim params(4) As SqlParameter
        Try
            With oCustomclass
                params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
                params(0).Value = .CurrentPage
                params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
                params(1).Value = .PageSize
                params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
                params(2).Value = .WhereCond
                params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
                params(3).Value = .SortBy
                params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
                params(4).Direction = ParameterDirection.Output
            End With
            oCustomclass.ListData = SqlHelper.ExecuteDataset(oCustomclass.strConnection, CommandType.StoredProcedure, INSURANCE_DUE_PAGING, params).Tables(0)
            oCustomclass.TotalRecord = CType(params(4).Value, Int16)
            Return oCustomclass

        Catch exp As Exception
            WriteException("InsuranceDue", "InsuranceDuepaging", exp.Message + exp.StackTrace)

        End Try
    End Function
    Public Function InsuranceDueReport(ByVal oCustomclass As Parameter.InsuranceDue) As Parameter.InsuranceDue
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@where", SqlDbType.VarChar, 1000)
            params(0).Value = oCustomclass.wherecond
            params(1) = New SqlParameter("@Sort", SqlDbType.VarChar, 50)
            params(1).Value = oCustomclass.SortBy

            oCustomclass.ListReport = SqlHelper.ExecuteDataset(oCustomclass.strConnection, CommandType.StoredProcedure, INSURANCE_DUE_REPORT, params)

            Return oCustomclass
        Catch exp As Exception
            WriteException("InsuranceDue", "InsuranceDueReport", exp.Message + exp.StackTrace)

        End Try
    End Function

End Class
