
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InsuranceInq : Inherits DataAccessBase
    Private Const spInqInsInvoice As String = "spInqInsInvoice"
    Private Const spInqInsExpired As String = "spInqInsExpired"
    Private Const spRptInsStatistic As String = "spRptInsStatistic"
    Private Const PARAM_BUSSINES_DATE As String = "@BusinessDate"
    Private Const PARAM_DAYTOEXPIRED As String = "@daytoexpired"
    Private Const PARAM_WHERECOND2 As String = "@wherecond2"
    Private Const PARAM_STATISTICYEAR As String = "@StatisticYear"
    Protected Const PARAM_STRKEY As String = "@strKey"
    Protected Const PARAM_CGID As String = "@CGID"
    Protected Const PARAM_COLLECTORTYPE As String = "@CollectorType"
    Public Function InqInsInvoice(ByVal customclass As Parameter.InsInqEntities) As Parameter.InsInqEntities

        Dim params(4) As SqlParameter

        With customclass

            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 8000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = .SortBy
            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
            params(4).Direction = ParameterDirection.Output

        End With

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInqInsInvoice, params).Tables(0)
            customclass.TotalRecord = CType(params(4).Value, Int16)

            Return customclass
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function InqInsExpired(ByVal customclass As Parameter.InsInqEntities) As Parameter.InsInqEntities

        Dim params(7) As SqlParameter

        With customclass

            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = .CurrentPage
            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = .PageSize
            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = .WhereCond
            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 1000)
            params(3).Value = .SortBy
            params(4) = New SqlParameter(PARAM_BUSSINES_DATE, SqlDbType.DateTime)
            params(4).Value = .BusinessDate
            params(5) = New SqlParameter(PARAM_DAYTOEXPIRED, SqlDbType.Int)
            params(5).Value = .DayToExpired
            params(6) = New SqlParameter(PARAM_WHERECOND2, SqlDbType.VarChar, 1000)
            params(6).Value = .WhereCond2
            params(7) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.Int)
            params(7).Direction = ParameterDirection.Output


        End With

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spInqInsExpired, params).Tables(0)
            customclass.TotalRecord = CType(params(7).Value, Int16)

            Return customclass
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetReportInsStatitistic(ByVal customclass As Parameter.InsInqEntities) As Parameter.InsInqEntities
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter(PARAM_STATISTICYEAR, SqlDbType.VarChar, 4)
        params(1).Value = customclass.StatisticYear


        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRptInsStatistic, params)
        Return customclass
    End Function
End Class
