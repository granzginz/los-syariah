
#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InsEndorsInq : Inherits Maxiloan.SQLEngine.DataAccessBase
    'description : fungsi ini untuk menampilkan data customer yang telah mengajukan endorsment ke datagrid
    Public Function InsInqEndorsList(ByVal customclass As Parameter.InsEndorsInq) As Parameter.InsEndorsInq
        Dim data As New Parameter.InsEndorsInq
        Dim params() As SqlParameter = New SqlParameter(6) {}
        params(0) = New SqlParameter("@piintCurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@piintPageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize
        params(2) = New SqlParameter("@pivchrWhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond
        params(3) = New SqlParameter("@pivchrSortBy", SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy
        params(4) = New SqlParameter("@pivchCriteria", SqlDbType.VarChar, 100)
        params(4).Value = customclass.Criteria
        params(5) = New SqlParameter("@eventdate", SqlDbType.VarChar, 100)
        params(5).Value = customclass.EventDate
        params(6) = New SqlParameter("@pointTotalRecords", SqlDbType.SmallInt)
        params(6).Direction = ParameterDirection.Output
        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spInsEndorsmentAmandment", params).Tables(0)
            data.TotalRecord = CInt(params(6).Value)
            Return data
        Catch exp As Exception
            WriteException("InsInqEndors", "InsuranceInquiryAmandment", exp.Message + exp.StackTrace)
        End Try
    End Function
    'description fungsi ini untuk mengambil data endorsment history yang pernah dilakukan oleh user
    Public Function InsInqEndorsDetail(ByVal customclass As Parameter.InsEndorsInq) As Parameter.InsEndorsInq
        Dim data As New Parameter.InsEndorsInq
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(2).Value = customclass.AssetSequenceNo
        params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(3).Value = customclass.InsSequenceNo
        params(4) = New SqlParameter("@BDEndors", SqlDbType.Int)
        params(4).Value = customclass.BDEndors
        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spInsEndorsmentHistoryList", params).Tables(0)
            data.Detail = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spInsEndorsmentHistoryDetail", params).Tables(0)

            Return data
        Catch exp As Exception
            WriteException("InsInqEndors", "InsuranceEndorsmentHistory", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
