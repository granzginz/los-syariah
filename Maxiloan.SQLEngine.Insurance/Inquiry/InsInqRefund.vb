
#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region
Public Class InsInqRefund : Inherits Maxiloan.SQLEngine.DataAccessBase
    'description : fungsi ini untuk menampilkan data customer yang mengajukan refund ke datagrid
    Public Function InsInqRefundList(ByVal customclass As Parameter.InsInqRefund) As Parameter.InsInqRefund
        Dim data As New Parameter.InsInqRefund
        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@piintCurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@piintPageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize
        params(2) = New SqlParameter("@pivchrWhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond
        params(3) = New SqlParameter("@pivchrSortBy", SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy
        params(4) = New SqlParameter("@pivchCriteria", SqlDbType.VarChar, 100)
        params(4).Value = customclass.criteria
        params(5) = New SqlParameter("@pointTotalRecords", SqlDbType.SmallInt)
        params(5).Direction = ParameterDirection.Output
        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spInsRefundView", params).Tables(0)
            data.TotalRecord = CInt(params(5).Value)
            Return data
        Catch exp As Exception
            WriteException("InsInqRefund", "InsuranceInquiryRefund", exp.Message + exp.StackTrace)
        End Try

    End Function




    'description : fungsi ini untuk menampilkan data detil customer jika icon terminate diklik
    Public Function InsInqRefundDetail(ByVal customclass As Parameter.InsInqRefund) As Parameter.InsInqRefund
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int, 3)
        params(2).Value = customclass.AssetSequenceNo
        params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int, 3)
        params(3).Value = customclass.InsSequenceNo


        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spInsRefundDetail", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("InsInqRefund", "InsuranceInquiryRefundDetail", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
