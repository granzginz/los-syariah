

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.General

#End Region

Public Class Renewal

#Region "Constanta"
    Private Const SP_GET_RENEWAL_DETAIL As String = "spCoverageRenewalViewTop"
    Private Const SP_GET_RENEWAL_DETAIL_NEW_COVERAGE As String = "spCoverageRenewalNewCoverage"
    Private Const SP_GET_GRID_INSURANCE_RENEWAL As String = "spCoverageRenewalGridIns"
    Private Const SP_CALCULATE_SAVE_RENEWAL As String = "SpInsRenewalCalculateSave"
    'Private Const SP_CALCULATE_SAVE_RENEWAL As String = "SpInsRenewalCalculateSaveTest29Sept2003"
#End Region
#Region "GetRenewalDetail"

    Public Function GetRenewalDetailTop(ByVal customClass As Parameter.Renewal) As Parameter.Renewal
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@WhereBy", SqlDbType.Char, 1000)
        params(0).Value = customClass.WhereCond
        Try
            Dim oReturnValue As New Parameter.Renewal
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, SP_GET_RENEWAL_DETAIL, params)
            If reader.Read Then
                oReturnValue.BranchId = reader("BranchId").ToString.Trim
                oReturnValue.ApplicationID = reader("ApplicationID").ToString.Trim
                oReturnValue.AgreementNo = reader("AgreementNo").ToString.Trim
                oReturnValue.InsuranceComBranchName = reader("InsuranceComBranchName").ToString.Trim
                oReturnValue.CustomerID = reader("CustomerID").ToString.Trim
                oReturnValue.CustomerName = reader("CustomerName").ToString.Trim
                oReturnValue.AssetseqNo = CType(reader("AssetseqNo").ToString, Short)
                oReturnValue.InsseqNo = CType(reader("InsSequenceNo").ToString, Short)
                oReturnValue.AssetMasterDescr = reader("AssetMasterDescr").ToString.Trim
                oReturnValue.SerialNo1 = reader("SerialNo1").ToString.Trim
                oReturnValue.SerialNo2 = reader("SerialNo2").ToString.Trim
                oReturnValue.ApplicationType = reader("ApplicationType").ToString.Trim
                oReturnValue.ManufacturingYear = reader("ManufacturingYear").ToString.Trim
                oReturnValue.InsuranceTypeDescr = reader("InsuranceTypeDescr").ToString.Trim
                oReturnValue.ApplicationTypeDescr = reader("ApplicationTypeDescr").ToString.Trim
                oReturnValue.StartDate = CType(reader("StartDate").ToString, Date)
                oReturnValue.EndDate = CType(reader("EndDate").ToString, Date)
                oReturnValue.UsedNew = reader("UsedNew").ToString.Trim
                oReturnValue.AccNotes = reader("AccNotes").ToString.Trim
                oReturnValue.InsNotes = reader("InsNotes").ToString.Trim
                oReturnValue.AssetUsageDescr = reader("AssetUsageDescr").ToString.Trim
                oReturnValue.PolicyNumber = reader("PolicyNumber").ToString.Trim

                oReturnValue.SumInsured = CType(reader("SumInsured").ToString, Double)
                oReturnValue.MainPremiumToCust = CType(reader("MainPremiumToCust").ToString, Double)
                oReturnValue.SRCCToCust = CType(reader("SRCCToCust").ToString, Double)
                oReturnValue.TPLToCust = CType(reader("TPLToCust").ToString, Double)
                oReturnValue.FloodToCust = CType(reader("FloodToCust").ToString, Double)
                oReturnValue.AdminFeeToCust = CType(reader("AdminFeeToCust").ToString, Double)
                oReturnValue.MeteraiFeeToCust = CType(reader("MeteraiFeeToCust").ToString, Double)
                oReturnValue.DiscToCustAmount = CType(reader("DiscToCustAmount").ToString, Double)
                '    oReturnValue.PremiumAmountByCust = CType(reader("PremiumAmountByCust").ToString, Double)
            End If

            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("Renewal.vb", "GetRenewalDetailTop", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        End Try
    End Function
#End Region
#Region "GetRenewalNewCoverage"
    Public Function GetRenewalNewCoverage(ByVal customClass As Parameter.Renewal) As Parameter.Renewal
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@WhereBy", SqlDbType.Char, 1000)
        params(0).Value = customClass.WhereCond
        Try
            Dim oReturnValue As New Parameter.Renewal
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, SP_GET_RENEWAL_DETAIL_NEW_COVERAGE, params)
            If reader.Read Then
                oReturnValue.BranchId = reader("BranchId").ToString.Trim
                oReturnValue.ApplicationID = reader("ApplicationID").ToString.Trim
                oReturnValue.AgreementNo = reader("AgreementNo").ToString.Trim
                oReturnValue.AccNotes = reader("AccNotes").ToString.Trim
                oReturnValue.InsNotes = reader("InsNotes").ToString.Trim
                'oReturnValue.AssetUsageDescr = reader("AssetUsageDescr").ToString.Trim
                oReturnValue.EndDate = CType(reader("EndDate").ToString, Date)
                oReturnValue.SumInsured = CType(reader("SumInsured").ToString, Double)
                oReturnValue.MainPremiumToCust = CType(reader("MainPremiumToCust").ToString, Double)
                oReturnValue.SRCCToCust = CType(reader("SRCCToCust").ToString, Double)
                oReturnValue.TPLToCust = CType(reader("TPLToCust").ToString, Double)
                oReturnValue.FloodToCust = CType(reader("FloodToCust").ToString, Double)
                oReturnValue.AdminFeeToCust = CType(reader("AdminFeeToCust").ToString, Double)
                oReturnValue.MeteraiFeeToCust = CType(reader("MeteraiFeeToCust").ToString, Double)
                oReturnValue.DiscToCustAmount = CType(reader("DiscToCustAmount").ToString, Double)
                oReturnValue.PremiumAmountByCust = CType(reader("PremiumAmountByCust").ToString, Double)
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("Renewal.vb", "GetRenewalNewCoverage", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        End Try
    End Function
#End Region
#Region "CalculateAndSaveRenewal -"

    Public Function CalculateAndSaveRenewal(ByVal customClass As Parameter.Renewal) As Parameter.Renewal

        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction

        Dim oReturnValue As New Parameter.Renewal

        If objConnection.State = ConnectionState.Closed Then objConnection.Open()
        objtrans = objConnection.BeginTransaction

        Dim params(13) As SqlParameter
        params(0) = New SqlParameter("@branchid", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@AgreementNo", SqlDbType.VarChar, 20)
        params(1).Value = customClass.AgreementNo
        params(2) = New SqlParameter("@NewSumInsured", SqlDbType.Float)
        params(2).Value = customClass.SumInsured
        params(3) = New SqlParameter("@InsNotes", SqlDbType.VarChar, 500)
        params(3).Value = customClass.InsNotes
        params(4) = New SqlParameter("@AccNotes", SqlDbType.VarChar, 500)
        params(4).Value = customClass.AccNotes
        params(5) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 500)
        params(5).Value = customClass.CoverageType
        params(6) = New SqlParameter("@MainPremiumToCust", SqlDbType.Float)
        params(6).Value = customClass.MainPremiumToCust
        params(7) = New SqlParameter("@SRCCAmountToCust", SqlDbType.Float)
        params(7).Value = customClass.SRCCToCust
        params(8) = New SqlParameter("@FloodAmountToCust", SqlDbType.Float)
        params(8).Value = customClass.FloodToCust
        params(9) = New SqlParameter("@TPLPremiumToCust", SqlDbType.Float)
        params(9).Value = customClass.TPLPremium
        params(10) = New SqlParameter("@LoadingFeeToCustomer", SqlDbType.Float)
        params(10).Value = customClass.LoadingFeeToCust
        params(11) = New SqlParameter("@PremiumAmountByCust", SqlDbType.Float)
        params(11).Value = customClass.PremiumAmountByCust
        params(12) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(12).Value = customClass.BusinessDate
        params(13) = New SqlParameter("@tplAmount", SqlDbType.Float)
        params(13).Value = customClass.TPLToCust

        Try
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SP_CALCULATE_SAVE_RENEWAL, params)
            objtrans.Commit()
        Catch ex As Exception
            objtrans.Rollback()
            Dim err As New MaxiloanExceptions
            err.WriteLog("Renewal.vb", "CalculateAndSaveRenewal", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try


    End Function
#End Region
#Region "GetCoverageRenewalGridInsCALCULATE"

    Public Function GetCoverageRenewalGridInsCALCULATE(ByVal customClass As Parameter.Renewal) As Parameter.Renewal
        'Dim oReturnValue As New Entities.GeneralPaging
        Dim params(7) As SqlParameter
        params(0) = New SqlParameter("@branchid", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@AgreementNo", SqlDbType.VarChar, 20)
        params(1).Value = customClass.AgreementNo
        params(2) = New SqlParameter("@NewSumInsured", SqlDbType.Float)
        params(2).Value = customClass.SumInsured
        params(3) = New SqlParameter("@CoverageType", SqlDbType.VarChar, 5)
        params(3).Value = customClass.CoverageType
        params(4) = New SqlParameter("@BolSRCC", SqlDbType.Bit)
        params(4).Value = customClass.BolSRCC
        params(5) = New SqlParameter("@TPLAmount", SqlDbType.Float)
        params(5).Value = customClass.TPLToCust
        params(6) = New SqlParameter("@BolFlood", SqlDbType.Bit)
        params(6).Value = customClass.BolFlood
        params(7) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(7).Value = customClass.BusinessDate

        Try
            customClass.ListDataSet = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spCoverageRenewalGridInsCALCULATE", params)
            Return customClass
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("Renewal.vb", "DataAccess Method GetCoverageRenewalGridInsCALCULATE ", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try
    End Function

#End Region


End Class
