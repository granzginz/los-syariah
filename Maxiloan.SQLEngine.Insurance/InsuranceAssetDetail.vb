

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.General

#End Region

Public Class InsuranceAssetDetail

#Region "Private Constanta"

    Private Const SP_VIEW_INSURANCE_DETAIL As String = "spViewInsuranceDetail"
    Private Const SP_VIEW_ASSET_INSURANCE_DETAIL As String = "spViewAssetInsuranceDetail"

#End Region

    Public Function GetViewInsuranceDetail(ByVal customClass As Parameter.InsuranceAssetDetail) As Parameter.InsuranceAssetDetail
        Try

            Dim reader As SqlDataReader
            Dim params(0) As SqlParameter
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 2000)
            params(0).Value = customClass.WhereCond

            Select Case customClass.PageSource

                Case CommonVariableHelper.PAGE_SOURCE_INSURANCE_DETAIL_INFORMATION
                    Dim oReturnValue As New Parameter.InsuranceAssetDetail
                    reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, SP_VIEW_INSURANCE_DETAIL, params)
                    If reader.Read Then
                        oReturnValue.BranchId = reader("BranchId").ToString.Trim
                        oReturnValue.ApplicationID = reader("ApplicationID").ToString.Trim
                        oReturnValue.AgreementNo = reader("AgreementNo").ToString.Trim
                        oReturnValue.CustomerID = reader("CustomerID").ToString.Trim
                        oReturnValue.CustomerName = reader("CustomerName").ToString.Trim
                        oReturnValue.AssetCode = reader("AssetCode").ToString.Trim
                        oReturnValue.AssetMasterDescription = reader("AssetMasterDescription").ToString.Trim
                        oreturnvalue.AccountPayableNo = reader("AccountPayableNo").ToString.Trim

                    End If

                    reader.Close()
                    Return oReturnValue




                Case CommonVariableHelper.PAGE_SOURCE_ASSET_INSURANCE_DETAIL_INFORMATION

                    Dim oReturnValue As New Parameter.InsuranceAssetDetail
                    reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, SP_VIEW_ASSET_INSURANCE_DETAIL, params)
                    If reader.Read Then
                        oReturnValue.BranchId = reader("BranchId").ToString.Trim
                        oReturnValue.ApplicationID = reader("ApplicationID").ToString.Trim
                        oreturnvalue.AgreementNo = reader("agreementno").ToString.Trim
                        oReturnvalue.AssetseqNo = CType(reader("AssetSeqno").ToString.Trim, Short)
                        oreturnvalue.InsseqNo = CType(reader("InssequenceNo").ToString.Trim, Short)
                        oReturnValue.InsuranceComBranchID = reader("MaskAssBranchID").ToString.Trim
                        oReturnValue.InsuranceComBranchName = reader("MaskAssBranchName").ToString.Trim
                        oReturnValue.SumInsured = CType(reader("SumInsured").ToString.Trim, Double)
                        oReturnValue.InsuredByDescr = reader("InsuredByDescr").ToString.Trim
                        oReturnValue.PaidByDescr = reader("PaidByDescr").ToString.Trim
                        If reader("StartDate").ToString <> "-" Then
                            oReturnValue.StartDate = CType(reader("StartDate").ToString, Date)
                        End If
                        If reader("EndDate").ToString <> "-" Then
                            oReturnValue.EndDate = CType(reader("EndDate").ToString, Date)
                        End If
                        If reader("InsCoSelectionDate").ToString <> "-" Then
                            oReturnValue.InsCoSelectionDate = CType(reader("InsCoSelectionDate").ToString, Date)
                        End If

                        oReturnValue.SPPANo = reader("SPPANo").ToString.Trim
                        If reader("SPPADate").ToString <> "-" Then
                            oReturnValue.SPPADate = CType(reader("SPPADate").ToString, Date)
                        End If
                        If reader("InsActivateDate").ToString <> "-" Then
                            oReturnValue.InsActivateDate = CType(reader("InsActivateDate").ToString, Date)
                        End If

                        oReturnValue.PolicyNumber = reader("PolicyNumber").ToString.Trim
                        If reader("PolicyReceiveDate").ToString <> "-" Then
                            oReturnValue.PolicyReceiveDate = CType(reader("PolicyReceiveDate").ToString, Date)
                        End If

                        oReturnValue.PremiumBaseForRefundSupp = CType(reader("PremiumBaseForRefundSupp").ToString.Trim, Double)
                        oReturnValue.AccNotes = reader("AccNotes").ToString.Trim
                        oReturnValue.InsNotes = reader("InsNotes").ToString.Trim
                        oReturnValue.MainPremiumToCust = CType(reader("MainPremiumToCust").ToString.Trim, Double)
                        oReturnValue.LoadingFeeToCust = CType(reader("LoadingFeeToCust").ToString.Trim, Double)
                        oReturnValue.LoadingFeeToInsco = CType(reader("LoadingFeeToInsco").ToString.Trim, Double)
                        oReturnValue.SRCCToCust = CType(reader("SRCCToCust").ToString.Trim, Double)

                        oReturnValue.FloodToCust = CType(reader("FloodToCust").ToString.Trim, Double)
                        oReturnValue.TPLToCust = CType(reader("TPLToCust").ToString.Trim, Double)
                        oReturnValue.AdminFeeToCust = CType(reader("AdminFeeToCust").ToString.Trim, Double)
                        oReturnValue.MeteraiFeeToCust = CType(reader("MeteraiFeeToCust").ToString.Trim, Double)

                        oReturnValue.PremiumAmountByCustBeforeDisc = CType(reader("PremiumAmountByCustBeforeDisc").ToString.Trim, Double)

                        oReturnValue.DiscToCustAmount = CType(reader("DiscToCustAmount").ToString.Trim, Double)
                        oReturnValue.PremiumAmountByCust = CType(reader("PremiumAmountByCust").ToString.Trim, Double)
                        oReturnValue.MainPremiumToInsco = CType(reader("MainPremiumToInsCo").ToString.Trim, Double)
                        oReturnValue.RSCCToInsco = CType(reader("SRCCToInsco").ToString.Trim, Double)
                        oReturnValue.FloodToInsco = CType(reader("FloodToInsCo").ToString.Trim, Double)
                        oReturnValue.TPLToInsco = CType(reader("TPLToInsco").ToString.Trim, Double)
                        oReturnValue.AdminFee = CType(reader("AdminFee").ToString.Trim, Double)
                        oReturnValue.MeteraiFee = CType(reader("MeteraiFee").ToString.Trim, Double)
                        oReturnValue.PremiumAmountToInsco = CType(reader("PremiumAmountToInsCo").ToString.Trim, Double)
                        oReturnValue.CoverPeriod = reader("CoverPeriod").ToString.Trim

                        oReturnValue.InvoiceNo = reader("InvoiceNo").ToString.Trim
                        If reader("InvoiceDate").ToString <> "-" Then
                            oReturnValue.PolicyReceiveDate = CType(reader("InvoiceDate").ToString, Date)
                        End If
                        oReturnValue.PaidAmountByCust = CType(reader("PaidAmountByCust").ToString.Trim, Double)
                        oReturnValue.PaidAmountToInsco = CType(reader("PaidAmountToInsco").ToString.Trim, Double)

                        oReturnValue.PAToCust = CType(reader("PAToCust").ToString.Trim, Double)
                        oReturnValue.EQVETToCust = CType(reader("EQVETToCust").ToString.Trim, Double)
                        oReturnValue.TERRORISMToCust = CType(reader("TERRORISMToCust").ToString.Trim, Double)
                        oReturnValue.PADriverToCust = CType(reader("PADriverToCust").ToString.Trim, Double)
                        oReturnValue.PAToInsco = CType(reader("PAToInsco").ToString.Trim, Double)
                        oReturnValue.EQVETToInsco = CType(reader("EQVETToInsco").ToString.Trim, Double)
                        oReturnValue.TERRORISMToInsco = CType(reader("TERRORISMToInsco").ToString.Trim, Double)
                        oReturnValue.PADriverToInsco = CType(reader("PADriverToInsco").ToString.Trim, Double)
                        oReturnValue.TotalRate = CType(reader("TotalInsuranceRate").ToString.Trim, Double)

                        'By Anjar: 18/11/2017
                        'Add Detail premi CP and JK
                        oReturnValue.TotalPremiCP = CType(reader("PremiCreditProtection").ToString.Trim, Double)
                        oReturnValue.TotalPremiJK = CType(reader("PremiJaminanCredit").ToString.Trim, Double)

                    End If

                    reader.Close()
                    Return oReturnValue

            End Select

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsuranceAssetDetail.vb", "DataAccess", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try


    End Function



End Class
