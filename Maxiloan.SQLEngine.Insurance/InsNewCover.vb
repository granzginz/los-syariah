
#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class InsNewCover : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Private Const CHECK_PROSPECT As String = "spInsCheckProspectSelect"
    Private Const INSURANCEBYCOMPANYPAID_STEP_1 As String = "spInsbyCompanyPaidByCustomerSelect"

    Private Const SQL_QUERY_FILL_INSURANCE_COYBRANCH As String = "select InsuranceComBranchid AS ID ,InsuranceComBranchname as Name from InsuranceComBranch "
    Private Const SQL_QUERY_SELECT_COVERAGE_TYPE As String = "Select ID,Description as Name from tblCoverageType Order by ID "
#End Region
    'description : fungsi ini untuk menampilkan data customer yang mengajukan refund ke datagrid
    Public Function InsNewCoverView(ByVal customclass As Parameter.InsNewCover) As Parameter.InsNewCover
        Dim data As New Parameter.InsNewCover
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@piintCurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@piintPageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize
        params(2) = New SqlParameter("@pivchrWhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond
        params(3) = New SqlParameter("@pivchrSortBy", SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy
        params(4) = New SqlParameter("@pointTotalRecords", SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output
        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spNewCoverView", params).Tables(0)
            data.TotalRecord = CInt(params(4).Value)
            Return data
        Catch exp As Exception
            WriteException("InsNewCover", "Insurance NewCover", exp.Message + exp.StackTrace)
        End Try

    End Function
    'description : fungsi ini untuk menampilkan insuredby ke combo insuredby
    Public Function InsNewCoverGetInsured(ByVal customclass As Parameter.InsNewCover) As Parameter.InsNewCover
        Dim data As New Parameter.InsNewCover

        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spNewCoverGetInsured").Tables(0)

            Return data
        Catch exp As Exception
            WriteException("InsNewCover", "Insurance NewCover", exp.Message + exp.StackTrace)
        End Try

    End Function
    'description : fungsi ini untuk menampilkan informasi paid by ke combo paidby
    Public Function InsNewCoverGetPaid(ByVal customclass As Parameter.InsNewCover) As Parameter.InsNewCover
        Dim data As New Parameter.InsNewCover

        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spNewCoverGetPaid").Tables(0)

            Return data
        Catch exp As Exception
            WriteException("InsNewCover", "Insurance NewCover", exp.Message + exp.StackTrace)
        End Try

    End Function
    'description : fungsi ini untuk menampilkan segala informasi customer ke form jika dipilih insured by company
    Public Function GetInsuranceEntryStep1List(ByVal customClass As Parameter.InsNewCover) As Parameter.InsNewCover
        Dim reader As SqlDataReader
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = customClass.ApplicationID.Trim
        params(1) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(1).Value = customClass.BusinessDate



        Try
            Dim oReturnValue As New Parameter.InsNewCover
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, "spNewCoverPaidByCustomerSelect", params)
            If reader.Read Then

                oReturnValue.BranchId = reader("BranchId").ToString.Trim
                oReturnValue.ApplicationID = reader("ApplicationID").ToString.Trim
                oReturnValue.CustomerId = reader("CustomerID").ToString.Trim
                oReturnValue.CustomerName = reader("CustomerName").ToString.Trim
                oReturnValue.AssetUsageNewUsed = reader("UsedNew").ToString
                oReturnValue.AssetMasterDescr = reader("AssetMasterDescr").ToString
                oReturnValue.InsuranceAssetDescr = reader("InsuranceAssetTypeDescr").ToString
                oReturnValue.AssetUsageDescr = reader("AssetUsageDescr").ToString
                oReturnValue.InsAssetInsuredByName = reader("InsByDescr").ToString
                oReturnValue.InsAssetPaidByName = reader("PaidByDescr").ToString
                oReturnValue.TotalOTR = CType(reader("TotalOTR").ToString, Double)
                oReturnValue.Tenor = CType(reader("Tenor").ToString, Double)
                oReturnValue.MinimumTenor = CType(reader("MinimumTenor"), Int16)
                oReturnValue.MaximumTenor = CType(reader("MaximumTenor"), Int16)
                oReturnValue.PBMinimumTenor = CType(reader("PBMinimumTenor"), Int16)
                oReturnValue.PBMaximumTenor = CType(reader("PBMaximumTenor"), Int16)
                oReturnValue.PMinimumTenor = CType(reader("PMinimumTenor"), Int16)
                oReturnValue.PMaximumTenor = CType(reader("PMaximumTenor"), Int16)
                oReturnValue.InsAdminFee = CType(reader("InsAdminFee"), Double)
                oReturnValue.InsStampDutyFee = CType(reader("InsStampDutyFee"), Double)
                oReturnValue.InsAdminFeeBehaviour = reader("InsAdminFeeBehaviour").ToString
                oReturnValue.InsStampDutyFeeBehaviour = reader("InsStampDutyFeeBehaviour").ToString
                oReturnValue.InsuranceType = reader("InsRateCategory").ToString
                oReturnValue.AssetUsageID = reader("AssetUsageID").ToString
                oReturnValue.ManufacturingYear = reader("ManufacturingYear").ToString
                oReturnValue.Prepaid = CDbl(reader("Prepaid"))
                oReturnValue.MaturityDate = CType(reader("MaturityDate"), DateTime)
                oReturnValue.InsLength = CType(reader("inslength"), Integer)
            End If
            reader.Close()
            Return oReturnValue
        Catch exp As Exception
            WriteException("NewCoverInsuranceByCompany", "GetInsuranceEntryStep1List", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
            ' Throw New Exception("Error On DataAccess.Marketing.AssetTypeAttribute.GetAssetTypeAttributeEdit")
        End Try
    End Function
    'description : fungsi ini untuk menampilkan insco ke combo insco
    Public Function FillInsuranceComBranch(ByVal customClass As Parameter.InsNewCover) As DataTable
        Dim oReader As SqlDataReader
        Dim dttResult As New DataTable
        Dim dtrResult As DataRow

        dttResult.Columns.Add("ID", GetType(String))
        dttResult.Columns.Add("Name", GetType(String))
        Try
            oReader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.Text, SQL_QUERY_FILL_INSURANCE_COYBRANCH & " Where BranchID = '" & customClass.BranchId.Trim & "'")

            While oReader.Read
                dtrResult = dttResult.NewRow
                dtrResult("ID") = oReader("ID").ToString
                dtrResult("Name") = oReader("Name").ToString
                dttResult.Rows.Add(dtrResult)
            End While
            Return dttResult
        Catch exp As Exception
            WriteException("NewCoverInsuranceByCompany", "FillInsuranceComBranch", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
    'description fungsi ini untuk menampilkan informasi tentang asuransi customer ke datagrid jika diklik btn ok
    Public Function GetInsuranceEntryStep2List(ByVal customClass As Parameter.InsNewCover) As Parameter.InsNewCover
        Dim oReturnValue As New Parameter.InsNewCover
        Dim params(2) As SqlParameter

        params(0) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(0).Value = customClass.BusinessDate
        params(1) = New SqlParameter("@MaturityDate", SqlDbType.DateTime)
        params(1).Value = customClass.MaturityDate
        params(2) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(2).Value = customClass.BranchId.Trim
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spNewCoverInsByCompany", params).Tables(0)
            Return oReturnValue
        Catch exp As Exception
            WriteException("NewCoverInsuranceByCompany", "GetInsuranceEntryStep2List", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.GetPremiumToCustomer")
        End Try
    End Function
    'description : sub ini untuk menghapus segala data customer di tabel insuranceasset dan insuranceassetdetail jika tombol cancel diklik
    Public Sub InsNewCoverInsuredByCustDelete(ByVal customClass As Parameter.InsNewCover)
        Dim oReturnValue As New Parameter.InsNewCover
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId.Trim
        params(1) = New SqlParameter("@Applicationid", SqlDbType.Char, 20)
        params(1).Value = customClass.ApplicationID.Trim
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, "spNewCoverInsuredByCustDelete", params)

        Catch exp As Exception
            WriteException("NewCoverInsuranceByCompany", "GetInsuranceEntryStep2List", exp.Message + exp.StackTrace)
            Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.GetPremiumToCustomer")
        End Try
    End Sub
    'description : fungsi ini untuk mengecek prospek yang ada
    Public Function CheckProspect(ByVal CustomClass As Parameter.InsNewCover) As Parameter.InsNewCover
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = CustomClass.ApplicationID.Trim

        Try
            Dim oReturnValue As New Parameter.InsNewCover
            reader = SqlHelper.ExecuteReader(CustomClass.strConnection, CommandType.StoredProcedure, CustomClass.SpName, params)
            If reader.Read Then
                oReturnValue.MaskAssID = reader("MaskAssID").ToString.Trim
                oReturnValue.InsuranceComBranchID = reader("InsuranceComBranchID").ToString.Trim
                oReturnValue.InsuranceComBranchName = reader("InsuranceComBranchName").ToString.Trim
            End If
            reader.Close()
            Return oReturnValue
        Catch exp As Exception
            WriteException("NewCoverInsuranceByCompany", "CheckProspect", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("NewCoverInsuranceByCompany-DA", "CheckProspect", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
            Throw New Exception(exp.StackTrace)
        End Try
    End Function
    'description : sub ini untuk menyimpan segala data tentang cust tipe insured by company jika btn save diklik
    Public Sub ProcessSaveInsuranceApplicationLastProcess(ByVal customClass As Parameter.InsNewCover)

        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            Dim params(12) As SqlParameter
            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customClass.ApplicationID
            params(2) = New SqlParameter("@AmountCoverage", SqlDbType.Decimal)
            params(2).Value = customClass.AmountCoverage
            params(3) = New SqlParameter("@AdminFeeToCust", SqlDbType.Decimal)
            params(3).Value = customClass.AdminFeeToCust
            params(4) = New SqlParameter("@MeteraiFeeToCust", SqlDbType.Decimal)
            params(4).Value = customClass.MeteraiFeeToCust
            params(5) = New SqlParameter("@DiscToCustAmount", SqlDbType.Decimal)
            params(5).Value = customClass.DiscToCustAmount


            params(6) = New SqlParameter("@AccNotes", SqlDbType.Char, 500)
            params(6).Value = customClass.AccNotes
            params(7) = New SqlParameter("@InsNotes", SqlDbType.Char, 500)
            params(7).Value = customClass.InsNotes
            params(8) = New SqlParameter("@InsLength", SqlDbType.SmallInt)
            params(8).Value = customClass.InsLength
            params(9) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(9).Value = customClass.BusinessDate
            params(10) = New SqlParameter("@PremiumAmountByCust", SqlDbType.Decimal)
            params(10).Value = customClass.PremiumToCustAmount
            params(11) = New SqlParameter("@InsuranceComBranchID", SqlDbType.Char, 20)
            params(11).Value = customClass.InsuranceComBranchID
            params(12) = New SqlParameter("@IsPageSource", SqlDbType.Bit)
            params(12).Value = customClass.IsPageSource



            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spNewCoverInsuranceByCompanyLastProcess", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("InsuranceCalculation", "ProcessSaveInsuranceApplicationLastProcess", exp.Message + exp.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsuranceCalculation.vb", "ProcessSaveInsuranceApplicationLastProcess", exp.Source, exp.TargetSite.Name, exp.Message, exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try


    End Sub
    'description : sub ini untuk menyimpan data cust tipe insured  by cust jika btn save diklik
    Public Sub ProcessSaveInsuranceByCustomer(ByVal customClass As Parameter.InsNewCover)
        Dim objConnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            Dim params(9) As SqlParameter
            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(1).Value = customClass.ApplicationID
            params(2) = New SqlParameter("@InsuranceCompany", SqlDbType.Char, 50)
            params(2).Value = customClass.InsuranceCompanyName
            params(3) = New SqlParameter("@SumInsured", SqlDbType.Decimal)
            params(3).Value = customClass.SumInsured
            params(4) = New SqlParameter("@CoverageType", SqlDbType.Char, 3)
            params(4).Value = customClass.CoverageType.Trim
            params(5) = New SqlParameter("@PolicyNo", SqlDbType.Char, 25)
            params(5).Value = customClass.PolicyNo
            params(6) = New SqlParameter("@ExpiredDate", SqlDbType.DateTime)
            params(6).Value = customClass.ExpiredDate
            params(7) = New SqlParameter("@InsNotes", SqlDbType.Char, 500)
            params(7).Value = customClass.InsNotes
            params(8) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(8).Value = customClass.BusinessDate

            params(9) = New SqlParameter("@ApplicationType", SqlDbType.VarChar, 2)
            params(9).Value = customClass.ApplicationTypeDescr


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "SpNewCoverInsuredByCustomerSave  ", params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("NewCoverInsuranceByCust", "ProcessSaveInsuranceByCustomer", exp.Message + exp.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub
    'description : fungsi ini untuk menampilkan informasi cust tipe insured by cust
    Public Function GetInsuredByCustomer(ByVal customClass As Parameter.InsNewCover) As Parameter.InsNewCover

        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(0).Value = customClass.ApplicationID.Trim

        Try
            Dim oReturnValue As New Parameter.InsNewCover
            reader = SqlHelper.ExecuteReader(customClass.strConnection, CommandType.StoredProcedure, "spNewCoverInsuredByCustomerSelect", params)

            If reader.Read Then
                oReturnValue.BranchId = reader("BranchId").ToString.Trim
                oReturnValue.ApplicationID = reader("ApplicationID").ToString.Trim
                oReturnValue.CustomerName = reader("CustomerName").ToString.Trim
                oReturnValue.MinimumTenor = CType(reader("MinimumTenor").ToString, Int16)
                oReturnValue.MaximumTenor = CType(reader("MaximumTenor").ToString, Int16)
                oReturnValue.PBMaximumTenor = CType(reader("PBMaximumTenor").ToString, Int16)
                oReturnValue.PBMinimumTenor = CType(reader("PBMinimumTenor").ToString, Int16)
                oReturnValue.PMaximumTenor = CType(reader("PMaximumTenor").ToString, Int16)
                oReturnValue.PMinimumTenor = CType(reader("PMinimumTenor").ToString, Int16)
                oReturnValue.InsAssetInsuredByName = reader("InsuredByName").ToString
                oReturnValue.InsAssetPaidByName = reader("PaidByName").ToString
                'oReturnValue.InterestType = reader("InterestType").ToString
                'oReturnValue.InstallmentScheme = reader("InstallmentScheme").ToString
                oReturnValue.CustomerId = reader("CustomerID").ToString.Trim
                oReturnValue.MaturityDate = CType(reader("MaturityDate"), DateTime)
            End If
            reader.Close()
            Return oReturnValue

        Catch exp As Exception
            WriteException("NewCoverInsuranceByCust", "GetInsuredByCustomer", exp.Message + exp.StackTrace)
            Throw New Exception(exp.Message)
        End Try
    End Function
End Class
