

#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class InsClaimAct : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region "Activity"


    Public Function ClaimActList(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spClaimActList", params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("ClaimAct", "ClaimActList", exp.Message + exp.StackTrace)
        End Try

    End Function
    Public Function ClaimActDetail(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct
        Dim params() As SqlParameter = New SqlParameter(6) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@Businessdate", SqlDbType.DateTime)
        params(2).Value = customclass.BusinessDate

        params(3) = New SqlParameter("@InsSequenceNo", SqlDbType.Int)
        params(3).Value = customclass.InsSequenceNo

        params(4) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(4).Value = customclass.AssetSequenceNo

        params(5) = New SqlParameter("@ClaimType", SqlDbType.Char, 20)
        params(5).Value = customclass.ClaimType

        params(6) = New SqlParameter("@InsClaimSeqNo", SqlDbType.Int)
        params(6).Value = customclass.InsClaimSeqNo

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spClaimActDetail", params).Tables(0)
            Return customclass
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("InsClaim.aspx", "ClaimActDetail", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            'WriteException("ClaimAct", "ClaimActDetail", exp.Message + exp.StackTrace)

        End Try
    End Function

    Public Function ClaimActGetDoc(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct
        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@InsClaimSeqNo", SqlDbType.VarChar, 20)
        params(2).Value = customclass.InsClaimSeqNo

        params(3) = New SqlParameter("@InsSequenceNo", SqlDbType.Int)
        params(3).Value = customclass.InsSequenceNo

        params(4) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(4).Value = customclass.AssetSequenceNo

        params(5) = New SqlParameter("@ClaimType", SqlDbType.Char, 20)
        params(5).Value = customclass.ClaimType


        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGetClaimDoc", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("ClaimAct", "ClaimActGetDoc", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ClaimActsave(ByVal customclass As Parameter.InsClaimAct) As Boolean
        Dim intloop As Integer
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        If objcon.State = ConnectionState.Closed Then objcon.Open()
        objtrans = objcon.BeginTransaction
        Dim params() As SqlParameter = New SqlParameter(14) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@InsSequenceNo", SqlDbType.Int)
        params(2).Value = customclass.InsSequenceNo

        params(3) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(3).Value = customclass.AssetSequenceNo

        params(4) = New SqlParameter("@InsClaimSeqNo", SqlDbType.Int)
        params(4).Value = customclass.InsClaimSeqNo

        params(5) = New SqlParameter("@LetterFromKaditserseDate", SqlDbType.VarChar, 10)
        params(5).Value = customclass.LetterFromKaditserseDate

        params(6) = New SqlParameter("@Activity", SqlDbType.VarChar, 50)
        params(6).Value = customclass.Activity

        params(7) = New SqlParameter("@Result", SqlDbType.VarChar, 50)
        params(7).Value = customclass.Result

        params(8) = New SqlParameter("@EmployeeId", SqlDbType.VarChar, 10)
        params(8).Value = customclass.LoginId

        params(9) = New SqlParameter("@Notes", SqlDbType.VarChar, 50)
        params(9).Value = customclass.Notes

        params(10) = New SqlParameter("@OKInscoDate", SqlDbType.DateTime)
        params(10).Value = CDate(customclass.OKInscoDate)

        params(11) = New SqlParameter("@OKCustDate", SqlDbType.DateTime)
        params(11).Value = CDate(customclass.OKCustDate)

        params(12) = New SqlParameter("@ClaimAmount", SqlDbType.VarChar, 20)
        params(12).Value = customclass.ClaimAmount

        params(13) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(13).Value = customclass.BusinessDate

        params(14) = New SqlParameter("@saveStatus", SqlDbType.Bit)
        params(14).Direction = ParameterDirection.Output


        Try

            If customclass.ListData.Rows.Count > 0 Then
                Dim param() As SqlParameter = New SqlParameter(9) {}
                For intloop = 0 To customclass.ListData.Rows.Count - 1

                    param(0) = New SqlParameter("@ReceivedFrom", SqlDbType.VarChar, 1000)
                    param(0).Value = customclass.ListData.Rows(intloop).Item("ReceivedFrom")

                    param(1) = New SqlParameter("@ReceivedocDate", SqlDbType.DateTime)
                    If CType(customclass.ListData.Rows(intloop).Item("ReceivedDate"), Date) = CDate("01/01/1900") Then
                        param(1).Value = DBNull.Value
                    Else
                        param(1).Value = customclass.ListData.Rows(intloop).Item("ReceivedDate")
                    End If

                    param(2) = New SqlParameter("@OriginalDoc", SqlDbType.VarChar, 1)
                    param(2).Value = customclass.ListData.Rows(intloop).Item("Original")

                    param(3) = New SqlParameter("@CopyDoc", SqlDbType.VarChar, 1)
                    param(3).Value = customclass.ListData.Rows(intloop).Item("Copy")

                    param(4) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                    param(4).Value = customclass.BranchId

                    param(5) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
                    param(5).Value = customclass.ApplicationID

                    param(6) = New SqlParameter("@InsSequenceNo", SqlDbType.Int)
                    param(6).Value = customclass.InsSequenceNo

                    param(7) = New SqlParameter("@InsClaimSeqNo", SqlDbType.Int)
                    param(7).Value = customclass.InsClaimSeqNo

                    param(8) = New SqlParameter("@InsDocId", SqlDbType.Int)
                    param(8).Value = customclass.ListData.Rows(intloop).Item("InsDocID")

                    param(9) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
                    param(9).Value = customclass.AssetSequenceNo

                    SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spInsClaimActSaveDetail", param)
                Next

            End If
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spInsClaimActSaveHeader", params)
            objtrans.Commit()
            Return CType(params(14).Value, Boolean)
        Catch exp As Exception
            objtrans.Rollback()
            Throw New Exception(exp.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try

    End Function

    Public Function ClaimRejectsave(ByVal customclass As Parameter.InsClaimAct) As Boolean


        Dim params() As SqlParameter = New SqlParameter(8) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@InsSequenceNo", SqlDbType.Int)
        params(2).Value = customclass.InsSequenceNo

        params(3) = New SqlParameter("@InsClaimSeqNo", SqlDbType.Int)
        params(3).Value = customclass.InsClaimSeqNo


        params(4) = New SqlParameter("@Notes", SqlDbType.VarChar, 50)
        params(4).Value = customclass.Notes

        params(5) = New SqlParameter("@RejectReasonID", SqlDbType.Char, 10)
        params(5).Value = customclass.ReasonID

        params(6) = New SqlParameter("@RejectDate", SqlDbType.VarChar, 10)
        params(6).Value = customclass.ActivityDate


        params(7) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(7).Value = customclass.AssetSequenceNo

        params(8) = New SqlParameter("@saveStatus", SqlDbType.Bit)
        params(8).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spClaimRejectSave", params)
            Return CType(params(8).Value, Boolean)
        Catch exp As Exception
            WriteException("ClaimAct", "ClaimActsave", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ClaimAcceptsave(ByVal customclass As Parameter.InsClaimAct) As Boolean


        Dim params() As SqlParameter = New SqlParameter(7) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@InsSequenceNo", SqlDbType.Int)
        params(2).Value = customclass.InsSequenceNo

        params(3) = New SqlParameter("@InsClaimSeqNo", SqlDbType.Int)
        params(3).Value = customclass.InsClaimSeqNo

        params(4) = New SqlParameter("@Notes", SqlDbType.VarChar, 50)
        params(4).Value = customclass.Notes

        params(5) = New SqlParameter("@AcceptDate", SqlDbType.Date)
        params(5).Value = customclass.ActivityDate

        params(6) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(6).Value = customclass.AssetSequenceNo

        params(7) = New SqlParameter("@saveStatus", SqlDbType.Bit)
        params(7).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spClaimAcceptSave", params)
            Return CType(params(7).Value, Boolean)
        Catch exp As Exception
            WriteException("ClaimAct", "ClaimActsave", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "setting"
    Public Function GetInsRateCategory(ByVal customclass As Parameter.InsClaimAct) As DataSet

        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGetInsRateCategory")

        Catch exp As Exception
            WriteException("ClaimAct", "GetAssetType", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ClaimDocReport(ByVal customclass As Parameter.InsClaimAct) As DataSet
        Dim params As SqlParameter
        params = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params.Value = customclass.WhereCond

        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spClaimDocumentReport", params)

        Catch exp As Exception
            WriteException("ClaimAct", "ClaimDocReport", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function SaveClaimDoc(ByVal customclass As Parameter.InsClaimAct) As Boolean


        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@InsuranceType", SqlDbType.Char, 2)
        params(1).Value = customclass.InsAssetType

        params(2) = New SqlParameter("@ClaimType", SqlDbType.Char, 10)
        params(2).Value = customclass.ClaimType

        params(3) = New SqlParameter("@InsDocID", SqlDbType.Char, 10)
        params(3).Value = customclass.InsDocID

        params(4) = New SqlParameter("@saveStatus", SqlDbType.Bit)
        params(4).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spInsDocClaimSave", params)
            Return CType(params(4).Value, Boolean)
        Catch exp As Exception
            WriteException("ClaimAct", "SaveClaimDoc", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function DocclaimList(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGetClaimDocument", params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("ClaimAct", "DocclaimList", exp.Message + exp.StackTrace)
        End Try

    End Function


    Public Function GetDocMaster(ByVal customclass As Parameter.InsClaimAct) As DataSet

        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGetDocMaster")

        Catch exp As Exception
            WriteException("ClaimAct", "GetDocMaster", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function DeleteClaimDoc(ByVal customclass As Parameter.InsClaimAct) As Boolean


        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond


        params(1) = New SqlParameter("@saveStatus", SqlDbType.Bit)
        params(1).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spDeleteInsDoc", params)
            Return CType(params(1).Value, Boolean)
        Catch exp As Exception
            WriteException("ClaimAct", "SaveClaimDoc", exp.Message + exp.StackTrace)
        End Try
    End Function

#End Region

#Region "InsuranceBilling"
    Public Function InsuranceBillingReport(ByVal customclass As Parameter.InsClaimAct) As DataSet
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@InvoiceNo", SqlDbType.Char, 20)
        params(1).Value = customclass.InvoiceNo
        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPrintBillingReport", params)

        Catch exp As Exception
            WriteException("InsuranceBilling", "InsuranceBillingReport", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "ClaimInquery"
    Public Function GetInsCo(ByVal customclass As Parameter.InsClaimAct) As DataSet
        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGetInsCo")

        Catch exp As Exception
            WriteException("ClaimInquery", "GetInsCo", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ClaimInquiryList(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spClaimInquiryList", params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            'WriteException("ClaimInquery", "ClaimInquiryList", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetClaimInqDet(ByVal customclass As Parameter.InsClaimAct) As DataSet
        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@ClaimStatus", SqlDbType.Char, 1)
        params(2).Value = customclass.ClaimStatus

        params(3) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(3).Value = customclass.AssetSequenceNo

        params(4) = New SqlParameter("@InsSequenceNo", SqlDbType.Int)
        params(4).Value = customclass.InsSequenceNo

        params(5) = New SqlParameter("@InsClaimSeqNo", SqlDbType.Int)
        params(5).Value = customclass.ClaimSeqNo
        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spClaimInqDet", params)
        Catch exp As Exception
            WriteException("ClaimInquery", "GetClaimInqDet", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetClaimActList(ByVal customclass As Parameter.InsClaimAct) As DataSet
        Dim params() As SqlParameter = New SqlParameter(5) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@ClaimStatus", SqlDbType.Char, 1)
        params(2).Value = customclass.ClaimStatus

        params(3) = New SqlParameter("@InsClaimSeqNo", SqlDbType.Int)
        params(3).Value = customclass.InsClaimSeqNo

        params(4) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(4).Value = customclass.AssetSequenceNo

        params(5) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(5).Value = customclass.InsSequenceNo

        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spInsInqActList", params)
        Catch exp As Exception
            WriteException("ClaimInquery", "GetClaimActList", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function GetClaimActHistList(ByVal customclass As Parameter.InsClaimAct) As DataSet
        Dim params() As SqlParameter = New SqlParameter(6) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@ClaimStatus", SqlDbType.Char, 1)
        params(2).Value = customclass.ClaimStatus

        params(3) = New SqlParameter("@InsClaimSeqNo", SqlDbType.Int)
        params(3).Value = customclass.InsClaimSeqNo

        params(4) = New SqlParameter("@ActSeqNo", SqlDbType.Int)
        params(4).Value = customclass.ActSeqNo

        params(5) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(5).Value = customclass.AssetSequenceNo

        params(6) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(6).Value = customclass.InsSequenceNo

        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spInsInqActHistList", params)
        Catch exp As Exception
            WriteException("ClaimInquery", "GetClaimActHistList", exp.Message + exp.StackTrace)
        End Try

    End Function
    Public Function ViewPolicy(ByVal customclass As Parameter.InsClaimAct) As DataSet
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@ClaimStatus", SqlDbType.Char, 1)
        params(2).Value = customclass.ClaimStatus

        params(3) = New SqlParameter("@InsClaimSeqNo", SqlDbType.Int)
        params(3).Value = customclass.InsClaimSeqNo

        Try
            Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spViewPolicy", params)
        Catch exp As Exception
            WriteException("ClaimInquery", "ViewPolicy", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

End Class
