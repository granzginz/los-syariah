
#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region

Public Class AdvClaimReq : Inherits maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    Private Const GET_ADVANCE_CLAIM_COST_REQUEST As String = "spAdvanceClaimCostRequestPaging"
    Private Const ADVANCE_CLAIM_COST_REQUEST_VIEW As String = "spAdvanceClaimCostRequestView"
    Private Const ADVANCE_CLAIM_COST_REQUEST_VIEWGRID As String = "spAdvanceClaimCostRequestViewGrid"
    Private Const ADVANCE_CLAIM_COST_REQUEST_SAVE As String = "spAdvanceClaimCostRequestSaveAdd"
    Private Const ADVANCE_CLAIM_COST_REQUEST_VIEW_APPROVAL As String = "spAdvanceClaimCostRequestViewApproval"
#End Region

#Region "AdvClaimReqPaging"
    Public Function AdvClaimReqPaging(ByVal customClass As Parameter.AdvClaimReq) As Parameter.AdvClaimReq
        Dim oReturnValue As New Parameter.AdvClaimReq

        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
        params(3).Value = customClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.listData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, GET_ADVANCE_CLAIM_COST_REQUEST, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Integer)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Insurance.Claim.AdvanceClaimCost.AdvClaimReqPaging")
        End Try

    End Function
#End Region
#Region "AdvanceClaimCostRequestView"
    Public Function AdvanceClaimCostRequestView(ByVal customclass As Parameter.AdvClaimReq) As Parameter.AdvClaimReq
        Dim objread As SqlDataReader
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20)
            params(1).Value = customclass.ApplicationId.Trim

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, ADVANCE_CLAIM_COST_REQUEST_VIEW, params)

            With customclass
                If objread.Read Then
                    .BranchId = CType(objread("BranchId"), String)
                    .ApplicationId = CType(objread("ApplicationId"), String)
                    .AgreementNo = CType(objread("AgreementNo"), String)
                    .CustomerName = CType(objread("Name"), String)
                    .AssetDescription = CType(objread("AssetDescription"), String)
                    .InsuranceComBranchId = CType(objread("InsuranceCompany"), String)
                    .PolicyNumber = CType(objread("PolicyNumber"), String)
                    .MainCoverage = CType(objread("MainCoverage"), String)
                    .CustomerId = CType(objread("CustomerId"), String)
                    '.Status = CType(objread("Status"), String)
                    .AssetSeqNo = CType(objread("AssetSeqNo"), Integer)
                    .InsSequnceNo = CType(objread("InsSequenceNo"), Integer)
                    .InsClaimSeqNo = CType(objread("InsClaimSeqNo"), Integer)
                    .CanRequest = CBool(objread("CanRequest"))

                End If
                objread.Close()
            End With
            Return customclass
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("AdvClaimReq.vb", "AdvanceClaimCostRequestView", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region
#Region "AdvanceClaimCostRequestViewGrid"
    Public Function AdvanceClaimCostRequestViewGrid(ByVal customClass As Parameter.AdvClaimReq) As Parameter.AdvClaimReq
        Dim oReturnValue As New Parameter.AdvClaimReq

        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
        params(0).Value = customClass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customClass.ApplicationId.Trim
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt)
        params(2).Value = customClass.AssetSeqNo
        params(3) = New SqlParameter("@InsSequenceNo", SqlDbType.SmallInt)
        params(3).Value = customClass.InsSequnceNo
        params(4) = New SqlParameter("@InsClaimSeqNo", SqlDbType.SmallInt)
        params(4).Value = customClass.InsClaimSeqNo
        Try
            oReturnValue.ViewGrid = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, ADVANCE_CLAIM_COST_REQUEST_VIEWGRID, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
            'Throw New Exception("Error On DataAccess.Insurance.Claim.AdvanceClaimCost.AdvanceClaimCostRequestViewGrid")
        End Try
    End Function

#End Region
#Region "AdvanceClaimCostRequestSave"

    Public Sub AdvanceClaimCostRequestSave(ByVal customClass As Parameter.AdvClaimReq)
        Dim oApprovalID As New Maxiloan.SQLEngine.Workflow.Approval
        Dim oEntitiesApproval As New Parameter.Approval
        Dim strApprovalNo As String
        Dim objconnection As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction
        Dim params(12) As SqlParameter
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objtrans = objconnection.BeginTransaction

            With oEntitiesApproval
                .ApprovalTransaction = objtrans
                .BranchId = customClass.BranchId
                .SchemeID = "CADV"
                .RequestDate = customClass.BusinessDate
                .TransactionNo = customClass.ApplicationId
                .ApprovalValue = customClass.ClaimCost
                .UserRequest = customClass.LoginId
                .UserApproval = customClass.ApproveBy
                .ApprovalNote = customClass.Notes
                .AprovalType = Parameter.Approval.ETransactionType.AdvanceClaimCost_Approval
            End With
            strApprovalNo = oApprovalID.RequestForApproval(oEntitiesApproval)


            params(0) = New SqlParameter(PARAM_BRANCHID, SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = customClass.ApplicationId
            params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt)
            params(2).Value = customClass.AssetSeqNo
            params(3) = New SqlParameter("@InsSequenceNo", SqlDbType.SmallInt)
            params(3).Value = customClass.InsSequnceNo
            params(4) = New SqlParameter("@InsClaimSeqNo", SqlDbType.SmallInt)
            params(4).Value = customClass.InsClaimSeqNo
            params(5) = New SqlParameter("@RequestDate", SqlDbType.DateTime)
            params(5).Value = customClass.RequestDate
            params(6) = New SqlParameter("@ClaimCost", SqlDbType.Decimal)
            params(6).Value = customClass.ClaimCost
            params(7) = New SqlParameter("@Notes ", SqlDbType.VarChar, 50)
            params(7).Value = customClass.Notes
            params(8) = New SqlParameter("@Status", SqlDbType.Char, 3)
            params(8).Value = customClass.Status
            params(9) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
            params(9).Value = strApprovalNo

            params(10) = New SqlParameter("@ApproveBy", SqlDbType.Char, 20)
            params(10).Value = customClass.ApproveBy
            params(11) = New SqlParameter("@RequestBy", SqlDbType.VarChar, 12)
            params(11).Value = customClass.LoginId

            params(12) = New SqlParameter("@BankAccountId", SqlDbType.Char, 10)
            params(12).Value = customClass.BankAccountId



            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, ADVANCE_CLAIM_COST_REQUEST_SAVE, params)
            objtrans.Commit()
        Catch ex As Exception
            objtrans.Rollback()
            Throw New Exception(ex.Message)
            'Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.PremiumToCustomerRateCustSaveEdit")
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try

    End Sub
#End Region
#Region "AdvanceClaimCostRequestViewApproval"
    Public Function AdvanceClaimCostRequestViewApproval(ByVal customclass As Parameter.AdvClaimReq) As Parameter.AdvClaimReq
        Dim objread As SqlDataReader
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@ApprovalNo", SqlDbType.VarChar, 50)
            params(0).Value = customclass.ApprovalNo
            params(1) = New SqlParameter("@ApplicationId", SqlDbType.Char, 20)
            params(1).Value = customclass.ApplicationId

            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, ADVANCE_CLAIM_COST_REQUEST_VIEW_APPROVAL, params)

            With customclass
                If objread.Read Then
                    .AgreementNo = CType(objread("AgreementNo"), String)
                    .CustomerName = CType(objread("Name"), String)
                    .AssetDescription = CType(objread("AssetDescription"), String)
                    .InsuranceComBranchId = CType(objread("InsuranceCompany"), String)
                    .CustomerId = CType(objread("CustomerId"), String)
                    .PolicyNumber = CType(objread("PolicyNumber"), String)
                    .MainCoverage = CType(objread("MainCoverage"), String)
                    .ClaimCost = CType(objread("ClaimCost"), Double)
                    .RequestDate = CType(objread("RequestDate"), Date)
                    .Notes = CType(objread("Notes"), String)
                    .ApplicationId = CType(objread("ApplicationId"), String)
                End If
                objread.Close()
            End With
            Return customclass
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("AdvClaimReq.vb", "AdvanceClaimCostRequestViewApproval", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try
    End Function
#End Region




End Class
