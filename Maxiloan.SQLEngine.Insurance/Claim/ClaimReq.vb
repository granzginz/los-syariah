#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class ClaimReq : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region "ClaimreqList"
    Public Function ClaimrequestList(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest
        Dim data As New Parameter.ClaimRequest
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spClaimRequest", params).Tables(0)
            data.TotalRecord = CInt(params(4).Value)
            Return data
        Catch exp As Exception
            WriteException("ClaimReq", "ClaimrequestList", exp.Message + exp.StackTrace)
        End Try

    End Function
    Public Function ClaimrequestListApproval(ByVal customclass As Parameter.InsClaimAct) As Parameter.InsClaimAct
        Dim data As New Parameter.InsClaimAct
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spClaimRequestApproval", params).Tables(0)
            data.TotalRecord = CInt(params(4).Value)
            Return data
        Catch exp As Exception
            WriteException("ClaimReq", "ClaimrequestListApproval", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region
    Public Function ClaimrequesDetail(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest
        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(2).Value = customclass.AssetSequenceNo

        params(3) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(3).Value = customclass.InsSequenceNo

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spClaimRequestProcessForm", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("ClaimRequest", "ClaimrequesDetail", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ClaimrequestCoverage(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@YearNum", SqlDbType.Int)
        params(2).Value = customclass.YearNum

        params(3) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(3).Value = customclass.AssetSequenceNo

        params(4) = New SqlParameter("@InsSeqNo", SqlDbType.Int)
        params(4).Value = customclass.InsSequenceNo


        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spClaimReqCoverage", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("ClaimRequest", "ClaimrequestCoverage", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ClaimrequestSave(ByVal customclass As Parameter.ClaimRequest) As Boolean
        Dim params() As SqlParameter = New SqlParameter(24) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@ClaimType", SqlDbType.Char, 10)
        params(2).Value = customclass.ClaimType

        params(3) = New SqlParameter("@AgreementNo", SqlDbType.Char, 20)
        params(3).Value = customclass.AgreementNo

        params(4) = New SqlParameter("@CustomerName", SqlDbType.VarChar, 50)
        params(4).Value = customclass.CustomerName

        params(5) = New SqlParameter("@DefectPart", SqlDbType.VarChar, 200)
        params(5).Value = customclass.DefectPart

        params(6) = New SqlParameter("@BengkelName", SqlDbType.VarChar, 50)
        params(6).Value = customclass.BengkelName

        params(7) = New SqlParameter("@BengkelAddress", SqlDbType.VarChar, 100)
        params(7).Value = customclass.BengkelAddress

        params(8) = New SqlParameter("@BengkelPhone", SqlDbType.VarChar, 20)
        params(8).Value = customclass.BengkelPhone

        params(9) = New SqlParameter("@BengkelPIC", SqlDbType.VarChar, 50)
        params(9).Value = customclass.BengkelPIC

        params(10) = New SqlParameter("@ClaimDate", SqlDbType.VarChar, 35)
        params(10).Value = customclass.ClaimDate

        params(11) = New SqlParameter("@EventDate", SqlDbType.VarChar, 35)
        params(11).Value = customclass.EventDate

        params(12) = New SqlParameter("@ReportDate", SqlDbType.VarChar, 35)
        params(12).Value = customclass.ReportDate

        params(13) = New SqlParameter("@ReportedBy", SqlDbType.VarChar, 50)
        params(13).Value = customclass.ReportedBy

        params(14) = New SqlParameter("@ReportedAs", SqlDbType.VarChar, 50)
        params(14).Value = customclass.ReportedAs

        params(15) = New SqlParameter("@EventLocation", SqlDbType.VarChar, 50)
        params(15).Value = customclass.EventLocation

        params(16) = New SqlParameter("@ClaimAmount", SqlDbType.Decimal)
        params(16).Value = customclass.ClaimAmount

        params(17) = New SqlParameter("@DerekDate", SqlDbType.VarChar, 35)
        params(17).Value = customclass.DerekDate

        params(18) = New SqlParameter("@SurveyDate", SqlDbType.VarChar, 35)
        params(18).Value = customclass.SurveyDate

        params(19) = New SqlParameter("@Proses", SqlDbType.VarChar, 35)
        params(19).Value = customclass.ProsesDate

        params(20) = New SqlParameter("@InsSequenceNo", SqlDbType.Int)
        params(20).Value = customclass.InsSequenceNo

        params(21) = New SqlParameter("@InsCoClaimNo", SqlDbType.VarChar, 50)
        params(21).Value = customclass.InsClaimNo

        params(22) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(22).Value = customclass.AssetSequenceNo

        params(23) = New SqlParameter("@Cases", SqlDbType.VarChar, 50)
        params(23).Value = customclass.Cases

        params(24) = New SqlParameter("@saveStatus", SqlDbType.Bit)
        params(24).Direction = ParameterDirection.Output


        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spAddInsuranceClaim", params)
            Return CType(params(24).Value, Boolean)
        Catch exp As Exception
            WriteException("ClaimRequest", "ClaimrequestSave", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ClaimrequestSaveApprove(ByVal customclass As Parameter.InsClaimAct) As Boolean
        Dim params() As SqlParameter = New SqlParameter(10) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@InsSequenceNo", SqlDbType.Int)
        params(2).Value = customclass.InsSequenceNo

        params(3) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(3).Value = customclass.AssetSequenceNo

        params(4) = New SqlParameter("@InsClaimSeqNo", SqlDbType.Int)
        params(4).Value = customclass.InsClaimSeqNo

        params(5) = New SqlParameter("@ApprovalStatus", SqlDbType.Char, 5)
        params(5).Value = customclass.ApprovalStatus

        params(6) = New SqlParameter("@LoginId", SqlDbType.Char, 20)
        params(6).Value = customclass.LoginId

        params(7) = New SqlParameter("@ApprovalNotes", SqlDbType.Char, 100)
        params(7).Value = customclass.ApprovalNotes

        params(8) = New SqlParameter("@ClaimType", SqlDbType.Char, 20)
        params(8).Value = customclass.ClaimType

        params(9) = New SqlParameter("@saveStatus", SqlDbType.Bit)
        params(9).Direction = ParameterDirection.Output

        params(10) = New SqlParameter("@ClaimAmountByCust", SqlDbType.Decimal)
        params(10).Value = customclass.ClaimAmount

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spApproveRejectInsuranceClaim", params)
            Return CType(params(9).Value, Boolean)
        Catch exp As Exception
            WriteException("ClaimRequest", "ClaimrequestSaveApprove", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ClaimrequestGetClaimType(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spGetClaimType").Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("ClaimRequest", "ClaimrequestGetClaimType", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ClaimrequestPrint(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spPrintClaimForm", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("ClaimRequest", "ClaimrequestCoverage", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ViewInsuraceClaimInq(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID
        params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(2).Value = customclass.AssetSequenceNo
        params(3) = New SqlParameter("@InsSequenceNo", SqlDbType.Int)
        params(3).Value = customclass.InsSequenceNo
        params(4) = New SqlParameter("@InsClaimSeqNo", SqlDbType.Int)
        params(4).Value = customclass.InsClaimSeq

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spViewInsuranceClaimInq", params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("ClaimRequest", "ViewInsuraceClaimInq", exp.Message + exp.StackTrace)
        End Try
    End Function
    Public Function ClaimrequestEdit(ByVal customclass As Parameter.ClaimRequest) As Parameter.ClaimRequest
        Dim params() As SqlParameter = New SqlParameter(22) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId

        params(1) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
        params(1).Value = customclass.ApplicationID

        params(2) = New SqlParameter("@ClaimType", SqlDbType.Char, 10)
        params(2).Value = customclass.ClaimType

        params(3) = New SqlParameter("@InsClaimSeqNo", SqlDbType.Int)
        params(3).Value = customclass.InsClaimSeq

        params(4) = New SqlParameter("@DefectPart", SqlDbType.VarChar, 200)
        params(4).Value = customclass.DefectPart

        params(5) = New SqlParameter("@BengkelName", SqlDbType.VarChar, 50)
        params(5).Value = customclass.BengkelName

        params(6) = New SqlParameter("@BengkelAddress", SqlDbType.VarChar, 100)
        params(6).Value = customclass.BengkelAddress

        params(7) = New SqlParameter("@BengkelPhone", SqlDbType.VarChar, 20)
        params(7).Value = customclass.BengkelPhone

        params(8) = New SqlParameter("@BengkelPIC", SqlDbType.VarChar, 50)
        params(8).Value = customclass.BengkelPIC

        params(9) = New SqlParameter("@ClaimDate", SqlDbType.VarChar, 35)
        params(9).Value = customclass.ClaimDate

        params(10) = New SqlParameter("@EventDate", SqlDbType.VarChar, 35)
        params(10).Value = customclass.EventDate

        params(11) = New SqlParameter("@ReportDate", SqlDbType.VarChar, 35)
        params(11).Value = customclass.ReportDate

        params(12) = New SqlParameter("@ReportedBy", SqlDbType.VarChar, 50)
        params(12).Value = customclass.ReportedBy

        params(13) = New SqlParameter("@ReportedAs", SqlDbType.VarChar, 50)
        params(13).Value = customclass.ReportedAs

        params(14) = New SqlParameter("@EventLocation", SqlDbType.VarChar, 50)
        params(14).Value = customclass.EventLocation

        params(15) = New SqlParameter("@ClaimAmount", SqlDbType.Decimal)
        params(15).Value = customclass.ClaimAmount

        params(16) = New SqlParameter("@DerekDate", SqlDbType.VarChar, 35)
        params(16).Value = customclass.DerekDate

        params(17) = New SqlParameter("@SurveyDate", SqlDbType.VarChar, 35)
        params(17).Value = customclass.SurveyDate

        params(18) = New SqlParameter("@Proses", SqlDbType.VarChar, 35)
        params(18).Value = customclass.ProsesDate

        params(19) = New SqlParameter("@InsSequenceNo", SqlDbType.Int)
        params(19).Value = customclass.InsSequenceNo

        params(20) = New SqlParameter("@InsCoClaimNo", SqlDbType.VarChar, 50)
        params(20).Value = customclass.InsClaimNo

        params(21) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
        params(21).Value = customclass.AssetSequenceNo

        params(22) = New SqlParameter("@Cases", SqlDbType.VarChar, 50)
        params(22).Value = customclass.Cases

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, "spEditInsuranceClaim", params)
            Return customclass
        Catch exp As Exception
            WriteException("ClaimRequest", "ClaimrequestEdit", exp.Message + exp.StackTrace)
        End Try
    End Function
End Class
