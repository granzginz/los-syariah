
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class Agency : Inherits DataAccessBase
#Region "Constanta"
    Private Const STNK_AGENCY_PAGING As String = "spSTNKAgencyPaging"
    Private Const STNK_AGENCY_SAVE As String = "spSTNKAgencySave"
    Private Const STNK_AGENCY_UPDATE As String = "spSTNKAgencyUpdate"
    Private Const STNK_AGENCY_DELETE As String = "spSTNKAgentDelete"
    Private Const STNK_AGENCY_VIEW As String = "spSTNKAgencyView"
    Private Const STNK_AGENCY_REPORT As String = "spSTNKAgencyReport"

#End Region
#Region "STNKAgencyPaging"

    Public Function STNKAgencyPaging(ByVal customclass As Parameter.Agency) As Parameter.Agency
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, STNK_AGENCY_PAGING, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)

            Return customclass
        Catch exp As Exception
            WriteException("Agency", "STNKAgencyPaging", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region "STNKAgencyView"

    Public Function STNKAgencyView(ByVal strConnection As String, _
                                    ByVal BranchId As String, _
                                    ByVal AgentId As String) As DataTable
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@AgentId", SqlDbType.Char, 20)
            params(0).Value = AgentId

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = BranchId

            Return SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, STNK_AGENCY_VIEW, params).Tables(0)
        Catch exp As Exception
            WriteException("Agency", "STNKAgencyView", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region
#Region "STNKAgency Reports"
    Public Function STNKAgencyReport(ByVal oCustomClass As Parameter.Agency) As Parameter.Agency
        Dim params(1) As SqlParameter
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = oCustomClass.WhereCond
            params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(1).Value = oCustomClass.SortBy
            oCustomClass.ListReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, STNK_AGENCY_REPORT, params)
            Return oCustomClass

        Catch exp As Exception
            WriteException("Agency", "STNKAgencyReport", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region
#Region "STNKAgencySave"

    Public Sub STNKAgencySave(ByVal oAgency As Parameter.Agency, _
                                ByVal oBankAccount As Parameter.BankAccount, _
                                ByVal oAddress As Parameter.Address, _
                                ByVal oPersonal As Parameter.Personal)
        Dim objConnection As New SqlConnection(oAgency.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            Dim params(25) As SqlParameter

            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = oAgency.BranchId
            params(1) = New SqlParameter("@AgentId", SqlDbType.Char, 20)
            params(1).Value = oAgency.AgentId
            params(2) = New SqlParameter("@AgentName", SqlDbType.VarChar, 50)
            params(2).Value = oAgency.AgentName
            params(3) = New SqlParameter("@AgentAddress", SqlDbType.VarChar, 100)
            params(3).Value = oAddress.Address
            params(4) = New SqlParameter("@rt", SqlDbType.Char, 3)
            params(4).Value = oAddress.RT
            params(5) = New SqlParameter("@rw", SqlDbType.Char, 3)
            params(5).Value = oAddress.RW
            params(6) = New SqlParameter("@kelurahan", SqlDbType.VarChar, 30)
            params(6).Value = oAddress.Kelurahan
            params(7) = New SqlParameter("@kecamatan", SqlDbType.VarChar, 30)
            params(7).Value = oAddress.Kecamatan
            params(8) = New SqlParameter("@city", SqlDbType.VarChar, 30)
            params(8).Value = oAddress.City
            params(9) = New SqlParameter("@zipcode", SqlDbType.VarChar, 5)
            params(9).Value = oAddress.ZipCode
            params(10) = New SqlParameter("@areaphone1", SqlDbType.VarChar, 4)
            params(10).Value = oAddress.AreaPhone1
            params(11) = New SqlParameter("@phone1", SqlDbType.VarChar, 10)
            params(11).Value = oAddress.Phone1
            params(12) = New SqlParameter("@areaphone2", SqlDbType.VarChar, 4)
            params(12).Value = oAddress.AreaPhone2
            params(13) = New SqlParameter("@phone2", SqlDbType.VarChar, 10)
            params(13).Value = oAddress.Phone2
            params(14) = New SqlParameter("@areafax", SqlDbType.VarChar, 4)
            params(14).Value = oAddress.AreaFax
            params(15) = New SqlParameter("@fax", SqlDbType.VarChar, 10)
            params(15).Value = oAddress.Fax
            params(16) = New SqlParameter("@email", SqlDbType.VarChar, 30)
            params(16).Value = oPersonal.Email
            params(17) = New SqlParameter("@mobilephone", SqlDbType.VarChar, 20)
            params(17).Value = oPersonal.MobilePhone
            params(18) = New SqlParameter("@contactpersonname", SqlDbType.VarChar, 50)
            params(18).Value = oPersonal.PersonName
            params(19) = New SqlParameter("@contactpersontitle", SqlDbType.VarChar, 50)
            params(19).Value = oPersonal.PersonTitle
            params(20) = New SqlParameter("@BankId", SqlDbType.Char, 5)
            params(20).Value = oBankAccount.BankID
            params(21) = New SqlParameter("@BankBranch", SqlDbType.VarChar, 50)
            params(21).Value = oBankAccount.BankBranch
          
            params(22) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
            params(22).Value = oBankAccount.AccountName
            params(23) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 25)
            params(23).Value = oBankAccount.AccountNo
            params(24) = New SqlParameter("@IsActive", SqlDbType.Bit)
            params(24).Value = oAgency.IsActive
            params(25) = New SqlParameter("@bankbranchid", SqlDbType.Int)
            params(25).Value = CInt(IIf(oBankAccount.BankBranchId.Trim = "", "0", oBankAccount.BankBranchId.Trim))

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, STNK_AGENCY_SAVE, params)
            objtrans.Commit()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            WriteException("Agent", "STNKAgencySave", exp.Message + exp.StackTrace)
        End Try

    End Sub
#End Region
#Region "STNKAgencyUpdate"
    Public Sub STNKAgencyUpdate(ByVal oAgency As Parameter.Agency, _
                                   ByVal oBankAccount As Parameter.BankAccount, _
                                   ByVal oAddress As Parameter.Address, _
                                   ByVal oPersonal As Parameter.Personal)
        Dim objConnection As New SqlConnection(oAgency.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            Dim params(25) As SqlParameter

            params(0) = New SqlParameter("@BranchId", SqlDbType.Char, 3)
            params(0).Value = oAgency.BranchId
            params(1) = New SqlParameter("@AgentId", SqlDbType.Char, 20)
            params(1).Value = oAgency.AgentId
            params(2) = New SqlParameter("@AgentName", SqlDbType.VarChar, 50)
            params(2).Value = oAgency.AgentName
            params(3) = New SqlParameter("@AgentAddress", SqlDbType.VarChar, 100)
            params(3).Value = oAddress.Address
            params(4) = New SqlParameter("@rt", SqlDbType.Char, 3)
            params(4).Value = oAddress.RT
            params(5) = New SqlParameter("@rw", SqlDbType.Char, 3)
            params(5).Value = oAddress.RW
            params(6) = New SqlParameter("@kelurahan", SqlDbType.VarChar, 30)
            params(6).Value = oAddress.Kelurahan
            params(7) = New SqlParameter("@kecamatan", SqlDbType.VarChar, 30)
            params(7).Value = oAddress.Kecamatan
            params(8) = New SqlParameter("@city", SqlDbType.VarChar, 30)
            params(8).Value = oAddress.City
            params(9) = New SqlParameter("@zipcode", SqlDbType.VarChar, 5)
            params(9).Value = oAddress.ZipCode
            params(10) = New SqlParameter("@areaphone1", SqlDbType.VarChar, 4)
            params(10).Value = oAddress.AreaPhone1
            params(11) = New SqlParameter("@phone1", SqlDbType.VarChar, 10)
            params(11).Value = oAddress.Phone1
            params(12) = New SqlParameter("@areaphone2", SqlDbType.VarChar, 4)
            params(12).Value = oAddress.AreaPhone2
            params(13) = New SqlParameter("@phone2", SqlDbType.VarChar, 10)
            params(13).Value = oAddress.Phone2
            params(14) = New SqlParameter("@areafax", SqlDbType.VarChar, 4)
            params(14).Value = oAddress.AreaFax
            params(15) = New SqlParameter("@fax", SqlDbType.VarChar, 10)
            params(15).Value = oAddress.Fax
            params(16) = New SqlParameter("@email", SqlDbType.VarChar, 30)
            params(16).Value = oPersonal.Email
            params(17) = New SqlParameter("@mobilephone", SqlDbType.VarChar, 20)
            params(17).Value = oPersonal.MobilePhone
            params(18) = New SqlParameter("@contactpersonname", SqlDbType.VarChar, 50)
            params(18).Value = oPersonal.PersonName
            params(19) = New SqlParameter("@contactpersontitle", SqlDbType.VarChar, 50)
            params(19).Value = oPersonal.PersonTitle
            params(20) = New SqlParameter("@BankId", SqlDbType.Char, 5)
            params(20).Value = oBankAccount.BankID
            params(21) = New SqlParameter("@BankBranch", SqlDbType.VarChar, 50)
            params(21).Value = oBankAccount.BankBranch
            params(22) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
            params(22).Value = oBankAccount.AccountName
            params(23) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 25)
            params(23).Value = oBankAccount.AccountNo
            params(24) = New SqlParameter("@IsActive", SqlDbType.Bit)
            params(24).Value = oAgency.IsActive
            params(25) = New SqlParameter("@bankbranchid", SqlDbType.Int)
            params(25).Value = CInt(IIf(oBankAccount.BankBranchId.Trim = "", "0", oBankAccount.BankBranchId.Trim))

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, STNK_AGENCY_UPDATE, params)
            objtrans.Commit()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            WriteException("Agent", "STNKAgencyUpdate", exp.Message + exp.StackTrace)
        End Try

    End Sub
#End Region
#Region "STNKAgencyDelete"
    Public Sub STNKAgencyDelete(ByVal oAgency As Parameter.Agency)
        Dim objConnection As New SqlConnection(oAgency.strConnection)
        Dim objtrans As SqlTransaction
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objtrans = objConnection.BeginTransaction
            Dim params(1) As SqlParameter

            params(0) = New SqlParameter("@AgentId", SqlDbType.Char, 20)
            params(0).Value = oAgency.AgentId

            params(1) = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params(1).Value = oAgency.BranchId

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, STNK_AGENCY_DELETE, params)
            objtrans.Commit()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()

        Catch exp As Exception
            objtrans.Rollback()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            WriteException("Agent", "STNKAgencyDelete", exp.Message + exp.StackTrace)
        End Try
    End Sub
#End Region
End Class
