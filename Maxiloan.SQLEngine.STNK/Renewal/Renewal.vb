

#Region "Imports "
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
Imports Maxiloan.General

#End Region
Public Class Renewal : Inherits DataAccessBase
#Region "STNK Renewal"
    Public Function GetSTNKRequest(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
        Dim m_connection As New SqlConnection(oCustomClass.strConnection)
        Dim intLoop As Integer
        Dim objread As SqlDataReader
        Dim params(1) As SqlParameter
        Try
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@WhereBy", SqlDbType.VarChar, 500)
            params(1).Value = oCustomClass.WhereCond.Trim

            objread = SqlHelper.ExecuteReader(m_connection, CommandType.StoredProcedure, "spGetSTNKRenewal", params)
            If objread.Read Then
                With oCustomClass
                    .BranchId = objread("BranchID").ToString()
                    .AssetDescription = objread("Description").ToString()
                    .SerialNo1 = objread("SerialNo1").ToString()
                    .SerialNo2 = objread("SerialNo2").ToString()
                    .AssetType = objread("AssetTypeID").ToString()
                    .ManufacturingYear = objread("ManufacturingYear").ToString()
                    .OwnerName = objread("OwnerName").ToString()
                    .OwnerAddress = objread("OwnerAddress").ToString()
                    .LicensePlate = objread("LicensePlate").ToString()
                    .TaxDate = objread("TaxDate").ToString()
                    .Color = objread("attributecontent").ToString()
                    .agentname = objread("AGENTNAME").ToString()
                    .ReffNo = objread("RegisterRefNo").ToString()
                    .DP = CDbl(objread("DownPayment"))
                    .txtSearch = objread("Status").ToString
                    .RegisterNotes = objread("RegisterNotes").ToString
                    If objread("Requestdate").ToString = "" Then .RequestDate = Nothing Else .RequestDate = CDate(objread("Requestdate"))
                    .APAmountToAgent = CDbl(objread("APAmountToAgent"))
                    .AdmFee = CDbl(objread("AdministrationFee"))
                    .AgentFee = CDbl(objread("AgentFee"))
                    .OtherFee = CDbl(objread("OtherFee"))
                    .STNKFee = CDbl(objread("AgrementSTNKFee"))
                    .STNKFeePaid = CDbl(objread("STNKFeePaid"))
                    .TaxAmount = CDbl(objread("TaxAmount"))
                    .TaxRate = CDbl(objread("TaxRate"))
                    If objread("InvoiceDate").ToString = "" Then .InvoiceDate = Nothing Else .InvoiceDate = CDate(objread("InvoiceDate"))
                    .InvoiceNo = objread("InvoiceNo").ToString()
                    If objread("StatusDate").ToString = "" Then .StatusDate = Nothing Else .StatusDate = CDate(objread("StatusDate"))
                    .RequestNotes = objread("RequestNotes").ToString()
                    .CustomerID = objread("CustomerID").ToString()
                    .AgreementNo = objread("AgreementNo").ToString()
                    .ApplicationID = objread("ApplicationID").ToString()
                    .CustomerName = objread("Name").ToString()
                    .RequestNo = objread("RequestNo").ToString()
                    .CustReq = objread("CustReq").ToString().Trim
                End With
            End If
            Return oCustomClass
        Catch exp As Exception
            WriteException("GetSTNKRequest", "GetSTNKRequest", exp.Message + exp.StackTrace)
            Throw New Exception("Error On GetSTNKRequest")
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Function
#End Region
#Region "SaveSTNK"
    Public Function SaveSTNKRequest(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
        Dim m_connection As New SqlConnection(oCustomClass.strConnection)
        Dim intLoop As Integer
        Dim params(13) As SqlParameter
        Try
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter("@notes", SqlDbType.VarChar, 500)
            params(2).Value = oCustomClass.Notes

            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = oCustomClass.BusinessDate

            params(4) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
            params(4).Value = oCustomClass.LoginId

            params(5) = New SqlParameter("@AdmFee", SqlDbType.Decimal)
            params(5).Value = oCustomClass.admFee

            params(6) = New SqlParameter("@STNKDate", SqlDbType.DateTime)
            params(6).Value = oCustomClass.STNKDate

            params(7) = New SqlParameter("@BranchAgreement", SqlDbType.VarChar, 3)
            params(7).Value = oCustomClass.BranchAgreement.Replace("'", "")

            params(8) = New SqlParameter("@RenewalType", SqlDbType.VarChar, 10)
            params(8).Value = oCustomClass.RenewalType.Replace("'", "")

            params(9) = New SqlParameter("@CustReq", SqlDbType.Char, 1)
            params(9).Value = oCustomClass.CustReq.Replace("'", "")

            params(10) = New SqlParameter("@EstimasiPajak", SqlDbType.Decimal)
            params(10).Value = oCustomClass.EstimasiPajak

            params(11) = New SqlParameter("@EstimasiDenda", SqlDbType.Decimal)
            params(11).Value = oCustomClass.EstimasiDenda

            params(12) = New SqlParameter("@EstimasiJasa", SqlDbType.Decimal)
            params(12).Value = oCustomClass.EstimasiJasa

            params(13) = New SqlParameter("@EstimasiByLain", SqlDbType.Decimal)
            params(13).Value = oCustomClass.EstimasiByLain


            SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, "spSaveSTNKRenewal", params)
            oCustomClass.ErrorSave = False
            Return oCustomClass
        Catch exp As Exception
            oCustomClass.ErrorSave = True
            WriteException("spSaveSTNKRenewal", "spSaveSTNKRenewal", exp.Message + exp.StackTrace)
            Throw New Exception("Error On GetSTNKRequest")
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Function
    Public Function SaveEstimasiBiayaSTNK(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
        Dim m_connection As New SqlConnection(oCustomClass.strConnection)        
        Dim params(1) As SqlParameter
        Try
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()

            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(0).Value = oCustomClass.ApplicationID

            params(1) = New SqlParameter("@STNKFee", SqlDbType.Decimal)
            params(1).Value = oCustomClass.STNKFee


            SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, "spEstimasiBiayaSTNKSave", params)
            oCustomClass.ErrorSave = False
            Return oCustomClass
        Catch exp As Exception
            oCustomClass.ErrorSave = True
            WriteException("SaveEstimasiBiayaSTNK", "SaveEstimasiBiayaSTNK", exp.Message + exp.StackTrace)
            Throw New Exception("Error On SaveEstimasiBiayaSTNK")
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Function
#End Region
#Region "ReportSTNK"
    Public Function ReportSTNKRenewal(ByVal oCustomClass As Parameter.STNKRenewal) As DataSet
        Dim oData As New DataSet

        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
        params(0).Value = oCustomClass.ApplicationID

        params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
        params(1).Value = oCustomClass.BranchId.Replace("'", "")

        oData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spReportSTNKRenewal", params)

        Return oData
    End Function


#End Region

#Region "STNK Registration"
    Public Function SaveSTNKRegister(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
        Dim m_connection As New SqlConnection(oCustomClass.strConnection)
        Dim intLoop As Integer
        Dim params(10) As SqlParameter
        Try
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.RequestNo

            params(3) = New SqlParameter("@AgentID", SqlDbType.Char, 20)
            params(3).Value = oCustomClass.AgentID

            params(4) = New SqlParameter("@DownPayment", SqlDbType.Decimal)
            params(4).Value = oCustomClass.DP

            params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(5).Value = oCustomClass.BusinessDate

            params(6) = New SqlParameter("@ValueDate", SqlDbType.DateTime)
            params(6).Value = oCustomClass.ValueDate

            params(7) = New SqlParameter("@ReffNo", SqlDbType.Char, 20)
            params(7).Value = oCustomClass.ReffNo

            params(8) = New SqlParameter("@LoginID", SqlDbType.VarChar, 20)
            params(8).Value = oCustomClass.LoginId

            params(9) = New SqlParameter("@Notes", SqlDbType.Text)
            params(9).Value = oCustomClass.Notes

            params(10) = New SqlParameter("@AdminFee", SqlDbType.Decimal)
            params(10).Value = oCustomClass.AdmFee

            SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, "spSaveSTNKRegister", params)
            oCustomClass.ErrorSave = False
            Return oCustomClass
        Catch exp As Exception
            oCustomClass.ErrorSave = True
            WriteException("spSaveSTNKRegister", "spSaveSTNKRegister", exp.Message + exp.StackTrace)
            Throw New Exception("Error On GetSTNKRequest")
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Function

    Public Function CancelSTNKRegister(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
        Dim m_connection As New SqlConnection(oCustomClass.strConnection)
        Dim intLoop As Integer
        Dim params(3) As SqlParameter
        Try
            If m_connection.State = ConnectionState.Closed Then m_connection.Open()

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 20)
            params(2).Value = oCustomClass.RequestNo

            params(3) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(3).Value = oCustomClass.BusinessDate


            SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, "spCancelSTNKRegister", params)
            oCustomClass.ErrorSave = False
            Return oCustomClass
        Catch exp As Exception
            oCustomClass.ErrorSave = True
            WriteException("spCancelSTNKRegister", "spCancelSTNKRegister", exp.Message + exp.StackTrace)
            Throw New Exception("Error On GetSTNKRequest")
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Function
    Public Function STNKView(ByVal strConnection As String, _
                                   ByVal BranchId As String) As DataTable
        Dim params As SqlParameter
        Try
            params = New SqlParameter(PARAM_BRANCHID, SqlDbType.VarChar, 3)
            params.Value = BranchId


            Return SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, "spSTNKView", params).Tables(0)
        Catch exp As Exception
            WriteException("Agency", "STNKAgencyView", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region

#Region "STNK Invoicing"
    Public Function SaveSTNKInvoicing(ByVal oCustomClass As Parameter.STNKRenewal) As Parameter.STNKRenewal
        Dim m_connection As New SqlConnection(oCustomClass.strConnection)
        Dim intLoop As Integer
        Dim params(12) As SqlParameter
        Try

            If m_connection.State = ConnectionState.Closed Then m_connection.Open()

            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = oCustomClass.BranchId.Replace("'", "")

            params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(1).Value = oCustomClass.ApplicationID

            params(2) = New SqlParameter("@AssetSeqNo", SqlDbType.Int)
            params(2).Value = oCustomClass.AssetSeqNo

            params(3) = New SqlParameter("@RenewalSeqNo", SqlDbType.Int)
            params(3).Value = oCustomClass.RenewalSeqNo

            params(4) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(4).Value = oCustomClass.BusinessDate

            params(5) = New SqlParameter("@STNKFee", SqlDbType.Decimal)
            params(5).Value = oCustomClass.STNKFee

            params(6) = New SqlParameter("@OtherFee", SqlDbType.Decimal)
            params(6).Value = oCustomClass.OtherFee

            params(7) = New SqlParameter("@AgentFee", SqlDbType.Decimal)
            params(7).Value = oCustomClass.AgentFee

            params(8) = New SqlParameter("@TotalAP", SqlDbType.Decimal)
            params(8).Value = oCustomClass.TotalAP

            params(9) = New SqlParameter("@TaxAmount", SqlDbType.Decimal)
            params(9).Value = oCustomClass.TaxAmount

            params(10) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 20)
            params(10).Value = oCustomClass.InvoiceNo

            params(11) = New SqlParameter("@InvoiceDate", SqlDbType.DateTime)
            params(11).Value = oCustomClass.InvoiceDate

            params(12) = New SqlParameter("@AdmFee", SqlDbType.Decimal)
            params(12).Value = oCustomClass.AdmFee

            SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, "spSTNKInvoice", params)
            oCustomClass.ErrorSave = False
            Return oCustomClass
        Catch exp As Exception
            oCustomClass.ErrorSave = True
            WriteException("spSTNKInvoice", "spSTNKInvoice", exp.Message + exp.StackTrace)
            Throw New Exception("Error On GetSTNKRequest")
        Finally
            If m_connection.State = ConnectionState.Open Then m_connection.Close()
            m_connection.Dispose()
        End Try
    End Function
#End Region

#Region "STNK Inquiry"
    Public Function STNKRequestInquiryPaging(ByVal customclass As Parameter.STNKRenewal) As Parameter.STNKRenewal
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter(PARAM_CURRENTPAGE, SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter(PARAM_PAGESIZE, SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter(PARAM_WHERECOND, SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter(PARAM_SORTBY, SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter(PARAM_TOTALRECORD, SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            params(5) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
            params(5).Value = customclass.BusinessDate

            customclass.listData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, customclass.SpName, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)

            Return customclass
        Catch exp As Exception
            WriteException("STNKRenewal", "STNKRequestInquiryPaging", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function STNKInquiryReport(ByVal customclass As Parameter.STNKRenewal) As Parameter.STNKRenewal
        Dim spName As String
        Dim params As SqlParameter
        params = New SqlParameter("@SearchBy", SqlDbType.VarChar, 2000)
        params.Value = customclass.Search

        Try
            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, customclass.SpName, params)
            Return customclass

        Catch exp As Exception
            WriteException("STNKRenewal", "STNKRenewal", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "STNKRenewalLetter"
    'Created By Parang june 21th 2004

    Public Function STNKRenewalUpdatePrint(ByVal customclass As Parameter.STNKRenewal) As Boolean
        Dim objConnection As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction
        Dim intloop As Integer

        Dim params(3) As SqlParameter
        If objConnection.State = ConnectionState.Closed Then objConnection.Open()
        objtrans = objConnection.BeginTransaction
        Try
            With customclass.listData
                For intloop = 0 To customclass.listData.Rows.Count - 1

                    params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 2000)
                    params(0).Value = .Rows(intloop).Item("BranchID")
                    params(1) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 2000)
                    params(1).Value = .Rows(intloop).Item("ApplicationID")
                    params(2) = New SqlParameter("@BusinessDate", SqlDbType.VarChar, 2000)
                    params(2).Value = customclass.BusinessDate
                    params(3) = New SqlParameter("@LoginID", SqlDbType.VarChar, 2000)
                    params(3).Value = customclass.LoginId
                    SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, "spSTNKRenewalLetterUpdatePrint", params)

                Next
            End With
            objtrans.Commit()
            Return True
        Catch ex As Exception
            objtrans.Rollback()
            objConnection.Dispose()
            Return False
        Finally
            objConnection.Dispose()
        End Try

    End Function
    Public Function GetLastDate(ByVal strConnection As String) As String
        Dim params As SqlParameter
        Dim strLastDate As String
        params = New SqlParameter("@LastDate", SqlDbType.VarChar, 10)
        params.Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(strConnection, CommandType.StoredProcedure, "spSTNKRenewalLetterLastDate", params)
            strLastDate = CType(params.Value, String)
            Return strLastDate
        Catch ex As Exception
            WriteException("STNKRenewal", "STNKRenewal", ex.Message + ex.StackTrace)
        End Try
    End Function




#End Region

#Region "STNKRenewalIncome"
    Public Function GetReportSTNKRenewalIncome(ByVal strConnection As String, ByVal BranchID As String, ByVal BusinessDate As String) As DataSet
        'Dim oReturnValue As New Entities.GeneralPaging

        Dim dtsListDataReport As New DataSet
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@BusinessDate", SqlDbType.DateTime)
        params(0).Value = BusinessDate
        params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(1).Value = BranchID


        Try
            dtsListDataReport = SqlHelper.ExecuteDataset(strConnection, CommandType.StoredProcedure, "spSTNKIncomeRpt", params)
            Return dtsListDataReport
        Catch exp As Exception
            WriteException("STNKRenewal", "GetReportSTNKRenewalIncome", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

End Class
